/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from "$/ox";

import type { App, AppLauncher, AppLauncherModule } from "$/io.ox/core/desktop";
import { manifestManager } from "$/io.ox/core/manifests";

import { fun, jpromise } from "@/io.ox/office/tk/algorithms";
import { onceMethod } from "@/io.ox/office/tk/objects";
import { LogLevel, globalLogger } from "@/io.ox/office/tk/utils/logger";

import type { CoreApp } from "@/io.ox/office/baseframework/app/appfactory";

// types ======================================================================

/**
 * Application configuration passed to new application instances.
 */
export interface AppFactoryConfig {

    /**
     * Unique module name of the application. Used as cache key for the
     * respective application factory.
     */
    moduleName: string;

    /**
     * If set to `true`, the application can be launched once only. Relaunching
     * the same application will simply reactivate the existing application
     * instance. Default value is `false`.
     */
    singletonApp?: boolean;
}

/**
 * Application configuration passed to new application instances.
 */
export interface AbstractLaunchConfig { }

/**
 * Shape of a code module exporting an application factory class. The factory
 * class must subclass `AbstractAppFactory`, and is expected to be the default
 * export.
 */
export interface AppFactoryModule<FactoryT extends AbstractAppFactory<any>> {
    default: CtorType<FactoryT>;
}

// private types --------------------------------------------------------------

/**
 * Type of the entries in the global application module registry.
 */
interface FactoryRegistryEntry {

    /**
     * Returns a new application factory.
     */
    createAppFactory(): Promise<AbstractAppFactory<any>>;

    /**
     * The loader callback function expected by `ox.launch()`.
     */
    appLoaderFn(): Promise<AppLauncherModule>;
}

// globals ====================================================================

/**
 * Global map with lazy callback functions creating application factories,
 * mapped by module identifiers.
 */
const appFactoryRegistry = new Map<string, FactoryRegistryEntry>();

// private functions ==========================================================

/**
 * Returns the entry for the specified application module from the factory
 * registry.
 *
 * @param moduleName
 *  The unique module identifier of the application factory.
 *
 * @returns
 *  The registry entry for the specified application module.
 */
function getRegistryEntry(moduleName: string): FactoryRegistryEntry {

    // get the factory function that will return the application factory
    const entry = appFactoryRegistry.get(moduleName);
    if (entry) { return entry; }

    globalLogger.error(`$badge{launch} no application factory found for module "${moduleName}"`);
    throw new Error(`application factory for "${moduleName}" not found`);
}

// public functions ===========================================================

/**
 * Global registration for a new application module type, needed to be able to
 * launch application from everywhere, and to have a consistent central place
 * for loading manifest plugins for each application type.
 *
 * @param moduleName
 *  The identifier of the application module.
 *
 * @param factoryLoader
 *  A callback function what will be invoked exactly once, and must load and
 *  return the application factory class. The return value is expected to be a
 *  Promise fulfilling with a source code module with `default` export.
 *
 * @returns
 *  The application loader function imitating a source code module with default
 *  export, intended to be attached to the `load()` callback function of an
 *  application instance.
 */
export function registerAppModule<FactoryT extends AbstractAppFactory<any>>(
    moduleName: string,
    factoryLoader: FuncType<Promise<AppFactoryModule<FactoryT>>>
): FuncType<Promise<AppLauncherModule>> {

    if (moduleName in appFactoryRegistry) {
        globalLogger.error(`$badge{launch} multiple registration of module "${moduleName}"`);
    } else {
        globalLogger.info(`$badge{launch} registering application module "${moduleName}"`);
    }

    // a factory function for the application factory (so meta...), used for lazy instantiation
    const createAppFactory = fun.once(async () => {
        globalLogger.trace(`$badge{launch} loading manifest plugins for module "${moduleName}"`);
        await manifestManager.loadPluginsFor(moduleName);
        globalLogger.trace(`$badge{launch} loading application factory for module "${moduleName}"`);
        const { default: AppFactoryClass } = await factoryLoader();
        return new AppFactoryClass();
    });

    // wrap it into a function that returns a module-like object with "default export"
    const appLoaderFn = async (): Promise<AppLauncherModule> => ({ default: await createAppFactory() });

    // create an entry in the global registry
    appFactoryRegistry.set(moduleName, { createAppFactory, appLoaderFn });

    // return the loader function expected by `ox.launch()`
    return appLoaderFn;
}

/**
 * Returns a loader function that is intended to be passed to the global
 * `ox.launch()` function.
 *
 * @param moduleName
 *  The unique module identifier of the application factory.
 *
 * @returns
 *  A function intended to be passed to `ox.launch()`.
 */
export function getAppLoaderFn(moduleName: string): FuncType<Promise<AppLauncherModule>> {
    return getRegistryEntry(moduleName).appLoaderFn;
}

/**
 * Fetches the source code of the specified application.
 *
 * @param moduleName
 *  The unique module identifier of the application factory.
 */
export const prefetchApp = fun.memoize((moduleName: string): void => {

    // start fetching after a reasonable delay
    window.setTimeout(() => {
        // measure and log duration needed to load the sources
        jpromise.floating(globalLogger.takeTime(`$badge{launch} prefetching module "${moduleName}"...`, async () => {
            const registryEntry = getRegistryEntry(moduleName);
            const appFactory = await registryEntry.createAppFactory();
            await appFactory.loadAppClass();
        }, LogLevel.INFO));
    }, 2500);
}, fun.identity);

/**
 * Tries to launch an application using an existing application factory.
 *
 * @param moduleName
 *  The unique module identifier of the application factory.
 *
 * @param launchConfig
 *  The launch configuration to be passed to the application constructor.
 *
 * @returns
 *  A promise that will fulfil with the launched application.
 */
export async function launchApp(moduleName: string, launchConfig: AbstractLaunchConfig): Promise<App> {
    return await ox.launch(getAppLoaderFn(moduleName), launchConfig);
}

// class AbstractAppFactory ===================================================

/**
 * A factory for new Documents applications of a specific module type.
 */
export abstract class AbstractAppFactory<AppClassT> implements AppLauncher {

    // properties -------------------------------------------------------------

    /**
     *  The unique module name of the application.
     */
    readonly moduleName: string;

    /**
     * This method exists to fulfil the API requirements of the global launcher
     * function `ox.launch()` which expects an object with a `getApp` function
     * that will be called unbound.
     */
    readonly getApp: AppLauncher["getApp"];

    // constructor ------------------------------------------------------------

    protected constructor(factoryConfig: AppFactoryConfig) {

        // properties
        this.moduleName = factoryConfig.moduleName;
        globalLogger.info(`$badge{launch} constructing application factory for module "${this.moduleName}"`);

        // wrap the `getApp` function into `fun.once` for singleton applications
        const getApp = (launchConfig?: AbstractLaunchConfig): CoreApp => this.implCreateApp(launchConfig);
        this.getApp = factoryConfig.singletonApp ? fun.once(getApp) : getApp;
    }

    // public methods ---------------------------------------------------------

    /**
     * Calls the implementation method `implLoadAppClass` once, and caches and
     * returns the resulting promise.
     *
     * @returns
     *  The cached return value of the method `implLoadAppClass()`.
     */
    @onceMethod
    loadAppClass(): Promise<AppClassT> {
        return this.implLoadAppClass();
    }

    // abstract API -----------------------------------------------------------

    /**
     * Implementations may load additional application code. The loader will be
     * called early when launching the first application, and will be cached
     * for subsequent application launches. Additionally, this method will be
     * called from the global function `prefetchAppSource()`.
     *
     * @returns
     *  A promise that will fulfil with custom implementation-defined contents
     *  (generic type `AppClassT`).
     */
    protected abstract implLoadAppClass(): Promise<AppClassT>;

    /**
     * Implementations must return a new application instance for the passed
     * launch configuration.
     *
     * For regular applications (constructor parameter "singletonApp" not set),
     * this method will be called for every launch action.
     *
     * For singleton applications (constructor parameter "singletonApp" set),
     * this method will be called once per module type only, and the returned
     * application will be cached and returned for subsequent launches.
     *
     * @param [launchConfig]
     *  The configuration to be passed to the application constructor.
     *
     * @returns
     *  A new application instance for the passed launch configuration.
     */
    protected abstract implCreateApp(launchConfig?: AbstractLaunchConfig): CoreApp;
}
