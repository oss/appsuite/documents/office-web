/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str, map, dict } from "@/io.ox/office/tk/algorithms";

// class TranslationDictionary ================================================

/**
 * Wrapper for translated string mappings loaded from JSON resource modules.
 */
export class TranslationDictionary {

    // properties -------------------------------------------------------------

    readonly #db = new Map<string, { re: RegExp; translation: string }>();
    readonly #cache = new Map<string, string>();

    // constructor ------------------------------------------------------------

    constructor(strings?: Dict<string>) {
        if (strings) {
            dict.forEach(strings, (translation, key) => {
                this.#db.set(key, { re: new RegExp(`^${key}$`, "i"), translation });
            });
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the translation of the passed text.
     *
     * @param text
     *  The original (English) text to be translated.
     *
     * @returns
     *  The translated and adjusted text. If no translation is available, the
     *  passed original text will be adjusted and returned.
     */
    translate(text: string): string {

        // try to resolve exact cached translation (with replaced placeholders etc.)
        return map.upsert(this.#cache, text.toLowerCase(), () => {

            // the resulting translated text
            let translation = "";

            // try to find a database entry for the base word
            for (const entry of this.#db.values()) {

                // test whether the current entry matches the passed text
                const matches = entry.re.exec(text);
                if (matches) {

                    // matching entry found, translate it and replace the placeholders
                    translation = entry.translation;
                    for (let index = matches.length - 1; index > 0; index -= 1) {
                        translation = translation.replace(new RegExp(`\\$${index}`, "g"), matches[index]);
                    }

                    // exit the loop
                    break;
                }
            }

            // fallback to passed text
            return translation || str.capitalizeWords(text);
        });
    }

    /**
     * Visits all translations contained in this dictionary.
     *
     * @param callback
     *  The callback function to be invoked for each translated entry in this
     *  dictionary.
     *
     * @param [context]
     *  The calling context for the passed callback function.
     */
    forEach(callback: (translation: string, key: string) => void, context?: object): void {
        this.#db.forEach((entry, key) => callback(entry.translation, key), context);
    }
}
