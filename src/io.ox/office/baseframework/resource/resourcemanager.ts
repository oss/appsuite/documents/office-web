/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, re, map, dict, pick, jpromise } from "@/io.ox/office/tk/algorithms";
import { EObject } from "@/io.ox/office/tk/objects";
import { LOCALE_DATA, localeLogger, localeDataRegistry } from "@/io.ox/office/tk/locale";

import { TranslationDictionary } from "@/io.ox/office/baseframework/resource/translationdictionary";
import type { BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";

// types ======================================================================

/**
 * Configuration for a localized JSON resource.
 */
export interface ResourceConfig {

    /**
     * The base name of the module containing the JSON resources, i.e. the
     * directory name of a code module in `src/io.ox/office` (e.g. "text",
     * "spreadsheet", or "editframework").
     */
    module: "baseframework" | "editframework" | "textframework" | "text" | "spreadsheet" | "presentation";

    /**
     * The name of the JSON resources, i.e. the name of the directory inside
     * the specified module's `resource` directory.
     */
    name: string;

    /**
     * If set to `true`, the data mapped by the generic language name (e.g.
     * "en"), and the data mapped by the exact locale identifier (e.g. "en_GB")
     * will be merged. Default value is `false`.
     */
    merge?: boolean;

    /**
     * Whether to fall-back to the default language of a given region, if the
     * exact locale cannot be resolved. By default, the default resource data
     * of the given language will be loaded. Example: With this flag set, the
     * locale "en_DE" will fall-back to the resource "de_DE" instead of "en".
     */
    preferRegion?: boolean;
}

/**
 * Type mapping for the events emitted by `BaseResourceManager` instances.
 */
export interface BaseResourceManagerEventMap {

    /**
     * Will be emitted after the UI locale was changed, and the resource
     * manager has updated itself.
     */
    "change:locale": [];
}

// constants ==================================================================

// regular expression to split language and optional region from a locale identifier
const LOCALE_ID_RE = /^([a-z]+)(?:_([A-Z]+))?$/;

// globals ====================================================================

// static cache for JSON resources already loaded, mapped by resource module name and locale name
const resourceCache = new Map<string, Promise<Dict>>();

// private functions ==========================================================

/**
 * Tries to load the JSON resource data for the passed language and region.
 *
 * @param resConfig
 *  The configuration for the resource.
 */
async function fetchResource(resConfig: ResourceConfig, language: string, region: string): Promise<Dict> {

    try {

        const { module, name } = resConfig;
        const resourceId = `${module}/${name}`;

        // load the JSON source file for the passed language (the Rollup plugin "dynamic-import-vars" expects relative
        // paths with simple path components, see https://github.com/rollup/plugins/tree/master/packages/dynamic-import-vars)
        localeLogger.log(`$badge{ResourceManager} fetchResource: loading resource "${resourceId}" for language "${language}"`);
        const jsonData = pick.prop(await import(`../../${module}/resource/${name}/${language}.json`), "default");

        // if the file consists of a simple string, use it as fall-back locale
        if (is.string(jsonData) && LOCALE_ID_RE.test(jsonData)) {
            localeLogger.log(`$badge{ResourceManager} fetchResource: resource forwards to language "${jsonData}"`);
            return await importResource(resConfig, jsonData);
        }

        // JSON data must be a dictionary
        if (!is.dict(jsonData)) {
            localeLogger.error(`$badge{ResourceManager} fetchResource: invalid JSON data in resource "${resourceId}"`);
            throw new SyntaxError("invalid JSON data");
        }

        // validate resource data (must be a map with language and/or local identifiers
        const keyPattern = new RegExp(`^${re.escape(language)}(_[A-Z]+)?$`);
        for (const key in jsonData) {
            if (!keyPattern.test(key)) {
                localeLogger.error(`$badge{ResourceManager} fetchResource: invalid map key "${key}" in resource "${resourceId}"`);
            }
            if (!is.dict(jsonData[key])) {
                localeLogger.error(`$badge{ResourceManager} fetchResource: object expected for key "${key}" in resource "${resourceId}"`);
                throw new SyntaxError("invalid JSON data");
            }
        }

        // default data for language code only must exist
        const langData = jsonData[language];
        if (!is.dict(langData)) {
            localeLogger.error(`$badge{ResourceManager} fetchResource: missing default entry for "${language}" in resource "${resourceId}"`);
            throw new SyntaxError("invalid JSON data");
        }

        // DOCS-1600: fall-back to default language of region if specified
        let locale = `${language}_${region}`;
        const localeData = jsonData[locale];
        if (!localeData && resConfig.preferRegion) {
            const defLang = localeDataRegistry.getDefaultLanguage(region);
            if (defLang && (defLang !== language)) {
                locale = `${defLang}_${region}`;
                localeLogger.log(`$badge{ResourceManager} fetchResource: missing region, fall-back to "${locale}"`);
                return await importResource(resConfig, locale);
            }
        }

        // if existing, locale-specific data entry must be a dictionary
        if ((localeData !== undefined) && !is.dict(localeData)) {
            localeLogger.error(`$badge{ResourceManager} fetchResource: invalid entry type for "${locale}" in resource "${resourceId}" (dictionary expected)`);
            throw new SyntaxError("invalid JSON data");
        }

        // merge region specific resources (e.g. "en_GB") over generic language resources (e.g. "en")
        localeLogger.log(`$badge{ResourceManager} fetchResource: resource data for "${locale}" loaded successfully`);
        return resConfig.merge ? { ...langData, ...localeData } : (localeData ?? langData);

    } catch (err) {

        // on error, fall back to en_US (unless English is already the requested language)
        if (language === "en") {
            localeLogger.log("$badge{ResourceManager} fetchResource: resource not found, giving up");
            throw err;
        }
        localeLogger.warn('$badge{ResourceManager} fetchResource: resource file not found, trying to load resource for "en_US"');
        return await fetchResource(resConfig, "en", "US");
    }
}

// public functions ===========================================================

/**
 * Loads the specified localized JSON resource for the current UI locale. If
 * there is no JSON module available for the current language, tries to fall
 * back to the language code "en". All JSON resources loaded once will be
 * cached internally, and will be returned immediately when calling this
 * function again.
 *
 * @param resConfig
 *  The configuration for the resource to be loaded.
 *
 * @param [locale]
 *  If specified, the resource data for the specified locale will be loaded
 *  instead for the current UI locale. MUST be a complete locale identifier
 *  with language and region, separated by an underscore character (e.g.
 *  "en_GB"), or dash (e.g. "en-GB").
 *
 * @returns
 *  A promise that will fulfil with the JSON data loaded from the specified
 *  resource file, or that will be rejected on any error (e.g., resource file
 *  not found, or invalid JSON data).
 */
export async function importResource(resConfig: ResourceConfig, locale?: string): Promise<Dict> {

    // get the name of the locale used to load the resource data
    locale = (locale ?? LOCALE_DATA.lc).replace(/-/g, "_");
    const matches = LOCALE_ID_RE.exec(locale);
    if (!matches?.[2]) {
        localeLogger.error(`$badge{ResourceManager} importResource: invalid locale identifier "${locale}"`);
        throw new Error("invalid locale identifier");
    }

    // the (pending or settled) promise is stored in the cache
    const cacheKey = `${resConfig.module}/${resConfig.name}/${locale}`;
    return await map.upsert(resourceCache, cacheKey, () => fetchResource(resConfig, matches[1], matches[2]));
}

// class BaseResourceManager ==================================================

/**
 * Storage for various localized resources, mapped by custom resource keys. A
 * resource is a JSON dictionary of arbitrary data (usually strings).
 *
 * The resource modules must be stored as JSON source files inside a `resource`
 * directory of a code submodule, containing the plain language code as file
 * name, for example `src/io.ox/office/[MODULE]/resource/[ID]/[LANGUAGE].json`.
 *
 * The resource file is a JSON file containing a dictionary with locale names
 * as property keys, and the resource data as property values, for example:
 *
 * `{ "en": {...}, "en_US": {...}, "en_GB": {...} }`
 *
 * The exact locale identifier (e.g. "en_GB") will be preferred over data
 * mapped by a generic language name (e.g. "en"). The latter will be used as
 * fall-back in case no data exists for the exact locale identifier (e.g., the
 * data mapped by "en" will be loaded for the locale "en_CA" in the example
 * above).
 *
 * Instances will trigger a "change:locale" event after the UI locale has been
 * changed, *and* the new resources have been loaded successfully.
 */
export class BaseResourceManager<
    DocAppT extends BaseApplication,
    ConfigDictT extends Dict<ResourceConfig>
> extends EObject<BaseResourceManagerEventMap> {

    /**
     * The document application owning this instance.
     */
    readonly docApp: DocAppT;

    readonly #configs = new Map<KeysOf<ConfigDictT>, ResourceConfig>();
    readonly #store = new Map<string, Dict>();

    // constructor ------------------------------------------------------------

    protected constructor(docApp: DocAppT, configs: ConfigDictT) {
        super({ _kind: "root" });

        this.docApp = docApp;

        // insert all passed resource configurations into the map
        dict.forEach(configs, (config, key) => this.#configs.set(key, config));

        // reload all registered resources when UI locale has changed
        this.listenToGlobal("change:locale", jpromise.wrapFloating(async () => {
            await this.loadResources();
            this.trigger("change:locale");
        }));
    }

    // public methods ---------------------------------------------------------

    /**
     * Loads all JSON resources that have been registered in the constructor.
     * Will be called automatically by the application launching process. Can
     * be overwritten by subclasses to run additional initialization tasks.
     *
     * @returns
     *  A promise that will fulfil when all resources have been loaded.
     */
    async loadResources(): Promise<void> {
        await Promise.all(Array.from(this.#configs, async ([key, config]) => {
            this.#store.set(key, await importResource(config));
        }));
    }

    /**
     * Returns the localized resource data for the specified resource key.
     *
     * @param resKey
     *  The resource key.
     *
     * @returns
     *  The resource data stored for the specified resource key.
     */
    getResource(resKey: KeysOf<ConfigDictT>): Dict {
        const resData = this.#store.get(resKey);
        if (resData) { return resData; }
        // catch typos in `resKey` in unchecked JS code
        throw new Error(`invalid resource key "${resKey}"`);
    }

    /**
     * Loads the specified JSON resource module with translated strings for the
     * current UI locale. The resource must contain dictionaries for the locale
     * identifiers, mapping the original strings to the translated strings.
     *
     * @param resKey
     *  The resource key.
     *
     * @returns
     *  The translation dictionary for the current GUI locale.
     */
    getTranslations(resKey: KeysOf<ConfigDictT>): TranslationDictionary {
        const resData = this.getResource(resKey);
        if (dict.every(resData, is.string)) { return new TranslationDictionary(resData as Dict<string>); }
        localeLogger.error("$badge{ResourceManager} getTranslations: invalid data in translation dictionary");
        throw new Error("invalid data in translation dictionary");
    }
}
