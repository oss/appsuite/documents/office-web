/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from '$/ox';
import $ from '$/jquery';
import gt from 'gettext';
import { locationHash } from '$/url';

import Theming from '$/io.ox/core/theming/main';
import ext from '$/io.ox/core/extensions';
import addLauncher from '$/io.ox/core/main/addLauncher';

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import { SMALL_DEVICE, setElementAttribute, createDiv, createIcon, createButton, createInput, setInputSelection } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { useEditorThemeColors } from "@/io.ox/office/settings/api";
import { DOCUMENTS_SETTINGS_TITLE } from '@/io.ox/office/settings/view/labels';

import { emptySearchToolbar } from '@/io.ox/office/portal/action/portalsearchaction';

import { AppType, isPortalAvailable, parseModulePath, getGlobalAppConfig } from "@/io.ox/office/baseframework/utils/apputils";
import { getCurrentEditorCoreApp, getCurrentEditorApp } from "@/io.ox/office/baseframework/app/appfactory";
import { DOCUMENTS_COLLABORA } from "@/io.ox/office/baseframework/utils/baseconfig";

import '$/io.ox/backbone/views/search'; // side effect import for css rules for the search field

// private functions ==========================================================

/**
 * Enables specific extensions in the extension point and disables all others.
 *
 * @param {Point} point
 *  The extension point to be manipulated.
 *
 * @param {string[]} ids
 *  The identifiers of the extensions to be enabled. All existing extensions
 *  not mentioned in this list will be disabled.
 */
function enableExtensions(point, ...ids) {
    // `point.all` iterates all items (also disabled), in difference to `point.each`
    point.all().forEach(ext => point.toggle(ext.id, ids.includes(ext.id)));
}

/**
 * Disables specific extensions in the extension point and enables all others.
 *
 * @param {Point} point
 *  The extension point to be manipulated.
 *
 * @param {string[]} ids
 *  The identifiers of the extensions to be disabled. All existing extensions
 *  not mentioned in this list will be enabled.
 */
function disableExtensions(point, ...ids) {
    // `point.all` iterates all items (also disabled), in difference to `point.each`
    point.all().forEach(ext => point.toggle(ext.id, !ids.includes(ext.id)));
}

// class FileNameField ========================================================

class FileNameField {

    constructor() {

        this.el = createDiv("file-name-field");

        this.docApp = null;
        this.docController = null;

        this._input = this.el.appendChild(createInput({
            classes: "form-control",
            placeholder: gt("Document name")
        }));

        // select entire text when receiving focus
        this._input.addEventListener("focus", () => setInputSelection(this._input));

        // change filename if pressing Enter key in textfield
        this._input.addEventListener("change", jpromise.wrapFloating(async () => {
            const fileName = this._getValue();
            if (fileName) {
                this._input.value = fileName; // put trimmed value into the textfield
                try {
                    await this.docController.executeItem("document/rename", fileName);
                } catch {
                    this._resetValue();
                }
            } else {
                this._resetValue();
            }
        }));

        // immediately save current text in application UI (for quit or tab close)
        this._input.addEventListener("input", () => {
            this.docController.executeItem("document/rename", this._getValue(), { preview: true });
        });

        // handle Escape key in textfield
        this._input.addEventListener("keydown", event => {
            if (event.key === "Escape") {
                this.docController.cancelItem("document/rename");
                this._resetValue();
            }
        });

        // initialize event handlers for current editor application
        this.initializeApp();
    }

    // public methods ---------------------------------------------------------

    initializeApp() {
        this.#initializeApp(getCurrentEditorCoreApp());
    }

    // private methods --------------------------------------------------------

    #initializeApp(coreApp) {

        // application instance may still be partially uninitialized
        this.docApp = null;
        this.docController = null;
        this.el.classList.add("hidden");
        this._input.readOnly = true;
        this._input.value = "";

        // wait for complete initialization of application members
        coreApp?.onInit(docApp => {

            // update enabled state according to controller
            this.docApp = docApp;
            this.docController = docApp.docController;

            // control is hidden during document import
            docApp.waitForImportStart(() => {
                this._resetValue();
                this.el.classList.remove("hidden");
            });

            // enable editing filename when import succeeds
            docApp.waitForImport(() => {
                this._resetValue();
                this._updateState();
                this.docController.on("change:items", () => this._updateState());
            });

            // control is hidden during document import
            docApp.waitForImport(() => {
                this._resetValue();
                this.el.classList.remove("hidden");
            });

            // update document name from external rename actions
            // DOCS-3682: do not use "change:document:title" event (sent by floating mail app inside OX Documents)
            docApp.on("docs:filename", () => this._resetValue());

            // DOCS-1930: after a reload, the new application must be used
            ox.once(`docs:reload:done:${coreApp.id}`, newCoreApp => this.#initializeApp(newCoreApp));
        });
    }

    /*private*/ _getValue() {
        return this._input.value.trim();
    }

    /*private*/ _resetValue() {
        this._input.value = this.docApp.getShortFileName();
    }

    /*private*/ _updateState() {
        this._input.readOnly = !this.docController.isItemEnabled("document/rename");
    }
}

// public functions ===========================================================

/**
 * Performs all initialization steps for the global topbar in Documents child
 * tabs.
 */
export function initializeTopBar() {

    // collect extension points
    const appControlPoint = ext.point('io.ox/core/appcontrol');
    const leftSectionPoint = ext.point('io.ox/core/appcontrol/left');
    const rightSectionPoint = ext.point('io.ox/core/appcontrol/right');
    const helpMenuPoint = ext.point('io.ox/core/appcontrol/right/help');
    const accountMenuPoint = ext.point('io.ox/core/appcontrol/right/account');

    // hide standard controls in the topbar
    disableExtensions(appControlPoint, 'search');
    disableExtensions(leftSectionPoint, 'launcher', 'quicklauncher');
    disableExtensions(rightSectionPoint, 'notifications', 'ai-chat');
    enableExtensions(accountMenuPoint, 'logout');

    // whether the active application is a Documents portal
    let appIsPortal = null;
    // type of the current application
    let appType = null;
    // DOCS-2760: whether the Settings dialog is currently open
    let settingsDialogOpen = false;

    // selects using theme colors or Documents application colors
    function refreshTopBarTheme() {
        const useAppColor = !appIsPortal && !!appType && (appType !== AppType.PRESENTER) && useEditorThemeColors();
        setElementAttribute(document.documentElement, "data-docs-app-theme", useAppColor ? appType : null);
    }

    // shows the "Documents Settings" modal dialog
    function showSettingsDialog() {

        // DOCS-2760: do not open the Settings dialog multiple times
        if (settingsDialogOpen) { return; }

        // hide the asynchronous code from caller
        jpromise.invokeFloating(async () => {
            settingsDialogOpen = true;
            try {
                const { SettingsDialog } = await import('@/io.ox/office/settings/view/dialog/settingsdialog');
                await new SettingsDialog().show();
            } catch (err) {
                globalLogger.exception(err);
            } finally {
                // restore browser selection when closing the dialog
                getCurrentEditorApp()?.docModel.getSelection().restoreBrowserSelection({ forceWhenReadOnly: true });
            }
            settingsDialogOpen = false;
        });
    }

    // main entry point for the custom Documents topbar
    appControlPoint.extend({
        id: 'documents',
        index: 1000,
        draw() {

            // require multitab mode on large devices
            if (SMALL_DEVICE || !ox.office?.openInTabs) { return; }

            // create container node for additional controls (button, filename, search)
            const topBarNode = this;
            const rightSectionNode = topBarNode.find('#io-ox-toprightbar');
            const docsBarNode = $('<div id="io-ox-documentsbar">');
            rightSectionNode.before(docsBarNode);

            // "Back to portal" button and textfield for filename
            let portalButton = null;
            const fileNameField = new FileNameField();
            // cache for the svg inside the respin button (DOCS-4456)
            let refreshIcon = null;

            // helper function that refreshes the entire topbar according to the active application
            function refreshTopBar() {

                const appSpec = parseModulePath(locationHash("app") || "");
                if (!appSpec) { return; }
                appIsPortal = appSpec.isPortal;
                appType = appSpec.appType;

                // update settings in global `ox.office` object
                Object.assign(ox.office, {
                    isOffice: true,
                    portal: appIsPortal,
                    type: appType
                });

                // initialize theming
                refreshTopBarTheme();
                Theming.restoreCurrent();

                // special appearance of editor applications
                topBarNode.toggleClass("docs-small-topbar", !appIsPortal);

                // remove old form controls
                portalButton?.remove();
                fileNameField.el.remove();

                // enable/disable extensions according to application type
                if (appIsPortal) {
                    disableExtensions(leftSectionPoint, 'launcher', 'back-to-portal', 'quicklauncher');
                    disableExtensions(rightSectionPoint, 'notifications', 'settings-dropdown', 'ai-chat');
                    const allHelpMenuPoints = ['help', 'feedback', 'divider-first', 'get-started', 'divider-second', 'about'];
                    if (DOCUMENTS_COLLABORA) { allHelpMenuPoints.shift(); } // #46: no online help for Collabora
                    enableExtensions(helpMenuPoint, ...allHelpMenuPoints);
                    // not using "display: block" at the search field, that is set, when a document is closed and the portal is opened again (DOCS-4415)
                    const searchField = topBarNode.find('.search-view');
                    if (searchField.css('display') === 'block') { searchField.css('display', ''); } // "display: flex" is active again via css
                } else {
                    disableExtensions(leftSectionPoint, 'launcher', 'quicklauncher');
                    disableExtensions(rightSectionPoint, 'refresh-mobile', 'notifications', 'settings-dropdown', 'ai-chat');
                    const allHelpMenuPoints = ['help', 'feedback', 'divider-first', 'about'];
                    if (DOCUMENTS_COLLABORA) { allHelpMenuPoints.shift(); } // #46: no online help for Collabora
                    enableExtensions(helpMenuPoint, ...allHelpMenuPoints);

                    if (appType !== AppType.PRESENTER) {
                        // recreate the "Back to portal" button for editor application (type may have changed)
                        portalButton = createButton({
                            action: "app/quit",
                            classes: "btn-toolbar btn-topbar f6-target",
                            icon: getGlobalAppConfig(appType)?.appIcon ?? null,
                            disabled: !isPortalAvailable(appType), // DOCS-4350: portal application may not be available
                            // tooltip: gt("Home"),
                            click: () => jpromise.floating(getCurrentEditorCoreApp({ allowWopi: true })?.quit())
                        });
                        docsBarNode.append(portalButton, fileNameField.el);
                    }

                    fileNameField.initializeApp();
                }

                // caching the respin button, because it is already cached in core in "initRefreshAnimation" (DOCS-4456)
                if (appIsPortal) {
                    refreshIcon = refreshIcon || rightSectionNode.find('#io-ox-refresh-icon svg');
                    if (refreshIcon.length === 0) { refreshIcon = null; }
                }

                // repaint the right section with all dropdown menus according to extension configuration
                const rightTaskBar = rightSectionNode.find('.taskbar').empty();
                rightSectionPoint.invoke('draw', rightTaskBar);

                // DOCS-4781: No "notification" button in portals -> another button must be prepared for pressing "tab" key
                if (appIsPortal && rightTaskBar.find("button:not([tabindex='-1'])").length === 0 && rightTaskBar.find("button[tabindex='-1']").length > 0) {
                    rightTaskBar.find("button[tabindex='-1']").first().removeAttr("tabindex");
                }

                // using the cached refresh icon, so that the animation still works (DOCS-4456)
                if (appIsPortal && refreshIcon) { rightSectionNode.find('#io-ox-refresh-icon svg').replaceWith(refreshIcon); }

                // clearing search field, when user clicks on documents bar
                docsBarNode.click(function (evt) {
                    if ($(evt.target).is('#io-ox-documentsbar')) { emptySearchToolbar(appType); }
                });

            }

            // immediately refresh topbar, and repeat for every application start/resume
            refreshTopBar();
            ox.on('app:start app:resume', refreshTopBar);

            // update theme colors when configuration has changed
            globalEvents.on("change:config:docs", refreshTopBarTheme);
        }
    });

    // simple "Settings" button that opens a modal dialog instead of the Settings app
    rightSectionPoint.extend({
        id: 'documents-settings',
        after: "settings-dropdown",
        draw() {
            const iconNode = $(createIcon("bi:gear", "launcher-icon"));
            const launcherNode = addLauncher("right", iconNode, showSettingsDialog, DOCUMENTS_SETTINGS_TITLE);
            launcherNode.children("button").addClass("f6-target"); // DOCS-4450
            this.append(launcherNode.attr({ id: "io-ox-settings-topbar-icon", title: DOCUMENTS_SETTINGS_TITLE }));
        }
    });

    return appControlPoint;
}
