/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from '$/ox';
import { locationHash } from '$/url';

import ext from '$/io.ox/core/extensions';

import { invokeFloating } from '@/io.ox/office/tk/algorithms/jpromise';

import '@/io.ox/office/tab/style.less';

function setOfficeTabParameters() {
    const modulePath = locationHash('app') || "";
    ox.office = {
        openInTabs: true,
        app: modulePath,
        portal: modulePath.includes('portal'),
        isOffice: true,
        type: modulePath.substr(modulePath.lastIndexOf('/') + 1)
    };
}

function hasOfficeAppUrl() {
    return !!locationHash('app')?.includes('office');
}

function hasOfficePathUrl() {
    return window.location.pathname.includes('/office');
}

function setupMultiTabOffice() {
    // set global office parameters
    setOfficeTabParameters();

    // Set favicon as fast as possible when openening a new tab.
    // Keep in mind that it's set a second time when launching a app,
    // that is still needed for other use-cases were the app changes in the same tab
    invokeFloating(async () => {
        const { setAppFavIcon } = await import('@/io.ox/office/baseframework/utils/apputils');
        setAppFavIcon(ox.office.type);
    });

    // create toolbar for tabbed mode
    ext.point('io.ox/core/stages').extend({
        id: 'tabTopbar',
        before: 'topbars',
        async run() {
            const { initializeTopBar } = await import('@/io.ox/office/tab/tabtoolbar');
            initializeTopBar();
        }
    });

    // overwrite global refresh, only refresh things which are needed in office
    ext.point('io.ox/core/stages').extend({
        id: 'globalRefresh',
        after: 'tabTopbar',
        run() {
            ox.off('refresh^');
            ox.on('refresh^', function () {
                ox.trigger('refresh-portal');
            });
        }
    });
}

function setupSingleTabOffice() {
    // url must be rewritten when a multi tab url is pasted into single tab instance
    // - to fit to the single tab url pattern for apps
    // - the url is used to detect whether it's office in tabs, so the pattern must fit to multi-/singe-tab

    // Info: This code line is marked by Coverity (security tools), but works fine in this context.
    //       Only the part "office" is removed from the URL.
    window.location.href = ox.abs + ox.root + '/' + window.location.hash;
}

// this is executed very early when Core is loaded
if (hasOfficeAppUrl()) {
    if (ox.tabHandlingEnabled) {
        //TODO rewriting url pasted from single tab, so without '/office' is missing
        setupMultiTabOffice();

    // use-case: paste url from office with multi tab to device without multi tab support
    } else if (hasOfficePathUrl()) {
        setupSingleTabOffice();
    }
}
