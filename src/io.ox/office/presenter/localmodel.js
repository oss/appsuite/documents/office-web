/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import UserAPI from '$/io.ox/core/api/user';

import _ from '$/underscore';
import ox from '$/ox';
import Backbone from '$/backbone';

// class LocalModel ===========================================================

/**
 * Stores the data for a presentation in local mode.
 *
 * @param {Object} [attributes]
 *  Initial values of the attributes, which will be set when creating an instance.
 *
 *  @param {String} attributes.presenterId
 *   The user id of the presenter, or an empty string.
 *
 *  @param {String} attributes.presenterName
 *   The display name of the presenter, or an empty string.
 *
 *  @param {Boolean} attributes.paused
 *   Whether the presenter paused the presentation.
 */
export const LocalModel = Backbone.Model.extend({

    defaults() {
        return {
            presenterId: '',
            presenterName: '',
            paused: false
        };
    },

    initialize() {
        /*
        this.on('change', function (model) {
            console.log('Presenter - local model - change', model);
        });
        */
    },

    /**
     * Starts the presentation.
     * - sets the presenter id and name.
     *
     * @param {String} userId
     *  The user id of the presenter.
     */
    startPresentation(userId) {
        var self = this;

        if (this.canStart(userId)) {

            UserAPI.getName(ox.user_id).done(function (name) {
                self.set('presenterName', name);
            });

            this.set({
                presenterId: userId,
                paused: false
            });
        }
    },

    /**
     * Ends the presentation.
     * - sets the presenter id and name to defaults.
     *
     * @param {String} userId
     *  The user id of the presenter.
     */
    endPresentation(userId) {
        if (this.isPresenter(userId)) {
            this.set({
                presenterId: '',
                presenterName: '',
                paused: false
            });
        }
    },

    /**
     * Pauses the presentation
     * if the user is the presenter and the presentation is running.
     */
    pausePresentation(userId) {
        if (this.canPause(userId)) {
            this.set('paused', true);
        }
    },

    /**
     * Continues the presentation
     * if the user is the presenter and the presentation is paused.
     */
    continuePresentation(userId) {
        if (this.canContinue(userId)) {
            this.set('paused', false);
        }
    },

    /**
     * Returns true if the provided user can start the presentation.
     *
     * @param {String} userId
     *  The user id to check.
     */
    canStart(userId) {
        var presenterId = this.get('presenterId');
        return (_.isString(userId) && _.isString(presenterId) && _.isEmpty(presenterId));
    },

    /**
     * Returns true if a presenter is set,
     * which means the presentation is started.
     *
     * @returns {Boolean}
     *  Whether a presenter is set.
     */
    hasPresenter() {
        return !_.isEmpty(this.get('presenterId'));
    },

    /**
     * Returns true if the passed user id belongs to the presenter.
     *
     * @param {String} userId
     *  The user id to check.
     *
     * @returns {Boolean}
     *  Whether the user is the presenter.
     */
    isPresenter(userId) {
        return (userId === this.get('presenterId'));
    },

    /**
     * Returns true if the passed user id belongs to the former presenter.
     * Which means the given user started and ended the presentation again. And the presentation has not yet been started.
     * - Intended to be called after a Backbone model change event (otherwise the result may not be valid).
     *
     * @param {String} userId
     *  The user id to check.
     *
     * @returns {Boolean}
     *  Whether the user was the presenter.
     */
    wasPresenter(userId) {
        var formerPresenter = this.previous('presenterId');
        return (!this.hasPresenter() && !_.isEmpty(formerPresenter) && userId === formerPresenter);
    },

    /**
     * Returns true if the passed user id belongs to the presenter and the presentation is running.
     *
     * @param {String} userId
     *  The user id to check.
     *
     * @returns {Boolean}
     *  Whether the user is currently presenting.
     */
    isPresenting(userId) {
        return (!this.isPaused() && this.isPresenter(userId));
    },

    /**
     * Returns true if the presentation is paused.
     *
     * @returns {Boolean}
     *  Whether the presentation is paused.
     */
    isPaused() {
        return this.get('paused');
    },

    /**
     * Returns true if the provided user can pause the presentation.
     * Which means the user must be the presenter and the presentation must be running.
     *
     * @param {String} userId
     *  The user id to check.
     */
    canPause(userId) {
        return (!this.isPaused() && this.isPresenter(userId));
    },

    /**
     * Returns true if the provided user can continue the presentation.
     * Which means the user must be the presenter and the presentation must be paused.
     *
     * @param {String} userId
     *  The user id to check.
     */
    canContinue(userId) {
        return (this.isPaused() && this.isPresenter(userId));
    },

    /**
     * Returns true if the pause overlay can be displayed for the provided user.
     * Which means the user must be joined and the presentation must be paused.
     *
     * @param {String} userId
     *  The user id to check.
     */
    canShowPauseOverlay(userId) {
        return (this.isPresenter(userId) && this.isPaused());
    }

});
