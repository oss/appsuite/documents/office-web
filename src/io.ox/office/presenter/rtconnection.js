/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ox from '$/ox';
import $ from '$/jquery';
import _ from '$/underscore';

import { BaseObject } from '@/io.ox/office/tk/objects';

import { rtLogger } from '@/io.ox/office/editframework/utils/editconfig';

import DocRTBase from '@/io.ox/office/rt2/docrtbase';
import RT2Const from '@/io.ox/office/rt2/rt2const';
import RT2InitOptions from '@/io.ox/office/rt2/rt2initoptions';

// class RTConnection =========================================================

/**
 * Represents the connection to the real-time framework, used to send
 * message to and receive message from the server.
 *
 * Triggers the following events:
 * - 'update': Update notifications containing actions created by remote
 *  clients, and state information for the application (read-only, name and
 *  identifier of the current editor, etc.).
 *
 * @param {EditApplication} application
 *  The application containing this operations queue.
 *
 * @param {Object} initOptions
 *  optional (!) set of options for initializing instance.
 */
export class RTConnection extends BaseObject {

    constructor(application, initOptions) {

        // base constructor
        super();

        var // self reference
            self = this,

            // a constant prefix used
            PRESENTER_DOCID_PREFIX = 'presenter',

            // edit application reference
            app = application,

            // the init options for this instance
            initOpts = mergeWithPredefinedInitOptions(initOptions),

            // the real-time-2 connection instance
            rt2 = null;

        // public methods ----------------------------------------------------

        /* public string */ this.getRTUuid = function () {
            return mem_RT2().getClientUID();
        };

        /* public string */ this.getDocUID = function () {
            return mem_RT2().getDocUID();
        };

        /* public Promise */ this.connect = function () {
            rtLogger.log('RTConnection.connect called (Presenter)');
            return mem_RT2().join();
        };

        /* public Promise */ this.open = function (/* json */ jOpenData) {
            jOpenData = _.extend({ oldClientUID: '' }, jOpenData);
            rtLogger.log('RTConnection.openDocument called (Presenter)', jOpenData);
            return mem_RT2().openDocument(jOpenData);
        };

        /* public Promise */ this.closeDocument = function (/* json */ jCloseData) {
            rtLogger.log('RTConnection.closeDocument called (Presenter)', jCloseData);
            return mem_RT2().closeDocument(jCloseData);
        };

        /* public Promise */ this.disconnect = function () {
            rtLogger.log('RTConnection.disconnect called (Presenter)');
            return mem_RT2().leave();
        };
        /**
         * Send an emergency leave request to the server.
         *
         *  @returns {Boolean}
         *   TRUE if the request could be sent otherwise FALSE.
         */
        /* public boolean */ this.emergencyLeave = function () {

            rtLogger.log('RTConnection.emergencyLeave called (Presenter)');

            var sent = false;
            var params = { client_uid: mem_RT2().getClientUID(), doc_uid: mem_RT2().getDocUID(), channel_uid: mem_RT2().getChannelUID() };
            var url = getRT2Url('emergencyleave', params);

            var payload = JSON.stringify({});

            try {
                var header = { type: 'text/plain; charset=utf-8' };
                payload = new Blob([payload], header);

                sent = navigator.sendBeacon(url, payload);

            } catch {
                rtLogger.log('RTConnection.emergencyLeave - sendBeacon triggered exception (Presenter)');
            }

            return sent;
        };

        /**
         * Merges the pre-defined init options for a Presenter
         * connection to the possible provided initOptions.
         *
         * @param {RT2InitOptions} initOptions init options provided
         *
         * @returns {RT2InitOptions} merged RT2InitOptions or the pre-defined
         * init options if nothing has been provided. Provided options
         * overwrite pre-defined options.
         */
        function mergeWithPredefinedInitOptions(/* RT2InitOptions */ initOptions) {
            var mergedInitOptions = new RT2InitOptions();

            mergedInitOptions.setOption(RT2InitOptions.INITOPT_DOC_ID_PREFIX, PRESENTER_DOCID_PREFIX);
            mergedInitOptions.takeOver(initOptions);
            return mergedInitOptions;
        }

        /**
         * Provides a RT2 server target url to be used for http requests.
         *
         * @returns {String} a target url to be used for rt2 specific functions.
         */
        function getRT2Url(action, params) {
            var url = ox.apiRoot + '/rt2?action=' + action;
            var params_url = _.map(params, function (value, name) { return name + '=' + value; }).join('&');
            if (_.isString(params_url) && params_url.length > 0) {
                url += '&' + params_url;
            }
            url +=  '&session=' + ox.session;
            return url;
        }

        /**
         * Sends slide data update to the server. This slide update data will
         * broadcasted to all collaborating clients.
         *
         * @param {Object} slideInfo
         *  An object containing relevant slide data.
         *
         * @returns {jQuery.Promise}
         */
        /* public Promise */ this.updateSlide = function (/* object */ slideInfo) {
            rtLogger.log('RTConnection.updateSlide called (Presenter)', slideInfo);
            return mem_RT2().sendAppAction('update_slide_request', { slideInfo });
        };

        /**
         * Start the presentation as presenter.
         *
         * @param {Object} slideInfo
         *  An object containing relevant data for the initial slide.
         *
         * @returns {jQuery.Promise}
         */
        this.startPresentation = function (/* object */ slideInfo) {
            rtLogger.log('RTConnection.startPresentation called (Presenter)', slideInfo);
            return mem_RT2().sendAppAction('start_presentation_request', { slideInfo });
        };

        /**
         * Pause the presentation as presenter.
         *
         * @returns {jQuery.Promise}
         */
        this.pausePresentation = function () {
            rtLogger.log('RTConnection.pausePresentation called (Presenter)');
            return mem_RT2().sendAppAction('pause_presentation_request', {});
        };

        /**
         * Continue the previously paused presentation as presenter.
         *
         * @returns {jQuery.Promise}
         */
        this.continuePresentation = function () {
            rtLogger.log('RTConnection.continuePresentation called (Presenter)');
            return mem_RT2().sendAppAction('continue_presentation_request', {});
        };

        /**
         * End the presentation as presenter.
         *
         * @returns {jQuery.Promise}
         */
        this.endPresentation = function () {
            rtLogger.log('RTConnection.endPresentation called (Presenter)');
            return mem_RT2().sendAppAction('end_presentation_request', {});
        };

        /**
         * Join the presentation as participant / listener.
         *
         * @returns {jQuery.Promise}
         */
        this.joinPresentation = function () {
            rtLogger.log('RTConnection.joinPresentation called (Presenter)');

            // try to join the presentation
            var promise = mem_RT2().sendAppAction('join_presentation_request', {});

            // US #99684978: wait for the update message, reject with error code
            var def = $.Deferred();
            this.one('update', function (data) {
                if (_.isObject(data) && _.isObject(data.error) && (data.error.error === 'NO_ERROR')) {
                    def.resolve();
                } else {
                    def.reject(_.isObject(data) ? data.error : null);
                }
            });

            // return a promise that resolves/rejects after the update message
            return promise.then(_.constant(def));
        };

        /**
         * Leave the presentation as participant / listener.
         *
         * @returns {jQuery.Promise}
         */
        this.leavePresentation = function () {
            rtLogger.log('RTConnection.leavePresentation called (Presenter)');
            return mem_RT2().sendAppAction('leave_presentation_request', {});
        };

        /*
            * Determines if this instance has been initialized.
            */
        this.isInitialized = function () {
            return _.isObject(rt2);
        };

        // initialization -----------------------------------------------------

        rtLogger.log('RTConnection initialization (Presenter)');

        // forward realtime events to own listeners
        mem_RT2().on(RT2Const.EVENT_RECEIVE, function (response) {
            self.trigger(response.type, response.body);
        });

        // forward RT2 events regarding communication to possible handlers
        for (const eventType of RT2Const.EVENT_DEFINITION_LIST) {
            mem_RT2().on(eventType, function (data) {
                self.trigger(eventType, data);
            });
        }

        // private functions ---------------------------------------------------

        function mem_RT2() {
            if (!rt2) {
                rt2 = self.member(DocRTBase.create(app, initOpts));
            }
            return rt2;
        }
    }
}
