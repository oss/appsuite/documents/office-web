/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import ext from '$/io.ox/core/extensions';
import { Action as createAction } from '$/io.ox/backbone/views/actions/util';

import { BROWSER_TAB_SUPPORT } from '@/io.ox/office/tk/utils/tabutils';

// constants ==================================================================

export const PRESENTER_ACTION_ID = "io.ox/office/presenter/actions";

export const PRESENTER_TOOLBAR_ID = "io.ox/office/presenter/toolbar";

// private functions ==========================================================

function canStartLocal(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app        = baton.context.app;
    var localModel = app.localModel;
    var userId     = app.rtConnection.getRTUuid();

    return (localModel.canStart(userId));
}

function isPresenterLocal(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app        = baton.context.app;
    var localModel = app.localModel;
    var userId     = app.rtConnection.getRTUuid();

    return (localModel.isPresenter(userId));
}

function canPauseLocal(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app        = baton.context.app;
    var localModel = app.localModel;
    var userId     = app.rtConnection.getRTUuid();

    return (localModel.canPause(userId));
}

function canContinueLocal(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app        = baton.context.app;
    var localModel = app.localModel;
    var userId     = app.rtConnection.getRTUuid();

    return (localModel.canContinue(userId));
}

// helper function for remote presentation --------------------------------

function canStartRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app       = baton.context.app;
    var encrypted = app.fileModel.isEncrypted();
    var rtModel   = app.rtModel;
    var userId    = app.rtConnection.getRTUuid();

    return (!encrypted && rtModel.canStart(userId));
}

function isPresenterRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;
    var userId  = app.rtConnection.getRTUuid();

    return (rtModel.isPresenter(userId));
}

function hasPresenterRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;

    return (rtModel.hasPresenter());
}

function isJoinedRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;
    var userId  = app.rtConnection.getRTUuid();

    return (rtModel.isJoined(userId));
}

function canPauseRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;
    var userId  = app.rtConnection.getRTUuid();

    return (rtModel.canPause(userId));
}

function isPausedRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var rtModel = baton.context.app.rtModel;

    return (rtModel.isPaused());
}

function canContinueRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;
    var userId  = app.rtConnection.getRTUuid();

    return (rtModel.canContinue(userId));
}

function canJoinRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;
    var userId  = app.rtConnection.getRTUuid();

    return (rtModel.canJoin(userId));
}

function canLeaveRemote(baton) {
    if (!baton.context || !baton.context.app) { return false; }

    var app     = baton.context.app;
    var rtModel = app.rtModel;
    var userId  = app.rtConnection.getRTUuid();

    return (rtModel.canLeave(userId));
}

// start presentation drop-down
ext.point(PRESENTER_TOOLBAR_ID + '/dropdown/start-presentation').extend(
    {
        index: 100,
        id: 'startlocal',
        //#. 'start presentation' dropdown menu entry to start a local only presentation where no remote participants would be able to join.
        title: gt('Start local presentation'),
        caption: gt('View the presentation on your device.'),
        ref: PRESENTER_ACTION_ID + '/start/local'
    },
    {
        index: 200,
        id: 'startremote',
        //#. 'start presentation' dropdown menu entry to start a remote presentation where remote participants would be able to join.
        title: gt('Start remote presentation'),
        caption: gt('Broadcast your presentation over the Web.'),
        ref: PRESENTER_ACTION_ID + '/start/remote'
    }
);

// Presenter actions
createAction(PRESENTER_ACTION_ID + '/start/local', {
    matches(baton) {
        return (canStartLocal(baton) && !isPresenterRemote(baton) && !isJoinedRemote(baton));
    },
    action(baton) {
        var app         = baton.context.app;
        var localModel  = app.localModel;
        var userId      = app.rtConnection.getRTUuid();

        localModel.startPresentation(userId);
    }
});

createAction(PRESENTER_ACTION_ID + '/start/remote', {
    // starting a remote presentation requires the 'remote_presenter' capability
    capabilities: 'remote_presenter',
    matches(baton) {
        return (!isPresenterLocal(baton) && canStartRemote(baton));
    },
    action(baton) {
        var app     = baton.context.app;
        var slideId = app.mainView.getActiveSlideIndex();

        app.rtConnection.startPresentation({ activeSlide: slideId });
    }
});

createAction(PRESENTER_ACTION_ID + '/end', {
    matches(baton) {
        return (isPresenterLocal(baton) || isPresenterRemote(baton));
    },
    action(baton) {
        var app         = baton.context.app;
        var rtModel     = app.rtModel;
        var localModel  = app.localModel;
        var userId      = app.rtConnection.getRTUuid();

        if (localModel.isPresenter(userId)) {
            localModel.endPresentation(userId);

        } else if (rtModel.isPresenter(userId)) {
            app.rtConnection.endPresentation();
        }
    }
});

createAction(PRESENTER_ACTION_ID + '/pause', {
    matches(baton) {
        return (canPauseLocal(baton) || canPauseRemote(baton));
    },
    action(baton) {
        var app         = baton.context.app;
        var rtModel     = app.rtModel;
        var localModel  = app.localModel;
        var userId      = app.rtConnection.getRTUuid();

        if (localModel.canPause(userId)) {
            localModel.pausePresentation(userId);

        } else if (rtModel.canPause(userId)) {
            app.rtConnection.pausePresentation();
            app.mainView.toggleFullscreen(false);
        }
    }
});

createAction(PRESENTER_ACTION_ID + '/continue', {
    matches(baton) {
        return (canContinueLocal(baton) || canContinueRemote(baton));
    },
    action(baton) {
        var app         = baton.context.app;
        var rtModel     = app.rtModel;
        var localModel  = app.localModel;
        var userId      = app.rtConnection.getRTUuid();

        if (localModel.canContinue(userId)) {
            localModel.continuePresentation(userId);

        } else if (rtModel.canContinue(userId)) {
            baton.context.app.rtConnection.continuePresentation();
        }
    }
});

createAction(PRESENTER_ACTION_ID + '/join', {
    matches(baton) {
        return (!isPresenterLocal(baton) && canJoinRemote(baton));
    },
    action(baton) {
        baton.context.app.mainView.joinPresentation();
    }
});

createAction(PRESENTER_ACTION_ID + '/leave', {
    matches(baton) {
        return (canLeaveRemote(baton));
    },
    action(baton) {
        baton.context.app.rtConnection.leavePresentation();
    }
});

createAction(PRESENTER_ACTION_ID + '/fullscreen', {
    // iOS doesn't support full-screen
    device: '!iOS',
    matches(baton) {
        if (!baton.context) { return false; }

        return (isPresenterLocal(baton) || isPresenterRemote(baton) || isJoinedRemote(baton));
    },
    action(baton) {
        baton.context.app.mainView.toggleFullscreen();
    }
});

createAction(PRESENTER_ACTION_ID + '/togglesidebar', {
    matches(baton) {
        return (!isPresenterLocal(baton) && hasPresenterRemote(baton) && (!isPresenterRemote(baton) || isPausedRemote(baton)));
    },
    action(baton) {
        baton.context.app.mainView.onToggleSidebar();
    }
});

createAction(PRESENTER_ACTION_ID + '/zoomin', {
    matches(baton) {
        return !isPresenterRemote(baton);
    },
    action(baton) {
        baton.context.app.mainView.presenterEvents.trigger('presenter:zoomin');
    }
});

createAction(PRESENTER_ACTION_ID + '/zoomout', {
    matches(baton) {
        return !isPresenterRemote(baton);
    },
    action(baton) {
        baton.context.app.mainView.presenterEvents.trigger('presenter:zoomout');
    }
});

createAction(PRESENTER_ACTION_ID + '/close', {
    matches(baton) {
        return _.isObject(baton.context) && !BROWSER_TAB_SUPPORT;
    },
    action(baton) {
        baton.context.app.mainView.presenterEvents.trigger('presenter:close');
    }
});
