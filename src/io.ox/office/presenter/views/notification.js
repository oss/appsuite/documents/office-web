/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import yell from '$/io.ox/core/yell';
import { invoke } from '$/io.ox/backbone/views/actions/util';
import ErrorMessages from '@/io.ox/office/presenter/errormessages';
import { createIcon, createButton } from "@/io.ox/office/tk/dom";

import { PRESENTER_ACTION_ID } from '@/io.ox/office/presenter/actions';

// public functions ===========================================================

/**
 * Creates a message node from the given message text and icon.
 *
 * @param {string} [message='']
 *  the notification String, if omitted no notification text will be added.
 *
 * @param {IconId} [iconId='bi:exclamation-triangle']
 *  a CSS class name to be applied on the notification icon.
 *
 * @returns {JQuery}
 *  The notification node.
 */
export function createMessageNode(message, iconId) {
    // class "viewer-displayer-notification" needed to derive CSS styling of OX Viewer
    return $('<div class="viewer-displayer-notification">').append(
        createIcon(iconId || 'bi:exclamation-triangle'),
        $('<p class="apology">').text(message || '')
    );
}

/**
 * Creates an error node from the given error object.
 * Supported origins for the error object are Realtime, Drive and document conversion.
 * An unknown error code will be translated to a general error message.
 *
 * @param {Object} error
 *  The error object to create the error node for.
 *
 * @param {Object} [options]
 *  @param {String} options.category
 *      The error category to provide more specific messages for unknown error codes.
 *      Supported are 'rt', 'drive' and 'conversion'.
 *
 * @returns {jQuery.node}
 *  The notification node.
 */
export function createErrorNode(error, options) {
    const locked = error?.cause === 'passwordProtected';
    const iconId = locked ? 'bi:lock-fill' : 'bi:exclamation-triangle';
    const message = ErrorMessages.getErrorMessage(error, options);
    return createMessageNode(message, iconId);
}

/**
 * Shows an alert banner.
 *
 * @param {Object} yellOptions
 *  The settings for the alert banner:
 *  @param {String} [yellOptions.type='info']
 *      The type of the alert banner. Supported types are 'success',
 *      'info', 'warning', and 'error'.
 *  @param {String} [yellOptions.headline]
 *      An optional headline shown above the message text.
 *  @param {String} yellOptions.message
 *      The message text shown in the alert banner.
 *  @param {Number} [yellOptions.duration]
 *      The time to show the alert banner, in milliseconds; or -1 to
 *      show a permanent alert.
 *  @param {Object} [yellOptions.action]
 *      An arbitrary action button that will be shown below the
 *      message text, with the following properties:
 *      @param {String} yellOptions.action.label
 *          The display text for the button.
 *      @param {String} yellOptions.action.ref
 *          The action reference id.
 *      @param {Baton} [yellOptions.action.baton=null]
 *          The baton to hand over to the action.
 */
export function showNotification(yellOptions) {
    var // the notification DOM element
        yellNode = null;

    // add default options
    yellOptions = { type: 'info', ...yellOptions };
    // create and show the notification DOM node
    yellNode = yell(yellOptions);
    // register event handlers

    if (_.isObject(yellOptions.action)) {
        yellNode.one('notification:appear', function () {
            // add action button to the message

            // the button label
            var label = yellOptions.action.label;
            // the action ref the button invokes
            var ref = yellOptions.action.ref;
            // the baton to hand over to the action
            var baton = yellOptions.action.baton || null;
            // the message node as target for additional contents
            var node = yellNode.find('.message');
            // the button node to add to the message
            var button = createButton({
                type: 'primary',
                action: 'action',
                style: { width: '100%' },
                label,
                click: () => {
                    invoke(ref, baton);
                    yell.close();
                }
            });

            node.append($('<div>').append(button));
        });
    }
}

/**
 * Shows a notification for the participants when the presenter starts the presentation.
 *
 * @param {RTModel} rtModel
 *  The real time model.
 *
 * @param {RTConnection} rtConnection
 *  The real time connection.
 *
 * @param {Object} baton
 *  The baton for the join presentation action.
 */
export function notifyPresentationStart(rtModel, rtConnection, baton) {
    var clientUID = rtConnection.getRTUuid();

    if (rtModel.isPresenter(clientUID) || rtModel.isJoined(clientUID)) { return; }

    var yellOptions = {
        //#. headline of a presentation start alert
        headline: gt('Presentation start'),
        //#. message text of of a presentation start alert
        //#. %1$d is the presenter name
        message: gt('%1$s has started the presentation.', rtModel.get('presenterName')),
        duration: -1,
        focus: true,
        action: {
            //#. link button to join the currently running presentation
            label: gt('Join Presentation'),
            ref: PRESENTER_ACTION_ID + '/join',
            baton
        }
    };

    showNotification(yellOptions);
}

/**
 * Shows a notification to all participants when the presenter ends the presentation.
 *
 * @param {RTModel} rtModel
 *  The real time model.
 *
 * @param {RTConnection} rtConnection
 *  The real time connection.
 */
export function notifyPresentationEnd(rtModel, rtConnection) {
    var clientUID = rtConnection.getRTUuid();
    var presenterId;
    var presenterName;

    if (_.isEmpty(rtModel.get('presenterId'))) {
        // the presenter has already been reset, look for previous data
        presenterId = rtModel.previous('presenterId');
        presenterName = rtModel.previous('presenterName');

    } else {
        // use current presenter
        presenterId = rtModel.get('presenterId');
        presenterName = rtModel.get('presenterName');
    }

    if (clientUID === presenterId) { return; }

    showNotification({
        //#. headline of a presentation end alert
        headline: gt('Presentation end'),
        //#. message text of a presentation end alert
        //#. %1$d is the presenter name
        message: gt('%1$s has ended the presentation.', presenterName),
        duration: 6000
    });
}

/**
 * Shows a notification to the participant who joined the presentation.
 *
 * @param {RTModel} rtModel
 *  The real time model.
 *
 * @param {RTConnection} rtConnection
 *  The real time connection.
 */
export function notifyPresentationJoin(rtModel, rtConnection) {
    var clientUID = rtConnection.getRTUuid();
    var presenterName = rtModel.get('presenterName');

    if (rtModel.isPresenter(clientUID) || rtModel.isJoined(clientUID)) { return; }

    showNotification({
        //#. headline of a presentation join alert
        headline: gt('Presentation join'),
        //#. message text of a presentation join alert
        //#. %1$d is the presenter name
        message: gt('Joining the presentation of %1$s.', presenterName),
        duration: 6000
    });
}

/**
 * Shows a notification to the participant if joining the presentation
 * fails because the maximum of allowed participants is reached.
 *
 * @param {String} presenterName
 *  The presenter name.
 */
export function notifyMaxParticipantsReached(presenterName) {
    showNotification({
        type: 'error',
        //#. message text of an alert box if joining a presentation fails
        //#. %1$d is the name of the user who started the presentation
        message: gt('The limit of participants has been reached. Please contact the presenter %1$s.', presenterName),
        focus: true
    });
}

/**
 * Shows a notification when the file that is currently used for the presentation is deleted.
 *
 * @param {String} filename
 *  The file name.
 */
export function notifyFileDelete(filename) {
    showNotification({
        type: 'error',
        //#. message text of an alert indicating that the presentation file was deleted while presenting it
        //#. %1$d is the file name
        //message: gt('%1$s has ended the presentation.', filename),
        message: gt('The presentation document %1$s was deleted.', filename),
        duration: -1
    });
}

/**
 * Shows a Realtime error notification.
 *
 * @param {Object|String} errorCode
 *  The error type String provided by the RT error event.
 */
export function notifyRealtimeError(errorCode) {
    showNotification({
        type: 'error',
        message: ErrorMessages.getRealtimeErrorMessage({ error: errorCode, code: errorCode }),
        duration: -1,
        focus: true
    });
}

/**
 * Shows a Realtime online notification.
 */
export function notifyRealtimeOnline() {
    showNotification({
        type: 'info',
        //#. message text of a Realtime connection online info
        message: gt('The realtime connection is established.'),
        duration: 10000
    });
}

/**
 * Shows a Realtime offline notification.
 */
export function notifyRealtimeOffline() {
    showNotification({
        type: 'warning',
        //#. message text of a Realtime connection offline alert.
        message: gt('The realtime connection is lost.'),
        duration: -1
    });
}
