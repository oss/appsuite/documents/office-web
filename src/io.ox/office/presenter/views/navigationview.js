/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import ext from '$/io.ox/core/extensions';
import DisposableView from '$/io.ox/backbone/views/disposable';
import ToolbarView from '$/io.ox/backbone/views/toolbar';
import { createIcon } from "@/io.ox/office/tk/dom";

import { PRESENTER_ACTION_ID } from '@/io.ox/office/presenter/actions';

// constants ==================================================================

/**
 * The NavigationView is responsible for displaying the bottom navigation bar.
 */

const NAVIGATION_ID = 'io.ox/office/presenter/navigation';
const SHORT_LABELS = $(window).width() < 580;

// private functions ==========================================================

/**
 * Creates the HTML mark-up for a slide navigation button.
 *
 * @param {String} type
 *  the button type to create, could be 'prev' or 'next'.
 *
 * @returns {jQuery}
 *  the button node.
 */
function createNavigationButton(type) {

    const next = type === 'next';
    const button = $('<a href="#" class="btn btn-toolbar" role="menuitem" aria-disabled="false">');

    button.attr({ 'aria-label': next ? gt('Next slide') : gt('Previous slide') });
    button.tooltip({
        placement: 'top',
        title: next ?
            //#. button tooltip for 'go to next presentation slide' action
            gt('Next slide') :
            //#. button tooltip for 'go to previous presentation slide' action
            gt('Previous slide')
    });

    const iconId = next ? 'bi:chevron-right' : 'bi:chevron-left';
    return button.append(createIcon(iconId));
}

/**
 * Places the tooltip on top of the node
 *
 * @param {jQuery} node
 *  the button node.
 */
function setTooltipTopPlacement(node) {
    if (node instanceof $) {
        var data = node.data('bs.tooltip');
        if (data?.options) {
            data.options.placement = 'top';
        }
    }
}

// navigation link meta object used to generate extension points later
const NAVIGATION_LINK_ENTRIES = [{
    id: "pause",
    prio: 'hi',
    mobile: _.device('iOS') ? 'none' : 'lo',
    //#. button label for pausing the presentation
    title: SHORT_LABELS ? gt('Pause') : gt('Pause presentation'),
    //#. button tooltip for pausing the presentation
    tooltip: gt('Pause the presentation'),
    ref: PRESENTER_ACTION_ID + '/pause',
    customize() {
        setTooltipTopPlacement(this);
        this.addClass('presenter-navigation-pause');
    }
}, {
    id: "continue",
    prio: 'hi',
    mobile: _.device('iOS') ? 'none' : 'lo',
    //#. button label for continuing the presentation
    title: SHORT_LABELS ? gt('Continue') : gt('Continue presentation'),
    //#. button tooltip for continuing the presentation
    tooltip: gt('Continue the presentation'),
    ref: PRESENTER_ACTION_ID + '/continue',
    customize() {
        setTooltipTopPlacement(this);
        this.addClass('presenter-navigation-continue');
    }
}, {
    id: "fullscreen",
    prio: 'hi',
    mobile: 'lo',
    icon: 'bi/arrows-fullscreen.svg',
    //#. button label for toggling fullscreen mode
    title: gt('Toggle fullscreen'),
    //#. button label for toggling fullscreen mode
    tooltip: gt('Toggle fullscreen'),
    ref: PRESENTER_ACTION_ID + '/fullscreen',
    customize() {
        setTooltipTopPlacement(this);
        this.addClass('presenter-navigation-fullscreen');
    }
}];

// define extension point for this NavigationView
const navigationPoint = ext.point(NAVIGATION_ID);

// extend navigation point with the navigation links
NAVIGATION_LINK_ENTRIES.forEach((linkMeta, index) => {
    navigationPoint.extend({ index: (index + 1) * 100, ...linkMeta });
});

// Slide navigation
navigationPoint.extend({
    id: 'slide-navigation',
    index: 100,
    draw(baton) {
        baton.context?.renderSlideNavigation();
    }
});

// Participants drop down
navigationPoint.extend({
    id: 'participants-dropdown',
    index: 200,
    draw(baton) {
        // TODO: check if needed: if (_.device('smartphone')) return;
        baton.context?.renderParticipantsDropdown();
    }
});

// class NavigationView =======================================================

export const NavigationView = DisposableView.extend({

    // class "viewer-toolbar" needed to derive CSS styling of OX Viewer
    className: 'viewer-toolbar presenter-navigation-toolbar',

    initialize(options) {
        Object.assign(this, options);

        // register event handlers
        this.listenTo(this.presenterEvents, 'presenter:local:slide:change', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:start', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:end', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:pause', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:continue', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:participants:change', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:fullscreen:enter', this.updateNavigationBar);
        this.listenTo(this.presenterEvents, 'presenter:fullscreen:exit', this.updateNavigationBar);

        const container1 = $('<div class="left-container">');
        const container2 = $('<div class="right-container">');
        this.$el.append(container1, container2);

        this.toolbar = new ToolbarView({ el: container2, point: NAVIGATION_ID, strict: false, simple: true });

        this.participantsDropdownId = _.uniqueId('participants-dropdown-');

        // hide initially
        this.$el.hide();
    },

    /**
     * Returns the data object to create a Baton from.
     *
     * @returns {Object}
     *  the data object.
     */
    getBatonData() {
        return {
            context: this,
            model: this.model,
            models: [this.model],
            data: this.model.toJSON()
        };
    },

    /**
     * Renders this NavigationView.
     *
     * @returns {NavigationView}
     *  this view object itself.
     */
    render() {
        //#. aria label for the presenter navigation bar, for screen reader only.
        this.$el.attr({ role: 'menu', 'aria-label': gt('Presenter navigation bar') });

        this.updateNavigationBar();
        return this;
    },

    /**
     * Update inner toolbar.
     */
    updateNavigationBar: _.debounce(function () {

        if (this.disposed) { return; }

        var navigation = this.$el;
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();

        // the navigation panel is displayed for the presenter only and if the presentation is not paused.
        if (localModel.isPresenting(clientUID) || rtModel.isPresenting(clientUID)) {

            // update tool bar
            this.toolbar.setSelection([this.model.toJSON()], this.getBatonData.bind(this));

            // render navigation links
            var baton = ext.Baton(this.getBatonData());
            navigationPoint.invoke('draw', navigation, baton);
        }

    }, 10),

    /**
     * Show navigation panel.
     */
    showNavigation() {
        if (this.disposed) { return; }
        this.$el.css('display', '');
    },

    /**
     * Hide navigation panel,
     * but only if the participants dropdown is currently closed.
     *
     * @param {[Number = 1000]} duration
     *  the duration of the fade out animation in milliseconds. Defaults to 1000 ms.
     */
    hideNavigation(duration) {
        if (this.disposed) { return; }

        if (this.$el.find('.presenter-participants-dropdown').hasClass('open')) { return; }

        this.$el.fadeOut(duration || 1000);
    },

    /**
     * Renders the slide navigation controls.
     */
    renderSlideNavigation() {

        const prevBtn = createNavigationButton('prev');
        const prevGroup = $('<li role="presentation" class="viewer-toolbar-navigation">').append(prevBtn);

        const nextBtn = createNavigationButton('next');
        const nextGroup = $('<li role="presentation" class="viewer-toolbar-navigation">').append(nextBtn);

        const slideInput = $('<input type="text" class="form-control viewer-toolbar-page" role="textbox">');
        const slideLabel = $('<div class="viewer-toolbar-page-total">');
        const slideGroup = $('<li role="presentation" class="viewer-toolbar-navigation">').append(slideInput, slideLabel);

        // slide number and count
        var slideNumber       = this.app.mainView.getActiveSlideIndex() + 1;
        var slideCount        = this.app.mainView.getSlideCount();
        var safariFullscreen  = this.app.mainView.fullscreen && _.device('safari');
        var self              = this;

        if (SHORT_LABELS) {
            prevBtn.css({ 'min-width': '20px' });
            nextBtn.css({ 'min-width': '20px' });
        }

        function setButtonState($btn, state) {
            if (state) {
                $btn.removeClass('disabled').removeAttr('aria-disabled');
            } else {
                $btn.addClass('disabled').attr('aria-disabled', true);
            }
        }

        function onPrevSlide(event) {
            event.preventDefault();
            self.app.mainView.showPreviousSlide();
        }
        function onNextSlide(event) {
            event.preventDefault();
            self.app.mainView.showNextSlide();
        }
        function onInputKeydown(e) {
            e.stopPropagation();
            if (e.which === 13 || e.which === 27) { self.$el.parent().focus(); }
        }
        function onInputChange() {
            var newValue = parseInt($(this).val(), 10);

            if (Number.isNaN(newValue)) {
                $(this).val(slideNumber);
                return;

            } else if (newValue <= 0) {
                $(this).val(1);
                newValue = 1;

            } else if (newValue > slideCount) {
                $(this).val(slideCount);
                newValue = slideCount;
            }

            self.app.mainView.showSlide(newValue - 1);
        }
        function onClick() {
            $(this).select();
        }

        // set slide number in the slide input control
        slideInput.val(slideNumber);
        // Safari blocks almost all key events to input controls in fullscreen mode. see: https://bugs.webkit.org/show_bug.cgi?id=121496
        if (safariFullscreen) {
            slideInput.attr({ readonly: true, disabled: true, 'aria-readonly': true });
        }

        //#. text of a presentation slide count display
        //#. Example result: "of 10"
        //#. %1$d is the total slide count
        slideLabel.text(gt('of %1$d', slideCount));
        setButtonState(prevBtn, slideNumber > 1);
        setButtonState(nextBtn, slideNumber < slideCount);

        slideInput.on('keydown', onInputKeydown).on('change', onInputChange).on('click', onClick);
        prevBtn.on('click', onPrevSlide);
        nextBtn.on('click', onNextSlide);

        slideInput.tooltip({
            //#. button tooltip for 'jump to presentation slide' action
            title: gt('Jump to slide'),
            placement: 'top'
        });

        // TODO: use a real toolbar with actions...
        this.$el.find('.left-container').empty().append(
            $('<ul role="toolbar" class="classic-toolbar">').append(prevGroup, nextGroup, slideGroup)
        );
    },

    /**
     * Renders the slide Participants drop down.
     */
    renderParticipantsDropdown() {
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();

        // no participants list for local presenter
        if (localModel.isPresenter(clientUID)) { return; }

        var participantsJoined = false;
        var participants       = rtModel.get('participants');
        var presenterId        = rtModel.get('presenterId') || 'none';
        var presenterName      = rtModel.get('presenterName') ||
            //#. text of a user list that shows the names of presenting user and participants.
            //#. the text to display as presenter name if no user is presenting yet.
            gt('none');

        var createMenuItem = function (clientUID, userDisplayName, options) {
            options = options || {};

            var item = $('<li role="presentation">');
            var link = $('<a href="#" draggable="false" role="menuitem" tabindex="-1">')
                .attr('data-name', clientUID)
                // in firefox draggable=false is not enough to prevent dragging...
                .on('dragstart', false)
                .append(
                    // options.icon ? $('<i class="fa fa-fw" aria-hidden="true">') : $(), ES6-TODO: ui-redesign
                    // bug #54320 - no need for <span>
                    $.txt(userDisplayName)
                );

            // add halo link
            if (options.internal_userid) {
                link.addClass('needs-action person')
                    .attr({ href: '#', 'data-detail-popup': 'halo' })
                    .data({ internal_userid: options.internal_userid });
            }

            return item.append(link);
        };

        var dropdownButton = $('<a role="button" class="btn btn-toolbar dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">', { id: this.participantsDropdownId }).append(
            //#. text of a user list that shows the names of presenting user and participants.
            //#. the dropdown button label for the participants dropdown.
            $('<span>').text(gt('Participants')),
            $('<span class="caret">')
        );

        var presenterLink = $('<a class="presenter-link" role="menuitem" tabindex="-1" href="#" draggable="false">').attr('data-name', presenterId).text(presenterName);

        var dropdownMenu = $('<ul class="dropdown-menu" role="menu">').attr('aria-labelledby', this.participantsDropdownId).append(
            //#. text of a user list that shows the names of presenting user and participants.
            //#. the presenter section label.
            $('<h5 role="separator" class="dropdown-header">').text(gt('Presenter')),
            $('<li role="presentation">').append(presenterLink),
            $('<li role="presentation" class="divider">'),
            //#. text of a user list that shows the names of presenting user and participants.
            //#. the participants section label.
            $('<h5 role="separator" class="dropdown-header">').text(gt('Participants'))
        );

        var dropdown = $('<li class="dropup pull-right presenter-participants-dropdown">').append(dropdownButton, dropdownMenu);

        _.each(participants, function (user) {
            if (!rtModel.isPresenter(user.clientUID)) {
                // create and append participant menu item
                participantsJoined = true;
                dropdownMenu.append(createMenuItem(user.clientUID, user.userDisplayName, { internal_userid: user.id }));

            } else {
                // add halo link data for presenter
                presenterLink.addClass('needs-action person')
                    .attr({ href: '#', 'data-detail-popup': 'halo' })
                    .data({ internal_userid: user.id });
            }

        }, this);

        if (!participantsJoined) {
            //#. text of a user list that shows the names of presenting user and participants.
            //#. the text to display as participants names if no users are listening yet.
            dropdownMenu.append(createMenuItem('none', gt('none'), null));
        }

        dropdownButton.tooltip({
            //#. the dropdown button tooltip for the participants dropdown.
            title: gt('View participants'),
            placement: 'top',
            trigger: 'hover'
        });

        dropdown.on('show.bs.dropdown', function () { dropdownButton.tooltip('hide'); });

        this.toolbar.$el.find('.classic-toolbar').prepend(dropdown);
    },

    /**
     * Destructor of this view
     */
    onDispose() {
        this.model = null;
    }

});
