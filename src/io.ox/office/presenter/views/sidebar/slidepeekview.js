/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import DisposableView from '$/io.ox/backbone/views/disposable';
import DocConverterUtils from '$/io.ox/core/tk/doc-converter-utils';
import { createAbortableDeferred } from '@/io.ox/office/presenter/util';

// class SlidePeekView ========================================================

export const SlidePeekView = DisposableView.extend({

    className: 'presenter-sidebar-section',

    events: {
        'click .slidepeek': 'onSlidePeekClicked'
    },

    initialize(options) {
        Object.assign(this, options);

        this.slidePeekLoadDef = createAbortableDeferred($.noop);
        this.activeSlideIndex = this.app.mainView.getActiveSlideIndex();

        this.onResizeDebounced = _.debounce(this.onResize.bind(this), 50);
        this.listenTo(this.presenterEvents, 'presenter:resize', this.onResizeDebounced);
        this.listenTo(this.presenterEvents, 'presenter:page:loaded', this.onPageLoaded);
    },

    render() {
        function beginConvertSuccess(convertData) {
            var slideWrapper = $('<div class="slidepeek">');
            var slideImage   = new window.Image();

            slideImage.className = 'slidepeek-image';
            this.slideImage = slideImage;
            slideWrapper.append(slideImage);
            this.$el.append(slideWrapper);
            this.loadSlidePeek(convertData);
            return convertData;
        }

        function beginConvertError(response) {
            return $.Deferred().reject(response);
        }

        function beginConvertFinished() {
            if (!this.disposed) {
                this.$el.removeClass('io-ox-busy');
            }
        }

        this.$el.empty().addClass('io-ox-busy');
        this.slidePeekLoadDef = DocConverterUtils.beginConvert(this.model)
            .done(beginConvertSuccess.bind(this))
            .fail(beginConvertError.bind(this))
            .always(beginConvertFinished.bind(this));

        return this;
    },

    /**
     * Loads the slide peek preview image.
     *
     * @param {Object} convertData
     *  a response object from document converter containing
     *  the convert jobID and the total page count.
     */
    loadSlidePeek(convertData) {
        var self           = this;
        var slidePeek      = this.$('.slidepeek');
        var slidePeekImage = this.$('.slidepeek-image');
        var peekPageNumber = this.activeSlideIndex + 2;
        // build thumbnail request param
        var params = {
            action: 'convertdocument',
            convert_action: 'getpage',
            target_format: 'png',
            target_width: 600,
            target_zoom: 1,
            page_number: peekPageNumber,
            job_id: convertData.jobID,
            id: encodeURIComponent(this.model.get('id')),
            folder_id: this.model.get('folder_id'),
            filename: encodeURIComponent(this.model.get('filename')),
            version: this.model.get('version')
        };

        if (peekPageNumber > convertData.pageCount) {
            //#. info text on the next slide preview, which means the presenting user reached the last slide.
            var endNotification = $('<div class="end-notification">').text(gt('End of Slides'));
            // apply slide size to slide peek
            slidePeek.empty().addClass('end').append(endNotification);
            this.updateSlidePeekSize();

        } else {
            // load the preview image
            var thumbnailUrl = DocConverterUtils.getConverterUrl(params, { encodeUrl: true });

            slidePeekImage.attr('src', thumbnailUrl)
            .on('load', function () {
                $(this).show();
                self.updateSlidePeekSize();
            });
        }
    },

    /**
     * Sets the size of the slide peek according to the main slide size.
     * - the slide peek matches 50% of the main slide size.
     */
    updateSlidePeekSize() {
        var slidePeek = this.$('.slidepeek');
        var pageSize  = this.app.mainView.slideView.pdfDefaultSize;
        var zoom      = this.app.mainView.slideView.zoomState.factor;
        var factor    = zoom * 0.5;
        var maxWidth  = slidePeek.parent().width();
        var width;
        var height;

        if (pageSize && pageSize.width > 0 && pageSize.height > 0) {
            width = pageSize.width * factor;
            height = pageSize.height * factor;

            if (width > maxWidth) {
                width = maxWidth;
                height = maxWidth * pageSize.height / pageSize.width;
            }

            slidePeek.css({
                width,
                height
            });
        }
    },

    /**
     * Handles page loaded events.
     *
     * @param {Number} page
     *  The 1-based page number.
     *
     */
    onPageLoaded(page) {
        if (this.disposed || ox.ui.App.getCurrentApp().getName() !== 'io.ox/office/presenter') {
            return;
        }

        if (page === (this.activeSlideIndex + 1)) {
            this.updateSlidePeekSize();
        }
    },

    /**
     * Handles presenter resize events
     */
    onResize() {
        if (this.disposed || ox.ui.App.getCurrentApp().getName() !== 'io.ox/office/presenter') {
            return;
        }

        this.updateSlidePeekSize();
    },

    /**
     * Handles clicks on the slide peek.
     */
    onSlidePeekClicked(event) {
        event.preventDefault();
        this.app.mainView.showNextSlide();
    },

    onDispose() {
        var def = this.slidePeekLoadDef;
        // cancel any pending thumbnail loading
        if (def.state() === 'pending') {
            def.abort();
        }
        // close convert jobs while quitting
        def.done(function (response) {
            DocConverterUtils.endConvert(this.model, response.jobID);
        }.bind(this));
    }
});
