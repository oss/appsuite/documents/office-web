/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import DisposableView from '$/io.ox/backbone/views/disposable';

import { UserbadgeView } from '@/io.ox/office/presenter/views/sidebar/userbadgeview';

// class ParticipantsView =====================================================

export const ParticipantsView = DisposableView.extend({

    className: 'presenter-sidebar-section',

    initialize(options) {
        Object.assign(this, options);

        this.listenTo(this.presenterEvents, 'presenter:participants:change', this.render);
    },

    render() {
        var sectionHeading   = $('<div class="sidebar-section-heading">');
        var headline         = $('<h3 class="sidebar-section-headline">').text('Participants');
        var sectionBody      = $('<div class="sidebar-section-body">');
        var participantsList = $('<ul class="participants-list">');

        var participants     = this.app.rtModel.get('participants');
        var participantsUIDs = Object.keys(participants);

        participantsUIDs.forEach(function (clientUID) {
            var userbadgeView = new UserbadgeView({ participant: participants[clientUID], app: this.app });
            participantsList.append(userbadgeView.render().el);
        }, this);

        if (participantsUIDs.length === 0) {
            //#. info text in the participants list.
            var notification = $('<li>').text(gt('There are currently no participants.'));
            participantsList.append(notification);
        }

        sectionHeading.append(headline);
        sectionBody.append(participantsList);
        this.$el.empty().append(sectionHeading, sectionBody);

        return this;
    }
});
