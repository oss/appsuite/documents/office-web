/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import DisposableView from '$/io.ox/backbone/views/disposable';

import { createIcon } from '@/io.ox/office/tk/dom';
import { createContactPicture } from '@/io.ox/office/tk/utils/contactutils';

// class UserbadgeView ========================================================

export const UserbadgeView = DisposableView.extend({

    tagName: 'li',

    className: 'participant',

    initialize(options) {
        Object.assign(this, options);
    },

    render() {

        const contactPicture = createContactPicture({ userId: this.participant.id ?? -1 });

        const nameLink = $('<a class="name needs-action">')
            .attr({ href: '#', 'data-detail-popup': 'halo' })
            .text(this.participant.userDisplayName)
            .data({ internal_userid: this.participant.id });

        this.$el.append(
            $('<div class="participant-picture">').append(contactPicture),
            $('<div class="participant-username">').append(nameLink)
        );

        if (this.participant.clientUID === this.app.rtModel.get('presenterId')) {
            this.$el.append(
                //#. tooltip for the icon that identifies the presenting user
                $('<div class="participant-role">').attr('title', gt('Presenter')).append(createIcon('bi:tv'))
            );
        }

        return this;
    }
});
