/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';
import Swiper, { Navigation } from '$/swiper';

import DisposableView from '$/io.ox/backbone/views/disposable';
import DocConverterUtils from '$/io.ox/core/tk/doc-converter-utils';

import { pdfloader } from '$/io.ox/core/pdf/pdfloader';
import PDFDocumentManager from '$/io.ox/core/pdf/pdfdocumentmanager';
import { renderSinglePage } from '$/io.ox/core/pdf/canvasrenderer';
import { getPDFloadErrorDescription, getPdfPageDimension, clearCanvas } from '$/io.ox/core/pdf/util';

import { createIcon } from "@/io.ox/office/tk/dom";
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ZoomState } from '@/io.ox/office/baseframework/utils/zoomstate';
import { NavigationView } from '@/io.ox/office/presenter/views/navigationview';
import * as Notification from '@/io.ox/office/presenter/views/notification';

// constants ==================================================================

const SWIPER_PARAMS_DEFAULT = {
    loop: false,
    loopedSlides: 0,
    followFinger: false,
    simulateTouch: false,
    noSwiping: true,
    speed: 0,
    spaceBetween: 0,
    runCallbacksOnInit: false
};

const SWIPER_PARAMS_SWIPING_ENABLED = {
    noSwiping: false,
    noSwipingClass: 'swiper-no-swiping',    // set back to default
    followFinger: true,
    speed: 300,
    spaceBetween: 100
};

const SWIPER_PARAMS_SWIPING_DISABLED = {
    noSwiping: true,
    noSwipingClass: 'swiper-slide',         // disable swiping for every slide
    followFinger: false,
    speed: 0,
    spaceBetween: 0
};

// predefined zoom factors.
// iOS Limits are handled by pdfview.js
const ZOOM_STEPS = [0.25, 0.35, 0.5, 0.75, 1, 1.25, 1.5, 2, 3, 4];

// private functions ==========================================================

/**
 * Creates the HTML mark-up for a slide navigation button.
 *
 * @param {String} type = 'left'|'right'
 *  the button type to create, could be 'left' or 'right'.
 *
 * @param {String} selectorClass
 *  the CSS selector class for the button.
 *
 * @returns {jQuery}
 *  the button node.
 */
function createNavigationButton(type, selectorClass) {

    const left = type === 'left';
    const button = $('<a href="#" class="viewer-overlay-button swiper-button-control" role="button" aria-controls="presenter-carousel">');

    button.addClass(selectorClass);
    button.addClass('visible-swiper-button');
    button.addClass(left ? 'swiper-button-prev  left' : 'swiper-button-next right');
    button.attr(left ? {
        //#. button tooltip for 'go to previous presentation slide' action
        title: gt('Previous slide'),
        'aria-label': gt('Previous slide')
    } : {
        //#. button tooltip for 'go to next presentation slide' action
        title: gt('Next slide'),
        'aria-label': gt('Next slide')
    });

    const iconId = left ? 'bi:chevron-left' : 'bi:chevron-right';
    return button.append(createIcon(iconId));
}

// class SlideView ============================================================

/**
 * The presentation view is responsible for displaying the presentation slides.
 */
export const SlideView = DisposableView.extend({

    // class "viewer-displayer" needed to derive CSS styling of OX Viewer
    className: 'viewer-displayer presenter-presentation',

    attributes: { tabindex: -1, role: 'main' },

    initialize(options) {
        Object.assign(this, options);

        // the RT connection
        this.rtConnection = this.app.rtConnection;
        // amount of page side margins in pixels
        this.PAGE_SIDE_MARGIN = _.device('desktop') ? 30 : 15;
        // timeout object for the slide caption
        this.captionTimeoutId = null;
        // the swiper carousel root node
        this.carouselRoot = null;
        // instance of the swiper plugin
        this.swiper = null;
        // the pdfDocumentManager instance
        this.pdfDocumentManager = null;
        // a Deferred object indicating the load process of this document view.
        this.documentLoad = $.Deferred();
        // all page nodes with contents, keyed by one-based page number
        this.loadedPageNodes = {};
        // the timer that loads more pages above and below the visible ones
        this.loadMorePagesTimerId = null;
        // current zoom factor
        this.zoomState = new ZoomState(ZOOM_STEPS);
        // current scroll position of the page
        this.currentScrollPosition = { top: 0, left: 0 };
        // the pdf document container
        this.documentContainer = null;
        // create a debounced version of zoom function
        this.autoZoomDebounced = _.debounce(() => this.zoomState?.set("slide"), 100);
        // create a debounced version of the resize handler
        this.onResizeDebounced = _.debounce(this.onResize.bind(this), 100);
        // create a debounced version of refresh function
        this.refreshDebounced = _.debounce(this.refresh.bind(this), 100);
        // the index of the current slide, defaults to the first slide
        this.currentSlideIndex = 0;
        // the slide count, defaults to 1
        this.numberOfSlides = 1;
        // the index of the slide to start the presentation with
        // TODO: check if needed here
        this.startIndex = 0;
        // the slide navigation view
        this.navigationView = new NavigationView(options);
        // register resize handler
        this.listenTo(this.presenterEvents, 'presenter:resize', this.onResizeDebounced);
        // bind zoom events
        this.listenTo(this.presenterEvents, 'presenter:zoomin', this.onZoomIn);
        this.listenTo(this.presenterEvents, 'presenter:zoomout', this.onZoomOut);
        // register slide change handler
        this.listenTo(this.presenterEvents, 'presenter:remote:slide:change', this.onRemoteSlideChange);
        // register participants change handler
        this.listenTo(this.presenterEvents, 'presenter:participants:change', this.onParticipantsChange);
        // register presentation pause /continue handler
        this.listenTo(this.presenterEvents, 'presenter:presentation:pause', this.onPresentationPause);
        this.listenTo(this.presenterEvents, 'presenter:presentation:continue', this.onPresentationContinue);
        // register thumbnail view slide select handler
        this.listenTo(this.presenterEvents, 'presenter:showslide', this.showSlide);
        // register presentation start/end handler
        this.listenTo(this.presenterEvents, 'presenter:presentation:start', this.onPresentationStartEnd);
        this.listenTo(this.presenterEvents, 'presenter:presentation:end', this.onPresentationStartEnd);

        // update CSS when zooming
        this.zoomState.register("slide", () => this.getFitScreenZoomFactor());
        this.zoomState.on("change:state", event => this.onZoomChanged(event));
        this.updateZoomAttrs();
    },

    /**
     * Renders the SlideView.
     *
     * @returns {SlideView}
     */
    render() {
        var documentUrl   = DocConverterUtils.getEncodedConverterUrl(this.model),
            carouselRoot  = $('<div id="presenter-carousel" class="swiper swiper-container" role="listbox">'),
            carouselInner = $('<div class="swiper-wrapper document-container io-ox-core-pdf">'),
            caption       = $('<div class="viewer-displayer-caption">');

        // append carousel to view
        carouselRoot.append(carouselInner);
        this.$el.append(
            carouselRoot,
            caption,
            this.navigationView.render().el
        );

        // create pause overlay
        this.renderPauseOverlay();

        this.carouselRoot = carouselRoot;
        this.documentContainer = carouselInner;
        // load the pdf file
        this.pdfloader = pdfloader();
        // display loading animation
        this.documentContainer.busy();

        // enable touch events
        if (this.documentContainer.enableTouch) {
            this.documentContainer.enableTouch({
                tapHandler: this.onTap.bind(this),
                // pinchHandler: this.onPinch.bind(this)
            });
        }

        // wait for PDF document to finish loading
        $.when(this.pdfloader.fetchWithProgress(documentUrl, () => {}))
        .then(this.pdfDocumentLoadSuccess.bind(this), this.pdfDocumentLoadError.bind(this))
        .always(this.pdfDocumentLoadFinished.bind(this));

        return this;
    },

    /**
     * Creates the presentation pause overlay.
     */
    renderPauseOverlay() {
        var overlay = $('<div class="pause-overlay">');

        var infoBox = $('<div class="pause-infobox">');
        //#. Info text that says the presentation is paused.
        var pauseNotification = $('<span class="pause-message">').text(gt('Presentation is paused.'));
        var leaveButton = $('<button type="button" class="btn btn-default pause-leave">')
            .attr({
                //#. tooltip for the leave presentation button
                title: gt('Leave presentation'),
                'aria-label': gt('Leave presentation')
            })
            //#. label for the leave presentation button
            .text(gt('Leave'));

        var pauseButton = $('<a href="#" class="pause-continue" role="button">')
            .attr({
                //#. tooltip for the continue presentation button
                title: gt('Continue presentation'),
                'aria-label': gt('Continue presentation')
            });

        pauseButton.append(createIcon('bi:pause-fill', 100));

        // leave the paused remote presentation
        function onPauseLeave() {
            this.togglePauseOverlay();
            this.app.rtConnection.leavePresentation();
            this.app.mainView.toggleFullscreen(false);
        }
        // continue the paused local presentation
        function onPauseContinue() {
            var localModel = this.app.localModel;
            var clientUID = this.rtConnection.getRTUuid();
            localModel.continuePresentation(clientUID);
            this.togglePauseOverlay();
        }

        leaveButton.on('click', onPauseLeave.bind(this));
        infoBox.append(pauseNotification, leaveButton);
        pauseButton.on('click', onPauseContinue.bind(this));
        overlay.append(pauseButton, infoBox);
        this.$el.append(overlay);
    },

    /**
     * Tap event handler.
     * - switches to the next slide.
     * - zooms the presentation slides to fit on screen in case of a double tap.
     *
     * @param {jQuery.Event} event
     *  The jQuery event object.
     *
     * @param {Number} tapCount
     *  The count of taps, indicating a single or double tap.
     */
    onTap(event, tapCount) {
        // on hybrid devices onSlideClick() handles click and tap
        if (_.device('desktop')) { return; }

        if (tapCount === 1) {
            this.showNextSlide();

        } else if (tapCount === 2) {
            this.zoomState.set("slide");
            // prevent iOS default double tap zoom
            if (_.device('iOS')) { event.preventDefault(); }
        }
    },

    /**
     * Handles Swiper slide change end events
     */
    onSlideChangeEnd() {
        var activeSlideIndex  = this.swiper.activeIndex;
        var activeSlideNode   = this.getActiveSlideNode();
        var previousSlideNode = this.getPreviousSlideNode();

        this.currentSlideIndex = activeSlideIndex;

        this.currentScrollPosition = { top: 0, left: 0 };

        // a11y
        activeSlideNode.attr('aria-selected', 'true');
        previousSlideNode.attr('aria-selected', 'false');

        this.loadVisiblePages();

        //#. text of a presentation slide caption
        //#. Example result: "1 of 10"
        //#. %1$d is the slide index of the current
        //#. %2$d is the total slide count
        this.showCaption(gt('%1$d of %2$d', activeSlideIndex + 1, this.numberOfSlides));

        this.presenterEvents.trigger('presenter:local:slide:change', activeSlideIndex);
    },

    /**
     * Handles Swiper slide change start events
     */
    onSlideChangeStart() {
        var rtModel   = this.app.rtModel;
        var clientUID = this.app.rtConnection.getRTUuid();
        if (rtModel.isPresenter(clientUID)) {
            this.rtConnection.updateSlide({ activeSlide: this.swiper.activeIndex });
        }
    },

    /**
     * Handles presentation start and end.
     */
    onPresentationStartEnd() {
        this.updateNavigationArrows();
        this.updateSwiperParams();
    },

    /**
     * Handles remote slide changes invoked by the real-time framework.
     *
     * @param {Number} index
     *  the index of the slide to be shown.
     */
    onRemoteSlideChange(index) {
        var rtModel   = this.app.rtModel,
            clientUID = this.app.rtConnection.getRTUuid();

        if (rtModel.isJoined(clientUID) && !rtModel.isPresenter(clientUID)) {
            this.internalShowSlide(index);
        }
    },

    /**
     * Handles remote participants changes invoked by the real-time framework.
     */
    onParticipantsChange() {
        this.updateNavigationArrows();
        this.updateSwiperParams();
        this.togglePauseOverlay();
    },

    /**
     * Handles presentation pause invoked by the real-time framework.
     */
    onPresentationPause() {
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();

        this.updateNavigationArrows();
        this.updateSwiperParams();
        this.togglePauseOverlay();

        if (localModel.canContinue(clientUID) || rtModel.canContinue(clientUID)) {
            this.hideNavigation(0);
        }
    },

    /**
     * Handles presentation continue invoked by the real-time framework.
     */
    onPresentationContinue() {
        this.updateNavigationArrows();
        this.updateSwiperParams();
        this.togglePauseOverlay();
    },

    /**
     * Toggles the visibility of the pause overlay for presentation participants.
     */
    togglePauseOverlay() {
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();

        // to see the pause overlay the presentation needs to be paused and the user needs to be
        // the presenter of a local presentation or joined in a remote presentation.
        if (localModel.canShowPauseOverlay(clientUID)) {
            this.$('.pause-overlay').addClass('local-presenation').removeClass('remote-presenation').show();
        } else if (rtModel.canShowPauseOverlay(clientUID)) {
            this.$('.pause-overlay').addClass('remote-presenation').removeClass('local-presenation').show();
        } else {
            this.$('.pause-overlay').hide();
        }
    },

    /**
     * Returns the previously active Swiper slide node.
     *
     * @returns {jQuery}
     *  the active node.
     */
    getPreviousSlideNode() {
        if (!this.swiper || !this.swiper.slides || !_.isNumber(this.swiper.previousIndex)) {
            return $();
        }

        var node = this.swiper.slides[this.swiper.previousIndex];
        return node ? $(node) : $();
    },

    /**
     * Returns the active Swiper slide node.
     *
     * @returns {jQuery}
     *  the active node.
     */
    getActiveSlideNode() {
        if (!this.swiper || !this.swiper.slides) {
            return $();
        }

        var node = this.swiper.slides[this.swiper.activeIndex];
        return node ? $(node) : $();
    },

    /**
     * Focuses the swiper's current active slide.
     */
    focusActiveSlide() {
        this.getActiveSlideNode().focus();
    },

    /**
     * Returns the active slide index.
     *
     * @returns {Number}
     *  the active slide index.
     */
    getActiveSlideIndex() {
        return (this.swiper) ? this.swiper.activeIndex : 0;
    },

    /**
     * Returns the slide count.
     *
     * @returns {Number}
     *  the slide count.
     */
    getSlideCount() {
        return this.numberOfSlides || 0;
    },

    /**
     * Switches Swiper to the slide with the given index.
     *
     * @param {Number} index
     *  the index of the slide to be shown.
     */
    internalShowSlide(index) {
        if (this.swiper && _.isNumber(index)) {
            this.swiper.slideTo(index, 0);
        }
    },

    /**
     * Switches Swiper to the slide with the given index,
     * but only if the user is presenter or has not joined the presentation.
     *
     * @param {Number} index
     *  the index of the slide to be shown.
     */
    showSlide(index) {
        var rtModel   = this.app.rtModel,
            clientUID = this.app.rtConnection.getRTUuid();

        if (!rtModel.isJoined(clientUID) || rtModel.isPresenter(clientUID)) {
            this.internalShowSlide(index);
        }
    },

    /**
     * Switches Swiper to the next slide,
     * but only if the user is presenter or has not joined the presentation.
     *
     * @param {jQuery.Event} [event]
     *  the optional event.
     */
    showNextSlide(event) {
        var rtModel   = this.app.rtModel;
        var clientUID = this.app.rtConnection.getRTUuid();

        if (event) {
            event.preventDefault();
        }

        if (this.swiper && (!rtModel.isJoined(clientUID) || rtModel.isPresenter(clientUID))) {
            this.swiper.slideNext();
        }
    },

    /**
     * Switches Swiper to the previous slide,
     * but only if the user is presenter or has not joined the presentation.
     *
     * @param {jQuery.Event} [event]
     *  the optional event.
     */
    showPreviousSlide(event) {
        var rtModel   = this.app.rtModel;
        var clientUID = this.app.rtConnection.getRTUuid();

        if (event) {
            event.preventDefault();
        }

        if (this.swiper && (!rtModel.isJoined(clientUID) || rtModel.isPresenter(clientUID))) {
            this.swiper.slidePrev(0, true);
        }
    },

    /**
     * Updates the navigation arrow buttons visible state according to the RTModel data.
     *  Show the buttons in the following cases:
     *  - the presentation has not been started
     *  - the current user is a participant and has not (yet) joined the presentation
     *  - the current user is the presenter and the presentation is paused
     */
    updateNavigationArrows() {
        var rtModel          = this.app.rtModel;
        var localModel       = this.app.localModel;
        var clientUID        = this.app.rtConnection.getRTUuid();
        var navigationArrows = this.$el.find('.swiper-button-control');

        if (!localModel.isPresenter(clientUID) && (!rtModel.isJoined(clientUID) || (rtModel.isPresenter(clientUID) && rtModel.isPaused()))) {
            navigationArrows.show();
        } else {
            navigationArrows.hide();
        }
    },

    /**
     * Updates the Swiper parameters according to the RTModel data.
     *  Changing slides is enabled if:
     *  - the presentation has not yet been started
     *  - the current user is a participant and has not (yet) joined the presentation
     *  - the current user is the presenter
     */
    updateSwiperParams() {
        var rtModel   = this.app.rtModel;
        var clientUID = this.app.rtConnection.getRTUuid();
        var enableSlideChange  = !rtModel.isJoined(clientUID) || rtModel.isPresenter(clientUID);
        var enableSwiping;

        if (!this.swiper) { return; }

        // on touch devices handle swiping
        if (_.device('touch')) {
            enableSwiping = enableSlideChange && !this.isZoomedIn();
            _.extend(this.swiper.params, (enableSwiping ? SWIPER_PARAMS_SWIPING_ENABLED : SWIPER_PARAMS_SWIPING_DISABLED));
        }
    },

    /**
     * Shows the passed text in a caption for a specific duration.
     *
     * @param {String} text
     *  the text to be displayed in the caption.
     *
     * @param {[Number = 3000]} duration
     *  the display duration of the caption in milliseconds. Defaults to 3000 ms.
     */
    showCaption(text, duration) {
        var slideCaption = this.$el.find('.viewer-displayer-caption');
        var captionContent = $('<div class="caption-content">').text(text);

        slideCaption.empty().append(captionContent);
        window.clearTimeout(this.captionTimeoutId);
        slideCaption.show();
        this.captionTimeoutId = window.setTimeout(function () {
            slideCaption.fadeOut();
        }, (duration || 3000));
    },

    /**
     * Show navigation panel,
     * but only if the current user is the presenter and the presentation is not paused.
     */
    showNavigation() {
        var clientUID  = this.app.rtConnection.getRTUuid();
        var localModel = this.app.localModel;
        var rtModel    = this.app.rtModel;

        if (localModel.isPresenting(clientUID) || rtModel.isPresenting(clientUID)) {
            this.navigationView.showNavigation();
        }
    },

    /**
     * Hide navigation panel
     *
     * @param {[Number = 1000]} duration
     *  the duration of the fade out animation in milliseconds. Defaults to 1000 ms.
     */
    hideNavigation(duration) {
        this.navigationView.hideNavigation(duration || 1000);
    },

    /**
     * Returns the page node for the given page number.
     *
     * @param {Number} pageNumber
     *  The 1-based number of the page node to return.
     *
     * @returns {jquery.Node} pageNode
     *  The jQuery page node for the requested page number.
     */
    getPageNode(pageNumber) {
        return ((pageNumber > 0) && this.documentContainer) ?
            this.documentContainer.children().eq(pageNumber - 1).find('.document-page') : $();
    },

    /**
     * Returns the active page index.
     *
     * @returns {Number}
     *  the active page index.
     */
    getActivePageIndex() {
        return (this.getActiveSlideIndex() + 1);
    },

    /**
     * Returns the active page node.
     *
     * @returns {jquery.Node} pageNode
     *  The active jQuery page node.
     */
    getActivePageNode() {
        return this.getPageNode(this.getActivePageIndex());
    },

    /**
     * Loads all pages that are currently visible in the DocumentView plus
     * one page before the visible pages and one page after the visible pages.
     */
    loadVisiblePages() {
        var currentPage = this.currentSlideIndex + 1;

        // abort old requests not yet running
        this.cancelMorePagesTimer();

        // load visible page with high priority
        this.loadPage(currentPage);

        // load the invisible pages above and below the visible area with medium priority after a short delay
        this.loadMorePagesTimerId = window.setTimeout(() => {
            // pages before the visible pages
            if (currentPage > 1) {
                this.loadPage(currentPage - 1);
            }
            // pages after the visible pages
            if (currentPage < this.numberOfSlides) {
                this.loadPage(currentPage + 1);
            }
        }, 50);

        // clear all other pages
        _.each(this.loadedPageNodes, (pageNode, pageNumber) => {
            if ((pageNumber < currentPage - 1) || (pageNumber > currentPage + 1)) {
                this.emptyPageNode(pageNode);
                delete this.loadedPageNodes[pageNumber];
            }
        });
    },

    /**
     * Loads the specified page, and stores the original page size at the
     * page node.
     *
     * @param {Number} pageNumber
     *  The 1-based page number.
     */
    loadPage(pageNumber) {
        // the page node of the specified page
        var pageNode = this.getPageNode(pageNumber);
        // the slide node of the specified page
        var slideNode = pageNode.parent();

        // to prevent mobile Safari crashing due to rendering overload
        // only the rendered slides are set to visible, all others are hidden.
        slideNode.css({ visibility: 'visible' });

        // do not load correctly initialized page again
        if (pageNode.children().length === 0) {
            this.loadedPageNodes[pageNumber] = pageNode;
            this.refresh(pageNumber);
            this.presenterEvents.trigger('presenter:page:loaded', pageNumber);
        }
    },

    renderPDFslide(pageNode, pageZoom) {
        // re-use canvas if possible, this will reduce memory usage especially on iOS
        const canvasNode = pageNode.find('canvas').length ? pageNode.find('canvas') : $('<canvas style="display: block;">');
        canvasNode.empty();
        const pageNumber = parseInt(pageNode.attr('data-page') || '', 10) || 1;
        renderSinglePage(this.pdfDocumentManager.pdfDocument, canvasNode[0], pageNumber, pageZoom);
        pageNode.append(canvasNode);
    },

    /**
     * Refreshes the specified page or all pages.
     *
     * @param {Number} [pageNumberToRefresh]
     *  The 1-based page number to refresh.
     *  If not set all pages were refreshed.
     */
    refresh(pageNumberToRefresh) {
        if (this.numberOfSlides > 0) {

            var pageZoom = this.zoomState.factor;

            if (_.isNumber(pageNumberToRefresh)) {

                var pageNode  = this.getPageNode(pageNumberToRefresh);
                this.renderPDFslide(pageNode, pageZoom);
            } else {
                var slides = this.swiper?.slides;
                // empty all pages in case of a complete refresh request
                _.each(slides, slide => {
                    // get page node from slide node
                    const pageNode = $(slide).children(':first');
                    // detach page node
                    pageNode.detach();
                    // empty page node
                    this.emptyPageNode(pageNode);
                    // re-attach page node to slide node again
                    pageNode.appendTo(slide);

                }, this);

                this.loadVisiblePages();
            }
        }
    },

    /**
     * Cancels all running background tasks regarding updating the page
     * nodes in the visible area.
     */
    cancelMorePagesTimer() {
        // cancel the timer that loads more pages above and below the visible
        // area, e.g. to prevent (or defer) out-of-memory situations on iPad
        if (this.loadMorePagesTimerId) {
            window.clearTimeout(this.loadMorePagesTimerId);
            this.loadMorePagesTimerId = null;
        }
    },

    /**
     * Clears a page node
     *
     * @param {jquery.Node} pageNode
     *  The jQuery page node to clear.
     */
    emptyPageNode(pageNode) {
        if (pageNode) {
            // to prevent mobile Safari crashing due to rendering overload
            // set slides with empty pages to hidden, only rendered slides
            // are set to visible.
            pageNode.parent().css({ visibility: 'hidden' });
            // canvas size should be reset to reduce canvas memory consumption on iOS
            const canvas = pageNode.find('canvas');
            if (canvas.length) {
                clearCanvas(canvas[0]);
                canvas.remove();
            }
            pageNode.empty();
        }
    },

    /**
     * Actions which always have to be done after pdf document loading process
     */
    pdfDocumentLoadFinished() {
        //console.info('Presenter - pdfDocumentLoadFinished()');
        if (this.documentContainer) { this.documentContainer.idle(); }
    },

    /**
     * PDF document load handler
     *
     * @param {Object} pdfFile
     *  page count of the pdf document delivered by the PDF.js library,
     *  or an error object.
     */
    async pdfDocumentLoadSuccess(pdfFile) {

        //console.info('Presenter - pdfDocumentLoadSuccess()', 'page-count:', pageCount);

        this.pdfDocumentManager = new PDFDocumentManager(pdfFile);
        try {
            await this.pdfDocumentManager.open();
        } catch (er) {
            this.pdfDocumentLoadError(er);
            return;
        }

        const pageCount = this.pdfDocumentManager.pdfDocument.numPages;

        this.pdfDefaultSize = await getPdfPageDimension(this.pdfDocumentManager.pdfDocument, 1);

        var uniqueId = _.uniqueId();
        var swiperNextButtonClass = 'presenter-button-next-' + uniqueId;
        var swiperPrevButtonClass = 'presenter-button-prev-' + uniqueId;

        // configure Swiper
        //
        // listen to onTransitionEnd instead of onSlideChangeEnd. Swiper doesn't trigger the onSlideChangeEnd event
        // when a resize events occurs during the processing of a swiper.slideTo() call.

        this.numberOfSlides    = pageCount;
        // set scale/zoom according to device's viewport
        this.zoomState.set("slide");

        // add navigation buttons
        if (pageCount > 1) {
            this.carouselRoot.append(
                createNavigationButton('left', swiperPrevButtonClass),
                createNavigationButton('right', swiperNextButtonClass)
            );
        }

        _.times(pageCount, function (index) {
            // to prevent mobile Safari crashing due to rendering overload default the slide 'visibility' to 'hidden'
            // and set only the rendered slides to 'visible'.
            var pageNumber   = index + 1;
            // create DOM from strings to optimize performance for presentations with lots of slides
            var swiperSlide  = '<div class="swiper-slide" tabindex="-1" role="option" aria-selected="false" style="visibility: hidden;">';
            var documentPage = '<div class="document-page" data-page="' + pageNumber + '"></div>';

            this.documentContainer.append(swiperSlide + documentPage + '</div>');

        }, this);

        var swiperParameter = _.extend({
            initialSlide: this.startIndex,
            navigation: {
                nextEl: '.' + swiperNextButtonClass,
                prevEl: '.' + swiperPrevButtonClass
            },
            on: {
                slideChangeTransitionStart: this.onSlideChangeStart.bind(this),
                slideChangeTransitionEnd: this.onSlideChangeEnd.bind(this)
            }
        }, SWIPER_PARAMS_DEFAULT);

        if (_.device('touch')) {
            swiperParameter = _.extend(swiperParameter, SWIPER_PARAMS_SWIPING_ENABLED);
        }

        // initiate swiper
        Swiper.use([Navigation]);
        this.swiper = new Swiper(this.carouselRoot[0], swiperParameter);
        this.pages  = this.$el.find('.document-page');

        // render visible PDF pages
        this.loadVisiblePages();

        // trigger initial slide change event
        this.presenterEvents.trigger('presenter:local:slide:change', this.startIndex);

        // bind slide click handler
        if (_.device('desktop')) {
            this.pages.on('mousedown mouseup', this.onSlideClick.bind(this));
        }

        // resolve the document load Deferred: this document view is fully loaded.
        this.documentLoad.resolve();
        // focus first active slide initially
        this.focusActiveSlide();
    },

    /**
     * Error handler for the PDF loading process.
     *
     * @param {Object} error
     *  the error data
     */
    pdfDocumentLoadError(error) {
        globalLogger.warn('Presenter - failed loading PDF document. Cause: ', error, error.cause);

        // make notification visible by hiding the swiper
        this.carouselRoot.css('display', 'none');

        const { notificationText } = getPDFloadErrorDescription(error);
        this.$el.append(Notification.createMessageNode(notificationText, 'bi:exclamation-triangle'));

        // reject the document load Deferred.
        this.documentLoad.reject();
    },

    /**
     * Calculates the 'fit to page' zoom factor of this document.
     *
     * @param {boolean} [ignoreOffset=false]
     *  If true the 'fit to page' zoom doesn't leave space for slide margins.
     *
     * @returns {number}
     *  The zoom factor fitting the document to the screen.
     */
    getFitScreenZoomFactor(ignoreOffset) {
        const fullscreen          = this.app.mainView.fullscreen;
        const offset              = (fullscreen || ignoreOffset) ? 0 : 40;
        const slideHeight         = this.$el.height();
        const slideWidth          = this.$el.width();
        const originalPageSize    = { width: this.pdfDefaultSize.width, height: this.pdfDefaultSize.height };
        const fitWidthZoomFactor  = (slideWidth - offset) / originalPageSize.width;
        const fitHeightZoomFactor = (slideHeight - offset) / originalPageSize.height;
        return Math.min(fitWidthZoomFactor, fitHeightZoomFactor);
    },

    /**
     * Zooms in of a document.
     */
    onZoomIn() {
        this.zoomState.inc();
    },

    /**
     * Zooms out of the document.
     */
    onZoomOut() {
        this.zoomState.dec();
    },

    updateZoomAttrs() {
        const { type, factor } = this.zoomState;
        this.app.setRootAttribute('data-zoom-type', type);
        this.app.setRootAttribute('data-zoom-factor', Math.round(factor * 100));
    },

    onZoomChanged(event) {

        var activeSlideNode = this.getActiveSlideNode();

        // the vertical scroll position before zooming
        var scrollTopBeforeZoom = activeSlideNode.scrollTop();
        // the horizontal scroll position before zooming
        var scrollLeftBeforeZoom = activeSlideNode.scrollLeft();
        // the vertical scroll position after zooming
        var scrollTopAfterZoom = scrollTopBeforeZoom * event.scale;
        // the horizontal scroll position after zooming
        var scrollLeftAfterZoom = scrollLeftBeforeZoom * event.scale;

        // store scroll position to be set when calling refresh
        this.currentScrollPosition = { top: scrollTopAfterZoom, left: scrollLeftAfterZoom };

        // set page zoom to all pages and apply the new size to all page wrappers
        this.refreshDebounced();

        // update document status according to the zoom level
        this.$el.toggleClass('zoomed-in', this.isZoomedIn());

        // update zoom level specific swiper params
        this.updateSwiperParams();

        //#. text of a presentation zoom level caption
        //#. Example result: "100 %"
        //#. %1$d is the zoom level
        this.showCaption(gt('%1$d %', Math.round(event.factor * 100)));

        // DOCS-4342: put zoom factor into DOM for automated tests
        this.updateZoomAttrs();
    },

    /**
     * Returns true if the document is zoomed that it does no longer fit
     * to the page, false otherwise.
     *
     * @returns {boolean}
     *  Wheter the document is zoomed that it doesn't fit to the page.
     */
    isZoomedIn() {
        return this.getFitScreenZoomFactor(true) < this.zoomState.factor;
    },

    /**
     * Resize handler of the SlideView.
     * - calculates and sets a new initial zoom factor
     */
    async onResize() {
        if (ox.ui.App.getCurrentApp().getName() !== 'io.ox/office/presenter') {
            return;
        }

        await this.documentLoad;

        // var swiperSlide = this.getActiveSlideNode();

        this.autoZoomDebounced();

        if (this.swiper) {
            this.swiper.update();
        }
    },

    /**
     * Presentation slide click handler.
     * - detects if a user is doing a text selection or a click
     * - show next slide on clicks.
     */
    onSlideClick: (function () {

        var x, y;

        return function (event) {
            //only allow left click
            if (event.button) { return; }

            switch (event.type) {
                case 'mousedown':
                    x = event.clientX;
                    y = event.clientY;
                    break;
                case 'mouseup':
                    if (event.clientX === x && event.clientY === y) {
                        this.showNextSlide();
                    }
                    break;
                // no default
            }
        };
    }()),

    /**
     * Destructor function of the SlideView.
     */
    onDispose() {
        //console.info('Presenter - dispose SlideView');

        // remove touch events
        if (this.documentContainer.disableTouch) {
            this.documentContainer.disableTouch();
        }

        // destroy the page loader instance
        this.cancelMorePagesTimer();

        // empty all slides to free memory in iOS
        const slides = this.swiper?.slides;
        _.each(slides, slide => {
            // get page node from slide node
            const pageNode = $(slide).children(':first');
            this.emptyPageNode(pageNode);

        }, this);

        // destroy the swiper instance
        if (this.swiper) {
            this.swiper.destroy();
            this.swiper = null;
        }

        // destroy the pdf view and model instances
        this.pdfDocumentManager?.close();
        this.pdfDocumentManager = null;
        this.zoomState.destroy();
        this.zoomState = null;

        this.model = null;
        this.captionTimeoutId = null;
        this.carouselRoot = null;
        this.documentContainer = null;
    }
});
