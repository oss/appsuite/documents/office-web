/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import _ from '$/underscore';
import Backbone from '$/backbone';

import DisposableView from '$/io.ox/backbone/views/disposable';
import ext from '$/io.ox/core/extensions';
import SessionRestore from '$/io.ox/core/tk/sessionrestore';

import { SlideView } from '@/io.ox/office/presenter/views/slideview';
import { SidebarView } from '@/io.ox/office/presenter/views/sidebarview';
import { ToolbarView } from '@/io.ox/office/presenter/views/toolbarview';
import { ThumbnailView } from '@/io.ox/office/presenter/views/thumbnailview';
import * as Notification from '@/io.ox/office/presenter/views/notification';

import { isFullscreenEnabled, isFullscreenActive, enterFullscreen, exitFullscreen, addChangeFullscreenHandler, removeChangeFullscreenHandler } from '@/io.ox/office/tk/dom';

import '$/io.ox/core/tk/nodetouch';

// side effect imports to recycle CSS of OX Viewer
import '$/io.ox/core/viewer/main';
import '$/io.ox/core/viewer/views/displayerview';

/**
 * The main view is the base view for the OX Presenter.
 * This view imports, manages and renders these children views:
 * - SlideView
 */
export const MainView = DisposableView.extend({

    // class "io-ox-viewer" needed to derive CSS styling of OX Viewer
    className: 'io-ox-viewer io-ox-presenter io-ox-office-main abs',

    attributes: { tabindex: -1 },

    events: {
        keydown: 'onKeydown'
    },

    // the full screen state of the main view, off by default.
    fullscreen: false,

    initialize(options) {

        Object.assign(this, options);

        // create the event dispatcher
        this.presenterEvents  = _.extend({}, Backbone.Events);

        // create child view(s)
        var childViewParams   = { model: this.model, presenterEvents: this.presenterEvents, app: this.app };
        this.slideView        = new SlideView(childViewParams);
        this.sidebarView      = new SidebarView(childViewParams);
        this.toolbarView      = new ToolbarView(childViewParams);
        this.thumbnailView    = new ThumbnailView(childViewParams);

        // remember the sidebar open state for fullscreen
        this.sidebarBeforeFullscreen = null;

        // handle DOM events
        $(window).on('resize.presenter', this.onWindowResize.bind(this));

        // listen to sidebar toggle events
        this.listenTo(this.presenterEvents, 'presenter:toggle:sidebar', this.onToggleSidebar);
        this.listenTo(this.presenterEvents, 'presenter:sidebar:change:state', this.onSideBarToggled);
        // listen to presentation start, end events
        this.listenTo(this.presenterEvents, 'presenter:presentation:start', this.onPresentationStart);
        this.listenTo(this.presenterEvents, 'presenter:presentation:end', this.onPresentationEnd);
        this.listenTo(this.presenterEvents, 'presenter:presentation:pause', this.onPresentationPause);
        this.listenTo(this.presenterEvents, 'presenter:presentation:continue', this.onPresentationContinue);
        // listen to presenter close evemts
        this.listenTo(this.presenterEvents, 'presenter:close', this.closePresenter);

        // show navigation panel on user activity
        this.$el.on('mousemove', _.throttle(this.onMouseMove.bind(this), 500));

        // listen to RTModel updates
        //this.listenTo(this.app.rtModel, 'change:presenterId change:activeSlide change:paused change:participants', this.onRTModelUpdate);
        this.listenTo(this.app.rtModel, 'change', this.onRTModelUpdate);

        // listen to local model updates
        this.listenTo(this.app.localModel, 'change', this.onLocalModelUpdate);

        // listen to full screen mode changes
        addChangeFullscreenHandler(this.slideView.el, this.onChangeFullscreen.bind(this));
    },

    /**
     * Renders the MainView.
     *
     * @returns {MainView}
     */
    render() {
        var state = false;   // TODO: set according to user role (presenter, listener)

        // append toolbar view
        this.$el.append(
            this.toolbarView.render().el,
            this.sidebarView.render().el,
            this.slideView.render().el,
            this.thumbnailView.render().el
        );

        // set initial sidebar state
        this.sidebarView.toggleSidebar(state);

        return this;
    },

    /**
     * Handles real-time model data changes that are triggered by
     * real-time update messages.
     *
     * @param {RTModel} rtModel
     *  The real-time model instance.
     */
    onRTModelUpdate(rtModel) {
        var currentPresenterId,
            previousPresenterId,
            localSlideId = this.getActiveSlideIndex(),
            remoteSlideId = rtModel.get('activeSlide');

        if (rtModel.hasChanged('activeSlide') || (localSlideId !== remoteSlideId)) {
            this.presenterEvents.trigger('presenter:remote:slide:change', remoteSlideId);
        }
        if (rtModel.hasChanged('participants')) {
            this.presenterEvents.trigger('presenter:participants:change', rtModel.get('participants'));
        }
        if (rtModel.hasChanged('presenterId')) {
            // compare current with previous presenter id
            currentPresenterId = rtModel.get('presenterId');
            previousPresenterId = rtModel.previous('presenterId');

            if (!_.isEmpty(currentPresenterId) && _.isEmpty(previousPresenterId)) {
                this.presenterEvents.trigger('presenter:presentation:start', { presenterId: currentPresenterId, presenterName: rtModel.get('presenterName') });

            } else if (_.isEmpty(currentPresenterId) && !_.isEmpty(previousPresenterId)) {
                this.presenterEvents.trigger('presenter:presentation:end', { presenterId: previousPresenterId, presenterName: rtModel.previous('presenterName') });
            }
        }
        if (rtModel.hasChanged('paused')) {
            // compare current with previous presentation pause state
            var eventType = (rtModel.get('paused') && !rtModel.previous('paused')) ? 'presenter:presentation:pause' : 'presenter:presentation:continue';
            this.presenterEvents.trigger(eventType);
        }

        // always focus in navigation for keyboard stuff
        this.slideView.focusActiveSlide();
    },

    /**
     * Handles local model data changes.
     *
     * @param {LocalModel} localModel
     *  The local model instance.
     */
    onLocalModelUpdate(localModel) {
        if (localModel.hasChanged('presenterId')) {
            // compare current with previous presenter id
            var currentPresenterId  = localModel.get('presenterId');
            var previousPresenterId = localModel.previous('presenterId');

            if (!_.isEmpty(currentPresenterId) && _.isEmpty(previousPresenterId)) {
                this.presenterEvents.trigger('presenter:presentation:start', { presenterId: currentPresenterId, presenterName: localModel.get('presenterName') });

            } else if (_.isEmpty(currentPresenterId) && !_.isEmpty(previousPresenterId)) {
                this.presenterEvents.trigger('presenter:presentation:end', { presenterId: previousPresenterId, presenterName: localModel.previous('presenterName') });
            }
        }
        if (localModel.hasChanged('paused')) {
            // compare current with previous presentation pause state
            var eventType = (localModel.get('paused') && !localModel.previous('paused')) ? 'presenter:presentation:pause' : 'presenter:presentation:continue';
            this.presenterEvents.trigger(eventType);
        }

        // always focus in navigation for keyboard stuff
        this.slideView.focusActiveSlide();
    },

    /**
     * Handles remote presentation start invoked by the real-time framework.
     */
    onPresentationStart() {
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();

        if (rtModel.isPresenter(clientUID)) {
            // store presenter state and slide id to restore presentation on browser reload
            SessionRestore.state('presenter~' + this.model.get('id'), { isPresenter: true, slideId: this.getActiveSlideIndex() });
        }

        if (!localModel.isPresenter(clientUID)) {
            // show presentation start notification to all participants in case of a remote presentation.
            var baton = ext.Baton({ context: this, model: this.model, data: this.model.toJSON() });
            Notification.notifyPresentationStart(this.app.rtModel, this.app.rtConnection, baton);
        }
    },

    /**
     * Handles remote presentation end invoked by the real-time framework.
     *  Note: since the event is not a key, click or touch event,
     *  leaving full screen may not work on all browsers.
     *
     * @param {Object} formerPresenter
     *  @param {String} formerPresenter.presenterId
     *   the user id of the former presenter
     *  @param {String} formerPresenter.presenterName
     *   the display name of the former presenter
     */
    onPresentationEnd(formerPresenter) {
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();

        function wasParticipant(clientUID) {
            return !rtModel.wasPresenter(clientUID) && _.some(rtModel.previous('participants'), function (user) {
                return (clientUID === user.clientUID);
            }, this);
        }

        // handle end of a remote / local presentation
        if (rtModel.wasPresenter(formerPresenter.presenterId)) {

            // show presentation end notification to all participants that joined the remote presentation.
            if (wasParticipant(clientUID)) {
                Notification.notifyPresentationEnd(this.app.rtModel, this.app.rtConnection);
            }

            // leave full screen mode
            this.toggleFullscreen(false);

            // remove presenter id from session store
            SessionRestore.state('presenter~' + this.model.get('id'), null);

        } else if (localModel.wasPresenter(formerPresenter.presenterId)) {
            this.toggleFullscreen(false);
        }
    },

    onPresentationPause() {
        this.onWindowResize();
    },

    onPresentationContinue() {
        this.onWindowResize();
    },

    /**
     * Handle OX Presenter keyboard events
     */
    onKeydown(event) {
        event.stopPropagation();

        var self         = this;
        var rtModel      = this.app.rtModel;
        var localModel   = this.app.localModel;
        var rtConnection = this.app.rtConnection;
        var clientUID    = rtConnection.getRTUuid();

        function togglePause() {
            if (rtModel.canPause(clientUID)) {
                rtConnection.pausePresentation();
                self.toggleFullscreen(false);
            } else if (localModel.canPause(clientUID)) {
                localModel.pausePresentation(clientUID);
            } else if (rtModel.canContinue(clientUID)) {
                rtConnection.continuePresentation();
            } else if (localModel.canContinue(clientUID)) {
                localModel.continuePresentation(clientUID);
            }
        }

        function startOrJoinPresentation() {
            if (rtModel.canStart(clientUID)) {
                var slideId = self.getActiveSlideIndex();
                rtConnection.startPresentation({ activeSlide: slideId });
            } else if (rtModel.canJoin(clientUID)) {
                self.joinPresentation();
            }
        }

        switch (event.which || event.keyCode) {

            case 37: // left arrow : show previous slide
            case 38: // up arrow : show previous slide
            case 33: // page up : show previous slide
                this.showPreviousSlide();
                break;

            case 39: // right arrow : show next slide
            case 40: // down arrow : show next slide
            case 34: // page down : show next slide
                this.showNextSlide();
                break;

            case 8: // ctrl + backspace : ends or leaves the presentation. backspace : show previous slide
                if (event.ctrlKey) {
                    this.endOrLeavePresentation();
                } else {
                    event.preventDefault();
                    this.showPreviousSlide();
                }
                break;

            case 13: // ctrl + enter :  starts or joins a presentation.
                if (event.ctrlKey) {
                    startOrJoinPresentation();
                } else { // enter: show next slide
                    this.showNextSlide();
                }
                break;

            case 36: // home : show first slide
                this.showFirstSlide();
                break;

            case 35: // end : show last slide
                this.showLastSlide();
                break;

            case 190: // period : pause / continue presentation
            case 188: // comma : pause / continue presentation
                togglePause();
                break;

            case 70: // ctrl + shift + f : go into fullscreen for presenters
                if (event.ctrlKey && event.shiftKey && rtModel.isPresenter(clientUID)) {
                    this.toggleFullscreen();
                }
                break;
            // no default
        }
    },

    /**
     * Handles mouse move events.
     * Show navigation panel if mouse location is in the lower part of the window.
     */
    onMouseMove: (function () {
        var x = 0, y = 0;
        return function (event) {
            if (!this.$el) { return; }

            var //max bottom postion relative to the document
                // since height of the main view will be null in full screen mode, we need to take height and offset from the presentation view
                maxY = this.slideView.$el.height() + this.slideView.$el.offset().top,
                // the lower 10% or the last 40 pixel of the window
                showY = Math.min((maxY * 0.90), (maxY - 40));

            // for Chrome's bug: it fires mousemove events without mouse movements
            if (event && event.type === 'mousemove') {
                if (event.clientX === x && event.clientY === y) {
                    return;
                }
                x = event.clientX;
                y = event.clientY;
            }

            // show navigation panel when mouse is inside the lower 10% or the last 40 pixel of the window
            //console.info('ON MOUSE MOVE', event.type, 'x:', x, 'y:', y, (y > showY) ? 'SHOW' : 'HIDE', showY);
            if (y > showY) {
                this.slideView.showNavigation();
            } else {
                this.slideView.hideNavigation();
            }
        };
    }()),

    /**
     * toggle sidebar after the sidebar button is clicked
     */
    onToggleSidebar() {
        this.sidebarView.toggleSidebar();
    },

    /**
     * Handle side-bar toggle
     */
    onSideBarToggled() {
        this.onWindowResize();
    },

    /**
     * Handle browser window resize.
     * Recalculate view dimensions after e.g. window resize events
     */
    onWindowResize() {
        //console.info('Presenter - mainview - onWindowResize()');

        var rightOffset = this.sidebarView.opened ? this.sidebarView.$el.outerWidth() : 0;

        this.slideView.$el.css({ width: window.innerWidth - rightOffset });

        this.thumbnailView.$el.css({ width: window.innerWidth - rightOffset });

        this.presenterEvents.trigger('presenter:resize');
    },

    /**
     * Toggles full screen mode of the main view depending on the given state.
     *  A state of 'true' starts full screen mode, 'false' exits the full screen mode and
     *  'undefined' toggles the full screen state.
     *
     * You can only call this from a user-initiated event (click, key, or touch event),
     * otherwise the browser will deny the request.
     */
    toggleFullscreen(state) {
        if (isFullscreenEnabled() && _.device('!iOS')) {
            if (_.isUndefined(state)) {
                if (isFullscreenActive()) {
                    exitFullscreen();
                } else {
                    enterFullscreen(this.slideView.el);
                }
            } else if (state) {
                enterFullscreen(this.slideView.el);
            } else if (this.fullscreen) {
                exitFullscreen();
            }
        }
    },

    /**
     * Handle full screen mode change event.
     */
    onChangeFullscreen() {
        if (!isFullscreenActive()) {
            // exit fullscreen
            this.fullscreen = false;
            this.sidebarView.toggleSidebar(this.sidebarBeforeFullscreen);
            this.toolbarView.$el.removeClass('hidden');
            this.presenterEvents.trigger('presenter:fullscreen:exit');

        } else {
            // enter fullscreen
            this.fullscreen = true;
            this.sidebarBeforeFullscreen = this.sidebarView.opened;
            this.sidebarView.toggleSidebar(false);
            this.toolbarView.$el.addClass('hidden');
            this.presenterEvents.trigger('presenter:fullscreen:enter');
        }
    },

    /**
     * Returns the active slide index.
     *
     * @returns {Number}
     *  the active slide index.
     */
    getActiveSlideIndex() {
        return this.slideView.getActiveSlideIndex();
    },

    /**
     * Returns the slide count.
     *
     * @returns {Number}
     *  the slide count.
     */
    getSlideCount() {
        return this.slideView.getSlideCount();
    },

    /**
     * Show the slide with the given index, but only if the user is presenter or has not joined the presentation.
     *
     * @param {Number} index
     *  the index of the slide to be shown.
     */
    showSlide(index) {
        this.slideView.showSlide(index);
    },

    /**
     * Show the next slide, but only if the user is presenter or has not joined the presentation.
     */
    showNextSlide() {
        this.slideView.showNextSlide();
        this.slideView.focusActiveSlide();
    },

    /**
     * Show the previous slide, but only if the user is presenter or has not joined the presentation.
     */
    showPreviousSlide() {
        this.slideView.showPreviousSlide();
        this.slideView.focusActiveSlide();
    },

    /**
     * Show the first slide, but only if the user is presenter or has not joined the presentation.
     */
    showFirstSlide() {
        this.showSlide(0);
        this.slideView.focusActiveSlide();
    },

    /**
     * Show the last slide, but only if the user is presenter or has not joined the presentation.
     */
    showLastSlide() {
        var slideCount = this.getSlideCount();

        if (slideCount > 0) {
            this.showSlide(slideCount - 1);
        }

        this.slideView.focusActiveSlide();
    },

    /**
     * Tries to join the presentation. Shows an error alert on failure.
     */
    joinPresentation() {

        // full-screen mode MUST be started synchronously (security)
        this.toggleFullscreen(true);

        // try to join the presentation (may fail due to participants limit)
        var promise = this.app.rtConnection.joinPresentation();

        // handle failed attempt to join the presentation
        promise.fail(function (error) {

            // ugly, but no better solution: leave full-screen mode started above
            this.toggleFullscreen(false);

            // show an alert box for known errors
            if (_.isObject(error) && (error.error === 'PRESENTER_MAX_PARTICIPANTS_FOR_PRESENTATION_REACHED_ERROR')) {
                Notification.notifyMaxParticipantsReached(this.app.rtModel.get('presenterName'));
            }
        }.bind(this));

        return promise;
    },

    /**
     * Ends the presentation if the user is currently presenting (locally or remote),
     * or leaves the presentation if the user participates a remote presentation.
     *
     * @returns {jQuery.Promise}
     */
    endOrLeavePresentation() {
        var rtModel      = this.app.rtModel;
        var localModel   = this.app.localModel;
        var rtConnection = this.app.rtConnection;
        var clientUID    = rtConnection.getRTUuid();

        if (rtModel.isPresenter(clientUID)) {
            return rtConnection.endPresentation();
        } else if (localModel.isPresenter(clientUID)) {
            localModel.endPresentation(clientUID);
        } else if (rtModel.canLeave(clientUID)) {
            return rtConnection.leavePresentation();
        }

        return $.when();
    },

    /**
     * Presenter close handler.
     */
    closePresenter() {
        this.endOrLeavePresentation().done(function () {
            removeChangeFullscreenHandler(this.slideView.el, this.onChangeFullscreen.bind(this));
            this.app.quit();
        }.bind(this));
    },

    /**
     * Destructor function of the MainView.
     */
    onDispose() {
        //console.info('Presenter - dispose MainView');

        $(window).off('resize.presenter');
        this.slideView.remove();
        this.sidebarView.remove();
        this.model.off().stopListening();
        this.slideView = null;
        this.sidebarView = null;

    }
});
