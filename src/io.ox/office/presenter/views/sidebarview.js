/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import DisposableView from '$/io.ox/backbone/views/disposable';

import { ParticipantsView } from '@/io.ox/office/presenter/views/sidebar/participantsview';
import { SlidePeekView } from '@/io.ox/office/presenter/views/sidebar/slidepeekview';

import '$/io.ox/core/viewer/views/sidebarview'; // side effect import to recycle CSS of OX Viewer
import '@/io.ox/office/presenter/views/sidebarview.less';

/**
 * The SidebarView is responsible for displaying additional information
 * for the presenter.
 * This includes sections for the listeners and the next slide.
 * Triggers 'presenter:sidebar:change:state' event when the sidebar opens / closes.
 */
export const SidebarView = DisposableView.extend({

    // class "viewer-sidebar" needed to derive CSS styling of OX Viewer
    className: 'viewer-sidebar presenter-sidebar',

    attributes: { tabindex: -1, role: 'tablist' },

    // the visible state of the side bar, hidden per default.
    opened: false,

    initialize(options) {
        Object.assign(this, options);

        this.listenTo(this.presenterEvents, 'presenter:presentation:start', this.onPresentationStartEnd);
        this.listenTo(this.presenterEvents, 'presenter:presentation:end', this.onPresentationStartEnd);
        this.listenTo(this.presenterEvents, 'presenter:presentation:pause', this.onPresentationPauseContinue);
        this.listenTo(this.presenterEvents, 'presenter:presentation:continue', this.onPresentationPauseContinue);
        this.listenTo(this.presenterEvents, 'presenter:local:slide:change', this.renderSections);
    },

    /**
     * Presentation start/end handler.
     * - renders or hides the next slide peek view for the presenter.
     */
    onPresentationStartEnd(presenter) {
        var rtModel = this.app.rtModel;
        var clientUID = this.app.rtConnection.getRTUuid();

        // check if user currently is, or was the presenter
        if (presenter && presenter.presenterId === clientUID) {
            this.toggleSidebar(rtModel.isPresenting(clientUID));
        }
    },

    /**
     * Handles presentation paused state invoked by the real-time framework.
     * - renders or hides the next slide peek view for the presenter.
     */
    onPresentationPauseContinue() {
        var rtModel = this.app.rtModel;
        var clientUID = this.app.rtConnection.getRTUuid();

        if (rtModel.isPresenter(clientUID)) {
            this.toggleSidebar(true);
        }
    },

    /**
     * Toggles the side bar depending on the state.
     *  A state of 'true' opens the panel, 'false' closes the panel and
     *  'undefined' toggles the side bar.
     *
     * @param {Boolean} [state]
     *  The panel state.
     */
    toggleSidebar(state) {
        var prevState = this.opened;
        // determine current state if undefined
        this.opened = (state === undefined) ? !this.opened : Boolean(state);
        this.$el.toggleClass('opened', this.opened);
        this.renderSections();
        if (prevState !== this.opened) {
            this.presenterEvents.trigger('presenter:sidebar:change:state', this.opened);
        }
    },

    /**
     * Renders the sections for participants and slide peek.
     */
    renderSections() {
        var rtModel           = this.app.rtModel;
        var clientUID         = this.app.rtConnection.getRTUuid();
        var isPresenting      = rtModel.isPresenting(clientUID);
        var sectionView       = null;

        var sectionViewParams = {
            model: this.model,
            presenterEvents: this.presenterEvents,
            app: this.app
        };

        // remove previous sections
        this.$el.empty();
        // handle the next slide peek view class for the presenter
        this.$el.toggleClass('presenting', isPresenting);

        // render sections only if side bar is open
        if (!this.opened) {
            return;
        }

        // show next slide peek if the current user is currently presenting, otherwise show the presentation participants
        sectionView = (isPresenting) ? new SlidePeekView(sectionViewParams) : new ParticipantsView(sectionViewParams);
        this.$el.append(sectionView.render().el);
    },

    /**
     * Renders the sidebar container.
     */
    render() {
        // attach the touch handlers
        if (this.$el.enableTouch) {
            this.$el.enableTouch({ selector: null, horSwipeHandler: this.onHorizontalSwipe.bind(this) });
        }
        return this;
    },

    /**
     * Handles horizontal swipe events.
     *
     * @param {String} phase
     *  The current swipe phase (swipeStrictMode is true, so we only get the 'end' phase)
     *
     * @param {jQuery.Event} event
     *  The jQuery tracking event.
     *
     * @param {Number} distance
     *  The swipe distance in pixel, the sign determines the swipe direction (left to right or right to left)
     *
     */
    onHorizontalSwipe(phase, event, distance) {
        if (distance > 0) {
            this.toggleSidebar();
        }
    },

    /**
     * Destructor function of this view.
     */
    onDispose() {
        if (this.$el.disableTouch) {
            this.$el.disableTouch();
        }
        this.model = null;
    }
});
