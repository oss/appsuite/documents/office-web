/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import ext from '$/io.ox/core/extensions';
import DisposableView from '$/io.ox/backbone/views/disposable';
import Toolbar from '$/io.ox/backbone/views/toolbar';
import { CLOSE_LABEL } from '@/io.ox/office/tk/dom';

import { PRESENTER_ACTION_ID, PRESENTER_TOOLBAR_ID } from '@/io.ox/office/presenter/actions';

// constants ==================================================================

const PRESENTER_RIGHT_TOOLBAR_ID = "io.ox/office/presenter/righttoolbar";

const SHORT_LABELS = $(window).width() < 580;

// toolbar link meta object used to generate extension points later
// - element order determines control order in toolbar
const LEFT_TOOLBAR_LINK_ENTRIES = [{
    id: "start",
    prio: 'hi',
    mobile: 'hi',
    //#. button label for the 'start presentation' dropdown
    title: SHORT_LABELS ? gt('Start') : gt('Start presentation'),
    //#. button tooltip for 'start presentation' dropdown
    tooltip: gt('Start the presentation'),
    dropdown: PRESENTER_TOOLBAR_ID + '/dropdown/start-presentation',
    customize() { this.addClass('presenter-toolbar-start'); }
}, {
    id: "end",
    prio: 'hi',
    mobile: 'hi',
    //#. button label for ending the presentation
    title: SHORT_LABELS ? gt('End') : gt('End presentation'),
    //#. button tooltip for ending the presentation
    tooltip: gt('End the presentation'),
    ref: PRESENTER_ACTION_ID + '/end',
    customize() { this.addClass('presenter-toolbar-end'); }
}, {
    id: "pause",
    prio: 'hi',
    mobile: 'hi',
    //#. button label for pausing the presentation
    title: SHORT_LABELS ? gt('Pause') : gt('Pause presentation'),
    //#. button tooltip for pausing the presentation
    tooltip: gt('Pause the presentation'),
    ref: PRESENTER_ACTION_ID + '/pause',
    customize() { this.addClass('presenter-toolbar-pause'); }
}, {
    id: "continue",
    prio: 'hi',
    mobile: 'hi',
    //#. button label for continuing the presentation
    title: SHORT_LABELS ? gt('Continue') : gt('Continue presentation'),
    //#. button tooltip for continuing the presentation
    tooltip: gt('Continue the presentation'),
    ref: PRESENTER_ACTION_ID + '/continue',
    customize() { this.addClass('presenter-toolbar-continue'); }
}, {
    id: "join",
    prio: 'hi',
    mobile: 'hi',
    //#. button label for joining the presentation
    title: SHORT_LABELS ? gt('Join') : gt('Join presentation'),
    //#. button tooltip for joining the presentation
    tooltip: gt('Join the presentation'),
    ref: PRESENTER_ACTION_ID + '/join',
    customize() { this.addClass('presenter-toolbar-join'); }
}, {
    id: "leave",
    prio: 'hi',
    mobile: 'hi',
    //#. button label for leaving the presentation
    title: SHORT_LABELS ? gt('Leave') : gt('Leave presentation'),
    //#. button tooltip for leaving the presentation
    tooltip: gt('Leave the presentation'),
    ref: PRESENTER_ACTION_ID + '/leave',
    customize() { this.addClass('presenter-toolbar-leave'); }
}];

const RIGHT_TOOLBAR_LINK_ENTRIES = [{
    id: "zoomout",
    prio: 'hi',
    mobile: 'lo',
    icon: 'bi/zoom-out.svg',
    ref: PRESENTER_ACTION_ID + '/zoomout',
    //#. button label for zooming out the presentation
    title: gt('Zoom out'),
    //#. button tooltip for zooming out the presentation
    tooltip: gt('Zoom out'),
    customize() { this.addClass('presenter-toolbar-zoomout'); }
}, {
    id: "zoomin",
    prio: 'hi',
    mobile: 'lo',
    icon: 'bi/zoom-in.svg',
    //#. button label for zooming in the presentation
    title: gt('Zoom in'),
    //#. button tooltip for zooming in the presentation
    tooltip: gt('Zoom in'),
    ref: PRESENTER_ACTION_ID + '/zoomin',
    className: "test",
    customize() { this.addClass('presenter-toolbar-zoomin'); }
}, {
    id: "togglesidebar",
    prio: 'hi',
    mobile: 'hi',
    icon: 'bi/people.svg',
    //#. button label for toggling participants view
    label: gt('View participants'),
    //#. button tooltip for toggling participants view
    title: gt('View participants'),
    ref: PRESENTER_ACTION_ID + '/togglesidebar',
    customize() { this.addClass('presenter-toolbar-togglesidebar'); }
}, {
    id: "fullscreen",
    prio: 'hi',
    mobile: 'lo',
    icon: 'bi/arrows-fullscreen.svg',
    //#. button label for toggling fullscreen mode
    label: gt('Toggle fullscreen'),
    //#. button tooltip for toggling fullscreen mode
    title: gt('Toggle fullscreen'),
    ref: PRESENTER_ACTION_ID + '/fullscreen',
    customize() { this.addClass('presenter-toolbar-fullscreen'); }
}, {
    id: "close",
    prio: 'hi',
    mobile: 'hi',
    icon: 'bi/x-lg.svg',
    label: CLOSE_LABEL,
    title: CLOSE_LABEL,
    ref: PRESENTER_ACTION_ID + '/close',
    customize() { this.addClass('presenter-toolbar-close'); }
}];

// extensions =================================================================

// define extension points for the left toolbar
const extPointLeft = ext.point(PRESENTER_TOOLBAR_ID);
LEFT_TOOLBAR_LINK_ENTRIES.forEach((linkEntry, index) => {
    extPointLeft.extend({ index: (index + 1) * 100, ...linkEntry });
});

// define extension points for the right toolbar
const extPointRight = ext.point(PRESENTER_RIGHT_TOOLBAR_ID);
RIGHT_TOOLBAR_LINK_ENTRIES.forEach((linkEntry, index) => {
    extPointRight.extend({ index: (index + 1) * 100, ...linkEntry });
});

// class ToolbarView ==========================================================

/**
 * The ToolbarView is responsible for displaying the top toolbar, with all its
 * functions buttons/widgets.
 */
export const ToolbarView = DisposableView.extend({

    // class "viewer-toolbar" needed to derive CSS styling of OX Viewer
    className: 'viewer-toolbar presenter-toolbar',

    initialize(options) {
        Object.assign(this, options);

        this.listenTo(this.presenterEvents, 'presenter:presentation:start', this.updateToolbar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:end', this.updateToolbar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:pause', this.updateToolbar);
        this.listenTo(this.presenterEvents, 'presenter:presentation:continue', this.updateToolbar);
        this.listenTo(this.presenterEvents, 'presenter:participants:change', this.updateToolbar);

        const container1 = $('<div class="left-container">');
        const container2 = $('<div class="right-container">');
        this.$el.append(container1, container2);

        // put two different "ToolbarView" instances (left and right), they need distinct root elements!
        this.toolbar1 = new Toolbar({ el: container1, point: PRESENTER_TOOLBAR_ID, strict: false });
        this.toolbar2 = new Toolbar({ el: container2, point: PRESENTER_RIGHT_TOOLBAR_ID, strict: false });
    },

    /**
     * Toggles the visibility of the sidebar.
     */
    onToggleSidebar() {
        this.presenterEvents.trigger('presenter:toggle:sidebar');
    },

    /**
     * Publishes zoom-in event to the MainView event aggregator.
     */
    onZoomIn() {
        this.presenterEvents.trigger('presenter:zoomin');
    },

    /**
     * Publishes zoom-out event to the MainView event aggregator.
     */
    onZoomOut() {
        this.presenterEvents.trigger('presenter:zoomout');
    },

    /**
     * Renders this DisplayerView with the supplied model.
     *
     * @returns {this}
     *  this view object itself.
     */
    render() {
        this.updateToolbar();
        return this;
    },

    /**
     * Update inner toolbar.
     */
    updateToolbar: _.debounce(function () {
        if (!this.model) { return; }

        var modelJson = this.model.toJSON();
        const selection = {
            context: this,
            $el: this.$el,
            model: this.model,
            models: [this.model],
            data: modelJson
        };

        this.toolbar1.setSelection([modelJson], selection);
        this.toolbar2.setSelection([modelJson], selection);
    }, 10),

    /**
     * Destructor of this view
     */
    onDispose() {
        this.model = null;
    }
});
