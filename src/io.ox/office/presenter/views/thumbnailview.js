/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import _ from '$/underscore';

import DisposableView from '$/io.ox/backbone/views/disposable';
import DocConverterUtils from '$/io.ox/core/tk/doc-converter-utils';

import { createElement, createDiv } from '@/io.ox/office/tk/dom';
import { createAbortableDeferred, getThumbnailsToLoad } from '@/io.ox/office/presenter/util';

import '@/io.ox/office/presenter/views/thumbnailview.less';

// class ThumbnailView ========================================================

export const ThumbnailView = DisposableView.extend({

    className: 'presenter-thumbnails',

    events: {
        'click .presenter-thumbnail-link': 'onThumbnailClicked',
        'keydown .presenter-thumbnail-link': 'onThumbnailKeydown'
    },

    initialize(options) {
        Object.assign(this, options);
        // listen to slide change
        this.listenTo(this.presenterEvents, 'presenter:local:slide:change', this.selectThumbnail);
        // listen to window resize
        this.listenTo(this.presenterEvents, 'presenter:resize', this.refreshThumbnails);
        // listen to presenter events that affect the visibility of this view
        this.listenTo(this.presenterEvents, 'presenter:presentation:start', this.onToggleVisibility);
        this.listenTo(this.presenterEvents, 'presenter:presentation:end', this.onToggleVisibility);
        this.listenTo(this.presenterEvents, 'presenter:presentation:pause', this.onToggleVisibility);
        this.listenTo(this.presenterEvents, 'presenter:presentation:continue', this.onToggleVisibility);
        this.listenTo(this.presenterEvents, 'presenter:participants:change', this.onToggleVisibility);
        this.listenTo(this.presenterEvents, 'presenter:fullscreen:enter', this.onToggleVisibility);
        this.listenTo(this.presenterEvents, 'presenter:fullscreen:exit', this.onToggleVisibility);

        // bind scroll handler
        this.$el.on('scroll', _.throttle(this.onThumbnailScroll.bind(this), 500));

        this.thumbnailLoadDef = createAbortableDeferred($.noop);
        this.thumbnailImages = [];
    },

    render() {
        var self = this;
        this.$el.busy();

        function beginConvertSuccess(convertData) {
            // create thumbnail nodes
            _.times(convertData.pageCount, function (pageIndex) {
                self.$el.append(self.createThumbnailNode(pageIndex + 1));
            });
            // load initial visible thumbnails
            self.loadThumbnails(convertData);
            self.selectThumbnail(0); // DOCS-5068
            return convertData;
        }
        function beginConvertError(response) {
            return $.Deferred().reject(response);
        }
        function beginConvertFinished() {
            self.$el.idle();
        }

        this.app.mainView.slideView.$el.addClass('thumbnails-opened');

        this.thumbnailLoadDef = DocConverterUtils.beginConvert(this.model)
            .done(beginConvertSuccess)
            .fail(beginConvertError)
            .always(beginConvertFinished);

        return this;
    },

    /**
     * Creates a complete thumbnail node.
     *
     * @param {Number} pageNumber
     *  the one-based page number that should be shown in the thumbnail.
     *
     * @returns {HTMLAnchorElement} thumbnailLink
     */
    createThumbnailNode(pageNumber) {

        const thumbnailLink = $('<a class="presenter-thumbnail-link" role="button" aria-selected="false">').attr('data-page', pageNumber)[0];

        const thumbnailNode = thumbnailLink.appendChild(createDiv("presenter-thumbnail"));
        const thumbnailImage = thumbnailNode.appendChild(this.createDocumentThumbnailImage('thumbnail-image'));
        this.thumbnailImages.push(thumbnailImage);

        thumbnailLink.appendChild(createDiv({ classes: "page-number", label: _.noI18n(String(pageNumber)) }));

        return thumbnailLink;
    },

    /**
     * Loads thumbnail images, which are visible in the browser window.
     *
     * @param {Object} convertData
     *  a response object from document converter containing
     *  the convert jobID and the total page count.
     */
    loadThumbnails(convertData) {
        var thumbnailsToLoad = getThumbnailsToLoad(this.thumbnailImages);
        var params = {
            action: 'convertdocument',
            convert_action: 'getpage',
            target_format: 'png',
            target_width: 200,
            target_zoom: 1,
            job_id: convertData.jobID,
            page_number: convertData.pageNumber,
            id: this.model.get('id'),
            folder_id: this.model.get('folder_id'),
            filename: this.model.get('filename'),
            version: this.model.get('version')
        };

        _.each(thumbnailsToLoad, function (pageNumber) {
            var image = this.thumbnailImages[pageNumber - 1];
            if (image.src) {
                return;
            }

            $(image.parentNode).busy();

            params.page_number = pageNumber;
            image.src = DocConverterUtils.getConverterUrl(params, { encodeUrl: true });
        }, this);
    },

    /**
     * Creates thumbnail image of a document page.
     *
     * @returns {HTMLImageElement} image
     *  the image HTML element.
     */
    createDocumentThumbnailImage(className) {
        const image = createElement('img', className);
        image.addEventListener('load', () => {
            $(image).parent().idle();
        });
        image.addEventListener('error', () => {
            $(image).parent().idle();
            image.remove();
        });
        return image;
    },

    /**
     * Thumbnail scroll handler:
     * - refresh thumbnails by loading visible ones.
     */
    onThumbnailScroll(/*event*/) {
        this.refreshThumbnails();
    },

    /**
     * Thumbnail click handler:
     * - selects/highlights the clicked thumbnail.
     * - triggers 'presenter:showslide' event.
     *
     * @param {jQueryEvent} event
     */
    onThumbnailClicked(event) {
        var clickedThumbnail = $(event.currentTarget);
        var clickedPageNumber = clickedThumbnail.data('page');

        this.selectThumbnail(clickedPageNumber - 1);
        this.presenterEvents.trigger('presenter:showslide', clickedPageNumber - 1);
    },

    /**
     * Thumbnail keydown handler.
     * - selects a thumbnail with ENTER or SPACE key.
     *
     * @param {jQuery.Event} event
     */
    onThumbnailKeydown(event) {
        switch (event.which || event.keyCode) {
            case 13: // enter
                event.stopPropagation();
                this.onThumbnailClicked(event);
                break;
            case 32: // space
                event.stopPropagation();
                this.onThumbnailClicked(event);
                break;
            // no default
        }
    },

    /**
     * Handles updates to the real-time message data and sets the visibility of this view accordingly.
     */
    onToggleVisibility() {
        var rtModel    = this.app.rtModel;
        var localModel = this.app.localModel;
        var clientUID  = this.app.rtConnection.getRTUuid();
        var fullscreen = this.app.mainView.fullscreen;

        this.toggleVisibility(!fullscreen && rtModel.canShowThumbnails(clientUID) && !localModel.isPresenter(clientUID));
    },

    /**
     * Toggles the visibility of the thumbnail view.
     *
     * @param {Boolean} visibility
     */
    toggleVisibility(visibility) {
        if (typeof visibility !== 'boolean') {
            return;
        }
        var slideView = this.app.mainView.slideView;
        var thumbnailsOpened = slideView.$el.hasClass('thumbnails-opened');

        if (thumbnailsOpened !== visibility) {
            this.$el.toggle(visibility);
            slideView.$el.toggleClass('thumbnails-opened', visibility);
            // Workaround: when joining the presentation calling onResize() on iOS or Android will make Swiper failing to load the initial slide.
            if (_.device('!iOS && !Android')) {
                this.presenterEvents.trigger('presenter:resize');
            }
        }
    },

    /**
     * Selects a thumbnail of a particular page number.
     *
     * @param {Number} pageNumber
     *  page number to be selected, 0-based.
     */
    selectThumbnail(pageNumber) {
        var thumbnail = this.$el.find('.presenter-thumbnail-link[data-page=' + (pageNumber + 1) + ']');

        if ((thumbnail.length > 0) && (!thumbnail.hasClass('selected'))) {
            thumbnail.addClass('selected').attr('aria-selected', true);
            thumbnail.siblings('.selected').removeClass('selected').attr('aria-selected', false);
            // scroll if the selected thumbnail is not wholly visible
            var thumbnailPane = this.$el,
                thumbnailPaneScrollLeft = thumbnailPane.scrollLeft(),
                thumbnailRightEdge = thumbnailPaneScrollLeft + thumbnail.offset().left + thumbnail.width(),
                thumbnailPaneRightEdge = thumbnailPane.scrollLeft() + window.innerWidth,
                thumbnailLeftEdge = thumbnail.offset().left,
                marginOffset = 10;
            if (thumbnailRightEdge > thumbnailPaneRightEdge) {
                thumbnailPane.scrollLeft(thumbnailPaneScrollLeft + thumbnailRightEdge - thumbnailPaneRightEdge + marginOffset);
            }
            if (thumbnailLeftEdge < 0) {
                thumbnailPane.scrollLeft(thumbnailPaneScrollLeft + thumbnailLeftEdge - marginOffset);
            }
        }
        thumbnail.focus();
    },

    /**
     * Refresh thumbnails by loading visible ones.
     */
    refreshThumbnails() {
        this.thumbnailLoadDef.done(function (convertData) {
            if (this.$el.is(':visible')) {
                // load thumbnails only if thumbnailview is visible
                this.loadThumbnails(convertData);
            }
        }.bind(this));
    },

    onDispose() {
        var def = this.thumbnailLoadDef;
        // cancel any pending thumbnail loading
        if (def.state() === 'pending') {
            def.abort();
        }
        // close convert jobs while quitting
        def.done(function (response) {
            DocConverterUtils.endConvert(this.model, response.jobID);
        }.bind(this));
        // unbind image on load handlers
        _.each(this.thumbnailImages, function (image) {
            image.onload = null;
            image.onerror = null;
        });
        this.thumbnailImages = null;
    }
});
