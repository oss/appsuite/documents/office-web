/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { getStringOption } from '@/io.ox/office/tk/utils';
import ConverterUtils from '$/io.ox/core/tk/doc-converter-utils';

// constants ==================================================================

/**
 * Default error messages
 */
var GENERAL_ERROR              = gt('A general error occurred. Please try to reload the document. In case this does not help, please contact your system administrator.');
var GENERAL_NETWORK_ERROR      = gt('A general network error occurred. Please try to reload the document. In case this does not help, please contact your system administrator.');
var GENERAL_FILE_STORAGE_ERROR = gt('A general file storage error occurred. Please try to reload the document. In case this does not help, please contact your system administrator.');

/**
 * Drive error messages
 */
var FILE_STORAGE = {
    'FILE_STORAGE-0008': gt('The requested document does not exist.'),
    'FILE_STORAGE-0017': gt('The presentation cannot be started. Please check the URL or contact the presenter.'),
    'FILE_STORAGE-0057': gt('This document does not have any content.'),
    'FILE_STORAGE-0058': gt('You do not have the appropriate permissions to read the document.')
};
FILE_STORAGE['FILE_STORAGE-0018'] = FILE_STORAGE['FILE_STORAGE-0028'] = FILE_STORAGE['FILE_STORAGE-0047'] = FILE_STORAGE['FILE_STORAGE-0017'];
FILE_STORAGE['FILE_STORAGE-0026'] = FILE_STORAGE['FILE_STORAGE-0055'] = FILE_STORAGE['FILE_STORAGE-0008'];
FILE_STORAGE['FILE_STORAGE-0062'] = FILE_STORAGE['FILE_STORAGE-0058'];

var IFO = {
    'IFO-0300': FILE_STORAGE['FILE_STORAGE-0008'],
    'IFO-0400': FILE_STORAGE['FILE_STORAGE-0058'],
    'IFO-0438': FILE_STORAGE['FILE_STORAGE-0008'],
    'IFO-0500': FILE_STORAGE['FILE_STORAGE-0057']
};

var DRIVE_ERROR_MESSAGES = _.extend({}, FILE_STORAGE, IFO);

/**
 * Realtime error messages
 */
var SVL = {
    'SVL-0002': FILE_STORAGE['FILE_STORAGE-0017'],
    'SVL-0004': FILE_STORAGE['FILE_STORAGE-0017'],
    'SVL-0010': FILE_STORAGE['FILE_STORAGE-0017'],
    'SVL-0015': gt('A general network error occurred. Please contact your system administrator.'),
    'SVL-0030': FILE_STORAGE['FILE_STORAGE-0017']
};
SVL['SVL-0016'] = SVL['SVL-0015'];

var RT_ERROR_CODES = {
    PRESENTER_MAX_PARTICIPANTS_FOR_PRESENTATION_REACHED_ERROR: gt('The maximum number of participants for this presentation has been reached. Joining is not possible.'),
    PRESENTER_PRESENTATION_IS_BEING_EDITED_ERROR: gt('The remote presentation cannot be started, because the document is currently being edited in collaboration. Please try again later.'),
    PRESENTER_REMOTE_PRESENTATION_ALREADY_RUNNING_ERROR: gt('The remote presentation cannot be started, because a presentation is already being presented. Please try again later.'),
    GENERAL_SERVICE_DISABLED_ERROR: gt('A remote presentation cannot be started, because a needed service is not enabled. Please contact your system administrator.')
};

var RT_ERROR_MESSAGES = _.extend({}, SVL, RT_ERROR_CODES);

/**
 * A central class that maps the file storage, realtime and document conversion error codes to user readable error messages.
 */
var ErrorMessages = {

    /**errorCode
     * Creates a user readable error message from error objects provided
     * by Realtime, Drive and the Document Converter.
     * An unknown error code will be translated to a general error message.
     *
     * @param {Object} error
     *  The error object to get a user-interface error message for.
     *
     * @param {Object} [options]
     *  @param {String} options.category
     *      The error category to provide more specific messages for unknown error codes.
     *      Supported categories are 'rt', 'drive' and 'conversion'.
     *
     * @returns {String}
     *  A user readable error message.
     */
    getErrorMessage(error, options) {
        // the error code provided by Drive and Realtime
        var errorCode  = (_.isObject(error) && _.isString(error.error) && error.code) || null;
        // the error cause provided by document conversion
        var errorCause = getStringOption(error, 'cause', null);
        // the error category
        var category   = getStringOption(options, 'category', null);
        // the resulting message
        var message    = DRIVE_ERROR_MESSAGES[errorCode] || RT_ERROR_MESSAGES[errorCode] || ConverterUtils.getErrorText(errorCause);

        if (!message) {
            switch (category) {
                case 'conversion':
                    message = ConverterUtils.getErrorText('importError');
                    break;

                case 'drive':
                    message = GENERAL_FILE_STORAGE_ERROR;
                    break;

                case 'rt':
                    message = GENERAL_NETWORK_ERROR;
                    break;

                default:
                    message = GENERAL_ERROR;
                    break;
            }
        }

        return message;
    },

    /**
     * Creates a user readable error message from the provided Realtime error object.
     * An unknown error code will be translated to a general Realtime error message.
     *
     * @param {Object} error
     *  The error object to get a user-interface error message for.
     */
    getRealtimeErrorMessage(error) {
        return this.getErrorMessage(error, { category: 'rt' });
    },

    /**
     * Creates a user readable error message from the provided Drive error object.
     * An unknown error code will be translated to a general Drive error message.
     *
     * @param {Object} error
     *  The error object to get a user-interface error message for.
     */
    getDriveErrorMessage(error) {
        return this.getErrorMessage(error, { category: 'drive' });
    },

    /**
     * Creates a user readable error message from the provided document conversion error object.
     * An unknown error code will be translated to a document conversion import error message.
     *
     * @param {Object} error
     *  The error object to get a user-interface error message for.
     */
    getConversionErrorMessage(error) {
        return this.getErrorMessage(error, { category: 'conversion' });
    }
};

export default ErrorMessages;
