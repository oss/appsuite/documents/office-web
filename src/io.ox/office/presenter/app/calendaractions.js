/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';
import ox from '$/ox';

import ext from '$/io.ox/core/extensions';
import { Action as createAction } from '$/io.ox/backbone/views/actions/util';
import FilesAPI from '$/io.ox/files/api';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { PRESENTATION_AVAILABLE } from '@/io.ox/office/editframework/utils/editconfig';

import { isNativePresentationFile, isTemplateFile, launchPresenter } from '@/io.ox/office/presenter/utils/presenterutils';
import { PRESENTER_ACTION_ID } from '@/io.ox/office/presenter/actions';

// constants ==================================================================

const RE_PGP_ENCRYPTED     = /application\/pgp/i;
const RE_PGP_ENCRYPTED_EXT = /\.pgp$/i;

// static initialization ======================================================

globalLogger.info('$badge{launch} creating calendar actions for presenter application');

function canPresent(data) {

    // TODO: Temporarely disabled functionality
    // var model = new FilesAPI.Model(data); // the file model representing the pim attachment
    // return ((FilesAPI.isPresentation(data) || FilesAPI.isPDF(data)) && model.isPIMAttachment());

    // Temporary functionality, because open in new tab fails for the presenter and ppt (pps) can only be loaded without pictures into OX Presentation
    // - singleTab, the presenter can present it
    // - no single tab and is native file (pptx oder odp) -> OX Presentation can present it
    // Still open is DOCS-2905: Presenter App can present pdf and binary files (ppt, pps) in new tab (same tab already works fine)
    var model = new FilesAPI.Model(data); // the file model representing the pim attachment
    // if (!TabUtils.BROWSER_TAB_SUPPORT && model.isPIMAttachment() && (FilesAPI.isPresentation(data) || FilesAPI.isPDF(data))) { return true; } // the presenter can handle it, but disabled: No special handling for single tab
    if (PRESENTATION_AVAILABLE && isNativePresentationFile(model) && FilesAPI.isPresentation(data) && !isTemplateFile(model) && !hasPGPFileExtension(data) && model.isPIMAttachment()) { return true; } // OX Presentation can handle it

    return false;
}

function isPGPAttachment(data) {
    return (data.security && data.security.decrypted) || (RE_PGP_ENCRYPTED.test(data.content_type));
}

// checking the file extension to find a pgp file (TODO)
function hasPGPFileExtension(data) {
    return RE_PGP_ENCRYPTED_EXT.test(data.filename);
}

function appendGuardData(data) {
    if ((data.source !== 'guardMail') || _.isEmpty(data.guardUrl)) {
        // Guard URL params
        var params = '&emailid=' + data.parent.id +
            '&attach=' + data.id +
            '&session=' + ox.session +
            '&userid=' + ox.user_id +
            '&cid=' + ox.context_id +
            '&folder=' + data.parent.folder_id +
            '&attname=' + encodeURIComponent(data.filename);

        if (window.oxguarddata && window.oxguarddata.passcode) {
            params += '&auth=' + encodeURIComponent(window.oxguarddata.passcode);
        }
        if (data.epass) {
            params += '&epassword=' + encodeURIComponent(data.epass);
        }

        data.guardUrl  = ox.apiRoot + '/oxguard/pgpmail/' + encodeURIComponent(data.filename) + '?action=getattach' + params;
        data.source    = 'guardMail';
        data.origData  = data;
        data.folder_id = data.parent.folder_id;
    }

    return data;
}

createAction(PRESENTER_ACTION_ID + '/launchpresenter/pim', {
    capabilities: 'presenter',
    collection: 'one',

    matches(baton) {
        return canPresent(baton.first());
    },
    action(baton) {
        // the descriptor of the mail attachment
        var attachment = baton.first();
        // the file model representing the mail attachment
        var model = null;
        if (isPGPAttachment(attachment)) {
            attachment = appendGuardData(attachment);
        }

        model = new FilesAPI.Model(attachment);
        launchPresenter(model, attachment.auth);
    }
});

const link = {
    id: 'launchpresenter_pim',
    index: 190,
    prio: 'hi',
    mobile: 'lo',
    title: /*#. launch the presenter app */ gt('Present'),
    ref: PRESENTER_ACTION_ID + '/launchpresenter/pim'
};

// const iconLink = {
//     ...link,
//     index: 650,
//     icon: 'play-circle'
// };

// define link for the OX Presenter launch action in OX Calendar and OX Task
ext.point('io.ox/core/tk/attachment/links').extend({ ...link });

// define link for the OX Presenter launch action in the OX Viewer toolbar.
// ext.point('io.ox/core/viewer/toolbar/links/pim').extend({ ...iconLink });
// ext.point('io.ox/core/viewer/toolbar/links/guardPim').extend({ ...iconLink });
