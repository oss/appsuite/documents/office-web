/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import ext from '$/io.ox/core/extensions';
import { Action as createAction } from '$/io.ox/backbone/views/actions/util';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { DOCUMENTS_COLLABORA } from '@/io.ox/office/baseframework/utils/baseconfig';
import { PRESENTATION_AVAILABLE } from '@/io.ox/office/editframework/utils/editconfig';

import { isNativePresentationFile, launchPresenter } from '@/io.ox/office/presenter/utils/presenterutils';
import { PRESENTER_ACTION_ID } from '@/io.ox/office/presenter/actions';

// static initialization ======================================================

globalLogger.info('$badge{launch} creating file actions for presenter application');

function canPresent(model) {
    var type = getMimeType(model);
    // 'pdf' always, 'pptx' and 'odp' only, if OX Presentation is not available
    // 'pps' and 'ppt' would result in a new file using OX Presentation. Therefore they shall also be presented with the Presenter app (DOCS-2787).
    var isPresentationType = model.isPresentation(type);
    if (DOCUMENTS_COLLABORA && !model.isPDF(type)) { return false; } // when Collabora is used, the presenter is only used for PDF files (#15)
    return ((isPresentationType && !PRESENTATION_AVAILABLE) || (isPresentationType && !isNativePresentationFile(model)) || model.isPDF(type)) && model.isFile();
}

function getMimeType(model) {
    return model.isEncrypted() ? model.getGuardMimeType() : model.getMimeType();
}

function isConversionError(model) {
    var meta = model.get('meta');
    return meta && meta.document_conversion_error && (meta.document_conversion_error.length > 0);
}

function isCurrentVersion(model) {
    return model.get('current_version') !== false;
}

createAction(PRESENTER_ACTION_ID + '/launchpresenter/file', {
    capabilities: 'presenter',
    collection: 'one && read',

    matches(baton) {
        var model = baton.models[0];
        return !isConversionError(model) && canPresent(model) && isCurrentVersion(model);
    },

    action(baton) {
        launchPresenter(baton.models[0]);
    }
});

var link = {
    id: 'launchpresenter_file',
    index: 450,
    prio: 'hi',
    mobile: 'lo',
    title: /*#. launch the presenter app */ gt('Present'),
    icon: 'bi/play-circle.svg',
    ref: PRESENTER_ACTION_ID + '/launchpresenter/file'
};

// define link for the OX Presenter launch action in the OX Viewer toolbar.
ext.point('io.ox/core/viewer/toolbar/links/drive').extend({ ...link });
ext.point('io.ox/core/viewer/toolbar/links/guardDrive').extend({ ...link });

// define link for the OX Presenter launch action in the OX Drive toolbar.
ext.point('io.ox/files/toolbar/links').extend({ ...link });

ext.point('io.ox/files/listview/contextmenu').extend({
    id: 'launchpresenter_file',
    index: 160,
    ref: PRESENTER_ACTION_ID + '/launchpresenter/file',
    section: '10',
    title: gt('Present')
});
