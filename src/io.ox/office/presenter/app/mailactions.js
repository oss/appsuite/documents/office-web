/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';
import ox from '$/ox';

import ext from '$/io.ox/core/extensions';
import { Action as createAction } from '$/io.ox/backbone/views/actions/util';
import FilesAPI from '$/io.ox/files/api';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { isTemplateFile, launchPresenter } from '@/io.ox/office/presenter/utils/presenterutils';
import { PRESENTER_ACTION_ID } from '@/io.ox/office/presenter/actions';

// constants ==================================================================

const RE_PGP_ENCRYPTED    = /application\/pgp/i;

// static initialization ======================================================

globalLogger.info('$badge{launch} creating mail actions for presenter application');

function canPresent(data) {
    // the file model representing the mail attachment
    var model = new FilesAPI.Model(data);
    return (((FilesAPI.isPresentation(data) && !isTemplateFile(model)) || FilesAPI.isPDF(data)) && model.isMailAttachment());
}

function isPGPAttachment(data) {
    return (data.security && data.security.decrypted) || (RE_PGP_ENCRYPTED.test(data.content_type));
}

function appendGuardData(data) {
    if ((data.source !== 'guardMail') || _.isEmpty(data.guardUrl)) {
        // Guard URL params
        var params = '&emailid=' + data.parent.id +
            '&attach=' + data.id +
            '&session=' + ox.session +
            '&userid=' + ox.user_id +
            '&cid=' + ox.context_id +
            '&folder=' + data.parent.folder_id +
            '&attname=' + encodeURIComponent(data.filename);

        if (window.oxguarddata && window.oxguarddata.passcode) {
            params += '&auth=' + encodeURIComponent(window.oxguarddata.passcode);
        }
        if (data.epass) {
            params += '&epassword=' + encodeURIComponent(data.epass);
        }

        data.guardUrl  = ox.apiRoot + '/oxguard/pgpmail/' + encodeURIComponent(data.filename) + '?action=getattach' + params;
        data.source    = 'guardMail';
        data.origData  = data;
        data.folder_id = data.parent.folder_id;
    }

    return data;
}

createAction(PRESENTER_ACTION_ID + '/launchpresenter/mail', {
    capabilities: 'presenter',
    collection: 'one',

    matches(baton) {
        return canPresent(baton.first());
    },
    action(baton) {
        // the descriptor of the mail attachment
        var attachment = baton.first();
        // the file model representing the mail attachment
        var model = null;
        if (isPGPAttachment(attachment)) {
            attachment = appendGuardData(attachment);
        }

        model = new FilesAPI.Model(attachment);
        launchPresenter(model, attachment.auth);
    }
});

const link = {
    id: 'launchpresenter_mail',
    index: 200,
    prio: 'hi',
    mobile: 'lo',
    title: /*#. launch the presenter app */ gt('Present'),
    ref: PRESENTER_ACTION_ID + '/launchpresenter/mail'
};

const iconLink = {
    ...link,
    index: 650,
    icon: 'bi/play-circle.svg'
};

// define link for the OX Presenter launch action in OX Mail.
ext.point('io.ox/mail/attachment/links').extend({ ...link });

// define link for the OX Presenter launch action in the OX Viewer toolbar.
ext.point('io.ox/core/viewer/toolbar/links/mail').extend({ ...iconLink });
ext.point('io.ox/core/viewer/toolbar/links/guardMail').extend({ ...iconLink });
