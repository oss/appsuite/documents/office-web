/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';
import { locationHash } from '$/url';

import FilesAPI from '$/io.ox/files/api';
import MailAPI from '$/io.ox/mail/api';
import { getAttachments } from '$/io.ox/mail/util';
import PageController from '$/io.ox/core/page-controller';
import SessionRestore from '$/io.ox/core/tk/sessionrestore';

import { CLOSE_LABEL } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getObjectOption } from '@/io.ox/office/tk/utils';
import { BROWSER_TAB_SUPPORT } from '@/io.ox/office/tk/utils/tabutils';

import { AppType, getEditorModulePath, setAppFavIcon } from '@/io.ox/office/baseframework/utils/apputils';
import { AbstractAppFactory } from '@/io.ox/office/baseframework/appregistry';

import { RTConnection } from '@/io.ox/office/presenter/rtconnection';
import { RTModel } from '@/io.ox/office/presenter/rtmodel';
import { LocalModel } from '@/io.ox/office/presenter/localmodel';
import { MainView } from '@/io.ox/office/presenter/views/mainview';
import * as Notification from '@/io.ox/office/presenter/views/notification';

import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';

import '@/io.ox/office/presenter/style.less';

// private functions ==========================================================

// Get crypto information from url and add to model
function addCryptoInfo(model) {
    if (locationHash('auth_code') && locationHash('crypto_action') === 'Decrypt') {
        var params = {
            cryptoAction: 'Decrypt',
            cryptoAuth: locationHash('auth_code')
        };
        model.set('file_options', { params });
    }
}

// constants ==================================================================

const MODULE_NAME = getEditorModulePath(AppType.PRESENTER);

// global initialization ======================================================

ox.ui.App.mediator(MODULE_NAME, {

    'pages-desktop'(app) {

        // add page controller
        app.pages = new PageController(app);

        app.pages.addPage({
            name: 'slideView',
            container: app.getWindow().nodes.main
        });

        app.pages.setCurrentPage('slideView');
    },

    'get-file-descriptor'(app) {
        // needed by rt2 connection
        app.getFileDescriptor = function () {
            return (app.fileModel ? app.fileModel.toJSON() : null);
        };
    },

    'get-document-type'(app) {
        // needed by rt2 connection
        app.appType = 'presenter';
    },

    'show-error-notification'(app) {
        app.showErrorNotification = function (error, options) {
            var page = app.pages.getPage('slideView');
            var notificationNode = Notification.createErrorNode(error, options);
            var closeButton = $('<button type="button" class="btn btn-primary">')
                .one('click', function () { app.quit(); })
                .text(CLOSE_LABEL)
                .attr('title', gt('Close Presenter'));

            notificationNode.append(closeButton);
            page.empty().append(notificationNode);
            closeButton.focus();
        };
    },

    'connect-realtime'(app) {
        app.connectRealtime = function () {

            var def = null;

            // RT connect success handler
            function rtConnectSuccess() {
                app.rtConnection.open();
            }

            // RT connect error handler
            function rtConnectError(response) {
                globalLogger.warn('RT Connect Error', response);
                app.showErrorNotification(response, { category: 'rt' });
            }

            // RT connection update event handler
            function rtUpdateHandler(data) {
                app.rtModel.set(app.rtModel.parse(data));
            }

            // RT online handler
            function rtOnlineHandler() {
                if (app.offlineHandlerTriggered) {
                    app.offlineHandlerTriggered = false;
                    Notification.notifyRealtimeOnline();
                }
            }

            // RT offline handler
            function rtOfflineHandler() {
                app.offlineHandlerTriggered = true;
                Notification.notifyRealtimeOffline();
            }

            // RT error handler
            function rtErrorHandler(data) {
                var errorCode = (_.isObject(data) && _.isFunction(data.getCodeAsStringConstant)) ?
                    data.getCodeAsStringConstant() : "error";
                Notification.notifyRealtimeError(errorCode);
            }

            // init RT connection
            app.rtModel      = new RTModel();
            app.rtConnection = new RTConnection(app);

            if (app.rtConnection.isInitialized()) {
                def = app.rtConnection.connect().then(rtConnectSuccess, rtConnectError);
                app.rtConnection.on(RT2Protocol.BROADCAST_UPDATE, rtUpdateHandler);
                app.rtConnection.on(RT2Protocol.BROADCAST_UPDATE_CLIENTS, rtUpdateHandler);
                app.rtConnection.on(RT2Protocol.RESPONSE_OPEN_DOC, rtUpdateHandler);
                app.rtConnection.on("online", rtOnlineHandler);
                app.rtConnection.on("offline", rtOfflineHandler);
                app.rtConnection.on("error", rtErrorHandler);
            } else {
                app.showErrorNotification({ error: '', code: 'SVL-0015' }, { category: 'rt' });
                def = $.Deferred().reject();
            }

            return def;
        };
    },

    'start-presentation'(app) {

        app.startPresenter = function (file) {

            var title = file.get('filename') || file.get('title');

            app.setTitle(title);

            app.fileModel = file;
            app.offlineHandlerTriggered = false;

            // init local model
            app.localModel = new LocalModel();

            // init and connect RT
            app.connectRealtime().then(function () {
                var page      = app.pages.getPage('slideView');
                var lastState = SessionRestore.state('presenter~' + app.fileModel.get('id'));

                app.mainView = new MainView({ model: app.fileModel, app });
                page.append(app.mainView.render().$el);

                // restore state before the browser reload
                if (lastState && lastState.isPresenter) {
                    app.rtConnection.startPresentation({ activeSlide: lastState.slideId || 0 });
                }
                // join a runnig presentation if Presenter was started from a deep link
                if (app.deepLink && app.rtModel.canJoin()) {
                    Notification.notifyPresentationJoin(app.rtModel, app.rtConnection);
                    app.mainView.joinPresentation();
                }
            });
        };
    },

    'dispose-rt-connection'(app) {
        // dispose RT connection instance
        app.disposeRTConnection = function () {
            if (app.rtConnection) {
                if (app.rtConnection.isInitialized()) {
                    var rtConnection = app.rtConnection;
                    rtConnection.off();
                    rtConnection.closeDocument().always(function () {
                        rtConnection.disconnect();
                        app.rtConnection = null;
                    });
                } else if (app.rtConnection) {
                    app.rtConnection.off();
                    app.rtConnection = null;
                }
            }
        };
    },

    'on-app-window-show'(app) {
        app.getWindow().on('show', function () {
            var id        = app.fileModel && app.fileModel.get('id');
            var folder_id = app.fileModel && app.fileModel.get('folder_id');

            if (id && folder_id) {
                app.setState({ id, folder: folder_id });
            }
        });
    }
});

// class PresenterAppFactory ==================================================

export default class PresenterAppFactory extends AbstractAppFactory {

    constructor() {
        super({ moduleName: MODULE_NAME });
    }

    /*protected*/ implLoadAppClass() {
        return Promise.resolve();
    }

    // multi instance pattern
    /*protected*/ implCreateApp() {

        // application object
        var app = ox.ui.createApp({
            closable: true,
            name: MODULE_NAME,
            title: '',
            // no need to load anything, but callback must exist
            load() { return Promise.resolve(true); }
        });

        // update app favicon for tabbed mode (e.g. open a text document in spreadsheet portal, close Doc and go back to portal)
        if (BROWSER_TAB_SUPPORT) {
            setAppFavIcon('presenter');
        }

        function beforeUnloadHandler() {
            var id    = 'presenter~' + app.fileModel.get('id');
            var state = SessionRestore.state(id);

            if (app.rtConnection.isInitialized() && state) {
                state.slideId = app.mainView.getActiveSlideIndex();
                SessionRestore.state(id, state);
            }

            app.rtConnection.emergencyLeave();
            app.disposeRTConnection();
        }

        function quitHandler() {
            ox.off('beforeunload', beforeUnloadHandler);

            if (app.rtConnection.isInitialized()) {
                SessionRestore.state('presenter~' + app.fileModel.get('id'), null);
            }
            app.disposeRTConnection();

            // clear file authentication from file model
            if (app.fileModel.has('file_options')) {
                app.fileModel.set('file_options', {});
            }
        }

        function getModelFromFileDescriptor(file) {
            function showFileError(error) {
                globalLogger.warn('File Error', error);
                app.showErrorNotification(error, { category: 'drive' });
            }
            var modelPromise;
            switch (file.source) {
                case 'mail':
                    modelPromise = MailAPI.get({ id: file.mail, folder_id: file.folder_id }).then(function (mail) {
                        var attachments = getAttachments(mail);
                        var descriptor = _.find(attachments, function (attachment) {
                            return attachment.id === file.id;
                        });
                        var model = new FilesAPI.Model(descriptor);
                        return model;
                    });
                    break;
                case 'drive':
                default:
                    modelPromise = FilesAPI.get(file).then(function (data) {
                        return FilesAPI.pool.get('detail').get(_.cid(data));
                    }, showFileError);
                    break;
            }
            return modelPromise;
        }

        // launcher
        return app.setLauncher(function (options) {

            var state = null;
            var model = getObjectOption(options, 'model', null);

            var win = ox.ui.createWindow({
                chromeless: true,
                name: app.getName(),
                toolbar: false
            });

            app.setRootAttribute = function (name, value) {
                win.nodes.outer.attr(name, value);
            };

            app.setWindow(win);
            app.mediate();

            ox.on('beforeunload', beforeUnloadHandler);
            app.on('quit', quitHandler);

            win.show();

            if (model instanceof FilesAPI.Model) {
                // launched with a file, mail attachment or PIM attachment
                app.deepLink = false;

                addCryptoInfo(model);

                if (model.isFile()) {
                    app.setState({ folder: model.get('folder_id'), id: model.get('id') });
                }

                app.startPresenter(model);

            } else {
                // launched with a deep-link
                app.deepLink = true;
                state = app.getState();

                // get file model and start
                getModelFromFileDescriptor(state).then(function (model) {
                    addCryptoInfo(model);
                    app.startPresenter(model);
                });
            }
        });
    }
}
