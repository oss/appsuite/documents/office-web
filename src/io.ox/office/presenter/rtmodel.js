/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import * as Utils from '@/io.ox/office/tk/utils';

import _ from '$/underscore';
import Backbone from '$/backbone';

// class RTModel ==============================================================

/**
 * Represents the data of an update message send by the real-time framework.
 *
 * @param {Object} [attributes]
 *  Initial values of the attributes, which will be set on the RTModel when
 *  creating an instance.
 *
 *  @param {String} attributes.presenterId
 *   The user id of the presenter, or an empty string.
 *  @param {String} attributes.presenterName
 *   The display name of the presenter, or an empty string.
 *  @param {Number} attributes.activeSlide
 *   The index of the active slide.
 *  @param {Boolean} attributes.paused
 *   Whether the presenter paused the presentation.
 *  @param {Array} attributes.activeUsers
 *   An array of objects representing a real-time user.
 *
 *   @param {String} attributes.activeUsers[].clientUID
 *    The unique client id - generate by the RT connection.
 *   @param {String} attributes.activeUsers[].userDisplayName
 *    The display name of the user.
 *   @param {Boolean} attributes.activeUsers[].active
 *    Whether the RT connection considers this user as active.
 *   @param {Number} attributes.activeUsers[].id
 *    The user id associated with the OX AppSuite account.
 *   @param {Number} attributes.activeUsers[].durationOfInactivity
 *    The amount of seconds that a user has been inactive.
 *   @param {Boolean} attributes.activeUsers[].joined
 *    Whether the user joined the presentation.
 */
export const RTModel = Backbone.Model.extend({

    defaults() {
        return {
            // rtConnection attributes
            presenterId: '',
            presenterName: '',
            activeUsers: {},
            activeSlide: 0,
            paused: false,
            // client generated attributes
            participants: {}
        };
    },

    /**
     * Parses the data of an update message sent by the real-time framework,
     * filters the users list for the ones who joined the presentation and for the presenter.
     *
     * @param {Object} data
     *  the real-time message data.
     *
     * @returns {Object}
     *  the real-time message data supplemented by the participants list.
     */
    parse(data) {
        var docStatus     = Utils.getObjectOption(data, 'docStatus', null);
        var clientsStatus = Utils.getObjectOption(data, 'clients', null);
        var result        = (docStatus) ? _.copy(docStatus, true) : this.toJSON();

        if (clientsStatus) {
            // update of clients
            if (_.isArray(clientsStatus.list)) {
                // complete list of clients
                result.activeUsers = {};
                clientsStatus.list.forEach(function (clientData) {
                    result.activeUsers[clientData.clientUID] = clientData;
                });
            } else {
                // In all other cases we have changes of our previous state
                if (_.isArray(clientsStatus.left)) {
                    // remove clients left
                    clientsStatus.left.forEach(function (clientUID) {
                        delete result.activeUsers[clientUID];
                    });
                }

                if (_.isArray(clientsStatus.joined)) {
                    // add clients joined
                    clientsStatus.joined.forEach(function (clientData) {
                        result.activeUsers[clientData.clientUID] = clientData;
                    });
                }

                if (_.isArray(clientsStatus.changed)) {
                    // modify existing clients data
                    clientsStatus.changed.forEach(function (clientData) {
                        result.activeUsers[clientData.clientUID] = clientData;
                    });
                }
            }

            // filter out participants from the active users
            result.participants = {};
            Object.keys(result.activeUsers).forEach(function (clientUID) {
                var clientData = result.activeUsers[clientUID];
                if ((clientData.joined && (clientData.active || (!clientData.active && clientData.durationOfInactivity < 30))) || (clientData.clientUID === result.presenterId)) {
                    result.participants[clientUID] = clientData;
                }
            }, this);
        }

        return result;
    },

    initialize() {
        /*
        this.on('change', function (model) {
            console.log('Presenter - RTModel - change', model);
        });
        */
    },

    /**
     * Returns true if a presenter is set,
     * which means the presentation is started.
     *
     * @returns {Boolean}
     *  Whether a presenter is set.
     */
    hasPresenter() {
        return !_.isEmpty(this.get('presenterId'));
    },

    /**
     * Returns true if the passed user id belongs to the presenter.
     *
     * @param {String} clientUID
     *  The user id to check.
     *
     * @returns {Boolean}
     *  Whether the user is the presenter.
     */
    isPresenter(clientUID) {
        return (clientUID === this.get('presenterId'));
    },

    /**
     * Returns true if the passed user id belongs to the former presenter.
     * Which means the given user started and ended the presentation again. And the presentation has not yet been started.
     * - Intended to be called after a Backbone model change event (otherwise the result may not be valid).
     *
     * @param {String} clientUID
     *  The user id to check.
     *
     * @returns {Boolean}
     *  Whether the user was the presenter.
     */
    wasPresenter(clientUID) {
        var formerPresenter = this.previous('presenterId');
        return (!this.hasPresenter() && !_.isEmpty(formerPresenter) && clientUID === formerPresenter);
    },

    /**
     * Returns true if the passed user id belongs to the presenter and the presentation is running.
     *
     * @param {String} clientUID
     *  The user id to check.
     *
     * @returns {Boolean}
     *  Whether the user is currently presenting.
     */
    isPresenting(clientUID) {
        return (!this.isPaused() && this.isPresenter(clientUID));
    },

    /**
     * Returns true if the provided user can start the presentation.
     * Which means the presentation must not be running and the user must not be joined.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canStart(clientUID) {
        var presenterId = this.get('presenterId');
        return (_.isString(presenterId) && _.isEmpty(presenterId) && !this.isJoined(clientUID));
    },

    /**
     * Returns true if the presentation is paused.
     *
     * @returns {Boolean}
     *  Whether the presentation is paused.
     */
    isPaused() {
        return this.get('paused');
    },

    /**
     * Returns true if the provided user can pause the presentation.
     * Which means the user must be the presenter and the presentation must be running.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canPause(clientUID) {
        return (!this.isPaused() && this.isPresenter(clientUID));
    },

    /**
     * Returns true if the provided user can continue the presentation.
     * Which means the user must be the presenter and the presentation must be paused.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canContinue(clientUID) {
        return (this.isPaused() && this.isPresenter(clientUID));
    },

    /**
     * Returns the real-time user object for the provided user id.
     *
     * @param {String} clientUID
     *  The user id to look for.
     */
    getUser(clientUID) {
        return this.get('activeUsers')[clientUID];
    },

    /**
     * Returns true if the provided user has joined the presentation as participant.
     *
     * @param {String} clientUID
     *  The user id to look for.
     *
     * @returns {Boolean}
     *  Whether the user has joined the presentation.
     */
    isJoined(clientUID) {
        var user = this.getUser(clientUID);
        return (user && user.joined);
    },

    /**
     * Returns true if the provided user can join the presentation.
     * Which means the presentation must be running and the user must not be joined.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canJoin(clientUID) {
        return (this.hasPresenter() && !this.isJoined(clientUID));
    },

    /**
     * Returns true if the provided user can leave the presentation.
     * Which means the user must be joined and must not be the presenter.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canLeave(clientUID) {
        return (this.isJoined(clientUID) && !this.isPresenter(clientUID));
    },

    /**
     * Returns true if the thumb-nail view can be displayed for the provided user.
     * Which means the user must not be joined, or must be the presenter with the presentation paused.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canShowThumbnails(clientUID) {
        return (!this.isJoined(clientUID) || (this.isPresenter(clientUID) && this.isPaused()));
    },

    /**
     * Returns true if the pause overlay can be displayed for the provided user.
     * Which means the user must be joined and the presentation must be paused.
     *
     * @param {String} clientUID
     *  The user id to check.
     */
    canShowPauseOverlay(clientUID) {
        return (this.isPaused() && !this.isPresenter(clientUID) && this.isJoined(clientUID));
    }
});
