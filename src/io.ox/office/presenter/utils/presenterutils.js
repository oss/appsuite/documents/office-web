/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';

import Capabilities from '$/io.ox/core/capabilities';
import FilesAPI from '$/io.ox/files/api';

import { BROWSER_TAB_SUPPORT, openEditorChildTab, openPresenterChildTab } from '@/io.ox/office/tk/utils/tabutils';
import { authorizeGuard, authorizeGuardWithTab, isDriveFile } from '@/io.ox/office/tk/utils/driveutils';
import { AppType, getEditorModulePath } from '@/io.ox/office/baseframework/utils/apputils';
import { isNative, isTemplate } from '@/io.ox/office/baseframework/app/extensionregistry';
import { launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';

// constants ==================================================================

// the module name of the presentation edit application
const PRESENTATION_MODULE_NAME = getEditorModulePath(AppType.PRESENTATION);

// private functions ==========================================================

function getFileDescriptionForMailAttachment(model) {

    var fileDesc = {};
    var origData = model.get('origData');
    fileDesc.source = 'mail';
    fileDesc.id = origData.mail.id;
    fileDesc.folder_id = origData.mail.folder_id;
    fileDesc.version = origData.id;
    fileDesc.file_mimetype = model.get('file_mimetype');
    fileDesc.filename = model.getDisplayName();
    // cryptoAuth = origData.auth || (origData.security && origData.security.authentication) || null;

    return fileDesc;
}

function getFileDescriptionForPIMAttachment(model) {

    var fileDesc = {};
    var origData = model.get('origData');
    if (origData.module === 1) {
        fileDesc.source = 'calendar';
    } else if (origData.module === 4) {
        fileDesc.source = 'tasks';
    } else if (origData.module === 7) {
        fileDesc.source = 'contacts';
    }

    fileDesc.id = origData.attached;
    fileDesc.folder_id = origData.folder;
    fileDesc.version = origData.managedId || origData.id;
    fileDesc.file_mimetype = model.get('file_mimetype');
    fileDesc.filename = model.getDisplayName();

    return fileDesc;
}

// launch OX Presenter (pdf) or OX Presentation in presentation mode (pptx and odp)
function launch(model) {

    var isAttachmentFile = false;

    if (usePresentationApp(model)) {

        var fileDesc = null;

        if (model.isMailAttachment()) {
            isAttachmentFile = true;
            fileDesc = getFileDescriptionForMailAttachment(model);
        } else if (model.isPIMAttachment()) {
            isAttachmentFile = true;
            fileDesc = getFileDescriptionForPIMAttachment(model);
        } else {
            fileDesc = model.toJSON();
        }

        var launchOptions = { action: 'load', file: fileDesc, presentationMode: true, isAttachmentFile };
        var fileOptions = model.get('file_options');
        if (fileOptions && fileOptions.params && fileOptions.params.cryptoAuth) { launchOptions.auth_code = fileOptions.params.cryptoAuth; }

        return launchDocsApp(AppType.PRESENTATION, launchOptions);
    }

    return launchDocsApp(AppType.PRESENTER, { model });
}

// whether OX Presentation can be used instead of the presenter
function usePresentationApp(model) {
    var type = model.isEncrypted() ? model.getGuardMimeType() : model.getMimeType();
    return Capabilities.has('presentation') && model.isPresentation(type) && isNativePresentationFile(model) && (isModelOfDriveFile(model) || model.isMailAttachment() || model.isPIMAttachment());
}

// whether the specified model belongs to a file located in drive
function isModelOfDriveFile(model) {
    return isDriveFile(model.toJSON());
}

// authorize with Guard
function authorize(model) {
    // set authorization data to the model
    var file = {
        data: model.toJSON()
    };
    return authorizeGuard(file).then(function (auth_code) {
        var params = {
            cryptoAction: 'Decrypt',
            cryptoAuth: auth_code
        };
        model.set('file_options', { params });
        return model;
    });
}

// helper function to add required parameters to open the document in OX Presentation
// without creating a copy in drive.
function setAttachmentParametersForPresentationApp(model, parametersToOpenTab) {
    var fileDesc = model.isMailAttachment() ? getFileDescriptionForMailAttachment(model) : getFileDescriptionForPIMAttachment(model);
    parametersToOpenTab.isAttachmentFile = true;
    parametersToOpenTab.source = fileDesc.source;
    parametersToOpenTab.version = fileDesc.version;
    parametersToOpenTab.id = fileDesc.id;
}

// public functions ===========================================================

/**
 * Launches OX Presenter or OX Presentation in presentation mode.
 *
 * @param {Object} model
 *  The file model object.
 *
 * @param {String} auth_code
 *  The auth_code for a encrypted presentation, if the presentation is encrypted and the
 *  auth_code is null/undefined the Authorizer dialog will be shown.
 *
 * @returns {jQuery.Promise}
 *  A promise that will be resolved when the application is launched
 *  or rejected in case of an error.
 */
export function launchPresenter(model, auth_code) {

    var tryToOpenInTab = ox.ui.App.getCurrentApp().getName() !== 'io.ox/office/portal/presentation';
    var parametersToOpenTab;

    if (tryToOpenInTab && BROWSER_TAB_SUPPORT) {

        parametersToOpenTab = {
            id: model.get('id'),
            folder_id: model.get('folder_id'),
            source: model.get('source')
        };
        if (model.attributes.origData && model.attributes.origData.mail) {
            _.extend(parametersToOpenTab, { mail: model.attributes.origData.mail.id });
        }
    }

    if (isEncrypted(model) || auth_code) {
        if (_.isString(auth_code)) {
            var params = {
                cryptoAuth: auth_code
            };
            if (parametersToOpenTab) {
                if (usePresentationApp(model)) {
                    parametersToOpenTab.presentationMode = true;
                    if (model.isMailAttachment() || model.isPIMAttachment()) { setAttachmentParametersForPresentationApp(model, parametersToOpenTab); }
                    openEditorChildTab('presentation', _.extend(parametersToOpenTab, { auth_code, crypto_action: 'Decrypt' }));
                } else {
                    openPresenterChildTab(_.extend(parametersToOpenTab, { auth_code, crypto_action: 'Decrypt' }));
                }
                return $.Deferred().resolve();
            }
            model.set('file_options', { params });
            return launch(model);
        }

        if (parametersToOpenTab) {
            return authorizeGuardWithTab(model).then(function (result) {
                openPresenterChildTab({ id: model.get('id'), auth_code: result.authCode, crypto_action: 'Decrypt' }, result.targetTab);
            });
        }
        // make a copy since the Viewer clears the file authentication
        // from file models when closing
        model = new FilesAPI.Model(model.toJSON());

        return authorize(model).then(function () {
            return launch(model);
        });
    } else {
        if (parametersToOpenTab) {
            if (usePresentationApp(model)) {
                parametersToOpenTab.presentationMode = true;
                if (model.isMailAttachment() || model.isPIMAttachment()) { setAttachmentParametersForPresentationApp(model, parametersToOpenTab); }
                openEditorChildTab('presentation', parametersToOpenTab);
            } else {
                openPresenterChildTab(parametersToOpenTab);
            }
            return $.Deferred().resolve();
        }
        return launch(model);
    }
}

/**
 * Returns true if the file data is OX Guard encrypted.
 *
 * @param {FilesAPI.Model} model
 *  The file model.
 *
 * @returns {Boolean}
 *  Whether the file data is encrypted.
 */
export function isEncrypted(model) {
    if (!model) { return false; }

    var result = false;

    if (model.isFile()) {
        result = model.isEncrypted();

    } else if (model.isMailAttachment()) {
        result = /application\/pgp/.test(model.getMimeType());
    }

    return result;
}

/**
 * Returns whether the file is native for OX Presentation (odp and pptx) and
 * can therefore be presented in OX Presentation without creating a temporary
 * file (like for ppt or pps).
 *
 * @param {FilesAPI.Model} model
 *  The file model.
 *
 * @returns {Boolean}
 *  Whether the file is native for OX Presentation.
 */
export function isNativePresentationFile(model) {
    var fileName = model.get('filename');
    return isNative(fileName, PRESENTATION_MODULE_NAME);
}

/**
 * Returns whether the file is a template OX Presentation (potx, ...).
 * Templates will not be presented in OX Presentation.
 *
 * @param {FilesAPI.Model} model
 *  The file model.
 *
 * @returns {Boolean}
 *  Whether the file is native for OX Presentation.
 */
export function isTemplateFile(model) {
    var fileName = model.get('filename');
    return isTemplate(fileName, PRESENTATION_MODULE_NAME);
}
