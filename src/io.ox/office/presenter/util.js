/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck
import $ from '$/jquery';
import _ from '$/underscore';

/**
 * Detect visible nodes from given nodes array.
 *
 * @returns {Array} visibleNodes
 *  an array of indices of visible nodes.
 */
export function getVisibleNodes(nodes) {
    var visibleNodes = [];

    // return the visible pages
    _.each(nodes, function (element, index) {
        if (!isVisibleNode(element)) { return; }
        visibleNodes.push(index + 1);
    });
    return visibleNodes;
}

/**
 * Detect visible thumbnail image nodes
 * that have not yet been loaded from given nodes array.
 *
 * @param {Array} nodes
 *  an array of image nodes.
 *
 * @returns {Array}
 *  an array of indices of visible image nodes.
 */
export function getThumbnailsToLoad(nodes) {
    var visibleNodes = [];

    // return the visible pages
    _.each(nodes, function (image, index) {
        if (image.src) {
            return;
        }
        if (!isVisibleNode(image)) {
            return;
        }
        visibleNodes.push(index + 1);
    });

    return visibleNodes;
}

/**
 * Returns whether the node is visible in the viewport, wholly or partially.
 *
 * @param {DOM} node
 *  The DOM node to check.
 *
 * @returns {Boolean}
 *  Whether the node the is visible.
 */
export function isVisibleNode(node) {

    function isVerticalInWindow(verticalPosition) {
        return verticalPosition >= 0 && verticalPosition <= window.innerHeight;
    }

    function overlapsVertical(nodeBoundingRect) {
        return nodeBoundingRect.top < 0 && nodeBoundingRect.bottom > window.innerHeight;
    }

    function isHorizontalInWindow(horizontalPosition) {
        return horizontalPosition >= 0 && horizontalPosition <= window.innerWidth;
    }

    function overlapsHorizontal(nodeBoundingRect) {
        return nodeBoundingRect.left < 0 && nodeBoundingRect.right > window.innerWidth;
    }

    var nodeBoundingRect = node.getBoundingClientRect();

    return (isHorizontalInWindow(nodeBoundingRect.left) || isHorizontalInWindow(nodeBoundingRect.right) || overlapsHorizontal(nodeBoundingRect)) &&
        (isVerticalInWindow(nodeBoundingRect.top) || isVerticalInWindow(nodeBoundingRect.bottom) || overlapsVertical(nodeBoundingRect));
}

export function createAbortableDeferred(abortFunction) {
    return _.extend($.Deferred(), {
        abort: abortFunction
    });
}
