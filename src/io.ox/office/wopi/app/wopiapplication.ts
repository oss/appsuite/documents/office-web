/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from "$/ox";

import { is, to, dict, json, pick, uuid, jpromise } from "@/io.ox/office/tk/algorithms";
import { setElementDataSet, insertHiddenNodes } from "@/io.ox/office/tk/dom";
import { getDebugFlag } from "@/io.ox/office/tk/config";
import { getFileBaseName, getFileExtension, getSanitizedFileName, isDriveFile, propagateChangeFile } from "@/io.ox/office/tk/utils/driveutils";
import { FILTER_MODULE_NAME, sendRequest } from "@/io.ox/office/tk/utils/io";
import { BROWSER_TAB_SUPPORT } from "@/io.ox/office/tk/utils/tabutils";

import type { FileDescriptor, CoreAppEventMap, CoreApp } from "@/io.ox/office/baseframework/app/appfactory";
import { launchPortalOnQuit } from "@/io.ox/office/baseframework/app/appfactory";
import { AbstractApplication } from "@/io.ox/office/baseframework/app/abstractapplication";

import { AccessTokenError } from "@/io.ox/office/wopi/utils/accesstokenerror";
import { showLaunchErrorAlert } from "@/io.ox/office/wopi/utils/erroralert";

import "@/io.ox/office/wopi/app/wopiapplication.less";

// types ======================================================================

/**
 * Type mapping for the events emitted by `WopiApplication` instances.
 */
export interface WopiApplicationEventMap extends CoreAppEventMap { }

/**
 * Configuration data needed to launch a WOPI editor for a specific file.
 */
interface WopiEditorConfig {
    /** The WOPI editor URL to be set to an iframe element. */
    editorUrl: string;
    /** The access token for the document file to be edited. */
    accessToken: string;
    /** The life time of the access token. */
    accessTokenTtl: number;
}

// constants ==================================================================

/**
 * The CSS marker class for the iframe container element.
 */
const WOPI_FRAME_CONTAINER = "io-ox-office-wopi-frame-container";

// functions ==================================================================

/**
 * Create a new `<form>` element prepared to send a POST request, and inserts
 * child `<input>` elements for all passed parameters to be submitted in the
 * request body.
 *
 * @param url
 *  The target URL to submit the POST request to.
 *
 * @param params
 *  Additional parameters to be inserted into body of the POST request.
 *
 * @returns
 *  The new `<form>` element.
 */
function createFormElement(url: string, params: Dict<string | number | boolean>): HTMLFormElement {

    // create the form element and prepare it for sending "post" requests
    const formElement = document.createElement("form");
    formElement.method = "post";
    formElement.action = url;
    formElement.enctype = "multipart/form-data";

    // create hidden input elements used to put the passed parameters into the request
    for (const [name, value] of dict.entries(params)) {
        const inputElement = document.createElement("input");
        inputElement.type = "hidden";
        inputElement.name = name;
        inputElement.value = String(value);
        formElement.append(inputElement);
    }

    // put the form element into the DOM
    return insertHiddenNodes(formElement);
}

// class WopiApplication ======================================================

export default class WopiApplication extends AbstractApplication {

    /** The descriptor of the document file being edited. */
    #fileDesc!: FileDescriptor;
    /** The iframe element containing the remote WOPI editor. */
    #frameElement: Opt<HTMLIFrameElement>;
    /** Whether the document has been loaded in the remote WOPI editor. */
    #isLoaded = false;
    /** Whether the document has been modified in the remote WOPI editor. */
    #isModified = false;

    // constructor ------------------------------------------------------------

    constructor(coreApp: CoreApp) {
        super(coreApp);

        // no async construction: immediately run all initialization handlers
        this.runInitHandlers();

        // launch editor in background task (ignore the promise)
        jpromise.floating(this.#launchApplication());
    }

    // protected methods ------------------------------------------------------

    /**
     * Core implementation for closing this application.
     *
     * @returns
     *  A promise that will fulfil when this application instance has been
     *  closed completely; or reject when the application shall continue
     *  running.
     */
    protected override async implQuit(): Promise<void> {

        // cleanup tasks, if the document has been loaded successfully
        // (quit process will be called too, if loading the document fails early)
        if (this.#isLoaded) {

            // update drive application in background
            jpromise.floating(propagateChangeFile(this.#fileDesc));

            // remove file from recent list if it was newly created and not edited
            if ((this.launchConfig.action === "new") && !this.launchConfig.templateFile && !this.#isModified) {
                this.#deleteFileFromRecentList();
            } else {
                this.#addFileToRecentList();
            }
        }

        // with multi-tab support the user should see the portal
        // (normal behavior: last open or default app will become visible)
        if (BROWSER_TAB_SUPPORT) {
            await launchPortalOnQuit(this.coreApp);
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Resolves the file descriptor for the document file to be opened, and
     * puts it into the application's model attributes.
     *
     * @returns
     *  A promise that will fulfil if the document file is ready to be used.
     */
    async #prepareDocumentFile(): Promise<void> {

        // resolve the file descriptor from launch options
        let fileDesc: Opt<FileDescriptor>;
        switch (this.launchConfig.action) {
            case "load":
                fileDesc = this.launchConfig.file;
                break;
            case "new":
            case "convert":
                // create new file from scratch or from template file
                fileDesc = await this.createNewDocumentFile();
                this.launchTracker.step("fileInitialized", "$badge{WopiApp} new file created");
                break;
        }

        // file descriptor must contain at least a folder ID and a file ID
        if (fileDesc?.folder_id && fileDesc.id) {
            this.#fileDesc = fileDesc;
            this.coreApp.set("docsFileDesc", fileDesc);
        } else {
            throw new Error("missing file descriptor, cannot launch WOPI app");
        }
    }

    /**
     * Fetches and returns the WOPI editor configuration, consisting of the
     * editor URL needed for the iframe, and an access token representing the
     * user session and document file to be edited.
     */
    async #getWopiEditorConfig(): Promise<WopiEditorConfig> {

        // get the file descriptor from the application attributes
        const fileDesc = this.#fileDesc;

        // fetch the WOPI access token and file ID
        const folderId = encodeURIComponent(fileDesc.folder_id);
        const fileId = encodeURIComponent(fileDesc.id);
        const mode = "edit"; // or "view"
        const url = `${ox.root}/wopi/appsuite/accesstoken?session=${ox.session}&folderId=${folderId}&fileId=${fileId}&mode=${mode}&t=${Date.now()}`;
        const headers = new Headers({ "X-Msg-Id": uuid.random() });
        const tokenDataRes = await fetch(url, { headers });
        const tokenData: unknown = await tokenDataRes.json();

        // collect some data from error responses to be shown in the alert banner
        if (!tokenDataRes.ok) {
            throw new AccessTokenError(tokenDataRes.status, tokenData);
        }

        // tokenData contains the following properties:
        //  - accessToken
        //  - accessTokenTtl
        //  - wopiFileId
        //  - wopiClientUrlSrc
        //  - favIconUrl
        //  - wopiClientName

        // create the editor URL with extra query parameters
        let editorUrl = pick.string(tokenData, "wopiClientUrlSrc");
        if (!editorUrl) { throw new Error("missing WOPI editor URL, cannot launch WOPI app"); }

        // start the editor in the current UI language
        editorUrl += "&lang=" + ox.language.replace(/_/g, "-");

        // show an embedded Close button in single-tab mode
        if (!BROWSER_TAB_SUPPORT) { editorUrl += "&closebutton=true"; }

        // enable internal debug mode of the editor
        if (getDebugFlag("office:debug-wopi")) { editorUrl += "&debug=true"; }

        // return the editor configuration (editor URL and access token)
        return {
            editorUrl,
            accessToken: pick.string(tokenData, "accessToken", ""),
            accessTokenTtl: pick.number(tokenData, "accessTokenTtl", 0),
        };
    }

    /**
     * Handler for WOPI postMessage events.
     *
     * @param event
     *  The message event received from the frame element.
     */
    #handleWopiMessage(event: MessageEvent): void {

        // ignore messages that have been sent from another iframe
        if (event.source !== this.#frameElement?.contentWindow) { return; }

        // parse JSON message data (must contain a string property "MessageId")
        const message = json.safeParse(to.string(event.data, ""));
        if (!is.dict(message) || !is.string(message.MessageId)) { return; }

        switch (message.MessageId) {

            // emitted while the document is being loaded into the remote editor
            case "App_LoadingStatus":
                switch (pick.string(message.Values, "Status", "")) {
                    case "Initialized":
                        this.launchTracker.step("WOPI_App_LoadingStatus_Init", "Initialized");
                        this.#frameElement.classList.remove("hidden");
                        break;
                    case "Frame_Ready":
                        this.launchTracker.step("WOPI_App_LoadingStatus_Frame_Ready", "Frame_Ready");
                        break;
                    case "Document_Loaded":
                        this.launchTracker.step("WOPI_App_LoadingStatus_Document_Loaded", "Document_Loaded");
                        this.#addFileToRecentList(); // register file in recent list immediately after document is loaded
                        break;
                }
                break;

            // emitted after loading and when the open document is modified the first time
            case "Doc_ModifiedStatus":
                if (pick.boolean(message.Values, "Modified")) {
                    this.#isModified = true;
                }
                break;

            // close the application when clicking the "Close" button in the WOPI editor
            case "UI_Close":
                jpromise.floating(this.quit());
                break;
        }
    }

    /**
     * Creates an iframe and connects it to the WOPI editor service.
     */
    #createWopiEditorFrame(editorConfig: WopiEditorConfig): void {

        // use "main" element of the application window as the container for the WOPI iframe
        const officeMain = this.coreApp.getWindowNode()[0];
        officeMain.classList.add(WOPI_FRAME_CONTAINER);

        // create a temporary form to send accessToken and WOPI source URL to the WOPI server
        const formElement = createFormElement(editorConfig.editorUrl, {
            access_token: editorConfig.accessToken,
            access_token_ttl: editorConfig.accessTokenTtl,
        });

        // create a globally unique identifier for the iframe
        const frameName = `wopi-office-frame-${uuid.random()}`;
        formElement.target = frameName;

        this.#frameElement = document.createElement("iframe");
        this.#frameElement.name = frameName;
        this.#frameElement.id = frameName;
        this.#frameElement.className = "wopi-office-frame hidden"; // "hidden", so that unformatted topbar is not visible
        this.#frameElement.title = "Office Frame"; // the title should be set for accessibility
        this.#frameElement.setAttribute("allowfullscreen", "true"); // this attribute allows true fullscreen mode in slideshow view when using PowerPoint's "view" action.

        // The sandbox attribute is needed to allow automatic redirection to the O365 sign-in page in the business user flow
        // frameElement.setAttribute("sandbox", "allow-scripts allow-same-origin allow-forms allow-popups allow-top-navigation allow-popups-to-escape-sandbox");

        // insert the iframe into the DOM (important: after assigning the "name" attribute!)
        officeMain.append(this.#frameElement);

        // submit the form, so that the document will be inserted into the iframe
        formElement.submit();

        // remove the form after submit
        formElement.remove();

        // register a message listener at the browser window that filters messages sent from the own iframe
        this.listenTo(window, "message", this.#handleWopiMessage);
    }

    /**
     * Updates the title in the browser window and the application title.
     */
    #updateTitleAndUrl(): void {

        // the file descriptor
        const fileDesc = this.#fileDesc;

        // update window title (browser tab)
        const title = getFileBaseName(getSanitizedFileName(fileDesc));
        this.coreApp.setTitle(title);
        this.coreApp.getWindow().setTitle(title);

        // insert folder ID and file ID into the URL (needed for browser refresh)
        if (isDriveFile(fileDesc) && this.coreApp.getWindow().state.visible) {
            this.coreApp.setState({ folder: fileDesc.folder_id, id: fileDesc.id });
        }

        // add CSS classes and data attributes used in automated tests
        const winRootNode = this.coreApp.getWindow().nodes.outer[0];
        winRootNode.classList.add("io-ox-office-edit-window");
        setElementDataSet(winRootNode, {
            fileFolder: fileDesc.folder_id,
            fileId: fileDesc.id,
            fileName: fileDesc.filename,
            fileVersion: fileDesc.version,
            fileSource: fileDesc.source,
            fileAttachment: fileDesc.attachment,
            fileFormat: getFileExtension(fileDesc.filename),
        });
    }

    /**
     * Prepares the document file, and launches the WOPI remote editor in a
     * dedicated `<iframe>` element.
     */
    async #launchApplication(): Promise<void> {

        try {

            // resolve file descriptor for the document file to be opened
            await this.#prepareDocumentFile();
            // update title of application and browser window
            this.#updateTitleAndUrl();

            // fetch the WOPI access token and editor URL
            const editorConfig = await this.#getWopiEditorConfig();
            // launch the WOPI editor in a dedicated iframe
            this.#createWopiEditorFrame(editorConfig);

            // leave global busy mode
            this.coreApp.getWindow().idle();

            // loading the document has succeeded
            this.#isLoaded = true;

        } catch (error) {
            this.launchTracker.exception(error);

            // show an alert banner
            showLaunchErrorAlert(error);

            // close this application, return to launching app
            jpromise.floating(this.quit());
        }
    }

    /**
     * Add a file to the recent list or shift it to the top position. This is
     * required when a file is opened and when it is closed.
     */
    #addFileToRecentList(): void {
        jpromise.floating(sendRequest(FILTER_MODULE_NAME, {
            action: "addfiletorecentlist",
            type: this.appType,
            id: this.#fileDesc.id,
            session: ox.session,
        }));
    }

    /**
     * Delete a file from the recent list. This is required when a document is
     * closed and the document was a new and unmodified file.
     */
    #deleteFileFromRecentList(): void {
        jpromise.floating(sendRequest(FILTER_MODULE_NAME, {
            action: "deletefilefromrecentlist",
            type: this.appType,
            remove_file_id: this.#fileDesc.id,
            session: ox.session,
        }));
    }
}
