/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import yell from "$/io.ox/core/yell";

import { createDiv } from "@/io.ox/office/tk/dom";
import { AccessTokenError } from "@/io.ox/office/wopi/utils/accesstokenerror";

// constants ==================================================================

/**
 * Maps response error codes to special error messages to be shown in an alert
 * banner.
 */
const ERROR_MESSAGES: Dict<Opt<(details: unknown) => string>> = {
    ERROR_NO_PERMISSION: () => gt("You do not have permissions to access the file."),
    ERROR_NO_WRITE_PERMISSION: () => gt("You do not have permissions to modify the file."),
    ERROR_NO_CREATE_PERMISSION: () => gt("You do not have permissions to create the file."),
    ERROR_FILE_NOT_FOUND: () => gt("The file does not exist anymore. Please refresh your application."),
    ERROR_FOLDER_NOT_FOUND: () => gt("The folder does not exist anymore. Please refresh your application."),
    ERROR_QUOTA_EXCEEDED: () => gt("Your quota has been exceeded. Please remove files then try again."),
    ERROR_CRYPTO_NO_SUPPORTED: () => gt("Editing encrypted files is currently not supported."),
};

// functions ==================================================================

export function showLaunchErrorAlert(error: unknown): void {

    // main message for the alert banner
    let message: Opt<string>;
    // additional debug message
    let details: Opt<string>;

    // add more information if available
    if (error instanceof AccessTokenError) {
        const getMessage = ERROR_MESSAGES[error.cause.code];
        if (getMessage) {
            message = getMessage(error.cause.details);
        } else {
            details = `${error.cause.status} - ${error.cause.code} - ${error.message}`;
        }
    }

    // show an alert banner
    const $alert = yell("error", message ?? gt("Failed to start application. Maybe you have connection problems. Please try again."));

    // add more details to the message if available
    if ($alert && details) {
        // wait for deferred initialization of the DOM node
        $alert.one("notification:appear", () => {
            $alert.find(".message")[0]?.append(createDiv({
                style: { color: "var(--text-gray)", fontSize: "85%" },
                label: details,
            }));
        });
    }
}
