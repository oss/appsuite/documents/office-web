/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { pick } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * Additional error information returned from the access token API endpoint.
 */
export interface AccessTokenErrorCause {
    /** HTTP status code. */
    status: number;
    /** Error code. */
    code: string;
    /** More data specific to the error code. */
    details: unknown;
}

// class AccessTokenError =====================================================

/**
 * A specific exception containing additional error information returned from
 * the access token API endpoint.
 */
export class AccessTokenError extends Error {

    declare readonly cause: AccessTokenErrorCause;

    // constructor ------------------------------------------------------------

    constructor(status: number, errorData: unknown) {

        const message = pick.string(errorData, "error", "Unknown error");
        const cause: AccessTokenErrorCause = {
            status,
            code: pick.string(errorData, "errorcode", "UNKNOWN_ERROR"),
            details: pick.prop(errorData, "errordetails", null),
        };

        super(message, { cause });
    }
}
