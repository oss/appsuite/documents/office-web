/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { str, re } from "@/io.ox/office/tk/algorithms";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { BREAK, getBooleanOption, iterateSelectedDescendantNodes, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';
import { DELETE, TEXT_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { findValidSelection, getOxoPosition, getSelectedElement, hasSameParentComponentAtSpecificIndex, increaseLastIndex,
    isValidTextPosition, iterateParagraphChildNodes } from '@/io.ox/office/textframework/utils/position';
import { CONTENT_NODE_SELECTOR, EMPTYTEXTFRAME_CLASS } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { createSpan, getFocus } from '@/io.ox/office/tk/dom';
import { searchLogger } from '@/io.ox/office/editframework/utils/editconfig';
import { SearchCommand, ReplaceCommand } from '@/io.ox/office/editframework/app/editcontroller';

// constants ==============================================================

/**
 * Timeout in milliseconds to abort long-running quick serach highligter.
 */
const SEARCH_HIGHLIGHT_TIMEOUT = 500;

// class SearchHandler ====================================================

/**
 * The search handler object.
 *
 * @param {TextBaseModel} docModel
 *  The document model of the application.
 */
class SearchHandler extends ModelObject {

    // all text spans that are highlighted (for quick removal)
    #highlightedSpans = [];
    // the results of a quick search as oxoPositions
    #searchResults = [];
    // whether all results have been found in time (performance protection)
    #searchResultsComplete = true;
    // the current index of the searchResults array
    #currentSearchResultId = -1;
    // indicates if the applied operations is due to a search&replace
    // TODO: piggyback the information onto the operation directly
    #replaceOperation = false;
    // the current query of the search result;
    #currentQuery = null;
    // the highlighted span in a spellchecked paragraph in a jQuery collection (or null)
    #spellCheckSpans = null;

    constructor(docModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        this.listenTo(this.docModel, 'operations:before', this.reset);
        this.listenTo(this.docModel, 'paragraph:spellcheck:before', this.#handleSpellCheckBefore);
        this.listenTo(this.docModel, 'paragraph:spellcheck:after', this.#handleSpellCheckAfter);
    }

    // methods ------------------------------------------------------------

    /**
     * Removes all highlighting (e.g. from quick-search) from the document
     * and clears the search results.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.notify=false] -- If set to `true`, this method
     *    will execute the controller item "document/search/result" which
     *    will update all elements in the UI displaying the number of
     *    search results.
     */
    @searchLogger.profileMethod("$badge{SearchHandler} removeHighlighting")
    removeHighlighting(options) {

        // remove highlighting and merge sibling text spans
        this.removeSpansHighlighting(this.#highlightedSpans);
        this.#highlightedSpans = [];

        // used for search and replace
        this.#searchResults = [];
        this.#searchResultsComplete = true;
        this.#currentSearchResultId = -1;

        if (options && options.notify) {
            this.#notifySearchResults(true);
        }
    }

    /**
     * Removes highlighting (e.g. from quick-search) on assigned spans
     */
    removeSpansHighlighting(spans) {

        // remove highlighting and merge sibling text spans
        _.each(spans, span => {
            this.docModel.characterStyles.setElementAttributes(span, { character: { highlight: null } });
            mergeSiblingTextSpans(span);
            mergeSiblingTextSpans(span, true);
        });
    }

    /**
     * Sets the browser selection to the next search result,
     * does nothing if no results are available.
     */
    selectNextSearchResult() {

        if (this.#searchResults.length > 0) {
            const id = this.#currentSearchResultId < this.#searchResults.length - 1 ? this.#currentSearchResultId + 1 : 0;
            this._selectSearchResult(id);
        } else {
            this.#handleNoResult();
        }
    }

    /**
     * Sets the browser selection to the previous search result,
     * does nothing if no results are available.
     */
    selectPreviousSearchResult() {

        if (this.#searchResults.length > 0) {
            const id = (this.#currentSearchResultId > 0) ? this.#currentSearchResultId - 1 : this.#searchResults.length - 1;
            this._selectSearchResult(id);
        } else {
            this.#handleNoResult();
        }
    }

    /**
     * Replaces the search result defined by the given id with the replacement text.
     *
     * @param {Number} id
     *  The id of search result to replace.
     *
     * @param {String} replacement
     *  The replacement for the found text occurrences.
     *
     * @returns {jQuery.Promise}
     *  A jQuery promise object.
     */
    replaceSearchResult(id, replacement) {

        // the change track object
        const changeTrack = this.docModel.getChangeTrack();
        // the undo manager object
        const undoManager = this.docModel.getUndoManager();
        // the selection object
        const selection = this.docModel.getSelection();
        // the search result as range of oxo positions
        const currentResult = this.#searchResults[id];
        let result;
        // the delta of the searched and replaced text
        let textDelta;
        // the highlighted text node
        let node;
        let position;
        // whether the replacement string is empty
        let isEmptyReplace = false;
        // the content length, that was inserted by the same author
        // with activated change tracking
        let countInsertedBySameAuthor = 0;
        // the index inside the logical position that need to be updated
        let changeIndex = 0;
        // currently active root node
        const activeRootNode = this.docModel.getCurrentRootNode();
        // the deferred return value
        const def = this.createDeferred();

        // helper function to remove highlighting from a specified result
        const removeHighlightingLocal = oneResult => {

            position = _.clone(oneResult.start);

            while (_.last(position) < _.last(oneResult.end)) {
                // loop through the range and remove the highlight character style from the spans
                node = getSelectedElement(activeRootNode, position, 'span');
                position[position.length - 1] += $(node).text().length || 1;
                this.docModel.characterStyles.setElementAttributes(node, { character: { highlight: null } });
            }

        };

        // helper function to update the list of all search results
        const updateSearchResults = oneResult => {

            // remove replaced result from search results
            this.#searchResults = _.without(this.#searchResults, oneResult);

            if (this.#currentSearchResultId > this.#searchResults.length - 1) {
                this.#currentSearchResultId = 0;
            }
        };

        if (!currentResult) {
            this.#currentSearchResultId = 0;
            return $.when();
        }

        // handling search results that cannot be replaced, because the contain additional content,
        // like non in-line drawings or comments.
        if (!currentResult.isReplaceable) {
            // removing the highlighting without replacing the content
            removeHighlightingLocal(currentResult);
            // removing the current result from the list of all results
            updateSearchResults(currentResult);
            // ... but do not replace this search result
            return $.when();
        }

        // activating or deactivating the target, if required -> the user might have changed it in the meantime (DOCS-3004)
        const currentResultTarget = currentResult.target || '';
        if (currentResultTarget !== this.docModel.getActiveTarget()) {
            return this._selectSearchResult(id).then(() => { return this.replaceSearchResult(id, replacement); }); // calling this function again later
        }

        if (this.docModel.useSlideMode()) {
            this.docModel.selectSlideView({ showMaster: currentResult.isMaster });
            this.docModel.setActiveSlideId(currentResult.slideID);
        }

        // no replacement of identical strings, if the full search result is inside a single span (DOCS-3719))
        if (this.#currentQuery && replacement && str.equalsICC(this.#currentQuery, replacement)) {
            const spanNode = getSelectedElement(activeRootNode, _.clone(currentResult.start), 'span');
            if ($(spanNode).text() === replacement) { // comparing the precise text with the replacement string, but this time case sensitive
                removeHighlightingLocal(currentResult); // then remove the highlighting
                updateSearchResults(currentResult); // remove replaced result from search results
                return $.when(); // no operation required
            }
        }

        const startPosition = _.clone(currentResult.start);
        const endPosition = _.clone(currentResult.end);
        const fullLength = _.last(endPosition) - _.last(startPosition);
        replacement = _.isString(replacement) ? replacement : '';
        isEmptyReplace = replacement.length === 0;

        // calculating the difference of the length from the searched
        // and the replaced string. This is necessary to recalculate
        // the positions of the following results.
        textDelta = replacement.length - fullLength;

        // Fixing the text delta, if change tracking is active. In this case the delta is
        // typically the length of the replacement, because the old text is not removed, but
        // only marked for removal. But additionally it is possible, that the old text was
        // inserted before with activated change tracking, so that it will be really deleted.
        // Therefore textDelta need to be reduced by that content, that was inserted by the
        // same author.
        if (changeTrack.isActiveChangeTracking()) {
            // then remove the highlighting
            position = _.clone(currentResult.start);
            while (_.last(position) < _.last(currentResult.end)) {
                // loop through the range and count the characters inserted by the same author
                node = getSelectedElement(activeRootNode, position, 'span');
                countInsertedBySameAuthor = countInsertedBySameAuthor + changeTrack.isInsertNodeByCurrentAuthor(node) ? ($(node).text().length || 1) : 0;
                position[position.length - 1] += $(node).text().length || 1;
            }
            textDelta = replacement.length - countInsertedBySameAuthor;
        }

        undoManager.enterUndoGroup(() => {
            // indicate that these operations should not trigger a remove of the highlighting
            // TODO: piggiback this onto the operations
            this.#replaceOperation = true;

            // if we replace a text next to another higlighted text span,
            // we would inherit the highlighting if we do the replacement the common way.
            // In order to keep the span(s) which contains the text to replace,
            // we first add the replacement at the start position.
            // Taking care of empty replacement strings, task 30347
            if (!isEmptyReplace) {
                const attrs = this.#handleAttrsAndCT(getSelectedElement(activeRootNode, startPosition, 'span'));
                this.docModel.insertText(replacement, startPosition, attrs);
                endPosition[endPosition.length - 1] += replacement.length;
            }

            // then remove the highlighting
            removeHighlightingLocal(currentResult);

            // finally delete the previous text.
            startPosition[startPosition.length - 1] += replacement.length;
            selection.setTextSelection(startPosition, endPosition);

            return this.docModel.deleteSelected().done(() => {

                // reset indicator
                this.#replaceOperation = false;

                // update all positions that are in the same paragraph or table cell. Also handle
                // all following positions inside text frames, that are in the same paragraph (40911)
                changeIndex = currentResult.start.length - 1;

                for (let updateId = id + 1; updateId < this.#searchResults.length; updateId++) {
                    result = this.#searchResults[updateId];
                    if (result && hasSameParentComponentAtSpecificIndex(currentResult.start, result.start, changeIndex) && (currentResult.start[changeIndex] < result.start[changeIndex]) && this.#hasSameTarget(currentResult, result)) {
                        result.start[changeIndex] += textDelta;
                        result.end[changeIndex] += textDelta;
                    }
                }

                // remove replaced result from search results
                updateSearchResults(currentResult);

                def.resolve();
            });
        });

        return def.promise();
    }

    /**
     * Replaces the current search result with the replacement text.
     *
     * @param {String} replacement
     *  The replacement for the found text occurrences.
     */
    replaceSelectedSearchResult(replacement) {
        const replacePromise = this.replaceSearchResult(this.#currentSearchResultId, replacement);
        replacePromise.done(() => {
            this._selectSearchResult(this.#currentSearchResultId);
            this.#notifySearchResults();
        });
    }

    /**
     * Searches and highlights the passed text in the entire document.
     * If a selectId is given the corresponding search result is selected.
     *
     * @param {String} query
     *  The text that will be searched in the document.
     *
     * @param {Number} [selectId]
     *  The id of search result to be selected after the search run.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.select=false]
     *    If set too `true`, the matched text will be selected in the
     *    document.
     *  - {boolean} [options.restoreSelection=false]
     *    If set too `true`, the browser selection will be restored at
     *    the end of this function.
     *
     * @returns {SearchResult}
     *  A descriptor object with the following properties:
     *  - {number} count -- The number of matches found in the document.
     *  - {boolean} complete -- Whether all matches in the document have
     *    been found. Will be `false`, if the performance timeout value was
     *    hit. Note that this method will always find and highlight at
     *    least 100 results, before the timeout limit will be checked.
     */
    @searchLogger.profileMethod("$badge{SearchHandler} quickSearch")
    quickSearch(query, selectId, options) {

        // the document root node
        const rootNode = this.docModel.getNode();
        // helper array, used to store matches in target nodes
        const targetSearchResults = [];
        // the regular expression object used to find the query string
        let regexp = null;

        // remove old highlighting and clear previous search results
        // (but do not notify the cleared results to external listeners to prevent flickering)
        this.removeHighlighting();
        this.#currentQuery = query;

        // the starting time of this operation (DOCS-3367: performance limit)
        const startTime = Date.now();
        // whether to exit (embedded) loops (DOCS-3367: performance limit)
        let earlyExit = false;

        // check input parameter
        if (!_.isString(query) || (query.length === 0)) {
            return this.#notifySearchResults();
        }

        query = query.toLowerCase();

        regexp = this.#generateRegExpForSearchString(query);

        if (this.docModel.useSlideMode()) {
            this.docModel.appendAllSlidesToDom();
        }

        // search in all paragraph nodes (also embedded in tables etc.)
        searchLogger.takeTime('find and highlight all matches', () => {
            iterateSelectedDescendantNodes(rootNode, DOM.PARAGRAPH_NODE_SELECTOR, paragraph => {

                // all text spans in the paragraph, as array
                const textSpans = [];
                // the concatenated text from all text spans
                let elementText = '';
                // all matching ranges of the query text in the complete paragraph text
                const matchingRanges = [];
                let start = 0;
                // information about a text span while iterating matching ranges
                const spanInfo = { index: 0, start: 0 };
                // start, end position of the range
                let startPosition;
                let endPosition;
                // the match for the regular expression
                let arrMatch = null;
                // the length of the found string
                let queryLength = 0;
                // the target node of a found string
                let targetNode = null;
                // the target string of a found string
                let target = '';
                // a list with logical node positions for nodes, that do not avoid finding of
                // queries, but avoid replacing of this strings, for example non-inline drawings
                // (inline drawings even avoid finding of query)
                // -> it is sufficient to save only the last position, because search happens
                //    only inside one paragraph.
                const exceptionList = [];

                // adds information of the text span located at 'spanInfo.index' in the 'textSpans' array to 'spanInfo'
                const getTextSpanInfo = () => {
                    spanInfo.span = textSpans[spanInfo.index];
                    spanInfo.length = spanInfo.span ? spanInfo.span.firstChild.nodeValue.length : 0;
                };

                // goes to the next text span in the 'textSpans' array and updates all information in 'spanInfo'
                const getNextTextSpanInfo = () => {
                    spanInfo.index += 1;
                    spanInfo.start += spanInfo.length;
                    getTextSpanInfo();
                };

                // collect all non-empty text spans in the paragraph
                iterateParagraphChildNodes(paragraph, node => {

                    // the replacement string used for non text elements
                    let replacementString = '';

                    // iterate child nodes...
                    if (DOM.isTextSpan(node) && !DOM.isTextFrameTemplateTextSpan(node)) {
                        // for spans add the text
                        textSpans.push(node);
                        elementText += $(node).text();
                    } else if (!DOM.isZeroLengthNode(node)) {
                        // for all but list labels, page breaks and drawing space makers (they don't have a length of 1) add a replacement (FFFC - Object replacement char)
                        // -> adding FFFD for in-line drawings, so that those drawings avoid finding text around it
                        replacementString = this.#isAvoidFindingNode(node) ? '\ufffd' : '\ufffc';
                        textSpans.push(createSpan({ label: replacementString }));
                        elementText += replacementString;
                        exceptionList.push(getOxoPosition(paragraph, node, 0)[0]); // only the position inside the paragraph
                    }
                }, this, { allNodes: true });

                // replace all whitespace characters, and convert to lower case
                // for case-insensitive matching
                elementText = elementText.replace(/\s/g, ' ').toLowerCase();

                let matches = true;
                let slideID = null;
                let isMaster = false;
                let nodeType = '';

                if (DOM.isMarginalNode(paragraph)) {
                    nodeType = 'marginal';

                    targetNode = DOM.getMarginalTargetNode(rootNode, paragraph);
                    target = DOM.getTargetContainerId(targetNode);

                    if (!DOM.isFirstMarginalInDocument(rootNode, targetNode, target)) {
                        matches = false;
                    }
                } else {
                    if (this.docModel.useSlideMode()) {
                        slideID = this.docModel.getSlideIdForNode(paragraph);
                        isMaster = this.docModel.isLayoutOrMasterId(slideID);
                        matches = this.#isValidDrawing(paragraph, isMaster);
                    }
                }

                while (matches && (arrMatch = regexp.exec(elementText))) {

                    start = arrMatch.index;
                    queryLength = arrMatch[0].length;

                    let result;

                    switch (nodeType) {
                        case 'marginal':
                            startPosition = getOxoPosition(targetNode, paragraph, 0);
                            startPosition.push(start);
                            endPosition = increaseLastIndex(startPosition, queryLength);
                            targetSearchResults.push({ start: startPosition, end: endPosition, target, isReplaceable: this.#isReplaceableRange(startPosition, endPosition, exceptionList) });
                            break;
                        case 'comment':
                            startPosition = getOxoPosition(targetNode, paragraph, 0);
                            startPosition.push(start);
                            endPosition = increaseLastIndex(startPosition, queryLength);
                            targetSearchResults.push({ start: startPosition, end: endPosition, target, isReplaceable: this.#isReplaceableRange(startPosition, endPosition, exceptionList) });
                            break;
                        default:
                            startPosition = getOxoPosition(rootNode, paragraph, 0);
                            startPosition.push(start);
                            endPosition = increaseLastIndex(startPosition, queryLength);
                            result = { start: startPosition, end: endPosition, isReplaceable: this.#isReplaceableRange(startPosition, endPosition, exceptionList) };
                            if (this.docModel.useSlideMode()) {
                                result.slideID = slideID;
                                result.isMaster = isMaster;
                            }

                            this.#searchResults.push(result);
                    }

                    matchingRanges.push({ start, end: start + queryLength });

                    // continue after the currently handled text
                    start += queryLength;
                }

                // set highlighting to all occurrences
                getTextSpanInfo();
                matchingRanges.some(range => {

                    let newSpan = null;

                    // find first text span that contains text from current matching range
                    while (spanInfo.start + spanInfo.length <= range.start) {
                        getNextTextSpanInfo();
                    }

                    // process all text spans covered by the current matching range
                    while (!earlyExit && (spanInfo.start < range.end)) {

                        // split beginning of text span not covered by the range
                        if (spanInfo.start < range.start) {
                            DOM.splitTextSpan(spanInfo.span, range.start - spanInfo.start);
                            // update spanInfo
                            spanInfo.length -= (range.start - spanInfo.start);
                            spanInfo.start = range.start;
                        }

                        // split end of text span NOT covered by the range
                        if (range.end < spanInfo.start + spanInfo.length) {
                            newSpan = DOM.splitTextSpan(spanInfo.span, range.end - spanInfo.start, { append: true });
                            // insert the new span into textSpans after the current span
                            textSpans.splice(spanInfo.index + 1, 0, newSpan[0]);
                            // update spanInfo
                            spanInfo.length = range.end - spanInfo.start;
                        }

                        // set highlighting to resulting text span and store it in the global list
                        this.docModel.characterStyles.setElementAttributes(spanInfo.span, { character: { highlight: true } });
                        this.#highlightedSpans.push(spanInfo.span);

                        // DOCS-3367: check whether to leave the loops early:
                        // - highlight at least 100 matches regardless of time
                        // - after that, check elapsed time once after every 20th span
                        const totalSpans = this.#highlightedSpans.length;
                        earlyExit = (totalSpans >= 100) && (totalSpans % 20 === 0) && (Date.now() - startTime > SEARCH_HIGHLIGHT_TIMEOUT);

                        // go to next text span
                        getNextTextSpanInfo();
                    }

                    // DOCS-3367: leave loop early if it takes too much time to highlight the matches
                    return earlyExit;

                }, this);

                // DOCS-3367: leave loop early if it takes too much time to highlight the matches
                if (earlyExit) { return BREAK; }

            }, this);

            searchLogger.log('highlighted ' + this.#highlightedSpans.length + ' matches (' + (earlyExit ? 'exited early' : 'all found') + ')');
        });

        this.#searchResults = _.union(this.#searchResults, targetSearchResults);
        this.#searchResultsComplete = !earlyExit;
        this.#currentSearchResultId = (this.#searchResults.length > 0) ? 0 : -1;

        if (this.#highlightedSpans.length > 0) {

            // DOCS-1947: after "Enter" key in Search toolbar, the first found element must be selected
            const forceSelection = options && options.select;

            // select the given search result
            // TODO: only if user does not change focus to other search elements, but there must be a better way!!!
            if (_.isNumber(selectId) && (forceSelection || !$(getFocus()).parents('.group-container').children('[data-key="document/search/text"], [data-key="document/replace/text"]').length > 0)) {
                this._selectSearchResult(selectId);
            } else if (options && options.restoreSelection) {
                this.docModel.getSelection().restoreBrowserSelection(); // DOCS-3352
            }
        } else if (!(options && options.preview)) {
            this.#handleNoResult();
        }

        // return whether any text in the document matches the passed query text
        return this.#notifySearchResults();
    }

    /**
     * Searches the passed text in the entire document and replaces it with
     * the replacement text
     *
     * @param {String} query
     *  The text that will be searched in the document.
     *
     * @param {String} replacement
     *  The replacement for the found text occurrences.
     *
     * @param {Boolean} [matchCase=false]
     *  Determines if the search is case sensitive (optional, defaults to false).
     *
     * @returns {JPromise<T> | void}
     *  A promise that will always fulfil after the generated operations are
     *  asynchronously applied.
     */
    @searchLogger.profileMethod("$badge{SearchHandler} searchAndReplaceAll")
    searchAndReplaceAll(query, replacement, matchCase) {

        // the selection object
        const selection = this.docModel.getSelection();
        // the undo manager object
        const undoManager = this.docModel.getUndoManager();
        // the change track object
        const changeTrack = this.docModel.getChangeTrack();
        // the documents root node
        const rootNode = this.docModel.getNode();
        // whether the replacement string is empty
        let isEmptyReplace = false;
        // the length of the query and the replacement string
        let queryLength = 0;
        let replacementLength = 0;
        // the delta of the searched and replaced text
        let textDelta = 0;
        // the initial text start position
        let initialStart = selection.getStartPosition();
        // the regular expression object used to find the query string
        let regexp = null;

        // remove old quick search highlighting and clear previous search results
        this.removeHighlighting({ notify: true });

        // check input parameter
        if (!_.isString(query) || (query.length === 0)) { return; }

        if (this.docModel.useSlideMode()) {
            this.docModel.appendAllSlidesToDom();
        }
        this.#replaceOperation = true;
        query = matchCase ? query : query.toLowerCase();
        queryLength = query.length;
        replacement = _.isString(replacement) ? replacement : '';
        replacementLength = replacement.length;
        isEmptyReplace = replacementLength === 0;

        // creating the regular expression to find the query string
        // using 'simpleString', so that string with special character '\ufffc' is NOT found
        regexp = this.#generateRegExpForSearchString(query, { simpleString: true });

        // search in all paragraph nodes (also embedded in tables etc.)
        return undoManager.enterUndoGroup(() => {

            // the deferred to keep the undo group open until it is resolved or rejected
            const undoDef = this.createDeferred();
            // the apply actions deferred
            let applyDef = null;
            // the operations generator
            const generator = this.docModel.createOperationGenerator();
            // the collector for all paragraphs inside text frames
            const paragraphInTextFrameCollector = [];
            // the collector for all paragraphs not inside text frames
            let paragraphCollector = [];
            // the collector for the target IDs that are modified by 'searchAndReplaceAll'
            let savedTargetIDs = null;

            // show a nice message with cancel button
            this.docApp.getView().enterBusy({
                cancelHandler: () => { if (applyDef && applyDef.abort) { applyDef.abort(); } },
                warningLabel: /*#. message shown while replacing text in entire document */ gt('Replacing text.')
            });

            // collecting all paragraphs. All paragraphs inside text frames need to be handled first (40911)
            iterateSelectedDescendantNodes(rootNode, DOM.PARAGRAPH_NODE_SELECTOR, paragraph => {
                if (DOM.isNodeInsideTextFrame(paragraph)) {
                    paragraphInTextFrameCollector.push(paragraph); // paragraphs inside text frames need to be handled first
                } else {
                    paragraphCollector.push(paragraph);
                }
            });

            // merging both collectors for following iteration
            paragraphCollector = paragraphInTextFrameCollector.concat(paragraphCollector);

            // ... an iterating over all paragraphs
            _.each(paragraphCollector, paragraph => {

                // the concatenated text from all text spans
                let elementText = '';
                let start = 0;
                // the offset to apply to the start and end position of the range
                let offset = 0;
                // attribute object for the generated operations
                let attrs = null;
                // the node position before replacing content
                let origPosition = null;
                // the node at the original position
                let node = null;
                // whether the original content was inserted by the same author in change tracking mode
                let insertedBySameAuthor = false;
                // object containing properties generated for operation
                let operationProperties = {};
                // the match for the regular expression
                let arrMatch = null;
                // the marginal node surrounding the found element
                let targetNode = null;
                // the target node for the search
                let target = null;

                // collect all non-empty text spans in the paragraph
                iterateParagraphChildNodes(paragraph, node => {

                    // the replacement string used for non text elements
                    const replacementString = '\ufffc';

                    // No, search and replace in template texts see bug: DOCS-DOCS-1866
                    if (DOM.isTextFrameTemplateTextSpan(node)) {
                        return;
                    }

                    // iterate child nodes...
                    if (DOM.isTextSpan(node)) {
                        // for spans add the text
                        elementText += $(node).text();
                    } else if (!DOM.isZeroLengthNode(node)) {
                        // for all but list labels, page breaks and drawing space makers (they don't have a length of 1) add a replacement (FFFC - Object replacement char)
                        elementText += replacementString;
                    }
                }, this, { allNodes: true });

                // replace all whitespace characters, and convert to lower case
                // for case-insensitive matching
                elementText = elementText.replace(/\s/g, ' ');

                if (!matchCase) {
                    elementText = elementText.toLowerCase();
                }

                if (DOM.isMarginalNode(paragraph)) {
                    targetNode = DOM.getMarginalTargetNode(rootNode, paragraph);
                    target = DOM.getTargetContainerId(targetNode);

                    savedTargetIDs = savedTargetIDs || [];
                    savedTargetIDs.push(target);

                    if (!DOM.isFirstMarginalInDocument(rootNode, targetNode, target)) {
                        return;
                    }

                } else if (this.docModel.useSlideMode()) {
                    const slideID = this.docModel.getSlideIdForNode(paragraph);
                    const masteOrLayout = this.docModel.isLayoutOrMasterId(slideID);

                    if (masteOrLayout) {
                        target = slideID;
                    }

                    if (!this.#isValidDrawing(paragraph, masteOrLayout)) {
                        return;
                    }
                }
                while ((arrMatch = regexp.exec(elementText))) {

                    start = arrMatch.index;
                    queryLength = arrMatch[0].length;
                    node = null;

                    // whether operations for a replacement must be ignored (DOCS-3719)
                    let createOps = true;

                    const startPosition = getOxoPosition(targetNode || rootNode, paragraph, 0);

                    origPosition = _.clone(startPosition);
                    origPosition.push(start);
                    startPosition.push(start + offset);
                    const endPosition = increaseLastIndex(startPosition, queryLength - 1);

                    // no replacement of identical strings, if the full search result is inside a single span (DOCS-3719)
                    if (this.#currentQuery && replacement && str.equalsICC(this.#currentQuery, replacement)) {
                        const spanNode = getSelectedElement(targetNode || rootNode, _.clone(startPosition), 'span');
                        const spanText = $(spanNode).text();
                        if (spanText.length === replacement.length) {
                            if ($(spanNode).text() === replacement) { createOps = false; }
                        } else if (spanText.length > replacement.length) {
                            const spanPosition = getOxoPosition(paragraph, spanNode, 0)[0]; // getting the start position of the span inside its paragraph
                            const positionText = spanText.substring(_.last(startPosition) - spanPosition - offset, _.last(endPosition) - spanPosition - offset + 1); // offset must be ignored
                            if (positionText === replacement) { createOps = false; }
                        }
                    }

                    if (createOps) {

                        // if change tracking is active, the node need to be checked, if it
                        // was inserted by the same author before (sufficient to use the start position?)
                        insertedBySameAuthor = false;
                        if (changeTrack.isActiveChangeTracking()) {
                            node = getSelectedElement(targetNode || rootNode, origPosition, 'span');
                            insertedBySameAuthor = changeTrack.isInsertNodeByCurrentAuthor(node);
                        }

                        if (changeTrack.isActiveChangeTracking() && !insertedBySameAuthor) {
                            operationProperties = { start: _.copy(startPosition), end: _.copy(endPosition), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                            this.docModel.extendPropertiesWithTarget(operationProperties, target);
                            generator.generateOperation(SET_ATTRIBUTES, operationProperties);
                            // the delta of the searched and replaced text -> in this case no text was deleted
                            textDelta = replacementLength;
                        } else {
                            operationProperties = { start: _.copy(startPosition), end: _.copy(endPosition) };
                            this.docModel.extendPropertiesWithTarget(operationProperties, target);
                            generator.generateOperation(DELETE, operationProperties);
                            // the delta of the searched and replaced text -> in this case, replaced text was deleted
                            textDelta = replacementLength - queryLength;
                        }

                        if (!isEmptyReplace) {
                            if (!node) { node = getSelectedElement(targetNode || rootNode, startPosition, 'span'); }
                            // set the attributes of the first letter on the replacement
                            attrs = this.#handleAttrsAndCT(node, { addEmptyRemoveAttrs: true });

                            operationProperties = { text: replacement, start: _.copy(startPosition), attrs };
                            this.docModel.extendPropertiesWithTarget(operationProperties, target);
                            generator.generateOperation(TEXT_INSERT, operationProperties);
                        }

                        // from elementText we get the occurrences before any replacement takes place,
                        // all replacements but the first one need to handle the text offset due to
                        // the replacement offsets of the previous occurrences.
                        offset += textDelta;

                        // continue after the currently handled text
                        start += queryLength;
                    }
                }

            }, this);

            this.docModel.setGUITriggeredOperation(true);
            // apply generated operations
            applyDef = this.docModel.applyOperationsAsync(generator)
            // set cursor behind last replacement text
                .done(() => {

                    if (savedTargetIDs) { // updating header/footer immediately, DOCS-2373
                        _.each(_.uniq(savedTargetIDs), targetID => { this.docModel.getPageLayout().updateEditingHeaderFooter({ givenNode: this.docModel.getRootNode(targetID) }); });
                    }

                    if (!isValidTextPosition(this.docModel.getCurrentRootNode(), initialStart)) { // the position might no longer be valid after replacing content (58543)
                        initialStart = findValidSelection(this.docModel.getCurrentRootNode(), initialStart, selection.getEndPosition(), selection.getLastDocumentPosition(), this.docModel.useSlideMode());
                        initialStart = initialStart.start;
                    }

                    if (initialStart) { selection.setTextSelection(initialStart); }

                    this.#replaceOperation = false;
                    this.docModel.setGUITriggeredOperation(false);
                })
                // leave busy state and close undo group
                .always(() => {
                    this.docApp.docView.leaveBusy();
                    this.docApp.docView.grabFocus();
                    undoDef.resolve();
                })
                // add progress handling
                .progress(progress => {
                    this.docApp.getView().updateBusyProgress(progress);
                });

            return undoDef.promise();

        }, this); // enterUndoGroup
    }

    /**
     * removing search highlighting
     */
    clearHighlighting() {
        // removing search highlighting
        if (this.#hasHighlighting()) {
            this.removeHighlighting({ notify: true });
        }
    }

    /**
     * clear search results and trigger the debounced quick search
     */
    reset() {
        if (!this.#replaceOperation) {
            // clear search results and trigger the debounced quick search
            this.removeHighlighting({ notify: true });
            if (this.docApp.docView.isSearchActive()) { // DOCS-3384, DOCS-3352
                this.#previewSearchResultsSlow(this.#currentQuery, { refresh: true, restoreSelection: true });
            }
        }
    }

    /**
     * Executes a "Search" command.
     */
    executeSearch(command, query/*, config*/) {
        switch (command) {
            case SearchCommand.CONFIG:
                return;
            case SearchCommand.PREVIEW:
                return this.#previewSearchResults(query);
            case SearchCommand.START:
                return this.quickSearch(query, 0, { select: true });
            case SearchCommand.NEXT:
                this.#startNewQuickSearchIfNecessary(query);
                return this.selectNextSearchResult();
            case SearchCommand.PREV:
                this.#startNewQuickSearchIfNecessary(query);
                return this.selectPreviousSearchResult();
            case SearchCommand.END:
                return this.removeHighlighting({ notify: true });
        }
        searchLogger.error('$badge{SearchHandler} executeSearch: unsupported command "' + command + '"');
    }

    /**
     * Executes a "Replace" command.
     */
    executeReplace(command, query, replace/*, config*/) {
        switch (command) {
            case ReplaceCommand.NEXT:
                if (this.#startNewQuickSearchIfNecessary(query)) {
                    return this.selectNextSearchResult();
                }
                return this.replaceSelectedSearchResult(replace);
            case ReplaceCommand.ALL:
                return this.searchAndReplaceAll(query, replace);
        }
        searchLogger.error('$badge{SearchHandler} executeReplace: unsupported command "' + command + '"');
    }

    // private methods ----------------------------------------------------

    /**
     * Returns whether the document contains any highlighted ranges.
     */
    #hasHighlighting() {
        return this.#highlightedSpans.length > 0;
    }

    /**
     * Helper function that generates a regular expression for a specified
     * search string.
     *
     * @param {String} text
     *  The query string.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.simpleString=false]
     *      If set to true, the regular expression will be generated directly
     *      with the specified string parameter. In this case the replacement
     *      value for the nodes inside the string ('\ufffc*') will not be used
     *      and those string elements will not be found. In this case the string,
     *      that include for example a floated drawing or a drawing place holder
     *      will not be found. This is used during replacement, so that those
     *      drawings are not automatically replaced. But it shall be possible
     *      to find those strings, that include such a drawing.
     *
     * @returns {RegExp}
     *  The generated regular expression object.
     */
    #generateRegExpForSearchString(text, options) {

        // the string for creating the regular expression
        let regExpString = '';
        // the loop iterator
        let i = 0;
        // the length of the string
        let len = 0;
        // whether only a simple string without special replacement characters shall be found
        const simpleString = getBooleanOption(options, 'simpleString', false);

        // bug 40404: escape special RE characters (search for \ or . or similar must work)

        if (simpleString) {
            regExpString = re.escape(text);
        } else {
            len = text.length;
            for (i = 0; i < len - 1; i++) {
                regExpString += re.escape(text[i]) + '\\ufffc*';
            }
            regExpString += re.escape(text[len - 1]);
        }

        return new RegExp(regExpString, 'g');
    }

    /**
     * Helper function to check, whether there is a node inside the specified range, that hinders
     * this range from being replaced.
     *
     * @param {Number[]} startPosition
     *  The logical start position of the range.
     *
     * @param {Number[]} endPosition
     *  The logical end position of the range.
     *
     * @param {Number[]} exceptionList
     *  The list with the logical positions of those nodes, that hinder the range from being replaced.
     *  This list contains only the positions inside the paragraph, no array.
     *
     * @returns {Boolean}
     *  Whether the specified range contains an element that hinders it from being replaced.
     */
    #isReplaceableRange(startPosition, endPosition, exceptionList) {

        if (exceptionList.length === 0) { return true; }

        // the start value inside the paragraph
        const start = _.last(startPosition);
        // the end value inside the paragraph
        const end = _.last(endPosition);

        // the exception list contains the logical positions of the not replaceable elements inside the paragraph,
        // only the numbers, not an array.
        return exceptionList.every(number => {
            return (number <= start) || (number >= end);
        });
    }

    /**
     * Comparing two search results, if they have the same target. This function also returns
     * 'true', if both results do not have a target.
     *
     * @param {Object} result1
     *  The first result object.
     *
     * @param {Object} result2
     *  The second result object.
     *
     * @returns {Boolean}
     *  Whether the two specified results have the same target or both have no target at all.
     */
    #hasSameTarget(result1, result2) {

        if (!result1.target && !result2.target) { return true; }

        if ((result1.target && !result2.target) || (!result1.target && result2.target)) { return false; }

        return result1.target === result2.target;
    }

    /**
     * Specifying those in-line nodes, that can be located inside a string and avoid, that the string
     * is found in a search. This is for example the case for in-line drawings. A floated drawing
     * or a placeholder for a drawing or a comment node do not disturb the search of a string.
     *
     * @param {Node|jQuery|Null} node
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @returns {Boolean}
     *  Whether this is a node that hinders a string to be found in a search, if it is located inside
     *  the string.
     */
    #isAvoidFindingNode(node) {
        return DOM.isInlineDrawingNode(node);
    }

    /**
     * Sets the browser selection to the search result given by the id of the this.#searchResults array
     *
     * @param {Number} id
     *  The id of search result to select.
     *
     * @returns {jQuery.Promise}
     *  A promise that is resolved, when the new selection is set.
     */
    @searchLogger.profileMethod("$badge{SearchHandler} _selectSearchResult")
    _selectSearchResult(id) {

        searchLogger.trace('resultId=' + id);

        // the page layout object
        const pageLayout = this.docModel.getPageLayout();
        // the selection object
        const selection = this.docModel.getSelection();
        // the search result as logical positions
        const result = this.#searchResults[id];
        // if target available, root node for switching context between main document and marginal nodes
        let $node = null;

        // Bug 28386: defer selection, otherwise Enter key event will delete selected search result
        return this.setTimeout(() => {

            // Important order:
            // 1. grabbing the focus
            // 2. evaluating and setting target, if necessary
            // 3. setting new text selection

            this.docApp.getView().grabFocus();

            if (result) {
                this.#currentSearchResultId = id;

                // Set the active slide if it is the presenter app
                if (this.docModel.useSlideMode()) {
                    this.docModel.selectSlideView({ showMaster: result.isMaster });
                    this.docModel.setActiveSlideId(result.slideID);
                }

                if (this.docModel.isHeaderFooterEditState()) {
                    pageLayout.leaveHeaderFooterEditMode();
                    selection.setNewRootNode(this.docModel.getNode()); // restore original rootNode
                    this.docModel.updateEditingHeaderFooterDebounced();
                }

                if (result.target) {
                    $node = pageLayout.getFirstMarginalByIdInDoc(result.target);
                    if (this.docModel.isHeaderFooterEditState()) {
                        if (result.target !== this.docModel.getActiveTarget()) {
                            pageLayout.switchTargetNodeInHeaderMode($node);
                        }
                    } else {
                        pageLayout.enterHeaderFooterEditMode($node);
                    }
                }

                searchLogger.trace('selecting result at [' + result.start.join() + ']');
                selection.setTextSelection(result.start, result.end);
            }
        }, 0);
    }

    #handleNoResult() {

        // reset text selection
        const selection = this.docModel.getSelection();
        selection.setTextSelection(selection.getStartPosition());

        //info
        this.docApp.docView.yell({
            type: "info",
            message: gt("No search result found.")
        });
    }

    #handleAttrsAndCT(node, options) {
        const changeTrack = this.docModel.getChangeTrack();
        const attrs = getExplicitAttributeSet(node);
        let changeAttrs = null;

        if (changeTrack.isActiveChangeTracking()) {
            changeAttrs = { inserted: changeTrack.getChangeTrackInfo() };
            if (getBooleanOption(options, 'addEmptyRemoveAttrs', false)) { changeAttrs.removed = null; } // DOCS-2780
            attrs.changes = changeAttrs;
        }
        return attrs;
    }

    /**
     * returns if the drawing of this paragraph is a valid search and replace drawing.
     * Placeholder with text on normal slides and shapes are valid. All other are not valid.
     *
     * @param {Node} paragraph node to test if drawing of the paragraph is valid
     * @param {Boolen} isMasterOrLayout true if it is a master- or layoutslide
     * @returns {Boolean} true if the drawing is valid, otherwise false.
     */
    #isValidDrawing(paragraph, isMasterOrLayout) {

        const currentDrawing = $(paragraph).closest('div.drawing');
        const currentDrawingContent = $(paragraph).closest(CONTENT_NODE_SELECTOR);

        return (!currentDrawingContent.hasClass(EMPTYTEXTFRAME_CLASS) || currentDrawing.hasClass('selected')) && (!isMasterOrLayout || !this.docModel.isPlaceHolderDrawing(currentDrawing));
    }

    /**
     * Start new quick search if the search result is empty or the current query is
     * different to the given query.
     *
     * @param {String} query
     * @returns {undefined}
     */
    #startNewQuickSearchIfNecessary(query) {
        if ((_.isString(query) && query.length > 0) && (this.#searchResults.length === 0 || query !== this.#currentQuery)) {
            this.quickSearch(query, 0);
            return true;
        }
        return false;
    }

    #notifySearchResults() {
        const result = { count: this.#searchResults.length, complete: this.#searchResultsComplete };
        this.docApp.docController.executeItem('document/search/result', result);
        return result;
    }

    /**
     * Handler function for event triggered by the spellchecker before a paragraph is spellchecked (DOCS-4564)
     *
     * @param {jQuery} paragraph
     *  The jQuerified paragraph that will be spellchecked.
     */
    #handleSpellCheckBefore(paragraph) {
        this.#spellCheckSpans = paragraph.find('span.highlight');
    }

    /**
     * Handler function for event triggered by the spellchecker after a paragraph is spellchecked (DOCS-4564)
     *
     * @param {jQuery} paragraph
     *  The jQuerified paragraph that was spellchecked.
     */
    #handleSpellCheckAfter(paragraph) {
        if (this.#spellCheckSpans?.length) {

            let spanIndex = 0;
            const newSpellCheckSpans = paragraph.find('span.highlight');

            if (newSpellCheckSpans.length < this.#spellCheckSpans.length) {
                // possible after merging spans (disable spellchecker) -> update array "this.#highlightedSpans"
                this.#spellCheckSpans.each((_idx, span) => {
                    const currentIndex = this.#highlightedSpans.indexOf(span); // exists span also in "this.#highlightedSpans"?
                    if (currentIndex > -1 && newSpellCheckSpans.index(span) < 0) {
                        this.#highlightedSpans.splice(currentIndex, 1);
                    }
                });
            } else if (newSpellCheckSpans.length > this.#spellCheckSpans.length) {
                // possible after splitting spans (enable spellchecker) -> update array "this.#highlightedSpans"
                newSpellCheckSpans.each((_idx, span) => {
                    const currentIndex = this.#highlightedSpans.indexOf(span); // exists span also in "this.#highlightedSpans"?
                    if (currentIndex > -1) {
                        spanIndex = currentIndex; // existed already before
                    } else {
                        this.#highlightedSpans.splice(++spanIndex, 0, span);
                    }
                });
            }
        }

        this.#spellCheckSpans = null;
    }

    /**
     * Debounced quick-search used to highlight the matches in the document
     * while typing in the search text field.
     *
     * @param {number} delayTime
     *  The delay time for the debounced function
     */
    #previewSearchResultsBase(delayTime) {

        let pendingQuery = null;
        let isRefresh = false;
        let restoreSelection = false;

        // direct callback
        const storeParameters = (query, options) => {
            pendingQuery = query;
            // immediately clear highlighting when deleting search text
            if (!query) {
                this.#currentQuery = '';
                this.clearHighlighting();
            }

            isRefresh = options && options.refresh;
            restoreSelection = options && options.restoreSelection;
        };

        // deferred callback: highlight search results
        const executeQuickSearch = () => {
            const selectId = isRefresh ? null : 0;
            if (pendingQuery && (isRefresh || (pendingQuery !== this.#currentQuery))) {
                this.quickSearch(pendingQuery, selectId, { preview: true, restoreSelection });
            }

            isRefresh = false;
            restoreSelection = false;
        };

        // create and return the debounced method (but waiting for end of all operations (DOCS-3419))
        return this.docApp.createDebouncedFor(this, storeParameters, executeQuickSearch, { delay: delayTime });
    }

    // two debounced function: one for fast update of search results and one for slower update
    #previewSearchResults = this.#previewSearchResultsBase(250);
    #previewSearchResultsSlow = this.#previewSearchResultsBase(2000); // reducing flickering when typing with active selection (DOCS-3384)
}

// exports ================================================================

export default SearchHandler;
