/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { ary, is, json } from "@/io.ox/office/tk/algorithms";
import type { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { getExplicitAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import type { CharacterAttributes, PtCharacterAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { checkForHyperlink } from "@/io.ox/office/editframework/utils/hyperlinkutils";
import type { InsertStyleSheetOperation, Position } from "@/io.ox/office/editframework/utils/operations";

import type TextBaseModel from "@/io.ox/office/textframework/model/editor";
import type Selection from "@/io.ox/office/textframework/selection/selection";
import { INSERT_STYLESHEET, SET_ATTRIBUTES, type SetAttributesOperation } from "@/io.ox/office/textframework/utils/operations";
import { isTextSpan, isValidNodeInsideWord } from "@/io.ox/office/textframework/utils/dom";
import { getDOMPosition, getParagraphLength, getPositionRangeForNode, getWordSelection,
    increaseLastIndex, type WordSelectionResult } from "@/io.ox/office/textframework/utils/position";

// types ==================================================================

/**
 * The return value of the function "findURLSelection"
 */

export interface FindURLSelectionResult {
    start: number | null;
    end: number | null;
}

/**
 * The return value of the function "getURLFromPosition"
 */
export interface GetURLFromPositionResult {
    url: string | null;
    beforeHyperlink: boolean;
    clearAttributes: boolean;
}

/**
 * The return value of the function "checkForHyperlinkText"
 */
export interface CheckForHyperlinkTextResult {
    start: Position;
    end: Position;
    text: string;
    url: string | null;
    isStartOfParagraph?: boolean;
}

// static private functions ===============================================

function createResultFromHyperlinkSelection(position: Position, hyperlinkSelection: WordSelectionResult): CheckForHyperlinkTextResult {

    const start = json.flatClone(position);
    const end = json.flatClone(position);

    // create result with correct Position objects
    start[start.length - 1] = hyperlinkSelection.start;
    end[end.length - 1] = hyperlinkSelection.end;

    return { start, end, text: hyperlinkSelection.text, url: null };
}

/**
 * Provides static helper methods for manipulation and calculation
 * of a hyperlink.
 */

export const Separators = ["!", "?", ".", " ", "-", ":", ",", "\xa0"];
export const Protocols = ["http://", "https://", "ftp://", "mailto:"];

// templates for web/ftp identifications
export const TextTemplates = ["www.", "ftp."];

/**
 * Predefined character attributes needed to remove a hyperlink style
 * from a piece of text.
 */
export const CLEAR_ATTRIBUTES = { styleId: null, character: { url: null } };

// methods ----------------------------------------------------------------

/**
 * Provides the URL of a selection without range.
 *
 * @param editor
 *  The editor object.
 *
 * @param selection
 *  A selection object which describes the current selection.
 *
 * @param [options]
 *  Optional options, introduced for performance reasons.
 *  @param [options.splitOperation] TODO
 *
 * @returns
 *  - url
 *      The URL or null if no URL character attribute is set at the
 *      selection.
 *  - beforeHyperlink
 *      True if we provide the URL for the hyperlink which is located at
 *      the next position.
 *  - clearAttributes
 *      True if the preselected attributes of the text model have to be
 *      extended by character attributes that remove the current URL style.
 */
export function getURLFromPosition(editor: TextBaseModel, selection: Selection, options?: { splitOperation?: boolean }): GetURLFromPositionResult  {

    const result: GetURLFromPositionResult = { url: null, beforeHyperlink: false, clearAttributes: false };
    const splitOperation = options?.splitOperation;

    if (!selection.hasRange()) {

        // find out a possible URL set for the current position
        let obj;
        if (splitOperation && selection.getInsertTextPoint()) {
            obj = selection.getInsertTextPoint();  // Performance: Using previously cached node point
        } else {
            obj = getDOMPosition(editor.getCurrentRootNode(), selection.getStartPosition());
        }

        const parentNode = obj?.node?.parentNode as Opt<HTMLElement>;

        if (parentNode && isTextSpan(parentNode)) {
            const attributes = getExplicitAttributes(parentNode, "character", true) as Partial<CharacterAttributes>;
            if (attributes.url) {
                // Now we have to check some edge cases to prevent to show
                // the popup for a paragraph which contains only an empty span
                // having set the url attribute.
                const span = $(parentNode);
                if ((span.text().length > 0) || (span.prev().length > 0) || isTextSpan(span.next())) {
                    result.url = attributes.url;
                } else {
                    result.clearAttributes = true;
                }
            }

            // Check next special case: Before a hyperlink we always want to show the popup
            if (splitOperation && selection.getParagraphCache()) {
                // Performance: Using previously cached paragraph and text position
                const paraCache = selection.getParagraphCache();
                const paraNode = paraCache?.node;
                const paraOffset = paraCache?.offset || 0;
                if (paraNode) { obj = getDOMPosition(paraNode, [paraOffset + 1]); }
            } else {
                const nextPosition = selection.getStartPosition();
                nextPosition[nextPosition.length - 1] += 1;
                obj = getDOMPosition(editor.getCurrentRootNode(), nextPosition);
            }

            const newParentNode = obj?.node?.parentNode as Opt<HTMLElement>;
            if (newParentNode && isTextSpan(newParentNode)) {
                const nextAttributes = getExplicitAttributes(newParentNode, "character", true) as Partial<CharacterAttributes>;
                if (nextAttributes.url && (attributes.url !== nextAttributes.url)) {
                    result.url = nextAttributes.url;
                    result.beforeHyperlink = true;
                }
            }
        }
    }

    return result;
}

/**
 * Tries to find a selection range based on the current text cursor
 * position. The URL character style is used to find a consecutive
 * range.
 *
 * @param editor
 *  The editor object.
 *
 * @param selection
 *  The current selection.
 *
 * @returns
 *  The new selection properties which includes a range with the same URL
 *  character style or null if no URL character style could be found.
 */
export function findSelectionRange(editor: TextBaseModel, selection: Selection): WordSelectionResult | FindURLSelectionResult | null  {

    // the return object
    let newSelection = null;
    // the url
    let url = null;
    // the result object from "getURLFromPosition"
    let result = null;

    if (!selection.hasRange()) {

        // the enclosing paragraph (or slide)
        const paragraph = selection.getEnclosingParagraph({ allowSlide: true });

        if (paragraph) {

            // the logical start position
            const startPosition = selection.getStartPosition();
            let pos = ary.last(startPosition)!;

            if (!selection.hasRange()) {
                result = getURLFromPosition(editor, selection);
                url = result.url;
            }

            if (url) {
                if (result?.beforeHyperlink) {
                    pos += 1;
                }
                startPosition[startPosition.length - 1] = pos;
                newSelection = findURLSelection(editor, startPosition, url);
            } else {
                newSelection = getWordSelection(paragraph, pos, Separators);
            }
        }
    }

    return newSelection;
}

/**
 * Tries to find a selection based on the provided position which includes
 * the provided url as character style.
 *
 * @param editor
 *  The editor instance.
 *
 * @param startPosition
 *  The startPosition in the paragraph
 *
 * @param url
 *  The hyperlink URL which is set as character style at pos
 *
 * @returns
 *  Contains start and end position of the selection where both could be
 *  null which means that there is no selection but the hyperlink should be
 *  inserted at the position.
 */
export function findURLSelection(editor: TextBaseModel, startPosition: Position, url: string): FindURLSelectionResult {

    const obj = getDOMPosition(editor.getCurrentRootNode(), startPosition);
    const parentNode = obj?.node?.parentNode as HTMLElement;
    if (!parentNode || !isTextSpan(parentNode)) {
        return { start: null, end: null };
    }

    let startNode = parentNode;
    let endNode = parentNode;

    while (endNode?.nextSibling && (isTextSpan(endNode.nextSibling as HTMLElement) || isValidNodeInsideWord(endNode.nextSibling as HTMLElement))) {

        if (isTextSpan(endNode.nextSibling as HTMLElement)) {
            const attributes = editor.characterStyles.getElementAttributes(endNode.nextSibling as HTMLElement).character;
            if (attributes.url !== url) {
                break;
            }
        }

        endNode = endNode.nextSibling as HTMLElement;
    }

    while (startNode?.previousSibling && (isTextSpan(startNode.previousSibling as HTMLElement) || isValidNodeInsideWord(startNode.previousSibling as HTMLElement))) {

        if (isTextSpan(startNode.previousSibling as HTMLElement)) {

            const attributes = editor.characterStyles.getElementAttributes(startNode.previousSibling as HTMLElement).character;
            if (attributes.url !== url) {
                break;
            }
        }

        startNode = startNode.previousSibling as HTMLElement;
    }

    const startPos = getPositionRangeForNode(editor.getCurrentRootNode(), startNode, true);
    const endPos = (startNode !== endNode) ? getPositionRangeForNode(editor.getCurrentRootNode(), endNode, true) : startPos;

    const startValue = startPos && is.number(startPos.start[startPos.start.length - 1]) ? startPos.start[startPos.start.length - 1] : null;
    const endValue = endPos && is.number(endPos.end[endPos.end.length - 1]) ? endPos.end[endPos.end.length - 1] : null;
    return { start: startValue, end: endValue };
}

/**
 * Checks for a text in a paragraph which defines a hyperlink
 * e.g. http://www.open-xchange.com
 *
 * @param editor
 *  The editor instance.
 *
 * @param paragraph
 *  The paragraph node to check
 *
 * @param position
 *  The rightmost position to check the text to the left for
 *  a hyperlink text.
 *
 * @returns
 *  Returns an object containing the start and end position
 *  to set the hyperlink style/url or null if no hyperlink
 *  has been found.
 */
export function checkForHyperlinkText(editor: TextBaseModel, paragraph: NodeOrJQuery, position: Position): CheckForHyperlinkTextResult | null {

    // the return object of this function
    let result = null;
    // whether the position is at the start of a paragraph (first text position)
    let isStartOfParagraph = false;

    // helper function to receive url attribute from specified text span node
    const  getUrlFromTextNode = (node: HTMLElement): string | null => {
        const charAttrs = isTextSpan(node) ? editor.characterStyles.getElementAttributes(node).character : null;
        return charAttrs?.url || null;
    };

    if (position !== null) {

        // the position inside the paragraph
        const pos = ary.last(position)!;
        const hyperlinkSelection = getWordSelection(paragraph, pos, [" ", "\xa0"], { onlyLeft: true, returnTextSpan: true });

        // check, if the node already contains a valid url (at the position, where "space" or "enter" was pressed)
        let url: string | null = null;
        if (hyperlinkSelection?.node) {
            url = getUrlFromTextNode(hyperlinkSelection.node);
        }

        // if the space was inserted into an existing link, it shall not split the link
        if (url) {

            // the selection range object for an URL
            const urlSelection = findURLSelection(editor, position, url); // finding the complete hyperlink range

            if (urlSelection) {

                // special case for spaces at the beginning of paragraphs before hyperlink (43830)
                if (hyperlinkSelection && pos === hyperlinkSelection.start && pos === hyperlinkSelection.end && is.number(urlSelection.start) && pos === urlSelection.start) { isStartOfParagraph = true; }

                // space in a hyperlink
                if (!isStartOfParagraph && is.number(urlSelection.end) && urlSelection.end > pos) { return null; }
            }
        }

        // if no url was found, try to evaluate it from the specified text
        if (!url && hyperlinkSelection?.text) {
            url = checkForHyperlink(hyperlinkSelection.text);
        }

        if (url) {
            // for the setAttributes operation, it is necessary to reduce the end position by "1"
            // -> otherwise following text spans behind "space" or "enter" receive also the url.
            if (hyperlinkSelection?.end) {
                hyperlinkSelection.end--;
                if (hyperlinkSelection.end < hyperlinkSelection.start) { hyperlinkSelection.start = hyperlinkSelection.end; } // avoiding inverse selections
            }
            // generating the return object from the found selection
            result = createResultFromHyperlinkSelection(position, hyperlinkSelection!);
            result.url = url;
            result.isStartOfParagraph = isStartOfParagraph;
        }
    }

    return result;
}

/**
 * "Inserts" a hyperlink at the provided selection using the
 * provided url.
 *
 * @param editor
 *  The editor instance to use.
 *
 * @param start
 *  The start position of the selection
 *
 * @param end
 *  The end position of the selection.
 *
 * @param url
 *  The url of the hyperlink to set at the selection
 *
 * @param [target]
 *  The Id referencing to header or footer root node.
 *
 * @param [options]
 *  Additional options:
 *  Optional parameters:
 *  @param [options.removeHyperLinkAttributes=false]
 *      If set to true, an additional operation for clearing the URL attributes
 *      will be generated.
 */
export function insertHyperlink(editor: TextBaseModel, start: Position, end: Position, url: string, target: string, options?: { removeHyperLinkAttributes: boolean }): void {

    const hyperlinkStyleId = editor.getDefaultUIHyperlinkStylesheet();
    const generator = editor.createOperationGenerator();
    const removeHyperLinkAttributes = options?.removeHyperLinkAttributes;

    // never generation hyperlink operation on empty paragraph (42353)
    if (ary.at(start, -1) === 0 && ary.at(end, -1) === 0 && getParagraphLength(editor.getRootNode(target), start.slice(0, -1) as Position) === 0) {
        globalLogger.warn("Ignoring invalid hyperlink detection in empty paragraph. URL: " + url);
        return;
    }

    if (hyperlinkStyleId && editor.characterStyles.isDirty(hyperlinkStyleId) && !editor.useSlideMode()) {

        const styleSheetOperationProperties: Partial<InsertStyleSheetOperation> = {
            attrs: editor.characterStyles.getStyleSheetAttributeMap(hyperlinkStyleId),
            type: "character",
            styleId: hyperlinkStyleId,
            styleName: editor.characterStyles.getName(hyperlinkStyleId)!,
            parent: editor.characterStyles.getParentId(hyperlinkStyleId)!,
            uiPriority: editor.characterStyles.getUIPriority(hyperlinkStyleId)
        };

        // parent is an optional value, should not be send as "null"
        if (styleSheetOperationProperties.parent === null || styleSheetOperationProperties.parent === "") { delete styleSheetOperationProperties.parent; }

        // insert hyperlink style to document
        generator.generateOperation(INSERT_STYLESHEET, styleSheetOperationProperties);
        editor.characterStyles.setDirty(hyperlinkStyleId, false);
    }

    const charAttrs: PtCharacterAttributeSet = { character: { url } };

    const setAttributesOperationProperties: Partial<SetAttributesOperation> = {
        attrs: charAttrs,
        start: json.flatClone(start),
        end: removeHyperLinkAttributes ? increaseLastIndex(end) : json.flatClone(end)
    };

    // OX Presentation does not use style Id
    if (hyperlinkStyleId && !editor.useSlideMode()) { charAttrs.styleId = hyperlinkStyleId; }

    if (target) { setAttributesOperationProperties.target = target; }
    generator.generateOperation(SET_ATTRIBUTES, setAttributesOperationProperties);

    if (removeHyperLinkAttributes) { generator.generateOperation(SET_ATTRIBUTES, { start: increaseLastIndex(end), attrs: CLEAR_ATTRIBUTES }); }

    editor.applyOperations(generator);
}

/**
 * "Removes" the hyperlink at the provided selection.
 *
 * @param editor
 *  The editor instance to use.
 *
 * @param start
 *  The start position of the selection
 *
 * @param end
 *  The end position of the selection.
 */
export function removeHyperlink(editor: TextBaseModel, start: Position, end: Position): void {

    const generator = editor.createOperationGenerator();

    generator.generateOperation(SET_ATTRIBUTES, {
        attrs: CLEAR_ATTRIBUTES,
        start: json.flatClone(start),
        end: json.flatClone(end)
    });

    editor.applyOperations(generator);
}

/**
 * Provides the text from a selection.
 *
 * @param selection
 *  A selection object which is used to extract the text from.
 */
export function getSelectedText(selection: Selection): string {

    let text = "";

    selection.iterateNodes((node, _pos, start, length) => {
        if ((start >= 0) && (length >= 0) && isTextSpan(node)) {
            const nodeText = $(node).text();
            if (nodeText) {
                text = text.concat(nodeText.slice(start, start + length));
            }
        }
    });

    return text;
}
