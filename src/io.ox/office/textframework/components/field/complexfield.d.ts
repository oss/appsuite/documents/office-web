/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";

import { PtCharacterAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { Operation, Position } from "@/io.ox/office/editframework/utils/operations";

import BaseField from "@/io.ox/office/textframework/components/field/basefield";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";

// class ComplexField =========================================================

export default class ComplexField extends BaseField {

    constructor(docModel: TextBaseModel);

    checkRestoringSpecialFields(searchNode: NodeOrJQuery, startOffset: number, endOffset: number): void;
    checkRestoringSpecialFieldsInContainer(node: NodeOrJQuery): void;
    convertToSpecialField(fieldId: string, marginalTarget: string, type: string): void;

    getAllFields(): Dict<string | JQuery>;
    getAllFieldsCount(): number;

    getComplexField(id: string): JQuery | null;
    getMarginalComplexFieldsTarget(id: string): JQuery | null;
    getNextAnchorName(num?: number): string;
    getNextBookmarkId(num?: number): string;
    getNextComplexFieldID(): string;
    handlePasteOperationIDs(operations: Operation[]): void;
    insertComplexField(fieldType: string, fieldFormat: string): JPromise;
    insertComplexFieldHandler(start: Position, instruction: string, attrs: PtCharacterAttributeSet, target: string): boolean;
    insertTOCField(fieldFormat: string, options?: { tabStyle?: string }): JPromise;
    isComplexFieldEmpty(): boolean;
    isComplexFieldId(id: string): boolean;
    isEmpty(): boolean;
    markAllFieldsAfterUndo(): void;
    markComplexFieldForHighlighting(field: NodeOrJQuery, id: string): void;

    refreshComplexFields(usedFastLoad: boolean): void;
    removeAllInsertedComplexFields(node: NodeOrJQuery): void;
    removeFromComplexFieldCollection(node: NodeOrJQuery): void;
    restoreSpecialField(node: NodeOrJQuery): void;

    updateByInstruction(node: NodeOrJQuery, instruction: string): JPromise;
    updateComplexFieldCollector(node: NodeOrJQuery): void;
    updateComplexFieldHandler(start: Position, instruction: string, attrs: PtCharacterAttributeSet, target: string, external: boolean): boolean;
    updatePageCountInCurrentNodeCx(node: JQuery, count: number): void;
    updatePageNumInCurrentMarginalCx(node: JQuery, isHeader: boolean, count: number): void;
    updatePageNumInCurrentNodeCx(node: JQuery, isHeader: boolean, count: number): void;
    updateComplexFieldFormat(fieldType: string, fieldFormat: string, fieldNodePos: Position, fieldId: string, oldInstruction: string): JPromise;
    updateCxPageFieldsInMarginals(): void;
    updateDateFromPopupValue(node: NodeOrJQuery, fieldValue: string): void;
    updateDateTimeAndSpecialFields(options?: { avoidFieldUpdate?: boolean; setNonModifyingMarker?: boolean }): void;
    updateDateTimeFieldOnDownload(node: NodeOrJQuery, instruction: string): void;
    updateTOCField(node: NodeOrJQuery, instruction: string | string[]): JPromise;
}
