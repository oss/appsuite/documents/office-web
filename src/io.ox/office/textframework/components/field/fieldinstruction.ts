/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { to, str } from "@/io.ox/office/tk/algorithms";

import { type FieldType, fromInstruction } from "@/io.ox/office/textframework/components/field/fieldtype";
import { FieldSwitch } from "@/io.ox/office/textframework/components/field/fieldswitch";
import { GeneralNumeric, GeneralString, GeneralAny } from "@/io.ox/office/textframework/components/field/fieldswitcharguments";

// types ======================================================================

// class FieldInstruction =======================================================

/**
 *
 * @param
 *  The instruction as it is saved in ooxml files.
 *
 * e.g: USERINITIALS "jaj" \* Upper
 * or: MERGEFIELD CoutesyTitle \f " " MERGEFIELD FirstName \f " "
 */
export class FieldInstruction {

    // properties -------------------------------------------------------------

    readonly #type: FieldType;
    readonly #userDefinedTypeValue?: string;
    #argument: string;

    readonly #switches: FieldSwitch[] = [];

    // constructor ------------------------------------------------------------

    constructor(instruction: string) {
        const parsedFieldType = fromInstruction(str.trimAll(instruction)),
            split = parsedFieldType.remainingInstruction.split("\\");

        this.#type = parsedFieldType.fieldType;
        this.#userDefinedTypeValue = parsedFieldType.userDefinedTypeValue;
        this.#argument = split[0].trim(); // the type is always the first entry within the split array
        for (let i = 1; i < split.length; i++) {
            this.#switches.push(new FieldSwitch(split[i]));
        }
    }

    getType(): FieldType {
        return this.#type;
    }

    getArgument(): string {
        return this.#argument;
    }

    setArgument(argument: string): void {
        this.#argument = argument;
    }

    getUserDefinedTypeValue(): Opt<string> {
        return this.#userDefinedTypeValue;
    }

    getSwitches(): FieldSwitch[] {
        return this.#switches;
    }

    getSwitch(type: string): Opt<FieldSwitch> {
        for (const fieldSwitch of this.#switches) {
            if (fieldSwitch.getType() === type) {
                return fieldSwitch;
            }
        }
        return undefined;
    }

    getDateAndTimeFormattingArgument(removeDoubleQuotes?: boolean): Opt<string> {
        return this.getSwitch("@")?.getArgument(removeDoubleQuotes) ?? undefined;
    }

    getNumericFormattingArgument(removeDoubleQuotes?: boolean): Opt<string> {
        return this.getSwitch("#")?.getArgument(removeDoubleQuotes) ?? undefined;
    }

    getGeneralFormattingNumericArgument(): Opt<GeneralNumeric> {
        return this.#getGeneralFormattingArgument(GeneralNumeric);
    }

    getGeneralFormattingStringArgument(): Opt<GeneralString> {
        return this.#getGeneralFormattingArgument(GeneralString);
    }

    getGeneralFormattingAnyArgument(): Opt<GeneralAny> {
        return this.#getGeneralFormattingArgument(GeneralAny);
    }

    getArrayOfTypeAndArguments(): string[] {
        // used only for tocs ... the toc should work on array of fieldSwitches and not on array of arguments
        const typeAndArguments: string[] = [];
        this.#switches.forEach(fieldSwitch => {
            let argument = fieldSwitch.getArgument();
            if (argument.length > 0) {
                argument = " " + argument;
            }
            typeAndArguments.push(fieldSwitch.getType() + argument);
        });
        return typeAndArguments;
    }

    #getGeneralFormattingArgument<T>(EnumType: T): Opt<T[keyof T]> {
        for (const fieldSwitch of this.#switches) {
            if (fieldSwitch.getType() === "*") {
                const argument = to.enum(EnumType, fieldSwitch.getArgument());
                if (argument !== undefined) {
                    return argument;
                }
            }
        }
        return undefined;
    }
}
