/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is, ary, dict, pick, jpromise, json } from "@/io.ox/office/tk/algorithms";
import { getLocalNowAsUTC } from "@/io.ox/office/tk/utils/dateutils";
import { type NodeOrJQuery, containsNode } from "@/io.ox/office/tk/dom";
import { setToolTip } from "@/io.ox/office/tk/forms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import type { JAbortablePromise } from "@/io.ox/office/tk/objects";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import type { FormatCategory } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import type { PtCharacterAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { NoOpOperation, Operation, Position } from "@/io.ox/office/editframework/utils/operations";

import ComplexField from "@/io.ox/office/textframework/components/field/complexfield";
import type { FieldType } from "@/io.ox/office/textframework/components/field/fieldtype";
import SimpleField from "@/io.ox/office/textframework/components/field/simplefield";
import type RangeMarker from "@/io.ox/office/textframework/components/rangemarker/rangemarker";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";
import type { NumberFormatter } from "@/io.ox/office/textframework/model/numberformatter";
import type Selection from "@/io.ox/office/textframework/selection/selection";
import type { SelectionUpdateEvent, SelectionPositionCalculatedFn, SelectionPositionCalculatedOptions } from "@/io.ox/office/textframework/selection/selection";

import { COMPLEXFIELDMEMBERNODE_CLASS, MARGINAL_NODE_CLASSNAME, createFieldNode, ensureExistingTextNode, getComplexFieldId,
    getComplexFieldInstruction, getComplexFieldMemberId, getFieldDateTimeFormat, getFieldInstruction, getFieldPageFormat,
    getRangeMarkerType, isAutomaticDateField, isChangeTrackNode, isComplexFieldNode, isFieldNode, isFixedSimpleField as isFixedSimpleFieldDOM,
    isInsideComplexFieldRange, isInsideHeaderFooterTemplateNode, isMarginalNode, isRangeMarkerStartNode, isSpecialField,
    splitTextSpan } from "@/io.ox/office/textframework/utils/dom";
import { COMPLEXFIELD_UPDATE, FIELD_UPDATE, NOOP } from "@/io.ox/office/textframework/utils/operations";
import { getDOMPosition, getOxoPosition, increaseLastIndex } from "@/io.ox/office/textframework/utils/position";
import type { FieldDatePopupEvent } from "@/io.ox/office/textframework/view/view";

// types ==================================================================

export interface FieldConfigFormatEntry {
    option: string;
    value: string;
}

export interface FieldConfigType {
    type?: FieldType;
    instruction?: string | string[];
    autoDate?: boolean;
    formats?: FieldConfigFormatEntry[];
}

export type CommonConfigKeys = "date" | "time";

export type OdfConfigKeys = CommonConfigKeys | "creator" | "file-name" | "page-number" | "page-count";

export type PptxConfigKeys = CommonConfigKeys | "author" | "filename" | "pagenumber" | "page" | "numpages";

// constants ==================================================================

const AUTHOR_FILENAME_ARRAY_PPTX: FieldConfigFormatEntry[] = [
    { option: "default", value: /*#. dropdown list option: original string value, no formatting applied */ gt("(no format)") },
    { option: "Lower", value: /*#. dropdown list option: all letters in string lowercase */ gt("lowercase") },
    { option: "Upper", value: /*#. dropdown list option: all letters in string uppercase */ gt("UPPERCASE") },
    { option: "FirstCap", value: /*#. dropdown list option: first letter uppercase only */ gt("First capital") },
    { option: "Caps", value: /*#. dropdown list option: all begining letters in words uppercase */ gt("Title Case") }
];

const PAGE_NUMBER_ARRAY_PPTX: FieldConfigFormatEntry[] = [
    { option: "default", value: /*#. dropdown list option: original string value */ gt("(no format)") },
    { option: "roman", value: "i, ii, iii, ..." },
    { option: "ROMAN", value: "I, II, III, ..." },
    { option: "alphabetic", value: "a, b, c, ..." },
    { option: "ALPHABETIC", value: "A, B, C, ..." },
    { option: "ArabicDash", value: "- 1 -, - 2 -, - 3 -, ..." }
];

const NUMPAGES_ARRAY_PPTX: FieldConfigFormatEntry[] = [
    { option: "default", value: /*#. dropdown list option: original string value, no formatting applied */ gt("(no format)") },
    { option: "roman", value: "i, ii, iii, ..." },
    { option: "ROMAN", value: "I, II, III, ..." },
    { option: "alphabetic", value: "a, b, c, ..." },
    { option: "ALPHABETIC", value: "A, B, C, ..." },
    { option: "Hex", value: /*#. dropdown list option: hexadecimal number formatting */ gt("hexadecimal") },
    { option: "Ordinal", value: "1., 2., 3., ..." }
];

const PAGE_NUMBER_ARRAY_ODF: FieldConfigFormatEntry[] = [
    { option: "default", value: /*#. dropdown list option: original string value, no formatting applied */ gt("(no format)") },
    { option: "i", value: "i, ii, iii, ..." },
    { option: "I", value: "I, II, III, ..." },
    { option: "a", value: "a, b, c, ..." },
    { option: "A", value: "A, B, C, ..." }
];

const PAGE_COUNT_ARRAY_ODF: FieldConfigFormatEntry[] = [
    { option: "default", value: /*#. dropdown list option: original string value, no formatting applied */ gt("(no format)") },
    { option: "i", value: "i, ii, iii, ..." },
    { option: "I", value: "I, II, III, ..." },
    { option: "a", value: "a, b, c, ..." },
    { option: "A", value: "A, B, C, ..." }
];

// class FieldManager =========================================================

/**
 * An instance of this class represents the dispatcher class
 * for all fields (simple and complex) in the edited document.
 *
 * @param {TextModel} docModel
 *  The model instance.
 */
export default class FieldManager extends ModelObject<TextBaseModel> {

    // simple field instance
    readonly #simpleField: SimpleField;
    // complex field instance
    readonly #complexField: ComplexField;
    // number formatter instance
    #numberFormatter!: NumberFormatter;
    // the selection object
    #selection!: Selection;
    // range marker object
    #rangeMarker!: RangeMarker;
    // flag to determine if cursor position is inside complex field
    #isCursorHighlighted = false;
    // saved id of currently highlighted complex field during cursor traversal or click
    #currentlyHighlightedId: string | null = null;
    // for simple fields we store node instance of highlighted field
    #currentlyHighlightedNode: JQuery | null = null;
    // field format popup promise
    #fieldFormatPopupTimer: Abortable | null = null;
    // holds value of type of highlighted field
    #highlightedFieldType: string | null = null;
    // holds value of format instruction of highlighted field
    #highlightedFieldInstruction: string | string[] | null = null;
    // structure that contains field formats for given field type
    readonly #localFormatList: Record<PptxConfigKeys, FieldConfigFormatEntry[]> | Record<OdfConfigKeys, FieldConfigFormatEntry[]>;
    // temporary collection of special complex field nodes, used during changetracking
    #tempSpecFieldCollection: JQuery[] = [];
    // complex field stack for id
    readonly #complexFieldStackId: string[] = [];

    constructor(docModel: TextBaseModel) {

        // base constructor
        super(docModel);

        this.#simpleField = this.member(new SimpleField(this.docModel));
        this.#complexField = this.member(new ComplexField(this.docModel));

        const localFormatListOdf: Record<OdfConfigKeys, FieldConfigFormatEntry[]> = {
            date: [],
            time: [],
            creator: [],
            "file-name": [],
            "page-number": PAGE_NUMBER_ARRAY_ODF,
            "page-count": PAGE_COUNT_ARRAY_ODF
        };

        const localFormatListPptx: Record<PptxConfigKeys, FieldConfigFormatEntry[]> = {
            date: [],
            time: [],
            author: AUTHOR_FILENAME_ARRAY_PPTX,
            filename: AUTHOR_FILENAME_ARRAY_PPTX,
            pagenumber: PAGE_NUMBER_ARRAY_PPTX,
            page: PAGE_NUMBER_ARRAY_PPTX,
            numpages: NUMPAGES_ARRAY_PPTX
        };

        this.#localFormatList = this.docApp.isODF() ? localFormatListOdf : localFormatListPptx;

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {

            const deferred1 = this.newJDeferred();
            const deferred2 = this.newJDeferred();

            this.#selection = this.docModel.getSelection();
            this.#rangeMarker = this.docModel.getRangeMarker();
            this.#numberFormatter = this.docModel.numberFormatter;

            this.listenTo(this.docApp, "docs:editmode:leave", () => {
                this.destroyFieldPopup();
            });

            this.waitForImportSuccess(() => {
                this.#complexField.refreshComplexFields(this.docModel.isFastLoadImport());
                this.#simpleField.refreshSimpleFields();
                // refreshing also the range marker model, if it was not done before
                this.#rangeMarker.refreshModel({ onlyOnce: true });
                // resolve import success for update fields callback
                deferred2.resolve();
                // setting the localFormatList
                this.#setDateTimeFormatList(this.#localFormatList, getLocalNowAsUTC());
            });

            this.waitForImportFailure(() => {
                // reject promise on import failed, so that update fields does not hang
                deferred1.reject();
                deferred2.reject();
            });

            this.listenTo(this.docApp, "docs:beforequit", () => {
                deferred1.reject();
                deferred2.reject();
            });

            // mark all complex fields contents after undo/redo
            this.listenTo(this.docModel.getUndoManager(), ["undo:after", "redo:after"], () => {
                this.#complexField.markAllFieldsAfterUndo();
                // also forcing update on all other remote clients (using noop)
                if (!this.#complexField.isEmpty()) {
                    const noopOperation: NoOpOperation = { name: NOOP, data: { updateComplexFields: true } };
                    this.docModel.applyOperations(noopOperation);
                }
            });

            // listening to a forced update of complex fields (sent via NOOP)
            this.listenTo(this.docModel, "operations:after", (operations, external): void => {
                // checking for a noop operation from another client
                if (external) {
                    _(operations).each(operation => {
                        if (operation.name === NOOP) {
                            if (pick.boolean((operation as NoOpOperation).data, "updateComplexFields")) {
                                this.#complexField.markAllFieldsAfterUndo(); // if this is not called, the complex field might no longer be recognized as complex field at remote client after undo (67074)
                            }
                        }
                    });
                }
            });

            this.docModel.one(["pageBreak:after", "initialPageBreaks:done"], () => { deferred1.resolve(); });
            // only when import is finished and page breaks are rendered call update fields
            jpromise.floating(Promise.all([deferred1, deferred2]).then(() => this.#updateFieldsAfterLoadCallback()));

            this.listenTo(this.#selection, "position:calculated", this.#checkRangeSelection);
            this.listenTo(this.#selection, "update", this.#checkHighlighting);

            this.listenTo(this.docModel, "document:reloaded", () => {
                this.#complexField.refreshComplexFields(false);
                this.#simpleField.refreshSimpleFields();
            });

            this.listenTo(this.docApp.docView, "fielddatepopup:change", this.#updateDateFromPopup);
            this.listenTo(this.docApp.docView, "fielddatepopup:autoupdate", state => jpromise.floating(this.#setDateFieldToAutoUpdate(state)));
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Updating field with given instruction.
     * Depending of passed instruction, different type of field is updated.
     *
     * @param node
     *  Complex field node
     *
     * @param instruction
     *  Complex field instruction, containing type,
     *  formating and optionally style, separated by \ .
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.complex=false]
     *      If field is simple or complex type.
     *
     * @returns
     *  A promise that will be resolved immediately in case of simple field,
     *  or when the promise from complex field update is resolved.
     */
    updateByInstruction(node: NodeOrJQuery, instruction: string, options?: { complex?: boolean }): JPromise {
        let promise = jpromise.resolve();

        if (instruction) {
            if (options?.complex) {
                promise = this.#complexField.updateByInstruction(node, instruction); // updateToc returns promise
            } else {
                this.#simpleField.updateByInstruction(node, instruction);
            }
        } else {
            globalLogger.warn("FieldManager.updateByInstruction(): missing instruction!");
        }
        return promise;
    }

    /**
     * Set highlighting state of complex field during cursor traversal.
     *
     * @param state
     *  If cursor is inside complex field or not.
     */
    setCursorHighlightState(state: boolean): void {
        this.#isCursorHighlighted = state;
    }

    /**
     * If complex field highlight state is active, or not.
     *
     * @returns
     *  Whether the complex field highlight state is active, or not.
     */
    isHighlightState(): boolean {
        return this.#isCursorHighlighted;
    }

    /**
     * Provides a jQuerified range start marker node.
     *
     * @param id
     *  The ID of the complex field.
     *
     * @returns
     *  The start marker with the specified id, or null, if no such start marker exists.
     */
    getComplexField(id: string): JQuery | null {
        return this.#complexField.getComplexField(id);
    }

    /**
     * Getter for target container id of marginal complex field node.
     *
     * @param id
     * Id of the field in collection model.
     *
     * @returns
     *  Target container id of marginal complex field node.
     */
    getMarginalComplexFieldsTarget(id: string): JQuery | null {
        return this.#complexField.getMarginalComplexFieldsTarget(id);
    }

    /**
     * Check if there are complex fields in document.
     *
     * @returns
     *  Whether there are complex fields in the document.
     */
    isComplexFieldEmpty(): boolean {
        return this.#complexField.isEmpty();
    }

    /**
     * Gets simple field from collection by passed id.
     *
     * @param id
     *  Id of queried field.
     *
     * @returns
     *  The simple field with the specified id, or null, if no such start marker exists.
     */
    getSimpleField(id: string): JQuery | null {
        return this.#simpleField.getSimpleField(id);
    }

    /**
     * Check if there are simple fields in document.
     *
     * @returns
     *  Whether there are simple fields in the document.
     */
    isSimpleFieldEmpty(): boolean {
        return this.#simpleField.isEmpty();
    }

    /**
     * Check if there are any type of fields in document.
     *
     * @returns
     *  Whether there is any type of fields in the document.
     */
    fieldsAreInDocument(): boolean {
        return !this.#simpleField.isEmpty() || !this.#complexField.isEmpty();
    }

    /**
     * Check, whether the specified target string is a valid target for a complex field.
     *
     * @param id
     *  The target string.
     *
     * @returns
     *  Whether the specified id string is a valid id for a complex field node.
     */
    isComplexFieldId(id: string): boolean {
        return this.getComplexField(id) !== null;
    }

    /**
     * Warning: This accessor is used only for unit testing! Please use DOMs public method!
     *
     * @param field
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @param isODF
     *  If document format is odf, or ooxml.
     *
     * @returns
     *  If the field has fixed date property or not
     */
    isFixedSimpleField(field: NodeOrJQuery, isODF: boolean): boolean {
        return isFixedSimpleFieldDOM(field, isODF);
    }

    /**
     * When pasting content, that contains complex fields, it is necessary, that the
     * complex fields get a new unique id.
     *
     * @param operations
     *  The collection with all paste operations.
     */
    handlePasteOperationIDs(operations: Operation[]): void {
        this.#complexField.handlePasteOperationIDs(operations);
    }

    /**
     * Callback function for mouseenter event trigger on complex field.
     *
     * @param event
     *  jQuery object event triggered on hover in.
     */
    createHoverHighlight(event: JTriggeredEvent): void {
        const id = getComplexFieldMemberId(event.currentTarget as HTMLElement); // id of the hovered field
        const node = this.getComplexField(id);
        const instruction = getComplexFieldInstruction(node);
        const isTOCfield = instruction && (/TOC/i).test(instruction);
        const tooltipText = gt("Table of Contents. Right click...");
        const isRotatedShape = (node?.length) ? node.parentsUntil(".page", ".rotated-drawing").length > 0 : false;

        if (!isRotatedShape && !isTOCfield && this.#currentlyHighlightedId !== id) { // no highlighting of toc fields, #50504
            this.#rangeMarker.highLightRange(id, "cx-field-highlight");
        }
        // set tooltip to table of contents field element, if hyperlink switch is set in instruction
        if (isTOCfield && (/\\h/i).test(instruction)) {
            setToolTip(event.currentTarget as HTMLElement, tooltipText);
        }

        // fallback with jquery/css highlighting
        // this.docModel.getCurrentRootNode().find(".complex" + id).addClass("cx-field-highlight");
    }

    /**
     * Callback function for mouseleave event trigger on complex field.
     *
     * @param event
     *  jQuery object event triggered on hover out.
     */
    removeHoverHighlight(event: JTriggeredEvent): void {
        // id of the hovered field
        const id = getComplexFieldMemberId(event.currentTarget as HTMLElement);

        if (this.#currentlyHighlightedId !== id) {
            this.#rangeMarker.removeHighLightRange(id);
        }

        // fallback with jquery/css highlighting
        // this.docModel.getCurrentRootNode().find(".complex" + id).removeClass("cx-field-highlight");
    }

    /**
     * Creates highlight range on enter cursor in complex field,
     * with keyboard key pressed or mouse click.
     *
     * @param field
     *  Field to be highlighted.
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.simpleField=false]
     *      If it is simple field or not.
     *  @param [options.mouseTouchEvent=false]
     *      If event is triggered by mouse or touch event.
     *  @param [options.rightClick=false]
     *      If its right click or not.
     */
    createEnterHighlight(field: NodeOrJQuery, options: { simpleField?: boolean; mouseTouchEvent?: boolean; rightClick?: boolean }): void {
        const id = getComplexFieldMemberId(field); // id of the highlighted field
        // let fieldinstruction = null;
        let complexFieldNode = null;
        let fieldConfig: FieldConfigType;

        if (options?.simpleField) {
            if (this.#currentlyHighlightedId || this.#currentlyHighlightedNode) {
                this.removeEnterHighlight();
            }
            this.#currentlyHighlightedNode = $(field);
            this.#currentlyHighlightedNode.addClass("sf-highlight");

            const fieldType = getFieldInstruction(field);
            if (fieldType && this.docApp.isODF() && (this.#simpleField.isCurrentDate(fieldType) || this.#simpleField.isCurrentTime(fieldType))) {
                fieldConfig = { type: fieldType as FieldType, instruction: getFieldDateTimeFormat(field) || "default" };
            } else if (fieldType && this.docApp.isODF() && this.#simpleField.isPageNumber(fieldType)) {
                fieldConfig = { type: fieldType as FieldType, instruction: getFieldPageFormat(field) || "default" };
            } else {
                fieldConfig = this.#simpleField.cleanUpAndExtractType(fieldType);
            }
            if (fieldConfig) { fieldConfig.autoDate = !isFixedSimpleFieldDOM(field, this.docApp.isODF()); }
        } else {
            complexFieldNode = this.getComplexField(id);
            if (this.#currentlyHighlightedId !== id) {
                if (this.#currentlyHighlightedId || this.#currentlyHighlightedNode) {
                    this.removeEnterHighlight();
                }
                this.#currentlyHighlightedId = id;

                // make no highlighting inside rotated shapes and rotated group shapes
                const allParents = complexFieldNode ? $(complexFieldNode).parentsUntil(".page", ".rotated-drawing") : null;
                if (allParents && !allParents.length) {
                    this.#rangeMarker.highLightRange(id, "cx-field-highlight");
                }
            }
            if (!complexFieldNode) {
                globalLogger.warn("fieldmanager.createEnterHighlight(): failed to fetch complex field node!");
                this.removeEnterHighlight();
                return;
            }
            const instruction = getComplexFieldInstruction(complexFieldNode);
            fieldConfig = this.#complexField.cleanUpAndExtractType(instruction);
            fieldConfig.autoDate = isAutomaticDateField(complexFieldNode);
        }

        if (fieldConfig?.type) {
            if (this.#complexField.isSaveDate(fieldConfig.type)) {
                const dateList: Record<CommonConfigKeys, FieldConfigFormatEntry[]> = {
                    date: [],
                    time: []
                };
                this.#setDateTimeFormatList(dateList, this.#complexField.getSaveDate());
                fieldConfig.formats = dateList.date;
                if (fieldConfig.instruction === "default") {
                    fieldConfig.instruction = `${LOCALE_DATA.shortDate} ${LOCALE_DATA.longTime}`;
                }
            } else {
                fieldConfig.formats = (this.#localFormatList as Dict<FieldConfigFormatEntry[]>)[fieldConfig.type.toLowerCase()];
            }
        }
        this.#highlightedFieldType = fieldConfig.type || null;
        this.#highlightedFieldInstruction = fieldConfig.instruction || null;

        this.setCursorHighlightState(true);
        const node = complexFieldNode ?? field;

        this.destroyFieldPopup();
        const isSupportedFormat = fieldConfig.formats?.length
            || (this.#highlightedFieldType && this.#complexField.isCurrentDate(this.#highlightedFieldType))
            || (this.#highlightedFieldType && this.#complexField.isCurrentTime(this.#highlightedFieldType));
        const isChangeTracked = isChangeTrackNode(field) || isChangeTrackNode($(field).parent());

        if (this.#highlightedFieldType && isSupportedFormat && options?.mouseTouchEvent && !options?.rightClick && !isChangeTracked) {
            this.#fieldFormatPopupTimer = this.setTimeout(() => {
                this.docApp.docView.showFieldFormatPopup(fieldConfig, node);
            }, 500);
        }

        // for debugging ->
        // if (!this.#highlightedFieldType) { globalLogger.error("no highlightedFieldType"); }
    }

    /**
     * Removes highlight range on enter cursor in complex field, or in simple field,
     * with keyboard key pressed or mouse click.
     */
    removeEnterHighlight(): void {
        if (this.#currentlyHighlightedNode) {
            // simple field
            this.#currentlyHighlightedNode.removeClass("sf-highlight");
            this.#currentlyHighlightedNode = null;
        } else {
            this.#rangeMarker.removeHighLightRange(this.#currentlyHighlightedId);
            this.#currentlyHighlightedId = null;
        }
        this.setCursorHighlightState(false);
        this.#highlightedFieldType = null;
        this.#highlightedFieldInstruction = null;

        this.destroyFieldPopup();
    }

    /**
     * Forces update of highlighted simple/complex field content.
     */
    updateHighlightedField(): JPromise {

        let promise: Opt<JPromise>;

        if (this.#currentlyHighlightedNode) { // simple field
            const $node = this.#currentlyHighlightedNode;
            const instruction = getFieldInstruction($node);
            jpromise.floating(this.updateByInstruction($node, instruction)); // TODO: ignore floating promise?

            // remove highlight from deleted node
            this.#currentlyHighlightedNode = null;
            this.setCursorHighlightState(false);

            // add highlight to newly created node
            const domPos = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
            if (domPos?.node) {
                let node: Node | null = domPos.node;
                let atEndOfSpan = false;
                if (node.nodeType === 3) {
                    atEndOfSpan = domPos.offset === (node.textContent?.length || 0);
                    node = node.parentNode;
                }
                if (atEndOfSpan) {
                    const nextSibling = node ? node.nextSibling as HTMLElement : null;
                    if (nextSibling && isFieldNode(nextSibling)) {
                        this.createEnterHighlight(nextSibling, { simpleField: true });
                    }
                }
            }
        } else if (this.#currentlyHighlightedId) { // complex field
            const node = this.getComplexField(this.#currentlyHighlightedId);
            const instruction = getComplexFieldInstruction(node);
            promise = this.updateByInstruction(node!, instruction, { complex: true });
        }

        return promise ?? jpromise.resolve();
    }

    /**
     * Forces update of complex fields inside selection range.
     */
    updateHighlightedFieldSelection(): void {
        let instruction = null;
        if (this.#simpleField.isEmpty() && this.#complexField.isEmpty()) { return; }

        this.#selection.iterateNodes(node => {
            if (isComplexFieldNode(node)) {
                instruction = getComplexFieldInstruction(node);
                jpromise.floating(this.updateByInstruction(node, instruction, { complex: true }));
            } else if (isFieldNode(node)) {
                instruction = getFieldInstruction(node);
                jpromise.floating(this.updateByInstruction(node, instruction));
            }
        });
    }

    /**
     * Update all fields in document.
     */
    updateAllFields(): JPromise | void {

        // helper iterator function for handling field update
        const handleFieldUpdate = (fieldId: string): JPromise | void => {

            const isSimpleField = fieldId?.includes("s");

            let field = isSimpleField ? this.getSimpleField(fieldId) : this.getComplexField(fieldId);
            const isMarginalField = isMarginalNode(field);

            if (!field) {
                // this usually happens with nested fields, that are beeing deleted durring update of base level field,
                // where update was triggered from this method. Iterator still has references to this dynamically deleted fields,
                // but they are removed from document and model.
                // Not expected to break anything, therefore log level. For debugging, change to globalLogger.error
                globalLogger.log("fieldmanager.updateAllFields(): field with id: " + fieldId + " is not in the document anymore ");
                return;
            }
            if (!isMarginalField || !isInsideHeaderFooterTemplateNode(this.docModel.getNode(), field)) {
                const instruction = isSimpleField ? getFieldInstruction(field) : getComplexFieldInstruction(field);
                if (!isSpecialField(field)) {
                    // #42671 and #45752
                    this.#selection.switchToValidRootNode(field);

                    // for marginal fields an update might be required, if they are removed from DOM after "switchToValidRootNode"
                    if (isMarginalField && !containsNode(this.docModel.getCurrentRootNode(), field)) { field = isSimpleField ? this.getSimpleField(fieldId) : this.getComplexField(fieldId); }

                    const options = isSimpleField ? undefined : { complex: true };
                    return this.updateByInstruction(field!, instruction, options);
                }
            }
            return jpromise.resolve();
        };

        if (this.fieldsAreInDocument()) {
            let generateDef: JAbortablePromise;
            const mergedFields = { ...this.#simpleField.getAllFields(), ...this.#complexField.getAllFields() };

            // show a message with cancel button
            this.docApp.docView.enterBusy({
                cancelHandler: () => {
                    if (generateDef?.abort) {
                        generateDef.abort();
                    }
                },
                warningLabel: gt("Updating all fields in document will take some time.")
            });

            return this.docModel.getUndoManager().enterUndoGroup(() => {
                // iterate objects
                generateDef = this.iterateSliced(dict.keys(mergedFields), handleFieldUpdate);
                generateDef.progress(partialProgress => {
                    // add progress handling
                    const progress = 0.2 + (partialProgress * 0.8);
                    this.docApp.docView.updateBusyProgress(progress);
                });
                generateDef.always(() => {
                    // leave busy state
                    this.docApp.docView.leaveBusy();
                    this.docApp.docView.grabFocus();
                });

                return generateDef;
            });
        }
    }

    /**
     * This method dispatch parent node to simple and complex field, for removing it.
     *
     * @param parentNode
     *  Element that is being searched for simple and complex fields.
     */
    removeAllFieldsInNode(parentNode: NodeOrJQuery): void {
        // checking, if the paragraph contains additional complex field nodes, that need to be removed, too.
        this.#complexField.removeAllInsertedComplexFields(parentNode);
        // checking, if the paragraph contains additional simple field nodes, that need to be removed, too.
        this.#simpleField.removeSimpleFieldsInNode(parentNode);
    }

    /**
     * Dispatch function to complexField class to check if search node in given start and end range
     * contains special fields that needs to be restored, like before delete operation.
     *
     * @param searchNode
     *  Node which we search for special fields.
     *
     * @param startOffset
     *  Start offset of given node.
     *
     * @param endOffset
     *  End offset of given node.
     */
    checkRestoringSpecialFields(searchNode: NodeOrJQuery, startOffset: number, endOffset: number): void {
        if (is.number(startOffset) && is.number(endOffset)) {
            this.#complexField.checkRestoringSpecialFields(searchNode, startOffset, endOffset);
        } else {
            this.#complexField.checkRestoringSpecialFieldsInContainer(searchNode); // if startOffset or endOffset are undefined, we search whole node. See #42235
        }
    }

    /**
     * Dispatch function to complexField class to restore special field to normal before delete operation.
     *
     * @param node
     *  Node which we convert.
     */
    restoreSpecialField(node: NodeOrJQuery): void {
        this.#complexField.restoreSpecialField(node);
    }

    /**
     * Dispatch call to complex field to handle restoring of special fields in container node.
     *
     * @param containerNode
     *  Node which we search for special fields.
     */
    checkRestoringSpecialFieldsInContainer(containerNode: NodeOrJQuery): void {
        this.#complexField.checkRestoringSpecialFieldsInContainer(containerNode);
    }

    /**
     * Dispatch function to simpleField class method removeSimpleFieldFromCollection.
     *
     * @param node
     *  One simple field node.
     */
    removeSimpleFieldFromCollection(node: NodeOrJQuery): void {
        this.#simpleField.removeSimpleFieldFromCollection(node);
    }

    /**
     * Dispatch function to complexField class method removeFromComplexFieldCollection.
     *
     * @param node
     *  One complex field node.
     */
    removeFromComplexFieldCollection(node: NodeOrJQuery): void {
        // if there is open field popup, close it and its promise before removing field, #42090
        this.destroyFieldPopup();
        this.#complexField.removeFromComplexFieldCollection(node);
    }

    /**
     * Dispatch to simpleField for adding node to collection.
     *
     * @param fieldNode
     *  One simple field node.
     *
     * @param [target]
     *  The target, where the simple field is located.
     */
    addSimpleFieldToCollection(fieldNode: NodeOrJQuery, target: string): void {
        this.#simpleField.addSimpleFieldToCollection(fieldNode, target);
    }

    /**
     * After splitting a paragraph, it is necessary, that all complex field nodes in the cloned
     * "new" paragraph are updated in the collectors.
     * This method dispatches to complex field class.
     *
     * @param paragraph
     *  The paragraph node.
     */
    updateComplexFieldCollector(paragraph: NodeOrJQuery): void {
        this.#complexField.updateComplexFieldCollector(paragraph);
    }

    /**
     * After splitting a paragraph, it is necessary, that all complex field nodes in the cloned
     * "new" paragraph are updated in the collectors.
     * This method dispatches to complex field class.
     *
     * @param paragraph
     *  The paragraph node.
     */
    updateSimpleFieldCollector(paragraph: NodeOrJQuery): void {
        this.#simpleField.updateSimpleFieldCollector(paragraph);
    }

    /**
     * Dispatch to complex fields method updateCxPageFieldsInMarginals.
     */
    updatePageFieldsInMarginals(): void {
        this.#complexField.updateCxPageFieldsInMarginals();
    }

    /**
     * Inserts a simple text field component into the document DOM.
     *
     * @param start
     *  The logical start position for the new text field.
     *
     * @param type
     *  A property describing the field type.
     *
     * @param representation
     *  A fallback value, if the placeholder cannot be substituted with a
     *  reasonable value.
     *
     * @param [attrs]
     *  Attributes to be applied at the new text field, as map of attribute
     *  maps (name/value pairs), keyed by attribute family.
     *
     * @returns
     *  Whether the text field has been inserted successfully.
     */
    implInsertField(start: Position, type: string, representation: string, attrs: PtCharacterAttributeSet, target: string): boolean {

        // text span that will precede the field
        const span = this.docModel.prepareTextSpanForInsertion(start, {}, target);
        // format of the field
        let format;

        if (!span) { return false; }

        // expanding attrs to disable change tracking, if not explicitely set
        attrs = this.docModel.checkChangesMode(attrs);

        // split the text span again to get initial character formatting
        // for the field, and insert the field representation text

        // Fix for 29265: Removing empty text node in span with ".contents().remove().end()".
        // Otherwise there are two text nodes in span after ".text(representation)" in IE.

        // new text span for the field node
        const fieldSpan = splitTextSpan(span, 0).contents().remove().end().text(representation);
        if (!representation) { ensureExistingTextNode(fieldSpan as JQuery); }

        // insert a new text field before the addressed text node, move
        // the field span element into the field node
        const fieldNode = createFieldNode();
        fieldNode.append(fieldSpan).insertAfter(span);
        fieldNode.data("type", type);

        // odf - field types needed to be updated in frontend
        if (type === "page-number" || type === "page-count") {
            fieldNode.addClass("field-" + type);
            if (attrs?.character?.field?.pageNumFormat) {
                format = attrs.character.field.pageNumFormat;
            }
            if (type === "page-count") {
                this.docModel.getPageLayout().updatePageCountField(fieldNode, format);
            } else {
                this.docModel.getPageLayout().updatePageNumberField(fieldNode, format);
            }
        }

        // microsoft - field types needed to be updated in frontend
        if ((/NUMPAGES/i).test(type)) {
            fieldNode.addClass("field-NUMPAGES");
            this.docModel.getPageLayout().updatePageCountField(fieldNode);
        } else if ((/PAGENUM/i).test(type)) {
            fieldNode.addClass("field-page-number");
            this.docModel.getPageLayout().updatePageNumberField(fieldNode);
        }

        if (!representation) { fieldNode.addClass("empty-field"); }

        // adding inherited character attributes, before applying the passed attributes
        this.docModel.inheritInlineCharacterAttributes(fieldNode);

        // apply the passed field attributes
        if (is.dict(attrs)) {
            if (is.dict(attrs.character) && attrs.character.field) {
                dict.forEach(attrs.character.field, (element, name) => {
                    fieldNode.data(name, element!);
                    if (name === "name") { fieldNode.addClass("user-field"); }
                });
            }
            this.docModel.characterStyles.setElementAttributes(fieldSpan as JQuery, attrs);
        }

        // it might be necessary to transfer the hard character attributes to a previous empty text span (DOCS-3244)
        if (attrs?.character) { this.docModel.checkTransferOfInlineCharacterAttributes(fieldNode, attrs.character); }

        this.addSimpleFieldToCollection(fieldNode, target);
        if (this.docModel.getPageLayout().isIdOfMarginalNode(target)) {
            fieldNode.addClass(MARGINAL_NODE_CLASSNAME); // #54118
        }

        // validate paragraph, store new cursor position
        this.docModel.implParagraphChanged(span.parentNode as HTMLElement);
        this.docModel.setLastOperationEnd(increaseLastIndex(start));
        return true;
    }

    /**
     * Dispatches to handler for insertComplexField operations.
     *
     * @param start
     *  The logical start position for the new complex field.
     *
     * @param instruction
     *  The instructions of the complex field.
     *
     * @param attrs
     *  The attributes of the complex field.
     *
     * @param target
     *  The target string corresponding to the specified start position.
     *
     * @returns
     *  Whether the complex field has been inserted successfully.
     */
    insertComplexFieldHandler(start: Position, instruction: string, attrs: PtCharacterAttributeSet, target: string): boolean {
        return this.#complexField.insertComplexFieldHandler(start, instruction, attrs, target);
    }

    /**
     * Dispatches to handler for updateComplexField operations.
     *
     * @param start
     *  The logical start position for the new complex field.
     *
     * @param instruction
     *  The instructions of the complex field.
     *
     * @param attrs
     * The attributes of the complex field.
     *
     * @param target
     *  The target string corresponding to the specified start position.
     *
     * @param external
     *  Whether this is an external operation.
     *
     * @returns
     *  Whether the complex field has been inserted successfully.
     */
    updateComplexFieldHandler(start: Position, instruction: string, attrs: PtCharacterAttributeSet, target: string, external: boolean): boolean {
        return this.#complexField.updateComplexFieldHandler(start, instruction, attrs, target, external);
    }

    /**
     * Dispatches to handler for updateField operations.
     *
     * @param start
     *  The logical start position for the new simple field.
     *
     * @param type
     *  Type of the simple field.
     *
     * @param representation
     *  Content of the field that is updated.
     *
     * @param attrs
     *  The partial set of character attributes.
     *
     * @param target
     *  The target corresponding to the specified logical start position.
     *
     * @param external
     *  Whether this is an external operation.
     *
     * @returns
     *  Whether the simple field has been inserted successfully.
     */
    updateSimpleFieldHandler(start: Position, type: string, representation: string, attrs: PtCharacterAttributeSet, target: string, external: boolean): boolean {
        return this.#simpleField.updateSimpleFieldHandler(start, type, representation, attrs, target, external);
    }

    /**
     * Dispatch to class complexField, to convert field to special field.
     *
     * @param fieldId
     *  Id of the field
     *
     * @param marginalTarget
     *  Id of the marginal target node
     *
     * @param type
     *  Type of field: num pages or page number
     */
    convertToSpecialField(fieldId: string, marginalTarget: string, type: string): void {
        this.#complexField.convertToSpecialField(fieldId, marginalTarget, type);
    }

    /**
     * Dispatch update of date time fields on download to field classes.
     */
    prepareDateTimeFieldsDownload(): void {

        // do nothing in read-only mode
        if (!this.docApp.isEditable()) { return; }

        // helper iterator function for handling field update
        const handleFieldUpdate = (fieldId: string): void => {

            this.destroyFieldPopup();

            if (fieldId?.includes("s")) { // simple field
                let field = this.getSimpleField(fieldId);
                const isMarginalField = isMarginalNode(field);
                if (field && !isFixedSimpleFieldDOM(field, this.docApp.isODF())) {
                    const instruction = getFieldInstruction(field);
                    this.#selection.switchToValidRootNode(field);
                    // for marginal fields an update might be required, if they are removed from DOM after "switchToValidRootNode"
                    if (isMarginalField && !containsNode(this.docModel.getCurrentRootNode(), field)) { field = this.getSimpleField(fieldId); }
                    this.#simpleField.updateDateTimeField(field!, instruction);
                }
            } else { // complex field
                let field = this.getComplexField(fieldId);
                const isMarginalField = isMarginalNode(field);
                if (field && isAutomaticDateField(field)) {
                    const instruction = getComplexFieldInstruction(field);
                    this.#selection.switchToValidRootNode(field);
                    // for marginal fields an update might be required, if they are removed from DOM after "switchToValidRootNode"
                    if (isMarginalField && !containsNode(this.docModel.getCurrentRootNode(), field)) { field = this.getComplexField(fieldId); }
                    this.#complexField.updateDateTimeFieldOnDownload(field!, instruction);
                }
            }
        };

        const mergedFields = { ...this.#simpleField.getAllFields(), ...this.#complexField.getAllFields() };
        dict.forEachKey(mergedFields, handleFieldUpdate);
    }

    /**
     * Calls method in simpleField class, for updating number of pages field(s) in given node.
     *
     * @param $node
     *  The node for the current page count.
     *
     * @param pageCount
     *  Number of total pages in document.
     */
    updatePageCountInCurrentNode($node: JQuery, pageCount: number): void {
        this.#simpleField.updatePageCountInCurrentNode($node, pageCount);
    }

    /**
     * Calls method in simpleField class, for updating current page number field(s) in given node.
     *
     * @param $node
     *  The node for the current page count.
     *
     * @param isHeader
     *  If node is header or not.
     *
     * @param pageCount
     *  Number of total pages in document.
     */
    updatePageNumInCurrentNode($node: JQuery, isHeader: boolean, pageCount: number): void {
        this.#simpleField.updatePageNumInCurrentNode($node, isHeader, pageCount);
    }

    /**
     * Calls method in complexField class, for updating number of pages field(s) in given node.
     *
     * @param $node
     *  The node for the current page count.
     *
     * @param pageCount
     *  Number of total pages in document.
     */
    updatePageCountInCurrentNodeCx($node: JQuery, pageCount: number): void {
        this.#complexField.updatePageCountInCurrentNodeCx($node, pageCount);
    }

    /**
     * Calls method in complexField class, for updating current page number field(s) in given node.
     *
     * @param $node
     *  The node for the current page number.
     *
     * @param isHeader
     *  If node is header or not.
     *
     * @param pageCount
     *  Number of total pages in document.
     */
    updatePageNumInCurrentNodeCx($node: JQuery, isHeader: boolean, pageCount: number): void {
        this.#complexField.updatePageNumInCurrentNodeCx($node, isHeader, pageCount);
    }

    /**
     * Calls method in complexField class, for updating current page number field(s) in given marginal node.
     *
     * @param $node
     *  The node for the current page number.
     *
     * @param isHeader
     *  If node is header or not.
     *
     * @param pageNum
     *  Number of total pages in document.
     */
    updatePageNumInCurrentMarginalCx($node: JQuery, isHeader: boolean, pageNum: number): void {
        this.#complexField.updatePageNumInCurrentMarginalCx($node, isHeader, pageNum);
    }

    /**
     * If document is odf format, insert simple fields, otherwise complex fields.
     *
     * @param fieldType
     *  The field type.
     *
     * @param fieldFormat
     *  The field format.
     */
    dispatchInsertField(fieldType: string, fieldFormat: string): void {
        if (this.docApp.isODF()) {
            jpromise.floating(this.#simpleField.insertField(fieldType, fieldFormat));
        } else {
            jpromise.floating(this.#complexField.insertComplexField(fieldType, fieldFormat));
        }
    }

    /**
     * Triggered by user from dropdown, inserts table of contents as complex field.
     *
     * @param value
     *  Value of the type of toc picked by user from dropdown menu.
     *
     * @returns
     *  A promise that will be resolved if the insertion of table of contents
     *  ended. It is rejected, if the dialog has been canceled.
     */
    dispatchInsertToc(value: number): JPromise {
        const tabStyle = value === 2 ? "none" : "dot";
        const fieldFormat = value === 3 ? "TOC \\o '1-3' \\n \\h \\u" : "TOC \\o '1-3' \\h \\u";
        return this.#complexField.insertTOCField(fieldFormat, { tabStyle });
    }

    /**
     * Public accessor method to dispatch update of passed complex field node and instruction.
     *
     * @param node
     *  Complex field node
     *
     * @param instruction
     *  Format of the field with switches. Can be one string, or array of strings.
     *
     * @returns
     *  A promise that will be resolved if the insertion of table of contents
     *  ended. It is rejected, if the dialog has been canceled.
     */
    dispatchUpdateTOC(node: NodeOrJQuery, instruction: string | string[]): JPromise {
        return this.#complexField.updateTOCField(node, instruction);
    }

    /**
     * Clicking on remove button in popup, removes highlighted simple or complex field.
     */
    removeField(): JPromise | void {

        let node: Opt<JQuery> | null;
        const rootNode = this.#selection.getRootNode();

        // hide popup
        this.destroyFieldPopup();

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
            const startPos = getOxoPosition(rootNode, node);

            if (startPos) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.docModel.deleteRange(startPos);
                });
            }

        } else if (this.#currentlyHighlightedId) { // complex field
            const startMarker = this.#rangeMarker.getStartMarker(this.#currentlyHighlightedId);
            const endMarker = this.#rangeMarker.getEndMarker(this.#currentlyHighlightedId);
            const startPos = startMarker && getOxoPosition(rootNode, startMarker);
            const endPos = endMarker && getOxoPosition(rootNode, endMarker);

            if (startPos && endPos) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);

                    if (this.docModel.isHeaderFooterEditState()) {
                        if (this.#currentlyHighlightedId) { node = this.#complexField.getComplexField(this.#currentlyHighlightedId); }
                        if (node && isSpecialField(node)) {
                            this.#complexField.restoreSpecialField(node);
                            this.#selection.setTextSelection(startPos, endPos);
                        }
                    }

                    jpromise.floating(this.docModel.deleteSelected());
                });
            }
        }
    }

    /**
     * Clicking on field format from popup will update field with new formatting.
     *
     * @param format
     *  The format string.
     */
    updateFieldFormatting(format: string): JPromise | void {

        const rootNode = this.#selection.getRootNode();
        const fieldType = this.#highlightedFieldType; // store values before closing popup and deleting field
        const highlightedNode = this.#currentlyHighlightedNode;
        const highlightedId = this.#currentlyHighlightedId;
        let isSpecialFieldLocal = false;

        // hide popup
        this.destroyFieldPopup();

        if (!fieldType) {
            globalLogger.warn("fieldmanager.updateFieldFormatting(): missing field type!");
            return;
        }

        if (highlightedNode) { // simple field
            const node = highlightedNode;
            const startPos = getOxoPosition(rootNode, node);
            const isFixedDate = isFixedSimpleFieldDOM(node, this.docApp.isODF());

            if (startPos) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.docModel.deleteRange(startPos);
                    jpromise.floating(this.#simpleField.insertField(fieldType, format, { isFixed: isFixedDate }));
                });
            }

        } else if (highlightedId) { // complex field
            const complexFieldNode = this.#complexField.getComplexField(highlightedId);
            const rangeEndMarkerNode = this.#rangeMarker.getEndMarker(highlightedId);
            if (!complexFieldNode) {
                globalLogger.error("fieldManager.updateFieldFormatting(): missing field node for the id: ", highlightedId);
                return;
            }
            if (!rangeEndMarkerNode) {
                globalLogger.error("fieldManager.updateFieldFormatting(): missing range end node for field id: ", highlightedId);
                return;
            }
            const fieldNodePos = getOxoPosition(rootNode, complexFieldNode);
            const startPos = fieldNodePos ? increaseLastIndex(fieldNodePos) : null;
            const endPos = getOxoPosition(rootNode, rangeEndMarkerNode);
            let node = this.#complexField.getComplexField(highlightedId);
            if (node?.length && isSpecialField(node)) {
                isSpecialFieldLocal = true;
            }

            if (startPos && endPos) {
                return this.docModel.getUndoManager().enterUndoGroup((): JPromise | void => {
                    this.#selection.setTextSelection(startPos, endPos);

                    if (this.docModel.isHeaderFooterEditState()) {
                        node = this.#complexField.getComplexField(highlightedId);
                        if (isSpecialFieldLocal) {
                            this.#complexField.restoreSpecialField(node!);
                            this.#selection.setTextSelection(startPos, endPos);
                        }
                    }
                    const oldInstruction = getComplexFieldInstruction(complexFieldNode);
                    if (this.docModel.getChangeTrack().isActiveChangeTracking()) {
                        jpromise.floating(this.#complexField.updateComplexFieldFormat(fieldType, format, fieldNodePos!, highlightedId, oldInstruction));
                        if (isSpecialFieldLocal) {
                            this.#complexField.convertToSpecialField(highlightedId, this.#selection.getRootNodeTarget(), fieldType);
                        }
                        return jpromise.resolve();
                    }

                    const deletePromise = this.docModel.deleteSelected();
                    deletePromise.done((): void => {
                        jpromise.floating(this.#complexField.updateComplexFieldFormat(fieldType, format, fieldNodePos!, highlightedId, oldInstruction));
                        if (isSpecialFieldLocal) {
                            this.#complexField.convertToSpecialField(highlightedId, this.#selection.getRootNodeTarget(), fieldType);
                        }
                    });

                    return deletePromise;
                });
            }
        }
    }

    /**
     * Returns highlighted field type if is explicitly setr.
     *
     * @returns
     *  Highlighted field type.
     */
    getSelectedFieldType(): string {
        return this.#highlightedFieldType || "";
    }

    /**
     * Returns highlighted field format, or default one.
     *
     * @returns
     *  Highlighted field format.
     */
    getSelectedFieldFormat(): string | string[] {
        return this.#highlightedFieldInstruction || "default";
    }

    /**
     * Gets the anchor (start) node of the given field.
     *
     * @returns
     *  The anchor node of the given field, otherwise null if its not found.
     */
    getSelectedFieldNode(): JQuery | null {
        let node = null;

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
        } else if (this.#currentlyHighlightedId) { // complex field
            node = this.#complexField.getComplexField(this.#currentlyHighlightedId);
        }
        return node;
    }

    /**
     * Method to open field format popup from context menu.
     */
    editFieldFromContextMenu(): void {
        const node = this.getSelectedFieldNode();
        if (node) { this.createEnterHighlight(node, { mouseTouchEvent: true, simpleField: !isComplexFieldNode(node) }); }
    }

    /**
     * During delete of complex field, take care
     * that following span (or other) elements do not keep the coresponding class name.
     *
     * @param node
     *  Node that is beeing deleted, and from which we fetch field id.
     */
    cleanUpClassWhenDeleteCx(node: NodeOrJQuery): void {
        const cxFieldContentSelector = "complex" + getComplexFieldId(node);
        const rootNode = this.#selection.getRootNode();
        $(rootNode).find("." + cxFieldContentSelector).removeClass(cxFieldContentSelector + " " + COMPLEXFIELDMEMBERNODE_CLASS).removeData("fieldId");
    }

    /**
     * On loosing edit right, close possible open field popup, and abort promise.
     */
    destroyFieldPopup(): void {
        this.docApp.docView.hideFieldFormatPopup();
        this.#fieldFormatPopupTimer?.abort();
        this.#fieldFormatPopupTimer = null;
    }

    /**
     * Add special field that is changetracked to temporary collection.
     *
     * @param trackingNode
     *  The node saved in the changetracked temporary collection.
     */
    addToTempSpecCollection(trackingNode: JQuery): void {
        this.#tempSpecFieldCollection.push(trackingNode);
    }

    /**
     * Return array of collected special field nodes.
     */
    getSpecFieldsFromTempCollection(): JQuery[] {
        return this.#tempSpecFieldCollection;
    }

    /**
     * Empty temporary collection after change track resolve is finished.
     */
    emptyTempSpecCollection(): void {
        this.#tempSpecFieldCollection = [];
    }

    /**
     * Converts number for page and numPages fields into proper format code.
     *
     * @param pageCount
     *  Number to be inserted into field.
     *
     * @param format
     *  Format of the number inserted into field.
     *
     * @param [isNumPages]
     *  NumPages fields have some formats different from PageNum.
     *
     * @returns
     *  Formatted number.
     */
    formatFieldNumber(pageCount: number, format: string, isNumPages?: boolean): string {
        return this.#simpleField.formatPageFieldInstruction(pageCount, format, isNumPages);
    }

    /**
     * Public function to mark complex field for highlighting, after inserting into dom,
     * especially after inserting range end marker.
     *
     * @param id
     *  Id of the field to be highlighted.
     *
     * @param [fieldNode]
     *  Optional parameter for field node, if we already have it
     */
    markFieldForHighlighting(id: string, fieldNode?: NodeOrJQuery): void {
        const node = fieldNode ?? this.#complexField.getComplexField(id);
        if (node) { this.#complexField.markComplexFieldForHighlighting(node, id); }
    }

    /**
     * Warning: Public accessor used only for unit tests! Not meant to be called directly.
     *
     * @param state
     *  State of the field, fixed or updated automatically
     */
    setDateFieldToAutoUpdate(state: boolean): void {
        jpromise.floating(this.#setDateFieldToAutoUpdate(state));
    }

    /**
     * Public method to refresh collection of simple fields.
     */
    refreshSimpleFieldsCollection(): void {
        this.#simpleField.refreshSimpleFields();
    }

    /**
     * Checks if highlighted field is supported type by OX Text.
     * Function is defined twice. This version was introduced with DOCS-3495.
     *
     * @returns
     *  Whether the highlighted field is supported type by OX Text
     */
    isSupportedFieldType(): boolean {
        return !!(this.#highlightedFieldType
            && (this.#complexField.isNumPages(this.#highlightedFieldType)
                || this.#complexField.isPageNumber(this.#highlightedFieldType)
                || this.#complexField.isCurrentTime(this.#highlightedFieldType)
                || this.#complexField.isCurrentDate(this.#highlightedFieldType)
                || this.#complexField.isTableOfContents(this.#highlightedFieldType)
                || this.#complexField.isFileName(this.#highlightedFieldType)
                || this.#complexField.isAuthor(this.#highlightedFieldType)
                || this.#complexField.isSaveDate(this.#highlightedFieldType)));
    }

    /**
     * Checks if highlighted field is supported type by OX Text.
     *
     * @returns {Boolean}
     */
    // isSupportedFieldType() {
    //     const supportedTypes = [/NUMPAGES/i, /PAGE/i, /^DATE/i, /^TIME/i, /TOC/, /FILE-?NAME/i, /^AUTHOR$/i, /^author-name$/, /^creator$/];
    //     let result = false;

    //     if (this.#highlightedFieldType && !(/PAGEREF/i).test(this.#highlightedFieldType)) {
    //         _.each(supportedTypes, type => {
    //             if (type.test(this.#highlightedFieldType)) {
    //                 result = true;
    //             }
    //         });
    //     }

    //     return result;
    // }

    /**
     * Checks if highlighted field is Table of contents (TOC) field type.
     *
     * @returns
     *  Whether the highlighted field is Table of contents (TOC) field type.
     */
    isTableOfContentsHighlighted(): boolean {
        return !!(this.#highlightedFieldType && (/TOC/i).test(this.#highlightedFieldType));
    }

    /**
     * Check if currently highlighted field does not support format change.
     * Usually ODF autor and document name fields do not support this feature.
     * Info: when we extend list of supported fields, this regex needs to be expanded!
     *
     * @returns
     *  Whether the currently highlighted field does not support format change.
     */
    supportsFieldFormatting(): boolean {
        const unsupportedRegex = /(^author-name$)|(^creator$)|(^file-name$)|(^TOC$)/;
        return !!(this.#highlightedFieldType && !unsupportedRegex.test(this.#highlightedFieldType));
    }

    /**
     * Used to stack locally id for field during creation of complex field and preceding range start and following range end.
     *
     * @param rangeStart
     *  If is requested from insert range start.
     *
     * @param rangeEnd
     *  If is requested from insert range end.
     *
     * @returns result
     *  Id of the field from the stack.
     */
    getComplexFieldIdFromStack(rangeStart: boolean, rangeEnd: boolean): string {

        let result: Opt<string>;

        if (rangeStart) {
            const localId = this.#complexField.getNextComplexFieldID();
            this.#complexFieldStackId.push(localId);
            result = localId;
        } else if (rangeEnd) {
            result = this.#complexFieldStackId.pop();
        } else {
            result = ary.last(this.#complexFieldStackId);
        }

        return result || "";
    }

    /**
     * Convenience method to get number of all fields present in the document.
     * If document is ODF, it will get count of simple fields,
     * otherwise count of complex fields from coresponding models.
     *
     * @returns
     *  Number of all fields present in the document.
     */
    getCountOfAllFields(): number {
        return this.#simpleField.getAllFieldsCount() + this.#complexField.getAllFieldsCount();
    }

    // private methods ----------------------------------------------------

    /**
     * Private function called after importing document.
     * It loops all fields in collection and:
     *  - marks them for highlighting,
     *  - updates date and time fields
     *  - converts page fields in header&footer to special fields
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.avoidFieldUpdate=false]
     *      If set to true, the fields must not be updated. This is required
     *      after load of document, if other users have the document already
     *      opened.
     *  @param [options.setNonModifyingMarker=false]
     *      If set to true, the generated operations get a marker "modifying: false"
     *      that the backend knows, that no new file version must be generated.
     *      This is required after loading a document and the fields are
     *      updated automatically (DOCS-2144).
     */
    #updateDateTimeAndSpecialFields(options?: { avoidFieldUpdate?: boolean; setNonModifyingMarker?: boolean }): void {
        this.#complexField.updateDateTimeAndSpecialFields(options);
        this.#simpleField.updateDateTimeFieldOnLoad(options);
    }

    /**
     * Receiving the start and end range markers for complex fields. Using the
     * current selection, this needs to be expanded, so that a complex field
     * is always inserted completely.
     * If the current selection is a range, it might be necessary to modify only
     * the start position or only the end position or both. If the range is inside
     * one complex field, it is not expanded. The aim of this expansion is, that,
     * if the selection includes a start range marker, the end range marker must
     * also be part of the selection. And vice versa.
     * There is another type of expansion for complex fields of type placeholder.
     * In this case, the selection shall always contain the complete placeholder.
     * Therefore the expansion also happens, if the selection has no range. This
     * check needs to be enabled because of performance reasons by the option
     * "handlePlaceHolderComplexField".
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.handlePlaceHolderComplexField=false]
     *      If set to true, it is not necessary, that the current selection
     *      has a range, that will be expanded. For complex fields of type
     *      "placeholder" also a cursor selection is automatically expanded.
     *
     * @returns
     *  An object containing the properties "start" and "end", that contain
     *  as values the range marker start node and the range marker end node.
     *  If they can be determined, they are already jQuerified. If not, this
     *  values can be null. If range markers cannot be determined, null is
     *  returned instead of an object.
     */
    #getRangeMarkersFromSelection(options: SelectionPositionCalculatedOptions): { start: JQuery | null; end: JQuery | null } | null {

        // the range marker start node for the complex field
        let startRangeMarker = null;
        // the range marker end node for the complex field
        let endRangeMarker = null;
        // whether the range of a complex field place holder node need to be set completely
        const handlePlaceHolderComplexField = options?.handlePlaceHolderComplexField;

        // only expand ranges or complex fields of type "placeholder", not text cursors inside complex fields
        if (!((this.#selection.hasRange() || handlePlaceHolderComplexField) && this.#selection.getSelectionType() === "text")) { return null; }

        const startNodePoint = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
        const endNodePoint = getDOMPosition(this.#selection.getRootNode(), this.#selection.getEndPosition());

        // TODO: Ignoring nested complex fields yet

        if (startNodePoint?.node && endNodePoint?.node) {

            // the start node of the current selection
            let startNode: Node | null = startNodePoint.node;
            // the end node of the current selection
            let endNode: Node | null = endNodePoint.node;

            if (startNode.nodeType === 3) { startNode = startNode.parentNode; }
            if (endNode.nodeType === 3) { endNode = endNode.parentNode; }

            // whether the start of the selection is inside a complex field range
            const startIsComplexField = isInsideComplexFieldRange(startNode as HTMLElement);
            // whether the end of the selection is inside a complex field range
            const endIsComplexField = isInsideComplexFieldRange(endNode as HTMLElement);

            // no complex field involved in selection
            if (!startIsComplexField && !endIsComplexField) { return null; }

            // at least one of the two nodes is inside a complex field
            if (startIsComplexField && !endIsComplexField) {

                // start of selection needs to be shifted to the left
                const startId = getComplexFieldMemberId(startNode as HTMLElement);
                startRangeMarker = this.#rangeMarker.getStartMarker(startId);

            } else if (endIsComplexField && !startIsComplexField) {

                // end of selection needs to be shifted to the right
                const endId = getComplexFieldMemberId(endNode as HTMLElement);
                endRangeMarker = this.#rangeMarker.getEndMarker(endId);

            } else if (startIsComplexField && endIsComplexField) {

                const startId = getComplexFieldMemberId(startNode as HTMLElement);
                const endId = getComplexFieldMemberId(endNode as HTMLElement);

                // both nodes are inside the same complex field -> nothing to do
                if (startId === endId && !handlePlaceHolderComplexField) { return null; }

                // start and end node are inside different complex fields
                // -> the selection needs to be expanded in both directions
                startRangeMarker = this.#rangeMarker.getStartMarker(startId);
                endRangeMarker = this.#rangeMarker.getEndMarker(endId);
            }
        }

        return { start: startRangeMarker, end: endRangeMarker };
    }

    /**
     * Check, if an existing selection range needs to be expanded, so that a complex field
     * is completely part of the selection range. So it is not possible to remove only a part
     * of a selected range.
     *
     * @param selectionHandler
     *  The private selection handler function, that allows modification of start position or
     *  end position.
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.handlePlaceHolderComplexField=false]
     *      If set to true, it is not necessary, that the current selection
     *      has a range, that will be expanded. For complex fields of type
     *      "placeholder" also a cursor selection is automatically expanded.
     *      This value is used inside the function "getRangeMarkersFromSelection".
     */
    #checkRangeSelection(selectionHandler: SelectionPositionCalculatedFn, options: SelectionPositionCalculatedOptions): void {

        // an object with the neighboring range marker nodes
        const rangeMarkers = this.#getRangeMarkersFromSelection(options);

        if (rangeMarkers !== null) {
            if (rangeMarkers.start) {
                selectionHandler(getOxoPosition(this.#selection.getRootNode(), rangeMarkers.start)!, { start: true, selectionExternallyModified: true });
            }

            if (rangeMarkers.end) { // end position need to be increased by 1 for selection range
                selectionHandler(increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), rangeMarkers.end)!), { start: false, selectionExternallyModified: true });
            }
        }
    }

    /**
     * Handler for highlighting of complex fields, after selection has changed.
     *
     * @param event
     *  Passed selection options.
     */
    #checkHighlighting(event: SelectionUpdateEvent): void {

        let atEndOfSpan = false;
        const relevantEvents = ["mousedown", "mouseup", "touchstart", "touchend"];
        const browserEvent = event.event;
        const mouseTouchEvent = is.object(browserEvent) && _.contains(relevantEvents, browserEvent.type);
        const rightClick = is.object(browserEvent) && (browserEvent.type === "mousedown") && (browserEvent.button === 2);
        const isFieldCollectionEmpty = this.#complexField.isEmpty() && this.#simpleField.isEmpty();
        const skipHighlighting = isFieldCollectionEmpty || event.simpleTextSelection || this.#selection.hasRange();

        const isBeforeCxFldStart = (element: NodeOrJQuery): boolean => {
            return isRangeMarkerStartNode(element) && getRangeMarkerType(element) === "field";
        };

        if (this.isHighlightState() || !skipHighlighting) {
            const domPos = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
            if (domPos?.node) {
                let node = domPos.node as HTMLElement;
                if (node.nodeType === 3) {
                    atEndOfSpan = domPos.offset === node?.textContent?.length;
                    node = node.parentNode as HTMLElement;
                }

                if (isComplexFieldNode(node) || isInsideComplexFieldRange(node)) {
                    this.createEnterHighlight(node, { mouseTouchEvent, rightClick });
                } else if (atEndOfSpan && isBeforeCxFldStart(node.nextSibling as HTMLElement)) {
                    const localNextSibling = node.nextSibling as HTMLElement;
                    this.createEnterHighlight(localNextSibling.nextSibling as HTMLElement, { mouseTouchEvent, rightClick }); // spec. handling for cursor positioned before cx field
                } else if (atEndOfSpan && isFieldNode(node.nextSibling as HTMLElement)) {
                    this.createEnterHighlight(node.nextSibling as HTMLElement, { simpleField: true, mouseTouchEvent, rightClick });
                } else if (this.isHighlightState()) {
                    this.removeEnterHighlight();
                }
            }
        }
    }

    /**
     * Callback function for event listener after initial page breaks
     * rendering is finished and after document is loaded.
     *
     * Stops operation distribution to server, and updates possible date fields,
     * and convert and update special page fields in headers.
     *
     * Info: The generated operations to update the fields after loading a document
     *       must not lead to a new file version. Therefore they get a marker
     *       "modifiying: false" (DOCS-2144).
     */
    #updateFieldsAfterLoadCallback(): void {

        // not updating fields after load, if there are already other users that have the document open (DOCS-1558)
        const avoidFieldUpdate = this.docApp.getActiveClients().length > 1;
        this.#updateDateTimeAndSpecialFields({ avoidFieldUpdate, setNonModifyingMarker: true });
        this.docModel.getUndoManager().clearUndoActions();

        if (!this.#simpleField.isEmpty()) {
            this.docModel.getPageLayout().updatePageSimpleFields(); // this happens without operations
        }
    }

    /**
     * Callback function for triggering event fielddatepopup:change, when value of datepicker of input field is changed.
     *
     * @param event
     *  The event object.
     */
    #updateDateFromPopup(event: FieldDatePopupEvent): void {

        let atEndOfSpan;
        let fieldValue = event.value;
        let fieldFormat = event.format;
        const standardizedDate = event.standard;

        if (!fieldValue) {
            if (!fieldFormat) { return; } // no data to update, exit
            if (fieldFormat === "default") { fieldFormat = LOCALE_DATA.shortDate; } // #54713
            fieldValue = this.#numberFormatter.formatNow(fieldFormat);
        }

        if (this.#currentlyHighlightedNode) { // simple field
            jpromise.floating(this.#simpleField.updateDateFromPopupValue(this.#currentlyHighlightedNode, fieldValue!, standardizedDate));

            // remove highlight from deleted node
            this.#currentlyHighlightedNode = null;
            this.setCursorHighlightState(false);

            // add highlight to newly created node
            const domPos = getDOMPosition(this.#selection.getRootNode(), this.#selection.getStartPosition());
            if (domPos?.node) {
                let node = domPos.node;
                if (node.nodeType === 3) {
                    atEndOfSpan = domPos.offset === node?.textContent?.length;
                    node = node.parentNode as HTMLElement;
                }
                if (atEndOfSpan) {
                    const nextSibling = node.nextSibling as HTMLElement;
                    if (nextSibling && isFieldNode(nextSibling)) {
                        this.createEnterHighlight(nextSibling, { simpleField: true });
                    }
                }
            }
        } else if (this.#currentlyHighlightedId) { // complex field
            const node = this.getComplexField(this.#currentlyHighlightedId);
            if (node) { this.#complexField.updateDateFromPopupValue(node, fieldValue!); }
        }
    }

    /**
     * Sets state of date field to be updated automatically or to be fixed value.
     * It is callback from field popup checkbox toggle.
     *
     * @param state
     *  State of the field, fixed or updated automatically
     */
    #setDateFieldToAutoUpdate(state: boolean): JPromise | void {

        let node: JQuery | null = null;
        const generator = this.docModel.createOperationGenerator();
        let operation = {};
        // target for operation - if exists, it is for ex. header or footer
        const target = this.docModel.getActiveTarget();
        let isComplex = true;
        let representation = "";
        let attrs = {};
        let type;
        let instruction;

        if (this.#currentlyHighlightedNode) { // simple field
            node = this.#currentlyHighlightedNode;
            isComplex = false;
            representation = $(node).text();
        } else if (this.#currentlyHighlightedId) { // complex field
            node = this.getComplexField(this.#currentlyHighlightedId);
        }
        if (!node) {
            globalLogger.warn("fieldmanager.setDateFieldToAutoUpdate(): node not fetched!");
            return;
        }
        const pos = getOxoPosition(this.#selection.getRootNode(), node);

        if (pos) {
            return this.docModel.getUndoManager().enterUndoGroup(() => {
                if (isComplex) {
                    operation = { start: json.flatClone(pos), attrs: { character: { autoDateField: state } } };
                    this.docModel.extendPropertiesWithTarget(operation, target);
                    generator.generateOperation(COMPLEXFIELD_UPDATE, operation);
                    this.docModel.applyOperations(generator);

                    instruction = getComplexFieldInstruction(node);
                } else {
                    if (this.docApp.isODF()) {
                        type = "date";
                        attrs = { character: { field: { "text:fixed": json.tryStringify(!state), dateFormat: getFieldDateTimeFormat(node) } } };
                    } else {
                        type = getFieldInstruction(node);
                        attrs = { character: { autoDateField: json.tryStringify(state) } };
                    }
                    operation = { start: json.flatClone(pos), type, representation,  attrs };
                    this.docModel.extendPropertiesWithTarget(operation, target);
                    generator.generateOperation(FIELD_UPDATE, operation);
                    this.docModel.applyOperations(generator);

                    instruction = getFieldInstruction(node);
                }
                // on activating auto update, refresh the date
                if (state && node) { jpromise.floating(this.updateByInstruction(node, instruction, { complex: isComplex })); }
            });
        }

        globalLogger.warn("fieldmanager.setDateFieldToAutoUpdate(): invalid position for field node!");
    }

    /**
     * Fills a formatList with localised date and time values
     *
     * @param formatList
     *  The format list that should be filled
     *
     * @param date
     *  Date that should be used for the formatting
     */
    #setDateTimeFormatList(formatList: Record<CommonConfigKeys, FieldConfigFormatEntry[]>, date: Date): void {

        // category codes loaded from local resource
        const categoryCodesDate = this.#numberFormatter.getCategoryCodes("date" as FormatCategory);
        const categoryCodesTime = this.#numberFormatter.getCategoryCodes("time" as FormatCategory);
        const categoryCodesDateTime = this.#numberFormatter.getCategoryCodes("datetime" as FormatCategory);

        const pushEntry = (targetArray: FieldConfigFormatEntry[], formatCode: string): void => {
            targetArray.push({ option: formatCode, value: this.#numberFormatter.formatValue(formatCode, date).text || "" });
        };

        // fill in date codes
        if (categoryCodesDate.length) {
            categoryCodesDate.forEach(formatCode => pushEntry(formatList.date, formatCode));
            categoryCodesDateTime.forEach(formatCode => pushEntry(formatList.date, formatCode));
        } else {
            pushEntry(formatList.date, LOCALE_DATA.shortDate);
            pushEntry(formatList.date, LOCALE_DATA.longDate);
        }

        // append two most common time formats in date menu
        pushEntry(formatList.date, LOCALE_DATA.shortTime);
        pushEntry(formatList.date, LOCALE_DATA.longTime);

        // fill in time codes
        if (categoryCodesTime.length) {
            categoryCodesTime.forEach(formatCode => pushEntry(formatList.time, formatCode));
        } else {
            pushEntry(formatList.time, LOCALE_DATA.shortTime);
            pushEntry(formatList.time, LOCALE_DATA.longTime);
        }
    }

}
