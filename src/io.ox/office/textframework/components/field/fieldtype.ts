/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { to } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * Enumeration for all supported field types used in complex fields specified
 * in ECMA-376-1 - 17.16.5
 */
export enum FieldType {

    /**
     * date and time
     */
    CREATEDATE = "CREATEDATE",
    DATE = "DATE",
    EDITTIME = "EDITTIME",
    PRINTDATE = "PRINTDATE",
    SAVEDATE = "SAVEDATE",
    TIME = "TIME",

    /**
     * document-automation
     */
    COMPARE = "COMPARE",
    DOCVARIABLE = "DOCVARIABLE",
    GOTOBUTTON = "GOTOBUTTON",
    IF = "IF",
    MACROBUTTON = "MACROBUTTON",
    PRINT = "PRINT",
    PRIVATE = "PRIVATE",

    /**
     * document-information
     */
    FILENAME = "FILENAME",
    FILESIZE = "FILESIZE",
    LASTSAVEDBY = "LASTSAVEDBY",
    NUMCHARS = "NUMCHARS",
    NUMPAGES = "NUMPAGES",
    NUMWORDS = "NUMWORDS",
    TEMPLATE = "TEMPLATE",

    /**
     * document-property
     */
    AUTHOR = "AUTHOR",
    COMMENTS = "COMMENTS",
    DOCPROPERTY = "DOCPROPERTY",
    KEYWORDS = "KEYWORDS",
    SUBJECT = "SUBJECT",
    TITLE = "TITLE",

    /**
     * equations-and-formulas
     */
    EQUATION = "=", // TODO: take care to parse EQUATIONS that do not have a space after the equal sign ... they will get the type USERDEFINED now
    ADVANCE = "ADVANCE",
    SYMBOL = "SYMBOL",

    /**
     * index-and-tables
     */
    INDEX = "INDEX",
    RD = "RD",
    TA = "TA",
    TC = "TC",
    TOA = "TOA",
    TOC = "TOC",
    XE = "XE",

    /**
     * links-and-references
     */
    AUTOTEXT = "AUTOTEXT",
    AUTOTEXTLIST = "AUTOTEXTLIST",
    BIBLIOGRAPHY = "BIBLIOGRAPHY",
    CITATION = "CITATION",
    HYPERLINK = "HYPERLINK",
    INCLUDEPICTURE = "INCLUDEPICTURE",
    INCLUDETEXT = "INCLUDETEXT",
    LINK = "LINK",
    NOTEREF = "NOTEREF",
    PAGEREF = "PAGEREF",
    QUOTE = "QUOTE",
    REF = "REF", // REF is optional
    STYLEREF = "STYLEREF",

    /**
     * mail-merge
     */
    ADDRESSBLOCK = "ADDRESSBLOCK",
    ASK = "ASK",
    DATABASE = "DATABASE",
    FILLIN = "FILLIN",
    GREETINGLINE = "GREETINGLINE",
    MERGEFIELD = "MERGEFIELD",
    MERGEREC = "MERGEREC",
    MERGESEQ = "MERGESEQ",
    NEXT = "NEXT",
    NEXTIF = "NEXTIF",
    SET = "SET",
    SKIPIF = "SKIPIF",

    /**
     * numbering
     */
    LISTNUM = "LISTNUM",
    PAGE = "PAGE",
    REVNUM = "REVNUM",
    SECTION = "SECTION",
    SECTIONPAGES = "SECTIONPAGES",
    SEQ = "SEQ",

    /**
     * user-information
     */
    USERADDRESS = "USERADDRESS",
    USERINITIALS = "USERINITIALS",
    USERNAME = "USERNAME",

    /**
     * form-field
     */
    FORMCHECKBOX = "FORMCHECKBOX",
    FORMDROPDOWN = "FORMDROPDOWN",
    FORMTEXT = "FORMTEXT",

    /**
     * user-defined, if other field types does not match then the type
     * is USERDEFINED. This is also the case it the optional "REF" type is missing.
     */
    USERDEFINED = "USERDEFINED"
}

/**
 * represents the return value of the fromInstruction helper method.
 */
export interface FromInstructionValue {

    /**
     * The fieldType, as listed above, the fieldType will be USERDEFINED if
     * no other type matches, then of course the type is saved in userDefinedTypeValue.
     */
    fieldType: FieldType;

    /**
     * the remaining instruction without type is returned. The string can be empty if there
     * are no further parameter in the instruction
     */
    remainingInstruction: string;

    /**
     * If the fieldType is USERDEFINED this value will contain the userDefinedTypeValue.
     */
    userDefinedTypeValue?: string;
}

/**
 * Returns a FromInstructionValue containing the FieldType and the remaining instruction without type
 *
 * @param instruction
 *  The instruction containing the field type
 */
export function fromInstruction(instruction: string): FromInstructionValue {
    let parsedType = "";
    const remainingInstruction = instruction.replace(/^\S+/, (match: string) => {
        parsedType = match;
        return "";
    }).trim();
    const fieldType = to.enum(FieldType, parsedType, FieldType.USERDEFINED);
    return { fieldType, remainingInstruction, userDefinedTypeValue: fieldType === FieldType.USERDEFINED ? parsedType : undefined };
}
