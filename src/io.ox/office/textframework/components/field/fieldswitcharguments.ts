/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

// types ======================================================================

/**
 * Enumeration for all supported numeric arguments in general formatting switches
 * in ECMA-376-1 - 17.16.4.3.1
 */
export enum GeneralNumeric {
    AIUEO = "AIUEO",
    ALPHABETIC = "ALPHABETIC",
    alphabetic = "alphabetic",
    Arabic = "Arabic",
    ARABICABJAD = "ARABICABJAD",
    ARABICALPHA = "ARABICALPHA",
    ArabicDash = "ArabicDash",
    BAHTTEXT = "BAHTTEXT",
    CardText = "CardText",
    CHINESENUM1 = "CHINESENUM1",
    CHINESENUM2 = "CHINESENUM2",
    CHINESENUM3 = "CHINESENUM3",
    CHOSUNG = "CHOSUNG",
    CIRCLENUM = "CIRCLENUM",
    DBCHAR = "DBCHAR",
    DBNUM1 = "DBNUM1",
    DBNUM2 = "DBNUM2",
    DBNUM3 = "DBNUM3",
    DBNUM4 = "DBNUM4",
    DollarText = "DollarText",
    GANADA = "GANADA",
    GB1 = "GB1",
    GB2 = "GB2",
    GB3 = "GB3",
    GB4 = "GB4",
    HEBREW1 = "HEBREW1",
    HEBREW2 = "HEBREW2",
    Hex = "Hex",
    HINDIARABIC = "HINDIARABIC",
    HINDICARDTEXT = "HINDICARDTEXT",
    HINDILETTER1 = "HINDILETTER1",
    HINDILETTER2 = "HINDILETTER2",
    IROHA = "IROHA",
    KANJINUM1 = "KANJINUM1",
    KANJINUM2 = "KANJINUM2",
    KANJINUM3 = "KANJINUM3",
    Ordinal = "Ordinal",
    OrdText = "OrdText",
    Roman = "Roman",
    roman = "roman",
    SBCHAR = "SBCHAR",
    THAIARABIC = "THAIARABIC",
    THAICARDTEXT = "THAICARDTEXT",
    THAILETTER = "THAILETTER",
    VIETCARDTEXT = "VIETCARDTEXT",
    ZODIAC1 = "ZODIAC1",
    ZODIAC2 = "ZODIAC2",
    ZODIAC3 = "ZODIAC3"
}

/**
 * Enumeration for all supported string formatting arguments in general formatting switches
 * in ECMA-376-1 - 17.16.4.3.2
 */
export enum GeneralString {
    Caps = "Caps",
    FirstCap = "FirstCap",
    Lower = "Lower",
    Upper = "Upper"
}

/**
 * Enumeration for all supported string formatting arguments in general formatting switches
 * in ECMA-376-1 - 17.16.4.3.3
 */
export enum GeneralAny {
    MERGEFORMAT = "MERGEFORMAT",
    CHARFORMAT = "CHARFORMAT"
}
