/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str, fmt } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import type { DisplayText } from "@/io.ox/office/editframework/model/formatter/autoformat";

import { FieldInstruction } from "@/io.ox/office/textframework/components/field/fieldinstruction";
import { FieldType } from "@/io.ox/office/textframework/components/field/fieldtype";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";
import type { NumberFormatter } from "@/io.ox/office/textframework/model/numberformatter";

// types ==================================================================

export interface TypeInstructionDescription {
    type: FieldType;
    instruction: string | string[];
}

// class BaseField ========================================================

/**
 * An instance of this class represents the base class for all fields in the
 * edited document.
 */
export default class BaseField extends ModelObject<TextBaseModel> {

    // instance of the number formatter
    #numberFormatter!: NumberFormatter;

    constructor(docModel: TextBaseModel) {

        // base constructor
        super(docModel);

        this.docApp.onInit(() => {
            this.#numberFormatter = docModel.numberFormatter;
        });
    }

    // public methods -----------------------------------------------------

    /**
     * Helper functions to determine type of field from instruction
     */
    isNumPages(type: string): boolean {
        return (/NUMPAGES/i).test(type);
    }

    isPageNumber(type: string): boolean {
        return (/PAGE/i).test(type) && !(/PAGEREF/i).test(type);
    }

    isCurrentDate(type: string): boolean {
        return (/^DATE/i).test(type);
    }

    isCurrentTime(type: string): boolean {
        return (/^TIME/i).test(type);
    }

    isTableOfContents(type: string): boolean {
        return (/^TOC/i).test(type);
    }

    isSaveDate(type: string): boolean {
        return (/^SAVEDATE/i).test(type);
    }

    isFileName(type: string): boolean {
        return (/FILE-?NAME/i).test(type);
    }

    isAuthor(type: string): boolean {
        return (/^AUTHOR/i).test(type) || type === "author-name" || type === "creator";
    }

    /**
     * Parse and trim instruction string. Remove escape chars,
     * format specific @ and * switches, and trims whitespaces.
     *
     * @param instruction
     *  The field instruction code.
     */
    trimFormatInstruction(instruction: string): string | null {
        let tempFormat = instruction.replace(/"/g, "");

        if (!tempFormat) { return null; }

        tempFormat = tempFormat.replace(/[@*]/g, "");

        return str.trimAll(tempFormat);
    }

    /**
     * From raw passed instruction string, extract type,
     * and clean up remaining instruction for future formatting.
     *
     * @param instruction
     *  Instruction to be cleaned up.
     *
     * @returns
     *  A descriptor object.
     */
    cleanUpAndExtractType(instruction: string): TypeInstructionDescription {
        const fieldInstruction = new FieldInstruction(instruction);
        const type = fieldInstruction.getType();
        let formattedFieldInstruction;

        switch (type) {
            case FieldType.DATE:
            case FieldType.TIME:
            case FieldType.SAVEDATE: {
                formattedFieldInstruction = fieldInstruction.getDateAndTimeFormattingArgument(true);
                // DOCS-4654, if no instruction is available then LOCALE_DATA format is returned
                if (!formattedFieldInstruction) {
                    if (type === FieldType.DATE) {
                        formattedFieldInstruction = LOCALE_DATA.shortDate;
                    } else if (type === FieldType.TIME) {
                        formattedFieldInstruction = LOCALE_DATA.shortTime;
                    }
                }
                break;
            }
            case FieldType.NUMPAGES:
            case FieldType.PAGE:
            case FieldType.PAGEREF: {
                formattedFieldInstruction = fieldInstruction.getGeneralFormattingNumericArgument();
                break;
            }
            case FieldType.AUTHOR:
            case FieldType.FILENAME: {
                formattedFieldInstruction = fieldInstruction.getGeneralFormattingStringArgument();
                break;
            }
            case FieldType.TOC: {
                formattedFieldInstruction = fieldInstruction.getArrayOfTypeAndArguments();
                break;
            }
            default:
                break;
        }
        return { type, instruction: formattedFieldInstruction || "default" };
    }

    /**
     * Converts number for page and numPages fields into proper format code.
     *
     * @param number
     *  Number to be inserted into field.
     *
     * @param format
     *  Format of the number inserted into field.
     *
     * @param [numPages]
     *  NumPages field have some formats different from PageNum.
     *
     * @returns
     *  Formatted number.
     */
    formatPageFieldInstruction(number: number, format: string, numPages?: boolean): string {

        if (!is.number(number)) {
            globalLogger.warn("basefield.formatPageFieldInstruction(): invalid number argument!");
            return "";
        }

        // ODF format codes
        if (this.docApp.isODF()) {
            switch (format) {
                case "A": return fmt.formatAlphabetic(number, { repeat: true });
                case "a": return fmt.formatAlphabetic(number, { repeat: true, lower: true });
                case "I": return fmt.formatRoman(number, { depth: 4 });
                case "i": return fmt.formatRoman(number, { depth: 4, lower: true });
                default: return number.toString();
            }
        }

        // OOXML format codes
        if (is.string(format)) {
            if (format.includes("ALPHABETIC")) { return fmt.formatAlphabetic(number, { repeat: true }); }
            if (format.includes("alphabetic")) { return fmt.formatAlphabetic(number, { repeat: true, lower: true }); }
            if (format.includes("ROMAN")) { return fmt.formatRoman(number, { depth: 4 }); }
            if (format.includes("roman")) { return fmt.formatRoman(number, { depth: 4, lower: true }); }
            if (format.includes("ArabicDash") && !numPages) { return `-${number}-`; }
            if (format.includes("Hex") && numPages) { return fmt.formatInt(number, 16, { lower: true }); }
            if (format.includes("Ordinal") && numPages) { return `${number}.`; }
        }
        return number.toString();
    }

    /**
     * Create current date(time) value string with passed format type.
     *
     * @param formatCode
     *  The format code for the field representation.
     *
     * @returns
     *  The field representation.
     */
    getDateTimeRepresentation(formatCode: string): DisplayText {
        return this.#numberFormatter.formatNow(formatCode);
    }

    /**
     * Returns whether the path should be used.
     *
     * @param formatCode
     *  The format code for the field representation.
     *
     * @returns
     *  Whether the path should be used.
     */
    usePath(formatCode: string): boolean {
        return (/\bp\b/i).test(formatCode);
    }

    /**
     * Getter for the save date.
     *
     * @returns
     *  returns the savedate
     *
     */
    getSaveDate(): Date {

        let date;
        const saveDate = this.docModel.globalConfig.modified;

        if (saveDate) {
            // docs-3246, it seems that word is using the local timezone in combination with the savedate
            date = new Date(Date.parse(saveDate));
            date.setUTCMilliseconds(date.getUTCMilliseconds() - new Date().getTimezoneOffset() * 60000);
            return date;
        }
        return new Date();
    }

}
