/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { dict, is, jpromise, json, str } from "@/io.ox/office/tk/algorithms";
import { type NodeOrJQuery, containsNode } from "@/io.ox/office/tk/dom";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { clearSelection } from "@/io.ox/office/drawinglayer/view/drawingframe";

import type { PtCharacterAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { Position } from "@/io.ox/office/editframework/utils/operations";

import type ChangeTrack from "@/io.ox/office/textframework/components/changetrack/changetrack";
import type PageLayout from "@/io.ox/office/textframework/components/pagelayout/pagelayout";
import type Selection from "@/io.ox/office/textframework/selection/selection";

import type TextBaseModel from "@/io.ox/office/textframework/model/editor";
import type { NumberFormatter } from "@/io.ox/office/textframework/model/numberformatter";
import { FIELD_INSERT, FIELD_UPDATE, type InsertFieldOperation, type UpdateFieldOperation } from "@/io.ox/office/textframework/utils/operations";
import { FIELD_NODE_SELECTOR, PAGECOUNT_FIELD_SELECTOR, PAGENUMBER_FIELD_SELECTOR, getClosestMarginalTargetNode,
    getFieldDateTimeFormat, getFieldInstruction, getFieldPageFormat, getPageContentNode, getSimpleFieldId,
    getTargetContainerId, isFieldNode, isFixedSimpleField, isFooterWrapper, isHeaderOrFooter, isHeaderWrapper,
    isInsideHeaderFooterTemplateNode, isMarginalNode, isParagraphNode } from "@/io.ox/office/textframework/utils/dom";
import { getDOMPosition, getOxoPosition, increaseLastIndex } from "@/io.ox/office/textframework/utils/position";
import BaseField from "@/io.ox/office/textframework/components/field/basefield";

// types ==================================================================

export type FieldCollectorType = Dict<string | JQuery>;

// class SimpleField ======================================================

/**
 * An instance of this class represents the model for all simple fields in the
 * edited document.
 *
 * @param {TextModel} docModel
 *  The model instance.
 */
export default class SimpleField extends BaseField {

    // the selection object
    #selection!: Selection;
    // page layout object
    #pageLayout!: PageLayout;
    // change track object
    #changeTrack!: ChangeTrack;
    // collector for simple fields
    // -> key is the field id (string), value is the jQueryfied field not or the target string for target nodes
    #allSimpleFields: FieldCollectorType = {};
    // counter for simple field Id
    #simpleFieldID = 0;
    // number formatter object
    #numberFormatter!: NumberFormatter;

    constructor(docModel: TextBaseModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {
            this.#selection = this.docModel.getSelection();
            this.#pageLayout = this.docModel.getPageLayout();
            this.#changeTrack = this.docModel.getChangeTrack();
            this.#numberFormatter = this.docModel.numberFormatter;
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Public method to add simple field to model.
     *
     * @param fieldNode
     *  One simple field node.
     *
     * @param [target]
     *  The target, where the simple field is located.
     */
    addSimpleFieldToCollection(fieldNode: NodeOrJQuery, target?: string): void {
        this.#addSimpleFieldToCollection(fieldNode, target);
    }

    /**
     * Public method to remove simple field from model.
     *
     * @param node
     *  One simple field node.
     */
    removeSimpleFieldFromCollection(node: NodeOrJQuery): void {
        this.#removeSimpleFieldFromCollection(getSimpleFieldId(node));
    }

    /**
     * Whether the document contains complex fields in the document.
     *
     * @returns
     *  Whether the document contains at least one complex field.
     */
    isEmpty(): boolean {
        return is.empty(this.#allSimpleFields);
    }

    /**
     * Public method to get collection of all simple fields from model.
     *
     * @returns
     *  The collector for all simple fields
     */
    getAllFields(): FieldCollectorType {
        return this.#allSimpleFields;
    }

    /**
     * Public method to get number of all simple fields from model.
     *
     * @returns
     *  The number of fields in the collection
     */
    getAllFieldsCount(): number {
        return _.size(this.#allSimpleFields);
    }

    /**
     * Gets simple field from collection by passed id.
     *
     * @param id
     *  Id of queried field.
     *
     * @returns
     *  The simple field with the specified id, or null, if no such start marker exists.
     */
    getSimpleField(id: string): JQuery | null {
        // the value saved under the specified id
        const field = this.#allSimpleFields[id] || null;
        return is.string(field) ? this.#getMarginalSimpleField(id, field) : field;
    }

    /**
     * After deleting a complete paragraph, it is necessary, that all simple field nodes
     * in the deleted paragraph are also removed from the model collectors.
     *
     * @param paragraph
     *  The paragraph node.
     */
    removeSimpleFieldsInNode(paragraph: NodeOrJQuery): void {

        // all simple field nodes inside the paragraph
        const simpleFieldNodes = $(paragraph).find(FIELD_NODE_SELECTOR);

        // update the marker nodes in the collection objects
        simpleFieldNodes.each((_index, field) => {

            // the id of the marker node
            const fieldId = getSimpleFieldId(field);

            if (is.string(fieldId)) {
                this.#removeSimpleFieldFromCollection(fieldId);
            }
        });
    }

    /**
     * Update simple field depending of type, by given instruction.
     *
     * @param node
     *  Simple field node
     *
     * @param instruct
     *  Simple field instruction, containing type,
     *  formating and optionally style, separated by \ .
     *  (In case of ODF, instruction is empty, and we fetch possible
     *  date/time format from data property of node).
     */
    updateByInstruction(node: NodeOrJQuery, instruct: string): void {

        let parsedFormatInstruction;
        const dividedData = this.cleanUpAndExtractType(instruct);
        const type = dividedData.type;
        let instruction = dividedData.instruction;
        // instruction can only be a string array for TOCs and this are no simple fields
        // -> in the following code instruction must be a string
        if (is.array(instruction)) { instruction = instruction[0]; }

        if (type) {
            if (this.isCurrentDate(type)) {
                parsedFormatInstruction = getFieldDateTimeFormat(node) || instruction;
                this.#updateDateTimeField(node, parsedFormatInstruction);
            } else if (this.isCurrentTime(type)) {
                parsedFormatInstruction = getFieldDateTimeFormat(node) || instruction;
                this.#updateDateTimeField(node, parsedFormatInstruction, { time: true });
            } else if (this.isFileName(type)) {
                this.#updateFileNameField(node, instruction);
            } else if (this.isAuthor(type)) {
                this.#updateAuthorField(node, instruction);
            } else if (this.isNumPages(type)) {
                parsedFormatInstruction = getFieldPageFormat(node) || instruction;
                this.#updateNumPagesField(node, parsedFormatInstruction);
            } else if (this.isPageNumber(type)) {
                parsedFormatInstruction = getFieldPageFormat(node) || instruction;
                this.#updatePageNumberField(node, parsedFormatInstruction);
            }
        }
    }

    /**
     * Update date and time fields on document load, or before running download or print action.
     *
     * @param field
     *  Field to be updated.
     *
     * @param instruct
     *  Instruction for updating.
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.setNonModifyingMarker=false]
     *      If set to true, the generated operations get a marker "modifying: false"
     *      that the backend knows, that no new file version must be generated.
     *      This is required after loading a document and the fields are
     *      updated automatically (DOCS-2144).
     */
    updateDateTimeField(field: NodeOrJQuery, instruct: string, options?: { setNonModifyingMarker?: boolean }): void {

        const dividedData = this.cleanUpAndExtractType(instruct);
        const type = dividedData.type;
        let instruction = dividedData.instruction;
        // instruction can only be a string array for TOCs and this are no simple fields
        // -> in the following code instruction must be a string
        if (is.array(instruction)) { instruction = instruction[0]; }

        if (type) {
            if ((this.isCurrentDate(type) || this.isCurrentTime(type))) {
                instruction = this.docApp.isODF() ? getFieldDateTimeFormat(field) : instruction;
                this.#updateDateTimeField(field, instruction, { time: this.isCurrentTime(type), setNonModifyingMarker: options?.setNonModifyingMarker });
            }
        }
    }

    /**
     * After load from local storage or fast load the collectors for the simple fields need to be filled.
     */
    refreshSimpleFields(): void {
        // the page content node of the document
        const pageContentNode = getPageContentNode(this.docModel.getNode());
        // all simple field nodes in the document
        const allSimpleFieldNodes = pageContentNode.find(FIELD_NODE_SELECTOR);
        // a collector for all marginal template nodes
        const allMargins = this.#pageLayout.getHeaderFooterPlaceHolder().children();
        // the target string node
        let target = "";

        // reset model
        this.#allSimpleFields = {};

        allSimpleFieldNodes.each((_index, fieldNode) => {
            if (!isMarginalNode($(fieldNode).parent())) {
                this.#addSimpleFieldToCollection(fieldNode);
            }
        });

        allMargins.each((_index, margin) => {
            // a collector for all range marker nodes inside one margin
            const allMarginSimpleFieldNodes = $(margin).find(FIELD_NODE_SELECTOR);

            if (allMarginSimpleFieldNodes.length > 0) {
                target = getTargetContainerId(margin);
                allMarginSimpleFieldNodes.each((_idx, oneMarginalField) => {
                    this.#addSimpleFieldToCollection(oneMarginalField, target);
                });
                this.#pageLayout.replaceAllTypesOfHeaderFooters();
            }
        });
    }

    /**
     * After splitting a paragraph, it is necessary, that all simple field nodes in the cloned
     * "new" paragraph are updated in the collectors.
     *
     * @param para
     *  The paragraph node.
     */
    updateSimpleFieldCollector(para: NodeOrJQuery): void {

        // not necessary for paragraphs in header or footer -> only the target are stored, not the nodes
        if (isMarginalNode(para)) { return; }

        // if this is not a paragraph, each single node need to be checked
        const checkMarginal = !isParagraphNode(para);

        // all simple fields inside the paragraph
        const allFields = $(para).find(FIELD_NODE_SELECTOR);

        // update the complex field nodes in the collection objects
        allFields.each((_index, fieldNode) => {
            if (!checkMarginal || !isMarginalNode(fieldNode)) {
                // simply overwriting the old simple fields with the new values
                this.#addSimpleFieldToCollection(fieldNode);
            }
        });
    }

    /**
     * On document load, this method loops all fields in collection
     * to find date/time fields and update them.
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.avoidFieldUpdate=false]
     *      If set to true, the fields must not be updated. This is required
     *      after load of document, if other users have the document already
     *      opened.
     *  @param [options.setNonModifyingMarker=false]
     *      If set to true, the generated operations get a marker "modifying: false"
     *      that the backend knows, that no new file version must be generated.
     *      This is required after loading a document and the fields are
     *      updated automatically (DOCS-2144).
     */
    updateDateTimeFieldOnLoad(options?: { avoidFieldUpdate?: boolean; setNonModifyingMarker?: boolean }): void {

        const startPos = this.#selection.getStartPosition();
        const endPos = this.#selection.getEndPosition();
        let returnOriginalCursorPos = false;

        if (options?.avoidFieldUpdate) { return; }

        for (const [fieldId, nodeOrTargetId] of dict.entries(this.#allSimpleFields)) {
            // dict.forEach(this.#allSimpleFields, (nodeOrTargetId, fieldId) => {

            let field = this.getSimpleField(fieldId);
            if (!field) { continue; }

            const fieldInstruction = getFieldInstruction(field);
            const isDateTimeField = fieldInstruction && (this.isCurrentDate(fieldInstruction) || this.isCurrentTime(fieldInstruction));
            const isFixed = isFixedSimpleField(field, this.docApp.isODF());
            let selectedDrawing: JQuery | undefined;
            const isMarginalField = isMarginalNode(field);

            if (this.docApp.isEditable() && isDateTimeField && !isFixed && (!isMarginalNode(field) || !isInsideHeaderFooterTemplateNode(this.docModel.getNode(), field))) {
                // if previously updated field is marginal, and this is not, leave header edit state
                this.#selection.switchToValidRootNode(field);
                // for marginal fields an update might be required, if they are removed from DOM after "switchToValidRootNode"
                if (isMarginalField && !containsNode(this.docModel.getCurrentRootNode(), field)) { field = this.getSimpleField(fieldId); }
                if (field) { this.updateDateTimeField(field, fieldInstruction, options); }
                returnOriginalCursorPos = true;
                if (is.string(nodeOrTargetId) && this.#pageLayout.isIdOfMarginalNode(nodeOrTargetId) && this.docModel.isHeaderFooterEditState() && isHeaderOrFooter(this.#selection.getRootNode())) {
                    // clear possible leftover selection on drawings and textframes
                    if (this.#selection.getSelectionType() === "drawing") {
                        selectedDrawing = this.#selection.getSelectedDrawing();
                    }
                    if (this.#selection.isAdditionalTextframeSelection()) {
                        selectedDrawing = this.#selection.getSelectedTextFrameDrawing();
                    }
                    if (selectedDrawing?.length) { clearSelection(selectedDrawing); }
                    this.#pageLayout.getHeaderFooterPlaceHolder().children(`.${nodeOrTargetId}`).empty().append($(this.#selection.getRootNode()).children().clone(true));
                    if (selectedDrawing?.length) { this.#selection.drawDrawingSelection(selectedDrawing); }
                }
            }
        }

        if (this.docModel.isHeaderFooterEditState()) {
            this.#pageLayout.leaveHeaderFooterEditMode();
            this.#selection.setNewRootNode(this.docModel.getNode());
        }
        if (returnOriginalCursorPos) {
            this.docModel.setActiveTarget();
            this.#selection.setNewRootNode(this.docModel.getNode());
            this.#selection.setTextSelection(startPos, endPos);
        }
    }

    /**
     * Public method for updating current page number field(s) in given node.
     *
     * @param $node
     *  The DOM element.
     *
     * @param isHeader
     *  If node is header or not.
     *
     * @param pageCount
     *  Number of total pages in document.
     */
    updatePageNumInCurrentNode($node: JQuery, isHeader: boolean, pageCount: number): void {
        if (isHeader) {
            if (isHeaderWrapper($node.parent())) {
                $node.find(PAGENUMBER_FIELD_SELECTOR).children().empty().html("1");
            } else {
                $node.find(PAGENUMBER_FIELD_SELECTOR).each((_index, field) => {
                    const pageNum = $(field).parentsUntil(".pagecontent", ".page-break").data("page-num") as string;
                    const newPageNum = parseInt(pageNum, 10) + 1;
                    $(field).children().empty().html(String(newPageNum));
                });
            }
        } else if (isFooterWrapper($node.parent())) {
            $node.find(PAGENUMBER_FIELD_SELECTOR).children().empty().html(String(pageCount));
        } else {
            $node.find(PAGENUMBER_FIELD_SELECTOR).each((_index, field) => {
                const pageNum = $(field).parentsUntil(".pagecontent", ".page-break").data("page-num") as string;
                $(field).children().empty().html(pageNum);
            });
        }
    }

    /**
     * Public method for updating number of pages field(s) in given node.
     *
     * @param $node
     *  The DOM element.
     *
     * @param pageCount
     *  Number of total pages in document.
     */
    updatePageCountInCurrentNode($node: JQuery, pageCount: number): void {
        $node.find(PAGECOUNT_FIELD_SELECTOR).children().empty().html(String(pageCount));
    }

    /**
     * Insert simple field (usually ODF format, or some legacy docx).
     *
     * @param fieldType
     *  The type of the new field.
     *
     * @param fieldFormat
     *  The format code for the new field.
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.isFixed="true"]
     *      If it is fixed date field.
     */
    insertField(fieldType: string, fieldFormat: string, options?: { isFixed?: boolean }): JPromise {

        return this.docModel.getUndoManager().enterUndoGroup((): JPromise => {

            const doInsertField = (): void => {

                const start = this.#selection.getStartPosition();
                const generator = this.docModel.createOperationGenerator();
                // target for operation - if exists, it"s for ex. header or footer
                const target = this.docModel.getActiveTarget();
                let representation = "";
                const isFixed = options?.isFixed ?? true; // default value is true

                this.docModel.doCheckImplicitParagraph(start);

                if (fieldType) {
                    const operation: Partial<InsertFieldOperation> = { start: json.flatClone(start), type: fieldType };

                    if (this.isCurrentDate(fieldType) || this.isCurrentTime(fieldType)) {
                        if (!fieldFormat || fieldFormat === "default") {
                            fieldFormat = LOCALE_DATA.shortDate;
                        }
                        representation = this.getDateTimeRepresentation(fieldFormat) || "";
                        if (this.docApp.isODF()) {
                            operation.attrs = { character: { field: { dateFormat: fieldFormat, "text:fixed": String(isFixed) } } };
                        } else {
                            operation.type = fieldType + " \\@ " + fieldFormat;
                        }
                    } else if (this.isFileName(fieldType) || this.isAuthor(fieldType)) {
                        representation = (this.isFileName(fieldType) ? this.docApp.getFullFileName() : this.docApp.getClientOperationName()) || "";
                        if ((/Lower/i).test(fieldFormat)) {
                            representation = representation.toLowerCase();
                        } else if ((/Upper/i).test(fieldFormat)) {
                            representation = representation.toUpperCase();
                        } else if ((/FirstCap/i).test(fieldFormat)) {
                            representation = str.capitalizeFirst(representation.toLowerCase());
                        } else if ((/Caps/i).test(fieldFormat)) {
                            representation = str.capitalizeWords(representation);
                        }
                        if (this.docApp.isODF()) {
                            if (this.isFileName(fieldType)) {
                                operation.type = "file-name";
                            } else {
                                operation.type = "creator";
                            }
                        }
                        // TODO format for this types of fields
                    } else if (this.isNumPages(fieldType) || fieldType === "page-count") { // check for "page-count" is required because of task DOCS-3357
                        representation = this.formatPageFieldInstruction(this.#pageLayout.getNumberOfDocumentPages(), fieldFormat, true);
                        if (this.docApp.isODF()) {
                            operation.attrs = operation.attrs ?? {};
                            operation.attrs.character = operation.attrs.character ?? {};
                            operation.attrs.character.field = { pageNumFormat: fieldFormat };
                            operation.type = "page-count";
                        } else {
                            operation.type = fieldType + " \\* " + fieldFormat;
                        }
                    } else if (this.isPageNumber(fieldType)) {
                        representation = this.formatPageFieldInstruction(this.#pageLayout.getPageNumber(), fieldFormat);
                        if (this.docApp.isODF()) {
                            operation.attrs = operation.attrs ?? {};
                            operation.attrs.character = operation.attrs.character ?? {};
                            operation.attrs.character.field = { pageNumFormat: fieldFormat };
                            operation.type = "page-number";
                        } else {
                            operation.type = fieldType + " \\* " + fieldFormat;
                        }
                    }

                    operation.representation = representation;
                    this.docModel.extendPropertiesWithTarget(operation, target);

                    // modifying the attributes, if changeTracking is activated
                    if (this.#changeTrack.isActiveChangeTracking()) {
                        operation.attrs = operation.attrs ?? {};
                        operation.attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
                    }

                    generator.generateOperation(FIELD_INSERT, operation);
                    this.docModel.applyOperations(generator);

                    this.#selection.setTextSelection(increaseLastIndex(start));
                }
            };

            if (this.#selection.hasRange()) {
                const deletePromise = this.docModel.deleteSelected();
                deletePromise.done(doInsertField);
                return deletePromise;
            }

            doInsertField();
            return jpromise.resolve();
        });
    }

    /**
     * Update date field with value from popup, datepicker or input field.
     *
     * @param node
     *   Field node that is going to be updated
     *
     * @param fieldValue
     *   Value with which will field be updated.
     *
     * @param standardizedDate
     *   Odf needs this standard date value for fixed fields.
     */
    updateDateFromPopupValue(node: JQuery, fieldValue: string, standardizedDate: string | null): JPromise | void {

        const operationType = $(node).data("type") as string;
        const operationAttrs = $(node).data("attributes") as PtCharacterAttributeSet;
        const dateFormat = getFieldDateTimeFormat(node);
        const isMarginal = isMarginalNode(node);
        const rootNode = isMarginal ? getClosestMarginalTargetNode(node) : this.#selection.getRootNode();
        const target = isMarginal ? getTargetContainerId(rootNode) : "";
        const startPos = getOxoPosition(rootNode, node);
        let operation: InsertFieldOperation;

        if (startPos) {
            return this.docModel.getUndoManager().enterUndoGroup(() => {
                this.docModel.deleteRange(startPos);
                operation = { name: FIELD_INSERT, start: json.flatClone(startPos), representation: fieldValue, type: operationType };
                if (target) {
                    operation.target = target;
                }
                if (operationAttrs) {
                    operation.attrs = operationAttrs;
                }
                if (dateFormat) {
                    operation.attrs = { character: { field: { dateFormat, "text:fixed": "true", dateValue: standardizedDate } } };
                }

                this.docModel.applyOperations(operation);
                this.#selection.setTextSelection(increaseLastIndex(startPos));
            });
        }

        globalLogger.error("simpleField.updateDateFromPopupValue(): Wrong start postition: ", startPos);
    }

    /**
     * Handler for updateField operation.
     *
     * @param start
     *  The logical start position for the new simple field.
     *
     * @param type
     *  Type of the simple field.
     *
     * @param representation
     *  Content of the field that is updated.
     *
     * @param attrs
     *  The character attributes.
     *
     * @param target
     *  The target corresponding to the specified logical start position.
     *
     * @param external
     *  Whether this is an external operation.
     *
     * @returns
     *  Whether the simple field has been inserted successfully.
     */
    updateSimpleFieldHandler(start: Position, type: string, representation: string, attrs: PtCharacterAttributeSet, target: string, external: boolean): boolean {

        const undoManager = this.docModel.getUndoManager();
        let undoOperation: UpdateFieldOperation;
        let redoOperation: UpdateFieldOperation;
        const fieldPoint = getDOMPosition(this.docModel.getRootNode(target), start, true);
        let odfFixedField;
        const fieldNode = fieldPoint?.node ?? null;

        if (attrs && fieldNode) {
            if (attrs.character && !is.undefined(attrs.character.autoDateField)) {
                this.docModel.characterStyles.setElementAttributes(fieldNode as HTMLElement, attrs); // This is needed to remove property set in SetAttributes operation, in data of the element!
                $(fieldNode).attr("data-auto-date", String(attrs.character.autoDateField));

                // create the undo/redo actions
                if (undoManager.isUndoEnabled() && !external) {
                    undoOperation = { name: FIELD_UPDATE, start: json.flatClone(start), type, attrs: { character: { autoDateField: !attrs.character.autoDateField } }, representation };
                    redoOperation = { name: FIELD_UPDATE, start: json.flatClone(start), type, attrs: { character: { autoDateField: attrs.character.autoDateField } }, representation };
                    if (target) {
                        undoOperation.target = target;
                        redoOperation.target = target;
                    }
                    undoManager.addUndo(undoOperation, redoOperation);
                }
            } else if (attrs.character?.field && !is.undefined(attrs.character.field["text:fixed"])) { // odf
                const fieldAttrs = $(fieldNode).data("attributes") as PtCharacterAttributeSet;
                odfFixedField = attrs.character.field["text:fixed"];
                $(fieldNode).data("text:fixed", odfFixedField);
                const tempAttrs = { character: { field: { fixed: odfFixedField === "true" ? "false" : "true" } } };
                const redoTempAttrs = { character: { field: { fixed: odfFixedField } } };
                if (fieldAttrs?.character?.field) {
                    fieldAttrs.character.field["text:fixed"] = odfFixedField;
                } else {
                    $(fieldNode).data("attributes", tempAttrs);
                }

                // create the undo/redo actions
                if (undoManager.isUndoEnabled() && !external) {
                    undoOperation = { name: FIELD_UPDATE, start: json.flatClone(start), type, attrs: tempAttrs, representation };
                    redoOperation = { name: FIELD_UPDATE, start: json.flatClone(start), type, attrs: redoTempAttrs, representation };
                    if (target) {
                        undoOperation.target = target;
                        redoOperation.target = target;
                    }
                    undoManager.addUndo(undoOperation, redoOperation);
                }
            }
        }
        return true;
    }

    // private methods ----------------------------------------------------

    /**
     * Registering one simple field in the collection (the model).
     *
     * @param fieldNode
     *  One simple field node.
     *
     * @param [target]
     *  The target, where the simple field is located.
     */
    #addSimpleFieldToCollection(fieldNode: NodeOrJQuery, target?: string): void {

        const $fieldNode = $(fieldNode);
        const id = $fieldNode.data("sFieldId") ? $fieldNode.data("sFieldId") as string : `sf${this.#simpleFieldID}`;
        const isMarginalId = target && this.#pageLayout.isIdOfMarginalNode(target);
        const fieldAttrs = $fieldNode.data("attributes") as PtCharacterAttributeSet;

        // when fastload, pageNum and dateFormat formats needs to be set to field data
        if (fieldAttrs?.character?.field) {
            const attrsField = fieldAttrs.character.field;
            if (attrsField.pageNumFormat && !$fieldNode.data("pageNumFormat")) {
                $fieldNode.data("pageNumFormat", attrsField.pageNumFormat);
            }
            if (attrsField.dateFormat && !$fieldNode.data("dateFormat")) {
                $fieldNode.data("dateFormat", attrsField.dateFormat);
            }
        }

        $fieldNode.data("sFieldId", id);
        this.#allSimpleFields[id] = isMarginalId ? target : $fieldNode;
        this.#simpleFieldID += 1;
    }

    /**
     * Removing one simple field with specified id from the collection (the model).
     *
     * @param id
     *  The unique id of the simple field node.
     *
     */
    #removeSimpleFieldFromCollection(id: string): void {
        delete this.#allSimpleFields[id];
    }

    /**
     * Before a field is updated, it can be checked, whether its content has changed.
     * Operations for deleting old content and inserting new content should only be
     * generated, if the content has really changed (DOCS-1558).
     *
     * Info: The change of a simple field is determined by a comparison of the old
     *       and the new string. Therefore any string attributes are ignored and a
     *       field update does not lead to a removal of additionally set attributes
     *       like in Word, if the string itself did not change.
     *
     * @param newString
     *  The new string value that shall be inserted into the complex field.
     *
     * @param fieldNode
     *  The field node.
     *
     * @returns
     *  Whether the string inside the complex field is modified.
     */
    #simpleFieldContentChanged(newString: string, fieldNode: NodeOrJQuery): boolean {
        return $(fieldNode).text() !== newString;
    }

    /**
     * Finding in a header or footer specified by the target the simple field
     * with the specified id.
     *
     * @param id
     *  The id string.
     *
     * @param target
     *  The target string to identify the header or footer node
     *
     * @returns
     *  The simple field node with the specified id and type, or null, if no such
     *  field exists.
     *
     */
    #getMarginalSimpleField(id: string, target: string): JQuery | null {

        // the specified header or footer node
        const marginalNode = this.docModel.getRootNode(target);
        if (!marginalNode) { return null; }

        // the searched complex field with the specified id
        const field = marginalNode.find(FIELD_NODE_SELECTOR).get().find(oneField => getSimpleFieldId(oneField) === id);
        return field ? $(field) : null;
    }

    /**
     * Update of content for simple field: Date
     *
     * @param node
     *  Complex field node
     *
     * @param formatCode
     *  Format of date
     *
     * @param [options]
     *  Optional parameters:
     *  @param [options.time=false]
     *      If it is time format only.
     *  @param [options.setNonModifyingMarker=false]
     *      If set to true, the generated operations get a marker "modifying: false"
     *      that the backend knows, that no new file version must be generated.
     *      This is required after loading a document and the fields are
     *      updated automatically (DOCS-2144).
     */
    #updateDateTimeField(node: NodeOrJQuery, formatCode: string, options?: { time?: boolean; setNonModifyingMarker?: boolean }): void {

        const time = options?.time;
        const localFormatCode = (formatCode && formatCode !== "default") ? formatCode : time ? LOCALE_DATA.shortTime : LOCALE_DATA.shortDate;
        const formattedDate = this.#numberFormatter.formatNow(localFormatCode) || "";

        const operationType = $(node).data("type") as string;
        let operationAttrs = $(node).data("attributes") as PtCharacterAttributeSet;
        const dateFormat = getFieldDateTimeFormat(node);
        const isMarginal = isMarginalNode(node);
        const rootNode = $(this.#selection.getRootNode());
        const target = isMarginal ? getTargetContainerId(rootNode) : "";
        const startPos = getOxoPosition(rootNode, node);

        if (!operationAttrs && isFieldNode(node)) {
            operationAttrs = $(node).children("span").data("attributes") as PtCharacterAttributeSet;
        }

        if (!startPos) {
            globalLogger.error("simpleField.updateDateTimeField(): Wrong start postition: ", startPos);
            return;
        }

        if (this.#simpleFieldContentChanged(formattedDate, node)) {
            this.docModel.getUndoManager().enterUndoGroup(() => {
                if (isMarginal) {
                    this.#pageLayout.enterHeaderFooterEditMode(rootNode);
                    this.#selection.setNewRootNode(rootNode);
                }
                if (options?.setNonModifyingMarker) { this.docModel.increaseModifyingCounter(); }
                this.docModel.deleteRange(startPos);

                const operation: InsertFieldOperation = {
                    name: FIELD_INSERT,
                    start: [...startPos],
                    representation: formattedDate,
                    type: operationType,
                };
                if (target) {
                    operation.target = target;
                }
                if (operationAttrs) {
                    operation.attrs = operationAttrs;
                }
                if (dateFormat) {
                    operation.attrs = { character: { field: { dateFormat } } };
                }

                this.docModel.applyOperations(operation);

                if (options?.setNonModifyingMarker) { this.docModel.decreaseModifyingCounter(); }

                this.#selection.setTextSelection(increaseLastIndex(startPos));
            });
        }
    }

    /**
     * Update the content of simple field: FILENAME
     *
     * @param node
     *  Simple field node
     *
     * @param format
     *  Format of the field: Upper case, lower case, first capitalize, all first letters in words capitalized.
     */
    #updateFileNameField(node: NodeOrJQuery, format: string): void {

        let fileName = this.docApp.getFullFileName() || "";
        const rootNode = this.#selection.getRootNode();
        const startPos = getOxoPosition(rootNode, node);
        const operationType = $(node).data("type") as string;
        const operationAttrs = $(node).data("attributes") as PtCharacterAttributeSet;
        const target = isMarginalNode(node) ? getTargetContainerId(rootNode) : "";

        if (format) {
            if (format.includes("Lower")) {
                fileName = fileName.toLowerCase();
            } else if (format.includes("Upper")) {
                fileName = fileName.toUpperCase();
            } else if (format.includes("FirstCap")) {
                fileName = str.capitalizeFirst(fileName.toLowerCase());
            } else if (format.includes("Caps")) {
                fileName = str.capitalizeWords(fileName);
            }
        }

        if (!startPos || !fileName) {
            globalLogger.error("simpleField.updateFileNameField(): Wrong start postition or text: ", startPos, fileName);
            return;
        }

        if (this.#simpleFieldContentChanged(fileName, node)) {
            this.docModel.getUndoManager().enterUndoGroup(() => {
                this.docModel.deleteRange(startPos);

                const operation: InsertFieldOperation = {
                    name: FIELD_INSERT,
                    start: [...startPos],
                    representation: fileName,
                    type: operationType,
                };
                if (target) {
                    operation.target = target;
                }
                if (operationAttrs) {
                    operation.attrs = operationAttrs;
                }
                this.docModel.applyOperations(operation);

                this.#selection.setTextSelection(increaseLastIndex(startPos));
            });
        }
    }

    /**
     * Update the content of simple field: AUTHOR
     *
     * @param node
     *  Simple field node
     *
     * @param format
     *  Format of the field: Upper case, lower case, first capitalize, all first letters in words capitalized.
     */
    #updateAuthorField(node: NodeOrJQuery, format: string): void {

        let authorName = this.docApp.getClientOperationName();
        const rootNode = this.#selection.getRootNode();
        const startPos = getOxoPosition(rootNode, node);
        const operationType = $(node).data("type") as string;
        const operationAttrs = $(node).data("attributes") as PtCharacterAttributeSet;
        const target = isMarginalNode(node) ? getTargetContainerId(rootNode) : "";

        if (format) {
            if (format.includes("Lower")) {
                authorName = authorName.toLowerCase();
            } else if (format.includes("Upper")) {
                authorName = authorName.toUpperCase();
            } else if (format.includes("FirstCap")) {
                authorName = str.capitalizeFirst(authorName.toLowerCase());
            } else if (format.includes("Caps")) {
                authorName = str.capitalizeWords(authorName);
            }
        }

        if (!startPos || !authorName) {
            globalLogger.error("simpleField.updateAuthorField(): Wrong start postition or text: ", startPos, authorName);
            return;
        }

        if (this.#simpleFieldContentChanged(authorName, node)) {
            this.docModel.getUndoManager().enterUndoGroup(() => {
                this.docModel.deleteRange(startPos);

                const operation: InsertFieldOperation = {
                    name: FIELD_INSERT,
                    start: [...startPos],
                    representation: authorName,
                    type: operationType,
                };
                if (target) {
                    operation.target = target;
                }
                if (operationAttrs) {
                    operation.attrs = operationAttrs;
                }
                this.docModel.applyOperations(operation);

                this.#selection.setTextSelection(increaseLastIndex(startPos));
            });
        }
    }

    /**
     * Update of content for simple field: Number of pages
     *
     * @param node
     *   Simple field node
     *
     * @param format
     *  Format of the number.
     */
    #updateNumPagesField(node: NodeOrJQuery, format: string): void {

        const numPages = this.#pageLayout.getNumberOfDocumentPages();
        const rootNode = this.#selection.getRootNode();
        const startPos = getOxoPosition(rootNode, node);
        const operationType = $(node).data("type") as string;
        let operationAttrs = $(node).data("attributes") as PtCharacterAttributeSet;
        const target = isMarginalNode(node) ? getTargetContainerId(rootNode) : "";

        if (!operationAttrs && isFieldNode(node)) {
            operationAttrs = $(node).children("span").data("attributes") as PtCharacterAttributeSet;
        }

        if (this.docModel.isHeaderFooterEditState()) { // no explicit update of numPages field in header/footer!
            return;
        }

        if (!startPos || !numPages) {
            globalLogger.error("simpleField.updateNumPagesField(): Wrong start postition or text: ", startPos, numPages);
            return;
        }

        const numPagesStr = this.formatPageFieldInstruction(numPages, format, true);

        if (this.#simpleFieldContentChanged(numPagesStr, node)) {
            this.docModel.getUndoManager().enterUndoGroup(() => {
                this.docModel.deleteRange(startPos);

                const operation: InsertFieldOperation = {
                    name: FIELD_INSERT,
                    start: [...startPos],
                    representation: String(numPages),
                    type: operationType,
                };
                if (target) {
                    operation.target = target;
                }
                if (operationAttrs) {
                    operation.attrs = operationAttrs;
                }
                this.docModel.applyOperations(operation);
            });
        }
    }

    /**
     * Update of content for simple field: Page number
     *
     * @param node
     *  Simple field node.
     *
     * @param format
     *  Format of the number.
     */
    #updatePageNumberField(node: NodeOrJQuery, format: string): JPromise | void {
        const number = this.#pageLayout.getPageNumber(node);
        const rootNode = this.#selection.getRootNode();
        const startPos = getOxoPosition(rootNode, node);
        const operationType = $(node).data("type") as string;
        let operationAttrs = $(node).data("attributes") as PtCharacterAttributeSet;
        const target = isMarginalNode(node) ? getTargetContainerId(rootNode) : "";

        if (!operationAttrs && isFieldNode(node)) {
            operationAttrs = $(node).children("span").data("attributes") as PtCharacterAttributeSet;
        }

        if (this.docModel.isHeaderFooterEditState()) { // no explicit update of pageNumber field in header/footer!
            return;
        }

        if (!startPos || !number) {
            globalLogger.error("simpleField.updatePageNumberField(): Wrong start postition or text: ", startPos, number);
            return;
        }

        const numberStr = this.formatPageFieldInstruction(number, format);

        if (this.#simpleFieldContentChanged(numberStr, node)) {
            return this.docModel.getUndoManager().enterUndoGroup(() => {
                this.docModel.deleteRange(startPos);

                const operation: InsertFieldOperation = {
                    name: FIELD_INSERT,
                    start: [...startPos],
                    representation: String(number),
                    type: operationType,
                };
                if (target) {
                    operation.target = target;
                }
                if (operationAttrs) {
                    operation.attrs = operationAttrs;
                }
                this.docModel.applyOperations(operation);
            });
        }
    }
}
