/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { str } from '@/io.ox/office/tk/algorithms';
import { containsNode } from '@/io.ox/office/tk/dom';
import { LOCALE_DATA } from '@/io.ox/office/tk/locale';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { UNITTEST } from "@/io.ox/office/tk/config";

import { convertLengthToHmm, getArrayOption, getBooleanOption, getDomNode, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import * as Op from '@/io.ox/office/textframework/utils/operations';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, decreaseLastIndex, getDOMPosition, getOxoPosition, getParagraphElement, getParagraphLength, increaseLastIndex,
    isNodePositionInsideRange } from '@/io.ox/office/textframework/utils/position';
import BaseField from '@/io.ox/office/textframework/components/field/basefield';

// class ComplexField =====================================================

/**
 * An instance of this class represents the model for all complex fields in the
 * edited document.
 *
 * @param {TextModel} docModel
 *  The model instance.
 */
export default class ComplexField extends BaseField {

    // an object with target string as key and target node as value, the model
    #allFields = {};
    // a counter for the complex field ID
    #fieldID = 0;
    // the selection object
    #selection = null;
    // page layout object
    #pageLayout = null;
    // number formatter object
    #numberFormatter = null;
    // range marker object
    #rangeMarker = null;
    // change track object
    #changeTrack = null;

    constructor(docModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {
            this.#selection = this.docModel.getSelection();
            this.#pageLayout = this.docModel.getPageLayout();
            this.#numberFormatter = this.docModel.numberFormatter;
            this.#rangeMarker = this.docModel.getRangeMarker();
            this.#changeTrack = this.docModel.getChangeTrack();
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Whether the document contains complex fields in the document.
     *
     * @returns {Boolean}
     *  Whether the document contains at least one complex field.
     */
    isEmpty() {
        return _.isEmpty(this.#allFields);
    }

    /**
     * Public method to get all complex fields from model.
     *
     * @returns {Object}
     */
    getAllFields() {
        return this.#allFields;
    }

    /**
     * Public method to get number of all complex fields from model.
     *
     * @returns {Number}
     */
    getAllFieldsCount() {
        return _.size(this.#allFields);
    }

    /**
     * Provides a jQuerified range start marker node.
     *
     * @returns {jQuery|null}
     *  The start marker with the specified id, or null, if no such start marker exists.
     */
    getComplexField(id) {
        const field = this.#allFields[id] || null; // the value saved under the specified id
        return _.isString(field) ? this.#getMarginalComplexField(id, field) : field;
    }

    /**
     * Getter for target container id of marginal complex field node.
     *
     * @param {String} id
     * Id of the field in collection model.
     *
     * @returns {jQuery|null}
     *  Target container id of marginal complex field node.
     */
    getMarginalComplexFieldsTarget(id) {
        const field = this.#allFields[id] || null; // the value saved under the specified id
        return _.isString(field) ? field : null;
    }

    /**
     * Check, whether the specified target string is a valid target for a complex field.
     *
     * @param {String} id
     * Id of the field in collection model.
     *
     * @returns {Boolean}
     *  Whether the specified id string is a valid id for a complex field node.
     */
    isComplexFieldId(id) {
        return this.getComplexField(id) !== null;
    }

    /**
     * Provides a unique ID for the complex field.
     *
     * @returns {String}
     *  A unique id.
     */
    getNextComplexFieldID() {

        // the next free complex field id
        let fieldIdString = 'fld' + this.#fieldID++;

        // checking, if there also exist no range markers with this id
        while (this.#rangeMarker.isUsedRangeMarkerId(fieldIdString)) {
            fieldIdString = 'fld' + this.#fieldID++;
        }

        return fieldIdString;
    }

    /**
     * Provides a unique ID for the bookmark.
     *
     * @param {Number} [increment]
     *  Optional increment value for generation of ID.
     *
     * @returns {String}
     *  A unique id.
     */
    getNextBookmarkId(increment) {

        let bookmarkIdString = 0;
        const allIDs = [];
        const allBookmarks = DOM.getPageContentNode(this.docModel.getNode()).find('.bookmark[bmPos="start"]');
        const locInc = increment || 1;

        _.each(allBookmarks, bmNode => {
            let id = DOM.getBookmarkId(bmNode);
            if (id && id.length > 2) {
                id = parseInt(id.substr(2), 10);
                allIDs.push(id);
            }
        });
        allIDs.sort((a, b) => { return b - a; });
        if (!allIDs.length) { allIDs.push(bookmarkIdString); } // if there were no achors found, add default value
        if (_.isNumber(allIDs[0])) {
            bookmarkIdString = allIDs[0] + locInc;
        }

        return 'bm' + bookmarkIdString;
    }

    /**
     * Provides a unique ID for the anchor.
     *
     * @param {Number} [increment]
     *  Optional increment value for generation of ID.
     *
     * @returns {String}
     *  A unique id.
     */
    getNextAnchorName(increment) {

        let defAnchorValue = 500000000;
        const allNames = [];
        const allBookmarks = DOM.getPageContentNode(this.docModel.getNode()).find('.bookmark[bmPos="start"]');
        const locInc = increment || 1;

        _.each(allBookmarks, bmNode => {
            let anchorName = DOM.getBookmarkAnchor(bmNode);
            if (anchorName) {
                anchorName = anchorName.match(/\d+/g);
                if (anchorName) {
                    if (_.isArray(anchorName)) {
                        anchorName = anchorName[0];
                    }
                    anchorName = parseInt(anchorName, 10);
                    allNames.push(anchorName);
                }
            }
        });
        allNames.sort((a, b) => { return b - a; });
        if (!allNames.length) { allNames.push(defAnchorValue); } // if there were no achors found, add default value
        if (_.isNumber(allNames[0])) { // highest value is sorted at first position
            defAnchorValue = allNames[0] + locInc;
        }

        return '_Toc' + defAnchorValue;
    }

    /**
     * Public method to remove complex field from model.
     *
     * @param {HTMLElement|jQuery} node
     *  One complex field node.
     */
    removeFromComplexFieldCollection(node) {
        this.#removeFromComplexFieldCollection(DOM.getComplexFieldId(node));
    }

    /**
     * Handler for insertComplexField operations. Inserts a complex field node into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new complex field.
     *
     * @param {String} instruction
     *  The instructions of the complex field.
     *
     * @param {Object} attrs
     *  The attributes of the complex field.
     *
     * @param {String} target
     *  The target string corresponding to the specified start position.
     *
     * @returns {Boolean}
     *  Whether the complex field has been inserted successfully.
     */
    insertComplexFieldHandler(start, instruction, attrs, target) {

        // the range marker element
        const complexFieldNode = $('<div>', { contenteditable: false }).addClass('inline ' + DOM.COMPLEXFIELDNODE_CLASS);
        // the text span needed for inserting the range marker node
        const textSpan = this.docModel.prepareTextSpanForInsertion(start, null, target);
        // id of the field, to connect with range markers
        let id = this.docModel.getFieldManager().getComplexFieldIdFromStack();
        // the start marker node, that belongs to this complex field
        let startMarker = null;

        if (!textSpan) { return false; }

        if (!id) {
            // On undo change-tracked complex field,
            // range start and range end are not handled, and it is possible not to fetch id from stack.
            // Try to fetch range marker and id from previous dom node
            id = this.#tryGetIDfromPrevRangeNode(start, target);

            if (!id) {
                globalLogger.error('ComplexField.insertComplexFieldHandler: no id!');
                return false;
            }
        }

        startMarker = this.#rangeMarker.getStartMarker(id);

        if (attrs && attrs.character &&  !_.isUndefined(attrs.character.autoDateField)) {
            complexFieldNode.attr('data-auto-date', false);
        }

        // saving also the id at the complex field node
        complexFieldNode.attr('data-field-id', id);
        complexFieldNode.data('fieldInstruction', instruction);

        // inserting the complex field node into the DOM (always using the text span at the valid position)
        complexFieldNode.insertAfter(textSpan);

        if (startMarker && getDomNode($(textSpan).prev()) === getDomNode(startMarker)) {
            $(textSpan).remove(); // no text span between start marker node and complex field
        } else {
            globalLogger.log('ComplexField.insertComplexFieldHandler: critical rangemarker start position.');
            // -> TODO for fixing task 53286: This needs to be handled as error, after filter made changes for 53286
            // globalLogger.error('ComplexField.insertComplexFieldHandler: invalid position of text span, rangemarker not valid!');
            // return false;
        }

        // setting the (change track) attributes
        if (_.isObject(attrs)) { this.docModel.characterStyles.setElementAttributes(complexFieldNode, attrs); }

        // adding inherited character attributes (before assigning the passed attributes)
        this.docModel.inheritInlineCharacterAttributes(complexFieldNode);

        // it might be necessary to transfer the hard character attributes to a previous empty text span (DOCS-3244)
        if (attrs && attrs.character) { this.docModel.checkTransferOfInlineCharacterAttributes(complexFieldNode, attrs.character); }

        // adding the complex field node in the collection of all complex fields
        // -> this also needs to be done after loading document from fast load or using local storage
        this.#addIntoComplexFieldCollection(complexFieldNode, id, target);

        this.#markComplexFieldForHighlighting(complexFieldNode, id);

        // updating the paragraph
        if (this.docModel.isImportFinished()) { this.docModel.implParagraphChangedSync(complexFieldNode.parent()); }

        // handling the global counter
        this.#updateFieldID(id);

        return true;
    }

    /**
     * Handler for updateComplexField operation.
     * Replaces original complex field DOM node with new one, with new instruction.
     *
     * @param {Number[]} start
     *  The logical start position for the complex field.
     *
     * @param {String} instruction
     *  The instructions of the complex field.
     *
     * @param {Object} attrs
     *  The attributes of the complex field.
     *
     * @param {String} target
     *  The target string corresponding to the specified start position.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the complex field has been updated successfully.
     */
    updateComplexFieldHandler(start, instruction, attrs, target, external) {

        const undoManager = this.docModel.getUndoManager();
        let undoOperation;
        let redoOperation;
        let complexFieldNode = getDOMPosition(this.docModel.getRootNode(target), start, true);

        if (!complexFieldNode || !complexFieldNode.node) {
            globalLogger.error('complexfield.updateComplexFieldHandler(): unable to fetch complex field node!');
            return false;
        }
        complexFieldNode = complexFieldNode.node;

        const id = DOM.getComplexFieldId(complexFieldNode);
        const oldInstruction = DOM.getComplexFieldInstruction(complexFieldNode);

        if (!id || !oldInstruction) {
            globalLogger.error('complexfield.updateComplexFieldHandler(): unable to fetch id or instruction!');
            return false;
        }
        // if there is data from datepicker, reset with new value of current date
        if ($(complexFieldNode).data('datepickerValue')) {
            $(complexFieldNode).data('datepickerValue', new Date());
        }
        // saving also the id at the complex field node
        $(complexFieldNode).attr('data-field-id', id);

        if (attrs && attrs.character && !_.isUndefined(attrs.character.autoDateField)) {
            $(complexFieldNode).attr('data-auto-date', attrs.character.autoDateField); // special case: for performance reasons, this is not handled in character styles.
            this.docModel.characterStyles.setElementAttributes(complexFieldNode, attrs); // This is needed to remove property set in SetAttributes operation, in data of the element!

            // create the undo action
            if (undoManager.isUndoEnabled() && !external) {
                undoOperation = { name: Op.COMPLEXFIELD_UPDATE, start: _.copy(start), attrs: { character: { autoDateField: !attrs.character.autoDateField } }, instruction: oldInstruction };
                redoOperation = { name: Op.COMPLEXFIELD_UPDATE, start: _.copy(start), attrs: { character: { autoDateField: attrs.character.autoDateField } }, instruction: oldInstruction };
                if (target) {
                    undoOperation.target = target;
                    redoOperation.target = target;
                }
                undoManager.addUndo(undoOperation, redoOperation);
            }
        }

        if (instruction) {
            $(complexFieldNode).data('fieldInstruction', instruction);

            // create the undo action
            if (undoManager.isUndoEnabled() && !external) {
                undoOperation = { name: Op.COMPLEXFIELD_UPDATE, start: _.copy(start), attrs: {}, instruction: oldInstruction };
                redoOperation = { name: Op.COMPLEXFIELD_UPDATE, start: _.copy(start), attrs: {}, instruction };
                if (target) {
                    undoOperation.target = target;
                    redoOperation.target = target;
                }
                undoManager.addUndo(undoOperation, redoOperation);
            }
        }

        // this.#markComplexFieldForHighlighting(complexFieldNode, id);

        // external special fields must be updated, after a change of page number type (66918)
        this.#checkForSpecialFieldUpdate($(complexFieldNode), target, external);

        return true;
    }

    /**
     * Inserting a complex field. This function generates the operations for the ranges and
     * the complex field node. It does not modify the DOM. This function is only executed in
     * the client with edit privileges, not in remote clients.
     *
     * @param {String} fieldType
     *
     * @param {String} fieldFormat
     *
     */
    insertComplexField(fieldType, fieldFormat) {

        return this.docModel.getUndoManager().enterUndoGroup(() => {

            const doInsertComplexField = () => {

                const start = this.#selection.getStartPosition();
                const fieldStart = increaseLastIndex(start);
                const textStart = increaseLastIndex(fieldStart);
                let rangeEndStart = null;
                const generator = this.docModel.createOperationGenerator();
                const operation = {};
                let insertTextOperation = null;
                let insertRangeOperation = null;
                // target for operation - if exists, it's for ex. header or footer
                const target = this.docModel.getActiveTarget();
                let representation = null;
                let instruction = null;

                this.docModel.doCheckImplicitParagraph(start);

                if (fieldType) {
                    operation.start = fieldStart;

                    [representation, instruction] = this.#getComplexFieldRepresentationAndInstruction(fieldType, fieldFormat);

                    if (this.isCurrentDate(fieldType) || this.isCurrentTime(fieldType)) {
                        operation.attrs = operation.attrs || {};
                        operation.attrs.character = operation.attrs.character || {};
                        operation.attrs.character = { autoDateField: false };
                    }

                    if (representation.length) {
                        operation.instruction = instruction;
                        this.docModel.extendPropertiesWithTarget(operation, target);

                        insertTextOperation = { start: _.copy(textStart), text: representation };

                        // modifying the attributes, if changeTracking is activated
                        if (this.#changeTrack.isActiveChangeTracking()) {
                            operation.attrs = operation.attrs || {};
                            operation.attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
                            insertTextOperation.attrs = { changes: { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null } };
                        }

                        insertRangeOperation = { start: _.copy(start), type: 'field', position: 'start' };

                        // adding character attributes for the first character in the paragraph (DOCS-4211)
                        const additionalCharacterAttrs = this.docModel.collectCharacterAttributesForInsertOperation(this.docModel.getCurrentRootNode(target), start);
                        if (additionalCharacterAttrs) {
                            insertRangeOperation.attrs = {};
                            insertRangeOperation.attrs.character = additionalCharacterAttrs;
                        }

                        generator.generateOperation(Op.RANGE_INSERT, insertRangeOperation);
                        generator.generateOperation(Op.COMPLEXFIELD_INSERT, operation);
                        generator.generateOperation(Op.TEXT_INSERT, insertTextOperation);
                        rangeEndStart = increaseLastIndex(textStart, representation.length);
                        generator.generateOperation(Op.RANGE_INSERT, { start: _.copy(rangeEndStart), type: 'field', position: 'end' });

                        // setting change track information at range marker nodes
                        if (this.#changeTrack.isActiveChangeTracking()) {
                            generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(start), attrs: { changes: _.copy(operation.attrs.changes, true) } });
                            generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(rangeEndStart), attrs: { changes: _.copy(operation.attrs.changes, true) } });
                        }

                        this.docModel.applyOperations(generator);

                        this.#selection.setTextSelection(increaseLastIndex(rangeEndStart));
                    } else {
                        globalLogger.warn('complexField.insertComplexField(): representation missing!');
                    }
                }
            };

            if (this.#selection.hasRange()) {
                return this.docModel.deleteSelected()
                    .done(() => {
                        doInsertComplexField();
                    });
            }

            doInsertComplexField();
            return $.when();
        });
    }

    /**
     * Creates and applies operations for generating Table of Contents as complex field.
     *
     * @param {String} fieldFormat
     *
     * @param {Object} options
     *  @param {String} options.tabStyle
     *      Style of the tabstops in table of contents
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the insertion of table of contents
     *  ended. It is rejected, if the dialog has been canceled.
     */
    insertTOCField(fieldFormat, options) {
        return this.#insertTableOfContents(fieldFormat, options);
    }

    /**
     * Creates and applies operations for updating Table of Contents as complex field.
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String|Array} instruction
     *  Format of the field with switches. Can be one string, or array of strings.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the insertion of table of contents
     *  ended. It is rejected, if the dialog has been canceled.
     */
    updateTOCField(node, instruction) {
        const dividedData = this.cleanUpAndExtractType(instruction);
        instruction = dividedData.instruction;
        return this.#updateTableOfContents(node, instruction);
    }

    /**
     * Public method that updates format of selected complex field.
     *
     * @param {String} fieldType
     *  Type of the field.
     * @param {String} fieldFormat
     *  New format that field will have.
     * @param {Array} fieldNodePos
     *  Oxo position of complex field node itself, between range markers.
     * @param {String} fieldId
     *  Id of updated field
     * @param {String} oldInstruction
     *
     * @returns {jQuery.Promise}
     */
    updateComplexFieldFormat(fieldType, fieldFormat, fieldNodePos, fieldId, oldInstruction) {

        const generator = this.docModel.createOperationGenerator();
        const operation = {};
        const target = this.docModel.getActiveTarget();
        let representation = null;
        let instruction = null;
        let insertTextOperation = null;
        const textStart = increaseLastIndex(fieldNodePos);
        const startPos = fieldNodePos;
        const rangeEndNode = this.#rangeMarker.getEndMarker(fieldId);

        if (!rangeEndNode) {
            globalLogger.error('complexField.updateComplexFieldFormat(): missing range end node for the id: ', fieldId);
            return $.when();
        }
        const endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndNode);

        if (fieldType && fieldNodePos.length) {
            operation.start = fieldNodePos;

            [representation, instruction] = this.#getComplexFieldRepresentationAndInstruction(fieldType, fieldFormat, oldInstruction);

            if (representation.length) {
                operation.instruction = instruction;
                //operation.id = fieldId;
                this.docModel.extendPropertiesWithTarget(operation, target);

                insertTextOperation = { start: _.copy(textStart), text: representation };

                if (startPos && endPos) {
                    return this.docModel.getUndoManager().enterUndoGroup(() => {

                        if (this.#changeTrack.isActiveChangeTracking()) { // insert new field
                            const changeTrackInfo = this.#changeTrack.getChangeTrackInfo();
                            const rangeStartNode = this.#rangeMarker.getStartMarker(fieldId);
                            const startRangePos = getOxoPosition(this.#selection.getRootNode(), rangeStartNode);
                            let startPosNew = _.clone(startRangePos);

                            generator.generateOperation(Op.RANGE_INSERT, { start: _.copy(startPosNew), type: 'field', position: 'start', attrs: { changes: { inserted: changeTrackInfo } } });
                            startPosNew = increaseLastIndex(startPosNew);
                            generator.generateOperation(Op.COMPLEXFIELD_INSERT, { start: _.copy(startPosNew), instruction: operation.instruction, attrs: { changes: { inserted: changeTrackInfo } } });
                            startPosNew = increaseLastIndex(startPosNew);
                            generator.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPosNew), text: representation, attrs: { changes: { inserted: changeTrackInfo } } });
                            startPosNew = increaseLastIndex(startPosNew, representation.length);
                            generator.generateOperation(Op.RANGE_INSERT, { start: _.copy(startPosNew), type: 'field', position: 'end', attrs: { changes: { inserted: changeTrackInfo } } });

                            //  mark old field as removed
                            const lenOffset = 3 + representation.length;
                            const singleParagraphField = _.isEqual(_.initial(startRangePos), _.initial(endPos));

                            if (singleParagraphField) {
                                generator.generateOperation(Op.SET_ATTRIBUTES, { start: increaseLastIndex(startRangePos, lenOffset), end: increaseLastIndex(endPos, lenOffset), attrs: { changes: { removed: changeTrackInfo } } });
                            } else {
                                // iterate over all paragraphs of the field
                                const textIndex = startRangePos.length - 1;
                                const paraIndex = textIndex - 1;
                                const startPara = startRangePos[paraIndex];
                                const lastPara = endPos[paraIndex];
                                let currentPara = startPara;
                                let localStartPos = null;
                                let localEndPos = null;

                                while (currentPara <= lastPara) {
                                    if (currentPara === startPara) { // first paragraph
                                        localEndPos = _.copy(startRangePos);
                                        localEndPos[textIndex] = getParagraphLength(this.#selection.getRootNode(), _.initial(startRangePos)) + lenOffset - 1; // until the end of the paragraph
                                        generator.generateOperation(Op.SET_ATTRIBUTES, { start: increaseLastIndex(startRangePos, lenOffset), end: _.copy(localEndPos), attrs: { changes: { removed: changeTrackInfo } } });
                                    } else if (currentPara === lastPara) { // last paragraph
                                        localStartPos = _.copy(endPos);
                                        localStartPos[textIndex] = 0;
                                        generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(localStartPos), end: _.copy(endPos), attrs: { changes: { removed: changeTrackInfo } } });
                                    } else { // all middle paragraphs
                                        localStartPos = _.copy(endPos);
                                        localStartPos[paraIndex] = currentPara;
                                        localStartPos[textIndex] = 0;
                                        localEndPos = _.copy(localStartPos);
                                        localEndPos[textIndex] = getParagraphLength(this.#selection.getRootNode(), _.initial(localEndPos)) - 1; // the complete paragraph
                                        generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(localStartPos), end: _.copy(localEndPos), attrs: { changes: { removed: changeTrackInfo } } });
                                    }
                                    currentPara++;
                                }
                            }

                            this.docModel.applyOperations(generator);
                            this.#selection.setTextSelection(increaseLastIndex(startPosNew));

                            return $.when();
                        } else {
                            this.#selection.setTextSelection(textStart, endPos);
                            return this.docModel.deleteSelected().done(() => {

                                // create update operation, and update content
                                generator.generateOperation(Op.COMPLEXFIELD_UPDATE, operation);
                                generator.generateOperation(Op.TEXT_INSERT, insertTextOperation);
                                this.docModel.applyOperations(generator);
                                this.#selection.setTextSelection(increaseLastIndex(textStart, representation.length + 1));
                            });
                        }
                    });
                } else {
                    globalLogger.error('complexField.updateDateTimeFieldCx(): Wrong start and end postitions: ', startPos, endPos);
                }
            } else {
                globalLogger.warn('complexField.updateComplexField(): representation missing!');
            }
        }

        return $.when();
    }

    /**
     * After splitting a paragraph, it is necessary, that all complex field nodes in the cloned
     * 'new' paragraph are updated in the collectors.
     *
     * @param {Node|jQuery} para
     *  The paragraph node.
     */
    updateComplexFieldCollector(para) {

        // whether each single node needs to be checked
        let checkMarginal = false;
        // all complex fields inside the paragraph
        let nodeFields = null;

        // not necessary for paragraphs in header or footer -> only the target are stored, not the nodes
        if (DOM.isMarginalNode(para)) { return; }

        // if this is not a paragraph, each single node need to be checked
        checkMarginal = !DOM.isParagraphNode(para);

        nodeFields = $(para).find(DOM.COMPLEXFIELDNODE_SELECTOR);

        // update the complex field nodes in the collection objects
        _.each(nodeFields, oneField => {
            if (!checkMarginal || !DOM.isMarginalNode(oneField)) {
                // simply overwriting the old complex fields with the new values
                this.#addIntoComplexFieldCollection(oneField, DOM.getComplexFieldId(oneField));
            }
        });
    }

    /**
     * After load from local storage or fast load the collectors for the complex fields need to be filled.
     *
     * @param {Boolean} usedFastLoad
     *  Whether the document was loaded with the fast load process.
     */
    refreshComplexFields(usedFastLoad) {

        // the page content node of the document
        const pageContentNode = DOM.getPageContentNode(this.docModel.getNode());
        // all range marker nodes in the document
        const allComplexFieldNodes = pageContentNode.find(DOM.COMPLEXFIELDNODE_SELECTOR);
        // a collector for all marginal template nodes
        const allMargins = this.#pageLayout.getHeaderFooterPlaceHolder().children();
        // the target string node
        let target = '';
        // whether an update of header and footer is required
        let updateMarginalNodes = false;

        // helper function to add a collection of range marker nodes into the model
        const addMarginalNodesIntoCollection = collection => {
            _.each(collection, oneComplexField => {
                this.#addIntoComplexFieldCollection(oneComplexField, DOM.getComplexFieldId(oneComplexField), target);
                this.#updateFieldID(DOM.getComplexFieldId(oneComplexField)); // updating the value for the global field id, so that new fields get an increased number.
                if (usedFastLoad) {
                    this.#removePreviousEmptyTextSpan(oneComplexField);
                    updateMarginalNodes = true;
                }
            });
            this.#pageLayout.replaceAllTypesOfHeaderFooters();
        };

        // reset model
        this.#allFields = {};

        // 1. Search in page content node
        // 2. Search in header/footer template node

        // adding all range marker nodes into the collector (not those from header or footer)
        _.each(allComplexFieldNodes, oneComplexField => {
            if (!DOM.isMarginalNode($(oneComplexField).parent())) {
                this.#addIntoComplexFieldCollection(oneComplexField, DOM.getComplexFieldId(oneComplexField));
                this.#updateFieldID(DOM.getComplexFieldId(oneComplexField)); // updating the value for the global field id, so that new fields get an increased number.
                if (usedFastLoad) { this.#removePreviousEmptyTextSpan(oneComplexField); }
            }
        });

        // adding the range markers that are located inside header or footer (using the template node)
        _.each(allMargins, margin => {

            // a collector for all range marker nodes inside one margin
            const allMarginComplexFieldNodes = $(margin).find(DOM.COMPLEXFIELDNODE_SELECTOR);

            if (allMarginComplexFieldNodes.length > 0) {
                target = DOM.getTargetContainerId(margin);
                addMarginalNodesIntoCollection(allMarginComplexFieldNodes);
            }
        });

        // if empty text spans were removed in header or footer node, an update of the header and
        // footer nodes in the document is required
        if (updateMarginalNodes) {
            // TODO:
            // Refreshing the marginal nodes in the document with the nodes from the template node (not available yet)
        }

        _.each(pageContentNode.find(DOM.RANGEMARKER_STARTTYPE_SELECTOR).filter('[data-range-type="field"]'), rangeStartNode => {
            const attributes = $(rangeStartNode).data('attributes');
            if (attributes && attributes.character && attributes.character.field && attributes.character.field.formFieldType === 'checkBox') {
                $(rangeStartNode).text(attributes.character.field.checked ? '☒' : '☐');
            }
        });
    }

    /**
     * After deleting a complete paragraph, it is necessary, that all complex field nodes
     * in the deleted paragraph are also removed from the model collectors.
     *
     * @param {Node|jQuery} para
     *  The paragraph node.
     */
    removeAllInsertedComplexFields(para) {

        // all range markers inside the paragraph
        const nodeFields = $(para).find(DOM.COMPLEXFIELDNODE_SELECTOR);

        // update the marker nodes in the collection objects
        _.each(nodeFields, field => {

            // the id of the marker node
            const fieldId = DOM.getComplexFieldId(field);

            if (_.isString(fieldId)) {
                this.#removeFromComplexFieldCollection(fieldId);
            }
        });
    }

    /**
     * When pasting content, that contains complex fields, no IDs are required. Not for the
     * complex field and not for the range marker.
     *
     * @param {Object[]} operations
     *  The collection with all paste operations.
     */
    handlePasteOperationIDs(operations) {

        // insertRange operations of type "field" do not need an id
        _.each(operations, operation => {
            if (operation && operation.name === Op.RANGE_INSERT && operation.type === 'field') {
                delete operation.id;
            }
        });
    }

    /**
     * Checks instruction, and depending of extracted field type,
     * updates passed complex field node.
     *
     * @param {jQuery|Node} node
     *  Node that is going to be updated.
     *
     * @param {String} instruction
     *  Field instruction.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after updateTableOfContents is resolved,
     *  or immediately in case of any other syncronosly executed field.
     */
    updateByInstruction(node, instruction) {

        const dividedData = this.cleanUpAndExtractType(instruction);
        const type = dividedData.type;
        const format = dividedData.instruction;
        let promise = $.when();

        if (type) {
            if (this.isNumPages(type)) {
                this.#updateNumPagesFieldCx(node, format);
            } else if (this.isPageNumber(type)) {
                this.#updatePageNumberFieldCx(node, format);
            } else if (this.isCurrentDate(type)) {
                this.#updateDateTimeFieldCx(node, format);
            } else if (this.isCurrentTime(type)) {
                this.#updateDateTimeFieldCx(node, format, { time: true });
            } else if (this.isSaveDate(type)) {
                this.#updateSaveDateFieldCx(node, format);
            } else if (this.isFileName(type)) {
                this.#updateFileNameFieldCx(node, format, this.usePath(instruction));
            } else if (this.isAuthor(type)) {
                this.#updateAuthorFieldCx(node, format);
            } else if (this.isTableOfContents(type)) {
                // this is async and returns promise, next field update needs to run after this promise is resolved
                promise = this.#updateTableOfContents(node, format);
            }
        }
        return promise;
    }

    /**
     * Update date and time fields before running download or print action.
     *
     * @param {jQuery|Node} field
     *  Field to be updated.
     *
     * @param {String} instruction
     *  Instruction for updating.
     */
    updateDateTimeFieldOnDownload(field, instruction) {

        const dividedData = this.cleanUpAndExtractType(instruction);
        const type = dividedData.type;

        instruction = dividedData.instruction;

        if (type && (this.isCurrentDate(type) || this.isCurrentTime(type)) && (!DOM.isMarginalNode(field) || !DOM.isInsideHeaderFooterTemplateNode(this.docModel.getNode(), field))) { // #42093
            this.#updateDateTimeFieldCx(field, instruction, { time: this.isCurrentTime(type) });
        }
    }

    /**
     * Converts normal complex field to special complex field, when field is inside header/footer and has type PAGE.
     * Content of field is detached and inserted inside complex field.
     * Inverse function from this function is restoreSpecialFields.
     *
     * @param {String} fieldId
     *  Id of the field
     *
     * @param {String} marginalTarget
     *  Id of the marginal target node
     *
     * @param {String} type
     *  Type of field: num pages or page number
     */
    convertToSpecialField(fieldId, marginalTarget, type) {

        let innerContent = $();
        const rootNode = this.docModel.getRootNode(marginalTarget);

        // OT: Refreshing with placeholder content required, before the complex field node in the document is modified (OT Tests 39x).
        //     This is only required, if the rootNode is inside the page (not in the placeholder area) and if the rootNode
        //     is not the currently active root node.
        if (this.docApp.isOTEnabled() && !DOM.isMarginalPlaceHolderNode(rootNode.parent()) && (!this.docModel.isHeaderFooterEditState() || (this.docModel.isHeaderFooterEditState() && getDomNode(rootNode) !== getDomNode(this.docModel.getCurrentRootNode())))) {
            this.#pageLayout.replaceAllTypesOfHeaderFooters(); // -> doing this step early, so that the nodes (rangeStart, fieldNode, ...) do not need to be refreshed
        }

        const rangeStart = rootNode.find('[data-range-id="' + fieldId + '"]').filter(DOM.RANGEMARKER_STARTTYPE_SELECTOR);
        const rangeEnd = rootNode.find('[data-range-id="' + fieldId + '"]').filter(DOM.RANGEMARKER_ENDTYPE_SELECTOR);

        if (!rangeStart.length || !rangeEnd.length) {
            globalLogger.error('complexfield.convertToSpecialField(): missing rangeStart or rangeEnd for the field with id: ', fieldId);
            return;
        }

        let fieldNode = null;
        const startPos = getOxoPosition(rootNode, rangeStart);
        const endPos = getOxoPosition(rootNode, rangeEnd);
        let innerLen = _.last(endPos) - _.last(startPos);
        const isNumPages = (/NUMPAGES/i).test(type);
        const className = isNumPages ? 'cx-numpages' : 'cx-page';
        let replacementNode;
        let helperNode;

        if (innerLen > 0) {
            innerLen -= 1;
        } else {
            globalLogger.warn('complexField.convertToSpecialField(): inner length is < 1');
        }

        if (rangeStart.length) {
            fieldNode = rangeStart.next();
            if (!DOM.isComplexFieldNode(fieldNode)) {
                globalLogger.warn('complexField.convertToSpecialField(): cx field node not found after range start!');
                return;
            }
        }

        helperNode = rangeEnd.prev();
        while (helperNode.length && (!DOM.isComplexFieldNode(helperNode) || DOM.getComplexFieldId(helperNode) !== fieldId)) {
            innerContent = innerContent.add(helperNode);
            helperNode = helperNode.prev();
        }
        innerContent.detach().empty();
        fieldNode.append(innerContent).data('length', innerLen).addClass(DOM.SPECIAL_FIELD_NODE_CLASSNAME).addClass(className);

        // the child spans must be valid text spans with text node to enable iteration with 'DOM.iterateTextSpans' (56996)
        _.each(innerContent, node => {
            DOM.ensureExistingTextNodeInSpecialField(node);
        });

        if (!DOM.isMarginalPlaceHolderNode(rootNode.parent())) {
            replacementNode = rootNode.children().clone(true);
            replacementNode.find('[contenteditable="true"]').attr('contenteditable', false);
            this.#pageLayout.getHeaderFooterPlaceHolder().children('[data-container-id="' + marginalTarget + '"]').empty().append(replacementNode);
        }
    }

    /**
     * Restoring special field to normal is required before delete operation.
     *
     * @param {jQuery} specialField
     */
    restoreSpecialField(specialField) {
        this.#restoreSpecialField(specialField);
    }

    /**
     * Check if there are special fields of type page in header, inside given searchNode,
     * and if yes, restore them before deleting them.
     *
     * @param {Node} searchNode
     *  Node which we search for special fields.
     *
     * @param {Number} startOffset
     *  Start offset of given node.
     *
     * @param {Number} endOffset
     *  End offset of given node.
     */
    checkRestoringSpecialFields(searchNode, startOffset, endOffset) {

        if (DOM.isMarginalNode(searchNode) && $(searchNode).find(DOM.SPECIAL_FIELD_NODE_SELECTOR).length) {
            _.each($(searchNode).find(DOM.SPECIAL_FIELD_NODE_SELECTOR), specialField => {
                const fieldPos = getOxoPosition(this.#selection.getRootNode(), specialField, 0);
                if (isNodePositionInsideRange(fieldPos, startOffset, endOffset)) {
                    this.#restoreSpecialField(specialField);
                }
            });
        }
    }

    /**
     * Check if there are special fields in container node, and revert conversion from special field to normal,
     * which is necessary before creating undo operations.
     *
     * @param {jQuery|Node} containerNode
     *  Can be whole header or footer, not restricted to paragraph parts.
     *  For that, see function: checkRestoringSpecialFields
     */
    checkRestoringSpecialFieldsInContainer(containerNode) {
        _.each($(containerNode).find(DOM.SPECIAL_FIELD_NODE_SELECTOR), specialField => {
            this.#restoreSpecialField(specialField);
        });
    }

    /**
     * After cloning headers&footers special page fields have to be updated with proper page number.
     */
    updateCxPageFieldsInMarginals() {

        let $pageContentNode = null;
        const firstHeadNum = 1;
        let lastFootNum = null;
        // first header in doc
        let $firstHeaderFields = null;
        // last footer in doc
        let $lastFooterFields = null;
        let numPagesNodes = null;

        // no special fields in the document, don't make unnecessary loops
        if (this.isEmpty() || !this.#pageLayout.getHeaderFooterPlaceHolder().find(DOM.SPECIAL_FIELD_NODE_SELECTOR).length) { return; }

        // performance: assign variables only if really needed
        $pageContentNode = DOM.getPageContentNode(this.docModel.getNode());
        lastFootNum = this.#pageLayout.getNumberOfDocumentPages();
        // first header in doc - page number always 1
        $firstHeaderFields = this.#pageLayout.getFirstHeaderWrapperNode().children(DOM.HEADER_SELECTOR).find('.cx-page');
        // last footer in doc gets total page count as page number
        $lastFooterFields = this.#pageLayout.getLastFooterWrapperNode().children(DOM.FOOTER_SELECTOR).find('.cx-page');

        _.each($firstHeaderFields, headerField => {
            const $headerField = $(headerField);
            const headerFormat = DOM.getComplexFieldInstruction($headerField);

            // nesting of fields in special fields (page fields in header/footer) is not allowed;
            // first span should not be deleted, to preserve formatting
            $headerField.children().not('span:first').remove();

            $headerField.children().empty().html(this.formatPageFieldInstruction(firstHeadNum, headerFormat));
        });

        _.each($lastFooterFields, footerField => {
            const $footerField = $(footerField);
            const footerFormat = DOM.getComplexFieldInstruction($footerField);

            $footerField.children().not('span:first').remove();
            $footerField.children().empty().html(this.formatPageFieldInstruction(lastFootNum, footerFormat));
        });

        _.each(this.#pageLayout.getPageBreaksCollection(), pageBreak => {

            const $pageBreak = $(pageBreak);
            const headerNumber = $pageBreak.data('page-num') + 1;
            const footerNumber = $pageBreak.data('page-num');
            const $headerFields = $pageBreak.children(DOM.HEADER_SELECTOR).find('.cx-page');
            const $footerFields = $pageBreak.children(DOM.FOOTER_SELECTOR).find('.cx-page');

            _.each($headerFields, headerField => {
                const $headerField = $(headerField);
                const headerFormat = DOM.getComplexFieldInstruction($headerField);

                $headerField.children().not('span:first').remove();

                $headerField.children().empty().html(this.formatPageFieldInstruction(headerNumber, headerFormat));
            });

            _.each($footerFields, footerField => {
                const $footerField = $(footerField);
                const footerFormat = DOM.getComplexFieldInstruction($footerField);

                $footerField.children().not('span:first').remove();
                $footerField.children().empty().html(this.formatPageFieldInstruction(footerNumber, footerFormat));
            });
        });

        // update number of pages fields, which all have same value
        if (this.#pageLayout.getHeaderFooterPlaceHolder().find('.cx-numpages').length) {
            numPagesNodes = $pageContentNode.find(DOM.PAGE_BREAK_SELECTOR).add(this.#pageLayout.getFirstHeaderWrapperNode()).add(this.#pageLayout.getLastFooterWrapperNode()).find('.cx-numpages');
            _.each(numPagesNodes, fieldNode => {
                const $fieldNode = $(fieldNode);
                const fieldFormat = DOM.getComplexFieldInstruction($fieldNode);

                // nesting of fields in special fields (page fields in header/footer) is not allowed;
                // first span should not be deleted, to preserve formatting
                $fieldNode.children().not('span:first').remove();

                $fieldNode.children().empty().html(this.formatPageFieldInstruction(lastFootNum, fieldFormat, true));
            });
        }
    }

    /**
     * Function called after importing document.
     * It loops all fields in collection and:
     *  - marks them for highlighting,
     *  - updates date and time fields
     *  - converts page fields in header&footer to special fields
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.avoidFieldUpdate=false]
     *      If set to true, the fields must not be updated. This is required
     *      after load of document, if other users have the document already
     *      opened.
     *  @param {Boolean} [options.setNonModifyingMarker=false]
     *      If set to true, the generated operations get a marker 'modifying: false'
     *      that the backend knows, that no new file version must be generated.
     *      This is required after loading a document and the fields are
     *      updated automatically (DOCS-2144).
     */
    updateDateTimeAndSpecialFields(options) {

        let updatePageFields = false;
        let returnOriginalCursorPos = false;
        const startPos = this.#selection.getStartPosition();
        const endPos = this.#selection.getEndPosition();

        _.each(this.#allFields, (entry, fieldId) => {

            let field = this.getComplexField(fieldId);
            let instruction = DOM.getComplexFieldInstruction(field);
            const dividedData = this.cleanUpAndExtractType(instruction);
            const type = dividedData.type;
            let format = null;
            const isMarginalField = DOM.isMarginalNode(field);

            instruction = dividedData.instruction;

            this.#markComplexFieldForHighlighting(field, fieldId);

            if (type) {
                if (this.isSaveDate(type)) {
                    format = instruction;
                    if (!isMarginalField || !DOM.isInsideHeaderFooterTemplateNode(this.docModel.getNode(), field)) {
                        // if previously updated field is marginal, and this is not, leave header edit state
                        this.#selection.switchToValidRootNode(field);

                        // for marginal fields an update might be required, if they are removed from DOM after 'switchToValidRootNode'
                        if (isMarginalField && !containsNode(this.docModel.getCurrentRootNode(), field)) { field = this.getComplexField(fieldId); }

                        if (!getBooleanOption(options, 'avoidFieldUpdate', false)) {
                            this.#updateSaveDateFieldCx(field, format);
                        }
                        this.#updateSaveDateFieldCx(field, format);
                        if (this.#pageLayout.isIdOfMarginalNode(entry)) {
                            this.#markComplexFieldForHighlighting(this.getComplexField(fieldId), fieldId);
                            this.#pageLayout.getHeaderFooterPlaceHolder().children('.' + entry).empty().append(this.#selection.getRootNode().children().clone(true));
                        }
                        returnOriginalCursorPos = true;
                    }
                } else if ((this.isCurrentDate(type) || this.isCurrentTime(type)) && this.docApp.isEditable()) {
                    format = instruction;

                    if (DOM.isAutomaticDateField(field) && (!isMarginalField || !DOM.isInsideHeaderFooterTemplateNode(this.docModel.getNode(), field))) { // update only if not fixed field, #42093
                        // if previously updated field is marginal, and this is not, leave header edit state
                        this.#selection.switchToValidRootNode(field);

                        // for marginal fields an update might be required, if they are removed from DOM after 'switchToValidRootNode'
                        if (isMarginalField && !containsNode(this.docModel.getCurrentRootNode(), field)) { field = this.getComplexField(fieldId); }

                        if (!getBooleanOption(options, 'avoidFieldUpdate', false)) { this.#updateDateTimeFieldCx(field, format, { time: this.isCurrentTime(type), setNonModifyingMarker: getBooleanOption(options, 'setNonModifyingMarker', false) }); }
                        if (this.#pageLayout.isIdOfMarginalNode(entry)) {
                            this.#markComplexFieldForHighlighting(this.getComplexField(fieldId), fieldId);
                            this.#pageLayout.getHeaderFooterPlaceHolder().children('.' + entry).empty().append(this.#selection.getRootNode().children().clone(true));
                        }
                        returnOriginalCursorPos = true;
                    }
                } else if (_.isString(entry) && this.#pageLayout.isIdOfMarginalNode(entry) && this.isPageNumber(type)) {
                    if (!DOM.isSpecialField(field)) {
                        this.convertToSpecialField(fieldId, entry, type);
                    }
                    updatePageFields = true;
                }
            } else {
                globalLogger.warn('complexField.updateDateTimeAndSpecialFields: invalid type: ', type);
            }
        });
        if (updatePageFields) {
            // update reference in collection because of replaced node
            this.#pageLayout.headerFooterCollectionUpdate();
            this.#pageLayout.replaceAllTypesOfHeaderFooters(); // complex fields in header/footers are refreshed in this method call, if any
        }
        if (returnOriginalCursorPos) {
            // if there was update of date fields, return cursor to first document position
            if (this.docModel.isHeaderFooterEditState()) {
                this.#pageLayout.leaveHeaderFooterEditMode();
            }
            this.docModel.setActiveTarget();
            this.#selection.setNewRootNode(this.docModel.getNode());
            this.#selection.setTextSelection(startPos, endPos);
        }
    }

    /**
     * Public method for updating current page number field(s) in given node.
     *
     * @param {jQuery} $node
     * @param {Boolean} isHeader
     *  If node is header or not.
     * @param {Number} pageCount
     *  Number of total pages in document.
     */
    updatePageNumInCurrentNodeCx($node, isHeader, pageCount) {

        if (isHeader) {
            if (DOM.isHeaderWrapper($node.parent())) {
                this.updatePageNumInCurrentMarginalCx($node, isHeader, 1);
            } else {
                this.updatePageNumInCurrentMarginalCx($node, isHeader);
            }
        } else {
            if (DOM.isFooterWrapper($node.parent())) {
                this.updatePageNumInCurrentMarginalCx($node, isHeader, pageCount);

            } else {
                this.updatePageNumInCurrentMarginalCx($node, isHeader);
            }
        }
    }

    updatePageNumInCurrentMarginalCx($node, isHeader, pageNum) {

        _.each($node.find('.cx-page'), field => {

            const $field = $(field);
            const format = DOM.getComplexFieldInstruction($field);
            const pageNumber = pageNum || ($field.parentsUntil('.pagecontent', '.page-break').data('page-num') + (isHeader ? 1 : 0));

            $field.children().empty().html(this.formatPageFieldInstruction(pageNumber, format));
        });
    }

    /**
     * Public method for updating number of pages field(s) in given node.
     *
     * @param {jQuery} $node
     * @param {Number} pageCount
     *  Number of total pages in document.
     */
    updatePageCountInCurrentNodeCx($node, pageCount) {

        _.each($node.find('.cx-numpages'), fieldNode => {

            const $fieldNode = $(fieldNode);
            const fieldFormat = DOM.getComplexFieldInstruction($fieldNode);

            $fieldNode.children().empty().html(this.formatPageFieldInstruction(pageCount, fieldFormat, true));
        });
    }

    /**
     * Update date field with value from popup, datepicker or input field.
     *
     * @param {jQuery} node
     *   Field node that is going to be updated
     *
     * @param {String} fieldValue
     *   Value with which will field be updated.
     */
    updateDateFromPopupValue(node, fieldValue) {

        const formattedDate = fieldValue;
        const id = DOM.getComplexFieldMemberId(node);
        const isMarginal = DOM.isMarginalNode(node);
        const rootNode = isMarginal ? DOM.getClosestMarginalTargetNode(node) : this.#selection.getRootNode();
        const startPos = increaseLastIndex(getOxoPosition(rootNode, node));
        const rangeEndNode = this.#rangeMarker.getEndMarker(id);

        if (!rangeEndNode) {
            globalLogger.error('complexField.updateDateFromPopupValue(): missing range end node for the id: ', id);
            return;
        }

        const endPos = getOxoPosition(rootNode, rangeEndNode);

        if (startPos && endPos) {
            return this.docModel.getUndoManager().enterUndoGroup(() => {
                this.#selection.setTextSelection(startPos, endPos);
                return this.docModel.deleteSelected().done(() => {
                    this.docModel.insertText(formattedDate, startPos);
                    this.#selection.setTextSelection(increaseLastIndex(startPos, formattedDate.length + 1));
                });
            });
        } else {
            globalLogger.error('complexField.updateDateTimeFieldCx(): Wrong start and end postitions: ', startPos, endPos);
        }
    }

    /**
     * Public method to mark all nodes that belong to complex field,
     * and are inside range start and range end of a field.
     *
     * @param {HTMLElement|jQuery} field
     *  Complex field node.
     *
     * @param {String} id
     *  Id of processed field.
     */
    markComplexFieldForHighlighting(field, id) {
        this.#markComplexFieldForHighlighting(field, id);
    }

    /**
     * Public method to mark all complex fields contents after undo/redo.
     */
    markAllFieldsAfterUndo() {
        _.each(this.#allFields, (node, fieldId) => {
            this.#markComplexFieldForHighlighting(node, fieldId);
        });
    }

    // private methods ----------------------------------------------------

    /**
     * Registering one complex field in the collection (the model).
     *
     * @param {HTMLElement|jQuery} field
     *  One complex field node.
     *
     * @param {String} id
     *  The unique id of the complex field node.
     *
     * @param {String} [target]
     *  The target, where the complex field is located.
     */
    #addIntoComplexFieldCollection(field, id, target) {

        // the complex field node in jQuery format
        const $field = $(field);
        // whether the complex field is inside header or footer
        // -> in this case it is useless to save the node itself, but saving
        //    the target allows to find the range with specific id fast
        const isMarginalId = target && this.#pageLayout.isIdOfMarginalNode(target);

        // adding the node also into the type specific collector
        this.#allFields[id] = isMarginalId ? target : $field;
    }

    /**
     * Removing one complex field with specified id from the collection (the model).
     *
     * @param {String} id
     *  The unique id of the complex field node.
     */
    #removeFromComplexFieldCollection(id) {
        delete this.#allFields[id];
    }

    /**
     * Finding in a header or footer specified by the target the complex field
     * with the specified id.
     *
     * @param {String} id
     *  The id string.
     *
     * @param {String} target
     *  The target string to identify the header or footer node
     *
     * @param {Number} [index]
     *  Cardinal number of target node in the document from top to bottom. If passed,
     *  forces to return node with that index in document, from top to bottom.
     *
     * @returns {jQuery|null}
     *  The range marker node with the specified id and type, or null, if no such
     *  range marker exists.
     */
    #getMarginalComplexField(id, target, index) {

        // the specified header or footer node
        let marginalNode = null;
        // the searched complex field with the specified id
        let field = null;

        marginalNode = this.docModel.getRootNode(target, index);

        if (marginalNode) {
            field = _.find(marginalNode.find(DOM.COMPLEXFIELDNODE_SELECTOR), field => { return DOM.getComplexFieldId(field) === id; });
        }

        return field ? $(field) : null;
    }

    /**
     * Before a field is updated, it can be checked, whether its content has changed.
     * Operations for deleting old content and inserting new content should only be
     * generated, if the content has really changed (DOCS-1558).
     *
     * Info: The change of a complex field is determined by a comparison of the old
     *       and the new string. Therefore any string attributes are ignored and a
     *       field update does not lead to a removal of additionally set attributes
     *       like in Word, if the string itself did not change.
     *
     * @param {String} newString
     *  The new string value that shall be inserted into the complex field.
     *
     * @param {Number[]} startPos
     *  The logical start position of the complex field.
     *
     * @param {Number[]} endPos
     *  The logical end position of the complex field.
     *
     * @returns {Boolean}
     *  Whether the string inside the complex field is modified.
     */
    #fieldChanged(newString, startPos, endPos) {

        const oldStartPosition = this.#selection.getStartPosition();
        const oldEndPosition = this.#selection.getEndPosition();

        this.#selection.setTextSelection(startPos, endPos);
        const oldString = this.#selection.getSelectedText();

        const changed = oldString !== newString;

        // restore the selection
        this.#selection.setTextSelection(oldStartPosition, oldEndPosition);

        return changed;
    }

    /**
     * Private method to mark all nodes that belong to complex field,
     * or, so to say, are inside range start and range end of a field.
     *
     * @param {HTMLElement|jQuery} cxField
     *  One range marker node.
     *
     * @param {String} id
     *  Id of processed field.
     */
    #markComplexFieldForHighlighting(cxField, id) {

        let $node = $(cxField).next();
        let $helperNode;

        if (!this.#rangeMarker.getEndMarker(id)) {
            return; // no range marker inserted yet, return
        }

        while ($node.length && (!DOM.isRangeMarkerEndNode($node) || DOM.getRangeMarkerId($node) !== id)) { // #42084, #42674
            if (!$node.data('fieldId')) {
                $node.addClass(DOM.COMPLEXFIELDMEMBERNODE_CLASS + ' complex' + id).data('fieldId', id);
            }
            $helperNode = $node.next();
            if (!$helperNode.length) {
                $helperNode = $node.parent().next().children().first(); // if field is in more than one paragraph, search for range-end in next p
                if (!$helperNode.length) {
                    globalLogger.warn('complexField.markComplexFieldForHighlighting(): rangeEnd not found!');
                    return;
                } else {
                    $node = $helperNode;
                }
            } else {
                $node = $helperNode;
            }
        }
    }

    /**
     * Removing empty text spans between the range marker start node and the
     * following complex field node.
     *
     * @param {HTMLElement|jQuery} field
     *  The complex field node.
     */
    #removePreviousEmptyTextSpan(field) {

        // the node preceding the complex field node
        const precedingNode = $(field).prev();

        if (precedingNode.length > 0 && precedingNode.prev().length > 0 && DOM.isEmptySpan(precedingNode) && DOM.isRangeMarkerStartNode(precedingNode.prev())) {
            precedingNode.remove();
        }
    }

    /**
     * Helper function to keep the global this.#fieldID up-to-date. After inserting
     * a new complex field or after loading from local storage, this number needs to be updated.
     * Then a new complex field can be inserted from the client with a valid and unique id.
     *
     * @param {String} id
     *  The id of the complex field node.
     */
    #updateFieldID(id) {

        // does the id end with a number?
        // -> then the this.#fieldID needs to be checked

        // resulting array of the regular expression
        const matches = /(\d+)$/.exec(id);
        // the number value at the end of the id
        let number = 0;

        if (_.isArray(matches)) {
            number = parseInt(matches[1], 10);

            if (number >= this.#fieldID) {
                this.#fieldID = number + 1;
            }
        }
    }

    /**
     * Update of content for complex field: Page number
     *
     * @param {jQuery|Node} node
     *  Complex field node.
     *
     * @param {String} format
     *  Format of the number.
     */
    #updatePageNumberFieldCx(node, format) {

        let number = this.#pageLayout.getPageNumber(node);
        const id = DOM.getComplexFieldMemberId(node);
        let startPos = null;
        let endPos = null;
        const rangeEndNode = this.#rangeMarker.getEndMarker(id);

        if (this.docModel.isHeaderFooterEditState()) { // no explicit update of pageNumber field in header/footer!
            return;
        }
        if (!rangeEndNode) {
            globalLogger.error('complexField.updatePageNumberFieldCx(): missing range end node for the id: ', id);
            return;
        }

        startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndNode);

        number = this.formatPageFieldInstruction(number, format);

        if (startPos && endPos && number) {
            if (this.#fieldChanged(number, startPos, endPos)) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);
                    // disabling changeTracking temporarily, as page number update is not tracked
                    this.#changeTrack.setChangeTrackSupported(false);
                    return this.docModel.deleteSelected().done(() => {
                        this.docModel.insertText(number, startPos);
                        this.#selection.setTextSelection(increaseLastIndex(startPos, number.length + 1));
                        this.#changeTrack.setChangeTrackSupported(true); // activating change track support back
                    });
                });
            }
        } else {
            globalLogger.error('complexField.updatePageNumberFieldCx(): Wrong start end postitions, or text: ', startPos, endPos, number);
        }
    }

    /**
     * Update of content for complex field: Number of pages.
     *
     * @param {jQuery|Node} node
     *   Complex field node
     *
     * @param {String} format
     *  Format of the number.
     */
    #updateNumPagesFieldCx(node, format) {

        let numPages = this.#pageLayout.getNumberOfDocumentPages();
        const id = DOM.getComplexFieldMemberId(node);
        let startPos = null;
        let endPos = null;
        const rangeEndNode = this.#rangeMarker.getEndMarker(id);

        if (this.docModel.isHeaderFooterEditState()) { // no explicit update of numPages field in header/footer!
            return;
        }
        if (!rangeEndNode) {
            globalLogger.error('complexField.updateNumPagesFieldCx(): missing range end node for the id: ', id);
            return;
        }

        startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndNode);

        numPages = this.formatPageFieldInstruction(numPages, format, true);

        // TODO: Nested complex fields

        if (startPos && endPos && numPages) {
            if (this.#fieldChanged(numPages, startPos, endPos)) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);
                    return this.docModel.deleteSelected().done(() => {
                        this.docModel.insertText(numPages, startPos);
                        this.#selection.setTextSelection(increaseLastIndex(startPos, numPages.length + 1));
                    });
                });
            }
        } else {
            globalLogger.error('complexField.updateNumPagesFieldCx(): Wrong start end postitions, or text: ', startPos, endPos, numPages);
        }
    }

    /**
     * Update of content for complex field: SaveDate
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String} formatCode
     *  Format of date
     *
     */
    #updateSaveDateFieldCx(node, formatCode) {

        const date = this.getSaveDate();
        const localeFormatCode = (formatCode && formatCode !== 'default') ? formatCode : LOCALE_DATA.shortDate + ' ' + LOCALE_DATA.longTime;
        const replacedLclFormat = localeFormatCode.replace(/'/g, '"'); // single quotes are replaced with double
        const formattedDate = this.#numberFormatter.formatValue(replacedLclFormat, date).text;
        const id = DOM.getComplexFieldMemberId(node);
        const startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        const rangeEndMarker = this.#rangeMarker.getEndMarker(id);

        if (!rangeEndMarker) {
            globalLogger.error('complexField.updateSaveDateFieldCx(): missing range end node for the id: ', id);
            return;
        }

        const endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndMarker);

        if (startPos && endPos) {
            if (this.#fieldChanged(formattedDate, startPos, endPos)) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);
                    this.docModel.increaseModifyingCounter();
                    return this.docModel.deleteSelected().done(() => {
                        this.docModel.insertText(formattedDate, startPos);
                        this.docModel.decreaseModifyingCounter();
                        this.#selection.setTextSelection(increaseLastIndex(startPos, formattedDate.length + 1));
                    });
                });
            }
        } else {
            globalLogger.error('complexField.updateSaveDateFieldCx(): Wrong start and end postitions: ', startPos, endPos);
        }
    }

    /**
     * Update of content for complex field: Date
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String} formatCode
     *  Format of date
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.time=false]
     *      If it's time format only.
     *  @param {Boolean} [options.setNonModifyingMarker=false]
     *      If set to true, the generated operations get a marker 'modifying: false'
     *      that the backend knows, that no new file version must be generated.
     *      This is required after loading a document and the fields are
     *      updated automatically (DOCS-2144).
     */
    #updateDateTimeFieldCx(node, formatCode, options) {

        const time = getBooleanOption(options, 'time', false);
        const localeFormatCode = (formatCode && formatCode !== 'default') ? formatCode : time ? LOCALE_DATA.shortTime : LOCALE_DATA.shortDate;
        const replacedLclFormat = localeFormatCode.replace(/'/g, '"'); // single quotes are replaced with double
        const formattedDate = this.#numberFormatter.formatNow(replacedLclFormat);
        const id = DOM.getComplexFieldMemberId(node);
        const startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        const setNonModifiyingMarker = getBooleanOption(options, 'setNonModifyingMarker');
        const rangeEndMarker = this.#rangeMarker.getEndMarker(id);

        if (!rangeEndMarker) {
            globalLogger.error('complexField.updateDateTimeFieldCx(): missing range end node for the id: ', id);
            return;
        }

        const endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndMarker);

        if (startPos && endPos) {
            if (this.#fieldChanged(formattedDate, startPos, endPos)) {
                if ($(node).data('datepickerValue')) {
                    $(node).data('datepickerValue', new Date());
                }
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);
                    if (setNonModifiyingMarker) { this.docModel.increaseModifyingCounter(); }
                    return this.docModel.deleteSelected().done(() => {
                        this.docModel.insertText(formattedDate, startPos);
                        if (setNonModifiyingMarker) { this.docModel.decreaseModifyingCounter(); }
                        this.#selection.setTextSelection(increaseLastIndex(startPos, formattedDate.length + 1));
                    });
                });
            }
        } else {
            globalLogger.error('complexField.updateDateTimeFieldCx(): Wrong start and end postitions: ', startPos, endPos);
        }

        // TODO: Nested complex fields
    }

    /**
     * Update the content of complex field: FILENAME
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String} format
     *  Format of the field: Upper case, lower case, first capitalize, all first letters in words capitalized.
     *
     */
    #updateFileNameFieldCx(node, format, usePath) {

        let fileName = this.docApp.getFullFileName({ path: usePath });
        const id = DOM.getComplexFieldMemberId(node);
        const startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        const rangeEndNode = this.#rangeMarker.getEndMarker(id);

        if (!rangeEndNode) {
            globalLogger.error('complexField.updateFileNameFieldCx(): missing range end node for the id: ', id);
            return;
        }

        const endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndNode);

        if (format && fileName) {
            if ((/Lower/).test(format)) {
                fileName = fileName.toLowerCase();
            } else if ((/Upper/).test(format)) {
                fileName = fileName.toUpperCase();
            } else if ((/FirstCap/).test(format)) {
                fileName = str.capitalizeFirst(fileName.toLowerCase());
            } else if ((/Caps/).test(format)) {
                fileName = str.capitalizeWords(fileName.toLowerCase());
            }
        }

        // TODO: Nested complex fields

        if (startPos && endPos && fileName) {
            if (this.#fieldChanged(fileName, startPos, endPos)) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);
                    return this.docModel.deleteSelected().done(() => {
                        this.docModel.insertText(fileName, startPos);
                        this.#selection.setTextSelection(increaseLastIndex(startPos, fileName.length + 1));
                    });
                });
            }
        } else {
            globalLogger.error('complexField.updateFileNameFieldCx(): Wrong start end postitions, or text: ', startPos, endPos, fileName);
        }
    }

    /**
     * Update the content of complex field: AUTHOR
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String} format
     *  Format of the field: Upper case, lower case, first capitalize, all first letters in words capitalized.
     *
     */
    #updateAuthorFieldCx(node, format) {

        let authorName = this.docApp.getClientOperationName();
        const id = DOM.getComplexFieldMemberId(node);
        const startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        const rangeEndNode = this.#rangeMarker.getEndMarker(id);

        if (!rangeEndNode) {
            globalLogger.error('complexField.updateAuthorFieldCx(): missing range end node for the id: ', id);
            return;
        }

        const endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndNode);

        if (format && authorName) {
            if ((/Lower/).test(format)) {
                authorName = authorName.toLowerCase();
            } else if ((/Upper/).test(format)) {
                authorName = authorName.toUpperCase();
            } else if ((/FirstCap/).test(format)) {
                authorName = str.capitalizeFirst(authorName.toLowerCase());
            } else if ((/Caps/).test(format)) {
                authorName = str.capitalizeWords(authorName.toLowerCase());
            }
        }

        // TODO: Nested complex fields

        if (startPos && endPos && authorName) {
            if (this.#fieldChanged(authorName, startPos, endPos)) {
                return this.docModel.getUndoManager().enterUndoGroup(() => {
                    this.#selection.setTextSelection(startPos, endPos);
                    return this.docModel.deleteSelected().done(() => {
                        this.docModel.insertText(authorName, startPos);
                        this.#selection.setTextSelection(increaseLastIndex(startPos, authorName.length + 1));
                    });
                });
            }
        } else {
            globalLogger.error('complexField.updateAuthorFieldCx(): Wrong start end postitions, or text: ', startPos, endPos, authorName);
        }
    }

    /**
     * Parses field formats for Table of contents and returns set variables in one object
     *
     * @param {Array} formats
     *
     * @returns {Object}
     */
    #getSwitchesFromTOCformats(formats) {

        const returnObj = {
            setAnchorAttribute: false,
            headingLevels: [1, 2, 3, 4, 5, 6, 7, 8, 9], // use page numbers for all 9 levels by default, if not explicitly overwritten by switch
            customSeparator: null,
            forbidenPageNumLevels: [],
            bookmarkName: null
        };

        _.each(formats, format => {

            let patternMatch = null;
            const forbidenPageNumLevels = [];
            let startLevel, endLevel;

            if (_.contains(format, 'h')) { // set anchor char attribute
                returnObj.setAnchorAttribute = true;
            }
            if ((/^o "/).test(format)) { // use only specified heading levels
                format = _.isString(format) ? format.replace(/"/g, '') : '';
                patternMatch = format.match(/\d/g);
                if (patternMatch && patternMatch.length === 2) {
                    const headingLevels = [];
                    for (let i = parseInt(patternMatch[0], 10); i <= parseInt(patternMatch[1], 10); i++) {
                        headingLevels.push(i);
                    }
                    returnObj.headingLevels = headingLevels;
                }
            }
            if ((/^p "/).test(format)) { // use custom separators instead of tabs
                format = (format).match(/"(.*?)"/);
                returnObj.customSeparator = (_.isArray(format) && format.length > 1) ? format[1] : null;
            }
            if ((/^n/).test(format)) { // whether to display page numbers or not, and which levels to forbid
                patternMatch = format.match(/\d/g);
                startLevel = (patternMatch && patternMatch.length === 2) ? parseInt(patternMatch[0], 10) : 1;
                endLevel = (patternMatch && patternMatch.length === 2) ? parseInt(patternMatch[1], 10) : 10;

                for (let j = startLevel; j <= endLevel; j++) {
                    forbidenPageNumLevels.push(j);
                }
                returnObj.forbidenPageNumLevels = forbidenPageNumLevels;
            }
            if ((/^b/).test(format)) { // create toc from bookmarks areas
                if (format.length > 2) {
                    returnObj.bookmarkName = format.substr(2);
                }
            }
        });

        return returnObj;
    }

    /**
     * Returns unique jQuery collection of all paragraphs from top to bottom,
     * inside bookmark range, or if left out, inside whole document.
     *
     * @param {String} [bookmarkName]
     *  Name of the bookmark inside which to fetch paragraphs
     *
     * @returns {jQuery}
     */
    #fetchAllParagraphs(bookmarkName) {

        let docParagraphs = $();
        const pageContentNode = DOM.getPageContentNode(this.docModel.getNode());
        let bookmarkStart;
        let bookmarkId;
        let bookmarkEnd;
        let bookmarkStartP;
        let bookmarkEndP;

        if (bookmarkName) {
            bookmarkStart = pageContentNode.find('.bookmark[anchor="' + bookmarkName + '"][bmPos="start"]');
            bookmarkId = DOM.getBookmarkId(bookmarkStart);
            bookmarkEnd = pageContentNode.find('.bookmark[bmId="' + bookmarkId + '"][bmPos="end"]');
            bookmarkStartP = bookmarkStart.closest('.p');
            bookmarkEndP = bookmarkEnd.closest('.p');
            while (bookmarkStartP.length && bookmarkStartP[0] !== bookmarkEndP[0]) {
                docParagraphs = docParagraphs.add(bookmarkStartP);
                bookmarkStartP = bookmarkStartP.next();
            }
            if (bookmarkStartP[0] && bookmarkStartP[0] === bookmarkEndP[0]) { docParagraphs = docParagraphs.add(bookmarkStartP); }
        } else {
            // fetching all paragraphs in document with specific heading levels
            docParagraphs = pageContentNode.find('.p').not('.marginal');
        }
        return docParagraphs;
    }

    /**
     * Process passed paragraphs and prepares data for Table of Contents insert/update.
     * This method also generates necessary operations for missing bookmarks and pushes to passed generator.
     *
     * @param {OperationGenerator} generator
     *  The operations generator to be filled with the operation.
     *
     * @param {jQuery} paragraphs
     *  Collection of jQuery paragraphs to process.
     *
     * @param {Array} headingLevels
     *  Range of allowed heading levels to search within
     *
     * @param {Boolean} setAnchorAttribute
     *  Whether or not to set anchor character attributes
     *
     * @returns {Array}
     *  Array of objects containing properties important for generating Table of Contents.
     */
    #fetchTOCdata(generator, paragraphs, headingLevels, setAnchorAttribute) {

        let bmIncrement = 1;
        const dataTOC = [];
        const localGenerator = this.docModel.createOperationGenerator();

        _.each(paragraphs, paragraph => {

            const paraStyleAttrSet = this.docModel.paragraphStyles.getElementAttributes(paragraph);
            const paraStyle = paraStyleAttrSet && paraStyleAttrSet.styleId;
            const outlineLevel = paraStyleAttrSet && paraStyleAttrSet.paragraph && paraStyleAttrSet.paragraph.outlineLevel;
            const paraHeadLevel = outlineLevel + 1;
            let titleText = $(paragraph).text();
            let oneData;
            let listLabel;

            if (_.contains(headingLevels, paraHeadLevel) && titleText.length) { // if it's allowed heading level
                listLabel = DOM.isListLabelNode(paragraph.firstChild) ? $(paragraph.firstChild).text() : null;
                titleText = listLabel ? titleText.substr(listLabel.length) : titleText;
                listLabel = listLabel ? listLabel + '    ' : null; // add substitute whitespaces for tab between list number and title text
                oneData = { text: titleText, paraNode: paragraph, pageNum: String(this.#pageLayout.getPageNumber(paragraph)), headingLevel: paraHeadLevel, styleId: paraStyle, listLabelText: listLabel };
                if (setAnchorAttribute) {
                    // first fetch positions of existing bookmarks in current paragraph
                    const bmStartNode = $(paragraph).children('.bookmark[anchor*=_]').first();
                    const oldBmId = DOM.getBookmarkId(bmStartNode);
                    let anchorName = DOM.getBookmarkAnchor(bmStartNode);
                    const bmEndNode = $(paragraph).children('.bookmark[bmId="' + oldBmId + '"]').not('.bookmark[anchor*=_]');
                    const paraStartPos = getOxoPosition(this.docModel.getNode(), paragraph);
                    const endPos = _.clone(paraStartPos);

                    if (!bmStartNode.length) {
                        const startPos = _.clone(paraStartPos);
                        const bmId = this.getNextBookmarkId(bmIncrement);
                        anchorName = this.getNextAnchorName(bmIncrement);
                        bmIncrement++;
                        startPos.push(0);
                        endPos.push(getParagraphLength(this.docModel.getNode(), paraStartPos)); // not increasing length, because operations are reversed later
                        localGenerator.generateOperation(Op.BOOKMARK_INSERT, { start: _.copy(startPos), id: bmId, anchorName, position: 'start' });
                        localGenerator.generateOperation(Op.BOOKMARK_INSERT, { start: _.copy(endPos), id: bmId,  position: 'end' });
                    } else if (!bmEndNode.length) {
                        endPos.push(getParagraphLength(this.docModel.getNode(), paraStartPos));
                        localGenerator.generateOperation(Op.BOOKMARK_INSERT, { start: _.copy(endPos), id: oldBmId,  position: 'end' });
                    }
                    oneData.anchor = anchorName;
                }
                dataTOC.push(oneData);
            }
        });

        localGenerator.reverseOperations(); // reverting operations, because of inserted end bookmarks and 66684
        generator.appendOperations(localGenerator.getOperations());

        return dataTOC;
    }

    /**
     * Returns position of right tab in hmm.
     *
     * @param {jQuery} rootNode
     *  Active root node of the document.
     *
     * @param {Array} startParaPos
     *  Oxo position of the paragraph node.
     *
     * @returns {Number}
     */
    #calcRightTabPos(rootNode, startParaPos) {

        const pageWidth = this.#pageLayout.getPageAttribute('width') - this.#pageLayout.getPageAttribute('marginLeft') - this.#pageLayout.getPageAttribute('marginRight');
        const paraNode = getParagraphElement(rootNode, startParaPos);
        const paraWidth = convertLengthToHmm($(paraNode).width(), 'px');

        return Math.min(paraWidth, pageWidth);
    }

    /**
     * Helper method to generate operations for Table of Contents, and append them to passed generator.
     *
     * @param {OperationGenerator} generator
     *  The operations generator to be filled with the operation.
     *
     * @param {Array} dataTOC
     *  Array of objects with properties for generating TOC
     *
     * @param {Array} startPos
     *  Starting position for TOC field
     *
     * @param {Object} options
     *  @param {Boolean} options.setAnchorAttribute
     *      Whether to set anchor char attribute or not.
     *  @param {Boolean} options.forbidenPageNumLevels
     *      Range of forbiden page number levels.
     *  @param {String} options.customSeparator
     *      If set, custom separator to be used.
     *  @param {String} options.tabStyle
     *      Tab style to be used.
     *  @param {Boolean} options.isTOCUpdate
     *      Whether this function is called by updateTOC (and not by insertTOC).
     *
     * @returns {Position}
     *  Oxo position after last generated operation.
     */
    #generateOperationsForTOC(generator, dataTOC, startPos, options) {

        let startParaPos = startPos.slice(0, startPos.length - 1);
        const tempCreatedStyles = [];
        const startIndent = 388, marginBottomDef = 176; // predefined values for 'TOC' style
        const rightTabPos = this.#calcRightTabPos(this.docModel.getNode(), startParaPos);
        const setAnchorAttribute = getBooleanOption(options, 'setAnchorAttribute', false);
        const forbidenPageNumLevels = getArrayOption(options, 'forbidenPageNumLevels', []);
        const customSeparator = getStringOption(options, 'customSeparator', null);
        let tabFillChar = getStringOption(options, 'tabStyle', null); // can be: none, dot, hyphen, underscore
        const noTOCmessage = gt('No table of contents entries in the document.');
        const defParaStyle = this.docModel.getDefaultParagraphStyleDefinition().styleId;
        let nullAttributeSet = null;
        const isActiveChangeTracking = this.#changeTrack.isActiveChangeTracking();
        const useCtTOCUpdateHandling = dataTOC.length && isActiveChangeTracking && getBooleanOption(options, 'isTOCUpdate', false);

        if (!tabFillChar) {
            const paragraph = getParagraphElement(this.docModel.getNode(), startParaPos);
            const paraAttrs = this.docModel.paragraphStyles.getElementAttributes(paragraph);
            const tabStops = (paraAttrs && paraAttrs.paragraph) ? paraAttrs.paragraph.tabStops : [];
            _.each(tabStops, el => { if (el.value === 'right') { tabFillChar = el.fillChar; } });
        }

        if (useCtTOCUpdateHandling) { generator.generateOperation(Op.PARA_SPLIT, { start: _.copy(startPos) }); } // DOCS-3299, 66463

        generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: this.docModel.characterStyles.buildNullAttributeSet() }); // #53460
        if (!dataTOC.length) {
            generator.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPos), text: noTOCmessage, attrs: { character: { bold: true } } });
            //generator.generateOperation(Operations.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: { paragraph: { tabStops: [{ value: 'right', pos: rightTabPos, fillChar: tabFillChar }] } } });
            startPos = increaseLastIndex(startPos, noTOCmessage.length);
            generator.generateOperation(Op.PARA_SPLIT, { start: _.copy(startPos) });
            if (isActiveChangeTracking) { generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: { changes: { inserted: this.#changeTrack.getChangeTrackInfo() } } }); }
            startParaPos = increaseLastIndex(startParaPos);
            startPos = _.clone(startParaPos);
            startPos.push(0);
        }

        _.each(dataTOC, (oneDataToc, index) => {

            const styleIdExt = (_.isString(tabFillChar) && tabFillChar.length) ? tabFillChar[0] : '';
            const TOCStyleId = gt('Contents') + ' ' + oneDataToc.headingLevel + styleIdExt;
            const leftIndent = startIndent * (oneDataToc.headingLevel - 1); // start from indentLeft 0
            const stylesheetAttrs = { paragraph: { indentLeft: leftIndent, marginBottom: marginBottomDef } };
            const paraAttrs = { styleId: TOCStyleId };
            const charAttrs = { character: { } };

            if (setAnchorAttribute) { charAttrs.character.anchor = oneDataToc.anchor; }
            if (tabFillChar) {  stylesheetAttrs.paragraph.tabStops = [{ value: 'right', pos: rightTabPos, fillChar: tabFillChar }]; }

            if (!this.docModel.paragraphStyles.containsStyleSheet(TOCStyleId) && !_.contains(tempCreatedStyles, TOCStyleId)) { // insert missing style
                generator.generateOperation(Op.INSERT_STYLESHEET, { styleId: TOCStyleId, styleName: TOCStyleId, type: 'paragraph', attrs: stylesheetAttrs, nextStyleId: defParaStyle, parent: defParaStyle, uiPriority: 39 });
                tempCreatedStyles.push(TOCStyleId);
            }
            if (index !== 0) { // for first data there is already paragraph
                generator.generateOperation(Op.PARA_SPLIT, { start: _.copy(startPos) });
                startParaPos = increaseLastIndex(startParaPos);
                startPos = _.clone(startParaPos);
                startPos.push(0);
            }
            generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: paraAttrs });
            if (oneDataToc.listLabelText && oneDataToc.listLabelText.length) {
                generator.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPos), text: oneDataToc.listLabelText, attrs: { styleId: null, character: { anchor: oneDataToc.anchor } } });
                startPos = increaseLastIndex(startPos, oneDataToc.listLabelText.length);
            }
            if (oneDataToc.text.length) {
                generator.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPos), text: oneDataToc.text, attrs: charAttrs });
                startPos = increaseLastIndex(startPos, oneDataToc.text.length);
            }

            if (!_.contains(forbidenPageNumLevels, oneDataToc.headingLevel)) {
                if (customSeparator) {
                    generator.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPos), text: customSeparator, attrs: { styleId: null, character: { anchor: oneDataToc.anchor } } });
                    startPos = increaseLastIndex(startPos, customSeparator.length);
                } else {
                    generator.generateOperation(Op.TAB_INSERT, { start: _.copy(startPos), attrs: { styleId: null, character: { anchor: oneDataToc.anchor } } });
                    startPos = increaseLastIndex(startPos);
                }
                // without bookmark - insert text only
                generator.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPos), text: oneDataToc.pageNum, attrs: { styleId: null, character: { anchor: oneDataToc.anchor } } });
                oneDataToc.pageNumOxo = { oxoPosStart: _.copy(startPos), oxoPosEnd: increaseLastIndex(startPos, oneDataToc.pageNum.length - 1) };
                startPos = increaseLastIndex(startPos, oneDataToc.pageNum.length);
            }

            if (index === dataTOC.length - 1 && !isActiveChangeTracking) { // split once more for last data in TOC, but not if changetrack is active (prevent extra paragraph at the end)
                nullAttributeSet = this.docModel.paragraphStyles.buildNullAttributeSet();
                startParaPos = increaseLastIndex(startParaPos);
                generator.generateOperation(Op.PARA_SPLIT, { start: _.copy(startPos) });
                generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: _.extend({ styleId: defParaStyle }, nullAttributeSet) });
                startPos = _.clone(startParaPos);
                startPos.push(0);
            }

            // explicitely removing the CT-remove attribute. This is required for clean handling on client side for the new inserted content (DOCS-3299, 66463)
            if (useCtTOCUpdateHandling) { generator.generateOperation(Op.SET_ATTRIBUTES, { start: appendNewIndex(startParaPos), end: decreaseLastIndex(startPos), attrs: { changes: { removed: null } } }); }
        });

        return startPos;
    }

    /**
     * Update the content of complex field: TOC - Table of contents
     *
     * @param {jQuery|Node} node
     *  Complex field node
     *
     * @param {String|Array} formats
     *  Format of the field with switches. Can be one string, or array of strings.
     *
     * @returns {jQuery.Promise | void} // TODO: check-void-return
     *  A promise that will be resolved if the insertion of table of contents
     *  ended. It is rejected, if the dialog has been canceled.
     */
    #updateTableOfContents(node, formats) {

        if (_.isString(formats)) { formats = [formats]; } // if there is only one switch, it is string. Convert to array for iteration

        const id = DOM.getComplexFieldMemberId(node);
        const startPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
        const rangeEndNode = this.#rangeMarker.getEndMarker(id);

        if (!rangeEndNode) {
            globalLogger.error('complexField.updateTableOfContents(): missing range end node for the id: ', id);
            return;
        }

        const endPos = getOxoPosition(this.#selection.getRootNode(), rangeEndNode);
        const generator = this.docModel.createOperationGenerator();
        const secGenerator = this.docModel.createOperationGenerator();
        let stopedPageNumUpdate = false;
        const fieldSwitches = this.#getSwitchesFromTOCformats(formats);
        const setAnchorAttribute = fieldSwitches.setAnchorAttribute;
        const headingLevels = fieldSwitches.headingLevels;
        const forbidenPageNumLevels = fieldSwitches.forbidenPageNumLevels;
        const customSeparator = fieldSwitches.customSeparator;
        const bookmarkName = fieldSwitches.bookmarkName;
        const docParagraphs = this.#fetchAllParagraphs(bookmarkName);
        let dataTOC = [];
        const changeTracking = this.#changeTrack;
        const isActiveChangeTracking = changeTracking.isActiveChangeTracking();
        let tocPromise = null;
        const target = this.docModel.getActiveTarget();

        // blocking keyboard input during applying of operations
        this.docModel.setBlockKeyboardEvent(true);

        if (startPos && endPos) {
            tocPromise = this.docModel.getUndoManager().enterUndoGroup(() => {
                // the deferred to keep the undo group open until it is resolved or rejected
                let undoPromise = null;
                const snapshot = new Snapshot(this.docModel);

                this.#selection.setTextSelection(startPos, endPos);
                undoPromise = this.docModel.deleteSelected({ snapshot, warningLabel: gt('Preparing Table of contents.') }).then(() => {

                    this.docModel.setBlockKeyboardEvent(true);

                    dataTOC = this.#fetchTOCdata(generator, docParagraphs, headingLevels, setAnchorAttribute);
                    this.#generateOperationsForTOC(generator, dataTOC, startPos, { setAnchorAttribute, forbidenPageNumLevels, customSeparator, isTOCUpdate: true });

                    if (isActiveChangeTracking) { changeTracking.handleChangeTrackingDuringPaste(generator.getOperations()); }
                    // if toc in part of the document with different target, extend operations, but not bookmarks that are inserted always in main document node
                    if (target) {
                        _.each(generator.getOperations(), operation => {
                            if (operation.name !== Op.BOOKMARK_INSERT) { this.docModel.extendPropertiesWithTarget(operation, target); }
                        });
                    }

                    // fire apply operations asynchronously
                    const operationsDef = this.docModel.applyOperationsAsync(generator);

                    this.docApp.getView().enterBusy({
                        cancelHandler: () => {
                            if (operationsDef && operationsDef.abort) {
                                snapshot.apply();  // restoring the old state
                                stopedPageNumUpdate = true;
                                this.docApp.enterBlockOperationsMode(() => { operationsDef.abort(); }); // block sending of operations
                            }
                        },
                        immediate: true,
                        warningLabel: /*#. shown while applying selected operations to update Table of contents */ gt('Updating Table of contents will take some time, please wait...')
                    });

                    // handle the result of operations
                    return operationsDef
                        .progress(progress => {
                            // update the progress bar according to progress of the operations promise
                            this.docApp.getView().updateBusyProgress(progress);
                        });
                });

                // it might happen that pagination is affected by creating TOC, run second pass to update page numbers if necessary
                undoPromise = undoPromise.then(() => {

                    if (UNITTEST || target) { return $.when(); } // in test mode, pagination is ignored for speeding up the tests, also in header/footer mode is pagination disabled
                    return this.docModel.waitForEvent(this.docModel, 'pagination:finished').done(() => {
                        if (!stopedPageNumUpdate) {
                            this.docModel.getUndoManager().enterUndoGroup(() => {
                                _.each(dataTOC, oneDataToc => {
                                    const newPageNum = String(this.#pageLayout.getPageNumber(oneDataToc.paraNode)); // converst from number to string to compare with oneDataToc.pageNum
                                    const pageNumOxo = oneDataToc.pageNumOxo;
                                    if (pageNumOxo && newPageNum !== oneDataToc.pageNum) {
                                        secGenerator.generateOperation(Op.DELETE, { start: _.copy(pageNumOxo.oxoPosStart), end: _.copy(pageNumOxo.oxoPosEnd) });
                                        secGenerator.generateOperation(Op.TEXT_INSERT, { start: _.copy(pageNumOxo.oxoPosStart), text: newPageNum });
                                    }
                                });
                                if (isActiveChangeTracking) { changeTracking.handleChangeTrackingDuringPaste(secGenerator.getOperations()); }
                                this.docModel.setBlockKeyboardEvent(true);
                                this.docModel.applyOperations(secGenerator);
                                this.docModel.setBlockKeyboardEvent(false);
                            });
                        }
                    });
                });

                undoPromise.always(() => {
                    this.docApp.getView().leaveBusy();
                    // deleting the snapshot
                    if (snapshot) { snapshot.destroy(); }
                    // allowing keyboard events again
                    this.docModel.setBlockKeyboardEvent(false);
                    this.#selection.setTextSelection(startPos);
                });

                return undoPromise;

            }); // enterUndoGroup()
        } else {
            globalLogger.error('complexField.updateTableOfContents(): Wrong start end postitions: ', startPos, endPos);
        }
        return tocPromise;
    }

    /**
     * Generates and applies all the necessary operations for creating Table of Contents,
     * defined in format switches.
     *
     * @param {String} format
     *  Field format switches.
     * @param {Object} options
     *  @param {String} options.tabStyle
     *      Parameter for style of the tab stop.
     *
     * @returns {jQuery.Promise | void} // TODO: check-void-return
     *  A promise that will be resolved if the insertion of table of contents
     *  ended. It is rejected, if the dialog has been canceled.
     */
    #insertTableOfContents(format, options) {

        const extrObj = this.cleanUpAndExtractType(format);
        // if there is only one switch, it is string. Convert to array for iteration
        const instruction = extrObj ? (_.isString(extrObj.instruction) ? [extrObj.instruction] : extrObj.instruction) : null;
        let startPos = this.#selection.getStartPosition();
        let endPos = this.#selection.getEndPosition();
        let fieldStartPos = null;
        const generator1 = this.docModel.createOperationGenerator();
        const generator2 = this.docModel.createOperationGenerator();
        let stopedPageNumUpdate = false;
        const tabStyle = getStringOption(options, 'tabStyle', 'dot');
        const fieldSwitches = this.#getSwitchesFromTOCformats(instruction);
        const setAnchorAttribute = fieldSwitches.setAnchorAttribute;
        const headingLevels = fieldSwitches.headingLevels;
        const forbidenPageNumLevels = fieldSwitches.forbidenPageNumLevels;
        const customSeparator = fieldSwitches.customSeparator;
        const bookmarkName = fieldSwitches.bookmarkName;
        let docParagraphs = null;
        let dataTOC = [];
        const fieldManager = this.docModel.getFieldManager();
        const changeTracking = this.#changeTrack;
        const selectedFieldFormat = fieldManager.isHighlightState() ? fieldManager.getSelectedFieldFormat() : null;
        let insertHeadline = true;
        let tocPromise = null;
        let nullAttributeSet = null;
        const isActiveChangeTracking = changeTracking.isActiveChangeTracking();
        const target = this.docModel.getActiveTarget();

        const increaseSecondLastIndex = (position, increment) => {
            if (_.isArray(position) && position.length > 1) {
                position = _.clone(position);
                position[position.length - 2] += (_.isNumber(increment) ? increment : 1);
            }
            return position;
        };

        const isSelectedFieldTypeTOC = () => {
            const selFieldType = fieldManager.getSelectedFieldType();
            return _.isString(selFieldType) && selFieldType.indexOf('TOC') > -1;
        };

        if (selectedFieldFormat) { // if cursor is already in some complex field
            const typeToCheck = _.isArray(selectedFieldFormat) ? selectedFieldFormat[0] : (_.isString(selectedFieldFormat) ? selectedFieldFormat : '');
            if (typeToCheck.indexOf('TOC') > -1 || isSelectedFieldTypeTOC()) { // if cursor is already in TOC field, don't insert field in field, but delete and create new one
                const node = fieldManager.getSelectedFieldNode();
                const id = DOM.getComplexFieldMemberId(node);
                startPos = decreaseLastIndex(getOxoPosition(this.#selection.getRootNode(), node));
                const rangeEndNode = this.#rangeMarker.getEndMarker(id);
                if (!rangeEndNode) {
                    globalLogger.error('complexField.insertTableOfContents(): missing range end node for the id: ', id);
                    return;
                }
                endPos = increaseLastIndex(getOxoPosition(this.#selection.getRootNode(), rangeEndNode));
                fieldStartPos = increaseLastIndex(startPos);
                insertHeadline = false;
            }
        }

        // blocking keyboard input during applying of operations
        this.docModel.setBlockKeyboardEvent(true);

        if (startPos && endPos) {
            tocPromise = this.docModel.getUndoManager().enterUndoGroup(() => {
                // the deferred to keep the undo group open until it is resolved or rejected
                let undoPromise = null;
                const snapshot = new Snapshot(this.docModel);

                this.#selection.setTextSelection(startPos, endPos);
                undoPromise = this.docModel.deleteSelected({ warningLabel: gt('Preparing Table of contents.') }).then(() => {
                    const tocHeadlineId = gt('Contents heading');
                    const headlineForTOC = gt('Table of contents');
                    const defParaStyle = this.docModel.getDefaultParagraphStyleDefinition().styleId;
                    let startParaPos;

                    this.docModel.doCheckImplicitParagraph(startPos);
                    this.docModel.setBlockKeyboardEvent(true);

                    startParaPos = startPos.slice(0, startPos.length - 1);
                    if (getParagraphLength(this.docModel.getNode(), startParaPos) !== 0) {
                        this.docModel.splitParagraph(startPos);
                        if (isActiveChangeTracking) { generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: { changes: { inserted: changeTracking.getChangeTrackInfo() } } }); }
                        startParaPos = increaseLastIndex(startParaPos);
                        if (isActiveChangeTracking) { generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: { changes: { inserted: changeTracking.getChangeTrackInfo() } } }); }
                        startPos = _.clone(startParaPos);
                        startPos.push(0);
                        this.docModel.splitParagraph(startPos);
                    }

                    const unsentCharacterAttrs = this.docModel.collectCharacterAttributesForInsertOperation(this.docModel.getCurrentRootNode(), startPos); // DOCS-4211

                    generator1.generateOperation(Op.RANGE_INSERT, { start: _.copy(startPos), type: 'field', position: 'start' });
                    fieldStartPos = increaseLastIndex(startPos);
                    generator1.generateOperation(Op.COMPLEXFIELD_INSERT, { start: _.copy(fieldStartPos), instruction: format });
                    fieldStartPos = increaseLastIndex(fieldStartPos);

                    docParagraphs = this.#fetchAllParagraphs(bookmarkName);
                    dataTOC = this.#fetchTOCdata(generator1, docParagraphs, headingLevels, setAnchorAttribute);
                    fieldStartPos = this.#generateOperationsForTOC(generator1, dataTOC, fieldStartPos, { setAnchorAttribute, forbidenPageNumLevels, customSeparator, tabStyle, isTOCUpdate: false });

                    generator1.generateOperation(Op.RANGE_INSERT, { start: _.copy(fieldStartPos), type: 'field', position: 'end' });
                    if (this.#changeTrack.isActiveChangeTracking()) {
                        generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(fieldStartPos), attrs: { character: { anchor: null } } });
                    }

                    // setting change track information at range marker nodes
                    if (this.#changeTrack.isActiveChangeTracking()) {
                        generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startPos), attrs: { changes: { inserted: changeTracking.getChangeTrackInfo() } } });
                        generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(fieldStartPos), attrs: { changes: { inserted: changeTracking.getChangeTrackInfo() } } });
                    }

                    if (insertHeadline) {
                        nullAttributeSet = this.docModel.paragraphStyles.buildNullAttributeSet();
                        if (!this.docModel.paragraphStyles.containsStyleSheet(tocHeadlineId)) {
                            generator1.generateOperation(Op.INSERT_STYLESHEET, { styleId: tocHeadlineId, styleName: tocHeadlineId, type: 'paragraph', attrs: { paragraph: { nextStyleId: defParaStyle, outlineLevel: 9 } }, parent: 'Heading1', uiPriority: 39 });
                        }
                        generator1.generateOperation(Op.TEXT_INSERT, { start: _.copy(startPos), text: headlineForTOC });
                        startPos = increaseLastIndex(startPos, headlineForTOC.length);
                        startParaPos = startPos.slice(0, startPos.length - 1);
                        generator1.generateOperation(Op.PARA_SPLIT, { start: _.copy(startPos) });
                        generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: _.extend({ styleId: tocHeadlineId }, nullAttributeSet) });
                        if (isActiveChangeTracking) {
                            startParaPos = increaseLastIndex(startParaPos);
                            generator1.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(startParaPos), attrs: { changes: { inserted: changeTracking.getChangeTrackInfo() } } });
                        }
                    }

                    if (isActiveChangeTracking) { changeTracking.handleChangeTrackingDuringPaste(generator1.getOperations()); }
                    if (unsentCharacterAttrs) { this.docModel.assignCharacterAttrsToInsertOperations(generator1.getOperations(), unsentCharacterAttrs, true); } // DOCS-4211

                    // fire apply operations asynchronously
                    const operationsDef = this.docModel.applyOperationsAsync(generator1);

                    this.docApp.getView().enterBusy({
                        cancelHandler: () => {
                            if (operationsDef && operationsDef.abort) {
                                snapshot.apply();  // restoring the old state
                                stopedPageNumUpdate = true;
                                this.docApp.enterBlockOperationsMode(() => { operationsDef.abort(); }); // block sending of operations
                            }
                        },
                        immediate: true,
                        warningLabel: /*#. shown while applying selected operations to insert Table of contents */ gt('Inserting Table of contents will take some time, please wait...')
                    });

                    // handle the result of operations
                    return operationsDef
                        .progress(progress => {
                            // update the progress bar according to progress of the operations promise
                            this.docApp.getView().updateBusyProgress(progress);
                        });
                });

                // it might happen that pagination is affected by creating TOC, run second pass to update page numbers if necessary
                undoPromise = undoPromise.then(() => {
                    if (UNITTEST || target) { return $.when(); } // in test mode, pagination is ignored for speeding up the tests, also in header/footer mode is pagination disabled
                    return this.docModel.waitForEvent(this.docModel, 'pagination:finished').done(() => {
                        if (!stopedPageNumUpdate) {
                            _.each(dataTOC, oneDataToc => {
                                const newPageNum = String(this.#pageLayout.getPageNumber(oneDataToc.paraNode)); // convert from number to string to compare with oneDataToc.pageNum
                                const pageNumOxo = oneDataToc.pageNumOxo;
                                if (pageNumOxo && newPageNum !== oneDataToc.pageNum) {
                                    let pageNumOxoPos = pageNumOxo.oxoPosStart;
                                    let endPageNumPos = pageNumOxo.oxoPosEnd;

                                    if (insertHeadline) {
                                        pageNumOxoPos = increaseSecondLastIndex(pageNumOxoPos);
                                        endPageNumPos = increaseSecondLastIndex(endPageNumPos);
                                    }
                                    if (pageNumOxoPos && endPageNumPos) {
                                        generator2.generateOperation(Op.DELETE, { start: _.copy(pageNumOxoPos), end: _.copy(endPageNumPos) });
                                        generator2.generateOperation(Op.TEXT_INSERT, { start: _.copy(pageNumOxoPos), text: newPageNum });
                                    }
                                }
                            });
                            if (isActiveChangeTracking) { this.#changeTrack.handleChangeTrackingDuringPaste(generator2.getOperations()); }
                            this.docModel.setBlockKeyboardEvent(true);
                            this.docModel.applyOperations(generator2);
                            this.docModel.setBlockKeyboardEvent(false);
                        }
                    });
                });

                undoPromise.always(() => {
                    this.docApp.getView().leaveBusy();
                    // deleting the snapshot
                    if (snapshot) { snapshot.destroy(); }
                    // allowing keyboard events again
                    this.docModel.setBlockKeyboardEvent(false);
                    this.#selection.setTextSelection(startPos);
                });

                return undoPromise;

            }); // enterUndoGroup()
        } else {
            globalLogger.error('complexField.insertTableOfContents(): Wrong start end postitions: ', startPos, endPos);
        }
        return tocPromise;
    }

    /**
     * Local helper function that reverts conversion from special field to normal,
     * which is necessary before creating undo operations.
     *
     * @param {jQuery|Node} specialField
     */
    #restoreSpecialField(specialField) {

        let fieldTxtLen = $(specialField).data('length');
        fieldTxtLen = (_.isNumber(fieldTxtLen) && fieldTxtLen > 0) ? (fieldTxtLen - 1) : 1;
        const allChildren = $(specialField).children();
        let firstChild = null;

        // using the first span as representative for all children. Changing the content, so that
        // its length is valid. Keeping data object, so that attributes stay valid (56461).
        // -> is there a case, in which the attributes of the following spans are also required?
        _.each(allChildren, (child, index) => {
            if (index === 0) {
                firstChild = child;
            } else {
                $(child).remove();
            }
        });

        $(firstChild).text('1'.repeat(fieldTxtLen)); // generating child with valid length
        $(specialField).data('length', 1).removeClass(DOM.SPECIAL_FIELD_NODE_CLASSNAME).after(firstChild); // append span behind special complex field
    }

    /**
     * Local helper used in case of undo of changetracked fields, where range start and range end are not changetracked.
     * Method getComplexFieldIdFromStack needs to have insertRange, insertComplexField, insertRange order.
     * Since ranges are not changetracked, on undo of changetrack operation, only insertComplexField operation comes,
     * and id is not found. This method tries to get id from rangeStart node, that is already in dom.
     *
     * @param  {Array} start
     *  Oxo position where field node should be inserted
     * @param  {String} target
     *  Target id of content node for position.
     * @returns {String|Null} Found id, or null.
     */
    #tryGetIDfromPrevRangeNode(start, target) {

        let id = null;
        const prevRangeMarkerPos = decreaseLastIndex(start);
        const startMarkerPoint = getDOMPosition(this.docModel.getRootNode(target), prevRangeMarkerPos, true);
        const startMarkerNode = startMarkerPoint && startMarkerPoint.node;

        if (DOM.isRangeMarkerStartNode(startMarkerNode) && DOM.getRangeMarkerType(startMarkerNode) === 'field') {
            id = DOM.getRangeMarkerId(startMarkerNode);
            if (this.getComplexField(id)) { // if field with this id is already in a model, it is not correct id
                id = null;
            }
        }

        return id;
    }

    /**
     * Helper function to run an update of special fields after the page number style was modified remotely (66918).
     * In this case all special fields must be generated from scratch. Typically this is only done once, after the
     * loading process.
     *
     * This update includes the following operations:
     *  1. delete { start: [0, 2], end: [0, 6], target: "target1" }   -> deleting the content inside the special field
     *  2. updateComplexField { start: [0, 1], instruction: "PAGE \\* ALPHABETIC", target: "target1" }
     *  3. insertText: { start: [0, 2], text: "A", target: "target1" }
     *
     * This function 'checkForSpecialFieldUpdate' is called in the handler for the updateComplexField operation. The
     * insertText operation is not applied at this point. Therefore a handler function is specified to wait for the
     * following 'operations'success' event, that is only triggered after a full set of external operations is applied.
     * Then an update of all special fields can be done successfully.
     *
     * @param {jQuery} complexFieldNode
     *  The jQuerified complex field node.
     *
     * @param {String} target
     *  The target string corresponding to the specified start position.
     *
     * @param {Boolean} external
     *  Whether this update was triggered by an external operation.
     */
    #checkForSpecialFieldUpdate(complexFieldNode, target, external) {
        // external special fields must be updated, after a change of page number type (66918)
        if (external && this.docApp.isOTEnabled() && this.docModel.isImportFinished()) {
            if (DOM.isMarginalNode(complexFieldNode) && (complexFieldNode.hasClass('cx-page') || complexFieldNode.hasClass('cx-numpages'))) {
                const follow1 = complexFieldNode.next();
                const follow2 = follow1.length ? follow1.next() : $();

                // after the previous delete operation, the follower of the complex field must be an empty span
                if (follow1.length && follow2.length && DOM.isEmptySpan(follow1) && DOM.isRangeMarkerEndNode(follow2)) {
                    // waiting for the following insertText operation, before the special fields can be updated, because
                    // this insertText operation contains the content for the special field.
                    this.docModel.one('operations:success', () => {
                        let currentRootNode;
                        // optionally leaving header/footer mode, if user is currently inside affected marginal node
                        if (this.docModel.isHeaderFooterEditState() && this.docModel.getActiveTarget() === target) {
                            currentRootNode = this.docModel.getCurrentRootNode();
                            this.#pageLayout.leaveHeaderFooterEditMode();
                        }
                        this.#pageLayout.replaceAllTypesOfHeaderFooters(); // replacing the existing headers and footer with templates
                        this.updateDateTimeAndSpecialFields(); // ... and then the special fields can be updated
                        // optionally activating header/footer mode again
                        if (currentRootNode) {
                            this.#pageLayout.enterHeaderFooterEditMode(currentRootNode);
                            this.#selection.restoreBrowserSelection();
                        }
                    });
                }
            }
        }

    }

    /**
     * Public method that updates format of selected complex field.
     *
     * @param {String} fieldType
     *  Type of the field.
     *
     * @param {String} fieldFormat
     *  FieldFormat of the field.
     *
     * @param {String} oldInstruction
     *
     * @returns {Array} [representation, instruction]
     */
    #getComplexFieldRepresentationAndInstruction(fieldType, fieldFormat, oldInstruction) {

        let representation;
        let instruction;

        if (this.isCurrentDate(fieldType) || this.isCurrentTime(fieldType)) {
            if (!fieldFormat || fieldFormat === 'default') {
                fieldFormat = LOCALE_DATA.shortDate;
            }
            representation = this.getDateTimeRepresentation(fieldFormat);
            instruction = fieldType.toUpperCase() + (fieldFormat.length ? ' \\@ "' + fieldFormat + '"' : '');
        } else if (this.isSaveDate(fieldType)) {
            if (!fieldFormat || fieldFormat === 'default') {
                fieldFormat = LOCALE_DATA.shortDate + ' ' + LOCALE_DATA.longTime;
            }
            representation = this.#numberFormatter.formatValue(fieldFormat, this.getSaveDate()).text;
            instruction = fieldType.toUpperCase() + (fieldFormat.length ? ' \\@ "' + fieldFormat + '"' : '');
        } else if (this.isFileName(fieldType) || this.isAuthor(fieldType)) {
            let usePath = false;
            if (this.isFileName(fieldType)) {
                usePath = this.usePath(oldInstruction);
                representation = this.docApp.getFullFileName({ path: usePath });
            } else {
                representation = this.docApp.getClientOperationName();
            }
            if ((/Lower/i).test(fieldFormat)) {
                representation = representation.toLowerCase();
            } else if ((/Upper/i).test(fieldFormat)) {
                representation = representation.toUpperCase();
            } else if ((/FirstCap/i).test(fieldFormat)) {
                representation = str.capitalizeFirst(representation.toLowerCase());
            } else if ((/Caps/i).test(fieldFormat)) {
                representation = str.capitalizeWords(representation.toLowerCase());
            }
            instruction = fieldType.toUpperCase() + ((fieldFormat.length && fieldFormat !== 'default') ? ' \\* ' + fieldFormat : '');
            if (usePath) {
                instruction += ' \\p';
            }
        } else if (this.isNumPages(fieldType)) {
            representation = this.formatPageFieldInstruction(this.#pageLayout.getNumberOfDocumentPages(), fieldFormat, true);
            instruction = fieldType.toUpperCase() + ((fieldFormat.length && fieldFormat !== 'default') ? ' \\* ' + fieldFormat : '');
        } else if (this.isPageNumber(fieldType)) {
            representation = this.formatPageFieldInstruction(this.#pageLayout.getPageNumber(), fieldFormat);
            instruction = fieldType.toUpperCase() + ((fieldFormat.length && fieldFormat !== 'default') ? ' \\* ' + fieldFormat : '');
        }
        return [representation, instruction];
    }

}
