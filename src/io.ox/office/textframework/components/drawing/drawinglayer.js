/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import { containsNode, parseCssLength, Rectangle } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

import { NODE_SELECTOR, isRotatedDrawingNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getRotatedDrawingPoints } from '@/io.ox/office/drawinglayer/utils/drawingutils';

import {
    SMALL_DEVICE, convertCssLength, convertHmmToLength, findAllIntersections, getAllAbsoluteDrawingsOnNode,
    getAllAbsoluteDrawingsOnPage, getBooleanOption, getDomNode, getNumberOption, getOption, getPageNumber,
    getStringOption, handleParagraphIndex, mergeSiblingTextSpans, sortDrawingsOrder
} from '@/io.ox/office/textframework/utils/textutils';
import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import {
    getOxoPosition, getParagraphElement, getParagraphNodeLength, getPixelPositionToRootNodeOffset,
    getPositionFromPagePixelPosition, getPositionInsideParagraph, getVerticalPagePixelPosition
} from '@/io.ox/office/textframework/utils/position';

// class DrawingLayer =====================================================

/**
 * An instance of this class represents the model for all drawings in the
 * drawing layer of the edited document. The drawing layer contains all
 * drawings that are anchored to the 'page'. All these drawings are
 * positioned absolutely.
 *
 * Also positioned absolutely are the drawings that are anchored to a
 * paragraph. These drawings are not shifted into the drawing layer and
 * therefore there is no model required from them. The drawings at paragraphs
 * only have a space maker node, that is updated on demand.
 *
 * @param {TextModel} docModel
 *  The model instance.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The root node of the document. If this object is a jQuery collection,
 *  uses the first node it contains.
 */
class DrawingLayer extends ModelObject {

    constructor(docModel, rootNode) {

        // base constructor
        super(docModel);

        var // self reference
            self = this,
            // the application instance
            docApp = this.docApp,
            // a list of all drawings (jQuery) in the main document in the drawing layer (model)
            drawings = [],
            // a counter for drawings in the margins (model)
            marginCounter = 0,
            // the page layout
            pageLayout = null,
            // the page styles of the document
            pageAttributes = null,
            // a place holder ID for absolute positioned drawings
            placeHolderDrawingID = 1000,
            // whether the OX Text drawing layer is active (disable this for testing
            // or performance reasons)
            isActive = true,
            // a temporary collector for all absolute drawings anchored to paragraph that need to be updated
            paragraphDrawingCollector = $(),
            // a temporary collector for all absolute drawings anchored to paragraph that need to be updated
            // in every update run (this is only necessary, if the corresponding space maker node is in a
            // different paragraph). This collector needs only to be updated after loading from local storage
            // or after applying a snapshot.
            paragraphDrawingCollectorUpdateAlways = $(),
            // during loading the document, the drawings need to be collected, to be made visible later
            loadDrawingCollector = $(),
            // whether the page breaks are already inserted
            pageBreaksInserted = false,
            // whether at least one space maker node was inserted during updateDrawings
            spaceMakerInserted = false,
            // flag storing info if page layout repainting is required after drawing update
            pageLayoutRepaintReq = false;

        // private methods ----------------------------------------------------

        /**
         * Adding one drawing into the drawing collections (the model).
         *
         * @param {HTMLElement|jQuery} drawing
         *  One drawing node that is positioned absolutely.
         *
         * @param {Boolean} isMarginalNode
         *  Whether the drawing is inside header or footer
         *
         * @returns {Number}
         *  The number of drawings in the main document.
         */
        function addIntoDrawingModel(drawing, isMarginalNode) {

            if (isMarginalNode) {
                marginCounter++;
            } else {
                // inserting drawing into the drawings list
                drawings.push($(drawing));
            }

            return drawings.length;
        }

        /**
         * Removing one drawing from the drawing collections (the model).
         *
         * @param {HTMLElement|jQuery} drawing
         *  One drawing node.
         *
         * @param {Boolean} isMarginalNode
         *  Whether the drawing is inside header or footer
         *
         * @returns {Number}
         *  The number of drawings in the main document.
         */
        function removeFromDrawingModel(drawing, isMarginalNode) {

            if (isMarginalNode) {
                marginCounter--;
            } else {
                // remove drawing from the drawings list
                if (!_.isEmpty(drawings)) {
                    // ... and finally removing the drawing from the collector 'drawings'
                    drawings = _.filter(drawings, function (node) {
                        return getDomNode(node) !== getDomNode(drawing);
                    });
                }
            }

            return drawings.length;
        }

        /**
         * Initializing page attributes needed for calculation of page breaks.
         * In some scenarios, like ex. loading from local storage,
         * initialPageBreaks method is not called, and variables are not initialized for later usage.
         *
         */
        function initializePageAttributes() {
            pageAttributes = docModel.pageStyles.getElementAttributes(rootNode);
        }

        /**
         * Helper function to make one specified drawing invisible. This is needed for all absolute
         * positioned drawings during loading.
         *
         * @param {jQuery} drawing
         *  The drawing node that will be made invisible, already jQuerified.
         */
        function hideDrawing(drawing) {
            drawing.css('visibility', 'hidden');
            loadDrawingCollector = loadDrawingCollector.add(drawing);
        }

        /**
         * Helper function to make all collected invisible drawings visible after the page breaks
         * are inserted into the document.
         */
        function makeDrawingsVisible() {
            loadDrawingCollector.css('visibility', '');
            loadDrawingCollector = $();
        }

        /**
         * Returns whether a drawing overlaps with the page content node. In this case
         * it is necessary to create a space maker element below the drawing. If the drawing
         * is completely located in the border region, no space maker element is required.
         *
         * @param {Object} drawingAttrs
         *  The explicit attributes at the drawing node.
         */
        function drawingOverlapsPageContent(drawingAttrs) {

            // drawingAttrs: {"left":0,"top":0,"width":3970,"height":2980,
            // "name":"Grafik 1","description":"","flipH":true,"flipV":false,"replacementData":"","imageUrl":"word/media/image1.jpeg",
            // "imageData":"","cropLeft":0,"cropRight":0,"cropTop":0,"cropBottom":0,
            // "marginTop":0,"marginBottom":0,"marginLeft":317,"marginRight":317,
            // "borderLeft":{"style":"none"},"borderRight":{"style":"none"},"borderTop":{"style":"none"},"borderBottom":{"style":"none"},
            // "inline":false,"anchorHorBase":"page","anchorHorAlign":"offset","anchorHorOffset":4000,"anchorVertBase":"page",
            // "anchorVertAlign":"offset","anchorVertOffset":6000,"textWrapMode":"topAndBottom","textWrapSide":"both"}

            var // the offset of the drawing in the page
                offset = 0;

            if (!drawingAttrs) { return false; }

            // pageAttributes: {"width":21000,"height":29700,"marginLeft":2499,"marginRight":2499,"marginTop":2499,"marginBottom":2000}
            if (!pageAttributes) {
                initializePageAttributes();
            }

            // drawing in top page frame
            if ((drawingAttrs.anchorVertBase === 'page') && (drawingAttrs.anchorVertAlign === 'offset' || drawingAttrs.anchorVertAlign === 'top')) {
                offset = (drawingAttrs.anchorVertAlign === 'offset') ? drawingAttrs.anchorVertOffset : 0;
                if (offset + drawingAttrs.height + drawingAttrs.marginTop + drawingAttrs.marginBottom <= pageAttributes.page.marginTop) { return false; }
            }

            // drawing in left page frame
            if ((drawingAttrs.anchorHorBase === 'page') && (drawingAttrs.anchorHorAlign === 'offset' || drawingAttrs.anchorHorAlign === 'left')) {
                offset = (drawingAttrs.anchorHorAlign === 'offset') ? drawingAttrs.anchorHorOffset : 0;
                if (offset + drawingAttrs.width + drawingAttrs.marginLeft + drawingAttrs.marginRight <= pageAttributes.page.marginLeft) { return false; }
            }

            // drawing in right page frame
            if ((drawingAttrs.anchorHorBase === 'page') && (drawingAttrs.anchorHorAlign === 'offset' || drawingAttrs.anchorHorAlign === 'right')) {
                offset = (drawingAttrs.anchorHorAlign === 'offset') ? (drawingAttrs.anchorHorOffset - drawingAttrs.marginLeft) : (pageAttributes.page.width - (drawingAttrs.width + drawingAttrs.marginLeft + drawingAttrs.marginRight));
                if (offset >= pageAttributes.page.width - pageAttributes.page.marginRight) { return false; }

            }

            // TODO: Drawing overlaps with header and footer -> also no space maker element required
            // TODO: anchorVertBase can also be 'column'
            // TODO: anchorHorBase can be any other type

            return true;
        }

        /**
         * Whether the drawing with the specified drawing attributes is fixed to the bottom of the page.
         *
         * @param {Object} drawingAttributes
         *  The attributes of the drawing
         *
         * @returns {Boolean}
         *  Whether the drawing is positioned at the bottom of the page
         */
        function isFixedAtPageBottom(drawingAttributes) {
            // not shifting drawing upwards, if it is at page aligned to 'bottom' (anchorVertBase anchorVertAlign 'bottom'
            return drawingAttributes && drawingAttributes.anchorVertBase === 'page' && drawingAttributes.anchorVertAlign === 'bottom';
        }

        /**
         * Returns whether the passed 'textWrapMode' attribute allows to wrap the
         * text around the drawing.
         *
         * @param {String} textWrapMode
         *  The text wrap mode of a drawing element
         */
        function isTextWrapped(textWrapMode) {

            var // values for the 'textWrapMode' attribute allowing to wrap the text around the drawing
                WRAPPED_TEXT_VALUES = /^(square|tight|through)$/;

            return WRAPPED_TEXT_VALUES.test(textWrapMode);
        }

        /**
         * Inserting the space maker node below the absolutely positioned drawing.
         *
         * @param {HTMLElement|jQuery} targetNode
         *  The current target node for the logical positions
         *
         * @param {Number[]} position
         *  The logical position, at which the space maker element needs to be inserted.
         *
         * @param {jQuery} drawing
         *  The drawing node, already jQuerified.
         *
         * @param {Number} zoomFactor
         *  The current zoom factor in percent.
         *
         * @param {Object} [drawingAttributes]
         *  The explicit drawing attributes at the drawing node. This is used for drawings anchored
         *  to the page. Drawings anchored to the paragraph have the required attributes saved in
         *  their data object and do not use these drawing attributes.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {String} [options.anchorType='page']
         *      The anchor type of the drawing. This can be 'page' or 'paragraph'. 'page' is the
         *      default value, if not specified.
         *  @param {Number} [options.additionalHeight=0]
         *      An additional height of the space maker node, that is required, if the node is inserted
         *      at the last position of the document or a table cell.
         *  @param {Object} [options.cache=null]
         *      An optional cache for a previous integrated space maker node, so that the positions
         *      of two following space maker nodes can be compared.
         *  @param {Boolean} [options.hasNegativeOffset=false]
         *      Whether the drawing has an negative vertical offset. In this case a later improvement of
         *      the position of the space maker node is required.
         */
        function insertDrawingSpaceMaker(targetNode, position, drawing, zoomFactor, drawingAttributes, options) {

            var // the space maker node below the drawing
                spaceMakerNode = null,
                // the width of the space maker
                width = 0,
                // the height of the space maker
                height = 0,
                // the zoomFactor used for calculations
                zoomValue = zoomFactor / 100,
                // an optional cache for the previous space maker node
                cache = (options && options.cache) || null,
                // the anchor type for the drawing
                anchorType = getStringOption(options, 'anchorType', 'page'),
                // the paragraph element, parent of the drawing
                paragraph = $(getParagraphElement(targetNode, _.initial(position))),
                // the width of the paragraph in px
                paraWidth = Math.round(paragraph.outerWidth(true)),
                // the object with margin information saved at the drawing
                pixelMargins = drawing.data(DOM.DRAWING_MARGINS_PIXEL) || null,
                // the top margin of the drawing (distance to the text)
                marginTop = pixelMargins ? pixelMargins.marginTop : 0,
                // the bottom margin of the drawing (distance to the text)
                marginBottom = pixelMargins ? pixelMargins.marginBottom : 0,
                // the left margin of the drawing (distance to the text)
                marginLeft = pixelMargins ? pixelMargins.marginLeft : 0,
                // the right margin of the drawing (distance to the text)
                marginRight = pixelMargins ? pixelMargins.marginRight : 0,
                // bounding rectangle of the drawing node
                drawingBoundRect = drawing[0].getBoundingClientRect(),
                // width of drawing without extra margin left and right
                drawingBaseWidth = drawingBoundRect.width / zoomValue,
                // the width of the drawing in px
                drawingWidth = Math.round(drawingBaseWidth) + marginLeft + marginRight,
                // height of drawing without extra margin top and bottom
                drawingBaseHeight = drawingBoundRect.height / zoomValue,
                // the height of the drawing in px
                drawingHeight = drawingBaseHeight + marginTop + marginBottom,
                // the left distance of the paragraph to the root node (taking care of tables)
                paragraphOffset = anchorType === 'page' ? getPixelPositionToRootNodeOffset(targetNode, paragraph, zoomFactor) : null,
                // the horizontal paragraph offset
                paraOffsetX = paragraphOffset ? paragraphOffset.x : 0,
                // the horizontal paragraph offset
                paraOffsetY = paragraphOffset ? paragraphOffset.y : 0,
                // position values of the drawing
                drawingPosition = drawing.position(),
                // the distance from left border of paragraph to the left border of the drawing in pixel
                leftDistance = Math.round((drawingPosition.left / zoomValue) - marginLeft - paraOffsetX),
                // the distance from right border of the drawing to the right border of the paragraph in pixel
                rightDistance = Math.round(paraOffsetX + paraWidth - ((drawingPosition.left / zoomValue) - marginLeft + drawingWidth)),
                // the distance from top border of paragraph to the top border of the drawing in pixel
                topDistance = anchorType === 'page' ? Math.round((drawingPosition.top / zoomValue) - marginTop - paraOffsetY) : 0,
                // text wrapping side (left/right/none)
                wrapMode = 'none',
                // the id of the drawing (required for restoring from local storage)
                drawingID = drawing.attr('data-drawingID'),
                // whether the target node is a footer
                isFooterNode = DOM.isFooterNode(targetNode),
                // the old and the new height of a footer node
                oldHeight = 0, newHeight = 0, newBottomOffset = 0,
                // an optional additional height of the space maker, if it is inserted at the last position of a document or table cell
                additionalHeight = getNumberOption(options, 'additionalHeight', 0),
                // the change of height of the footer node after inserting a drawing space maker node
                spaceMakerShift = 0,
                // the overlap of a drawing below the space maker node
                bottomOverlap = 0,
                // the text wrap mode of the drawing
                textWrapMode = null,
                // the text wrap side of the drawing
                textWrapSide = null,
                // flag to mark if insertion of spacemaker is necessary
                spacemakerInsertion = true,
                // flag to mark if correction of spacemaker is necessary
                spacemakerCorrection = true,
                // in case of wrap mode left or right we need additional distance of 10px
                additionalDrawingWidth = 10,
                // collection of all intersection drawings that are positioned bellow current drawing
                intersectDrawingsBellow = null;

            // helper function to fill a cache for the last inserted space maker node
            function fillCache(position, width, height, spaceMakerNode, drawing) {
                cache.position = _.clone(position);
                cache.width = width;
                cache.height = height;
                cache.spaceMakerNode = spaceMakerNode;
                cache.drawingNode = drawing;
            }

            // handle intersection drawings
            function handleDrawingIntersections() {
                var // collection of drawings that intersects with current drawing
                    intersectionDrawings = null,
                    // drawing's top offset value
                    drawingPositionTop = 0,
                    // drawing's bottom offset value
                    drawingPositionBottom = 0,
                    // drawing's left offset value
                    drawingPositionLeft = 0,
                    // drawing's right offset value
                    drawingPositionRight = 0,
                    // object containing top and left offset positions
                    drawingBoundRect = null,
                    // return this collection
                    intersectionCollection = [];

                intersectionDrawings = findAllIntersections(drawing[0], findAllDrawingsOnPage(drawing), { marginKey: DOM.DRAWING_MARGINS_PIXEL });
                if (intersectionDrawings.length) {
                    drawingBoundRect = drawing[0].getBoundingClientRect();
                    drawingPositionTop = drawingBoundRect.top;
                    drawingPositionLeft = drawingBoundRect.left;
                    drawingPositionBottom = drawingBoundRect.bottom + marginBottom;
                    drawingPositionRight = drawingBoundRect.right + marginRight;
                }

                _.each(intersectionDrawings, function (intersectionDrawing) {
                    //globalLogger.warn('drawing: ', drawingID, 'intersects drawing: ', $(intersectionDrawing).attr('data-drawingID'));
                    var intsectDrawingBoundRect = intersectionDrawing.getBoundingClientRect();
                    var intersectionDrawingPositionTop = intsectDrawingBoundRect.top;
                    var intersectionDrawingPositionLeft = intsectDrawingBoundRect.left;
                    var intersectionDrawingPositionBottom = intsectDrawingBoundRect.bottom;
                    var intersectionDrawingPositionRight = intsectDrawingBoundRect.right;
                    var intersectionDrawingSpacemaker;

                    // handling of margin for the intersection drawing -> needs to be saved at the drawing
                    var margins = $(intersectionDrawing).data(DOM.DRAWING_MARGINS_PIXEL);
                    if (margins) {
                        intersectionDrawingPositionBottom += margins.marginBottom;
                        intersectionDrawingPositionRight += margins.marginRight;
                    }

                    if (drawingPositionBottom > intersectionDrawingPositionBottom) {
                        if (drawingPositionTop <= intersectionDrawingPositionTop) {
                            // removing the previous space maker node
                            removeDrawingSpaceMaker(DOM.getDrawingSpaceMakerNode(intersectionDrawing));
                            $(intersectionDrawing).removeData(DOM.DRAWING_SPACEMAKER_LINK);
                            if (wrapMode === 'left') {
                                if (drawingPositionLeft > intersectionDrawingPositionLeft) {
                                    width += (drawingPositionLeft - intersectionDrawingPositionLeft) / zoomValue;
                                }
                            } else if (wrapMode === 'right') {
                                if (intersectionDrawingPositionRight > drawingPositionRight) {
                                    width += (intersectionDrawingPositionRight - drawingPositionRight) / zoomValue;
                                }
                            }
                        } else {
                            // correct height of other spacemaker node
                            intersectionDrawingSpacemaker = DOM.getDrawingSpaceMakerNode(intersectionDrawing);
                            if (intersectionDrawingSpacemaker && $(intersectionDrawingSpacemaker).length) {
                                height = (drawingPositionBottom - intersectionDrawingPositionBottom) / zoomValue;
                                spacemakerCorrection = false;
                            }
                        }
                    } else {
                        if (drawingPositionTop > intersectionDrawingPositionTop) {
                            spacemakerInsertion = false;
                        } else {
                            intersectionDrawingSpacemaker = DOM.getDrawingSpaceMakerNode(intersectionDrawing);
                            if (intersectionDrawingSpacemaker && $(intersectionDrawingSpacemaker).length) {
                                $(intersectionDrawingSpacemaker).css('height', (intersectionDrawingPositionBottom - drawingPositionBottom) / zoomValue);
                            }
                            if (wrapMode === 'left') {
                                if (drawingPositionLeft > intersectionDrawingPositionLeft) {
                                    width += (drawingPositionLeft - intersectionDrawingPositionLeft) / zoomValue;
                                }
                            } else if (wrapMode === 'right') {
                                if (intersectionDrawingPositionRight > drawingPositionRight) {
                                    width += (intersectionDrawingPositionRight - drawingPositionRight) / zoomValue;
                                }
                            }
                            intersectionCollection.push({ node: intersectionDrawing, boundingRect: intsectDrawingBoundRect });
                        }
                    }
                });

                // comparing the currently required space maker node with the last inserted space maker node
                if (!intersectionDrawings || !intersectionDrawings.length) {
                    if (cache && cache.position && _.isEqual(position, cache.position)) {
                        if (width <= cache.width && height <= cache.height) {
                            spacemakerInsertion = false;
                            return; // no new space maker node required
                        }
                        if (width >= cache.width && height >= cache.height) {
                            // removing the previous space maker node
                            cache.spaceMakerNode.remove();
                            cache.drawingNode.removeData(DOM.DRAWING_SPACEMAKER_LINK);
                        }
                    }
                }
                return intersectionCollection;
            }  // handleDrawingIntersections

            // setting value for textWrapMode and textWrapSide
            if (drawingAttributes) {
                textWrapMode = drawingAttributes.textWrapMode || null;
                textWrapSide = drawingAttributes.textWrapSide || '';
            } else {
                textWrapMode = drawing.data('textWrapMode') || null;
                textWrapSide = drawing.data('textWrapSide') || '';
            }

            // no need to insert spacemaker if no text wrapping
            // in ODF no text wrapping is called "through" - which is not the same as MS "through" property!
            if (textWrapMode === 'none' || (textWrapMode === 'through' && docApp.isODF())) { return; }

            // evaluating the text wrap mode and side
            if (textWrapMode && isTextWrapped(textWrapMode)) {
                switch (textWrapSide) {
                    case 'left':
                        wrapMode = 'left';
                        break;
                    case 'right':
                        wrapMode = 'right';
                        break;
                    case 'both':
                    case 'largest':
                        // no support for 'wrap both sides' in CSS, default to 'largest'
                        wrapMode = (leftDistance >= rightDistance) ? 'left' : 'right';
                        break;
                    default:
                        globalLogger.warn('insertDrawingSpaceMaker(): invalid text wrap side: ' + textWrapSide);
                        wrapMode = 'none';
                }
            } else {
                // text does not float beside drawing
                wrapMode = 'none';
            }

            // calculating the width of the space maker
            // -> outerWidth already contains the border width, although it is drawing with canvas. But an additional
            //    margin was set to the drawing before, representing the border.
            switch (wrapMode) {
                case 'none':
                    width = paraWidth;
                    additionalDrawingWidth = 0;
                    break;
                case 'left':
                    width = drawingWidth + rightDistance;
                    break;
                case 'right':
                    width = drawingWidth + leftDistance;
                    break;
            }

            // setting the height of the space maker node
            height = drawingHeight;

            // it might be necessary to reduce the height, if the drawing is above the paragraph
            if (topDistance < 0) { height += topDistance; }

            // try to make correction for intersected spacemakers
            if (docApp.isImportFinished()) { intersectDrawingsBellow = handleDrawingIntersections(); }

            if (spacemakerInsertion) {
                // inserting div element to create space for the drawing
                spaceMakerNode = $('<div>').addClass(((wrapMode === 'none') ? '' : 'float ' + ((wrapMode === 'left') ? 'right ' : 'left ')) + DOM.DRAWING_SPACEMAKER_CLASS)
                    .css('position', 'relative')
                    .height(height + additionalHeight)
                    .width(width + additionalDrawingWidth)  // adding 10 px to avoid horizontal overlapping
                    .attr('data-drawingID', drawingID);  // Adding the drawing id (required for local storage)

                // saving space maker node in cache
                if (cache) { fillCache(position, width, height, spaceMakerNode, drawing); }

                // saving the old height of the footer node
                if (isFooterNode) { oldHeight = Math.round($(targetNode).height()); }

                if (_.last(position) === 0) {
                    // inserting at the beginning of the paragraph, no empty span before
                    paragraph.prepend(spaceMakerNode);
                } else {
                    // splitting text span
                    spaceMakerNode.insertAfter(docModel.prepareTextSpanForInsertion(position, null, targetNode));
                }

                // registering insertion of space maker node at global marker
                spaceMakerInserted = true;

                // if the drawing is inside the footer, the vertical alignment relative to the bottom need to be updated (not shifting drawing upwards, if it is at page aligned to 'bottom')
                if (isFooterNode && !isFixedAtPageBottom(drawingAttributes)) {
                    // not shifting drawing upwards, if it is at page aligned to 'bottom'
                    newHeight = Math.round($(targetNode).height());
                    if (newHeight !== oldHeight) {
                        spaceMakerShift = newHeight - oldHeight;
                        newBottomOffset = Math.max(convertCssLength(drawing.css('bottom'), 'px', 1) + spaceMakerShift, 0);
                        drawing.css({ bottom: newBottomOffset, top: '' });
                        // saving shift at the drawing node so that it can be used, when the space maker node is removed
                        // and when the bottom offset is set in 'this.setAbsoluteDrawingPosition'.
                        drawing.data(DOM.DRAWING_SPACEMAKER_SHIFT, spaceMakerShift);
                    }
                }

                // Checking positions after inserting the space maker node
                if (!getBooleanOption(options, 'hasNegativeOffset', false) && spacemakerCorrection) {
                    // it is possible to increase the height of the space maker node.
                    // -> calculating, if the drawing is below the space maker node.
                    bottomOverlap = drawing.offset().top - marginTop + drawingHeight - (spaceMakerNode.offset().top + spaceMakerNode.height());
                    if (bottomOverlap > 0) {
                        // TODO: enabling this will increase precision of spacemakers, but decrease general performance

                        // var allSpacemakersOnPage = _.map(findAllDrawingsOnPage(drawing), function (drawing) { return DOM.getDrawingSpaceMakerNode(drawing); });
                        // allSpacemakersOnPage = _.filter(allSpacemakersOnPage, function (spacemaker) { return !_.isUndefined(spacemaker); });
                        // var drawingBoundRect = getDomNode(drawing).getBoundingClientRect();
                        // var completeOverlap = false;

                        // _.each(findAllIntersections(drawing[0], allSpacemakersOnPage, { marginKey: DOM.DRAWING_MARGINS_PIXEL }), function (spacemaker) {
                        //     if (!completeOverlap) {
                        //         var spacemakerBoundRect = spacemaker.getBoundingClientRect();
                        //         if (spacemakerBoundRect.top > drawingBoundRect.top) {
                        //             if (spacemakerBoundRect.bottom > drawingBoundRect.bottom) {
                        //                 completeOverlap = true;
                        //             } else {
                        //                 if (bottomOverlap < drawingBoundRect.bottom - spacemakerBoundRect.bottom) {
                        //                     bottomOverlap = drawingBoundRect.bottom - spacemakerBoundRect.bottom;
                        //                 }
                        //             }
                        //         } else { // spacemakerBoundRect.top < drawingBoundRect.top
                        //             if (spacemakerBoundRect.bottom > drawingBoundRect.bottom) {
                        //                 completeOverlap = true;
                        //             } else {
                        //                 bottomOverlap = spacemakerBoundRect.bottom - drawingBoundRect.bottom;
                        //             }
                        //         }
                        //     }
                        // });
                        // if (!completeOverlap) {
                        //     spaceMakerNode.height(spaceMakerNode.height() + bottomOverlap);
                        // }
                        spaceMakerNode.height(spaceMakerNode.height() + bottomOverlap);
                    }
                }

                // registering the space maker at the absolute positioned drawing, but not inside header or footer
                if (!DOM.isHeaderOrFooter(targetNode)) {
                    drawing.data(DOM.DRAWING_SPACEMAKER_LINK, getDomNode(spaceMakerNode));
                } else if (docApp.isImportFinished()) {
                    docModel.getPageLayout().updateEditingHeaderFooter({ preventUpdateMarginal: true, givenNode: targetNode });
                    docModel.getPageLayout().insertPageBreaks({ skipAbsoluteDrawings: true });
                }

                // #51816 - after insertion of drawing spacemaker, whole content might be shifted downwards.
                // If drawings are not intersecting anymore, correct spacemaker of intersected drawing
                _.each(intersectDrawingsBellow, function (intersectDrawing) {
                    var intersection = findAllIntersections(drawing[0], $(intersectDrawing.node), { marginKey: DOM.DRAWING_MARGINS_PIXEL });
                    if (!intersection.length) {
                        var spaceMaker = DOM.getDrawingSpaceMakerNode(intersectDrawing.node);
                        if (spaceMaker) { $(spaceMaker).css('height', (intersectDrawing.boundingRect.height / zoomValue)); }
                    }
                });
            }
        }

        /**
         * Sorts paragraph aligned drawings from one page. It uses two level of sorting,
         * first sortFromTopToBottomOnOnePage, and then by oxo position, as it can happen
         * that drawings are not in the same paragraph.
         *
         * @param {jQuery[]} drawingsCollection
         *  The list with all drawings aligned to paragraph on one page.
         *
         * @returns {jQuery[]} drawingsCollection
         *  Sorted collection of passed drawings.
         */
        function sortParagraphAlignedDrawingsOnPage(drawingsCollection) {
            var sortedByVertOffset = sortFromTopToBottomOnOnePage(drawingsCollection); // first sort by vertical offset
            var pageNode = docModel.getNode();

            // it can happen that some marginal nodes are not anymore in the DOM, filter them out
            sortedByVertOffset = _.filter(sortedByVertOffset, function (drawing) {
                if (DOM.isMarginalNode(drawing) && !DOM.getMarginalTargetNode(pageNode, drawing).length) { return false; }
                return true;
            });

            return _.sortBy(sortedByVertOffset, function (drawing) { // and then by more important oxo position
                var node = DOM.isMarginalNode(drawing) ? DOM.getMarginalTargetNode(pageNode, drawing) : pageNode;
                var drawingPos = getOxoPosition(node, drawing);
                return drawingPos ? _.last(drawingPos.slice(0, -1)) : 0;
            });
        }

        /**
         * Sorting the drawings in the order from top to bottom in the document. This is not
         * necessarily the order inside the DOM.
         *
         * @param {jQuery[]} sortDrawings
         *  The list with all drawings in the drawing layer.
         */
        function sortFromTopToBottomOnOnePage(sortDrawings) {
            return _.sortBy(sortDrawings, function (drawing) {
                var attrs = getExplicitAttributes(drawing, 'drawing', true);
                return attrs.anchorVertOffset || 0;
            });
        }

        /**
         * Removing the space maker node that is assigned to a specified drawing node.
         * Additionally it might be necessary to merge the neighboring text nodes
         *
         * @param {Node} drawingSpaceMaker
         *  The space maker element of a drawing.
         */
        function removeDrawingSpaceMaker(drawingSpaceMaker) {

            if (!drawingSpaceMaker) { return; }

            var prev = drawingSpaceMaker.previousSibling,
                tryToMerge = prev && DOM.isTextSpan(prev);
            $(drawingSpaceMaker).remove();
            if (tryToMerge) { mergeSiblingTextSpans(prev, true); }
        }

        /**
         * Collecting all absolutely positioned drawings that are located inside the
         * margin specified by the update node, whose change triggered the update of
         * absolute positioned drawings.
         *
         * @param {jQuery[]} updateNode
         *  The node, whose change triggered the update of absolute drawings. This node
         *  is not necessarily inside an activated node, because this process could be
         *  triggered by a remote client or with an undo operation.
         *
         * @returns {Object}
         *  An object with the two properties 'drawings' and 'marginals'.
         *  The 'drawings' property contains a list with all absolute positioned drawings
         *  inside the specified margin. Inside this margin, the drawings are sorted in
         *  vertical direction from top to bottom. Drawings in the template margins are
         *  not part of this collection.
         *  The 'marginals' property contains a list of all header or footer nodes, whose
         *  drawings are in the list of the 'drawings'. In this case, it contains only
         *  the marginal node corresponding to the specified updateNode.
         */
        function getAllDrawingsFromSelectedMargin(updateNode) {

            var // the currently activated root node
                activeRootNode = null,
                // the text drawing layer inside the margin
                marginalDrawingLayer = null,
                // the collector for all absolute drawings
                marginalDrawingsArray = [],
                // the header or footer node corresponding to the specified update node
                marginalRoot = null,
                // a collector for all affected header or footer nodes containing collected drawings
                allMarginalNodes = [];

            // determining the marginal root node (the header or footer node itself)
            if (updateNode) {
                marginalRoot = DOM.isHeaderOrFooter(updateNode) ? updateNode : (DOM.isMarginalNode(updateNode) && DOM.getClosestMarginalTargetNode(updateNode));
                marginalRoot = marginalRoot && marginalRoot.length ? marginalRoot : null;
            }

            // using active marginal node, if the update node cannot be used
            if (!marginalRoot) {
                activeRootNode = docModel.getCurrentRootNode();
                if (activeRootNode && DOM.isHeaderOrFooter(activeRootNode)) { marginalRoot = activeRootNode; }
            }

            if (marginalRoot) {
                marginalDrawingLayer = marginalRoot.children(DOM.DRAWINGLAYER_NODE_SELECTOR);
                if (marginalDrawingLayer.length > 0) {
                    _.each(marginalDrawingLayer.children(NODE_SELECTOR), function (drawing) { marginalDrawingsArray.push($(drawing)); });
                    if (marginalDrawingsArray.length > 1) {
                        marginalDrawingsArray = sortFromTopToBottomOnOnePage(marginalDrawingsArray);
                    }
                }
                allMarginalNodes.push(marginalRoot);
            }

            return { drawings: marginalDrawingsArray, marginals: allMarginalNodes };
        }

        /**
         * Collecting all absolutely positioned drawings in all margins. But every id
         * is only handled once. Not collected are drawings inside the template margins,
         * because in the template margins the positions cannot be calculated. The pixel
         * API fails in the template folder.
         *
         * @returns {Object}
         *  An object with the two properties 'drawings' and 'marginals'.
         *  The 'drawings' property contains a list with all absolute positioned drawings
         *  inside all margins, that are displayed in the document. Inside each margin, the
         *  drawings are sorted in vertical direction from top to bottom. Drawings in the
         *  template margins are not part of this collection.
         *  The 'marginals' property contains a list of all header or footer nodes, whose
         *  drawings are in the list of the 'drawings'.
         */
        function getAllDrawingsFromMargins() {

            var // the top level content nodes under div.page
                allContentNodes = rootNode.find(DOM.HEADER_WRAPPER_SELECTOR + ',' + DOM.FOOTER_WRAPPER_SELECTOR + ',' + DOM.PAGECONTENT_NODE_SELECTOR),
                // all text drawing layer that are inside headers or footers, but not in the templates
                allTextDrawingLayer = allContentNodes.find(DOM.HEADER_SELECTOR + ' > ' + DOM.DRAWINGLAYER_NODE_SELECTOR + ', ' + DOM.FOOTER_SELECTOR + ' > ' + DOM.DRAWINGLAYER_NODE_SELECTOR),
                // the collector for all absolute drawings
                marginalDrawingsArray = [],
                // a collector for all container ids
                allContainerIds = [],
                // a collector for all affected header or footer nodes containing collected drawings
                allMarginalNodes = [];

            if (allTextDrawingLayer.length > 0) {

                _.each(allTextDrawingLayer, function (drawingLayer) {

                    var oneLayerDrawings = null,
                        containerId = DOM.getTargetContainerId(drawingLayer.parentNode); // collecting every id only once

                    if (!_.contains(allContainerIds, containerId)) { // collecting drawings for every id only once

                        oneLayerDrawings = [];
                        _.each($(drawingLayer).children(NODE_SELECTOR), function (drawing) {
                            oneLayerDrawings.push($(drawing));
                        });
                        if (oneLayerDrawings.length > 1) {
                            oneLayerDrawings = sortFromTopToBottomOnOnePage(oneLayerDrawings);
                        }
                        marginalDrawingsArray = marginalDrawingsArray.concat(oneLayerDrawings);

                        allContainerIds.push(containerId); // not collecting each ID more than once, if forceAll is true
                        allMarginalNodes.push(drawingLayer.parentNode); // collecting all headers or footers
                    }
                });
            }

            return { drawings: marginalDrawingsArray, marginals: allMarginalNodes };
        }

        /**
         * If there is no text next to a paragraph aligned drawing (only above and below allowed) and the vertical offset of
         * this drawing is negative, the bottom border of the drawing must be used to calculate the distance to its paragraph.
         * Not the top border, like in all other calculations. So the drawing must be shifted upwards (for the height of the
         * drawing), but only until the header is reached.
         *
         * @param {Node|jQuery} drawing
         *  The paragraph aligned drawing node.
         *
         * @param {Number} verticalParagraphOffset
         *  The (negative) vertical paragraph offset of the drawing.
         *
         * @returns {Nunmber}
         *  The additional offset, so that the drawing is shifted upwards further.
         */
        function calculateUpwardShift(drawing, verticalParagraphOffset) {

            var upwardShift = 0;
            var pageNumber = pageLayout.getPageNumber(drawing);
            var headerHeight = pageLayout.getMarginalNodeHeight(pageNumber);
            var firstNode = pageLayout.getBeginingElementOnSpecificPage(pageNumber); // on the first page, null is returned
            var topOffset = firstNode ? firstNode.offset().top : docModel.getNode().offset().top;
            var paragraph = drawing.parent(); // the parent node of the drawing

            topOffset += headerHeight;

            var vertParaOffset = paragraph.offset().top; // the offset of the paragraph
            var maxShift = vertParaOffset - topOffset;

            var drawingAttrs = docModel.drawingStyles.getElementAttributes(drawing).drawing;
            var rotatedAttrs = getRotatedDrawingPoints(drawingAttrs, drawingAttrs.rotation);
            var drawingHeight = convertHmmToLength(rotatedAttrs.height, 'px', 1) || 0;
            var absoluteVerticalParagraphOffset = -verticalParagraphOffset;

            // shifting the drawing upwards, but not into the header
            if (maxShift > drawingHeight + absoluteVerticalParagraphOffset) {
                upwardShift = drawingHeight; // shift full drawing length upwards
            } else if (maxShift > absoluteVerticalParagraphOffset) {
                upwardShift = maxShift - absoluteVerticalParagraphOffset;  // shift the upper drawing border to the bottom border of the header
            }

            return upwardShift;
        }

        /**
         * Refreshing the links between a drawing from the drawing layer and its
         * place holder node and its space maker node.
         *
         * @param {Node|jQuery} absoluteDrawing
         *  The absolute positioned drawing in the drawing layer node.
         *
         * @param {Node|jQuery} contentNode
         *  The node, that contains the place holder nodes. This can be the page
         *  content node or any footer or header node.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.onlySpaceMaker=false]
         *      If set to true, only the space maker nodes are searched, not the
         *      drawing place holder nodes. This is the case for absolute paragraph
         *      drawings.
         */
        function refreshLinkForAbsoluteDrawing(absoluteDrawing, contentNode, options) {

            var // the jQuery version of the drawing
                $absoluteDrawing = $(absoluteDrawing),
                // the drawing id of the drawing in the drawing layer
                drawingID = $absoluteDrawing.attr('data-drawingID'),
                // the selector string for the search for place holder nodes and space maker nodes
                selectorString = '[data-drawingID=' + drawingID + ']',
                // finding the place holder and optionally the space maker node, that are located inside the page content node
                foundNodes = $(contentNode).find(selectorString),
                // whether the corresponding place holder node was found (this is required)
                placeHolderFound = false,
                // whether only a space maker node is searched
                onlySpaceMaker = getBooleanOption(options, 'onlySpaceMaker', false);

            if (_.isString(drawingID)) { drawingID = parseInt(drawingID, 10); }

            // updating the value for the global drawing ID, so that new drawings in drawing layer get an increased number.
            if (_.isNumber(drawingID) && (drawingID >= placeHolderDrawingID)) { placeHolderDrawingID = drawingID + 1; }

            if (foundNodes.length > 0) {

                _.each(foundNodes, function (oneNode) {

                    if (DOM.isDrawingPlaceHolderNode(oneNode)) {
                        // creating links in both directions
                        $absoluteDrawing.data(DOM.DRAWINGPLACEHOLDER_LINK, oneNode);
                        $(oneNode).data(DOM.DRAWINGPLACEHOLDER_LINK, getDomNode(absoluteDrawing));
                        placeHolderFound = true;
                    } else if (DOM.isDrawingSpaceMakerNode(oneNode)) {
                        // there might be a space maker node with this drawingID, too
                        // -> but not every drawing has a space maker node
                        // creating links in both directions
                        $absoluteDrawing.data(DOM.DRAWING_SPACEMAKER_LINK, oneNode);

                        // special handling for absolute drawings at paragraphs, that have a space maker
                        // node located in another paragraph. These drawings are collected in the collector
                        // 'paragraphDrawingCollectorUpdateAlways', so that they are updated in every
                        // update run. After loading from local storage or applying a snapshot it is
                        // necessary that this collector is refilled. Otherwise the drawings will get
                        // the correct link to their space maker nodes, but the update will not be
                        // triggered in every run (only if the specific paragraph requires an update).
                        if (onlySpaceMaker && DOM.isAbsoluteParagraphDrawing($absoluteDrawing) && getDomNode($absoluteDrawing.parent()) !== getDomNode($(oneNode).parent()) && !DOM.isMarginalNode($absoluteDrawing)) {
                            paragraphDrawingCollectorUpdateAlways = paragraphDrawingCollectorUpdateAlways.add($absoluteDrawing);
                        }
                    }
                });

                if (!placeHolderFound && !onlySpaceMaker) {
                    globalLogger.error('DrawingLayer.refreshLinkForAbsoluteDrawing(): failed to find place holder node with drawing ID: ' + drawingID);
                }

            } else {
                if (!onlySpaceMaker) {
                    globalLogger.error('DrawingLayer.refreshLinkForAbsoluteDrawing(): failed to find place holder node with drawingID: ' + drawingID);
                }
            }
        }

        /**
         * Updating the space maker element below the drawings in the drawing layer.
         * This function is called very often and is therefore performance critical.
         * Also needs to update the vertical pixel position of the absolute drawings,
         * because this changes, if the drawing changes the page.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.forceUpdateAllDrawings=false]
         *      If set to true, all drawing positions are updated, even if the page
         *      did not change. This is especially necessary after loading the
         *      document.
         *  @param {jQuery|Node} [options.targetNode=null]
         *      If such a target node is defined and no current processing node, the
         *      drawings inside this target node will be updated. This is especially
         *      useful, if a header or footer is deactivated and 'updateMarginal:leave'
         *      is triggered. In this case the absolute positions inside header or
         *      footer are recalculated.
         */
        var updateAbsoluteDrawings = globalLogger.profileMethod('DrawingLayer.updateAbsoluteDrawings(): ', function (options) {

            var // setting zoom factor, keeping percentage
                zoomFactor = docApp.getView().getZoomFactor() * 100,
                // a collector for all page numbers
                allPageNumbers = [],
                // a helper object for all drawing nodes
                allSortedDrawings = {},
                // an ordered collection of the absolute positioned drawings
                sortedDrawings = [],
                // the key name for saving the current page number at the drawing node
                dataPageKey = 'currentPageNumber',
                // check if a processing node is registered, so that only following drawings
                currentNode = docModel.getCurrentProcessingNode() || getOption(options, 'targetNode', null),
                // setting a page number until which the drawings need to be updated
                startPageNumber = 1,
                // whether the update of all drawings is forced
                forceUpdateAllDrawings =  getBooleanOption(options, 'forceUpdateAllDrawings', false),
                // an array with all absolutely positioned drawings in a currently activated header or footer
                marginalDrawingsArray = null,
                // an array with all header and footer nodes, that are updated in this update process
                marginalsArray = null,
                // an object with properties 'drawings' and 'marginals' for all drawings and header/footer node
                marginalDrawingsCollector = null,
                // whether marginal drawings also need to be updated
                updateDrawingsInMarginalNode = false,
                // cache for the previous inserted space maker node
                spacemakerCache = {},
                // structure that keeps track of newly created spacemakers, that were not before,
                // and vice versa, spacemakers that are deleted and been there before.
                // that is a signal to repaint page layout
                spacemakerBookkeeper = [];

            if (!pageLayout) { pageLayout = docModel.getPageLayout(); }

            // header-footer handling
            if (forceUpdateAllDrawings) {
                // collecting all affected marginal drawing nodes
                marginalDrawingsCollector = getAllDrawingsFromMargins();
            } else if (DOM.isMarginalNode(currentNode) || DOM.isHeaderOrFooter(currentNode)) {
                // collecting all marginal drawing nodes affected defined by the current node
                marginalDrawingsCollector = getAllDrawingsFromSelectedMargin(currentNode);
            }

            marginalDrawingsArray = (marginalDrawingsCollector && marginalDrawingsCollector.drawings) || [];
            marginalsArray = (marginalDrawingsCollector && marginalDrawingsCollector.marginals) || [];
            updateDrawingsInMarginalNode = marginalDrawingsArray && marginalDrawingsArray.length > 0;

            // Performance: Update only following pages
            if (!currentNode) {
                startPageNumber = pageLayout.getPageNumber(currentNode);
                // was is successful to determine the start page number?
                startPageNumber = startPageNumber || 1;

                if (startPageNumber > 1) { startPageNumber--; }
            }

            // no updates and space maker for drawings in header or footer
            sortedDrawings = _.filter(drawings, function (drawing) {
                return !DOM.isMarginalNode(drawing);
            });

            // sorting the drawings corresponding to their appearance in the document
            _.each(sortedDrawings, function (drawing) {
                // on which page is the place holder node?
                var page = pageLayout.getPageNumber(DOM.getDrawingPlaceHolderNode(drawing));
                // was is successful to determine the start page number?
                if (!page) { page = 0; }

                // updating only following drawings
                if (page >= startPageNumber) {
                    if (!_.contains(allPageNumbers, page)) { allPageNumbers.push(page); }
                    if (!allSortedDrawings[page]) { allSortedDrawings[page] = []; }
                    allSortedDrawings[page].push(drawing);
                    // saving the page number at the drawing
                    $(drawing).data(dataPageKey, page);
                }
            });

            // sorting the pages
            allPageNumbers.sort(function (a, b) { return a - b; });

            // reset the sorted drawings helper array
            sortedDrawings = [];

            // sorting all drawings on one page corresponding to their vertical offset
            _.each(allPageNumbers, function (page) {
                if (allSortedDrawings[page].length > 1) {
                    allSortedDrawings[page] = sortFromTopToBottomOnOnePage(allSortedDrawings[page]);  // order is now from top to down in the visibility
                }
                // concatenating all drawings into one array
                sortedDrawings = sortedDrawings.concat(allSortedDrawings[page]);
            });

            // also handling all drawings inside header or footer -> put them to the start of the sorted drawings collector
            if (updateDrawingsInMarginalNode) { sortedDrawings = marginalDrawingsArray.concat(sortedDrawings); }

            // removing the existing space maker nodes -> maybe they can be reused in the future?
            _.each(sortedDrawings, function (drawing, index) {
                var // the space maker node below the drawing
                    spaceMakerNode = DOM.getDrawingSpaceMakerNode(drawing),
                    // new bottom offset in pixel for drawings in footer
                    newBottomOffset = 0;

                if (spaceMakerNode) {
                    spacemakerBookkeeper[index] = $(spaceMakerNode).height();
                    removeDrawingSpaceMaker(spaceMakerNode);
                    drawing.removeData(DOM.DRAWING_SPACEMAKER_LINK);

                    // fix for position of drawings in footer, that are aligned to 'bottom'
                    if (drawing.data(DOM.DRAWING_SPACEMAKER_SHIFT)) {
                        newBottomOffset = Math.max(convertCssLength(drawing.css('bottom'), 'px', 1) - drawing.data(DOM.DRAWING_SPACEMAKER_SHIFT), 0);
                        drawing.css({ bottom: newBottomOffset, top: '' });
                        drawing.removeData(DOM.DRAWING_SPACEMAKER_SHIFT);
                    }
                } else {
                    spacemakerBookkeeper[index] = -1;
                }
            });

            // iterate over all drawings -> is a space maker node element required below the drawing?
            _.each(sortedDrawings, function (drawing, index) {

                var // the merged attributes of the drawing
                    allAttrs = docModel.drawingStyles.getElementAttributes(drawing),
                    // the drawing attributes set at the drawing
                    drawingAttrs = allAttrs.drawing,
                    // the horizontal and vertical offset of the drawing relative to the page
                    pos = null,
                    // the page info object for a specified pixel position
                    pageInfo = null,
                    // the page number at which the drawing is positioned
                    // page = drawing.data(dataPageKey);
                    // the current target node for the logical positions
                    targetNode = rootNode,
                    // whether the drawing is inside header or footer
                    isMarginalDrawing = DOM.isDrawingInsideMarginalDrawingLayerNode(drawing),
                    // node of newly inserted spacemaker
                    newSpacemakerNode;

                // immediately remove the current page number
                drawing.removeData(dataPageKey);

                // after deleting and inserting previous space maker nodes, it is necessary to update the vertical pixel position,
                // otherwise the vertical position is not precise enough.
                // Additionally some drawings need to be checked, if they need a new vertical offset, caused by page change
                self.setAbsoluteDrawingPosition(drawing, { forceUpdateAllDrawings: true });

                // handling space maker node, if required
                if (drawingOverlapsPageContent(drawingAttrs) || isMarginalDrawing) {

                    if (updateDrawingsInMarginalNode && isMarginalDrawing) {
                        targetNode = drawing.parent().parent();
                    }

                    // -> update or create the space maker element
                    // calculating pixel position relative to the page
                    pos = getPixelPositionToRootNodeOffset(targetNode, drawing, zoomFactor);

                    // now it is time to handle the margin
                    if (drawingAttrs.marginLeft) { pos.x -= convertHmmToLength(drawingAttrs.marginLeft, 'px', 1); }
                    if (drawingAttrs.marginTop) { pos.y -= convertHmmToLength(drawingAttrs.marginTop, 'px', 1); }
                    if (pos.x < 0) { pos.x = 0; }
                    if (pos.y < 0) { pos.y = 0; }

                    try {
                        // calculating the logical position at the specified position (first position in line)
                        pageInfo = getPositionFromPagePixelPosition(targetNode, pos.x, pos.y, zoomFactor, { getFirstTextPositionInLine: true });
                    } catch {}

                    if (pageInfo && pageInfo.pos) {
                        insertDrawingSpaceMaker(targetNode, pageInfo.pos, drawing, zoomFactor, drawingAttrs, { cache: spacemakerCache, anchorType: 'page' });

                    }
                    newSpacemakerNode = $(DOM.getDrawingSpaceMakerNode(drawing));
                    if (Math.abs(spacemakerBookkeeper[index] - (newSpacemakerNode.height() || 0)) > 10  && !isMarginalDrawing) { pageLayoutRepaintReq = true; } // #62068
                }
            });

            // transferring the changes into the header or footer templates, so that all header or footer nodes can be updated.
            if (marginalsArray.length > 0) { pageLayout.updateDataFromFirstNodeInDoc(marginalsArray); }

            globalLogger.log('Number of drawings: ' + sortedDrawings.length);
        });

        // find all drawings on page
        function findAllDrawingsOnPage(drawing) {
            if (DOM.isMarginalNode(drawing)) {
                return getAllAbsoluteDrawingsOnNode(DOM.getMarginalTargetNode(docModel.getNode(), drawing));
            } else {
                var pageNumber = getPageNumber(drawing[0], docModel);
                return getAllAbsoluteDrawingsOnPage(docModel.getNode(), pageNumber);
            }
        }

        /**
         * Improving the position of the space maker node for drawings anchored to the
         * paragraph, that have a negative offset. In this case the space maker node
         * needs to be inserted twice.
         *
         * @param {jQuery|Node} targetNode
         *  The target node of the drawing.
         *
         * @param {jQuery} oneDrawing
         *  One drawing node that is positioned absolutely at a paragraph.
         *
         * @param {Number} oldDrawingOffset
         *  The vertical offset in pixel of the drawing, before the space maker node
         *  was inserted.
         */
        function improveSpaceMakerPosition(targetNode, oneDrawing, oldDrawingOffset) {

            var // the top offset of the drawing
                drawingTopOffset = oneDrawing.offset().top,
                // the top offset of the space maker
                spaceMakerTopOffset = 0,
                // the vertical shift of the drawing after inserting the space maker node
                verticalShift = drawingTopOffset - oldDrawingOffset,
                // the space maker node
                spaceMakerNode = null,
                // an object containing the logical position for the space maker
                // and optional additional information
                spaceMakerPos = null,
                spaceMakerHeight = null,
                spacemakerBottomOffset = null,
                oldDrawingBottomOffset = null;

            if (verticalShift > 0) {

                spaceMakerNode = DOM.getDrawingSpaceMakerNode(oneDrawing);

                spaceMakerTopOffset = $(spaceMakerNode).offset().top;

                // checking, if space maker is not already good positioned
                if (spaceMakerTopOffset < drawingTopOffset && ((spaceMakerTopOffset + $(spaceMakerNode).height()) > (drawingTopOffset + oneDrawing.height()))) { return; }

                // removing an existing space maker node of the drawing
                if (spaceMakerNode) {
                    removeDrawingSpaceMaker(spaceMakerNode);
                    oneDrawing.removeData(DOM.DRAWING_SPACEMAKER_LINK);
                }

                // calculating the logical position again, adding the vertical shift caused by
                // inserting the space maker node
                spaceMakerPos = getPositionForParagraphDrawing(oneDrawing, targetNode, verticalShift);

                if (spaceMakerPos && spaceMakerPos.pos) {
                    insertDrawingSpaceMaker(targetNode, spaceMakerPos.pos, oneDrawing, docApp.getView().getZoomFactor() * 100, null, { anchorType: 'paragraph', additionalHeight: spaceMakerPos.additionalSpaceMakerHeight });
                }
            } else if (verticalShift === 0 && oldDrawingOffset !== 0 && docApp.isImportFinished()) {
                // this means drawing isn't moved, but correction is still needed
                spaceMakerNode = $(DOM.getDrawingSpaceMakerNode(oneDrawing));
                if (spaceMakerNode.length) {
                    spaceMakerHeight = spaceMakerNode.height();
                    spacemakerBottomOffset = spaceMakerNode.offset().top + spaceMakerHeight;
                    oldDrawingBottomOffset = oldDrawingOffset + oneDrawing.height();
                    spaceMakerNode.css('height', parseInt(Math.max(spaceMakerHeight - (spacemakerBottomOffset - oldDrawingBottomOffset), 0), 10));
                }
            }
        }

        /*
            * Improving the position of drawings that are located outside of their paragraph. This is
            * typically achieved by shifting the spacemaker node in function 'improveSpaceMakerPosition'.
            * But for drawings with 'textWrapMode' set to 'topAndBottom' (no text next to the drawing)
            * it is better to keep the inserted space maker and to shift the drawing into its spacemaker.
            * This happens within this function.
            *
            * @param {jQuery} oneDrawing
            *  The jQuerified drawing that shall be shifted into its space maker node.
            */
        function alignDrawingToSpaceMaker(oneDrawing) {

            // the space maker node for the specified drawing
            var spaceMakerNode = $(DOM.getDrawingSpaceMakerNode(oneDrawing));

            if (spaceMakerNode.length === 0) { return; } // do nothing, if no spacemaker exists

            // the offset of the drawing
            var drawingOffset = oneDrawing.offset();
            // the offset of the space maker node
            var spaceMakerOffset = spaceMakerNode.length ? spaceMakerNode.offset() : { top: 0, left: 0 };
            // the current zoom factor
            var zoomFactor = docApp.getView().getZoomFactor();
            // the vertical distance of drawing and its spacemaker
            var verticalShift = math.roundp((drawingOffset.top - spaceMakerOffset.top) / zoomFactor, 0.01);
            // the margins of the drawing saved at the node
            var pixelMargins = oneDrawing.data(DOM.DRAWING_MARGINS_PIXEL);
            // the top margin of the drawing in pixel
            var marginTop = (pixelMargins && _.isNumber(pixelMargins.marginTop)) ? pixelMargins.marginTop : 0;

            // shifting the drawing into its space maker node
            oneDrawing.css('top', parseCssLength(oneDrawing, 'top') - verticalShift + marginTop);
        }

        /**
         * Updating the space maker element below the drawings that are anchored to
         * paragraphs.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.forceUpdateAllDrawings=false]
         *      If set to true, all drawing positions are updated, even if the page
         *      did not change. This is especially necessary after loading the
         *      document.
         */
        function updateParagraphDrawings(options) {

            var // check if a processing node is registered, so that only following drawings
                currentNode = docModel.getCurrentProcessingNode(),
                // cache for the previous inserted space maker node
                spaceMakerCache = {},
                // whether the update of all drawings is forced
                forceUpdateAllDrawings =  getBooleanOption(options, 'forceUpdateAllDrawings', false),
                // structure that keeps track of newly created spacemakers, that were not before,
                // and vice versa, spacemakers that are deleted and been there before.
                // that is a signal to repaint page layout
                spacemakerBookkeeper = [];

            if (!forceUpdateAllDrawings && !currentNode && !paragraphDrawingCollector.length) { return; } // not necessary to update only paragraphDrawingCollectorUpdateAlways, see #51816

            // use processing node, if collector is empty (avoiding duplicate usage)
            if (currentNode) { self.registerUpdateElement(currentNode); }

            // adding the list of drawings, that need to be updated in every run
            // -> drawing node and space maker node are in different paragraphs
            if (paragraphDrawingCollectorUpdateAlways.length > 0) {
                paragraphDrawingCollector = paragraphDrawingCollector.add(paragraphDrawingCollectorUpdateAlways);
                // resetting global collector in every run to keep it up-to-date (no model needs to be handled)
                paragraphDrawingCollectorUpdateAlways = $();
            }

            // sorting the drawings from top to bottom
            paragraphDrawingCollector = $(sortParagraphAlignedDrawingsOnPage(paragraphDrawingCollector));

            // removing all space maker nodes
            _.each(paragraphDrawingCollector, function (oneDrawing, index) {

                var // the space maker node below the drawing
                    spaceMakerNode = DOM.getDrawingSpaceMakerNode(oneDrawing);

                // removing an existing space maker node of the drawing
                if (spaceMakerNode) {
                    spacemakerBookkeeper[index] = $(spaceMakerNode).height();
                    removeDrawingSpaceMaker(spaceMakerNode);
                    $(oneDrawing).removeData(DOM.DRAWING_SPACEMAKER_LINK);
                } else {
                    spacemakerBookkeeper[index] = -1;
                }
            });

            // iterate over all collected paragraph anchored drawings
            _.each(paragraphDrawingCollector, function (oneDrawing, index) {

                var // jQuerified drawing node
                    $oneDrawing = $(oneDrawing),
                    // whether the drawing is inside header or footer
                    isMarginalDrawing = DOM.isMarginalNode($oneDrawing.parent()),
                    // the target node of the drawing
                    targetNode = isMarginalDrawing ? DOM.getClosestMarginalTargetNode(oneDrawing) : rootNode,
                    // whether the drawing has a negative vertical offset
                    negativeVerticalOffset = false,
                    // an object containing the logical position for the space maker
                    // and optional additional information
                    spaceMakerPos = null,
                    // the offset in pixel of the drawing before inserting the space maker node
                    oldDrawingOffset = 0,
                    // node of newly inserted spacemaker
                    newSpacemakerNode;

                if (isMarginalDrawing && !containsNode(docModel.getNode(), targetNode)) { return; } // DOCS-3245 -> updating only marginal drawings that are still in the DOM

                // setting the pixel position at the drawing using the values saved at the
                // drawings data object
                self.setAbsoluteDrawingParagraphPosition($oneDrawing);

                // trying to determine the logical position for the space maker node from the
                // pixel position of the drawing
                spaceMakerPos = getPositionForParagraphDrawing($oneDrawing, targetNode);

                // whether the drawing has a negative vertical offset into the previous paragraph
                negativeVerticalOffset = ($oneDrawing.data('verticalParagraphOffset') || 0) < 0;

                // it is also possible, that a negative vertical offset is triggered by the top margin only (57548)
                if (!negativeVerticalOffset && $oneDrawing.data(DOM.DRAWING_MARGINS_PIXEL) && $oneDrawing.data('verticalParagraphOffset') < $oneDrawing.data(DOM.DRAWING_MARGINS_PIXEL).marginTop) {
                    negativeVerticalOffset = true;
                }

                // if the logical position could be determined, a space maker node can be inserted
                if (spaceMakerPos && spaceMakerPos.pos) {

                    // saving the vertical offset of the drawing, before inserting the space maker
                    if (negativeVerticalOffset) { oldDrawingOffset = $oneDrawing.offset().top; }

                    insertDrawingSpaceMaker(targetNode, spaceMakerPos.pos, $oneDrawing, docApp.getView().getZoomFactor() * 100, null, { anchorType: 'paragraph', additionalHeight: spaceMakerPos.additionalSpaceMakerHeight, cache: (negativeVerticalOffset ? null : spaceMakerCache), hasNegativeOffset: negativeVerticalOffset });

                    // ... and now the problem with negative vertical drawings offsets can be handled
                    // -> the new inserted space maker shifted the following paragraphs downwards
                    // -> the drawing is also shifted downwards
                    // -> the space maker node is not below the drawing

                    // Now the paragraph has the correct vertical extension. But the logical position of the space maker node needs to be recalculated.
                    // If there is no text next to the drawing ('topAndBottom') it is better to shift the drawing into its space maker (57548).
                    // -> but this latter process does not look good for header/footer (57721)
                    // -> sample document 58582 for valid veritical position of drawings, if text flow is modified (58582_1)
                    if (negativeVerticalOffset) {
                        if ($oneDrawing.data('textWrapMode') === 'topAndBottom' && !isMarginalDrawing) {
                            alignDrawingToSpaceMaker($oneDrawing); // no further shift of space maker for 'topAndBottom' drawings
                        } else {
                            improveSpaceMakerPosition(targetNode, $oneDrawing, oldDrawingOffset);
                        }

                    }
                }
                newSpacemakerNode = $(DOM.getDrawingSpaceMakerNode(oneDrawing));
                if (Math.abs(spacemakerBookkeeper[index] - (newSpacemakerNode.height() || 0)) > 10  && !isMarginalDrawing) { pageLayoutRepaintReq = true; } // #62068

                if (DOM.isMarginalNode($oneDrawing)) { self.updateCropMarginalParagraphDrawing($oneDrawing); }
            });

            // emptying the collector for the paragraph drawings, but only if the document was loaded successfully
            if (self.isImportFinished()) {
                paragraphDrawingCollector = $(); // collector for drawings, so that not all drawings need to be updated
            }

        }

        /**
         * After load from local storage the links between drawings and drawing place holders
         * need to restored.
         * Additionally the global 'drawings' collector is filled with the drawings.
         * This is only done for the drawings that are not located inside the header or footer.
         * Absolute positioned drawings inside a header or footer are only counted in the global
         * variable 'marginCounter'.
         */
        function refreshDrawingLayer() {

            // creating all links between drawing in drawing layer and the corresponding
            // drawing place holder
            // -> for this the data-drawingid is required
            // This process is not done for margins, where the links are not used. Instead
            // the number of absolute drawings in margins is counted.

            var // the page content node
                pageContentNode = DOM.getPageContentNode(docModel.getNode()),
                // collecting all drawings in the drawing layer
                allDrawings = self.getDrawingLayerNode().children('div.drawing'),
                // a list of all header and footer nodes
                allMargins = pageLayout.getHeaderFooterPlaceHolder().children();

            // reset model
            drawings = [];
            marginCounter = 0;

            // filling the list of absolute positioned drawings
            _.each(allDrawings, function (oneDrawing) {
                drawings.push($(oneDrawing));
            });

            if (drawings.length > 0) {
                _.each(drawings, function (absoluteDrawing) {
                    refreshLinkForAbsoluteDrawing(absoluteDrawing, pageContentNode);
                });
            }

            // counting the absolute positioned drawings in header or footer
            _.each(allMargins, function (margin) {

                var // the drawings in one drawing layer in one header or footer
                    allDrawings = null,
                    // the drawing layer inside the header or footer
                    drawingLayer = $(margin).children(DOM.DRAWINGLAYER_NODE_SELECTOR);

                if (drawingLayer.length > 0) {

                    allDrawings = drawingLayer.children('div.drawing');

                    // only counting the number of absolute drawings in header and footer
                    // -> no updating of links required
                    marginCounter += allDrawings.length;
                }
            });

            // also updating the link between absolute paragraph drawings
            // and their space maker nodes.
            refreshAbsoluteParagraphDrawings(pageContentNode);
        }

        /**
         * Drawing layer specific handling after reloading the document (for example after
         * canceling a long running action).
         */
        function handleDocumentReload() {
            refreshDrawingLayer();
            updateDrawings();
        }

        /**
         * Drawing layer specific handling after leaving the active state of a header or
         * footer. In this scenario all absolute drawings inside this specific header or
         * footer shall be updated. Therefore it is possible to set the target node inside
         * the options.
         *
         * The options are the same as described at function 'updateAbsoluteDrawings'.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.forceUpdateAllDrawings=false]
         *      If set to true, all drawing positions are updated, even if the page
         *      did not change. This is especially necessary after loading the
         *      document.
         *  @param {jQuery|Node} [options.targetNode=null]
         *      If such a target node is defined and no current processing node, the
         *      drawings inside this target node will be updated. This is especially
         *      useful, if a header or footer is deactivated and 'updateMarginal:leave'
         *      is triggered. In this case the absolute positions inside header or
         *      footer are recalculated.
         */
        function handleMarginalLeave(options) {
            updateDrawings(options);
        }

        /**
         * Function that handles the code to be executed after receiving the page break
         * event.
         */
        function handlePageBreakAfter() {
            // absolute drawings can be made visible now
            pageBreaksInserted = true;
            // making all drawings visible, after the page breaks were inserted
            makeDrawingsVisible();
            // ... resetting the global space maker control variable before calling updateDrawings
            spaceMakerInserted = false;
            // ... and setting the correct values for each drawing
            updateDrawings({ forceUpdateAllDrawings: true });
            // if space maker nodes were inserted in the previous updateDrawings, an update of page breaks is required, too.
            if (spaceMakerInserted) {
                pageLayout.insertPageBreaks(); // TODO: Inserting page breaks a second time -> optimization required (important).
            }

            updateDrawingsOrder();
        }

        /**
         * Function that handles the event 'update:drawingOrder' to update the z-order of all drawings.
         */
        function handleDrawingOrder() {
            updateDrawingsOrder();
        }

        /**
         * Function that handles the code to be executed after receiving the import success
         * event.
         */
        function handleImportSuccess() {
            if (docModel.isLocalStorageImport()) {
                // the information about the page breaks is required (for example after moving a drawing directly after load)
                pageLayout.refreshPageBreakCollection();
                // recreating the links between drawing, space maker and place holder nodes
                // -> a position update is not necessary after loading from local storage
                refreshDrawingLayer();
            } else {
                updateDrawings({ forceUpdateAllDrawings: true });
            }

            // after import success this must be set to true (even if handlePageBreakAfter was not called before)
            // -> for example after loading a new document, 'handlePageBreakAfter' is never called, so that 'pageBreaksInserted'
            // will never be set to true.
            pageBreaksInserted = true;
        }

        /**
         * Triggering the general update function for absolutely positioned drawings after
         * checking, if there are absolutely positioned drawings.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.forceUpdateAllDrawings=false]
         *      If set to true, all drawing positions are updated, even if the page
         *      did not change. This is especially necessary after loading the
         *      document.
         */
        function updateDrawings(options) {
            pageLayoutRepaintReq = false;

            updateParagraphDrawings(options); // handling absolute drawings anchored to paragraph
            if (!self.isEmpty()) { updateAbsoluteDrawings(options); }  // handling absolute drawings anchored to 'page'

            // #57660 - after removing spacemaker node, that was there before, or inserting where it didn't exist, repaint of layout is required
            if (pageLayoutRepaintReq && !docModel.isUndoRedoRunning()) { // #58171 - don't trigger pagebreaks directly if undo/redo is running
                docModel.getPageLayout().insertPageBreaks({ skipAbsoluteDrawings: true, fromUpdateDrawings: true });
            }
        }

        /**
         * update all drawings z-order
         *
         * every page for itself and every "maginal node" (header footer) for itself
         *
         * all step call "iterateArraySliced()" for better performance on big documents
         */
        function updateDrawingsOrder() {

            var pageCount = rootNode.find('.page-break').length + 1;
            var pages = [];
            pages.length = pageCount;
            var hfNodes = pageLayout.getallHeadersAndFooters();

            var promise = self.iterateArraySliced(pages, function (page, pageIndex) {
                sortDrawingsOrder(getAllAbsoluteDrawingsOnPage(rootNode, pageIndex + 1), 41);
            });

            promise = promise.then(function () {
                self.iterateArraySliced(hfNodes, function (hfNode) {
                    sortDrawingsOrder(getAllAbsoluteDrawingsOnNode($(hfNode)), 11);
                });
            });

            return promise;
        }

        /**
         * Handling drawings anchored to paragraphs.
         * After loading document from local storage or after applying a snapshot, it is
         * necessary to update the links between drawings and their space maker node. For
         * drawings anchored to paragraphs it is additionally necessary to fill the
         * collector for those drawings, that have a space maker node in a different
         * paragraph (paragraphDrawingCollectorUpdateAlways).
         *
         * @param {Node|jQuery} pageContentNode
         *  The node, that contains the place holder nodes.
         */
        function refreshAbsoluteParagraphDrawings(pageContentNode) {

            // creating all links between absolute drawings inside paragraphs and
            // the corresponding space maker nodes (no linking in header and footer).

            var // collecting all absolute drawings in the document
                allDrawings = pageContentNode.find(DOM.ABSOLUTE_DRAWING_SELECTOR);

            if (allDrawings.length > 0) {
                _.each(allDrawings, function (absoluteDrawing) {
                    refreshLinkForAbsoluteDrawing(absoluteDrawing, pageContentNode, { onlySpaceMaker: true });
                });
            }
        }

        /**
         * Handling drawings anchored to paragraphs.
         * Calculating the logical position of a space maker node for a drawing that is anchored to
         * the paragraph.
         *
         * @param {jQuery} drawing
         *  The jQuerified drawing node.
         *
         * @param {jQuery} targetNode
         *  The target node of the specified drawing.
         *
         * @param {Number} [verticalOffset]
         *  An optional vertical offset, that can be used to modify the calculated vertical
         *  pixel position of the drawing. This is needed for drawings, that are anchored to
         *  a paragraph and that have a negative vertical offset. The offset needs to be
         *  specified in pixel.
         *
         * @returns {Object|null}
         * pos: position.pos, additionalSpaceMakerHeight
         *  An object containing the logical position at which the space maker node needs
         *  to be inserted and optionally an additional height for the space maker node.
         *  The latter is required, if the space maker node is inserted at the end of a
         *  table cell.
         *  If the logical position could not be determined, null is returned.
         */
        function getPositionForParagraphDrawing(drawing, targetNode, verticalOffset) {

            var zoomFactor = docApp.getView().getZoomFactor(),
                // setting zoom factor, keeping percentage
                zoomValue = zoomFactor * 100,
                // the local position
                position = null,
                // left
                left,
                // top
                top,
                // the parent node of the drawing
                paragraph = drawing.parent(),
                // the height of the paragraph
                paraHeight = paragraph.height(),
                // the horizontal and vertical offset of the paragraph or table cell inside the page
                paragraphOffset = null, cellOffset = null,
                // an optional additional height for the space maker node (required at the
                // end of document or table cell)
                additionalSpaceMakerHeight = 0,
                // the cell node, that contains the paragraph that contains the drawing
                cellNode = null,
                // the offset of the paragraph and the last paragraph in a cell relative to the table cell
                paragraphOffsetToCell = null, lastParagraphOffsetToCell = null,
                // the last paragraph inside the table cell
                lastParagraphInTableCell = null,
                // the pixel margins object saved at the drawing (containing all margins of the drawing in pixel)
                pixelMargins = drawing.data(DOM.DRAWING_MARGINS_PIXEL),
                // the height of the drawing
                drawingHeight = 0;

            if (isRotatedDrawingNode(drawing)) {
                var drawingPos = drawing.position();
                left = drawingPos.left / zoomFactor;
                top = drawingPos.top / zoomFactor;
            } else {
                left = drawing.data('horizontalParagraphOffset') || 0;
                top = drawing.data('verticalParagraphOffset') || 0;

                // if the text is only above and below the drawing and the vertical offset is negative, the bottom border
                // of the drawing must be used to calculate the distance to the paragraph
                if (top < 0 && drawing.data('textWrapMode') === 'topAndBottom') {
                    drawingHeight = $(drawing).height() || 0;
                    top -= drawingHeight;
                }
            }

            // taking the margin of the drawing into account
            if (pixelMargins) {
                top -= pixelMargins.marginTop;
                left -= pixelMargins.marginLeft;
            }

            try {
                // calculating the logical position at the specified position

                if (top >= 0 && top <= paraHeight) {
                    if (top === 0) { top = 1; }
                    // the standard case: The drawing is located in the paragraph at which it is anchored
                    position = getPositionInsideParagraph(targetNode, paragraph, left, top, zoomValue, { getFirstTextPositionInLine: true });

                    // 44084, second try for Firefox, shifting one pixel downwards
                    if (!position && _.browser.Firefox) { position = getPositionInsideParagraph(targetNode, paragraph, left, top + 1, zoomValue, { getFirstTextPositionInLine: true }); }

                } else {

                    // The drawing must be located inside its table cell (otherwise the space maker node will not expand the correct cell)
                    if (DOM.isCellContentNode(paragraph.parent())) {

                        // receiving the position inside the table cell
                        cellNode = paragraph.closest(DOM.TABLE_CELLNODE_SELECTOR);
                        paragraphOffsetToCell = getPixelPositionToRootNodeOffset(cellNode, paragraph, zoomValue);
                        left += Math.round(paragraphOffsetToCell.x);  // horizontal position inside the paragraph
                        top += Math.round(paragraphOffsetToCell.y);   // vertical position inside the paragraph

                        if (top <= 0) {
                            // forcing the space maker node inside the first paragraph of the table cell
                            top = 1;
                            position = getPositionInsideParagraph(targetNode, paragraph, left, top, zoomValue, { getFirstTextPositionInLine: true });
                        } else if (top > paragraph.parent().height()) {
                            // the drawing position is behind the final paragraph in the table cell
                            // -> the space maker node must be at the final position in the table cell
                            // -> it must be larger than the drawing, because the distance to the paragraph must be added
                            position = {};
                            // searching last paragraph
                            lastParagraphInTableCell = paragraph.parent().children(DOM.PARAGRAPH_NODE_SELECTOR).last();
                            position = {};
                            position.pos = getOxoPosition(targetNode, lastParagraphInTableCell);
                            position.pos.push(getParagraphNodeLength(lastParagraphInTableCell));

                            lastParagraphOffsetToCell = getPixelPositionToRootNodeOffset(cellNode, lastParagraphInTableCell, zoomValue);
                            additionalSpaceMakerHeight = top - lastParagraphOffsetToCell.y - lastParagraphInTableCell.height() + 10; // adding 10px for optical reasons

                        } else {
                            // the drawing position is inside another paragraph in the same table cell
                            cellOffset = getPixelPositionToRootNodeOffset(targetNode, cellNode, zoomValue);
                            left += Math.round(cellOffset.x);  // horizontal position inside the document
                            top += Math.round(cellOffset.y);   // vertical position inside the document

                            // adding the optional vertical offset, if specified
                            if (verticalOffset) { top += verticalOffset; }

                            position = getPositionFromPagePixelPosition(targetNode, left, top, zoomValue, { getFirstTextPositionInLine: true });
                        }

                    } else {

                        // it is possible, that a drawing anchored to a paragraph is not located inside its paragraph! The
                        // vertical offset can be so big, that the drawing is shifted into a following paragraph. It can
                        // also be negative, so that the drawing is moved upwards. So the logical position can be anywhere
                        // in the document.
                        paragraphOffset = getPixelPositionToRootNodeOffset(targetNode, paragraph, zoomValue);
                        left += Math.round(paragraphOffset.x);  // horizontal position inside the paragraph
                        top += Math.round(paragraphOffset.y);   // vertical position inside the paragraph

                        // adding the optional vertical offset, if specified
                        if (verticalOffset) { top += verticalOffset; }

                        position = getPositionFromPagePixelPosition(targetNode, left, top, zoomValue, { getFirstTextPositionInLine: true });
                    }

                    // collecting all drawings, that have a space maker node in a different paragraph
                    // -> these drawings need to be updated in every update run
                    // but ignoring drawings in marginal nodes (header/footer)
                    if (!DOM.isMarginalNode(drawing)) { paragraphDrawingCollectorUpdateAlways = paragraphDrawingCollectorUpdateAlways.add(drawing); }
                }

            } catch {}

            return (position && position.pos) ? { pos: position.pos, additionalSpaceMakerHeight } : null;
        }

        /**
         * Calculating dynamically the vertical page offset. This is only necessary
         * for drawings that are vertically aligned to the margin, because the size
         * of the margin changes with the height of header or footer (at least it seems
         * so in the GUI, although the value of margin-top of the page does not change).
         *
         * Saved at the drawing as vertical page offset is only the pixel position
         * relative to the header node, not to the page
         * -> see drawingstyles.calculatePageTopOffset()
         *
         * @param {String} verticalMargin
         *  The alignment for the vertical margin. This can be 'offset', 'top' or 'bottom'.
         *
         * @param {Number} pageNumber
         *  The page number of the page, that contains the drawing.
         *
         * @param {Number} verticalPageOffset
         *  The vertical page offset in pixel that is stored at the node. For drawings
         *  that are vertically aligned to the margin, this is the distance relative
         *  to the header, not the page.
         *
         * @param {Boolean} useBottomMargin
         *  Whether a vertical alignment at the margin is relative to the bottom margin.
         *
         * @param {Boolean} isFooterNode
         *  Whether the drawing is located inside the footer.
         *
         * @param {jQuery} drawing
         *  One drawing node that is positioned absolutely and is located inside the
         *  drawing layer.
         *
         * @returns {Number}
         *  The vertical pixel position of the drawing relative to the page.
         */
        function calculateVerticalPageOffset(verticalMargin, pageNumber, verticalPageOffset, useBottomMargin, isFooterNode, drawing) {

            var // the vertical offset on the specified page in pixel
                verticalOffset = 0,
                // the height of the top margin
                topMarginHeight = 0,
                // the height of the bottom margin
                bottomMarginHeight = 0;

            if (verticalMargin === 'top') {
                verticalOffset = pageLayout.getMarginalNodeHeight(pageNumber) + 1; // increasing by 1 px to avoid rounding error
            } else if (verticalMargin === 'bottom') {
                verticalOffset = convertHmmToLength(pageLayout.getPageAttribute('height'), 'px', 1) - drawing.height() - pageLayout.getMarginalNodeHeight(pageNumber, { footer: true });
            } else if (verticalMargin === 'center') {
                topMarginHeight = pageLayout.getMarginalNodeHeight(pageNumber);
                bottomMarginHeight = pageLayout.getMarginalNodeHeight(pageNumber, { footer: true });
                verticalOffset = topMarginHeight + (convertHmmToLength(pageLayout.getPageAttribute('height'), 'px', 1) - topMarginHeight - bottomMarginHeight - drawing.height()) / 2;
            } else if (verticalMargin === 'offset') {

                if (useBottomMargin) {
                    if (isFooterNode) {
                        verticalOffset = verticalPageOffset; // no change required
                    } else {
                        bottomMarginHeight = pageLayout.getMarginalNodeHeight(pageNumber, { footer: true });
                        verticalOffset += (convertHmmToLength(pageLayout.getPageAttribute('height'), 'px', 1) - bottomMarginHeight + verticalPageOffset);
                    }
                } else {
                    topMarginHeight = pageLayout.getMarginalNodeHeight(pageNumber);
                    if (isFooterNode) {
                        bottomMarginHeight = pageLayout.getMarginalNodeHeight(pageNumber, { footer: true });
                        verticalOffset = verticalPageOffset - (convertHmmToLength(pageLayout.getPageAttribute('height'), 'px', 1) - topMarginHeight - bottomMarginHeight);
                    } else {
                        verticalOffset = topMarginHeight + verticalPageOffset;
                    }
                }

            } else {
                verticalOffset = verticalPageOffset;
            }

            return Math.ceil(verticalOffset);
        }

        /*
            * Improve the position of paragraph aligned drawings, that are inside a paragraph
            * that contains an internal page break. In this case, the position of the drawing
            * is sent from the filter relative to its paragraph on its own page. Therefore in
            * OX Text the distance to the top of the paragraph must be added.
            *
            * @param {jQuery} drawing
            *  The jQuerified drawing node.
            *
            * @returns {Number}
            *  The vertical offset in px that must be added to the given top position of the
            *  drawing because of the pagebreak inside the paragraph.
            */
        function checkManualPageBreakInParagraph(drawing) {

            // an optional offset inside the paragraph to handle the page break inside the paragraph
            var pageBreakOffset = 0;
            // the parent of the drawing (typically the paragraph)
            var paragraph = drawing.parent();

            // -> adding special handling for drawings in paragraphs that contain a MS page break
            //    These are only inserted after loading a Word document or after copy/paste from a
            //    Word document.
            if (DOM.isManualPageBreakNode(paragraph) && $(paragraph).find('> .hardbreak > .ms-hardbreak-page').length > 0) {

                var allPrevPageBreaks = drawing.prevAll('.page-break');

                if (allPrevPageBreaks.length > 0) {

                    var previousPageBreak = allPrevPageBreaks.first();
                    var zoomFactor = docApp.getView().getZoomFactor();
                    // calculating the position of the previous page break relative to the paragraph and also adding the height of the page break node
                    pageBreakOffset = Math.round((previousPageBreak.offset().top - paragraph.offset().top) / zoomFactor) + previousPageBreak.height();

                    // collecting this drawing, so that it is also updated in the next update run
                    paragraphDrawingCollectorUpdateAlways = paragraphDrawingCollectorUpdateAlways.add(drawing);
                }
            }

            return pageBreakOffset;
        }

        // methods ------------------------------------------------------------

        /**
         * Whether the drawing layer is active. This can be disabled for testing
         * or performance reasons.
         *
         * @returns {Boolean}
         *  Whether the drawing layer is active.
         */
        this.isActive = function () {
            return isActive;
        };

        /**
         * Whether the document contains absolutely positioned drawings in the
         * main document (those are collected inside the drawings container).
         *
         * @returns {Boolean}
         *  Whether the document contains at least one absolutely positioned
         *  drawing.
         */
        this.isEmptyMainDoc = function () {
            return drawings.length === 0;
        };

        /**
         * Whether the document contains absolutely positioned drawings in the
         * margins (not collected in drawings container).
         *
         * @returns {Boolean}
         *  Whether the document contains at least one absolutely positioned
         *  drawing in the margins.
         */
        this.isEmptyMargin = function () {
            return marginCounter === 0;
        };

        /**
         * Whether the document contains absolutely positioned drawings in the
         * margins or in the main document.
         *
         * @returns {Boolean}
         *  Whether the document contains at least one absolutely positioned
         *  drawing in the margins or in the main document.
         */
        this.isEmpty = function () {
            return (marginCounter === 0 && drawings.length === 0);
        };

        /**
         * Whether the document contains absolutely positioned drawings
         * inside the page content node. This excludes all drawings, that
         * are located in the header or footer.
         *
         * @returns {Boolean}
         *  Whether the document contains at least one absolutely positioned
         *  drawing in the page content node.
         */
        this.containsDrawingsInPageContentNode = function () {

            var // the drawing layer node inside the page div.page
                drawingLayerNode = self.returnDrawingLayerNode();

            return drawingLayerNode && drawingLayerNode.length > 0 && drawingLayerNode.children().length > 0;
        };

        /**
         * Provides the array with all drawings in the drawing layer.
         *
         * @returns {jQuery[]}
         *  The list with all drawings in the drawing layer.
         */
        this.getDrawings = function () {
            return drawings;
        };

        /**
         * Provides a unique ID for the drawings in the drawing layer and
         * its placeholder elements.
         *
         * @returns {Number}
         *  A unique id.
         */
        this.getNextDrawingID = function () {
            return placeHolderDrawingID++;
        };

        /**
         * Setting the 'left' and the 'top' css values for absolutely
         * positioned drawings anchored to the page.
         * This function is called, if drawings are inserted into the
         * drawing layer ('newValues' is true) or if they are removed
         * from the drawing layer('doClean' is true)
         * or during update of the positions (called from
         * updateAbsoluteDrawings). An update of the vertical
         * position can be necessary, if a paragraph containing a drawing
         * place holder element moves to another page. In this case the
         * absolute positioned drawing has to change its position, too.
         *
         * @param {jQuery} drawingNode
         *  One drawing node that is positioned absolutely. For convenience
         *  reasons this can also be the place holder node.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.newValues=false]
         *      If set to true, the drawing was inserted, set 'toPage' or modified
         *      in size or position. In this case the data values stored at the
         *      element need to be updated.
         *  @param {Boolean} [options.doClean=false]
         *      If set to true, the drawing was removed from the drawing layer. In
         *      this case the data values stored at the element need to be deleted.
         *  @param {Boolean} [options.isMarginalNode=false]
         *      Whether the drawing is inside header or footer.
         *  @param {Boolean} [options.isFooterNode=false]
         *      Whether the drawing is inside footer.
         *  @param {Boolean} [options.isBottomOffset=false]
         *      If set to true, the top value for the vertical offset is interpreted
         *      as bottom offset. This is only used for positions in footers to
         *      increase the precision.
         *  @param {Boolean} [options.forceUpdateAllDrawings=false]
         *      If set to true, all drawing positions are updated, even if the page
         *      did not change. This is especially necessary after loading the
         *      document.
         *  @param {String} [options.verticalMargin='']
         *      The vertical alignment, if the base is the margin. Allowed values are
         *      top, center, bottom, offset.
         *  @param {Boolean} [options.useBottomMargin=false]
         *      Whether a vertical alignment at the margin is related to the bottom
         *      margin. If not specified the top margin is used.
         *  @param {leftValue} [options.leftValue=0]
         *      The left page offset in pixel.
         *  @param {topValue} [options.topValue=0]
         *      The top page offset in pixel.
         *  @param {jQuery} [options.targetNode]
         *      An optional target node as an alternative to the page div.page. This
         *      is used for header or footer nodes.
         */
        this.setAbsoluteDrawingPosition = function (drawingNode, options) {

            var // whether the drawing offsets shall be cleaned
                doClean = getBooleanOption(options, 'doClean', false),
                // whether the values have changed (called from drawingStyles.updateDrawingFormatting)
                newValues = getBooleanOption(options, 'newValues', false),
                // whether the drawing is inside header or footer
                isMarginalNode = getBooleanOption(options, 'isMarginalNode', false),
                // whether the drawing is inside the footer
                isFooterNode = getBooleanOption(options, 'isFooterNode', false),
                // whether the update of all drawings is forced
                forceUpdateAllDrawings =  getBooleanOption(options, 'forceUpdateAllDrawings', false),
                // the left page offset in pixel
                leftValue = getNumberOption(options, 'leftValue', 0),
                // the top page offset in pixel
                topValue = getNumberOption(options, 'topValue', 0),
                // the page number, at which the drawing place holder is located
                pageNumber = 1,
                // the vertical and horizontal offsets relative to the page
                horizontalPageOffset = 0, verticalPageOffset = 0,
                // the vertical offset in the document
                documentOffset = 0,
                // the drawing in the drawing layer
                drawing = DOM.isDrawingPlaceHolderNode(drawingNode) ? $(DOM.getDrawingPlaceHolderNode(drawingNode)) : drawingNode,
                // an optional target node in header or footer
                inHeaderOrFooter = options && options.targetNode && options.targetNode.length > 0,
                // whether the topValue needs to be set as bottom offset (used in footers only)
                isBottomOffset = getBooleanOption(options, 'isBottomOffset', false),
                // the vertical alignment, if the base is the margin (this is dynamic corresponding to header and footer)
                // -> allowed values: top, center, bottom, offset
                verticalMargin = newValues ? getStringOption(options, 'verticalMargin', '') : (drawing.data('verticalMargin') || ''),
                // whether a vertical alignment at the margin is relative to the bottom margin
                useBottomMargin = getBooleanOption(options, 'useBottomMargin', false);

            if (newValues) {

                // hiding all absolute positioned drawings until page breaks are inserted
                // -> but not hiding marginal drawings, because the node in the collector for drawings might become invalid
                if (!pageBreaksInserted && !isMarginalNode) { hideDrawing(drawing); }

                // called from drawingStyles.updateDrawingFormatting
                horizontalPageOffset = leftValue;
                verticalPageOffset = topValue;

                if (!pageLayout) { pageLayout = docModel.getPageLayout(); }

                // determining the page number of the drawing, after the document is loaded completely.
                // The page number is always 1 inside headers or footers.
                if (self.isImportFinished() && !inHeaderOrFooter) {
                    pageNumber = pageLayout.getPageNumber(DOM.getDrawingPlaceHolderNode(drawing));
                    drawing.data('pageNumber', pageNumber);
                    drawing.attr('page-number', pageNumber);
                }

                // saving the values at the drawing node
                drawing.data('verticalPageOffset', verticalPageOffset);
                drawing.data('horizontalPageOffset', horizontalPageOffset);

                if (verticalMargin) {
                    drawing.data('verticalMargin', verticalMargin);
                } else {
                    drawing.removeData('verticalMargin');
                }

                drawing.data('isBottomOffset', isBottomOffset);
                drawing.data('isMarginalNode', isMarginalNode);
                drawing.data('isFooterNode', isFooterNode);
                drawing.data('useBottomMargin', useBottomMargin);

                // drawings, that are vertically aligned to the margin have an offset that is dependent from header and footer
                if (verticalMargin) {
                    verticalPageOffset = calculateVerticalPageOffset(verticalMargin, pageNumber, verticalPageOffset, useBottomMargin, isFooterNode, drawing);
                }

                documentOffset = (pageNumber === 1) ? verticalPageOffset : getVerticalPagePixelPosition(rootNode, pageLayout, pageNumber, docApp.getView().getZoomFactor() * 100, verticalPageOffset);

            } else if (doClean) {

                // removing the values at the drawing node
                drawing.removeData('verticalPageOffset');
                drawing.removeData('horizontalPageOffset');
                drawing.removeData('pageNumber');
                drawing.removeData('verticalMargin');
                drawing.removeData('isBottomOffset');
                drawing.removeData('isMarginalNode');
                drawing.removeData('isFooterNode');
                drawing.removeData('useBottomMargin');

                drawing.attr('page-number', null);

                documentOffset = 0;
                horizontalPageOffset = 0;

            } else {
                // this is the standard update case, only drawing is defined
                // -> modifications are only necessary, if the page has changed
                if (!pageLayout) { pageLayout = docModel.getPageLayout(); }

                if (drawing.data('currentPageNumber')) {
                    pageNumber = drawing.data('currentPageNumber');
                } else {
                    pageNumber = pageLayout.getPageNumber(DOM.getDrawingPlaceHolderNode(drawing));
                }

                // if it failed to determine the page, simply do nothing (leave drawing where it is)
                if (!pageNumber) { return; }

                // forcing an update of vertical alignment for this drawing node
                if (verticalMargin) { forceUpdateAllDrawings = true; }

                // page number not changed, nothing to do
                // -> but updating after first load is required, to be more precise!
                if (!forceUpdateAllDrawings && (pageNumber === drawing.data('pageNumber'))) { return; }

                drawing.data('pageNumber', pageNumber); // setting updated value
                drawing.attr('page-number', pageNumber);

                horizontalPageOffset = drawing.data('horizontalPageOffset');
                verticalPageOffset = drawing.data('verticalPageOffset');

                isBottomOffset = drawing.data('isBottomOffset');
                isMarginalNode = drawing.data('isMarginalNode');
                isFooterNode = drawing.data('isFooterNode');
                useBottomMargin = drawing.data('useBottomMargin');

                // drawings, that are vertically aligned to the margin have an offset that is dependent from header and footer
                if (verticalMargin) {
                    verticalPageOffset = calculateVerticalPageOffset(verticalMargin, pageNumber, verticalPageOffset, useBottomMargin, isFooterNode, drawing);
                }

                documentOffset = (pageNumber === 1 || isMarginalNode) ? verticalPageOffset : getVerticalPagePixelPosition(rootNode, pageLayout, pageNumber, docApp.getView().getZoomFactor() * 100, verticalPageOffset);
            }

            if (isBottomOffset) {
                if (drawing.data(DOM.DRAWING_SPACEMAKER_SHIFT)) { documentOffset += drawing.data(DOM.DRAWING_SPACEMAKER_SHIFT); }
                drawing.css({ bottom: documentOffset, top: '', left: horizontalPageOffset });
            } else {
                drawing.css({ top: documentOffset, bottom: '', left: horizontalPageOffset });
            }
        };

        /**
         * Handling drawings anchored to paragraphs.
         * Setting the 'left' and the 'top' css values for absolutely
         * positioned drawings anchored to a paragraph.
         * This function is called, if drawings are anchored to a paragraph
         * ('newValues' is true) or if the anchor is no longer a paragraph
         * ('doClean' is true) or during update of the positions (called from
         * updateParagraphDrawings).
         *
         * @param {jQuery} drawingNode
         *  One drawing node that is positioned absolutely at a paragraph.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.newValues=false]
         *      If set to true, the drawing was inserted, set 'toParagraph' or modified
         *      in size or position. In this case the data values stored at the
         *      element need to be updated.
         *  @param {Boolean} [options.doClean=false]
         *      If set to true, the drawing is no longer anchored to paragraph. In
         *      this case the data values stored at the element need to be deleted.
         *  @param {leftValue} [options.leftValue=0]
         *      The left paragraph offset in pixel.
         *  @param {topValue} [options.topValue=0]
         *      The top paragraph offset in pixel.
         *  @param {jQuery} [options.targetNode]
         *      An optional target node as an alternative to the page div.page. This
         *      is used for header or footer nodes.
         */
        this.setAbsoluteDrawingParagraphPosition = function (drawingNode, options) {

            var // whether the drawing offsets shall be cleaned
                doClean = getBooleanOption(options, 'doClean', false),
                // whether the values have changed (called from drawingStyles.updateDrawingFormatting)
                newValues = getBooleanOption(options, 'newValues', false),
                // the left page offset in pixel
                leftValue = getNumberOption(options, 'leftValue', 0),
                // the top page offset in pixel
                topValue = getNumberOption(options, 'topValue', 0),
                // the text wrap mode
                textWrapMode = getStringOption(options, 'textWrapMode', null),
                // the text wrap mode
                textWrapSide = getStringOption(options, 'textWrapSide', null),
                // the vertical and horizontal offsets relative to the paragraph
                horizontalParagraphOffset = 0, verticalParagraphOffset = 0,
                // an optional vertical offset that is caused by page breaks inside the paragraph before the drawing
                pageBreakOffset = 0,
                // the drawing in the drawing layer
                drawing = DOM.isDrawingPlaceHolderNode(drawingNode) ? $(DOM.getDrawingPlaceHolderNode(drawingNode)) : drawingNode;

            if (newValues) {

                // setting an id to the drawing (used to restore correctly after load from local storage)
                if (!$(drawing).attr('data-drawingID')) { drawing.attr('data-drawingID', self.getNextDrawingID()); }

                // called from drawingStyles.updateDrawingFormatting
                horizontalParagraphOffset = leftValue;
                verticalParagraphOffset = topValue;

                // saving the values at the drawing node
                drawing.data('verticalParagraphOffset', verticalParagraphOffset);
                drawing.data('horizontalParagraphOffset', horizontalParagraphOffset);
                drawing.data('textWrapMode', textWrapMode);
                drawing.data('textWrapSide', textWrapSide);

                // handling z-index of the paragraph, if required
                if (docApp.isTextApp() && docModel.isIncreasedDocContent() && drawing.data('textWrapMode') === 'none') { handleParagraphIndex(drawing.parent(), drawing); }

            } else if (doClean) {

                // removing the values at the drawing node
                drawing.removeData('verticalParagraphOffset');
                drawing.removeData('horizontalParagraphOffset');
                drawing.removeData('textWrapMode');
                drawing.removeData('textWrapSide');

                horizontalParagraphOffset = 0;
                verticalParagraphOffset = 0;

            } else {

                // checking whether there are manual page breaks inside the paragraph before this drawing (56266)
                pageBreakOffset = checkManualPageBreakInParagraph(drawing);

                if (pageBreakOffset) {
                    verticalParagraphOffset = drawing.data('verticalParagraphOffset') + pageBreakOffset;
                } else {
                    // this is the standard update case, only drawing is defined
                    verticalParagraphOffset = drawing.data('verticalParagraphOffset');
                }

                horizontalParagraphOffset = drawing.data('horizontalParagraphOffset');
            }

            // if the text is only above and below the drawing and the vertical offset is negative, the bottom border
            // of the drawing must be used to calculate the distance to the paragraph. So the drawing is shifted upwards,
            // but only until the header is reached.
            var negativeVerticalOffset = verticalParagraphOffset < 0 || (drawing.data(DOM.DRAWING_MARGINS_PIXEL) && drawing.data('verticalParagraphOffset') < drawing.data(DOM.DRAWING_MARGINS_PIXEL).marginTop);
            if (docApp.isTextApp() && negativeVerticalOffset && drawing.data('textWrapMode') === 'topAndBottom' && !DOM.isMarginalNode(drawing)) {
                verticalParagraphOffset -= calculateUpwardShift(drawing, verticalParagraphOffset);
            }

            drawing.css({ top: verticalParagraphOffset, bottom: '', left: horizontalParagraphOffset });

            // a refresh of the drawing selection in marginal nodes might be required (DOCS-4413)
            if (docApp.isTextApp() && DOM.isMarginalNode(drawing) && docModel.getSelection().isSelectedDrawingNode(drawingNode)) { docModel.getSelection().drawDrawingSelection(drawingNode); }
        };

        /**
         * Shifting a drawing from the page content node into the drawing
         * layer node. This happens during loading the document or if a
         * drawing mode is switched from 'asCharacter' or 'toParagraph' to
         * 'toPage'.
         *
         * @param {jQuery} drawing
         *  One drawing node that is positioned absolutely.
         *
         * @param {jQuery} [target]
         *  An optional alternative for the destination of the drawing layer. If not
         *  specified, div.page needs to be the parent of the drawing layer. For
         *  headers and footers, it is also possible, that div.header or div.footer
         *  are the parent for the drawing layer.
         */
        this.shiftDrawingIntoDrawingLayer = function (drawing, target) {

            var // the drawing layer node for absolute positioned drawings (will be created, if required)
                drawingLayerNode = self.getDrawingLayerNode(target),
                // the place holder element for the drawing in the page content
                drawingPlaceHolder = $('<div>', { contenteditable: false }).addClass('inline ' + DOM.DRAWINGPLACEHOLDER_CLASS),
                // a drawing ID to connect drawing and drawingPlaceHoder node
                drawingID = self.getNextDrawingID(),
                // the string identifier for the target
                targetString = target ? $(target).attr('data-container-id') : null;

            // removing a space maker, if required
            self.removeSpaceMakerNode(drawing);

            // adding the target string to the drawing ID
            if (targetString) { drawingID = drawingID + '_' + targetString; }

            // creating and inserting the drawing place holder behind the drawing
            drawing.after(drawingPlaceHolder);

            // shifting the drawing itself into the drawing layer node
            drawingLayerNode.append(drawing);

            drawing.attr('data-drawingID', drawingID);
            drawingPlaceHolder.attr('data-drawingID', drawingID);

            // direct link between the drawing and drawing place holder (not used in header or footer)
            if (!DOM.isHeaderOrFooter(target)) {
                drawing.data(DOM.DRAWINGPLACEHOLDER_LINK, getDomNode(drawingPlaceHolder));
                drawingPlaceHolder.data(DOM.DRAWINGPLACEHOLDER_LINK, getDomNode(drawing));
            }

            // and finally registering it at the drawing layer handler (the model)
            // -> do not register drawings in header or footer, that are NOT located
            // inside 'div.header-footer-placeholder'. All other drawings are
            // 'throw-away' drawings.
            addIntoDrawingModel(drawing, DOM.isHeaderOrFooter(target));
        };

        /**
         * Shifting a drawing from the drawing layer node into the page content
         * node. This happens if a drawing mode is switched from 'toPage' to
         * 'asCharacter' or 'toParagraph'.
         *
         * @param {jQuery} drawing
         *  One drawing node that is no longer positioned absolutely.
         *  This can be the place holder node or the drawing in the
         *  drawing layer node for convenience reasons.
         */
        this.shiftDrawingAwayFromDrawingLayer = function (drawing) {

            var // the drawing layer node for absolute positioned drawings
                drawingLayerNode = self.getDrawingLayerNode(),
                // the drawing node in the drawing layer
                drawingNode = null,
                // the drawing place holder in the page content
                placeHolderNode = null,
                // the space maker node for the absolute positioned drawing
                spaceMakerNode = null,
                // whether the drawing is inside header or footer
                isMarginalNode = false;

            // supporting space holder node and drawing node in drawing layer
            if (DOM.isDrawingPlaceHolderNode(drawing)) {
                drawingNode = DOM.getDrawingPlaceHolderNode(drawing);
                placeHolderNode = drawing;
            } else if (DOM.isDrawingLayerNode($(drawing).parent())) {
                drawingNode = drawing;
                placeHolderNode = DOM.getDrawingPlaceHolderNode(drawing);
            } else {
                return null;
            }

            // checking for header and footer
            isMarginalNode = DOM.isMarginalNode($(placeHolderNode).parent());

            // ... removing the space maker node
            spaceMakerNode = DOM.getDrawingSpaceMakerNode(drawingNode);
            if (spaceMakerNode) {
                removeDrawingSpaceMaker(spaceMakerNode);
                $(drawingNode).removeData(DOM.DRAWING_SPACEMAKER_LINK);
            }

            // shifting the drawing behind the place holder node
            $(placeHolderNode).after(drawingNode);

            // ... removing the place holder node
            $(placeHolderNode).remove();
            $(drawingNode).removeData(DOM.DRAWINGPLACEHOLDER_LINK);
            $(drawingNode).removeAttr('data-drawingID');

            // delete drawing layer, if it is no longer required
            if (drawingLayerNode.length === 0) {
                // creating the drawing layer
                $(drawingLayerNode).remove();

                if (!isMarginalNode) {
                    // no more drawings in collector
                    drawings = [];
                }
            }

            // updating the model
            removeFromDrawingModel(drawing, isMarginalNode);
        };

        /**
         * Removing all drawings from the drawing layer, that have place holder nodes
         * located inside the specified node. The node can be a paragraph that will be
         * removed. In this case it is necessary, that the drawings in the drawing layer
         * are removed, too, and that the model is updated.
         * This is a simplified function, that simply removes the drawing nodes
         * located inside the drawing layer. It does not take care of space maker
         * nodes or place holder nodes. This is not necessary, because it is assumed,
         * that the specified node will be removed, too.
         *
         * @param {HTMLElement|jQuery} node
         *  The node, for which all drawings in the drawing layer will be removed. This
         *  assumes that the node contains place holder nodes. Only the drawings in the
         *  drawing layer will be removed. This function does not take care of place
         *  holder nodes and space maker nodes.
         */
        this.removeAllInsertedDrawingsFromDrawingLayer = function (node) {

            var // the collection of all place holder nodes
                allPlaceHolder = $(node).find(DOM.DRAWINGPLACEHOLDER_NODE_SELECTOR);

            // starting to remove the drawings, if there are place holder nodes
            if (allPlaceHolder.length > 0) {
                _.each(allPlaceHolder, function (placeHolderNode) {

                    var // the drawing node corresponding to the place holder node
                        drawingNode = DOM.getDrawingPlaceHolderNode(placeHolderNode),
                        // the optional space maker node corresponding to the drawing node
                        spaceMakerNode = null,
                        // whether the drawing place holder node is inside header or footer
                        isMarginalNode = false;

                    if (drawingNode) {

                        // whether the drawing place holder node is inside header or footer
                        isMarginalNode = DOM.isMarginalNode($(placeHolderNode).parent());

                        // ... removing the space maker node
                        spaceMakerNode = DOM.getDrawingSpaceMakerNode(drawingNode);

                        if (spaceMakerNode) {
                            removeDrawingSpaceMaker(spaceMakerNode);
                            $(drawingNode).removeData(DOM.DRAWING_SPACEMAKER_LINK);
                        }

                        // updating the model
                        removeFromDrawingModel(drawingNode, isMarginalNode);

                        // ... and removing the node in the drawing layer from the DOM
                        $(drawingNode).remove();
                    } else {
                        globalLogger.warn('removeAllInsertedDrawingsFromDrawingLayer(): failed to find drawing node for place holder node!');
                    }
                });
            }
        };

        /**
         * Removing one drawing element from the drawing layer node. The parameter
         * is the place holder node in the page content.
         *
         * @param {jQuery} placeHolderNode
         *  The place holder node in the page content
         *
         * @param {object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.keepDrawingLayer=false]
         *      If set to true, the drawing in the drawing layer corresponding to the
         *      drawing place holder is NOT removed. This is for example necessary after
         *      a split of paragraph. The default is 'false' so that place holder node,
         *      space maker node and drawing in drawing layer are removed together.
         */
        this.removeFromDrawingLayer = function (placeHolderNode, options) {

            var // the drawing node in the drawing layer
                drawing = DOM.getDrawingPlaceHolderNode(placeHolderNode),
                // whether the drawings in the drawing layer shall not be removed
                // -> this is necessary after splitting a paragraph
                keepDrawingLayer = getBooleanOption(options, 'keepDrawingLayer', false),
                // the space maker node for the absolute positioned drawing
                spaceMakerNode = null,
                // whether the drawing is inside the margins
                isMarginalNode = DOM.isMarginalNode($(placeHolderNode).parent());

            if (keepDrawingLayer) {

                // removing only the place holder node
                $(placeHolderNode).removeData(DOM.DRAWINGPLACEHOLDER_LINK);
                $(placeHolderNode).remove();

            } else {

                // removing links to other nodes
                $(placeHolderNode).removeData(DOM.DRAWINGPLACEHOLDER_LINK);
                $(drawing).removeData(DOM.DRAWINGPLACEHOLDER_LINK);

                // safely release image data (see comments in BaseApplication.destroyImageNodes())
                docApp.destroyImageNodes(drawing);

                // ... removing the space maker node
                spaceMakerNode = DOM.getDrawingSpaceMakerNode(drawing);

                if (spaceMakerNode) {
                    removeDrawingSpaceMaker(DOM.getDrawingSpaceMakerNode(drawing));
                    $(drawing).removeData(DOM.DRAWING_SPACEMAKER_LINK);
                }

                // ... removing the drawing in the drawing layer
                $(drawing).remove();

                // ... removing the place holder node
                $(placeHolderNode).remove();

                // ... and finally updating the model
                removeFromDrawingModel(drawing, isMarginalNode);
            }
        };

        /**
         * After splitting a paragraph with the place holder in the new paragraph (that
         * was cloned before), it is necessary that the drawing in the drawing layer
         * updates its link to the place holder drawing in the new paragraph.
         * This is not necessary in headers or footers, where the links are not used.
         *
         * @param {Node|jQuery} paragraph
         *  The paragraph node.
         */
        this.repairLinksToPageContent = function (paragraph) {

            var // the function that is used to find the place holder nodes. If only a paragraph is updated
                // it is sufficient to search for children. In the case of a page content node, 'find' needs to be used
                searchFunction = DOM.isParagraphNode(paragraph) ? 'children' : 'find',
                // whether each single node needs to be checked
                checkMarginal = (searchFunction === 'find');

            if (!DOM.isMarginalNode(paragraph)) {
                _.each($(paragraph)[searchFunction](DOM.DRAWINGPLACEHOLDER_NODE_SELECTOR), function (placeHolder) {
                    var drawing = null;
                    if (!checkMarginal || !DOM.isMarginalNode(placeHolder)) {
                        drawing = DOM.getDrawingPlaceHolderNode(placeHolder);
                        $(drawing).data(DOM.DRAWINGPLACEHOLDER_LINK, placeHolder);
                    }
                });
            }

        };

        /**
         * Receiving the drawing layer node for absolute positioned drawings. It
         * is not created within this function. If it does not exist, null is
         * returned.
         *
         * @param {jQuery} [target]
         *  An optional alternative for the destination of the drawing layer. If not
         *  specified, div.page needs to be the parent of the drawing layer. For
         *  headers and footers, it is also possible, that div.header or div.footer
         *  are the parent for the drawing layer.
         *
         * @returns {jQuery|Null}
         *  The drawing layer node, if is exists. Otherwise null.
         */
        this.returnDrawingLayerNode = function (target) {

            var // the document page node or the target in header or footer
                pageNode = (target && target.length > 0) ? target : docModel.getNode(),
                // the drawing layer node in the dom
                drawingLayerNode = pageNode.children(DOM.DRAWINGLAYER_NODE_SELECTOR);

            return (drawingLayerNode && drawingLayerNode.length > 0) ? drawingLayerNode : null;
        };

        /**
         * Receiving the drawing layer node for absolute positioned drawings.
         * It is created, if it does not exist yet.
         *
         * @param {jQuery} [target]
         *  An optional alternative for the destination of the drawing layer. If not
         *  specified, div.page needs to be the parent of the drawing layer. For
         *  headers and footers, it is also possible, that div.header or div.footer
         *  are the parent for the drawing layer.
         *
         * @returns {jQuery}
         *  The drawing layer node.
         */
        this.getDrawingLayerNode = function (target) {

            var // the document page node or the target in header or footer
                pageNode = (target && target.length > 0) ? target : docModel.getNode(),
                // the drawing layer node in the dom
                drawingLayerNode = self.returnDrawingLayerNode(pageNode),
                // the page content node
                pageContentNode = null;

            // create drawing layer node, if necessary
            if (!drawingLayerNode || (drawingLayerNode && drawingLayerNode.length === 0)) {
                // the drawing layer needs to be created next to the page content node. In the case of
                // a header or footer node, it will be created directly below the element div.header
                // or div.footer. In the latter case, the drawing layer is created directly behind
                // the last paragraph.
                pageContentNode = (target && target.length > 0) ? DOM.getMarginalContentNode(pageNode) : DOM.getPageContentNode(pageNode);
                drawingLayerNode = $('<div>').addClass(DOM.DRAWINGLAYER_CLASS);

                //workaround for abs pos element, which get the "moz-dragger" but we dont want it
                drawingLayerNode.attr('contenteditable', 'false');

                // Adding the text drawing layer behind an optional footer wrapper
                if (!target && DOM.isFooterWrapper(pageContentNode.next())) { pageContentNode = pageContentNode.next(); }
                // inserting the text drawing layer node
                pageContentNode.after(drawingLayerNode);
            }

            return drawingLayerNode;
        };

        /**
         * Handling drawings anchored to paragraphs.
         * Removing the space maker node of a drawing node.
         * Currently this is only used for drawings anchored at the
         * paragraph.
         *
         * @param {jQuery|Node} drawingNode
         *  The drawing node whose space maker node shall be removed.
         */
        this.removeSpaceMakerNode = function (drawingNode) {

            var // the space maker node for the absolute positioned drawing
                spaceMakerNode = DOM.getDrawingSpaceMakerNode(drawingNode);

            // ... removing the space maker node
            if (spaceMakerNode) {
                removeDrawingSpaceMaker(spaceMakerNode);
                $(drawingNode).removeData(DOM.DRAWING_SPACEMAKER_LINK);
            }

        };

        /**
         * Removing a specific drawing from the paragraph drawing containers. This is especially useful,
         * if the drawing is switched from paragraph-aligned to in-line.
         *
         * @param {jQuery|Node} drawingNode
         *  The drawing node, that will be removed from the model for paragraph aligned drawings.
         */
        this.removeDrawingFromModelContainer = function (drawingNode) {

            var // the drawing DOM node
                localDrawingNode = getDomNode(drawingNode);

            if (paragraphDrawingCollectorUpdateAlways.length > 0 && _.contains(paragraphDrawingCollectorUpdateAlways, localDrawingNode)) {
                paragraphDrawingCollectorUpdateAlways = $(_.without(paragraphDrawingCollectorUpdateAlways, localDrawingNode));
            }

            if (paragraphDrawingCollector.length > 0) {
                paragraphDrawingCollector = $(_.without(paragraphDrawingCollector, localDrawingNode));
            }

        };

        /**
         * Handling drawings anchored to paragraphs.
         * Registering an element for updating the absolute drawings anchored to paragraphs.
         * This element can be a drawings itself or it can be container like a paragraph, that
         * can include paragraph anchored drawings.
         *
         * @param {jQuery|Node} element
         *  The drawing node or a container of drawing nodes, that will be registered for the
         *  next update of paragraph anchored absolute drawings.
         */
        this.registerUpdateElement = function (element) {

            if (DOM.isAbsoluteParagraphDrawing(element)) {
                paragraphDrawingCollector = paragraphDrawingCollector.add(element);
            } else if (element && DOM.hasAbsoluteParagraphDrawing(element)) {
                _.each($(element).find(DOM.ABSOLUTE_DRAWING_SELECTOR), function (drawing) {
                    paragraphDrawingCollector = paragraphDrawingCollector.add(drawing);
                });
            }
        };

        /**
         * Stopping listening to event 'update:absoluteElements'. This is specific
         * to 'presentation' application, where this drawing layer is not needed.
         * TODO: This solution needs an improvement.
         */
        this.stopUpdatingAbsoluteElements = function () {
            self.stopListeningTo(docModel, 'update:absoluteElements', updateDrawings);
        };

        /**
         * Helper function to get the drawing attributes for the drawing order. This
         * are 'anchorBehindDoc' and 'anchorLayerOrder'. This properties are calculated
         * inside the function 'updateDrawingOrder', but there are cases, where only
         * the attributes are required and no complete operation. Therefore this
         * wrapper function exists, that uses 'updateDrawingOrder', but only uses
         * the drawing attributes from the generated operations.
         *
         * @param {String} value
         *  'front', 'back', 'forward' or 'backward'. See description at function
         *  'updateDrawingOrder'.
         *
         * @param {jQuery|Node} refNode
         *  The reference node, that can be used to determine the page. This is necessary
         *  to find all drawings on this page.
         *
         * @returns {Object|Null}
         *  The calculated drawing attributes that specify the vertical drawing order.
         */
        this.getDrawingOrderAttributes = function (value, refNode) {

            // the operations generator object
            var generator = docModel.createOperationGenerator(); // using a local generator to get calculated attributes

            self.updateDrawingOrder(value, { refNode: $(refNode), generator });

            // using the last operation to get the required drawing attributes for anchorBehindDoc and anchorLayerOrder
            var operation = _.last(generator.getOperations());

            return (operation && operation.attrs && operation.attrs.drawing) || null;
        };

        /**
         * updates the depth order of current selected drawing/textframe
         *
         * updates attribute "anchorLayerOrder" as depth value.
         *
         * and it sets "anchorBehindDoc" to true
         * if current drawing is moved behind another drawing that has "anchorBehindDoc" on true, too.
         * and it sets "anchorBehindDoc" to false
         * if current drawing is moved in front of another drawing that has "anchorBehindDoc" on false, too.
         *
         * This function supports the change of 'anchorBehindDoc' by shifting drawings forward or backwards.
         * In MS Word there are two stacks of drawings: One stack of drawings for all drawings above the text,
         * and independent from that another stack for the drawings below this text. To support this, in the
         * future a checkbox like 'Bring behind the text' is required.
         * With the current implementation a dynamic switch is supported, because there is only one stack
         * for all drawings. 'Send to back' shifts the topmost drawing (above the text) behind the last
         * drawing behind the text. If there is no drawing behind the text, no drawing is ever shifted
         * behind the text.
         *
         * @param {String} value
         *  'front' & 'back'
         *  collect all drawings on same page which intersects with current drawing
         *  and tries to put current drawing in a new slot inside the drawings order
         *  if there is no slot, all drawings are update with a minimum distance of 1024
         *  and a new slot is searched for (fix for odt)
         *
         *  'forward' & 'backward'
         *  collect all drawings on same page
         *  and set new depth value on the edge of the list  +-1024
         *
         * @param {Object} [options]
         *  An optional object that can be used to generate the operation for an external
         *  operation generator. This is especially useful, if the operation generated within
         *  this function shall be used in a surrounding context. If this is the case, three
         *  different properties must be set within this options object. None of this two
         *  properties can be left out:
         *  @param {Object} [options.generator]
         *      The operations generation of the surrounding context.
         *  @param {jQuery} [options.refNode]
         *      The reference node, that can be used to determine the page. This is necessary
         *      to find all drawings on this page.
         */
        this.updateDrawingOrder = function (value, options) {

            // the operations generator object
            var generator = null;
            // a reference node to determine the page
            var refNode = null;
            // whether an external operation generator shall be used
            var useExternalGenerator = false;
            // a helper offset for the calculations
            var behindOffset = 0xFFFFFFFF; // stay in 32-bit range (61843)

            if (options) {
                // paragraph and generator must be specified
                refNode = options.refNode;
                generator = options.generator;
                useExternalGenerator = true;

                if (!refNode || !generator) {
                    globalLogger.error('DrawingLayer.updateDrawingOrder(): Not all required values available for external operation generator!');
                }

            } else {
                generator = docModel.createOperationGenerator();
            }

            function getOxoPositionLocal(drawing) {
                var node = docModel.getNode();
                if (DOM.isMarginalNode(drawing)) {
                    node = DOM.getMarginalTargetNode(node, drawing);
                }
                return getOxoPosition(node, drawing);
            }

            function updateDrawingSpacing(drawings, secondTry) {
                if (secondTry) {
                    globalLogger.warn('DrawingLayer.updateDrawingOrder() is called recursiv too often! we cancel here.');
                    return;
                }

                _.each(drawings, function (drawing, index) {
                    var currentOrder = parseInt(drawing.getAttribute('layer-order'), 10);
                    var behind = false;
                    if (currentOrder < 0) { // dynamically switching drawing before and behind the text
                        behind = true;
                        currentOrder += behindOffset;
                    }

                    var pos = getOxoPositionLocal(drawing);
                    var destOrder = 0xffff + ((index + 2) * 1024);
                    if (behind) {
                        drawing.setAttribute('layer-order', destOrder - behindOffset);
                    } else {
                        drawing.setAttribute('layer-order', destOrder);
                    }
                    generator.generateOperation(SET_ATTRIBUTES, { start: _.copy(pos), attrs: { drawing: { anchorBehindDoc: behind, anchorLayerOrder: destOrder } } });
                });

                updateDrawingOrderImpl(value, true);
            }

            function updateDrawingOrderImpl(value, secondTry) {
                var jNode = refNode ? refNode : docModel.getSelection().getSelectedDrawing();
                if (!jNode || !jNode.length) {
                    jNode = docModel.getSelection().getSelectedTextFrameDrawing();
                }
                var node = jNode[0];
                var allDrawings = findAllDrawingsOnPage(jNode);

                var oldOrderId = parseInt(node.getAttribute('layer-order'), 10);
                var newOrderId = null;
                var others = null;
                var otherOrder = null;

                switch (value) {
                    case 'forward':
                        others = findAllIntersections(node, allDrawings);

                        _.find(others, function (other, index) {
                            var otherOrder = parseInt(other.getAttribute('layer-order'), 10);
                            if (otherOrder >= oldOrderId) {
                                var nextOther = others[index + 1];
                                if (!nextOther) {
                                    if (otherOrder < -1024 || otherOrder > 0) {
                                        newOrderId = otherOrder + 1024;
                                    } else {
                                        updateDrawingSpacing(allDrawings, secondTry);

                                    }
                                } else {
                                    var nextOtherOrder = parseInt(nextOther.getAttribute('layer-order'), 10);
                                    if (Math.abs(nextOtherOrder - otherOrder) < 2) {
                                        updateDrawingSpacing(allDrawings, secondTry);
                                    } else {
                                        newOrderId = Math.round((nextOtherOrder + otherOrder) * 0.5);
                                    }
                                }
                                return true;
                            }
                        });
                        break;
                    case 'backward':
                        others = findAllIntersections(node, allDrawings);
                        others.reverse();

                        _.find(others, function (other, index) {
                            var otherOrder = parseInt(other.getAttribute('layer-order'), 10);
                            if (otherOrder <= oldOrderId) {
                                var nextOther = others[index + 1];
                                if (!nextOther) {
                                    if (otherOrder > 1024 || otherOrder < 0) {
                                        newOrderId = otherOrder - 1024;
                                    } else {
                                        updateDrawingSpacing(allDrawings, secondTry);
                                    }
                                } else {
                                    var nextOtherOrder = parseInt(nextOther.getAttribute('layer-order'), 10);
                                    if (Math.abs(nextOtherOrder - otherOrder) < 2) {
                                        updateDrawingSpacing(allDrawings, secondTry);
                                    } else {
                                        newOrderId = Math.round((nextOtherOrder + otherOrder) * 0.5);
                                    }
                                }
                                return true;
                            }
                        });
                        break;
                    case 'front':
                        var last = _.last(allDrawings);
                        if (last && last !== node) {
                            otherOrder = parseInt(last.getAttribute('layer-order'), 10);
                            if (otherOrder < -1024 || otherOrder >= 0) {
                                newOrderId = otherOrder + 1024;
                            } else {
                                updateDrawingSpacing(allDrawings, secondTry);
                            }
                        }
                        break;
                    case 'back':
                        var first = allDrawings[0];
                        if (first && first !== node) {
                            otherOrder = parseInt(first.getAttribute('layer-order'), 10);
                            if (otherOrder > 1024 || otherOrder < 0) {
                                newOrderId = otherOrder - 1024;
                            } else {
                                updateDrawingSpacing(allDrawings, secondTry);
                            }
                        }
                        break;
                }

                if (newOrderId !== null) {
                    var orderId = newOrderId;
                    var behindDoc = false;
                    if (newOrderId < 0) { // dynamically switching drawing before and behind the text
                        orderId = newOrderId + behindOffset;
                        behindDoc = true;
                    }

                    generator.generateOperation(SET_ATTRIBUTES, { start: getOxoPositionLocal(node), attrs: { drawing: { anchorBehindDoc: behindDoc, anchorLayerOrder: orderId } } });
                }
            }

            updateDrawingOrderImpl(value);
            if (!useExternalGenerator) { docModel.applyOperations(generator); }
        };

        /**
         * drawings outside of the page-bounds are hidden with css overflow
         * drawings outside of the page-break are hidden here
         *
         * with Rectangle.intersect(), a drawing rect and page rect
         * cropping on top and on bottom of a drawing will be calculated
         * and set via css "clip"
         */
        this.updateCropDrawing = function (drawing, drawingAttributes) {

            if (SMALL_DEVICE) { return; }

            var pageLayout = docModel.getPageLayout();
            var pageHeight = pageLayout.getPageAttribute('height');

            var pageRect = new Rectangle(0, 0, 1e+25, pageHeight);
            var drawRect = new Rectangle(drawingAttributes.anchorHorOffset, drawingAttributes.anchorVertOffset, drawingAttributes.width, drawingAttributes.height);

            if (drawingAttributes.anchorVertBase === 'margin') {
                drawRect.top += pageLayout.getPageAttribute('marginTop');
            }

            var interRect = pageRect.intersect(drawRect);

            if (interRect && (drawRect.top !== interRect.top || drawRect.height !== interRect.height)) {
                var cropTop = Math.round(convertHmmToLength(interRect.top - drawRect.top, 'px'));

                if (cropTop === 0) {
                    var cropHeight = Math.round(convertHmmToLength(interRect.height, 'px'));
                    drawing.css('clip', 'rect(-20000px 20000px ' + (cropHeight + cropTop) + 'px -20000px)');
                } else {
                    drawing.css('clip', 'rect(' + cropTop + 'px 20000px 20000px -20000px)');
                }
            } else {
                this.resetCropDrawing(drawing);
            }
        };

        /**
         * Set clip css property for paragraph aligned marginal drawings, that go outside of the marginal container.
         *
         * @param {jQuery|Node} drawing
         *  drawing node for which clip will be evaluated.
         */
        this.updateCropMarginalParagraphDrawing = function (drawing) {
            if (SMALL_DEVICE) { return; }

            // css clip support: depprecated
            // clip-path support: no IE11, EDGE - others with -webkit prefix

            var top = '0px';
            var right = 'auto';
            var bottom = 'auto';
            var left = '0px';
            var marginalContainer = DOM.getClosestMarginalTargetNode(drawing);
            if (!marginalContainer || !marginalContainer.length) { return; }
            if (isRotatedDrawingNode(drawing)) { return; } // see bug #62389
            var isHeader = DOM.isHeaderNode(marginalContainer);
            var marginalRect = getDomNode(marginalContainer).getBoundingClientRect();
            var drawRect = getDomNode(drawing).getBoundingClientRect();
            var shouldCrop = false;

            if (!isHeader && drawRect.bottom > marginalRect.bottom) {
                bottom = drawRect.height - (drawRect.bottom - marginalRect.bottom) + 'px';
                shouldCrop = true;
            } else if (isHeader && drawRect.top < marginalRect.top) {
                top = marginalRect.top - drawRect.top + 'px';
                shouldCrop = true;
            }

            if (drawRect.right > marginalRect.right) {
                right = drawRect.width - (drawRect.right - marginalRect.right) + 'px';
                shouldCrop = true;
            } else if (drawRect.left < marginalRect.left) {
                left = marginalRect.left - drawRect.left + 'px';
                shouldCrop = true;
            }

            if (shouldCrop) {
                drawing.css('clip', 'rect(' + top + ' ' + right + ' ' + bottom + ' ' + left + ')');
            } else {
                this.resetCropDrawing(drawing);
            }
        };

        /**
         * resets css "clip"
         */
        this.resetCropDrawing = function (drawing) {
            if (SMALL_DEVICE) { return; }

            drawing.css('clip', '');
        };

        // initialization -----------------------------------------------------

        docApp.onInit(function () {

            if (!docApp.isTextApp()) { return; } // drawinglayer currently only supported in OX Text

            docModel = docApp.getModel();

            pageLayout = docModel.getPageLayout();

            self.waitForImportSuccess(handleImportSuccess);

            docModel.one('pageBreak:after', handlePageBreakAfter);

            self.listenTo(docModel, 'update:drawingOrder', handleDrawingOrder);

            self.listenTo(docModel, 'update:absoluteElements', updateDrawings);

            self.listenTo(docModel, 'document:reloaded', handleDocumentReload);

            // register handler for tasks, when the header or footer are no longer active and all marginal nodes are exchanged
            self.listenTo(pageLayout, 'updateMarginal:leave', handleMarginalLeave);
        });

    } // class DrawingLayer
}

// export =================================================================

export default DrawingLayer;
