/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math, fun } from '@/io.ox/office/tk/algorithms';
import { convertHmmToLength } from '@/io.ox/office/tk/dom';
import { Canvas } from '@/io.ox/office/tk/canvas';

import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { ACTIVE_CROPPING_CLASS, ACTIVE_CROPPING_SELECTOR, CROPPING_RESIZER_CLASS, HIDE_CANVAS_ON_CROP_CLASS,
    drawSelection, getDrawingRotationAngle, getNormalizedMoveCoordinates, getNormalizedResizeDeltas,
    isFlippedHorz, isFlippedVert, toggleTracking, isActiveCropping } from '@/io.ox/office/drawinglayer/view/drawingframe';

// private functions ==========================================================

// convert px to percentage
function calcCropSize(frameSize, left, top) {
    return {
        left: Math.round((left * 100) / frameSize.width, 1),
        right: Math.round(((frameSize.width - frameSize.drawingW + left) * 100) / frameSize.width, 1),
        top: Math.round((top * 100) / frameSize.height, 1),
        bottom: Math.round(((frameSize.height - frameSize.drawingH + top) * 100) / frameSize.height, 1)
    };
}

const getResizerImages = fun.once(() => {

    var canvas = new Canvas({ location: { width: 17, height: 17 } });
    var minPos = 1.5;
    var maxPos = 16.5;
    var oneThirdPos = (maxPos - minPos) / 3;

    var dataURL1 = canvas.renderToDataURL(function (ctx) {
        ctx.setLineStyle({ style: 'white', width: 1 });
        ctx.setFillStyle('black');
        ctx.drawPolygon([[minPos, minPos], [minPos, oneThirdPos], [maxPos, oneThirdPos], [maxPos, minPos]]);
    });

    var dataURL2 = canvas.clear().renderToDataURL(function (ctx) {
        ctx.setLineStyle({ style: 'white', width: 1 });
        ctx.setFillStyle('black');
        ctx.drawPolygon([[minPos, minPos], [minPos, maxPos], [maxPos, maxPos], [maxPos, 11.5], [oneThirdPos + minPos, 11.5], [oneThirdPos + minPos, minPos]]);
    });

    canvas.destroy();

    return [dataURL1, dataURL2];
});

// public functions ===========================================================

/**
 * Makes crop frame and all necessary selection nodes for cropping.
 *
 * @param {jQuery} drawing
 * @param {jQuery} cropNode
 * @param {Number} zoomFactor
 */
export function drawCropFrame(drawing, cropNode, zoomFactor) {
    drawSelection(cropNode, { movable: true, resizable: true, zoomValue: zoomFactor * 100, scaleHandles: 1.5 / zoomFactor });

    drawing.addClass(ACTIVE_CROPPING_CLASS);
    drawing.find('>.selection').addClass('crop-selection');
    var dataSelection = drawing.data('selection');
    var drawingSelectionNode = dataSelection || drawing;
    if (dataSelection) { dataSelection.addClass(ACTIVE_CROPPING_CLASS).find('>.selection').addClass('crop-selection'); }
    var drawingSelResizers = drawingSelectionNode.find('>.selection').find('>.resizers>[data-pos]');
    const resizerImages = getResizerImages();
    _.each(drawingSelResizers, function (resizer, i) {
        var pos = i % 2; // only 2 original images
        var img = $('<img src="' + resizerImages[pos] + '">');
        img.addClass(CROPPING_RESIZER_CLASS); // setting a marker class to the image node
        $(resizer).append(img);
    });
    cropNode.addClass('src-img');

    toggleTracking(cropNode, true);
    if (dataSelection) { toggleTracking(dataSelection.find('>.selection'), true); }
}

/**
 * Clears and cleans up active drawing after crop.
 *
 * @param {jQuery} activeDrawing
 */
export function clearCropFrame(activeDrawing) {
    activeDrawing.removeClass(ACTIVE_CROPPING_CLASS).find('.crop').remove();
    activeDrawing.find('>.selection')
        .removeClass('crop-selection')
        .find('img').remove();
    var dataSelection = activeDrawing.data('selection');
    if (dataSelection) {
        dataSelection.removeClass(ACTIVE_CROPPING_CLASS)
            .find('>.selection').removeClass('crop-selection')
            .find('img').remove();
    }
}

/**
 * Repaints crop frame of the passed drawing
 *
 * @param {jQuery} drawing
 */
export function refreshCropFrame(drawing) {
    var cropNode = drawing.find('.content').not('.copy').find('.drawing.crop');
    var cropNodeImg = cropNode.find('img');
    var imgPosLeft = parseInt(cropNodeImg.css('left'), 10);
    var imgPosTop = parseInt(cropNodeImg.css('top'), 10);
    var imgWidth = parseInt(cropNodeImg.css('width'), 10);
    var imgHeight = parseInt(cropNodeImg.css('height'), 10);
    cropNode.css({ marginLeft: imgPosLeft, marginTop: imgPosTop, width: imgWidth, height: imgHeight });
    cropNodeImg.css({ left: 0, top: 0 });
}

/**
 * Returns type of crop handler for the given drawing node.
 *
 * @param {jQuery|Element} node
 * @returns {String|Null}
 */
export function getCropHandleType(node) {
    return $(node).is(ACTIVE_CROPPING_SELECTOR + ' .crop .resizers>[data-pos]') ? $(node).attr('data-pos') : null;
}

/**
 * Returns a set of callbacks to handle tracking notification for moving the
 * image crop frame.
 */
export function createCropMoveCallbacks(docApp, drawing) {

    const { docModel, docView } = docApp;

    // the current position change (in px)
    var shiftX = 0, shiftY = 0;
    // the current scroll position
    var scrollX = 0, scrollY = 0;
    // the start scroll position
    var startScrollX = 0, startScrollY = 0;
    var startCropX = 0, startCropY = 0;
    var zoomFactor;
    var scrollNode, cropFrameClone, cropFrameCloneImg, originalCropFrame, origCropFrameImg;
    var flipH, flipV, rotation;

    function prepareCropMove(event) {
        const isCropMove = isActiveCropping(drawing) && ($(event.target).closest('.crop').length > 0);
        if (isCropMove) { event.stopPropagation(); }
        return isCropMove;
    }

    function startCropMove(record) {
        zoomFactor = docView.getZoomFactor();
        scrollNode = docView.getContentRootNode();
        cropFrameClone = $(record.target).closest('.drawing.crop');
        cropFrameCloneImg = cropFrameClone.find('img').first();
        originalCropFrame = drawing.find('.cropping-frame');
        origCropFrameImg = originalCropFrame.find('img').first();
        startCropX = parseInt(cropFrameClone.css('margin-left'), 10);
        startCropY = parseInt(cropFrameClone.css('margin-top'), 10);
        flipH = isFlippedHorz(drawing);
        flipV = isFlippedVert(drawing);
        rotation = getDrawingRotationAngle(docModel, drawing);
        cropFrameClone.addClass('active-crop-event');
        drawing.find('canvas').addClass(HIDE_CANVAS_ON_CROP_CLASS);
    }

    function updateCropMove(record) {

        shiftX = (record.offset.x + scrollX) / zoomFactor;
        shiftY = (record.offset.y + scrollY) / zoomFactor;

        // only move the moveBox, if the mouse was really moved
        if ((shiftX !== 0) || (shiftY !== 0)) {
            var normalizedShift = getNormalizedMoveCoordinates(shiftX, shiftY, rotation, flipH, flipV);
            shiftX = Math.round(normalizedShift.x + startCropX, 1);
            shiftY = Math.round(normalizedShift.y + startCropY, 1);

            cropFrameClone.css({ marginLeft: shiftX, marginTop: shiftY });
            origCropFrameImg.css({ left: shiftX, top: shiftY });
        }
    }

    function updateCropScrollMove(record) {
        // update scrollPosition with suggestion from event
        if (record.scroll.x) { scrollNode.scrollLeft(scrollNode.scrollLeft() + record.scroll.x); }
        if (record.scroll.y) { scrollNode.scrollTop(scrollNode.scrollTop() + record.scroll.y); }

        scrollX = scrollNode.scrollLeft() - startScrollX;
        scrollY = scrollNode.scrollTop() - startScrollY;
    }

    function stopCropMove() {
        if ((shiftX !== 0) || (shiftY !== 0)) {
            var generator = docModel.createOperationGenerator();
            var start = docModel.getSelection().getStartPosition();
            var target = docModel.getActiveTarget();

            var frameSize = { width: cropFrameClone.width(), height: cropFrameClone.height(), drawingW: drawing.width(), drawingH: drawing.height() };
            var calcCropValue = calcCropSize(frameSize, shiftX, shiftY);
            var operationProperties = {
                start: _.copy(start),
                attrs: { image: {
                    cropLeft: -calcCropValue.left,
                    cropRight: calcCropValue.right,
                    cropTop: -calcCropValue.top,
                    cropBottom: calcCropValue.bottom
                } }
            };

            docModel.extendPropertiesWithTarget(operationProperties, target);
            generator.generateOperation(SET_ATTRIBUTES, operationProperties);

            // apply the operations (undo group is created automatically)
            docModel.applyOperations(generator);
            var finalX = origCropFrameImg.css('left');
            var finalY = origCropFrameImg.css('top');
            cropFrameClone.css({ marginLeft: finalX, marginTop: finalY });
            cropFrameCloneImg.css({ left: 0, top: 0 });
        }
    }

    function cancelCropMove() {
        cropFrameClone.css({ marginLeft: startCropX, marginTop: startCropY });
        origCropFrameImg.css({ left: startCropX, top: startCropY });
    }

    function clearCrop() {
        cropFrameClone.removeClass('active-crop-event');
        drawing.find('canvas').removeClass(HIDE_CANVAS_ON_CROP_CLASS);
    }

    return {
        prepare: prepareCropMove,
        start: startCropMove,
        move: updateCropMove,
        scroll: updateCropScrollMove,
        end: stopCropMove,
        cancel: cancelCropMove,
        finally: clearCrop
    };
}

/**
 * Returns a set of callbacks to handle tracking notification for resizing the
 * image crop frame.
 */
export function createCropResizeCallbacks(docApp, drawing) {

    const { docModel, docView } = docApp;

    // the size of the resized drawing (in px)
    var finalWidth = 0, finalHeight = 0;
    // the current scroll position
    var scrollX = 0, scrollY = 0;
    // the start scroll position
    var startScrollX = 0, startScrollY = 0;
    // correction factor for resizing to the left/top
    var scaleX = 0, scaleY = 0;
    var scrollNode, cropFrameClone, origCropFrame;
    var cropFrameImg, origCropFrameImg;
    var cropFrame2Left, cropFrame2Top;
    var zoomFactor, rotationAngle;
    var useX, useY, useLeft, useTop;
    var startTop, startLeft, oldWidth, oldHeight;
    var topPos, leftPos, bottomPos, rightPos;
    var isFlippedHorzLocal, isFlippedVertLocal;

    function prepareCropResize(event) {
        const isCropResize = !!getCropHandleType(event.target);
        if (isCropResize) { event.stopPropagation(); }
        return isCropResize;
    }

    function startCropResize(record) {
        var pos = $(record.target).attr('data-pos');

        // helper function to set position properties at move box(es)
        function setPositionAtMoveBox() {
            scaleX = useLeft ? -1 : 1;
            if (isFlippedHorz(drawing)) { scaleX *= -1; }

            scaleY = useTop ? -1 : 1;
            if (isFlippedVert(drawing)) { scaleY *= -1; }
        }

        cropFrameClone = $(record.target).closest('.drawing.crop').addClass('active-crop-event');
        origCropFrame = drawing.find('.cropping-frame');
        cropFrameImg = cropFrameClone.find('img');
        origCropFrameImg = origCropFrame.find('img');

        zoomFactor = docView.getZoomFactor();
        scrollNode = docView.getContentRootNode();
        rotationAngle = getDrawingRotationAngle(docModel, drawing);
        isFlippedHorzLocal = isFlippedHorz(drawing);
        isFlippedVertLocal = isFlippedVert(drawing);
        oldWidth = cropFrameClone.width();
        oldHeight = cropFrameClone.height();
        startLeft = parseInt(cropFrameClone.css('margin-left'), 10);
        startTop = parseInt(cropFrameClone.css('margin-top'), 10);

        // collecting information about the handle node
        useX = /[lr]/.test(pos);
        useY = /[tb]/.test(pos);
        useLeft = /[l]/.test(pos);
        useTop = /[t]/.test(pos);

        setPositionAtMoveBox(cropFrameClone);
        drawing.find('canvas').addClass(HIDE_CANVAS_ON_CROP_CLASS);
    }

    function updateCropResize(record) {
        // the horizontal shift
        var deltaX = record.offset.x / zoomFactor + scrollX;
        // the vertical shift
        var deltaY = record.offset.y / zoomFactor + scrollY;
        // the scaling factor for the width
        var scaleWidth = 1;
        // the scaling factor for the height
        var scaleHeight = 1;
        // normalize and scale deltas of the dimensions
        var normalizedDeltas = getNormalizedResizeDeltas(deltaX, deltaY, useX, useY, scaleX, scaleY, rotationAngle);
        // changed width and height
        var sumWidth = oldWidth + normalizedDeltas.x;
        var sumHeight = oldHeight + normalizedDeltas.y;
        var signWidth = sumWidth > 0 ? 1 : -1;
        var signHeight = sumHeight > 0 ? 1 : -1;
        var flipH = sumWidth < 0;
        var flipV = sumHeight < 0;

        // use the same scaling factor for vertical and horizontal resizing, if both are enabled
        // -> the larger number wins
        if (useX && useY) { // aspect ratio is always locked
            scaleWidth = Math.abs(sumWidth / oldWidth); // scale shouldn't be negative
            scaleHeight = Math.abs(sumHeight / oldHeight);

            if (scaleWidth > scaleHeight) {
                sumHeight = scaleWidth * oldHeight * signHeight;  // return sign of current width/height after scale
            } else {
                sumWidth = scaleHeight * oldWidth * signWidth;
            }
        }

        topPos = useTop ? 'auto' : Math.min(0, sumHeight);
        bottomPos = useTop ? Math.min(0, sumHeight) : 'auto';
        leftPos = useLeft ? 'auto' : Math.min(0, sumWidth);
        rightPos = useLeft ? Math.min(0, sumWidth) : 'auto';

        // update drawing size
        finalWidth = Math.round(Math.max(1, Math.abs(sumWidth)), 1);
        finalHeight = Math.round(Math.max(1, Math.abs(sumHeight)), 1);

        // reset drawing rotation and flip to get correct offset values
        var transformStr;
        if (rotationAngle || isFlippedHorzLocal || isFlippedVertLocal) {
            transformStr = drawing.css('transform');
            drawing.css('transform', '');
        }
        var origOffset = cropFrameClone.offset();
        var transfOffset = cropFrameImg.offset();
        if (rotationAngle || isFlippedHorzLocal || isFlippedVertLocal) {
            drawing.css('transform', transformStr);
        }

        cropFrame2Left = Math.round(startLeft + (transfOffset.left - origOffset.left) / zoomFactor, 1);
        cropFrame2Top = Math.round(startTop + (transfOffset.top - origOffset.top) / zoomFactor, 1);

        var cssProps = { width: finalWidth, height: finalHeight };
        if (useLeft) {
            if (flipH) {
                cssProps.right = -(startLeft + cropFrameClone.width() - drawing.width()) - finalWidth;
                cssProps.left = '';
            } else {
                cssProps.right = -(startLeft + cropFrameClone.width() - drawing.width());
                cssProps.left = '';
            }
        } else {
            if (flipH) {
                cssProps.right = drawing.width() - startLeft;
                cssProps.left = '';
            } else {
                cssProps.left = cropFrame2Left;
                cssProps.right = '';
            }
        }
        if (useTop) {
            if (flipV) {
                cssProps.top = startTop + cropFrameClone.height();
                cssProps.bottom = '';
            } else {
                cssProps.bottom = -(startTop + cropFrameClone.height() - drawing.height());
                cssProps.top = '';
            }
        } else {
            if (flipV) {
                cssProps.bottom = drawing.height() - startTop;
                cssProps.top = '';
            } else {
                cssProps.top = cropFrame2Top;
                cssProps.bottom = '';
            }
        }

        origCropFrameImg.css(cssProps);
        cropFrameImg.css({ width: finalWidth, height: finalHeight, left: leftPos, top: topPos, bottom: bottomPos, right: rightPos });
    }

    function updateCropScrollResize(record) {
        // update scrollPosition with suggestion from event
        scrollNode
            .scrollLeft(scrollNode.scrollLeft() + record.scroll.x)
            .scrollTop(scrollNode.scrollTop() + record.scroll.y);

        scrollX = scrollNode.scrollLeft() - startScrollX;
        scrollY = scrollNode.scrollTop() - startScrollY;
    }

    function stopCropResize() {
        if (_.isNumber(finalWidth) && _.isNumber(finalHeight) && ((finalWidth !== 0) || (finalHeight !== 0))) {
            var generator = docModel.createOperationGenerator();
            var start = docModel.getSelection().getStartPosition();
            var target = docModel.getActiveTarget();

            var frameSize = { width: cropFrameImg.width(), height: cropFrameImg.height(), drawingW: drawing.width(), drawingH: drawing.height() };
            var calcCropValue = calcCropSize(frameSize, cropFrame2Left, cropFrame2Top);
            var operationProperties = {
                start: _.copy(start),
                attrs: { image: {
                    cropLeft: -calcCropValue.left,
                    cropRight: calcCropValue.right,
                    cropTop: -calcCropValue.top,
                    cropBottom: calcCropValue.bottom
                } }
            };

            docModel.extendPropertiesWithTarget(operationProperties, target);
            generator.generateOperation(SET_ATTRIBUTES, operationProperties);

            // apply the operations (undo group is created automatically)
            docModel.applyOperations(generator);
            var finalX = origCropFrameImg.css('left');
            var finalY = origCropFrameImg.css('top');
            cropFrameClone.css({ marginLeft: finalX, marginTop: finalY, width: cropFrameImg.width(), height: cropFrameImg.height() });
            cropFrameImg.css({ left: 0, top: 0 });
        }
    }

    function cancelCropResize() {
        cropFrameClone.css({ marginLeft: startLeft, marginTop: startTop, width: oldWidth, height: oldHeight });
        cropFrameImg.css({ left: 0, top: 0, width: oldWidth, height: oldHeight });
        origCropFrameImg.css({ left: startLeft, top: startTop, width: oldWidth, height: oldHeight });
    }

    function clearCrop() {
        cropFrameClone.removeClass('active-crop-event');
        drawing.find('canvas').removeClass(HIDE_CANVAS_ON_CROP_CLASS);
    }

    return {
        prepare: prepareCropResize,
        start: startCropResize,
        move: updateCropResize,
        scroll: updateCropScrollResize,
        end: stopCropResize,
        cancel: cancelCropResize,
        finally: clearCrop
    };
}

/**
 * Calculate crop values for the operation during drawing move event.
 *
 * @param {jQuery} drawing
 * @param {Object} oldAttrs
 * @param {Object} newAttrs
 *
 * @returns {Object}
 */
export function getDrawingMoveCropOp(drawing, oldAttrs, newAttrs) {
    var cropFrame = drawing.find('.content').not('.copy').find('.drawing.crop');
    var cropFrameImg = cropFrame.find('img');
    var cropFrameLeft = parseInt(cropFrame.css('margin-left'), 10);
    var cropFrameTop = parseInt(cropFrame.css('margin-top'), 10);

    var topOffsetDiff = convertHmmToLength(oldAttrs.top - (!_.isUndefined(newAttrs.top) ? newAttrs.top : oldAttrs.top), 'px');
    var leftOffsetDiff = convertHmmToLength(oldAttrs.left - (!_.isUndefined(newAttrs.left) ? newAttrs.left : oldAttrs.left), 'px');
    if (!_.isUndefined(newAttrs.anchorVertOffset)) {
        var oldAnchorVertOff = oldAttrs.anchorVertOffset || 0;
        topOffsetDiff = convertHmmToLength(oldAnchorVertOff - newAttrs.anchorVertOffset, 'px');
    }
    if (!_.isUndefined(newAttrs.anchorHorOffset)) {
        var oldAnchorHorOff = oldAttrs.anchorHorOffset || 0;
        leftOffsetDiff = convertHmmToLength(oldAnchorHorOff - newAttrs.anchorHorOffset, 'px');
    }
    var drawingW = convertHmmToLength(newAttrs.width || oldAttrs.width, 'px');
    var drawingH = convertHmmToLength(newAttrs.height || oldAttrs.height, 'px');
    var scaleX = oldAttrs.flipH ? -1 : 1;
    var scaleY = oldAttrs.flipV ? -1 : 1;
    var normalizedDeltas = getNormalizedResizeDeltas(leftOffsetDiff, topOffsetDiff, true, true, scaleX, scaleY, oldAttrs.rotation);
    var frameSize = { width: cropFrameImg.width(), height: cropFrameImg.height(), drawingW, drawingH };
    var calcCropValue = calcCropSize(frameSize, cropFrameLeft + normalizedDeltas.x, cropFrameTop + normalizedDeltas.y);

    return {
        cropLeft: -calcCropValue.left,
        cropRight: calcCropValue.right,
        cropTop: -calcCropValue.top,
        cropBottom: calcCropValue.bottom
    };
}

/**
 * Calculate crop values for the operation during drawing resize event.
 *
 * @param {jQuery} drawing
 * @param {Object} oldAttrs
 * @param {Object} newAttrs
 * @param {Boolean} useLeft
 * @param {Boolean} useTop
 *
 * @returns {Object}
 */
export function getDrawingResizeCropOp(drawing, oldAttrs, newAttrs, useLeft, useTop) {
    var cropFrame = drawing.find('.content').not('.copy').find('.drawing.crop');
    var cropFrameImg = cropFrame.find('img');
    var cropFrameLeft = parseInt(cropFrame.css('margin-left'), 10);
    var cropFrameTop = parseInt(cropFrame.css('margin-top'), 10);
    var drawingW = convertHmmToLength(newAttrs.width || oldAttrs.width, 'px');
    var drawingH = convertHmmToLength(newAttrs.height || oldAttrs.height, 'px');
    var widthDiff = useLeft ? convertHmmToLength(newAttrs.width - oldAttrs.width, 'px') : 0;
    var heightDiff = useTop ? convertHmmToLength(newAttrs.height - oldAttrs.height, 'px') : 0;
    var frameSize = { width: cropFrameImg.width(), height: cropFrameImg.height(), drawingW, drawingH };

    var calcCropValue = calcCropSize(frameSize, cropFrameLeft + widthDiff, cropFrameTop + heightDiff);

    return {
        cropLeft: -calcCropValue.left,
        cropRight: calcCropValue.right,
        cropTop: -calcCropValue.top,
        cropBottom: calcCropValue.bottom
    };
}

/**
 * Calculates crop properties for set attribute operation.
 * Based on image crop Dialog settings!
 *
 * @param {Number} drawingWidth
 * @param {Number} drawingHeight
 * @param {Number} frameWidth
 * @param {Number} frameHeight
 * @param {Number} offsetX
 * @param {Number} offsetY
 *
 * @returns {Object}
 */
export function calculateCropOperation(drawingWidth, drawingHeight, frameWidth, frameHeight, offsetX, offsetY) {
    var leftPart = (frameWidth - drawingWidth) / 2 - offsetX;
    var rightPart = (frameWidth - drawingWidth) / 2 + offsetX;
    var topPart = (frameHeight - drawingHeight) / 2 - offsetY;
    var bottomPart = (frameHeight - drawingHeight) / 2 + offsetY;
    // convert to procentage
    var cropLeft = (100 * leftPart) / frameWidth;
    var cropRight = (100 * rightPart) / frameWidth;
    var cropTop = (100 * topPart) / frameHeight;
    var cropBottom = (100 * bottomPart) / frameHeight;

    return {
        cropLeft: math.roundp(cropLeft, 0.01),
        cropRight: math.roundp(cropRight, 0.01),
        cropTop: math.roundp(cropTop, 0.01),
        cropBottom: math.roundp(cropBottom, 0.01)
    };
}

/**
 * Create crop image properties used to crop image into bounding frame,
 * as fit or fill, depending on passed type.
 *
 * @param {DocumentModel} docModel
 * @param {Object} explAttrs
 * @param {String} type
 *
 * @returns {jQuery.Promise}
 */
export function cropFillFitSpace(docModel, explAttrs, type) {
    var imageAttrs = explAttrs.image;
    var opProps = {};
    var ratio;
    var frameWidth = explAttrs.drawing.width;
    var frameHeight = explAttrs.drawing.height;
    var url = imageAttrs.imageUrl || imageAttrs.imageData;

    if (docModel.getSelection().isCropMode()) { docModel.exitCropMode(); }

    var promise = docModel.getImageSize(url).then(function (size) {
        var imgRatio = size.width / size.height;
        var calcWidth, calcHeight;

        function adjustCropHeight() {
            opProps.cropLeft = null;
            opProps.cropRight = null;

            ratio = (calcHeight - frameHeight) / 2;
            ratio = math.roundp(100 * ratio / calcHeight, 0.01);
            opProps.cropTop = ratio;
            opProps.cropBottom = ratio;
        }

        function adjustCropWidth() {
            opProps.cropTop = null;
            opProps.cropBottom = null;

            ratio = (calcWidth - frameWidth) / 2;
            ratio = math.roundp(100 * ratio / calcWidth, 0.01);
            opProps.cropLeft = ratio;
            opProps.cropRight = ratio;
        }

        if (type === 'fill') {
            if (imgRatio < 1) { // width < height
                // width = frameWidth
                calcHeight = frameWidth / imgRatio;

                if (calcHeight < frameHeight) {
                    calcWidth = frameHeight * imgRatio;
                    adjustCropWidth();
                } else {
                    adjustCropHeight();
                }
            } else {
                // height = frameHeight
                calcWidth = frameHeight * imgRatio;

                if (calcWidth < frameWidth) {
                    calcHeight = frameWidth / imgRatio;
                    adjustCropHeight();
                } else {
                    adjustCropWidth();
                }
            }
        } else {
            if (imgRatio > 1) { // width > height
                // width = frameWidth
                calcHeight = frameWidth / imgRatio;

                if (calcHeight > frameHeight) {
                    calcWidth = frameHeight * imgRatio;
                    adjustCropWidth();
                } else {
                    adjustCropHeight();
                }
            } else {
                // height = frameHeight
                calcWidth = frameHeight * imgRatio;

                if (calcWidth > frameWidth) {
                    calcHeight = frameWidth / imgRatio;
                    adjustCropHeight();
                } else {
                    adjustCropWidth();
                }
            }
        }
        return opProps;
    })
    .fail(function () {
        docModel.getApp().rejectEditAttempt('image');
    });

    return promise;
}
