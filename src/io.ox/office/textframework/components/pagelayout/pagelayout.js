/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { ary } from '@/io.ox/office/tk/algorithms';
import {
    SMALL_DEVICE, TOUCH_DEVICE, CLOSE_LABEL,
    convertHmmToLength, escapeHTML, containsNode, createDiv, createSpan,
    createIcon, parseCssLength, getFocus
} from '@/io.ox/office/tk/dom';
import { Logger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { NODE_SELECTOR, clearSelection } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { BREAK, getBooleanOption, getDomNode, getObjectOption, insertHiddenNodes,
    mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';
import { CHANGE_CONFIG, DELETE_HEADER_FOOTER, INSERT_HEADER_FOOTER,
    PARA_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getFirstPositionInParagraph, getLastPositionInParagraph, getOxoPosition,
    getParagraphElement } from '@/io.ox/office/textframework/utils/position';
import { forceTableRendering } from '@/io.ox/office/textframework/components/table/table';

// globals ====================================================================

const pageLogger = new Logger("text:log-pagelayout", { tag: "PAGE", tagColor: 0x00FFFF });

// class PageLayout =======================================================

/**
 * An instance of this class represents a page layout handler in the edited document.
 */
export default class PageLayout extends ModelObject {

    /**
     * @param {TextModel} docModel
     *  The text model instance.
     *
     * @param {HTMLElement|jQuery} rootNode
     *  The root node of the document. If this object is a jQuery collection,
     *  uses the first node it contains.
     */
    constructor(docModel, rootNode) {

        // base constructor
        super(docModel);

        var // self reference
            self = this,
            // the application instance
            docApp = this.docApp,
            // the selection instance
            selection = null,
            // the field manager instance
            fieldManager = null,
            // node reference containing content nodes
            $pageContentNode = DOM.getPageContentNode(rootNode),
            // style values for page metrics
            pageAttributes,
            pageHeight,
            pageMaxHeight,
            pagePaddingLeft,
            pagePaddingRight,
            pagePaddingTop,
            pagePaddingBottom,
            pageWidth,
            marginalMaxHeight,
            footerMargin = 0,
            headerMargin = 0,
            firstPage = false,
            evenOddPages = false,
            // overlay layer element for field tooltip
            $tooltipOverlay = $(),
            // node reference for holding template headers and footers ready for cloning
            $headerFooterPlaceHolder = $('<div>').addClass('header-footer-placeholder').attr('contenteditable', _.browser.WebKit ? 'false' : '').css('display', SMALL_DEVICE ? 'none' : ''),
            // collection of headers&footers instances
            headerFooterCollection = [],
            // if header/footer is present in document
            marginalActivated = false,
            // page break nodes collection
            pageBreaksCollection = $(),
            // wrapper object of first header in document
            $firstHeaderWrapper = $(),
            // wrapper object of last footer in document
            $lastFooterWrapper = $(),
            // stored height values of different header&footer types
            heightOfDefHeader = 0,
            heightOfDefFooter = 0,
            heightOfFirstHeader = 0,
            heightOfFirstFooter = 0,
            heightOfEvenHeader = 0,
            heightOfEvenFooter = 0,
            // odf min-height values for h&f
            odfFirstHeader = { height: 0, bottom: 0 },
            odfEvenHeader = { height: 0, bottom: 0 },
            odfDefHeader = { height: 0, bottom: 0 },
            odfFirstFooter = { height: 0, top: 0 },
            odfEvenFooter = { height: 0, top: 0 },
            odfDefFooter = { height: 0, top: 0 },
            // pageHeights with different header&footer heights
            firstPageHeight = 0,
            evenPageHeight = 0,
            defPageHeight = 0,
            // store the value of current type of page: first, even, default
            currentPageType = 'default',
            // global variable containing info for fields update
            pageCount = 1,
            // variable used to generate target id, unique for document namespace
            generatedTargetNumber = 100,
            // store reference to header/footer node during creation, to jump into this node after createHeaderFooter operation
            $storedHFnode = null,
            // collection of Id numbers for header/footer targets used in document - NOT the whole ID string!
            targetIdNumbersCollection = [],
            // context menu for header
            contextHeaderMenu,
            // context menu for footer
            contextFooterMenu,
            // Collection of translated labels for marginal context menu
            translations = {
                header_default: gt('Header'),
                footer_default: gt('Footer'),
                header_odd: gt('Odd header'),
                footer_odd: gt('Odd footer'),
                header_first: gt('First header'),
                footer_first: gt('First footer'),
                header_even: gt('Even header'),
                footer_even: gt('Even footer')
            },
            // holds information if remote selection needs to be repaint after page attributes change
            repaintRemoteSelection = false,
            // save deleted nodes when toggling between first, even-odd types headers. In ODT only!
            savedNodesCollection = {},
            // promise for generated delayed page break function
            pbPromise = null,
            // constant for delaying execution of page breaks
            PB_DELAY_TIME = 1000,
            // tooltip for user fields in marginal nodes
            $marginalFieldTooltip = null,
            //
            cleanupElements = [];

        rootNode.append($headerFooterPlaceHolder);

        // private methods ----------------------------------------------------

        /**
         * Initial looping through current 'page content DOM' and doing addition of heights of elements to calculate page breaks positions.
         * This function is called only on document loading, and that's why it loops trough all DOM nodes
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.triggerEvent=false]
         *      If set to true, an event will be triggered after the page breaks are inserted completely.
         */
        function initialPageBreaks(options) {

            var // whether an event shall be triggered after all page breaks are included
                triggerEvent = getBooleanOption(options, 'triggerEvent', false);

            if (SMALL_DEVICE || docModel.isDraftMode()) { return; }
            return pageLogger.takeTime('pageLayout.initialPageBreaks(): ', function () {
                var
                    processingNodes,
                    contentHeight = 0,
                    elementHeightWithoutMB = 0,
                    elementHeight = 0,
                    nodeHelper,
                    zoomFactor = 1,
                    floatingDrawings,
                    fDrawingHeight,
                    fDrawingWidth = 0,
                    accHeightFromPrevDrawing = 0,
                    //heightOfRectPartHelper = 0,
                    elementSpan,
                    offsetPosition = [],
                    newlySplittedSpan,
                    markup = '',
                    pageBreak,
                    elMarginBottom,
                    elMarginTop,
                    diffMargin = 0,
                    prevElMarginBottom = 0,
                    prevPageBreaks,
                    lastPageBreak,
                    $lastPageBreakParent,
                    $drawing,
                    drawingHeightAndOffset,
                    $element,
                    iterationPromise = null,
                    containsMsBreak,
                    prevContentHeight;

                if (!pageAttributes) {
                    initializePageAttributes();
                }

                if (!$firstHeaderWrapper.length) {
                    toggleContentEditableTrueNodes();
                    updateStartHeaderFooterStyle();
                    markMarginalNodesInsideHeadFoot($headerFooterPlaceHolder.children());
                    _.each($headerFooterPlaceHolder.find(DOM.COMPLEXFIELDNODE_SELECTOR), function (field) {
                        fieldManager.markFieldForHighlighting(DOM.getComplexFieldId(field), field);
                    });

                    $firstHeaderWrapper = self.generateHeaderElementHtml();
                    rootNode.css({ paddingBottom: 0, paddingTop: 0 });
                    if (docApp.isODF() && !docModel.isLocalStorageImport() && !docModel.isFastLoadImport()) {
                        // odf can have more than one header or footer with same type, see #43889
                        self.headerFooterCollectionUpdate();
                    }
                }

                // check if there are already inserted page breaks for fast display of first pages, and continue after them
                prevPageBreaks = $pageContentNode.find('.page-break');
                if (prevPageBreaks.length > 0) {
                    lastPageBreak = prevPageBreaks.last();
                    $lastPageBreakParent = lastPageBreak.parent();
                    if ($lastPageBreakParent.is('td')) {
                        contentHeight = lastPageBreak.parentsUntil('.pagecontent', '.tb-split-nb').height() - (lastPageBreak.parentsUntil('.pagecontent', '.pb-row').next().position().top - lastPageBreak.parentsUntil('.pagecontent', '.tb-split-nb').position().top);
                        processingNodes = lastPageBreak.parentsUntil('.pagecontent', '.tb-split-nb').nextAll('.p, table');
                    } else if ($lastPageBreakParent.hasClass('p')) {
                        contentHeight = lastPageBreak.next().length ? ($lastPageBreakParent.height() - lastPageBreak.next().position().top) : 0;
                        processingNodes = $lastPageBreakParent.nextAll('.p, table');
                    } else if (lastPageBreak.next().hasClass('manual-page-break')) {
                        contentHeight = lastPageBreak.next().height();
                        processingNodes = lastPageBreak.next().nextAll('.p, table');
                    } else {
                        processingNodes = lastPageBreak.nextAll('.p, table');
                    }
                } else {
                    processingNodes = $pageContentNode.children('.p, table');
                }
                // if first call of initialPageBreaks is aborted or interrupted, update page breaks collection
                if (!pageBreaksCollection || pageBreaksCollection.length === 0) {
                    pageBreaksCollection = prevPageBreaks;
                    pageCount = pageBreaksCollection.length + 1;
                }
                resolvePageTypeAndHeight(processingNodes.first());

                // triggering event, so that current scroll position can be saved
                if (triggerEvent) { docModel.trigger('pageBreak:before'); }

                iterationPromise = self.iterateArraySliced(processingNodes, function (element, iterator) {
                    //console.warn('pageMaxHeight: ', pageMaxHeight);
                    //console.warn('  -> contentHeight: ', contentHeight);

                    $element = $(element);
                    containsMsBreak = $element.find('.ms-hardbreak-page').length > 0;
                    // processing node with floated drawing(s) inside of it
                    fDrawingHeight = 0;
                    floatingDrawings = $element.find('.drawingspacemaker.float');
                    if (floatingDrawings.length) {
                        _.each(floatingDrawings, function (drawing) {
                            $drawing = $(drawing);
                            var drawingOuterWidth = $drawing.outerWidth(true);
                            drawingHeightAndOffset = $drawing.outerHeight(true) + ($drawing.position().top / zoomFactor);
                            if (drawingOuterWidth !== $pageContentNode.width()) {
                                if (drawingHeightAndOffset > fDrawingHeight) {
                                    fDrawingHeight = drawingHeightAndOffset;
                                    fDrawingWidth = drawingOuterWidth;
                                }
                            }
                        });
                    }
                    if (accHeightFromPrevDrawing > fDrawingHeight) {
                        fDrawingHeight = 0;
                    }
                    elementHeightWithoutMB = element.offsetHeight;
                    elMarginBottom = parseInt($element.css('margin-bottom'), 10);
                    elMarginTop = parseInt($element.css('margin-top'), 10);
                    if (elMarginTop > prevElMarginBottom) { // el has margin top that is greater than margin bottom of prev el, include diff in page height
                        diffMargin =  elMarginTop - prevElMarginBottom;
                        elementHeightWithoutMB += diffMargin;
                    }
                    elementHeight = elementHeightWithoutMB + elMarginBottom;
                    prevElMarginBottom = elMarginBottom;
                    if (DOM.isManualPageBreakNode(element)) {
                        if ($element.prev().length > 0 || containsMsBreak) {
                            contentHeight = manualInsertionOfPageBreak(element, contentHeight, elementHeight);
                            if (containsMsBreak) {
                                // rest element height need to be recalculated then
                                prevContentHeight = getLastPageBreakInNode(element).next();
                                prevContentHeight = (prevContentHeight.length && prevContentHeight.position().top) || 0;
                                elementHeight = element.offsetHeight + elMarginBottom - prevContentHeight;

                                // check if the paragraph contains a MS break and additionally the attribute "pageBreakBefore"
                                // -> another page break is required -> this is a workaround to show empty pages correctly (DOCS-3377)
                                if (hasPageBreakBeforeAttribute($element)) {
                                    $element.before(generatePagebreakElementHtml(contentHeight, _.browser.WebKit));
                                }
                            }
                        } else { // first element in document, can't have page break before - can happen on delete all where last is manual pb paragraph
                            $element.removeClass('manual-page-break');
                        }
                    }
                    // if current content height is bellow max page height;
                    if (contentHeight + elementHeight <= pageMaxHeight  && (fDrawingHeight < 1 || contentHeight + fDrawingHeight <= pageMaxHeight) && (accHeightFromPrevDrawing < 1 || (contentHeight + accHeightFromPrevDrawing <= pageMaxHeight))) {
                        if (!DOM.isManualPageBreakNode(element) || !containsMsBreak) {
                            contentHeight += elementHeight;
                        }
                        if (accHeightFromPrevDrawing > elementHeight) {
                            accHeightFromPrevDrawing -= elementHeight;
                        } else {
                            //heightOfRectPartHelper = accHeightFromPrevDrawing;
                            accHeightFromPrevDrawing = 0;
                        }
                    // for last element that can fit on page we need to omit margin bottom
                    } else if (contentHeight + elementHeightWithoutMB <= pageMaxHeight  && (fDrawingHeight < 1 || contentHeight + fDrawingHeight <= pageMaxHeight) && (accHeightFromPrevDrawing < 1 || (contentHeight + accHeightFromPrevDrawing <= pageMaxHeight))) {
                        if (!DOM.isManualPageBreakNode(element) || !containsMsBreak) {
                            contentHeight += elementHeightWithoutMB;
                        }
                        if (accHeightFromPrevDrawing > elementHeightWithoutMB) {
                            accHeightFromPrevDrawing -= elementHeightWithoutMB;
                        } else {
                            //heightOfRectPartHelper = accHeightFromPrevDrawing;
                            accHeightFromPrevDrawing = 0;
                        }
                    } else {
                        prevElMarginBottom = 0;
                        accHeightFromPrevDrawing = 0;

                        if (DOM.isParagraphNode($element) && ($element.text().length > 10 || $element.find('.drawing.inline').length > 0)) { //&& floatingDrawings.length < 1) {
                            // paragraph split preparation
                            var cssNegativeTextIndent = _.browser.Chrome && parseInt($element.css('text-indent'), 10) < 0;
                            var arrBreakAboveSpans = [];
                            var upperPartDiv = 0;
                            var totalUpperParts = 0;
                            var diffPosTopAndPrev = 0;
                            var elementLeftIndent;

                            if (accHeightFromPrevDrawing > 0 && fDrawingWidth > 0) {
                                offsetPosition = getOffsetPositionFromElement(element, elementHeight, contentHeight, false, zoomFactor, accHeightFromPrevDrawing, fDrawingWidth);
                            } else {
                                offsetPosition = getOffsetPositionFromElement(element, elementHeight, contentHeight, false, zoomFactor);
                            }
                            if (offsetPosition && offsetPosition.length > 0) { // if offset position is returned
                                if (offsetPosition[0] === 0) { // if offset is 0, no need for p split, insert pb before element
                                    offsetPosition.shift();
                                    if (!(DOM.isManualPageBreakNode($element.prev()) && DOM.isPageBreakNode($element.prev().children().last()))) {
                                        contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
                                        if (offsetPosition.length > 0) { contentHeight = 0; }
                                    }
                                }
                                if (offsetPosition.length > 0) {
                                    elementSpan = $element.children('span').first();
                                    while (elementSpan.text().length < 1 || !(elementSpan.is('span'))) {
                                        elementSpan = elementSpan.next();
                                    }
                                    if (elementSpan && elementSpan.length > 0) { //fix for #32563, if div.p contains only empty spans
                                        _.each(offsetPosition, function (offset) {
                                            var elTextLength = $(elementSpan).text().length;
                                            while (elementSpan.length && elTextLength <= offset) {
                                                offset -= elTextLength;
                                                elementSpan = elementSpan.next();
                                                while (!elementSpan.is('span')) { // we might fetch some inline div nodes, like div.tab or div.linebreak
                                                    elementSpan = elementSpan.next();
                                                }
                                                elTextLength = $(elementSpan).text().length;
                                            }
                                            newlySplittedSpan = DOM.splitTextSpan(elementSpan, offset, { append: true });
                                            arrBreakAboveSpans.push(newlySplittedSpan);
                                            elementSpan = newlySplittedSpan;
                                        });
                                    }
                                    zoomFactor = docApp.getView().getZoomFactor();
                                    pageBreak = $();
                                    upperPartDiv = 0;
                                    totalUpperParts = 0;
                                    diffPosTopAndPrev = 0;
                                    elementLeftIndent = parseInt($element.css('margin-left'), 10) + parseInt($element.css('padding-left'), 10); // for indented elements such as lists, or special paragraphs

                                    if (arrBreakAboveSpans.length) {
                                        $element.addClass('contains-pagebreak');
                                        _.each(arrBreakAboveSpans, function (breakAboveSpan) {
                                            var pageBreakHeight = pageBreak.length ? $(pageBreak).outerHeight(true) : 0; // jQuery 3, avoiding NaN
                                            diffPosTopAndPrev += pageBreakHeight + upperPartDiv;
                                            upperPartDiv = $(breakAboveSpan).position().top / zoomFactor - diffPosTopAndPrev;
                                            totalUpperParts += upperPartDiv;
                                            contentHeight += upperPartDiv;
                                            nodeHelper = $element.prev();
                                            while (DOM.isPageBreakNode(nodeHelper)) {
                                                nodeHelper = nodeHelper.prev();
                                            }
                                            markup = generatePagebreakElementHtml(contentHeight, _.browser.WebKit, elementLeftIndent);
                                            $(breakAboveSpan).addClass('break-above-span').before(markup);
                                            pageBreak = $(breakAboveSpan).prev();
                                            contentHeight = 0;
                                            if (cssNegativeTextIndent) { // #40776
                                                pageBreak.css('display', 'inline-block');
                                            }
                                        });
                                    }
                                    contentHeight = elementHeight - totalUpperParts;
                                }
                            } else {
                                contentHeight = insertPageBreaksBetweenDrawings(element, elementHeight, contentHeight, offsetPosition, zoomFactor);
                                //contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
                            }

                        } else if (element.rows && element.rows.length > 1) { // table split
                            contentHeight = insertPageBreaksInTable(element, elementHeight, contentHeight, { initial: true, diffMargin });
                        } else { // default insertion of page break between elements
                            contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
                        }
                    }
                    if (DOM.isNodeWithPageBreakAfterAttribute(element)) {
                        contentHeight = insertManualPageBreakAfter(element, contentHeight);
                    }

                    if (fDrawingHeight > 0) { // store the diff between floated image height and height of containing p, for next iteration
                        accHeightFromPrevDrawing = fDrawingHeight - elementHeight;
                    }
                    //at the last page, we add extra padding to fill in to the size of max page height
                    if (processingNodes.length === iterator + 1) {
                        $pageContentNode.css({
                            'padding-bottom': pageMaxHeight - contentHeight
                        });
                    }
                });

                iterationPromise.done(function () {
                    if ($lastFooterWrapper.length === 0) {
                        $lastFooterWrapper = self.generateFooterElementHtml();
                    }

                    // refreshing the collection of page breaks
                    self.refreshPageBreakCollection();

                    populateHeaderFooterPaginationClasses(pageBreaksCollection);
                    if (self.isHeaderFooterInDocument()) {
                        //replaceAllTypesOfHeaderFooters();
                        repairImagesAndTfBordersInMarginal();
                    }

                    if (!fieldManager.isComplexFieldEmpty()) {
                        fieldManager.updatePageFieldsInMarginals();
                    }

                    _.each(cleanupElements, function (element) {
                        $(element).remove();
                    });
                    cleanupElements = [];

                    if (triggerEvent) {
                        docModel.trigger('pageBreak:after');
                    }
                    if (docModel.isLocalStorageImport()) {
                        // if page breaks are called from local storage, trigger new event, that will be listened for fields update
                        // new event is needed to avoid calling of update of drawings and comment layer that are called after 'pageBreak:after'
                        docModel.trigger('initialPageBreaks:done');
                    }
                });

                return iterationPromise;
            });
        }

        /**
         * Helper function to fetch last page break node inside splited node.
         *
         * @param {jQuery|Node} elementNode
         *  Element which we are querying for page break nodes.
         * @returns {jQuery} page break node, if found or empty jQuery object.
         */
        function getLastPageBreakInNode(elementNode) {
            return $(elementNode).find('.page-break').last();
        }

        /**
         * Depending on current page number and presence of different types of header/footer,
         * resolve type and height of current page.
         *
         * @param {jQuery} $firstNode
         *  Node from which page breaks insertion starts.
         */
        function resolvePageTypeAndHeight($firstNode) {
            var currentPageNumber = self.getPageNumber($firstNode);

            if (currentPageNumber === 1 && firstPage && firstPageHeight > 0) {
                pageMaxHeight = firstPageHeight;
                currentPageType = 'first';
            } else if (currentPageNumber % 2 === 0 && evenOddPages && evenPageHeight > 0) {
                pageMaxHeight = evenPageHeight;
                currentPageType = 'even';
            } else if (defPageHeight > 0) {
                pageMaxHeight = defPageHeight;
                currentPageType = 'default';
            } else {
                pageLogger.warn('pageLayout.initialPageBreaks(): default page height is wrong: ', defPageHeight);
            }
        }

        /**
         * After page breaks insertion is done, this method will insert correct types of headers&footers
         * inside page breaks. There are four types of headers&footers, listed by priority from highest to lowest:
         *  first   - for the first page only, if defined
         *  even    - for even pages, if defined
         *  odd     - see default
         *  default - fallback for all types - lowest prio. Odd pages uses this type
         * Higher priority header/footer, if is defined in document, will overwrite all bellow him,
         * except that even will not overwrite odd on odd page numbers.
         */
        function replaceAllTypesOfHeaderFooters() {

            var groupOfHeaderFooterToUpdate = pageBreaksCollection.add($firstHeaderWrapper).add($lastFooterWrapper), //rootNode.find('.page-break, .header-wrapper, .footer-wrapper'),
                placeholderHeaderChildren = self.getHeaderFooterChildrenFromPlaceholder('header_default'),
                placeholderHeaderTarget = self.getIdFromType('header_default'),
                placeholderFooterChildren = self.getHeaderFooterChildrenFromPlaceholder('footer_default'),
                placeholderFooterTarget = self.getIdFromType('footer_default'),
                typesOfHeader = [],
                typesOfHeaderInternal = [],
                typesOfFooter = [],
                typesOfFooterInternal = [],
                selector = '',
                i = 0,
                typesLength = 0,
                $tempMarginalNode = null;

            if (firstPage) {
                typesOfHeader.push('header_first');
                typesOfHeaderInternal.push('.header.first');
                typesOfFooter.push('footer_first');
                typesOfFooterInternal.push('.footer.first');
                selector += '.first';
            }
            if (evenOddPages) {
                typesOfHeader.push('header_even');
                typesOfHeaderInternal.push('.header.even');
                typesOfFooter.push('footer_even');
                typesOfFooterInternal.push('.footer.even');
                if (selector.length > 0) {
                    selector += ', .even';
                } else {
                    selector += '.even';
                }
            }

            // default
            if (placeholderHeaderChildren.length > 0) {
                groupOfHeaderFooterToUpdate.find('.header').not(selector).attr('data-container-id', placeholderHeaderTarget).empty().append(placeholderHeaderChildren);
            } else {
                groupOfHeaderFooterToUpdate.find('.header').not(selector).removeAttr('data-container-id').empty();
            }
            if (placeholderFooterChildren.length > 0) {
                groupOfHeaderFooterToUpdate.find('.footer').not(selector).attr('data-container-id', placeholderFooterTarget).empty().append(placeholderFooterChildren);
            } else {
                groupOfHeaderFooterToUpdate.find('.footer').not(selector).removeAttr('data-container-id').empty();
            }

            // all other types
            for (i = 0, typesLength = typesOfHeader.length; i < typesLength; i += 1) {
                placeholderHeaderChildren = self.getHeaderFooterChildrenFromPlaceholder(typesOfHeader[i]);
                placeholderHeaderTarget = self.getIdFromType(typesOfHeader[i]);
                if (placeholderHeaderChildren.length > 0) {
                    groupOfHeaderFooterToUpdate.find(typesOfHeaderInternal[i]).attr('data-container-id', placeholderHeaderTarget).empty().append(placeholderHeaderChildren);
                } else {
                    $tempMarginalNode = groupOfHeaderFooterToUpdate.find(typesOfHeaderInternal[i]);
                    $tempMarginalNode.removeAttr('data-container-id')
                        .css({ minHeight: pagePaddingTop, maxHeight: marginalMaxHeight, padding: headerMargin + 'px ' + pagePaddingRight + 'px 0 ' + (pagePaddingLeft) + 'px' })
                        .empty().prepend(createCoverOverlay())
                        .append($('<div>').addClass(DOM.MARGINALCONTENT_NODE_CLASSNAME).append($('<div>').addClass('p').data('implicit', true)));
                    if ($tempMarginalNode.find('.p').length) {
                        docModel.validateParagraphNode($tempMarginalNode.find('.p'));
                    }
                }
            }

            for (i = 0, typesLength = typesOfFooter.length; i < typesLength; i += 1) {
                placeholderFooterChildren = self.getHeaderFooterChildrenFromPlaceholder(typesOfFooter[i]);
                placeholderFooterTarget = self.getIdFromType(typesOfFooter[i]);
                if (placeholderFooterChildren.length > 0) {
                    groupOfHeaderFooterToUpdate.find(typesOfFooterInternal[i]).attr('data-container-id', placeholderFooterTarget).empty().append(placeholderFooterChildren);
                } else {
                    $tempMarginalNode = groupOfHeaderFooterToUpdate.find(typesOfFooterInternal[i]);
                    $tempMarginalNode.removeAttr('data-container-id')
                        .css({ minHeight: pagePaddingBottom, maxHeight: marginalMaxHeight, padding: '5px ' + pagePaddingRight + 'px ' + footerMargin + 'px ' + (pagePaddingLeft) + 'px' })
                        .empty().prepend(createCoverOverlay())
                        .append($('<div>').addClass(DOM.MARGINALCONTENT_NODE_CLASSNAME).append($('<div>').addClass('p').data('implicit', true)));
                    if ($tempMarginalNode.find('.p').length) {
                        docModel.validateParagraphNode($tempMarginalNode.find('.p'));
                    }
                }
            }

            // if one of nodes is active, no need to have cover overlay node
            groupOfHeaderFooterToUpdate.find('.active-selection').children('.cover-overlay').remove();

            // if there are images inside head/foot, might happen that they are replaced before image resource is loaded in browser
            repairImagesAndTfBordersInMarginal(groupOfHeaderFooterToUpdate.find('.header, .footer'));

            if (!fieldManager.isComplexFieldEmpty()) { fieldManager.updatePageFieldsInMarginals(); }

            if (selection.hasRange()) { selection.restoreBrowserSelection(); } // restoring the browser selection (DOCS-3116)
        }

        /**
         * Repairs images inside marginals, for ones that are replaced before image resource is loaded in browser,
         * and therefore image placeholder is shown. It also repairs canvas borders of text frames in marginals.
         *
         * @param {jQuery} [allHeadersFooters]
         *  Collection of all header and footer nodes in document.
         *
         */
        function repairImagesAndTfBordersInMarginal(allHeadersFooters) {
            var placeholdersOfImages = null,
                textFramesWithBordersInMarginal = null;

            // it is important to assign values only after all replacement is done
            allHeadersFooters = allHeadersFooters || pageBreaksCollection.add($firstHeaderWrapper).add($lastFooterWrapper).find('.header, .footer');
            // empty each placeholder inside image in header/footer, to trigger update formatting (and, replacing with real image resource)
            placeholdersOfImages = allHeadersFooters.find('.drawing').children('.placeholder');
            _.each(placeholdersOfImages, function (placeholder) {
                $(placeholder).empty();
                docModel.drawingStyles.updateElementFormatting($(placeholder).parent('.drawing'));
            });
            // remove references for GC
            placeholdersOfImages = null;

            // find text frames with canvas border inside header/footer and repaint them (after cloning they have to be manually repainted) #36576
            textFramesWithBordersInMarginal = allHeadersFooters.find(NODE_SELECTOR);
            if (textFramesWithBordersInMarginal.length) {
                docModel.trigger('drawingHeight:update', textFramesWithBordersInMarginal);
                textFramesWithBordersInMarginal = null;
            }

            // OT: Immediately format all drawings of active header/footer (especially after undo, 66982 (only for the active editor in OT mode))
            if (isOTMode() && docModel.isHeaderFooterEditState()) {
                _.each(docModel.getCurrentRootNode().find('div.drawing'), function (drawing) {
                    docModel.drawingStyles.updateElementFormatting($(drawing));
                });
            }
        }

        /**
         * Initializing page attributes needed for calculation of page breaks.
         * In some scenarios, like i.e. loading from local storage,
         * initialPageBreaks method is not called, and variables are not initialized for later usage.
         */
        function initializePageAttributes() {
            pageAttributes = docModel.pageStyles.getElementAttributes(rootNode);
            pagePaddingLeft = convertHmmToLength(pageAttributes.page.marginLeft, 'px', 0.001);
            pagePaddingRight = convertHmmToLength(pageAttributes.page.marginRight, 'px', 0.001);
            pagePaddingTop = convertHmmToLength(pageAttributes.page.marginTop, 'px', 0.001);
            pagePaddingBottom = convertHmmToLength(pageAttributes.page.marginBottom, 'px', 0.001);
            pageHeight = convertHmmToLength(pageAttributes.page.height, 'px', 0.001);
            pageMaxHeight = pageHeight - pagePaddingTop - pagePaddingBottom;
            pageWidth = convertHmmToLength(pageAttributes.page.width, 'px', 0.001);
            headerMargin = docApp.isODF() ? pagePaddingTop : convertHmmToLength(pageAttributes.page.marginHeader, 'px', 0.001);
            footerMargin = docApp.isODF() ? pagePaddingBottom : convertHmmToLength(pageAttributes.page.marginFooter, 'px', 0.001);
            firstPage = pageAttributes.page.firstPage;
            evenOddPages = pageAttributes.page.evenOddPages;
            marginalMaxHeight = 4 / 9 * pageHeight;
        }

        /**
         * In a element (paragraph) where there are drawing objects, but no text spans to split,
         * we insert page breaks directly before desired drawing.
         * For now, only inline type drawings are supported.
         *
         * @param {Node|jQuery} element
         *  Element which is being processed
         *
         * @param {Number} elementHeight
         *  Current height value of all processed nodes
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @param {Array} offsetPosition
         *  possible position(s) where to split text in paragraph
         *
         * @param {Number} zoomFactor
         *  Current zoom factor in document, can be number value, or undefined on doc loading
         *
         * @returns {Number}
         *  Height of content in current page after processing, in px
         */
        function insertPageBreaksBetweenDrawings(element, elementHeight, contentHeight, offsetPosition, zoomFactor) {
            var
                $element = $(element),
                $elDrawings = $element.find('.drawing'),
                numOfPageBreaks = ~~((elementHeight + contentHeight) / pageMaxHeight), //get int value how many times is element bigger than page
                multiplicator,
                upperPartDiv = 0,
                totalUpperParts = 0,
                diffPosTopAndPrev = 0,
                numOfInserts = 0,
                pageBreak = $(),
                elementLeftIndent = parseInt($element.css('margin-left'), 10) + parseInt($element.css('padding-left'), 10); // for indented elements such as lists, or special paragraphs

            if ($elDrawings.length > 1 && offsetPosition.length < numOfPageBreaks) {
                multiplicator = numOfPageBreaks;
                multiplicator -= 1; // decrement multiplier for 0 based values

                $elDrawings.each(function () {
                    if (numOfInserts === numOfPageBreaks) {
                        return BREAK;
                    }
                    var drawing = $(this);
                    if (drawing.hasClass('inline')) {
                        diffPosTopAndPrev += $(pageBreak).outerHeight(true) + upperPartDiv;
                        upperPartDiv = drawing.position().top / (zoomFactor || 1) - diffPosTopAndPrev;

                        if (upperPartDiv + drawing.outerHeight() > (pageMaxHeight - contentHeight) && multiplicator > -1) {
                            totalUpperParts += upperPartDiv;
                            contentHeight += upperPartDiv;
                            drawing.before(generatePagebreakElementHtml(contentHeight, _.browser.WebKit, elementLeftIndent));
                            multiplicator -= 1;
                            numOfInserts += 1;
                            pageBreak = drawing.prev();
                            contentHeight = 0;
                            $element.addClass('contains-pagebreak');
                            drawing.addClass('break-above-drawing');
                        }
                    }
                });
                // second pass, see bug #42455
                if (numOfInserts < numOfPageBreaks) {
                    $elDrawings.each(function () {
                        var drawing = $(this);
                        if (drawing.hasClass('inline')) {
                            upperPartDiv = drawing.position().top / (zoomFactor || 1) - (pageBreak.length ? (pageBreak.position().top + pageBreak.outerHeight(true)) : 0);

                            if (upperPartDiv + drawing.outerHeight() > (pageMaxHeight - contentHeight)) {
                                totalUpperParts += upperPartDiv;
                                contentHeight += upperPartDiv;
                                drawing.before(generatePagebreakElementHtml(contentHeight, _.browser.WebKit, elementLeftIndent));
                                numOfInserts += 1;
                                pageBreak = drawing.prev();
                                contentHeight = 0;
                                $element.addClass('contains-pagebreak');
                                drawing.addClass('break-above-drawing');
                            }
                        }
                    });
                }
                contentHeight = elementHeight - totalUpperParts;
            } else if (!DOM.isManualPageBreakNode(element)) { // place pagebreak before element, but not before manual pb that is alredy processed
                contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
            }

            return contentHeight;
        }

        /**
         *  Internal function for page layout rendering!
         *  Creates invisible helper node to calculate position(s) in text where paragraph should be split.
         *  Also stores position of all rest new line beginnings into node with jQuery data()
         *  Words and spaces are occurring in pairs, sequentially. Space can be one character, or more grouped, then nbsp is used
         *
         * @param {Node|jQuery} element
         *  Element whose offset is calculated
         *
         * @param {Number} elementHeight
         *  Height of element, in px
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @param {Boolean} modifyingElement
         *  If the current processing node is modified or untouched
         *
         * @param {Number} zoomFactor
         *  Current zoom factor
         *
         * @param {Number} [partHeight]
         *  If there is floating drawing from previous paragraphs, use height to create dummy div
         *
         * @param {Number} [partWidth]
         *  If there is floating drawing from previous paragraphs, use width to create dummy div
         *
         * @returns {Array}
         *  Calculated position(s) in text where paragraph should be split
         */
        function getOffsetPositionFromElement(element, elementHeight, contentHeight, modifyingElement, zoomFactor, partHeight, partWidth) {

            var
                offsetPosition = [],
                elementSpan,
                contentHeightCached = contentHeight,
                $newElement,
                $element = $(element),
                elementWidth,
                elementLineHeightStyle = $element.css('line-height'),
                oxoPosInPar,
                numOfPageBreaks = 0,
                cachedDataForLineBreaks = [],
                cachedDataForLineBreaksLength,
                helperOffsetArray = [],
                elSpanPositionTop,
                charLength = 0,
                //elSpans,
                $elDrawings, //paragraph contains image(s)
                //drawingTopPos,
                //drawingBottomPos,
                cachedPosTop,
                markup,
                splitPosForbidenRanges = [],
                allowedValueInRange;

            // in some cases during conversion from mm to px 1 pixel is lost, which leads to not identical text flow in cloned element,
            // and therefore we use getBoundingClientRect instead of jQuery's width which rounds up to int.
            zoomFactor = zoomFactor || 1;
            elementWidth = element && element.getBoundingClientRect().width / zoomFactor;

            function calculateOffsetPosition() {
                var diff = 0,
                    factorIncr = 1,
                    prevPositionTop,
                    prevOxoPos,
                    tempContHeight = contentHeightCached;

                helperOffsetArray = [];

                if (elementHeight > pageMaxHeight) { // more than one insertion of pb
                    numOfPageBreaks = ~~((elementHeight + contentHeightCached) / pageMaxHeight); // how many times we insert page breaks in table
                    if (offsetPosition.length === 0 && cachedDataForLineBreaksLength > 0) {
                        cachedDataForLineBreaks.forEach(function (el, i) {
                            if (el.distanceTop + diff > (pageMaxHeight * factorIncr) - tempContHeight && (prevPositionTop && prevPositionTop <= (pageMaxHeight * factorIncr) - tempContHeight)) {
                                diff += ((pageMaxHeight * factorIncr) - tempContHeight) - prevPositionTop; // difference that will be shifted down (image for ex.)
                                factorIncr += 1;
                                offsetPosition.push(prevOxoPos);
                            } else if (i === 0 && (el.distanceTop + diff > (pageMaxHeight * factorIncr) - tempContHeight)) { // first row cannot fit into free space, it is also shifted down (pb on begining, pos 0)
                                offsetPosition.push(0);
                                tempContHeight = 0;
                            }
                            prevPositionTop = el.distanceTop + diff;
                            prevOxoPos = el.oxoPosInPar;
                        });
                        if (offsetPosition.length < numOfPageBreaks) {
                            // now we know space left is not enough for last line, so we put page break before it
                            offsetPosition.push(cachedDataForLineBreaks[cachedDataForLineBreaksLength - 1].oxoPosInPar);
                        }
                    }

                    // normalize values: ex. [150, 400] to [150, 250]
                    _.each(offsetPosition, function (offPos, iterator) {
                        if (iterator > 0) {
                            offPos -=  offsetPosition[iterator - 1];
                        }
                        helperOffsetArray.push(offPos);
                    });
                    offsetPosition = helperOffsetArray.slice();
                } else {
                    //if its not multi paragraph split
                    if (offsetPosition.length === 0 && cachedDataForLineBreaksLength > 0) {
                        ary.some(cachedDataForLineBreaks, function (el, i) {
                            if (el.distanceTop <= (pageMaxHeight - contentHeightCached)) {
                                offsetPosition.push(el.oxoPosInPar);
                                return true;
                            }
                            if (i === 0 && (el.distanceTop > (pageMaxHeight - contentHeightCached))) { // first row (backwards) cannot fit into free space, it is also shifted down (pb on begining, pos 0)
                                offsetPosition.push(0);
                                return true;
                            }
                        }, { reverse: true });
                    }
                    if (offsetPosition.length === 0) {
                        // now we know space left is not enough for last line, so we put page break before it
                        offsetPosition.push(cachedDataForLineBreaks[cachedDataForLineBreaksLength - 1].oxoPosInPar);
                    }
                }
            }

            function fillWhitespacesMarkup(whitespacesLen) {
                var markup = '';
                for (var j = 0; j < whitespacesLen; j++) { //  translates for ex. '     ' into -> ' &nbsp; &nbsp; ' (5 whitespaces)
                    if (j % 2 !== 0) {
                        markup += ' ';
                    } else {
                        markup += '&nbsp;';
                    }
                }

                return markup;
            }

            function wrapWordsWithSpans(elementSpan) {
                var elText,
                    words,
                    markup,
                    spaces,
                    charAtBegining;

                _.each(elementSpan, function (el, index) {
                    var lastSpaceInSpan = '';

                    elText = $(el).text();
                    words = elText.match(/\S+/g);
                    markup = '';
                    spaces = elText.match(/(\s+)/gi);

                    charAtBegining = (elText[0]) ? elText[0].match(/\S+/g) : null; // check if text begins with characters or whitespace

                    if (words && words.length > 0) {
                        _.each(words, function (word, i) {
                            if (word.length > 0) {
                                var generatedSpace;
                                if (spaces && spaces[i]) {
                                    if (spaces[i].length > 1) {
                                        generatedSpace = '';
                                        generatedSpace += fillWhitespacesMarkup(spaces[i].length);
                                    } else {
                                        if (index === 0 && i === 0 && !charAtBegining) { // fix for first span with whitespace (it will render as 0px span, so we fake content with dot)
                                            generatedSpace = '.';
                                        } else {
                                            generatedSpace = ' ';
                                        }
                                    }
                                } else {
                                    generatedSpace = '';
                                }
                                if (charAtBegining) { // word, space direction
                                    if (generatedSpace.length > 0) {
                                        markup += '<span class="textData" data-charLength="' + (charLength += word.length) + '">' + escapeHTML(word) + '</span><span class="whitespaceData" data-charLength="' +  (charLength += spaces[i].length) + '">' + generatedSpace + '</span>';
                                    } else {
                                        markup += '<span class="textData" data-charLength="' + (charLength += word.length) + '">' + escapeHTML(word) + '</span>';
                                    }
                                } else { // space, word direction
                                    if (generatedSpace.length > 0) {
                                        markup += '<span class="whitespaceData" data-charLength="' + (charLength += spaces[i].length) + '">' + generatedSpace + '</span><span class="textData" data-charLength="' + (charLength += word.length) + '">' + escapeHTML(word) + '</span>';
                                    } else {
                                        markup += '<span class="textData" data-charLength="' + (charLength += word.length) + '">' + escapeHTML(word) + '</span>';
                                    }
                                }
                            }
                        });
                        if (spaces && spaces.length > words.length) {
                            if (_.last(spaces).length > 1) {
                                lastSpaceInSpan += fillWhitespacesMarkup(_.last(spaces).length);
                            } else {
                                lastSpaceInSpan = ' ';
                            }
                            markup += '<span class="whitespaceData" data-charLength="' + (charLength += _.last(spaces).length) + '">' + lastSpaceInSpan + '</span>';
                        }
                    } else if (spaces && spaces.length > 0) {
                        _.each(spaces, function (space) {
                            if (space.length > 1) {
                                lastSpaceInSpan += fillWhitespacesMarkup(space.length);
                            } else {
                                lastSpaceInSpan = ' ';
                            }
                            markup += '<span class="whitespaceData" data-charLength="' + (charLength += space.length) + '">' + lastSpaceInSpan + '</span>';
                        });
                    }
                    el.innerHTML = markup;
                });
            }

            if (_.isEmpty($element.data('lineBreaksData')) || modifyingElement) { // if no cached data, or element is modified, calculate all positions
                $newElement = $element.clone(true);
                if (partHeight > 0 && partWidth > 0) { // if there is drawing leftover from previous paragraphs, it will change p layout
                    markup = '<div style="width:' + partWidth + 'px;height:' + partHeight + 'px;float:left;"></div>';
                    $newElement.prepend(markup);
                }
                $newElement.css({ width: elementWidth, lineHeight: elementLineHeightStyle, fontSize: '16px' });
                insertHiddenNodes($newElement, true);

                // Handling of drawings - 2 groups: inline and floated.
                // inline are treated normally. can split before and after them
                // floating needs to fetch div offset also
                // there has to be range of values inside range of [0, paragraph height] for which splitting text is not valid
                // range itself can be made of ranges, ex. [[0, 25], [166, 333], [400, 550]]
                // Exception: floated drawing at the beginning of doc, in first p, before text
                $elDrawings = $newElement.find('.drawing');

                _.each($elDrawings, function (drawing) {
                    var $drawing = $(drawing),
                        range = [];
                    range[0] = $drawing.position().top;
                    range[1] = range[0] + $drawing.height() + parseCssLength($drawing, 'marginBottom');
                    if ($drawing.hasClass('float')) {
                        var diff = $drawing.prev('.offset').outerHeight(true);
                        range[0] = Math.max(0, range[0] - diff);
                    }

                    splitPosForbidenRanges.push(range);
                });

                elementSpan = $newElement.children('span');
                wrapWordsWithSpans(elementSpan);

                cachedPosTop = 2; // ignoring first line, 2 is rounding error for position of spans in first line
                _.each(elementSpan.find('.textData'), function (elSpan) {
                    var $elSpan = $(elSpan),
                        elSpanTextLength = $elSpan.text().length;

                    if (elSpanTextLength) { //if (getDomNode(elSpan).firstChild)
                        elSpanPositionTop = $elSpan.position().top;
                        if ((elSpanPositionTop > cachedPosTop)) {
                            cachedPosTop = elSpanPositionTop;
                            if (splitPosForbidenRanges.length > 0) {
                                allowedValueInRange = true;
                                _.each(splitPosForbidenRanges, function (range) {
                                    if (elSpanPositionTop > range[0] && elSpanPositionTop < range[1]) {
                                        allowedValueInRange = false;
                                    }
                                });
                                if (allowedValueInRange) {
                                    oxoPosInPar = parseInt($elSpan.attr('data-charLength'), 10) - elSpanTextLength;
                                    cachedDataForLineBreaks.push({ oxoPosInPar, distanceTop: elSpanPositionTop });
                                }
                            } else {
                                oxoPosInPar = parseInt($elSpan.attr('data-charLength'), 10) - elSpanTextLength;
                                cachedDataForLineBreaks.push({ oxoPosInPar, distanceTop: elSpanPositionTop });
                            }
                        }
                    }
                });
                $element.data('lineBreaksData', cachedDataForLineBreaks);
                //clear dom after getting position
                cleanupElements.push($newElement);
                //$newElement.remove();
                //$newElement = null;
            } else {
                cachedDataForLineBreaks = $element.data('lineBreaksData');
            }

            cachedDataForLineBreaksLength = cachedDataForLineBreaks.length;
            if (cachedDataForLineBreaksLength > 0) {
                calculateOffsetPosition();
            }

            return offsetPosition;
        }

        /**
         * Internal function for page layout rendering!
         * Default way for inserting page break node in DOM,
         * in between first level nodes, paragraphs and tables
         *
         * @param {Node|jQuery} element
         *  Element which is being proccessed
         *
         * @param {Number} elementHeight
         *  Current height value of all proccessed nodes
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @returns {Number}
         *  Height of content in current page after proccessing, in px
         */
        function defaultInsertionOfPageBreak(element, elementHeight, contentHeight) {
            var
                $element = $(element),
                nodeHelper = $element.prev();

            while (nodeHelper.hasClass('page-break')) {
                nodeHelper = nodeHelper.prev();
            }
            if (nodeHelper.length) {
                $element.before(generatePagebreakElementHtml(contentHeight, _.browser.WebKit));
            }
            $element.addClass('break-above'); // this is marker for first node bellow pagebreak
            contentHeight = elementHeight; // addition if content is shifted down

            return contentHeight;
        }

        /**
         * Internal function for page layout rendering!
         * Proccesses passed table element node, and appends generated page break in a new row.
         *
         * @param {Node|jQuery} element
         *  Element which is being proccessed
         *
         * @param {Number} elementHeight
         *  Current height value of all proccessed nodes
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.initial=false]
         *      Determines if function is called initialy after document loading.
         *  @param {Number} [options.diffMargin=0]
         *      Difference in margin-top from current element, and margin-bottom from previous,
         *      needs to be stored in accumulator, for more precise calculation.
         *
         * @returns {Number}
         *  Height of content in current page after proccessing
         */
        function insertPageBreaksInTable(element, elementHeight, contentHeight, options) {
            var
                $element = $(element),
                $prevElement = $element.prev(),
                // if there is difference in margin top of current element, and margin bottom of previous, add this diff as start value to accumulator
                accumulator = (options && options.diffMargin) ? options.diffMargin : 0,
                totalAcc = 0,
                arrAccumulator = [],
                // helper var for big tables
                tempContentHeight = contentHeight,
                tableRows = element.rows,
                breakAboveRows = [],
                // helper height value, for tables bigger than one page
                tableHeight = elementHeight,
                // height of current row inside table (used for iteration)
                trOffsetHeight,
                // option flag to check is it called during initial page break rendering
                initial = getBooleanOption(options, 'initial', false),
                //summed value of all rows above last page break
                summedAttrPBHeight = 0,
                // generated string with page break node
                markup,
                // IE needs 2px more to left for split tables, Firefox 1px - due to css border-collapse
                negativeMarginTablesFix = _.browser.IE ? 2 : (_.browser.Firefox ? 1 : 0),
                // storing unmodified page height available for content
                tempPageMaxHeight = pageMaxHeight,
                // if table has set property for repeating first row across pages, or not
                isRepeatedHeaderRow = shouldRepeatHeadRow(tableRows),
                // content of the repeated row
                repeatRowContent = isRepeatedHeaderRow ? getRepeatRowContent() : null, // in format definition, n-rows can be repeated, but we fetch only first
                // hight of repeated row
                repeatedRowHeight = isRepeatedHeaderRow ? tableRows[0].offsetHeight : 0,
                // number of columns in a table
                colspan = $(tableRows[0]).children('td').length,
                tableRow;

            // local helper for cloning and cleaning up content of the repeated row
            function getRepeatRowContent() {
                var content = $(tableRows[0]).children().clone();

                content.find('.resize').remove();
                return content;
            }
            // local helper for determining if first row has repeating property
            function shouldRepeatHeadRow(rows) {
                return rows.length > 1 && rows[0].offsetHeight < tempPageMaxHeight && DOM.isRepeatRowNode(rows[0]);
            }

            tableRow = tableRows[0];
            while (isRepeatedHeaderRow && tableRow) {
                if (isRepeatedHeaderRow && repeatedRowHeight + tableRow.offsetHeight > tempPageMaxHeight) {
                    isRepeatedHeaderRow = false;
                }
                tableRow = tableRow.nextSibling;
            }
            _.each(tableRows, function (tr, index) {
                trOffsetHeight = tr.offsetHeight;
                if (accumulator + trOffsetHeight < pageMaxHeight - tempContentHeight) {
                    accumulator += trOffsetHeight;
                } else {
                    if (index > 0 || !(DOM.isManualPageBreakNode($prevElement) && DOM.isPageBreakNode($prevElement.children().last()))) {
                        breakAboveRows.push(tr);
                        totalAcc += accumulator;
                        if (arrAccumulator.length === 0 && isRepeatedHeaderRow) { pageMaxHeight -= repeatedRowHeight; } // reducing available page content space for repeated row, starting from second splitted page
                        arrAccumulator.push(totalAcc);
                    }
                    tempContentHeight = trOffsetHeight;
                    totalAcc = tempContentHeight;
                    accumulator = 0;
                }
            });

            if (tableRows[0] === breakAboveRows[0]) { // page break inside table before first table row is unnecessary, we insert pb before table.
                if ($prevElement.length > 0) {
                    markup = generatePagebreakElementHtml(contentHeight, _.browser.WebKit);
                    $element.before(markup);
                }
                $element.addClass('break-above'); // this is marker for first node bellow pagebreak
                contentHeight = 0;
                arrAccumulator.shift();
                breakAboveRows.shift(); // see #45622
            }

            if (breakAboveRows.length > 0) {
                $element.addClass('tb-split-nb');
                _.each(breakAboveRows, function (breakAboveRow, i) {
                    contentHeight += arrAccumulator[i];
                    markup = generateTableRowPagebreakHTML(contentHeight, negativeMarginTablesFix, colspan);
                    $(breakAboveRow).before(markup);
                    if (isRepeatedHeaderRow) {
                        markup = generateRepeatedRowHTML(contentHeight, repeatRowContent.clone());
                        $(breakAboveRow).before(markup);
                    }
                    if (tableHeight > pageMaxHeight) {
                        contentHeight = 0;
                    } else {
                        contentHeight = tableHeight - arrAccumulator[i];
                    }
                    tableHeight -= arrAccumulator[i];
                    summedAttrPBHeight += arrAccumulator[i];
                });
                contentHeight = elementHeight - summedAttrPBHeight; // height of part of table transferred to new page
                if (isRepeatedHeaderRow) { // return original pageMaxHeight value if modified by repeating row
                    pageMaxHeight = tempPageMaxHeight;
                    contentHeight += repeatedRowHeight;
                }
            } else {
                contentHeight = elementHeight;
                //  $element.removeClass('tb-split-nb');
            }
            if (!initial && _.browser.Firefox) { forceTableRendering(element); } // forcing Firefox to re-render table (32461)

            return contentHeight;
        }

        /**
         * In read only mode, tables have to have all elements with contenteditable set to false.
         * Collection saves all nodes for later restoration, in editmode.
         *
         */
        function toggleContentEditableTrueNodes() {
            $headerFooterPlaceHolder.find('[contenteditable="true"]').attr('contenteditable', false);
        }

        /**
         * Internal helper function that marks all children of headerfooter node with marginal class.
         * This helps with evaluating changeTrack nodes.
         *
         * @param {jQuery} $node
         *  container node whose children are marked
         */
        function markMarginalNodesInsideHeadFoot($node) {
            $node.find('.p, table').addClass(DOM.MARGINAL_NODE_CLASSNAME).find('table, tr, td, div, span').addClass(DOM.MARGINAL_NODE_CLASSNAME);
        }

        /**
         * Internal function for page layout rendering!
         * Generating html markup string for Header to insert into page break html markup.
         *
         * @returns {String}
         *  Div header html markup
         */
        function getHeaderHtmlContent(type) {

            type = type ? (' ' + type) : '';

            var currentHeight = 0,
                minHeight,
                distanceBottom = 0,
                currentTypeChildren = null,
                useHeaderMargin = self.isHeaderInDocument() || docApp.isODF();

            if (docApp.isODF()) {
                currentHeight = (type === ' first' && firstPage) ? odfFirstHeader.height : (currentPageType === 'even' ? odfEvenHeader.height : (odfDefHeader.height ? odfDefHeader.height : 0));
                distanceBottom = (type === ' first' && firstPage) ? odfFirstHeader.bottom : (currentPageType === 'even' ? odfEvenHeader.bottom : (odfDefHeader.bottom ? odfDefHeader.bottom : 0));
            }
            minHeight = Math.max(currentHeight + pagePaddingTop, pagePaddingTop);

            if (type === ' first') {
                if (firstPage && firstPageHeight > 0) {
                    currentPageType = 'first';
                } else if (defPageHeight > 0) {
                    currentPageType = 'default';
                }
            } else {
                switchPageTypeAndHeight();
            }

            if (self.isHeaderInDocument() && $headerFooterPlaceHolder.children('.header_' + currentPageType).length) {
                currentTypeChildren = $headerFooterPlaceHolder.children('.header_' + currentPageType).last();
                return currentTypeChildren.css({ minHeight, maxHeight: marginalMaxHeight, paddingTop: headerMargin, paddingRight: pagePaddingRight, paddingBottom: distanceBottom, paddingLeft: pagePaddingLeft }).clone(true);
            } else {
                return '<div class="header inactive-selection' + type + '" contenteditable=' + (_.browser.WebKit ? '"false"' : '""') + ' style="min-height: ' + minHeight +
                    'px; max-height: ' + marginalMaxHeight + 'px;  padding: ' + (useHeaderMargin ? headerMargin : 0) + 'px ' + pagePaddingRight + 'px ' + distanceBottom + 'px ' + (pagePaddingLeft) + 'px;" data-focus-role="header">' +
                    '<div class="cover-overlay" contenteditable="false"></div><div class="marginalcontent"></div></div>';
            }
        }

        /**
         * Internal function for page layout rendering!
         * Generating html markup string for Footer to insert into page break html markup.
         *
         * @returns {String}
         *  Div header html markup
         */
        function getFooterHtmlContent(type) {

            type = type || '';

            var currentHeight = 0,
                minHeight,
                distanceTop = 0,
                currentTypeChildren = null,
                useFooterMargin = self.isFooterInDocument() || docApp.isODF();

            if (docApp.isODF()) {
                currentHeight = (currentPageType === 'first') ? odfFirstFooter.height : (currentPageType === 'even' ? odfEvenFooter.height : (odfDefFooter.height ? odfDefFooter.height : 0));
                distanceTop = (currentPageType === 'first') ? odfFirstFooter.top : (currentPageType === 'even' ? odfEvenFooter.top : (odfDefFooter.top ? odfDefFooter.top : 0));
            }
            minHeight = Math.max(currentHeight + pagePaddingBottom, pagePaddingBottom);

            if (self.isFooterInDocument() && $headerFooterPlaceHolder.children('.footer_' + currentPageType).length) {
                currentTypeChildren = $headerFooterPlaceHolder.children('.footer_' + currentPageType).last();
                return currentTypeChildren.css({ minHeight, maxHeight: marginalMaxHeight, paddingTop: Math.max(distanceTop, 5), paddingRight: pagePaddingRight, paddingBottom: footerMargin, paddingLeft: pagePaddingLeft }).clone(true);
            } else {
                return '<div class="footer inactive-selection ' + (_.browser.IE ? '' : 'flexfooter') + type + '" contenteditable=' + (_.browser.WebKit ? '"false"' : '""') + ' style="min-height: ' + minHeight +
                    'px; max-height: ' + marginalMaxHeight + 'px; padding: ' + Math.max(distanceTop, 5) + 'px ' + pagePaddingRight + 'px ' + (useFooterMargin ? footerMargin : 0) + 'px ' + (pagePaddingLeft) + 'px;" data-focus-role="footer"><div class="cover-overlay" contenteditable="false"></div><div class="marginalcontent' + (_.browser.IE ? '' : ' flexcontent') + '"></div></div>';
            }
        }

        /**
         * Called from implCreateHeaderFooter function, generates header DOM element for given operation attributes.
         *
         * @param {String} operationId
         * @param {String} operationType
         */
        function createHeaderContainer(operationId, operationType) {
            return $('<div>').addClass('header inactive-selection ' + operationId + ' ' + operationType)
                .attr('contenteditable', false).attr('data-container-id', operationId).attr('data-focus-role', 'header')
                .data({ id: operationId, type: operationType })
                .prepend(createCoverOverlay())
                .append($('<div>').addClass(DOM.MARGINALCONTENT_NODE_CLASSNAME).append($('<div>').addClass('p').data('implicit', true)));
        }

        /**
         * Called from implCreateHeaderFooter function, generates footer DOM element for given operation attributes.
         *
         * @param {String} operationId
         * @param {String} operationType
         */
        function createFooterContainer(operationId, operationType) {
            return $('<div>').addClass('footer inactive-selection ' + operationId + ' ' + operationType + (_.browser.IE ? '' : ' flexfooter'))
                .attr('contenteditable', false).attr('data-container-id', operationId).attr('data-focus-role', 'footer')
                .data({ id: operationId, type: operationType })
                .prepend(createCoverOverlay())
                .append($('<div>').addClass(DOM.MARGINALCONTENT_NODE_CLASSNAME + (_.browser.IE ? '' : ' flexcontent')).append($('<div>').addClass('p').data('implicit', true)));
        }

        /**
         * Creates overlay node over headers/footer while they are inactive, to prevent clicking/ selecting.
         *
         * @returns {jQuery} created jQuery node for covering marginal nodes.
         */
        function createCoverOverlay() {
            return $('<div>').addClass('cover-overlay').attr('contenteditable', false);
        }

        /**
         * Update styling of header&footer inside placeholder, and fetch their heights.
         */
        function updateStartHeaderFooterStyle() {
            var
                headers = $headerFooterPlaceHolder.find('.header'),
                footers = $headerFooterPlaceHolder.find('.footer');

            if (!pageAttributes) {
                initializePageAttributes();
            }

            $headerFooterPlaceHolder.css({
                width: pageWidth,
                margin: '0 0 150px -' + (pagePaddingLeft) + 'px',
                left: pagePaddingLeft
            });
            headers.css({
                minHeight: pagePaddingTop,
                maxHeight: marginalMaxHeight,
                maxWidth: pageWidth,
                padding: headerMargin + 'px ' + pagePaddingRight + 'px 0 ' + (pagePaddingLeft) + 'px'
            });
            footers.css({
                minHeight: pagePaddingBottom,
                maxHeight: marginalMaxHeight,
                maxWidth: pageWidth,
                padding: '5px ' + pagePaddingRight + 'px ' + footerMargin + 'px ' + (pagePaddingLeft) + 'px'
            });
            if (!_.browser.IE) {
                footers.addClass('flexfooter');
                footers.children('.marginalcontent').addClass('flexcontent');
            }

            rootNode.css({ paddingBottom: 0, paddingTop: 0 });

            // update height vaules of headers&footers
            _.each(headers, function (header) {
                if (header.classList.contains('header_first')) {
                    heightOfFirstHeader = $(header).outerHeight(true);
                } else if (header.classList.contains('header_even')) {
                    heightOfEvenHeader = $(header).outerHeight(true);
                } else {
                    heightOfDefHeader = $(header).outerHeight(true);
                }
            });
            _.each(footers, function (footer) {
                if (footer.classList.contains('footer_first')) {
                    heightOfFirstFooter = $(footer).outerHeight(true);
                } else if (footer.classList.contains('footer_even')) {
                    heightOfEvenFooter = $(footer).outerHeight(true);
                } else {
                    heightOfDefFooter = $(footer).outerHeight(true);
                }
            });

            if (heightOfDefHeader !== 0 || heightOfDefFooter !== 0) {
                defPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfDefHeader) - Math.max(pagePaddingBottom, heightOfDefFooter);
            } else {
                defPageHeight = self.getDefPageActiveHeight({ convertToPixel: true });
            }

            if (heightOfFirstHeader !== 0 || heightOfFirstFooter !== 0) {
                firstPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfFirstHeader) - Math.max(pagePaddingBottom, heightOfFirstFooter);
                if (docApp.isODF()) {
                    firstPage = true; // for ODF docs this document attribute isn't set
                }
            } else if (firstPage && !docApp.isODF()) {
                firstPageHeight = defPageHeight;
            }

            if (heightOfEvenHeader !== 0 || heightOfEvenFooter !== 0) {
                evenPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfEvenHeader) - Math.max(pagePaddingBottom, heightOfEvenFooter);
                if (docApp.isODF()) {
                    evenOddPages = true; // for ODF docs this document attribute isn't set
                }
            } else if (evenOddPages && !docApp.isODF()) {
                evenPageHeight = defPageHeight;
            }

            //console.warn('firstPageHeight: ', firstPageHeight);
            //console.warn('evenPageHeight: ', evenPageHeight);
            //console.warn('defPageHeight: ', defPageHeight);
        }

        /**
         * On switching to new page, change global properties for main page content height, and type of page
         */
        function switchPageTypeAndHeight() {

            switch (currentPageType) {
                case 'first':
                case 'default':
                    if (evenOddPages && evenPageHeight > 0) {
                        pageMaxHeight = evenPageHeight;
                        currentPageType = 'even';
                    } else if (currentPageType === 'first') {
                        currentPageType = 'default';
                        pageMaxHeight = defPageHeight;
                    }
                    break;
                case 'even':
                    pageMaxHeight = defPageHeight;
                    currentPageType = 'default';
                    break;
            }
        }

        /**
         * Internal function for page layout rendering!
         * Generating html markup string to insert as page break div element inside page.
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @param {Boolean} contentEditableProperty
         *  Webkit browsers needs to set contenteditable to false, others to empty
         *
         * @param {Number} [elementLeftIndent]
         *  Left indent of element, in px
         *
         * @returns {String|jQuery}
         *  Div page break html string or node markup
         */
        function generatePagebreakElementHtml(contentHeight, contentEditableProperty, elementLeftIndent) {
            var leftIndent = elementLeftIndent || 0,
                markup,
                marginLeft = Math.max(pageMaxHeight - contentHeight, 0);

            if (self.isHeaderFooterInDocument()) {

                markup = $('<div>').addClass('page-break').attr('contenteditable', (contentEditableProperty ? 'false' : ''))
                    .css({ width: pageWidth, margin: (marginLeft + 'px 0px 0px -' + (pagePaddingLeft + leftIndent) + 'px') })
                    .append(getFooterHtmlContent())
                    .append($('<div>').addClass('inner-pb-line'))
                    .append(getHeaderHtmlContent());
            } else {

                markup = '<div class="page-break" contenteditable=' + (contentEditableProperty ? '"false"' : '""') + 'style="width: ' + pageWidth +
                    'px; margin: ' + marginLeft + 'px 0px 0px -' + (pagePaddingLeft + leftIndent) + 'px">' +
                    getFooterHtmlContent() + '<div class="inner-pb-line"></div>' + getHeaderHtmlContent() + '</div>';
            }

            return markup;
        }

        /**
         * Internal function for page layout rendering!
         * Generating html markup string to insert inside table as row containing page break div.
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @param {Number} negativeMarginTablesFix
         *  IE needs 2px, FF 1px, due to border collapse
         *
         * @param {Number} colspan
         *  Number of columns in table
         *
         * @returns {String|jQuery}
         *  Table row containing div page break html string or node markup
         */
        function generateTableRowPagebreakHTML(contentHeight, negativeMarginTablesFix, colspan) {
            var markup;

            if (self.isHeaderFooterInDocument()) {
                markup = $('<tr>').addClass('pb-row').css('border', 'none')
                    .append($('<td>').css({ padding: '0', border: 'none' }).attr('colspan', String(colspan))
                        .append($('<div>').addClass('page-break').attr('contenteditable', _.browser.WebKit ? 'false' : '')
                            .css({ width: pageWidth, margin: (Math.max(pageMaxHeight - contentHeight, 0) + 'px 0px 0px -' + (pagePaddingLeft + negativeMarginTablesFix) + 'px') })
                            .append(getFooterHtmlContent())
                            .append($('<div>').addClass('inner-pb-line'))
                            .append(getHeaderHtmlContent())
                        )
                    );
            } else {
                markup = '<tr class="pb-row" style="border: none"><td style="padding: 0px; border: none" colspan="' + colspan + '"><div class="page-break" contenteditable=' +
                    (_.browser.WebKit ? '"false"' : '""') + 'style="width: ' + pageWidth + 'px; margin: ' + (Math.max(pageMaxHeight - contentHeight, 0)) +
                    'px 0px 0px -' + (pagePaddingLeft + negativeMarginTablesFix) + 'px">' + getFooterHtmlContent() + '<div class="inner-pb-line"></div>' + getHeaderHtmlContent() + '</div></td></tr>';
            }
            return markup;
        }

        /**
         * Internal function for page layout rendering!
         * Generating html markup string to insert inside table as row containing page break div.
         *
         * @param {Number} contentHeight
         *  Height of all content in current page, in px
         *
         * @returns {String|jQuery}
         *  Table row containing div page break html string or node markup
         */
        function generateRepeatedRowHTML(contentHeight, row) {
            var markup = $('<tr>').addClass('pb-row repeated-row').css({ userSelect: 'none', cursor: 'default' })
                .append(row);
            markup.find('[contenteditable="true"], div').attr('contenteditable', false);

            return markup;
        }

        /**
         * Check, whether a specified paragraph has the attibute 'pageBreakBefore'.
         *
         * @returns {Boolean}
         *   Whether a specified paragraph has the attibute 'pageBreakBefore'.
         */
        function hasPageBreakBeforeAttribute($element) {
            var paraAttrs = docModel.paragraphStyles.getElementAttributes($element);
            return !!paraAttrs.paragraph?.pageBreakBefore;
        }

        /**
         * Internal function for page layout rendering!
         * Default insertion of manual page breaks,
         * it adds page break before passed element if it is paragraph style attribute,
         * or after if it is Microsoft's hardbreak type page
         *
         * @param {Node} element
         *  DOM element currently beeing proccessed
         *
         * @param {Number} contentHeight
         *  Height of content in current page before proccessing
         *
         * @param {Number} elementHeight
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.cleanup=false]
         *      If its neccessary to do pagebreaks cleanup before insertion.
         *      Happens if it's not called from initial page break render function.
         *
         * @returns {Number}
         *  Height of content in current page after proccessing
         */
        function manualInsertionOfPageBreak(element, contentHeight, elementHeight, options) {
            var
                $element = $(element),
                cleanup = getBooleanOption(options, 'cleanup', false),
                msHardbreakPage = $element.find('.ms-hardbreak-page'),
                textSpanAfterHardbreak = msHardbreakPage.last().parent('.hardbreak').next(),
                markup,
                $nodeHelper,
                tempContentHeight = contentHeight,
                isAfterLastElement = false,
                elementLeftIndent = parseInt($element.css('margin-left'), 10) + parseInt($element.css('padding-left'), 10); // for indented elements such as lists, or special paragraphs;

            if (cleanup) {
                $nodeHelper = $element.prev();
                // remove old page break
                while ($nodeHelper.hasClass('page-break')) {
                    $nodeHelper.remove();
                    // TODO
                    pageBreaksCollection = $pageContentNode.find('.page-break');
                    resolvePageTypeAndHeight(element);

                    $nodeHelper = $element.prev();
                }

                prepareNodeAndRemovePageBreak(element);
            }
            if (!_.isUndefined(element.rows) && $element.find(DOM.MANUAL_PAGE_BREAK_SELECTOR).length < 1) {
                $element.removeClass('manual-page-break');
                return contentHeight;
            }

            $element.next().addClass('break-above'); // this is marker for first node bellow pagebreak
            if (msHardbreakPage.length > 0) { // MS Word hardbreak page
                tempContentHeight += msHardbreakPage.last().parent('.hardbreak').position().top;

                _.each(msHardbreakPage, function (msBreak) {
                    var spanAfterHardbreak,
                        nextAfterSpan,
                        hardbreakNode = $(msBreak).parent('.hardbreak');

                    if (hardbreakNode.prevAll(DOM.LIST_LABEL_NODE_SELECTOR).length > 0 && _.last(getOxoPosition(docModel.getNode(), hardbreakNode, 0)) === 0) { // #35649 hardbreak between text and list label (no text before the hardbreak node (67935))
                        markup = generatePagebreakElementHtml(tempContentHeight, _.browser.WebKit);
                        $element.before(markup);
                    } else {
                        markup = generatePagebreakElementHtml(tempContentHeight, _.browser.WebKit, elementLeftIndent);
                        // if next element after mpb is empty text span, that is last in paragraph -> insert after it
                        // if next element after mpb is empty text span, that is not last in p, but before next mpb -> insert after it
                        // in other cases -> insert manual pb before span
                        spanAfterHardbreak = hardbreakNode.next();
                        nextAfterSpan = spanAfterHardbreak.next();
                        if (DOM.isEmptySpan(spanAfterHardbreak) && ((!nextAfterSpan.length || nextAfterSpan.is('br')) || (nextAfterSpan.hasClass('.hardbreak') && nextAfterSpan.data('type') === 'page'))) {
                            if (nextAfterSpan.is('br')) {
                                spanAfterHardbreak.addClass('break-above-span');
                                nextAfterSpan.after(markup);
                            } else {
                                spanAfterHardbreak.addClass('break-above-span').after(markup);
                            }
                            if (!nextAfterSpan.length || nextAfterSpan.is('br')) {
                                isAfterLastElement = true; // if manual pb is after last element
                            }
                        } else {
                            spanAfterHardbreak.addClass('break-above-span').before(markup);
                        }
                        $element.addClass('contains-pagebreak');
                    }
                });
                if (elementHeight <= pageMaxHeight && !isAfterLastElement) {
                    contentHeight = textSpanAfterHardbreak.length > 0 ? $element.height() - textSpanAfterHardbreak.position().top : 0;
                } else {
                    contentHeight = 0;
                }
                if (isAfterLastElement) {
                    $element.addClass('no-margin-bottom');
                } else {
                    $element.removeClass('no-margin-bottom');
                }
            } else {
                var followingParagraphsHavePageBreakBefore = false;
                if ($element.hasClass('manual-page-break') && $element.prev().hasClass('manual-page-break')) { // DOCS-3377
                    // check, if the current and the previous paragraph have the attribute 'pageBreakBefore' -> then the page break must be inserted
                    followingParagraphsHavePageBreakBefore = hasPageBreakBeforeAttribute($element) && hasPageBreakBeforeAttribute($element.prev());
                }
                if (followingParagraphsHavePageBreakBefore || !$element.prev().hasClass('manual-page-break') || !$element.prev().find('.ms-hardbreak-page').length) { // #38544
                    markup = generatePagebreakElementHtml(contentHeight, _.browser.WebKit);
                    $element.before(markup);
                }
                contentHeight = 0;
            }

            $element.addClass('manual-page-break');

            return contentHeight;
        }

        /**
         *
         * Internal function for page layout rendering!
         * Support function for Open Document Format, adds manual page break after passed node element
         *
         * @param {Node} element
         *  DOM element currently beeing proccessed
         *
         * @param {Number} contentHeight
         *  Height of content in current page before proccessing; in px
         *
         * @returns {Number}
         *  Height of content in current page after proccessing; in px
         */
        function insertManualPageBreakAfter(element, contentHeight) {
            $(element).after(generatePagebreakElementHtml(contentHeight, _.browser.WebKit));
            // new element will always start at the beginning of new page
            contentHeight = 0;
            return contentHeight;
        }

        /**
         * Internal function for page layout rendering!
         * Makes preparation for node proccessing,
         * like cleanup of previous page breaks, and class naming
         *
         * @param {Node} element
         *  DOM element currently beeing proccessed
         *
         */
        function prepareNodeAndRemovePageBreak(element) {
            var
                $element = $(element),
                $nodeHelper = $element.next();

            //if element is last in document
            if ($nodeHelper.length) {
                // remove old page break
                while ($nodeHelper.hasClass('page-break')) {
                    $nodeHelper.remove();
                    $nodeHelper = $element.next();
                }
            }
        }

        /**
         * Loops through collection of page breaks,
         * and determines correct class name for each header and footer,
         * regarding of page number.
         *
         * @param {Number[]|jQuery} pageBreaksCollection
         */
        function populateHeaderFooterPaginationClasses(pageBreaksCollection) {

            _.each(pageBreaksCollection, function (pageBreak, index) {
                if (index === 0) {
                    $(pageBreak).children('.footer').addClass('first');
                    $(pageBreak).children('.header').addClass('even');
                } else if (index % 2 === 0) {
                    $(pageBreak).children('.header').addClass('even');
                    $(pageBreak).children('.footer').addClass('default');
                } else {
                    $(pageBreak).children('.footer').addClass('even');
                    $(pageBreak).children('.header').addClass('default');
                }
            });
            $firstHeaderWrapper.children('.header').addClass('first');
            $lastFooterWrapper.children('.footer').removeClass('first default even');
            if (pageBreaksCollection.length === 0) {
                $lastFooterWrapper.children('.footer').addClass('first');
            } else if (pageBreaksCollection.length % 2 === 0) {
                $lastFooterWrapper.children('.footer').addClass('default');
            } else {
                $lastFooterWrapper.children('.footer').addClass('even');
            }
        }

        /**
         * Internal function to update data inside special fields with type: page-count.
         * Frontend needs to overwrite dummy values backend sends,
         * because backend has no knowledge of page count after rendering doc in browser.
         *
         */
        function updatePageCountFields() {
            var $fields = docApp.isODF() ? rootNode.find(DOM.PAGECOUNT_FIELD_SELECTOR) : pageBreaksCollection.add($firstHeaderWrapper).add($lastFooterWrapper).find(DOM.PAGECOUNT_FIELD_SELECTOR);

            _.each($fields, function (field) {
                var format = (docApp.isODF()) ? DOM.getFieldPageFormat(field) : DOM.getFieldInstruction(field);
                $(field.childNodes).empty().html(fieldManager.formatFieldNumber(pageCount, format, true));
            });
        }

        /**
         * Internal function to update data inside special fields with type: page-number.
         * Frontend needs to overwrite dummy values backend sends,
         * because backend has no knowledge of page count after rendering doc in browser.
         *
         */
        function updatePageNumberFields() {
            // update in headers & footers
            _.each($pageContentNode.find('.header').find(DOM.PAGENUMBER_FIELD_SELECTOR), function (field) {
                var format = (docApp.isODF()) ? DOM.getFieldPageFormat(field) : DOM.getFieldInstruction(field),
                    number = $(field).parentsUntil('.pagecontent', '.page-break').data('page-num') + 1;
                $(field).children().empty().html(fieldManager.formatFieldNumber(number, format));
            });

            _.each($pageContentNode.find('.footer').find(DOM.PAGENUMBER_FIELD_SELECTOR), function (field) {
                var format = (docApp.isODF()) ? DOM.getFieldPageFormat(field) : DOM.getFieldInstruction(field),
                    number = $(field).parentsUntil('.pagecontent', '.page-break').data('page-num');
                $(field).children().empty().html(fieldManager.formatFieldNumber(number, format));
            });
            // first header in doc - page number always 1
            _.each($firstHeaderWrapper.children('.header').find(DOM.PAGENUMBER_FIELD_SELECTOR), function (field) {
                var format = (docApp.isODF()) ? DOM.getFieldPageFormat(field) : DOM.getFieldInstruction(field);
                $(field).children().empty().html(fieldManager.formatFieldNumber(1, format));
            });
            // last footer in doc gets total page count as page number
            _.each($lastFooterWrapper.children('.footer').find(DOM.PAGENUMBER_FIELD_SELECTOR), function (field) {
                var format = (docApp.isODF()) ? DOM.getFieldPageFormat(field) : DOM.getFieldInstruction(field);
                $(field).children().empty().html(fieldManager.formatFieldNumber(pageCount, format));
            });

            if (docApp.isODF()) {
                // update in paragraphs as first level children of pagecontent
                _.each($pageContentNode.children(DOM.PARAGRAPH_NODE_SELECTOR).children(DOM.PAGENUMBER_FIELD_SELECTOR), function (field) {
                    var format = DOM.getFieldPageFormat(field),
                        number = fieldManager.formatFieldNumber(self.getPageNumber(field), format);
                    $(field).children().empty().html(number);
                });
                // update in paragraphs inside tables as first level children of pagecontent
                _.each($pageContentNode.children(DOM.TABLE_NODE_SELECTOR).find('tr').find(DOM.PAGENUMBER_FIELD_SELECTOR), function (field) {
                    var format = DOM.getFieldPageFormat(field),
                        number = fieldManager.formatFieldNumber(self.getPageNumber(field), format);
                    $(field).children().empty().html(number);
                });
            }
        }

        /**
         * Creates overlay layer for tooltip over user-fields.
         *
         */
        function createTooltipOverlay() {
            $tooltipOverlay = $('<div>').addClass('field-tooltip-layer').attr('contenteditable', 'false');
            rootNode.append($tooltipOverlay);
        }

        /**
         * Generates unique id, used as target parameter for insertHeaderFooter operation
         *
         * @param {String} type
         *  Type of header or footer to be generated.
         *
         * @returns {String}
         *  generated target string
         */
        function generateOperationTargetString(type) {
            if (docApp.isODF()) {
                return type + '_Standard';
            } else {
                if (targetIdNumbersCollection.length) {
                    generatedTargetNumber = _.last(targetIdNumbersCollection) + 1;
                }
                return 'OX_rId' + generatedTargetNumber;
            }
        }

        /**
         * Generate operation for header or footer insertion in document.
         *
         * @param {String} type
         *  Type of header or footer that is going to be created.
         */
        function createOperationInsertHeaderFooter(type) {
            var localGenerator = docModel.createOperationGenerator(),
                operation = { id: generateOperationTargetString(type), type },
                isHeader = type.indexOf('header') > -1,
                localStyleId = isHeader ? docModel.paragraphStyles.getStyleIdByName('Header') : docModel.paragraphStyles.getStyleIdByName('Footer'),
                paraAttr = null;

            pageLogger.info('pageLayout.createOperationInsertHeaderFooter() with type', type);

            if ($headerFooterPlaceHolder.children('.' + type).length) {
                pageLogger.warn('createOperationInsertHeaderFooter: node with this type already exists!', type);
                replaceAllTypesOfHeaderFooters();
                return;
            }

            if (docModel.paragraphStyles.isDirty(localStyleId)) {
                docModel.generateInsertStyleOp(localGenerator, 'paragraph', localStyleId);
            }
            paraAttr = { styleId: localStyleId };

            localGenerator.generateOperation(INSERT_HEADER_FOOTER, operation);
            localGenerator.generateOperation(PARA_INSERT, { start: [0], target: operation.id, attrs: paraAttr });
            docModel.applyOperations(localGenerator);

            // Important: The following code was moved to 'implCreateHeaderFooter', but in that function
            // it is executed too early. The update of the marginal nodes is required, after inserting
            // the paragraph, not after inserting the header node without paragraph.
            //
            // The problem at this position is, that this code is only executed for that client
            // that inserted this header/footer. But this problem could be less problematic in this
            // case, because the update from placeholder to document happens also, if another user
            // starts to activate the header/footer.
            //
            // TODO: Check if the code can be removed from 'implCreateHeaderFooter' again.

            // at the end mark all nodes with marginal class, in both current node and placeholder.
            // it is important that placeholder node is also processed, because there can be situation where updateEditingHeaderFooter is not triggered.
            rootNode = docModel.getRootNode(operation.id); // modifying root node is allowed, because this function is not triggered from external operations
            markMarginalNodesInsideHeadFoot(rootNode);
            if (!DOM.isMarginalPlaceHolderNode(rootNode.parent())) {
                $headerFooterPlaceHolder.children('.' + type).empty().append(rootNode.children().clone(true));
                replaceAllTypesOfHeaderFooters();
            }
            if (docApp.isOOXML()) {
                // change the padding of the root node after creation;
                // OOXML feature not to use headerMargin or footerMargin when no header/footer is in the document
                var cssPadding = isHeader ? { paddingTop: headerMargin } : { paddingBottom: footerMargin };
                var nodesCollection = isHeader ? self.getAllHeaders() : self.getAllFooters();
                nodesCollection.css(cssPadding);
            }
        }

        /**
         * ODF only: Generate operations for elements inside header/footer that is beeing deleted.
         *
         * @param {jQuery} node
         *  Cached node from which insert header/footer operation
         *  and following content operations are going to be reconstructed.
         * @param {String} type
         *  Type of header or footer that is going to be created.
         */
        function generateHFopODT(node, type) {
            var localGenerator = docModel.createOperationGenerator(),
                id = DOM.getTargetContainerId(node),
                operation = { id, type },
                isHeader = type.indexOf('header') > -1,
                localStyleId = isHeader ? docModel.paragraphStyles.getStyleIdByName('Header') : docModel.paragraphStyles.getStyleIdByName('Footer'),
                options = null,
                position = [],
                paraAttr = null;

            if (docModel.paragraphStyles.isDirty(localStyleId)) {
                docModel.generateInsertStyleOp(localGenerator, 'paragraph', localStyleId);
            }
            localGenerator.generateOperation(INSERT_HEADER_FOOTER, operation);

            node.children('.cover-overlay').remove();
            if (node.children('.marginalcontent').children().length) {
                options = { target: id };
                localGenerator.generateContentOperations(node, position, options);
            } else {
                paraAttr = { styleId: localStyleId };
                localGenerator.generateOperation(PARA_INSERT, { start: [0], target: id, attrs: paraAttr });
            }
            return localGenerator.getOperations();
        }

        /**
         * ODF only: on switching h/f types, operations are cached for restoring h/f nodes.
         * This function applies cached operations.
         *
         * @param {Object} operationsObject
         *  Contains id of header/footer, and set of pre-generated, cached operations to be applied.
         * @param {String} type
         *  Type of header or footer that is going to be created.
         */
        function callHFopODT(operationsObject, type) {
            var rootNode;
            var id = (operationsObject && operationsObject.id) || null;
            var operations = (operationsObject && operationsObject.operations) || null;

            if (operations && id) {
                docModel.applyOperations(operations);

                // at the end mark all nodes with marginal class, in both current node and placeholder.
                // it is important that placeholder node is also processed, because there can be situation where updateEditingHeaderFooter is not triggered.
                rootNode = docModel.getRootNode(id); // modifying rootNode is allowed, because this function is not triggered from external operation
                markMarginalNodesInsideHeadFoot(rootNode);
                if (!DOM.isMarginalPlaceHolderNode(rootNode.parent())) {
                    $headerFooterPlaceHolder.children('.' + type).empty().append(rootNode.children().clone(true));
                    replaceAllTypesOfHeaderFooters();
                }
            }
        }

        /**
         * In order not to jump to first header/footer in document,
         * store reference to node user selects, and after successful createHeaderFooter,
         * enter headerFooterEditMode for that node.
         *
         * @param {jQuery} $node
         *  Node instance that is going to be stored.
         */
        function storeNodeForHeaderFooterCreation($node) {
            $storedHFnode = $node;
        }

        /**
         * In order not to jump to first header/footer in document,
         * get the reference to node user selects, and after successful createHeaderFooter,
         * enter headerFooterEditMode for that node.
         *
         * @returns {jQuery}
         *  Returns stored node.
         */
        function getNodeStoredOnHeaderFooterInsertion() {
            return $storedHFnode;
        }

        /**
         * For passed container node, that is not yet header or footer,
         * based on position in page and page number, determine type of node
         * passing to insertHeaserFooter operation.
         *
         * @param {jQuery} $node
         *  Passed node for which we want to get type
         *
         * @returns {String}
         *  Calculated type.
         */
        function getHeadFootTypeForCreateOp($node) {
            var
                isHeader = $node.hasClass('header'),
                headerType = 'header_default',
                footerType = 'footer_default',
                pageNum = self.getPageNumber($node);

            if (firstPage && pageNum === 1) {
                headerType = 'header_first';
                footerType = 'footer_first';
            } else if (evenOddPages && (pageNum % 2 === 0)) {
                headerType = 'header_even';
                footerType = 'footer_even';
            }
            return isHeader ? headerType : footerType;
        }

        /**
         * Create new context menu node to append to parent header or footer container.
         *
         * @param {String} type
         *  Type of header or footer
         *
         * @param {Boolean} [isHeader]
         *  If true, it is header, if not, it's footer.
         *
         * @returns {jQuery}
         *  Created context menu.
         */
        function createContextMenu(type, isHeader) {

            var newContextMenu = $('<div class="marginal-context-menu ' + (isHeader ? 'below-node' : 'above-node') + ' ">').append(
                $('<div class="marginal-menu-type">').append(
                    $('<div class="caption">').append(
                        createSpan({ classes: 'marginal-label', label: generateMarginalNodeTypeText(type) }),
                        createIcon('bi:caret-down-fill', 'caret-icon')
                    ),
                    $('<div class="marginal-container">').append(
                        createDiv({ classes: 'marginal-same',       label: gt('Same across entire document') }),
                        createDiv({ classes: 'marginal-first',      label: gt('Different first page') }),
                        createDiv({ classes: 'marginal-even',       label: gt('Different even & odd pages') }),
                        createDiv({ classes: 'marginal-first-even', label: gt('Different first, even & odd pages') }),
                        createDiv('marginal-separator-line'),
                        createDiv({ classes: 'marginal-none',       label: gt('Remove all headers and footers') })
                    )
                ),
                $('<div class="marginal-menu-goto" data-target="' + (isHeader ? 'footer' : 'header') + '">').append(
                    createDiv({ classes: 'caption', label: isHeader ? gt('Go to footer') : gt('Go to header') })
                ),
                $('<div class="marginal-menu-close">').append(
                    $('<div class="caption">').append(
                        createSpan({ label: CLOSE_LABEL }),
                        createIcon('bi:x')
                    )
                )
            );

            if (_.browser.Chrome) {
                newContextMenu.children().css({ display: 'inline-flex' }); // fix of buttons in chrome, for different browser zoom values
            }

            return newContextMenu;
        }

        /**
         * Gets the context menu node for header.
         * If it's not created yet, it is first created.
         * If already existing, first updates the label with header type,
         * and then context menu is returned.
         *
         * @param {String} type
         *  Type of the header for which we are creating menu.
         *
         * @returns {jQuery}
         *  Context menu for header.
         */
        function getContextHeaderMenu(type) {
            if (!contextHeaderMenu) {
                contextHeaderMenu = createContextMenu(type, true);
            } else {
                contextHeaderMenu.find('.marginal-label').text(generateMarginalNodeTypeText(type));
            }
            return contextHeaderMenu;
        }

        /**
         * Gets the context menu node for footer.
         * If it's not created yet, it is first created.
         * If already existing, first updates the label with footer type,
         * and then context menu is returned.
         *
         * @param {String} type
         *  Type of the header for which we are creating menu.
         *
         * @returns {jQuery}
         *  Context menu for footer.
         */
        function getContextFooterMenu(type) {
            if (!contextFooterMenu) {
                contextFooterMenu = createContextMenu(type);
            } else {
                contextFooterMenu.find('.marginal-label').text(generateMarginalNodeTypeText(type));
            }
            return contextFooterMenu;
        }

        /**
         * Helper function to generate label for context menu.
         * It makes lookup in translations dictionary.
         *
         * @param {String} type
         *  Type of header or footer for which label is generated.
         *
         * @returns {String}
         *  Generated label for header or footer.
         *
         */
        function generateMarginalNodeTypeText(type) {
            if (evenOddPages && !(/first/i).exec(type)) {
                if (type === 'header_default') {
                    return translations.header_odd;
                } else if (type === 'footer_default') {
                    return translations.footer_odd;
                }
            }

            return translations[type];
        }

        /**
         * Inserts context menu into active header or footer node.
         *
         * @param {jQuery} $node
         *  Active header or footer node, in which we are inserting context menu.
         *
         * @param {Boolean} isHeader
         *  Wether it is header or footer.
         *
         * @param {String} type
         *  Type of the header or footer node (default, first, even).
         *
         */
        function insertContextMenu($node, isHeader, type) {
            if (!type) {
                pageLogger.error('pageLayout.insertContextMenu: type is missing!');
                return;
            }
            if (DOM.isMarginalPlaceHolderNode($node.parent())) {
                pageLogger.warn('pageLayout.insertContextMenu: trying to insert context menu on template node!');
                return;
            }

            if (isHeader) {
                getContextHeaderMenu(type).insertAfter($node);
            } else {
                getContextFooterMenu(type).insertBefore($node);
            }
        }

        /**
         * Local helper to replace active header/footer node with coresponding new node on implToggle functions.
         *
         * @param {Bolean} [removeAll]
         *  If set to true, no replacing with new node, but leaving edit state.
         */
        function replaceActiveNodeOnToggle(removeAll) {
            var $rootNode,
                id = null;

            replaceAllTypesOfHeaderFooters();

            if (docModel.isHeaderFooterEditState()) {
                if (removeAll) {
                    self.leaveHeaderFooterEditMode();
                    selection.setNewRootNode(docModel.getNode()); // restore original rootNode
                    selection.setTextSelection(selection.getFirstDocumentPosition());
                } else {
                    $rootNode = docModel.getHeaderFooterRootNode();
                    id = $rootNode.attr('data-container-id');
                    if (id) {
                        docModel.setActiveTarget(id);
                        $rootNode.children('.cover-overlay').remove();
                        // browser specific handling
                        if (_.browser.IE) {
                            _.each($rootNode.find('span'), function (span) {
                                DOM.ensureExistingTextNode(span);
                            });
                        }
                        insertContextMenu($rootNode, $rootNode.hasClass('header'), self.getTypeFromId(id));
                    } else {
                        // this is the case when remove all headers&footers is triggered, but no default nodes, see 43817
                        self.leaveHeaderFooterEditMode();
                        selection.setNewRootNode(docModel.getNode()); // restore original rootNode
                        selection.setTextSelection(selection.getFirstDocumentPosition());
                    }
                }
            }
        }

        /**
         * Local helper to refresh collected nodes children with replacement children,
         * and if necessary, trigger drawingHeight update.
         *
         * @param {jQuery} collectionNodes
         *  Nodes that are updated with replacement children.
         *
         * @param {jQuery} $replacementChildren
         *  Replacement nodes.
         *
         * @param {jQuery} $coverOverlay
         *  Overlay node that covers inactive header/footer nodes, also needs to be appended.
         */
        function refreshCollectionNodes(collectionNodes, $replacementChildren, $coverOverlay) {
            var $drawingFrameNodes = null;
            var $clonedReplChildren = $replacementChildren.not('.cover-overlay').clone(true);

            collectionNodes.empty().append($coverOverlay, $clonedReplChildren);
            $drawingFrameNodes = collectionNodes.find(NODE_SELECTOR);
            if ($drawingFrameNodes.length) {
                docModel.trigger('drawingHeight:update', $drawingFrameNodes);
            }
        }

        /**
         * On toggling types of h/f, if dropdown is open, hide it for rerender.
         * If toogling is triggered from menu, and not context menu,
         * can happen that edit mode of h/f is not active. Then jump to header on page where cursor is.
         */
        function prepareMarginalDropdown() {
            var activeDropdown;

            if (!docModel.isHeaderFooterEditState()) {
                self.insertHeader();
            } else {
                activeDropdown = docModel.getCurrentRootNode().parent().find('.dropdown-open');
                if (activeDropdown.length) {
                    self.toggleMarginalMenuDropdown(activeDropdown); // if dropdown is active, hide it before re-render
                }
            }
        }

        /**
         * Local helper to traverse over page break row nodes, and to fetch previous or next valid row node.
         */
        function getPrevNextRowSibling(node, direction) {
            while (node.length && DOM.isTablePageBreakRowNode(node)) {
                node = direction === 'next' ? node.next() : node.prev();
            }
            return node;
        }

        /**
         * Checking, whether the modification for OT must be used or if the 'classic'
         * single user mode can be used.
         * The single user mode helps to keep the code simpler and can be used, if
         * external operations are blocked because of long running local processes.
         * Using 'classic' pagelayout handling in long running processes, for example
         * during resolving a changetracks (DOCS-1641).
         * But undo/redo needs the new OT handling (maybe this should be modified
         * later, too).
         *
         * Within pagelayout.js this local function isOTMode() must be used instead
         * of the global function docApp.isOTEnabled().
         *
         * @returns {Boolean}
         *  Whether the OT specific code changes need to be used.
         */
        function isOTMode() {
            return docApp.isOTEnabled() && (!docModel.isProcessingInternalOperations() || docModel.isUndoRedoRunning() || docModel.isInactiveLocalHeaderFooterOp());
        }

        // methods ------------------------------------------------------------

        /**
         * Updates page by inserting page layout.
         *
         * @param {Object} [options]
         *
         * @returns {promise}
         *
         */
        this.callInitialPageBreaks = function (options) {
            return initialPageBreaks(options);
        };

        /**
         * Selector that queries all headers&footers curently present in document.
         *
         * @returns {jQuery}
         */
        this.getallHeadersAndFooters = function () {
            return $pageContentNode.find('.header, .footer').add($firstHeaderWrapper.children('.header')).add($lastFooterWrapper.children('.footer'));
        };

        /**
         * Selector that queries all headers curently present in document.
         *
         * @returns {jQuery}
         */
        this.getAllHeaders = function () {
            return $pageContentNode.find('.header').add($firstHeaderWrapper.children('.header'));
        };

        /**
         * Selector that queries all footers curently present in document.
         *
         * @returns {jQuery}
         */
        this.getAllFooters = function () {
            return $pageContentNode.find('.footer').add($lastFooterWrapper.children('.footer'));
        };

        /**
         * Queries and returns template node for storing headers and footers.
         *
         * @returns {jQuery}
         */
        this.getHeaderFooterPlaceHolder = function () {
            return $headerFooterPlaceHolder;
        };

        /**
         * If header/footer template node is empty or it contains headers and footers.
         *
         * @returns {Boolean}
         */
        this.hasContentHeaderFooterPlaceHolder = function () {
            return $headerFooterPlaceHolder.children().length > 0;
        };

        /**
         * Checks if first header wrapper node is already created or not.
         *
         * @returns {Boolean}
         */
        this.isExistingFirstHeaderWrapper = function () {
            return $firstHeaderWrapper.length > 0;
        };

        /**
         * Checks if last footer wrapper node is already created or not.
         *
         * @returns {Boolean}
         */
        this.isExistingLastFooterWrapper = function () {
            return $lastFooterWrapper.length > 0;
        };

        /**
         * Returns first header wrapper node in the document. (One right above pagecontent node)
         *
         * @returns {jQuery}
         */
        this.getFirstHeaderWrapperNode = function () {
            return $firstHeaderWrapper;
        };

        /**
         * Returns last footer wrapper node in the document. (One right bellow pagecontent node)
         *
         * @returns {jQuery}
         */
        this.getLastFooterWrapperNode = function () {
            return $lastFooterWrapper;
        };

        /**
         * Return first in collection of marginal nodes in document, with passed id
         *
         * @param {String} id
         *  Id of node we search.
         * @returns {jQuery}
         *  Found node in document.
         */
        this.getFirstMarginalByIdInDoc = function (id) {
            return docModel.getNode().find('.header, .footer').filter('[data-container-id="' + id + '"]').first();
        };

        /**
         * Public method to check if first header in document exist, and if does, return it's wrapper's height (in px).
         * If element exists, returns height of wrapper element, of first header in document (which is prepended to pagecontent node)
         * If element doesn't exist, returs 0.
         *
         * @returns {Number}
         */
        this.getHeightOfFirstHeaderWrapper = function () {
            return $firstHeaderWrapper ? $firstHeaderWrapper.height() : 0;
        };

        /**
         * If there are headers/footers in document.
         *
         * @returns {Boolean}
         */
        this.isHeaderFooterInDocument = function () {
            return $headerFooterPlaceHolder.children().length > 0;
        };

        /**
         * If there are headers in document.
         *
         * @returns {Boolean}
         */
        this.isHeaderInDocument = function () {
            return $headerFooterPlaceHolder.children('.header').length > 0;
        };

        /**
         * If there are footers in document.
         *
         * @returns {Boolean}
         */
        this.isFooterInDocument = function () {
            return $headerFooterPlaceHolder.children('.footer').length > 0;
        };

        /**
         * If headers/footers are activated in document.
         *
         * @returns {Boolean}
         */
        this.isHeaderFooterActivated = function () {
            return marginalActivated;
        };

        /**
         * Gets the header's or footer's id from given type.
         *
         * @param {String} type
         *  Type of header or footer - can be default, first, or even.
         *
         * @returns {String}
         *  Returns found id, or empty string
         *
         */
        this.getIdFromType = function (type) {
            if (type) {
                var obj = _.findWhere(headerFooterCollection, { headFootType: type });

                if (obj && obj.id) {
                    return obj.id;
                }
            }
            return '';
        };

        /**
         * Looks up the headerFooterCollection, and returns type of object for given Id.
         *
         * @param {String} operationId
         *
         * @returns {Opt<String>}
         *  If found, searched type.
         */
        this.getTypeFromId = function (operationId) {
            var $obj = _.findWhere(headerFooterCollection, { id: operationId });

            if ($obj && $obj.headFootType) {
                return $obj.headFootType;
            }

            pageLogger.warn('pageLayout.getTypeFromId: type for given id not found!');
            return undefined;
        };

        /**
         * Looks up the headerFooterCollection, and checks if given id belongs to marginal nodes collection.
         *
         * @param {String} id
         *  Queried id.
         * @returns {Boolean}
         *  Whether passed id belongs to marginal nodes.
         */
        this.isIdOfMarginalNode = function (id) {
            return !_.isUndefined(_.findWhere(headerFooterCollection, { id }));
        };

        /**
         * Mark paragraph and all it's children as marginal.
         *
         * @param {Node} paragraph
         *
         * @param {String} target
         */
        this.markParagraphAsMarginal = function (paragraph, target) {
            if (self.isIdOfMarginalNode(target)) {
                $(paragraph).addClass(DOM.MARGINAL_NODE_CLASSNAME).find('*').addClass(DOM.MARGINAL_NODE_CLASSNAME);
            }
        };

        /**
         * Public accessor for updating style of template node for header&footer.
         *
         */
        this.updateStartHeaderFooterStyle = function () {
            updateStartHeaderFooterStyle();
        };

        /**
         * Creates generic placeholder for the first header in the document.
         *
         * @returns {jQuery}
         *  Created placeholder header node, after insertion in DOM
         */
        this.generateHeaderElementHtml = function () {
            var
                firstHeaderWrapper = $('<div>').addClass('header-wrapper').attr('contenteditable', (_.browser.WebKit ? 'false' : ''))
                    .css({ width: pageWidth, margin: '0px 0px 0px ' + (-pagePaddingLeft) + 'px' }).
                    append(getHeaderHtmlContent('first'));

            $pageContentNode.before(firstHeaderWrapper);

            return firstHeaderWrapper;
        };

        /**
         * Creates generic placeholder for last footer in the document.
         *
         * @returns {jQuery}
         *  Created placeholder footer node, after insertion in DOM
         */
        this.generateFooterElementHtml = function () {
            var
                lastFooterWrapper = $('<div>').addClass('footer-wrapper').attr('contenteditable', (_.browser.WebKit ? 'false' : ''))
                    .css({ width: pageWidth, margin: '0px 0px 0px ' + (-pagePaddingLeft) + 'px' }).
                    append(getFooterHtmlContent());

            $pageContentNode.after(lastFooterWrapper);

            return lastFooterWrapper;
        };

        /**
         * Impl function triggered when INSERT_HEADER_FOOTER operation is called
         *
         * @param {String} operationId
         *
         * @param {String} operationType
         *
         * @param {Object} attributes
         *
         * @param {Boolean} undoRedoRunning - if undo or redo is running, h/f needs to be updated, so that changes are not lost
         *
         * @param {Boolean} external - whether this is an external operation
         *
         * @returns {Boolean}
         *  True if header or footer is succesfully created in DOM and collection, otherwise false
         *
         */
        this.implCreateHeaderFooter = function (operationId, operationType, attributes, undoRedoRunning, external) {
            var isHeader = (/header/i).test(operationType),
                localCollection,
                $paragraph,
                minHeight,
                distanceBottom,
                distanceTop,
                nodeHeight,
                savedRootNode,
                // the current rootnode of the user receiving an external operation
                externalRootNode;

            // saving the current root node, when an external operation for insertHeader/Footer is received
            if (this.isImportFinished() && external) { externalRootNode = rootNode; }

            if (selection.isDrawingSelection()) {
                selection.clearDrawingSelection(selection.getSelectedDrawing()); // 66648
            }

            localCollection = {
                headFootType: operationType,
                id: operationId,
                node: (isHeader ? createHeaderContainer(operationId, operationType) : createFooterContainer(operationId, operationType))
            };
            if ($headerFooterPlaceHolder.length > 0) {
                $headerFooterPlaceHolder.append(localCollection.node);
            } else {
                $headerFooterPlaceHolder = $('<div>').addClass('header-footer-placeholder').attr('contenteditable', _.browser.WebKit ? 'false' : '').css('display', SMALL_DEVICE ? 'none' : '').append(localCollection.node);
                rootNode.append($headerFooterPlaceHolder);
            }
            headerFooterCollection.push(localCollection);
            if (operationId.match(/\d+/)) {
                targetIdNumbersCollection.push(parseInt(operationId.match(/\d+/), 10));
                targetIdNumbersCollection.sort(function (a, b) { return a - b; });
            }
            marginalActivated = true;

            if (docApp.isODF()) {
                if ((/first/i).test(operationType)) {
                    firstPage = true;
                } else if ((/even/i).test(operationType)) {
                    evenOddPages = true;
                }
                if (attributes && attributes.page) {
                    minHeight = attributes.page.minHeight ? convertHmmToLength(attributes.page.minHeight, 'px', 0.001) : 0;
                    distanceBottom = attributes.page.marginBottom ? convertHmmToLength(attributes.page.marginBottom, 'px', 0.001) : 0;
                    distanceTop = attributes.page.marginTop ? convertHmmToLength(attributes.page.marginTop, 'px', 0.001) : 0;
                    if (isHeader) {
                        if ((/first/i).test(operationType)) {
                            odfFirstHeader.height = minHeight;
                            odfFirstHeader.bottom = distanceBottom;
                        } else if ((/even/i).test(operationType)) {
                            odfEvenHeader.height = minHeight;
                            odfEvenHeader.bottom = distanceBottom;
                        } else {
                            odfDefHeader.height = minHeight;
                            odfDefHeader.bottom = distanceBottom;
                        }
                    } else {
                        if ((/first/i).test(operationType)) {
                            odfFirstFooter.height = minHeight;
                            odfFirstFooter.top = distanceTop;
                        } else if ((/even/i).test(operationType)) {
                            odfEvenFooter.height = minHeight;
                            odfEvenFooter.top = distanceTop;
                        } else {
                            odfDefFooter.height = minHeight;
                            odfDefFooter.top = distanceTop;
                        }
                    }
                }

            }

            if (this.isImportFinished()) {
                $paragraph = DOM.getMarginalContentNode(docModel.getRootNode(operationId)).children('.p');
                if (!$paragraph.length) {
                    $paragraph = $('<div>').addClass('p marginal').data('implicit', true);
                    DOM.getMarginalContentNode(docModel.getRootNode(operationId)).append($paragraph);
                }
                docModel.validateParagraphNode($paragraph);

                // height of inserted node needs to be stored, and active page height updated
                nodeHeight = (localCollection.node).outerHeight(true);
                if (operationType === 'header_first') {
                    heightOfFirstHeader = Math.max(pagePaddingTop, nodeHeight);
                    firstPageHeight = pageHeight - heightOfFirstHeader - Math.max(pagePaddingBottom, heightOfFirstFooter);
                } else if (operationType === 'header_even') {
                    heightOfEvenHeader = Math.max(pagePaddingTop, nodeHeight);
                    evenPageHeight = pageHeight - heightOfEvenHeader - Math.max(pagePaddingBottom, heightOfEvenFooter);
                } else if (operationType === 'header_default') {
                    heightOfDefHeader = Math.max(pagePaddingTop, nodeHeight);
                    defPageHeight = pageHeight - heightOfDefHeader - Math.max(pagePaddingBottom, heightOfDefFooter);
                } else if (operationType === 'footer_first') {
                    heightOfFirstFooter = Math.max(pagePaddingBottom, nodeHeight);
                    firstPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfFirstHeader) - heightOfFirstFooter;
                } else if (operationType === 'footer_even') {
                    heightOfEvenFooter = Math.max(pagePaddingBottom, nodeHeight);
                    evenPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfEvenHeader) - heightOfEvenFooter;
                } else {
                    heightOfDefFooter = Math.max(pagePaddingBottom, nodeHeight);
                    defPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfDefHeader) -  heightOfDefFooter;
                }

                if (!undoRedoRunning) {
                    // replacing of all types of h/f, in case of undo/redo happens at the end,
                    // on triggering undo:after event
                    replaceAllTypesOfHeaderFooters();
                }
            }

            // at the end mark all nodes with marginal class, in both current node and placeholder.
            // it is important that placeholder node is also processed, because there can be situation where updateEditingHeaderFooter is not triggered.
            if (!this.isImportFinished()) { savedRootNode = rootNode; } // saving root node during loading phase (65989)
            rootNode = docModel.getRootNode(operationId); // setting new rootNode in impl-handler -> only possible because the active root node is saved in 'externalRootNode'
            markMarginalNodesInsideHeadFoot(rootNode);
            if (!DOM.isMarginalPlaceHolderNode(rootNode.parent())) {
                $headerFooterPlaceHolder.children('.' + operationType).empty().append(rootNode.children().clone(true));
                if (!undoRedoRunning) { replaceAllTypesOfHeaderFooters(); }
            }
            if (savedRootNode) { rootNode = savedRootNode; } // restore the root node during loading phase

            if (docApp.isOOXML()) {
                // change the padding of the root node after creation;
                // OOXML feature not to use headerMargin or footerMargin when no header/footer is in the document
                var cssPadding = isHeader ? { paddingTop: headerMargin } : { paddingBottom: footerMargin };
                var nodesCollection = isHeader ? self.getAllHeaders() : self.getAllFooters();
                nodesCollection.css(cssPadding);
            }
            //markMarginalNodesInsideHeadFoot($headerFooterPlaceHolder.children('.' + type));

            if (external && this.isImportFinished()) { // 66982, for the remote clients in OT- and none-OT-mode

                // restoring the previous root node for external operations (DOCS-3112)
                if (externalRootNode) { rootNode = externalRootNode; }

                docModel.one('operations:success', function () {
                    // format all drawings of the new inserted header/footer (especially after undo, 66982)
                    var pageRootNode = docModel.getNode().find('.header, .footer').filter('[data-container-id="' + operationId + '"]').first();
                    _.each(pageRootNode.find('div.drawing'), function (drawing) { docModel.drawingStyles.updateElementFormatting($(drawing)); });
                });
            }

            return true;
        };

        /**
         * Function triggered when operation DELETE_HEADER_FOOTER is called
         *
         * @param {String} operationId
         *
         * @returns {Boolean}
         *  True if header or footer is succesfully removed from DOM and collection, otherwise false.
         *
         */
        this.implDeleteHeaderFooter = function (operationId) {

            if (_.findWhere(headerFooterCollection, { id: operationId }).length < 1) {
                pageLogger.warn('pageLayout.deleteHeaderFooter(): no object found with passed Id');
                return false;
            }

            var $node = docModel.getRootNode(operationId),
                pos = null,
                type = self.getTypeFromId(operationId),
                className = null,
                $nodeParent = $node.parent();

            if (selection.isDrawingSelection()) {
                selection.clearDrawingSelection(selection.getSelectedDrawing()); // 66648
            }

            // updating the models for comments (only ODT), range markers and complex fields. This
            // is not necessary for drawings, because header and footer have their own drawing layer.
            docModel.updateCollectionModels($node, { marginal: true });

            if (docModel.containsUnrestorableElements($node)) {
                docModel.setDeleteUndoStack(true);
            }

            if (docModel.isHeaderFooterEditState()) {
                self.updateEditingHeaderFooter({ givenNode: selection.getRootNode() });
                self.leaveHeaderFooterEditMode();
                selection.setNewRootNode(docModel.getNode()); // restore original rootNode
            }

            if (type) {
                className = '.' + _.first(type.split('_'));
                if (docApp.isODF()) {
                    if ((/first/i).test(type)) {
                        firstPage = false;
                    } else if ((/even/i).test(type)) {
                        evenOddPages = false;
                    }
                }
            } else {
                pageLogger.warn('pageLayout.implDeleteHeaderFooter(): unknown type of node!');
            }

            if (className) {
                $pageContentNode.add($firstHeaderWrapper).add($lastFooterWrapper).find(className).filter('[data-container-id="' + operationId + '"]').removeAttr('data-container-id').empty();
            }

            $headerFooterPlaceHolder.children('[data-container-id="' + operationId + '"]').remove();
            headerFooterCollection = headerFooterCollection.filter(function (el) {
                return el.id !== operationId;
            });
            if (_.isEmpty(headerFooterCollection)) {
                marginalActivated = false;
            }

            // set selection
            if (DOM.isMarginalPlaceHolderNode($nodeParent)) {
                selection.setTextSelection(selection.getFirstDocumentPosition());
            } else {
                selection.setNewRootNode(docModel.getNode());

                if ($nodeParent.hasClass('header-wrapper')) {
                    $nodeParent = $nodeParent.next().children().first();
                } else if ($nodeParent.hasClass('footer-wrapper')) {
                    $nodeParent = $nodeParent.prev().children().last();
                } else if ($nodeParent.hasClass('page-break')) {
                    if ($nodeParent.parent().is('td')) {
                        $nodeParent = $nodeParent.closest('tr');
                        if (getPrevNextRowSibling($nodeParent, 'next').length) {
                            $nodeParent = getPrevNextRowSibling($nodeParent, 'next').find('.p').first();
                        } else {
                            $nodeParent = $nodeParent.closest('table').next().find('.p').first();
                        }
                    } else {
                        $nodeParent = $nodeParent.next();
                    }
                }
                if ($nodeParent.length) {
                    pos = getFirstPositionInParagraph(docModel.getNode(), getOxoPosition(docModel.getNode(), $nodeParent, 0));
                    if (_.isArray(pos)) {
                        selection.setTextSelection(pos);
                    }
                } else {
                    selection.setTextSelection(selection.getFirstDocumentPosition());
                }
            }

            // height of deleted node needs to reset, to get accurate page height
            if (type === 'header_first') {
                heightOfFirstHeader = pagePaddingTop;
                firstPageHeight = pageHeight - heightOfFirstHeader - Math.max(pagePaddingBottom, heightOfFirstFooter);
            } else if (type === 'header_even') {
                heightOfEvenHeader = pagePaddingTop;
                evenPageHeight = pageHeight - heightOfEvenHeader - Math.max(pagePaddingBottom, heightOfEvenFooter);
            } else if (type === 'header_default') {
                heightOfDefHeader = pagePaddingTop;
                defPageHeight = pageHeight - heightOfDefHeader - Math.max(pagePaddingBottom, heightOfDefFooter);
            } else if (type === 'footer_first') {
                heightOfFirstFooter = pagePaddingBottom;
                firstPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfFirstHeader) - heightOfFirstFooter;
            } else if (type === 'footer_even') {
                heightOfEvenFooter = pagePaddingBottom;
                evenPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfEvenHeader) - heightOfEvenFooter;
            } else {
                heightOfDefFooter = pagePaddingBottom;
                defPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfDefHeader) -  heightOfDefFooter;
            }

            // repaint layout debounced
            docModel.insertPageBreaks();

            return true;
        };

        /**
         * This method is called after user pressed button to insert header on current page.
         * If header is already created in the document, it will just jump into it.
         *
         */
        this.insertHeader = function () {
            var $node,
                pageNum = self.getPageNumber(),
                options = { calledFromCreate: true },
                headerType = 'header_default',
                footerType = 'footer_default';

            if (pageNum === 1) {
                $node = $firstHeaderWrapper.children('.header');
                if (firstPage) {
                    headerType = 'header_first';
                    footerType = 'footer_first';
                }
            } else {
                $node = $(pageBreaksCollection[pageNum - 2]).children('.header');
                if (evenOddPages && (pageNum % 2 === 0)) {
                    headerType = 'header_even';
                    footerType = 'footer_even';
                }
            }
            if (docModel.isHeaderFooterEditState()) {
                self.updateEditingHeaderFooter();
                self.leaveHeaderFooterEditMode();
                selection.setNewRootNode(docModel.getNode()); // restore original rootNode
            }
            if (!$node.attr('data-container-id')) {
                storeNodeForHeaderFooterCreation($node);
                createOperationInsertHeaderFooter($node.hasClass('header') ? headerType : footerType);

                //replaceAllTypesOfHeaderFooters();
                $node = getNodeStoredOnHeaderFooterInsertion();
                storeNodeForHeaderFooterCreation(null); // node is used, clean reference
                if (!$node || !$node.length) {
                    $node = docModel.getCurrentRootNode();
                }
                self.enterHeaderFooterEditMode($node, options);
                selection.setNewRootNode($node);
                selection.setTextSelection(selection.getFirstDocumentPosition());
            } else {
                self.enterHeaderFooterEditMode($node);
                selection.setNewRootNode($node);
                selection.resetSelection();
                selection.setTextSelection(selection.getFirstDocumentPosition());
            }
        };

        /**
         * This method is called after user pressed button to insert footer on current page.
         * If footer is already created in the document, it will just jump into it.
         */
        this.insertFooter = function () {
            var $node,
                numPages = self.getNumberOfDocumentPages(),
                pageNum = self.getPageNumber(),
                options = { calledFromCreate: true },
                headerType = 'header_default',
                footerType = 'footer_default';

            if (pageNum === numPages) {
                $node = $lastFooterWrapper.children('.footer');
            } else {
                $node = $(pageBreaksCollection[pageNum - 1]).children('.footer');
            }
            if (firstPage && pageNum === 1) {
                headerType = 'header_first';
                footerType = 'footer_first';
            }
            if (evenOddPages && (pageNum % 2 === 0)) {
                headerType = 'header_even';
                footerType = 'footer_even';
            }

            if (docModel.isHeaderFooterEditState()) {
                self.updateEditingHeaderFooter();
                self.leaveHeaderFooterEditMode();
                selection.setNewRootNode(docModel.getNode()); // restore original rootNode
            }
            if (!$node.attr('data-container-id')) {
                storeNodeForHeaderFooterCreation($node);
                createOperationInsertHeaderFooter($node.hasClass('footer') ? footerType : headerType);

                //replaceAllTypesOfHeaderFooters();
                $node = getNodeStoredOnHeaderFooterInsertion();
                storeNodeForHeaderFooterCreation(null); // node is used, clean reference
                if (!$node || !$node.length) {
                    $node = docModel.getCurrentRootNode();
                }
                self.enterHeaderFooterEditMode($node, options);
                selection.setNewRootNode($node);
                selection.setTextSelection(selection.getFirstDocumentPosition());
            } else {
                self.enterHeaderFooterEditMode($node);
                selection.setNewRootNode($node);
                selection.resetSelection();
                selection.setTextSelection(selection.getFirstDocumentPosition());
            }
        };

        /**
         * Method that removes active header or footer, where cursor is currently placed.
         * It creates delete operation, and calls it's impl function deleteHeaderFooter.
         * After removing, cursor is returned to main document.
         *
         */
        this.removeActiveHeaderFooter = function () {
            var $node = docModel.getHeaderFooterRootNode(),
                localGenerator = docModel.createOperationGenerator(),
                target = $node.attr('data-container-id'),
                operation = { id: target };

            // whether it is required to check for range markers inside the selection
            var  isRangeCheckRequired = docApp.isODF() && !docModel.getRangeMarker().isEmpty();
            // all classes used to identify the range markers
            var allRangeMarkerSelector = isRangeCheckRequired ? DOM.RANGEMARKERNODE_SELECTOR : null;
            // the collector for all range marker nodes (including comments)
            var allRangeMarkerNodes = $();
            // a container for all IDs of removed comments (OT)
            var allRemovedCommentIDs = [];

            localGenerator.generateOperation(DELETE_HEADER_FOOTER, operation);

            // handling of removed comments, only ODT (required for OT)
            if (isRangeCheckRequired) {
                if (allRangeMarkerSelector && !docModel.getCommentLayer().isEmpty()) { allRangeMarkerSelector += ', ' + DOM.COMMENTPLACEHOLDER_NODE_SELECTOR; }
                allRangeMarkerNodes = $node.find(allRangeMarkerSelector);
            }

            if (allRangeMarkerNodes.length > 0) {
                _.each(allRangeMarkerNodes, function (node) { if (DOM.isCommentPlaceHolderNode(node)) { allRemovedCommentIDs.push(DOM.getTargetContainerId(node)); } });
            }

            docModel.applyOperations(localGenerator);
        };

        /**
         * Method that removes all headers and footers from document.
         *
         */
        this.removeAllHeadersFooters = function () {
            var localGenerator = docModel.createOperationGenerator();
            var askUser = false;

            // whether it is required to check for range markers inside the selection
            var  isRangeCheckRequired = docApp.isODF() && !docModel.getRangeMarker().isEmpty();
            // all classes used to identify the range markers
            var allRangeMarkerSelector = isRangeCheckRequired ? DOM.RANGEMARKERNODE_SELECTOR : null;
            // the collector for all range marker nodes (including comments)
            var allRangeMarkerNodes = $();
            // a container for all IDs of removed comments (OT)
            var allRemovedCommentIDs = [];

            if (!headerFooterCollection.length) {
                return;
            }
            if (docModel.isHeaderFooterEditState()) {
                self.updateEditingHeaderFooter();
                self.leaveHeaderFooterAndSetCursor(docModel.getCurrentRootNode());
            }

            if (docApp.isODF()) {
                firstPage = false;
                evenOddPages = false;
                if (allRangeMarkerSelector && !docModel.getCommentLayer().isEmpty()) { allRangeMarkerSelector += ', ' + DOM.COMMENTPLACEHOLDER_NODE_SELECTOR; }
            }

            _.each(headerFooterCollection, function (element) {
                localGenerator.generateOperation(DELETE_HEADER_FOOTER, { id: element.id });
                if (!askUser && docModel.containsUnrestorableElements(element.node)) {
                    askUser = true;
                }

                // handling of removed comments, only ODT (required for OT)
                if (isRangeCheckRequired) { allRangeMarkerNodes = $(element.node).find(allRangeMarkerSelector); }

                if (allRangeMarkerNodes.length > 0) {
                    _.each(allRangeMarkerNodes, function (node) { if (DOM.isCommentPlaceHolderNode(node)) { allRemovedCommentIDs.push(DOM.getTargetContainerId(node)); } });
                }

            });

            if (askUser) {
                docModel.askUserHandler(true, false).then(function () {
                    docModel.applyOperations(localGenerator);
                    // inform listeners, that the marginal nodes were removed
                    docModel.trigger('removed:marginal');
                });
            } else {
                docModel.applyOperations(localGenerator);
                // inform listeners, that the marginal nodes were removed
                docModel.trigger('removed:marginal');
            }

        };

        /**
         * On document loading, using fast-load, references to created nodes in placeholder have to be updated.
         * Function is also used to update references after content update.
         *
         */
        this.headerFooterCollectionUpdate = function () {
            var
                placeholderChildren,
                elementData,
                localCollection,
                $element;

            if (docApp.isODF() && !this.isImportFinished()) {
                // clean-up possible nodes with same type, this is possible in ODF, see #43889
                var flags = {};
                var collectionUpdateReq = false;
                _.each($headerFooterPlaceHolder.children(), function (el) {
                    if (!flags[$(el).data('type')]) {
                        flags[$(el).data('type')] = true;
                    } else {
                        $(el).remove();
                        collectionUpdateReq = true;
                    }
                });
                if (collectionUpdateReq) {
                    fieldManager.refreshSimpleFieldsCollection();
                }
            }

            headerFooterCollection = [];
            targetIdNumbersCollection = [];
            placeholderChildren = $headerFooterPlaceHolder.children();

            _.each(placeholderChildren, function (element) {
                var minHeight,
                    distanceBottom,
                    distanceTop,
                    isHeader,
                    elemChildren;

                $element = $(element);
                elementData = $element.data();

                if (_.isEmpty(elementData)) {
                    pageLogger.warn('pageLayout.headerFooterCollectionUpdate(): Empty object for:', $element);
                    return;
                } else if (_.isEmpty(elementData.type)) {
                    pageLogger.warn('pageLayout.headerFooterCollectionUpdate(): Empty Type for:', $element);
                    return;
                }
                if (!$element.children('.cover-overlay').length) {
                    $element.prepend(createCoverOverlay());
                }
                if (!DOM.getMarginalContentNode($element).length) {
                    $element.append($('<div>').addClass(DOM.MARGINALCONTENT_NODE_CLASSNAME));
                }
                elemChildren = $element.children('.p, table');
                if (elemChildren.length) {
                    elemChildren.detach().appendTo(DOM.getMarginalContentNode($element));
                }

                isHeader = (/header/i).test(elementData.type);

                localCollection = {
                    headFootType: elementData.type,
                    id: elementData.id,
                    node: $element.attr('data-container-id', elementData.id)
                };
                headerFooterCollection.push(localCollection);
                if (elementData.id.match(/\d+/)) {
                    targetIdNumbersCollection.push(parseInt(elementData.id.match(/\d+/), 10));
                }
                if (docApp.isODF()) {
                    if ((/first/i).test(elementData.type)) {
                        firstPage = true;
                    } else if ((/even/i).test(elementData.type)) {
                        evenOddPages = true;
                    }

                    if (elementData.attributes && elementData.attributes.page) {
                        minHeight = elementData.attributes.page.minHeight ? convertHmmToLength(elementData.attributes.page.minHeight, 'px', 0.001) : 0;
                        distanceBottom = elementData.attributes.page.marginBottom ? convertHmmToLength(elementData.attributes.page.marginBottom, 'px', 0.001) : 0;
                        distanceTop = elementData.attributes.page.marginTop ? convertHmmToLength(elementData.attributes.page.marginTop, 'px', 0.001) : 0;
                        if (isHeader) {
                            if ((/first/i).test(elementData.type)) {
                                odfFirstHeader.height = minHeight;
                                odfFirstHeader.bottom = distanceBottom;
                            } else if ((/even/i).test(elementData.type)) {
                                odfEvenHeader.height = minHeight;
                                odfEvenHeader.bottom = distanceBottom;
                            } else {
                                odfDefHeader.height = minHeight;
                                odfDefHeader.bottom = distanceBottom;
                            }
                        } else {
                            if ((/first/i).test(elementData.type)) {
                                odfFirstFooter.height = minHeight;
                                odfFirstFooter.top = distanceTop;
                            } else if ((/even/i).test(elementData.type)) {
                                odfEvenFooter.height = minHeight;
                                odfEvenFooter.top = distanceTop;
                            } else {
                                odfDefFooter.height = minHeight;
                                odfDefFooter.top = distanceTop;
                            }
                        }
                    }
                }
            });
            targetIdNumbersCollection.sort(function (a, b) { return a - b; });

            // for local storage and fast load, enable flag if document is loaded with h/f
            if (!this.isImportFinished() && headerFooterCollection.length) {
                marginalActivated = true;
            }

        };

        /**
         * Returns information to controller, which state is currently set in document:
         *  - HF with first page different,
         *  - HF with even and odd pages different,
         *  - HF same across all document
         *  - no HF in document
         *
         * @returns {String}
         */
        this.getHeaderFooterTypeInDoc = function () {
            if (docApp.isODF()) {
                if (firstPage && evenOddPages) {
                    return 'all';
                } else if (firstPage) {
                    return 'first';
                } else if (evenOddPages) {
                    return 'evenodd';
                } else if (self.isHeaderFooterActivated()) {
                    return 'default';
                } else {
                    return 'none';
                }
            } else {
                if (self.getPageAttribute('firstPage') && self.getPageAttribute('evenOddPages')) {
                    return 'all';
                } else if (self.getPageAttribute('firstPage')) {
                    return 'first';
                } else if (self.getPageAttribute('evenOddPages')) {
                    return 'evenodd';
                } else if (self.isHeaderFooterActivated()) {
                    return 'default';
                } else {
                    return 'none';
                }
            }
        };

        /**
         * Sets the state of header&footer in document. Called from controller and context menu.
         *
         * @param {String} value
         *  Deppending on value passed, different h/f types in document.
         */
        this.setHeaderFooterTypeInDoc = function (value) {
            switch (value) {
                case 'none':
                    docModel.getUndoManager().enterUndoGroup(function () {
                        if (firstPage) {
                            self.toggleFirstPageHF(false, { removeAll: true });
                        }
                        if (evenOddPages) {
                            self.toggleEvenOddHF(false, { removeAll: true });
                        }
                        self.removeAllHeadersFooters();
                    });
                    break;
                case 'default':
                    docModel.getUndoManager().enterUndoGroup(function () {
                        prepareMarginalDropdown();

                        if (firstPage) {
                            self.toggleFirstPageHF(false);
                        }
                        if (evenOddPages) {
                            self.toggleEvenOddHF(false);
                        }
                    });
                    break;
                case 'first':
                    docModel.getUndoManager().enterUndoGroup(function () {
                        prepareMarginalDropdown();

                        if (evenOddPages) {
                            self.toggleEvenOddHF(false);
                        }
                        self.toggleFirstPageHF(true);
                    });
                    break;
                case 'evenodd':
                    docModel.getUndoManager().enterUndoGroup(function () {
                        prepareMarginalDropdown();

                        if (firstPage) {
                            self.toggleFirstPageHF(false);
                        }
                        self.toggleEvenOddHF(true);
                    });
                    break;
                case 'all':
                    docModel.getUndoManager().enterUndoGroup(function () {
                        prepareMarginalDropdown();

                        self.toggleEvenOddHF(true);
                        self.toggleFirstPageHF(true);
                    });
                    break;
                case 'goto':
                    self.insertHeader();
                    break;

                default:
                    self.insertHeader();
            }
        };

        /**
         * Gets deep copy the header or footer node's children from placeholder.
         *
         * @param {String} type
         *  Type of header/footer - can be default, first, or even.
         *
         * @returns {jQuery|Node}
         *  Returns copy of found node's children, or empty jQuery object
         *
         */
        this.getHeaderFooterChildrenFromPlaceholder = function (type) {
            if (type) {
                var obj = _.findWhere(headerFooterCollection, { headFootType: type });

                if (obj && obj.node) {
                    return (obj.node).children().clone(true);
                }
            }
            return $();
        };

        /**
         * Called from editor's function this.getRootNode, when target is header or footer type.
         * Returns found node, depending of document loading, remote client and passed index in editdiv.
         *
         * @param {String} target
         *
         * @param {Number} index
         *
         * @returns {jQuery}
         */
        this.getRootNodeTypeHeaderFooter = function (target, index) {
            var $localNode = null;

            function hfRootNodeWithIndex() {
                $localNode = docModel.getNode().find('.header, .footer').filter('[data-container-id="' + target + '"]').eq(index);
                if (!$localNode.length) {
                    pageLogger.warn('editor.getRootNode(): node with given index not found, fallback to first in document.');
                    $localNode = docModel.getNode().find('.header, .footer').filter('[data-container-id="' + target + '"]').first();
                }
            }

            if (isOTMode() && docApp.isEditable()) {

                if (this.isImportFinished() && !docModel.isProcessingExternalOperations()) {
                    if (docModel.isHeaderFooterEditState() && docModel.getActiveTarget() === target) {
                        $localNode = docModel.getCurrentRootNode(target);
                    } else if (_.isNumber(index)) {
                        hfRootNodeWithIndex();
                    } else if (docModel.isUndoRedoRunning() || docModel.isInactiveLocalHeaderFooterOp()) {
                        // using the placeholder for undo/redo, avoiding restored content after deleting on page and entering header/footer edit mode
                        $localNode = $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').first();
                    } else {
                        $localNode = docModel.getNode().find('.header, .footer').filter('[data-container-id="' + target + '"]').first();
                    }
                } else {
                    // remote client, only for external operations
                    if (this.isImportFinished() && docModel.isProcessingExternalOperations() && docModel.isHeaderFooterEditState() && docModel.getActiveTarget() === target) {
                        $localNode = docModel.getCurrentRootNode(target); // special case: The local client edits the same header/footer
                    } else if (_.isNumber(index)) {
                        hfRootNodeWithIndex();
                    } else {
                        $localNode = $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').first();
                    }
                }

            } else {

                if (this.isImportFinished() && docApp.isEditable()) {
                    if (docModel.isHeaderFooterEditState() && docModel.getActiveTarget() === target) {
                        $localNode = docModel.getCurrentRootNode(target);
                    } else if (_.isNumber(index)) {
                        hfRootNodeWithIndex();
                    } else {
                        $localNode = docModel.getNode().find('.header, .footer').filter('[data-container-id="' + target + '"]').first();
                    }
                } else {
                    // remote client
                    if (_.isNumber(index)) {
                        hfRootNodeWithIndex();
                    } else {
                        $localNode = $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').first();
                    }
                }
            }

            return $localNode;
        };

        /**
         * Gets number of the page where passed node, logical position, or selection start belongs to.
         *
         * @param {jQuery|Node|Array<Number>} [position]
         *  Can accept node, logical position as array, or no argument (then
         *  fetches element's selection where cursor is positioned)
         *
         * @returns {Number}
         *  The number of the page where node is located. If no valid node is
         *  fetched, returns 0.
         */
        this.getPageNumber = function (position) {
            var firstPageBreakAbove,
                parentSplit,
                pb,
                traverseUp = false,
                pageNumber = 1,
                helperNode = null;

            if (!position) {
                position = selection.getStartPosition();
            }

            if (_.isArray(position)) {
                position = getDOMPosition(docModel.getCurrentRootNode(), position);
                helperNode = position ? position.node : null;
                if (helperNode && helperNode.nodeType === 3) {
                    position = helperNode.parentNode;
                }
            }

            if ($(position).length === 0) {
                pageLogger.warn('Unknown position of element!');
                pageNumber = 0;
                return pageNumber;
            }
            if (!pageBreaksCollection || pageBreaksCollection.length === 0) {
                pageNumber = 1;
                return pageNumber;
            }

            firstPageBreakAbove = $(position);

            // check if element is inside header or footer
            if (DOM.isMarginalNode(firstPageBreakAbove)) {
                helperNode = firstPageBreakAbove.parentsUntil('.page', '.header, .footer');
                if (helperNode.length) {
                    firstPageBreakAbove = helperNode;
                }
            }

            // if header or footer is passed, calculate page num directly, otherwise traverse up to get to first element on page (bellow pagebreak)
            if (firstPageBreakAbove.hasClass('header')) {
                parentSplit = firstPageBreakAbove.parent();
                if (parentSplit.hasClass('header-wrapper')) {
                    pageNumber = 1;
                } else {
                    pageNumber = pageBreaksCollection.index(parentSplit) + 2;
                }
                return pageNumber;
            } else if (firstPageBreakAbove.hasClass('footer')) {
                parentSplit = firstPageBreakAbove.parent();
                if (parentSplit.hasClass('footer-wrapper')) {
                    pageNumber = pageBreaksCollection.length + 1;
                } else {
                    pageNumber =  pageBreaksCollection.index(parentSplit) + 1;
                }
                return pageNumber;
            } else {
                while (firstPageBreakAbove.length > 0 && !firstPageBreakAbove.hasClass('page-break')) { // fetching first page break element above passed element to fetch its index
                    if (DOM.nodeContainsPageBreak(firstPageBreakAbove) || firstPageBreakAbove.hasClass('tb-split-nb')) { // first level node contains page break inside itself
                        firstPageBreakAbove = getLastPageBreakInNode(firstPageBreakAbove);
                        if (firstPageBreakAbove.length < 1) {
                            pageNumber = null;
                            return pageNumber;
                        }
                        if (traverseUp) {
                            pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 2) : 1;
                        } else {
                            pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 1) : 1;
                        }

                        return pageNumber;
                    } else {
                        if (firstPageBreakAbove.parent().hasClass('pagecontent')) { // first level node
                            firstPageBreakAbove = firstPageBreakAbove.prev();
                            traverseUp = true;
                        } else {
                            if (firstPageBreakAbove.parentsUntil('.page', '.pagecontent').length) {
                                parentSplit = firstPageBreakAbove.parentsUntil('.pagecontent').last();
                                pb = parentSplit.find('.page-break').first();
                            } else {
                                parentSplit = $();
                                pb = $();
                            }

                            if (DOM.nodeContainsPageBreak(parentSplit)) { // parent node is paragraph split by page break
                                while (firstPageBreakAbove.length > 0 && !firstPageBreakAbove.hasClass('page-break')) { // search above
                                    firstPageBreakAbove = firstPageBreakAbove.prev();
                                }
                                if (firstPageBreakAbove.length < 1) { // if none found, page break is bellow element
                                    if (pb.length < 1) {
                                        pageNumber = null;
                                        return pageNumber;
                                    }
                                    firstPageBreakAbove = pb;
                                    pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 1) : 1;
                                    return pageNumber;
                                }

                                pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 2) : 1;
                                return pageNumber;
                            } else if (parentSplit.hasClass('tb-split-nb')) {  // parent node is table split by page break
                                if (!firstPageBreakAbove.is('tr')) {
                                    firstPageBreakAbove = firstPageBreakAbove.parentsUntil('.pagecontent', 'tr').last();
                                }
                                while (firstPageBreakAbove.length > 0 && !firstPageBreakAbove.hasClass('pb-row')) { // search above
                                    firstPageBreakAbove = firstPageBreakAbove.prev();
                                }
                                if (firstPageBreakAbove.length < 1) { // if none found, page break is bellow element
                                    firstPageBreakAbove = pb;
                                    if (pb.length < 1) {
                                        pageNumber = null;
                                        return pageNumber;
                                    }

                                    pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 1) : 1;
                                    return pageNumber;
                                }
                                firstPageBreakAbove = firstPageBreakAbove.find('.page-break');

                                pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 2) : 1;
                                return pageNumber;
                            } else if (parentSplit.hasClass('page-break') && firstPageBreakAbove.length > 0) {
                                if (firstPageBreakAbove.parentsUntil(parentSplit, '.header').length || firstPageBreakAbove.hasClass('header')) {
                                    pageNumber = pageBreaksCollection.index(parentSplit) + 2;
                                } else if (firstPageBreakAbove.parentsUntil(parentSplit, '.footer').length || firstPageBreakAbove.hasClass('footer')) {
                                    pageNumber =  pageBreaksCollection.index(parentSplit) + 1;
                                }

                                return pageNumber;
                            } else if (parentSplit.length === 0 && firstPageBreakAbove.length > 0) {
                                if (firstPageBreakAbove.parentsUntil('.page', '.header-wrapper').length || firstPageBreakAbove.hasClass('header-wrapper')) { // first page header
                                    pageNumber = 1;
                                    return pageNumber;
                                } else if (firstPageBreakAbove.parentsUntil('.page', '.footer-wrapper').length || firstPageBreakAbove.hasClass('footer-wrapper')) { // last page footer
                                    pageNumber = pageBreaksCollection.length + 1;
                                    return pageNumber;
                                }
                            }

                            firstPageBreakAbove = parentSplit.prev();
                            traverseUp = true;
                        }
                    }
                } // end of while
            }
            pageNumber = firstPageBreakAbove.length > 0 ? (pageBreaksCollection.index(firstPageBreakAbove) + 2) : 1;
            return pageNumber;
        };

        /**
         * Method to get number of pages in whole document.
         *
         * @returns {Number}
         *
         */
        this.getNumberOfDocumentPages = function () {
            return pageCount;
        };

        /**
         * Method to get first element (line inside page break) on demanded page number.
         *
         * @param {Number} pageNumber
         *  Number of page for which we want begining of page element.
         *
         * @returns {jQuery|Node|Boolean}
         *  If found, it returns line node, if not, returns boolean false or null (if first page is demanded).
         *
         */
        this.getBeginingElementOnSpecificPage = function (pageNumber) {
            if (!_.isNumber(pageNumber) || pageNumber < 1 || pageNumber % 1 !== 0) {
                return false;
            }
            if (pageBreaksCollection.length === 0 || pageNumber === 1) {
                return null;
            }
            var $element = $(pageBreaksCollection[pageNumber - 2]).find(DOM.PAGE_BREAK_LINE_SELECTOR);

            if (!$element || $element.length < 1) {
                pageLogger.warn('Page doesn\'t exist!');
                return false;
            }

            return $element;

        };

        /**
         * Method for getting index of passed node from collection of all nodes with that target in whole document.
         * Collection is sorted from top to bottom of document.
         *
         * @param {jQuery} $node
         *
         * @param {String} target
         *
         * @returns {Number}
         *  Index (cardinal number) of passed node from collection of all nodes with that target in whole document,
         *  top to bottom; or -1 if no node index is found.
         */
        this.getTargetsIndexInDocument = function ($node, target) {
            return target ? docModel.getNode().find('[data-container-id="' + target + '"]').index($node) : -1;
        };

        /**
         * If parent container doesn't have style "position:relative",
         * safest way is to calculate difference of position.top,
         * between element and his parent.
         *
         * @returns {Number}
         *  Difference in px between top edge of element and top edge of it's parent.
         *
         */
        this.getDistanceTopRelativeToParent = function (element) {
            var $element = $(element),
                $parent = $element.parent();

            return ($parent.length > 0)  ? ($element.position().top - $parent.position().top) : 0;
        };

        /**
         * Getter method for active height in 1/100 mm, of default page (without margins) defined in page attributes
         *
         *  @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.convertToPixel=true]
         *      If set to true, the page height is calculated in pixel. Otherwise
         *      the returned value is in 1/100 mm.
         *
         * @returns {Number}
         *  The height of the page.
         */
        this.getDefPageActiveHeight = function (options) {
            var // whether the page height shall be returned in pixel
                convertToPixel =  getBooleanOption(options, 'convertToPixel', false);

            if (!pageAttributes) { // #35499 - if updateDocumentFormatting is not executed (start new empty doc), initialization of pageAttributes is required
                initializePageAttributes();
            }

            return convertToPixel ? (pageHeight - pagePaddingTop - pagePaddingBottom) : pageAttributes.page.height - pageAttributes.page.marginTop - pageAttributes.page.marginBottom;
        };

        /**
         * Getter method for page attributes
         *
         * @param {String} property
         *  The property for that the value shall be returned
         *
         * @returns {Number}
         *  The value for the specified page property.
         */
        this.getPageAttribute = function (property) {

            if (!pageAttributes) {
                initializePageAttributes();
            }

            return pageAttributes && pageAttributes.page && pageAttributes.page[property];
        };

        /**
         * Public function that updates value of field, with type page count,
         * based on frontend pageCount calculus.
         *
         * @param {jQuery|Node} field
         *  Field whose value is going to be updated.
         *
         * @param {String} [format]
         *  Optional - format of the field.
         *
         */
        this.updatePageCountField = function (field, format) {
            var tempFormat = format || DOM.getFieldInstruction(field);

            $(field.childNodes).empty().html(fieldManager.formatFieldNumber(pageCount, tempFormat, true));
        };

        /**
         * Public function that updates value of field, with type page number,
         * based on frontend function getPageNumber.
         *
         * @param {jQuery|Node} field
         *  Field whose value is going to be updated.
         *
         * @param {String} [format]
         *  Optional - format of the field.
         *
         * @param {Number} [num]
         *  Optional - a number that is used as (slide) page number (DOCS-4916)
         */
        this.updatePageNumberField = function (field, format, num) {
            var tempFormat = format || DOM.getFieldInstruction(field),
                pageNumber = _.isNumber(num) ? num : self.getPageNumber(field);

            $(field).children().empty().html(fieldManager.formatFieldNumber(pageNumber, tempFormat));
        };

        /**
         * Callback function for mouseenter event on user type fields.
         * Creates popup with name of user defined field.
         *
         * @param {jQuery.Event} event
         */
        this.createTooltipForField = function (event) {
            var
                $fieldNode = $(event.currentTarget),
                name = $fieldNode.data('name') || '',
                $marginalNode = null,
                isMarginal = DOM.isMarginalNode($fieldNode),
                contentNodeSelector = isMarginal ? '.marginalcontent' : '.pagecontent',
                zoomFactor = docApp.getView().getZoomFactor(),
                tooltipTopPos = ($fieldNode.parentsUntil(contentNodeSelector).last().position().top + $fieldNode.position().top + $fieldNode.children().position().top) / zoomFactor,
                tooltipLeftPos = (event.clientX - rootNode.offset().left) / zoomFactor,
                $tooltip = $('<div>').addClass('field-tooltip-box').attr('contenteditable', 'false').text(escapeHTML(name));

            // fastload stores different data structure of attributes
            if (name.length === 0 && _.isObject($fieldNode.data('attributes')) && _.isObject($fieldNode.data('attributes').field) && $fieldNode.data('attributes').field.name) {
                $tooltip.text($fieldNode.data('attributes').field.name);
            }

            if (document.documentElement.clientHeight - event.clientY - $fieldNode.children().height() * zoomFactor < 30 * zoomFactor) {
                // tooltip goes above field
                tooltipTopPos -= ($tooltip.outerHeight() + 15);
            } else {
                tooltipTopPos += $fieldNode.children().height();
            }
            if (isMarginal) {
                $marginalNode = $fieldNode.closest('.header, .footer');
                if (DOM.isHeaderNode($marginalNode) && !DOM.isHeaderWrapper($marginalNode.parent())) {
                    tooltipTopPos += $marginalNode.position().top;
                }
                $tooltip.css('z-index', 41); // to go over marginal context menu, which has z-40
            }

            $tooltip.css({ top: tooltipTopPos, left: tooltipLeftPos });

            if (isMarginal) {
                $marginalNode.after($tooltip); // attach after header or footer node
                $marginalFieldTooltip = $tooltip;
            } else {
                if ($tooltipOverlay.length === 0) {
                    createTooltipOverlay();
                }
                $tooltipOverlay.append($tooltip);
            }
        };

        /**
         * Callback function for mouseleave event on user type fields.
         * Removes popup with name of user defined field.
         *
         */
        this.destroyTooltipForField = function () {
            $tooltipOverlay.empty();
            if ($marginalFieldTooltip) {
                $marginalFieldTooltip.remove();
                $marginalFieldTooltip = null;
            }
        };

        /**
         * Current header or footer that we are editing, needs to be updated in collection also.
         * If height of element is changed, update this value also, it is needed for correct page height calculus.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preventUpdateMarginal=false]
         *      If set to true, update of marginal nodes won't be triggered,
         *      to prevent endless looping when called from updateDrawing
         *  @param {Object|jQuery} [options.givenNode=null]
         *      If exists, this passed node is used as marginal node for update.
         *      It is set only from insertDrawingSpaceMaker, and for normal use, shouldn't be passed directly,
         *      but as docModel.updateEditingHeaderFooterDebounced.
         */
        this.updateEditingHeaderFooter = function (options) {
            pageLogger.takeTime('pageLayout.updateEditingHeaderFooter(): ', function () {
                function breakExecutionWithoutNode() {
                    if (!$node || !$node.length) {
                        pageLogger.error('pageLayout.updateEditingHeaderFooter(): no editing node provided or fetched!');
                        return true;
                    }
                }

                var givenNode = getObjectOption(options, 'givenNode', null);
                var $node = (givenNode && $(givenNode)) || docModel.getHeaderFooterRootNode();
                if (breakExecutionWithoutNode()) { return; }
                var target = $node.attr('data-container-id') || ''; // if at this point, target is already removed, we fetch it from node
                var obj = _.findWhere(headerFooterCollection, { id: target });
                if (!obj) { return; }
                if (docModel.isHeaderFooterEditState() && target === docModel.getActiveTarget() && givenNode && $(givenNode)[0] === obj.node[0]) { // #62381
                    pageLogger.warn('pageLayout.updateEditingHeaderFooter(): tried to fetch placeholder node while editing is active');
                    $node = docModel.getHeaderFooterRootNode();
                    if (breakExecutionWithoutNode()) { return; }
                }
                var $replacementChildren = null;
                var nodeHeight = $node.outerHeight(true);
                var $coverOverlay = createCoverOverlay();
                var collectionNodes, type = '', selectorHF = '';
                var repaintLayout = false;
                var drawingNode = null;
                // variables used for possible search results
                var invokeSearch = false;
                var searchSpans = null;
                var searchValue = '';
                var preventUpdateMarginal = getBooleanOption(options, 'preventUpdateMarginal', false);

                var isDebounced = getBooleanOption(options, 'isDebounced', false);

                var isInvalidEditState = false;

                // checking that an editable node in the page (not below the place holder) is no longer active.
                // Then it might have been modified in the meantime by external operations (test 28)
                if (isOTMode() && isDebounced && !docModel.isHeaderFooterEditState() && givenNode && getDomNode(givenNode) !== getDomNode($headerFooterPlaceHolder)) {
                    isInvalidEditState = true; // this specified node cannot be used anymore (but its content was already transferred during leaveHeaderFooterEditMode)
                }

                if (docModel.isClipboardPasteInProgress()) {
                    // #38528 - Don't run update if paste is in progress, just deffer it.
                    self.executeDelayed(function () {
                        self.updateEditingHeaderFooter(options);
                    });
                    return;
                }

                if (docModel.isUndoRedoRunning() || docModel.isInactiveLocalHeaderFooterOp()) { // TODO for DOCS-1641: || docModel.getBlockKeyboardEvent()) {
                    // #49211 - Don't run update if undo/redo is in progress, just deffer it.
                    self.executeDelayed(function () {
                        self.updateEditingHeaderFooter(options);
                    });
                    return;
                }

                if (DOM.isInsideHeaderFooterTemplateNode(docModel.getNode(), $node)) {
                    replaceAllTypesOfHeaderFooters(); // TODO: This is not clear in OT environment (causes task 66982 that requires repair)
                    return;
                }

                if (obj && obj.headFootType) {
                    type = obj.headFootType;
                    selectorHF = (type.indexOf('header') > -1) ? '.header' : '.footer';
                }

                if (!docModel.isHeaderFooterEditState()) {
                    switch (type) {
                        case 'header_first':
                            if (Math.abs(nodeHeight - heightOfFirstFooter) > 1) {
                                heightOfFirstHeader = Math.max(pagePaddingTop, (odfFirstHeader.height + pagePaddingTop), nodeHeight);
                                firstPageHeight = pageHeight - heightOfFirstHeader - Math.max(pagePaddingBottom, heightOfFirstFooter);
                                repaintLayout = true;
                            }
                            break;
                        case 'header_even':
                            if (Math.abs(nodeHeight - heightOfEvenHeader) > 1) {
                                heightOfEvenHeader = Math.max(pagePaddingTop, (odfEvenHeader.height + pagePaddingTop), nodeHeight);
                                evenPageHeight = pageHeight - heightOfEvenHeader - Math.max(pagePaddingBottom, heightOfEvenFooter);
                                repaintLayout = true;
                            }
                            break;
                        case 'header_default':
                            if (Math.abs(nodeHeight - heightOfDefHeader) > 1) {
                                heightOfDefHeader = Math.max(pagePaddingTop, (odfDefHeader.height + pagePaddingTop), nodeHeight);
                                defPageHeight = pageHeight - heightOfDefHeader - Math.max(pagePaddingBottom, heightOfDefFooter);
                                repaintLayout = true;
                            }
                            break;
                        case 'footer_first':
                            if (Math.abs(nodeHeight - heightOfFirstFooter) > 1) {
                                heightOfFirstFooter = Math.max(pagePaddingBottom, (odfFirstFooter.height + pagePaddingBottom), nodeHeight);
                                firstPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfFirstHeader) - heightOfFirstFooter;
                                repaintLayout = true;
                            }
                            break;
                        case 'footer_even':
                            if (Math.abs(nodeHeight - heightOfEvenFooter) > 1) {
                                heightOfEvenFooter = Math.max(pagePaddingBottom, (odfEvenFooter.height + pagePaddingBottom), nodeHeight);
                                evenPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfEvenHeader) - heightOfEvenFooter;
                                repaintLayout = true;
                            }
                            break;
                        case 'footer_default':
                            if (Math.abs(nodeHeight - heightOfDefFooter) > 1) {
                                heightOfDefFooter = Math.max(pagePaddingBottom, (odfDefFooter.height + pagePaddingBottom), nodeHeight);
                                defPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfDefHeader) -  heightOfDefFooter;
                                repaintLayout = true;
                            }
                            break;
                        default:
                            pageLogger.warn('pagelayout.updateEditingHeaderFooter(): Unknown type of Header-Footer!');
                            return;
                    }
                }

                markMarginalNodesInsideHeadFoot($node);

                // find highlighted search words in header/footer, and clean them before cloning to template node
                searchSpans = $node.find('.highlight');
                if (searchSpans.length) {
                    invokeSearch = true;
                    searchValue = searchSpans.first().text();
                    searchSpans.removeClass('highlight');

                    docModel.getSearchHandler().removeSpansHighlighting(searchSpans);
                }
                // remove selection from drawing before cloning to template container (#53818)
                drawingNode = selection.getSelectedDrawing();
                if (drawingNode && drawingNode.length) { clearSelection(drawingNode); }

                if (isInvalidEditState) {
                    $replacementChildren = $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').children().clone(true);
                } else {
                    $replacementChildren = $node.children().clone(true); // using the specified node to transfer the data
                    $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').empty().append($replacementChildren);
                }

                // update refrence in collection because of replaced node
                self.headerFooterCollectionUpdate();

                if (repaintLayout && !docModel.isHeaderFooterEditState() && !preventUpdateMarginal) {
                    docModel.insertPageBreaks();
                    // we need to update first header, or last footer in document, because they are not handled with insertPageBreaks function
                    if (selectorHF === '.header') {
                        collectionNodes = $firstHeaderWrapper.children('[data-container-id="' + target + '"]').not('.active-selection');
                    } else {
                        collectionNodes = $lastFooterWrapper.children('[data-container-id="' + target + '"]').not('.active-selection');
                    }
                    refreshCollectionNodes(collectionNodes, $replacementChildren, $coverOverlay);
                } else { // just replace header-footer content
                    collectionNodes = $pageContentNode.add($firstHeaderWrapper).add($lastFooterWrapper).find(selectorHF).not('.active-selection').filter('[data-container-id="' + target + '"]');
                    refreshCollectionNodes(collectionNodes, $replacementChildren, $coverOverlay);

                    if (!fieldManager.isComplexFieldEmpty()) { fieldManager.updatePageFieldsInMarginals(); }
                }
                // invoke search again to refresh detached and invalid references
                if (invokeSearch) { docModel.getSearchHandler().quickSearch(searchValue, null); }
                // restore selection of the drawing node
                if (drawingNode && drawingNode.length) { selection.drawDrawingSelection(drawingNode); }

                // inform listeners about modified header and footer in the case that header or footer are no longer active
                if (!docModel.isHeaderFooterEditState() && !preventUpdateMarginal) {
                    self.trigger('updateMarginal:leave', { targetNode: $node });
                }

                if (selection.hasRange()) { selection.restoreBrowserSelection(); } // restoring the browser selection (DOCS-3116)
            });
        };

        /**
         * Method to trigger update of template header/footer node with changes in first node inside main document.
         * Also distribute changes to all other nodes of same type.
         * It is necessary to trigger this update after updateAbsoluteDrawings make changes in node.
         *
         * @param {Array|HTMLElement|jQuery} nodesCollection
         *  List of unique, first appearence header/footer nodes inside main document,
         *  that contains spacemaker for absolute positioned drawings.
         *  Currently, up to 6 elements can be in array (3 unique header, and 3 footer types).
         *
         */
        this.updateDataFromFirstNodeInDoc = function (nodesCollection) {
            pageLogger.takeTime('pageLayout.updateDataFromFirstNodeInDoc(): ', function () {
                var $node = null,
                    $replacementChildren = null,
                    $coverOverlay = createCoverOverlay(),
                    collectionNodes,
                    nodeHeight = 0,
                    target = '',
                    obj = null,
                    type = '',
                    repaintLayout = false;

                if (nodesCollection && nodesCollection.length) {
                    _.each(nodesCollection, function (node) {
                        $node = $(node);
                        nodeHeight = $node.outerHeight(true);
                        target = $node.attr('data-container-id') || '';
                        // first check if node is not detached from DOM;
                        // If it is, find new one in DOM with same target
                        if (!$node.parent().length) {
                            $node = docModel.getRootNode(target);
                        }
                        obj = _.findWhere(headerFooterCollection, { id: target });

                        if (obj && obj.headFootType) {
                            type = obj.headFootType;
                        }

                        if (!docModel.isHeaderFooterEditState()) {
                            switch (type) {
                                case 'header_first':
                                    if (nodeHeight !== heightOfFirstHeader) {
                                        heightOfFirstHeader = Math.max(pagePaddingTop, (odfFirstHeader.height + pagePaddingTop), nodeHeight);
                                        firstPageHeight = pageHeight - heightOfFirstHeader - Math.max(pagePaddingBottom, heightOfFirstFooter);
                                        repaintLayout = true;
                                    }
                                    break;
                                case 'header_even':
                                    if (nodeHeight !== heightOfEvenHeader) {
                                        heightOfEvenHeader = Math.max(pagePaddingTop, (odfEvenHeader.height + pagePaddingTop), nodeHeight);
                                        evenPageHeight = pageHeight - heightOfEvenHeader - Math.max(pagePaddingBottom, heightOfEvenFooter);
                                        repaintLayout = true;
                                    }
                                    break;
                                case 'header_default':
                                    if (nodeHeight !== heightOfDefHeader) {
                                        heightOfDefHeader = Math.max(pagePaddingTop, (odfDefHeader.height + pagePaddingTop), nodeHeight);
                                        defPageHeight = pageHeight - heightOfDefHeader - Math.max(pagePaddingBottom, heightOfDefFooter);
                                        repaintLayout = true;
                                    }
                                    break;
                                case 'footer_first':
                                    if (nodeHeight !== heightOfFirstFooter) {
                                        heightOfFirstFooter = Math.max(pagePaddingBottom, (odfFirstFooter.height + pagePaddingBottom), nodeHeight);
                                        firstPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfFirstHeader) - heightOfFirstFooter;
                                        repaintLayout = true;
                                    }
                                    break;
                                case 'footer_even':
                                    if (nodeHeight !== heightOfEvenFooter) {
                                        heightOfEvenFooter = Math.max(pagePaddingBottom, (odfEvenFooter.height + pagePaddingBottom), nodeHeight);
                                        evenPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfEvenHeader) - heightOfEvenFooter;
                                        repaintLayout = true;
                                    }
                                    break;
                                case 'footer_default':
                                    if (nodeHeight !== heightOfDefFooter) {
                                        heightOfDefFooter = Math.max(pagePaddingBottom, (odfDefFooter.height + pagePaddingBottom), nodeHeight);
                                        defPageHeight = pageHeight - Math.max(pagePaddingTop, heightOfDefHeader) -  heightOfDefFooter;
                                        repaintLayout = true;
                                    }
                                    break;
                                default:
                                    pageLogger.warn('pagelayout.updateDataFromFirstNodeInDoc(): Unknown type of Header-Footer!');
                                    return;
                            }
                        }

                        markMarginalNodesInsideHeadFoot($node);

                        if ($node.length > 0) {
                            $replacementChildren = $node.children().clone(true);
                            $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').empty().append($replacementChildren);
                            // update refrence in collection because of replaced node
                            self.headerFooterCollectionUpdate();
                        }
                        if (!repaintLayout) {
                            collectionNodes = $pageContentNode.add($firstHeaderWrapper).add($lastFooterWrapper).find('.header, .footer').not('.active-selection').filter('[data-container-id="' + target + '"]');
                            refreshCollectionNodes(collectionNodes, $replacementChildren, $coverOverlay);

                            if (!fieldManager.isComplexFieldEmpty()) { fieldManager.updatePageFieldsInMarginals(); }
                        }
                    });
                    if (repaintLayout && !docModel.isHeaderFooterEditState()) {
                        docModel.insertPageBreaks();
                        // we need to update first header, or last footer in document, because they are not handled with insertPageBreaks function
                        if (type.indexOf('header') > -1) {
                            collectionNodes = $firstHeaderWrapper.children('[data-container-id="' + target + '"]').not('.active-selection');
                        } else {
                            collectionNodes = $lastFooterWrapper.children('[data-container-id="' + target + '"]').not('.active-selection');
                        }
                        refreshCollectionNodes(collectionNodes, $replacementChildren, $coverOverlay);
                    }

                } else {
                    pageLogger.warn('pageLayout.updateDataFromFirstNodeInDoc(): Passed empty collection of nodes as argument.');
                }
            });
        };

        /**
         * Debounced callback method to update content of all cloned nodes of repeated table header row.
         * Called from 'paragraphUpdate:after'
         *
         * @param {Node} node
         */
        this.updateRepeatedTableRows = function (node) {
            var tableCell = $(node).closest(DOM.TABLE_CELLNODE_SELECTOR);
            var tableRow = tableCell.parent();
            var clonedContent = tableCell.children().clone();
            var tbCellIndex = tableCell.index();
            var table = tableCell.closest(DOM.TABLE_NODE_SELECTOR);

            if (tableRow && DOM.isRepeatRowNode(tableRow)) {
                _.each(table.find(DOM.TABLE_REPEATED_ROWNODE_SELECTOR), function (repeatedRow) {
                    $(repeatedRow).children('td').eq(tbCellIndex).empty().append(clonedContent.clone());
                });
            }
        };

        /**
         * Method to jump into necessary edit mode of header/footer, from main document programatically.
         *
         * @param {jQuery} $node
         *  Header or footer root node
         *
         * @param {Object} [options]
         *  @param {Boolean} [options.calledFromCreate]
         *   Whether this function was called after creating the header/footer node.
         *  @param {Boolean} [options.keepNodes]
         *   Whether the header/footer must not be exchanged with the placeholder node (OT).
         *
         */
        this.enterHeaderFooterEditMode = function ($node, options) {
            var nodeType,
                $marginalContent,
                target = $node.attr('data-container-id'),
                updateRequired = false,
                placeHolderContent = null,
                calledFromCreate = getBooleanOption(options, 'calledFromCreate', false),
                keepNodes = getBooleanOption(options, 'keepNodes', false);

            if (!docApp.isEditable()) { return; }

            // if (docModel.isProcessingExternalOperations()) { return; } // OT: TODO (maybe the header is currently edited)

            if (!target) {
                if (calledFromCreate) {
                    pageLogger.warn('pageLayout.enterHeaderFooterEditMode(): tried to call itself recursively!');
                    return;
                }
                nodeType = getHeadFootTypeForCreateOp($node);
                pageLogger.info('pageLayout.enterHeaderFooterEditMode(): Node not existing yet. Creating node with type:', nodeType);
                createOperationInsertHeaderFooter(nodeType);
                updateRequired = true;

            } else if (docModel.isHeaderFooterEditState() && $node.length) {
                if (docModel.getHeaderFooterRootNode()[0] === $node[0]) {
                    pageLogger.info('pageLayout.enterHeaderFooterEditMode(): Already in edit mode with passed node!');
                    return;
                }
                self.leaveHeaderFooterEditMode();
                pageLogger.info('pageLayout.enterHeaderFooterEditMode(): Not the same node as passed is in edit mode!');
            }

            if (isOTMode()) {
                // OT: Immediately synchronously update the header/footer node with the placeholder content, because the corresponding
                //     placeholder might be modified by external operations (increases stability of test 28).
                if (target && !calledFromCreate && !keepNodes) {
                    placeHolderContent = $headerFooterPlaceHolder.children('[data-container-id="' + target + '"]').children().clone(true);
                    if (placeHolderContent && placeHolderContent.length) { $node.empty().append(placeHolderContent); }
                }

                // OT: Immediately format all drawings (otherwise the shapes will be invisible)
                _.each($node.find('div.drawing'), function (drawing) { docModel.drawingStyles.updateElementFormatting($(drawing)); });

                // OT: Update all special fields (otherwise the number from the template will become visible)
                if (!fieldManager.isSimpleFieldEmpty()) { self.updatePageSimpleFields(); }
                if (!fieldManager.isComplexFieldEmpty()) { fieldManager.updatePageFieldsInMarginals(); }
            }

            docModel.setHeaderFooterEditState(true);
            docModel.setActiveTarget($node.attr('data-container-id'));

            // an update is required for new created header/footer because of the inserted paragraph (66741)
            if (updateRequired || calledFromCreate) { self.updateEditingHeaderFooter({ givenNode: $node }); }

            // take care of dom and css
            $node.removeClass('inactive-selection').addClass('active-selection').attr({ contenteditable: true, 'data-gramm': false });
            insertContextMenu($node, $node.hasClass('header'), getHeadFootTypeForCreateOp($node));

            $node.find('[contenteditable="false"]').not('.inline').attr({ contenteditable: true, 'data-gramm': false });
            $node.find('.cover-overlay').remove();
            $marginalContent = DOM.getMarginalContentNode($node);
            // node must have at least implicit paragraph to set cursor inside
            if (!$marginalContent.children().length) {
                $marginalContent.append($('<div>').addClass('p').data('implicit', true));
                docModel.validateParagraphNode($marginalContent.children());
            }
            docModel.getNode().addClass('inactive-mode').removeAttr('contenteditable');

            docModel.setHeaderFooterRootNode($node);

            // browser specific handling
            if (_.browser.IE) {
                _.each($node.find('span'), function (span) {
                    DOM.ensureExistingTextNode(span);
                });

                $node.parent().attr('contenteditable', false);
                $node.find('.cell').attr('contenteditable', false);
                $node.find('.textframecontent').parent().attr('contenteditable', false);
            } else if (_.browser.Firefox) {
                $node.parent().removeAttr('contenteditable');
            }

        };

        /**
         * Method to leave edit state of header/footer that was edited. Css classes are beeing cleaned up.
         *
         * @param {jQuery} [$node]
         *  Header or footer root node
         *
         * @param {jQuery} [$parent]
         *  Parent container of root node
         */
        this.leaveHeaderFooterEditMode = function ($node, $parent) {

            // if (docModel.isProcessingExternalOperations()) { return; } // OT: TODO (maybe the header is currently edited)

            var selectedDrawing = null;
            var footerNode = null;

            function removeMarginalContextMenu(contextMenu) {
                contextMenu.detach();
                contextMenu.find('.marginal-container').hide();
                contextMenu.find('.marginal-menu-type').removeClass('dropdown-open');
            }

            if (docModel.isHeaderFooterEditState()) {
                if (!$node || !$node.length) {
                    $node = docModel.getCurrentRootNode();
                    $parent = $node.parent();
                }
                if (!$parent) {
                    $parent = $node.parent();
                }
                // clear possible leftover selection on drawings and textframes
                if (selection.getSelectionType() === 'drawing' || selection.isAdditionalTextframeSelection()) {
                    selectedDrawing = selection.getSelectedDrawing();
                    if (!selectedDrawing.length) { selectedDrawing =  selection.getSelectedTextFrameDrawing(); }

                    clearSelection(selectedDrawing);
                    // removing additional tracking handler options, that are specific for OX Text (Chrome only)
                    selectedDrawing.removeData('trackingOptions');
                    selectedDrawing.removeData('tracking-options');
                    selectedDrawing.closest('.p').removeClass('visibleAnchor').find('.anchorIcon').remove();
                }
                $node.find('table.selected').removeClass('selected');
                $node.removeClass('active-selection').addClass('inactive-selection').attr('contenteditable', false);
                $node.find('[contenteditable="true"]').attr('contenteditable', false);

                removeMarginalContextMenu($node.hasClass('header') ? contextHeaderMenu : contextFooterMenu);

                $node.prepend(createCoverOverlay());

                if (_.browser.IE || _.browser.Firefox) {  $parent.attr('contenteditable', ''); }
                if (fieldManager.isHighlightState()) { fieldManager.removeEnterHighlight(); }
                if ($marginalFieldTooltip) {
                    $marginalFieldTooltip.remove();
                    $marginalFieldTooltip = null;
                }

                docModel.getNode().removeClass('inactive-mode').attr({ contenteditable: true, 'data-gramm': false });

                // OT: Immediately synchronously update the corresponding placeholder because of external operations
                //     that follow immediately and rely on the placeholder node (Test 28).
                if (isOTMode()) { self.updateEditingHeaderFooter({ givenNode: $node }); } // calling this before target and marker handling

                if (!fieldManager.isSimpleFieldEmpty()) { self.updatePageSimpleFields(); }
                if (!fieldManager.isComplexFieldEmpty()) { fieldManager.updatePageFieldsInMarginals(); }

                // target and marker handling
                docModel.setActiveTarget(null);
                docModel.setHeaderFooterEditState(false);

                // the block for inserting page breaks must be removed (53070)
                if (docModel.getBlockOnInsertPageBreaks()) { docModel.setBlockOnInsertPageBreaks(false); }

                // checking the footer node on Firefox browser. If it has no children, it must get contenteditable 'false' (DOCS-3118)
                if (_.browser.Firefox) {
                    footerNode = docModel.getNode().children('.footer-wrapper').children('.footer.inactive-selection');
                    if (footerNode.length === 1 && footerNode.children().length === 0) { footerNode.attr('contenteditable', false); }
                }

            } else {
                pageLogger.warn('pageLayout.leaveHeaderFooterEditMode(): Not in edit mode currently!');
            }
        };

        /**
         * On leaving header/footer edit state and returning to main document, calculate where to place cursor.
         * If we are leaving header, cursor will be placed in first position on that page, bellow header.
         * If we are leaving footer, cursor will be placed in last postion on that page, above footer.
         *
         * @param {Node|jQuery} activeRootNode
         *  Header or footer node that is currently in active mode, and which we are leaving.
         *
         * @param {Node|jQuery} [parentOfRootNode]
         *  Optional node, parent node of activeRootNode. If not provided, will be fetched inside function.
         *
         * @param {Object} [options]
         * Optional parameters:
         *  @param {Boolean} [options.asyncCursor=false]
         *    If set to true, the selection is set asynchronously after leaving header/footer edit mode.
         */
        this.leaveHeaderFooterAndSetCursor = function (activeRootNode, parentOfRootNode, options) {
            var pos,
                isHeader = activeRootNode.hasClass('header');

            if (!parentOfRootNode) {
                parentOfRootNode = activeRootNode.parent();
            }

            self.leaveHeaderFooterEditMode(activeRootNode, parentOfRootNode);
            if (!isOTMode()) { docModel.updateEditingHeaderFooterDebounced(activeRootNode); }

            selection.setNewRootNode(docModel.getNode()); // restore original rootNode

            // set selection
            if (parentOfRootNode.hasClass('header-wrapper')) {
                parentOfRootNode = parentOfRootNode.next().children().first();
            } else if (parentOfRootNode.hasClass('footer-wrapper')) {
                parentOfRootNode = parentOfRootNode.prev().children().last();
            } else if (parentOfRootNode.hasClass('page-break')) {
                if (parentOfRootNode.parent().is('td')) {
                    parentOfRootNode = parentOfRootNode.closest('tr');
                    if (isHeader) {
                        if (getPrevNextRowSibling(parentOfRootNode, 'next').length) {
                            parentOfRootNode = getPrevNextRowSibling(parentOfRootNode, 'next').find('.p').first();
                        } else {
                            parentOfRootNode = parentOfRootNode.closest('table').next().find('.p').first();
                        }
                    } else {
                        if (getPrevNextRowSibling(parentOfRootNode, 'prev').leng) {
                            parentOfRootNode = getPrevNextRowSibling(parentOfRootNode, 'prev').find('.p').last();
                        } else {
                            parentOfRootNode = parentOfRootNode.closest('table').prev().find('.p').last();
                        }
                    }
                } else {
                    parentOfRootNode = isHeader ? parentOfRootNode.next() : parentOfRootNode.prev();
                }
            }
            if (parentOfRootNode.length) {
                if (DOM.isParagraphNode(parentOfRootNode)) {
                    pos = isHeader ? getFirstPositionInParagraph(docModel.getNode(), getOxoPosition(docModel.getNode(), parentOfRootNode, 0)) : getLastPositionInParagraph(docModel.getNode(), getOxoPosition(docModel.getNode(), parentOfRootNode, 0));
                } else {
                    pos = getOxoPosition(docModel.getNode(), parentOfRootNode, 0);
                }

                if (_.isArray(pos)) {
                    if (options?.asyncCursor) {
                        // after using closer of header/footer, the selection must be set asynchronously (DOCS-4594)
                        self.setTimeout(() => selection.setTextSelection(pos), 200);
                    } else {
                        // after clicking into the main document, the selection can be set immediately
                        selection.setTextSelection(pos);
                    }
                }
            }
        };

        /**
         * Setting the focus into header or footer node after the browser could render the activated header node (DOCS-3232).
         *
         * @param {number} timer1
         *  The time in ms for the first try to set the focus into header/footer.
         *
         * @param {number} timer2
         *  The time in ms for the second try to set the focus into header/footer after the first try failed.
         */
        function setCursorDelayedIntoHeaderFooterNode(timer1, timer2) {
            self.executeDelayed(function () {
                var rootNode = docModel.getCurrentRootNode();
                if (!containsNode(rootNode, getFocus(), { allowSelf: true })) { docApp.getView().grabFocus({ restoreBrowserSelection: true }); }
                self.executeDelayed(function () {
                    if (!containsNode(rootNode, getFocus(), { allowSelf: true })) { docApp.getView().grabFocus({ restoreBrowserSelection: true }); }
                }, timer2); // second try
            }, timer1); // first try
        }

        /**
         * Method to switch edit mode from main document to header,
         * on the page where selection is.
         *
         * @param {Number} [pageNumber]
         *  Already calculated page number where to jump - optional
         */
        this.jumpToHeaderOnCurrentPage = function (pageNumber) {
            var pageNum = pageNumber || self.getPageNumber();

            if (pageNum === 1) {
                self.enterHeaderFooterEditMode($firstHeaderWrapper.children('.header'));
            } else {
                self.enterHeaderFooterEditMode($(pageBreaksCollection[pageNum - 2]).children('.header'));
            }
            selection.resetSelection();
            selection.setTextSelection(selection.getFirstDocumentPosition());
            docApp.getView().scrollToChildNode(docModel.getCurrentRootNode(), { regardSoftkeyboard: true });

            // Setting the focus after the browser could render the activated header node (DOCS-3232)
            setCursorDelayedIntoHeaderFooterNode(100, 200);
        };

        /**
         * Method to switch edit mode from main document to footer,
         * on the page where selection is.
         *
         * @param {Number} [pageNumber]
         *  Already calculated page number where to jump - optional
         */
        this.jumpToFooterOnCurrentPage = function (pageNumber) {
            var numPages = self.getNumberOfDocumentPages(),
                pageNum = pageNumber || self.getPageNumber();

            if (pageNum === numPages) {
                self.enterHeaderFooterEditMode($lastFooterWrapper.children('.footer'));
            } else {
                self.enterHeaderFooterEditMode($(pageBreaksCollection[pageNum - 1]).children('.footer'));
            }
            selection.resetSelection();
            selection.setTextSelection(selection.getFirstDocumentPosition());

            // next element from footer is pb line, because we want whole footer to be displayed
            // but only if is not last footer - which doesn't have pb line next.
            docApp.getView().scrollToChildNode(pageNum === numPages ? docModel.getCurrentRootNode() : docModel.getCurrentRootNode().next(), { regardSoftkeyboard: true });

            // Setting the focus after the browser could render the activated footer node (DOCS-3232)
            setCursorDelayedIntoHeaderFooterNode(100, 200);
        };

        /**
         * Method to programatically jump from one Header/footer to another.
         * Used for highlighting nodes in search, or changetracking.
         *
         * @param {jQuery} $node
         *  Header or footer root node
         */
        this.switchTargetNodeInHeaderMode = function ($node) {
            self.leaveHeaderFooterEditMode();
            //self.updateEditingHeaderFooter();

            self.enterHeaderFooterEditMode($node);
            selection.setNewRootNode($node); // set new selection root node
        };

        /**
         * Public method called when user clicks on checklist for header/footer different on first page.
         *
         * @param {Boolean} state
         *
         * @param {Object} [options]
         * Optional parameters:
         *  @param {Boolean} [options.removeAll=false]
         *      If set to true, this method call is precceding removing all HF in doc,
         *      and therefore there is no need to create default type before removing them all.
         */
        this.toggleFirstPageHF = function (state, options) {
            if (state === firstPage) {
                return;
            }
            var $rootNode = docModel.getHeaderFooterRootNode(),
                isHeader = $rootNode.hasClass('header'),
                activeNodeType = isHeader ? 'header_first' : 'footer_first',
                passiveNodeType = isHeader ? 'footer_first' : 'header_first',
                defNodeType = isHeader ? 'header_default' : 'footer_default',
                $activePlaceholderNode = $headerFooterPlaceHolder.children('.' + activeNodeType),
                $passivePlaceholderNode = $headerFooterPlaceHolder.children('.' + passiveNodeType),
                $defPlaceholderNode = $headerFooterPlaceHolder.children('.' + defNodeType),
                localGenerator = docApp.isODF() ? docModel.createOperationGenerator() : null,
                odfRestoredNodePasive,
                odfRestoredNodeActive,
                removeAll = getBooleanOption(options, 'removeAll', false);

            if (state) {
                if (!$passivePlaceholderNode.length) {
                    if (docApp.isODF()) {
                        odfRestoredNodePasive = savedNodesCollection[passiveNodeType];
                        if (odfRestoredNodePasive) {
                            if (localGenerator) {
                                callHFopODT(odfRestoredNodePasive, passiveNodeType);
                            }
                            delete savedNodesCollection[passiveNodeType];
                        } else {
                            createOperationInsertHeaderFooter(passiveNodeType);
                        }
                    } else {
                        createOperationInsertHeaderFooter(passiveNodeType);
                    }
                }
                if (!$activePlaceholderNode.length) {
                    if (docApp.isODF()) {
                        odfRestoredNodeActive = savedNodesCollection[activeNodeType];
                        if (odfRestoredNodeActive) {
                            if (localGenerator) {
                                callHFopODT(odfRestoredNodeActive, activeNodeType);
                            }
                            delete savedNodesCollection[activeNodeType];
                        } else {
                            createOperationInsertHeaderFooter(activeNodeType);
                        }
                    } else {
                        createOperationInsertHeaderFooter(activeNodeType);
                    }
                }

                if (localGenerator) {
                    docModel.applyOperations(localGenerator);
                }
            } else {
                // on de-checking type of HF, create default if not existing, before placing cursor inside
                if (!$defPlaceholderNode.length && !removeAll) {
                    createOperationInsertHeaderFooter(defNodeType);
                }
            }

            if (docApp.isODF()) {
                firstPage = state;
                if (!state && !removeAll) {
                    if ($activePlaceholderNode.length) {
                        // save node reference to restore it later
                        savedNodesCollection[activeNodeType] = { operations: generateHFopODT($activePlaceholderNode, activeNodeType), id: $activePlaceholderNode.attr('data-container-id') };
                        localGenerator.generateOperation(DELETE_HEADER_FOOTER, { id: $activePlaceholderNode.attr('data-container-id') });
                    }
                    if ($passivePlaceholderNode.length) {
                        // save node reference to restore it later
                        savedNodesCollection[passiveNodeType] = { operations: generateHFopODT($passivePlaceholderNode, passiveNodeType), id: $passivePlaceholderNode.attr('data-container-id') };
                        localGenerator.generateOperation(DELETE_HEADER_FOOTER, { id: $passivePlaceholderNode.attr('data-container-id') });
                    }
                    docModel.applyOperations(localGenerator);
                }
                self.implToggleFirstPageHF(state, { removeAll: true });
            } else {
                docModel.applyOperations({ name: CHANGE_CONFIG, attrs: { page: { firstPage: state } } });
            }
            selection.setTextSelection(selection.getFirstDocumentPosition());
        };

        /**
         * Public method called when user clicks on checklist for headers/footers different on even and odd pages.
         *
         * @param {Boolean} state
         *
         * @param {Object} [options]
         * Optional parameters:
         *  @param {Boolean} [options.removeAll=false]
         *      If set to true, this method call is precceding removing all HF in doc,
         *      and therefore there is no need to create default type before removing them all.
         */
        this.toggleEvenOddHF = function (state, options) {
            if (state === evenOddPages) {
                return;
            }
            var $rootNode = docModel.getHeaderFooterRootNode(),
                isHeader = $rootNode.hasClass('header'),
                activeNodeType = isHeader ? 'header_even' : 'footer_even',
                passiveNodeType = isHeader ? 'footer_even' : 'header_even',
                defNodeType = isHeader ? 'header_default' : 'footer_default',
                $activePlaceholderNode = $headerFooterPlaceHolder.children('.' + activeNodeType),
                $passivePlaceholderNode = $headerFooterPlaceHolder.children('.' + passiveNodeType),
                $defPlaceholderNode = $headerFooterPlaceHolder.children('.' + defNodeType),
                localGenerator = docApp.isODF() ? docModel.createOperationGenerator() : null,
                odfRestoredNodePasive,
                odfRestoredNodeActive,
                removeAll = getBooleanOption(options, 'removeAll', false);

            if (state) {
                if (!$passivePlaceholderNode.length) {
                    if (docApp.isODF()) {
                        odfRestoredNodePasive = savedNodesCollection[passiveNodeType];
                        if (odfRestoredNodePasive) {
                            if (localGenerator) {
                                callHFopODT(odfRestoredNodePasive, passiveNodeType);
                            }
                            delete savedNodesCollection[passiveNodeType];
                        }  else {
                            createOperationInsertHeaderFooter(passiveNodeType);
                        }
                    } else {
                        createOperationInsertHeaderFooter(passiveNodeType);
                    }
                }
                if (!$activePlaceholderNode.length) {
                    if (docApp.isODF()) {
                        odfRestoredNodeActive = savedNodesCollection[activeNodeType];
                        if (odfRestoredNodeActive) {
                            if (localGenerator) {
                                callHFopODT(odfRestoredNodeActive, activeNodeType);
                            }
                            delete savedNodesCollection[activeNodeType];
                        }  else {
                            createOperationInsertHeaderFooter(activeNodeType);
                        }
                    } else {
                        createOperationInsertHeaderFooter(activeNodeType);
                    }
                }

                if (localGenerator) {
                    docModel.applyOperations(localGenerator);
                }
            } else {
                // on de-checking type of HF, create default if not existing, before placing cursor inside
                if (!$defPlaceholderNode.length && !removeAll) {
                    createOperationInsertHeaderFooter(defNodeType);
                }
            }

            if (docApp.isODF()) {
                evenOddPages = state;
                if (!state && !removeAll) {
                    localGenerator = docModel.createOperationGenerator();
                    if ($activePlaceholderNode.length) {
                        savedNodesCollection[activeNodeType] = { operations: generateHFopODT($activePlaceholderNode, activeNodeType), id: $activePlaceholderNode.attr('data-container-id') };
                        localGenerator.generateOperation(DELETE_HEADER_FOOTER, { id: $activePlaceholderNode.attr('data-container-id') });
                    }
                    if ($passivePlaceholderNode.length) {
                        savedNodesCollection[passiveNodeType] = { operations: generateHFopODT($passivePlaceholderNode, passiveNodeType), id: $passivePlaceholderNode.attr('data-container-id') };
                        localGenerator.generateOperation(DELETE_HEADER_FOOTER, { id: $passivePlaceholderNode.attr('data-container-id') });
                    }
                    docModel.applyOperations(localGenerator);
                }
                self.implToggleEvenOddHF(state, { removeAll: true });
            } else {
                docModel.applyOperations({ name: CHANGE_CONFIG, attrs: { page: { evenOddPages: state } } });
            }
            selection.setTextSelection(selection.getFirstDocumentPosition());
        };

        /**
         * Function triggered as implication of changed property for page atrribuge firstPage.
         *
         * @param {Boolean} state
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.removeAll=false]
         *      If set to true, this method call is precceding removing all HF in doc,
         *      and therefore there is no need to create default type before removing them all.
         */
        this.implToggleFirstPageHF = function (state, options) {
            var
                removeAll = getBooleanOption(options, 'removeAll', false);

            if (pageAttributes && pageAttributes.page) {
                if (!docApp.isODF()) {
                    pageAttributes.page.firstPage = state;
                }
                firstPage = state;

                replaceActiveNodeOnToggle(removeAll);
            }
        };

        /**
         * Function triggered as implication of changed property for page atrribuge evenOddPages.
         *
         * @param {Boolean} state
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.removeAll=false]
         *      If set to true, this method call is precceding removing all HF in doc,
         *      and therefore there is no need to create default type before removing them all.
         */
        this.implToggleEvenOddHF = function (state, options) {
            var
                removeAll = getBooleanOption(options, 'removeAll', false);

            if (pageAttributes && pageAttributes.page) {
                if (!docApp.isODF()) {
                    pageAttributes.page.evenOddPages = state;
                }
                evenOddPages = state;

                replaceActiveNodeOnToggle(removeAll);
            }
        };

        /**
         * Public accesor of replaceAllTypesOfHeaderFooters function.
         *
         */
        this.replaceAllTypesOfHeaderFooters = function () {
            replaceAllTypesOfHeaderFooters();
        };

        /**
         * Public accesor for updating simple fields of type page count and page number.
         */
        this.updatePageSimpleFields = function () {
            updatePageCountFields();
            updatePageNumberFields();
        };

        /**
         * Looping through current 'page content DOM' and adding heights of elements to calculate page breaks positions.
         * Starts from element which is modified and continues downwards trough DOM. Stops if no more elements are shifted to new page
         * break-above marker: used to mark element above which page break node should be inserted
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.skipAbsoluteDrawings=false]
         *     If set to true, don't trigger update of absolute positioned drawings, to avoid endless loop.
         */
        this.insertPageBreaks = function (options) {
            if (docModel.skipPageLayoutRendering()) { return; } // if page layout is disabled, dont run the function
            var contentHeight = 0,
                floatingDrawings,
                elementHeight = 0,
                elementHeightWithoutMB = 0,
                prevContentHeight = 0,
                processingNodes = $(),
                elementSpan,
                newlySplittedSpan,
                nodeHelper,
                zoomFactor = docApp.getView().getZoomFactor(),
                offsetPosition = [],
                modifyingElement,
                pageBreak,
                markup = '',
                elMarginBottom,
                elMarginTop,
                diffMargin = 0,
                prevElMarginBottom = 0,
                fDrawingHeight,
                fDrawingWidth = 0,
                accHeightFromPrevDrawing = 0,
                //heightOfRectPartHelper = 0,
                // helper for fetching first node from where to start calculating
                procNodesFirst,
                $element,
                currentProcessingNode = docModel.getCurrentProcessingNode(),
                skipUpdateAbsoluteDrawings = getBooleanOption(options, 'skipAbsoluteDrawings', false);

            if (!pageAttributes) { initializePageAttributes(); }

            if (docModel.checkQuitFromPageBreaks()) { // if there is signal to interup page break processing, break from loop (ex. fast typing in big docs, succesive char deletion)
                docModel.setQuitFromPageBreaks(false);
                if (pbPromise) {
                    pbPromise.abort();
                    pbPromise = null;
                }
                pbPromise = self.executeDelayed(function () {
                    docModel.insertPageBreaks(currentProcessingNode ? currentProcessingNode : {});
                }, PB_DELAY_TIME);
                return;
            }

            function paragraphSplitWithPageBreak(element) {
                var arrBreakAboveSpans = [];
                var upperPartDiv, totalUpperParts, diffPosTopAndPrev,
                    elementLeftIndent, cssNegativeTextIndent;

                $element = $(element);
                offsetPosition = [];
                modifyingElement = currentProcessingNode[0] === element; // flag if we are modifying the element, then cached line positions are invalid
                if (modifyingElement) {
                    selection.setInsertTextPoint(null); // #32603 invalidate cached text point - reference to text node that is changed, so it returns wrong value for getFocusPositionBoundRect when scrollToSelection
                }

                offsetPosition = getOffsetPositionFromElement(element, elementHeight, contentHeight, modifyingElement, zoomFactor, accHeightFromPrevDrawing, fDrawingWidth);

                if (offsetPosition && offsetPosition.length > 0) { // if offset position is returned
                    if (offsetPosition[0] === 0) { // if offset is 0, no need for p split, insert pb before element
                        offsetPosition.shift();
                        if (!(DOM.isManualPageBreakNode($element.prev()) && DOM.isPageBreakNode($element.prev().children().last()))) {
                            contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
                            if (offsetPosition.length > 0) { contentHeight = 0; }
                        }
                    }
                    if (offsetPosition.length) {
                        zoomFactor = docApp.getView().getZoomFactor();
                        pageBreak = $();
                        upperPartDiv = 0;
                        totalUpperParts = 0;
                        diffPosTopAndPrev = 0;
                        elementLeftIndent = parseInt($element.css('margin-left'), 10) + parseInt($element.css('padding-left'), 10); // for indented elements such as lists, or special paragraphs
                        cssNegativeTextIndent = _.browser.Chrome && parseInt($element.css('text-indent'), 10) < 0;

                        elementSpan = $element.children('span').first();
                        while (elementSpan.text().length < 1 || !(elementSpan.is('span'))) {
                            elementSpan = elementSpan.next();
                        }
                        if (elementSpan && elementSpan.length > 0) { //fix for #32563, if div.p contains only empty spans
                            _.each(offsetPosition, function (offset) {
                                var elTextLength = elementSpan.text().length;
                                while (elementSpan.length && elTextLength <= offset) {
                                    offset -= elTextLength;
                                    elementSpan = elementSpan.next();
                                    while (!elementSpan.is('span')) { // we might fetch some inline div nodes, like div.tab or div.linebreak
                                        elementSpan = elementSpan.next();
                                    }
                                    elTextLength = elementSpan.text().length;
                                }
                                newlySplittedSpan = DOM.splitTextSpan(elementSpan, offset, { append: true });
                                arrBreakAboveSpans.push(newlySplittedSpan);
                                elementSpan = newlySplittedSpan;
                            });
                        }

                        if (arrBreakAboveSpans.length) {
                            $element.addClass('contains-pagebreak');
                            _.each(arrBreakAboveSpans, function (breakAboveSpan) {
                                diffPosTopAndPrev += ($(pageBreak).length ? ($(pageBreak).outerHeight(true)) + upperPartDiv : upperPartDiv);
                                upperPartDiv = $(breakAboveSpan).position().top / zoomFactor - diffPosTopAndPrev;
                                totalUpperParts += upperPartDiv;
                                contentHeight += upperPartDiv;
                                nodeHelper = $element.prev();
                                while (DOM.isPageBreakNode(nodeHelper)) {
                                    nodeHelper = nodeHelper.prev();
                                }
                                markup = generatePagebreakElementHtml(contentHeight, _.browser.WebKit, elementLeftIndent);
                                $(breakAboveSpan).addClass('break-above-span').before(markup);
                                pageBreak = $(breakAboveSpan).prev();
                                contentHeight = 0;
                                if (cssNegativeTextIndent) { // #40776
                                    pageBreak.css('display', 'inline-block');
                                }
                            });
                        }
                        contentHeight = elementHeight - totalUpperParts;
                    }
                } else {
                    contentHeight = insertPageBreaksBetweenDrawings(element, elementHeight, contentHeight, offsetPosition, zoomFactor);
                    //contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
                }
            }

            function processNodeForPageBreaksInsert(element, index) {
                var containsMsBreak = null;
                var pbRows = null;
                var checkPageBreakNode = false;
                if (docModel.checkQuitFromPageBreaks()) { // if there is signal to interup page break processing, break from loop (ex. fast typing in big docs, succesive char deletion)
                    docModel.setQuitFromPageBreaks(false);
                    //quitFromPageBreak = false;
                    if (pbPromise) {
                        pbPromise.abort();
                        pbPromise = null;
                    }
                    pbPromise = self.executeDelayed(function () {
                        docModel.insertPageBreaks(currentProcessingNode ? currentProcessingNode : {});
                    }, PB_DELAY_TIME);
                    return BREAK;
                }
                $element = $(element);
                containsMsBreak = $element.find('.ms-hardbreak-page').length > 0;

                // first merge back any previous paragraph splits
                pageBreak = $element.find('.page-break');
                if (DOM.nodeContainsPageBreak($element) || pageBreak.length > 0) {
                    checkPageBreakNode = true;
                    pageBreak.remove();
                    $element.children('.break-above-drawing').removeClass('break-above-drawing');
                    _.each($element.children('span'), function (span) {
                        if (span.classList.contains('break-above-span')) {
                            $(span).removeClass('break-above-span');
                            mergeSiblingTextSpans(span);
                        }
                    });
                    if (DOM.isParagraphNode($element)) {
                        docModel.validateParagraphNode(element);
                        $element.removeClass('contains-pagebreak');
                        if (containsMsBreak) {
                            $element.addClass('manual-page-break');
                        }
                    }
                }
                pbRows = $element.find(DOM.TABLE_PAGEBREAK_ROWNODE_SELECTOR);
                // if table is split, merge back
                if (pbRows.length) {
                    checkPageBreakNode = true;
                    pbRows.remove();
                    if ((_.browser.Firefox) && (DOM.isTableNode(element))) { forceTableRendering(element); } // forcing Firefox to re-render table (32461)
                }
                $element.removeClass('tb-split-nb'); // return box shadow property to unsplit tables

                fDrawingHeight = 0;
                floatingDrawings = $element.find('.drawingspacemaker.float');
                if (floatingDrawings.length > 0) {
                    _.each(floatingDrawings, function (drawing) {
                        var drawingOuterWidth = $(drawing).outerWidth(true);
                        var drawingOuterHeight = $(drawing).outerHeight(true);
                        var scaledDrawingPosTop = ($(drawing).position().top / zoomFactor);
                        if (drawingOuterWidth !== $pageContentNode.width()) {
                            if (drawingOuterHeight + scaledDrawingPosTop > fDrawingHeight) {
                                fDrawingHeight = drawingOuterHeight + scaledDrawingPosTop;
                                fDrawingWidth = drawingOuterWidth;
                            }
                        }
                    });
                }
                if (accHeightFromPrevDrawing > fDrawingHeight) {
                    fDrawingHeight = 0;
                }

                elementHeightWithoutMB = element.offsetHeight;
                elMarginBottom = parseCssLength($element, 'marginBottom');
                elMarginTop = parseCssLength($element, 'marginTop');

                if (elMarginTop > prevElMarginBottom) { // el has margin top that is greater than margin bottom of prev el, include diff in page height
                    diffMargin = elMarginTop - prevElMarginBottom;
                    elementHeightWithoutMB += diffMargin;
                }
                elementHeight = elementHeightWithoutMB + elMarginBottom;
                prevElMarginBottom = elMarginBottom;

                if (DOM.isManualPageBreakNode(element)) {
                    checkPageBreakNode = true;
                    if (index === 0 &&  !containsMsBreak) {
                        // if first element that's modified, and it's not microsoft pb, we know we are at the begining of page, and inserting pb again isn't needed, see #42064
                    } else {
                        if ($element.prev().length > 0 || containsMsBreak) {
                            contentHeight = manualInsertionOfPageBreak(element, contentHeight, elementHeight, { cleanup: true });
                            if (containsMsBreak) {
                                // rest element height need to be recalculated then
                                prevContentHeight = getLastPageBreakInNode(element).next();
                                prevContentHeight = (prevContentHeight.length && prevContentHeight.position().top) || 0;
                                elementHeight = element.offsetHeight + elMarginBottom - prevContentHeight;

                                // check if the paragraph contains a MS break and additionally the attribute "pageBreakBefore"
                                // -> another page break is required -> this is a workaround to show empty pages correctly (DOCS-3377)
                                if (hasPageBreakBeforeAttribute($element)) {
                                    $element.before(generatePagebreakElementHtml(contentHeight, _.browser.WebKit));
                                }
                            }
                        } else {
                            $element.removeClass('manual-page-break');
                        }
                    }
                }

                // if current content height is bellow max page height
                // and if there is no image floated inside paragraph, or if so, p fits inside page with it also
                // and if there is no floated image from one of previous paragraphs that intersects current p, or if exist, element can fit on page with it also;
                if (contentHeight + elementHeight <= pageMaxHeight && (fDrawingHeight < 1 || contentHeight + fDrawingHeight <= pageMaxHeight) && (accHeightFromPrevDrawing < 1 || (contentHeight + accHeightFromPrevDrawing <= pageMaxHeight))) {
                    if (!DOM.isManualPageBreakNode(element) || !containsMsBreak) {
                        contentHeight += elementHeight;
                    }
                    if (accHeightFromPrevDrawing > elementHeight) {
                        accHeightFromPrevDrawing -= elementHeight;
                    } else {
                        //heightOfRectPartHelper = accHeightFromPrevDrawing;
                        accHeightFromPrevDrawing = 0;
                    }
                    if (!DOM.isManualPageBreakNode(element)) {
                        prepareNodeAndRemovePageBreak(element);
                    }
                // for last element that can fit on page we need to omit margin bottom
                } else if (contentHeight + elementHeightWithoutMB <= pageMaxHeight && (fDrawingHeight < 1 || contentHeight + fDrawingHeight <= pageMaxHeight) && (accHeightFromPrevDrawing < 1 || (contentHeight + accHeightFromPrevDrawing <= pageMaxHeight))) {
                    if (!DOM.isManualPageBreakNode(element) || !containsMsBreak) {
                        contentHeight += elementHeightWithoutMB;
                    }
                    if (accHeightFromPrevDrawing > elementHeightWithoutMB) {
                        accHeightFromPrevDrawing -= elementHeightWithoutMB;
                    } else {
                        //heightOfRectPartHelper = accHeightFromPrevDrawing;
                        accHeightFromPrevDrawing = 0;
                    }
                    if (!DOM.isManualPageBreakNode(element)) {
                        prepareNodeAndRemovePageBreak(element);
                    }
                } else { //shift to new page or split
                    prevElMarginBottom = 0;
                    accHeightFromPrevDrawing = 0;
                    nodeHelper = $element.next();
                    //if element is last in document
                    if (nodeHelper.length) {
                        // remove old page break
                        while (nodeHelper.hasClass('page-break')) {
                            nodeHelper.remove();
                            nodeHelper = $element.next();
                        }
                    }

                    if (DOM.isParagraphNode($element) && ($element.text().length > 10 || $element.find('.drawing.inline').length > 0)) { // paragraph has to be split
                        paragraphSplitWithPageBreak(element);
                        // end of paragraph split
                    } else if (element.rows && element.rows.length > 1) { // table split
                        contentHeight = insertPageBreaksInTable(element, elementHeight, contentHeight, { diffMargin });
                    } else { // default insertion of pb between elements
                        contentHeight = defaultInsertionOfPageBreak(element, elementHeight, contentHeight);
                    }
                }
                if (DOM.isNodeWithPageBreakAfterAttribute(element)) {
                    if ($element.next().length > 0) {
                        contentHeight = insertManualPageBreakAfter(element, contentHeight);
                    } else { // first element in document, can't have page break before - can happen on delete all where last is manual pb paragraph
                        $element.removeClass('manual-pb-after');
                    }
                }

                if (fDrawingHeight > 0) { // store the diff between floated image height and height of containing p, for next iteration
                    accHeightFromPrevDrawing = fDrawingHeight - elementHeight;
                }
                // at the last page, we add extra padding, to fill it out to the size of max page height
                if (processingNodes.length === index + 1) {
                    $pageContentNode.css({ 'padding-bottom': (pageMaxHeight - contentHeight > 0) ? pageMaxHeight - contentHeight : '0px' });
                }

                // if the element is a node for a page break, the selected drawing must be checked
                if ((containsMsBreak || checkPageBreakNode) && selection.isAnyDrawingSelection()) {
                    var drawingsInPageBreak = $element.children('.drawing.selected');
                    if (drawingsInPageBreak.length > 0) {
                        var selectedDrawing = selection.getAnyDrawingSelection();
                        _.each(drawingsInPageBreak, function (oneDrawing) {
                            if (getDomNode(selectedDrawing) === getDomNode(oneDrawing)) {
                                docApp.getView().scrollToChildNode(selectedDrawing); // jumping to the selected drawing, if it was child of the pagebreak node
                            }
                        });
                    }
                }

            } //end of processNodeForPageBreaksInsert

            if (docModel.isProcessingOperations()) {
                // to prevent triggering method when not all DOM nodes are rendered (still operations in the stack, #32067)
                self.executeDelayed(function () {
                    docModel.insertPageBreaks(currentProcessingNode ? currentProcessingNode : {});
                }, PB_DELAY_TIME);
            } else {
                // cleanup after deleting big blocs of data at once
                while ($pageContentNode.children().first().hasClass('page-break')) {
                    $pageContentNode.children().first().remove();
                }

                if (currentProcessingNode) {
                    currentProcessingNode = $(currentProcessingNode);
                    if (currentProcessingNode.hasClass('break-above')) { // first element bellow pagebreak, we want to refresh previous page also
                        nodeHelper = currentProcessingNode.prev();
                        while (nodeHelper.hasClass('page-break')) {
                            nodeHelper = nodeHelper.prev();
                        }
                        if (nodeHelper.length > 0) {
                            currentProcessingNode = nodeHelper;
                        } else {
                            currentProcessingNode.prev('.page-break').remove();
                        }
                    }
                    if (currentProcessingNode.next().hasClass('page-break')) {
                        nodeHelper = currentProcessingNode.prev();
                        while (nodeHelper.hasClass('page-break')) {
                            nodeHelper = nodeHelper.prev();
                        }
                        if (nodeHelper.length === 0) {
                            currentProcessingNode.prevAll('.page-break').remove();
                        }
                    }
                    if (currentProcessingNode.parentsUntil('.pagecontent', 'table').length > 0) { //if we fetch valid div.p, but inside of table
                        currentProcessingNode = currentProcessingNode.parentsUntil('.pagecontent', 'table').last();
                    }
                    nodeHelper = currentProcessingNode.prev();
                    if (nodeHelper.hasClass('page-break')) {
                        while (nodeHelper.hasClass('page-break')) { // might happen when deleting multiple elements at once, some pb are left - clean-up
                            nodeHelper = nodeHelper.prev();
                        }
                        if (nodeHelper.length === 0) {
                            currentProcessingNode.prevAll('.page-break').remove();
                            processingNodes = currentProcessingNode.nextAll('.p, table').addBack();
                        } else {
                            nodeHelper = nodeHelper.prevUntil('.page-break, .contains-pagebreak').last();
                            if (DOM.nodeContainsPageBreak(nodeHelper.prev())) {
                                contentHeight = (nodeHelper.prev().height() + parseInt(nodeHelper.prev().css('margin-bottom'), 10)) - nodeHelper.prev().children('.break-above-span, .break-above-drawing').last().position().top / zoomFactor;
                            }
                            processingNodes = nodeHelper.nextAll('.p, table').addBack();
                        }
                        //} else if (DOM.isManualPageBreakNode(currentProcessingNode)) {
                        //  processingNodes = nodeHelper.nextAll('div.p, table').addBack();
                    } else if (DOM.nodeContainsPageBreak(nodeHelper)) { //if node is second bellow pagebreak - fallback
                        if (nodeHelper.find('.break-above-span').length > 0) { // special case: split p with breakabove span, above page break
                            if (DOM.isManualPageBreakNode(currentProcessingNode)) { // fix for inserting manual page break in paragraph bigger than page height
                                processingNodes = nodeHelper.prevUntil('.page-break').last().nextAll('.p, table').addBack();
                            } else {
                                processingNodes = currentProcessingNode.nextAll('.p, table').addBack();
                                contentHeight = (nodeHelper.height() + parseInt(nodeHelper.css('margin-bottom'), 10)) - nodeHelper.children('.break-above-span, .break-above-drawing').last().position().top / zoomFactor;
                            }
                        } else {
                            procNodesFirst = nodeHelper.prevUntil('.page-break, .contains-pagebreak').last();
                            nodeHelper = procNodesFirst.prev();
                            if (DOM.nodeContainsPageBreak(nodeHelper)) {
                                processingNodes = procNodesFirst.nextAll('.p, table').addBack();
                                contentHeight = (nodeHelper.height() + parseInt(nodeHelper.css('margin-bottom'), 10)) - nodeHelper.children('.break-above-span, .break-above-drawing').last().position().top / zoomFactor;
                            } else {
                                processingNodes = procNodesFirst.nextAll('.p, table').addBack();
                            }
                        }
                    } else {
                        procNodesFirst = currentProcessingNode.prevUntil('.page-break, .contains-pagebreak').last();
                        nodeHelper = procNodesFirst.prev();
                        if (DOM.nodeContainsPageBreak(nodeHelper)) {
                            processingNodes = procNodesFirst.nextAll('.p, table').addBack();
                            if (DOM.isPageBreakNode(nodeHelper.children().last())) { // #40430 - if ms page break is last in paragraph, it's a new page and content height is reset to 0
                                contentHeight = 0;
                            } else {
                                contentHeight = (nodeHelper.height() + parseInt(nodeHelper.css('margin-bottom'), 10)) - nodeHelper.children('.break-above-span, .break-above-drawing').last().position().top / zoomFactor;
                            }
                        } else {
                            processingNodes = procNodesFirst.nextAll('.p, table').addBack();
                        }
                    }
                    if (processingNodes.length === 0) { // if we are somewhere on first page, no pageBreak class at the top!
                        processingNodes = $pageContentNode.children('.p, table');
                    }
                } else if (_.isEmpty(currentProcessingNode)) { // fallback option if we don't fetch any valid node
                    processingNodes = $pageContentNode.children('.p, table');
                    currentProcessingNode = processingNodes.first();
                }

                if (processingNodes) {
                    // remove all break-above classes except the top one
                    processingNodes.removeClass('break-above').first().addClass('break-above');
                    resolvePageTypeAndHeight(processingNodes.first());

                    // process all fetched nodes
                    self.iterateArraySliced(processingNodes, processNodeForPageBreaksInsert, { delay: 50 })
                        .done(function () {
                            pageBreaksCollection = $pageContentNode.find('.page-break');
                            // store indexes of pagebreaks as values for page numbers
                            _.each(pageBreaksCollection, function (pageBreakInCollection, index) {
                                $(pageBreakInCollection).data('page-num', index + 1);
                            });
                            pageCount = pageBreaksCollection.length + 1;

                            populateHeaderFooterPaginationClasses(pageBreaksCollection);
                            if (self.isHeaderFooterInDocument() && !docModel.isPendingDebouncedHeaderUpdate()) { // #58750 - don't replace headers if there is pending debounced update
                                replaceAllTypesOfHeaderFooters();
                            } else {
                                if (!fieldManager.isComplexFieldEmpty()) { fieldManager.updatePageFieldsInMarginals(); }
                            }
                            if (!fieldManager.isSimpleFieldEmpty()) { self.updatePageSimpleFields(); }

                            _.each(cleanupElements, function (element) {
                                $(element).remove();
                            });
                            cleanupElements = [];

                            if (isOTMode()) {
                                // OT, no scrolling forced by external operations (it is very disturbing)
                                // -> would be good to know, if insertPageBreaks was triggered by internal or external operation
                                docApp.getView().recalculateDocumentMargin({ keepScrollPosition: true });
                            } else {
                                docApp.getView().recalculateDocumentMargin({ keepScrollPosition: !docApp.isEditable() });  // Checking edit mode for 39904
                            }
                            if (repaintRemoteSelection) {
                                docModel.getRemoteSelection().renderCollaborativeSelections();
                                repaintRemoteSelection = false;
                            }
                            if (!skipUpdateAbsoluteDrawings) {
                                docModel.trigger('update:absoluteElements');   // updating comments and absolutely positioned drawings
                            }
                            // to trigger formatting for paragraphs with color or border after page-breaks are finished
                            docModel.trigger('update:borderAndColor');
                            // trigger event for clearing processing nodes collector
                            docModel.trigger('empty:processingNodesCollector');
                            // trigger event after pagination is finished, so that methods relying on f.e. page number, can update
                            docModel.trigger('pagination:finished');

                            // an additional update of the vertical scroll position might be required after 'update:absoluteElements'
                            if (selection.isAnyDrawingSelection()) { docApp.getView().scrollToChildNode(selection.getAnyDrawingSelection()); }

                            var focusInPage = containsNode(docModel.getNode(), getFocus(), { allowSelf: true });
                            var keepFocus = isOTMode() && !focusInPage; // not removing any selection outside the page
                            if (selection.hasRange() && !selection.isCropMode() && !docModel.isDrawingChangeActive()) {
                                selection.setTextSelection(selection.getStartPosition(), selection.getEndPosition(), { keepFocus, backwards: selection.isBackwards() });
                            }

                            if (focusInPage) { // only restore browser selection, if the focus is inside the page (DOCS-3316)
                                selection.restoreBrowserSelection({ preserveFocus: true }); // DOCS-3280, inserting pageBreaks can destroy existing browser selection
                            }
                        });
                }
            }
        };

        /**
         * Public method triggered when some of page metric properties are changed,
         * like width, height or margins.
         *
         * @param {Object} changedPageAttrs
         *  List of changed page attributes
         */
        this.refreshPageLayout = function (changedPageAttrs) {
            var cssAttrs = {};
            var useHeaderMargin = self.isHeaderInDocument() || docApp.isODF();
            var useFooterMargin = self.isFooterInDocument() || docApp.isODF();

            // leaving an optionally activated header or footer edit mode
            self.leaveHeaderFooterAndSetSelection();

            // for external operations the root node might still be header/footer
            // -> but it must be the page, because css attributes will be assigned to the page (DOCS-3112)
            // -> this problem should no longer happen after fixing rootNode handling in 'implCreateHeaderFooter',
            //    where the rootNode was also changed for external clients
            if (getDomNode(rootNode) !== getDomNode(docModel.getNode())) { rootNode = docModel.getNode(); }

            // re-initialize page styles
            pageAttributes = docModel.pageStyles.getElementAttributes(rootNode);

            pagePaddingLeft = convertHmmToLength(pageAttributes.page.marginLeft, 'px', 0.001);
            pagePaddingRight = convertHmmToLength(pageAttributes.page.marginRight, 'px', 0.001);
            pagePaddingTop = convertHmmToLength(pageAttributes.page.marginTop, 'px', 0.001);
            pagePaddingBottom = convertHmmToLength(pageAttributes.page.marginBottom, 'px', 0.001);
            pageHeight = convertHmmToLength(pageAttributes.page.height, 'px', 0.001);
            pageMaxHeight = pageHeight - pagePaddingTop - pagePaddingBottom;
            pageWidth = convertHmmToLength(pageAttributes.page.width, 'px', 0.001);
            marginalMaxHeight = 4 / 9 * pageHeight;

            if (_.isNumber(changedPageAttrs.marginLeft)) { cssAttrs.paddingLeft = changedPageAttrs.marginLeft / 100 + 'mm'; }
            if (_.isNumber(changedPageAttrs.marginRight)) { cssAttrs.paddingRight = changedPageAttrs.marginRight / 100 + 'mm'; }
            if (_.isNumber(changedPageAttrs.marginTop)) { cssAttrs.paddingTop = changedPageAttrs.marginTop / 100 + 'mm'; }
            if (_.isNumber(changedPageAttrs.marginBottom)) { cssAttrs.paddingBottom = changedPageAttrs.marginBottom / 100 + 'mm'; }
            if (_.isNumber(changedPageAttrs.width) || _.isNumber(changedPageAttrs.marginRight) || _.isNumber(changedPageAttrs.marginLeft)) {
                cssAttrs.width = (pageAttributes.page.width - pageAttributes.page.marginLeft - pageAttributes.page.marginRight) / 100 + 'mm';
            }
            if (_.isNumber(changedPageAttrs.height)) { cssAttrs.minHeight = changedPageAttrs.height / 100 + 'mm'; }

            if (!_.isEmpty(cssAttrs)) { rootNode.css(cssAttrs); } // assigning page attributes to the page

            // force repaint layout
            updateStartHeaderFooterStyle();
            $firstHeaderWrapper.css({ width: pageWidth, margin: ' 0px 0px 0px ' + (-pagePaddingLeft) + 'px' }).children('.header').css({
                minHeight: pagePaddingTop,
                maxHeight: marginalMaxHeight,
                maxWidth: pageWidth,
                padding: (useHeaderMargin ? headerMargin : 0) + 'px ' + pagePaddingRight + 'px 0 ' + (pagePaddingLeft) + 'px'
            });
            $lastFooterWrapper.css({ width: pageWidth, margin: ' 0px 0px 0px ' + (-pagePaddingLeft) + 'px' }).children('.footer').css({
                minHeight: pagePaddingBottom,
                maxHeight: marginalMaxHeight,
                maxWidth: pageWidth,
                padding: '5px ' + pagePaddingRight + 'px ' + (useFooterMargin ? footerMargin : 0) + 'px ' + (pagePaddingLeft) + 'px'
            });

            if (docModel.isHeaderFooterEditState()) {
                self.leaveHeaderFooterEditMode();
                selection.setNewRootNode(docModel.getNode()); // restore original rootNode
                selection.setTextSelection(selection.getFirstDocumentPosition());
            }
            // invalidate cached line break positions, because of layout change
            $pageContentNode.children('.contains-pagebreak').removeData('lineBreaksData');

            repaintRemoteSelection = true;
            // update of comments, absolute drawings etc. is triggered from inside this function
            docModel.insertPageBreaks();
        };

        /**
         * Public method triggered on document attributes change, like width/height.
         * It changes width/height of all slides in document, and updates slide background.
         *
         * @param {Object} changedPageAttributes
         *  List of changed page attributes
         */
        this.refreshSlidesLayout = function (changedPageAttributes) {
            // re-initialize page styles
            pageAttributes = docModel.pageStyles.getElementAttributes(rootNode);

            if (changedPageAttributes.width) {
                rootNode.css('width', (pageAttributes.page.width) / 100 + 'mm');
            }
            if (changedPageAttributes.height) {
                rootNode.css('min-height', changedPageAttributes.height / 100 + 'mm');
            }

            _.each(docModel.getAllSlides(), function ($slide, slideId) {
                var mergedAttributes = docModel.slideStyles.getElementAttributes($slide);
                docModel.slideStyles.updateBackground($slide, mergedAttributes, slideId);
            });
        };

        /**
         * Delete pageBreakBefore property from setAttributes in pasteInternalClipboard operations array,
         * if destination paragraph already has this property, so that the property is not overwritten by new operation.
         *
         * @param {Array} operations
         */
        this.handleManualPageBreakDuringPaste = function (operations) {
            var // setAttributes operation extracted from list of paste operations
                setAttrOperation = _.findWhere(operations, { name: SET_ATTRIBUTES }),
                // oxo position of destination paragraph where clipboard is going to be pasted
                destinationParagraphPosition,
                // dom node of destination paragraph
                destinationParagraphNode;

            if (setAttrOperation && setAttrOperation.start) {
                destinationParagraphPosition = setAttrOperation.start.slice(0, 1);
                destinationParagraphNode = getParagraphElement(docModel.getNode(), destinationParagraphPosition);
                if (DOM.isManualPageBreakNode(destinationParagraphNode) && setAttrOperation.attrs && setAttrOperation.attrs.paragraph) {
                    if (!_.isUndefined(setAttrOperation.attrs.paragraph.pageBreakBefore)) {
                        delete setAttrOperation.attrs.paragraph.pageBreakBefore;
                    }
                }
            }
        };

        /**
         * Toggles dropdown menu for marginal context menu in header/footer.
         *
         * @param {Node|jQuery} item
         *  Child dom element of context menu type button.
         */
        this.toggleMarginalMenuDropdown = function (item) {
            var
                marginalMenuType = $(item).closest('.marginal-menu-type');

            marginalMenuType.toggleClass('dropdown-open');
            marginalMenuType.find('.marginal-container').toggle();
            marginalMenuType.find('.selected').removeClass('selected');
            if (firstPage) {
                if (evenOddPages) {
                    marginalMenuType.find('.marginal-first-even').addClass('selected');
                } else {
                    marginalMenuType.find('.marginal-first').addClass('selected');
                }
            } else if (evenOddPages) {
                marginalMenuType.find('.marginal-even').addClass('selected');
            } else {
                marginalMenuType.find('.marginal-same').addClass('selected');
            }
        };

        /**
         * After applying snapshoot (for ex. after canceling paste in header), all DOM nodes are replaced.
         * References to them need to be refreshed.
         */
        this.refreshActiveMarginalNode = function () {
            var pageNode = docModel.getNode();
            var newRootNode = pageNode.find('.active-selection');
            var isHeader = newRootNode.hasClass('header');

            $firstHeaderWrapper = pageNode.children('.header-wrapper');
            $lastFooterWrapper = pageNode.children('.footer-wrapper');
            docModel.setHeaderFooterRootNode(newRootNode);
            self.refreshPageBreakCollection();
            docModel.cancelDebouncedUpdatingHeaders();
            self.updateEditingHeaderFooter();
            replaceAllTypesOfHeaderFooters();
            // ensure no overlays for active header node, #53757
            newRootNode.children('.cover-overlay').remove();

            if (isHeader) {
                contextHeaderMenu = newRootNode.parent().find('.marginal-context-menu');
            } else {
                contextFooterMenu = newRootNode.parent().find('.marginal-context-menu');
            }
        };

        /**
         * Leaving an optionally activated header/footer edit mode and set the cursor
         * into the first position of the main document. If the header/footer mode is
         * not active, no changes are done.
         */
        this.leaveHeaderFooterAndSetSelection = function () {
            if (docModel.isHeaderFooterEditState()) { // leaving header footer mode
                self.leaveHeaderFooterEditMode();
                selection.setNewRootNode(docModel.getNode()); // restore original rootNode
                selection.setTextSelection(selection.getFirstDocumentPosition());
                rootNode = docModel.getNode(); // all clients have to leave header/footer mode, also after external operation ?!
            }
        };

        /**
         * Updating the cached page content node. This is necessary after the page
         * content node is exchanged after cancelling a long running action.
         */
        this.refreshPageContentNode = function () {
            $pageContentNode = DOM.getPageContentNode(rootNode);
        };

        /**
         * Returns all pageBreaks currently present in pagecontent.
         *
         * @returns {Array} pageBreaksCollection
         */
        this.getPageBreaksCollection = function () {
            return pageBreaksCollection;
        };

        /**
         * Getter for height of header or footer on given page. In Pixels!
         *
         * @param {Number} pageNum
         *  Number of page for which we want header hight.
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.footer=false]
         *      If set to true, height of footer will be returned, otherwise height of header.
         * @returns {Number}
         */
        this.getMarginalNodeHeight = function (pageNum, options) {
            var height = 0,
                footer = getBooleanOption(options, 'footer', false);
            if (!pageNum || pageNum < 1) {
                pageLogger.warn('pageLayout.getHeaderHeight(): invalid page number argument: ', pageNum);
                return height;
            }
            if (self.isHeaderFooterInDocument()) {
                if (pageNum === 1) {
                    height = firstPage ? (footer ? heightOfFirstFooter : heightOfFirstHeader) : (footer ? heightOfDefFooter : heightOfDefHeader);
                } else if (pageNum % 2 === 0) {
                    height = evenOddPages ? (footer ? heightOfEvenFooter : heightOfEvenHeader) : (footer ? heightOfDefFooter : heightOfDefHeader);
                } else {
                    height = (footer ? heightOfDefFooter : heightOfDefHeader);
                }
            } else {
                height = pagePaddingTop;
            }
            return Math.max(pagePaddingTop, height);
        };

        /**
         * Refreshing the collection of page breaks. This is for example needed
         * by absolutely positioned drawings in the drawinglayer, whose position
         * needs the knowledge of the page numbers very early after loading the
         * document.
         */
        this.refreshPageBreakCollection = function () {
            pageBreaksCollection = $pageContentNode.find('.page-break');
            // store indexes of page breaks as values for page numbers
            _.each(pageBreaksCollection, function (pageBreakInCollection, index) {
                $(pageBreakInCollection).data('page-num', index + 1);
            });
            pageCount = pageBreaksCollection.length + 1;
        };

        /**
         * If whole header with content is created with undo/redo,
         * there might be some nodes like fields, tabs etc, that come after insert paragraph,
         * and they also need to be marked as marginal.
         *
         * @param {Object} collectionOfInsertHeaderOperations
         *  Collection of insertHeaderFooter operations from undo/redo after event.
         */
        this.markAllUndoRedoNodesAsMarginal = function (collectionOfInsertHeaderOperations) {
            _.each(collectionOfInsertHeaderOperations, function (createOp) {
                var target = createOp.id;
                var collObject = _.findWhere(headerFooterCollection, { id: target });

                if (collObject && collObject.node) {
                    markMarginalNodesInsideHeadFoot(collObject.node);
                }
            });
        };

        /**
         * If nodes are inserted in header or footer during undo or redo, they should be marked
         * as marginal after undo or redo.
         *
         * @param {String[]} collectionOfTargets
         *  Collection of targets modified by undo/redo.
         */
        this.markAllTargetNodesAfterUndoRedoAsMarginal = function (collectionOfTargets) {
            _.each(collectionOfTargets, function (target) {
                var collObject = _.findWhere(headerFooterCollection, { id: target });

                if (collObject && collObject.node) {
                    markMarginalNodesInsideHeadFoot(collObject.node);
                }
            });
        };

        /**
         * Updating the drawings in a new inserted header/footer (after undo/redo).
         * It can happen, that the drawings are not correctly formatted in the template for
         * the header/footer nodes. Therefore an additional formatting might be required for
         * the drawings that are already in the document (66982).
         *
         * @param {Object} collectionOfInsertHeaderOperations
         *  Collection of insertHeaderFooter operations from undo/redo after event.
         */
        this.updateFormattingOfDrawingsInPage = function (collectionOfInsertHeaderOperations) {
            _.each(collectionOfInsertHeaderOperations, function (createOp) {
                var marginalRootNode = docModel.getNode().find('.header, .footer').filter('[data-container-id="' + createOp.id + '"]').first();
                if (marginalRootNode.length && !DOM.isMarginalPlaceHolderNode(marginalRootNode.parent())) {
                    var allDrawings = marginalRootNode.find('div.drawing');
                    if (allDrawings.length) {
                        _.each(allDrawings, function (drawing) { docModel.drawingStyles.updateElementFormatting($(drawing)); });
                        // transferring immediately to template node
                        self.updateEditingHeaderFooter({ givenNode: marginalRootNode });
                    }
                }
            });
        };

        /**
         * If drawing in marginal node is selected, clear selection node in placeholder node.
         * See #57723 for more details.
         *
         * @param {String} target
         */
        this.clearSelectionFromPlaceholderNode = function (target) {
            var targetClass = '.' + target;
            var selectedDrawing = $headerFooterPlaceHolder.children(targetClass).find('.drawing.selected');
            clearSelection(selectedDrawing);
            // removing additional tracking handler options, that are specific for OX Text (Chrome only)
            selectedDrawing.removeData('trackingOptions');
            selectedDrawing.removeData('tracking-options');
            selectedDrawing.closest('.p').removeClass('visibleAnchor').find('.anchorIcon').remove();
        };

        // initialization -----------------------------------------------------

        docApp.onInit(function () {
            selection = docModel.getSelection();
            fieldManager = docModel.getFieldManager();

            self.waitForImportSuccess(function () {
                if (!pageAttributes) {
                    initializePageAttributes();
                }

                if (TOUCH_DEVICE && !docModel.useSlideMode()) { self.insertPageBreaks(); } // one extra call required for touch devices (Bug 39954) & (Bug 40901)

                // handle changed attribute default values
                docModel.defAttrPool.on("change:defaults", changedSet => {
                    if (!self.isImportFinished()) {
                        return;
                    }
                    if (changedSet.page) {
                        if (changedSet.page.firstPage !== undefined) {
                            self.implToggleFirstPageHF(changedSet.page.firstPage);
                        } else if (changedSet.page.evenOddPages !== undefined) {
                            self.implToggleEvenOddHF(changedSet.page.evenOddPages);
                        } else {
                            if (docModel.useSlideMode()) {
                                self.refreshSlidesLayout(changedSet.page);
                            } else {
                                self.refreshPageLayout(changedSet.page);
                                updateStartHeaderFooterStyle();
                                // ... and refreshing page layout once more
                                docModel.insertPageBreaks();
                            }

                            // only necessary for updating horizontal comment layer.
                            // Update of comments and absolute drawings is triggered from inside insertPageBreaks, with update:absoluteElements
                            docModel.trigger('change:pageSettings', changedSet);
                        }
                    }
                });
            });
        });
    }
}
