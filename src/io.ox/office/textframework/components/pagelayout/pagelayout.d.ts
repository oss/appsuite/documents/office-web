/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { Position } from "@/io.ox/office/editframework/utils/operations";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";

// types ======================================================================

/**
 * Type mapping for the events emitted by `PageLayout` instances.
 */
export interface PageLayoutEventMap {

    /**
     * Will be emitted after leaving the edit mode of header and footer AND
     * after the marginal nodes are completely exchanged. Listeners can rely on
     * the existence of the nodes, that are now in the header and footer.
     */
    "updateMarginal:leave": [event: { targetNode: JQuery }];
}

// class PageLayout ===========================================================

export default class PageLayout extends ModelObject<TextBaseModel, PageLayoutEventMap> {

    constructor(docModel: TextBaseModel, rootNode: NodeOrJQuery);

    enterHeaderFooterEditMode(node: JQuery, options?: { calledFromCreate?: boolean; keepNodes?: boolean }): void;
    getHeaderFooterPlaceHolder(): JQuery;
    getNumberOfDocumentPages(): number;
    getPageNumber(node?: Position | NodeOrJQuery): number;
    isIdOfMarginalNode(id: string): boolean;
    leaveHeaderFooterEditMode(node?: JQuery, parent?: JQuery): void;
    replaceAllTypesOfHeaderFooters(): void;
    updatePageCountField(node: NodeOrJQuery, format?: string): void;
    updatePageNumberField(node: NodeOrJQuery, format?: string, num?: number): void;
    updatePageSimpleFields(): void;
}
