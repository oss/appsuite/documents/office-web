/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { spellingSettings, userDictionary } from '@/io.ox/office/settings/api';
import { ary } from '@/io.ox/office/tk/algorithms';
import { LOCALE_DATA } from '@/io.ox/office/tk/locale';
import { debounceMethod } from "@/io.ox/office/tk/objects";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { SPELLING_ENABLED, getLocalesWithDictionary } from '@/io.ox/office/textframework/utils/config';
import { NO_SPELLCHECK_CLASSNAME, PARAGRAPH_NODE_LIST_EMPTY_CLASS, PARAGRAPH_NODE_SELECTOR, SPELLERRORNODE_CLASS, isHardBreakNode,
    isListLabelNode, isPageBreakNode, isSpellerrorNode, isTabNode, isTextSpan, iterateTextSpans, splitTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getWordBoundaries, getWordSelection, increaseLastIndex, iterateParagraphChildNodes } from '@/io.ox/office/textframework/utils/position';
import { getArrayOption, getBooleanOption, getStringOption, mergeSiblingTextSpans, textLogger } from '@/io.ox/office/textframework/utils/textutils';

// class SpellChecker =====================================================

/**
 * The spell checker object.
 *
 * @param {Editor} docModel
 *  The text editor model.
 */
class SpellChecker extends ModelObject {

    // online spelling mode off (there are no app specific spell-check user settings anymore due to DOCS-630)
    #spellingSupported;
    // the scroll node
    #scrollNode = null;
    // collects replacements of misspelled words by locale
    #spellReplacementsByLocale = {};
    // the cache for the modified paragraphs, that need to be spell checked
    #paragraphCache = $();
    //supported languages to check the spelling
    #supportedLanguages;
    // list for the unsupported language without a notification
    #unsupportedLanguageWithoutNotification = [];
    // list of unsupported languages with a notification
    #unsupportedLanguages = [];
    // in case a page does start as empty it will flag whether it always stod empty or if it, at one point, did contain real text content.
    #hasPageAlwaysBeenEmpty = true; // the `true` value default will be overwritten exactly once as soon as the page starts carrying content.
    // local list of misspelled words which are ignored by the user
    #ignoredMisspelledWords;
    // The undo manager
    #undoManager;
    // ignore actions for add or remove a ignore word, it will be used in the 'this.#implStartAddOrRemoveIgnoreWords' function
    #ignoreWordActions = [];
    // counter to set a unique request id
    #requestIDCounter = 0;
    // Set to true if the User Dictionary is changed, but the app is not visible
    #userDictionaryChanges = false;
    // If a word is misspelled select the whole word, on closing the contextmenu the selection must be restored
    #restoreStartPosition;

    constructor(docModel, initOptions) {

        // base constructor
        super(docModel);

        this.#spellingSupported = SPELLING_ENABLED && getBooleanOption(initOptions, 'spellingSupported', false);
        this.#ignoredMisspelledWords = userDictionary.getWords();
        this.#undoManager = this.docModel.getUndoManager();

        // initialization -----------------------------------------------------

        // load the list of locales with available spelling dictionary
        // (using `onFulfilled()` prevents type error when closing document very quickly)
        this.onFulfilled(getLocalesWithDictionary(), locales => {

            // add the supported languages to list to show notifications for used but unsupported languages
            this.#supportedLanguages = locales.map(locale => locale.replace('_', '-'));

            this.waitForImportSuccess(this.#documentLoaded);
        });

        this.docApp.onInit(() => {
            // updating the docModel after the document was reloaded (after cancelling long running actions)
            this.listenTo(this.docModel, 'document:reloaded', this.#handleDocumentReload);
        });
    }

    // public methods -----------------------------------------------------

    getSpellErrorWord() {

        /** @type {Selection} the selection object */
        const selection = this.docModel.getSelection();
        // the returning object containing the spellchecker infos
        const result = { url: null, spellError: false, word: '', replacements: [] };
        // the selected text span
        let span = null;
        // an object containing an expanded word selection
        let newSelection = null;
        // the locale of the selected text span
        let locale = null;
        // the character attributes of the selected text span
        let attributes = null;
        // the logical start position of the selection
        let startPosition = null;
        // Chrome on osx makes a text-range-selection on right mouseclick
        const chromeOnMacOS = (_.browser.MacOS && _.browser.Chrome);
        // whether the start position equals the beginning word boundary or not
        let startPositionEqualsWordStart = false;

        // find out a possible URL set for the current position
        startPosition = selection.getStartPosition();

        if (ary.equals(getWordBoundaries(this.docModel.getCurrentRootNode(), startPosition)[0], selection.getStartPosition())) {
            startPositionEqualsWordStart = true;
        }

        const paragraphPosition = _.initial(_.clone(startPosition));
        // on this special case, we need to count up the start-position. Otherwise,
        // the startposition of the selection breaks the detection of wrong spelled words

        // Bug 61466: Workaround for getDOMPosition() to not return the previous node.
        // The forcePositionCounting parameter is not a viable alternative.
        // It would return the spelling error span instead of the contained text node.
        if (chromeOnMacOS || startPositionEqualsWordStart) {
            startPosition = increaseLastIndex(startPosition);
        }

        const obj = getDOMPosition(this.docModel.getCurrentRootNode(), startPosition);

        if (obj && obj.node && isTextSpan(obj.node.parentNode)) {
            attributes = this.docModel.characterStyles.getElementAttributes(obj.node.parentNode).character;

            if (attributes.url.length > 0) {
                span = $(obj.node.parentNode);

                if ((span.text().length > 0) || (span.next().length > 0) || isTextSpan(span.next())) {
                    result.url = attributes.url;
                }
            } else if ($(obj.node.parentNode).hasClass(SPELLERRORNODE_CLASS) === true) {
                span = $(obj.node.parentNode);

                if ((span.text().length > 0) || (span.prev().length > 0) || isTextSpan(span.next())) {
                    result.spellError = true;
                }

                newSelection = getWordSelection(selection.getEnclosingParagraph(), startPosition[startPosition.length - 1], null, { returnAllTextSpans: true, returnTextSpan: true });

                if (newSelection) {
                    locale = attributes.language.replace(/-/, '_');
                    result.word = newSelection.text;
                    result.start = _.clone(paragraphPosition).concat(newSelection.start);
                    result.end = _.clone(paragraphPosition).concat(newSelection.end - 1);

                    result.replacements = this.#getSpellReplacements(result.word, locale);
                    result.node = newSelection.node;
                    result.nodes = newSelection.nodes;
                }
            }
        }

        return result;
    }

    replaceWord(paraPosition, startEndIndex, replacement) {

        this.#restoreTextSelection();

        const selection = this.docModel.getSelection();
        const startPosition = _.clone(paraPosition);
        const endPosition = _.clone(paraPosition);
        const newEndPosition = _.clone(paraPosition);

        startPosition[endPosition.length - 1] = startEndIndex.start;
        endPosition[endPosition.length - 1] = startEndIndex.end;

        this.#undoManager.enterUndoGroup(() => {
            selection.setTextSelection(startPosition, endPosition);

            return this.docModel.deleteSelected().done(() => {
                this.docModel.insertText(replacement, selection.getStartPosition(), this.docModel.getPreselectedAttributes()); // restoring start position is required!

                paraPosition = selection.getStartPosition();
                startEndIndex = getWordSelection(selection.getEnclosingParagraph(), paraPosition[paraPosition.length - 1]);
                newEndPosition[endPosition.length - 1] = startEndIndex.end;
                selection.setTextSelection(newEndPosition);
            });
        });
    }

    /**
     *
     */
    reset(attributes, paragraph) {
        //reset spelling status on language changes
        if (attributes.character && attributes.character.language) {
            this.resetDirectly(paragraph);
        }
    }

    /**
     *
     */
    resetClosest(newAttributes, element) {
        // invalidate spell result if language attribute changes
        if (newAttributes.character && newAttributes.character.language) {
            this.resetDirectly($(element).closest(PARAGRAPH_NODE_SELECTOR));
        }
    }
    /**
     * Removing the class marker for spell-checked paragraphs
     *
     * @param {Node|jQuery} paragraph
     *  The DOM node from which the class shall be removed.
     */
    resetDirectly(paragraph) {
        // reset to 'not spelled'
        $(paragraph).removeClass('p_spelled');
        // ... and caching paragraph for performance reasons
        if (!this.docModel.useSlideMode()) {
            this.#paragraphCache = this.#paragraphCache.add(paragraph);
        }
        $(paragraph).removeData(SpellChecker.SPELL_REQUEST_ID);
    }

    /**
     * Removing classes & runtimeclasses, multi selection possible.
     * Supported are div elements and text spans.
     * Avoiding reformatting of document, because this function
     * is only called when preparing document for local storage.
     *
     * @param {jQuery} node
     *  The jQuerified DOM node from which the class shall be removed.
     */
    clearSpellcheckHighlighting(node, dataObject) {
        // removing classes, multi selection possible -> no 'else if'
        if (node.is('span.' + SPELLERRORNODE_CLASS)) {
            // remove spellcheck attribute and class
            // not reformatting during closing the document via 'this.clearSpellErrorAttributes(node)'
            node.removeClass(SPELLERRORNODE_CLASS);
            if (dataObject && dataObject.attributes && dataObject.attributes.character && dataObject.attributes.character.spellerror) { delete dataObject.attributes.character.spellerror; }
        } else if (node.is('div')) {
            // remove runtime classes and cached result data
            node.removeClass('selected p_spelled');
            this.clearSpellResultCache(node);
        }
    }

    /**
     * Removes the character attribute 'spellerror', so that
     * the dotted underline of a text span disappears.
     *
     * @param {Node|jQuery} span
     *  The DOM node from which the attribute shall be removed.
     */
    clearSpellErrorAttributes(span) {

        // remove spell attributes - if there are any
        this.docModel.characterStyles.setElementAttributes(span, { character: { spellerror: null } });
    }

    /**
     * Toggle automatic spell checking in the document.
     */
    setOnlineSpelling(state) {
        if (!this.#spellingSupported) {
            return;
        }
        if (state !== this.isOnlineSpelling()) {
            spellingSettings.enableSpelling(state);
        }
    }

    /**
     * Checking, if spell checking is enabled.
     *
     * @returns {Boolean}
     *  Whether spellcheck in config and in user-hud is enabled
     */
    isOnlineSpelling() {
        return this.#spellingSupported && spellingSettings.isSpellingEnabled();
    }

    /**
     * Checking, whether a specified paragraph has cached spell results
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node, that is checked for existing spell results.
     *
     * @returns {Boolean}
     *  Whether a specified paragraph has cached spell results
     */
    hasSpellResultCache(paragraph) {
        return $(paragraph).data(SpellChecker.SPELL_RESULT_CACHE);
    }

    /**
     * Clearing the spell result cache at a specified paragraph
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node, whose cached spell results will be removed.
     */
    clearSpellResultCache(paragraph) {
        $(paragraph).removeData(SpellChecker.SPELL_RESULT_CACHE);
    }

    /**
     * Add the word to the list of words which are ignored by the spellchecker.
     * ignoreWordAction.start and ignoreWordAction.end or ignoreWordAction.nodes must be set.
     * To add the word to the dictionary, the "igonreWordAction.updateUserDict" option can be
     * set to true, to not only save the word temporarly.
     *
     * @param {Object} ignoreWordAction The action for add or remove a ignore word.
     * @param {String} ignoreWordAction.word The word for add or remove.
     * @param {Node[]} [ignoreWordAction.nodes] The nodes of the selected word. It is set if the function is called from the ContextMenu.
     * @param {Array<Number>} [ignoreWordAction.start] The logical start index of the selected word. It is set if the function is called from Undo/Redo
     * @param {Array<Number>} [ignoreWordAction.end] The logical end index of the selected word. It is set if the function is called from Undo/Redo
     * @param {String} ignoreWordAction.target the target id of the word element(s)
     * @param {Boolean} [ignoreWordAction.updateUserDict=false] if the word is for the user dictionary, otherwise ignore the word temporarly.
     */
    addWordToIgnoreSpellcheck(ignoreWordAction) {
        this.#restoreTextSelection();
        if (ignoreWordAction) {
            let word = getStringOption(ignoreWordAction, 'word', '');
            word = word.trim();
            const target = getStringOption(ignoreWordAction, 'target', null);
            const updateUserDict = ignoreWordAction.updateUserDict;

            if (word && !this.isIgnoredWord(word) && target !== null && ignoreWordAction.nodes && ignoreWordAction.start && ignoreWordAction.end) {

                this.#addIgnoreWordAction({ word, nodes: ignoreWordAction.nodes, target, updateUserDict }, true);

                this.#undoManager.addUndo(() => {
                    this.#addIgnoreWordAction({ word, remove: true, start: ignoreWordAction.start, end: ignoreWordAction.end, target, updateUserDict });
                }, () => {
                    this.#addIgnoreWordAction({ word, remove: false, start: ignoreWordAction.start, end: ignoreWordAction.end, target, updateUserDict });
                });
            }
        }
    }

    /**
     * Check if the word is in the list of ignored misspelled words.
     *
     * @param {String} word the word to check
     * @returns {Boolean} true if the word is on the list, otherwise false
     */
    isIgnoredWord(word) {
        return _.contains(this.#ignoredMisspelledWords, word);
    }

    /**
     * Test if the node contains the given class.
     *
     * @param {Node} node for test.
     * @returns {Boolean} true if the node contains the class, otherwise false.
     */
    hasSpellerrorClass(node) {
        return $(node).hasClass(SPELLERRORNODE_CLASS);
    }

    selectMisspelledWord(contextmenu) {

        const selection = this.docModel.getSelection();
        const paraPosition = selection.getStartPosition();
        const startEndIndex = getWordSelection(selection.getEnclosingParagraph(), paraPosition[paraPosition.length - 1]);
        const startPosition = _.clone(paraPosition);
        const endPosition = _.clone(paraPosition);

        this.#restoreStartPosition = _.clone(selection.getStartPosition());

        startPosition[endPosition.length - 1] = startEndIndex.start;
        endPosition[endPosition.length - 1] = startEndIndex.end;

        selection.setTextSelection(startPosition, endPosition);

        contextmenu.one('popup:beforehide', () => { this.#restoreTextSelection(); });
    }

    // private methods ----------------------------------------------------

    /**
     * Starting spell checking by calling the worker function 'this.#implCheckSpelling'.
     * Info: Not executing this.#implCheckSpelling, if the document is closed (this.docApp.isInQuit()).
     * It can happen, that operations are sent to the server during closing and a following
     * event 'operations:success' triggers 'this._implStartOnlineSpelling'. But this should
     * not be executed anymore and even causes problems when saving document in local storage.
     */
    @debounceMethod({ delay: 1000 })
    _implStartOnlineSpelling() {
        // # 51390: Spellchecking can interrupt an active composition by manipulating
        // DOM nodes. Make sure that this doesn't happen.
        if (this.isOnlineSpelling() && this.docApp.isEditable() && !this.docApp.isInQuit() && (_.isFunction(this.docModel.isImeActive) && !this.docModel.isImeActive()) && this.isImportFinished()) {
            this.#implCheckSpelling();
        }
    }

    /**
     * Receiving a list of replacement words for a specified word in a specified language from
     * the global replacement collection.
     *
     * @param {String} word
     *  The word to be replaced.
     *
     * @param {String} language
     *  The language of the word to be replaced.
     *
     * @returns {String[]}
     *  An array containing all replacement words for the specified word in the specified language.
     *  If there is no replacement word, the array will be empty.
     */
    #getSpellReplacements(word, language) {
        return this.#spellReplacementsByLocale[language] ? this.#spellReplacementsByLocale[language][word] : [];
    }

    /**
     * Checking, whether two different spell results are identical. A spell check result is an array containing
     * spell check error objects. One object has the following format:
     * Example: {"start":0,"length":2,"replacements":["da","dB","DSL"],"locale":"de_DE","word":"ds"}
     *
     * @param {Object[]} result1
     *  An spell check result container.
     *
     * @param {Object[]} result2
     *  Another spell check result container.
     *
     * @returns {Number}
     *  The number of the first difference. If number is '-1', there is no difference.
     */
    #sameSpellResults(result1, result2) {

        let sameResults = 0;
        let counter = 0;
        const length1 = result1.length;
        const length2 = result2.length;
        const minLength = Math.min(length1, length2);
        let sameError = minLength > 0;

        // comparing two single result objects
        const compareResultObjects = (res1, res2) => {
            return res1.start === res2.start && res1.length === res2.length && res1.word === res2.word && res1.locale === res2.locale;
        };

        while (sameError && result1[counter] && result2[counter]) {

            sameError = compareResultObjects(result1[counter], result2[counter]);

            if (sameError) {
                // comparing the next error objects
                counter++;
            } else {
                // finding the first different position
                sameResults = Math.min(result1[counter].start, result2[counter].start);
            }
        }

        // reaching the end of at least one error array
        if (sameError) {
            if (length1 === length2) {
                // result arrays are identical
                sameResults = -1;
            } else {
                // using the previous error
                counter--;
                // increasing the minimum position for the same results
                sameResults = result1[counter].start + result1[counter].length;
            }
        }

        return sameResults;
    }

    /**
     * Try to merge a span, that is no longer marked with spell error attribute. This can
     * happen for example after inserting a missing letter or after removing a superfluous
     * letter, so that several different text span can be merged.
     *
     * @param {Node[]|jQuery} spans
     *  The list of spans, that need to be checked, if they can be merged with their neighbors.
     */
    #mergeNonSpellErrorSpans(spans) {
        if (spans && _.isNumber(spans.length) && spans.length > 0) {
            _.each(spans, span => {
                if (!isSpellerrorNode(span)) {
                    mergeSiblingTextSpans(span);
                    mergeSiblingTextSpans(span, true);
                }
            });
        }
    }

    /**
     * Removing all spell errors inside a paragraph
     *
     * @param {Node|jQuery} paragraph
     *  The DOM node from which the class shall be removed.
     */
    #removeAllSpellErrorsInParagraph(paragraph) {

        // a container for all spans with spell error
        const allSpans = $(paragraph).find('span.spellerror');

        _.each(allSpans, span => {
            this.clearSpellErrorAttributes(span); // remove spell attributes - if there are any
        });

        this.docModel.trigger('paragraph:spellcheck:before', $(paragraph));

        // try to merge with neighbors after removing the spell check attribute
        this.#mergeNonSpellErrorSpans(allSpans);

        this.docModel.trigger('paragraph:spellcheck:after', $(paragraph));
    }

    /**
     * Helper function that applies the results received from the server to the DOM.
     *
     * @param {Object[]} spellResult
     *  A collection of spell results for the specified paragraph received from the server.
     *
     * @param {Node[]|jQuery} textSpans
     *  The collected text spans inside the specified paragraph.
     *
     * @param {Number} minPos
     *  The minimum position that will be evaluated inside the paragraph.
     */
    #applySpellcheckResults(spellResult, textSpans, minPos) {

        // a collector for all 'error spans'
        const errorSpanArray = [];
        // the offset of the span inside the paragraph
        let spanOffset = 0;
        // the length of a text span
        let spanLength;
        // a collector for text spans, that can maybe be merged later
        const allClearedSpans = [];

        // saving the results in the global collector for all replacements
        _(spellResult).each(result => {
            if (result.replacements && result.locale && result.word) {
                if (!this.#spellReplacementsByLocale[result.locale]) {
                    this.#spellReplacementsByLocale[result.locale] = { };
                }
                this.#spellReplacementsByLocale[result.locale][result.word] = result.replacements;
            }
        });

        // iterate over all text spans to generate an array for the error spans
        _(textSpans).each(currentSpan => {

            let result;
            const localErrors = [];
            let ignoreError = false;
            const isSpanInHardBreak = isHardBreakNode(currentSpan.parentNode); // special hardbreak handling (DOCS-3132)

            spanLength = isSpanInHardBreak ? 1 : currentSpan.firstChild.nodeValue.length;

            if (isSpanInHardBreak || isTabNode(currentSpan.parentNode)) { // spans inside tabs or hard breaks -> no spell errors (DOCS-3252)
                ignoreError = true;
            }

            // ignore all spans at positions left of the minimal position
            if (spanOffset + spanLength < minPos) {
                ignoreError = true;
            }

            if (!ignoreError) {

                // removing an existing spell-error attribute from the text span
                if ($(currentSpan).hasClass(SPELLERRORNODE_CLASS)) {
                    // clearing existing spell-error attributes from text spans behind 'minpos'
                    this.clearSpellErrorAttributes(currentSpan);
                    // collecting this span to enable later merging of text spans
                    allClearedSpans.push(currentSpan);
                }

                // iterating over all errors, to find precise position inside the current text span
                for (result = 0; result < spellResult.length; ++result) {

                    let currErrorStart = spellResult[result].start;
                    let currErrorLength = spellResult[result].length;
                    const maxError = currErrorStart + currErrorLength;
                    let minError = currErrorStart;

                    if (maxError <= spanOffset) {
                        continue;
                    }

                    if (minError >= spanOffset + spanLength) {
                        break;
                    }

                    if (spellResult[result].word && this.isIgnoredWord(spellResult[result].word.slice(0, currErrorLength))) {
                        continue;
                    }

                    if (minError < spanOffset) {
                        currErrorLength -= spanOffset - minError;
                        minError = 0;
                    }

                    currErrorStart = (minError <= spanOffset) ? 0 : (minError - spanOffset);

                    if (maxError > spanOffset + spanLength) {
                        currErrorLength = spanLength - (currErrorStart - spanOffset);
                    }

                    // saving start and length of error range
                    localErrors.push({ start: currErrorStart, length: currErrorLength });
                }

                // collecting all error ranges for the current span
                if (localErrors.length > 0) {
                    errorSpanArray.push({ span: currentSpan, errors: localErrors });
                }
            }

            spanOffset += spanLength;
        });

        // iterating over all error spans, to make them visible
        _.each(errorSpanArray, errorSpan => {

            let spanPositionOffset = 0;
            let newSpan;
            let currentSpan = errorSpan.span;

            _.each(errorSpan.errors, localError => {

                newSpan = null;

                if (localError.start > spanPositionOffset && (localError.start - spanPositionOffset) < currentSpan.textContent.length) {
                    splitTextSpan(currentSpan, localError.start - spanPositionOffset);
                    spanPositionOffset += localError.start - spanPositionOffset;
                }

                // split end of text span NOT covered by the error
                if (localError.length > 0 && currentSpan.textContent.length > localError.length) {
                    newSpan = splitTextSpan(currentSpan, localError.length, { append: true });
                    spanPositionOffset += localError.length;
                }

                // setting the 'spellerror' attribute to the span, so that it will be underlined
                this.docModel.characterStyles.setElementAttributes(currentSpan, { character: { spellerror: true } });

                if (newSpan !== null) {
                    currentSpan = newSpan[0];
                }
            });
        });

        // try to merge collected text spans with their neighbors after removing the spell check attribute
        this.#mergeNonSpellErrorSpans(allClearedSpans);
    }

    /**
     * The handler function, that evaluates the results received from the server.
     *
     * @param {Object[]} spellResult
     *  A collection of spell results for the specified paragraph received from the server.
     *
     * @param {Node[]|jQuery} textSpans
     *  The collected text spans inside the specified paragraph.
     *
     * @param {Node} paragraph
     *  The paragraph, to which the spell results will be applied.
     */
    @textLogger.profileMethod("Spellchecker.spellResultHandler()")
    _spellResultHandler(spellResult, textSpans, paragraph) { // TODO: Rename to really private "#spellResultHandler" when decorators are natively supported

        // the first position with a changed spell check error
        let firstChangedPos = 0;
        // the spell check results of a previous spell check process
        let oldSpellResult = null;
        // the selection object
        const selection = this.docModel.getSelection();

        // convert paragraph to jQuery Object
        paragraph = $(paragraph);

        // do nothing, if the paragraph was modified in the meantime (before the answer was received)
        if (!paragraph.hasClass('p_spelled')) {
            return;
        }

        // checking for the old spelling results
        oldSpellResult = paragraph.data(SpellChecker.SPELL_RESULT_CACHE);

        // handling empty result array (will be handled in fail handler)
        if (spellResult.length === 0) {
            if (oldSpellResult) {
                // no errors now, but some errors before
                this.#removeAllSpellErrorsInParagraph(paragraph);
                this.clearSpellResultCache(paragraph);
            } else {
                // no errors now and no errors before
                return;
            }
        }

        // finding differences to a previous spell check process
        if (oldSpellResult) {
            // do nothing, if the spell results have not changed
            firstChangedPos = this.#sameSpellResults(spellResult, oldSpellResult);
            if (firstChangedPos === -1) {
                return;
            }
        }

        this.docModel.trigger('paragraph:spellcheck:before', paragraph);

        // applying the results received from the server
        // -> the array with textSpans is still valid, because 'p_spelled' is still set at the paragraph
        this.#applySpellcheckResults(spellResult, textSpans, firstChangedPos);

        // saving spell results at the paragraph to compare it with the next spell check results
        paragraph.data(SpellChecker.SPELL_RESULT_CACHE, spellResult);

        // invalidate an existing cached text point
        selection.setInsertTextPoint(null);

        // restore the selection, but not if a drawing or a table cell is selected (Task 26214)
        if (this.docApp.getView().hasAppFocus() && this.docModel.isTextOnlySelected()) {
            selection.restoreBrowserSelection({ preserveFocus: true, keepFocus: this.docModel.forceToKeepFocus() });
        }

        // trigger information about paragraph modified by spellchecking
        this.docModel.trigger('paragraph:spellcheck:after', paragraph);
    }

    #wordIterator(paraSpans, textSpans) {

        return node => {

            // avoiding spellchecking of pagebreak nodes inside spellchecked paragraphs (DOCS-3312)
            if (isPageBreakNode(node)) { return; }

            if (isHardBreakNode(node)) { // special hardbreak handling (DOCS-3132) -> simulating a space of length 1 for the backend
                const span = node.firstChild;
                textSpans.push(span);
                paraSpans.push({ word: " ", locale: this.docModel.characterStyles.getElementAttributes(span).character.language });
                return;
            }

            // iterating over all text spans
            iterateTextSpans(node, span => {

                if (!isListLabelNode(span.parentNode)) {

                    const charAttributes = this.docModel.characterStyles.getElementAttributes(span);
                    if (charAttributes.character.url === '') {

                        // only supported language spans will be sent to the server
                        if (_.contains(this.#supportedLanguages, charAttributes.character.language)) {
                            textSpans.push(span);
                            paraSpans.push({ word: span.firstChild.nodeValue, locale: charAttributes.character.language });
                        } else {
                            this.clearSpellErrorAttributes(span); // remove spell attributes - if there are any
                            this.#addUnsupportedLanguageForNotification(charAttributes.character.language);
                        }

                    } else {
                        // In case the <span> is part of an URL it needs to be explicitely cleared from spelling error indicators,
                        // if there is another spelling error in the current paragraph.
                        // See bug 63690 for further information.
                        this.clearSpellErrorAttributes(span);
                    }
                }
            });

        };
    }

    /**
     * The worker function, that checks the paragraphs, generates server calls and
     * applies the response from the server to the paragraphs.
     */
    #implCheckSpelling() {

        // the vertical offset of the specific node
        let topOffset = 0;
        // the maximum offset for paragraphs that will be spell checked (one and a half screen downwards)
        const maxOffset = 1.5 * (this.#scrollNode.height() + this.#scrollNode.offset().top);
        // the minimum offset for paragraphs that will be spell checked (a half screen upwards)
        const minOffset = -0.3 * maxOffset;
        // container of the paragraph if presentation only the paragraphs of the active slide
        const paragraphContainer = this.docModel.useSlideMode() ? this.docModel.getSlideById(this.docModel.getActiveSlideId()) : this.docModel.getNode();
        // iterating over all paragraphs, that are not marked with class 'p_spelled'. If the paragraph cache was filled, use it
        const allParagraphs = paragraphContainer === null ? [] : this.#paragraphCache.length > 0 ? this.#paragraphCache : paragraphContainer.find(PARAGRAPH_NODE_SELECTOR).not('.p_spelled');

        // Slicing, so that there are not too many parallel server calls
        // -> it is useful to profile the sliced function using 'profileMethod' in following performance tests.
        const promise = this.docModel.iterateArraySliced(allParagraphs, paragraph => {

            // all text spans in the paragraph, as array
            let textSpans;
            // all spans of the paragraph - each span is a word/language object
            let paraSpans;
            // requestID
            let requestID;

            if ($(paragraph).hasClass(NO_SPELLCHECK_CLASSNAME) || $(paragraph).hasClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS)) {
                this.#removeAllSpellErrorsInParagraph(paragraph);
            } else if (!$(paragraph).hasClass('p_spelled')) {

                // calculating if this paragraph is in or near the visible region (for performance reasons)
                topOffset = Math.round($(paragraph).offset().top);

                // TODO: This check is not sufficient for long paragraphs
                if (topOffset < maxOffset && minOffset < topOffset) {

                    textSpans = [];
                    paraSpans = [];
                    // marking paragraph immediately as spell checked, so that directly following changes can be recognized
                    $(paragraph).addClass('p_spelled');

                    // collect all non-empty text spans in the paragraph
                    iterateParagraphChildNodes(paragraph, this.#wordIterator(paraSpans, textSpans), undefined, { allNodes: true });

                    // preparing server call for the paragraph, if text spans were found
                    if (paraSpans.length > 0) {
                        requestID = this.#requestIDCounter++;
                        $(paragraph).data(SpellChecker.SPELL_REQUEST_ID, requestID);
                        const request = this.docApp.sendRequest('spellchecker', { action: 'spellparagraph', paragraph: JSON.stringify(paraSpans) }, { method: 'POST' });
                        this.onFulfilled(request, data => {
                            if ($(paragraph).data(SpellChecker.SPELL_REQUEST_ID) !== requestID) { return false; }
                            // remove unused data from paragraph
                            $(paragraph).removeData(SpellChecker.SPELL_REQUEST_ID);

                            const spellResult = getArrayOption(data, 'spellResult', []);
                            if (spellResult.length === 0) {
                                if ($(paragraph).data(SpellChecker.SPELL_RESULT_CACHE) || $(paragraph).data(SpellChecker.HANDLE_EMPTY_SPELL_RESULT)) {
                                    // no errors now, but some errors before
                                    this.#removeAllSpellErrorsInParagraph(paragraph);
                                    this.clearSpellResultCache(paragraph);
                                }
                            } else {
                                this._spellResultHandler(spellResult, textSpans, paragraph);
                            }
                            $(paragraph).removeData(SpellChecker.HANDLE_EMPTY_SPELL_RESULT); // DOCS-3826
                        });
                    } else {
                        this.#removeAllSpellErrorsInParagraph(paragraph);
                    }

                    // - trigger dialog if unsupported languages without a notification exist.
                    // -> no notification in presentation mode (DOCS-3115)
                    if (this.#unsupportedLanguageWithoutNotification.length && !this.docModel.blockPopupMenus()) {
                        this._showUnsupportedLanguageNotification();
                    }
                }
            }
        });

        promise.always(() => { this.#paragraphCache = $(); });
    }

    /**
     * Initialization of spell checker after document is loaded successfully.
     */
    #documentLoaded() {

        // setting scroll node and register listener for 'scroll' events
        this.#scrollNode = this.docApp.getView().getContentRootNode();

        // online spelling
        const setOnlineSpellingLocal = state => {

            if (!this.#spellingSupported) { return; }
            this.docModel.getNode().toggleClass('spellerror-visible', state);

            this.#setEventListeners();

            if (this.isOnlineSpelling()) {
                // deleting the paragraph cache, so that all paragraphs are checked
                this.#paragraphCache = $();
                // start timeout handler
                this._implStartOnlineSpelling();
            } else {
                const allParagraphs = this.docModel.getNode().find(PARAGRAPH_NODE_SELECTOR + '.p_spelled');
                this.docModel.iterateArraySliced(allParagraphs, paragraph => {
                    this.#removeAllSpellErrorsInParagraph(paragraph);
                    $(paragraph).removeClass('p_spelled');
                    this.clearSpellResultCache(paragraph);
                });
                this.#paragraphCache = $();
            }
        };

        if (this.isOnlineSpelling()) {
            setOnlineSpellingLocal(true);
        }

        // listen to office settings changes from other tabs
        this.listenToGlobal("change:config:docs", event => {
            if (event.path === 'isCheckSpellingPermanently') {
                setOnlineSpellingLocal(event.value);
            }
        });
    }

    /**
     * Called if the 'save' event on UserDictionary occurred.
     *
     * @param {String} newWords
     *  The words which are added to the dictionary
     *
     * @param {boolean} removedWords
     *  The words which are removed from the dictionary
     */
    #onSaveUserDictionary(newWords, removedWords) {

        _.each(newWords, word => {
            if (!this.isIgnoredWord(word)) {
                this.#ignoredMisspelledWords.push(word);
                this.#changeParagraphSpelledAttributeForIgnoreWords(word, false);

                this.#userDictionaryChanges = true;
            }
        });
        _.each(removedWords, word => {
            if (this.isIgnoredWord(word)) {
                this.#ignoredMisspelledWords = _.without(this.#ignoredMisspelledWords, word);
                this.#changeParagraphSpelledAttributeForIgnoreWords(word, true);
                this.#userDictionaryChanges = true;
            }

        });
    }

    /**
     * Start adding or removing ignored words.
     */
    #implStartAddOrRemoveIgnoreWords() {
        if (this.isOnlineSpelling() && this.docApp.isEditable() && this.isImportFinished()) {
            while (this.#ignoreWordActions.length > 0) {
                this.#implAddWordToIgnoreSpellcheck(this.#ignoreWordActions.shift());
            }
        }
    }

    // A debounced function for starting adding or removing ignored words
    @debounceMethod({ delay: 500 })
    _implStartAddOrRemoveIgnoreWordsDebounced() {
        this.#implStartAddOrRemoveIgnoreWords();
    }

    /**
     * Add or remove a word to/from the ignore word list.
     * ignoreWordAction.node or ignoreWordAction.start/ignoreWordAction.end are optional BUT at least one must be set.
     * The 'spellerror' class of selected word (ignoreWordAction.node or ignoreWordAction.start/ignoreWordAction.end) will be added or removed synchronously,
     * the other word occurrence will be changed asynchronous.
     *
     * @param {Object} ignoreWordAction The action for add or remove a ignore word.
     * @param {String} ignoreWordAction.word The word for add or remove.
     * @param {Boolean} [ignoreWordAction.remove=false] If true remove the word from the igonre words from spellcheck list.
     * @param {Node[]} [ignoreWordAction.nodes] The nodes of the selected word. It is set if the function is called from the ContextMenu.
     * @param {Array<Number>} [ignoreWordAction.start] The logical start index of the selected word. It is set if the function is called from Undo/Redo
     * @param {Array<Number>} [ignoreWordAction.end] The logical end index of the selected word. It is set if the function is called from Undo/Redo
     * @param {String} ignoreWordAction.target the target id of the word element(s)
     */
    #implAddWordToIgnoreSpellcheck(ignoreWordAction) {

        const remove = getBooleanOption(ignoreWordAction, 'remove', false);
        const word = ignoreWordAction.word.trim();
        let paragraph;
        let startNode;
        const updateUserDict = ignoreWordAction && ignoreWordAction.updateUserDict;

        if (!word || (!ignoreWordAction.nodes && !ignoreWordAction.start && !ignoreWordAction.end)) {
            return;
        }

        if (remove && this.isIgnoredWord(word)) {
            this.#ignoredMisspelledWords = _.without(this.#ignoredMisspelledWords, word);
            if (updateUserDict) {
                userDictionary.removeWord(word);
            }
        } else if (!remove && !this.isIgnoredWord(word)) {
            this.#ignoredMisspelledWords.push(word);
            if (updateUserDict) {
                userDictionary.addWord(word);
            }
        } else {
            return;
        }

        if (ignoreWordAction.nodes) {
            _.each(ignoreWordAction.nodes, node => {

                $(node).toggleClass(SPELLERRORNODE_CLASS, remove);

                if (!remove) {
                    this.clearSpellErrorAttributes(node);
                }
            });
        } else {

            startNode = getDOMPosition(this.docModel.getRootNode(ignoreWordAction.target), ignoreWordAction.start, true);

            if (startNode) {

                paragraph = startNode.node.parentNode;

                this.resetDirectly(paragraph);
                this.clearSpellResultCache(paragraph);

                iterateParagraphChildNodes(paragraph, node => {
                    $(node).toggleClass(SPELLERRORNODE_CLASS, remove);
                    if (!remove) {
                        this.clearSpellErrorAttributes(node);
                    }
                }, undefined, {
                    allNodes: false,
                    start: _(ignoreWordAction.start).last(),
                    end: _(ignoreWordAction.end).last(),
                    split: true
                });

            }
        }
        this.#changeParagraphSpelledAttributeForIgnoreWords(word, remove);
    }

    /**
     * Remove the p_spelled class from paragraphs which includes the given word to test it with the spellchecker again.
     *
     * @param {String} ignoreWord
     *  the word to check
     *
     * @param {Boolean} remove
     *  true if the word is removed from the ignore list, otherwise it is added to the ignore list.
     */
    #changeParagraphSpelledAttributeForIgnoreWords(ignoreWord, remove) {

        const word = ignoreWord.toLowerCase();
        const paragraphs = this.docModel.getNode().find(PARAGRAPH_NODE_SELECTOR + '.p_spelled');

        const promise = this.docModel.iterateArraySliced(paragraphs, paragraph => {
            if ((remove || this.hasSpellResultCache(paragraph)) && $(paragraph).text().toLowerCase().indexOf(word) >= 0) {
                this.resetDirectly(paragraph);
                this.clearSpellResultCache(paragraph);
            }
        });

        promise.always(() => {
            this.#paragraphCache = $();
            this._implStartOnlineSpelling();
        });
    }

    /**
     * Add a ignore word add or remove action to the ignore words stack.
     * action.start and action.end or action.nodes must be set.
     *
     * @param {Object} action The action for add or remove a ignore word.
     * @param {String} action.word The word for add or remove.
     * @param {Boolean} [action.remove=false] If true remove the word from the igonre words from spellcheck list.
     * @param {Node[]} [action.nodes] The nodes of the selected word. It is set if the function is called from the ContextMenu.
     * @param {Array<Number>} [action.start] The logical start index of the selected word. It is set if the function is called from Undo/Redo
     * @param {Array<Number>} [action.end] The logical end index of the selected word. It is set if the function is called from Undo/Redo
     * @param {String} action.target the target id of the word element(s)
     * @param {Boolean} [immediate = false] execute the action immediate if true, otherwise debounced.
     */
    #addIgnoreWordAction(action, immediate) {
        this.#ignoreWordActions.push(action);
        if (immediate) {
            this.#implStartAddOrRemoveIgnoreWords();
        } else {
            this._implStartAddOrRemoveIgnoreWordsDebounced();
        }
    }

    /**
     * Add the language to the language list without a notification. This languages will be shown in a notification.
     *
     * @param {String} lang the language local e.g. en-US
     */
    #addUnsupportedLanguageForNotification(lang) {
        if (
            !this.#unsupportedLanguages.includes(lang) &&
            !this.#unsupportedLanguageWithoutNotification.includes(lang) &&
            !spellingSettings.isSpellingNotificationBlocked(lang)
        ) {
            this.#unsupportedLanguageWithoutNotification.push(lang);
        }
    }

    /**
     * Shows a notification with the unsupported but used languages.
     * The dialog appears only once for each language.
     */
    @debounceMethod({ delay: 1000 })
    _showUnsupportedLanguageNotification() {

        // see: https://bugs.open-xchange.com/show_bug.cgi?id=50880
        //
        // - does prevent raising the dialog right after having opened an entirely empty page.
        // - does work with guarding flag since `this.docModel.isEmptyPage()` is quite expensive and will be invoked otherwise every second.
        const isPageConsideredEmpty = this.#hasPageAlwaysBeenEmpty && (this.#hasPageAlwaysBeenEmpty = this.docModel.isEmptyPage());
        const isLanguageNotification = this.#unsupportedLanguageWithoutNotification.length > 0;

        if (!isPageConsideredEmpty && isLanguageNotification) {

            // get available language names for the alert message
            const langNames = this.#unsupportedLanguageWithoutNotification.map(id => LOCALE_DATA.getLanguageAndRegionName(id, true)).filter(Boolean).join(', ');

            // immediately save any newly detected language into the spell-check language allowlist.
            this.#unsupportedLanguages.push(...this.#unsupportedLanguageWithoutNotification);
            spellingSettings.addLanguagesWithoutSpelling(this.#unsupportedLanguages, true);
            this.#unsupportedLanguageWithoutNotification = [];

            if (langNames) {
                this.docApp.getView().yell({
                    message: gt('This document contains text in languages which could not be proofed: %1$s', langNames),
                    action: {
                        itemKey: 'document/settings/missingspelling/dialog',
                        //#. Button label for open the user spell-check languages dialog
                        label: gt('Set language notification.'),
                        icon: 'bi:gear'
                    }
                });
            }
        }
    }

    #restoreTextSelection() {
        if (this.#restoreStartPosition) {
            const selection = this.docModel.getSelection();
            selection.setTextSelection(this.#restoreStartPosition, this.#restoreStartPosition);
            this.#restoreStartPosition = null;
            this.docApp.getView().grabFocus();
        }
    }

    /**
     * Set or remove the event listeners
     */
    #setEventListeners() {

        if (this.isOnlineSpelling()) {
            // after receiving edit privileges it is necessary to check the document
            this.listenTo(this.docApp, 'docs:editmode:enter', this._implStartOnlineSpelling);
            this.listenTo(this.docModel, 'operations:success', this._implStartOnlineSpelling);

            this.listenTo(this.#scrollNode, 'scroll', this._implStartOnlineSpelling);

            // listen if the user update the user dictionary
            this.listenTo(userDictionary, 'save', this.#onSaveUserDictionary);

            // listen if the app will be visible. It's needed if e.g. the user change the dictionary in the settings and go back to the document,
            // now the spellchecker must start to check the text with the updated dictionary.
            this.listenTo(this.docApp.getWindow(), 'show', this.#appVisibleHandler);
        } else {
            this.stopListeningTo(this.docApp, 'docs:editmode:enter', this._implStartOnlineSpelling);
            this.stopListeningTo(this.docModel, 'operations:success', this._implStartOnlineSpelling);
            this.stopListeningTo(this.#scrollNode, 'scroll', this._implStartOnlineSpelling);
            this.stopListeningTo(userDictionary, 'save', this.#onSaveUserDictionary);
            this.stopListeningTo(this.docApp.getWindow(), 'show', this.#appVisibleHandler);
        }
    }

    /**
     * After the event 'document:reloaded' was fired by the docModel, some cached nodes and caches
     * need to be invalidated.
     */
    #handleDocumentReload() {
        // invalidating paragraph cache, after the document was reloaded
        this.#paragraphCache = $();
    }

    #appVisibleHandler() {
        // "this.#userDictionaryChanges" is true, if the user changes the dictionary while the spellchecker document is not visible.
        if (this.#userDictionaryChanges) {
            this.#userDictionaryChanges = false;
            this._implStartOnlineSpelling();
        }
    }
}

// constants --------------------------------------------------------------

/**
 * The name of the jQuery data key that is used as cache for the spellcheck results.
 */
SpellChecker.SPELL_RESULT_CACHE = 'spellresult';

/**
 * The name of the jQuery data key that is used as marker for forced spellcheck result
 * evaluation (required only, if the result is empty).
 */
SpellChecker.HANDLE_EMPTY_SPELL_RESULT = 'handleemptyresult';

/**
 * The name of the jQuery data key that is used to add a request sendID at the paragraph.
 * After changing the paragraph while the request is not complete the id will be
 * removed or a second request change the id then is the request is not valid. If another
 * request is started for the paragraph the will be changed and the requests before are
 * no more valid.
 */
SpellChecker.SPELL_REQUEST_ID = '';

// exports ================================================================

export default SpellChecker;
