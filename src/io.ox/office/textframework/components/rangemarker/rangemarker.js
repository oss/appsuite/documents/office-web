/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { DELETE, RANGE_INSERT, comparePositions } from '@/io.ox/office/textframework/utils/operations';
import { convertCssLength, findNextSiblingNode, getBooleanOption, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';
import { RANGEMARKER_ENDTYPE_CLASS, RANGEMARKER_ENDTYPE_SELECTOR, RANGEMARKER_STARTTYPE_CLASS, RANGEMARKER_STARTTYPE_SELECTOR,
    RANGEMARKERNODE_CLASS, RANGEMARKERNODE_SELECTOR, RANGEMARKEROVERLAYNODE_CLASS, getComplexFieldInstruction, getPageContentNode,
    getRangeMarkerId, getRangeMarkerType, getTargetContainerId, isChangeTrackNode, isCommentPlaceHolderNode, isEmptySpan, isFooterWrapper,
    isListLabelNode, isMarginalNode, isParagraphNode, isRangeMarkerEndNode, isRangeMarkerStartNode, isSpecialField } from '@/io.ox/office/textframework/utils/dom';
import { decreaseLastIndex, getOxoPosition, hasSameParentComponent, getPixelPositionToRootNodeOffset } from '@/io.ox/office/textframework/utils/position';

// constants ==============================================================

/**
 * A marker class to identify highlight elements introduced by range marker nodes.
 */
export const RANGEMARKER_CLASS = "isRangeMarker";

/**
 * A selector to identify highlight elements introduced by range marker nodes.
 */
export const RANGEMARKER_SELECTOR = '.' + RANGEMARKER_CLASS;

/**
 * The minimum width of a highlighted range in pixel.
 */
export const MIN_HIGHLIGHT_RANGE_WIDTH = 2;

// class RangeMarker ======================================================

/**
 * An instance of this class represents the model for all markers in the
 * edited document.
 *
 * @param {TextModel} docModel
 *  The text model instance.
 */
class RangeMarker extends ModelObject {

    // an object with all range start markers (jQuery)
    #allStartMarkers = {};
    // an object with all range end markers (jQuery)
    #allEndMarkers = {};
    // the overlay node that is used to visualize the ranges
    #rangeOverlayNode = null;
    // whether the range marker model was refreshed after the loading phase
    #rangeMarkerModelRefreshed = false;
    // a collector for the temporary inserted nodes (performance)
    #temporaryRangeMarkerNodes = {};

    constructor(docModel) {

        // base constructor
        super(docModel);

        // initialization -----------------------------------------------------

        this.docApp.onInit(() => {

            this.waitForImportSuccess(() => {
                if ((!this.#rangeMarkerModelRefreshed) && (this.docModel.isLocalStorageImport() || this.docModel.isFastLoadImport())) { this.#refreshRangeMarker(); }
            });

            this.listenTo(this.docModel, 'update:absoluteElements update:rangeMarkers', this.updateHighlightedRanges);
            this.listenTo(this.docModel, 'document:reloaded', this.#refreshRangeMarker);
        });
    }

    // methods ------------------------------------------------------------

    /**
     * Whether the document contains range markers in the main document.
     *
     * @returns {Boolean}
     *  Whether the document contains at least one range marker.
     */
    isEmpty() {
        return _.isEmpty(this.#allStartMarkers) && _.isEmpty(this.#allEndMarkers);
    }

    /**
     * Check, whether the specified id is a valid id of a range start marker.
     *
     * @param {String} id
     *  The id string.
     *
     * @returns {Boolean}
     *  Whether the specified id string is a valid id of a range start marker node.
     */
    isStartMarkerId(id) {
        return this.getStartMarker(id) !== null;
    }

    /**
     * Check, whether the specified id is a valid id of a range end marker.
     *
     * @param {String} id
     *  The id string.
     *
     * @returns {Boolean}
     *  Whether the specified id string is a valid id of a range end marker node.
     */
    isEndMarkerId(id) {
        return this.getEndMarker(id) !== null;
    }

    /**
     * Check, whether the specified id is a valid id for a range marker.
     *
     * @param {String} id
     *  The id string.
     *
     * @returns {Boolean}
     *  Whether the specified id is a valid id of a range node.
     */
    isRangeMarkerId(id) {
        return this.isStartMarkerId(id) || this.isEndMarkerId(id);
    }

    /**
     * Whether there are highlighted ranges.
     *
     * @returns {Boolean}
     *  Whether there is at least one highlighted range.
     */
    isRangeHighlighted() {
        return this.#rangeOverlayNode && this.#rangeOverlayNode.children.length > 0;
    }

    /**
     * Provides a jQuerified range start marker node.
     *
     * @returns {jQuery|null}
     *  The start marker with the specified id, or null, if no such start marker exists.
     */
    getStartMarker(id) {

        // the value saved under the specified id
        const marker = this.#allStartMarkers[id] || null;

        return _.isString(marker) ? this.#getMarginalRangeMarker(id, marker, RANGEMARKER_STARTTYPE_SELECTOR) : marker;
    }

    /**
     * Provides a jQuerified range end marker node.
     *
     * @returns {jQuery|null}
     *  The end marker with the specified id, or null, if no such end marker exists.
     */
    getEndMarker(id) {

        // the value saved under the specified id
        const marker = this.#allEndMarkers[id] || null;

        return _.isString(marker) ? this.#getMarginalRangeMarker(id, marker, RANGEMARKER_ENDTYPE_SELECTOR) : marker;
    }

    /**
     * Whether the specified id is already used by a range marker node.
     *
     * @returns {Boolean}
     *  Returns whether the specified id is already used by a range marker node.
     */
    isUsedRangeMarkerId(id) {
        return (this.getStartMarker(id) !== null) || (this.getEndMarker(id) !== null);
    }

    /**
     * Provides the range overlay node, that contains the nodes, that are used to
     * visualize the ranges.
     *
     * @returns {jQuery|null}
     *  The range overlay node, that can be used to visualize the ranges.
     */
    getRangeOverlayNode() {
        return this.#rangeOverlayNode;
    }

    /**
     * In the loading phase, other components like comments or complex fields,
     * need to rely on an up-to-date model for the range markers. Therefore
     * these other components can trigger an update of the model by calling
     * this function.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.onlyOnce=false]
     *      If set to true, the model will only be refreshed, if it was not
     *      refreshed before.
     */
    refreshModel(options) {

        // whether the model needs to be refreshed only once (if it was not refreshed before)
        const onlyOnce = getBooleanOption(options, 'onlyOnce', false);

        if (!(onlyOnce && this.#isRangeMarkerModelRefreshed())) {
            this.#refreshRangeMarker();
        }
    }

    /**
     * Removing one range marker with specified id from range marker collections.
     *
     * @param {String} marker
     *  The unique id of the range marker node.
     */
    removeRangeMarker(marker) {

        // the unique id of the range node
        const rangeId = getRangeMarkerId(marker);

        if (_.isString(rangeId)) {
            this.#removeFromRangeMarkerCollection(marker, rangeId);
        }
    }

    /**
     * Handler for inserting a range marker node into the document DOM. These ranges can be
     * used for several features like comments, complex fields, ...
     *
     * @param {Number[]} start
     *  The logical start position for the new range marker.
     *
     * @param {String} id
     *  The unique id of the range marker. This can be used to identify the
     *  corresponding node, that requires this range. This can be a comment
     *  node for example.
     *
     * @param {String} type
     *  The type of the range marker.
     *
     * @param {String} position
     *  The position of the range marker. Currently supported are 'start'
     *  and 'end'.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new range component, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {String} [target]
     *  The target corresponding to the specified logical start position.
     *
     * @param {Object} [nodeCollector]
     *  An optional collector for the range marker nodes.
     *
     * @returns {Boolean}
     *  Whether the range marker has been inserted successfully.
     */
    insertRangeHandler(start, id, type, position, attrs, target, nodeCollector) {

        // whether this is a range marker for the start or the end of a range
        const rangeMarkerTypeClass = (position === 'end') ? RANGEMARKER_ENDTYPE_CLASS : RANGEMARKER_STARTTYPE_CLASS;
        // the range marker element
        const rangeMarkerNode = $('<div>', { contenteditable: false }).addClass('inline ' + RANGEMARKERNODE_CLASS + ' ' + rangeMarkerTypeClass);
        // the text span needed for inserting the range marker node
        const textSpan = this.docModel.prepareTextSpanForInsertion(start, null, target);
        // if range belongs to complex field
        let field = null;
        // if range is at position start or end
        let rangeStart;
        let rangeEnd;
        // field instruction
        let instruction;

        if (!textSpan) { return false; }

        // if is range for type field, get id from stack
        if (type === 'field') {
            rangeEnd = position === 'end';
            rangeStart = !rangeEnd;
            id = this.docModel.getFieldManager().getComplexFieldIdFromStack(rangeStart, rangeEnd);
        }

        // No inheritance of change track to following empty text span node (this would not be resolved during change track resolving)
        if (isChangeTrackNode(textSpan) && textSpan.nextSibling && isEmptySpan(textSpan.nextSibling)) {
            this.docModel.characterStyles.setElementAttributes(textSpan.nextSibling, { changes: { inserted: null, removed: null, modified: null } });
        }

        // saving also type and id at the range marker node
        rangeMarkerNode.attr('data-range-type', type);
        rangeMarkerNode.attr('data-range-id', id);

        // splitting text span to insert range marker node
        rangeMarkerNode.insertAfter(textSpan);

        // adding inherited character attributes
        this.docModel.inheritInlineCharacterAttributes(rangeMarkerNode);

        // Adding attribute set to range marker node, if specified
        if (attrs) { this.docModel.characterStyles.setElementAttributes(rangeMarkerNode, attrs); }

        // it might be necessary to transfer the hard character attributes to a previous empty text span (DOCS-3244)
        if (attrs && attrs.character) { this.docModel.checkTransferOfInlineCharacterAttributes(rangeMarkerNode, attrs.character); }

        // adding the range marker node in the collection of all range markers
        // -> this also needs to be done after loading document from fast load or using local storage
        this.#addIntoRangeMarkerCollection(rangeMarkerNode, id, target);

        // saving the nodes in an optionally available node collector object
        if (nodeCollector) { nodeCollector[position] = rangeMarkerNode; }

        // complex fields highlight
        if (this.docModel.isImportFinished() && position === 'end' && type === 'field') {
            this.docModel.getFieldManager().markFieldForHighlighting(id);
            // converts page fields in header&footer to special fields
            if (target && this.docModel.getPageLayout().isIdOfMarginalNode(target)) {
                field = this.docModel.getFieldManager().getComplexField(id);
                instruction = getComplexFieldInstruction(field);
                if ((/PAGE/i).test(instruction) && !isSpecialField(field)) {
                    this.docModel.getFieldManager().convertToSpecialField(id, target, instruction);
                }
            }
        }

        return true;
    }

    /**
     * After splitting a paragraph, it is necessary, that all range markers in the cloned
     * 'new' paragraph are updated in the collectors.
     *
     * @param {Node|jQuery} para
     *  The paragraph node.
     */
    updateRangeMarkerCollector(para) {

        // whether each single node needs to be checked
        let checkMarginal = false;
        // all range markers inside the paragraph
        let allMarkers = null;

        // not necessary for paragraphs in header or footer -> only the target are stored, not the nodes
        if (isMarginalNode(para)) { return; }

        // if this is not a paragraph, each single node need to be checked
        checkMarginal = !isParagraphNode(para);

        // finding all markers inside the specified node
        allMarkers = $(para).find(RANGEMARKERNODE_SELECTOR);

        // update the marker nodes in the collection objects
        _.each(allMarkers, oneMarker => {
            if (!checkMarginal || !isMarginalNode(oneMarker)) {
                // simply overwriting the old markers with the new values
                this.#addIntoRangeMarkerCollection(oneMarker, getRangeMarkerId(oneMarker));
            }
        });
    }

    /**
     * After deleting a complete paragraph, it is necessary, that all range markers
     * in the deleted paragraph are also removed from the model collectors.
     *
     * @param {Node|jQuery} para
     *  The paragraph node.
     */
    removeAllInsertedRangeMarker(para) {

        // all range markers inside the paragraph
        const allMarkers = $(para).find(RANGEMARKERNODE_SELECTOR);

        // update the marker nodes in the collection objects
        _.each(allMarkers, marker => {

            // the id of the marker node
            const rangeId = getRangeMarkerId(marker);

            if (_.isString(rangeId)) {
                this.#removeFromRangeMarkerCollection(marker, rangeId);
            }
        });
    }

    /**
     * Highlighting the range with the specified unique id. It is required, that there is
     * a start marker and an end marker for this range. Otherwise a highlighting is not
     * possible and this function returns 'false'.
     * The type of highlighting can be specified optionally by assigning specified
     * class names to the generated 'div'-elements.
     *
     * @param {String} id
     *  The unique id of the range, that will be highlighted. To be highlighted it is
     *  necessary that there is an existing start range marker and end range marker
     *  corresponding to the specified id.
     *
     * @param {String} [highlightClassNames]
     *  One or more space-separated classes to be added to the class attribute of the
     *  generated 'div'-elements, that are used to highlight the range.
     *
     * @returns {Object}
     *  An object containing the properties x and y, that contain the horizontal and
     *  vertical position in pixel of the upper left corner of the range marker.
     */
    highLightRange(id, highlightClassNames) {

        // the range marker start node
        let startNode = this.getStartMarker(id);
        // the range marker end node
        const endNode = this.getEndMarker(id);
        // the current zoom factor
        const zoomFactor = this.docApp.getView().getZoomFactor() * 100;
        // the parent nodes of the start and end node
        let startParentNode = null;
        let endParentNode = null;
        // the pixel positions of the range marker nodes and its parents
        let startPos = null;
        let endPos = null;
        let startParentPos = null;
        let endParentPos = null;
        // whether this is a multi line range
        let isMultiLine = false;
        // the page node
        const pageNode = this.docModel.getNode();
        // the overlay node for visualizing the range
        let overlayNode = null;
        // the transparent nodes for visualizing the commented range
        let headOverlay = null;
        let bodyOverlay = null;
        let tailOverlay = null;
        // the height of start line and end line in pixel
        let startLineHeight = 0;
        let endLineHeight = 0;
        // the nodes before start node and end node
        let startPrevNode = null;
        let endPrevNode = null;
        // the widths of the nodes before the start node and the end node
        let startPrevNodeWidth = 0;
        let endPrevNodeWidth = 0;
        // the width and height of the main overlay node in pixel
        let bodyWidth = 0;
        let bodyHeight = 0;
        // whether the range starts or ends in a list
        let isListRange = false;
        // the position of the upper left corner of the highlighted range
        let upperLeftCorner = null;
        // the classes for the new highlight elements (adding marker for ranges)
        const classNames = highlightClassNames ? highlightClassNames + ' ' + RANGEMARKER_CLASS : RANGEMARKER_CLASS;
        // the height of the start node and the end node in pixel
        let startNodeHeight = 0;
        let endNodeHeight = 0;
        // the span following the start node
        let textSpan = null;
        // the comment model for the specified ID
        let commentModel = null;
        // whether a placeholder node is used as start node
        let isPlaceHolderStartNode = false;
        // a correction value that handles the font-size
        let correction = null;

        // 1. in ODF the comment place holder node can be used as start node
        // 2. if no start node and no end node could be found, use the comment node itself (if possible)
        if (!startNode && (this.docApp.isODF() || !endNode) && this.docApp.isTextApp() && this.docModel.getCommentLayer().isCommentId(id)) {
            commentModel = this.docModel.getCommentLayer().getCommentCollection().getById(id);
            startNode = commentModel.getCommentPlaceHolderNode();
            if (startNode && startNode.length === 0) { startNode = null; }
            if (startNode) { isPlaceHolderStartNode = true; }
        }

        // drawing the comment range, if start and end range markers are valid
        if (startNode && endNode) {

            overlayNode = this.#getOrCreateRangeOverlayNode();

            startPos = getPixelPositionToRootNodeOffset(pageNode, startNode, zoomFactor);
            endPos = getPixelPositionToRootNodeOffset(pageNode, endNode, zoomFactor);

            startNodeHeight = startNode.height();
            endNodeHeight = endNode.height();

            // comparing the bottom positions, to check, if this is a multi line range
            isMultiLine = ((startPos.y + startNodeHeight) !== (endPos.y + endNodeHeight));

            // there might be a small difference in height between range marker nodes and place holder nodes -> adding a tolerance of 2 pixel
            if (isMultiLine && isPlaceHolderStartNode) { isMultiLine = Math.abs((startPos.y + startNodeHeight) - (endPos.y + endNodeHeight)) > 2;  }

            // adding simple correction factors for spans with different font sizes
            if (isSpecialField(startNode.next())) {
                textSpan = startNode.next().children('span').first();
            } else {
                textSpan = findNextSiblingNode(startNode, 'span');
            }
            correction = textSpan ? convertCssLength($(textSpan).css('font-size'), 'px', 1) - convertCssLength($(startNode).css('font-size'), 'px', 1) : 0;

            startPos.y -= correction;
            endPos.y -= correction;

            // setting the line height values for start and end node
            startLineHeight = startNodeHeight + correction;
            endLineHeight = endNodeHeight + correction;

            if (isMultiLine) {

                // creating three transparent overlay nodes
                headOverlay = $('<div>').addClass(classNames).attr('data-range-id', id);
                bodyOverlay = $('<div>').addClass(classNames).attr('data-range-id', id);
                tailOverlay = $('<div>').addClass(classNames).attr('data-range-id', id);

                // the nodes directly before the range nodes
                startPrevNode = startNode.prev();
                endPrevNode = endNode.prev();

                // checking if the range marker are inside a list item
                isListRange = isListLabelNode(startPrevNode) || isListLabelNode(endPrevNode);

                if (isListRange) {
                    // the widths of the nodes before the range nodes
                    startPrevNodeWidth = startPrevNode.width();
                    endPrevNodeWidth = endPrevNode.width();
                }

                // the parent nodes of the range nodes
                startParentNode = startNode.parent();
                endParentNode = endNode.parent();

                // the width of the body overlay node is the maximum of the parents widths
                // -> using outerWidth() instead of width() to include the padding (51588)
                bodyWidth = Math.max(startParentNode.outerWidth(), endParentNode.outerWidth());

                // start and end node are empty lines (paragraphs)
                if (!bodyWidth) { bodyWidth = pageNode.width(); }

                // setting the parent positions (the paragraphs)
                startParentPos = getPixelPositionToRootNodeOffset(pageNode, startParentNode, zoomFactor);
                endParentPos = getPixelPositionToRootNodeOffset(pageNode, endParentNode, zoomFactor);

                headOverlay.css({
                    top: startPos.y,
                    left: startPos.x,
                    width: isListRange ? startParentPos.x + startParentNode.width() - startPos.x + startPrevNodeWidth : startParentPos.x  + startParentNode.outerWidth() - startPos.x,
                    height: startLineHeight
                });

                bodyOverlay.css({
                    top: startPos.y + startLineHeight,
                    left: (isListRange ? getPageContentNode(pageNode).offset().left - pageNode.offset().left : Math.min(startParentPos.x, endParentPos.x)),
                    width: isListRange ? pageNode.width() : bodyWidth,
                    height: endPos.y - startPos.y - startLineHeight
                });

                tailOverlay.css({
                    top: endPos.y,
                    left: isListRange ? endPos.x - endPrevNodeWidth : endParentPos.x,
                    width: endPos.x - endParentPos.x,
                    height: endLineHeight
                });

                overlayNode.append(headOverlay, bodyOverlay, tailOverlay);
                upperLeftCorner = startPos;

            } else {

                // adding one transparent overlay node is sufficient
                bodyWidth = Math.max(endPos.x - startPos.x, MIN_HIGHLIGHT_RANGE_WIDTH); // comparing with minimum width in pixel
                bodyHeight = startLineHeight > 0 ? startLineHeight : convertCssLength(startNode.next().css('line-height'), 'px', 1); // using line-height of following span

                bodyOverlay = $('<div>').addClass(classNames).attr('data-range-id', id).css({ top: startPos.y, left: startPos.x, width: bodyWidth, height: bodyHeight });
                overlayNode.append(bodyOverlay);
                upperLeftCorner = startPos;
            }
        } else if (startNode || endNode) {

            overlayNode = this.#getOrCreateRangeOverlayNode();

            // only one range marker (or comment node could be found)
            startNode = startNode || endNode;

            startPos = getPixelPositionToRootNodeOffset(pageNode, startNode, zoomFactor);

            // adding one transparent overlay node is sufficient
            bodyOverlay = $('<div>').addClass(classNames).attr('data-range-id', id).css({ top: startPos.y, left: startPos.x, width: MIN_HIGHLIGHT_RANGE_WIDTH, height: startNode.height() });
            overlayNode.append(bodyOverlay);
            upperLeftCorner = startPos;
        }

        return upperLeftCorner;
    }

    /**
     * Removing the highlighting of one or all ranges.
     *
     * @param {String} [id]
     *  The optional unique id of the range, whose highlighting shall be removed. If not
     *  specified, all highlighting of ranges is removed.
     */
    removeHighLightRange(id) {

        // the overlay node
        const overlayNode = this.#getOrCreateRangeOverlayNode();
        // a selector to remove only specified visualized ranges
        let idSelector = null;

        if (id) {
            idSelector = '[data-range-id=' + id + ']';
            overlayNode.children(idSelector).remove();
        } else {
            overlayNode.empty();
        }

    }

    /**
     * Removing the highlight nodes specified by a class name.
     *
     * @param {String} className
     *  The class name that is used to specify the nodes to be removed.
     */
    removeHighLightRangeByClass(className) {

        // the overlay node
        const overlayNode = this.#getOrCreateRangeOverlayNode();
        // the selector
        const classSelector = '.' + className;

        overlayNode.children(classSelector).remove();
    }

    /**
     * Updating the highlighting of a currently highlighted range. This happens,
     * if an operation modifies the document in that way, that a repaint of the
     * range highlighting is necessary.
     */
    updateHighlightedRanges() {

        // the document was modified (by an external operation)
        // -> highlighted ranges need to be updated.

        // the document was modified while a comment range is visible (caused by hover)
        // -> highlighted ranges need to be updated.

        // a collector for the children in the range overlay node
        const allOverlayChildren = this.#rangeOverlayNode && this.#rangeOverlayNode.children();
        // all IDs
        let allIDs = null;
        // the node, whose change triggered this update
        let currentNode = null;
        // the vertical offset of the current node (if it is not defined, update all range hightlights
        let currentNodePos = 0;
        // the jQuery object received from offset function
        let offset = null;

        if (allOverlayChildren && allOverlayChildren.length > 0) {

            allIDs = {};
            currentNode = this.docModel.getCurrentProcessingNode();

            if (currentNode) {
                offset = $(currentNode).offset();
                if (offset) { currentNodePos = offset.top; }
            }

            _.each(allOverlayChildren, child => {

                let id = 0;

                if ($(child).is(RANGEMARKER_SELECTOR)) { // only collecting
                    id = getRangeMarkerId(child);
                    allIDs[id] = child; // collecting all IDs
                }
            });

            // iterating over all keys
            _.each(_.keys(allIDs), id => {

                // whether this is the id of a temporary comment thread (inserted without operation)
                const isTemporaryCommentId = this.docModel.getCommentLayer().getCommentCollection().isTemporaryCommentId(id);

                if (isTemporaryCommentId) { this.insertTemporaryRangeMarker(this.docModel.getCommentLayer().getCommentCollection().getTemporaryModel()); }

                // the end marker node with the specific id
                const endMarkerNode = this.getEndMarker(id);
                // the vertical pixel position of the end marker node
                let endMarkerPos = (endMarkerNode && endMarkerNode.offset().top) || -1;
                // the classes at the element
                let allClassString = '';
                // the start position of the connection line
                let startPos = null;
                // whether the temporary range marker nodes are already inserted
                const alreadyInserted = true;

                if (isTemporaryCommentId && endMarkerPos > 0) { endMarkerPos += 5; } // adding some tolerance pixel, because temporary range marker might have a wrong height (DOCS-4271)

                // comparing the vertical pixel positions of end marker node and current node
                if (endMarkerPos < 0 || endMarkerPos >= currentNodePos) {
                    allClassString = $(allIDs[id]).attr('class');  // creating string with all classes
                    this.removeHighLightRange(id);  // removing all elements with corresponding id
                    startPos = this.highLightRange(id, allClassString); // reusing the classes

                    // checking if this is a comment id
                    // -> then the connection line needs to be redrawn (but only with valid start position (DOCS-2649))
                    if (this.docApp.isTextApp() && startPos) { this.docModel.getCommentLayer().updateCommentLineConnection(id, startPos, allOverlayChildren, alreadyInserted); }
                }

                if (isTemporaryCommentId) { this.removeTemporaryRangeMarker(); }
            });
        }

    }

    /**
     * Handling delete operations, that contain only a part of an existing range. In this case additional
     * operations need to be generated. These operations can be used to insert or delete the range nodes.
     * This can be used, to reduce the width of the range, or to clean up nodes, if, for example, a comment
     * node is removed. The behavior is dependent from the file type (OOXML <-> ODT).
     * This function is called from the textModel from deleteSelected, deleteRows or deleteColumns.
     * In these functions, all range marker nodes from the components, that will be deleted, must be
     * collected. Then affected start marker without end marker or affected end marker without start
     * marker can be detected.
     * In the OOXML case a deleted start marker can be inserted at a following logical position, so that
     * the width of the range is reduced. A deleted end marker requires, that the start marker is also
     * deleted, so that there are no superfluous range markers inside the document.
     * TODO: Handling for ODT.
     *
     * @param {jQuery} allRangeMarkerNodes
     *  The sorted jQuery list of all collected range marker nodes, that are in the range, that will be
     *  deleted.
     *
     * @param {Object} generator
     *  The operations generator, that already contains the delete operations for the selected area. These
     *  operations must already be in the correct order. This means, that the opertions with the 'largest'
     *  logical position is the first operation saved in the generator.
     *
     * @param {Number[]} [insertPosition]
     *  A logical position at which the start ranges will be inserted. If not defined, the corresponding
     *  end range markers will be removed to avoid superfluous range markers in the code.
     */
    handlePartlyDeletedRanges(allRangeMarkerNodes, generator, insertPosition) {

        // the ids of all start range markers
        const allLocalStartMarkers = {};
        // the ids of all end range markers
        const allLocalEndMarkers = {};
        // the ids of all comment nodes
        const allLocalComments = {};
        // all affected IDs
        const allIds = [];
        // the ids of the range start markers, that are inside the selection and whose end markers are not inside the selection
        const allStartPartIds = [];
        // the ids of the range end markers, that are inside the selection and whose start markers are not inside the selection
        const allEndPartIds = [];
        // ODF comments start nodes (the comment nodes itself)
        const allODFCommentStartIds = [];
        // ODF comments end nodes (range end nodes, without start node)
        const allODFCommentEndIds = [];
        // the collection of all comment models
        let commentCollection = null;
        // a temporary helper for logical positions
        let localPos = null;
        // the position of the start markers, that need to be deleted additionally (before the selected range)
        const allPrePositions = [];
        // the position of the end markers, that need to be deleted additionally (behind the selected range)
        const allPostPositions = [];
        // the new start position that needs to be used after deleteSelected. The old position can be invalid, because of additional delete or insert operations
        let newStartPosition = _.clone(insertPosition);

        // helper function, that adds additionally required operations to the existing generator.
        // In this case a simple process can be used, that creates only delete operations from
        // the specified logical positions. In the future this might become more complicated, if
        // there will be another type of operation, that is added to the existing generator.
        const addAdditionalOperations = (allPositions, options) => {

            // whether the parameter 'localGenerator' is the generator for pre or post operations
            const postPositions = getBooleanOption(options, 'postPositions', true);

            // an ordering of operations is necessary, if the array contains more than one value
            if (allPositions.length > 1) {
                allPositions.sort(comparePositions);
                if (!postPositions) { allPositions.reverse(); } // positions before the selection, can be added directly, but in reverted order
            }

            // delete operations need to be executed before the already defined delete operations,
            // because the nodes are behind the selected range
            if (postPositions) { generator.reverseOperations(); }  // positions behind the selection must be added to the beginning of the generated operations
            _.each(allPositions, position => {
                generator.generateOperation(DELETE, { start: _.copy(position) });
            });
            if (postPositions) { generator.reverseOperations(); }

        };

        if (!allRangeMarkerNodes || allRangeMarkerNodes.length === 0 || !generator) { return; }

        // searching for start marker, end marker and comments -> comparing the id
        _.each(allRangeMarkerNodes, oneMarker => {

            let currentId = null;

            if (isRangeMarkerStartNode(oneMarker)) {
                currentId = getRangeMarkerId(oneMarker);
                allLocalStartMarkers[currentId] = oneMarker;
            } else if (isRangeMarkerEndNode(oneMarker)) {
                currentId = getRangeMarkerId(oneMarker);
                allLocalEndMarkers[currentId] = oneMarker;
            } else if (this.docApp.isODF() && isCommentPlaceHolderNode(oneMarker)) {
                currentId = getTargetContainerId(oneMarker);
                allLocalComments[currentId] = oneMarker;  // only filled for ODF documents
            }

            if (currentId && !_.contains(allIds, currentId)) { allIds.push(currentId);  } // using array, to keep correct order
        });

        // Finding ranges, that are only partly affected (requires special handling for comments in ODF)
        _.each(allIds, id => {

            if (this.docApp.isODF()) {

                if (allLocalStartMarkers[id] && !allLocalEndMarkers[id]) {  // classical range start marker (no comment)
                    allStartPartIds.push(id);
                } else if (allLocalEndMarkers[id]) {
                    // is this an end marker for a comment or for a 'classic' range
                    if (this.docModel.getCommentLayer().isCommentId(id)) {
                        if (!allLocalComments[id]) {  // the comment itself is not in the current selection
                            allODFCommentEndIds.push(id);
                        }
                    } else if (!allLocalStartMarkers[id]) {
                        allEndPartIds.push(id);  // classical range end marker (no comment)
                    }
                } else if (allLocalComments[id] && !allLocalEndMarkers[id]) {
                    allODFCommentStartIds.push(id); // a comment node without the corresponding end range marker
                }

            } else {

                if (allLocalStartMarkers[id] && !allLocalEndMarkers[id]) {
                    allStartPartIds.push(id);
                } else if (allLocalEndMarkers[id] && !allLocalStartMarkers[id]) {
                    allEndPartIds.push(id);
                }
            }

        });

        // Finally generating new operations.
        // special handling for comments in ODF, where the start range is marked by the comment node itself
        // -> this is handled by the two specific collectors 'allODFCommentStartIds' and 'allODFCommentEndIds'.
        if (this.docApp.isODF()) {

            // Comments in ODF:
            // Deleting only the start comment node -> additionally delete the end range node (comment is removed completely)
            // Deleting only the end range node -> insert the end range node before the delete selection

            // only the start node was removed (the comment node itself) -> deleting the comment
            // -> in case of existing child comments, these need to be removed, too (only comment nodes without range)
            if (allODFCommentStartIds.length > 0) {

                commentCollection = this.docModel.getCommentLayer().getCommentCollection();

                _.each(allODFCommentStartIds, id => {

                    // the range end marker for the comment range
                    const endMarker = this.getEndMarker(id);
                    // the comment model of the removed comment
                    const commentModel = commentCollection.getById(id);

                    // deleting the end marker
                    if (endMarker && !allRangeMarkerNodes.is(endMarker)) {
                        localPos = getOxoPosition(this.docModel.getCurrentRootNode(), endMarker);
                        allPostPositions.push(_.clone(localPos));
                    }

                    // deleting the comment children in the same thread
                    if (commentModel && !commentModel.isReply()) {
                        _.each(commentCollection.getChildrenById(id), childModel => {
                            const onePlaceHolderNode = childModel.getCommentPlaceHolderNode();
                            // deleting the child comment, but not if it is already in the list of deleted comments (DOCS-2836)
                            if (onePlaceHolderNode && !allRangeMarkerNodes.is(onePlaceHolderNode)) { allPostPositions.push(getOxoPosition(this.docModel.getCurrentRootNode(), onePlaceHolderNode)); }
                        });
                    }
                });
            }

            // only the end node of a comment range was removed -> reducing the width of the comment, if the
            // new position at insertPosition was defined
            if (allODFCommentEndIds.length > 0) {

                if (insertPosition) {
                    allODFCommentEndIds.reverse(); // -> simplifies the insertion at the start position
                    _.each(allODFCommentEndIds, id => {
                        const endMarker = allLocalEndMarkers[id];
                        const type = getRangeMarkerType(endMarker);

                        // this operations can simply be added to the already filled operations generator, because the affected positions are
                        // directly behind the delete range and the insertions happens directly after the delete operations. So it is not
                        // possible, that the logical positions become invalid.
                        generator.generateOperation(RANGE_INSERT, { start: _.copy(insertPosition), id, type, position: 'end' });
                    });
                }
            }
        }

        // Classical behavior for range marker nodes (not valid for comments in ODF)
        // Deleting only the end range node -> additionally delete the start range node
        // Deleting only the start range node -> insert the start range node behind the delete selection
        // -> the comment node itself can be ignored

        // if only the start marker is removed, add operations to insert the start marker at a new position
        if (allStartPartIds.length > 0) {
            if (insertPosition) {
                allStartPartIds.reverse(); // -> simplifies the insertion at the start position
                _.each(allStartPartIds, id => {
                    const startMarker = allLocalStartMarkers[id];
                    const type = getRangeMarkerType(startMarker);

                    // Checking that the end marker really exists (67214)
                    if (this.isEndMarkerId(id)) {
                        // this operations can simply be added to the already filled operations generator, because the affected positions are
                        // directly behind the delete range and the insertions happens directly after the delete operations. So it is not
                        // possible, that the logical positions become invalid.
                        generator.generateOperation(RANGE_INSERT, { start: _.copy(insertPosition), id, type, position: 'start' });
                    }
                });
            } else {
                // failed to determine new position for start range markers (for example after deleteColumn)
                // -> deleting also the end range marker to avoid superfluous range markers in the code
                // -> this is also the behavior of ODF comments
                _.each(allStartPartIds, id => {
                    const endMarker = this.getEndMarker(id);
                    if (endMarker && !allRangeMarkerNodes.is(endMarker)) {
                        localPos = getOxoPosition(this.docModel.getCurrentRootNode(), endMarker);
                        allPostPositions.push(_.clone(localPos));
                    }
                });
            }
        }

        // if only the end marker is removed, add operations to delete the start marker, too
        // -> in this case the 'allPrePositions' needs to be filled, because the logical positions are before the selected and deleted area
        if (allEndPartIds.length > 0) {
            _.each(allEndPartIds, id => {
                const startMarker = this.getStartMarker(id);
                if (startMarker && !allRangeMarkerNodes.is(startMarker)) {
                    localPos = getOxoPosition(this.docModel.getCurrentRootNode(), startMarker);
                    allPrePositions.push(_.clone(localPos));
                    // if the position is inside the same paragraph as the current start position,
                    // this position need to be reduced. So after a call of deleteSelected(), there is a
                    // valid position for a directly following input.
                    if (hasSameParentComponent(localPos, newStartPosition)) { newStartPosition = decreaseLastIndex(newStartPosition); }
                }
            });
        }

        // checking, if additional operations from 'preGenerator' or 'postGenerator' need to be added to the existing operation generator.
        // In this case, it is necessary to sort the logical positions and to add the operations to the end of the operations list
        // (from allPrePositions) and to add them to the beginning of the operations list (from allPostPositions).
        if (allPrePositions.length > 0) { addAdditionalOperations(allPrePositions, { postPositions: false }); }
        if (allPostPositions.length > 0) { addAdditionalOperations(allPostPositions, { postPositions: true }); }

        return newStartPosition;
    }

    /**
     * Inserting a temporary range marker for the temporary comment model. This
     * range marker is NOT inserted with an operation and must therefore be
     * removed synchronously again.
     *
     * @param {Object} dataModel
     *  An object that must have the following function:
     *  - getId, to get a unique id, that will also be used for the range marker.
     *  - getSelectionState, to get an object, that has the properties 'start', 'end'
     *    and optionally 'target', to find the valid positions in the document for
     *    inserting the start and end rangeMarker nodes.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.onlyStart=false]
     *      Whether only the start range marker shall be inserted, or both.
     */
    insertTemporaryRangeMarker(dataModel, options) {

        if (!dataModel) { return; } // nothing to do

        // the ID of the comment model
        const commentId = dataModel.getId();
        // the selection state of the new inserted comment thread
        const selectionState = dataModel.getSelectionState();
        // whether only the start node shall be inserted
        const onlyStart = getBooleanOption(options, 'onlyStart', false);
        // the start position for the comment range
        const startPosition = selectionState.start;
        // the end position for the comment range
        const endPosition = onlyStart ? null : selectionState.end;
        // the target of the logical position
        const target = selectionState.target;

        // registering in the temporaryRangeMarkerNodes something like 'singleParagraph: true'
        this.#temporaryRangeMarkerNodes.singleParagraph = onlyStart || hasSameParentComponent(startPosition, endPosition);

        if (!onlyStart) { this.insertRangeHandler(endPosition, commentId, 'comment', 'end', null, target, this.#temporaryRangeMarkerNodes); }
        this.insertRangeHandler(startPosition, commentId, 'comment', 'start', null, target, this.#temporaryRangeMarkerNodes);
    }

    /**
     * Removing a temporary range marker for a specified comment model. This
     * range marker was NOT inserted with an operation and must therefore be
     * removed synchronously again.
     */
    removeTemporaryRangeMarker() {

        const startRangeMarker = this.#temporaryRangeMarkerNodes.start;
        const endRangeMarker = this.#temporaryRangeMarkerNodes.end;

        const startParagraph = startRangeMarker ? startRangeMarker.parent() : null;
        const endParagraph = (endRangeMarker && (!startParagraph || !this.#temporaryRangeMarkerNodes.singleParagraph)) ? endRangeMarker.parent() : null;

        const startTextSpan = startRangeMarker ? startRangeMarker.next() : null;
        const endTextSpan = endRangeMarker ? endRangeMarker.next() : null;

        // removing the range marker model
        if (startRangeMarker) { this.removeRangeMarker(startRangeMarker); }
        if (endRangeMarker) { this.removeRangeMarker(endRangeMarker); }

        // deleting the stored node
        if (startRangeMarker) { startRangeMarker.remove(); }
        if (endRangeMarker) { endRangeMarker.remove(); }

        // validating the paragraph node (one or two)
        if (startParagraph && startParagraph.length) { this.docModel.validateParagraphNode(startParagraph); }
        if (endParagraph && endParagraph.length) { this.docModel.validateParagraphNode(endParagraph); }

        // merging text spans
        if (startTextSpan && startTextSpan.length) { mergeSiblingTextSpans(startTextSpan); }
        if (endTextSpan && endTextSpan.length) { mergeSiblingTextSpans(endTextSpan); }

        // empty the collector
        this.#temporaryRangeMarkerNodes = {};
    }

    // private methods ----------------------------------------------------

    /**
     * Registering one range marker in the range marker collection.
     *
     * @param {HTMLElement|jQuery} marker
     *  One range marker node.
     *
     * @param {String} id
     *  The unique id of the range marker node.
     *
     * @param {String} [target]
     *  The target, where the range marker is located.
     */
    #addIntoRangeMarkerCollection(marker, id, target) {

        // the comment node in jQuery format
        const $marker = $(marker);
        // whether the target is inside header or footer
        // -> in this case it is useless to save the node itself, but saving
        //    the target allows to find the range with specific id fast
        const isMarginalId = target && this.docModel.getPageLayout().isIdOfMarginalNode(target);

        // adding the node also into the type specific collector
        if (isRangeMarkerStartNode(marker)) {
            this.#allStartMarkers[id] = isMarginalId ? target : $marker;
        } else if (isRangeMarkerEndNode(marker)) {
            this.#allEndMarkers[id] = isMarginalId ? target : $marker;
        }
    }

    /**
     * Removing one range marker with specified id from range marker collections (the model).
     *
     * @param {HTMLElement|jQuery} marker
     *  One range marker node.
     *
     * @param {String} id
     *  The unique id of the range marker node.
     */
    #removeFromRangeMarkerCollection(marker, id) {

        // adding the node also into the type specific collector
        if (isRangeMarkerStartNode(marker)) {
            delete this.#allStartMarkers[id];
        } else if (isRangeMarkerEndNode(marker)) {
            delete this.#allEndMarkers[id];
        }
    }

    /**
     * Generating or simply returning the overlay node, that is used to visualize the ranges.
     *
     * @returns {Boolean}
     *  The overlay node, that contains the nodes to visualize the ranges.
     */
    #getOrCreateRangeOverlayNode() {

        // the page content node
        let pageContentNode = null;

        // is there is already a comment overlay node, return it
        if (this.#rangeOverlayNode && this.#rangeOverlayNode.length > 0) { return this.#rangeOverlayNode; }

        // create a new comment layer node
        pageContentNode = getPageContentNode(this.docModel.getNode());

        // Adding the text drawing layer behind an optional footer wrapper
        if (isFooterWrapper(pageContentNode.next())) { pageContentNode = pageContentNode.next(); }

        // creating the (global) overlay node
        this.#rangeOverlayNode = $('<div>').addClass(RANGEMARKEROVERLAYNODE_CLASS).attr('contenteditable', false);

        // inserting the range layer node in the page
        pageContentNode.after(this.#rangeOverlayNode);

        return this.#rangeOverlayNode;
    }

    /**
     * Finding in a header or footer specified by the target the range marker
     * with the specified id of type 'start' or 'end'.
     *
     * @param {String} id
     *  The id string.
     *
     * @param {String} target
     *  The target string to identify the header or footer node
     *
     * @param {String} selector
     *  The selector string to identify range start marker or range end marker
     *
     * @param {Number} [index]
     *  Cardinal number of target node in the document from top to bottom. If passed,
     *  forces to return node with that index in document, from top to bottom.
     *
     * @returns {jQuery|null}
     *  The range marker node with the specified id and type, or null, if no such
     *  range marker exists.
     */
    #getMarginalRangeMarker(id, target, selector, index) {

        // the specified header or footer node
        let marginalNode = null;
        // the searched range marker with the specified id
        let marker = null;

        marginalNode = this.docModel.getRootNode(target, index);

        if (marginalNode) {
            marker = _.find(marginalNode.find(selector), marker => { return getRangeMarkerId(marker) === id; });
        }

        return marker ? $(marker) : null;
    }

    /**
     * After load from local storage or fast load the collectors for the range markers need to be filled.
     */
    #refreshRangeMarker() {

        // the page content node of the document
        const pageContentNode = getPageContentNode(this.docModel.getNode());
        // all range marker nodes in the document
        const allRangeMarkerNodes = pageContentNode.find(RANGEMARKERNODE_SELECTOR);
        // a collector for all marginal template nodes
        const allMargins = this.docModel.getPageLayout().getHeaderFooterPlaceHolder().children();
        // the target string node
        let target = '';

        // helper function to add a collection of range marker nodes into the model
        const addMarginalNodesIntoCollection = collection => {
            _.each(collection, oneRangeMarker => {
                this.#addIntoRangeMarkerCollection(oneRangeMarker, getRangeMarkerId(oneRangeMarker), target);
            });
        };

        // reset model
        this.#allStartMarkers = {};
        this.#allEndMarkers = {};

        // 1. Search in page content node
        // 2. Search in header/footer template node (-> overwriting previous nodes by target strings)

        // adding all range marker nodes into the collector (not those from header or footer)
        _.each(allRangeMarkerNodes, oneRangeMarker => {
            if (!isMarginalNode($(oneRangeMarker).parent())) {
                this.#addIntoRangeMarkerCollection(oneRangeMarker, getRangeMarkerId(oneRangeMarker));
            }
        });

        // adding the range markers that are located inside header or footer (using the template node)
        _.each(allMargins, margin => {

            // a collector for all range marker nodes inside one margin
            const allMarginRangeMarkerNodes = $(margin).find(RANGEMARKERNODE_SELECTOR);

            if (allMarginRangeMarkerNodes.length > 0) {
                target = getTargetContainerId(margin);
                addMarginalNodesIntoCollection(allMarginRangeMarkerNodes);
            }
        });

        // during loading, it is necessary, that other components (like comments) know,
        // the range marker model is update-to-date
        this.#rangeMarkerModelRefreshed = true;
    }

    /**
     * Returns, whether the range marker model was refreshed at least once. This
     * is necessary after loading from local storage or fast load.
     *
     * @returns {Boolean}
     *  Whether the range marker model was refreshed at least once.
     */
    #isRangeMarkerModelRefreshed() {
        return this.#rangeMarkerModelRefreshed;
    }
}

// export =================================================================

export default RangeMarker;
