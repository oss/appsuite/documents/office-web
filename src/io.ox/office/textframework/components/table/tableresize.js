/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { TrackingObserver } from '@/io.ox/office/tk/tracking';
import { convertHmmToLength, convertLengthToHmm } from '@/io.ox/office/tk/utils';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { LIST_LABEL_NODE_SELECTOR, isTableNodeInTableDrawing, isResizeNode } from '@/io.ox/office/textframework/utils/dom';
import { getOxoPosition } from '@/io.ox/office/textframework/utils/position';
import { getGridPositionFromCellPosition } from '@/io.ox/office/textframework/components/table/table';

// class TableResizeObserver ==================================================

/**
 * A special `TrackingObserver` that implements resizing columns/rows in table
 * elements.
 *
 * @param {TextBaseApplication} docApp
 *  The application using this observer.
 *
 * @param {TrackingPrepareFn} [prepare]
 *  Additional filter predicate function called before a tracking sequence.
 */
export class TableResizeObserver extends TrackingObserver {

    constructor(docApp, prepare) {

        super({
            prepare: prepareResizeTable,
            start: startResizeTable,
            move: moveResizeTable,
            scroll: scrollResizeTable,
            end: stopResizeTable,
            finally: finalizeResizeTable
        });

        const editor = this.docModel = docApp.docModel;
        const view = this.docView = docApp.docView;
        const officemaindiv = docApp.getWindowNode();

        // the node containing the scroll bar
        const scrollNode = view.getContentRootNode();

        var startX = 0,
            startY = 0,
            currentX = 0,
            currentY = 0,
            shiftX = 0,
            shiftY = 0,
            // verticalResize is true, if the column width will be modified
            verticalResize = false,
            // horizontalResize is true, if the row height will be modified
            horizontalResize = false,
            // verticalResize is true, if the column width will be modified
            isLeftResize = false,
            // horizontalResize is true, if the row height will be modified
            isTopResize = false,
            // the container element used to visualize the resizing
            resizeLine = $('<div>').addClass('resizeline'),
            // the cell node for the selected resize node
            cellNode = null,
            // the row node for the selected resize node
            rowNode =  null,
            // the table node for the selected resize node
            tableNode = null,
            // is the selected cell the last cell in its row
            lastCell = false,
            // logical position of the selected node
            tablePosition = [],
            // table width after shifting column
            newTableWidth = 0,
            // table width before shifting column
            oldTableWidth = 'auto',
            // table grid, containing calculated pixel widhts
            pixelGrid = [],
            // sum of all grid values, will not be modified
            gridSum = 0,
            // the number of the grid count, that will be modified
            shiftedGrid = 0,
            // maximum shift to the left (-1 means 'no limit')
            maxLeftShift = 0,
            // maximum shift to the right (-1 means 'no limit')
            maxRightShift = 0,
            // maximum shift to the top (-1 means 'no limit')
            maxTopShift = 0,
            // maximum shift to the bottom (-1 means 'no limit')
            maxBottomShift = 0,
            // maximum right value of shift position (-1 means 'no limit')
            maxRightValue = 0,
            // minimum left value of shift position (-1 means 'no limit')
            minLeftValue = 0,
            // minimum width of a column in px
            // Fix for 29678, using min width like in Word
            minColumnWidth = 0,
            // minimum height of a row in px
            minRowHeight = 0,
            //zoom factor in floating point notation
            zoomFactor = 0,
            // the current scroll position
            scrollX = 0, scrollY = 0,
            // the start scroll position
            startScrollX = 0, startScrollY = 0,
            // whether the most right cell can be expanded to the right
            allowLastCellOverflow = false,
            // whether the table is located inside a drawing node
            isTableDrawing = false,
            // maximum height of page content in pixels (without margins)
            pageMaxHeight = 0;

        /**
         * Filter predicate to restrict this tracking observer to resizer
         * elements in table cells.
         */
        function prepareResizeTable(event) {
            if (!isResizeNode(event.target)) { return false; }

            // prevent default click handling of the browser
            event.preventDefault();
            // set focus to the document container (may be located in GUI edit fields)
            view.grabFocus();

            return !prepare || prepare(event);
        }

        /**
         * Calculates the required data, when mouse down happens on resize node.
         *
         * @param {TrackingRecord} record
         *  The tracking notification record.
         */
        function startResizeTable(record) {

            verticalResize = horizontalResize = isLeftResize = isTopResize = lastCell = false;
            shiftedGrid = maxLeftShift = maxRightShift = maxTopShift = maxBottomShift = maxRightValue = minLeftValue = 0;
            minColumnWidth = 16;
            minRowHeight = 5;
            zoomFactor = view.getZoomFactor();
            pageMaxHeight = editor.getPageLayout().getDefPageActiveHeight({ convertToPixel: true }) - 5; // -5 px because of difference in rounding and offsetHeight, position top in IE, FF

            const offset = officemaindiv.offset();
            startX = record.point.x - offset.left;
            startY = record.point.y - offset.top;

            const resizeNode = $(record.target);
            if (resizeNode.is('div.resize.right')) {
                verticalResize = true;
            } else if (resizeNode.is('div.resize.bottom')) {
                horizontalResize = true;
            } else if (resizeNode.is('div.resize.left')) {
                verticalResize = true;
                isLeftResize = true;
            } else if (resizeNode.is('div.resize.top')) {
                horizontalResize = true;
                isTopResize = true;
            }

            // calculating maximum resize values
            cellNode = resizeNode.closest('td, th');
            rowNode =  resizeNode.closest('tr');
            tableNode = resizeNode.closest('table');

            // in drawings of type 'table' the most right cells can be expanded to the right
            isTableDrawing = isTableNodeInTableDrawing(tableNode);
            allowLastCellOverflow = isTableDrawing;

            if (verticalResize) {
                $(resizeLine).css({ width: '1px', height: '100%', left: startX, top: '0px' });
                officemaindiv.append(resizeLine);

                // calculating maxLeftShift and maxRightShift
                lastCell = !cellNode[0].nextSibling;
                tablePosition = getOxoPosition(editor.getCurrentRootNode(), tableNode.get(0), 0);
                const tableNodeAttrs = editor.tableStyles.getElementAttributes(tableNode).table;
                oldTableWidth = tableNodeAttrs.width;
                const maxTableWidth = tableNode.parent().width();

                if (oldTableWidth === 'auto') {
                    oldTableWidth = tableNode.outerWidth();
                } else {
                    oldTableWidth = convertHmmToLength(oldTableWidth, 'px', 1);
                }

                // converting from relational grid to pixel grid
                const oldTableGrid = tableNodeAttrs.tableGrid;
                gridSum = oldTableGrid.reduce((sum, width) => sum + width, 0);
                pixelGrid = oldTableGrid.map(width => Math.round(width * oldTableWidth / gridSum));

                // which border was shifted?
                shiftedGrid = getGridPositionFromCellPosition(rowNode, cellNode.prevAll().length).end;

                // Fix for 30798, cells with lists need an increased minimum width
                if (cellNode.find(LIST_LABEL_NODE_SELECTOR).length > 0) { minColumnWidth *= 4; }

                if (isLeftResize) {
                    maxLeftShift = -1; // no limit
                    maxRightShift = pixelGrid[shiftedGrid] * zoomFactor - resizeNode.position().left; // taking into account, that the resizer is shifted to the right
                    allowLastCellOverflow = false;
                } else {
                    maxLeftShift = pixelGrid[shiftedGrid] * zoomFactor;
                    if (!lastCell) {
                        maxRightShift = pixelGrid[shiftedGrid + 1] * zoomFactor;
                        allowLastCellOverflow = false;
                    } else {
                        maxRightShift = (maxTableWidth - oldTableWidth) * zoomFactor;
                    }
                }

            } else if (horizontalResize) {
                $(resizeLine).css({ width: '100%', height: '1px', left: 0, top: startY });
                officemaindiv.append(resizeLine);

                if (isTopResize) {
                    // calculating maxTopShift
                    maxTopShift = -1; // no limit

                    // calculating maxBottomShift - max height that single row can have
                    maxBottomShift = cellNode.outerHeight() * zoomFactor - resizeNode.position().top - minRowHeight;
                } else {
                    // calculating maxTopShift
                    maxTopShift = cellNode.outerHeight() * zoomFactor;
                    // calculating maxBottomShift - max height that single row can have
                    maxBottomShift = (pageMaxHeight - cellNode.outerHeight()) * zoomFactor;
                }
            }

            editor.getCurrentRootNode().css('cursor', resizeNode.css('cursor'));  // setting cursor for increasing drawing
            $(resizeLine).css('cursor', resizeNode.css('cursor'));  // setting cursor for increasing drawing

            startScrollX = scrollNode.scrollLeft();
            startScrollY = scrollNode.scrollTop();
        }

        /**
         * Calculates the required data, when mouse move happens.
         *
         * @param {TrackingRecord} record
         *  The tracking notification record.
         */
        function moveResizeTable(record) {

            const offset = officemaindiv.offset();
            currentX = record.point.x + scrollX - offset.left;
            currentY = record.point.y + scrollY - offset.top;

            if (verticalResize) {

                maxRightValue = (maxRightShift > -1) ? startX + maxRightShift : -1;
                minLeftValue = (maxLeftShift > -1) ? (startX - maxLeftShift + (minColumnWidth * zoomFactor)) : -1;

                if (maxRightValue > -1 && !lastCell) { maxRightValue -= (minColumnWidth * zoomFactor); }

                shiftX = currentX;
                shiftY = 0;

                if (maxRightValue > -1 && shiftX >= maxRightValue && !allowLastCellOverflow) {
                    shiftX = maxRightValue;
                } else if (minLeftValue > -1 && shiftX <= minLeftValue) {
                    shiftX = minLeftValue;
                }

            } else if (horizontalResize) {
                shiftX = 0;
                shiftY = currentY;

                if (maxTopShift > -1 && (shiftY - startY) <= -maxTopShift) {
                    shiftY = startY - maxTopShift;
                }
                if (maxBottomShift > -1 && (shiftY - startY) >= maxBottomShift) {
                    shiftY = startY + maxBottomShift;
                }
            }

            if ((_.isNumber(shiftX)) && (_.isNumber(shiftY))) {
                $(resizeLine).css({ left: shiftX - scrollX, top: shiftY - scrollY });
            }
        }

        /**
         * Updates scroll position according to the passed tracking event.
         *
         * @param {TrackingRecord} record
         *  The tracking notification record.
         */
        function scrollResizeTable(record) {

            // update scrollPosition with suggestion from event
            if (record.scroll.x) {
                scrollNode.scrollLeft(scrollNode.scrollLeft() + record.scroll.x);
            }

            if (record.scroll.y) {
                scrollNode.scrollTop(scrollNode.scrollTop() + record.scroll.y);
            }

            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;
        }

        /**
         * Calculates the required data, when mouse up happens and generates
         * operations.
         *
         * @param {TrackingRecord} record
         *  The tracking notification record.
         */
        function stopResizeTable(record) {

            var generator = editor.createOperationGenerator(),
                rowPosition = null,
                rowHeight = 0,
                newRowHeight = 0, oldRowHeight = 0,
                // the old attributes
                oldAttrs = null,
                // the attributes for the setAttributes operation(s)
                attrs = null, drawingOperationProperties = null,
                target = editor.getActiveTarget(),
                operationProperties = {},
                // the logical table position
                tablePos = null,
                // the shift of the table
                tableShift = null,
                // the drawing node of a table drawing
                drawingNode = null,
                // the attributes of a table drawing node and row
                drawingAttrs = null, rowAttrs = null,
                // a shift of the position of the table drawing node
                deltaTablePos = 0,
                // saving the old value for the top and left position in the drawing attributes
                oldDrawingAttrsTop = 0, oldDrawingAttrsLeft = 0,
                // the height of the table row in the DOM
                realRowNodeHeight = 0;

            // helper function for the left resizer
            function handleLeftResize() {

                // setting the table attributes
                drawingNode = tableNode.closest('div.drawing');
                // table drawings always have explicit 'drawing' attributes
                drawingAttrs = getExplicitAttributes(drawingNode, 'drawing');

                oldDrawingAttrsLeft = drawingAttrs.left;

                if (editor.useSlideMode()) {
                    // setting the left value of the drawing by comparing the vertical line with the slide position
                    drawingAttrs.left = convertLengthToHmm((resizeLine.offset().left - drawingNode.closest('div.slide').offset().left) / zoomFactor);

                    // improving precision -> table width can only be modified like change of left drawing position

                } else {
                    // TODO
                    // drawingAttrs.left += convertLengthToHmm(shiftX, 'px'); // setting new left position of the table drawing (OX Text style)
                }

                attrs = attrs || {};
                attrs.drawing = drawingAttrs;
                attrs.drawing.width = newTableWidth; // modifying the table width

                // improving precision of shiftX by simply using the shift of the table in horizontal direction
                deltaTablePos = drawingAttrs.left - oldDrawingAttrsLeft;

                shiftX = convertHmmToLength(deltaTablePos, 'px', 1);
            }

            // helper function for the top resizer
            function handleTopResize() {

                // the upper left position of the table (drawing) was changed
                tablePos = _.initial(rowPosition);
                tableShift = convertLengthToHmm(rowHeight - oldRowHeight, 'px');

                if (tableShift !== 0 && isTableDrawing) {

                    drawingNode = tableNode.closest('div.drawing');
                    // table drawings always have explicit 'drawing' attributes
                    drawingAttrs = getExplicitAttributes(drawingNode, 'drawing');

                    oldDrawingAttrsTop = drawingAttrs.top;

                    if (editor.useSlideMode()) {
                        // setting the top value of the drawing by comparing the horizontal line with the slide position
                        drawingAttrs.top = convertLengthToHmm((resizeLine.offset().top - drawingNode.closest('div.slide').offset().top) / zoomFactor);
                    } else {
                        // TODO
                        drawingAttrs.top -= tableShift;
                    }

                    drawingOperationProperties = {
                        attrs: { drawing: drawingAttrs },
                        start: _.copy(tablePos)
                    };

                    if (target) { drawingOperationProperties.target = target; }

                    generator.generateOperation(SET_ATTRIBUTES, drawingOperationProperties);

                    // shift of table in vertical direction
                    deltaTablePos = oldDrawingAttrsTop - drawingAttrs.top;

                    // the height of the first row can be calculated more precise by using the difference of top position of table
                    rowAttrs = getExplicitAttributes(rowNode, 'row');
                    if (rowAttrs && _.isNumber(rowAttrs.height)) {
                        realRowNodeHeight = convertLengthToHmm(rowNode.outerHeight(true), 'px');
                        newRowHeight = Math.max(realRowNodeHeight, rowAttrs.height) + deltaTablePos;
                    }
                }
            }

            // taking care of scroll node
            scrollX = scrollNode.scrollLeft() - startScrollX;
            scrollY = scrollNode.scrollTop() - startScrollY;

            const offset = officemaindiv.offset();
            currentX = record.point.x + scrollX - offset.left;
            currentY = record.point.y + scrollY - offset.top;

            if (verticalResize) {

                if (currentX !== startX) {

                    maxRightValue = (maxRightShift > -1) ? (startX + maxRightShift) : -1;
                    minLeftValue = (maxLeftShift > -1) ? (startX - maxLeftShift + (minColumnWidth * zoomFactor)) : -1;
                    if (!lastCell && maxRightValue > -1) { maxRightValue -= (minColumnWidth * zoomFactor); }

                    if (maxRightValue > -1 && currentX >= maxRightValue && !allowLastCellOverflow) {
                        currentX = maxRightValue;
                    } else if (minLeftValue > -1 && currentX <= minLeftValue) {
                        currentX = minLeftValue;
                    }

                    shiftX = (currentX - startX) / zoomFactor;

                    // the table (drawing) needs to get a new upper left position
                    if (isLeftResize && shiftX !== 0 && isTableDrawing) { handleLeftResize(); }

                    newTableWidth = isLeftResize ? (oldTableWidth - shiftX) : (lastCell ? (oldTableWidth + shiftX) : oldTableWidth);

                    // -> shifting the border
                    pixelGrid[shiftedGrid] = isLeftResize ? (pixelGrid[shiftedGrid] - shiftX) : (pixelGrid[shiftedGrid] + shiftX);

                    if (!lastCell && !isLeftResize) { pixelGrid[shiftedGrid + 1] -= shiftX; }

                    // converting modified pixel grid to new relation table grid
                    const tableGrid = pixelGrid.map(width => Math.round(gridSum * width / newTableWidth));

                    if (!lastCell && !isLeftResize && (editor.tableStyles.getElementAttributes(tableNode).table.width === 'auto')) {
                        newTableWidth = 'auto';
                    } else {
                        newTableWidth = convertLengthToHmm(newTableWidth, 'px');
                    }

                    // the table attributes for the setAttributes operation
                    attrs = attrs || {};
                    if (isTableDrawing) {
                        attrs.table = { tableGrid };
                        if (newTableWidth !== 'auto') {
                            attrs.drawing = attrs.drawing || {};
                            attrs.drawing.width = newTableWidth;
                        } // width must be assigned to table drawing
                    } else {
                        attrs.table = { tableGrid, width: newTableWidth };
                    }

                    if (editor.getChangeTrack().isActiveChangeTracking()) {
                        // Expanding operation for change tracking with old explicit attributes
                        oldAttrs = editor.getChangeTrack().getOldNodeAttributes(tableNode);
                        // adding the old attributes, author and date for change tracking
                        if (oldAttrs) {
                            oldAttrs = _.extend(oldAttrs, editor.getChangeTrack().getChangeTrackInfo());
                            attrs.changes = { modified: oldAttrs };
                        }
                    }
                    operationProperties = {
                        attrs,
                        start: _.copy(tablePosition)
                    };
                    if (target) {
                        operationProperties.target = target;
                    }

                    generator.generateOperation(SET_ATTRIBUTES, operationProperties);

                    // Performance: Mark the table with information about vertical resize, so that no full update of cell
                    // attributes is required, but only a recalculation of tab stops.
                    tableNode.data('tableResize', true);

                    editor.applyOperations(generator);

                    tableNode.removeData('tableResize');
                }

            } else if (horizontalResize) {

                if (currentY !== startY) {

                    oldRowHeight = _.browser.IE ? rowNode.children('td').first().outerHeight() : rowNode.outerHeight(); // IE returns for rows always '100' !?

                    rowHeight = isTopResize ? (oldRowHeight - ((currentY - startY) / zoomFactor)) : (oldRowHeight + ((currentY - startY) / zoomFactor));
                    if (rowHeight < 0) { rowHeight = 0; }
                    if (rowHeight > pageMaxHeight) { rowHeight = pageMaxHeight; }
                    newRowHeight = convertLengthToHmm(rowHeight, 'px');
                    rowPosition = getOxoPosition(editor.getCurrentRootNode(), rowNode.get(0), 0);

                    if (isTopResize && isTableDrawing) { handleTopResize(); }  // handling a top resize of the table

                    // the row attributes for the setAttributes operation
                    attrs = { row: { height: newRowHeight } };

                    if (editor.getChangeTrack().isActiveChangeTracking()) {
                        // Expanding operation for change tracking with old explicit attributes
                        oldAttrs = editor.getChangeTrack().getOldNodeAttributes(rowNode);
                        // adding the old attributes, author and date for change tracking
                        if (oldAttrs) {
                            oldAttrs = _.extend(oldAttrs, editor.getChangeTrack().getChangeTrackInfo());
                            attrs.changes = { modified: oldAttrs };
                        }
                    }

                    operationProperties = {
                        attrs,
                        start: _.copy(rowPosition)
                    };
                    if (target) {
                        operationProperties.target = target;
                    }

                    generator.generateOperation(SET_ATTRIBUTES, operationProperties);

                    editor.applyOperations(generator);

                    if (editor.getApp().isODF() && editor.useSlideMode()) { // table height must be send to the filter via operation
                        editor.trigger('drawingHeight:update', drawingNode ? drawingNode : rowNode.closest('div.drawing'));
                    }
                }
            }

        }

        /**
         * Finalizes the tracking of the table resizing process.
         */
        function finalizeResizeTable(record) {
            view.scrollToChildNode(record.target);

            // removing the resize line
            officemaindiv.children('.resizeline').remove();

            // Resetting cursor, using css file again
            editor.getCurrentRootNode().css('cursor', '');

            // Resetting variables
            shiftX = shiftY = scrollX = scrollY = 0;
        }
    }

    // public methods ---------------------------------------------------------

    /*override*/ observe(elem, config) {
        super.observe(elem, {
            scrollDirection: "all",
            scrollMinSpeed: 10,
            scrollMaxSpeed: 250,
            scrollBoundary: this.docView.$contentRootNode[0],
            scrollAccelBand: [-30, 30], // start scrolling before reaching the container borders
            ...config
        });
    }
}
