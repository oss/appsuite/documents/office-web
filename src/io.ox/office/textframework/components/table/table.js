/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import { containsNode, parseIntAttribute } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { convertLengthToHmm, getBooleanOption } from '@/io.ox/office/tk/utils';

import { isTableDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getElementStyleId, getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { NONE } from '@/io.ox/office/editframework/utils/border';

import { MAX_TABLE_CELLS, MAX_TABLE_ROWS } from '@/io.ox/office/textframework/utils/config';
import { INSERT_STYLESHEET, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { PARAGRAPH_NODE_SELECTOR, TABLE_CELLNODE_SELECTOR, TABLE_NODE_AND_DRAWING_SELECTOR, TABLE_NODE_SELECTOR,
    TABLE_ROWNODE_SELECTOR, getTableCells, getTableRows, isExceededSizeTableNode, isTableCellNode, isTableNode,
    Point, Range } from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getFirstPositionInCurrentCell, getFirstPositionInNextCell, getFirstPositionInParagraph,
    getLastNodeFromPositionByNodeName, getLastPositionInCurrentCell, getLastPositionInParagraph, getLastPositionInPrevCell,
    getOxoPosition, getParagraphLength, hasSameParentComponent, isFirstPositionInTableCell, isLastPositionInTableCell,
    isPositionInImplicitShrinkedParagraph, isPositionInTable } from '@/io.ox/office/textframework/utils/position';

// class Table =====================================================

/**
 * Provides static helper methods for manipulation and calculation
 * of table nodes and its children.
 */

// private global functions ===============================================

/**
 * Whether the table cell selection is restricted to only one column.
 *
 * @param {DOM.Range[]} ranges
 *  An array of DOM ranges representing the current browser selection.
 *
 * @returns {Boolean}
 *  Whether the table cell selection is restricted to only one column.
 */
function isSingleColumnSelection(ranges) {

    var startOffsets = [],
        endOffsets = [];

    for (var i = 0; i < ranges.length; i++) {
        if (i > 0) {
            if (!_.contains(startOffsets, ranges[i].start.offset)) { return false; }
            if (!_.contains(endOffsets, ranges[i].end.offset)) { return false; }
        }
        startOffsets.push(ranges[i].start.offset);
        endOffsets.push(ranges[i].end.offset);
    }

    return true;
}

/**
 * Whether the table cell selection is restricted to only one row.
 *
 * @param {DOM.Range[]} ranges
 *  An array of DOM ranges representing the current browser selection.
 *
 * @returns {Boolean}
 *  Whether the table cell selection is restricted to only one row.
 */
function isSingleRowSelection(ranges) {

    var allRows = [];

    for (var i = 0; i < ranges.length; i++) {
        if (i > 0) {
            if (!_.contains(allRows, ranges[i].start.node)) { return false; }
        }
        allRows.push(ranges[i].start.node);
    }

    return true;
}

/**
 * Whether the horizontal modification of the cell range is an expansion or an
 * reduction of the existing cell selection.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {Object} browserSelection
 *  A selection object containing an array of DOM ranges representing the current
 *  browser selection.
 *
 * @param {Object} active
 *  The DOM.Range that describes the cell that is located in the opposite direction
 *  of the anchor cell of a cell selection in the table. This 'active' cell range is
 *  important for expanding or decreasing the cell selection.
 *
 * @param {Boolean} backwards
 *  Whether the modification of the cell selection is backwards (KeyCode.UP_ARROW
 *  and KeyCode.LEFT_ARROW) or not (KeyCode.DOWN_ARROW and KeyCode.RIGHT_ARROW).
 *
 * @returns {Boolean}
 *  Whether the horizontal modification of the cell range is an expansion or an
 *  reduction of the existing cell selection.
 */
function checkHorizontalExpand(selection, browserSelection, active, backwards) {

    var ranges = browserSelection.ranges,
        expand = true;

    if ((ranges.length > 1) && !isSingleColumnSelection(ranges)) {

        if (active.start.offset === ranges[0].start.offset) {
            if (!backwards) {
                expand = false;
            }
        } else if (active.end.offset ===  ranges[ranges.length - 1].end.offset) {
            if (backwards) {
                expand = false;
            }
        }
    }

    return expand;
}

/**
 * Whether the vertical modification of the cell range is an expansion or an
 * reduction of the existing cell selection.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {Object} browserSelection
 *  A selection object containing an array of DOM ranges representing the current
 *  browser selection.
 *
 * @param {Object} active
 *  The DOM.Range that describes the cell that is located in the opposite direction
 *  of the anchor cell of a cell selection in the table. This 'active' cell range is
 *  important for expanding or decreasing the cell selection.
 *
 * @param {Boolean} backwards
 *  Whether the modification of the cell selection is backwards (KeyCode.UP_ARROW
 *  and KeyCode.LEFT_ARROW) or not (KeyCode.DOWN_ARROW and KeyCode.RIGHT_ARROW).
 *
 * @returns {Boolean}
 *  Whether the vertical modification of the cell range is an expansion or an
 *  reduction of the existing cell selection.
 */
function checkVerticalExpand(selection, browserSelection, active, backwards) {

    var ranges = browserSelection.ranges,
        expand = true;

    if ((ranges.length > 1) && !isSingleRowSelection(ranges)) {
        if (active.start.node === ranges[0].start.node) {
            if (!backwards) {
                expand = false;
            }
        } else if (active.end.node === ranges[ranges.length - 1].end.node) {
            if (backwards) {
                expand = false;
            }
        }
    }

    return expand;
}

/**
 * Collecting all offset intervals from an array of DOM.Range objects, that
 * correspond to a specific DOM node. This can be used for a 2-dimensional
 * cell selection in a table to find all offset values belonging to a specific row.
 *
 * @param {Array<DOM.Range>} ranges
 *  An array of DOM ranges representing the current browser selection.
 *
 * @param {Node} node
 *  The DOM node whose offset intervals shall be collected.
 *
 * @returns {Array<Array<Number>>}
 *  An array of sorted intervals with the start and end offset values of the
 *  DOM.Range array ranges. Example: [[1, 2], [2, 3], [3, 4]]
 */
function getAllRangeOffsets(ranges, node) {
    var allOffsets = [];

    for (var i = 0; i < ranges.length; i++) {
        if ((ranges[i].start.node === node) && (ranges[i].end.node === node)) {
            allOffsets.push([ranges[i].start.offset, ranges[i].end.offset]);
        } else if (containsNode(node, ranges[i].start.node, { allowSelf: true }) && containsNode(node, ranges[i].end.node, { allowSelf: true })) {
            // if the selection is not the td but inside a td
            var nodeChildren = $(node).children();
            var startNodeOffset = nodeChildren.index($(ranges[i].start.node).closest('td'));
            var endNodeOffset = nodeChildren.index($(ranges[i].end.node).closest('td')) + 1;
            if (endNodeOffset >= 0) {
                startNodeOffset = startNodeOffset >= 0 ? startNodeOffset : endNodeOffset > 0 ? endNodeOffset - 1 : 0;
                allOffsets.push([startNodeOffset, endNodeOffset]);
            }
        }
    }

    allOffsets.sort(function (i1, i2) { return i1.first - i2.first; });

    return allOffsets;
}

/**
 * Setting the active cell, that is located in the rectangular cell selection
 * in the opposite corner with the anchor cell.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {DOM.Range[]} ranges
 *  An array of DOM ranges representing the current browser selection.
 *
 * @returns {DOM.Range}
 *  The DOM.Range that describes the 'active' cell of a cell selection.
 */
function determineActiveCellRange(selection, ranges) {

    var anchorCellRange = selection.getAnchorCellRange(),
        activeCellNode,
        startOffset,
        endOffset,
        allRangeOffsets;

    if (anchorCellRange === null) {
        // This should not happen.  In the case, that the selection was created with the mouse
        // the anchor node should already be set in processMouseDown().
        // For safety reasons, using upper left corner as anchor.
        selection.setAnchorCellRange(ranges[0]);
        anchorCellRange = ranges[0];
    }

    // the active cell range has to be in the opposite direction of the anchor cell range.

    // checking offset
    allRangeOffsets = getAllRangeOffsets(ranges, anchorCellRange.start.node);

    if (ranges.length === 0 || allRangeOffsets.length === 0) {
        return null;
    }

    if (anchorCellRange.start.offset === allRangeOffsets[0][0]) {
        startOffset = allRangeOffsets[allRangeOffsets.length - 1][0];
        endOffset = allRangeOffsets[allRangeOffsets.length - 1][1];
    } else {
        startOffset = allRangeOffsets[0][0];
        endOffset = allRangeOffsets[0][1];
    }
    // checking the node inside the range
    if (ranges[0].start.node === anchorCellRange.start.node) {
        activeCellNode = ranges[ranges.length - 1].end.node;
    } else {
        activeCellNode = ranges[0].start.node;
    }

    return (new Range(new Point(activeCellNode, startOffset), new Point(activeCellNode, endOffset)));
}

/**
 * Returns whether the passed table cell elements contain equal border formatting attributes.
 *  Table borders are implemented in a way that border attributes are assigned inside each table cell.
 *  We compare top upper left and bottom right cells of each table.
 *
 * @param {HTMLElement|jQuery} cell1
 *  The first DOM element whose formatting attributes will be compared with
 *  the attributes of the other passed DOM element. If this object is a
 *  jQuery collection, uses the first DOM node it contains.
 *
 * @param {HTMLElement|jQuery} cell2
 *  The second DOM element whose formatting attributes will be compared
 *  with the attributes of the other passed DOM element. If this object is
 *  a jQuery collection, uses the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether both DOM elements contain equal explicit formatting attributes.
 */
function hasEqualTableBordersAttributes(cell1, cell2) {

    function getCellBorderAttributes(cell) {
        var // deep copy of complete attribute map
            attributes = _.copy($(cell).data('attributes'), true);

        if (_.isObject(attributes) && ('cell' in attributes)) {
            _.each(attributes.cell, function (attribute, name) {
                // we are interested only in border attributes, kick out others
                if (_.indexOf(['borderBottom', 'borderLeft', 'borderRight', 'borderTop'], name) === -1) {
                    delete attributes.cell[name];
                }
            });

            if (_.isEmpty(attributes.cell)) {
                delete attributes.cell;
            }
        }

        // return attributes as a deep clone
        return _.isObject(attributes) ? attributes : {};
    }

    return _.isEqual(
        getCellBorderAttributes(cell1),
        getCellBorderAttributes(cell2)
    );
}

// static functions =======================================================

/**
 * Creating the grid of widths for all grid positions in a table.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} tablePos
 *  The logical position of the table element
 *
 * @returns {Array<Number>}
 *  Array of grid widths of the table as relative values.
 */
export function getTableGrid(startnode, tablePos) {

    var tablePosition = getDOMPosition(startnode, tablePos),
        tableGrid = null;

    if (tablePosition && !$(tablePosition).is(TABLE_NODE_AND_DRAWING_SELECTOR)) {
        tablePosition = getDOMPosition(startnode, tablePos, true);
    }

    if (tablePosition) {
        tableGrid = getTableGridFromNode(tablePosition.node);
    }

    return tableGrid || [];
}

/**
 * Receiving the table grid attribute from a specified table node.
 *
 * @param {HTMLElement|jQuery} tableNode
 *  The DOM element whose table grid attribute will be returned.
 *  If object is a jQuery collection, uses the first node it
 *  contains.
 *
 * @returns {Array<Number>}
 *  Array of grid widths of the table as relative values.
 */
export function getTableGridFromNode(tableNode) {

    var tableGrid = null;

    if (isTableDrawingFrame(tableNode)) {
        tableNode = $(tableNode).find('table');
    }

    if (isTableNode(tableNode)) {
        tableGrid = getExplicitAttributes(tableNode, 'table').tableGrid;
    }

    return tableGrid || [];

}

/**
 * Calculation the width of a table as the some of the width of all
 * grid positions.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} tablePos
 *  The logical position of the table element
 *
 * @returns {Number}
 *  The width of the table in 1/100 mm
 */
export function getTableWidth(startnode, tablePos) {

    var tableWidth = null,
        tablePosition = getDOMPosition(startnode, tablePos);

    if (tablePosition) {

        var tableNode = tablePosition.node;

        if ($(tableNode).css('width')) {
            tableWidth = $(tableNode).css('width');
        }
    }

    return tableWidth;
}

/**
 * Recalculating the grid widths of a table, if a new column is inserted.
 * Assuming that the width of the complete table does not change.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} tablePos
 *  The logical position of the table element
 *
 * @param {Number} gridPosition
 *  The grid number that is the basis for the new grid
 *
 * @param {String} insertmode
 *  The insert mode can be 'before' or 'behind'. This is relevant for
 *  the position of the added column.
 *
 * @returns {number[] | void} // TODO: check-void-return
 *  Array of grid widths of the table in 1/100 mm
 */
export function getTableGridWithNewColumn(startnode, tablePos, gridPosition, insertmode) {

    var tableGrid = _.copy(getTableGrid(startnode, tablePos), true),
        tableWidth = 0;

    if (!tableGrid) {
        globalLogger.error('Table.getTableGridWithNewColumn(): Unable to get existing table grid');
        return;
    }

    var i = 0;
    for (i = 0; i < tableGrid.length; i++) {
        tableWidth += tableGrid[i];
    }

    var additionalWidth = tableGrid[gridPosition],
        completeWidth = tableWidth + additionalWidth,
        factor = math.roundp(tableWidth / completeWidth, 0.01);

    var insertPos = gridPosition;
    if (insertmode === 'behind') {
        insertPos++;
    }
    tableGrid.splice(insertPos, 0, additionalWidth);  // ignoring insertmode !?

    for (i = 0; i < tableGrid.length; i++) {
        tableGrid[i] = Math.round(factor * tableGrid[i]);  // only ints
    }

    return tableGrid;

}

/**
 * Calculates the grid row range of the specified cell.
 *
 * @param {HTMLTableCellElement|jQuery} cellNode
 *  The table cell element. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @returns {Object}
 *  An object with 'start' and 'end' integer attributes representing the
 *  grid row range of the selected cell (zero-based, closed range). If
 *  the cell has a height of one grid row, start and end will be equal.
 */
export function getGridRowRangeOfCell(cellNode) {

    const $cellNode = $(cellNode);

    var // the parent row node of the cell
        rowNode = $cellNode.parent(),
        // the start grid row index of the cell
        start = rowNode.parent().children('tr').not('.pb-row').index(rowNode);

    // find end grid row index of the cell, and return the result
    return {
        start,
        end: start + parseIntAttribute($cellNode[0], 'rowspan', 1) - 1
    };
}

/**
 * Calculates the grid column range of the specified cell.
 *
 * @param {HTMLTableCellElement|jQuery} cellNode
 *  The table cell element. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @returns {Object}
 *  An object with 'start' and 'end' integer attributes representing the
 *  grid column range of the selected cell (zero-based, closed range). If
 *  the cell has a width of one grid column, start and end will be equal.
 */
export function getGridColumnRangeOfCell(cellNode) {

    var // the start grid column index of the cell
        start = 0;

    // find start index of the cell by adding the colspans of all previous cells
    const $cellNode = $(cellNode);
    $cellNode.prevAll('td').each(function () {
        start += parseIntAttribute(this, 'colspan', 1);
    });

    // find end grid column index of the cell, and return the result
    return {
        start,
        end: start + parseIntAttribute($cellNode[0], 'colspan', 1) - 1
    };
}

/**
 * Calculating the grid position of a selected cell. The attribute
 * 'colspan' of all previous cells in the row have to be added.
 * The return value is 0-based. The first cell in a row always has
 * grid position 0.
 *
 * @param {HTMLTableRowElement|jQuery} rowNode
 *  The table row element. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @param {Number} cellIndex
 *  The DOM child index of the cell inside the row node.
 *
 * @returns {Object}
 *  An object with 'start' and 'end' integer attributes representing the
 *  grid range of the selected cell (closed range). If the cell has a width
 *  of one grid position, start and end will be equal.
 */
export function getGridPositionFromCellPosition(rowNode, cellIndex) {
    return getGridColumnRangeOfCell($(rowNode).children('td').eq(cellIndex));
}

/**
 * Calculating the cell node in a row that fits to the specified
 * grid position. The attribute 'colspan' of all previous cells in
 * the row have to be added.
 *
 * @param {Node} rowNode
 *  The dom node of the row element
 *
 * @param {Number} gridPosition
 *  The grid number that is the basis for the new grid
 *
 * @param {Boolean} defaultToLastCell
 *  This boolean specifies, if the position of the last cell shall be returned,
 *  if no cell is found corresponding to the grid position. This can happen, if
 *  there is a short row between longer rows. In insertColumn a new cell shall
 *  always be added behind the last cell in this short row. In deleteColumns
 *  the last column of the short row shall not be deleted, if the gridposition
 *  is not valid.
 *
 * @param {Boolean} [documentLoaded=true]
 *  Whether the document is loaded completely. This can be important to check,
 *  if attributes are already set. If not, it might be necessary to check the
 *  explicit attributes set at the node. If not specified, the value will be
 *  set to true.
 *
 * @returns {Number}
 *  The cell position that corresponds to the grid position. If no cell has
 *  the specified grid position, the last cell position is returned.
 */
export function getCellPositionFromGridPosition(rowNode, gridPosition, defaultToLastCell, documentLoaded) {

    var cellPosition = 0,
        allCells = $(rowNode).children(),
        gridSum = 0,
        foundCell = true,
        colSpanTarget = gridPosition + 1, // grid position is 0-based
        // whether the document is loaded completely
        docLoaded = (documentLoaded === undefined) ? true : documentLoaded;

    if (defaultToLastCell !== false) {
        defaultToLastCell = true;  // no explicit setting required for 'true'
    }

    allCells.each(function (index) {

        var // the column span of one cell
            colSpan = 0,
            // the explicit attributes at the table cellPosition
            explicitAttrs = null;

        cellPosition = index;
        if (gridSum < colSpanTarget) {

            if (docLoaded) {
                // the attributes are already set, document is loaded
                colSpan = parseIntAttribute(this, 'colspan', 1);
            } else {
                // the attributes are not set, the document is not loaded completely (44286)
                explicitAttrs = getExplicitAttributeSet(this);
                colSpan = (explicitAttrs && explicitAttrs.cell && explicitAttrs.cell.gridSpan) || 1;
            }

            gridSum += colSpan;

            if (gridSum >= colSpanTarget) {
                return false; // leaving the each-loop
            }

        } else {
            return false; // leaving the each-loop
        }
    });

    if (gridSum < colSpanTarget) {
        // maybe there are not enough cells in this row
        foundCell = false;
    }

    // In deleteColumns, cells shall only be deleted, if there is a cell
    // with the specified grid position.
    // In insertColumn, cells shall always be added.

    if (!foundCell && !defaultToLastCell) {
        cellPosition = -1;
    }

    return cellPosition;

}

/**
 * Calculating the sum of all colspans from a selection of cells.
 *
 * @param {jQuery} cellSelection
 *  The jQuery selection of cell element(s).
 *
 * @returns {Number}
 *  The sum of all col spans in the selection of cells.
 */
export function getColSpanSum(cellSelection) {

    var sum = 0;

    cellSelection.each(function () {
        sum += parseIntAttribute(this, 'colspan', 1);
    });

    return sum;
}

/**
 * Shifting the content of table cells from one or more cells to one
 * target cell.
 *
 * @param {jQuery} targetCell
 *  The jQuery selection containing a cell element in which the content
 *  of the other cells shall be included.
 *
 * @param {jQuery} sourceCells
 *  The jQuery selection of cell element(s), whose content shall be
 *  added to another cell.
 */
export function shiftCellContent(targetCell, sourceCells) {

    sourceCells.each(function () {
        targetCell.append($(this).children());
    });
}

/**
 * Collecting for a collection of rows the cell positions that correspond
 * to a specific grid position.
 *
 * @param {jQuery} allRows
 *  The jQuery collection containing row elements, whose cell positions
 *  corresponding to a specific grid position shall be collected in an array.
 *
 * @param {Number} gridposition
 *  The integer grid position.
 *
 * @param {String} insertmode
 *  The calculated cell position depends from the insert mode concerning
 *  the grid position. Allowed values are 'behind' and 'before'.
 *
 * @param {Node} rootNode
 *  The root node corresponding to the logical position.
 *
 * @param {Object} [options]
 *  A map with additional options. The following options are supported:
 *  @param {Boolean} [options.fullPosition=false]
 *      If set to true, the complete logical position of the cells is returned.
 *
 * @returns {Array<Array<Number>>}
 *  An array, that contains the integer insert positions for each row in the
 *  correct order. If fullPosition is set to true, all logical cell positions
 *  of the column are returned.
 */
export function getAllInsertPositions(allRows, gridposition, insertmode, rootNode, options) {

    var allInsertPositions = [],
        // whether the complete logical position shall be returned
        fullPosition = getBooleanOption(options, 'fullPosition', false),
        // the logical position of the row
        rowPosition = null;

    allRows.each(function () {

        var insertPosition = getCellPositionFromGridPosition(this, gridposition);

        if (insertmode === 'behind') {
            insertPosition++;
        }

        if (fullPosition) {
            rowPosition = getOxoPosition(rootNode, this, 0);
            rowPosition.push(insertPosition);
            insertPosition = _.clone(rowPosition);
        }

        allInsertPositions.push(insertPosition);
    });

    return allInsertPositions;
}

/**
 * Collecting for a collection of rows the cell positions that correspond
 * to specified grid start position and grid end position. For each row
 * an array with two integer values is returned, representing the start
 * and the end position of the cells. If no cells correspond to the specified
 * grid position end can be '-1' or even start and end can be '-1'. This
 * happens for example if startgrid and endgrid have high numbers, but the
 * currently investigated row is very short. In the case of removal of cells,
 * no cell is removed from such a cell.
 *
 * @param {jQuery} allRows
 *  The jQuery collection containing row elements, whose cell positions
 *  corresponding to the specified grid positions shall be collected in an array.
 *
 * @param {Number} startgrid
 *  The integer start grid position.
 *
 * @param {Number} endgrid
 *  The integer end grid position.
 *
 * @returns {Array<Array<Number>>}
 *  An array, that contains for each row an array with two integer values
 *  for start and end position of the cells. If no cells correspond to the
 *  specified grid position end can be '-1' or even start and end can be '-1'.
 */
export function getAllRemovePositions(allRows, startgrid, endgrid) {

    var allRemovePositions = [];

    allRows.each(function () {

        var removeRangeInOneRow = [],  // an array collecting the start and end position (integer) of the cells to be removed
            startCol = getCellPositionFromGridPosition(this, startgrid, false),
            endCol = getCellPositionFromGridPosition(this, endgrid, false);

        // startCol and endCol might be '-1', if the grid positions are out of range. In this case, no cells are deleted
        removeRangeInOneRow.push(startCol);
        removeRangeInOneRow.push(endCol);

        allRemovePositions.push(removeRangeInOneRow);
    });

    return allRemovePositions;
}

/**
 * Calculating the relative width of one grid column in relation to the
 * width of the table. The result is the percentage.
 *
 * @param {Array<Number>} tablegrid
 *  The grid array.
 *
 * @param {Number} width
 *  The relative width of one column of the grid array.
 *
 * @returns {Number}
 *  The percentage of the one specified grid width in relation to the
 *  complete grid array.
 */
export function getGridWidthPercentage(tablegrid, width) {

    var fullWidth = 0,
        percentage = 0;

    _(tablegrid).each(function (gridWidth) { fullWidth += gridWidth; });

    percentage = math.roundp(width * 100 / fullWidth, 0.1);

    return percentage;
}

/**
 * Returns the number of rows in the passed table.
 *
 * @param {HTMLElement|jQuery} tableNode
 *  The table element.
 *
 * @returns {Number}
 *  The number of rows in the table.
 */
export function getRowCount(tableNode) {
    return getTableRows(tableNode).length;
}

/**
 * Returns the number of columns (a.k.a. the grid width) in the passed
 * table.
 *
 * @param {HTMLElement|jQuery} tableNode
 *  The table element.
 *
 * @returns {Number}
 *  The number of columns in the table.
 */
export function getColumnCount(tableNode) {
    // do not use the number of <col> elements in <colgroup>, these elements
    // are inserted/deleted deferred while formatting the table. Furthermore
    // in FF there is an extra "col"-element, to prevent wrong border-display
    var attributes = getExplicitAttributeSet(isTableDrawingFrame(tableNode) ? $(tableNode).find('table') : tableNode);
    if (_.isObject(attributes) && _.isObject(attributes.table) && _.isArray(attributes.table.tableGrid)) {
        return attributes.table.tableGrid.length;
    }
    // tableGrid attribute may be missing, e.g. while creating a table
    return 0;
}

/**
 * Returning an array with all logical positions of the cells inside a column, specified
 * by a given cell node.
 *
 * @param {Node} rootNode
 *  The root node corresponding to the logical position.
 *
 * @param {HTMLElement|jQuery} cellNode
 *  The table cell element.
 *
 *  @returns {[]}
 *  An array with all logical positions of all cells in the column
 */
export function getAllCellPositionsInColumn(rootNode, cellNode) {

    var // the table node containing the specified cell
        tableNode = $(cellNode).closest(TABLE_NODE_SELECTOR),
        // the row node containing the specified cell
        rowNode = $(cellNode).closest(TABLE_ROWNODE_SELECTOR),
        // all rows in the table
        allRows = getTableRows(tableNode),
        // the logical position of the cell
        cellPos = getOxoPosition(rootNode, cellNode, 0),
        // the grid position of the cell inside the table
        gridPosition = getGridPositionFromCellPosition(rowNode, _.last(cellPos)).start;

    return getAllInsertPositions(allRows, gridPosition, null, rootNode, { fullPosition: true });
}

/**
 * Returning an array with all cell nodes inside a column, specified
 * by a given cell node.
 *
 * @param {Node} rootNode
 *  The root node corresponding to the logical position.
 *
 * @param {HTMLElement|jQuery} cellNode
 *  The table cell element.
 *
 *  @returns {HTMLTableCellElement[]}
 *  An array with all html table cells.
 */
export function getAllCellsNodesInColumn(rootNode, cellNode) {

    var // a collector for all table cells
        allCellNodes = [],
        // a collector with all logical cell positions of a specified column
        allCellPositions = getAllCellPositionsInColumn(rootNode, cellNode);

    _.each(allCellPositions, function (onePosition) {
        var oneCell = getDOMPosition(rootNode, onePosition).node;
        if (isTableCellNode(oneCell)) {
            allCellNodes.push(oneCell);
        }
    });

    return allCellNodes;
}

/**
 * Forcing the browser to rerender the table. This is required for Firefox
 * that has often a problem with the right table border.
 *
 * @param {HTMLElement|jQuery} table
 *  The table element.
 */
export function forceTableRendering(table) {

    var // the colgroup element inside the table
        colgroup = $(table).children('colgroup');

    colgroup.append($('<col>').width('0%'));
}

/**
 * Updating the column group elements in a table element. This is necessary
 * after inserting or deleting columns or cells.
 *
 * @param {jQuery} table
 *  The `<table>` element whose table attributes have been changed, as
 *  jQuery object.
 *
 * @param {Array<Number>} tablegrid
 *  The grid array.
 */
export function updateColGroup(table, tablegrid) {

    var colgroup = table.children('colgroup'),
        gridsum = 0,
        i = 0;

    colgroup.children('col').remove(); // removing all col entries

    // check if tablegrid contains valid values -> workaround for tablegrids containing only '0'.
    for (i = 0; i < tablegrid.length; i++) { gridsum += tablegrid[i]; }
    if (gridsum === 0) {
        for (i = 0; i < tablegrid.length; i++) { tablegrid[i] += 1000; }
    }

    for (i = 0; i < tablegrid.length; i++) {
        var oneGridWidth = getGridWidthPercentage(tablegrid, tablegrid[i]) + '%';  // converting to %
        colgroup.append($('<col>').width(oneGridWidth));

        // Fix for 29212: Inserting column (or row) requires little trick in colgroup to render right border reliable.
        // Fix for 29675: This is also necessary after removing rows (Firefox only)
        // Fix for 29401: This is also necessary during pasting internal clipboard with tables
        // Fix for 30477: This is also necessary in undo operations with tables
        // Firefox renders correctly after removing entry from colgroup.
        if (i === (tablegrid.length - 1) && _.browser.Firefox) {
            // Forcing Firefox to render the table correctly
            forceTableRendering(table);
        }
    }
}

/**
 * Checks if the table style is dirty and
 * if yes, insert it into the document styles.
 *
 * @param {Generator} generator
 *  The operations generator
 *
 * @param {Editor} editor
 *  The editor instance to use.
 *
 * @param {String} tableStyleId
 *  The table style id
 */
export function checkForLateralTableStyle(generator, editor, tableStyleId) {

    if (_.isString(tableStyleId) && editor.tableStyles.isDirty(tableStyleId)) {

        // insert pending table style to document
        const operProps = {
            attrs: editor.tableStyles.getStyleSheetAttributeMap(tableStyleId),
            type: 'table',
            styleId: tableStyleId,
            styleName: editor.tableStyles.getName(tableStyleId),
            parent: editor.tableStyles.getParentId(tableStyleId),
            uiPriority: editor.tableStyles.getUIPriority(tableStyleId)
        };

        // adding property 'fallbackValue' to themed colors inside the specified attributes
        editor.addFallbackValueToThemeColors(operProps.attrs);

        // parent is optional, should not be send with null or empty string (46937)
        if (operProps.parent === null || operProps.parent === '') { delete operProps.parent; }

        generator.generateOperation(INSERT_STYLESHEET, operProps);
        editor.tableStyles.setDirty(tableStyleId, false);
    }
}

/**
 * Changes the horizontal cell selection in a table using the SHIFT key and
 * the cursor keys.
 *
 * @param {TextApplication} app
 *  The application instance.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {Object} browserSelection
 *  A selection object containing an array of DOM ranges representing the
 *  current browser selection.
 *
 * @param {Object} options
 *  Optional parameters:
 *  @param {Boolean} [options.backwards=false]
 *      If set to true, the current selection will be modified from right
 *      to left or from bottom to top (LEFT cursor key).
 */
export function changeCellSelectionHorz(app, selection, browserSelection, options) {

    if (browserSelection === null) { return; }

    var ranges = browserSelection.ranges,
        newRanges = [],
        allRangeOffsets = null,
        last = ranges.length - 1,
        backwards = getBooleanOption(options, 'backwards', false),
        expandSelection,
        siblingRow = null,
        startPoint = null,
        endPoint = null,
        startOffset,
        endOffset,
        activeCellRange = determineActiveCellRange(selection, browserSelection.ranges),
        activeCellNode = $(activeCellRange.start.node).children().get(activeCellRange.start.offset),
        i = 0;

    // scrolling to the active cell node, if it is not in the visible area
    if (activeCellNode && activeCellNode.length > 0) {
        app.getView().scrollToChildNode(activeCellNode);
    }

    expandSelection = checkHorizontalExpand(selection, browserSelection, activeCellRange, backwards);
    if (backwards) {

        if (expandSelection) {

            if (ranges[0].start.offset > 0) {  // this is not the first cell in the row
                startOffset = ranges[0].start.offset;
                endOffset = ranges[0].end.offset;
                for (i = 0; i < ranges.length; i++) {
                    if ((ranges[i].start.offset === startOffset) && (ranges[i].end.offset === endOffset)) {
                        startPoint = new Point(ranges[i].start.node, startOffset - 1);
                        endPoint = new Point(ranges[i].end.node, endOffset - 1);
                        newRanges.push(new Range(startPoint, endPoint));
                    }
                    newRanges.push(ranges[i]);
                }
                ranges = newRanges;
            } else {

                if ($(ranges[0].start.node).prev().length > 0) {
                    // adding the content from the previous row
                    allRangeOffsets = getAllRangeOffsets(ranges, ranges[0].start.node);
                    siblingRow = $(ranges[0].start.node).prev();
                    while (siblingRow.hasClass('pb-row')) { //skip row containing page break
                        siblingRow = siblingRow.prev();
                    }

                    for (i = allRangeOffsets.length - 1; i >= 0; i--) {
                        startPoint = new Point(siblingRow[0], allRangeOffsets[i][0]);
                        endPoint = new Point(siblingRow[0], allRangeOffsets[i][1]);
                        ranges.unshift(new Range(startPoint, endPoint));
                    }
                }
                // else: Adding content before the table!
            }
        } else {

            for (i = 0; i < ranges.length; i++) {
                if (!Range.equalRangeOffsets(activeCellRange, ranges[i])) {
                    newRanges.push(ranges[i]);
                }
            }
            ranges = newRanges;
        }

    } else {

        if (expandSelection) {

            if (ranges[last].end.offset < $(ranges[last].end.node).children().length) {  // this is not the last cell in the row
                startOffset = ranges[last].start.offset;
                endOffset = ranges[last].end.offset;
                for (i = 0; i < ranges.length; i++) {
                    newRanges.push(ranges[i]);
                    if ((ranges[i].start.offset === startOffset) && (ranges[i].end.offset === endOffset)) {
                        startPoint = new Point(ranges[i].start.node, endOffset);
                        endPoint = new Point(ranges[i].end.node, endOffset + 1);
                        newRanges.push(new Range(startPoint, endPoint));
                    }
                }
                ranges = newRanges;

            } else {
                if ($(ranges[last].end.node).next().length > 0) {
                    // adding the content in the following row
                    siblingRow = $(ranges[last].end.node).next();
                    while (siblingRow.hasClass('pb-row')) { //skip row containing page break
                        siblingRow = siblingRow.next();
                    }
                    allRangeOffsets = getAllRangeOffsets(ranges, ranges[last].end.node);

                } else {
                    // get the tr for the last selection node
                    siblingRow = $(ranges[last].end.node).closest('tr');
                    if (siblingRow.length > 0) {
                        while (siblingRow.hasClass('pb-row')) { //skip row containing page break
                            siblingRow = siblingRow.next();
                        }
                        allRangeOffsets = getAllRangeOffsets(ranges, siblingRow.prev()[0]);
                    }
                }

                if (siblingRow && allRangeOffsets && allRangeOffsets.length > 0) {
                    for (i = 0; i < allRangeOffsets.length; i++) {
                        startPoint = new Point(siblingRow, allRangeOffsets[i][0]);
                        endPoint = new Point(siblingRow, allRangeOffsets[i][1]);
                        ranges.push(new Range(startPoint, endPoint));
                    }
                }
            }

        } else {

            for (i = 0; i < ranges.length; i++) {
                if (!Range.equalRangeOffsets(activeCellRange, ranges[i])) {
                    newRanges.push(ranges[i]);
                }
            }
            ranges = newRanges;
        }
    }

    selection.setCellSelection(ranges);
}

/**
 * Changes the vertical cell selection in a table using the SHIFT key and
 * the cursor keys.
 *
 * @param {TextApplication} app
 *  The application instance.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {Object} browserSelection
 *  A selection object containing an array of DOM ranges representing the
 *  current browser selection.
 *
 * @param {Object} options
 *  Optional parameters:
 *  @param {Boolean} [options.backwards=false]
 *      If set to true, the current selection will be modified from right
 *      to left or from bottom to top (UP cursor key).
 */
export function changeCellSelectionVert(app, selection, browserSelection, options) {

    if (browserSelection === null) { return; }

    var ranges = browserSelection.ranges,
        newRanges = [],
        allRangeOffsets = null,
        last = ranges.length - 1,
        backwards = getBooleanOption(options, 'backwards', false),
        expandSelection,
        siblingRow = null,
        startPoint = null,
        endPoint = null,
        activeCellRange = determineActiveCellRange(selection, browserSelection.ranges),
        activeCellNode = $(activeCellRange.start.node).children().get(activeCellRange.start.offset),
        i = 0;

    // scrolling to the active cell node, if it is not in the visible area
    if (activeCellNode && activeCellNode.length > 0) {
        app.getView().scrollToChildNode(activeCellNode);
    }

    expandSelection = checkVerticalExpand(selection, browserSelection, activeCellRange, backwards);

    if (backwards) {

        if (expandSelection) {

            if ($(ranges[0].start.node).prev().length > 0) {  // this is not the last row in the table

                siblingRow = $(ranges[0].start.node).prev();
                while (siblingRow.hasClass('pb-row')) { //skip row containing page break
                    siblingRow = siblingRow.prev();
                }
                allRangeOffsets = getAllRangeOffsets(ranges, ranges[0].start.node);

                for (i = allRangeOffsets.length - 1; i >= 0; i--) {
                    startPoint = new Point(siblingRow[0], allRangeOffsets[i][0]);
                    endPoint = new Point(siblingRow[0], allRangeOffsets[i][1]);
                    ranges.unshift(new Range(startPoint, endPoint));
                }
            } else {
                return; // Do nothing, keep selection as it is
            }

        } else {

            for (i = 0; i < ranges.length; i++) {
                if (ranges[i].end.node !== ranges[last].end.node) {
                    newRanges.push(ranges[i]);
                }
            }
            ranges = newRanges;
        }

    } else {

        if (expandSelection) {

            if ($(ranges[last].end.node).next().length > 0) {  // this is not the last row in the table

                siblingRow = $(ranges[last].end.node).next();
                while (siblingRow.hasClass('pb-row')) { //skip row containing page break
                    siblingRow = siblingRow.next();
                }
                allRangeOffsets = getAllRangeOffsets(ranges, ranges[last].end.node);

                for (i = 0; i < allRangeOffsets.length; i++) {
                    startPoint = new Point(siblingRow, allRangeOffsets[i][0]);
                    endPoint = new Point(siblingRow, allRangeOffsets[i][1]);
                    ranges.push(new Range(startPoint, endPoint));
                }

            } else {
                return; // Do nothing, keep selection as it is
            }

        } else {

            for (i = 0; i < ranges.length; i++) {
                if (ranges[i].start.node !== ranges[0].start.node) {
                    newRanges.push(ranges[i]);
                }
            }

            ranges = newRanges;
        }

    }

    selection.setCellSelection(ranges);
}

export function changeCellSelectionHorzIE(app, startnode, selection, browserSelection, options) {

    var backwardSelection = selection.isBackwards(),
        backwards = getBooleanOption(options, 'backwards', false),
        start = selection.getStartPosition(),
        end = selection.getEndPosition(),
        multiCellSelection = false,
        positionsInSameTable = false,
        returnObj,
        sibling,
        preventDefault = false;

    positionsInSameTable = ((start.length > 4) && (end.length > 4) && (hasSameParentComponent(start, end, 4)));

    if (backwards) {

        if (backwardSelection || !backwardSelection) {  // Fix for 28667
            // increasing the selection with shift - cursor left

            if (positionsInSameTable) {

                multiCellSelection = !hasSameParentComponent(start, end, 2);

                if (multiCellSelection) {
                    // only selections from beginning of cell to beginning of cell are allowed
                    returnObj = getLastPositionInPrevCell(startnode, _.clone(start));
                    if (!returnObj.beginOfTable) {
                        selection.setTextSelection(returnObj.position, end);
                        preventDefault = true;
                    } else {
                        // trying to get first position before the table
                        sibling = $(getLastNodeFromPositionByNodeName(startnode, start, TABLE_NODE_SELECTOR)).prev();
                        start = getOxoPosition(startnode, sibling, 0);
                        start = getFirstPositionInParagraph(startnode, start);
                        selection.setTextSelection(start, end);
                        preventDefault = true;
                    }

                } else {

                    // creating a cell selection, if the begin of the cell is reached
                    if (isFirstPositionInTableCell(startnode, start)) {
                        returnObj = getLastPositionInPrevCell(startnode, _.clone(start));
                        if (!returnObj.beginOfTable) {
                            selection.setTextSelection(returnObj.position, end, { backwards: true });
                            preventDefault = true;
                        } else {
                            // TODO Setting end position behind the table
                            selection.setTextSelection(start, end);
                            preventDefault = true;
                        }
                    }
                }

            } else if (isPositionInTable(startnode, start)) {
                // only selections from beginning of cell to beginning of cell are allowed
                returnObj = getLastPositionInPrevCell(startnode, _.clone(start));
                if (!returnObj.beginOfTable) {
                    selection.setTextSelection(returnObj.position, end);
                    preventDefault = true;
                } else {
                    // trying to get first position behind the table
                    sibling = $(getLastNodeFromPositionByNodeName(startnode, start, TABLE_NODE_SELECTOR)).prev();
                    start = getOxoPosition(startnode, sibling, 0);
                    start = getLastPositionInParagraph(startnode, start);
                    selection.setTextSelection(start, end);
                    preventDefault = true;
                }

            }  else if (isPositionInTable(startnode, end)) {
                selection.setTextSelection(start, end);
                preventDefault = true;
            }

        } else {

            // decreasing the selection with shift - cursor left -> not handled yet

        }

    } else {

        if (!backwardSelection) {
            // increasing the selection with shift - cursor right

            if (positionsInSameTable) {

                multiCellSelection = !hasSameParentComponent(start, end, 2);

                if (multiCellSelection) {
                    // only selections from beginning of cell to beginning of cell are allowed
                    returnObj = getFirstPositionInNextCell(startnode, _.clone(end));
                    if (!returnObj.endOfTable) {
                        selection.setTextSelection(start, returnObj.position);
                        preventDefault = true;
                    } else {
                        // trying to get first position behind the table
                        sibling = $(getLastNodeFromPositionByNodeName(startnode, end, TABLE_NODE_SELECTOR)).next();
                        end = getOxoPosition(startnode, sibling, 0);
                        end = getFirstPositionInParagraph(startnode, end);
                        selection.setTextSelection(start, end);
                        preventDefault = true;
                    }

                } else {

                    // creating a cell selection, if the end of the cell is reached
                    if (isLastPositionInTableCell(startnode, end)) {
                        start = getFirstPositionInCurrentCell(startnode, start);
                        returnObj = getFirstPositionInNextCell(startnode, start);
                        if (!returnObj.endOfTable) {
                            selection.setTextSelection(start, returnObj.position);
                            preventDefault = true;
                        } else {
                            selection.setTextSelection(start, end);
                            preventDefault = true;
                        }
                    }
                }

            } else if (isPositionInTable(startnode, end)) {
                // only selections from beginning of cell to beginning of cell are allowed
                returnObj = getFirstPositionInNextCell(startnode, _.clone(end));
                if (!returnObj.endOfTable) {
                    selection.setTextSelection(start, returnObj.position);
                    preventDefault = true;
                } else {
                    // trying to get first position behind the table
                    sibling = $(getLastNodeFromPositionByNodeName(startnode, end, TABLE_NODE_SELECTOR)).next();
                    end = getOxoPosition(startnode, sibling, 0);
                    end = getFirstPositionInParagraph(startnode, end);
                    selection.setTextSelection(start, end);
                    preventDefault = true;
                }

            }  else if (isPositionInTable(startnode, start)) {
                selection.setTextSelection(start, end);
                preventDefault = true;
            }

        }

    }

    return preventDefault;

}

/**
 * Replacing an existing cell selection with a text selection. This happens
 * after the ESCAPE key was pressed.
 *
 * @param {Selection} selection
 *  The selection object of the text application.
 *
 * @param {HTMLElement|jQuery} startnode
 *  The start node corresponding to the logical position.
 */
export function resolveCellSelection(selection, startnode) {
    // setting cursor into the first position of the active cell
    var browserSelection = selection.getBrowserSelection(),
        activeCellRange = determineActiveCellRange(selection, browserSelection.ranges),
        cellNode = $(activeCellRange.start.node).children().get(activeCellRange.start.offset);

    selection.setTextSelection(getFirstPositionInCurrentCell(startnode, getOxoPosition(startnode, cellNode, 0)));
}

/**
 * Simulation cursor up and down in MS Internet Explorer and Webkit browsers.
 *
 * @param {Event} event
 *  A jQuery keyboard event object.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Object} options
 *  Optional parameters:
 *  @param {Boolean} [options.up=false]
 *      Whether KeyCode.UP_ARROW was pressed.
 *  @param {Boolean} [options.down=false]
 *      Whether KeyCode.DOWN_ARROW was pressed.
 */
export function updownCursorTravel(event, selection, startnode, options) {

    var startPosition = selection.getStartPosition(),
        textPosition = startPosition[startPosition.length - 1],
        para = $(getDOMPosition(startnode, startPosition).node).closest(PARAGRAPH_NODE_SELECTOR),
        cell = para.closest(TABLE_CELLNODE_SELECTOR),
        row = cell.closest(TABLE_ROWNODE_SELECTOR),
        up = getBooleanOption(options, 'up', false),
        down = getBooleanOption(options, 'down', false),
        readOnly = getBooleanOption(options, 'readonly', false),
        selectionSet = false,
        repeatSearch = true,
        maxTextPos,
        newPos,
        table;

    // Special handling for implicit paragraphs in readonly mode (28563)
    function checkImplicitShrinkedParagraph(pos) {
        if (isPositionInImplicitShrinkedParagraph(startnode, pos)) {
            repeatSearch = true;
            para = $(getDOMPosition(startnode, pos).node).closest(PARAGRAPH_NODE_SELECTOR);
            cell = para.closest(TABLE_CELLNODE_SELECTOR);
            row = cell.closest(TABLE_ROWNODE_SELECTOR);
        }
    }

    if (up) {
        while (repeatSearch) {
            repeatSearch = false;
            if (para.prev().length === 0) {
                while (row.prev().hasClass('pb-row')) { //jump over tr containing page break
                    row = row.prev();
                }
                if (row.prev().length > 0) {
                    event.preventDefault();
                    row = row.prev();
                    cell = row.children().get(cell.prevAll().length);
                    if (cell) {
                        newPos = getLastPositionInCurrentCell(startnode, getOxoPosition(startnode, cell, 0));
                        if (readOnly) { checkImplicitShrinkedParagraph(newPos); }
                        if (!repeatSearch) {
                            if (newPos[newPos.length - 1] > textPosition) { newPos[newPos.length - 1] = textPosition; }
                            selection.setTextSelection(newPos);
                            selectionSet = true;
                        }
                    }
                } else {
                    table = row.closest(TABLE_NODE_SELECTOR);
                    if (table.prev().length > 0) {
                        event.preventDefault();
                        table = table.prev();
                        while (table.hasClass('page-break')) { table = table.prev(); } //skip over page break
                        while (table && isExceededSizeTableNode(table)) { table = table.prev(); }
                        if (table.length === 0) { return true; }  // simply not setting cursor
                        newPos = getLastPositionInParagraph(startnode, getOxoPosition(startnode, table, 0));
                        if (readOnly) { checkImplicitShrinkedParagraph(newPos); }
                        if (!repeatSearch) {
                            if (newPos[newPos.length - 1] > textPosition) { newPos[newPos.length - 1] = textPosition; }
                            selection.setTextSelection(newPos);
                            selectionSet = true;
                        }
                    }
                }
            }
        }
    }

    if (down) {
        while (repeatSearch) {
            repeatSearch = false;
            if (para.next().length === 0) {
                while (row.next().hasClass('pb-row')) { //jump over tr containing page break
                    row = row.next();
                }
                if (row.next().length > 0) {
                    event.preventDefault();
                    row = row.next();
                    cell = row.children().get(cell.prevAll().length);
                    if (cell) {
                        newPos = getFirstPositionInCurrentCell(startnode, getOxoPosition(startnode, cell, 0));
                        if (readOnly) { checkImplicitShrinkedParagraph(newPos); }
                        if (!repeatSearch) {
                            maxTextPos = getParagraphLength(startnode, newPos);
                            newPos[newPos.length - 1] = maxTextPos < textPosition ? maxTextPos : textPosition;
                            selection.setTextSelection(newPos);
                            selectionSet = true;
                        }
                    }
                } else {
                    table = row.closest(TABLE_NODE_SELECTOR);
                    if (table.next().length > 0) {
                        event.preventDefault();
                        table = table.next();
                        while (table.hasClass('page-break')) { table = table.next(); } //skip over page break
                        while (isExceededSizeTableNode(table)) { table = table.next(); }
                        newPos = getFirstPositionInParagraph(startnode, getOxoPosition(startnode, table, 0));
                        if (readOnly) { checkImplicitShrinkedParagraph(newPos); }
                        if (!repeatSearch) {
                            maxTextPos = getParagraphLength(startnode, newPos);
                            newPos[newPos.length - 1] = maxTextPos < textPosition ? maxTextPos : textPosition;
                            selection.setTextSelection(newPos);
                            selectionSet = true;
                        }
                    }
                }
            }
        }
    }

    return selectionSet;
}

/**
 * Setting cursor up and down in Firefox before and behind tables with exceeded size.
 *
 * @param {Event} event
 *  A jQuery keyboard event object.
 *
 * @param {Selection} selection
 *  The selection object of the text application
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Object} options
 *  Optional parameters:
 *  @param {Boolean} [options.up=false]
 *      Whether KeyCode.UP_ARROW was pressed.
 *  @param {Boolean} [options.down=false]
 *      Whether KeyCode.DOWN_ARROW was pressed.
 */
export function updownExceededSizeTable(event, selection, startnode, options) {

    var startPosition = selection.getStartPosition(),
        textPosition = startPosition[startPosition.length - 1],
        para = $(getDOMPosition(startnode, startPosition).node).closest(PARAGRAPH_NODE_SELECTOR),
        up = getBooleanOption(options, 'up', false),
        down = getBooleanOption(options, 'down', false),
        selectionSet = false,
        validElement,
        validPosition;

    if ((up) && (isExceededSizeTableNode(para.prev()))) {
        validElement = para.prev().prev();
        while (isExceededSizeTableNode(validElement)) {
            validElement = validElement.prev();
        }
        // validElement might be a paragraph or a table
        if (validElement.length > 0) {
            validPosition = getLastPositionInParagraph(startnode, getOxoPosition(startnode, validElement, 0));
            if (validPosition[validPosition.length - 1] > textPosition) {
                validPosition[validPosition.length - 1] = textPosition;
            }
            selection.setTextSelection(validPosition);
        } else {
            selection.setTextSelection(startPosition);
        }
        event.preventDefault();
        selectionSet = true;
    }

    if ((down) && (isExceededSizeTableNode(para.next()))) {
        validElement = para.next().next();
        while (isExceededSizeTableNode(validElement)) {
            validElement = validElement.next();
        }
        // validElement might be a paragraph or a table
        validPosition = getLastPositionInParagraph(startnode, getOxoPosition(startnode, validElement, 0));
        if (validPosition[validPosition.length - 1] > textPosition) {
            validPosition[validPosition.length - 1] = textPosition;
        }
        selection.setTextSelection(validPosition);
        event.preventDefault();
        selectionSet = true;
    }

    return selectionSet;
}

/**
 * Removing the attributes that are set at a table and all its cells. This is necessary,
 * if a new table style is assigned to a table. Only if all 'hard' attributes are
 * removed, the style will be visible.
 *
 * @param {TextModel} docModel
 *  The text model instance.
 *
 * @param {Node} rootNode
 *  The root node corresponding to the logical position.
 *
 * @param {HTMLTableElement|jQuery} tableNode
 * The table node element, whose attributes shall be deleted.
 *
 * @param {Array<Number>} position
 *  The logical position of the table.
 *
 * @param {OperationGenerator} generator
 *  The operations generator
 */
export function removeTableAttributes(docModel, rootNode, tableNode, position, generator) {

    var // the options for the table operation
        tableOperationOptions = {},
        // the attribute pool used for table attributes
        attrPool = docModel.tableStyles.attrPool,
        // remove all explicit table attributes
        tableAttributes = attrPool.buildNullValues('table');

    // exclude some table attributes
    delete tableAttributes.width;
    delete tableAttributes.tableGrid;
    delete tableAttributes.paddingTop;
    delete tableAttributes.paddingBottom;
    delete tableAttributes.paddingLeft;
    delete tableAttributes.paddingRight;
    delete tableAttributes.exclude;  // this is an attribute of the table!

    tableOperationOptions.start = _.clone(position);
    tableOperationOptions.attrs = { table: tableAttributes };

    // removing the table attributes
    generator.generateOperation(SET_ATTRIBUTES, tableOperationOptions);

    getTableCells(tableNode).each(function () {

        var // the options for the cell operation
            cellOperationOptions = {},
            // remove all explicit cell attributes
            cellAttributes = attrPool.buildNullValues('cell'),
            // the old element attributes
            oldAttrs = null;

        // exclude some cell attributes
        delete cellAttributes.gridSpan;
        delete cellAttributes.paddingTop;
        delete cellAttributes.paddingBottom;
        delete cellAttributes.paddingLeft;
        delete cellAttributes.paddingRight;

        cellOperationOptions.start = _.clone(getOxoPosition(rootNode, this, 0));
        cellOperationOptions.attrs = { cell: cellAttributes };

        // Defining the oldAttrs, if change tracking is active
        if (docModel.getChangeTrack().isActiveChangeTracking()) {
            // Expanding operation for change tracking with old explicit attributes
            oldAttrs = docModel.getChangeTrack().getOldNodeAttributes(this);
            // adding the old attributes for change tracking
            if (oldAttrs) {
                oldAttrs = _.extend(oldAttrs, docModel.getChangeTrack().getChangeTrackInfo());
                cellOperationOptions.attrs.changes = { modified: oldAttrs };
            }
        }

        // generate the 'setAttributes' operation for each cell, removing cell attributes
        generator.generateOperation(SET_ATTRIBUTES, cellOperationOptions);
    });
}

/**
 * Setting the width attribute to all cells inside a table. This function is used, if the
 * width of the borders inside a table are set from the GUI.
 *
 * @param {HTMLTableElement|jQuery} tableNode
 * The table node element, whose attributes shall be deleted.
 *
 * @param {Object} attributes
 *  A map of attribute value maps (name/value pairs), keyed by
 *  attribute families.
 *
 * @param {TableCellStyles} tableCellStyles
 *  The available table cell styles
 *
 * @param {Node} rootNode
 *  The root node corresponding to the logical position.
 *
 * @param {Generator} generator
 *  The operations generator
 *
 * @param {TextModel} model
 *  The text model instance.
 */
export function setBorderWidthToVisibleCells(tableNode, attributes, tableCellStyles, rootNode, generator, model) {

    getTableCells(tableNode).each(function () {

        var cellAttributes = tableCellStyles.getElementAttributes(this).cell,
            operationOptions = {},
            cellOperationOptions = {},
            allCellOperationOptions = {},
            border = null,
            createOperation = false,
            oldAttrs = null;

        operationOptions.start = _.clone(getOxoPosition(rootNode, this, 0));

        _.each(['borderTop', 'borderBottom', 'borderLeft', 'borderRight'], function (borderName) {
            if (cellAttributes[borderName].style !== 'none') {
                border = _.clone(cellAttributes[borderName]);
                border.width = attributes.table.borderTop.width; // using borderTop (any other border would be good, too)
                cellOperationOptions[borderName] = border;
                createOperation = true;
            }
        });

        if (createOperation) {
            allCellOperationOptions.cell = cellOperationOptions;
            operationOptions.attrs = allCellOperationOptions;

            // INFO: In Presentation app it is possible, that 'borderInsideHor' and 'borderInsideVert' are set
            //       to the 'cell' family in table styles. Therefore it can happen, that these properties are
            //       set via the conditionalAttributes although this might not be wanted (48609). Then this
            //       properties need to be overwritten with hard attributes in operations.
            if (model.useSlideMode()) {
                operationOptions.attrs.cell.borderInsideHor = _.clone(NONE);
                operationOptions.attrs.cell.borderInsideVert = _.clone(NONE);
            }

            // Defining the oldAttrs, if change tracking is active
            if (model.getChangeTrack().isActiveChangeTracking()) {
                // Expanding operation for change tracking with old explicit attributes
                oldAttrs = model.getChangeTrack().getOldNodeAttributes(this);
                // adding the old attributes for change tracking
                if (oldAttrs) {
                    oldAttrs = _.extend(oldAttrs, model.getChangeTrack().getChangeTrackInfo());
                    operationOptions.attrs.changes = { modified: oldAttrs };
                }
            }

            // generate the 'setAttributes' operation for each cell, removing cell attributes
            generator.generateOperation(SET_ATTRIBUTES, operationOptions);
        }
    });
}

/**
 * Setting the border flags to all cells inside a table. This function is used, if the
 * mode for the table is set from the GUI. The border flags define, which borders are
 * visible and which not. This is defined with the properties 'borderTop', 'borderBottom',
 * 'borderLeft', 'borderRight', 'borderInsideHor' and 'borderInsideVert' for the whole
 * table. This function calculates from the table properties the properties for each single
 * cell, dependent from its position inside the table.
 *
 * @param {HTMLTableElement|jQuery} tableNode
 * The table node element, whose attributes shall be deleted.
 *
 * @param {Object} attributes
 *  A map of attribute value maps (name/value pairs), keyed by
 *  attribute families.
 *
 * @param {Number} borderWidth
 *  The unified width of all borders in the cell (in pt), if available.
 *  Switching border flags must not modify border width.
 *
 * @param {Node} rootNode
 *  The root node corresponding to the logical position.
 *
 * @param {Generator} generator
 *  The operations generator
 *
 * @param {TextModel} model
 *  The text model instance.
 */
export function setBorderFlagsToCells(tableNode, attributes, borderWidth, rootNode, generator, model) {

    var tableAttributes = attributes.table;

    // Using border width, that is set at the side pane (if available)
    // -> Switching border flags must not modify border width -> Fix for 29019
    // converting from pt to 1/100 mm
    if (borderWidth) {
        borderWidth = convertLengthToHmm(borderWidth, 'pt');
    }

    getTableCells(tableNode).each(function () {

        var firstRow = ($(this).parent().prev().length === 0),
            firstCol = ($(this).prev().length === 0),
            lastRow = ($(this).parent().next().length === 0),
            lastCol = ($(this).next().length === 0),
            useBorders = { borderTop: 'borderInsideHor', borderRight: 'borderInsideVert', borderBottom: 'borderInsideHor', borderLeft: 'borderInsideVert' },
            operationOptions = {},
            cellOperationOptions = {},
            allCellOperationOptions = {},
            border = null,
            createOperation = false,
            oldAttrs = null;

        operationOptions.start = _.clone(getOxoPosition(rootNode, this, 0));

        if (firstRow) { useBorders.borderTop = 'borderTop'; }
        if (lastCol) { useBorders.borderRight = 'borderRight'; }
        if (lastRow) { useBorders.borderBottom = 'borderBottom'; }
        if (firstCol) { useBorders.borderLeft = 'borderLeft'; }

        _.each(['borderTop', 'borderBottom', 'borderLeft', 'borderRight'], function (borderName) {
            if (useBorders[borderName] in tableAttributes) {
                border = _.clone(tableAttributes[useBorders[borderName]]);
                if (borderWidth) { border.width = borderWidth; }  // overwriting border width, if available (Fix for 29019)
                cellOperationOptions[borderName] = border;
                createOperation = true;
            }
        });

        if (createOperation) {

            // INFO: In Presentation app it is possible, that 'borderInsideHor' and 'borderInsideVert' are set
            //       to the 'cell' family in table styles. Therefore it can happen, that these properties are
            //       set via the conditionalAttributes although this might not be wanted (48609). Then this
            //       properties need to be overwritten with hard attributes in operations.
            if (model.useSlideMode()) {
                cellOperationOptions.borderInsideHor = _.clone(NONE);
                cellOperationOptions.borderInsideVert = _.clone(NONE);
            }

            allCellOperationOptions.cell = cellOperationOptions;
            operationOptions.attrs = allCellOperationOptions;

            // Defining the oldAttrs, if change tracking is active
            if (model.getChangeTrack().isActiveChangeTracking()) {
                // Expanding operation for change tracking with old explicit attributes
                oldAttrs = model.getChangeTrack().getOldNodeAttributes(this);
                // adding the old attributes for change tracking
                if (oldAttrs) {
                    oldAttrs = _.extend(oldAttrs, model.getChangeTrack().getChangeTrackInfo());
                    operationOptions.attrs.changes = { modified: oldAttrs };
                }
            }

            // generate the 'setAttributes' operation for each cell
            generator.generateOperation(SET_ATTRIBUTES, operationOptions);
        }
    });
}

/**
 * Returns whether merge operation can be applied on two tables.
 * Maximum number of rows and cells cannot be exceeded.
 * The maximum number of rows and cells can
 * be defined in the configuration.
 *
 * @param {jQuery|Node} firstTable
 *  First of two table nodes for which we are checking merge possibility
 *
 * @param {jQuery|Node} secondTable
 *  Second table node
 *
 * @returns {Boolean}
 *  If merge is possible or not
 */
export function mergingTableHasAllowedSize(firstTable, secondTable) {

    var // the number of rows in the first table
        rowCountFirstTable = getRowCount(firstTable),
        // the number of rows in the second table
        rowCountSecondTable = getRowCount(secondTable),
        // the number of columns in the first table
        colCountFirstTable = getColumnCount(firstTable),
        // the number of columns in the second table
        colCountSecondTable = getColumnCount(secondTable),
        // summed row count of both tables
        rowCount = rowCountFirstTable + rowCountSecondTable,
        // summed col count of both tables
        colCount = colCountFirstTable + colCountSecondTable;

    // check maximum row count and maximum cell count
    return _.isNumber(rowCount) && _.isNumber(colCount) &&
        (rowCount < MAX_TABLE_ROWS) &&
        ((rowCount + 1) * colCount <= MAX_TABLE_CELLS);
}

/**
 * Returns whether merge operation can be applied on two tables. This
 * includes five tests:
 *
 * - both tables have a table grid with equal length.
 * - both tables have same table style
 * - both tables have same border attributes
 * - both tables are not exceeded-size tables.
 * - the new created table is not an exceeded-size table.
 *
 * @param {jQuery|Node} firstTable
 *  First of two table nodes for which we are checking merge possibility
 *
 * @param {jQuery|Node} secondTable
 *  Second of two table nodes for which we are checking merge possibility
 *
 * @returns {Boolean}
 *  If merge of two specified tables is possible or not
 */
export function mergeableTables(firstTable, secondTable) {
    var
        firstTableStyleId = getElementStyleId(firstTable),
        secondTableStyleId = getElementStyleId(secondTable),
        // collection of all first level children cells in first passed table
        tableCellsFirstTable = getTableCells(firstTable),
        // collection of all first level children cells in second passed table
        tableCellsSecondTable = getTableCells(secondTable),
        // first cell node of first table
        firstTableFirstCell = tableCellsFirstTable.first(),
        // first cell node of second table
        secondTableFirstCell = tableCellsSecondTable.first(),
        // last cell node of first table
        firstTableLastCell = tableCellsFirstTable.last(),
        // last cell node of second table
        secondTableLastCell = tableCellsSecondTable.last();

    // comparing table grid of both tables
    if (getTableGridFromNode(firstTable).length !== getTableGridFromNode(secondTable).length) { return false; }

    // comparing table style of both tables
    if (firstTableStyleId !== secondTableStyleId) { return false; }

    // comparing borders of both tables - we compare top upper left and bottom right cells of each table
    if (!hasEqualTableBordersAttributes(firstTableFirstCell, secondTableFirstCell) || !hasEqualTableBordersAttributes(firstTableLastCell, secondTableLastCell)) {
        return false;
    }

    // excluding exceeded-size tables
    if ((isExceededSizeTableNode(firstTable) || isExceededSizeTableNode(secondTable))) { return false; }

    // checking that new table is not an exceeded size table
    if (!mergingTableHasAllowedSize(firstTable, secondTable)) { return false; }

    return true;
}

/**
 * Helper function that checks, if a specified table cell is a horizontally merged cell.
 *
 * @param {Node|jQuery} cell
 *  The table cell node.
 *
 * @returns {Boolean}
 *  Whether the specified cell node is horizontally merged.
 */
export function isHorizontallyMergedCell(cell) {
    var expCellAttributes = getExplicitAttributes(cell, 'cell');
    return !!(expCellAttributes && _.isNumber(expCellAttributes.gridSpan) && expCellAttributes.gridSpan > 1);
}

/**
 * Getting the horizontal length of a table cell.
 *
 * @param {Node|jQuery} cell
 *  The table cell node.
 *
 * @returns {Number|Null}
 *  The horizontal length of a table cell. Or null, if the specified node is no table cell.
 */
export function getHorizontalCellLength(cell) {

    if (!isTableCellNode(cell)) { return null; }

    var expCellAttributes = getExplicitAttributes(cell, 'cell');

    return (expCellAttributes && _.isNumber(expCellAttributes.gridSpan)) ? expCellAttributes.gridSpan : 1;
}

/**
 * Collecting the cell border attributes, so that after the table is
 * completely formatted, the border attributes for the table can be
 * calculated (DOCS-2400).
 *
 * @param {HTMLTableCellElement|jQuery} cell
 *  The table cell element.
 *
 * @param {Object} cellAttrs
 *  The set of attributes that is assigned to the table cell.
 *
 * @param {Object[]} cellBorderCollector
 *  The collector for the cell attributes and the cell position inside
 *  the table.
 */
export function setCellBorderAttributes(cell, cellAttrs, cellBorderCollector) {
    var odtBorderAttrs = { attrs: cellAttrs };
    odtBorderAttrs.colInterval = getGridColumnRangeOfCell(cell);
    odtBorderAttrs.rowInterval = getGridRowRangeOfCell(cell);
    cellBorderCollector.push(odtBorderAttrs);
}
