/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import { is } from '@/io.ox/office/tk/algorithms';
import { parseCssLength } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { calculateUserId, resolveUserId } from '@/io.ox/office/editframework/utils/operationutils';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { getMSFormatIsoDateString } from '@/io.ox/office/editframework/utils/dateutils';

import { isDrawingFrame, isTextFrameShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { BREAK, SMALL_DEVICE, convertCssLength, findNextNode, findPreviousNode,
    getBooleanOption, getDomNode, getStringOption } from '@/io.ox/office/textframework/utils/textutils';
import * as Op from '@/io.ox/office/textframework/utils/operations';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, getDOMPosition, getFirstPositionInParagraph, getNodeStartAndEndPosition, getOxoPosition,
    getParagraphNodeLength, getTextLevelPositionInNode, increaseLastIndex, isParagraphPosition,
    isValidElementRange } from '@/io.ox/office/textframework/utils/position';
import { getAllCellsNodesInColumn, getGridColumnRangeOfCell, mergeableTables } from '@/io.ox/office/textframework/components/table/table';

// constants ==============================================================

// the allowed changeTrack types (order is relevant)
const CHANGE_TRACK_TYPES = ['inserted', 'removed', 'modified'];
// the allowed changeTrack information saved at nodes
const CHANGE_TRACK_INFOS = ['author', 'date', 'userId'];
// the allowed changeTrack information saved at nodes
const CHANGE_TRACK_AUTHOR_INFOS = ['author', 'userId'];
// clip board operations, that will be expanded with change tracking information (explicitly not setAttributes)
const CHANGETRACK_CLIPBOARD_OPERATIONS = [
    Op.TEXT_INSERT,
    Op.INSERT_DRAWING,
    Op.RANGE_INSERT,
    Op.FIELD_INSERT,
    Op.COMPLEXFIELD_INSERT,
    Op.TAB_INSERT,
    Op.HARDBREAK_INSERT,
    Op.PARA_INSERT,
    Op.ROWS_INSERT,
    Op.TABLE_INSERT
];
// whether the resizing of a drawing needs to be change tracked
const CHANGE_TRACK_SUPPORTS_DRAWING_RESIZING = false;
// whether the moving of a drawing needs to be change tracked
const CHANGE_TRACK_SUPPORTS_DRAWING_MOVE = false;
// whether the assignment of a position to a drawing needs to be change tracked
const CHANGE_TRACK_SUPPORTS_DRAWING_POSITION = false;

// the timer interval for updating change track time (in ms).
const UPDATE_DATE_INTERVAL = 30000;

// class ChangeTrack ======================================================

/**
 * An instance of this class represents a change track handler in the edited document.
 *
 * @param {TextModel} docModel
 *  The text model instance.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The root node of the document. If this object is a jQuery collection,
 *  uses the first node it contains.
 */
class ChangeTrack extends ModelObject {

    // the document root node
    #rootNode;
    // the current changeTrack date
    #changeTrackInfoDate = null;
    // the local user id of the current user
    #userid = null;
    // the interval timer for the repeated date update process
    #dateUpdateTimer = null;
    // the change track selection object
    // -> activeNode: HTMLElement, the one currently node used by the iterator
    // -> activeNodeType: String, the one currently used node type, one of 'inserted', 'removed', 'modified'
    // -> activeNodeTypesOrdered: String[], ordered list of strings, corresponding to priority of node types
    // -> selectedNodes: HTMLElement[], sorted list of HTMLElements of the group belonging to 'activeNode' and 'activeNodeType
    // -> allChangeTrackNodes: jQuery, optional, cache for performance reasons, contains all change track elements in the document
    #changeTrackSelection = null;
    // a collector for additional clipboard operations
    #clipboardCollector = [];
    // css class for showing change track nodes
    #highlightClassname = 'change-track-highlight';
    // whether change track is supported in the current context (for example not inside comments)
    #isChangeTrackSupported = true;
    // whether the handler for the change track side bar are active or not
    #sideBarHandlerActive = false;
    // the node for the change track side bar
    #sideBarNode = null;
    // the node containing the scroll bar
    #scrollNode = null;
    // the node with the page content
    #pageContentNode = null;
    // collector for all change tracked nodes (required only for the side bar)
    #allNodesSideBarCollector = null;
    // a collector for all markers in the change track side bar
    #allSideBarMarker = {};

    constructor(docModel, rootNode) {

        // base constructor ---------------------------------------------------

        super(docModel);

        // initialization -----------------------------------------------------

        this.#rootNode = rootNode;

        this.docApp.onInit(() => {

            // updating the model after the document was reloaded (after cancelling long running actions)
            this.listenTo(this.docModel, 'document:reloaded', this.#handleDocumentReload);

            // undo/redo need to remove an existing change track selection (55245)
            this.listenTo(this.docModel.getUndoManager(), 'undo:after redo:after', this.clearChangeTrackSelection);
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Whether the assignment of a new position (in-line, left floated, ...) to a drawing
     * needs to be change tracked. Currently this is not supported in OOXML format.
     * Therefore the change track information will not be available in MS Word and will
     * be lost after reloading a document into OX Text. In this case, it will lead to an
     * error, if the change will be rejected in OX Text.
     */
    ctSupportsDrawingPosition() {
        return CHANGE_TRACK_SUPPORTS_DRAWING_POSITION;
    }

    /**
     * Whether the resizing of a drawing in OX Text needs to be change tracked. Currently
     * this is not supported in OOXML format. Therefore the change track information will
     * not be available in MS Word and will be lost after reloading a document into OX Text.
     * In this case, it will lead to an error, if the change will be rejected in OX Text.
     */
    ctSupportsDrawingResizing() {
        return CHANGE_TRACK_SUPPORTS_DRAWING_RESIZING;
    }

    /**
     * Whether the moving of a drawing in OX Text needs to be change tracked. Currently
     * this is not supported in OOXML format. Therefore the change track information will
     * not be available in MS Word and will be lost after reloading a document into OX Text.
     * In this case, it will lead to an error, if the change will be rejected in OX Text.
     * On the other hand, OOXML supports change tracking of moving of in-line drawings
     * (from in-line to in-line), what is not supported by OX Text yet.
     */
    ctSupportsDrawingMove() {
        return CHANGE_TRACK_SUPPORTS_DRAWING_MOVE;
    }

    /**
     * Updates the current date for change track operations on a regular basis.
     * This is needed for performance reasons, so that the function
     * getMSFormatIsoDateString() is not called for every operation.
     */
    updateChangeTrackInfoDate() {
        this.#changeTrackInfoDate = getMSFormatIsoDateString();
    }

    /**
     * Returns, whether change tracking is activated on the document.
     *
     * @returns {Boolean}
     *  Whether change tracking is activated locally.
     */
    isActiveChangeTracking() {
        if (this.docApp.isODF()) { return false; } // in odf files change tracking is not supported yet
        return (this.#isChangeTrackSupported && this.docModel.globalConfig.changeTracking) || false;
    }

    /**
     * Returns, whether change tracking is activated in the document attributes. This state is
     * shown in the UI check box. This is not used during working with the document, because the
     * state can be changed temporarily, for example if a comment node is activated.
     *
     * @returns {Boolean}
     *  Whether change tracking is activated locally (independent from the global variable
     *  'this.#isChangeTrackSupported' that can be used to deactivate change tracking temporarily).
     */
    isActiveChangeTrackingInDocAttributes() {
        // in odf files change tracking is not supported yet
        return !this.docApp.isODF() && this.docModel.globalConfig.changeTracking;
    }

    /**
     * Sets the state for change tracking to 'true' or 'false'.
     * If set to true, change tracking is activated on the document.
     *
     * @param {Boolean} state
     *  The state for the change tracking. 'true' enables change tracking,
     *  'false' disables' change tracking.
     *
     * @returns {Boolean}
     *  Whether the state of change change tracking was set successfully.
     */
    setActiveChangeTracking(state) {
        if (typeof state !== 'boolean') { return false; }
        const success = this.docModel.applyOperations({ name: Op.CHANGE_CONFIG, attrs: { document: { changeTracking: state } } });
        // activating the change tracking timer update
        if (state) {
            this.#startDateUpdate();
        } else {
            this.#stopDateUpdate();
        }

        return success;
    }

    /**
     * Returns the current state of the global 'this.#isChangeTrackSupported'. This
     * variable can be used to disable change tracking temporarily, for example
     * inside comments.
     *
     * @returns {Boolean}
     *  Whether change tracking is supported in the current context.
     */
    isChangeTrackSupported() {
        return this.#isChangeTrackSupported;
    }

    /**
     * Sets the state of the global variable 'this.#isChangeTrackSupported'. This
     * variable can be used to disable change tracking temporarily, for example
     * inside comments.
     *
     * @param {Boolean} state
     *  Setting the state for the change tracking support in the current context.
     */
    setChangeTrackSupported(state) {
        this.#isChangeTrackSupported = state;
    }

    /**
     * Returning the current author of the application as string
     * with full display name.
     *
     * @returns {String}
     *  The author string for the change track info object.
     */
    getChangeTrackInfoAuthor() {
        return this.docApp.getClientOperationName();
    }

    /**
     * Returning the current date as string in ISO date format.
     * This function calls getMSFormatIsoDateString() to simulate
     * the MS behavior, that the local time is send a UTC time. The
     * ending 'Z' in the string represents UTC time, but MS always
     * uses local time with ending 'Z'.
     *
     * @returns {String}
     *  The date string for the change track info object.
     */
    getChangeTrackInfoDate() {
        // Performance: this.#changeTrackInfoDate is stored in a variable, needs only to be updated every 30s
        if (!this.#dateUpdateTimer && this.isActiveChangeTracking()) { this.#startDateUpdate(); }
        return this.#changeTrackInfoDate || getMSFormatIsoDateString();
    }

    /**
     * Returning an object used for additional change track information with author
     * and date of tracked change.
     *
     * @returns {Object}
     *  The change track info object containing author and date.
     */
    getChangeTrackInfo() {
        if (!this.#dateUpdateTimer && this.isActiveChangeTracking()) { this.#startDateUpdate(); }
        if (!this.#userid) { this.#userid = calculateUserId(ox.user_id); }
        return { author: this.getChangeTrackInfoAuthor(), date: this.getChangeTrackInfoDate(), userId: this.#userid.toString() };
    }

    /**
     * Receiving the author, date and userId of a change track node for a specified type.
     *
     * @param {Node|jQuery|Null} node
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @param {String} info
     *  The info that is searched from the node. Allowed values are listed in
     *  'CHANGE_TRACK_INFOS'. 'author', 'date' and 'userId' are currently supported.
     *
     * @param {String} type
     *  The type for which the author is searched. Allowed values are listed in
     *  'CHANGE_TRACK_TYPES'.
     *
     * @returns {Number|String|Null}
     *  The requested information. This can be the 'author', the 'date' or null,
     *  if the information could not be found.
     */
    getChangeTrackInfoFromNode(node, info, type) {

        if (!node || !_.contains(CHANGE_TRACK_TYPES, type) || !_.contains(CHANGE_TRACK_INFOS, info)) { return null; }

        if (DOM.isTextComponentNode(node) && !DOM.isComplexFieldNode(node) && !DOM.isRangeMarkerNode(node)) { node = getDomNode(node).firstChild; }

        // the author or date of the change tracked node
        let value = null;
        // the explicit attributes set at the specified node
        const explicitAttributes = getExplicitAttributeSet(node);
        // an optionally specified author ID
        let authorId = null;
        // an optionally available author info object from the globals author list
        let authorInfo = null;

        if (explicitAttributes && explicitAttributes.changes && explicitAttributes.changes[type]) {
            value = explicitAttributes.changes[type][info];

            if (!value && _.contains(CHANGE_TRACK_AUTHOR_INFOS, info)) {
                authorId = explicitAttributes.changes[type].authorId; // support of property 'authorId'
                if (!_.isNull(authorId) && !_.isUndefined(authorId)) {
                    authorInfo = this.#getAuthorFromAuthorId(authorId);
                    value = this.#getAuthorInfo(authorInfo, info);
                }
            }

            if (info === 'userId') { value = resolveUserId(parseInt(value, 10)); }
        }

        return value;
    }

    /**
     * Checking whether a specified node is a change tracked 'inserted' node, that was inserted
     * by the currently editing user.
     *
     * @param {Node|jQuery|Null} node
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @returns {Boolean}
     *  Whether a specified node is a change tracked 'inserted' node, that was inserted
     *  by the currently editing user.
     */
    isInsertNodeByCurrentAuthor(node) {
        return node && DOM.isChangeTrackInsertNode(node) && (this.getChangeTrackInfoFromNode(node, 'author', 'inserted') === this.getChangeTrackInfoAuthor());
    }

    /**
     * Checking whether all specified nodes in the nodes collector are change tracked
     * 'inserted', and this was inserted by the currently editing user.
     *
     * @param {Node[]|jQuery|Null} nodes
     *  The DOM nodes to be checked. This can be an array of nodes or a jQuery collection.
     *  If missing or empty, returns false.
     *
     * @returns {Boolean}
     *  Whether all specified nodes are change tracked 'inserted', and this was inserted
     *  by the currently editing user.
     */
    allAreInsertedNodesByCurrentAuthor(nodes) {

        if (!nodes || (nodes.length === 0)) { return false; }

        return $(nodes).get().every(node => {
            return this.isInsertNodeByCurrentAuthor(node);
        });
    }

    /**
     * Checking whether a specified node has a specified change track type.
     *
     * @param {Node|jQuery|Null} node
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @param {String} type
     *  The type for which the node is checked. Allowed values are listed in
     *  'CHANGE_TRACK_TYPES'.
     *
     * @returns {Boolean}
     *  Whether the specified node has the specified change track type.
     */
    nodeHasSpecifiedChangeTrackType(node, type) {
        return node && (
            (type === 'inserted' && DOM.isChangeTrackInsertNode(node)) ||
            (type === 'removed' && DOM.isChangeTrackRemoveNode(node)) ||
            (type === 'modified' && DOM.isChangeTrackModifyNode(node))
        );
    }

    /**
     * Checking whether a specified node has a specified author for a specified
     * change track type.
     *
     * @param {Node|jQuery|Null} node
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @param {String} type
     *  The type for which the author is searched. Allowed values are listed in
     *  'CHANGE_TRACK_TYPES'.
     *
     * @param {String} author
     *  The specified author, who is checked to be the change track owner.
     *
     * @returns {Boolean}
     *  Whether the specified node has the specified author.
     */
    nodeHasChangeTrackAuthor(node, type, author) {
        return node && this.nodeHasSpecifiedChangeTrackType(node, type) && (this.getChangeTrackInfoFromNode(node, 'author', type) === author);
    }

    /**
     * During pasting content into the document, if change tracking is NOT
     * activated, one additional setAttributes operations is required,
     * directly following the first splitParagraph operation. This
     * setAttributes operation removes change tracking information, from
     * the split paragraph.
     *
     * @param {Object[]} operations
     *  The list of all operations that will be applied during pasting.
     */
    removeChangeTrackInfoAfterSplitInPaste(operations) {

        // a counter for the operations
        let counter = 0;
        // the first split operation in the operations array
        let splitOperation = null;

        // helper function to create a setAttributes operation
        // to remove change tracks family.
        const generateChangeTrackSetAttributesOperation = operation => {
            const paraPos = _.clone(operation.start);
            paraPos.pop();
            paraPos[paraPos.length - 1]++;
            return { name: Op.SET_ATTRIBUTES, start: _.copy(paraPos), attrs: { changes: { inserted: null, removed: null, modified: null } } };
        };

        // finding the first splitParagraph operation
        splitOperation = _.find(operations, operation => {
            counter++;
            return (Op.PARA_SPLIT === operation.name);
        });

        // Adding a setAttributes operation behind the first splitParagraph operation
        if (splitOperation) {
            operations.splice(counter, 0, generateChangeTrackSetAttributesOperation(splitOperation));
        }
    }

    /**
     * During pasting content into the document, if change tracking is activated,
     * the change track information need to be added to the operations.
     * Additionally some additional setAttributes operations are required,
     * directly following the splitParagraph operations.
     * If there is content pasted into a comment, this must not be change tracked.
     *
     * @param {Object[]} operations
     *  The list of all operations that will be applied during pasting.
     */
    handleChangeTrackingDuringPaste(operations) {

        // the information object for the change tracking
        const changeTrackInfo = this.getChangeTrackInfo();

        _(operations).map(operation => {
            if (!operation) { return null; }
            this.#addChangeTrackInfoToOperation(operation, changeTrackInfo, { clipboard: true });
            return operation;
        });

        // order the new setAttributes operations into the correct positions
        // in the operations collector
        if (this.#clipboardCollector.length) {
            _.each(operations, (operation, index) => {
                if (Op.PARA_SPLIT === operation.name) {
                    const newOperation = this.#clipboardCollector.shift();
                    operations.splice(index + 1, 0, newOperation);
                }
            });
            // operations = operations.concat(this.getClipboardCollector());
            this.#clipboardCollector = [];
        }
    }

    /**
     * Shows the change track group of a user selection by setting
     * temporary background colors to all change tracking nodes,
     * which belongs to the current change track node of the selection.
     */
    showChangeTrackGroup() {

        // do nothing with range selections (explicitely also not with drawing selections)
        if (this.docModel.getSelection().hasRange()) { return; } // && !this.docModel.getSelection().isDrawingFrameSelection()) { return; }

        // the node currently selected by the text cursor (or at the beginning of an arbitrary selection range)
        const node = this.#findChangeTrackNodeAtCursorPosition();

        // quit if a change track node is not found
        if (node.length === 0) { return; }

        const nodeRelevantType = this.getRelevantTrackingTypeByNode(node);
        const nodeGroup = this.#groupTrackingNodes(node, nodeRelevantType);
        const activeNode = nodeGroup[0];
        const activeNodeTypesOrdered = this.#getOrderedChangeTrackTypes(activeNode);
        const localChangeTrackSelection = this.#createChangeTrackSelection(activeNode, nodeRelevantType, activeNodeTypesOrdered, nodeGroup);

        if (localChangeTrackSelection) {
            // setting the new change track selection. Because this process also modifies the text
            // selection, this could repaint the visible change track pop up again. To avoid this
            // the flag 'keepChangeTrackPopup' must be transported to the 'change' and 'selection'
            // events, so that the change track pop up is not
            this.#setChangeTrackSelection(localChangeTrackSelection, { keepChangeTrackPopup: true });
        }

    }

    /**
     *  Clears the current change track selection and remove all of their highlights
     */
    clearChangeTrackSelection() {
        if (!this.#changeTrackSelection) { return; }
        $(this.#changeTrackSelection.selectedNodes).removeClass(this.#highlightClassname);
        this.#changeTrackSelection = null;
    }

    /**
     * Receiving the relevant tracking information of a specified node.
     * Relevant means, that 'inserted' or 'removed' are more important
     * than 'modified'.
     *
     * @param {HTMLElement|jQuery} node
     *  The element whose tracking information is investigated.
     *
     * @returns {Object}
     *  The relevant tracking info of a given node, or null, if the
     *  selection contains no tracking node. The relevant info is an
     *  object containing the properties 'type', 'author' and 'date'.
     */
    getRelevantTrackingInfoByNode(node) {

        node = getDomNode(node);
        if (!node) { return null; }

        if (DOM.isTextComponentNode(node) && node.firstChild) { node = node.firstChild; }

        const attrSet = getExplicitAttributeSet(node);
        if (!attrSet?.changes) { return null; }

        // finding the relevant change track type
        const type = this.#getOrderedChangeTrackTypes(node)[0];
        const changes = type ? attrSet.changes[type] : null;
        if (!changes) { return null; }

        // the info object to be returned to cller
        const changeTrackInfo = { node, type, date: changes.date };

        // author settings
        if (!is.nullish(changes.authorId)) {
            const authorInfo = this.#getAuthorFromAuthorId(changes.authorId);
            changeTrackInfo.author = this.#getAuthorInfo(authorInfo, 'author');
            changeTrackInfo.userId = resolveUserId(parseInt(this.#getAuthorInfo(authorInfo, 'userId'), 10));
        } else {
            changeTrackInfo.author = changes.author;
            changeTrackInfo.userId = _.isString(changes.userId) ? resolveUserId(parseInt(changes.userId, 10)) : null;
        }

        // type of the changed content node
        if (DOM.isTextSpan(node) || DOM.isTextComponentNode(node)) {
            changeTrackInfo.kind = 'text';
        } else if (DOM.isParagraphNode(node)) {
            changeTrackInfo.kind = 'paragraph';
        } else if (DOM.isTableNode(node)) {
            changeTrackInfo.kind = 'table';
        } else if (DOM.isTableRowNode(node)) {
            changeTrackInfo.kind = 'row';
        } else if (DOM.isTableCellNode(node)) {
            changeTrackInfo.kind = 'cell';
        } else if (isTextFrameShapeDrawingFrame(node)) {
            const geomAttrs = attrSet.geometry;
            changeTrackInfo.kind = (is.empty(geomAttrs) || (geomAttrs.presetShape === 'rect')) ? 'frame' : 'shape';
        } else if (DOM.isDrawingFrame(node)) {
            changeTrackInfo.kind = 'drawing';
        } else {
            globalLogger.error(`$badge{ChangeTrack} getRelevantTrackingInfoByNode: unknown node kind for change tracking type "${changeTrackInfo.type}"`);
            changeTrackInfo.kind = 'text';
        }

        return changeTrackInfo;
    }

    /**
     * Receiving the relevant tracking information of the current selection.
     * If the selection has a range, null is returned. Relevant means,
     * that 'inserted' or 'removed' are more important than 'modified'.
     *
     * @returns {Object|null}
     *  The relevant tracking info of a given node, or null, if the
     *  selection contains no tracking node. The relevant info is an
     *  object containing the properties 'type', 'author' and 'date'.
     */
    getRelevantTrackingInfo() {

        // the selection object
        const selection = this.docModel.getSelection();
        // the change tracked node at cursor position
        let node = null;

        // no relevant type, if there is a selection range
        // but allowing selection of inline elements (tab, drawing, ...)

        // -> checking inline div elements or (floated) drawings
        if (selection.hasRange() && !selection.isInlineComponentSelection() && !selection.isDrawingFrameSelection()) { return null; }

        node = this.#findChangeTrackNodeAtCursorPosition();

        if (!node || node.length === 0) { return null; }

        // Not showing info about a drawing, if the drawing is not selected. This can be caused
        // if the selection is the last text position before the drawing (50388)
        if (DOM.isDrawingFrame(node) && !selection.isDrawingSelection() && !selection.isAdditionalTextframeSelection()) { return null; }

        return this.getRelevantTrackingInfoByNode(node);
    }

    /**
     * Receiving the relevant tracking information of a specified node.
     * Relevant tracking type means, that 'inserted' or 'removed' are
     * more important than 'modified'.
     *
     * @param {HTMLElement|jQuery} node
     *  The element whose tracking type is investigated.
     *
     * @returns {String}
     *  The relevant tracking type of a given node, or null, if the
     *  selection contains no tracking node.
     */
    getRelevantTrackingTypeByNode(node) {
        if (DOM.isDrawingPlaceHolderNode(node)) { node = DOM.getDrawingPlaceHolderNode(node); }
        const info = this.getRelevantTrackingInfoByNode(node);
        return info ? info.type : null;
    }

    /**
     * Receiving the relevant tracking type of the current selection.
     * If the selection has a range, null is returned. Relevant means,
     * that 'inserted' or 'removed' are more important than 'modified'.
     *
     * @returns {String}
     *  The relevant tracking type of a given node, or null, if the
     *  selection contains no tracking node.
     */
    getRelevantTrackingType() {
        const info = this.getRelevantTrackingInfo();
        return info ? info.type : null;
    }

    /**
     * Checking, if inside an array of operations at least one operation
     * contains change tracking information.
     *
     * @param {Array} operations
     *  The operations to be checked.
     *
     * @returns {Boolean}
     *  Whether at least one operation in the specified array of operations
     *  contains change tracking information.
     */
    hasChangeTrackOperation(operations) {
        return _.filter(operations, this.isChangeTrackOperation).length > 0;
    }

    /**
     * Checking, whether a specified operation contains change tracking information.
     *
     * @param {Object} operation
     *  The operation to be checked.
     *
     * @returns {Boolean}
     *  Whether the specified operation contains change tracking information.
     */
    isChangeTrackOperation(operation) {
        return operation && operation.attrs && operation.attrs.changes && (
            (operation.attrs.changes.inserted && (operation.attrs.changes.inserted.author || operation.attrs.changes.inserted.authorId)) ||
            (operation.attrs.changes.removed && (operation.attrs.changes.removed.author || operation.attrs.changes.removed.authorId)) ||
            (operation.attrs.changes.modified && (operation.attrs.changes.modified.author || operation.attrs.changes.modified.authorId))
        );
    }

    /**
     * Checking whether a specified node can be change tracked. This is for example used for deleting
     * with activated change tracking. In this case not all nodes of the deleted range must be marked
     * as deleted.
     *
     * @param {HTMLElement|jQuery} node
     *  The node whose trackability is investigated.
     *
     * @returns {Boolean}
     *  Whehter the specified node can be change tracked or not.
     */
    isChangeTrackableNode(node) {

        // whether a specified node can be change tracked.
        let isTrackable = true;

        // no change tracking for comments and its range marker nodes.
        if (DOM.isCommentRangeMarkerNode(node) || DOM.isCommentPlaceHolderNode(node)) { isTrackable = false; }

        return isTrackable;
    }

    /**
     * Saving the state of the change track side bar handler. This is used for
     * performance reasons. Registering handler is only necessary, if they are not
     * already registered. The same for de-registering.
     *
     * @param {Boolean} state
     *  The state of the change track side bar handler.
     */
    setSideBarHandlerActive(state) {
        this.#sideBarHandlerActive = state;
    }

    /**
     * Returning information about the state of the change track side bar handler.
     *
     * @returns {Boolean}
     *  Whether the handler for the sidebar are activated or not.
     */
    isSideBarHandlerActive() {
        return this.#sideBarHandlerActive;
    }

    /**
     * Resolving the change tracking for all text spans or only for one
     * selected text span.
     *
     * @param {HTMLElement|jQuery} [trackingNodes]
     *  Optional html element that will be evaluated for change tracking.
     *  If specified, the selection or the parameter 'all' are ignored.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the preparation of the span.
     *  The following options are supported:
     *  @param {String} [options.type='']
     *      The type of resolving ('inserted', 'removed' or 'modified').
     *  @param {Boolean} [options.accepted=true]
     *      If set to true, the change tracking will be resolved as 'accepted'.
     *      Otherwise it will be resolved as 'rejected'.
     *  @param {Boolean} [options.all=false]
     *      If set to true, all text span with change tracking attribute
     *      will be resolved. Otherwise only the currently selected text
     *      span.
     *  @param {Boolean} [options.refreshNodes=false]
     *      If set to true, the change track selection is refreshed
     *      asynchronously, after all change trackes are resolved (53777).
     *  @param {Boolean} [options.reuseSameNode=true]
     *      The parameter of 'getNextChangeTrack' that is just transferred (53777).
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved or rejected after all operations for
     *  resolving the change tracks are applied asynchronously. If no operations
     *  are generated, the promise is resolved immediately.
     */
    resolveChangeTracking(trackingNodes, options) {

        // the operations generator
        const generator = this.docModel.createOperationGenerator();
        // whether all change tracks shall be accepted or rejected
        let all = getBooleanOption(options, 'all', false);
        // saving the start position of an existing range
        let savedStartPosition = null;
        // saving the target for the saved start position
        let savedRootNode = null;
        // saving the current start position
        const currentStartPos = this.docModel.getSelection().getStartPosition();
        // whether a delete operation was used for resolving change tracking
        let contentDeleted = false;
        // whether the change shall be accepted or rejected
        const accepted = getBooleanOption(options, 'accepted', true);
        // the type of resolving ('inserted', 'removed' or 'modified')
        let resolveType = getStringOption(options, 'type', '');
        // a collector for all tracking nodes
        let trackingNodesCollector = null;
        // a deferred object for applying the actions
        let operationsDef = null;
        // error during resolving change tracks
        let resolveError = false;
        // a collector for logical positions of additionally generated delete operations
        const additionalDeleteOps = [];
        // a collector for logical positions of all deleted paragraphs inside table cells
        const allDeletedParagraphsInCells = [];
        // a collector for all header or footer nodes
        let allContainers = $();
        // container for all generated operations
        let allOperations = null;
        // whether a refresh of selected nodes is required (required in the context of task 53777)
        const refreshNodes = getBooleanOption(options, 'refreshNodes', false);
        // whether a refresh of selected nodes is required (required in the context of task 53777)
        const reuseSameNode = getBooleanOption(options, 'reuseSameNode', true);

        // restoring the old character attribute values
        const receiveOldAttributes = (currentAttrs, oldAttrs) => {

            // using old attributes, if available
            const attrs =  _.copy(oldAttrs, true);  // using all old attributes
            // the attributes, that are currently defined
            let key = null;

            currentAttrs = currentAttrs || {};

            // and setting all new attributes, that are not defined in the old attributes, to null.
            for (key in currentAttrs) {
                if (!(key in oldAttrs)) { attrs[key] = null; }
            }

            return attrs;
        };

        // checking the validity of an existing changeTrack selection
        if (this.#changeTrackSelection && this.#changeTrackSelection.selectedNodes) { this.#checkSelectionValidity(this.#changeTrackSelection); }

        // setting tracking, that will be resolved
        if (this.#changeTrackSelection && this.#changeTrackSelection.selectedNodes && !all) {
            // using selected nodes and resolve type from current change track selection
            trackingNodes = this.#changeTrackSelection.selectedNodes;
            resolveType = this.#changeTrackSelection.activeNodeType;
        } else if (trackingNodes && !all) {
            trackingNodes = $(trackingNodes);
        } else {
            if (all) {
                // collecting all change track nodes in the document (using from change tracking selection, if available)
                trackingNodes = (this.#changeTrackSelection && this.#changeTrackSelection.allChangeTrackNodes) ? this.#changeTrackSelection.allChangeTrackNodes : this.#getAllChangeTrackNodes();
            } else {
                // using the current selection, to resolve the change tracking
                // -> this can be a selection with or without range.
                // -> splitting nodes, if they are not completely inside the selection and the selection has a range
                trackingNodesCollector = this.#getTrackingNodesInSelection({ split: this.docModel.getSelection().hasRange(), markLastParagraph: true });

                // no change tracking nodes were found in the selection
                if (trackingNodesCollector.length === 0) {
                    return $.when();
                }

                // change tracking nodes were found in the selection
                if (this.docModel.getSelection().hasRange()) {

                    // simply using the collected (and already split) nodes
                    // from the selection with range.
                    trackingNodes = trackingNodesCollector;

                    if (this.docModel.getSelection().isDrawingFrameSelection()) {
                        // special handling for drawing selections, where the popup box is displayed
                        // and not all change tracks shall be resolved but only the specified type
                        if (!resolveType) { resolveType = this.getRelevantTrackingTypeByNode(trackingNodesCollector); }

                    } else {

                        // after determining the affected nodes, the process of 'all' can be used
                        // to resolve the change tracks. This is not limited to the relevant
                        // resolve type, but includes all resolve types. So inside a given arbitrary
                        // selection, all affected nodes (after grouping maybe even more) need to
                        // be resolved with all resolve types.
                        all = true;
                    }

                } else {
                    // if resolve type is not specified, the relevant type need to be defined
                    if (!resolveType) {
                        resolveType = this.getRelevantTrackingTypeByNode(trackingNodesCollector);
                    }
                    // -> finding all relevant tracking nodes (keeping each node unique)
                    if (resolveType) {
                        trackingNodes = this.#groupTrackingNodes(trackingNodesCollector, resolveType);
                    }
                }
            }
        }

        if (trackingNodes && trackingNodes.length > 0) {

            // checking complex fields behind range markers in the tracking node collection
            this.#checkTrackingNodeValidity(trackingNodes); // required for task 54373

            //  check if special complex fields are changetracked, and restore them to original state
            this.docModel.trigger('specialFieldsCT:before', trackingNodes);

            // iterating over all tracking nodes
            _.each(trackingNodes, trackingNode => {

                // whether the node is located inside header or footer (checking parent because of 58739)
                const isMarginalNode = DOM.isMarginalNode(trackingNode) || DOM.isMarginalNode(trackingNode.parentNode);
                // the local root node for the tracking node
                const localRootNode = isMarginalNode ? DOM.getMarginalTargetNode(this.#rootNode, trackingNode) : this.#rootNode;
                // the logical start and end position of the currently evaluated node (must always be inside one paragraph)
                const changeTrackingRange = getNodeStartAndEndPosition(trackingNode, localRootNode, { fullLogicalPosition: true });
                // a neighbor node of the tracking node
                let neighborNode = null;
                // the following node of the tracking node
                let followingNode = null;
                // the explicit node attributes
                let explicitAttributes = null;
                // the original attributes
                let oldCharAttrs = null, oldParaAttrs = null, oldCellAttrs = null, oldRowAttrs = null, oldTableAttrs = null, oldStyleId = null, oldDrawingAttrs = null;
                // the old attributes to be restored
                let restoredAttrs = null;
                // whether the node is a text span, a text component (tab, hard break, ...) or a drawing
                const isTextSpan = DOM.isTextSpan(trackingNode) || DOM.isTextComponentNode(trackingNode) || isDrawingFrame(trackingNode);
                // wheter operation creation would lead to an error (empty text spans cannot be addressed, but should not occur here). But allowing empty spans inside tabs.
                const skipNode = isTextSpan && DOM.isEmptySpan(trackingNode) && !DOM.isTabSpan(trackingNode);
                // whether the node is a paragraph
                const isParagraph = DOM.isParagraphNode(trackingNode);
                // whether the node is a table
                const isTable = DOM.isTableNode(trackingNode);
                // whether the node is a table row
                const isTableRow = DOM.isTableRowNode(trackingNode);
                // whether the node is a table cell
                const isTableCell = DOM.isTableCellNode(trackingNode);
                // helper variables for column handling
                let tableNode = null, tableRowNode = null, tableGrid = null, tablePos = null, tableRowNodePos = null, allTableCellNodes = null, allRemovedCellNodes = null, tableRowDeleted = false;
                // whether a page break needs to be removed from table
                let removePageBreak = false;
                // the operations generator for one node (increasing resiliance)
                const localGenerator = this.docModel.createOperationGenerator();
                // the header or footer container node
                const containerNode = isMarginalNode ? localRootNode : null;
                // the header or footer target id (if not set, must be 'undefined' for later comparisons)
                const targetId = isMarginalNode ? DOM.getTargetContainerId(getDomNode(containerNode)) : undefined;

                // checking for an error in determining the logical positions
                if (!_.isArray(changeTrackingRange.start) || changeTrackingRange.start.length === 0) { resolveError = true; }

                // increasing stability, avoiding invalid operations (going to next node)
                if (resolveError) { return; }

                // saving start position and the corresponding root node of the first node
                savedStartPosition = savedStartPosition || _.clone(changeTrackingRange.start);
                savedRootNode = savedRootNode || localRootNode;

                // increasing stability, avoiding invalid operations (going to next node)
                if (skipNode) { return; }

                // creating operation(s) required to accept or reject the change track
                if ((DOM.isChangeTrackInsertNode(trackingNode)) && ((resolveType === 'inserted') || all)) {
                    if (accepted) {
                        localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: { changes: { inserted: null } } });
                    } else {
                        if (isTextSpan) {
                            localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end) });
                            // the 'inserted' is no longer valid at a completely removed node
                            // Info: The order is reversed -> 1. step: Setting attributes, 2. step: Removing all content
                            // Setting attributes, before deleting node. This is (only) necessary if an empty text node remains (for example last node in paragraph)
                            localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: { changes: { inserted: null } } });
                            contentDeleted = true;
                        } else if (isParagraph) {
                            neighborNode = DOM.getAllowedNeighboringNode(trackingNode, { next: false });
                            followingNode = DOM.getAllowedNeighboringNode(trackingNode, { next: true });
                            // checking, if a following table contains page break attribute
                            if (DOM.isTableNode(followingNode) && DOM.isTableWithManualPageBreak(followingNode) && !this.#isRemovalNode(followingNode, trackingNodes, accepted)) { removePageBreak = true; }

                            // merging the paragraph with its previous sibling, if available
                            if (neighborNode && DOM.isParagraphNode(neighborNode)) {
                                localGenerator.generateOperation(Op.PARA_MERGE, { start: increaseLastIndex(changeTrackingRange.start, -1), paralength: getParagraphNodeLength(neighborNode) });
                                contentDeleted = true;
                                // saving the delete operation to check later, if cell will be empty
                                if (DOM.isParagraphInTableCell(trackingNode)) { allDeletedParagraphsInCells.push({ name: Op.PARA_MERGE, pos: _.clone(changeTrackingRange.start), targetId }); }
                            } else if (!neighborNode || DOM.isTableNode(neighborNode)) {
                                // the paragraph is marked with 'inserted' (maybe even between two tables)
                                // -> NOT moving text into previous or next table (like MS Office)
                                // -> delete paragraph, if it is empty or contains only content marked as inserted
                                // -> merge tables, if they are directly following
                                if ((getParagraphNodeLength(trackingNode) === 0) || (this.#paragraphEmptyAfterResolving(trackingNode, trackingNodes, accepted))) {
                                    // taking care of operation order: Later reverting of operations -> first delete paragraph,
                                    // then merge tables (if possible). Additionally both tables must still exist, when
                                    // the mergeTable operation is executed (43803).
                                    if (DOM.isTableNode(followingNode) && mergeableTables(neighborNode, followingNode) && !this.#nodeRemovedDuringResolving(neighborNode, trackingNodes, accepted) && !this.#nodeRemovedDuringResolving(followingNode, trackingNodes, accepted)) {
                                        localGenerator.generateOperation(Op.TABLE_MERGE, { start: _.copy(getOxoPosition(localRootNode, neighborNode)), rowcount: DOM.getTableRows(neighborNode).length });
                                    }
                                    // deleting an empty paragraph (or a paragraph containing content completely marked as 'inserted'
                                    localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start) });
                                    contentDeleted = true;

                                    // saving the delete operation to check later, if cell will be empty
                                    if (DOM.isParagraphInTableCell(trackingNode)) { allDeletedParagraphsInCells.push({ name: Op.DELETE, pos: _.clone(changeTrackingRange.start), targetId }); }
                                }
                            }
                            // removing page break attribute from following table, if required (calculating position before deleting table)
                            if (removePageBreak) {
                                localGenerator.generateOperation(Op.SET_ATTRIBUTES, {
                                    attrs: { paragraph: { pageBreakBefore: false } },
                                    start: getFirstPositionInParagraph(localRootNode, getOxoPosition(localRootNode, followingNode)).slice(0, -1)
                                });
                            }
                            // removing change track attribute from paragraph
                            localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: { changes: { inserted: null } } });
                        } else if (isTableCell) {
                            // rejecting for all cells in the column
                            localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start) });

                            // -> Setting new table grid attribute to table
                            tableGrid = this.#recalculateTableGrid(trackingNode, trackingNodes, DOM.isChangeTrackInsertNode);
                            if (tableGrid && tableGrid.length > 0) {
                                tablePos = getOxoPosition(localRootNode, $(trackingNode).closest(DOM.TABLE_NODE_SELECTOR), 0);
                                localGenerator.generateOperation(Op.SET_ATTRIBUTES, { attrs: { table: { tableGrid } }, start: _.clone(tablePos) });
                            }

                            contentDeleted = true;

                        } else if (isTable || isTableRow) {

                            // deleting of table needs to be checked and generated before deleting of rows
                            // -> handling the problem, that after reload, table no longer is marked as 'inserted'
                            if (isTableRow && ($(trackingNode).prev().length === 0)) {
                                tableNode = $(trackingNode).closest(DOM.TABLE_NODE_SELECTOR);
                                if (tableNode && !DOM.isChangeTrackInsertNode(tableNode)) {
                                    // is there a next sibling, that is not marked as inserted?
                                    // -> comparing the number of next inserted siblings + 1 with the number of rows in the table
                                    if (((($(trackingNode).nextAll(DOM.CHANGETRACK_INSERTED_ROW_SELECTOR)).length) + 1) === DOM.getTableRows(tableNode).length) {
                                        tablePos = getOxoPosition(localRootNode, tableNode, 0);
                                        additionalDeleteOps.push({ pos: tablePos, targetId }); // Checking later, if the table already got a setAttributes operation (36652)
                                        localGenerator.generateOperation(Op.DELETE, { start: _.copy(tablePos) });
                                    }
                                }
                            }

                            localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start) });
                            contentDeleted = true;
                        }
                    }
                }

                if ((DOM.isChangeTrackRemoveNode(trackingNode)) && ((resolveType === 'removed') || all)) {
                    if (accepted) {
                        if (isTextSpan) {
                            localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end) });
                            contentDeleted = true;
                        } else if (isParagraph) {
                            neighborNode = DOM.getAllowedNeighboringNode(trackingNode, { next: false });
                            followingNode = DOM.getAllowedNeighboringNode(trackingNode, { next: true });
                            // checking, if the following table contains page break attribute
                            if (DOM.isTableNode(followingNode) && DOM.isTableWithManualPageBreak(followingNode) && !this.#isRemovalNode(followingNode, trackingNodes, accepted)) { removePageBreak = true; }

                            // merging the paragraph with its next sibling, if available
                            if (neighborNode && DOM.isParagraphNode(neighborNode)) {
                                // merging previous paragraph with marked paragraph
                                localGenerator.generateOperation(Op.PARA_MERGE, { start: increaseLastIndex(changeTrackingRange.start, -1), paralength: getParagraphNodeLength(neighborNode) });
                                contentDeleted = true;
                                // saving the delete operation to check later, if cell will be empty
                                if (DOM.isParagraphInTableCell(trackingNode)) { allDeletedParagraphsInCells.push({ name: Op.PARA_MERGE, pos: _.clone(changeTrackingRange.start), targetId }); }
                            } else if (!neighborNode || DOM.isTableNode(neighborNode)) {
                                // the paragraph is marked with 'deleted' (maybe even between two tables)
                                // -> delete paragraph, if it is empty or contains only content marked as inserted
                                // -> merge tables, if they are directly following
                                if ((getParagraphNodeLength(trackingNode) === 0) || (this.#paragraphEmptyAfterResolving(trackingNode, trackingNodes, accepted))) {
                                    // taking care of operation order: Later reverting of operations -> first delete paragraph,
                                    // then merge tables (if possible). Additionally both tables must still exist, when
                                    // the mergeTable operation is executed (43803).
                                    if (DOM.isTableNode(followingNode) && mergeableTables(neighborNode, followingNode) && !this.#nodeRemovedDuringResolving(neighborNode, trackingNodes, accepted) && !this.#nodeRemovedDuringResolving(followingNode, trackingNodes, accepted)) {
                                        // Checking, if the following table is also change tracked(?)
                                        localGenerator.generateOperation(Op.TABLE_MERGE, { start: getOxoPosition(localRootNode, neighborNode), rowcount: DOM.getTableRows(neighborNode).length });
                                    }
                                    // deleting an empty paragraph (or a paragraph containing content completely marked as 'inserted'
                                    localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start) });
                                    contentDeleted = true;
                                    // saving the delete operation to check later, if cell will be empty
                                    if (DOM.isParagraphInTableCell(trackingNode)) { allDeletedParagraphsInCells.push({ name: Op.PARA_MERGE, pos: _.clone(changeTrackingRange.start), targetId }); }
                                }
                            }
                            // removing page break attribute from second table, if required (calculating position before deleting table)
                            if (removePageBreak) {
                                localGenerator.generateOperation(Op.SET_ATTRIBUTES, {
                                    attrs: { paragraph: { pageBreakBefore: false } },
                                    start: getFirstPositionInParagraph(localRootNode, getOxoPosition(localRootNode, followingNode)).slice(0, -1)
                                });
                            }
                            // removing change track attribute from paragraph
                            localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: { changes: { removed: null } } });
                        } else if (isTableCell) {
                            // -> checking if the complete row or table needs to be deleted (maybe all columns were marked for removal)
                            if ($(trackingNode).prev().length === 0) {

                                tableRowNode = $(trackingNode).closest(DOM.TABLE_ROWNODE_SELECTOR);

                                if (tableRowNode.prev().length === 0) { // deleting the table?
                                    tableNode = $(trackingNode).closest(DOM.TABLE_NODE_SELECTOR);
                                    if (tableNode && !DOM.isChangeTrackRemoveNode(tableNode)) {
                                        allTableCellNodes = DOM.getTableCells(tableNode);
                                        allRemovedCellNodes = _.filter(allTableCellNodes, cellNode => { return $(cellNode).is(DOM.CHANGETRACK_REMOVED_CELL_SELECTOR); });
                                        if (allTableCellNodes.length === allRemovedCellNodes.length) { // the complete table needs to be deleted
                                            tablePos = getOxoPosition(localRootNode, tableNode, 0);
                                            additionalDeleteOps.push({ pos: _.clone(tablePos), targetId }); // Checking later, if the table already got a setAttributes operation
                                            localGenerator.generateOperation(Op.DELETE, { start: _.clone(tablePos) });
                                        }
                                    }
                                }

                                if (tableRowNode && !DOM.isChangeTrackRemoveNode(tableRowNode)) { // deleting the row?
                                    // -> comparing the number of next removed siblings + 1 with the number of cells in the row
                                    if (((($(trackingNode).nextAll(DOM.CHANGETRACK_REMOVED_CELL_SELECTOR)).length) + 1) === (tableRowNode.children('td')).length) {
                                        tableRowNodePos = getOxoPosition(localRootNode, tableRowNode, 0);
                                        additionalDeleteOps.push({ pos: _.clone(tableRowNodePos), targetId }); // Checking later, if the table row already got a setAttributes operation
                                        localGenerator.generateOperation(Op.DELETE, { start: _.clone(tableRowNodePos) });
                                        tableRowDeleted = true;
                                    }
                                }
                            }

                            // accepting for all cells in the column (only one cell determines complete column)
                            localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start) });

                            // -> additionally the table grid needs to be set, if table row is not removed
                            if (!tableRowDeleted) {
                                tableGrid = this.#recalculateTableGrid(trackingNode, trackingNodes, DOM.isChangeTrackRemoveNode);
                                if (tableGrid && tableGrid.length > 0) {
                                    tablePos = getOxoPosition(localRootNode, $(trackingNode).closest(DOM.TABLE_NODE_SELECTOR), 0);
                                    localGenerator.generateOperation(Op.SET_ATTRIBUTES, { attrs: { table: { tableGrid } }, start: _.clone(tablePos) });
                                }
                            }

                            contentDeleted = true;

                        } else if (isTable || isTableRow) {

                            // deleting of table needs to be checked and generated before deleting of rows
                            // -> handling the problem, that after reload, table no longer is marked as 'removed'
                            if (isTableRow && ($(trackingNode).prev().length === 0)) {
                                tableNode = $(trackingNode).closest(DOM.TABLE_NODE_SELECTOR);
                                if (tableNode && !DOM.isChangeTrackRemoveNode(tableNode)) {
                                    // is there a next sibling, that is not marked as removed?
                                    // -> comparing the number of next removed siblings + 1 with the number of rows in the table
                                    if (((($(trackingNode).nextAll(DOM.CHANGETRACK_REMOVED_ROW_SELECTOR)).length) + 1) === DOM.getTableRows(tableNode).length) {
                                        tablePos = getOxoPosition(localRootNode, tableNode, 0);
                                        additionalDeleteOps.push({ pos: tablePos, targetId }); // Checking later, if the table already got a setAttributes operation
                                        localGenerator.generateOperation(Op.DELETE, { start: _.copy(tablePos) });
                                    }
                                }
                            }

                            localGenerator.generateOperation(Op.DELETE, { start: _.copy(changeTrackingRange.start) });
                            contentDeleted = true;
                        }
                    } else {
                        localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: { changes: { removed: null } } });
                    }
                }

                if ((DOM.isChangeTrackModifyNode(trackingNode)) && ((resolveType === 'modified') || (all && !$(trackingNode).data('skipModify')))) {

                    if (accepted) {
                        localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: { changes: { modified: null } } });
                    } else {
                        // using drawing node instead of its place holder node (57619)
                        if (DOM.isDrawingPlaceHolderNode(trackingNode)) { trackingNode = DOM.getDrawingPlaceHolderNode(trackingNode); }
                        // Restoring the old attribute state
                        explicitAttributes = getExplicitAttributeSet(trackingNode);

                        // comparing 'character' with 'changes.modified.attrs.character' and 'paragraph' with 'changes.modified.attrs.paragraph'
                        oldCharAttrs = receiveOldAttributes(explicitAttributes.character, (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.character) || {});
                        oldParaAttrs = receiveOldAttributes(explicitAttributes.paragraph, (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.paragraph) || {});
                        oldCellAttrs = receiveOldAttributes(explicitAttributes.cell, (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.cell) || {});
                        oldRowAttrs = receiveOldAttributes(explicitAttributes.row, (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.row) || {});
                        oldTableAttrs = receiveOldAttributes(explicitAttributes.table, (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.table) || {});
                        oldDrawingAttrs = receiveOldAttributes(explicitAttributes.drawing, (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.drawing) || {});
                        oldStyleId = (explicitAttributes.changes.modified.attrs && explicitAttributes.changes.modified.attrs.styleId) || null;
                        restoredAttrs = { character: oldCharAttrs, paragraph: oldParaAttrs, cell: oldCellAttrs, row: oldRowAttrs, table: oldTableAttrs, drawing: oldDrawingAttrs, changes: { modified: null } };
                        if (oldStyleId) {
                            restoredAttrs.styleId = oldStyleId;
                        } else if (explicitAttributes.styleId) {
                            restoredAttrs.styleId = null;
                        }
                        localGenerator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(changeTrackingRange.start), end: _.copy(changeTrackingRange.end), attrs: restoredAttrs });
                    }
                }

                // receiving the new operations for this one node (increasing resiliance)
                const oneNodeOperations = localGenerator.getOperations();

                if (oneNodeOperations && oneNodeOperations.length > 0) {

                    // adding target id to operations, if necessary
                    if (isMarginalNode) {
                        _.each(oneNodeOperations, operation => {
                            operation.target = targetId;
                        });
                        allContainers = allContainers.add(getDomNode(containerNode)); // jQuery keeps the collection unique
                    }

                    // checking the new operations for this one node (increasing resiliance)
                    // (alternative solution: special operations generator for delete operations)
                    if (all) { this.#checkOperationsIntegrity(oneNodeOperations); }

                    // adding operations to the global generator
                    generator.appendOperations(oneNodeOperations);
                }

                // removing data attribute 'skipModify'
                if ($(trackingNode).data('skipModify')) { $(trackingNode).removeData('skipModify'); }

            });

            // stopping iteration, if error occured -> no operation must be sent
            if (resolveError) {
                this.clearChangeTrackSelection();
                return $.when();
            }

            // getting all created operations from the generator
            allOperations = generator.getOperations();

            // check for paragraphs removed in table cells: cell must not be empty and there must be a paragraph behind a table in the cell
            if (allDeletedParagraphsInCells) { this.#checkOperationsIntegrity(allOperations, allDeletedParagraphsInCells, { cellParagraphs: true }); }

            // checking if additional delete operations were generated (for foreign elements (36652))
            if (additionalDeleteOps) { this.#checkOperationsIntegrity(allOperations, additionalDeleteOps); }

            // optimizing the operations to send less operations to the backend
            this.#combineOperations(allOperations);

            // operations MUST be executed in reverse order to preserve the positions
            // -> from end of document to beginning of document
            allOperations.reverse();

            // invalidate an existing change track selection
            // -> clearing selection before (!) applying operations, so that
            // the highlighting attributes are not handled inside the operations
            if (this.#changeTrackSelection) { this.clearChangeTrackSelection(); }

            // applying the operations
            this.docModel.setGUITriggeredOperation(true);

            // blocking keyboard input during applying of operations
            this.docModel.setBlockKeyboardEvent(true);

            // invalidate the selection (so that it is not used for example in operation success handlers)
            this.docModel.getSelection().setTemporaryInvalidSelection(true);

            // fire apply operations asynchronously
            operationsDef = this.docModel.applyOperationsAsync(allOperations);

            // Wait for a second before showing the progress bar (entering busy mode).
            // Users applying minimal or 'normal' amount of change tracks will not see this progress bar at all.
            this.docApp.getView().enterBusy({
                cancelHandler: () => {
                    if (operationsDef) { operationsDef.abort(); }
                },
                delay: 1000,
                warningLabel: /*#. shown while all recorded changes will be applied to the document */ gt('Applying changes, please wait...')
            });

            // handle the result of change track operations
            operationsDef
                .progress(progress => {
                    // update the progress bar according to progress of the operations promise
                    this.docApp.getView().updateBusyProgress(progress);
                })
                .done(() => {

                    // OT: The none-external client updates headers and footers in the document, not in the template
                    // -> an immediate update of template placeholders is required.
                    if (all && this.docApp.isOTEnabled() && allContainers.length > 0) {
                        _.each(allContainers, oneContainer => {
                            this.docModel.getPageLayout().updateEditingHeaderFooter({ givenNode: oneContainer });
                        });
                    }

                    // updating all headers or footers synchronously (required for 'next' or 'prev')
                    if (allContainers.length > 0) {
                        this.docModel.trigger('paragraphUpdate:after', allContainers);
                        if (this.docModel.isHeaderFooterEditState()) {
                            this.docModel.getPageLayout().updateEditingHeaderFooter(); // #62362 - debounced call of editing header/footer is too late, must be direct update
                        }
                        // updating the already updated next nodes. This happens in the context of task 53777.
                        // It is possible, that 'getNextChangeTrack' was already called synchronously in the
                        // controller and therefore the change track selection contains header/footer nodes that
                        // are no longer in the DOM.
                        if (refreshNodes) {
                            this.clearChangeTrackSelection();
                            this.getNextChangeTrack({ reuseSameNode });
                        }
                    }
                })
                .always(() => {

                    // the logical position that can be used after change tracks are resolved.
                    let newPosition = null;

                    if (this.docApp.docView?.leaveBusy) { this.docApp.docView.leaveBusy(); }
                    this.docModel.setGUITriggeredOperation(false);
                    // allowing keyboard events again
                    this.docModel.setBlockKeyboardEvent(false);
                    // after change track is finnished, convert page fields in header to special ones, if they were among change track nodes
                    this.docModel.trigger('specialFieldsCT:after');
                    // reactivate the selection
                    this.docModel.getSelection().setTemporaryInvalidSelection(true);

                    // if content was deleted and there was a range, the new selection must be a text cursor at start position
                    if (savedStartPosition && contentDeleted) {
                        newPosition = this.#getValidPosition(savedRootNode, savedStartPosition, currentStartPos);
                        if (newPosition) {
                            this.executeDelayed(() => {
                                const sel = this.docModel.getSelection();
                                sel.setTemporaryInvalidSelection(false); // reactivate the selection
                                if (!isValidElementRange(sel.getRootNode(), sel.getStartPosition(), sel.getEndPosition())) {
                                    sel.setTextSelection(newPosition);
                                } else if (sel.isDrawingSelection()) {
                                    const drawingNode = getDOMPosition(sel.getRootNode(), sel.getStartPosition(), true);
                                    if (!drawingNode || !DOM.isDrawingFrame(drawingNode)) { sel.setTextSelection(newPosition); }
                                }
                            });
                        } else {
                            this.docModel.getSelection().setTemporaryInvalidSelection(false); // reactivate the selection
                        }
                    } else {
                        this.docModel.getSelection().setTemporaryInvalidSelection(false); // reactivate the selection
                    }

                    this.docModel.trigger('changetrack:resolve:after');
                });
        }

        return operationsDef ? operationsDef : $.when();
    }

    /**
     * Updating the change track attributes for a given node.
     *
     * @param {jQuery} node
     *  The node whose character attributes shall be updated, as
     *  jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    updateChangeTrackAttributes(node, mergedAttributes) {

        if (this.docModel.isCTFreeDoc()) { return; } // fast exit for performance reasons

        // the explicit attributes of the specified node
        let nodeAttrs = null;
        // the place holder node from an absolute positioned drawing
        let placeHolderNode = null;
        // the optionally available author ID from the global author's list
        let authorId = null;
        // an optionally available author info object from the globals author list
        let authorInfo = null;

        // helper function to remove all change track attributes
        // from a specified node
        const removeAllChangeTrackAttributesFromNode = oneNode => {
            oneNode.removeAttr('data-change-track-modified');
            oneNode.removeAttr('data-change-track-inserted');
            oneNode.removeAttr('data-change-track-removed');
            oneNode.removeAttr('data-change-track-modified-author');
            oneNode.removeAttr('data-change-track-inserted-author');
            oneNode.removeAttr('data-change-track-removed-author');
        };

        // helper function to copy all change track attributes from
        // one node to another node
        const copyChangeTrackAttributes = (from, to) => {
            if (from.attr('data-change-track-modified')) { to.attr('data-change-track-modified', from.attr('data-change-track-modified')); }
            if (from.attr('data-change-track-inserted')) { to.attr('data-change-track-inserted', from.attr('data-change-track-inserted')); }
            if (from.attr('data-change-track-removed')) { to.attr('data-change-track-removed', from.attr('data-change-track-removed')); }
            if (from.attr('data-change-track-modified-author')) { to.attr('data-change-track-modified-author', from.attr('data-change-track-modified-author')); }
            if (from.attr('data-change-track-inserted-author')) { to.attr('data-change-track-inserted-author', from.attr('data-change-track-inserted-author')); }
            if (from.attr('data-change-track-removed-author')) { to.attr('data-change-track-removed-author', from.attr('data-change-track-removed-author')); }
        };

        // Setting and deleting changes attributes, but without using attribute from parent!
        if (_.isObject(mergedAttributes.changes)) {

            nodeAttrs = getExplicitAttributeSet(node); // -> no inheritance

            if (_.isObject(nodeAttrs.changes) && (!DOM.isEmptySpan(node) || DOM.isTabSpan(node))) {  // not for empty text spans

                if (_.isObject(nodeAttrs.changes.modified)) {
                    node.attr('data-change-track-modified', true);  // necessary for visualization of modifications
                    if (nodeAttrs.changes.modified.author) {
                        node.attr('data-change-track-modified-author', this.docApp.getAuthorColorIndex(nodeAttrs.changes.modified.author));
                    } else if (nodeAttrs.changes.modified.authorId) {
                        authorId = nodeAttrs.changes.modified.authorId; // support of property 'authorId'
                        authorInfo = this.#getAuthorFromAuthorId(authorId);
                        node.attr('data-change-track-modified-author', this.docApp.getAuthorColorIndex(this.#getAuthorInfo(authorInfo, 'author')));
                    }
                } else {
                    node.removeAttr('data-change-track-modified');
                    node.removeAttr('data-change-track-modified-author');
                }

                if (_.isObject(nodeAttrs.changes.inserted)) {
                    node.attr('data-change-track-inserted', true);  // necessary for visualization of modifications
                    if (nodeAttrs.changes.inserted.author) {
                        node.attr('data-change-track-inserted-author', this.docApp.getAuthorColorIndex(nodeAttrs.changes.inserted.author));
                    } else if (nodeAttrs.changes.inserted.authorId) {
                        authorId = nodeAttrs.changes.inserted.authorId; // support of property 'authorId'
                        authorInfo = this.#getAuthorFromAuthorId(authorId);
                        node.attr('data-change-track-inserted-author', this.docApp.getAuthorColorIndex(this.#getAuthorInfo(authorInfo, 'author')));
                    }
                } else {
                    node.removeAttr('data-change-track-inserted');
                    node.removeAttr('data-change-track-inserted-author');
                }

                if (_.isObject(nodeAttrs.changes.removed)) {
                    node.attr('data-change-track-removed', true);  // necessary for visualization of modifications
                    if (nodeAttrs.changes.removed.author) {
                        node.attr('data-change-track-removed-author', this.docApp.getAuthorColorIndex(nodeAttrs.changes.removed.author));
                    } else if (nodeAttrs.changes.removed.authorId) {
                        authorId = nodeAttrs.changes.removed.authorId; // support of property 'authorId'
                        authorInfo = this.#getAuthorFromAuthorId(authorId);
                        node.attr('data-change-track-removed-author', this.docApp.getAuthorColorIndex(this.#getAuthorInfo(authorInfo, 'author')));
                    }
                } else {
                    node.removeAttr('data-change-track-removed');
                    node.removeAttr('data-change-track-removed-author');
                }
            } else {
                removeAllChangeTrackAttributesFromNode(node);
            }

        } else {
            removeAllChangeTrackAttributesFromNode(node);
        }

        // setting change track attributes at absolute positioned drawings AND
        // at their place holder nodes. This is important, to keep the correct
        // order of change tracked attributes.
        if (DOM.isDrawingFrame(node) && DOM.isInsideDrawingLayerNode(node)) {
            placeHolderNode = $(DOM.getDrawingPlaceHolderNode(node));
            if (placeHolderNode) {
                removeAllChangeTrackAttributesFromNode(placeHolderNode);
                copyChangeTrackAttributes(node, placeHolderNode);
            }
        }
    }

    /**
     * Receiving the existing explicit attributes for a given node.
     * They are returned for a change track modification operation.
     *
     * @param {jQuery} node
     *  The node whose character attributes have been changed, as
     *  jQuery object.
     *
     * @returns {Object}
     *  An object with all existing explicit node attributes, taking
     *  special care of the change tracking attributes.
     */
    getOldNodeAttributes(node) {

        // an object with the old node attributes
        let oldAttrs = {};

        // Expanding operation for change tracking with old explicit attributes
        oldAttrs.attrs = getExplicitAttributeSet(node);

        // special handling for multiple modifications
        if (_.isObject(oldAttrs.attrs.changes) && _.isObject(oldAttrs.attrs.changes.modified) && _.isObject(oldAttrs.attrs.changes.modified.attrs)) {
            // Never overwriting existing changes.modified.attrs, because there might be multiple attribute modifications
            oldAttrs = oldAttrs.attrs.changes.modified;  // -> reusing the already registered old attributes
        }

        // old attributes must not contain the information about the changes
        if (oldAttrs && _.isObject(oldAttrs.attrs.changes)) {
            delete oldAttrs.attrs.changes;
        }

        return oldAttrs;
    }

    /**
     * Setting the change track selection, for example after pressing
     * the 'Next' or 'Previous' button in the change track GUI.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the change track selection.
     *  The following options are supported:
     *  @param {Boolean} [options.next=true]
     *      Whether the following (next is true) or the previous change track
     *      node shall be selected.
     *  @param {Boolean} [options.reuseSameNode=false]
     *      Whether the same selected node can be reused. This is useful, if
     *      the same node contains a further change tracked modification.
     *
     * @returns {Boolean}
     *  Whether the next change track could be selected. If there are no change
     *  track nodes in the document, false is returned.
     */
    getNextChangeTrack(options) {

        // the next valid selection
        const nextSelection = this.#iterateNextValidChangeTrackSelection(options);
        // whether the next change track could be found
        const nextChangeTrackSelected = !!nextSelection;

        if (nextSelection) {
            this.#setChangeTrackSelection(nextSelection);  // setting CT selection and making it visible
        } else {
            this.docApp.getView().yell({ type: 'info', message: gt('There are no tracked changes in your document.') });
        }

        return nextChangeTrackSelected;
    }

    /**
     * Returning the change track selection.
     *
     * @returns {Object|Null}
     *  An object describing the change track selection or Null, if no change track selection
     *  exists.
     */
    getChangeTrackSelection() {
        return this.#changeTrackSelection;
    }

    /**
     * Returns whether the current selection contains at least one change track element.
     *
     * @returns {Boolean}
     *  Whether the current selection contains at least one change track element.
     */
    isOnChangeTrackNode() {
        // if there were no changeTracked elements, get out here (for performance)
        if (this.#getAllChangeTrackNodes(this.#rootNode).length === 0) { return false; }

        // if there were changeTracked elements and the hole document is selected, get out here (for performance)
        if (this.docModel.getSelection().isAllSelected() && this.#getAllChangeTrackNodes(this.#rootNode).length > 0) { return true; }

        // receiving at least one change track node from the current selection
        const changeTrackNodes = this.#getTrackingNodesInSelection({ minimalSelection: true });

        return (changeTrackNodes && changeTrackNodes.length > 0);
    }

    /**
     * Setting, removing or updating the side bar and the markers for the change tracking.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.invalidate=true]
     *      Whether an existing side bar needs to be invalidated.
     *
     * @returns {Boolean}
     *  Whether there is at least one active change track node in the document.
     */
    updateSideBar(options) {

        // the first child of the page content node
        let firstPageContentNodeChild = null;
        // the top offset of the page content node
        let pageContentNodeTopOffset = 0;
        // the top margin of the first child of the content node
        let firstChildTopOffset = 0;
        // the minimum and maximum offset, until which the change track divs are created
        let minOffset = 0;
        let maxOffset = 0;
        // whether the current marker in the side pane need to be invalidated
        const invalidate = getBooleanOption(options, 'invalidate', true);
        // the current zoom factor of the document
        let zoomFactor = 0;

        // creating one marker in the change track side bar
        // for a specified change track node
        const createSideBarMarker = node => {

            // the vertical offset of the specific node
            const topOffset = Math.round($(node).offset().top);
            // the height and offset of the change track marker
            let markerHeight = 0, markerOffset = 0;
            // a key for each marker
            let markerKey = null;
            // whether the node is a table cell
            const isCellNode = DOM.isTableCellNode(node);
            // the target node of a marginal node
            let targetNode = null;
            // the maximum vertical offset inside a target node
            let maxTargetOffset = 0;

            // Performance: Only insert divs, if the value for offset().top is '> minOffset' and
            // smaller than the height of the scrollNode multiplied with 1.5

            if (topOffset > maxOffset) { return BREAK; }

            if (topOffset > minOffset) {

                markerHeight = $(node).is('div.p') ? convertCssLength($(node.firstChild).css('line-height'), 'px', 1) : (isCellNode ? $(node).outerHeight() : $(node).height());
                markerOffset = Math.round((topOffset + firstChildTopOffset - pageContentNodeTopOffset) / zoomFactor);

                // handling marginal nodes (39323)
                if (DOM.isMarginalNode(node)) {

                    targetNode = DOM.getClosestMarginalTargetNode(node);

                    if (targetNode.length > 0) {
                        maxTargetOffset = Math.round((targetNode.offset().top + firstChildTopOffset - pageContentNodeTopOffset) / zoomFactor);
                        maxTargetOffset += targetNode.innerHeight();

                        if (markerOffset + markerHeight > maxTargetOffset) {
                            markerHeight -= markerOffset + markerHeight - maxTargetOffset;
                            if (markerHeight < 0) { markerHeight = 0; }
                        }
                    }
                }

                markerKey = markerOffset + '_' + markerHeight;

                // finally inserting the marker for the change tracked element, if it is not already inserted
                if (!this.#allSideBarMarker[markerKey]) {
                    this.#sideBarNode.append($('<div>').addClass('ctdiv').height(markerHeight).css('top', markerOffset));
                    this.#allSideBarMarker[markerKey] = 1;
                }
            }

        };

        // not supporting small devices
        if (SMALL_DEVICE) { return false; }

        // setting scroll node, if not already done
        if (!this.#scrollNode) { this.#scrollNode = this.docApp.getView().getContentRootNode(); }

        // setting page content node, if not already done
        if (!this.#pageContentNode) { this.#pageContentNode = this.#rootNode.children(DOM.PAGECONTENT_NODE_SELECTOR); }

        // invalidating an existing side bar
        if (this.#sideBarNode && invalidate) {
            this.#sideBarNode.remove();
            this.#sideBarNode = null;
            this.#allSideBarMarker = {};
        }

        // collecting all nodes with change track information (not necessary after scrolling)
        if (!this.#allNodesSideBarCollector || invalidate) {
            this.#allNodesSideBarCollector = this.#getAllChangeTrackNodes(this.#rootNode, { multiple: true }); // Very fast call, but avoid if possible
        }

        // drawing the side bar, if there is at least one change track
        if (this.#allNodesSideBarCollector.length > 0) {

            // setting the zoom factor
            zoomFactor = this.docApp.getView().getZoomFactor();

            // the top offset of the content node
            pageContentNodeTopOffset = Math.round(this.#pageContentNode.offset().top);

            // if the first paragraph in the document has a top margin, this also needs to be added
            // into the calculation. Otherwise all markers are shifted by this top margin.
            firstPageContentNodeChild = getDomNode(this.#pageContentNode).firstChild;
            if (firstPageContentNodeChild && $(firstPageContentNodeChild).css('margin-top')) {
                firstChildTopOffset = Math.round(parseCssLength(firstPageContentNodeChild, 'marginTop') * zoomFactor);
            }

            // calculating the minimum and maximum offset in that region, for which the
            // side bar marker will be created (for performance reasons)
            maxOffset = 1.5 * (this.#scrollNode.height() + this.#scrollNode.offset().top);  // one and a half screen downwards
            minOffset = -0.3 * maxOffset; // a half screen upwards

            // creating and appending side bar, if it does not exist,
            // and if there is at least one change track
            if (this.#sideBarNode === null) {
                this.#sideBarNode = $('<div>').addClass('ctsidebar')
                    .css('top',  parseCssLength(this.#rootNode, 'paddingTop') + this.docModel.getPageLayout().getHeightOfFirstHeaderWrapper()) // if header exists, move ctsidebar down for the value of height
                    .css('left', Math.round(parseCssLength(this.#rootNode, 'paddingLeft') / 2))
                    .height(this.#pageContentNode.height());

                this.#rootNode.append(this.#sideBarNode);
            }

            // iterating over all collected change tracked nodes
            this.#allNodesSideBarCollector.get().forEach(ctNode => {
                if (DOM.isTableWithPageBreakRowNode(ctNode)) {
                    // do not mark complete table, because of problem with row inserted for
                    // page break -> mark all rows except the page break row
                    _.each(DOM.getTableRows(ctNode), createSideBarMarker);
                } else {
                    createSideBarMarker(ctNode);
                }
            });

        }

        // if there are no change tracks, inform listeners about this
        if (this.#allNodesSideBarCollector.length === 0) {
            this.docModel.trigger('changeTrack:stateInfo', { state: false });
        }

        return this.#allNodesSideBarCollector.length > 0;
    }

    /**
     * Sets the highlight marker to the current selected change track group.
     *  But first, it removes all existing highlights.
     *  Only used for ios-devices.
     */
    setHighlightToCurrentChangeTrack() {
        const node = this.#findChangeTrackNodeAtCursorPosition();

        this.#removeHighlighting();
        if (node && node.length > 0) {
            node.addClass(this.#highlightClassname);
        }

    }

    // private methods ----------------------------------------------------

    /**
     * Returns all tracking nodes inside the selection. If the selection
     * has no range, return only the currently selected node.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.minimalSelection=false]
     *      If set to true and the selection is a range, the iteration
     *      stops after finding the first change track node. If the
     *      selection is not a range, this parameter will be ignored.
     *  @param {Boolean} [options.markLastParagraph=false]
     *      If set to true and the selection is a range, the final paragraph
     *      of the selection will be marked, if it is not completely part of
     *      the selection. In this case the 'inserted' and 'removed' change
     *      tracks must be resolved, but not the 'modified' change track.
     *  @param {Boolean} [options.split=false]
     *      If set to true the spans that are only partially covered by
     *      the selection, are split during the iteration. As a result
     *      all nodes in the collector are covered completely.
     *
     * @returns {jQuery}
     *  A list of nodes in a jQuery element. The list is sorted corresponding
     *  to the appearance of the nodes in the DOM.
     */
    #getTrackingNodesInSelection(options) {

        // the tracking nodes in the current selection
        let trackingNodes = $();
        // whether it is sufficient to find only one change tracking node
        const minimalSelection = getBooleanOption(options, 'minimalSelection', false);
        // whether the final paragraph of a selection shall be marked in that way, that
        // the 'modified' change track attribute shall not be resolved. This is the case
        // if this final paragraph is not completely included into the selection.
        const markLastParagraph = getBooleanOption(options, 'markLastParagraph', false);
        // whether spans that are only partially covered by the selection, need to be split
        const split = getBooleanOption(options, 'split', false);
        // a container with all paragraphs. This is necessary to check, if the last paragraph
        // shall be added to the selection
        const paragraphsCollector = [];

        // all tracking nodes that are parents, grandparents, ... need to be added
        // to the list of all tracking nodes. Otherwise only text nodes are collected.
        const collectParagraphs = paraNode => {
            if (_.contains(paragraphsCollector, paraNode)) { return; }  // paragraph already checked
            paragraphsCollector.push(paraNode); // collecting each paragraph only once
        };

        // checking, if the final paragraph needs to be added to the change track collector.
        // This is necessary, if it was 'inserted' or 'removed', and is not completely included
        // into the selection.
        const checkFinalParagraph = () => {

            if (paragraphsCollector.length === 0) { return; }

            // the final paragraph
            const lastParagraph = paragraphsCollector[paragraphsCollector.length - 1];
            // an array with the tracking nodes (used to find and insert the paragraph node
            const trackingNodeArray = trackingNodes.get();
            // the change tracked children of the last paragraph
            let paraChildren = null;
            // the first change tracked child of the last paragraph
            let firstChild = null;
            // the position of the first child in the list of change tracked nodes
            let firstChildPosition = null;

            // is the last paragraph a change tracked node ('inserted' or 'removed' that is not
            // already part of the collected tracking nodes?
            // -> searching from end, because it probably needs to be added to the end
            if (lastParagraph && (DOM.isChangeTrackInsertNode(lastParagraph) || DOM.isChangeTrackRemoveNode(lastParagraph)) && (trackingNodeArray.lastIndexOf(lastParagraph) === -1)) {
                // the paragraph must be inserted into the list of tracking nodes
                // -> at a position before its children or at the end, if none of its children
                // are part of the list
                paraChildren = this.#getAllChangeTrackNodes(lastParagraph);

                if (paraChildren.length > 0) {
                    firstChild = paraChildren[0];
                    firstChildPosition = trackingNodeArray.lastIndexOf(firstChild);
                    if (firstChildPosition === -1) {
                        trackingNodes = trackingNodes.add(lastParagraph);  // Simply adding to the end
                    } else {
                        trackingNodeArray.splice(firstChildPosition, 0, lastParagraph);  // inserting last paragraph before its first child
                        trackingNodes = $(trackingNodeArray); // converting back to jQuery list
                    }

                } else {
                    trackingNodes = trackingNodes.add(lastParagraph);  // Simply adding to the end
                }

                // setting the 'skipModify' data attribute, so that 'modify' will not be resolved/rejected
                if (markLastParagraph) { $(lastParagraph).data('skipModify', true); }
            }
        };

        if (this.docModel.getSelection().hasRange()) {

            // iterating over all nodes in the selection (using option split
            // to split partially covered spans). Using 'shortestPath: true' it is possible to check,
            // if complete paragraphs or tables are part of the selection. In this case the change
            // tracked children can be found with the 'find' function.
            this.docModel.getSelection().iterateNodes(node => {

                // a collector for all change tracked children of a content node
                let changeTrackChildren = null;
                // the parent node of a text span
                let parentNode = null;

                // in text component nodes the span has the change track attribute, not the
                // text component itself. But collector must contain the text component.
                const getValidChangeTrackNode = node => {
                    return DOM.isTextComponentNode(node.parentNode) ? node.parentNode : node;
                };

                // collecting all paragraphs in a collector
                if (DOM.isTextSpan(node)) {
                    parentNode = getDomNode(node).parentNode;
                    collectParagraphs(parentNode);
                }

                if (DOM.isChangeTrackNode(node)) {
                    trackingNodes = trackingNodes.add(node);
                }

                if (minimalSelection && trackingNodes.length > 0) { return BREAK; }  // exit as early as possible

                // collecting all children inside the content node
                if (DOM.isContentNode(node)) {
                    changeTrackChildren = this.#getAllChangeTrackNodes(node);

                    if (changeTrackChildren.length > 0) {
                        _.each(changeTrackChildren, node => {
                            trackingNodes = trackingNodes.add(getValidChangeTrackNode(node));
                        });
                    }
                }

                if (minimalSelection && trackingNodes.length > 0) { return BREAK; }  // exit as early as possible

            }, null, { split, shortestPath: true });

            // special handling for the final paragraph, that needs to be inserted into the tracking
            // node collection, if it was 'inserted' or 'removed' AND the previous paragraph is also
            // added to the paragraph collector.
            checkFinalParagraph();

            // special handling for absolute positioned drawings that move with paragraph
            if (trackingNodes.length === 0 && this.docModel.getSelection().isDrawingSelection() && DOM.isChangeTrackNode(this.docModel.getSelection().getSelectedDrawing())) {
                trackingNodes = this.docModel.getSelection().getSelectedDrawing();
            }

            // TODO: grouping of cells to columns? In which scenario is this necessary? Tables are always
            // completely part of the selection or not. Otherwise only paragraphs are completely evaluated.

        } else {
            trackingNodes = this.#findChangeTrackNodeAtCursorPosition();
        }

        return trackingNodes;
    }

    /**
     * Returns the currently selected tracking node at the cursor position or at the
     * start position of a selection range. If there is no change tracking element, an
     * empty jQuery list is returned.
     *
     * @returns {jQuery}
     *  A list of selected nodes in a jQuery element.
     */
    #findChangeTrackNodeAtCursorPosition() {

        // root node holder, either default application root node, or if exists, header/footer root node
        const thisRootNode = this.docModel.getCurrentRootNode();
        // the node info for the start position of the current selection
        const nodeInfo = getDOMPosition(thisRootNode, this.docModel.getSelection().getStartPosition());
        // the node at the cursor position
        const currentNode = nodeInfo ? nodeInfo.node : null;
        // the offset at the cursor position
        const offset = nodeInfo ? nodeInfo.offset : null;
        // a helper node for empty text spans
        let emptySpan = null;
        // the currently selected tracking node
        let trackingNode = null;
        // a currently selected drawing
        let selectedDrawing  = null;

        // fast exit, if the node could not be determined
        if (!currentNode) { return $(); }

        // special handling for drawings
        if (this.docModel.isDrawingSelected()) {
            selectedDrawing = this.docModel.getSelection().getSelectedDrawing();
            // -> using real drawing, not the one in the overlay selection
            return DOM.isChangeTrackNode(selectedDrawing) ? selectedDrawing : $();
        }

        // special handling for empty text nodes between inline nodes, for example div elements
        if ((currentNode.nodeType === 3) && currentNode.parentNode) {
            if (DOM.isEmptySpan(currentNode.parentNode)) {
                emptySpan = currentNode.parentNode;
                // checking for previous or following
                if (emptySpan.previousSibling && !DOM.isDrawingFrame(emptySpan.previousSibling) && DOM.isChangeTrackNode(emptySpan.previousSibling)) {
                    trackingNode = $(emptySpan.previousSibling);
                } else if (emptySpan.nextSibling && !DOM.isDrawingFrame(emptySpan.nextSibling) && DOM.isChangeTrackNode(emptySpan.nextSibling)) {
                    trackingNode = $(emptySpan.nextSibling);
                }
            } else if (DOM.isChangeTrackNode(currentNode.parentNode)) {
                trackingNode = $(currentNode.parentNode);
            }
        }

        trackingNode = trackingNode || $(currentNode).closest(DOM.CHANGETRACK_NODE_SELECTOR); // standard process to find change track node (also handling rows or shapes)

        if (trackingNode && (trackingNode.length === 0) && (offset === 0) && currentNode.parentNode.previousSibling && DOM.isChangeTrackNode(currentNode.parentNode.previousSibling)) {
            // this happens behind a div.inline component
            trackingNode = $(currentNode.parentNode.previousSibling);
        }

        if (trackingNode && (trackingNode.length === 0) && (DOM.isEmptySpan(currentNode.parentNode) || ((currentNode.nodeType === 3) && (currentNode.length === offset))) && DOM.isChangeTrackNode(currentNode.parentNode.nextSibling)) {
            // this happens before a change tracked component, div.inline or text span (special handling for empty spans between two tabs)
            trackingNode = $(currentNode.parentNode.nextSibling);
        }

        return trackingNode ? trackingNode : $();
    }

    /**
     * Returns for a given list of nodes an expanded list, that contains
     * all the given nodes and additionally all nodes affected by the
     * change track process.
     * Using a jQuery element as collection, keeps all nodes unique.
     *
     * @param {HTMLElement[]|jQuery} nodes
     *  A list of all nodes, for that the adjacent nodes shall be detected
     *
     * @param {String} [type]
     *  The type of resolving ('inserted', 'removed' or 'modified'). I not specified,
     *  all types will be evaluated.
     *
     * @returns {jQuery}
     *  A list of nodes in a jQuery element.
     */
    #groupTrackingNodes(nodes, type) {

        // the jQuery collector for all grouped nodes
        let allNodes = $();
        // whether all types shall be resolved (if 'type' is not defined)
        const allResolveTypes = !type;
        // a list with all resolve types
        const resolveTypeList = allResolveTypes ? CHANGE_TRACK_TYPES : [type];

        _.each(nodes, node => {
            _.each(resolveTypeList, oneType => {
                const combinedNodes = this.#combineAdjacentNodes(node, { type: oneType });
                allNodes = allNodes.add(combinedNodes);
            });
        });

        return allNodes;
    }

    /**
     * Starting the process for updating the change track info date
     * regularly. This is needed for performance reasons, so that
     * 'this.updateChangeTrackInfoDate()' is not called for every
     * operation.
     */
    #startDateUpdate() {
        if (!this.#dateUpdateTimer) {
            // immediately start updating
            this.#dateUpdateTimer = this.setInterval(this.updateChangeTrackInfoDate, { delay: 0, interval: UPDATE_DATE_INTERVAL });
        }
    }

    /**
     * Stopping the process for updating the change track info date
     * regularly. This is needed for performance reasons, so that
     * 'this.updateChangeTrackInfoDate()' is not called for every
     * operation.
     */
    #stopDateUpdate() {
        if (this.#dateUpdateTimer) {
            this.#dateUpdateTimer.abort();
            this.#dateUpdateTimer = null;
        }
    }

    /**
     * Collecting all change tracking nodes in the document
     *
     * @param {HTMLElement|jQuery} [node]
     *  The node, in which the change track nodes are searched. If
     *  not specified, the rootNode is used.
     *  If this object is a jQuery collection, uses the first node
     *  it contains.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.multiple=false]
     *      If set to true all change tracked nodes in all headers and
     *      footers will be collected. The default behavior is, that
     *      only the nodes from one repeated header or footer are
     *      collected.
     *      For resolving change tracks, only the first occurrence of
     *      a node is required. For visualization on left side bar, all
     *      nodes are required.
     *
     * @returns {jQuery}
     *  A list of change track nodes in the document.
     */
    #getAllChangeTrackNodes(node, options) {

        // the node, in which the change track nodes are searched
        const searchNode = node || this.#rootNode;
        // the ordered list of all change track nodes (using change tracked place holder nodes instead of
        // absolute positioned drawoings because of correct node order, 36318)
        let allChangeTrackNodes = $(searchNode).find(DOM.CHANGETRACK_NODE_SELECTOR).not('.textdrawinglayer > .drawing.absolute, .list-label > span');
        // the change track nodes inside the header or footer
        const allChangeTrackNodesMarginal = [];
        // collecting all nodes with a specific id
        const allHeaderFooterNodes = {};
        // whether all nodes in cloned targets shall be collected (used for identical headers or footers)
        const multiple = getBooleanOption(options, 'multiple', false);

        // whether there is at least one marginal node inside the tracking node collector
        const containsMarginalNode = allNodes => {
            return _.find(allNodes, DOM.isMarginalNode);
        };

        // whether a marginal node was already collected in another header/footer with the same id
        // -> every header/footer node with specific ID must be handled only once
        const alreadyCollectedInAnotherContainer = (localRootNode, node) => {

            // the container, that contains the specified node
            let containerNode = DOM.getMarginalTargetNode(localRootNode, node);
            // the container id
            const containerId  = (containerNode.length > 0) ? DOM.getTargetContainerId(getDomNode(containerNode)) : null;

            if (_.isString(containerId)) {

                containerNode = getDomNode(containerNode);

                if (allHeaderFooterNodes[containerId]) {
                    // same container id, but different container nodes -> this node must not be collected
                    if (containerNode !== allHeaderFooterNodes[containerId]) {
                        return true;
                    }

                } else {
                    allHeaderFooterNodes[containerId] = containerNode;
                }
            }

            return false;
        };

        // 1. keeping correct order for all those nodes with class 'marginal'
        // 2. filtering all those nodes, that are in more than one header or footer
        if (containsMarginalNode(allChangeTrackNodes)) {
            allChangeTrackNodes = _.reject(allChangeTrackNodes, node => {
                if (DOM.isMarginalNode(node) || DOM.isMarginalNode(node.parentNode)) { // checking parent to increase resilience (DOCS-1626)
                    // collecting all change track nodes in header or footer, but not in template and only once for each container id
                    // -> collection all nodes in all headers and footers can be enabled with the property 'multiple'
                    if (!DOM.isInsideHeaderFooterTemplateNode(this.#rootNode, node) && (multiple || !alreadyCollectedInAnotherContainer(this.#rootNode, node))) { allChangeTrackNodesMarginal.push(node); }
                    return true;
                }
                return false;
            });

            // adding the marginal change tracking nodes to the end of the collector
            if (allChangeTrackNodesMarginal.length > 0) {
                allChangeTrackNodes = allChangeTrackNodes.concat(allChangeTrackNodesMarginal);
            }

            // needs to be a jQuery collection
            allChangeTrackNodes = $(allChangeTrackNodes);
        }

        return allChangeTrackNodes;
    }

    /**
     * Checking, whether the specified change track selection is a column
     * selection. This is the case if the change track type is 'inserted'
     * and 'removed' and only table cell elements are part of the selection.
     *
     * @param {Object} selection
     *  The change track selection that will be investigated.
     *
     * @returns {Boolean}
     *  Whether the specified change track selection is a column selection.
     */
    #checkColumnSelection(selection) {

        // a helper node
        let noCellNode = null;

        // checking node type
        if (selection.activeNodeType === 'modified') { return false; }

        // checking the selected nodes: A column selection must have 2 cells at least
        if ((!selection.selectedNodes) || (selection.selectedNodes.length < 2)) { return false; }

        // checking, that all members of selected nodes are cells
        noCellNode = _.find(selection.selectedNodes, oneNode => {
            return !DOM.isTableCellNode(oneNode);
        });

        if (noCellNode) { return false; }

        return true;
    }

    /**
     * If cells (or columns) are removed during rejecting insertion of columns or accepting
     * deleting of columns, it is necessary, that a new table grid is calculated. The new
     * table grid must be sent via setAttribute operation to the filter.
     *
     * In this function a new grid is calculated, but only for the first cell of a table,
     * that will be deleted during resolving the change track (a rejected insertion or an
     * accepted deletion). The cell need to be checked, because the grid shall only be
     * calculated once and not for every cell.
     *
     * @param {Node|jQuery} cellNode
     *  The cell node to be checked.
     *
     * @param {jQuery} allTrackingNodes
     *  The collection with all tracking nodes that will be resolved in this step.
     *
     * @param {Function} changeTrackState
     *  The function, that check the change track state for the specified node. This can
     *  be DOM.isChangeTrackInsertNode for rejecting the CT or DOM.isChangeTrackRemoveNode
     *  for accepting the CT.
     *
     * @returns {Number[]|Null}
     *  The new calculated table grid. Or null, if it can or shall not be determined.
     */
    #recalculateTableGrid(cellNode, allTrackingNodes, changeTrackState) {

        // the row of the specified cell
        const tableRowNode = getDomNode($(cellNode).closest(DOM.TABLE_ROWNODE_SELECTOR));

        // is this cell node the first relevant cell inside its row?
        // -> making the check only for the first cell node that is change tracked for removal inside a row.
        const firstRemovedCellInRow = _.find(allTrackingNodes, node => {
            return DOM.isTableCellNode(node) && changeTrackState(node) && node.parentNode === tableRowNode;
        });

        if (!firstRemovedCellInRow) { return null; } // this should never happen

        if (getDomNode(cellNode) !== getDomNode(firstRemovedCellInRow)) { return null; } // nothing to do, this is not the relevant cell

        // the table node that contains the specified cell node
        const tableNode = $(cellNode).closest(DOM.TABLE_NODE_SELECTOR);

        // the table grid of the table node before the change tracks are resolved
        const tableGrid = _.clone(this.docModel.tableStyles.getElementAttributes(tableNode).table.tableGrid); // the current table grid

        // collecting all cells inside the row, that will be deleted
        let allRemovedCellsInRow = _.filter(allTrackingNodes, node => {
            return DOM.isTableCellNode(node) && changeTrackState(node) && node.parentNode === tableRowNode;
        });

        // using cells in reverse order
        allRemovedCellsInRow = allRemovedCellsInRow.reverse();

        _.each(allRemovedCellsInRow, cell => {
            const gridRange = getGridColumnRangeOfCell(cell);
            tableGrid.splice(gridRange.start, gridRange.end - gridRange.start + 1);  // removing column(s) in tableGrid (automatically updated in table node)
        });

        return (tableGrid && tableGrid.length > 0) ? _.clone(tableGrid) : null;
    }

    /**
     * Setting a change track selection. The new change track selection must be
     * given to this function. The text selection is set to the first logical
     * position of the active node of the change track selection. So it must
     * not be possible, that a text selection is visible next to a change track
     * selection.
     * Before setting the new change track selection, an old change track
     * selection is cleared.
     *
     * @param {Object} selection
     *  The change track selection that will be set.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the change track selection.
     *  The following options are supported:
     *  @param {Boolean} [options.makeVisible=true]
     *      Whether the change track selection shall be made visible immediately.
     *  @param {Boolean} [options.keepChangeTrackPopup=false]
     *      Whether this change of change track selection was triggered by the change
     *      track pop up. In this case the resulting change of text selection must
     *      not lead to an update of the change track pop up.
     */
    #setChangeTrackSelection(selection, options) {

        // whether the change track selection shall be made visible
        const makeVisible = getBooleanOption(options, 'makeVisible', true);
        // whether the change track pop up triggered this change of selection
        const keepChangeTrackPopup = getBooleanOption(options, 'keepChangeTrackPopup', false);
        // whether this change track selection is a column selection (in this case the text selection must be a cursor selection)
        const isColumnSelection = this.#checkColumnSelection(selection);
        // whether this is a selection inside header or footer
        const isMarginalSelection = DOM.isMarginalNode(selection.activeNode);
        // the root node for the logical position
        const localRootNode = isMarginalSelection ? DOM.getMarginalTargetNode(this.#rootNode, selection.activeNode) : this.#rootNode;
        // the logical start position of the change track selection
        let textStartPosition = null;
        // the logical end position of the change track selection
        let textEndPosition = null;
        // whether the last position of the node shall be calculated
        let useLastPosition = true;

        // if the change track is located inside header or footer, the edit mode must be activated
        if (isMarginalSelection) {
            // Check, if this is the correct header / footer that is activated
            if (this.docModel.isHeaderFooterEditState()) {
                if (getDomNode(this.docModel.getCurrentRootNode()) !== getDomNode(localRootNode)) {
                    // Switching to another header or footer
                    this.docModel.getPageLayout().switchTargetNodeInHeaderMode(localRootNode);
                }
            } else {
                this.docModel.getPageLayout().enterHeaderFooterEditMode(localRootNode, { keepNodes: true });
                // OT: In the future it might be necessary to update all CT nodes after activating header/footer (67215)
            }
        } else {
            if (this.docModel.isHeaderFooterEditState()) {
                this.docModel.getPageLayout().leaveHeaderFooterEditMode();
                this.docModel.getSelection().setNewRootNode(this.#rootNode); // restore original rootNode
            }
        }

        // the logical start position of the change track selection
        textStartPosition = getTextLevelPositionInNode(_.first(selection.selectedNodes), localRootNode);
        // the logical end position of the change track selection
        useLastPosition = !DOM.isParagraphNode(_.last(selection.selectedNodes)); // task 40735, not highlighting complete following paragraph
        textEndPosition = isColumnSelection ? null : getTextLevelPositionInNode(_.last(selection.selectedNodes), localRootNode, { lastPosition: useLastPosition });

        // removing color of existing selection
        this.clearChangeTrackSelection();

        // assigning the new selection
        this.#changeTrackSelection = selection;

        // highlight change track nodes
        if (makeVisible) {
            _.each(this.#changeTrackSelection.selectedNodes, node => {
                // highlight table nodes only
                if (DOM.isTableNode(node) || DOM.isTableRowNode(node) || DOM.isTableCellNode(node)) { $(node).addClass(this.#highlightClassname); }
            });
        }

        // adapting the text selection to the change track selection
        this.docModel.getSelection().setTextSelection(textStartPosition, textEndPosition,  { keepChangeTrackPopup });
    }

    /**
     * Finding the next or following change tracking node from the current text selection (not
     * change track selection!).
     *
     * @param {jQuery} allNodes
     *  A jQuery list with all change track nodes in the document.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the search of the change track node.
     *  The following options are supported:
     *  @param {Boolean} [options.next=true]
     *      Whether the following or the previous change track node shall be found.
     *  @param {Boolean} [options.reuseSameNode=false]
     *      Whether the same selected node can be reused. This is useful, if
     *      the same node contains a further change tracked modification and in the
     *      previous step one of these changes was accepted or rejected (41153).
     *  @param {Boolean} [options.useDocumentBorder=false]
     *      Whether the change track node shall be found starting the search from the beginning
     *      (for next = true) or the end (for next = false) of the document.
     *
     * @returns {HTMLElement|Null}
     *  The next or the previous change track node in relation to the current cursor position.
     *  If this element is a span inside an in-line component, the in-line component itself
     *  must be returned.
     */
    #getNextChangeTrackNode(allNodes, options) {

        // whether the following valid change track shall be found or the previous
        const next = getBooleanOption(options, 'next', true);
        // whether the following valid change track shall be found or the previous
        const reuseSameNode = getBooleanOption(options, 'reuseSameNode', false);
        // whether the search shall be started at the beginning or end of the document
        const useDocumentBorder = getBooleanOption(options, 'useDocumentBorder', false);
        // the start position from which the neighboring change track node shall be searched
        const startPosition = useDocumentBorder ? (next ? this.docModel.getSelection().getFirstDocumentPosition() : this.docModel.getSelection().getLastDocumentPosition()) : this.docModel.getSelection().getStartPosition();
        // the root node corresponding to the logical position
        const localRootNode = this.docModel.getSelection().getRootNode();
        // node info for the current selection
        const nodeInfo = getDOMPosition(localRootNode, startPosition);
        // the node currently selected by the text cursor (or at the beginning of an arbitrary selection range)
        let node = nodeInfo ? nodeInfo.node : null;
        // the change tracked node for the selection (must not be a span inside an in-line component
        let nextNode = null;
        // whether the search need to be continued, because the found node is inside template node
        let repeat = true;

        if (!node) { return null; }

        // replacing text nodes by its parent text span
        if (node.nodeType === 3) {
            node = node.parentNode;
        }

        // handle a change tracked node immediately, if it contains selection (also handling for 41153)
        // -> handling the parent node, because the paragraph might be the change tracked node
        if (reuseSameNode) {
            if (allNodes.is(node)) {
                repeat = false;
                nextNode = node;
            }

            if (allNodes.is(node.parentNode)) { // but prefer the parent, because paragraph is in collector before its span
                repeat = false;
                nextNode = node.parentNode;
            }
        }

        while (repeat) {
            nextNode = next ? findNextNode(localRootNode, node, DOM.CHANGETRACK_NODE_SELECTOR, null) : findPreviousNode(localRootNode, node, DOM.CHANGETRACK_NODE_SELECTOR, null);
            repeat = DOM.isMarginalNode(nextNode) && DOM.isInsideHeaderFooterTemplateNode(this.#rootNode, nextNode);
            if (repeat) { node = nextNode; }
        }

        if (!nextNode && allNodes.length > 0) {
            nextNode = next ? allNodes[0] : allNodes[allNodes.length - 1]; // no change track found in the specified direction (and DOCS-3301)
        }

        // the next node for the selection must not be the span inside an in-line component. In this
        // case it must be the in-line component itself.
        if (nextNode && nextNode.parentNode && DOM.isTextComponentNode(nextNode.parentNode)) {
            nextNode = nextNode.parentNode;
        }

        return nextNode;
    }

    /**
     * Creating and returning an ordered list of change track types for a specified node.
     *
     * @param {Node|jQuery|Null} node
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains. If missing or null, returns false.
     *
     * @returns {Any[]}
     *  The ordered list of change track types for the given node. This can be
     *  an array of strings are an empty array.
     */
    #getOrderedChangeTrackTypes(node) {

        // the different change track node types ordered by priority
        const nodeTypeOrder = [];

        // Info: If a change track is 'inserted' and 'removed', the removal has to happen
        // AFTER the insertion. Therefore removal is always the more relevant tracking
        // type, because it happened after the insertion.

        if (DOM.isChangeTrackRemoveNode(node)) {
            nodeTypeOrder.push('removed');
        }

        if (DOM.isChangeTrackInsertNode(node)) {
            nodeTypeOrder.push('inserted');
        }

        if (DOM.isChangeTrackModifyNode(node)) {
            nodeTypeOrder.push('modified');
        }

        return nodeTypeOrder;
    }

    /**
     * Creating and returning a new selection object describing a change track selection.
     * This object requires the elements 'activeNode', 'activeNodeType', 'activeNodeTypesOrdered',
     * 'selectedNodes' and optionally 'allChangeTrackNodes'.
     *
     * @param {HTMLElement} activeNode
     *  The HTMLElement of the change track selection. Must be the first element of all elements
     *  in the selection.
     *
     * @param {String} activeNodeType
     *  The change track type of the active node. Must be one of the values defined in
     *  CHANGE_TRACK_TYPES.
     *
     * @param {String[]} activeNodeTypesOrdered
     *  An ordered list of strings of node types defined in CHANGE_TRACK_TYPES. The order
     *  corresponds to the priority of the change tracking types for the active node.
     *
     * @param {HTMLElement[]} selectedNodes
     *  An ordered list of all HTML elements, that are combined in the current change track
     *  selection.
     *
     * @param {jQuery} [allChangeTrackNodes]
     *  An optional cache that contains all change track elements in the document. This is
     *  saved in the selection for performance reasons.
     *
     * @returns {Object}
     *  An object, describing the change track selection.
     */
    #createChangeTrackSelection(activeNode, activeNodeType, activeNodeTypesOrdered, selectedNodes, allChangeTrackNodes) {

        // the new change track selection object
        const selection = {};

        selection.activeNode = activeNode;
        selection.activeNodeType = activeNodeType;
        selection.activeNodeTypesOrdered = activeNodeTypesOrdered;
        selection.selectedNodes = selectedNodes;
        selection.allChangeTrackNodes = allChangeTrackNodes;  // this can be 'undefined'

        return selection;
    }

    /**
     * Finding a valid change track selection. This can start with an existing
     * change track selection or without such a selection at any text selection.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the search of a valid change
     *  track selection.
     *  The following options are supported:
     *  @param {Boolean} [options.next=true]
     *      Whether the following or the previous change track selection node shall be found.
     *  @param {Boolean} [options.reuseSameNode=false]
     *      Whether the same selected node can be reused. This is useful, if
     *      the same node contains a further change tracked modification.
     *
     * @returns {Object|Null}
     *  The next or the previous change track selection or Null, if no valid change track selection
     *  can be determined.
     */
    #iterateNextValidChangeTrackSelection(options) {

        // whether the following valid change track shall be found or the previous
        const next = getBooleanOption(options, 'next', true);
        // whether the following valid change track shall be found or the previous
        const reuseSameNode = getBooleanOption(options, 'reuseSameNode', false);
        // whether the iteration shall be continued
        let continueIteration = true;
        // whether a valid change track selection was found
        let foundValidChangeTrackSelection = false;
        // the active node of a change track selection
        let activeNode = null;
        // an ordered list of change track types for the active node
        let activeNodeTypesOrdered = null;
        // the change track type for the active node
        let activeNodeType = null;
        // the ordered array of selected nodes neighboring the active node and its change track type
        let selectedNodes = null;
        // the list of all change track nodes in a jQuery list
        let allChangeTrackNodes = null;
        // a helper object to create a change track selection
        let localChangeTrackSelection = null;

        // helper function to find next or previous change track node type in ordered list of change track type.
        // If there is no previous or following change track type in the list, null is returned.
        const getNextNodeType = () => {

            let index = null;
            let newIndex = null;

            if (next && (_.last(activeNodeTypesOrdered) === activeNodeType)) { return null; }

            if (!next && (_.first(activeNodeTypesOrdered) === activeNodeType)) { return null; }

            index = _.indexOf(activeNodeTypesOrdered, activeNodeType);

            newIndex = next ? (index + 1) : (index - 1);

            return activeNodeTypesOrdered[newIndex];
        };

        // helper function to find next or previous change track node in jQuery list of all change track nodes.
        // If there is no previous or following change track node in the jQuery list, null is returned.
        const getNextChangeTrackingNodeInListOfChangeTrackingNodes = () => {

            // the index of the node inside the jQuery collection of all change track nodes
            let index = null;
            // the modified new index
            let newIndex = null;
            // a helper node
            let checkNode = activeNode;
            //whether the child nodes shall be checked
            let checkChildren = true;

            // receiving the node from list of all nodes at specified index
            // -> for text component nodes the 'div' element must be returned, not the span inside
            const getNodeAtIndex = index => {

                if (!allChangeTrackNodes || !allChangeTrackNodes[index]) { return null; }

                if (DOM.isTextComponentNode(getDomNode(allChangeTrackNodes[index]).parentNode)) {
                    return getDomNode(allChangeTrackNodes[index]).parentNode;
                }
                return allChangeTrackNodes[index];
            };

            // avoiding endless loop when checking child nodes and stepping to previous changetrack (57253)
            if (!next && allChangeTrackNodes.index(checkNode) > -1) { checkChildren = false; }

            // the active node can be an in-line component, but the list of all change tracks contains
            // the span inside the in-line components (checking complex fields, that might have no children).
            if (checkChildren && DOM.isTextComponentNode(checkNode) && !DOM.isDrawingPlaceHolderNode(checkNode) && getDomNode(checkNode).firstChild) { checkNode = getDomNode(checkNode).firstChild; }

            // searching the current node inside the list of all change track nodes.
            index = allChangeTrackNodes.index(checkNode);

            if (next && (index === allChangeTrackNodes.length - 1)) { return getNodeAtIndex(0); }

            if (!next && (index === 0)) { return getNodeAtIndex(allChangeTrackNodes.length - 1); }

            newIndex = next ? (index + 1) : (index - 1);

            return getNodeAtIndex(newIndex);
        };

        if (this.#changeTrackSelection && this.#changeTrackSelection.selectedNodes) { this.#checkSelectionValidity(this.#changeTrackSelection); } // checking selection, DOCS-2614

        if (this.#changeTrackSelection) {

            // there is an existing change track selection -> using this selection as start point for new selection
            if (!this.#changeTrackSelection.allChangeTrackNodes) { this.#changeTrackSelection.allChangeTrackNodes = this.#getAllChangeTrackNodes(); }

            allChangeTrackNodes = this.#changeTrackSelection.allChangeTrackNodes;
            activeNode = this.#changeTrackSelection.activeNode;
            activeNodeType = this.#changeTrackSelection.activeNodeType;
            activeNodeTypesOrdered = this.#changeTrackSelection.activeNodeTypesOrdered;

            // first iteration step for existing change track selection
            activeNodeType = getNextNodeType();
            if (!activeNodeType) {
                // finding the next change tracking node
                activeNode = getNextChangeTrackingNodeInListOfChangeTrackingNodes();
                if (!activeNode) { return null; }
                activeNodeTypesOrdered = this.#getOrderedChangeTrackTypes(activeNode);
                activeNodeType = next ? _.first(activeNodeTypesOrdered) : _.last(activeNodeTypesOrdered);
            }

        } else {
            // There is no existing change track selection
            // -> collecting all change tracking nodes in the document
            allChangeTrackNodes = this.#getAllChangeTrackNodes();
            // there is no existing change track selection -> finding the next or previous change track node
            if (allChangeTrackNodes.length > 0) {
                activeNode = this.#getNextChangeTrackNode(allChangeTrackNodes, { next, reuseSameNode });
                if (!activeNode) {
                    // there is the chance to find a change track element, starting from beginning or end of document
                    activeNode = this.#getNextChangeTrackNode(allChangeTrackNodes, { next, useDocumentBorder: true });
                    if (!activeNode) { return null; }
                }
                activeNodeTypesOrdered = this.#getOrderedChangeTrackTypes(activeNode);
                activeNodeType = next ? _.first(activeNodeTypesOrdered) : _.last(activeNodeTypesOrdered);
            } else {
                return null;
            }

        }

        // 1. iterating over all change tracking nodes
        while (activeNode && continueIteration) {

            // 2. iterating over all activeNodesTypesOrdered
            while (activeNodeType && continueIteration) {

                if (DOM.isFloatingNode(activeNode) || DOM.isDrawingPlaceHolderNode(activeNode)) {
                    // floated nodes (drawings) are never grouped with their neighbors
                    selectedNodes = [activeNode];
                } else {
                    // finding all adjacent elements for the specified active node and the change track type.
                    // Using 'findTableChildren', so that search in this.#combineAdjacentNodes happens not from span to
                    // paragraph to cell to row to table, but in the opposite direction.
                    selectedNodes = this.#combineAdjacentNodes(activeNode, { type: activeNodeType, findTableChildren: true });
                }

                // is the active node type the first element of the selected nodes?
                if (selectedNodes && (selectedNodes[0] === activeNode)) {
                    continueIteration = false;
                    foundValidChangeTrackSelection = true;
                }

                if (!foundValidChangeTrackSelection) {
                    // changing to the next/previous change tracking type
                    activeNodeType = getNextNodeType();
                }
            }

            if (!foundValidChangeTrackSelection) {
                // finding the next change tracking node
                activeNode = getNextChangeTrackingNodeInListOfChangeTrackingNodes();
                if (!activeNode) { return null; }
                activeNodeTypesOrdered = this.#getOrderedChangeTrackTypes(activeNode);
                activeNodeType = next ? _.first(activeNodeTypesOrdered) : _.last(activeNodeTypesOrdered);
            }
        }

        if (foundValidChangeTrackSelection) {
            localChangeTrackSelection = this.#createChangeTrackSelection(activeNode, activeNodeType, activeNodeTypesOrdered, selectedNodes, allChangeTrackNodes);
        }

        return localChangeTrackSelection;
    }

    /**
     * Selecting all nodes with the same tracking type that are direct neighbors.
     *
     * @param {HTMLElement|jQuery} trackingNode
     *  Element that will be evaluated for change tracking.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the preparation of the span.
     *  The following options are supported:
     *  @param {String} [options.type='']
     *   The type of resolving, must be member of CHANGE_TRACK_TYPES.
     *  @param {Boolean} [options.findTableChildren=false]
     *   Whether the group shall be created from table to its children and not
     *   vice versa. Typically the grouping process is based to search from table
     *   cells to table rows to tables. But if a table node is given to this
     *   function as trackingNode and the rows or cells of the same group need
     *   to be defined, 'findTableChildren' must be set to true.
     *
     * @returns {HTMLElement[]}
     *  All nodes with the same tracking type that are direct neighbors within
     *  one array.
     */
    #combineAdjacentNodes(trackingNode, options) {

        // all adjacent nodes with specified type
        let allNodes = [];
        // the tracking type of combined nodes
        const type = getStringOption(options, 'type', '').toLowerCase();
        // the author, whose tracking nodes will be combined
        let author = null;
        // the currently investigated node
        const currentNode = getDomNode(trackingNode);
        // whether the tracking node is a table cell node
        const isTableCellTrackingNode = DOM.isTableCellNode(trackingNode);
        // whether the tracking node is inside header or footer
        let isMarginalNode = false;
        // the root node corresponding to the tracking node
        let trackingRootNode = null;
        // whether a table needs to find affected children
        const findTableChildren = getBooleanOption(options, 'findTableChildren', false);

        // helper function to find a previous or following change track node, that
        // has the same resolveType and the same author (checked by the validator).
        const neighboringChangeTrackNode = (node, validator, options) => {

            // the relevant change track node
            let neighborNode = null;
            // the parent of the current node and its neighbor node
            let parentNode = null;
            let neighborParentNode = null;
            let checkNode = null;
            // the child of a specified paragraph node
            let childNode = null;
            // the currently investigated paragraph, cell, row, or table node
            let paraNode = null;
            let cellNode = null;
            let rowNode = null;
            let tableNode = null;
            // whether the next or previous node shall be found
            const next = getBooleanOption(options, 'next', true);
            // whether neighbors with same modification shall be searched
            const modified = getBooleanOption(options, 'modified', false);
            // the name of the dom function to be called
            const finder = next ? 'nextSibling' : 'previousSibling';

            if (DOM.isTableNode(node)) {
                neighborNode = null;  // no additional node need to be collected
                // -> is it possible, that rows and cells need to be collected, too?
                // -> neighboring tables will not be combined (like neighboring paragraphs)
            } else if (DOM.isTableRowNode(node)) {

                rowNode = node;

                if (rowNode[finder]) {
                    rowNode = rowNode[finder];

                    // skipping page break rows
                    while (rowNode && DOM.isTablePageBreakRowNode(rowNode)) {
                        rowNode = rowNode[finder] || null;
                    }

                    if (rowNode && DOM.isTableRowNode(rowNode) && validator.call(this, rowNode)) {
                        neighborNode = rowNode;
                    }
                } else {
                    if (next) {
                        // checking also the complete table element, if there is no following row
                        tableNode = rowNode.parentNode.parentNode;

                        if (tableNode && DOM.isTableNode(tableNode) && validator.call(this, tableNode)) {
                            neighborNode = tableNode;
                        }
                    }
                }

            } else if (DOM.isTableCellNode(node)) {

                cellNode = node;

                // 'modified': Finding all single modified cells, also the table (ignoring rows)
                // 'inserted'/'deleted': All cells in column are found in another process

                if (!modified) {
                    neighborNode = null;  // accepting insert/delete of one column at a time
                    // -> neighboring columns will not be combined (like neighboring paragraphs)
                    // -> 'inserted' and 'deleted' can be set at cells only for complete columns.
                } else {

                    // simple case for modifications:
                    // -> accepting/rejecting all modifications in the table in one step
                    // -> this includes modifications in cells and the table itself,
                    // but not rows (example: modified table style)

                    if (cellNode[finder]) {
                        cellNode = cellNode[finder];

                        if (cellNode && DOM.isTableCellNode(cellNode) && validator.call(this, cellNode)) {
                            neighborNode = cellNode;
                        }
                    } else {
                        // no neighboring cell found -> checking cells in next row (but ignoring the row itself)
                        rowNode = cellNode.parentNode;
                        if (rowNode && rowNode[finder]) {
                            rowNode = rowNode[finder];
                            if (rowNode && DOM.isTableRowNode(rowNode)) {
                                cellNode = next ? rowNode.firstChild : rowNode.lastChild;
                                if (cellNode && DOM.isTableCellNode(cellNode) && validator.call(this, cellNode)) {
                                    neighborNode = cellNode;
                                }
                            }
                        } else {
                            if (next) {
                                // checking also the complete table element, if there is no following row
                                tableNode = rowNode.parentNode.parentNode;
                                if (tableNode && DOM.isTableNode(tableNode) && validator.call(this, tableNode)) {
                                    neighborNode = tableNode;
                                }
                            }
                        }
                    }
                }

            } else if (DOM.isParagraphNode(node)) {

                if (modified) {
                    paraNode = node;
                } else {
                    paraNode = next ? node : node[finder]; // using previous paragraph, when searching to the beginning
                }

                // skipping page break nodes
                while (paraNode && DOM.isPageBreakNode(paraNode)) {
                    paraNode = paraNode[finder] || null;
                }

                if (paraNode && DOM.isParagraphNode(paraNode)) {
                    if (modified) {
                        // modifications can be grouped on paragraph level, independent from the content inside the paragraph
                        paraNode = paraNode[finder];
                        if (paraNode && DOM.isParagraphNode(paraNode) && validator.call(this, paraNode)) {
                            neighborNode = paraNode;  // improvement: checking type of modification
                        }
                    } else {
                        childNode = next ? DOM.findFirstPortionSpan(paraNode) : DOM.findLastPortionSpan(paraNode);
                        if (childNode && (DOM.isTextSpan(childNode) || DOM.isTextComponentNode(childNode)) && validator.call(this, childNode)) {
                            neighborNode = childNode;
                        } else if (getParagraphNodeLength(paraNode) === 0) {
                            // checking neighboring paragraph, if it is empty
                            if (next) {
                                paraNode = paraNode[finder];
                                // skipping page break nodes
                                while (paraNode && DOM.isPageBreakNode(paraNode)) {
                                    paraNode = paraNode[finder] || null;
                                }
                            }

                            if (paraNode && DOM.isParagraphNode(paraNode) && validator.call(this, paraNode)) {
                                neighborNode = paraNode;  // using the current paragraph
                            }
                        }
                    }
                }

            } else {  // text level components

                checkNode = node[finder];  // getting the sibling

                while (checkNode && !DOM.isInlineComponentNode(checkNode) && !DOM.isTextSpan(checkNode)) {  // but handling only text component siblings
                    checkNode = checkNode[finder] || null;
                }

                if (checkNode) {
                    // checking a direct sibling, but ignoring nodes of specified type
                    if (validator.call(this, checkNode)) {
                        neighborNode = checkNode;  // improvement for 'modified': checking type of modification
                    }
                } else {
                    // checking if a paragraph parent node can be used temporary (only valid for text level nodes)
                    // For modified attributes, no switch from text to para and vice versa
                    if ((!modified) && (DOM.isTextSpan(node) || DOM.isInlineComponentNode(node))) {

                        parentNode = node.parentNode;

                        if (parentNode) {
                            if (!next) {
                                // searching to the beginning -> adding current paragraph parent
                                if (DOM.isParagraphNode(parentNode) && validator.call(this, parentNode)) {
                                    neighborNode = parentNode;  // using the current paragraph
                                }
                            } else {
                                // searching to the end -> adding the following paragraph parent
                                neighborParentNode = parentNode[finder];

                                // skipping page break nodes
                                while (neighborParentNode && DOM.isPageBreakNode(neighborParentNode)) {
                                    neighborParentNode = neighborParentNode[finder] || null;
                                }

                                if (DOM.isParagraphNode(parentNode) && neighborParentNode && DOM.isParagraphNode(neighborParentNode) && validator.call(this, neighborParentNode)) {
                                    neighborNode = neighborParentNode;  // using the previous or following paragraph
                                }
                            }
                        }
                    }
                }
            }

            return neighborNode;
        };

        // helper function to collect all neighboring nodes with
        const collectNeighbours = (node, validator, options) => {

            // whether the modified neighbors shall be search
            const modified = getBooleanOption(options, 'modified', false);
            // the currently investigated neighbor node
            let current = neighboringChangeTrackNode(node, validator, { next: false, modified });

            while (current) {
                allNodes.unshift(current);
                current = neighboringChangeTrackNode(current, validator, { next: false, modified });
            }

            current = neighboringChangeTrackNode(node, validator, { next: true, modified });

            while (current) {
                allNodes.push(current);
                current = neighboringChangeTrackNode(current, validator, { next: true, modified });
            }

        };

        // helper function to remove empty text spans, range marker nodes or comment
        // place holder nodes, if they are at the beginning or at the end of the nodes
        // collection. This can be more than one on each side, for example caused by
        // floated drawings
        const removeUntrackedNodesAtBorders = allNodes => {

            // the node to be checked, if it can be removed from selection
            let checkNode = allNodes[allNodes.length - 1];

            while (isChangeTrackSkipNode(checkNode) && !DOM.isChangeTrackNode(checkNode)) {
                allNodes.pop();
                checkNode = allNodes[allNodes.length - 1];
            }

            checkNode = allNodes[0];

            while (isChangeTrackSkipNode(checkNode) && !DOM.isChangeTrackNode(checkNode)) {
                allNodes.shift();
                checkNode = allNodes[0];
            }

        };

        // Some specified nodes must not split a group of neighboring change track nodes, because
        // they contain meta information, like comments. (ComplexFieldNode only if loaded from Word (54373))
        const isChangeTrackSkipNode = node => {
            return DOM.isEmptySpan(node) || DOM.isRangeMarkerNode(node) || DOM.isBookmarkNode(node) || DOM.isComplexFieldNode(node) || DOM.isCommentPlaceHolderNode(node);
        };

        // special handling required for table columns (cells that are inserted or removed)
        if (isTableCellTrackingNode &&
            (((type === 'inserted') && DOM.isChangeTrackInsertNode(currentNode)) || ((type === 'removed') && DOM.isChangeTrackRemoveNode(currentNode)))) {
            // -> collecting all cells of the column and adding it to allNodes
            // -> no further collecting required
            isMarginalNode = DOM.isMarginalNode(currentNode); // whether the cell node is in header/footer
            trackingRootNode = isMarginalNode ? DOM.getMarginalTargetNode(this.#rootNode, currentNode) : this.#rootNode;
            return getAllCellsNodesInColumn(trackingRootNode, currentNode);
        }

        // special handling for table nodes, if this is a 'up-down-selection' (caused by next and forward button)
        // The process in 'collectNeighbours' is a 'down-up' from table cells to rows to tables.
        if (DOM.isTableNode(currentNode) && findTableChildren) {

            // a modified table must be grouped with the modified cells
            if ((type === 'modified') && DOM.isChangeTrackModifyNode(currentNode)) {
                _.each($(currentNode).find('> tbody > tr > td[data-change-track-modified="true"]'), node => {
                    allNodes.push(node);
                });
            } else if ((type === 'inserted') && DOM.isChangeTrackInsertNode(currentNode)) { // an inserted table must be grouped with all rows
                _.each($(currentNode).find('> tbody > tr[data-change-track-inserted="true"]'), node => {
                    allNodes.push(node);
                });
            }

            // filtering by author
            if (type) {
                author = this.getChangeTrackInfoFromNode(currentNode, 'author', type);
                allNodes = _.filter(allNodes, oneNode => {
                    return this.nodeHasChangeTrackAuthor(oneNode, type, author);
                });
            }

            // finally adding the table node
            // -> in the selection the table must be 'before' its rows, so that resolving of
            // change tracking happens in the correct order (-> unshift instead of push)
            allNodes.unshift(currentNode);

            return allNodes;
        }

        allNodes.push(currentNode);
        author = this.getChangeTrackInfoFromNode(currentNode, 'author', type);

        if ((DOM.isChangeTrackRemoveNode(currentNode)) && (type === 'removed')) {
            collectNeighbours(currentNode, node => {
                return (DOM.isChangeTrackRemoveNode(node) && this.nodeHasChangeTrackAuthor(node, 'removed', author)) || (!DOM.isChangeTrackNode(node) && isChangeTrackSkipNode(node)); // adding skip check, if node is changeTracked at all (56461)
            });
        } else if ((DOM.isChangeTrackInsertNode(currentNode)) && (type === 'inserted')) {
            collectNeighbours(currentNode, node => {
                return (DOM.isChangeTrackInsertNode(node) && this.nodeHasChangeTrackAuthor(node, 'inserted', author)) || (!DOM.isChangeTrackNode(node) && isChangeTrackSkipNode(node));
            });
        } else if (DOM.isChangeTrackModifyNode(currentNode) && (type === 'modified')) {
            collectNeighbours(currentNode, node => {
                return (DOM.isChangeTrackModifyNode(node) && this.nodeHasChangeTrackAuthor(node, 'modified', author)) || (!DOM.isChangeTrackNode(node) && isChangeTrackSkipNode(node));
            }, { modified: true });
        }

        // empty spans are only allowed between tracking nodes, if they are not tracked
        removeUntrackedNodesAtBorders(allNodes);

        return allNodes;
    }

    /**
     * Checking whether inside a specified paragraph all elements are change tracked
     * and after resolving all change tracks, the paragraph will be empty.
     *
     * The paragraph will be empty, if:
     * - all children from paragraph node are in the specified list of resolve nodes
     *   and are of type 'inserted' and accepted is false.
     *   or
     * - all children from paragraph node are in the specified list of resolve nodes
     *   and are of type 'removed' and accepted is true.
     *
     * @param {HTMLElement|jQuery} node
     *  The paragraph element whose children will be investigated.
     *
     * @param {jQuery|Object[]} resolveNodes
     *  The list of all nodes, that are resolved in this specific process.
     *
     * @param {Boolean} accepted
     *  Whether the change tracks shall be accepted or rejected.
     *
     * @returns {Boolean}
     *  Whether all sub nodes of the paragraph are removed during accepting
     *  or rejecting the change tracks.
     */
    #paragraphEmptyAfterResolving(node, resolveNodes, accepted) {

        // the html dom node
        const domNode = getDomNode(node);
        // the child, that is not correctly change tracked
        let childNode = null;
        // the change track type that needs to be checked
        const type = accepted ? 'removed' : 'inserted';

        // check, if the node is a paragraph
        if (!DOM.isParagraphNode(domNode)) { return false; }

        // iterating over all children. Trying to find one node, that is
        // not an empty text span and that is not of the specific type or
        // that is not in the list of all nodes that are resolved in this process.
        childNode = _.find($(domNode).children(), child => {
            return !DOM.isEmptySpan(child) && (!this.nodeHasSpecifiedChangeTrackType(child, type) || !_.contains(resolveNodes, child));
        });

        return !childNode;
    }

    /**
     * Checking, whether a specified node will be removed during current accepting
     * or rejecting of change tracks.
     *
     * @param {HTMLElement|jQuery} node
     *  The node, that will be investigated.
     *
     * @param {jQuery|Object[]} resolveNodes
     *  The list of all nodes, that are resolved in this specific process.
     *
     * @param {Boolean} accepted
     *  Whether the change tracks shall be accepted or rejected.
     *
     * @returns {Boolean}
     *  Whether the node will be removed during current accepting or rejecting of
     *  change tracks.
     */
    #nodeRemovedDuringResolving(node, resolveNodes, accepted) {

        // whether the node will be removed
        let removed = false;

        // helper function
        const nodeRemoved = oneNode => {
            return _.contains(resolveNodes, getDomNode(oneNode)) && ((DOM.isChangeTrackInsertNode(oneNode) && !accepted) || (DOM.isChangeTrackRemoveNode(oneNode) && accepted));
        };

        // if the node is a table node, all rows need to be checked
        if (DOM.isTableNode(node)) {
            removed = _.all(DOM.getTableRows(node), row => {
                return nodeRemoved(row);
            });
        } else {
            removed = nodeRemoved(node);
        }

        return removed;
    }

    /**
     * Adding change tracking information (change track type object with author and
     * date) to a given operation. The operation is modified directly, no copy is
     * created. This functionality is currently implemented for clipboard operations.
     *
     * @param {Object} operation
     *  The operation that will be expanded for the change track information.
     *
     * @param {Object} [info]
     *  The change tracking information object containing the author and the date.
     *  This parameter is optional. If it is not set, this object is generated.
     *
     * @param {Object} [options]
     *  A map with additional options controlling the addition of change tracking
     *  information.
     *  The following options are supported:
     *  @param {Boolean} [options.clipboard=false]
     *      Whether this function was called from the clip board API.
     */
    #addChangeTrackInfoToOperation(operation, info, options) {

        if (!operation) { return; }

        // the key for the changes object
        let changesKey = null;
        // whether this function was triggered from clip board API
        const isClipboard = getBooleanOption(options, 'clipboard', false);

        // helper function to generate and collect additional setAttributes
        // operations after splitParagraph operations.
        const generateChangeTrackSetAttributesOperation = () => {
            const paraPos = _.clone(operation.start);
            paraPos.pop();
            paraPos[paraPos.length - 1]++;
            this.#clipboardCollector.push({ name: Op.SET_ATTRIBUTES, start: _.copy(paraPos), attrs: { changes: { inserted: info } } });
        };

        // never adding change track info to operations that add content into comments
        if (this.docApp.isTextApp() && operation.target && this.docModel.getCommentLayer().isCommentId(operation.target)) { return; }

        // generating change track info, if not provided
        if (!info) { info = this.getChangeTrackInfo(); }

        // Info: Operations.SET_ATTRIBUTES does not need to be change tracked from clip board operations
        // because during pasting, attributes can also be assigned to inserted content. It is even better
        // not to change track setAttributes (with 'modified' key), because otherwise the user needs to
        // resolve two change tracks.

        if (isClipboard) {
            if (_.contains(CHANGETRACK_CLIPBOARD_OPERATIONS, operation.name)) {
                if (!(operation.name === Op.RANGE_INSERT && operation.type === 'comment')) { // no change tracking for comment range markers
                    changesKey = 'inserted';
                }
            } else if (Op.PARA_SPLIT === operation.name) {
                // After splitParagraph an additional setAttributes operations is required,
                // so that the new paragraph is marked as inserted.
                generateChangeTrackSetAttributesOperation();
            }
        }

        // Idea: In the future this function can be the central point, where change track information is
        // added to any operation. Not only for clip board generated operations. In this case, the
        // changesKey must be a parameter or it must be determined dynamically.

        if (changesKey) {
            operation.attrs = operation.attrs || {};
            operation.attrs.changes = operation.attrs.changes || {};
            operation.attrs.changes[changesKey] = info;
        }
    }

    /**
     * Checking whether the specified node is a table node, that will be removed within this change
     * track resolution, because all table rows will be removed.
     *
     * @param {HTMLElement|jQuery} node
     *  The element whose that will be investigated.
     *
     * @returns {Boolean}
     *  Whether the node is a table node that will be removed within this change track resolution process.
     */
    #allRowsOfTableCTInserted(node) {
        return DOM.isTableNode(node) && _.every(DOM.getTableRows(node), DOM.isChangeTrackInsertNode);
    }

    /**
     * Checking whether a node will be removed within this change track resolution.
     * In this case, further operations for this node must not be generated, for
     * example additional setAttributes operation, that might cause problems because
     * of the operation order.
     *
     * @param {HTMLElement|jQuery} node
     *  The element whose that will be investigated.
     *
     * @param {jQuery|Object[]} resolveNodes
     *  The list of all nodes, that are resolved in this specific process.
     *
     * @param {Boolean} accepted
     *  Whether the change tracks shall be accepted or rejected.
     *
     * @returns {Boolean}
     *  Whether the node will be removed within this change track resolution
     *  process.
     */
    #isRemovalNode(node, resolveNodes, accepted) {

        if (!_.contains(resolveNodes, node)) { return false; }

        return (accepted && DOM.isChangeTrackRemoveNode(node)) || (!accepted && (DOM.isChangeTrackInsertNode(node) || this.#allRowsOfTableCTInserted(node)));
    }

    /**
     * Checking, if after all change tracks are resolved, there will be table cells
     * without paragraphs, or missing paragraphs behind tables in table cells.
     *
     * Info: If required, the delete operations are directly removed from the
     *       operations container within this function.
     *
     * @param {Object[]} operations
     *  The operation container.
     *
     * @param {Object[]} deleteOps
     *  A container with all delete operations of paragraphs inside table cells.
     */
    #validateTableCells(operations, deleteOps) {

        const allTableCells = [];

        // helper function to find all affected table cells
        const cellInCollector = (cellPos, target) => {
            return _.isObject(_.find(allTableCells, oneCell => {
                return oneCell.target === target && _.isEqual(oneCell.pos, cellPos);
            }));
        };

        // helper function to delete one operation from the operations array
        const removeDeleteOperation = (paraPos, cellTarget) => {

            const operationIndex = _.findIndex(operations, op => {
                const target = op.target || '';
                return op.name === Op.DELETE && target === cellTarget && _.isEqual(op.start, paraPos);
            });

            if (operationIndex > -1) {
                operations.splice(operationIndex, 1); // delete the delete operation on the operation array
            }
        };

        // searching for all table cells
        _.each(deleteOps, op => {
            const target = op.targetId || '';
            const cellPos = _.initial(op.pos);

            if (!cellInCollector(cellPos, target)) {
                allTableCells.push({ pos: _.clone(cellPos), target });
            }
        });

        // iterating over all affected table cells
        _.each(allTableCells, oneCell => {

            const nodeInfo = getDOMPosition(this.docModel.getRootNode(oneCell.target), oneCell.pos);
            const cellNode = (nodeInfo && nodeInfo.node) ? nodeInfo.node : null;
            let cellContentNode = null;
            let allTablesInCell = null;
            let lastTable = null;
            let tablePos = null;
            let numberOfParagraphsInCell = 0;
            let deletedParagraphsInCell = 0;
            let numberOfParagraphsInCellBehindTable = 0;
            let deletedParagraphsInCellBehindTable = 0;

            if (cellNode) {
                cellContentNode = DOM.getCellContentNode(cellNode);
                allTablesInCell = cellContentNode.children('table');

                if (allTablesInCell.length > 0) {
                    lastTable = allTablesInCell.last();
                    tablePos = getOxoPosition(cellNode, lastTable, true); // position of the table inside the cell
                    numberOfParagraphsInCellBehindTable = lastTable.nextAll(DOM.PARAGRAPH_NODE_SELECTOR).length; // number of paragraphs in the cell

                    deletedParagraphsInCellBehindTable = _.filter(deleteOps, op => {
                        const target = op.target || '';
                        return oneCell.target === target && _.isEqual(oneCell.pos, _.initial(op.pos)) && tablePos[0] < _.last(op.pos);
                    });

                    if (deletedParagraphsInCellBehindTable && deletedParagraphsInCellBehindTable.length === numberOfParagraphsInCellBehindTable) {
                        // all paragraphs in cell behind table removed
                        // -> removing the delete operation of the first paragraph behind the table from the list of all operations
                        removeDeleteOperation(appendNewIndex(oneCell.pos, tablePos[0] + 1), oneCell.target);
                    }

                } else {
                    numberOfParagraphsInCell = cellContentNode.children(DOM.PARAGRAPH_NODE_SELECTOR).length; // number of paragraphs in the cell

                    deletedParagraphsInCell = _.filter(deleteOps, op => {
                        const target = op.target || '';
                        return oneCell.target === target && _.isEqual(oneCell.pos, _.initial(op.pos));
                    });

                    if (deletedParagraphsInCell && deletedParagraphsInCell.length === numberOfParagraphsInCell) {
                        // all paragraphs in cell removed -> removing the delete operation of the first paragraph from the list of all operations
                        removeDeleteOperation(appendNewIndex(oneCell.pos), oneCell.target);
                    }
                }
            }
        });
    }

    /**
     * Test function, to guarantee that a setAttributes operation is never executed
     * after a delete operation on the same range. For this it is necessary, that
     * in the specified operations array, the setAttributes operation must not be
     * listed before the delete operation (the order is reversed later).
     * Checked are only operations with logical positions, that are listed in the
     * array 'positions'.
     *
     * @param {Object[]} operations
     *  The operation container.
     *
     * @param {Object} positions
     *  Optionally an array containing objects with the logical positions and target IDs,
     *  that need to be investigated. These objects must have the properties 'pos' and
     *  'targetId'. For comparison of targets it is important, that 'undefined' can be
     *  used.
     */
    #validateDeleteOrderAtPositions(operations, positions) {

        // Iterating over all specified positions
        _.each(positions, onePosition => {

            const position = onePosition.pos;
            const target = onePosition.targetId;  // using undefined as valid value for comparison
            // an array with all indexes of setAttributes operations at specified positions
            const setAttributesCollector = [];

            // checking the order of setAttributes and delete operations
            operations.some((operation, index) => {

                if (operation.name === Op.SET_ATTRIBUTES && _.isEqual(position, operation.start) && operation.target === target) {
                    setAttributesCollector.push(index); // saving the index for later usage
                }

                if (operation.name === Op.DELETE && _.isEqual(position, operation.start) && operation.target === target) {

                    if (setAttributesCollector.length > 0) {

                        // reverting the order, so that the index decreases
                        setAttributesCollector.reverse();

                        // iterating over all affected indexes in the operations array
                        _.each(setAttributesCollector, oneIndex => {
                            // inserting all set attributes operation behind the delete operation
                            operations.splice(index + 1, 0, operations[oneIndex]);
                            // delete the set attributes operations
                            operations.splice(oneIndex, 1);
                            // reducing the current index of the delete operation
                            index--;
                        });

                    }

                    return true; // exit the loop
                }
            });
        });
    }

    /**
     * Test function, to guarantee that a setAttributes operation is never executed
     * after a delete or a merge operation on the same range. For this it is necessary,
     * that in the specified operations array, the setAttributes operation must not be
     * listed before the delete or merge operation (the order is reverted later).
     *
     * @param {Object[]} operations
     *  The operation container.
     *
     * @param {String} operationName
     *  The name of the operation that is compared with the setAttributes operation.
     *
     * @param {Number} [positionCorrection]
     *  An optional correction value for the logical start position, dependent from
     *  the operation. The 'delete' operation can be used as is, but the 'merge'
     *  operation needs to be increased by '1', because a setAttribute at position
     *  [1] must not be executed after a merge operation at position [0].
     */
    #validateSetAttributesOrder(operations, operationName, positionCorrection) {

        // whether the check needs to be repeated
        let doCheck = true;
        // a collector for all setAttributes operations
        let setAttributesCollector = {};
        // a key generated from start and end position
        let key = '';

        // helper function to generate keys to recognize start and end position
        const generateKey = (posA, posB, target) => {
            return posA.join('_') + '__' + posB.join('_') + (_.isString(target) ? ('__' + target) : '');
        };

        // checking the order of setAttributes and delete operations
        const validateOrder = (operation, index) => {

            // a logical position that includes optionally the specified corrected position
            let correctedPosition = null;

            if (operation.name === Op.SET_ATTRIBUTES) {
                key = generateKey(operation.start, operation.end || operation.start, operation.target);
                setAttributesCollector[key] = index; // saving the index for later usage
            }

            if (operation.name === operationName) {

                if (_.isNumber(positionCorrection) && positionCorrection > 0) {
                    correctedPosition = _.clone(operation.start);
                    correctedPosition[correctedPosition.length - 1] += positionCorrection;
                }

                key = generateKey(correctedPosition ? correctedPosition : operation.start, operation.end || (correctedPosition ? correctedPosition : operation.start), operation.target);

                if (_.isNumber(setAttributesCollector[key])) {
                    // inserting the set attributes operation behind the delete operation
                    operations.splice(index + 1, 0, operations[setAttributesCollector[key]]);
                    // delete the set attributes operation
                    operations.splice(setAttributesCollector[key], 1);
                    // repeating the check from the beginning
                    doCheck = true;
                    return true; // exit the some() loop
                }
            }
        };

        // running the test
        while (doCheck) {
            doCheck = false;
            setAttributesCollector = {};
            operations.some(validateOrder);
        }
    }

    /**
     * Combining directly following delete operations and remove setAttributes operations for deleted content (65809).
     * But only, if the content is inside one paragraph (67202) -> never combining cells or rows
     *
     * @param {Object[]} operations
     *  The operation container that might be modified within this function.
     */
    #combineOperations(operations) {

        let followingOp = null;
        let currentOp = null;
        let max = operations.length;
        let idx = 0;

        // iterating in reverse order, so that setAttributes operations can be directly removed
        for (idx = max - 1; idx >= 0; idx--) {
            currentOp = operations[idx];
            if (followingOp && followingOp.name === Op.SET_ATTRIBUTES && currentOp.name === Op.DELETE && _.isEqual(currentOp.start, followingOp.start) && _.isEqual(currentOp.end, followingOp.end)) {
                operations.splice(idx + 1, 1); // removing the setAttributes operation
            }
            followingOp = currentOp;
        }

        followingOp = null;
        currentOp = null;

        // combining the adjacent delete operations (but only inside paragraphs)
        max = operations.length;
        for (idx = max - 1; idx >= 0; idx--) {
            currentOp = operations[idx];
            if (followingOp && followingOp.name === Op.DELETE && currentOp.name === Op.DELETE) {
                if (currentOp.start.length > 1 && _.isEqual(_.initial(currentOp.start), _.initial(followingOp.start)) && currentOp.target === followingOp.target) {
                    if (isParagraphPosition(this.docModel.getRootNode(currentOp.target), _.initial(currentOp.start))) {
                        if (!currentOp.end) { currentOp.end = _.copy(currentOp.start); }
                        if (!followingOp.end) { followingOp.end = _.copy(followingOp.start); }
                        if (_.isEqual(_.initial(currentOp.end), _.initial(followingOp.end))) {
                            if (_.last(currentOp.end) === _.last(followingOp.start) - 1) {
                                currentOp.end = _.copy(followingOp.end, true); // expanding the range of the current delete operation
                                operations.splice(idx + 1, 1); // removing the follwing delete operation
                            }
                        }
                    }
                }
            }
            followingOp = currentOp;
        }
    }

    /**
     * To increase the stability of the operations it is sometimes necessary to
     * check the correct order of the operations. This is especially necessary,
     * if the all change track types are resolved in one step. This happens after
     * pressing button 'Accept all' or 'Reject all' or after accepting or rejecting
     * a selection range.
     *
     * @param {Object[]} operations
     *  The operation container.
     *
     * @param {Object} [allPositions]
     *  Optionally an array containing objects with the logical positions and target IDs,
     *  that need to be investigated. These objects must have the properties 'pos' and
     *  'targetId'. For comparison of targets it is important, that 'undefined' can be
     *  used.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.cellParagraphs=false]
     *      Whether the check for empty table cells shall be done. Only supported, if
     *      allPositions is an array of operations.
     */
    #checkOperationsIntegrity(operations, allPositions, options) {

        if (_.isArray(allPositions)) {
            if (getBooleanOption(options, 'cellParagraphs', false)) {
                this.#validateTableCells(operations, allPositions);
            } else {
                this.#validateDeleteOrderAtPositions(operations, allPositions);
            }
        } else {
            // Check: setAttributes must be positioned before delete (for same range)
            this.#validateSetAttributesOrder(operations, Op.DELETE);
            // Check: setAttributes must be positioned before merge (for same range)
            this.#validateSetAttributesOrder(operations, Op.PARA_MERGE, 1);
        }

        // additional checks can follow here
    }

    /**
     * Checking the validity of an existing change track selection. In some cases
     * it might happen, that a selection is no longer valid (36299)
     *
     * @param {Object} selection
     *  The change track selection that will be set.
     */
    #checkSelectionValidity(selection) {

        try {
            // check, if the node is still in the DOM
            const pos = getTextLevelPositionInNode(_.first(selection.selectedNodes), this.#rootNode, { suppressWarning: true });

            if (pos) {
                // check, if marginal nodes are still in the DOM (55220)
                if (selection.allChangeTrackNodes) {
                    let foundAll = true;
                    _.each(selection.allChangeTrackNodes, node => {
                        if (DOM.isMarginalNode(node)) {
                            if (foundAll && !getTextLevelPositionInNode(node, this.#rootNode, { suppressWarning: true })) { foundAll = false; }
                        }
                    });
                    if (!foundAll) { this.clearChangeTrackSelection(); }
                }
            } else {
                this.clearChangeTrackSelection();
            }

        } catch {
            // deleting an existing change track selection
            this.clearChangeTrackSelection();
        }
    }

    /**
     * Checking the validity of the collection of change tracked nodes.
     * Currently only complex fields need to be checked (only because of 54373), but
     * in the future this might be the correct place for further tests.
     *
     * @param {Node[]|jQuery} collection
     *  The collection of tracking nodes. This can be an array of nodes or a jQuery
     *  collection.
     */
    #checkTrackingNodeValidity(collection) {

        // whether there is a rangemarker inside the change track collection
        const containsRangeMarkers = _.isObject(_.find(collection, DOM.isRangeMarkerStartNode));
        // all range marker start nodes
        let allRangeMarkerStartNodes = null;

        if (containsRangeMarkers) {

            // Three cases are possible:
            // 1. The complex field is part of the selection and is also change tracked -> compare change track attributes and transfer, if required (54480)
            // 2. The complex field is part of the selection, but not change tracked (loaded from Word, resolving one change track)
            // 3. the complex field is not part of the selection, but its rangemarker start node is (loaded from Word, resolving all change tracks)

            allRangeMarkerStartNodes = _.filter(collection, DOM.isRangeMarkerStartNode);

            _.each(allRangeMarkerStartNodes, startNode => {

                // check, whether:
                //  - the node is a change tracked node of type 'field'
                //  - the following node is a complex field node
                //  - the following complex field node is also change tracked

                // the position of the range marker node in the collection
                let rangeMarkerIndex = 0;
                // the next node in the collection
                let nextNode = null;
                // the next sibling node
                let nextSiblingNode = null;
                // the explicit attributes of type 'changes'
                let explicitChangesAttrs = null;
                // the explicit attributes of type 'changes' of the complex field
                let nextExplicitChangesAttrs = null;

                if (DOM.isChangeTrackNode(startNode) && DOM.getRangeMarkerType(startNode) === 'field') {

                    // searching the current node in the collection (can be jQuery collection or array)
                    rangeMarkerIndex = _.isFunction(collection.index) ? collection.index(startNode) : _.indexOf(collection, startNode);
                    nextNode = _.isFunction(collection.get) ? collection.get(rangeMarkerIndex + 1) : collection[rangeMarkerIndex + 1];

                    if (DOM.isComplexFieldNode(nextNode)) {
                        explicitChangesAttrs = getExplicitAttributes(startNode, 'changes');
                        if (DOM.isChangeTrackNode(nextNode)) {
                            // comparing the change track attributes, transfer to complex field, if required (54480)
                            nextExplicitChangesAttrs = getExplicitAttributes(nextNode, 'changes');
                            if (!_.isEqual(explicitChangesAttrs, nextExplicitChangesAttrs)) {
                                // transferring the change track info from range marker node to the complex field, if required
                                this.docModel.characterStyles.setElementAttributes(nextNode, { changes: explicitChangesAttrs });
                            }
                        } else {
                            // transferring the change track info to the complex field, if required
                            this.docModel.characterStyles.setElementAttributes(nextNode, { changes: explicitChangesAttrs });
                        }
                    } else {
                        nextSiblingNode = startNode.nextSibling; // checking the next sibling in the DOM

                        if (DOM.isComplexFieldNode(nextSiblingNode)) {
                            // transferring the change track info to the complex field
                            explicitChangesAttrs = getExplicitAttributes(startNode, 'changes');
                            this.docModel.characterStyles.setElementAttributes(nextSiblingNode, { changes: explicitChangesAttrs });

                            // inserting the complex field node into the collection behind the range marker start node
                            collection.splice(rangeMarkerIndex + 1, 0, nextSiblingNode);
                        } else {
                            globalLogger.log('Warning: ChangeTracking: The node following the range marker start node is NOT a complex field node! ID: ' + DOM.getRangeMarkerId(startNode));
                        }
                    }
                }

            });
        }
    }

    /**
     * Finding a valid position after the change tracks are resolved. This is especially difficult, if content
     * was removed. Problematic is also, that content might have been removed below different root nodes (55225).
     *
     * @param {Node} savedRootNode
     *  The root node of the first change track node that was handled.
     *
     * @param {Number[]} savedStartPosition
     *  The logical start position of the first change track node that was handled.
     *
     * @param {Number[]} currentStartPos
     *  The logical start position when resolving of change tracks was started.
     *
     * @returns {Number[]}
     *  Returns a logical position that can be used after all change tracks are resolved.
     */
    #getValidPosition(savedRootNode, savedStartPosition, currentStartPos) {

        // the new logical position that can be used after change tracks are resolved
        let newPosition = null;
        // the current root node after the last change track is resolved
        const currentRootNode = this.docModel.getCurrentRootNode();
        // the logical position that is used to find a new valid logical position (dependent from current root node)
        const checkPosition = (getDomNode(currentRootNode) === getDomNode(savedRootNode)) ? savedStartPosition : currentStartPos;

        newPosition = getFirstPositionInParagraph(currentRootNode, checkPosition); // finding position below current root node

        if (!newPosition) {
            // maybe at the specfied position was a table, that no longer exists
            newPosition = getFirstPositionInParagraph(currentRootNode, [_.first(checkPosition)]);
            // paragraph/table do no longer exist -> trying to find last paragraph/table below the current root node
            if (!newPosition) {
                newPosition = this.docModel.getSelection().getLastDocumentPosition(); // using last position below current root node
            }
        }

        return newPosition;
    }

    /**
     * Removes all change track highlight marker.
     * Only used for iOS devices.
     */
    #removeHighlighting() {
        this.#rootNode.find('.' + this.#highlightClassname).removeClass(this.#highlightClassname);
    }

    /**
     * After the event 'document:reloaded' was fired by the model, some cached nodes
     * need to be invalidated.
     */
    #handleDocumentReload() {
        // invalidating cached nodes, after the document was reloaded
        this.#sideBarNode = null;
        this.#pageContentNode = null;
    }

    /**
     * Receiving the author for the list of all document authors for a specified author ID.
     *
     * @returns {Object}
     *  An object from the global authors list containing the information about an author
     *  that is specified by its author ID.
     *  This author contains the properties: 'id', 'name', 'initials', 'providerId' and 'userId'
     */
    #getAuthorFromAuthorId(authorId) {
        return this.docApp.getRegisteredAuthorItemById(authorId);
    }

    /**
     * Getting information from an author object, that is received from the global
     * author's list.
     *
     * @param {Object} author
     *  An author object as specified in the global author's list.
     *  This author object contains the properties: 'id', 'name', 'initials', 'providerId'
     *  and 'userId'.
     *
     * @param {String} property
     *  The requested property. Currently supported and required are only 'userId' and 'author'.
     *
     * @returns {String|Null}
     *  The found value, or null if it could not be found.
     */
    #getAuthorInfo(author, property) {

        let value = null;

        if (!author) { return null; }

        if (property === 'userId') {
            value = (!_.isNull(author.userId) && !_.isUndefined(author.userId)) ? author.userId : author.id;
        } else if (property === 'author') {
            value = author.name;
        }

        return value;
    }

}

// export =================================================================

export default ChangeTrack;
