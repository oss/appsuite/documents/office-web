/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { ChangeTrackInfo } from "@/io.ox/office/editframework/utils/attributeutils";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";
import { TextBaseApplication } from "@/io.ox/office/textframework/app/application";

// class ChangeTrack ==========================================================

export default class ChangeTrack extends ModelObject<TextBaseModel> {

    constructor(docApp: TextBaseApplication, rootNode: NodeOrJQuery, docModel: TextBaseModel);

    allAreInsertedNodesByCurrentAuthor(nodes: JQuery | HTMLElement[]): boolean;

    clearChangeTrackSelection(): void;
    ctSupportsDrawingPosition(): boolean;
    ctSupportsDrawingResizing(): boolean;
    ctSupportsDrawingMove(): boolean;

    getChangeTrackInfo(): ChangeTrackInfo;
    getChangeTrackInfoAuthor(): string;
    getChangeTrackInfoDate(): string;
    getChangeTrackInfoFromNode(node: NodeOrJQuery, info: string, type: string): number | string | null;
    getRelevantTrackingType(): string | null;

    isActiveChangeTracking(): boolean;
    isActiveChangeTrackingInDocAttributes(): boolean;
    isChangeTrackableNode(node: NodeOrJQuery): boolean;
    isChangeTrackSupported(): boolean;
    isInsertNodeByCurrentAuthor(node: NodeOrJQuery): boolean;
    isSideBarHandlerActive(): boolean;

    nodeHasChangeTrackAuthor(node: NodeOrJQuery, type: string, author: string): boolean;
    nodeHasSpecifiedChangeTrackType(node: NodeOrJQuery, type: string): boolean;

    resolveChangeTracking(nodes: JQuery | HTMLElement[], options?: { type?: string; accepted?: boolean; all?: boolean; refreshNodes?: boolean; reuseSameNode?: boolean }): JPromise;

    setActiveChangeTracking(state: boolean): boolean;
    setChangeTrackSupported(state: boolean): void;
    setHighlightToCurrentChangeTrack(): void;
    setSideBarHandlerActive(state: boolean): void;
    showChangeTrackGroup(): void;
    updateChangeTrackAttributes(node: JQuery, attrs: Dict): void;
    updateChangeTrackInfoDate(): void;
}
