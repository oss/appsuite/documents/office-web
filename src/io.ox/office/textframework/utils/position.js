/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import { containsNode, parseIntAttribute, parseCssLength } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { BREAK, caretRangeFromPoint, findDescendantNode, getArrayOption, getBooleanOption,
    getDomNode, getIntegerOption, getNumberOption, getObjectOption, getStringOption,
    indexOfValuesInsideTextSpan, intervalOverlapsInterval, iterateDescendantNodes,
    iterateSelectedDescendantNodes } from '@/io.ox/office/textframework/utils/textutils';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as DrawingFrame from '@/io.ox/office/drawinglayer/view/drawingframe';

// constants --------------------------------------------------------------

// a full list of word boundary characters
var WORDBOUNDENDCHARS = ['\xa0', ' ', '.', ':', '?', '!', ':', ',', ';', '%', '\xa7', '$', '+', '-', '(', ')', '=', '"', '#', '*', '~', '\'', '[', ']', '{', '}', '&', '/', '\\', '@', '\u20ac'];

// static methods ---------------------------------------------------------

/**
 * Creates a clone of the passed logical position and appends the specified
 * index. The passed array object will not be changed.
 *
 * @param {Array<Number>} position
 *  The initial logical position.
 *
 * @param {Number} [index=0]
 *  The value that will be appended to the new position.
 *
 * @returns {Array<Number>}
 *  A clone of the passed logical position, with the specified start value
 *  appended.
 */
export function appendNewIndex(position, index) {
    position = _.clone(position);
    position.push(_.isNumber(index) ? index : 0);
    return position;
}

/**
 * Creates a clone of the passed logical position and increases the last
 * element of the array by the specified value. The passed array object
 * will not be changed.
 *
 * @param {Array<Number>} position
 *  The initial logical position.
 *
 * @param {Number} [increment=1]
 *  The value that will be added to the last element of the position.
 *
 * @returns {Array<Number>}
 *  A clone of the passed logical position, with the last element
 *  increased.
 */
export function increaseLastIndex(position, increment) {
    position = _.clone(position);
    position[position.length - 1] += (_.isNumber(increment) ? increment : 1);
    return position;
}

/**
 * Creates a clone of the passed logical position and decreases the last
 * element of the array by the specified value but not lower than 0.
 * The passed array object will not be changed.
 *
 * @param {Array<Number>} position
 *  The initial logical position.
 *
 * @param {Number} [decrement=1]
 *  The value that will be subtracted from the last element of the position.
 *
 * @returns {Array<Number>}
 *  A clone of the passed logical position, with the last element
 *  increased.
 */
export function decreaseLastIndex(position, decrement) {
    position = _.clone(position);
    position[position.length - 1] -= (_.isNumber(decrement) ? decrement : 1);
    if (position[position.length - 1] < 0) {
        position[position.length - 1] = 0;
    }
    return position;
}

/**
 * This function calculates the logical position from dom positions.
 * Receiving a dom position consisting of a dom node and optionally
 * an offset, it calculates the logical position (oxoPosition) that
 * is an array of integer values.
 * The offset is only used for text nodes, where the logical position
 * can be shifted with the offset.
 * So contrary to the function getTextLevelOxoPosition, this function
 * does not always return a text level oxo position (a full logical
 * position), but it can also return positions of elements like p, table,
 * tr, th and td.
 *
 * @param {Node|jQuery} rootNode
 *  The dom root node, that is the frame for the complete
 *  search and calculation process. No dom position outside of this
 *  rootNode can be calculated.
 *  If this object is a jQuery collection, uses the first node it contains.
 *
 * @param {Node|jQuery} node
 *  The dom node, whose logical position will be calculated. If this object
 *  is a jQuery collection, uses the first node it contains.
 *
 * @param {Number} [offset]
 *  An additional offset, that can be used to modify the last position
 *  inside the logical position array. This is typically only useful
 *  for text nodes.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.suppressWarning=false]
 *      If set to true, the error messages in this function are not
 *      written into the console. This is useful, if the correctness
 *      of a position is checked.
 *
 * @returns {Array<Number>|undefined}
 *  The logical position corresponding to the specified node and offset.
 *  Or undefined, it the logical position cannot be determined.
 */
export function getOxoPosition(rootNode, node, offset, options) {

    // Starting to calculate the logical position
    var // the logical position specified by rootNode, node and offset
        oxoPosition = [],
        // TODO: Move validDomElements to DOM module
        validDomElements,
        // A collector for valid table row elements
        validTableElements,
        // a drawing place holder node (switching from drawing layer to page content)
        placeHolderNode = null;

    // convert to DOM node object
    rootNode = getDomNode(rootNode);
    node = getDomNode(node);

    // Checking offset for text nodes
    if ((node.nodeType === 3) && !_.isNumber(offset)) {
        if (!getBooleanOption(options, 'suppressWarning', false)) { globalLogger.error('getOxoPosition(): Invalid start position: text node without offset'); }
        return;
    }

    if (offset < 0) {
        if (!getBooleanOption(options, 'suppressWarning', false)) { globalLogger.error('getOxoPosition(): Invalid DOM position. Offset < 0 : ' + offset + ' . Node: ' + node.nodeName + ',' + node.nodeType); }
        return;
    }

    // Check, if the selected node is a descendant of the rootNode
    if (!containsNode(rootNode, node)) {
        if (!getBooleanOption(options, 'suppressWarning', false)) { globalLogger.error('getOxoPosition(): Invalid DOM position. It is not part of the editor DIV: ! Offset : ' + offset + ' . Node: ' + node.nodeName + ',' + node.nodeType); }
        return;
    }

    // checking offset, must be a number
    offset = _.isNumber(offset) ? offset : 0;

    // forcing to use span instead of text node directly
    if (node.nodeType === 3) { node = node.parentNode; }

    if (DOM.getPageContentNode(rootNode).length) {
        validDomElements = DOM.getPageContentNode(rootNode).children(DOM.CONTENT_NODE_SELECTOR);
    } else if (DOM.getMarginalContentNode(rootNode).length) {
        validDomElements = DOM.getMarginalContentNode(rootNode).children(DOM.CONTENT_NODE_SELECTOR);
    } else {
        validDomElements = $();
    }

    // drawings in the drawing layer node can be substituted with their place holder nodes
    if (DOM.isDrawingLayerNode(node.parentNode)) { node = DOM.getDrawingPlaceHolderNode(node); }

    // resetting offset, if the direct parent is not a paragraph (sub elements might be selected by the browser)
    if (!DOM.isParagraphNode(node.parentNode)) { offset = 0; }

    // evaluate all nodes from specified node until rootNode
    for (; node && (node !== rootNode); node = node.parentNode) {

        if (placeHolderNode) {
            node = placeHolderNode;
            placeHolderNode = null;
        }

        // children of paragraphs need a character position
        if (node.parentNode && DOM.isParagraphNode(node.parentNode)) {
            offset += getStartOfParagraphChildNode(node);
            oxoPosition.unshift(offset);
            offset = 0;
        } else if (DOM.isContentNode(node) || DOM.isTableCellOrRowNode(node)) {

            if (DOM.isTableCellOrRowNode(node) || DOM.isCellContentNode($(node).parent()) || DrawingFrame.isTextFrameNode($(node).parent()) || DOM.isSlideNode(node)) {
                if (DOM.isTopLevelRowNode(node)) {
                    // with introduction of tr containing page break, we now have them as not valid nodes, so we have to skip them from indexing
                    validTableElements = $(node).parents('table').find('> tbody > tr').not('.pb-row');
                    oxoPosition.unshift(validTableElements.index($(node)));  // zero based
                } else if (DOM.isTableNodeInTableDrawing(node)) {
                    // ignoring table nodes in drawings of type 'table'
                } else {
                    oxoPosition.unshift($(node).index());  // zero based
                }
            } else if (DOM.isTableNodeInTableDrawing(node)) {
                // ignoring table nodes in drawings of type 'table'
            } else {
                oxoPosition.unshift(validDomElements.index($(node)));  // zero based position of node among first level DOM siblings - div.p and tables
            }

        } else if (DrawingFrame.isGroupContentNode(node.parentNode)) {
            // Special handling for drawing frames inside groups
            oxoPosition.unshift($(node).index());  // zero based
        } else if (DOM.isDrawingLayerNode(node.parentNode)) {
            // Special handling for drawings in the drawing layer -> switching to place holder node
            placeHolderNode = DOM.getDrawingPlaceHolderNode(node);
        }

    }

    // TODO: Old code, to be removed asap
    /*
        if (evaluateCharacterPosition) {
        // move up until we are a child node of a paragraph
        while ((!DOM.isParagraphNode(node.parentNode)) && (!DOM.isPageNode(node.parentNode))) {
        offset = 0;
        node = node.parentNode;
        if (DOM.isPageNode(node.parentNode) && (!DOM.isPageContentNode(node))) {   // -> to be removed soon (should never happen)
        node = node.previousSibling.lastChild.lastChild;
        offset = getLastTextNodePositionInParagraph(node.parentNode) - getStartOfParagraphChildNode(node);
        }
        }
        offset += getStartOfParagraphChildNode(node);
        characterPositionEvaluated = true;
        }
        */

    return oxoPosition;
}

/**
 * This function calculates the logical position from DOM positions.
 * Receiving a DOM position consisting of a DOM node and an offset, it
 * calculates the logical position that is an array of
 * integer values. The calculated logical position is always a valid text
 * level position. This means, that even if the DOM position is a DIV, a TR
 * or a similar node type, the logical position describes always the
 * position of a text node or image or field.
 *
 * @param {DOM.Point} domposition
 *  The DOM position, consisting of dom node and offset, whose logical
 *  position will be calculated.
 *
 * @param {jQuery} maindiv
 *  The jQuery object of a DIV node, that is the frame for the complete
 *  search and calculation process. No dom position outside of this
 *  maindiv can be calculated.
 *
 * @param {Boolean} isEndPoint
 *  The information, if the specified DOM position is the end point
 *  of a range. This is important for some calculations, where the
 *  DOM node is a row inside a table.
 *
 * @param {Boolean} hasRange
 *  The information, if the specified DOM position is the start or end point
 *  of a range or a cursor selection. This is important for some calculations,
 *   where the DOM node is inside a list.
 *
 * @returns {Opt<Position>}
 *  The calculated logical position.
 */
export function getTextLevelOxoPosition(domposition, maindiv, isEndPoint, hasRange) {

    var node = domposition.node,
        offset = domposition.offset,
        next = null, previous = null;

    isEndPoint = !!isEndPoint;

    // check input values
    if (!node) {
        globalLogger.error('getTextLevelOxoPosition(): Invalid DOM position. Node not defined');
        return undefined;
    }

    // Handling all selections, in which the node is below paragraph level
    if (DOM.isEmptySpan(node) || DOM.isTextNodeInTextComponent(node) || DOM.isTextFrameTemplateTextNode(node)) {
        // empty spans must not have an offset (special case for spans behind tabs)
        // or text nodes of fields/tabs are embedded in <span> elements in the <div>
        // or also text spans with template texts
        offset = 0;
    } else if (DOM.isExceededSizeTableNode($(node).closest(DOM.TABLE_NODE_SELECTOR))) {
        // handling all positions inside an exceeded size table
        next = $(node).closest(DOM.TABLE_NODE_SELECTOR).next().get(0);
        while (next && DOM.isExceededSizeTableNode(next)) { next = next.nextSibling; }
        node = DOM.isTableNode(next) ? $(next).find('div.p')[0] : next;
        offset = 0;
    }

    // handling for Edge browser, so that last positions in paragraphs and documents can be set (46647)
    if (_.browser.Edge && DOM.isTextSpan(node)) {
        if (hasRange) { // selection handling
            if (isEndPoint && offset === 1 && ($(node).next().length === 0 || DOM.isPageBreakNode($(node).next()) || DOM.isRangeMarkerNode($(node).next()))) {
                // last position in paragraph or before page break node
                node = node.firstChild;
                offset = node.nodeValue.length;
            } else if (!isEndPoint && offset === 0 && $(node).prev().length === 0 && $(node.parentNode).next().length === 0) { // last position in document
                node = node.parentNode.lastChild.firstChild; // the text node in the last text span in the paragraph (document)
                offset = node.nodeValue.length;
            }
        } else {  // cursor handling
            if (!isEndPoint && offset === 1 && ($(node).next().length === 0 || DOM.isPageBreakNode($(node).next()))) { // last position in paragraph or before page break
                node = node.firstChild;
                offset = node.nodeValue.length;
            }
        }
    }

    // handling page content or cell content nodes directly
    if (DOM.isPageContentNode(node) || DOM.isCellContentNode(node)) {
        if (DOM.isExceededSizeTableNode(node.childNodes[offset])) {
            next = $(node.childNodes[offset]).next().get(0);
            while (next && DOM.isExceededSizeTableNode(next)) { next = next.nextSibling; }
            if (next) {
                node = DOM.isTableNode(next) ? $(next).find('div.p')[0] : next;
                offset = 0;
            }
        } else if ((isEndPoint) && (DOM.isExceededSizeTableNode($(node.childNodes[offset]).prev()))) {
            node = node.childNodes[offset];
            if (DOM.isTableNode(node)) { node = $(node).find('div.p').get(0); }  // Fix for 27387
            offset = 0;
        } else if (isEndPoint && hasRange && (offset > 0) && DOM.isTableNode(node.childNodes[offset]) && _.browser.WebKit) {
            node = node.childNodes[offset - 1];  // Fixing triple click on Chrome in paragraph before table (46707)
            offset = 1;
        } else if (isEndPoint) {
            if (offset >= node.childNodes.length) { offset = node.childNodes.length - 1; }  // Fix for 29237 (using last available node)
            node = node.childNodes[offset];  // Fix for 28375
            offset = 0;
        } else if ((!isEndPoint) && (DOM.isTableNode($(node.childNodes[offset]).prev()))) {  // Fix for 28590
            previous = $(node.childNodes[offset]).prev().get(0);
            while (previous && DOM.isExceededSizeTableNode(previous)) { previous = previous.previousSibling; }
            if (previous) {
                node = DOM.isTableNode(previous) ? $(previous).find('div.p')[0] : previous;
                offset = 0;
            }
        } else if ((!isEndPoint) && DOM.isTableNode(node.childNodes[offset])) {  // Fix for 30878 and 32055 (setting cursor behind table)
            node = node.childNodes[offset];
            node = DOM.isParagraphNode(node.nextSibling) ? node.nextSibling : $(node).find('div.p')[0];
            offset = 0;
        } else {
            if (DOM.isPageContentNode(node)) {
                node = $(node).closest(DOM.PAGE_NODE_SELECTOR)[0];
            }
        }
    }

    // it is even possible to receive TR elements of tables with exceeded size (task 31625)
    if (DOM.isTableRowNode(node) && DOM.isExceededSizeTableNode(node.parentNode.parentNode)) {
        next = $(node.parentNode.parentNode).next()[0];
        while (next && DOM.isExceededSizeTableNode(next)) { next = next.nextSibling; }
        if (next) {
            node = DOM.isTableNode(next) ? $(next).find('div.p')[0] : next;
        }
        offset = 0;
    }

    // handling global page nodes
    if (DOM.isPageNode(node)) {
        if (offset > 1) {   // ignoring everything behind 'div.pagecontent'
            isEndPoint = true;
            node = DOM.getPageContentNode(node).children().last()[0];
            offset = node.childNodes.length;  // 40905, setting to end of document
        }
    } else if (_.browser.Firefox) { // Fix for FF (40905)
        if (isEndPoint && $(node).is(DOM.FOOTER_SELECTOR + '.inactive-selection')) {
            node = DOM.getPageContentNode(maindiv).children().last()[0];
            offset = node.childNodes.length;
        } else if (!isEndPoint && $(node).is(DOM.HEADER_SELECTOR + '.inactive-selection')) {
            node = DOM.getPageContentNode(maindiv).children().first()[0];
            offset = 0;
        } else if (DOM.isListLabelNode(node)) { // DOCS-2839
            node = node.nextSibling;
            offset = 0;
        }
    }

    // handling drawing nodes
    if (DOM.isDrawingFrame(node.parentNode)) {
        // inside the contents of a drawing
        node = node.parentNode;
    }

    if (DOM.isDrawingFrame(node)) {
        offset = 0;
    }

    // handling resize nodes inside tables (triple click in Chrome)
    if (DOM.isResizeNode(node)) {
        // using table cell content node instead of resize nodes
        node = getDomNode(DOM.getCellContentNode($(node).closest('td')[0]));
    }

    // handling selection nodes inside text frames (triple click in Chrome (48065)) or mouse up on drawing resizers
    if (isEndPoint && offset === 0 && $(node).is('div') && node.className === '' && (node.parentNode.className === 'borders' || node.parentNode.className === 'resizers')) {
        node = $(node).closest('div.drawing')[0]; // setting selection to drawing
        offset = 1;
    }

    // handling invalid cursor position directly behind complex fields (happens after cursor-up, cursor-down or pos1, 66382)
    if (offset === 0 && !hasRange && node.nodeType === 3 && DOM.isInsideComplexFieldRange(node.parentNode) && node.parentNode.previousSibling) {
        previous = node.parentNode.previousSibling;
        if (DOM.isComplexFieldNode(previous) && previous.previousSibling) {
            previous = previous.previousSibling;
            if (DOM.isRangeMarkerNode(previous) && previous.previousSibling) {
                previous = previous.previousSibling;
                if (DOM.isTextSpan(previous)) {
                    node = previous.firstChild;
                    offset = node.nodeValue.length;
                }
            }
        }
    }

    // Sometimes (double click in FireFox) a complete paragraph is selected with DIV + Offset 3 and DIV + Offset 4.
    // These DIVs need to be converted to the correct paragraph first.
    // Also cells in columns have to be converted at this point.

    if (DOM.isParagraphNode(node) || DOM.isPageNode(node) || $(node).is('tr, td') || DOM.isCellContentNode(node)) {

        var newNode = getTextNodeFromCurrentNode(node, offset, isEndPoint, hasRange);

        if (newNode) {
            node = newNode.node;
            offset = newNode.offset;
        } else {
            globalLogger.error('getTextLevelOxoPosition(): Failed to determine text node from node: ' + node.nodeName + ' with offset: ' + offset);
            return;
        }

    }

    // Calculating the logical position for the specified text node, span, or image
    return getOxoPosition(maindiv, node, offset);
}

/**
 * This function calculates a logical selection consisting of two
 * logical positions for a specified dom position. Using the boolean
 * parameter useRangeNode it is possible to switch between the
 * selection in which the endPosition describes the final character
 * of the selection (useRangeMode === false) and that selection, in
 * which the end position is a position behind the last character of
 * the selection.
 * For all nodes, that describe dom positions that are not on text level,
 * like p, table, td, ... the startposition and endposition are equal.
 *
 * @param {jQuery} maindiv
 *  The jQuery object of a DIV node, that is the frame for the complete
 *  search and calculation process. No dom position outside of this
 *  maindiv can be calculated.
 *
 * @param {Node} node
 *  The dom node, whose logical position will be calculated.
 *
 * @param {Boolean} useRangeMode
 *  A boolean value that can be used to switch the endposition between
 *  a 'range' mode (end position behind the final character) and a
 *  'position' mode (end position points to the final character).
 *
 * @returns {Object|Null}
 *  An object containing the attribute 'start' with the logical start
 *  position, and an attribute 'end' with the logical end position of the
 *  passed node.
 *  If the position cannot be specified, null is returned.
 */
export function getPositionRangeForNode(maindiv, node, useRangeMode) {

    var // logical start position of the passed node
        startPosition = getOxoPosition(maindiv, node, 0),
        // logical end position of the passed node
        endPosition = _.clone(startPosition),
        // logical length of the node (using 1 for template texts because of offset, otherwise 0 would be better)
        length = DOM.isPortionSpan(node) ? (DOM.isTextFrameTemplateTextSpan(node) ? 1 : getDomNode(node).firstChild.nodeValue.length) : 1,
        // open/close range offset
        offset = ((useRangeMode === undefined) || (useRangeMode !== true)) ? 1 : 0;

    if (endPosition) { endPosition[endPosition.length - 1] += (length - offset); }
    return (startPosition && endPosition) ? { start: startPosition, end: endPosition } : null;
}

/**
 * The central function to calculate a dom position from a logical
 * position. Receiving a logical position (oxoPosition) together with
 * the start node, the current dom node and the corresponding offset
 * are determined. This information is stored in the DOM.Point object.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} oxoPosition
 *  The logical position.
 *
 * @param {Boolean} [forcePositionCounting=false]
 *  A boolean value that can be used to switch between range counting (false)
 *  and direct position counting (true). If this value is false, a text span
 *  will be returned. If this value is true the node at the specified position
 *  is returned. This can be for example a drawing, comment, ...
 *
 * @returns {DOM.Point|undefined}
 *  The calculated dom position consisting of dom node and offset.
 *  Offset is only set for text nodes, otherwise it is undefined.
 *  If the position cannot be determined, 'undefined' is returned
 */
export function getDOMPosition(startnode, oxoPosition, forcePositionCounting) {

    var node = startnode,
        offset = null,
        parent = null,
        returnObj = null;

    if (!_.isArray(oxoPosition)) {
        globalLogger.error('getDOMPosition(): invalid/missing logical position');
        return;
    }

    for (var i = 0; i < oxoPosition.length; i++) {

        returnObj = getNextChildNode(node, oxoPosition[i]);

        if (returnObj && returnObj.node) {
            node = returnObj.node;
        } else {
            return;
        }
    }

    // child node of a paragraph: resolve element to DOM text node
    if (!forcePositionCounting && returnObj && returnObj.node) {
        parent = returnObj.node.parentNode;

        if (DOM.isParagraphNode(parent)) {
            returnObj = getTextSpanFromNode(returnObj);
        } else if (DOM.isDrawingLayerNode(parent)) {
            returnObj.node = DOM.getDrawingPlaceHolderNode(returnObj.node);
            returnObj = getTextSpanFromNode(returnObj);
        }
    }

    if (returnObj && returnObj.node) {
        node = returnObj.node;
    }

    if (returnObj && _.isNumber(returnObj.offset)) {
        offset = returnObj.offset;
    }

    return new DOM.Point(node, offset);
}

/**
 * Validating the existence of an element at a specified position and root node.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The specified logical position.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *  @param {Boolean} [options.allowFinalPosition=false]
 *      Whether the position that is identical with the offset is valid or not. Default
 *      is that this is not a valid position.
 *
 * @returns {Boolean}
 *  Whether there is an element at the specified logical position.
 */
export function isValidElementPosition(startnode, position, options) {

    // the dom point for the specified position and root node
    var domPos = (position.length > 0) ? getDOMPosition(startnode, position, true) : null;
    // whether this is a valid position with an existing element
    var isValid = false;
    // whether the position behind the 'offset' is a valid position
    var allowFinalPosition = getBooleanOption(options, 'allowFinalPosition', false);

    if (domPos && domPos.node) {

        isValid = true;

        if (DOM.isTextSpan(domPos.node) && (domPos.offset >= domPos.node.firstChild.nodeValue.length)) {
            if (!(allowFinalPosition && domPos.offset === domPos.node.firstChild.nodeValue.length)) { // using 'allowFinalPosition' the 'offset' is an allowed position
                isValid = false;
            }
        }
    }

    return isValid;
}

/**
 * Validating the existence of elements at the specified start and end positions for a given root node.
 * If the end position is not specified, only the start position will be evaluated.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical positions.
 *
 * @param {Array<Number>} startPosition
 *  The specified logical start position.
 *
 * @param {Array<Number>} [endPosition]
 *  The specified logical end position. This end position can be omitted, so that only the start position
 *  is evaluated.
 *
 * @param {Object} [options]
 *  Additional options supported by isValidElementPosition
 *
 * @returns {Boolean}
 *  Whether the specified range contains elements at the specified logical positions. This is especially
 *  important for attribute operations, where at the logical positions elements must exist.
 */
export function isValidElementRange(startnode, startPosition, endPosition, options) {

    // whether the start position is valid
    var isValidStart = isValidElementPosition(startnode, startPosition, options);
    // whether the end position is valid (if not specified, this is set to true, because start position will be used
    var isValidEnd = endPosition ? isValidElementPosition(startnode, endPosition, options) : true;

    return isValidStart && isValidEnd;
}

/**
 * Tries to get a DOM element from the specified logical position. The
 * passed position must match the passed selector. Otherwise, a warning
 * will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target element.
 *
 * @param {String|Function|Node|jQuery} selector
 *  A jQuery selector that will be used to match the element at the
 *  specified position. The selector will be passed to the jQuery method
 *  jQuery.is() for each node. If this selector is a function, it will be
 *  called with the current DOM node bound to the symbol 'this'. See the
 *  jQuery API documentation at http://api.jquery.com/is for details.
 *
 * @returns {HTMLElement|Null}
 *  The DOM element at the passed logical position, if it matches the
 *  passed selector, otherwise null.
 */
export function getSelectedElement(startnode, position, selector) {

    var // the DOM node located at the passed position
        domPos = (position.length > 0) ? getDOMPosition(startnode, position, true) : null;

    if (domPos && domPos.node && $(domPos.node).is(selector)) {
        return domPos.node;
    }

    globalLogger.warn('getSelectedElement(): expected element not found at position ' + JSON.stringify(position) + '.');
    return null;
}

/**
 * Tries to get a DOM paragraph element from the specified logical
 * position. The passed position must point to a paragraph element.
 * Otherwise, a warning will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target paragraph element.
 *
 * @returns {HTMLElement|Null}
 *  The DOM paragraph element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getParagraphElement(startnode, position) {
    return getSelectedElement(startnode, position, DOM.PARAGRAPH_NODE_SELECTOR);
}

/**
 * Tries to get a DOM table element from the specified logical position.
 * The passed position must point to a table element. Otherwise, a warning
 * will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target table element.
 *
 * @returns {HTMLTableElement|Null}
 *  The DOM table element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getTableElement(startnode, position) {
    return getSelectedElement(startnode, position, DOM.TABLE_NODE_SELECTOR);
}

/**
 * Tries to get a DOM drawing element from the specified logical position.
 * The passed position must point to a drawing element. Otherwise, a warning
 * will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target drawing element.
 *
 * @returns {HTMLTableElement|Null}
 *  The DOM drawing element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getDrawingElement(startnode, position) {
    return getSelectedElement(startnode, position, DrawingFrame.NODE_SELECTOR);
}

/**
 * Tries to get a DOM table row element from the specified logical position.
 * The passed position must point to a table row element. Otherwise, a warning
 * will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target table row element.
 *
 * @returns {HTMLTableElement|Null}
 *  The DOM table row element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getSelectedTableRowElement(startnode, position) {
    return getSelectedElement(startnode, position, DOM.TABLE_ROWNODE_SELECTOR);
}

/**
 * Tries to get a DOM table cell element from the specified logical position.
 * The passed position must point to a table cell element. Otherwise, a warning
 * will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target table cell element.
 *
 * @returns {HTMLTableElement|Null}
 *  The DOM table cell element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getSelectedTableCellElement(startnode, position) {
    return getSelectedElement(startnode, position, DOM.TABLE_CELLNODE_SELECTOR);
}

/**
 * Tries to get a DOM table or paragraph element from the specified logical position.
 * The passed position must point to a table or paragraph element. Otherwise, a warning
 * will be printed to the debug console.
 * Not calling getSelectedElement, because this can fail (during loading the
 * document) and the calling function has to check, whether the return value is null.
 * A warning should therefore not always be printed to the console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target table or paragraph element.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *  @param {Boolean} [options.allowDrawingGroup=false]
 *      Whether drawing frames of type 'group' are also allowed as content node.
 *
 * @returns {HTMLTableElement|HTMLParagraphElement|Null}
 *  The DOM table or paragraph element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getContentNodeElement(startnode, position, options) {

    var // the DOM node located at the passed position
        domPos = getDOMPosition(startnode, position, true),
        // whether drawing frames of type 'group' are also allowed as content node
        allowDrawingGroup = getBooleanOption(options, 'allowDrawingGroup', false),
        // whether it is allowed to return an implicit paragraph
        allowImplicitParagraphs = getBooleanOption(options, 'allowImplicitParagraphs', true);

    if (domPos && domPos.node) {
        if (($(domPos.node).is(DOM.CONTENT_NODE_SELECTOR)) || (allowDrawingGroup && DrawingFrame.isGroupDrawingFrame(domPos.node))) {
            if ((!allowImplicitParagraphs) && DOM.isImplicitParagraphNode(domPos.node)) { return null; }
            return domPos.node;
        }
    }

    return null;
}

/**
 * Tries to get a DOM table row element from the specified logical
 * position. The passed position must point to a table row element.
 * Otherwise, a warning will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target table row element.
 *
 * @returns {HTMLTableRowElement|Null}
 *  The DOM table row element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getTableRowElement(startnode, position) {
    var table = getTableElement(startnode, position.slice(0, -1)),
        tableRow = table && DOM.getTableRows(table).get(position[position.length - 1]);
    if (table && !tableRow) {
        globalLogger.warn('getTableRowElement(): expected element not found at position ' + JSON.stringify(position) + '.');
    }
    return tableRow || null;
}

/**
 * Tries to get a DOM table cell element from the specified logical
 * position. The passed position must point to a table cell element.
 * Otherwise, a warning will be printed to the debug console.
 *
 * @param {jQuery} startnode
 *  The list of top-level content nodes.
 *
 * @param {Array<Number>} position
 *  The logical position of the target table cell element.
 *
 * @returns {HTMLTableCellElement|Null}
 *  The DOM table cell element at the passed logical position, if existing,
 *  otherwise null.
 */
export function getTableCellElement(startnode, position) {
    var tableRow = getTableRowElement(startnode, position.slice(0, -1)),
        tableCell = tableRow && $(tableRow).children('td').get(position[position.length - 1]);
    if (tableRow && !tableCell) {
        globalLogger.warn('getTableCellElement(): expected element not found at position ' + JSON.stringify(position) + '.');
    }
    return tableCell || null;
}

/**
 * Helper function for getTextLevelOxoPosition. If the node is not
 * a text node, this function determines the correct text node, that
 * is used for calculation of the logical position instead of the
 * specified node. This could be a 'div.page', 'div.p', 'table', ... . It is
 * often browser dependent, which node type is used after a double
 * or a triple click.
 *
 * @param {Node} node
 *  The dom node, whose logical position will be calculated.
 *
 * @param {Number} offset
 *  The offset of the dom node, whose logical position will be
 *  calculated.
 *
 * @param {Boolean} isEndPoint
 *  The information, if the specified domposition is the end point
 *  of a range. This is important for some calculations, where the
 *  dom node is a row inside a table.
 *
 * @param {Boolean} hasRange
 *  The information, if the specified DOM position is the start or end point
 *  of a range or a cursor selection. This is important for some calculations,
 *   where the DOM node is inside a list.
 *
 * @returns {Opt<DOM.Point>}
 *  The text node, that will be used in getTextLevelOxoPosition
 *  for the calculation of the logical position.
 */
export function getTextNodeFromCurrentNode(node, offset, isEndPoint, hasRange) {

    var useFirstTextNode = true,  // can be false for final child in a paragraph
        usePreviousCell = false,
        foundValidNode = false,
        localNode = node.childNodes[offset], // offset can be zero for start points but too high for end points
        offsetSave = offset;

    offset = 0;

    if ((DOM.isPageNode(node)) && (offsetSave === 1) && !DOM.isPageContentNode(localNode)) {
        // the local node is an invalid node under special circumstances. At this position
        // it must be page content node and not one of its neighbors (38633).
        // Try using 'usePreviousCell' trick to find valid position.
        usePreviousCell = true;
    }

    if ((isEndPoint) && ($(node).is('tr'))) {
        usePreviousCell = true;
    }

    if ((isEndPoint) && (localNode) && DOM.isDrawingFrame(localNode.previousSibling)) {
        usePreviousCell = true;
    }

    if (!localNode || usePreviousCell) {
        localNode = node.childNodes[offsetSave - 1];
        useFirstTextNode = false;
    }

    // special handling for ranges ending directly behind list labels -> using previous text span instead (FireFox)
    if ((isEndPoint) && (hasRange) && (offsetSave <= 1) && (DOM.isParagraphNode(node)) && (DOM.isListLabelNode(node.childNodes[0])) && (node.previousSibling)) {
        localNode = node.previousSibling;  // using previous paragraph
        useFirstTextNode = false;
    }

    // special handling for list labels, using following text span instead
    // special handling for dummy text node, use last preceding text node instead
    if (DOM.isListLabelNode(localNode)) {
        localNode = localNode.nextSibling;
    }

    // special handling for dummy text node, use last preceding text node instead
    if (DOM.isDummyTextNode(localNode)) {
        localNode = localNode.previousSibling;
        useFirstTextNode = false;
    }

    // setting some properties for drawing nodes or text component nodes like fields
    if (DOM.isDrawingFrame(localNode) || DOM.isTextComponentNode(localNode) || DOM.isTextFrameTemplateTextNode(localNode)) {
        foundValidNode = true;  // drawing nodes are valid
        offset = 0;
    }

    // checking, if a valid node was already found
    if ((localNode) && (localNode.nodeType === 3)) {
        foundValidNode = true;  // text nodes are valid
    }

    // if page break element which splits paragraph is fetched
    if ($(localNode).hasClass('page-break')) {
        localNode = localNode.previousSibling;
        useFirstTextNode = false;
    }

    var foundNode = localNode;

    if (!foundValidNode) {
        // find the first or last text node contained in the element
        if (localNode && (localNode.nodeType !== 3)) {
            if (DOM.isPortionSpan(localNode)) {
                foundNode = findDescendantNode(localNode, function () { return DOM.isTextNodeInPortionSpan(this); }, { reverse: !useFirstTextNode });
                if (foundNode) {
                    localNode = foundNode;
                }
            } else {
                foundNode = findDescendantNode(localNode, function () { return DOM.isPortionSpan(this); }, { reverse: !useFirstTextNode });
                if (foundNode) {
                    localNode = foundNode = foundNode.firstChild;
                }
            }
        }
    }

    if (!foundNode) {
        var nodeName = localNode ? localNode.nodeName : '';
        globalLogger.error('getTextNodeFromCurrentNode(): Failed to determine text node from current node! (useFirstTextNode: ' + useFirstTextNode + ' : ' + nodeName + ')');
        return undefined;
    }

    // getting offset for text nodes
    if (localNode.nodeType === 3) {
        offset = useFirstTextNode ? 0 : foundNode.nodeValue.length;
    }

    return new DOM.Point(foundNode, offset);
}

/**
 * Returns the following node and offset corresponding to the next
 * logical position. With a node and the next position index
 * the following node and in the case of a text node the offset
 * are calculated.This function does not take care of
 * ranges but only of direct positions. So [6,0] points to the first
 * character or drawing or field in the seventh paragraph, not to a
 * cursor position before.
 *
 * @param {Node} node
 *  The top level node, whose child is searched.
 *
 * @param {Number} pos
 *  The one integer number, that determines the child according to the
 *  parent position.
 *
 * @returns {DOM.Point}
 *  The child node and an offset. Offset is only set for text nodes,
 *  otherwise it is undefined.
 */
export function getNextChildNode(node, pos) {

    var childNode = null,
        offset = 0,
        lastTextSpanInfo = null;

    node = getDomNode(node);

    if (DOM.isTableNode(node)) {
        childNode = DOM.getTableRows(node).get(pos);
    } else if (DOM.isSlideContainerNode(node)) {
        childNode = node.childNodes[pos];
    } else if ($(node).is('tr')) {
        // Adding all colspans, to get the selected cell
        // iterateRowChildNodes(node, function (cellNode, nodeStart, nodeColSpan) {
        //    // check if passed position points inside the current cell
        //     if (nodeStart + nodeColSpan > pos) {
        //        childNode = cellNode;
        //        return BREAK;
        //    }
        // });
        childNode = $(node).children('td').get(pos);  // this is a table cell
    } else if (DOM.isPageNode(node)) {
        childNode = DOM.allowedChildNodes(DOM.getPageContentNode(node)[0].childNodes)[pos]; // modified to exclude pagebreaks and helper divs from count
    } else if ($(node).is('td') && DOM.getCellContentNode(node)[0]) {  // node is a table cell, but not inside an exceeded size table
        childNode = DOM.getCellContentNode(node)[0].childNodes[pos];
    } else if (DrawingFrame.isGroupDrawingFrame(node)) {
        childNode = DrawingFrame.getGroupDrawingChildren(node, pos);
    } else if (DrawingFrame.isTableDrawingFrame(node)) {
        childNode = DOM.getTableRows(node).get(pos);
    } else if (DrawingFrame.isTextFrameShapeDrawingFrame(node)) {
        childNode = DrawingFrame.getTextFrameNode(node)[0].childNodes[pos];
    } else if (DOM.isParagraphNode(node)) {

        iterateParagraphChildNodes(node, function (_node, nodeStart, nodeLength) {

            var // offset inside the current child node
                nodeOffset = pos - nodeStart;

            // check if passed position points inside the current node
            if ((nodeOffset >= 0) && (nodeOffset < nodeLength)) {
                childNode = _node;
                offset = nodeOffset;

                // Replacing the place holder drawing with its 'real' drawing in the drawing layer
                if (DOM.isDrawingPlaceHolderNode(childNode)) { childNode = DOM.getDrawingPlaceHolderNode(childNode); }

                return BREAK;
            }

            // store temporary text span info, will be used to find the last
            // text span for a position pointing behind the last character
            if (DOM.isTextSpan(_node)) {
                lastTextSpanInfo = { node: _node, start: nodeStart, length: nodeLength };
            }
        }, undefined, { allNodes: true });

        // no node found that represents the passed position, try to match position after last character
        if (!childNode && lastTextSpanInfo && (lastTextSpanInfo.start + lastTextSpanInfo.length === pos)) {
            childNode = lastTextSpanInfo.node;
            offset = lastTextSpanInfo.length;
        }
    } else if (DOM.isHeaderOrFooter(node)) {
        childNode = DOM.getMarginalContentNode(node);
        childNode = DOM.allowedChildNodes(childNode.children())[pos];
    }

    return new DOM.Point(childNode, offset);
}

/**
 * Determining a text node for a DOM position, if this is required.
 * For drawing nodes, drawing nodes or text span nodes, it is sometimes
 * necessary to get a valid text node.
 *
 * @param {DOM.Point} domPoint
 *  The child node and the offset, which might be modified to get
 *  a valid text node.
 *
 * @returns {Opt<DOM.Point>}
 *  The child node and an offset.
 */
export function getTextSpanFromNode(domPoint) {

    var // a text span corresponding to a component node
        span = null;

    function createTextPointForSpan(span, offset) {
        return new DOM.Point(span.firstChild, _.isNumber(offset) ? offset : span.firstChild.nodeValue.length);
    }

    if (!domPoint || !domPoint.node) {
        globalLogger.warn('getTextSpanFromNode(): Parameter domPoint not set.');
        return undefined;
    }

    // use preceding text node for all components (fields, tabs, drawings)
    if (DOM.isInlineComponentNode(domPoint.node) || DOM.isFloatingNode(domPoint.node) || DOM.isDrawingFrame(domPoint.node)) {

        // go to previous inline node (must exist, must be a text span), skip floating helper nodes
        span = domPoint.node.previousSibling;

        // there might be the following nodes without text span in between
        while (DOM.isOffsetNode(span) || DOM.isPageBreakNode(span) || DOM.isCommentPlaceHolderNode(span) || DOM.isRangeMarkerNode(span) || DOM.isSpecialField(span)) {
            span = span.previousSibling;
        }

        if (span) {
            return createTextPointForSpan(span);
        }

        globalLogger.error('getTextSpanFromNode(): missing preceding text span for inline component node');
        return undefined;
    }

    if (DOM.isPortionSpan(domPoint.node)) {

        // beginning of a text span: try to go to the end of the previous text node
        if ((domPoint.offset === 0) && DOM.isTextSpan(domPoint.node.previousSibling)) {
            return createTextPointForSpan(domPoint.node.previousSibling);
        }

        // from text span to text node
        return createTextPointForSpan(domPoint.node, domPoint.offset);
    }

    // other node types need to be handled if existing
    globalLogger.warn('getTextSpanFromNode(): unsupported paragraph child node');
    return undefined;
}

/**
 * Returns the index and the dom node of the position, at which the
 * corresponding dom node is of the specified selector.
 * Returns -1 for the index and null for the dom node, if the selector is
 * never fulfilled.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {String} selector
 *  The selector against which the dom node is compared.
 *
 * @returns {Object}
 */
export function getLastNodeInformationInPositionByNodeName(startnode, position, selector) {

    var index = -1,
        counter = -1,
        value = -1,
        searchedNode = null,
        oxoPos = _.copy(position, true),
        node = startnode;

    while (oxoPos.length > 0) {

        var valueSave = oxoPos.shift(),
            returnObj = getNextChildNode(node, valueSave);

        counter++;

        if (returnObj) {
            if (returnObj.node) {
                node = returnObj.node;
                if ($(node).is(selector)) {
                    index = counter;
                    value = valueSave;
                    searchedNode = node;
                }
            } else {
                // index = -1;
                // globalLogger.error('getLastNodeInformationInPositionByNodeName(): (2) Invalid position: ' + position + ' . Failed to get node at index: ' + counter);
                break;
            }
        } else {
            // index = -1;
            // globalLogger.error('getLastNodeInformationInPositionByNodeName(): (1) Invalid position: ' + position + ' . Failed to get node at index: ' + counter);
            break;
        }
    }

    return { index, value, node: searchedNode };
}

/**
 * Returns the index of the position, at which the corresponding dom
 * node is of the specified selector. Returns -1, if the selector is
 * never fulfilled.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {String} selector
 *  The selector against which the dom node is compared.
 *
 * @returns {Numnber}
 *  The index in the logical position or -1, if no corresponding
 *  dom node can be found.
 *  Example: In the logical position [3,5,7,2,12] the index for a
 *  table row is 1 and for a table column it is 2.
 */
export function getLastIndexInPositionByNodeName(startnode, position, selector) {

    var index = getLastNodeInformationInPositionByNodeName(startnode, position, selector).index;

    return index;
}

/**
 * Returns the value in the position, at which the corresponding dom
 * node is of the specified selector. Returns -1, if the selector is
 * never fulfilled.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {String} selector
 *  The selector against which the dom node is compared.
 *
 * @returns {Numnber}
 *  The value at the specific position in the logical position or -1,
 *  if no corresponding dom node can be found.
 *  Example: In the logical position [3,5,7,2,12] the 5 is value for a
 *  table row, the 7 the value for a table column.
 */
export function getLastValueFromPositionByNodeName(startnode, position, selector) {

    var value = getLastNodeInformationInPositionByNodeName(startnode, position, selector).value;

    return value;
}

/**
 * Returns the dom node which is selected by the specified selector.
 * Returns null, if the selector is never fulfilled.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {String} selector
 *  The selector against which the dom node is compared.
 *
 * @returns {Node|Null}
 *  The searched dom node or null, if no corresponding
 *  dom node can be found.
 */
export function getLastNodeFromPositionByNodeName(startnode, position, selector) {

    var node = getLastNodeInformationInPositionByNodeName(startnode, position, selector).node;

    return node;
}

/**
 * Returns the logical position, at which the corresponding dom
 * node is of the specified selector. Returns -1, if the selector is
 * never fulfilled.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {String} selector
 *  The selector against which the dom node is compared.
 *
 * @returns {[]}
 *  The complete logical position or null, if no corresponding
 *  dom node can be found.
 */
export function getLastPositionFromPositionByNodeName(startnode, position, selector) {

    var pos = null,
        index = getLastNodeInformationInPositionByNodeName(startnode, position, selector).index;

    if (index !== -1) {
        pos = [];
        for (var i = 0; i <= index; i++) {
            pos.push(position[i]);
        }
    }

    return pos;
}

/**
 * Returns 'true' if the logical position is a position inside a drawing group,
 * otherwise false.
 *
 * @param {jQuery|Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Boolean}
 *  Returns 'true', if the logical position is inside a drawing group,
 *  otherwise false.
 */
export function isPositionInsideDrawingGroup(startnode, position) {

    var // the dom position corresponding to the logical position
        domPos = getDOMPosition(startnode, position);

    // using that class 'grouped' is set at the drawing
    return (domPos && domPos.node && DrawingFrame.isGroupedDrawingFrame(domPos.node));
}

/**
 * Returns 'true' if the logical position is a position inside a text
 * frame drawing, otherwise false.
 *
 * @param {jQuery|Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Boolean}
 *  Returns 'true', if the logical position is inside a text frame drawing,
 *  otherwise false.
 */
export function isPositionInsideTextframe(startnode, position) {

    var // the dom position corresponding to the logical position
        domPos = getDOMPosition(startnode, position);

    return (domPos && domPos.node && $(domPos.node).closest(DrawingFrame.TEXTFRAME_NODE_SELECTOR).length > 0);
}

/**
 * Returns the drawing node, that represents a text frame, that contains the specified
 * logical position. If the position is not inside a text frame, null is returned.
 *
 * @param {jQuery|Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {jQuery|Null}
 *  Returns the drawing node in a jQuery list, that represents a text frame, that contains
 *  the specified logical position. If the position is not inside a text frame, null is returned.
 */
export function getClosestDrawingTextframe(startnode, position) {

    var // the dom position corresponding to the logical position
        domPos = getDOMPosition(startnode, position),
        // a text frame ancestor node, if it exists
        textFrameNode = domPos && domPos.node && $(domPos.node).closest(DrawingFrame.TEXTFRAME_NODE_SELECTOR);

    return (textFrameNode && textFrameNode.length > 0) ? DrawingFrame.getDrawingNode(textFrameNode) : null;
}

/**
 * Returns the closest drawing node corresponding to a specified logical position.
 *
 * @param {jQuery|Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {jQuery|Null}
 *  Returns the drawing node in a jQuery list, that is the closest drawing node relative
 *  to the specified position. Or null, if no drawing can be detected.
 */
export function getClosestDrawingAtPosition(startnode, position) {

    var // the dom position corresponding to the logical position
        domPos = getDOMPosition(startnode, position),
        // a drawing node, if it exists
        drawingNode = domPos && domPos.node && $(domPos.node).closest(DrawingFrame.NODE_SELECTOR);

    return (drawingNode && drawingNode.length > 0) ? drawingNode : null;
}

/**
 * Check whether the specified position describes the position of a grouped drawing.
 *
 * @param {jQuery|Node} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {jQuery|Null}
 *  Returns whether the specified position describes the position of a grouped drawing.
 */
export function isGroupedDrawingPosition(startNode, position) {
    var domPoint = getDOMPosition(startNode, position);
    return domPoint && domPoint.node && DrawingFrame.isGroupedDrawingFrame(domPoint.node);
}

/**
 * Returns 'true' if the logical position is a position inside a
 * table, otherwise false.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Boolean}
 *  Returns true, if the logical position is inside a table,
 *  otherwise false.
 */
export function isPositionInTable(startnode, position) {

    var positionInTable = false,
        domNode = startnode,
        localPos = _.copy(position, true);

    while (localPos.length > 0) {

        var nextChild = getNextChildNode(domNode, localPos.shift());

        if (nextChild) {

            domNode = nextChild.node;

            if (DOM.isTableNode(domNode) || DrawingFrame.isTableDrawingFrame(domNode)) {
                positionInTable = true;
                break;
            }
        }
    }

    return positionInTable;
}

/**
 * Convenience function, that returns the last table node, if available.
 * Otherwise null we be returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {Object} [options]
 *  An object containing some additional properties:
 *  @param {Boolean} [options.onlyTableNode=false]
 *      Whether only table nodes shall be returned. If not specified, also drawings of type 'table'
 *      will be returned.
 *
 * @returns {Node}
 *  Returns the last table node of the logical position if available, otherwise null. If 'onlyTableNode'
 *  is true, the returned node can also be a drawing node of type 'table'.
 */
export function getCurrentTable(startnode, position, options) {

    var // whether only table nodes shall be searched
        onlyTableNodes = getBooleanOption(options, 'onlyTableNode', false),
        // the search selector
        selector = DOM.TABLE_NODE_SELECTOR;

    if (!onlyTableNodes) { selector += (', ' + DrawingFrame.TABLE_TEXTFRAME_NODE_SELECTOR); }

    return getLastNodeFromPositionByNodeName(startnode, position, selector);
}

/**
 * Convenience function, that returns the last paragraph node, if available.
 * Otherwise null we be returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Node|Null}
 *  Returns the last paragraph node of the logical position if available,
 *  otherwise null.
 */
export function getCurrentParagraph(startnode, position) {
    return getLastNodeFromPositionByNodeName(startnode, position, DOM.PARAGRAPH_NODE_SELECTOR);
}

/**
 * Determining the number of rows in a table. Returned is the last
 * index, the value is 0-based. So this is not the length.
 * Otherwise -1 we be returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Number}
 *  Returns the index of the last row or -1, if the position
 *  is not included in a table.
 */
export function getLastRowIndexInTable(startnode, position) {

    var rowIndex = -1,
        table = getLastNodeFromPositionByNodeName(startnode, position, DOM.TABLE_NODE_AND_DRAWING_SELECTOR);

    if (table) {
        rowIndex = DOM.getTableRows(table).length - 1;
    }

    return rowIndex;
}

/**
 * Determining the number of columns in a row specified by the
 * logical position. Returned is the last index, the
 * value is 0-based. So this is not the length.
 * Otherwise -1 we be returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Number}
 *  Returns the index of the last column of the specified row of a
 *  table or -1, if the position is not included in a table.
 */
export function getLastColumnIndexInRow(startnode, position) {

    var columnIndex = -1,
        row = getLastNodeFromPositionByNodeName(startnode, position, 'tr');

    if (row) {
        columnIndex = $(row).children('td').length - 1;
    }

    return columnIndex;
}

/**
 * Determining the index of the row specified by the logical position
 * inside a table. The first row has index 0, the second index 1
 * and so on. The value is 0-based.
 * If no row is found in the logical position, -1 will be
 * returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Number}
 *  Returns the index of the row inside a table, or -1, if the
 *  logical position does not contain a row.
 */
export function getRowIndexInTable(startnode, position) {
    return getLastValueFromPositionByNodeName(startnode, position, 'tr');
}

/**
 * Determining the index of the column/cell specified by the logical
 * position inside a row. The first column/cell has index 0, the second index 1
 * and so on. The value is 0-based.
 * If no column/cell is found in the logical position, -1 will be
 * returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Number}
 *  Returns the index of the column/cell inside a row, or -1, if the
 *  logical position does not contain a column/cell.
 */
export function getColumnIndexInRow(startnode, position) {
    return getLastValueFromPositionByNodeName(startnode, position, 'td');
}

/**
 * Determining the index of the paragraph specified by the logical
 * position inside the document root or inside a table cell.
 * The first paragraph has index 0, the second index 1
 * and so on. The value is 0-based.
 * If no paragraph is found in the logical position, -1 will be
 * returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Number}
 *  Returns the index of the paragraph inside the document row
 *  or inside a table cell, or -1, if the
 *  logical position does not contain a paragraph.
 */
export function getParagraphIndex(startnode, position) {
    return getLastValueFromPositionByNodeName(startnode, position, DOM.PARAGRAPH_NODE_SELECTOR);
}

/**
 * Determining the index of the last paragraph in a cell specified by the logical
 * position inside a row. The first cell has index 0, the second index 1
 * and so on. The value is 0-based.
 * If no cell is found in the logical position, -1 will be
 * returned.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *  @param {Boolean} [options.ignoreImplicitParagraphs=false]
 *      Whether implicit paragraphs shall be ignored.
 *
 * @returns {Number}
 *  Returns the index of the last paragraph in a cell, or -1, if the
 *  logical position does not contain a cell.
 */
export function getLastParaIndexInCell(startnode, position, options) {

    var lastPara = -1,
        cell = getLastNodeFromPositionByNodeName(startnode, position, 'td'),
        ignoreImplicitParagraphs = getBooleanOption(options, 'ignoreImplicitParagraphs', false);

    if (cell) {
        lastPara = DOM.getCellContentNode(cell)[0].childNodes.length - 1;

        if ((ignoreImplicitParagraphs) && (lastPara > 0) && (DOM.isImplicitParagraphNode(DOM.getCellContentNode(cell)[0].lastChild))) {
            lastPara -= 1;
        }

    }

    return lastPara;
}

/**
 * Determining the final paragraph or table inside a specified table cell node.
 * This final node is returned. If no final node is found, null is returned. It
 * is possible to set via option, if the final paragraph can be an implicit
 * paragraph.
 *
 * @param {Node} cellNode
 *  The cell node, whose last child is required.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *  @param {Boolean} [options.ignoreImplicitParagraphs=false]
 *      Whether implicit paragraphs shall be ignored.
 *
 * @returns {Node}
 *  Returns the final paragraph or table node inside a specified table node.
 *  If it does not exist, null is returned.
 */
export function getLastParagraphOrTableNodeInCellNode(cellNode, options) {

    if (!DOM.isTableCellNode(cellNode)) { return null; }

    var // the collector of all top level paragraphs or tables
        allParagraphs = DOM.getCellContentNode(cellNode)[0].childNodes,
        // the final paragraph in the cell
        finalParagraph = allParagraphs[allParagraphs.length - 1] || null;

    if (getBooleanOption(options, 'ignoreImplicitParagraphs', false) && finalParagraph && DOM.isImplicitParagraphNode(finalParagraph)) {
        finalParagraph = finalParagraph.previousSibling ? finalParagraph.previousSibling : null;
    }

    return finalParagraph;
}

/**
 * Returns the following paragraph that follows a specified paragraph.
 *
 * @param {Node|jQuery} para
 *  The paragraph node for that the following paragraph is searched.
 *
 * @returns {jQuery|Null}
 *  The following paragraph or Null, if no following paragraph could be found.
 */
export function getDirectlyFollowingParagraph(para) {

    var paragraph = null;

    if ($(para).next().hasClass('page-break')) {
        paragraph = $(para).next().next();
    } else {
        paragraph = $(para).next();
    }

    if (!paragraph || paragraph.length === 0 || !DOM.isParagraphNode(paragraph)) { paragraph = null; }

    return paragraph;
}

/**
 * Returns the logical length of the content nodes of the paragraph located
 * at the specified logical position. Text fields and drawing nodes have a
 * logical length of 1.
 *
 * @param {Node|jQuery} startnode
 *  The start node corresponding to the logical position. if this object is
 *  a jQuery collection, uses the first DOM node it contains.
 *
 * @param {Array<Number>} position
 *  The logical position of the paragraph.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.next=false]
 *      If set to true, the length of the following paragraph is calculated.
 *
 * @returns {Number | void} // TODO: check-void-return
 *  The logical length of all content nodes inside the paragraph or 0, if
 *  the logical position does not point to a paragraph.
 */
export function getParagraphLength(startnode, position, options) {

    var // the paragraph element addressed by the passed logical position
        paragraph = getLastNodeFromPositionByNodeName(startnode, position, DOM.PARAGRAPH_NODE_SELECTOR),
        // length of the paragraph contents
        length = 0;

    if (paragraph && getBooleanOption(options, 'next', false)) {
        paragraph = getDirectlyFollowingParagraph(paragraph);

        if (!paragraph) {
            globalLogger.error('getParagraphLength(): No paragraph found as next sibling!');
            return;
        }
    }

    if (paragraph) {
        iterateParagraphChildNodes(paragraph, function (node, start, nodeLength) {
            length += nodeLength;
        });
    }

    return length;
}

/**
 * Returns the logical length of the content nodes of the paragraph.
 * Text fields and drawing nodes have a logical length of 1.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node. If this object is a jQuery collection, uses the first
 *  DOM node it contains.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.next=false]
 *      If set to true, the length of the following paragraph is calculated.
 *  @param {Boolean} [options.ignoreDrawings=false]
 *      If set to true, the length of drawings will be set to 0. This makes it
 *      possible to find paragraphs, that contain only drawings.
 *
 * @returns {Number | void} // TODO: check-void-return
 *  The logical length of all content nodes inside the paragraph or 0, if
 *  the logical position does not point to a paragraph.
 */
export function getParagraphNodeLength(paragraph, options) {

    // length of the paragraph contents
    var length = 0;
    // whether drawings shall be ignored and get a length of 0
    var ignoreDrawings = getBooleanOption(options, 'ignoreDrawings', false);

    if (getBooleanOption(options, 'next', false)) {
        paragraph = getDirectlyFollowingParagraph(paragraph);

        if (!paragraph) {
            globalLogger.error('getParagraphNodeLength(): No paragraph found as next sibling!');
            return;
        }
    }

    iterateParagraphChildNodes(paragraph, function (node, start, nodeLength) {
        if (ignoreDrawings && nodeLength === 1) { if (DOM.isDrawingFrame(node)) { nodeLength = 0; } }
        length += nodeLength;
    });

    return length;
}

/**
 * Performance optimized version of the function "getParagraphNodeLength". This function
 * does not calculate the full length of the paragraph, but only checks if there is at least
 * one node, whose length is larger than 0. If such a node is found, the iteration is
 * terminated.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node. If this object is a jQuery collection, uses the first DOM node it
 *  contains.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.ignoreDrawings=false]
 *      If set to true, the length of drawings will be set to 0. This makes it
 *      possible to find paragraphs, that contain only drawings.
 *
 * @returns {Boolean}
 *  Whether the specified paragraph is empty. All childnodes have a node length of 0.
 */
export function isEmptyParagraph(paragraph, options) {

    // whether the paragraph is empty
    let isEmpty = true;
    // whether drawings shall be ignored and get a length of 0
    const ignoreDrawings = getBooleanOption(options, 'ignoreDrawings', false);

    iterateParagraphChildNodes(paragraph, function (childNode, start, nodeLength) {
        if (ignoreDrawings && nodeLength === 1) { if (DOM.isDrawingFrame(childNode)) { nodeLength = 0; } }
        if (nodeLength > 0) {
            isEmpty = false;
            return BREAK; // returns as soon, as the length of one node is not null
        }
    });

    return isEmpty;
}

/**
 * Returns the logical text level start or end position for a specified
 * node. This can be an arbitrary node, like a text span, a paragraph, a
 * table cell, a table row or a table node.
 *
 * @param {HTMLElement|jQuery} node
 *  The node, whose logical start position or end position will
 *  be calculated. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @param {Node} [startnode]
 *  The start node corresponding to the logical position.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.lastPosition=false]
 *      If set to true, the last logical text level position is returned.
 *      Default is, that the first logical text level position is returned.
 *  @param {Boolean} [options.suppressWarning=false]
 *      If set to true, the error messages in Position.getOxoPosition() are
 *      not written into the console. This is useful, if the correctness of
 *      a position is checked.
 *
 * @returns {Array<Number>|Null}
 *  Returns the logical start or end position on text level for a specified
 *  node. The calculated positions can be used for setting the text selection.
 *  This means that the end is behind the final character.
 *  If the position cannot be determined, null is returned.
 */
export function getTextLevelPositionInNode(node, startnode, options) {

    var // the logical text level position
        position = null,
        // whether the first or last logical position shall be calculated
        lastPosition = getBooleanOption(options, 'lastPosition', false),
        // a paragraph helper node
        paraNode = null,
        // a collection of all paragraphs in the specified node
        allParaNodes = null,
        // the range of the specified node
        range = null;

    if (DOM.isParagraphNode(node)) {
        paraNode = node;
    } else {
        allParaNodes = $(node).find(DOM.PARAGRAPH_NODE_SELECTOR);

        if (allParaNodes.length > 0) {
            paraNode = lastPosition ? allParaNodes.last() : allParaNodes.first();
        }
    }

    if (paraNode) {
        position = getOxoPosition(startnode, paraNode, 0, options);
        position.push(lastPosition ? getParagraphNodeLength(paraNode) : 0);
    } else {
        // no paragraph node could be determined -> checking that at least an ancestor is a paragraph node
        if ($(node).closest(DOM.PARAGRAPH_NODE_SELECTOR).length > 0) {
            range = getNodeStartAndEndPosition(node, startnode, { fullLogicalPosition: true, suppressWarning: getBooleanOption(options, 'suppressWarning', false) });
            if (range) {
                position = lastPosition ? increaseLastIndex(range.end) : range.start; // increasing last index, so it can be used for selection
            }
        }
    }

    return position;
}

/**
 * Returns the logical start and end index of a text component node in its
 * parent paragraph. The passed node may be any child node of a paragraph
 * (either an editable content node, or a helper node such as an offset
 * container used to position floated drawings).
 *
 * @param {HTMLElement|jQuery} node
 *  The paragraph child node, whose logical start index and the length will
 *  be calculated. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @param {Node} [startnode]
 *  The start node corresponding to the logical position.
 *  This parameter is optional, but if the option 'fullLogicalPosition' is
 *  set to true, this parameter must be defined.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.fullLogicalPosition=false]
 *      If set to true, the full logical position of the specified node is
 *      returned. In this case the parameter startnode must be defined. If
 *      set to false (the default) only the index positions are calculated.
 *      This does not require the parameter startnode.
 *  @param {Boolean} [options.suppressWarning=false]
 *      If set to true, the error messages in Position.getOxoPosition() are
 *      not written into the console. This is useful, if the correctness of
 *      a position is checked.
 *
 * @returns {Object|Null}
 *  Returns an the logical start and end index of the specified node in its
 *  parent paragraph element. If 'fullLogicalPosition' is set to true in
 *  the options, not only the index is returned, but the complete logical
 *  position of the node. If the index could not be determined, null is
 *  returned.
 */
export function getNodeStartAndEndPosition(node, startnode, options) {

    var // the paragraph containing the specified node
        paragraph = null,
        // whether the node could be found
        foundNode = false,
        // the logical start index of the passed node
        start = -1, length = -1,
        // whether the complete logical position is required, or only the index
        fullLogicalPosition = getBooleanOption(options, 'fullLogicalPosition', false),
        // the logical position of the paragraph node
        paraPos = null,
        // the object containing the required start and end position
        pos = null;

    node = getDomNode(node);

    if (DOM.isParagraphNode(node.parentNode)) {

        // visit all content nodes of the node parent (the paragraph), and search for the node
        paragraph = node.parentNode;

        iterateParagraphChildNodes(paragraph, function (childNode, nodeStart, nodeLength) {
            if (node === childNode) {
                start = nodeStart;
                length = nodeLength;
                foundNode = true;
                return BREAK;
            }
        }, undefined, { allNodes: true }); // visit all child nodes of the paragraph

        if (foundNode) {
            if (fullLogicalPosition) {
                paraPos = getOxoPosition(startnode, paragraph, null, options);
                if (paraPos) {
                    pos = { start: paraPos.concat([start]), end: paraPos.concat([start + length - 1]) };
                }
            } else {
                pos = { start, end: length - 1 };
            }
        }
    } else {
        paraPos = getOxoPosition(startnode, node);
        pos = { start: paraPos, end: paraPos };
    }

    return pos;
}

/**
 * Returns the number of leading floating drawing objects in a paragraph.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node.
 *
 * @param {Number} [verticalOffset]
 *  An optional vertical offset in px. If set, only those drawings are counted,
 *  that are nearer to the top edge of the paragraph.
 *
 * @param {HTMLElement|jQuery} drawingNode - optional
 *  Current drawing from which we count leading floating drawing objects, and has to be excluded from count
 *
 * @returns {Number}
 *  The number of leading floating drawing nodes inside the paragraph.
 */
export function getLeadingFloatingDrawingCount(paragraph, verticalOffset, drawingNode) {

    var // number of leading floating drawing nodes
        count = 0;

    if (drawingNode === undefined) {
        drawingNode = null;
    } else {
        if (drawingNode instanceof $) { drawingNode = drawingNode[0]; } // convert to DOM element for comparing with childNode
    }

    if (verticalOffset === undefined) { verticalOffset = -1; }

    iterateParagraphChildNodes(paragraph, function (childNode) {

        if (DOM.isFloatingDrawingNode(childNode) && (childNode !== drawingNode)) {

            if ((verticalOffset >= 0) && (($(childNode).offset().top - paragraph.offset().top) > verticalOffset)) {
                return BREAK;
            }

            count += 1;
        } else {
            return BREAK;
        }
    });

    return count;
}

/**
 * Returns the number of leading absolute positioned drawings inside a
 * specified paragraph. These absolute positioned drawings are anchored
 * to the specified paragraph.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node.
 *
 * @returns {Number}
 *  The number of leading absolute positioned drawings.
 */
export function getLeadingAbsoluteDrawingCount(paragraph) {

    var // number of leading floating drawing nodes
        count = 0;

    iterateParagraphChildNodes(paragraph, function (childNode) {

        if (DOM.isAbsoluteParagraphDrawing(childNode)) {
            count += 1;
        } else {
            return BREAK;
        }
    });

    return count;
}

/**
 * Returns the number of leading tabs inside a specified paragraph. Between
 * the tabs empty text spans can be located.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node.
 *
 * @returns {Number}
 *  The number of leading tabs.
 */
export function getLeadingTabsInParagraph(paragraph) {

    var // number of leading tab nodes
        count = 0,
        // whether the specified node is a tab node
        isTabNode = false;

    iterateParagraphChildNodes(paragraph, function (childNode) {

        isTabNode = DOM.isTabNode(childNode);

        if (isTabNode || DOM.isEmptySpan(childNode)) {
            if (isTabNode) { count += 1; } // counting only tabs
        } else {
            return BREAK;
        }
    });

    return count;
}

/**
 * Returns the number of leading absolute positioned drawings or place
 * holder drawing nodes inside a specified paragraph.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node.
 *
 * @returns {Number}
 *  The number of leading absolute positioned drawings.
 */
export function getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount(paragraph) {

    var // number of leading floating drawing nodes
        count = 0;

    iterateParagraphChildNodes(paragraph, function (childNode) {

        if (DOM.isAbsoluteParagraphDrawing(childNode) || DOM.isDrawingPlaceHolderNode(childNode)) {
            count += 1;
        } else {
            return BREAK;
        }
    });

    return count;
}

/**
 * Returns the number of absolute positioned drawings or place holder drawing nodes that
 * are positioned before a specified node inside a paragraph.
 *
 * @param {HTMLElement|jQuery} node
 *  The paragraph child node.
 *
 * @returns {Number}
 *  The number of absolute positioned drawings or place holder drawing nodes that
 *  are positioned before a specified node inside a paragraph.
 */
export function getPreviousAbsoluteDrawingOrDrawingPlaceHolderCount(node) {

    var // the found drawing place holder nodes or absolute drawings before the specified node inside a paragraph
        affectedNodes = $(node).prevAll(DOM.ABSOLUTE_DRAWING_SELECTOR + ', ' + DOM.DRAWINGPLACEHOLDER_NODE_SELECTOR);

    return affectedNodes.length;
}

/**
 * Returns the logical start index of a text component node in its parent
 * paragraph. The passed node may be any child node of a paragraph (either
 * an editable content node, or a helper node such as an offset container
 * used to position floated drawings).
 *
 * @param {HTMLElement|jQuery} node
 *  The paragraph child node, whose logical start index will be calculated.
 *  If this object is a jQuery collection, uses the first node it contains.
 *
 * @returns {Number}
 *  The logical start index of the specified node in its parent paragraph
 *  element.
 */
export function getStartOfParagraphChildNode(node) {

    var // the logical start index of the passed node
        start = -1;

    // visit all content nodes of the node parent (the paragraph), and search for the node
    node = getDomNode(node);
    iterateParagraphChildNodes(node.parentNode, function (childNode, nodeStart) {
        if (node === childNode) {
            start = nodeStart;
            return BREAK;
        }
    }, undefined, { allNodes: true }); // visit all child nodes of the paragraph

    return start;
}

/**
 * Checks, if a specified position is the first position
 * inside a text node in a cell in a table.
 *
 * @param {HTMLElement|jQuery} startnode
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is the first position inside a table cell.
 */
export function isFirstPositionInTable(startnode, pos) {

    var isTableStartPosition = false,
        localPos = _.copy(pos, true);

    if (localPos.pop() === 0) {   // start position
        if (localPos.pop() === 0) {   // start paragraph
            if (localPos.pop() === 0) {   // start column
                if (localPos.pop() === 0) {   // start row
                    var domPos = getDOMPosition(startnode, localPos);
                    if ((domPos) && ($(domPos.node).is(DOM.TABLE_NODE_SELECTOR))) {
                        isTableStartPosition = true;
                    }
                }
            }
        }
    }

    return isTableStartPosition;
}

/**
 * Checks, if a specified position is inside a text node that comes after at
 * least one drawing with text frame inside its paragraph.
 *
 * @param {Node|jQuery|Null} para
 *  The paragraph node to be checked.
 *
 * @param {Number} pos
 *  The last position of the logical position. Only the position inside the
 *  specified paragraph is relevant.
 *
 * @param {Object} [options]
 *  @param {Boolean} [options.ignoreOffset=false]
 *    Whether the offset inside the specified node can be ignored. Typically for
 *    cursor-left-right handling the offset is important. But for cursor-up-down
 *    the offset inside the node is not important.
 *
 * @returns {Boolean}
 *  Whether the specified position is inside a text node that comes after at
 *  least one drawing with text frame inside its paragraph.
 */
export function isTextPositionBehindDrawingWithTextframe(para, pos, options) {
    if (pos === 0) { return false; }
    const domPos = getDOMPosition(para, [pos]);
    if (!domPos || !domPos.node) { return false; }
    if (domPos.offset > 0 && !getBooleanOption(options, 'ignoreOffset', false)) { return false; }
    return DOM.isTextNodeBehindDrawingWithTextframe(domPos.node.nodeType === 3 ? domPos.node.parentNode : domPos.node);
}

/**
 * Checks, if a specified position is inside a text node that comes after a
 * hard break node at the beginning of its own paragraph.
 *
 * @param {Node|jQuery|Null} para
 *  The paragraph node to be checked.
 *
 * @param {Number} pos
 *  The last position of the logical position. Only the position inside the
 *  specified paragraph is relevant.
 *
 * @param {Object} [options]
 *  @param {Boolean} [options.ignoreOffset=false]
 *    Whether the offset inside the specified node can be ignored. Typically for
 *    cursor-left-right handling the offset is important. But for cursor-up-down
 *    the offset inside the node is not important.
 *
 * @returns {Boolean}
 *  Whether the specified position is inside a text node that comes after a
 *  hard break node at the beginning of its own paragraph.
 */
export function isTextPositionBehindHardbreakAtParaStart(para, pos, options) {
    if (pos === 0) { return false; }
    const domPos = getDOMPosition(para, [pos]);
    if (!domPos || !domPos.node) { return false; }
    if (domPos.offset > 0 && !getBooleanOption(options, 'ignoreOffset', false)) { return false; }
    return DOM.isTextNodeBehindHardbreakAtParaStart(para, domPos.node.nodeType === 3 ? domPos.node.parentNode : domPos.node);
}

/**
 * Checks, if a specified position is the first position inside a
 * table that is directly behind another table. If this table is located
 * behind another table, the outer table is more relevant.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Array<Number>|Null}
 *  The position of the table, that is directly following another table. If
 *  this is not the case, null is returned.
 */
export function getTableBehindTablePosition(startnode, pos) {

    var tablePos = null,
        domPos = null,
        proceed = (pos.length >= 4),
        localPos = _.copy(pos);

    // checking if a position is table start position of a table
    // that is a direct follower of another table.

    if (localPos.pop() === 0) {

        while ((localPos.pop() === 0) &&
            (localPos.pop() === 0) &&
            (localPos.pop() === 0) &&
            (proceed)) {

            domPos = getDOMPosition(startnode, localPos);
            if (domPos && DOM.isTableNode(domPos.node) && DOM.isTableNode(DOM.getAllowedNeighboringNode(domPos.node, { next: false }))) {
                tablePos = _.copy(localPos);
            }

            proceed = (pos.length >= 4);
        }
    }

    return tablePos;
}

/**
 * Checks, if a specified position is the first position inside a
 * table that is at the beginning of a table cell. If this table is located
 * in another table, the outer table is more relevant.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Array<Number>|Null}
 *  The position of the table, that is at the beginning of a table cell. If
 *  this is not the case, null is returned.
 */
export function getTableAtCellBeginning(startnode, pos) {

    var tablePos = null,
        domPos = null,
        proceed = (pos.length >= 4),
        localPos = _.copy(pos);

    // checking if a position is table start position of a table
    // that is at the beginning of a table cell.

    if (localPos.pop() === 0) {

        while ((localPos.pop() === 0) &&
            (localPos.pop() === 0) &&
            (localPos.pop() === 0) &&
            (proceed)) {

            domPos = getDOMPosition(startnode, localPos);
            if ((domPos) && (DOM.isTableNode(domPos.node)) && (localPos[localPos.length - 1] === 0)) {
                tablePos = _.copy(localPos);
            }

            proceed = (pos.length >= 4);
        }
    }

    return tablePos;
}

/**
 * Checks, if a specified position is the first position
 * inside a text node in a cell in a table.
 *
 * @param {HTMLElement|jQuery} startnode
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is the first position inside a table cell.
 */
export function isFirstPositionInTableCell(startnode, pos) {

    var isCellStartPosition = false,
        localPos = _.copy(pos, true);

    if (localPos.pop() === 0) {   // start position
        if (localPos.pop() === 0) {   // start paragraph
            var domPos = getDOMPosition(startnode, localPos);
            if ((domPos) && ($(domPos.node).is('td'))) {
                isCellStartPosition = true;
            }
        }
    }

    return isCellStartPosition;
}

/**
 * Checks, if a specified position is the last position
 * inside a text node in a cell in a table.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *
 * @returns {Boolean}
 *  Whether the position is the last position inside a table cell.
 */
export function isLastPositionInTableCell(startnode, pos, options) {
    var isCellEndPosition = false,
        localPos = _.copy(pos, true);

    var index = localPos.pop();
    if (index === getParagraphLength(startnode, localPos)) {   // last position
        var lastPara = localPos.pop();
        if (lastPara === getLastParaIndexInCell(startnode, localPos, options)) { // last paragraph
            var domPos = getDOMPosition(startnode, localPos);
            if ((domPos) && ($(domPos.node).is('td'))) {
                isCellEndPosition = true;
            }
        }
    }

    return isCellEndPosition;
}

/**
 * Checks, if two given logical positions describe positions inside one table but
 * in different table cells.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} pos1
 *  The first logical position.
 *
 * @param {Array<Number>} pos2
 *  The second logical position.
 *
 * @returns {Boolean}
 *  Whether the two positions are logical positions inside one table but in
 *  different table cells.
 */
export function positionsInTowCellsInSameTable(startnode, pos1, pos2) {

    var // the cell node for pos1
        cell1 = null,
        // the cell node for pos2
        cell2 = null;

    if (pos1.length !== pos2.length) { return false; } // only comparing same level

    cell1 = getLastNodeFromPositionByNodeName(startnode, pos1, DOM.TABLE_CELLNODE_SELECTOR);
    cell2 = getLastNodeFromPositionByNodeName(startnode, pos2, DOM.TABLE_CELLNODE_SELECTOR);

    if (!cell1 || !cell2 || (cell1 === cell2)) { return false; }

    // checking, if both cells are inside the same table
    return getDomNode($(cell1).closest(DOM.TABLE_NODE_AND_DRAWING_SELECTOR)) === getDomNode($(cell2).closest(DOM.TABLE_NODE_AND_DRAWING_SELECTOR));
}

/**
 * Checks, if two given logical positions describe the first and the last position of the
 * same table, so that the table is selected completely.
 * Info: This function works only reliable, if the selection type is 'cell'. This must
 * be checked, before calling this function.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} posA
 *  The first logical position.
 *
 * @param {Array<Number>} posB
 *  The second logical position.
 *
 * @returns {Boolean}
 *  Whether the two positions are the first and the last logical positions inside one table.
 */
export function isCompleteTableSelection(startnode, posA, posB) {

    if (!posA || !posB) { return false; }

    var cellnodeA = getLastNodeFromPositionByNodeName(startnode, posA, DOM.TABLE_CELLNODE_SELECTOR),
        cellnodeB = getLastNodeFromPositionByNodeName(startnode, posB, DOM.TABLE_CELLNODE_SELECTOR),
        tableNode = null,
        allTableCells = null,
        isCompleteTableSelection = false;

    if (cellnodeA && cellnodeB) {
        // getting table
        tableNode = $(cellnodeA).closest(DOM.TABLE_NODE_AND_DRAWING_SELECTOR);
        // first cell or last cell
        if (tableNode.length > 0) {
            allTableCells = DOM.getTableCells(tableNode);

            if (allTableCells.length > 0 && getDomNode(allTableCells.first()) === cellnodeA && getDomNode(allTableCells.last()) === cellnodeB) {
                isCompleteTableSelection = true;
            }
        }
    }

    return isCompleteTableSelection;
}

/**
 * Check, if two given logical positions describe a selection, in which one position is inside
 * a table cell and the other is NOT inside this table cell.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} posA
 *  The first logical position.
 *
 * @param {Array<Number>} posB
 *  The second logical position.
 *
 * @returns {Boolean}
 *  Whether the two positions describe a selection, in which one position is inside a table cell
 *  and the other is NOT inside this table cell.
 */
export function isCellExceedingSelection(startnode, posA, posB) {

    var isCellExceedingSelection = false,
        cellnodeA = getLastNodeFromPositionByNodeName(startnode, posA, DOM.TABLE_CELLNODE_SELECTOR),
        cellnodeB = getLastNodeFromPositionByNodeName(startnode, posB, DOM.TABLE_CELLNODE_SELECTOR);

    if ((cellnodeA && !cellnodeB) || (cellnodeB && !cellnodeA) || (cellnodeA && cellnodeB && cellnodeA !== cellnodeB)) {
        isCellExceedingSelection = true;
    }

    return isCellExceedingSelection;
}

/**
 * Checks, if a specified position is the last position
 * inside a text node in a paragraph.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is the last position inside a paragraph.
 */
export function isLastPositionInParagraph(startnode, pos) {
    var isParagraphEndPosition = false,
        localPos = _.copy(pos, true),
        index = localPos.pop();

    if (index === getParagraphLength(startnode, localPos)) {   // last position
        isParagraphEndPosition = true;
    }

    return isParagraphEndPosition;
}

/**
 * Checks, if a specified position is the position of a paragraph.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is the position of a paragraph.
 */
export function isParagraphPosition(startnode, pos) {
    var para = getParagraphElement(startnode, pos);
    return (para && DOM.isParagraphNode(para));
}

/**
 * Checks, if a specified position is the position of an implicit paragraph.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is the position of an implicit paragraph.
 */
export function isImplicitParagraphPosition(startnode, pos) {
    var para = getParagraphElement(startnode, pos);
    return (para && DOM.isImplicitParagraphNode(para));
}

/**
 * Checks, if a specified position is the position of an implicit paragraph
 * with height 0. These paragraphs can be ignored in read-only mode.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is the position of a shrinked implicit paragraph.
 */
export function isImplicitShrinkedParagraphPosition(startnode, pos) {
    var para = getParagraphElement(startnode, pos);
    return (para && DOM.isImplicitParagraphNode(para) && $(para).height() === 0);
}

/**
 * Checks, if a specified position is inside an implicit paragraph.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is a text position inside an implicit paragraph.
 */
export function isPositionInImplicitParagraph(startnode, pos) {
    var para = $(getDOMPosition(startnode, pos).node).closest(DOM.PARAGRAPH_NODE_SELECTOR);
    return (para && DOM.isImplicitParagraphNode(para));
}

/**
 * Checks, if a specified position is inside an implicit paragraph
 * with height 0. These paragraphs can be ignored in read-only mode.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} pos
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the position is a text position inside a shrinked implicit
 *  paragraph.
 */
export function isPositionInImplicitShrinkedParagraph(startnode, pos) {
    var para = $(getDOMPosition(startnode, pos).node).closest(DOM.PARAGRAPH_NODE_SELECTOR);
    return (para && DOM.isImplicitParagraphNode(para) && $(para).height() === 0);
}

/**
 * Checks, if two logical positions of the same length
 * reference two positions inside the same table. All
 * values inside the logical position of representing the
 * table node, must be identical.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} posA
 *  The logical position.
 *
 * @param {Array<Number>} posB
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the positions have the same length and reference positions
 *  within the same table.
 */
export function isSameTableLevel(startnode, posA, posB) {
    // If both position are in the same table, but in different cells (this
    // can happen in Chrome, but not in Firefox. In Firefox the complete cells
    // are selected.
    var isSameTableLevel = true;

    if (posA.length === posB.length) {
        var tableA = getLastPositionFromPositionByNodeName(startnode, posA, DOM.TABLE_NODE_AND_DRAWING_SELECTOR),
            tableB = getLastPositionFromPositionByNodeName(startnode, posB, DOM.TABLE_NODE_AND_DRAWING_SELECTOR);

        // Both have to be identical
        if (tableA.length === tableB.length) {
            var max = tableA.length - 1;
            for (var i = 0; i <= max; i++) {
                if (tableA[i] !== tableB[i]) {
                    isSameTableLevel = false;
                    break;
                }
            }
        } else {
            isSameTableLevel = false;
        }
    } else {
        isSameTableLevel = false;
    }

    return isSameTableLevel;
}

/**
 * Calculating the last logical position inside a paragraph or a
 * table. In a table the last cell can again be filled with a table.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} paragraph
 *  The logical position.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *
 * @returns {Array<Number>}
 *  Returns the last logical position inside a paragraph or
 *  a table. If the parameter 'paragraph' is a logical position, that
 *  is not located inside a table or paragraph, null is returned.
 */
export function getLastPositionInParagraph(startnode, paragraph, options) {

    var paraPosition = getLastPositionFromPositionByNodeName(startnode, paragraph, DOM.CONTENT_NODE_SELECTOR),
        infoNode;

    if ((paraPosition) && (paraPosition.length > 0)) {

        infoNode = getDOMPosition(startnode, paraPosition);

        while (infoNode && DOM.isTableNode(infoNode.node)) {
            paraPosition.push(getLastRowIndexInTable(startnode, paraPosition));
            paraPosition.push(getLastColumnIndexInRow(startnode, paraPosition));
            paraPosition.push(getLastParaIndexInCell(startnode, paraPosition, options));
            infoNode = getDOMPosition(startnode, paraPosition);
        }

        paraPosition.push(getParagraphLength(startnode, paraPosition));
    }

    return paraPosition;
}

/**
 * Calculating the last logical position of a paragraph that is preceeding
 * a paragraph specified by its logical position.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Number[]} position
 *  The logical position of the paragraph.
 *
 * @returns {Number[]}
 *  Returns the last logical position of a paragraph that is preceeding
 *  a specified paragraph.
 */
export function getLastPositionOfPreviousParagraph(startnode, position) {

    var // the new calculated last position of the previous paragraph
        lastPosition = null,
        // the position of the previous paragraph
        paraPosition = null;

    if (_.last(position) > 0) {
        paraPosition = _.clone(position);
        paraPosition[position.length - 1]--; // decreasing last index
        lastPosition = getLastPositionInParagraph(startnode, paraPosition);
    }

    return lastPosition;
}

/**
 * Calculating the first logical position inside a paragraph or a
 * table. In a table the first cell can again be filled with a table.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Array<Number>}
 *  Returns the first logical position inside a paragraph or
 *  a table. If the parameter 'paragraph' is a logical position, that
 *  is not located inside a table or paragraph, null is returned.
 */
export function getFirstPositionInParagraph(startnode, position) {

    var paraPosition = getLastPositionFromPositionByNodeName(startnode, position, DOM.CONTENT_NODE_SELECTOR),
        infoNode;

    if ((paraPosition) && (paraPosition.length > 0)) {

        infoNode = getDOMPosition(startnode, paraPosition);

        while (infoNode && DOM.isTableNode(infoNode.node)) {
            paraPosition.push(0);  // row
            paraPosition.push(0);  // column
            paraPosition.push(0);  // paragraph
            infoNode = getDOMPosition(startnode, paraPosition);
        }

        paraPosition.push(0);
    }

    return paraPosition;
}

/**
 * Returns the first logical text position inside a specified text frame.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {HTMLElement|jQuery|Array<Number>} frame
 *  The frame node as html element or as jQuery element or its logical position.
 *
 * @returns {Array<Number>|null}
 *  The logical position that is the first text position inside the text frame. Or
 *  null, if the position cannot be determined.
 */
export function getFirstTextPositionInTextFrame(startnode, frame) {

    var // the node of the text frame
        frameNode = null,
        // the first paragraph node
        paragraph = null;

    if (_.isArray(frame)) {
        frameNode = getDOMPosition(startnode, frame, true);
        if (frameNode.node) { frameNode = frameNode.node; }
    } else {
        frameNode = frame;
    }

    if (!DrawingFrame.isTextFrameShapeDrawingFrame(frameNode)) { return null; }

    // finding the first paragraph inside the text frame
    paragraph = $(frameNode).find(DOM.PARAGRAPH_NODE_SELECTOR).first();

    if (paragraph.length === 0) { return null; }

    // calculating the first position inside the paragraph
    return getFirstPositionInParagraph(startnode, getOxoPosition(startnode, paragraph));
}

/**
 * Returns the last logical text position inside a specified text frame.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {HTMLElement|jQuery|Array<Number>} drawingFrame
 *  The DOM drawing frame node containing the text, as HTML element or as
 *  jQuery element, or its logical position relative to the passed start
 *  node.
 *
 * @returns {Array<Number>|Null}
 *  The logical position that is the last text position inside the text
 *  frame; or null, if the position cannot be determined.
 */
export function getLastTextPositionInTextFrame(startnode, drawingFrame) {

    var // the node of the text frame
        frameNode = null,
        // the last paragraph or table node
        contentNode = null;

    if (_.isArray(drawingFrame)) {
        frameNode = getDOMPosition(startnode, drawingFrame, true);
        if (frameNode.node) { frameNode = frameNode.node; }
    } else {
        frameNode = drawingFrame;
    }

    if (!DrawingFrame.isTextFrameShapeDrawingFrame(frameNode)) { return null; }

    // finding the last paragraph inside the text frame
    // (behind a table there is always an implicit paragraph, but this might change)
    contentNode = $(frameNode).find(DOM.CONTENT_NODE_SELECTOR).last();

    if (contentNode.length === 0) { return null; }

    // calculating the last position inside the paragraph or table
    return getLastPositionInParagraph(startnode, getOxoPosition(startnode, contentNode));
}

/**
 * Calculating the first logical position of a following table
 * cell. Following means, from left to right. If the last cell in
 * a row is reached, the first cell in the following row is used.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Object}
 *  Returns the first logical position inside a following cell. If the
 *  end of the table is reached, the value for 'endOfTable' is set to
 *  true. Otherwise it is false.
 */
export function getFirstPositionInNextCell(startnode, position) {

    var endOfTable = false,
        cellnode = getLastNodeFromPositionByNodeName(startnode, position, 'td'),
        nextRowSibling;

    function skipNextPbRows(nextRowSibling) {
        while (nextRowSibling && DOM.isTablePageBreakRowNode(nextRowSibling)) {
            nextRowSibling = nextRowSibling.nextSibling;
        }
        return nextRowSibling;
    }

    if (cellnode) {

        if (cellnode.nextSibling) {
            position = getOxoPosition(startnode, cellnode.nextSibling);
            position.push(0);
            position = getFirstPositionInParagraph(startnode, position);
        } else {
            // is this already the last row?
            nextRowSibling = cellnode.parentNode.nextSibling;
            if (nextRowSibling && !DOM.isTablePageBreakRowNode(nextRowSibling)) {  // -> following row
                position = getOxoPosition(startnode, nextRowSibling.firstChild);
                position.push(0);
                position = getFirstPositionInParagraph(startnode, position);
            } else if (nextRowSibling && DOM.isTablePageBreakRowNode(nextRowSibling) && nextRowSibling.nextSibling) { //skip over page break row
                nextRowSibling = skipNextPbRows(nextRowSibling);
                position = getOxoPosition(startnode, nextRowSibling.firstChild);
                position.push(0);
                position = getFirstPositionInParagraph(startnode, position);
            } else { // last row
                position = getLastPositionFromPositionByNodeName(startnode, position, DOM.TABLE_NODE_AND_DRAWING_SELECTOR);
                endOfTable = true;
            }
        }
    }

    return { position, endOfTable };
}

/**
 * Calculating the first logical position of a previous table
 * cell. Previous means, from right to left. If the first cell in
 * a row is reached, the last cell in the previous row is used.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Object}
 *  Returns the first logical position inside a previous cell. If the
 *  begin of the table is reached, the value for 'beginOfTable' is set to
 *  true. Otherwise it is false.
 */
export function getFirstPositionInPreviousCell(startnode, position) {

    var beginOfTable = false,
        cellnode = getLastNodeFromPositionByNodeName(startnode, position, 'td'),
        prevRowSibling;

    function skipPrevPbRows(prevRowSibling) {
        while (prevRowSibling && DOM.isTablePageBreakRowNode(prevRowSibling)) {
            prevRowSibling = prevRowSibling.previousSibling;
        }
        return prevRowSibling;
    }

    if (cellnode) {

        if (cellnode.previousSibling) {
            position = getOxoPosition(startnode, cellnode.previousSibling);
            position.push(0);
            position = getFirstPositionInParagraph(startnode, position);
        } else {
            // is this already the first row?
            prevRowSibling = cellnode.parentNode.previousSibling;
            if (prevRowSibling && !DOM.isTablePageBreakRowNode(prevRowSibling)) {  // -> previous row
                position = getOxoPosition(startnode, prevRowSibling.lastChild);
                position.push(0);
                position = getFirstPositionInParagraph(startnode, position);
            } else if (prevRowSibling && DOM.isTablePageBreakRowNode(prevRowSibling) && prevRowSibling.previousSibling) { //skip over page break row
                prevRowSibling = skipPrevPbRows(prevRowSibling);
                position = getOxoPosition(startnode, prevRowSibling.lastChild);
                position.push(0);
                position = getFirstPositionInParagraph(startnode, position);
            } else {
                beginOfTable = true;
            }
        }
    }

    return { position, beginOfTable };
}

/**
 * Calculating the last logical position of a specified table cell node.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Node|jQuery} cellnode
 *  The cell node for that the last logical position shall be determined.
 *  (This is tested with div.cell elements, but should work with other
 *  nodes, too).
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *  @param {Boolean} [options.ignoreImplicitParagraphs=false]
 *      Whether implicit paragraphs shall be ignored.
 *
 * @returns {Number[]}
 *  Returns the last logical position inside the specified cell.
 */
export function getLastTextPositionInTableCell(startnode, cellnode, options) {

    var position = getOxoPosition(startnode, cellnode);
    position.push(getLastParaIndexInCell(startnode, position, options));
    position = getLastPositionInParagraph(startnode, position);

    return position;
}

/**
 * Calculating the last logical position of a previous table
 * cell. Previous means, from right to left. If the first cell in
 * a row is reached, the last cell in the previous row is used.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *
 * @returns {Object}
 *  Returns the last logical position inside a previous cell. If the
 *  begin of the table is reached, the value for 'beginOfTable' is set to
 *  true. Otherwise it is false.
 */
export function getLastPositionInPrevCell(startnode, position, options) {

    var beginOfTable = false,
        continueSearch = true,
        cellnode = getLastNodeFromPositionByNodeName(startnode, position, 'td'),
        prevRowSibling;

    while ((cellnode) && (continueSearch)) {

        if (cellnode.previousSibling) {
            position = getOxoPosition(startnode, cellnode.previousSibling);
            position.push(getLastParaIndexInCell(startnode, position, options));
            position = getLastPositionInParagraph(startnode, position);
            continueSearch = false;
        } else {
            // is this already the first row?
            prevRowSibling = cellnode.parentNode.previousSibling;
            if (prevRowSibling && !(DOM.isTablePageBreakRowNode(prevRowSibling))) {  // -> previous row
                position = getOxoPosition(startnode, prevRowSibling.lastChild);
                position.push(getLastParaIndexInCell(startnode, position, options));
                position = getLastPositionInParagraph(startnode, position);
                continueSearch = false;
            } else if (prevRowSibling && DOM.isTablePageBreakRowNode(prevRowSibling) && prevRowSibling.previousSibling.lastChild) { //skip over page break row
                position = getOxoPosition(startnode, prevRowSibling.previousSibling.lastChild);
                position.push(getLastParaIndexInCell(startnode, position, options));
                position = getLastPositionInParagraph(startnode, position);
                continueSearch = false;
            } else { // first row
                position = getLastPositionFromPositionByNodeName(startnode, position, DOM.TABLE_NODE_AND_DRAWING_SELECTOR);
                beginOfTable = true;
            }
        }

        if (beginOfTable) {
            // There is no previous cell inside this table. So there is no previous cell
            // or the previous cell is inside an outer table.
            // Position now contains the table/paragraph selection
            cellnode = getLastNodeFromPositionByNodeName(startnode, position, DOM.TABLE_NODE_AND_DRAWING_SELECTOR);

            if (cellnode.previousSibling) {  // this table is not the first table/paragraph
                beginOfTable = true;
                continueSearch = false;  // simply jump into preceeding paragraph/table
            } else {  // this table is the first table/paragraph
                cellnode = getLastNodeFromPositionByNodeName(startnode, position, 'td');
                if (cellnode) {
                    position = getLastPositionFromPositionByNodeName(startnode, position, 'td');
                    beginOfTable = false;
                } else {
                    continueSearch = false;
                }
            }
        }
    }

    return { position, beginOfTable };
}

/**
 * Calculating the first logical position of a table cell
 * specified by the parameter 'cellPosition'.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} cellPosition
 *  The logical position.
 *
 * @returns {Array<Number>}
 *  Returns the first logical position inside the specified cell.
 */
export function getFirstPositionInCurrentCell(startnode, cellPosition) {

    var position = _.copy(cellPosition, true);

    position = getLastPositionFromPositionByNodeName(startnode, position, 'td');
    position.push(0);  // first paragraph or table
    position = getFirstPositionInParagraph(startnode, position);

    return position;
}

/**
 * Calculating the last logical position of a table cell
 * specified by the parameter 'cellPosition'.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} cellPosition
 *  The logical position.
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *
 * @returns {Array<Number>}
 *  Returns the last logical position inside the specified cell.
 */
export function getLastPositionInCurrentCell(startnode, cellPosition, options) {

    var position = _.copy(cellPosition, true);

    position = getLastPositionFromPositionByNodeName(startnode, position, 'td');
    position.push(getLastParaIndexInCell(startnode, position, options));  // last paragraph or table
    position = getLastPositionInParagraph(startnode, position);

    return position;
}

/**
 * Returns the logical offset of the first text span in a paragraph.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  A paragraph node. If this object is a jQuery collection, uses the first
 *  DOM node it contains.
 *
 * @returns {Number}
 *  The start position of the first text component in the passed paragraph.
 */
export function getFirstTextNodePositionInParagraph(paragraph) {

    var offset = 0;

    iterateParagraphChildNodes(paragraph, function (node, nodeStart) {
        if (DOM.isPortionSpan(node)) {
            offset = nodeStart;
            return BREAK;
        }
    }, undefined, { allNodes: true });

    return offset;
}

/**
 * returns the logical offset behind the last text span in a paragraph.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  A paragraph node. If this object is a jQuery collection, uses the first
 *  DOM node it contains.
 *
 * @returns {Number}
 *  The position behind the last text span in the passed paragraph.
 */
export function getLastTextNodePositionInParagraph(paragraph) {

    var offset = 0;

    iterateParagraphChildNodes(paragraph, function (node, nodeStart, nodeLength) {
        if (DOM.isPortionSpan(node)) {
            offset = nodeStart + nodeLength;
        }
    }, undefined, { allNodes: true });

    return offset;
}

/**
 * Checking if a specified logical position describes a position with node and offset,
 * that directly follows or precedes a node, that can be specified with a selector.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {String} selector
 *  A jQuery selector describing a specific node type. This node is the reference for
 *  the position described by the logical position.
 *
 * @param {Object} [options]
 *  An object containing some additional properties.
 *  @param {Boolean} [options.follow=true]
 *      Whether the node at the specified position shall follow
 *      or precede the node specified by the selector.
 *
 * @returns {Boolean}
 *  Whether the position described by the specified logical position is a position directly
 *  before or behind a node specified by a selector.
 */
export function isPositionNeighboringSpecifiedNode(startnode, position, selector, options) {

    var // the 'follow' property
        follow = getBooleanOption(options, 'follow', true),
        // the node info at the specified logical position
        nodeInfo = getDOMPosition(startnode, position),
        // the iterator to find the neighboring node
        iterator = follow ? 'previousSibling' : 'nextSibling',
        // the parent node of the node specified by the logical position
        parent = null;

    if (nodeInfo && nodeInfo.node && _.isNumber(nodeInfo.offset)) {

        // the parent of the (text) node
        parent = nodeInfo.node.parentNode;

        if (parent && DOM.isTextSpan(parent)) {

            // check, if the offset is at the border of the node at the logical position
            if ((follow && (nodeInfo.offset > 0)) || (!follow && (nodeInfo.offset !== $(parent).text().length))) { return false; }

            // checking if the neighboring node is of specified type
            if (parent[iterator] && $(parent[iterator]).is(selector)) { return true; }
        }
    }

    return false;
}

/**
 * After splitting a paragraph, it might be necessary to remove
 * leading empty text spans at the beginning of the paragraph. This
 * is especially important, if there are following floated drawings.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 */
export function removeLeadingEmptyTextSpans(startnode, position) {

    var paraNode = getCurrentParagraph(startnode, position);

    if ((paraNode) && ($(paraNode).children('div.float').length > 0)) {

        var child = paraNode.firstChild,
            continue_ = true;

        while ((child !== null) && (continue_)) {

            if (DOM.isEmptySpan(child)) {
                var removeElement = child;
                child = child.nextSibling;
                $(removeElement).remove();
            } else if ($(child).is('div.float')) {
                child = child.nextSibling;
            } else {
                continue_ = false;
            }
        }
    }

}

/**
 * After splitting a paragraph, it might be necessary to remove leading
 * floating drawings at the beginning of the paragraph.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 */
export function removeLeadingImageDivs(startnode, position) {

    var paraNode = getCurrentParagraph(startnode, position);

    if ((paraNode) && ($(paraNode).children('div.float').length > 0)) {

        var child = paraNode.firstChild;

        while (child) {

            var nextChild = child.nextSibling;

            if (DOM.isOffsetNode(child) && !DOM.isDrawingFrame(nextChild)) {
                var removeElement = child;
                $(removeElement).remove();
            } else if (!DOM.isFloatingDrawingNode(child)) {
                break;
            }
            child = nextChild;
        }
    }

}

/**
 * After splitting a paragraph, it might be necessary to remove
 * divs from images, because they no longer belong to a following
 * image.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node. If this object is a jQuery collection, uses the
 *  first node it contains.
 */
export function removeUnusedDrawingOffsetNodes(paragraph) {

    // find and remove all drawing offset nodes without following drawing node
    iterateSelectedDescendantNodes(paragraph, DOM.OFFSET_NODE_SELECTOR, function (childNode) {
        if (!DOM.isDrawingFrame(childNode.nextSibling)) {
            $(childNode).remove();
        }
    }, undefined, { children: true });
}

/**
 * Changing the value for startPosition and endPosition in that way,
 * that floated drawings and exceeded size tables are not included
 * into the travel process. This is especially important for 'delete'
 * and 'backspace'.
 * Important: This function modifies the parameter 'startPosition'
 * and 'endPosition'.
 *
 * @param {Array<Number>} startPosition
 *  The logical start position of the selection.
 *
 * @param {Array<Number>} endPosition
 *  The logical end position of the selection.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Object} [options]
 *  An object containing some attribute values.
 *  @param {Boolean} [options.backwards=false]
 *      Whether the skipping shall happen in backward direction.
 *
 * @returns {Object}
 *  The new logical position for start and end and the length of
 *  the paragraph of the new start position.
 */
export function skipDrawingsAndTables(startPosition, endPosition, startnode, options) {

    var // whether skipping shall happen in backward direction or not
        backwards = getBooleanOption(options, 'backwards', false),
        // the length of the current paragraph
        paragraphLength = 0,
        // whether the position was modified
        positionModified = false,
        // whether complex field has to be selected and not deleted
        selectComplexField = false,
        // caching current node for passing back
        currentNode = null;

    // skipping over floated drawings, absolute positioned drawings, ranges, complex fields or comment placeholders
    function isSkipNode(node) {
        return DOM.isFloatingDrawingNode(node) ||
            DOM.isAbsoluteParagraphDrawing(node) ||
            (node.parentNode && DOM.isDrawingLayerNode(node.parentNode)) ||
            DOM.isRangeMarkerNode(node) ||
            DOM.isCommentPlaceHolderNode(node) ||
            DOM.isComplexFieldNode(node);
    }

    // skipping floated drawings
    function skipFloatedDrawings() {

        var testPosition,
            node;

        if (backwards) {
            while (startPosition[startPosition.length - 1] > 0) {
                testPosition = _.clone(startPosition);
                testPosition[testPosition.length - 1] -= 1;
                node = getDOMPosition(startnode, testPosition, true).node;

                // is the node at testPosition a floated drawing or a node in the drawing layer?
                if (node && isSkipNode(node)) {
                    if (DOM.isRangeMarkerEndNode(node) && DOM.getRangeMarkerType(node) === 'field') {
                        selectComplexField = true;
                        currentNode = node;
                        break;
                    } else if (DOM.isComplexFieldNode(node)) {
                        selectComplexField = true;
                        currentNode = $(node).prev();
                        break;
                    } else if (DOM.isMarginalNode(node) && node.previousSibling && DOM.isSpecialField(node.previousSibling)) {
                        // check if node is special complex field of type page in header/footer
                        startPosition = getOxoPosition(startnode, node.previousSibling);
                        endPosition = increaseLastIndex(getOxoPosition(startnode, node));
                        break;
                    } else {
                        startPosition[startPosition.length - 1] -= 1;
                        endPosition[endPosition.length - 1] -= 1;
                    }
                } else {
                    break;
                }
            }
        } else {
            while (startPosition[startPosition.length - 1] < paragraphLength) {
                testPosition = _.clone(startPosition);
                node = getDOMPosition(startnode, testPosition, true).node;
                // is the node at testPosition a floated drawing or a node in the drawing layer?
                if (node && isSkipNode(node)) {
                    if (DOM.isRangeMarkerStartNode(node) && DOM.getRangeMarkerType(node) === 'field') {
                        selectComplexField = true;
                        currentNode = node;
                        break;
                    } else if (DOM.isMarginalNode(node) && node.nextSibling && DOM.isSpecialField(node.nextSibling) && node.nextSibling.nextSibling) {
                        // check if node is special complex field of type page in header/footer
                        startPosition = getOxoPosition(startnode, node);
                        endPosition = getOxoPosition(startnode, node.nextSibling.nextSibling); // increasing only required for backspace (40155)
                        break;
                    } else {
                        startPosition[startPosition.length - 1] += 1;
                        endPosition[endPosition.length - 1] += 1;
                    }
                } else {
                    break;
                }
            }
        }

    }

    // skipping over exceeded size tables
    function skipReplacementTables() {

        var tableTestPosition = _.clone(startPosition),
            tableTestDomPos = null,
            jumpOverExceededTables = false;

        if (backwards) {
            if (startPosition[startPosition.length - 1] === 0) {
                tableTestPosition.pop();
                tableTestPosition[tableTestPosition.length - 1] -= 1;
                tableTestDomPos = getDOMPosition(startnode, tableTestPosition);

                // jumping over all replacement tables
                while ((tableTestDomPos) && (DOM.isExceededSizeTableNode(tableTestDomPos.node))) {
                    tableTestPosition[tableTestPosition.length - 1] -= 1;
                    tableTestDomPos = getDOMPosition(startnode, tableTestPosition);
                    jumpOverExceededTables = true;
                }
                if (jumpOverExceededTables) {
                    if (tableTestPosition[tableTestPosition.length - 1] > -1) {
                        startPosition = _.clone(tableTestPosition);
                        startPosition = getLastPositionInParagraph(startnode, startPosition);
                        endPosition = _.clone(startPosition);
                        positionModified = true;  // check again, if there are floated drawings to skip
                    } else { // #33154 backspace on table with position [0, 0] and exceeded size limit
                        startPosition = null;
                        endPosition = null;
                    }
                }
            }
        } else {
            if (startPosition[startPosition.length - 1] === paragraphLength) {
                tableTestPosition.pop();
                tableTestPosition[tableTestPosition.length - 1] += 1;
                tableTestDomPos = getDOMPosition(startnode, tableTestPosition);
                // jumping over all replacement tables
                while ((tableTestDomPos) && (DOM.isExceededSizeTableNode(tableTestDomPos.node))) {
                    tableTestPosition[tableTestPosition.length - 1] += 1;
                    tableTestDomPos = getDOMPosition(startnode, tableTestPosition);
                    jumpOverExceededTables = true;
                }
                if (jumpOverExceededTables) {
                    startPosition = _.clone(tableTestPosition);
                    startPosition = getFirstPositionInParagraph(startnode, startPosition);
                    endPosition = _.clone(startPosition);
                    paragraphLength = getParagraphLength(startnode, startPosition);
                    positionModified = true;  // check again, if there are floated drawings to skip
                }
            }
        }
    }

    // skipDrawingsAndTables
    if (!backwards) {
        paragraphLength = getParagraphLength(startnode, startPosition);
    }

    do {
        positionModified = false;
        skipFloatedDrawings();
        skipReplacementTables();
    } while (positionModified);

    if (selectComplexField) {
        return { selectComplexField: true, node: currentNode };
    }

    return { start: startPosition, end: endPosition, paraLen: paragraphLength };
}

/**
 * This function skips over all floated drawings at the beginning of a paragraph.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 */
export function skipFloatedDrawings(startnode, position) {
    var // logical position
        pos = _.clone(position),
        // the length of the paragraph
        paragraphLength = getParagraphLength(startnode, pos),
        // an html node
        node;

    while (pos[pos.length - 1] < paragraphLength) {
        node = getDOMPosition(startnode, pos, true).node;
        // is the node at testPosition a floated drawing?
        if ((node) && (DOM.isFloatingDrawingNode(node))) {
            pos[pos.length - 1] += 1;
        } else {
            break;
        }
    }

    return pos;
}

/**
 * This function calculates the logical position of a word boundary used as a group of CTRL key and Backspace/Delete.
 *
 * INFO: Although this function looks similar to Position.getWordBoundary,
 * it handles more edge cases as it returns different logical positions,
 * depending of position of whitespace, position of cursor inside word,
 * interpunction characters, inline elements etc.
 *
 * Given a logical start position the search can be done forward (used with Delete key),
 * or backwards (with Backspace key).
 *
 * @param {Node|jQuery} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {Boolean} backwards
 *  Whether the beginning of the word shall be search ('backwards' is true) or
 *  the end of the word ('backwards' is false).
 *
 * @returns {Array<Number>}
 *  The logical position of the range to delete with control key.
 */
export function getRangeForDeleteWithCtrl(startnode, position, backwards) {

    var pos = _.clone(position),
        lastPos = pos.length - 1,
        // current paragraph
        para = null,
        // helper for traversing characters
        char = null,
        // starting character
        startChar = null,
        // one character before starting character
        oneBeforeStartChar = null,
        isInlineComponentNode = false,
        interpunctionChars = ['.', ':', '?', '!', ':', ',', ';', '%', '\xa7', '$', '+', '-', '_', '(', ')', '=', '"', '#', '*', '~', '\'', '[', ']', '{', '}', '&', '/', '\\', '@', '\u20ac'],
        // interpunction plus whitespace characters
        notRegularChars = [' ', '\xa0', '.', ':', '?', '!', ':', ',', ';', '%', '\xa7', '$', '+', '-', '_', '(', ')', '=', '"', '#', '*', '~', '\'', '[', ']', '{', '}', '&', '/', '\\', '@', '\u20ac'],
        // if previous character of starting character is whitespace
        isPrevCharWhitespace,
        // is previous character of starting character an interpunction
        isPrevCharInterpunction,
        // if starting character is first in current paragraph
        isStartCharFirstInPar;

    // Local helper function, that returns the character at a
    // specified position. The position is described by a DOM point,
    // consisting of a node and an offset.
    // If there is no character at this position, null is returned.
    function getCharacter(dompoint) {
        var text = null,
            character = null;

        if (dompoint && dompoint.node) {
            if (DOM.isTextSpan(dompoint.node) && _.isNumber(dompoint.offset)) {
                text = $(dompoint.node).text();
                if (text.length > 0) { character = text.charAt(dompoint.offset); }
            } else if (DOM.isInlineComponentNode(dompoint.node)) {
                isInlineComponentNode = true;
            }
        }
        return character;
    }

    // Local helper function to check if passed character is whitespace.
    function isWhitespaceChar(char) {
        return char === ' ' || char === '\xa0';
    }

    // Local helper function that checks if passed character is one of interpunction characters.
    function isInterpunctionChar(char) {
        return _.contains(interpunctionChars, char);
    }

    // Local helper function that checks if passed character is not whitespace nor interpunction char.
    function isRegularChar(char) {
        return char !== null && !_.contains(notRegularChars, char);
    }

    // Local helper function to extend selection over group of whitespace characters.
    // Optional Boolean parameter [backwards] indicates that pos is decreased.
    // If omited, pos is incresed.
    function jumpOverGroupOfWhitespaces() {
        while (isWhitespaceChar(char)) {
            if (backwards) {
                pos[lastPos]--;
            } else {
                pos[lastPos]++;
            }
            char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        }
    }

    // Local helper function to extend selection over group of regular alphabet charactrs.
    // Optional Boolean parameter [backwards] indicates that pos is decreased.
    // If omited, pos is incresed.
    function jumpOverGroupOfRegularChars() {
        while (isRegularChar(char)) {
            if (backwards) {
                pos[lastPos]--;
            } else {
                pos[lastPos]++;
            }
            char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        }
    }

    // Local helper function to extend selection over group of interpunction characters.
    // Optional Boolean parameter [backwards] indicates that pos is decreased.
    // If omited, pos is incresed.
    function jumpOverGroupOfInterpunctionChars() {
        while (isInterpunctionChar(char)) {
            if (backwards) {
                pos[lastPos]--;
            } else {
                pos[lastPos]++;
            }
            char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        }
    }

    // Local function thats called when DEL is pressed, and text selection (first char) is before whitespace.
    function startWithWhitespace() {
        jumpOverGroupOfWhitespaces();
        if (!isPrevCharWhitespace) {
            pos[lastPos]++;
            char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
            if (isRegularChar(char)) {
                startWithRegularChar();
            }
        }
    }

    // Local function thats called when DEL is pressed, and text selection (first char) is before interpunction.
    function startWithInterpunctionChar() {
        pos[lastPos]++;
        char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        if (!isRegularChar(char)) {
            if (isWhitespaceChar(char)) {
                jumpOverGroupOfWhitespaces();
            } else {
                jumpOverGroupOfInterpunctionChars();
            }
        }
    }

    // Local function thats called when DEL is pressed, and text selection (first char) is before alphabet character.
    function startWithRegularChar() {
        if (isStartCharFirstInPar || isPrevCharWhitespace || isPrevCharInterpunction) {
            jumpOverGroupOfRegularChars();
            if (isInterpunctionChar(char)) {
                if (isPrevCharWhitespace) {
                    if (position[lastPos] - 2 > -1 && !isWhitespaceChar(getCharacter(getDOMPosition(para, [position[lastPos] - 2], true)))) {
                        // NOTICE: changing original position argument by reference!!!
                        position[lastPos]--;
                    }
                }
            } else {
                jumpOverGroupOfWhitespaces();
            }
        } else {
            jumpOverGroupOfRegularChars();
        }
    }

    para = getParagraphElement(startnode, _.initial(pos));
    if (!para) { return pos; }
    startChar = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
    oneBeforeStartChar = getCharacter(getDOMPosition(para, [pos[lastPos] - 1], true));
    char = startChar;

    isPrevCharWhitespace = isWhitespaceChar(oneBeforeStartChar);

    isPrevCharInterpunction = isInterpunctionChar(oneBeforeStartChar);

    isStartCharFirstInPar = _.isUndefined(oneBeforeStartChar);

    // ctrl + backspace
    if (backwards) {
        pos[lastPos]--;
        char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        if ((char === null) && (isInlineComponentNode)) {
            pos[lastPos]++;
            return pos;
        }
        if (isWhitespaceChar(char)) {
            jumpOverGroupOfWhitespaces();
            jumpOverGroupOfRegularChars();
            if (isWhitespaceChar(char) && (isWhitespaceChar(startChar) || isInterpunctionChar(startChar)) && pos[lastPos] > 0) {
                jumpOverGroupOfWhitespaces();
            }
            if (isInterpunctionChar(char) && isRegularChar(startChar) && pos[lastPos] > 0) {
                jumpOverGroupOfInterpunctionChars();
            }
        } else if (isInterpunctionChar(char)) {
            jumpOverGroupOfInterpunctionChars();
            // followed by whitespace
            if (isWhitespaceChar(char) && isWhitespaceChar(startChar) && pos[lastPos] > 0) {
                jumpOverGroupOfWhitespaces();
            }
        } else {
            // regular character
            jumpOverGroupOfRegularChars();
            // followed by whitespace
            if (isWhitespaceChar(char) && (isWhitespaceChar(startChar) || isInterpunctionChar(startChar)) && pos[lastPos] > 0) {
                pos[lastPos]--;
                char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
            }
        }
        // increase for one, because we reduced at the begining to get char before cursor
        pos[lastPos]++;
    } else { // ctrl + delete
        if (isWhitespaceChar(char)) {
            startWithWhitespace();
        } else if (isInterpunctionChar(char)) {
            startWithInterpunctionChar();
        } else {
            // regular character
            startWithRegularChar();
        }
        // Fixing end position at end of paragraph
        if ((char === null) && !isInlineComponentNode) { pos[lastPos]--; }
    }

    return pos;
}

/**
 * Method that returns the character at the specified DOM position.
 *
 * @param {DOM.Point} domPoint
 *  Object containing node and the offset.
 *
 * @returns {String|null} character
 */
export function getCharacterAtDomPoint(domPoint) {
    var text = null,
        character = null;

    if (domPoint && domPoint.node) {
        if ((DOM.isTextSpan(domPoint.node) || DOM.isTextNodeInPortionSpan(domPoint.node)) && _.isNumber(domPoint.offset)) {
            text = $(domPoint.node).text();
            if (text.length > 0) { character = text.charAt(domPoint.offset); }
            if (character === '') { character = null; } // this might happen if offset is the length of the text or bigger
        }
    }
    return character;
}

/**
 * Method that returns the character at the specified logical position.
 *
 * @param {Node|jQuery} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {Position} position
 *  The logical position.
 *
 * @returns {string|null}
 *  The character found at the specified position, or null, if not character was found.
 */
export function getCharacterAtPosition(startNode, position) {
    let character = null;
    const domPoint = getDOMPosition(startNode, position);
    if (domPoint) { character = getCharacterAtDomPoint(domPoint); }
    return character;
}

/**
 * Checks if current selection is inside word, and that should be extended to whole word,
 * for setting set attributes operation.
 *
 * @param {Node|jQuery} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Boolean}
 *  Should set attributes be extended to whole world or not.
 */
export function shouldSetAttsExtendToWord(startNode, position) {
    var pos = _.clone(position);
    var lastPos = pos.length - 1;
    var para = null;
    var startChar = null;
    var prevChar = null;

    // Local helper function that checks if passed character is not whitespace nor interpunction char.
    function isRegularChar(char) {
        return char !== null && !_.contains(WORDBOUNDENDCHARS, char);
    }

    para = getParagraphElement(startNode, _.initial(pos));
    if (!para) { return pos; }
    startChar = getCharacterAtDomPoint(getDOMPosition(para, [pos[lastPos]], true));
    prevChar = getCharacterAtDomPoint(getDOMPosition(para, [pos[lastPos] - 1], true));

    if (isRegularChar(startChar) && isRegularChar(prevChar)) {
        return true;
    }
    return false;
}

/**
 * Checks if current selection is placed on whitespace character.
 *
 * @param {Node|jQuery} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether current selection is placed on whitespace character or not.
 */
export function isWhitespaceCharInPosition(startNode, position) {
    var pos = _.clone(position);
    var lastPos = pos.length - 1;
    var para = null;
    var posChar = null;

    // Local helper function to check if passed character is whitespace.
    function isWhitespaceChar(char) {
        return char === ' ' || char === '\xa0';
    }

    para = getParagraphElement(startNode, _.initial(pos));
    if (!para) { return pos; }
    posChar = getCharacterAtDomPoint(getDOMPosition(para, [pos[lastPos]], true));

    return isWhitespaceChar(posChar);
}

/**
 * returns an array of chars which hint an end of a word,
 * inside are white-spaces and special characters
 *
 * @returns {Array}
 */
export function getWordBoundEndChars() {
    return WORDBOUNDENDCHARS;
}

/**
 * INFO: This function is DEPRECATED. Please use getWordBoundaries
 * or getWordSelection instead.
 *
 * This function calculates the logical position of a word boundary.
 * Given a logical start position the search can be done to the beginning
 * of a word or to the end of a word.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *  (Can be a jQuery object for performance reasons.)
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @param {Boolean} beginning
 *  Whether the beginning of the word shall be searched ('beginning' is true) or
 *  the end of the word ('beginning' is false).
 *
 * @param {Boolean} dontAddSpace
 *  by default if there is a whitespace behind the word, it is included in the result,
 *  if set dontAddSpace on true this does not happened
 *
 * @returns {Array<Number>}
 *  The logical position of the word boundary. If there is a space behind the word,
 *  this space is also added to the word. (can be disabled by 'dontAddSpace')
 */
export function getWordBoundary(startnode, position, beginning, dontAddSpace) {

    // DEPRECATED. Please use getWordBoundaries or getWordSelection instead.

    var // the logical position where the search starts
        pos = _.clone(position),
        // the last index of the specified logical position
        lastPos = pos.length - 1,
        // the paragraph node, that contains the specified logical position
        para = null,
        // one character
        char = null,
        // whether a node is an inline component node
        isInlineComponentNode = false,
        // an array with characters to identify a word boundary
        endChars = WORDBOUNDENDCHARS;

    // Local helper function, that returns the character at a
    // specified position. The position is described by a DOM point,
    // consisting of a node and an offset.
    // If there is no character at this position, null is returned.
    function getCharacter(dompoint) {

        var text = null,
            character = null;

        if (dompoint && dompoint.node) {
            if (DOM.isTextSpan(dompoint.node) && _.isNumber(dompoint.offset)) {
                text = $(dompoint.node).text();
                if (text.length > 0) { character = text.charAt(dompoint.offset); }
            } else if (DOM.isInlineComponentNode(dompoint.node)) {
                // mark inline component and return null, so that iteration stops
                isInlineComponentNode = true;
            }
        }

        return character;
    }

    // receiving the current paragraph node
    para = getParagraphElement(startnode, _.initial(pos));
    if (!para) { return pos; }

    char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));

    if (beginning) {

        while ((pos[lastPos] > 0) && (char !== null) && !_.contains(endChars, char)) {
            pos[lastPos]--;
            // TODO: reducing using of getDOMPosition()
            char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        }

        // Removing special chars from beginning of string
        if (_.contains(endChars, char)) { pos[lastPos]++; }
        // Fixing start position behind inline components
        if ((char === null) && (isInlineComponentNode)) { pos[lastPos]++; }

    } else {

        while ((char !== null) && !_.contains(endChars, char)) {
            pos[lastPos]++;
            // TODO: reducing using of getDOMPosition()
            char = getCharacter(getDOMPosition(para, [pos[lastPos]], true));
        }

        // Adding space at end to string
        if (char === ' ' && !dontAddSpace) { pos[lastPos]++; }
        // Fixing end position at end of paragraph
        if ((char === null) && !isInlineComponentNode) { pos[lastPos]--; }
    }

    return pos;
}

/**
 * This function calculates the logical border positions of a word at a
 * specified logical position.
 *
 * Info: This is a convenience function with predefined values and a return value
 * that can be directly used for setting a selection. Internally it uses the
 * function 'getWordSelection()' that shall be the only function
 * that looks for word boundaries.
 *
 * Info: The return value is an array of one or two logical positions. This return
 * value can directly be used to set the selection with:
 * selection.setTextSelection.apply(selection, getWordBoundaries(...));
 *
 * @param {Node} rootnode
 *  The root node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position for that the word boundaries shall be determined.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.addFinalSpaces=false]
 *      If set to true, all spaces following the word are included into the calculated
 *      selection of the word boundaries.
 *
 * @returns {Array<Number>[]}
 *  An array containing the two logical positions for the two calculated word
 *  boundaries. The first logical position is the start position of the word,
 *  the second position is the logical end position of the word. This is the
 *  position BEHIND(!) the last character of the word.
 *  If no word boundaries can be found, an array that contains only the specified
 *  logical position, is returnd.
 */
export function getWordBoundaries(rootnode, position, options) {

    var // the logical position where the search starts
        pos = _.clone(position),
        // the logical position of the paragraph
        paraPos = _.initial(pos),
        // the paragraph node, that contains the specified logical position
        para = getParagraphElement(rootnode, _.initial(pos)),
        // an array with characters to identify a word boundary
        endChars = WORDBOUNDENDCHARS,
        // increasing the position for a final space
        addFinalSpaces = getBooleanOption(options, 'addFinalSpaces', false),
        // the word selection object
        wordSelection = null;

    if (!para) { return pos; }

    wordSelection = getWordSelection(para, _.last(position), endChars, { addFinalSpaces, ignoreText: true });

    return wordSelection ? [paraPos.concat(wordSelection.start), paraPos.concat(wordSelection.end)] : [pos];
}

/**
 * Searching a word inside a paragraph at a specified position. For a specified paragraph
 * and a number that is the logical position inside the specified paragraph, a complete
 * word is searched around the position. As word boundaries a list of characters can be
 * defined. If it is not set, a default list is used.
 * The searched word can contain all valid elements, that can be located inside a word in
 * the document. This includes range markers, or placeholders for comments or drawings.
 *
 * @param {Node|jQuery} paragraph
 *  The paragraph node, in which the word is searched.
 *
 * @param {Number} pos
 *  The logical position inside the paragraph, around that the word is searched.
 *
 * @param {String[]} [charList]
 *  An optional list of character values, that will be used to find the word boundaries. If
 *  not specified, a default list is used.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.onlyLeft=false]
 *      If set to true, only the word boundary left of the specified position is searched.
 *      In this case, no check to the right is done. Instead the specified logical start
 *      position for the search is used as right boundary.
 *  @param {Boolean} [options.returnTextSpan=false]
 *      If set to true, the text span node, that is located at the specified logical
 *      position will be included into the return object, if possible.
 *  @param {Boolean} [options.addFinalSpaces=false]
 *      If set to true, all spaces following the word are included into the calculated
 *      selection. This can be used for double clicking.
 *  @param {Boolean} [options.ignoreText=false]
 *      If set to true, the text string is not generated. This can be used for performance
 *      reasons, if the string is not required as return value.
 *  @param {Boolean} [options.returnAllTextSpans=false]
 *      If set to true, collect all spans of the word and add it in the return object with
 *      name 'nodes'
 *  @param {Boolean} [options.fillPlaceholders=false]
 *      If set to true, for RangeMarkerNodes & CommentPlaceHolderNodes
 *      whitespaces to the resulting text will be added
 *
 * @returns {Object|Null}
 *  An object, that contains the properties 'start', 'end', 'text' and 'node'. 'start' is the
 *  logical start position of the first character of the word inside the paragraph, 'end' is
 *  the logical end position of the character BEHIND(!) the word inside the paragraph, 'text'
 *  contains the complete word as string and 'node' contains a link to the text span node, that
 *  contains the specified position. 'node' is only set, if the option 'returnTextSpan' is
 *  set to true.
 *  If no word is found, null is returned.
 */
export function getWordSelection(paragraph, pos, charList, options) {

    var // the dom point specified by the paragraph and the position
        textPoint = getDOMPosition(paragraph, [pos]),
        // the list of characters used to find the word boundaries
        list = charList || WORDBOUNDENDCHARS,
        // whether a final space shall be included into the selection
        addFinalSpaces = getBooleanOption(options, 'addFinalSpaces', false),
        // whether the word text as a string can be ignored (for performance reasons)
        ignoreText = getBooleanOption(options, 'ignoreText', false),
        // whether only the left border of the word needs to be determined
        onlyLeft = getBooleanOption(options, 'onlyLeft', false),
        // whether the text span at the specified position shall be returned
        returnTextSpan = getBooleanOption(options, 'returnTextSpan', false),
        // helper nodes to iterate to start and end of paragraph
        startNode = null, endNode = null,
        // helper text span nodes
        startTextNode = null, endTextNode = null,
        // the logical start and end positions
        startPos = null, endPos = null,
        // the offsets inside the start text node and end text node
        startNodeOffset = -1, endNodeOffset = -1,
        // whether a word start and a word end was found
        foundStart = false, foundEnd = false,
        // the length of the text of a text span node
        nodeLength = 0,
        // the text of a text span node
        nodeText = '',
        // the complete word text
        wordText = '',
        // the result object, that will be returned
        result = null,
        // all spans of the word will be returned
        returnAllTextSpans = getBooleanOption(options, 'returnAllTextSpans', false),
        // the array for the option returnAllTextSpans
        textSpans = [];

    var fillPlaceholders = getBooleanOption(options, 'fillPlaceholders', false);

    // helper function to check if passed character is whitespace.
    function isWhitespaceChar(char) {
        return char === ' ' || char === '\xa0';
    }

    // helper function to add final spaces to a word boundary
    function expandFinalSpaces(nodeText, pos) {
        while (isWhitespaceChar(nodeText[pos])) { pos++; }
        return pos;
    }

    function addTextSpan(span) {
        if (returnAllTextSpans) {
            textSpans.push(span);
        }
    }

    if (textPoint && textPoint.node && DOM.isTextSpan(textPoint.node.parentNode)) {

        startNode = endNode = startTextNode = endTextNode = textPoint.node.parentNode;

        // Searching in current node to left and right (search to left needs a decrease of offset)
        nodeText = $(endNode).text();
        startNodeOffset = indexOfValuesInsideTextSpan(nodeText, textPoint.offset > 0 ? textPoint.offset - 1 : 0, list, { reverse: true });
        if (startNodeOffset >= 0) {
            startNodeOffset++; // skip to following character
            foundStart = true;
        }

        if (onlyLeft) {
            endNodeOffset = textPoint.offset;
            foundEnd = true; // -> only search on left side
        } else {
            endNodeOffset = indexOfValuesInsideTextSpan(nodeText, textPoint.offset, list);
            if (endNodeOffset >= 0) {
                if (addFinalSpaces && isWhitespaceChar(nodeText[endNodeOffset])) { endNodeOffset = expandFinalSpaces(nodeText, endNodeOffset); }
                foundEnd = true;
            }
        }

        // selecting text from current span
        if (foundStart && foundEnd) {
            wordText = ignoreText ? '' : nodeText.slice(startNodeOffset, endNodeOffset);
        } else if (foundStart && !foundEnd) {
            wordText = ignoreText ? '' : nodeText.slice(startNodeOffset);
        } else if (!foundStart && foundEnd) {
            wordText = ignoreText ? '' : nodeText.slice(0, endNodeOffset);
        } else {
            wordText = ignoreText ? '' : nodeText;
        }

        // searching in following text spans to the right
        while (!foundEnd && endNode && endNode.nextSibling && (DOM.isTextSpan(endNode.nextSibling) || DOM.isValidNodeInsideWord(endNode.nextSibling))) {

            endNode = endNode.nextSibling;

            if (DOM.isTextSpan(endNode)) {
                endTextNode = endNode;
                nodeText = $(endNode).text();
                endNodeOffset = indexOfValuesInsideTextSpan(nodeText, 0, list);

                if (endNodeOffset >= 0) {
                    if (addFinalSpaces && isWhitespaceChar(nodeText[endNodeOffset])) { endNodeOffset = expandFinalSpaces(nodeText, endNodeOffset); }
                    if (endNodeOffset > 0) { wordText = ignoreText ? '' : wordText.concat(nodeText.slice(0, endNodeOffset)); }
                    foundEnd = true;
                } else {
                    wordText = ignoreText ? '' : wordText.concat(nodeText);
                }
                addTextSpan(endNode);
            } else if (!ignoreText && fillPlaceholders && (DOM.isRangeMarkerNode(endNode) || DOM.isSubstitutionPlaceHolderNode(endNode))) {
                wordText = wordText.concat(' ');
            }
        }

        // handling the final text span in a paragraph
        if (!foundEnd && endTextNode) {
            endNodeOffset = nodeText.length;
            foundEnd = true;
        }

        // searching in previous text spans to the left
        while (!foundStart && startNode && startNode.previousSibling && (DOM.isTextSpan(startNode.previousSibling) || DOM.isValidNodeInsideWord(startNode.previousSibling))) {

            startNode = startNode.previousSibling;

            if (DOM.isTextSpan(startNode)) {
                startTextNode = startNode;
                nodeText = $(startNode).text();
                nodeLength = nodeText.length;
                startNodeOffset = indexOfValuesInsideTextSpan(nodeText, nodeLength, list, { reverse: true });

                if (startNodeOffset >= 0) {
                    startNodeOffset++; // skip to following character
                    wordText = ignoreText ? '' : nodeText.slice(startNodeOffset).concat(wordText);
                    foundStart = true;
                } else {
                    wordText = ignoreText ? '' : nodeText.concat(wordText);
                }
                addTextSpan(startNode);
            } else if (!ignoreText && fillPlaceholders && (DOM.isRangeMarkerNode(startNode) || DOM.isSubstitutionPlaceHolderNode(startNode))) {
                wordText = ' '.concat(wordText);
            }
        }

        // handling the first text span in a paragraph
        if (!foundStart && startTextNode) {
            startNodeOffset = 0;
            foundStart = true;
        }

        // returning to logical positions
        if (foundStart && foundEnd) {
            startPos = getOxoPosition(paragraph, startTextNode, startNodeOffset);
            endPos = getOxoPosition(paragraph, endTextNode, endNodeOffset);

            if (startPos && endPos) {
                addTextSpan(textPoint.node.parentNode);
                result = { start: startPos[0], end: endPos[0], text: wordText, node: (returnTextSpan ? textPoint.node.parentNode : null), nodes: (returnAllTextSpans ? textSpans : null) };
            }
        }
    }

    return result;
}

/**
 * Finding a valid selection corresponding to a given logical start or end
 * position.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} startPos
 *  The logical start position.
 *
 * @param {Array<Number>|Null|undefined} endPos
 *  The optional logical end position, can also be undefined or null.
 *
 * @param {Array<Number>} lastPos
 *  The last logical position of the document.
 *
 * @param {Boolean} isSlideMode
 *  If it is slide mode or not.
 *
 * @returns {Object}
 *  An object containing two logical positions with the property names 'start'
 *  and 'end'. The property value for 'end' can be null.
 */
export function findValidSelection(startnode, startPos, endPos, lastPos, isSlideMode) {

    var // an object, that will be filled with valid logical start and end positions
        validPos = { },
        // start position of content node
        contentNodeStartPos = isSlideMode ? _.initial(startPos) : [_.first(startPos)],
        // end position of content node
        contentNodeEndPos = isSlideMode ? _.initial(endPos) : [_.first(endPos)],
        // the paragraph/table at the start position
        startPara = getContentNodeElement(startnode, contentNodeStartPos),
        // the paragraph/table at the end position
        endPara = _.isArray(endPos) ? getContentNodeElement(startnode, contentNodeEndPos) : null,
        // the text position of the start position
        startTextPos = _.last(startPos),
        // the text position of the end position
        endTextPos = _.isArray(endPos) ? _.last(endPos) : null,
        // the parameter length
        paraLength = 0,
        // valid logical positions for start and end
        validStartPos = null, validEndPos = null;

    if (!startPara) {
        // even the start paragraph does not exist anymore -> setting selection to end of document
        validPos.start = lastPos;
        validPos.end = null;
    } else {
        // calculation new start value
        validStartPos = getFirstPositionInParagraph(startnode, contentNodeStartPos);
        startPara = getParagraphElement(startnode, _.initial(validStartPos));
        paraLength = getParagraphNodeLength(startPara);
        validStartPos[validStartPos.length - 1] = (startTextPos < paraLength) ? startTextPos : paraLength;

        // calculation new end value
        if (endPara) {
            validEndPos = getFirstPositionInParagraph(startnode, contentNodeEndPos);
            endPara = getParagraphElement(startnode, _.initial(validEndPos));
            paraLength = getParagraphNodeLength(endPara);
            validEndPos[validEndPos.length - 1] = (endTextPos < paraLength) ? endTextPos : paraLength;
        }

        validPos.start = validStartPos;
        validPos.end = validEndPos;
    }

    return validPos;
}

// position arrays --------------------------------------------------------

/**
 * Returns whether the passed logical position is an existing text position.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} position
 *  The logical position.
 *
 * @returns {Boolean}
 *  Whether the logical positions is an existing text position.
 */
export function isValidTextPosition(startnode, position) {
    if (!startnode || !position || _.isEmpty(position)) {
        return false;
    }
    var nodeInfo = getDOMPosition(startnode, position, true);
    return (nodeInfo && nodeInfo.node && DOM.isTextSpan(nodeInfo.node) && _.isNumber(nodeInfo.offset));
}

/**
 * Function, that splits a specified range defined by the logical positions
 * start and end into chunks with width 'maxSize'.
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {Array<Number>} start
 *  The logical start position of the range
 *
 * @param {Array<Number>} end
 *  The logical end position of the range
 *
 * @param {Number} maxSize
 *  The maximum value of top level elements inside the range specified by
 *  the two logical positions.
 *
 * @returns {Array<Array<Number>>}
 *  An array of arrays containing two logical positions. The first array in
 *  each array describes the logical start position, the second the logical
 *  end position.
 */
export function splitLargeSelection(startnode, start, end, maxSize) {

    var // the array with the splitted operation ranges
        splittedRanges = [],
        // the index of the paragraph position inside the start and end positions
        paraIndex = getParagraphIndex(start, end),
        // the index of the start paragraph
        startPara = start[paraIndex],
        // the index of the end paragraph
        endPara = end[paraIndex],
        // a current start paragraph index
        currentStartPara = startPara,
        // a current end paragraph index
        currentEndPara = endPara,
        // the common logical postion of start and end position
        paraParentPos = (paraIndex > 0) ? start.slice(0, paraIndex) : [],
        // the start position for the current paragraph
        currentStartParaPos = null,
        // the end position for the current paragraph
        currentEndParaPos = null;

    // Helper function to detect the index of the paragraph inside the specified start and end postions.
    // It is also checked, if the paragraph in start and end positions have the same parent.
    function getParagraphIndex(posA, posB) {

        var paraIndex = 0;
        var paraPos = null;
        var domPoint = null;
        var posALength = posA.length;
        var posBLength = posB.length;

        if ((posALength === 2 && posBLength === 2) || (posALength !== posBLength)) { return paraIndex; } // top level text selection

        // check, if the selection is inside one drawing or one cell
        paraPos = _.initial(start);

        if (hasSameParentComponentAtSpecificIndex(posA, posB, paraPos - 1)) {
            domPoint = getDOMPosition(startnode, paraPos);
            if (domPoint && domPoint.node && DOM.isParagraphNode(domPoint.node)) {
                paraIndex = paraPos.length - 1;
            }
        }

        return paraIndex;
    }

    // check for a parent of a paragraph, that is equal in start and end

    if ((endPara - startPara) < maxSize) {
        splittedRanges.push([start, end]);
        return splittedRanges;
    }

    // so there is a selection that includes at least 100 top level elements (or elements inside one drawing or cell)
    currentEndPara = startPara + maxSize;

    while (currentEndPara < endPara) {

        currentEndParaPos = appendNewIndex(paraParentPos, currentEndPara);

        if (startPara === currentStartPara) {
            splittedRanges.push([start, getLastPositionInParagraph(startnode, currentEndParaPos)]);
        } else {
            currentStartParaPos = appendNewIndex(paraParentPos, currentStartPara);
            splittedRanges.push([getFirstPositionInParagraph(startnode, currentStartParaPos), getLastPositionInParagraph(startnode, currentEndParaPos)]);
        }

        currentStartPara = currentEndPara + 1;
        currentEndPara += maxSize;  // for example from 101 to 200
    }

    // also adding the final selection range
    currentStartParaPos = appendNewIndex(paraParentPos, currentStartPara);
    splittedRanges.push([getFirstPositionInParagraph(startnode, currentStartParaPos), end]);

    return splittedRanges;
}

/**
 * Returns whether the passed logical positions are located in the same
 * parent component (all array elements but the last are equal).
 *
 * @param {Array<Number>} position1
 *  The first logical position.
 *
 * @param {Array<Number>} position2
 *  The second logical position.
 *
 * @param {Number} [parentLevel=1]
 *  The number of parent levels. If omitted, the direct parents of the
 *  logical positions will be checked (only the last element of each
 *  position array will be ignored). Otherwise, the specified number of
 *  trailing array elements will be ignored (for example, a value of 2
 *  checks the grand parents).
 *
 * @returns {Boolean}
 *  Whether the logical positions are located in the same parent component.
 */
export function hasSameParentComponent(position1, position2, parentLevel) {

    if (!position1 || !position2) {
        return false;
    }

    var index = 0, length = position1.length;

    // length of both positions must be equal
    parentLevel = _.isNumber(parentLevel) ? parentLevel : 1;
    if ((length < parentLevel) || (length !== position2.length)) {
        return false;
    }

    // compare all array elements but the last ones
    for (index = length - parentLevel - 1; index >= 0; index -= 1) {
        if (position1[index] !== position2[index]) {
            return false;
        }
    }

    return true;
}

/**
 * Returns whether the passed logical positions have the same values from the
 * beginning until a specified index. With this function it is possible to
 * check, if two positions of different length share the same parent at a
 * specified index.
 * Example:
 * Position [1, 8] and postion [1, 12, 0, 5] (text position in a text frame)
 * Checking at index '1', the text position at [1, 8] is compared with the
 * drawing position at [1, 12]. In this case, both have the same parent at
 * index '1'. Therefore this function should return true.
 *
 * @param {Number[]} position1
 *  The first logical position.
 *
 * @param {Number[]} position2
 *  The second logical position.
 *
 * @param {Number} index
 *  The index inside the logical positions that need to be compared. This number
 *  is not 0 based. If index is 2, the first two positions are compared. This
 *  is bases on _.first, where the second parameter n returns the first n elements
 *  of an array.
 *
 * @returns {Boolean}
 *  Whether the logical positions have relative to the specified index the same
 *  parent.
 */
export function hasSameParentComponentAtSpecificIndex(position1, position2, index) {

    if (!position1 || !position2) {
        return false;
    }

    if (position1.length < index || position2.length < index) {
        return false;
    }

    return _.isEqual(_.first(position1, index), _.first(position2, index));
}

/**
 * Validates the correct order of two given logical positions pos1 and
 * pos2. After running this function, pos1 will describe an element
 * that is in the dom before the element at the logical position pos2.
 *
 * @param {Nullable<Position>} pos1
 *  The first logical position.
 *
 * @param {Nullable<Position>} pos2
 *  The second logical position.
 *
 * @returns {Boolean}
 *  Whether the two logical positions have the correct order.
 */
export function isValidPositionOrder(pos1, pos2) {
    // missing positions: no error in order
    return !pos1 || !pos2 || (comparePositions(pos1, pos2) <= 0);
}

/**
 * Checks if given node position is inside interval of start and end numbers.
 * It is valid to check only inside one paragraph, because it compares only last node position.
 *
 * @param {Array<Number>} nodePos
 *  OXO position of element.
 *
 * @param {Number} startOffset
 *  Start value of interval.
 *
 * @param {Number} endOffset
 *  End value of interval.
 *
 * @returns {Boolean}
 *  Whether the node position is inside given interval.
 *
 */
export function isNodePositionInsideRange(nodePos, startOffset, endOffset) {
    return _.isArray(nodePos) && (startOffset <= _.last(nodePos)) && (_.last(nodePos) <= endOffset);
}

// iteration --------------------------------------------------------------

/**
 * Calls the passed iterator function for all or selected child elements in
 * a paragraph node (text spans, text fields, drawings, and other helper
 * nodes).
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph element whose child nodes will be visited. If this object
 *  is a jQuery collection, uses the first DOM node it contains.
 *
 * @param {Function} iterator
 *  The iterator function that will be called for every matching node.
 *  Receives the following parameters:
 *      (1) {HTMLElement} the DOM node object,
 *      (2) {Number} the logical start index of the node in the paragraph,
 *      (3) {Number} the logical length of the node,
 *      (4) {Number} the offset of the covered part in the visited node,
 *          relative to its start,
 *      (5) {Number} the length of the covered part of the child node.
 *  The last two parameters are important if the options 'options.start'
 *  and 'options.end' will be used to iterate over a specific sub-range in
 *  the paragraph where the first and last visited text nodes may be
 *  covered only partially. Note that text components (e.g. fields or tabs)
 *  and drawing nodes have a logical length of 1, and other helper nodes
 *  that do not represent editable contents have a logical length of 0. If
 *  the iterator returns the BREAK object, the iteration process will
 *  be stopped immediately.
 *
 * @param {Object} [context]
 *  If specified, the iterator will be called with this context (the symbol
 *  'this' will be bound to the context inside the iterator function).
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.allNodes=false]
 *      If set to true, all child nodes of the paragraph will be visited,
 *      also helper nodes that do not represent editable content and have a
 *      logical length of 0. Otherwise, only real content nodes will be
 *      visited (non-empty text portions, text fields, and drawing nodes).
 *  @param {Number} [options.start]
 *      The logical index of the first text component to be included into
 *      the iteration process. Text spans covered partly will be visited
 *      too.
 *  @param {Number} [options.end]
 *      The logical index of the last text component to be included in
 *      the iteration process (closed range). Text spans covered partly
 *      will be visited too.
 *  @param {Boolean} [options.split=false]
 *      If set to true, the first and last text span not covered completely
 *      by the specified range will be split before the iterator function
 *      will be called. The iterator function will always receive a text
 *      span that completely covers the contained text.
 *
 * @returns {BREAK|Undefined}
 *  A reference to the BREAK object, if the iterator has returned
 *  BREAK to stop the iteration process, otherwise undefined.
 */
export function iterateParagraphChildNodes(paragraph, iterator, context, options) {

    var // whether to visit all child nodes
        allNodes = getBooleanOption(options, 'allNodes', false),
        // logical index of first node to be visited
        rangeStart = getIntegerOption(options, 'start'),
        // logical index of last node to be visited
        rangeEnd = getIntegerOption(options, 'end'),
        // split partly covered text spans before visiting them
        split = getBooleanOption(options, 'split', false),
        // the logical start index of the visited content node
        nodeStart = 0,
        // result of the iteration
        result = null;

    // visit the content nodes of the specified paragraph element (only child nodes, no other descendants)
    result = iterateDescendantNodes(paragraph, function (node) {

        var // the logical length of the node
            nodeLength = 0,
            // start offset of partly covered nodes
            offsetStart = 0,
            // offset length of partly covered nodes
            offsetLength = 0,
            // whether node is a regular text span
            isTextSpan = DOM.isTextSpan(node),
            // whether node is located before the range start point
            isBeforeStart = false;

        // calculate length of the node
        if (isTextSpan) {
            // portion nodes contain regular text
            nodeLength = DOM.isTextFrameTemplateTextSpan(node) ? 0 : node.firstChild.nodeValue.length;
        } else if (DOM.isSpecialField(node)) {
            nodeLength = $(node).data('length') || 1;
        } else if (DOM.isTextComponentNode(node) || DOM.isDrawingFrame(node)) {
            // special text components (e.g. fields, tabs) and drawings count as one character
            nodeLength = 1;
        }

        // node starts after the specified end index (escape from iteration)
        if (_.isNumber(rangeEnd) && (rangeEnd < nodeStart)) {
            return BREAK;
        }

        // node ends before the specified start index
        isBeforeStart = _.isNumber(rangeStart) && (nodeStart + nodeLength <= rangeStart);

        // always visit non-empty nodes, but skip nodes before the start position
        if (!isBeforeStart && (allNodes || (nodeLength > 0))) {

            // calculate offset start and length of partly covered text nodes
            offsetStart = _.isNumber(rangeStart) ? Math.max(rangeStart - nodeStart, 0) : 0;
            offsetLength = (_.isNumber(rangeEnd) ? Math.min(rangeEnd - nodeStart + 1, nodeLength) : nodeLength) - offsetStart;

            // split first text span (insert new span before current span)
            if (split && isTextSpan && (offsetStart > 0)) {
                DOM.splitTextSpan(node, offsetStart);
                nodeStart += offsetStart;
                nodeLength -= offsetStart;
                offsetStart = 0;
            }

            // split last text span (insert new span before current span)
            if (split && isTextSpan && (offsetLength < nodeLength)) {
                DOM.splitTextSpan(node, offsetLength, { append: true });
                nodeLength = offsetLength;
            }

            // call the iterator for the current content node
            if (iterator.call(context, node, nodeStart, nodeLength, offsetStart, offsetLength) === BREAK) {
                return BREAK;
            }
        }

        // update start index of next visited node
        nodeStart += nodeLength;

    }, undefined, { children: true });

    return result;
}

/**
 * Calls the passed iterator function for all or selected child elements in
 * a row node (only cell nodes, 'th# and 'td').
 *
 * @param {HTMLElement|jQuery} row
 *  The row element whose child nodes will be visited. If this object
 *  is a jQuery collection, uses the first DOM node it contains.
 *
 * @param {Function} iterator
 *  The iterator function that will be called for every matching node.
 *  Receives the DOM cell node object as first parameter, the logical start
 *  index of the node in the row as second parameter, and the logical
 *  length (colspan) of the cell as third parameter.
 *  If the iterator returns the BREAK object, the iteration process
 *   will be stopped immediately.
 *
 * @param {Object} [context]
 *  If specified, the iterator will be called with this context (the symbol
 *  'this' will be bound to the context inside the iterator function).
 *
 * @returns {BREAK|Undefined}
 *  A reference to the BREAK object, if the iterator has returned
 *  BREAK to stop the iteration process, otherwise undefined.
 */
export function iterateRowChildNodes(row, iterator, context) {

    var // the logical start index of the visited cell node
        cellNodeStart = 0,
        // result of the iteration
        result = null;

    // visit the content nodes of the specified cell element (only child nodes, no other descendants)
    result = iterateDescendantNodes(row, function (cellNode) {

        var currentColSpan = parseIntAttribute(cellNode, 'colspan', 1);

        // call the iterator for the current content node
        if (iterator.call(context, cellNode, cellNodeStart, currentColSpan) === BREAK) {
            return BREAK;
        }

        // update start index of next visited node
        cellNodeStart += currentColSpan;

    }, undefined, { children: true });

    return result;
}

// pixel positions---------------------------------------------------------

/**
 * Calculating the pixel position of a node relative to a specified root
 * node.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The start node for which the relative pixel position is calculated.
 *
 * @param {HTMLElement|jQuery} node
 *  The node, whose pixel position relative to the root node shall
 *  be calculated.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @returns {Object}
 *  An object containing the properties x and y, that contain the horizontal and
 *  vertical offset in pixel relative to the specified root node.
 */
export function getPixelPositionToRootNodeOffset(rootNode, node, zoomFactor) {

    var // the offset position of the root node
        rootOffset = $(rootNode).offset(),
        // the offset position of the search node
        nodeOffset = $(node).offset();

    // setting default value to 100% and converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    return {
        x: math.roundp((nodeOffset.left - rootOffset.left) / zoomFactor, 0.01),
        y: math.roundp((nodeOffset.top - rootOffset.top) / zoomFactor, 0.01)
    };
}

/**
 * Calculating the pixel position of a node relative to a specified root
 * node, with a difference that takes rotation angle in calculation.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The start node for which the relative pixel position is calculated.
 *
 * @param {HTMLElement|jQuery} node
 *  The node, whose pixel position relative to the root node shall
 *  be calculated.
 *
 * @param {Number|Null} angle
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @returns {Object}
 *  An object containing the properties x and y, that contain the horizontal and
 *  vertical offset in pixel relative to the specified root node.
 */
export function getUnrotatedPixelPositionToRoot(rootNode, node, angle, zoomFactor) {

    var // the offset position of the root node
        rootOffset = $(rootNode).offset(),
        // the offset position of the search node
        nodeOffset,
        $node = $(node);

    if (_.isNumber(angle) && (angle !== 0)) {
        $node.css('transform', ''); // reset to def to get values
        nodeOffset = $node.offset();
        DrawingFrame.updateCssTransform(node, angle);
    } else {
        nodeOffset = $node.offset();
    }

    return {
        x: Math.round((nodeOffset.left - rootOffset.left) / zoomFactor),
        y: Math.round((nodeOffset.top - rootOffset.top) / zoomFactor)
    };
}

/**
 * Converting a specified pixel position (with values x and y) that is relative
 * to a specified root node (this can be the div.page node) to a pixel position
 * relative to the document (the browser window). The specified pixel position can
 * come from the $.position() function. The returned pixel position can be
 * handled with event.clientX/Y, event.pageX/Y or from the $.offset() function.
 *
 * @param {Number} x
 *  The start node for which the relative pixel position is calculated. This
 *  is typically the page node (div.page)
 *
 * @param {Number} y
 *  The start node for which the relative pixel position is calculated. This
 *  is typically the page node (div.page)
 *
 * @param {HTMLElement} rootNode
 *  The node, for which the pixel postition relative to the document (the browser
 *  window) shall be calculated.
 *
 * @returns {Object}
 *  An object containing the properties x and y, that contain the horizontal and
 *  vertical offset in pixel relative to the document (the browser window).
 */
export function convertRootNodePixelToDocumentPixel(x, y, rootNode) {
    return { x: x + $(rootNode).offset().left, y: y + $(rootNode).offset().top };
}

/**
 * Converting a specified pixel position (with values x and y) that is relative
 * to the document (the browser window) to a pixel position relative to a specified
 * root node (this can be the div.page node). The specified pixel position can
 * come from event.clientX/Y, event.pageX/Y or from the $.offset() function.
 * The returned pixel position can then be handled for example with the
 * $.position() function.
 *
 * @param {Number} x
 *  The start node for which the relative pixel position is calculated. This
 *  is typically the page node (div.page)
 *
 * @param {Number} y
 *  The start node for which the relative pixel position is calculated. This
 *  is typically the page node (div.page)
 *
 * @param {HTMLElement} rootNode
 *  The node, for which the pixel postition relative to the specified root node
 *  shall be calculated.
 *
 * @returns {Object}
 *  An object containing the properties x and y, that contain the horizontal and
 *  vertical offset in pixel relative to the specified root node.
 */
export function convertDocumentPixelToRootNodePixel(x, y, rootNode) {
    return { x: x - $(rootNode).offset().left, y: y - $(rootNode).offset().top };
}

/**
 * Calculating the text offset inside (number of characters before) a text span
 * given a pixel position x and y. This function uses a binary splitting to
 * determine the selected character. It is important to specify the correct
 * pixel values. Two versions are supported, that can be switched using the
 * useOffset-option (see below):
 *  1. pixels are calculated corresponding to the offset parent. This is
 *     typically the paragraph node. In this case the jQuery function
 *     $.position() can be used.
 *  2. pixels are calculated corresponding to the document. This can be a
 *     position received by an event using event.pageX or event.clientX. In this
 *     case the jQuery function $.offset() can be used.
 *
 * @param {Node} searchSpan
 *  The text node, for which the text offset shall be calculated. This node
 *  must not be a jQuery element.
 *
 * @param {Number} x
 *  The horizontal offset. Dependent from the parameter 'useOffset' (see below)
 *  this can be an offset relative to the document (upper left corner of the
 *  browser window, using $.offset()) or relative to the offset parent (using
 *  $.position()).
 *
 * @param {Number} y
 *  The vertical offset. Dependent from the parameter 'useOffset' (see below)
 *  this can be an offset relative to the document (upper left corner of the
 *  browser window, using $.offset()) or relative to the offset parent (using
 *  $.position()).
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.useOffset=false]
 *      If set to true, the x and y position are calculated via $.offset() function
 *      (relative to document). The alternative is the position relative to the offset
 *      parent, that is received via $.position().
 *  @param {Boolean} [options.checkForNewLine=false]
 *      If set to true, a special mode is used to find the beginning text positions
 *      of each line in a paragraph. Setting this values increases the precision. In
 *      multi line text spans it can happen, that a drawing that is larger than a text
 *      line causes the new line to be recognized at the end of the line above the
 *      drawing. This is suppressed by setting checkForNewLine to true. This fails, if
 *      'useOffset' is set to true. But calculating the line start positions should
 *      anyhow be used with 'useOffset' set to false.
 *  @param {Number} [options.textlength]
 *      If the text length of the text span is known, it can be given to this function
 *      for performance reasons.
 *
 * @returns {Object|Number}
 *  If the option 'checkForNewLine' is set to true, an object with the properties
 *  'offset' and 'top' is returned. The 'offset' is the text offset number (zero-based),
 *  that describes the number of previous characters before the specified pixel position
 *  inside the text span node. The property 'top' is the vertical position inside the
 *  paragraph of the bottom line at the specific position.
 *  If the option 'checkForNewLine' is set to false (the default value) the offset inside
 *  the text span is returned. This value describes the number of previous characters
 *  before the specified pixel position inside the text span node.
 */
export function getOffsetInsideTextSpan(searchSpan, x, y, zoomFactor, options) {

    var // the line height of the span
        spanLineHeight = parseCssLength(searchSpan, 'lineHeight'),
        // the text value inside the span
        spanText = searchSpan.firstChild.nodeValue,
        // the length of the text span defined in the options object
        optionsTextLength = getNumberOption(options, 'textlength'),
        // the left offset inside the span (setting to optional value 'startOffset' or 0
        leftOffset = getNumberOption(options, 'startOffset', 0),
        // the right offset inside the span (setting to last text position in span)
        rightOffset = _.isNumber(optionsTextLength) ? optionsTextLength : spanText.length,
        // the offset that will be checked in the current loop
        checkOffset = leftOffset + Math.floor((rightOffset - leftOffset) / 2), // splitting the span always in the middle,
        // helper span nodes needed for splitting the span
        endSpan = null, finalSpan = null,
        // the offset of the currently investigated span (in px)
        currentLeftOffset = 0, currentTopOffset = 0,
        // whether the x and y position are calculated via $.offset() function (relative to page)
        // -> the alternative is the position relative to the offset parent, that is received via $.position()
        useOffset = getBooleanOption(options, 'useOffset', false),
        // whether the special mode for finding all line beginning positions is used
        checkForNewLine = getBooleanOption(options, 'checkForNewLine', false),
        // the jQuery function, that is needed for calculating the text offset inside the span
        funcName = useOffset ? 'offset' : 'position',
        // whether the specified xy-position is inside the left node
        isInRightSpan = false,
        // whether the correct position was found
        found = false,
        // the vertical position of the line bottom inside the paragraph
        verticalPosInParagraph = 0,
        // an optional zoom level, dependent from funcName
        optionalZoomFactor = 1;

    // short version for empty text span and text span with only one character
    if (rightOffset <= 1) { // setting cursor to the left of the only character (if there is one)
        return 0;
    }

    // setting default value to 100% and converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    if (!useOffset) {
        optionalZoomFactor = zoomFactor;
    }

    // splitting the paragraph, until the correct position is found
    while (!found) {

        endSpan = DOM.splitTextSpan(searchSpan, checkOffset, { append: true });

        // splitting this endSpan again, so that it contains only one character.
        // This guarantees, that the endSpan is inside one line, so that offset().left
        // and offset().top return the upper left corner of this one character.
        finalSpan = DOM.splitTextSpan(endSpan, 1, { append: true });

        currentLeftOffset = Math.round(endSpan[funcName]().left / optionalZoomFactor);
        currentTopOffset = Math.round(endSpan[funcName]().top / optionalZoomFactor);

        // is the point (x, y) in the left or the right span
        isInRightSpan = (y > currentTopOffset && x > currentLeftOffset) || (y > currentTopOffset + spanLineHeight);

        // special handling caused by drawings (split at the beginning of a line)
        // This fails, if useOffset is set to true (which should not be the case
        // for calculating line positions)
        if (checkForNewLine && !useOffset && !isInRightSpan && currentLeftOffset === 0) {
            isInRightSpan = true;
            found = true;
        }

        if (isInRightSpan) {
            leftOffset = checkOffset;
            if (checkForNewLine) {
                verticalPosInParagraph = currentTopOffset + spanLineHeight;
            } // saving for return
        } else {
            rightOffset = checkOffset;
        }

        // merging spans -> setting text to first span, deleting second span
        searchSpan.firstChild.nodeValue = spanText;
        $(endSpan).remove();
        $(finalSpan).remove();

        if (!found) {
            found = (rightOffset - leftOffset <= 1);
            if (!found) {
                // splitting span again in the middle
                checkOffset = leftOffset + Math.floor((rightOffset - leftOffset) / 2);
                found = (checkOffset === leftOffset || checkOffset === rightOffset); // avoid endless loop
            }

            // special handling for floated drawing (should be removed with new drawing layer
            if (found && checkForNewLine && verticalPosInParagraph === 0) {
                verticalPosInParagraph = currentTopOffset + spanLineHeight;
            }
        }
    }

    return checkForNewLine ? { offset: leftOffset, top: verticalPosInParagraph } : leftOffset;
}

/**
 * Calculating the lines inside a multi line text span. Specified are the position
 * offsets inside the text span that describe text positions at the beginning
 * of a line. Additionally for each offset the distance to the top of the
 * paragraph in pixel is returned.
 *
 * Testing this function can happen in various scenarios. The following
 * code can be used in the event (mousedown) handler to display the calculated
 * offsets in the console:
 *
 * globalLogger.takeTime('getAllLineOffsetsInsideTextSpan', function () {
 *     var allLines = getAllLineOffsetsInsideTextSpan($(event.target).closest('span'), app.getView().getZoomFactor() * 100);
 *     window.console.log('Lines: ' + allLines.length + ' : ' + JSON.stringify(allLines));
 * });
 *
 * @param {HTMLElement|jQuery} node
 *  The multi line text span node. If this object is a jQuery collection, uses the first
 *  DOM node it contains.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {String} options
 *  Optional parameters:
 *  @param {Object[]} [options.allOffsetInfos=[]]
 *   The list with all found new lines. This is necessary as option, because this
 *   function is used recursively.
 *  @param {HTMLElement[]|jQuery[]} [options.allRemoveNodes=[]]
 *   The list with all nodes that need to be removed after all new lines are found.
 *   This is necessary as option, because this function is used recursively.
 *  @param {Number} [options.startOffset=0]
 *   The start offset inside the text span
 *  @param {Number} [options.paraOffset=0]
 *   The offset of the text span inside the paragraph
 *  @param {Object} [options.allTopLineHeights={}]
 *   A collector for all already registered lines. This is necessary to avoid
 *   duplicates.
 *  @param {Number} [options.blockNodeLength=0]
 *   The length of a default block that is used for the linear strategy inside the
 *   multi line text span.
 *   -> should be smaller than one line, to avoid that every span is a multi line span
 *   -> should be as large as possible to go forward in the span as fast as possible
 *
 * @returns {Array<Object>}
 *  An array with object, that describe the beginning of each line in the
 *  text span. This object contains the property 'offset' for the logical offset
 *  position inside this text span. If 'paraOffset' is defined, the offset
 *  describes the logical position inside the paragraph. This value combined
 *  with the logical position of the paragraph is a complete valid logical
 *  text position.
 *  The second and third property in the object are 'top' and 'left'. These values
 *  describe the lower (!) left corner of the new line relative to the upper left
 *  corner of the paragraph in pixel.
 */
export function getAllLineOffsetsInsideTextSpan(node, zoomFactor, options) {

    if (!DOM.isMultiLineTextSpan(node)) {
        return [];
    }

    var // the dom node of the node
        localNode = getDomNode(node),
        // the jQuery node of the node
        $node = $(localNode),
        // a container for all offset infos that describe beginning of lines
        allOffsetInfos = getArrayOption(options, 'allOffsetInfos', []),
        // a collector for all created nodes
        allRemoveNodes = getArrayOption(options, 'allRemoveNodes', []),
        // the start offset inside the outermost multi line span
        startOffset = getNumberOption(options, 'startOffset', 0),
        // the offset of the multi line text span inside the paragraph
        paraOffset = getNumberOption(options, 'paraOffset', 0),
        // an object with all already registered line heights
        allTopLineHeights = getObjectOption(options, 'allTopLineHeights', { }),
        // object with offset and vertical distance in pixel to top of span
        oneOffsetInfo = null,
        // the text value inside the span
        spanText = localNode.firstChild.nodeValue,
        // the line height inside a text node
        nodeLineHeight = parseCssLength(node, 'lineHeight'),
        // the length of an investigated node
        // -> should be smaller than one line, to avoid that every span is a multi line span
        // -> should be as large as possible to go forward in the span as fast as possible
        defaultNodeLength = 64, // supporting multiples of 4 (but other number are fine, too)
        // the length of an investigated block
        blockNodeLength = getNumberOption(options, 'blockNodeLength', defaultNodeLength),
        // saving the start span for later restoring
        // -> all other splitted nodes can be deleted
        restSpan = DOM.splitTextSpan(node, 1, { append: true }),
        // the distance to the offset parent of the current line in pixel
        currentTopLineHeight = 0,
        // the distance to the offset parent of the new created text span
        newTopLineHeight = 0,
        // the jQuery position of a text span
        spanPosition = null,
        // the text span node that is currently investigated
        checkNode = null,
        // whether the rest span is empty, this stops the iteration
        emptyRestSpan = false;

    // check, if the specified node is a multi line text span
    function isMultiLineNode(node) {
        return $(node).height() > nodeLineHeight;
    }

    // getting the next block of characters
    // -> always removing the characters from the beginning of the span
    // -> the rest textspan is shrinking
    function getNextBlock(span) {

        var // the DOM node
            domNode = getDomNode(span);

        // is the span longer than the proposed default length?
        if (domNode.firstChild.nodeValue.length < blockNodeLength) {
            emptyRestSpan = true;
            return span; // no further split required (last span)
        }

        // splitting at the beginning
        return DOM.splitTextSpan(span, blockNodeLength, { append: false });
    }

    // setting default value to 100% and converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    // setting values f
    currentTopLineHeight = Math.round($node.position().top / zoomFactor);
    newTopLineHeight = Math.round(restSpan.position().top / zoomFactor);

    // the rest span can be registered for removal
    allRemoveNodes.push(restSpan);
    // the offset inside the text span, this is already increased by 1 after first split
    startOffset += 1;

    // iteration while the rest span is not empty
    while (!emptyRestSpan) {

        // always searching character blocks of length blockNodeLength
        checkNode = getNextBlock(restSpan);
        allRemoveNodes.push(checkNode);

        spanPosition = checkNode.position();
        newTopLineHeight = Math.round(spanPosition.top / zoomFactor);

        // check node starts directly in a new line and is not already registered?
        if (currentTopLineHeight !== newTopLineHeight && !_.isNumber(allTopLineHeights[newTopLineHeight])) {
            oneOffsetInfo = { offset: paraOffset + startOffset, top: newTopLineHeight + nodeLineHeight, left: Math.round(spanPosition.left / zoomFactor) };
            allOffsetInfos.push(oneOffsetInfo);
            allTopLineHeights[newTopLineHeight] = 1;  // saving to avoid duplicates
            currentTopLineHeight = newTopLineHeight; // increasing the current line height
        }

        // is this block a multi line block? Otherwise the next block an be checked.
        if (isMultiLineNode(checkNode)) {
            // finding all splits inside the checkNode (recursive)
            getAllLineOffsetsInsideTextSpan(checkNode, zoomFactor * 100, { blockNodeLength: Math.floor(defaultNodeLength / 4), allOffsetInfos, allTopLineHeights, startOffset, paraOffset });
        }

        // increasing the start offset for the next block
        startOffset += blockNodeLength;
    }

    // restoring the node -> setting text to first span, deleting further spans
    localNode.firstChild.nodeValue = spanText;
    _.each(allRemoveNodes, function (splitNode) {
        $(splitNode).remove();
    });

    return allOffsetInfos;
}

/**
 * Calculating the lines in a specified paragraph. Specified are the postion
 * offsets inside the paragraph that describe text positions at the beginning
 * of a line. Additionally for each offset the distance to the top of the
 * paragraph in pixel is returned.
 *
 * Testing this function can happen in various scenarios. The following
 * code can be used in the event (mousedown) handler to display the calculated
 * offsets in the console:
 *
 * globalLogger.takeTime('Position.getAllFirstTextPositionsInLines', function () {
 *     var allLines = Position.getAllFirstTextPositionsInLines($(event.target).closest('div.p'), app.getView().getZoomFactor() * 100);
 *     window.console.log('Lines: ' + allLines.length + ' : ' + JSON.stringify(allLines));
 * });
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node. If this object is a jQuery collection, uses the first
 *  DOM node it contains.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {String} options
 *  Optional parameters:
 *  @param {Boolean} [options.strategy='linear']
 *    The search strategy for new lines inside multi line text spans. If not defined
 *    'linear' is used. This uses an iteration process from the beginning of the span
 *    to the end. All positions are searched, for that the $.position().top is modified
 *    from one position to the next position. This is the recommended (and very precise)
 *    process.
 *    The alternative strategy (option.strategy !== 'linear') is a binary position
 *    search using a point defined with pixel in x and y. In this case the x/y position
 *    inside the paragraph is set and the logical position for this point is searched
 *    in a binary process. This is not efficient, if there are drawings, that increase
 *    the height of the multi line span.
 *  @param {Boolean} [options.useClone=false]
 *    Sometimes it can be necessary, that the specified paragraph is not modified.
 *    Temporary modifications are required within this function to calculate the
 *    positions. But these modifications disturb for example the cursor movements.
 *    Therefore it is sometimes necessary to work on a clone of the specified
 *    paragraph. This clone is inserted directly at the position of the specified
 *    paragraph (DOCS-3291).
 *
 * @returns {Array<Object>}
 *  An array with object, that describe the beginning of each line in the
 *  paragraph. This object contains the property 'offset' for the logical offset
 *  position inside this paragraph. This value combined with the logical position
 *  of the paragraph is a complete valid logical text position. The offset is
 *  always calculated for text nodes.
 *  The second and third property in the object are 'top' and 'left'. These values
 *  describe the lower (!) left corner of the new line relative to the upper left
 *  corner of the paragraph in pixel.
 */
export function getAllFirstTextPositionsInLines(paragraph, zoomFactor, options) {

    var // a container for all already registered offset value
        allOffsets = [],
        // a container for all objects that describe the lines. This is the return value
        allOffsetInfos = [],
        // the vertical pixel interval of the currently investigated line
        currentInterval = null,
        // the vertical pixel interval of the currently investigated text node
        nodeInterval = null,
        // whether a text node starts in a new line
        isNewLine = false,
        // the offset of a text span inside the paragraph (the start position of the text node)
        paraOffset = null,
        // the height of a text node
        nodeHeight = 0,
        // the line height inside a text node
        nodeLineHeight = 0,
        // an height iterator used for multi line text nodes
        currentLineHeight = 0,
        // the offset inside a text node
        spanOffset = 0,
        // the horizontal pixel position for searching in multi line text nodes
        posX = 0,
        // the length of a text span
        textLength = 0,
        // the distance of a text node relative to the top of the paragraph in pixel
        paraTopDistance = 0,
        // the new calculated offset inside the paragraph
        newOffset = 0,
        // the last found distance to the top of the paragraph in pixel
        lastTopValue = 0,
        // an object describing the position inside the text span
        offsetInfo = 0,
        // the right offset value of a text span
        rightValue = 0,
        // the jQuery position of a text span
        spanPosition = null,
        // the maximum length of a splitted text span, reduced for performance reasons
        maxSpanLength = 128,
        // the search stategy for multi line text spans
        strategy = getStringOption(options, 'strategy', 'linear'),
        // whether a clone of the paragraph has to be used (this is required to avoid cursor travelling problems)
        useClone = getBooleanOption(options, 'useClone', false),
        // a clone of the specified paragraph. This is used, if 'useClone' is set to true.
        cloneParagraph = null,
        // the paragraph node, that is used for the calculations
        localParagraph = null;

    // helper function to calculate the vertical range
    // of a specified node in pixel.
    function getNodeInterval(node) {
        var nodeTop = $(node).position().top / zoomFactor;
        return { first: nodeTop, last: nodeTop + $(node).height() };
    }

    if (!DOM.isParagraphNode(paragraph)) {
        return null;
    }

    if (useClone) {
        cloneParagraph = $(paragraph).clone(true);
        cloneParagraph.insertBefore(paragraph); // inserting the cloned paragraph at the same position as the specified paragraph
    }

    // the paragraph node, that is used for the calculations
    localParagraph = cloneParagraph ? cloneParagraph : paragraph;

    // setting default value to 100% and converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    $(localParagraph).children().get().forEach(function (node) {

        if (DOM.isTextSpan(node)) {

            paraOffset = null;
            isNewLine = false;

            nodeInterval = getNodeInterval(node);

            if (!currentInterval || !intervalOverlapsInterval(currentInterval, nodeInterval)) {
                isNewLine = true;
            }

            if (!currentInterval) {
                currentInterval = nodeInterval;
            }

            if (isNewLine) {
                // saving the first position for a specific vertical position
                paraOffset = getNodeStartAndEndPosition(node, localParagraph).start;
                allOffsets.push(paraOffset);
                spanPosition = $(node).position();
                lastTopValue = Math.round((spanPosition.top / zoomFactor) + parseCssLength(node, 'lineHeight'));
                allOffsetInfos.push({ offset: paraOffset, top: lastTopValue, left: Math.round(spanPosition.left / zoomFactor) });
                currentInterval = nodeInterval;
            }

            if (DOM.isMultiLineTextSpan(node)) {

                // the offset of the multi line span inside the paragraph
                if (!_.isNumber(paraOffset)) {
                    paraOffset = getNodeStartAndEndPosition(node, localParagraph).start;
                }

                // two different strategies are supported:
                // 1. Linear: Investigate smaller blocks
                // 2. Binary: Always splitting the multi line text span in the middle
                if (strategy === 'linear') {
                    // collecting all new lines in multi line span from start to end (recommended)
                    allOffsetInfos = allOffsetInfos.concat(getAllLineOffsetsInsideTextSpan(node, zoomFactor * 100, { paraOffset }));
                } else {
                    // the binary search strategy inside multi line text spans (not recommended)
                    textLength = node.firstChild.nodeValue.length;
                    spanOffset = 0;

                    posX = ($(node).position().left / zoomFactor) + 1; // 1 pixel to the right in the found multiline node

                    // add all first text positions in each line (except the first position in the text span)
                    nodeHeight = $(node).height();
                    nodeLineHeight = parseCssLength(node, 'lineHeight');

                    // distance to the top paragraph border
                    paraTopDistance = Math.round($(node).position().top / zoomFactor);

                    // the vertical offset in pixel of the current line inside the text span
                    currentLineHeight = nodeLineHeight + 1; // starting in the second line of text span

                    // Info: currentLineHeight might be an invalid position for the height. This can happen
                    // if there are drawings inside the text span. In this case the function
                    // getOffsetInsideTextSpan() in the following while-loop returns the
                    // same value several times (dependent from the height of the drawing).

                    while (currentLineHeight < nodeHeight) {
                        rightValue = (textLength - spanOffset > maxSpanLength) ? (spanOffset + maxSpanLength) : textLength;
                        // using binary search process (finding one position inside the complete multi line node)
                        offsetInfo = getOffsetInsideTextSpan(node, posX, paraTopDistance + currentLineHeight, zoomFactor * 100, { checkForNewLine: true, startOffset: spanOffset, textlength: rightValue });
                        spanOffset = offsetInfo.offset;
                        newOffset = paraOffset + spanOffset;

                        // values appearing more than once are wrong values (for example caused by drawings)
                        if (_.contains(allOffsets, newOffset)) {
                            currentLineHeight += nodeLineHeight; // go to next line, not saving value
                        } else {
                            // special check for floated drawings
                            if (lastTopValue === offsetInfo.top) {
                                // this is not a new line (caused by a floated drawing)
                                // -> go to same line with increased offset
                                spanOffset++; // increasing the start value for the next run (not increasing
                            } else {
                                allOffsets.push(newOffset);
                                allOffsetInfos.push({ offset: newOffset, top: offsetInfo.top, left: null });  // left is not defined in this 'strategy'
                                lastTopValue = offsetInfo.top;
                                currentLineHeight = lastTopValue + 1 - paraTopDistance; // go to next line
                            }
                        }
                    }
                }

                // modifying the node interval to height of multi line span (for all strategies)
                currentInterval = nodeInterval;
            }
        }
    });

    // cleanup
    if (useClone && cloneParagraph) { cloneParagraph.remove(); }

    return allOffsetInfos;
}

/**
 * Calculating the logical position inside a paragraph, given a paragraph node
 * and the vertical and horizontal pixel position relative to the upper left
 * corner of the paragraph.
 *
 * Important: This function does NOT use native functions from the different
 * browsers and it is NOT restricted to the visible browser window. It uses
 * binary text span splitting to receive the precise text level position.
 *
 * Important: This function does NOT try to 'improve' the returned position. If
 * it fails to return a logical position inside the paragraph, this function
 * returns null.
 *
 * Important: This function uses the x- and y-position relative to the upper
 * left corner of the paragraph node.
 *
 * @param {Node} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node. If this object is a jQuery collection, uses the first
 *  DOM node it contains.
 *
 * @param {Number} posX
 *  The horizontal pixel position relative to the upper left corner of the
 *  paragraph node.
 *
 * @param {Number} posY
 *  The vertical pixel position relative to the upper left corner of the
 *  paragraph node.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.getFirstTextPositionInLine=false]
 *      If set to true, the first logical text position in a line is determined
 *
 * @returns {Object|Null}
 *  Object containing the properties 'pos' and 'point'. 'pos' is the logical position
 *  and 'point' the dom point corresponding to the specified horizontal and vertical
 *  pixel values relative to the page node. If it could not be determined, null is
 *  returned.
 */
export function getPositionInsideParagraph(startNode, paragraph, posX, posY, zoomFactor, options) {

    var // whether the node inside the paragraph was found
        searchNode = null,
        // the offset inside a text span
        offset = 0,
        // a helper text node for the option 'getFirstTextPositionInLine'
        textNode = null,
        // whether the first text position in a line shall be determined
        getFirstTextPositionInLine = getBooleanOption(options, 'getFirstTextPositionInLine', false),
        // the offset inside a paragraph (used for paragraphs in text frames)
        paragraphOffset = 0,
        // whether positions inside page breaks shall be handled (not supported yet)
        handlePageBreakPosition = true,
        // whether the specified position is inside an existing space maker node
        isSpaceMakerPosition = false,
        // a cache for the searched node
        cachedNode = null;

    if (!DOM.isParagraphNode(paragraph)) {
        return null;
    }

    // whether a vertical pixel position is inside the range of a specific node
    function isVerticalPositionInsideNode(node, posY, counter) {
        if (!(node instanceof $)) {
            node = $(node);
        }

        var nodeHeight = Math.max(node.outerHeight(), 1); // minimum height 1px (increasing resilience, 56729)

        var isInNode = (Math.round((node.position().top / zoomFactor)) <= posY) && (posY <= Math.round((node.position().top / zoomFactor) + nodeHeight));
        if (!isInNode && counter === 1 && posY > 0 && posY < Math.round(node.position().top / zoomFactor)) {
            isInNode = true;
        } // 36335, browser have different offsets
        return isInNode;
    }

    // whether a vertical pixel position is before the range of a specific node
    function isVerticalPositionBeforeNode(node, posY) {
        if (!(node instanceof $)) {
            node = $(node);
        }
        return posY < Math.round((node.position().top / zoomFactor));
    }

    // whether a vertical pixel position is behind the range of a specific node
    function isVerticalPositionBehindNode(node, posY) {
        if (!(node instanceof $)) {
            node = $(node);
        }
        return Math.round((node.position().top / zoomFactor) + node.outerHeight()) < posY;
    }

    // checking whether the position (x,y) is in the bottom right corner of a multiline text span
    function positionIsRightOfSpan(textSpan, textlength, x, y) {

        var // the text value inside the span
            spanText = textSpan.firstChild.nodeValue,
            // helper span node needed for splitting the span at the last position
            finalSpan = null,
            // the offset and the width of the currently investigated span (in px)
            currentLeftOffset = 0, currentTopOffset = 0, currentWidth = 0,
            // whether the position (x,y) is right of the text span
            isRightOfTextSpan = false;

        // splitting the text span at its final position to get information about the end of the span
        finalSpan = DOM.splitTextSpan(textSpan, textlength - 1, { append: true });

        currentLeftOffset = finalSpan.position().left / zoomFactor;
        currentTopOffset = finalSpan.position().top / zoomFactor;
        currentWidth = finalSpan.outerWidth();

        // is the point (x, y) in the left or the right span
        isRightOfTextSpan = (y > currentTopOffset && x > currentLeftOffset + currentWidth);

        // merging spans -> setting text to first span, deleting second span
        textSpan.firstChild.nodeValue = spanText;
        $(finalSpan).remove();

        return isRightOfTextSpan;
    }

    // Checking, if the node (that must be a text span) has the pixel height y within its first row
    function isPositionInFirstLine(node, y) {
        var nodeTop = Math.round($(node).position().top / zoomFactor);
        return (nodeTop <= y) && (y <= nodeTop + parseCssLength(node, 'lineHeight'));
    }

    // Helper function to find the first text node at a specified pixel position y
    function getFirstTextNodeInLine(paragraph, y) {

        var // the first text node that contains the vertical pixel position y
            textNode = null,
            // a counter for the children to increase resilience for first text span (36335)
            counter = 0;

        $(paragraph).children().get().some(function (node) {
            if (DOM.isTextSpan(node)) {
                counter++;
                if (isVerticalPositionInsideNode(node, y, counter)) {
                    textNode = node;
                    return true; // leaving the loop
                }
            }
        });

        return textNode;
    }

    // Improving handling of absolute positioned drawings on header or footer nodes
    function handlePositionOnPagebreak(node, posY) {

        var // the searched text node
            textNode = null,
            // the page break node
            pageBreakNode = $(node).children(DOM.PAGEBREAK_NODE_SELECTOR),
            // the footer node in the page break node
            footer = null,
            // the header node in the page break node
            header = null;

        if (pageBreakNode) {

            footer = $(pageBreakNode).children('div.footer');
            header = $(pageBreakNode).children('div.header');

            // Is the y position before or behind the page break node?
            // -> Before the header or behind the footer?
            if (footer && footer.length > 0 && isVerticalPositionBeforeNode(footer, posY)) {
                textNode = pageBreakNode.prev();
            } else if (header && header.length > 0 && isVerticalPositionBehindNode(header, posY)) {
                textNode = pageBreakNode.next();
            }
        }

        return textNode;
    }

    // Improving handling of absolute positioned drawings on space maker nodes
    function handleSpaceMakerNode(node, x, y) {

        var // the searched text node
            textNode = null,
            // the page break node
            spaceMakerNodes = $(node).children(DOM.DRAWING_SPACEMAKER_NODE_SELECTOR);

        spaceMakerNodes.get().some(function (oneNode) {
            if (isPositionInsideNode(oneNode, x, y)) {
                textNode = $(oneNode).next();
                return true;
            }
        });

        return textNode;
    }

    // Checking, if the given position specified by the pixel values posX and posY is
    // located inside the paragraph child node.
    // Additionally the offset inside a specified text node is set.
    function isPositionInsideNode(node, x, y) {

        var inNodeRectangle = false,
            textLength = 0,
            $node = $(node),
            // a horizontal correction, if zoom factor is not 1
            correction = 0;

        // correction for the left offset is requird, if the zoom level is not 1
        if (zoomFactor !== 1 && DOM.isDrawingFrame(node)) {
            correction = (zoomFactor - 1) * parseCssLength(node, 'marginLeft');
        }

        inNodeRectangle = (Math.round(($node.position().left - correction) / zoomFactor) <= x) &&
            (x <= Math.round((($node.position().left - correction) / zoomFactor) + $node.outerWidth())) &&
            (Math.round($node.position().top / zoomFactor) <= y) &&
            (y <= Math.round(($node.position().top / zoomFactor) + $node.outerHeight()));

        if (inNodeRectangle) {

            if (DOM.isTextSpan(node)) {

                textLength = node.firstChild.nodeValue.length;

                if (textLength > 1) {
                    // text spans can contain multiple lines. In this case the rectangular value must not be correct.
                    // There is a problem, if the pixel position is in the upper left or the bottom right region of the
                    // span, that was approximated by a rectangle.
                    // Make a binary split, until the position is found (but it might be, that it fails)
                    // Checking the bottom right part, if it contains the pixel position
                    if (DOM.isMultiLineTextSpan(node) && positionIsRightOfSpan(node, textLength, x, y)) {
                        return false;
                    }
                    offset = getOffsetInsideTextSpan(node, x, y, zoomFactor * 100, { textlength: textLength });
                    if (!_.isNumber(offset)) {
                        return false;
                    }
                } else {
                    offset = 0;
                }
            } else if (DOM.isListLabelNode(node)) {
                return false;
            }

            return true;
        }
    }

    // setting default value to 100% and converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    // option to get the first text position in a line
    if (getFirstTextPositionInLine) {
        textNode = getFirstTextNodeInLine(paragraph, posY);

        // increasing resilience, checking page breaks inside paragraph
        if (!textNode && handlePageBreakPosition && DOM.hasPageBreakNode(paragraph)) {
            textNode = handlePositionOnPagebreak(paragraph, posY);
        }

        // increasing resilience, checking space maker nodes inside paragraph
        if (!textNode && DOM.hasDrawingSpaceMakerNode(paragraph)) {
            textNode = handleSpaceMakerNode(paragraph, posX, posY);
            if (textNode) {
                isSpaceMakerPosition = true;
            }
        }

        if (!textNode) {
            return null;
        }

        // simple case: This node is in one line or the pixel position is inside the
        // first line of a multi-line text span.
        if (!DOM.isMultiLineTextSpan(textNode) || isPositionInFirstLine(textNode, posY) || isSpaceMakerPosition) {
            searchNode = textNode;
            offset = 0;
        } else {
            posX = ($(textNode).position().left / zoomFactor) + 1; // 1 pixel to the right in the found multiline node
        }
    }

    // standard search process
    if (!searchNode) {

        cachedNode = null;

        $(paragraph).children().get().some(function (node) {
            if (isPositionInsideNode(node, posX, posY)) {
                if (DOM.isTextSpan(node)) {
                    searchNode = node;
                    return true; // leaving the loop
                }
                cachedNode = node;  // preferring text spans (this might be an absolute positioned drawing)
            }
        });

        if (!searchNode && cachedNode) {
            searchNode = cachedNode;
        }

    }

    // If the found node is a text frame, it can contain paragraphs again. So a further iteration
    // is required, to find the valid paragraph inside the text frame.
    if (searchNode && DrawingFrame.isTextFrameShapeDrawingFrame(searchNode)) {
        // searching the paragraph inside the text frame
        // First step: Searching the top level node in the page

        searchNode = getTopLevelParagraphInNode(paragraph, DrawingFrame.getTextFrameNode(searchNode), posX, posY, zoomFactor * 100);

        if (!searchNode) {
            return null;
        }

        paragraphOffset = getPixelPositionToRootNodeOffset(paragraph, searchNode, zoomFactor * 100);
        posX -= Math.round(paragraphOffset.x);  // horizontal position inside the paragraph
        posY -= Math.round(paragraphOffset.y);   // vertical position inside the paragraph
        if (posX === 0) {
            posX = 1;
        } // increasing resilience, if the pixel position is on the edge of a paragraph (36274)
        if (posY <= 0) {
            posY = 1;
        } // increasing resilience, if the pixel position is between two paragraphs
        if (posY === $(searchNode).height()) {
            posY -= 1;
        } // increasing resilience, if the pixel position is at the bottom of a paragraph
        // calling getPositionInsideParagraph() recursively
        return getPositionInsideParagraph(startNode, searchNode, posX, posY, (zoomFactor * 100), { getFirstTextPositionInLine });
    }

    if (!searchNode || (searchNode instanceof $ && searchNode.length === 0)) {
        return null;
    }

    // returning an object with the properties 'pos' for the logical position
    // and 'point' for the dom point
    return { pos: getOxoPosition(startNode, searchNode, offset), point: new DOM.Point(searchNode, offset) };
}

/**
 * Calculating the logical position from a given pixel position. The given pixel
 * position must be related to a root node, which is the parameter startNode
 * (typically) div.page). For a specific node, Position.getPixelPositionToRootNodeOffset()
 * can be used to receive the pixel position relative to the page.
 *
 * Important: This function does NOT use native functions from the different
 * browsers and it is NOT restricted to the visible browser window.
 *
 * Important: This function does NOT try to 'improve' the returned position. If
 * posX and posY are positions on the border or between paragraphs or on page breaks,
 * this function returns null.
 *
 * Important: Testing this function can happen in various scenarios. The following
 * code can be used in the event (mousedown) handler to display the calculated
 * position in the console:
 *  var zoomFactor = app.getView().getZoomFactor() * 100;
 *  var deltaX = (event.clientX - $(event.target).offset().left) * 100 / zoomFactor; // left offset inside the event target
 *  var deltaY = (event.clientY - $(event.target).offset().top) * 100 / zoomFactor; // top offset inside the event target
 *  var pagePos = getPixelPositionToRootNodeOffset(editdiv, event.target, zoomFactor, deltaX, deltaY);  // the position relative to div.page
 *  window.console.log('Calculated position: ' + getPositionFromPagePixelPosition(editdiv, pagePos.x, pagePos.y, zoomFactor, {}).pos);
 *
 * Important: This function uses the x- and y-position relative to the page node,
 * NOT relative to the browser window. If you want to use the values relative to
 * the browser window, please take a look at function
 * getOxoPositionFromPixelPosition().
 *
 * @param {Node} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {Number} posX
 *  The horizontal pixel position relative to the page node (div.page) for which
 *  the logical position shall be determined. The function
 *  getPixelPositionToRootNodeOffset() might help, to receive this value.
 *
 * @param {Number} posY
 *  The vertical pixel position relative to the page node (div.page) for which
 *  the logical position shall be determined. The function
 *  getPixelPositionToRootNodeOffset() might help, to receive this value.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.onlyParagraphPosition=false]
 *      If set to true, only the logical position of the found paragraph
 *      is returned. The text position inside the paragraph is not
 *      determined.
 *  @param {Boolean} [options.getFirstTextPositionInLine=false]
 *      If set to true, the first logical position in a line is
 *      determined
 *
 * @returns {Object|Null}
 *  Object containing the properties 'pos' and 'point'. 'pos' is the logical position
 *  and 'point' the dom point corresponding to the specified horizontal and vertical
 *  pixel values relative to the page node. If it could not be determined, null is
 *  returned.
 */
export function getPositionFromPagePixelPosition(startNode, posX, posY, zoomFactor, options) {

    var // the top level node, in which the searched pixel position is located
        childNode = null,
        // the page content node
        pageContentNode = DOM.isHeaderOrFooter(startNode) ? DOM.getMarginalContentNode(startNode) : DOM.getPageContentNode(startNode),
        // whether paragraph position is sufficient
        onlyParagraphPosition = getBooleanOption(options, 'onlyParagraphPosition', false),
        // whether the first text position in a line shall be determined
        getFirstTextPositionInLine = getBooleanOption(options, 'getFirstTextPositionInLine', false),
        // an object with the x and y values for the paragraph offset
        paragraphOffset = null,
        // the return object containing the properties 'pos' for the logical postion and 'point' for the dom point
        infoObject = null;

    if (!pageContentNode) {
        return null;
    }

    // converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    childNode = getTopLevelParagraphInNode(startNode, pageContentNode, posX, posY, zoomFactor * 100);

    // First try to find a valid paragraph, even if child node was not found yet
    // if (!childNode && elementBelowDrawing) { childNode = elementBelowDrawing; }

    if (!childNode) {
        return null;
    }

    if (onlyParagraphPosition) {
        infoObject = { pos: getOxoPosition(startNode, childNode, 0), point: new DOM.Point(childNode, 0) };
    } else {
        // Second step: Finding the content inside the paragraph (if paragraph is not sufficient)
        paragraphOffset = getPixelPositionToRootNodeOffset(startNode, childNode, zoomFactor * 100);
        posX -= Math.round(paragraphOffset.x);  // horizontal position inside the paragraph
        posY -= Math.round(paragraphOffset.y);   // vertical position inside the paragraph
        if (posX === 0) {
            posX = 1;
        } // increasing resilience, if the pixel position is direct at edge of paragraph (36274)
        if (posY <= 0) {
            posY = 1;
        } // increasing resilience, if the pixel position is between two paragraphs
        if (posY === $(childNode).height()) {
            posY -= 1;
        } // increasing resilience, if the pixel position is at the bottom of a paragraph
        infoObject = getPositionInsideParagraph(startNode, childNode, posX, posY, (zoomFactor * 100), { getFirstTextPositionInLine });
    }

    return infoObject;
}

/**
 * Detecting the top level paragraph in a specified content node given a pixel position.
 * The given pixel position is relative to a specified anchor node. Typically this is
 * the page node (div.page), but it can also be a text frame node. If a table is found
 * as top level child, the paragraph inside the table is searched. So this function always
 * returns the first paragraph child of the specified content node.
 *
 * @param {Node} anchorNode
 *  The anchor node corresponding to the pixel position specified by posX and posY.
 *
 * @param {Node} contentNode
 *  The content node that is the direct parent of the paragraph nodes.
 *
 * @param {Number} posX
 *  The horizontal pixel position relative to the anchor node (div.page or div.drawing
 *  as text frame) for which the logical position shall be determined. The function
 *  getPixelPositionToRootNodeOffset() might help, to receive this value.
 *
 * @param {Number} posY
 *  The vertical pixel position relative to the page node (div.page or div.drawing
 *  as text frame) for which the logical position shall be determined. The function
 *  getPixelPositionToRootNodeOffset() might help, to receive this value.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @returns {Node|Null}
 *  The paragraph node that contains the specified pixel position. Or null, if no
 *  paragraph node could be determined.
 */
export function getTopLevelParagraphInNode(anchorNode, contentNode, posX, posY, zoomFactor) {

    var // the top level node, in which the searched pixel position is located
        childNode = null;

    // Checking, if the given position specified by the pixel values posX and posY are
    // located inside the page content node.
    function isPositionInsideNode(node, x, y) {

        if (!(node instanceof $)) {
            node = $(node);
        }

        var // the offset of the node relative to the start node
            nodeOffset = getPixelPositionToRootNodeOffset(anchorNode, node, zoomFactor * 100);

        return (Math.round(nodeOffset.x) <= x && x <= Math.round(nodeOffset.x + node.outerWidth()) &&
            Math.round(nodeOffset.y) <= y && y <= Math.round(nodeOffset.y + node.outerHeight()));
    }

    // Checking the position specified by the pixel values posX and posY relative to the node.
    function getRelativePositionToNode(node, x, y) {

        if (!(node instanceof $)) {
            node = $(node);
        }

        var // the position object
            position = { },
            // the offset of the node relative to the start node
            nodeOffset = getPixelPositionToRootNodeOffset(anchorNode, node, zoomFactor * 100),
            // the left pixel position of the node
            left = Math.round(nodeOffset.x),
            // the right pixel position of the node
            right = Math.round(nodeOffset.x + node.outerWidth()),
            // the top pixel position of the node
            top = Math.round(nodeOffset.y),
            // the bottom pixel position of the node
            bottom = Math.round(nodeOffset.y + node.outerHeight());

        // defining the position of (x/y) relative to the node
        position.isLeft = x < left;
        position.isRight = x > right;
        position.isTop = y < top;
        position.isBottom = y > bottom;
        position.topDistance = top - y;

        return position;
    }

    // searching for a valid paragraph inside a table (or tables)
    function getParagraphInTable(tableNode) {

        var // the cells within the table
            tableCells = DOM.getTableCells(tableNode),
            // the found node inside the table
            foundNode = null;

        var tableCellNode = _.find(tableCells.get(), function (node) {
            return isPositionInsideNode(node, posX, posY);
        });

        if (tableCellNode) {
            DOM.getCellContentNode(tableCellNode).children().get().some(function (node) {

                var // the relative position from
                    pos = getRelativePositionToNode(node, posX, posY);

                if (!pos.isTop && !pos.isBottom) {
                    foundNode = node;
                    // adapting position for left and right pixel might be required
                    return true; // leaving the loop, it the point is below the node
                }

                if (pos.isTop) {
                    foundNode = node;
                    // adapting the vertical position
                    posY = posY + pos.topDistance + 1;
                    return true; // leaving the loop, it the point is below the node
                }

                // below the last paragraph in a table cell
                // if (pos.isBottom) {
                //     foundNode = node;
                //     return BREAK; // leaving the loop, it the point is below the node
                // }
            });
        }

        return foundNode;
    }

    // converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    // First step: Searching the top level node in the page
    DOM.allowedChildNodes(contentNode.children()).get().some(function (node) {

        var // the relative position from
            pos = getRelativePositionToNode(node, posX, posY);

        if (!pos.isTop && !pos.isBottom) {
            childNode = node;
            // adapting position for left and right pixel might be required
            return true; // leaving the loop, it the point is below the node
        }

        if (pos.isTop) {
            childNode = node;
            // adapting the vertical position
            posY = posY + pos.topDistance + 1;
            return true; // leaving the loop, it the point is below the node
        }
    });

    // Finding paragraph, if pageChild is a table (-> the cell elements are the new offset parents)
    while (DOM.isTableNode(childNode)) {
        childNode = getParagraphInTable(childNode);
    }

    return childNode;
}

/**
 * Calculating the logical position from a given pixel position. The given pixel
 * position must be related to the current window. For this the event properties
 * pageX/pageY or clientX/clientY can be used.
 *
 * Important: This function returns only a valid logical position, if the pixel
 * position defined by posX and posY is in the visible browser window (!). Otherwise
 * the used native functions do not return a useful node. In this case, this
 * function returns null.
 *
 * Important: This function uses native functions from Chrome, Firefox and IE. For
 * IE the function 'document.elementFromPoint' needs additional improvements, so
 * that a binary split of text spans is used within this function.
 *
 * Important: This function tries to 'improve' the returned position. If posX and
 * posY are positions on the border or between paragraphs or on page breaks, inside
 * this function a valid node is searched. This enables to find a logical position
 * that is 'as good as possible'. Therefore it can be used for example in a
 * 'mouseover' event handler, to display the current logical position of the event.
 *
 * Important: Testing this function can happen in various scenarios. The returned
 * logical position is displayed via drag and drop from an external application
 * over OX Text. In this case the possible 'drop' position is visualized.
 * Alternatively the following code can be used in the event (mousedown) handler:
 * getOxoPositionFromPixelPosition(startNode, event.clientX, event.clientY, app.getView().getZoomFactor() * 100))
 *
 * Important: This function uses the x- and y-position relative to the browser
 * window. If you want to use the values relative to the startNode (div.page),
 * please take a look at function getPositionFromPagePixelPosition().
 *
 * @param {Node} startNode
 *  The start node corresponding to the logical position.
 *
 * @param {Number} posX
 *  The horizontal pixel offset for which the node shall be found. Typically
 *  event.clientX must be used to get the correct pixel value. The clientX event
 *  attribute returns the horizontal coordinate (according to the client area).
 *  The client area is the current window.
 *
 * @param {Number} posY
 *  The vertical pixel offset for which the node shall be found. Typically
 *  event.clientY must be used to get the correct pixel value. The clientY event
 *  attribute returns the vertical coordinate (according to the client area).
 *  The client area is the current window.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @returns {Object|Null}
 *  An object containing the two properties 'start' and 'end' with logical positions.
 *  The property 'end' is only defined, if it is different from start, otherwise
 *  it is null. If the logical start position cannot be determined, null is
 *  returned.
 */
export function getOxoPositionFromPixelPosition(startNode, posX, posY, zoomFactor) {

    var // the selection range specified by posX and posY
        range = null,
        // the start point of the selection range
        domStartPoint = null,
        // the end point of the selection range
        domEndPoint = null,
        // the logical start position
        oxoStartPosition = null,
        // the logical end position
        oxoEndPosition = null,
        // whether start and end point are identical
        samePoint = true,
        // whether the result from 'elementFromPoint' was improved
        elementFromPointImproved = false,
        // whether the position was calculated using 'elementFromPoint' function
        usedElementFromPoint = false;

    // try to improve a found logical position
    function improveOxoPosition(point) {

        var // a child node
            childNode = null;

        if (DrawingFrame.isDrawingContentNode(point.node)) {
            // setting correct end position for drawings
            oxoEndPosition = _.clone(oxoStartPosition);
            oxoEndPosition = increaseLastIndex(oxoEndPosition);
            if (!samePoint) {
                domEndPoint = null;
            } // no further evaluation of end point
        } else if (samePoint && DOM.isParagraphNode(point.node) && point.offset > 0) {
            // setting text cursor position, if paragraph is specified
            childNode = point.node.childNodes[point.offset - 1];

            if (childNode) {

                if (DOM.isPageBreakNode(childNode)) {
                    childNode = childNode.previousSibling;
                }

                if (DOM.isEmptySpan(childNode)) {
                    if (point.offset === 1) {
                        oxoStartPosition.push(0);
                    } else if (point.offset > 1) {
                        oxoStartPosition = getOxoPosition(startNode, childNode, 0);
                    }
                } else if (DOM.isTextSpan(childNode)) {
                    // the text span is not empty -> go to end of span (page break)
                    oxoStartPosition = getOxoPosition(startNode, childNode, $(childNode).text().length);
                } else if (DOM.isInlineComponentNode(childNode)) {
                    if (childNode.previousSibling && DOM.isTextSpan(childNode.previousSibling)) {
                        oxoStartPosition = getOxoPosition(startNode, childNode.previousSibling, $(childNode.previousSibling).text().length);
                    }
                }
            }
        }
    }

    // try to find a valid logical position: This is necessary, if the point node is
    // a top level element like the page content node or a page break node.
    function findValidOxoPosition(point) {

        var // a child node
            childNode = null;

        if (DOM.isPageContentNode(point.node)) {

            if (point.offset > 0) {
                childNode = point.node.childNodes[point.offset - 1];
                if (childNode && DOM.isContentNode(childNode)) {
                    if (childNode.nextSibling && DOM.isPageBreakNode(childNode.nextSibling)) {
                        oxoStartPosition = getLastPositionInParagraph(startNode, getOxoPosition(startNode, childNode));
                    } else {
                        oxoStartPosition = getFirstPositionInParagraph(startNode, getOxoPosition(startNode, childNode));
                    }
                } else if (childNode && DOM.isPageBreakNode(childNode)) {
                    if (DOM.isContentNode(childNode.nextSibling)) {
                        oxoStartPosition = getFirstPositionInParagraph(startNode, getOxoPosition(startNode, childNode.nextSibling));
                    }
                }
            } else if (point.offset === 0) {
                oxoStartPosition = getFirstPositionInParagraph(startNode, [0]); // clicking above table in pos [0]
            }
        } else if (DOM.isPageBreakNode(point.node)) {
            if (DOM.isContentNode(point.node.previousSibling)) {
                oxoStartPosition = getFirstPositionInParagraph(startNode, getOxoPosition(startNode, point.node.previousSibling));
            }
        }
    }

    // receiving the DOM starting point from a specified position
    function getDomStartPointAtPosition(x, y, node) {
        var range = caretRangeFromPoint(x, y, node);
        return (range.startContainer && _.isNumber(range.startOffset)) ? new DOM.Point(range.startContainer, range.startOffset) : null;
    }

    // try to find the position anywhere on the document, because the searchnode is a top level node
    // like the page node. This can happen if the position is between two paragraphs or on a page break.
    // Above or below must be a top-level element like a paragraph or table element
    // -> stepping upwards and downwards to find a valid position
    function modifyPositionInsidePage() {

        var // the step width in pixel to find a valid element
            step = 5 * zoomFactor,
            // the current step width in pixel
            currentStep = step,
            // the maximum step width, avoiding endless loop (can be a large value at the end of the document)
            maxStep = 100 * step,
            // the current vertical position
            currentY = posY,
            // the range object at the current vertical position
            currentRange = null,
            // the dom point at the current vertical position
            currentDomStartPoint = null,
            // whether a node was found, that is not a page node
            foundValidPosition = false,
            // whether the position modification was upwards or downwards
            upwards = true;

        while (!foundValidPosition) {

            // modifying the current vertical position
            currentY = upwards ? (posY - currentStep) : (posY + currentStep);
            // determining the range of specified position
            currentRange = caretRangeFromPoint(posX, currentY, startNode);
            currentDomStartPoint = new DOM.Point(currentRange.startContainer, currentRange.startOffset);

            if (!DOM.isPageNode(currentDomStartPoint.node)) {
                foundValidPosition = true;
            }

            // checking page break positions, travelling downwards
            if (foundValidPosition && DOM.isPageBreakNode(currentDomStartPoint.node.childNodes[currentDomStartPoint.offset - 1])) {
                foundValidPosition = false;
                upwards = false;
                currentStep = step;
            }

            if (!foundValidPosition) {
                // if (!upwards) { currentStep += step; } // increasing step width after downward check
                // upwards = !upwards;  // switching between upwards and downwards search -> only searching upwards
                currentStep += step;
                // avoiding endless loop
                if (currentStep > maxStep) {
                    return;
                }
            }
        }

        // handling value for end point, too
        if (foundValidPosition) {
            posY = currentY;  // modifying the 'real' vertical position
            domStartPoint = currentDomStartPoint;  // modifying the existing dom point
            range = currentRange;  // modifying the existing range object
        }

    }

    // try to find the position anywhere on the document, because the searchnode is a top level node
    // like the application content node. This can happen if the position is on the document border.
    // In this case it is necessary to find a valid node in one of the four directions. For this the
    // position must be modified by the width of the border. Additionally it might be necessary to
    // modify both posX and posY, if the position is in the document corner.
    function modifyPositionInsideApplication() {

        var // the page node, that contains the border as padding
            pageNode = $(domStartPoint.node).children(DOM.PAGE_NODE_SELECTOR),
            // the page content node
            pageContentNode = DOM.getPageContentNode(pageNode),
            // the top width of the document border in pixel
            topWidth = parseCssLength(pageNode, 'paddingTop') * zoomFactor,
            // the right width of the document border in pixel
            rightWidth = parseCssLength(pageNode, 'paddingRight') * zoomFactor,
            // the bottom width of the document border in pixel
            bottomWidth = parseCssLength(pageNode, 'paddingBottom') * zoomFactor,
            // the left width of the document border in pixel
            leftWidth = parseCssLength(pageNode, 'paddingLeft') * zoomFactor,
            // a container with the eight positions that need to be checked
            allPositions = [[posX, posY + topWidth, 'TOP'],
                [posX - rightWidth, posY, 'RIGHT'],
                [posX, posY - bottomWidth, 'BOTTOM'],
                [posX + leftWidth, posY, 'LEFT'],
                [posX + leftWidth, posY + topWidth, 'TOPLEFT'],
                [posX - rightWidth, posY + topWidth, 'TOPRIGHT'],
                [posX - rightWidth, posY - bottomWidth, 'BOTTOMRIGHT'],
                [posX + leftWidth, posY - bottomWidth, 'BOTTOMLEFT']],
            // the border direction (as string)
            direction = null,
            // left offset of the page content node
            leftOffset = Math.round(pageContentNode.offset().left),
            // top offset of the page content node
            topOffset = Math.round(pageContentNode.offset().top),
            // right offset of the page content node
            rightOffset = Math.round((pageContentNode.offset().left + pageContentNode.outerWidth() * zoomFactor)),
            // bottom offset of the page content node
            bottomOffset = Math.round((pageContentNode.offset().top + pageContentNode.outerHeight() * zoomFactor));

        // for positions on the right border it can be checked, if a paragraph node is found after moving
        // the value for the horizontal position further into the page content node.
        function checkParagraphFromRightBorder() {

            var // whether it is useful to search for a paragraph
                findParagraph = false,
                // the range corresponding to the currently used horizontal position
                currentRange = null,
                // the start point to the currently used horizontal position
                currentStartPoint = null,
                // whether the correct position inside the paragraph was found
                found = false,
                // the left and the right offset inside the paragraph
                leftValue = leftOffset, rightValue = rightOffset,
                // the offset that will be checked in the current loop
                checkOffset = 0,
                // whether a found node is a paragraph node
                isParaNode = false,
                // the minimum distance between left and right border in pixel
                minimumDistance = 5;

            // searching a paragraph at the left document side
            currentStartPoint = getDomStartPointAtPosition(leftOffset + 1, posY, startNode);
            findParagraph = currentStartPoint && currentStartPoint.node && DOM.isParagraphNode(currentStartPoint.node);

            if (findParagraph) {

                checkOffset = leftValue + Math.floor((rightValue - leftValue) / 2);

                // splitting the paragraph, until the correct position is found
                while (!found) {
                    currentStartPoint = getDomStartPointAtPosition(checkOffset, posY, startNode);
                    isParaNode = currentStartPoint && currentStartPoint.node && DOM.isParagraphNode(currentStartPoint.node);

                    if (isParaNode) {
                        leftValue = checkOffset;
                    } else {
                        rightValue = checkOffset;
                    }

                    // stopping iteration, if distance is smaller than 5 px
                    if ((rightValue - leftValue) < minimumDistance) {
                        found = true;
                    }

                    // setting the new offset node
                    if (!found) {
                        checkOffset = leftValue + Math.floor((rightValue - leftValue) / 2);
                    }
                }

                currentRange = caretRangeFromPoint(leftValue, posY, startNode);
                currentStartPoint = new DOM.Point(currentRange.startContainer, currentRange.startOffset);

                if (DOM.isParagraphNode(currentStartPoint.node)) {
                    posX = leftValue;  // modifying the 'real' vertical position
                    domStartPoint = currentStartPoint;  // modifying the existing dom point
                    range = currentRange;  // modifying the existing range object
                }
            }
        }

        // Checking, if the given position specified by the pixel values posX and posY are
        // located inside the page content node.
        function isPositionInsidePageContentNode(posX, posY) {
            return ((leftOffset <= posX) && (posX <= rightOffset) && (topOffset <= posY) && (posY <= bottomOffset));
        }

        allPositions.some(function (position) {
            if (isPositionInsidePageContentNode(position[0], position[1])) {
                direction = position[2];
                return true; // leaving the loop
            }
        });

        if (direction && direction.match(/LEFT/)) {
            posX = leftOffset + 1;
        } else if (direction && direction.match(/RIGHT/)) {
            posX = rightOffset - 1;
        }

        if (direction && direction.match(/TOP/)) {
            posY = topOffset + 4; // add 4 to reach the paragraph
        } else if (direction && direction.match(/BOTTOM/)) {
            posY = bottomOffset - 1;
        }

        // setting new range and new starting dom point
        range = caretRangeFromPoint(posX, posY, startNode);  // modifying the existing range object
        domStartPoint = new DOM.Point(range.startContainer, range.startOffset);  // modifying the existing dom point

        // trying to reach a paragraph from the right side, avoiding page content node
        if (direction && direction.match(/RIGHT/) && DOM.isPageContentNode(domStartPoint.node)) {
            checkParagraphFromRightBorder();
        }

    }

    // try to improve the position calculated with the function 'elementFromPoint', if the page content node is returned
    function findPositionInsideContent(searchNode) {

        var // the last element inside a table cell
            lastElement = null;

        if (DOM.isParagraphNode(searchNode)) {
            // using the last position inside the paragraph
            oxoStartPosition = getLastPositionInParagraph(startNode, getOxoPosition(startNode, searchNode));
        } else if (DOM.isTableCellNode(searchNode)) {
            // using the last position inside the table cell node
            if (DOM.isTablePageBreakRowNode(searchNode.parentNode)) {
                oxoStartPosition = null; // explicitely not supported node
            } else {
                lastElement = getLastParagraphOrTableNodeInCellNode(searchNode);
                if (lastElement) {
                    oxoStartPosition = getLastPositionInParagraph(startNode, getOxoPosition(startNode, lastElement));
                }
            }
        } else if (DOM.isPageBreakNode(searchNode)) {
            oxoStartPosition = getLastPositionInParagraph(startNode, getOxoPosition(startNode, searchNode.previousSibling));
        } else if (DOM.isSplittedTableNode(searchNode) || DOM.isPlaceholderNode(searchNode)) {
            oxoStartPosition = null; // explicitely not supported node
        }

    }

    // try to improve the position calculated with the function 'elementFromPoint', if a paragraph is returned
    function findPositionInsideParagraph(searchSpan) {

        var // the text offset inside the text span
            offset = 0,
            // the range of logical positions for a specified span
            searchSpanRange = null;

        if (searchSpan && DOM.isTextSpan(searchSpan)) {

            offset = getOffsetInsideTextSpan(searchSpan, posX, posY, zoomFactor, { useOffset: true });

            if (_.isNumber(offset)) {
                searchSpanRange = getNodeStartAndEndPosition(searchSpan, startNode, { fullLogicalPosition: true });
                oxoStartPosition = increaseLastIndex(searchSpanRange.start, offset);
            }
        }
    }

    // converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    // Bug 1025815 in Mozilla: If the zoom factor is not 100%, the calculated offset in 'caretRangeFromPoint()' is wrong.

    // determining the range of specified position
    range = caretRangeFromPoint(posX, posY, startNode);

    // Failed to determine the range (maybe posX and posY are not in the visible browser window)
    if (!range) {
        return null;
    }

    domStartPoint = new DOM.Point(range.startContainer, range.startOffset);

    // checking end position of the range
    if ((range.startContainer !== range.endContainer) || (range.startOffset !== range.endOffset)) {
        domEndPoint = new DOM.Point(range.endContainer, range.endOffset);
        samePoint = false;
    }

    // reading the marker property at the range object
    usedElementFromPoint = range._usedElementFromePoint_;

    // improving the position, if the functions 'caretRangeFromPoint' or 'caretPositionFromPoint' do not exist (IE)
    if (usedElementFromPoint && domStartPoint.node) {

        // modifying posX and posY, if it is between paragraphs or on document border
        if (DOM.isAppContentNode(domStartPoint.node)) {
            modifyPositionInsideApplication();
        }
        if (DOM.isPageNode(domStartPoint.node)) {
            modifyPositionInsidePage();
        }

        // improving the position returned from 'elementFromPoint'
        if (DOM.isParagraphNode(domStartPoint.node)) {
            elementFromPointImproved = true;
            findPositionInsideParagraph(domStartPoint.node.childNodes[domStartPoint.offset - 1]);
        } else if (DOM.isPageContentNode(domStartPoint.node) || DOM.isCellContentNode(domStartPoint.node) || DOM.isTableRowNode(domStartPoint.node)) {
            elementFromPointImproved = true;
            findPositionInsideContent(domStartPoint.node.childNodes[domStartPoint.offset - 1]);
        } else if (DOM.isTableCellNode(domStartPoint.node) || DOM.isTableCellNode(domStartPoint.node.parentNode)) {
            elementFromPointImproved = true;
            findPositionInsideContent(domStartPoint.node);
        }
    }

    // improving the logical position or finding the correct node, if 'caretRangeFromPoint' or
    // 'caretPositionFromPoint' could be used (domStartPoint should be in the page (63061))
    if (!elementFromPointImproved && domStartPoint && domStartPoint.node && containsNode(startNode, domStartPoint.node) && _.isNumber(domStartPoint.offset)) {
        oxoStartPosition = getOxoPosition(startNode, domStartPoint.node, domStartPoint.offset);
        if (_.isArray(oxoStartPosition) && oxoStartPosition.length === 0) {
            oxoStartPosition = null;
        }

        if (oxoStartPosition) {
            // improving existing position to get a valid text position (drawings, tabs, empty paragraphs, ...)
            improveOxoPosition(domStartPoint);
        } else {
            // trying to find a valid position, if it could not be determined in getOxoPosition()
            findValidOxoPosition(domStartPoint);
        }
    }

    if (oxoStartPosition && !samePoint && domEndPoint && domEndPoint.node && _.isNumber(domEndPoint.offset)) {
        oxoEndPosition = getOxoPosition(startNode, domEndPoint.node, domEndPoint.offset);
        if (_.isArray(oxoEndPosition) && oxoEndPosition.length === 0) {
            oxoEndPosition = null;
        }
    }

    return oxoStartPosition ? { start: oxoStartPosition, end: oxoEndPosition } : null;
}

/**
 * Calculating the logical position for a specified absolute positioned node.
 *
 * position must be related to the current window. For this the event properties
 * pageX/pageY or clientX/clientY can be used.
 *
 * @param {HTMLElement|jQuery} startNode
 *  The start node for which the logical position is calculated.
 *
 * @param {HTMLElement|jQuery} node
 *  The node, whose logical position shall be calculated.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @returns {Number[]|Null}
 *  The logical position relative to the start node, or null, if it cannot be
 *  determined.
 */
export function getOxoPositionOfAbsolutePositionedNode(startNode, node, zoomFactor) {

    // the offset in pixel of the specified node relative to the start node
    var nodeOffset = getPixelPositionToRootNodeOffset(startNode, node, zoomFactor);
    // the logical position of the specified node
    var oxoPos = getPositionFromPagePixelPosition(startNode, nodeOffset.x, nodeOffset.y, zoomFactor);

    if (!oxoPos) {
        // trying with first position in line, if precise logical position cannot be determined
        oxoPos = getPositionFromPagePixelPosition(startNode, nodeOffset.x, nodeOffset.y, zoomFactor, { getFirstTextPositionInLine: true });
    }

    return (oxoPos && oxoPos.pos) ? oxoPos.pos : null;
}

/**
 * Calculating the pixel position relative to the specified root element (div.page)
 * from a given logical position or a specified dom point. If the pixel position
 * relative to the document (the browser window) is required, the result can easily
 * be transformed with the converter function:
 * convertRootNodePixelToDocumentPixel(pagePosition.left, pagePosition.top, rootNode)
 *
 * As a convenience the position can be specified as logical position or already as a
 * dom point. If it is a logical position, it is transformed in a first step to the
 * corresponding dom point.
 *
 * Testing code for this function:
 * Inserting this code into mouse down event handler, requires double clicking
 * window.console.log('Pixel: ' + JSON.stringify(selection.getStartPosition()) + ' : ' + JSON.stringify(getPixelPositionFromDomPoint(editdiv, selection.getStartPosition(), app.getView().getZoomFactor() * 100)));
 *
 * @param {Node} startnode
 *  The start node corresponding to the logical position.
 *
 * @param {DOM.Point|Array<Number>} point
 *  The position, for that the pixel position shall be calculated. This can be a DOM.Point,
 *  that contains a dom node and an offset. Alternatively it can be a logical position,
 *  that needs to be transformed to a DOM.Point in a first step.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {Node} [pointRoot]
 *  The root node corresponding to a logical position is not always identical with the
 *  root node, to which the pixel position shall be calculated. So if 'point' is defined
 *  as logical position it is possible to use this parameter pointRoot to specify the
 *  root corresponding to this logical position. If it is not defined, that start node
 *  is used as root for the logical position.
 *  This parameter is only used, if the parameter 'point' is a logical position.
 *
 * @returns {Object|null}
 *  An object containing the properties 'left', 'top', 'width' and 'height', if the
 *  pixel position relative to the root element (div.page) could be determined. Otherwise
 *  null is returned.
 *  If the pixel position relative to the document (the browser window) is required,
 *  it can easily be converted using convertRootNodePixelToDocumentPixel().
 */
export function getPixelPositionFromDomPoint(startnode, point, zoomFactor, pointRoot) {

    var // the DOM point located at the passed position
        domPoint = point,
        // the pixel position of the point relative to the root element
        posX = null, posY = null,
        // the width and height in pixel of the element at the specified position
        width = null, height = null,
        // the pixel position relative to the specified start node
        pixelPosToStartNode = null,
        // the text span, that needs to be splitted
        searchNode = null,
        // helper span for splitting the text node
        singleSpan = null, finalSpan = null,
        // the text inside the text span
        spanText = null,
        // the length of the text span
        textSpanLength = 0,
        // whether the text span was splitted
        splittedTextSpan = false,
        // whether the right position shall be used
        useRightPosition = false;

    // convenience: converting for logical position to dom point
    if (_.isArray(point)) {
        domPoint = getDOMPosition(pointRoot ? pointRoot : startnode, point, true);
    }

    if (!domPoint || !domPoint.node) {
        return null;
    }

    // converting zoom from percentage to factor
    zoomFactor = (zoomFactor || 100) / 100;

    searchNode = domPoint.node;

    // splitting text span, that have at least two characters
    if (DOM.isTextSpan(searchNode)) {

        if (!_.isNumber(domPoint.offset)) {
            return null;
        }

        spanText = searchNode.firstChild.nodeValue;
        textSpanLength = spanText.length;

        if (textSpanLength > 1) {

            if (domPoint.offset === 0) {  // first character in text span
                singleSpan = DOM.splitTextSpan(searchNode, 1, { append: false });
            } else if (domPoint.offset === textSpanLength - 1) {  // last character in text span
                singleSpan = DOM.splitTextSpan(searchNode, textSpanLength - 1, { append: true });
            } else {
                singleSpan = DOM.splitTextSpan(searchNode, domPoint.offset, { append: true });
                finalSpan = DOM.splitTextSpan(singleSpan, 1, { append: true });
            }

            searchNode = singleSpan;
            splittedTextSpan = true;
        }

        // selecting the end of a text node (for example end of paragraph)
        if (domPoint.offset === textSpanLength) {
            useRightPosition = true;
        }

    }

    // getting the pixel position relative to the root node
    pixelPosToStartNode = getPixelPositionToRootNodeOffset(startnode, searchNode, zoomFactor * 100);

    // evaluating the pixel position
    if (pixelPosToStartNode) {
        posX = pixelPosToStartNode.x;
        posY = pixelPosToStartNode.y;
        width = $(searchNode).width();
        height = $(searchNode).height();

        if (useRightPosition) {
            posX += width;
            width = 0;
        }
    }

    // repair text span again
    if (splittedTextSpan) {
        // merging spans -> setting text to first span, deleting second span
        domPoint.node.firstChild.nodeValue = spanText;
        if (singleSpan) {
            $(singleSpan).remove();
        }
        if (finalSpan) {
            $(finalSpan).remove();
        }
    }

    return (_.isNumber(posX) && _.isNumber(posY)) ? { left: posX, top: posY, width, height } : null;
}

/**
 * Calculating the vertical offset relative to the document root (specified
 * by 'rootNode'). Required is the page number and the vertical offset of
 * an element on its page (in pixel).
 *
 * @param {Node} rootNode
 *  The root node for which the relative pixel position is calculated. This
 *  is typically the page node (div.page)
 *
 * @param {Object} pageLayout
 *  The page layout manager.
 *
 * @param {Number} pageNumber
 *  The page number of an element.
 *
 * @param {Number} [zoomFactor]
 *  The current zoom factor in percent. If not set, a default value of 100 is used.
 *
 * @param {Number} [verticalOffset=0]
 *  The vertical offset in pixel relative to the page, not to the root
 *  element (div.page). If this is not defined, it is set to 0.
 *
 * @returns {Number}
 *  The vertical offset in pixel relative to the document root, specified
 *  by 'rootNode'.
 */
export function getVerticalPagePixelPosition(rootNode, pageLayout, pageNumber, zoomFactor, verticalOffset) {

    var // the first element in a page
        firstElement = null,
        // the pixel position relative to the page node
        pageNodePosition = null;

    if (!_.isNumber(verticalOffset)) {
        verticalOffset = 0;
    }

    if (pageNumber === 1) {
        return verticalOffset;
    }

    firstElement = pageLayout.getBeginingElementOnSpecificPage(pageNumber);

    if (firstElement && firstElement.length > 0) {
        // getting the vertical offset in pixel relative to the page node (for example div.page)
        pageNodePosition = getPixelPositionToRootNodeOffset(rootNode, firstElement, zoomFactor);
        verticalOffset += pageNodePosition.y;
        verticalOffset += firstElement.outerHeight(true); // Adding height of the page break element
    }

    return verticalOffset;
}

/**
 * Calculating the position of a drawing selection inside the overlay selection node relative to the page.
 * This function is supported only in OX Text.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The root node of the selected drawing with the selection in the selection overlay node.
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page node, for that the relative pixel position is calculated.
 *
 * @param {HTMLElement|jQuery} drawing
 *  The (selected) drawing node.
 *
 * @param {Number} [zoomFactor=100]
 *  The current zoom factor in percent. If not specified the value is set to 100.
 *
 * @returns {Object}
 *  An object with properties x and y for the horizontal and vertical
 *  pixel positions of the drawing selection in the overlay node.
 */
export function getPixelPositionOfSelectionInSelectionLayer(rootNode, pageNode, drawing, zoomFactor) {

    var // the parent node of the specified drawing
        drawingParent = null,
        // an object containing the x and y position in px of the paragraph relative to the page
        pos = null,
        // the x position of the drawing relative to the page in px
        posX = 0,
        // the y position of the drawing relative to the page in px
        posY = 0,
        // an optional offset object for drawings with another root node
        offset = 0,
        // an optional vertical offset for drawings aligned to a paragraph
        rootNodeOffset = 0,
        // the jQueryfied drawing node
        $drawing = $(drawing);

    zoomFactor = (zoomFactor || 100);

    // the vertical shift of the current root node relative to the page
    if (getDomNode(rootNode) !== getDomNode(pageNode)) {
        offset = getPixelPositionToRootNodeOffset(pageNode, rootNode, zoomFactor);
        if (offset.y) { rootNodeOffset = offset.y; }
    }

    drawingParent = $drawing.parent();
    if (DOM.isParagraphNode(drawingParent)) {
        // paragraph aligned drawing (adding the paragraph offset to the page)
        pos = getPixelPositionToRootNodeOffset(rootNode, drawingParent, zoomFactor);
        posX = parseInt($drawing.css('left'), 10) + pos.x;
        posY = parseInt($drawing.css('top'), 10) + pos.y + rootNodeOffset;
    } else {
        // page aligned drawing
        posX = parseInt($drawing.css('left'), 10);
        posY = rootNodeOffset ? (parseInt($drawing.css('top'), 10) + rootNodeOffset) : parseInt($drawing.css('top'), 10);
    }

    return { x: posX, y: posY };
}

/**
 * Helper function to find nodes specified by a selector. It is required
 * to specify a node, that is used as parent for the search. The node
 * itself is also included into the search.
 *
 * @param {Node|jQuery|Array<Number>} searchNode
 *  The node, whose descendants and itself are investigated.
 *
 * @param {String} selectorString
 *  The selector string that defines the nodes that will be collected.
 *
 * @param {Number} [startOffset]
 *  An optional offset that can be used to reduce the search range inside
 *  the specified node. This parameter can only be used, if the specified
 *  search node is a paragraph and the end offset is also defined.
 *
 * @param {Number} [endOffset]
 *  An optional offset that can be used to reduce the search range inside
 *  the specified node. This parameter can only be used, if the specified
 *  search node is a paragraph and the start offset is also defined.
 *
 * @returns {jQuery}
 *  The collector for all nodes in the specified parentNode specified by the
 *  selector string.
 */
export function collectElementsBySelector(searchNode, selectorString, startOffset, endOffset) {

    var // the collector for the nodes
        collector = $(),
        // whehter the search node is a marginal node
        isMarginalSearchNode = DOM.isMarginalNode(searchNode);

    // helper function to handle only those nodes, that have the same target
    // -> it can happen, that there is a marginal paragraph inside a non-marginal paragraph (41555)
    function doSkip(node) {
        return !isMarginalSearchNode && DOM.isMarginalNode(node);
    }

    if (searchNode && selectorString) {
        // checking the specified node itself
        if ($(searchNode).is(selectorString)) {
            collector = collector.add(searchNode);
        } else {
            // if startOffset and endOffset are specified, it has to be checked, whether the node is inside the specified part
            if (_.isNumber(startOffset) && (_.isNumber(endOffset)) && DOM.isParagraphNode(searchNode)) {
                _.each($(searchNode).find(selectorString), function (oneNode) {
                    var logicalPos = null;
                    if (!doSkip(oneNode)) {
                        logicalPos = getOxoPosition(searchNode, oneNode, 0);  // -> searchNode must be a paragraph in this case
                        if (_.isArray(logicalPos) && (startOffset <= _.last(logicalPos)) && (_.last(logicalPos) <= endOffset)) {
                            collector = collector.add(oneNode);
                        }
                    }
                });
            } else {
                _.each($(searchNode).find(selectorString), function (oneNode) {
                    if (!doSkip(oneNode)) {
                        collector = collector.add(oneNode);
                    }
                });
            }
        }
    }

    return collector;
}
