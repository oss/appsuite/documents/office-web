/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { type Size, ary, dict, fun, is, json, unicode } from "@/io.ox/office/tk/algorithms";
import { type NodeOrJQuery, convertHmmToLength, Rectangle, type RectangleLike } from "@/io.ox/office/tk/dom";
import { Logger, globalLogger } from "@/io.ox/office/tk/utils/logger";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { getDomNode } from "@/io.ox/office/tk/utils";

import { getExplicitAttributes, getExplicitAttributeSet, hasEqualElementAttributes, type CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";

import type { TextBaseApplication } from "@/io.ox/office/textframework/app/application";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";
import { findFirstPortionSpan, isEmptySpan, isParagraphNode, isSpan, isTextSpan, splitTextSpan, type Point } from "@/io.ox/office/textframework/utils/dom";
import type { OptChangesOperation, TextDrawingAttributes } from "@/io.ox/office/textframework/utils/operations";

export * from "@/io.ox/office/tk/utils";

// types ======================================================================

interface MarginSet { marginLeft: number; marginTop: number; marginRight: number; marginBottom: number }

export interface TopLeftPoint { top: number; left: number }

export interface IntervalType { first: number; last: number }

type NO_DRAWING_BACKGROUND_TYPE = typeof NO_DRAWING_BACKGROUND;

type NO_SLIDE_BACKGROUND_TYPE = typeof NO_SLIDE_BACKGROUND;

// constants ==================================================================

// hangul for korean inputs
const HANGUL = /[\u1100-\u11FF\u3130-\u318F\uA960-\uA97F\uAC00-\uD7AF\uD7B0-\uD7FF]/;

// regions using special default paper format (all others will use "A4")
const DEFAULT_LETTER_FORMAT = ["BR", "CA", "MX", "US"];

// default paper sizes of the supported locales
const PAPERSIZE_BY_FORMAT = {
    Letter: { width: 21590, height: 27940 },
    A4: { width: 21000, height: 29700 }
};

// a logger bound to a specific URL option
export const textLogger = new Logger("office:log-textutils", { tag: "TEXT", tagColor: 0x00FF00 });

/**
 * Characters which starts a composition on the Mac as they can be used to create composed characters
 */
export const MAC_COMPOSE_CHARS = ["^", "~", "\xb4", "`"];

/**
 * Default background attributes for drawing.
 */
export const NO_DRAWING_BACKGROUND = { fill: { type: "none", color: null, color2: null, gradient: null, bitmap: null, pattern: null } };

/**
 * Default background attributes for slide.
 */
export const NO_SLIDE_BACKGROUND = { fill: { type: null, color: null, color2: null, gradient: null, bitmap: null, pattern: null } };

// private methods ----------------------------------------------------------------

/**
 * sort assigned drawing by their "layer-order" attribute
 */
function sortDrawings(drawings: JQuery): JQuery {
    const getLayerOrder = (el: HTMLElement): number => parseInt(el.getAttribute("layer-order") || "", 10);
    const allDrawings = drawings.get();
    allDrawings.sort((elemA: HTMLElement, elemB: HTMLElement): number => { return getLayerOrder(elemA) - getLayerOrder(elemB); });
    drawings = $(allDrawings);
    return drawings;
}

/**
 * Calculating a specified margin into a specified rectangle with the
 * properties "top", "bottom", "left", "right", "width" and "height". The
 * values in both objects "clientRect" and "margins" must use the same
 * unit, but the values itself must be numbers without unit.
 *
 * @param clientRect
 *  The rectangle to be expanded.
 *
 * @param margins
 *  The margins object with the properties "marginLeft", "marginRight",
 *  "marginTop" and "marginBottom".
 *
 * @returns
 *  The expanded rectangle object.
 */
function expandRectangleWithMargin(clientRect: RectangleLike, margins: MarginSet): Rectangle {
    return new Rectangle(
        clientRect.left - margins.marginLeft,
        clientRect.top - margins.marginTop,
        clientRect.width + margins.marginLeft + margins.marginRight,
        clientRect.height + margins.marginTop + margins.marginBottom
    );
}

// methods ----------------------------------------------------------------

/**
 * Searches inside a specified text string for any character, that is
 * included in a list of characters. So this function is an expanded
 * version of the indexOf() method. Additionally it is possible to define a
 * start index inside the text string and to define the search direction.
 * The search in the text string stops immediately, when the first
 * specified character from the character list is found.
 *
 * TODO: This function can probably be performance improved by using a
 * regular expression.
 *
 * @param text
 *  The text string, in which the characters will be searched.
 *
 * @param pos
 *  The start position, at which the search will begin.
 *
 * @param charList
 *  The list of characters, that will be searched inside the string.
 *
 * @param [options]
 *  Optional parameters:
 *  @param [options.reverse=false]
 *      Whether the search shall happen in reverse order, meaning from the
 *      end of the string to the beginning. Default is, that the string is
 *      searched from beginning to end.
 *
 * @returns
 *  The index of the first found character specified in the character list
 *  inside the specified text string. If no character from the list was
 *  found, "-1" is returned.
 */
export function indexOfValuesInsideTextSpan(text: string, pos: number, charList: string[], options?: { reverse: boolean }): number {

    // whether at least one character in the specified char list was found
    let found;
    // whether the search shall be done in reverse direction
    const reverse = options?.reverse;

    if (reverse) {
        while (pos >= 0 && !_.contains(charList, text[pos])) { pos--; }
        found = (pos >= 0);
    } else {
        const length = text.length;
        while (pos < length && !_.contains(charList, text[pos])) { pos++; }
        found = (pos < length);
    }

    return found ? pos : -1;
}

/**
 * Tries to merge the passed text span with its next or previous sibling
 * text span. To be able to merge two text spans, they must contain equal
 * formatting attributes. If merging was successful, the sibling span will
 * be removed from the DOM.
 *
 * @param node
 *  The DOM node to be merged with its sibling text span. If this object is
 *  a jQuery object, uses the first DOM node it contains.
 *
 * @param next
 *  If set to true, will try to merge with the next span, otherwise with
 *  the previous text span.
 */
export function mergeSiblingTextSpans(node: NodeOrJQuery, next?: boolean): void {

    // passed node and sibling node, as DOM nodes
    node = getDomNode(node);
    const sibling = node[next ? "nextSibling" : "previousSibling"] as (HTMLElement | null);

    // both nodes must be text spans with the same attributes
    if (sibling && isTextSpan(node) && isTextSpan(sibling)) {

        if ($(sibling).text().length === 0) {

            $(sibling).remove(); // empty span -> no merge required

        } else if (hasEqualElementAttributes(node, sibling)) {

            // add text of the sibling text node to the passed text node
            const text = node.firstChild?.nodeValue || "";
            const siblingText = sibling.firstChild?.nodeValue || "";
            if (node.firstChild) { node.firstChild.nodeValue = next ? (text + siblingText) : (siblingText + text); }

            // remove the entire sibling span element
            $(sibling).remove();
        }
    }
}

/**
 * Returns the absolute CSS position of the passed DOM.Point, relative to a
 * specific root node and zoom factor.
 *
 * Info: Please use "Position.getPixelPositionFromDomPoint" from the OX Text
 * pixel API.
 *
 * @param point
 *  the point object, from which the CSS position should be calculated
 *
 * @param rootNode
 *  the calculated CSS positions are relative to this root node.
 *
 * @param zoomFactor
 *  the current active zoom factor of the application view.
 *
 * @returns
 *  with the following CSS position properties:
 *  - {Number} top
 *  - {Number} left
 */
export function getCSSPositionFromPoint(point: Point, rootNode: NodeOrJQuery, zoomFactor: number): TopLeftPoint | null {

    if (!point?.node?.parentNode) { return null; }

    const caretSpan = $("<span>").text("|");
    const cursorElement = $(point.node.parentNode) as JQuery;
    const cursorElementLineHeight = parseFloat(cursorElement.css("line-height"));
    const zoom = zoomFactor / 100;

    if (point.offset === undefined) { point.offset = 0; }

    // break cursor element on the text offset
    if (point.offset === 0) {
        cursorElement.before(caretSpan);
    } else {
        if (isSpan(cursorElement)) {
            splitTextSpan(cursorElement, point.offset, { append: true });
        }
        cursorElement.after(caretSpan);
    }

    // create caret overlay and calculate its position
    const caretSpanOffset = caretSpan.offset();
    const caretSpanOffsetTop = caretSpanOffset ? caretSpanOffset.top : 0;
    const caretSpanOffsetLeft = caretSpanOffset ? caretSpanOffset.left : 0;
    const caretSpanOuterHeight = caretSpan.outerHeight() || 0;
    const rootNodeOffset = $(rootNode).offset();
    const rootNodeOffsetTop = rootNodeOffset ? rootNodeOffset.top : 0;
    const rootNodeOffsetLeft = rootNodeOffset ? rootNodeOffset.left : 0;

    const caretTop = (caretSpanOffsetTop - rootNodeOffsetTop) / zoom  - cursorElementLineHeight  + caretSpanOuterHeight;
    const caretLeft = (caretSpanOffsetLeft - rootNodeOffsetLeft) / zoom;

    // restore original state of document
    caretSpan.remove();

    if (point.offset > 0) { mergeSiblingTextSpans(cursorElement, true); }

    return { top: caretTop, left: caretLeft };
}

/**
 * Returns the absolute CSS position of the passed DOM.Point, relative to a
 * specific root node and zoom factor.
 *
 * Info: Please use "Position.getPixelPositionFromDomPoint" from the OX Text
 * pixel API.
 *
 * @param point1
 *  the start point object, from which the CSS position should be calculated
 *
 * @param point2
 *  the end point object, from which the CSS position should be calculated
 *
 * @param rootNode
 *  the calculated CSS positions are relative to this root node.
 *
 * @param zoom
 *  the current active zoom factor of the application view.
 *
 * @param start
 *  TODO
 *
 * @returns
 *  The CSS position of the DOM points.
 */
export function getCSSRectangleFromPoints(point1: Point, point2: Point, rootNode: JQuery | HTMLElement, zoom: number, start: boolean): RectangleLike {

    const range = document.createRange();
    let rect;

    if (point1.offset === undefined) { point1.offset = 0; }
    if (point2 && point2.offset === undefined) { point2.offset = 0; }

    range.setStart(point1.node, point1.offset);
    if (point2) {
        range.setEnd(point2.node, point2.offset!);
    } else  if (point1.offset + 1 <= (point1.node as Text).length) { // TODO: check
        range.setEnd(point1.node, point1.offset + 1);
    } else if (!start && (point1.node as Text).length > 0) { // TODO: check
        range.setStart(point1.node, point1.offset - 1);
        range.setEnd(point1.node, point1.offset);
        rect = range.getBoundingClientRect();
        rect = {
            top: rect.top,
            left: rect.right,
            height: rect.height,
            width: 0
        };
    } else {
        const nextNode = point1.node.nextSibling;
        if (nextNode) {
            range.setEnd(nextNode, 0);
        } else if (point1.node.parentNode) {

            const point1ParentNode = $(point1.node.parentNode);
            const point1ParentNodeOffset = point1ParentNode.offset();
            const point1ParentNodeOffsetLeft = point1ParentNodeOffset ? point1ParentNodeOffset.left : 0;
            const point1ParentNodeOffsetTop = point1ParentNodeOffset ? point1ParentNodeOffset.top : 0;
            const point1ParentNodeWidth = point1ParentNode.width() || 0;
            const point1ParentNodeHeight = point1ParentNode.height() || 0;

            const left = point1ParentNodeOffsetLeft + point1ParentNodeWidth * zoom;

            rect = {
                top: point1ParentNodeOffsetTop,
                left,
                height: point1ParentNodeHeight * zoom,
                width: 0
            };
        }
    }

    rect ??= range.getBoundingClientRect();

    const rootNodeOffset = $(rootNode).offset();
    const rootNodeOffsetTop = rootNodeOffset ? rootNodeOffset.top : 0;
    const rootNodeOffsetLeft = rootNodeOffset ? rootNodeOffset.left : 0;

    return { top: (rect.top - rootNodeOffsetTop) / zoom, left: (rect.left - rootNodeOffsetLeft) / zoom, height: rect.height / zoom, width: rect.width / zoom };
}

/**
 * Returns whether the passed intervals overlap with at least one index.
 *
 * @param interval1
 *  The first index interval, with the zero-based index properties "first"
 *  and "last".
 *
 * @param interval2
 *  The second index interval, with the zero-based index properties "first"
 *  and "last".
 *
 * @returns
 *  Whether the passed intervals are overlapping.
 */
export function intervalOverlapsInterval(interval1: IntervalType, interval2: IntervalType): boolean {
    return (interval1.first <= interval2.last) && (interval2.first <= interval1.last);
}

/**
 * Returns an array without "falsy" elements.
 *
 * @param arr
 *  The array, in which "falsy" elements will be removed.
 *
 * @returns
 *  An array without "falsy" elements.
 */
export function removeFalsyItemsInArray<T>(arr: T[]): T[] { // TODO: Check
    return arr.filter((item: T) => { return item; });
}

/**
 * Generating the initials for a given author name.
 *
 * @param authorName
 *  The author name.
 */
export function generateInitialsFromAuthorName(authorName: string): string {
    return authorName.split(/[-_.,;:\s]+/).reduce((initials, part) => {
        return initials + unicode.charAt(part, 0); // DOCS-2602: first Unicode character
    }, "").toUpperCase();
}

/**
 * Helper function to get the "character" attributes from a first empty text
 * span in a paragraph. This is used to restore the character attributes at a
 * paragraph, before the paragraph is removed or merged. Then the correct undo
 * operation can be generated.
 *
 * @param element
 *  The DOM element.
 *
 * @returns
 *  The character attributes of an first empty text span. Or null if this does not
 *  exist.
 */
export function getCharacterAttributesFromEmptyTextSpan(element: HTMLElement | JQuery): Partial<CharacterAttributes> | null {

    // the character attributes
    let characterAttributes = null;

    if (isParagraphNode(element)) {

        // saving character attributes from empty text spans at paragraph
        const firstSpan = findFirstPortionSpan(element);
        if (isEmptySpan(firstSpan) && ("character" in getExplicitAttributeSet(firstSpan))) {
            characterAttributes = getExplicitAttributeSet(firstSpan).character as Partial<CharacterAttributes>;
        }
    }

    return characterAttributes;
}

/**
 * Checks, whether two directly following insertText operations can be merged. This merging
 * happens for two reasons:
 * 1. Create undo operations, that remove complete words, not only characters.
 * 2. Reduce the number of operations before sending them to the server.
 *
 * The merge is possible under the following circumstances:
 * 1. The next operation has no attributes AND the previous operation is NOT a
 *    change tracked operation
 * OR
 * 2. The next operation has only the "changes" attribute (no other attributes
 *    allowed) and the previous operation has the same "changes" attribute.
 *    In this comparison not the complete "changes" attribute is compared, but
 *    only the "author" property of the "inserted" object. The date is ignored.
 *
 * @param lastOperation
 *  The preceeding operation that will be extended if possible.
 *
 * @param nextOperation
 *  The following operation that will be tried to merge into the preceeding
 *  operation.
 *
 * @returns
 *  Whether the two (insertText) operations can be merged successfully.
 */
export const canMergeNeighboringOperations = fun.do(() => {

    // Checking, if a specified operation is a change track operation.
    const isChangeTrackInsertOperation = (operation: OptChangesOperation): boolean => {
        return is.dict(operation.attrs) && is.dict(operation.attrs.changes);
    };

    // returns the author of an "insert" change action from the "changes" attribute of the passed operation
    const getInsertAuthor = (operation: OptChangesOperation): string | undefined => {
        return operation.attrs?.changes?.inserted?.author;
    };

    // Checking, if two following operations can be merged, although the next operation
    // contains an attribute. Merging is possible, if the attribute object only contains
    // the "changes" object added by change tracking. If the previous operation contains
    // the same changes object, a merge is possible.
    const isValidNextChangesAttribute = (lastOperation: OptChangesOperation, nextOperation: OptChangesOperation): boolean => {

        // extract the change authors from both operations
        const lastAuthor = getInsertAuthor(lastOperation);
        const nextAuthor = getInsertAuthor(nextOperation);

        // if the authors are identical, the operations can be merged (ignoring the time)
        // (the "changes" attribute family must be the only one in the next operation!)
        return is.string(lastAuthor) && is.string(nextAuthor) && (lastAuthor === nextAuthor) && !!nextOperation.attrs && dict.singleKey(nextOperation.attrs);
    };

    // return public method from local scope
    return (lastOperation: OptChangesOperation, nextOperation: OptChangesOperation) => {
        // 1. There are no attributes in the next operation AND the previous operation was not a change track operation OR
        // 2. Both are change track operations and have valid change track attributes
        return ((!("attrs" in nextOperation) && !isChangeTrackInsertOperation(lastOperation)) || isValidNextChangesAttribute(lastOperation, nextOperation));
    };
});

/**
 * Returns an Array of all absolute drawings in assigned rootNode,
 * this included anchored Drawing on the page it self and in pages paragraphs
 *
 * @param rootNode
 *  Parent node of the drawings (header or footer)
 *
 * @returns
 *  Returns a sorted Array all absolute drawings in assigned rootNode,
 */
export const getAllAbsoluteDrawingsOnNode = textLogger.profileMethod("TextUtils.getAllAbsoluteDrawingsOnNode", (targetRootNode: JQuery): JQuery => {

    const subSelector = ".drawing.absolute";
    const selector = ">.textdrawinglayer >" + subSelector + ", >.marginalcontent>.p " + subSelector;

    return sortDrawings(targetRootNode.find(selector));
});

/**
 * Returns an array of all absolute drawings in text-app node,
 * this included anchored Drawing on the page it self and in pages paragraphs
 *
 * @param targetRootNode
 *  Node of the current text-app
 *
 * @param pageNumber
 *  pageNumbers begin at 1
 *
 * @returns
 *  Returns a sorted Array all absolute drawings in textdrawinglayer and textcontent,
 */
export const getAllAbsoluteDrawingsOnPage = textLogger.profileMethod("TextUtils.getAllAbsoluteDrawingsOnPage", (targetRootNode: JQuery, pageNumber: number): JQuery => {

    const subSelector = ".drawing.absolute:not(.grouped)";
    const selector = `>.textdrawinglayer > ${subSelector}[page-number="${pageNumber}"]`;

    let allPageDrawings = targetRootNode.find(selector);

    const pageBreaks = targetRootNode.find(".page-break");

    let fromPB =  $(pageBreaks[pageNumber - 2]);
    let toPB =  $(pageBreaks[pageNumber - 1]);
    let allParas = null;

    if (!fromPB.length || !toPB.length) {
        allParas = targetRootNode.find(">.pagecontent").children();
    }

    if (!fromPB.length && allParas) {
        fromPB = $(allParas[0]);
    } else if (fromPB.parent().is(".p")) {

        const nextAll = fromPB.nextAll().find(subSelector);
        allPageDrawings = allPageDrawings.add(nextAll);

        fromPB = fromPB.parent();
    }

    if (!toPB.length && allParas) {
        toPB = $(allParas.last());
    } else if (toPB.parent().is(".p")) {

        const prevAll = toPB.prevAll().find(subSelector);
        allPageDrawings = allPageDrawings.add(prevAll);

        toPB = toPB.parent();
    }

    const allBetween = fromPB.nextUntil(toPB).add(fromPB).add(toPB).find(subSelector);
    allPageDrawings = allPageDrawings.add(allBetween);

    return sortDrawings(allPageDrawings);
});

/**
 * returns an Array of HTMLElements whose Bounding Rectangle intersect the Bounding Rectangle of assigned compare list
 * exclusive the source Element
 *
 * @param source
 *  element to compare with
 *
 * @param compareList
 *  Array of HTMLElements for comparing their BoundingRectangles
 *
 * @param options
 *  Optional parameters:
 *  @param [options.marginKey=null]
 *      If this key is defined, an optional margin will be added to
 *      the rectangle calculated by the function "getBoundingClientRect".
 *      The information about the margins must be specified at the
 *      elements in a jQuery data object using the "marginKey" as key.
 *      In this data object the properties "marginTop", "marginBottom",
 *      "marginLeft" and "marginRight" must be specified.
 *
 * @returns
 *  Returns all elements which intersect with source element
 *  exclusive the source element
 */
export const findAllIntersections = textLogger.profileMethod("TextUtils.findAllIntersections", (source: HTMLElement, compareList: JQuery | HTMLElement[], options?: { marginKey?: string }): HTMLElement[] => {

    // the jQuerified source element
    const $source = $(source);
    // the rectangle around the source element as DOMRect
    const sourceRectDom = source.getBoundingClientRect();
    // the key name in the data object, that contain the margin size in pixel
    const marginKey = options?.marginKey || null;
    // the result array
    const result = [] as HTMLElement[];
    // the rectangle around the source element as Rectangle
    let sourceRect: Rectangle;

    // ensure to have an instance of the class Rectangle
    if (marginKey && $source.data(marginKey)) {
        sourceRect = expandRectangleWithMargin(sourceRectDom, $source.data(marginKey) as MarginSet);
    } else {
        sourceRect = Rectangle.from(sourceRectDom);
    }

    ary.forEach(compareList, compare => {
        if (compare === source) { return; }

        // the rectangle around the compare element
        let compareRect: RectangleLike = compare.getBoundingClientRect();

        if (marginKey && $(compare).data(marginKey)) {
            compareRect = expandRectangleWithMargin(compareRect, $(compare).data(marginKey) as MarginSet);
        }

        if (sourceRect.overlaps(compareRect)) {
            result.push(compare);
        }
    });

    return result;
});

/**
 * Sets assigned Drawings list z-index by their assigned order
 *
 * @param drawings
 *  sorted Array of HTMLElements
 *
 * @param startIndex
 *  defines the minimal z-order
 *  (header & Footer should have 11, normal page should have 41)
 */
export function sortDrawingsOrder(drawings: JQuery | HTMLElement[], startIndex: number): void {

    let newIndex = 0;
    const newBehindIndex = -drawings.length - 1; // paragraph aligned drawings behind the text must have a negative z-index
    ary.forEach(drawings, (dr, index) => {
        const drawingAttrs = getExplicitAttributes(dr, "drawing"); // is behindDoc always an explicit attribute?
        if (drawingAttrs.anchorBehindDoc) { // -> handling of drawings behind the text
            if (drawingAttrs.anchorVertBase === "page" || drawingAttrs.anchorVertBase === "margin" || drawingAttrs.anchorVertBase === "bottomMargin") {
                newIndex = 0; // page aligned drawings behind the text (they must have index 0)
                // Info: Using this mechanism for page aligned drawings, it is not possible to sort the stack of page aligned drawings,
                //       if there are several drawings with the flag "anchorBehindDoc". But this should be an edge case.
            } else {
                newIndex = newBehindIndex + index;  // paragraph aligned drawings must have a negative index
            }
        } else {
            newIndex = startIndex + index;
        }
        if (String(newIndex) !== dr.style.zIndex) {
            dr.style.zIndex = String(newIndex);
        }
    });
}

/**
 * Searching for an absolutely positioned drawing that contains the specified pixel position. This function
 * can be used to detect a drawing that got a mouse click. This is for examle required, if the drawing
 * is ordered behind a paragraph.
 *
 * @param absoluteDrawings
 *  A container with absolutely postioned drawings (for example as returned by "getAllAbsoluteDrawingsOnPage").
 *
 * @param clientX
 *  The horizontal pixel position (used from event.clientX).
 *
 * @param clientY
 *  The vertical pixel position (used from event.clientY).
 *
 * @returns
 *  The drawing at the specified pixel position. Or null, if there is no drawing.
 */
export function findAbsoluteDrawingNodeByPixelPosition(absoluteDrawings: JQuery | HTMLElement[], clientX: number, clientY: number): JQuery | null {

    let foundDrawing = false;
    let foundDrawingNode = null;

    ary.forEach(absoluteDrawings, oneDrawing => {
        if (foundDrawing) { return; }
        const drawingRect = oneDrawing.getBoundingClientRect();
        foundDrawing = drawingRect.left <= clientX && clientX <= (drawingRect.left + drawingRect.width) && drawingRect.top <= clientY && clientY <= (drawingRect.top + drawingRect.height);
        if (foundDrawing) { foundDrawingNode = $(oneDrawing); }
    });

    return foundDrawingNode;
}

/**
 * Reducing the probability, that a paragraph aligned drawing with textWrapMode set to "none"
 * conflicts with the neighbour paragraphs by modifying the z-index of the current paragraph.
 * Warning: This only reduces the probability for problems. If a paragraph contains drawings
 * above and below the text that additionally reach into the neighbour paragraph(s), this change
 * of z-index for the current paragraph is not sufficient.
 *
 * @param paragraph
 *  The paragraph node, whose z-index might be increased or decreased.
 *
 * @param [drawing]
 *  Optional drawing node. If one specific drawing node is specified and this drawing has
 *  "textWrapMode" set to "none", only this drawing determines, how the z-index of the paragraph
 *  is modified.
 */
export function handleParagraphIndex(paragraph: NodeOrJQuery, drawing?: HTMLElement | JQuery): void {

    let drawingAttrs: Partial<TextDrawingAttributes>;
    let fullParagraphCheck = true;
    let isNextParaInc = false;
    let setParaIndex = true;

    const allClassNamesString = "incZIndex decZIndex doubleIncZIndex";

    // helper function to check, if a drawing is (partly) positioned outside its paragraph
    const outsideParagraph = (drawingNode: NodeOrJQuery): boolean => {
        const offset = is.number(drawingAttrs.anchorVertOffset) ? convertHmmToLength(drawingAttrs.anchorVertOffset, "px", 1) : null;
        const drawingHeight = $(drawingNode).height() || 0;
        const paragraphHeight = $(paragraph).height() || 0;
        return (offset !== null) && (offset < 0 || (offset + drawingHeight) > paragraphHeight);
    };

    // helper function to check, if the classes at the previous paragraph need to be increased to double inc
    const handleIncOfPreviousParagraph = (para: NodeOrJQuery): void => {
        const prevPara = $(para).prev();
        if (prevPara.length > 0 && prevPara.hasClass("incZIndex")) {
            prevPara.removeClass("incZIndex");
            prevPara.addClass("doubleIncZIndex");
        }
    };

    // helper function to check, if the classes at the previous paragraph need to be decreased to simple inc
    const handleDecOfPreviousParagraph = (para: NodeOrJQuery): void => {
        const prevPara = $(para).prev();
        if (prevPara.length > 0 && prevPara.hasClass("doubleIncZIndex")) {
            prevPara.removeClass("doubleIncZIndex");
            prevPara.addClass("incZIndex");
        }
    };

    // helper function to check, if the classes at the next paragraph are set
    const checkIncOfNextParagraph = (para: NodeOrJQuery): boolean => {
        const nextPara = $(para).next();
        return (nextPara.length > 0 && (nextPara.hasClass("incZIndex") || nextPara.hasClass("doubleIncZIndex")));
    };

    // helper function to handle the increasing or decreasing of the current paragraph
    const handleZIndexForParagraph = (): void => {
        const behindDoc = !!drawingAttrs.anchorBehindDoc;
        $(paragraph).removeClass(allClassNamesString); // it might have been set before
        if (!behindDoc) { isNextParaInc = checkIncOfNextParagraph(paragraph); } // check, if the following paragraph is already increased
        if (behindDoc && is.number(drawingAttrs.anchorVertOffset) && drawingAttrs.anchorVertOffset >= 0) { setParaIndex = false; } // decreasing the z-index for underlying drawings is only required for negative vertical offset
        if (!behindDoc && is.number(drawingAttrs.anchorVertOffset) && drawingAttrs.anchorVertOffset < 0) { setParaIndex = false; } // increasing the z-index for overlay drawings is only required for positive vertical offset
        if (setParaIndex) { $(paragraph).addClass(behindDoc ? "decZIndex" : (isNextParaInc ? "doubleIncZIndex" : "incZIndex")); }
        if (behindDoc) {
            handleDecOfPreviousParagraph(paragraph);
        } else if (setParaIndex) {
            handleIncOfPreviousParagraph(paragraph);
        }
    };

    if (drawing) {
        drawingAttrs = getExplicitAttributes(drawing, "drawing"); // is behindDoc always an explicit attribute?
        if ((drawingAttrs.textWrapMode === "none") && outsideParagraph(drawing)) {
            handleZIndexForParagraph();
            fullParagraphCheck = false; // no further check of paragraph required (ignoring additional drawings)
        }
    }

    if (fullParagraphCheck) {
        // general check for paragraph aligned drawings that have "textWrapMode" set to "none"
        // This check is required after:
        //   - deleting a drawing
        //   - moving a drawing
        //   - splitting or merging of paragraphs
        //   - switching the drawing mode (inline, paragraph-aligned, ...)
        const allDrawings = $(paragraph).children(".drawing:not(.inline)"); // not inline!, but textWrapMode set to "none"
        if (allDrawings.length) {
            const oneDrawing = allDrawings.get().find(localDrawing => {
                drawingAttrs = getExplicitAttributes(localDrawing, "drawing"); // is behindDoc always an explicit attribute?
                return (drawingAttrs.textWrapMode === "none") && outsideParagraph(localDrawing);
            });
            if (oneDrawing) {
                // using ony the first drawing (not 100% reliable), "drawingAttrs" are still valid for the found drawing
                handleZIndexForParagraph();
            } else {
                $(paragraph).removeClass(allClassNamesString); // it might have been set before
                handleDecOfPreviousParagraph(paragraph);
            }
        } else {
            $(paragraph).removeClass(allClassNamesString); // it might have been set before
            handleDecOfPreviousParagraph(paragraph);
        }
    }
}

/**
 * Return the pageNumber for absolute positioned drawings.
 *
 * @param drawingEl
 *  A drawing node
 *
 * @param docModel
 *  The model object
 *
 * @returns
 *  The page number of the specified drawing
 */
export const getPageNumber = textLogger.profileMethod("TextUtils.getPageNumber", (drawingEl: HTMLElement, docModel: TextBaseModel): number => {

    const pageNumberString = drawingEl.getAttribute("page-number");
    const parentElement = drawingEl.parentNode;
    const classList = parentElement ? (parentElement as HTMLElement).classList : null;
    const pageDrawing = classList ? classList.contains("textdrawinglayer") : false;

    if (pageNumberString && !pageDrawing) {
        globalLogger.error("drawing has attribute page number, but is a paragraph oriented drawing!!!");
        drawingEl.removeAttribute("page-number");
    }

    let pageNumber: number;
    if (!pageNumberString) {
        pageNumber = docModel.getPageLayout().getPageNumber(drawingEl);
        if (pageDrawing) {
            drawingEl.setAttribute("page-number", String(pageNumber));
        }
    } else {
        pageNumber = parseInt(pageNumberString, 10);
    }

    return pageNumber;
});

/**
 * test the assigned string for hangul signs, used in korean language
 */
export function isHangulText(text: string): boolean {
    return HANGUL.test(text);
}

/**
 * Returns the default paper size of the current UI locale.
 *
 * @returns
 *  An object with the properties "width" and "height" of the paper size.
 *  If the locale is not supported return the page size of DIN A4.
 */
export function getDefaultPaperSize(): Size {
    const region = LOCALE_DATA.region;
    const paperFormat = region && DEFAULT_LETTER_FORMAT.includes(region) ? "Letter" : "A4";
    return PAPERSIZE_BY_FORMAT[paperFormat];
}

/**
 * Receiving the attributes that are necessary to remove a drawing background.
 *
 * @returns
 *  The attributes required to remove the drawing background.
 */
export function getEmptyDrawingBackgroundAttributes(): NO_DRAWING_BACKGROUND_TYPE {
    return json.deepClone(NO_DRAWING_BACKGROUND);
}

/**
 * Receiving the attributes that are necessary to remove a slide background.
 *
 * @returns
 *  The attributes required to remove the slide background.
 */
export function getEmptySlideBackgroundAttributes(): NO_SLIDE_BACKGROUND_TYPE {
    return json.deepClone(NO_SLIDE_BACKGROUND);
}

/**
 * This function converts the properties of a rectangle object ("top", "left",
 * "width" and "height"). Specified is a rectangle that has values in pixel
 * relative to the app-content-root node. This is for example created by the
 * selectionBox. All properties are converted into values relative to the page.
 *
 * @param app
 *  The application containing this instance.
 *
 * @param box
 *  The rectangle with the properties "top", "left", "width" and "height".
 *  The values are given in pixel relative to the app-content-root node.
 *
 * @returns
 *  The rectangle with the properties "top", "left", "width" and "height".
 *  The values are given in pixel relative to the page node.
 */
export function convertAppContentBoxToPageBox(app: TextBaseApplication, box: RectangleLike): RectangleLike {

    // the current zoom factor
    const zoomFactor = app.docView.getZoomFactor();
    // the offset of the active slide inside the content root node
    const pageOffset = app.docModel.getNode().offset();
    // the content root node, the base for the position calculation
    const contentRootNode = app.docView.getContentRootNode();
    // the position of the content root node
    const posOffset = contentRootNode.offset();
    // the horizontal scroll shift
    const scrollLeft = contentRootNode.scrollLeft() || 0;
    // the vertical scroll shift
    const scrollTop = contentRootNode.scrollTop() || 0;

    const pageOffsetLeft = pageOffset ? pageOffset.left : 0;
    const pageOffsetTop = pageOffset ? pageOffset.top : 0;
    const posOffsetLeft = posOffset ? posOffset.left : 0;
    const posOffsetTop = posOffset ? posOffset.top : 0;

    return {
        // the left position of the user defined box relative to the slide (can be negative)
        left: Math.round((box.left - (pageOffsetLeft - posOffsetLeft) - scrollLeft) / zoomFactor),
        // the top position of the user defined box relative to the slide (can be negative)
        top: Math.round((box.top - (pageOffsetTop - posOffsetTop) - scrollTop) / zoomFactor),
        // the width of the box
        width: Math.round(box.width / zoomFactor),
        // the height of the box
        height: Math.round(box.height / zoomFactor)
    };
}
