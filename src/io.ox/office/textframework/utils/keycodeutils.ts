/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is } from "@/io.ox/office/tk/algorithms";

import { KeyCode, hasKeyCode, matchModifierKeys, matchKeyCode } from "@/io.ox/office/tk/dom";

// public functions =======================================================

/**
 * Returns true, if the passed keyboard event is the global F6 accessibility key event
 *
 * @param event
 *  A jQuery keyboard event object.
 *
 * @returns
 *  Whether the passed event is the global F6 accessibility key event.
 */
export function isF6AcessibilityKeyEvent(event: JEvent): boolean {
    // ignore all modifier keys
    return hasKeyCode(event, "F6");
}

/**
 * Returns true, if the passed keyboard event is ctrl+f, meta+f, ctrl+g, meta+g, ctrl+shift+g or meta+shift+g.
 *
 * @param event
 *  A jQuery keyboard event object.
 *
 * @returns
 *  Whether the passed event is ctrl+f, meta+f, ctrl+g, meta+g, ctrl+shift+g or meta+shift+g.
 */
export function isSearchKeyEvent(event: JEvent): boolean {
    return matchModifierKeys(event, { ctrlOrMeta: true, shift: null }) && (event.charCode === 102 || event.charCode === 103 || hasKeyCode(event, "F", "G"));
}

/**
 * Returns true, if the passed keyboard event is a common browser keyboard shortcut
 * that should be handled by the browser itself.
 *
 * @param event
 *  A jQuery keyboard event object.
 *
 * @returns
 *  Whether the passed event is a browser shortcut.
 */
export function isBrowserShortcutKeyEvent(event: JEvent): boolean {

    // Ctrl
    const ctrl = matchModifierKeys(event, { ctrl: true });
    // Ctrl and Shift
    const ctrlShift = matchModifierKeys(event, { ctrl: true, shift: true });
    // Meta and Alt
    const metaAlt = matchModifierKeys(event, { meta: true, alt: true });
    // Meta and Shift
    const metaShift = matchModifierKeys(event, { meta: true, shift: true });
    // Ctrl and Meta
    const ctrlMeta = matchModifierKeys(event, { ctrl: true, meta: true });
    // Ctrl or Meta
    const ctrlOrMeta = matchModifierKeys(event, { ctrlOrMeta: true });
    // Ctrl or Meta and Shift
    const ctrlOrMetaShift = matchModifierKeys(event, { ctrlOrMeta: true, shift: true });
    // Ctrl or Meta and Alt
    const ctrlOrMetaAlt = matchModifierKeys(event, { ctrlOrMeta: true, alt: true });

    if (event.type === "keypress") {
        // check charCode for keypress event

        // Switch to the specified/last tab - Ctrl + 1...8, Cmd + 1...8 / Ctrl + 9, Cmd + 9
        if (ctrlOrMeta && is.number(event.charCode) && event.charCode >= 49 && event.charCode <= 57) {
            return true;
        }

        // Open a new tab             - Ctrl + T, Cmd + T
        // Reopen the last closed tab - Ctrl + Shift + T, Cmd + Shift + T
        if ((ctrlOrMeta || ctrlOrMetaShift) && (event.charCode === 116 || event.charCode === 84)) {
            return true;
        }

        // Close the current tab     - Ctrl + W, Ctrl + F4, Cmd + W
        // Open a new browser window - Ctrl + N, Cmd + N
        if (ctrlOrMeta && (event.charCode === 119 || event.charCode === 110)) {
            return true;
        }

        // Close the current window - Ctrl + Shift + W, Cmd + Shift + W
        if (ctrlOrMetaShift && (event.charCode === 87 || event.charCode === 119)) {
            return true;
        }

        // Open a new window in incognito mode - Ctrl + Shift + N, Cmd + Shift + N, Ctrl + Shift + P, Cmd + Shift + P
        if (ctrlOrMetaShift && (event.charCode === 78 || event.charCode === 80)) {
            return true;
        }

        // Minimize the current window - Ctrl + M, Cmd + M
        if (ctrlOrMeta && event.charCode === 109) {
            return true;
        }

        // Hide browser - Ctrl + H, Cmd + H
        // Hide all other windows - Ctrl + Alt + H, Cmd + Alt + H
        if ((ctrlOrMeta || ctrlOrMetaAlt) && (event.charCode === 104 || event.charCode === 170)) {
            return true;
        }

        // Quit browser - Ctrl + Q, Cmd + Q
        if (ctrlOrMeta && event.charCode === 113) {
            return true;
        }

        // Zoom in on the page - Ctrl + "+", Cmd + "+"
        // Zoom out on the page - Ctrl + "-", Cmd + "-"
        // Reset zoom level - Ctrl + 0, Cmd + 0
        if (ctrlOrMeta && (event.charCode === 43 || event.charCode === 45 || event.charCode === 48)) {
            return true;
        }

        // Full-screen mode - Ctrl + Cmd + F
        if (ctrlMeta && (event.charCode === 102 || event.charCode === 6)) { //6 is for safari
            return true;
        }

        // Open the browsing history - Ctrl + H, Cmd + Shift + H
        if ((ctrl || metaShift) && (event.charCode === 72 || event.charCode === 104)) {
            return true;
        }

        // Open the download history - Ctrl + J, Cmd + J, Cmd + Shift + J, Cmd + Alt + 2
        if (((ctrlOrMeta || metaShift) && (event.charCode === 106 || event.charCode === 74)) || (metaAlt && event.charCode === 8220)) {
            return true;
        }

        // Bookmark the current web site - Ctrl + D, Cmd + D
        if (ctrlOrMeta && event.charCode === 100) {
            return true;
        }

        // Toggles the bookmarks bar                                               - Ctrl + Shift + B, Cmd + Shift + B
        // Save all open pages in your current window as bookmarks in a new folder - Ctrl + Shift + D, Cmd + Shift + D
        if (ctrlOrMetaShift && (event.charCode === 66 || event.charCode === 68)) {
            return true;
        }

        // Open the Bookmarks window - Ctrl + Shift + B, Cmd + Shift + B, Cmd + Alt + B
        if ((ctrlOrMetaShift || metaAlt) && (event.charCode === 66 || event.charCode === 8747)) {
            return true;
        }

        // Open Developer Tools - Ctrl + Shift + (I,J,K,S), Cmd + Alt + (I,J,K,S)
        if ((ctrlShift || metaAlt) && (event.charCode === 8260 || event.charCode === 73 ||
            event.charCode === 186  || event.charCode === 74 ||
            event.charCode === 8710 || event.charCode === 75 ||
            event.charCode === 8218 || event.charCode === 83)) {
            return true;
        }

        // For function keys Firefox sends a keydown event with corresponding keyCode as expected,
        // but also sends a keypress event with charCode 0 and a corresponding keyCode.
        if (_.browser.Firefox && (event.charCode === 0)) {

            // Full-screen mode - F11, Open Firebug - F12
            if (hasKeyCode(event, "F11", "F12")) {
                return true;
            }
        }

    } else {
        // check keyCode for keyup and keydown events

        // Switch to the specified/last tab - Ctrl + 1...8, Cmd + 1...8 / Ctrl + 9, Cmd + 9
        if (ctrlOrMeta && is.number(event.keyCode) && event.keyCode >= KeyCode["1"] && event.keyCode <= KeyCode["9"]) {
            return true;
        }

        // Switch to the next/previous tab - Ctrl + Tab / Ctrl + Shift + Tab
        if ((matchKeyCode(event, "TAB", { ctrl: true }) || matchKeyCode(event, "TAB", { ctrl: true, shift: true }))) {
            return true;
        }

        // Open a new tab             - Ctrl + T, Cmd + T
        // Reopen the last closed tab - Ctrl + Shift + T, Cmd + Shift + T
        if ((ctrlOrMeta || ctrlOrMetaShift) && hasKeyCode(event, "T")) {
            return true;
        }

        // Close the current tab     - Ctrl + W, Ctrl + F4, Cmd + W
        // Open a new browser window - Ctrl + N, Cmd + N
        if (ctrlOrMeta && hasKeyCode(event, "W", "F4", "N")) {
            return true;
        }

        // Close the current window - Alt + F4, Ctrl + Shift + W, Cmd + Shift + W
        if (matchKeyCode(event, "F4", { alt: true }) || (ctrlOrMetaShift && hasKeyCode(event, "W"))) {
            return true;
        }

        // Open a new window in incognito mode - Ctrl + Shift + N, Cmd + Shift + N, Ctrl + Shift + P, Cmd + Shift + P
        if (ctrlOrMetaShift && hasKeyCode(event, "N", "P")) {
            return true;
        }

        // Minimize the current window - Ctrl + M, Cmd + M
        if (ctrlOrMeta && hasKeyCode(event, "M")) {
            return true;
        }

        // Hide browser - Ctrl + H, Cmd + H
        // Hide all other windows - Ctrl + Alt + H, Cmd + Alt + H
        if ((ctrlOrMeta || ctrlOrMetaAlt) && hasKeyCode(event, "H")) {
            return true;
        }

        // Quit browser - Ctrl + Q, Cmd + Q
        if (ctrlOrMeta && hasKeyCode(event, "Q")) {
            return true;
        }

        // Zoom in on the page - Ctrl + "+", Cmd + "+"
        // Zoom out on the page - Ctrl + "-", Cmd + "-"
        // Reset zoom level - Ctrl + 0, Cmd + 0
        if (ctrlOrMeta && hasKeyCode(event, "NUM_PLUS", "EQUAL_SIGN" /*Safari*/, "MOZ_PLUS", "NUM_MINUS", "DASH" /*Safari*/, "MOZ_DASH", "NUM_0", "0")) {
            return true;
        }

        // Full-screen mode - F11, Ctrl + Cmd + F
        if (hasKeyCode(event, "F11") || (ctrlMeta && hasKeyCode(event, "F"))) {
            return true;
        }

        // Open the browsing history - Ctrl + H, Cmd + Shift + H
        if ((ctrl || metaShift) && hasKeyCode(event, "H")) {
            return true;
        }

        // Open the download history - Ctrl + J, Cmd + J, Cmd + Shift + J, Cmd + Alt + 2
        if (((ctrlOrMeta || metaShift) && hasKeyCode(event, "J")) || (metaAlt && hasKeyCode(event, "2"))) {
            return true;
        }

        // Bookmark the current web site - Ctrl + D, Cmd + D
        if (ctrlOrMeta && hasKeyCode(event, "D")) {
            return true;
        }

        // Toggles the bookmarks bar                                               - Ctrl + Shift + B, Cmd + Shift + B
        // Save all open pages in your current window as bookmarks in a new folder - Ctrl + Shift + D, Cmd + Shift + D
        if (ctrlOrMetaShift && hasKeyCode(event, "B", "D")) {
            return true;
        }

        // Open the Bookmarks window - Ctrl + Shift + B, Cmd + Shift + B, Cmd + Alt + B
        if ((ctrlOrMetaShift || metaAlt) && hasKeyCode(event, "B")) {
            return true;
        }

        // Open Firebug - F12
        if (hasKeyCode(event, "F12")) {
            return true;
        }

        // Open Developer Tools - Ctrl + Shift + (I,J,K,S), Cmd + Alt + (I,J,K,S)
        if ((ctrlShift || metaAlt) && hasKeyCode(event, "I", "J", "K", "S")) {
            return true;
        }
    }

    return false;
}

/**
 * Getting the printable character for a specified char code.
 *
 * @param [charCode]
 *  The char code for that the corresponding printable char needs to be determined.
 *
 * @returns
 *  The character from the specified char code. Or undefined, if it cannot be determined.
 */
export function getPrintableCharFromCharCode(charCode?: number): string | undefined {
    return (is.number(charCode) && (charCode >= 32)) ? String.fromCharCode(charCode) : undefined;
}

/**
 * Getting the printable character for a specified event evaluating the event properties
 * "charCode" and "which".
 *
 * @param event
 *  A jQuery keyboard event object.
 *
 * @returns
 *  The character from the specified event. Or an empty string, if not character could be determined.
 */
export function getPrintableChar(event: JEvent): string {
    return getPrintableCharFromCharCode(event.charCode) || getPrintableCharFromCharCode(event.which) || "";
}
