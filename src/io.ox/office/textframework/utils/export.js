/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { BREAK, convertHmmToLength, escapeHTML, getArrayOption, getBooleanOption,
    getStringOption } from '@/io.ox/office/tk/utils';
import { ensureExistingTextNode, getBase64FromImageNode, isDocumentImageNode, isDrawingFrame,
    isHardBreakNode, isImageNode, isParagraphNode, getUrlParamFromImageNode, isSpan, isTableNode,
    isTabNode } from '@/io.ox/office/textframework/utils/dom';
import { isValidURL } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { getMimeTypeFromImageUri } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { getDrawingType } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { Color } from '@/io.ox/office/editframework/utils/color';

// constants ==============================================================

var COPY_STYLES = {
    paragraph: ['text-align', 'background-color', 'padding', 'margin', 'text-indent', 'margin', 'border-color', 'border-style', 'border-width'],
    span: ['font-family', 'font-weight', 'text-decoration', 'color', 'font-size', 'vertical-align', 'line-height', 'lang'],
    textframe_p: ['margin', 'padding'],
    table: ['width', 'background-color'],
    tr: ['background-color'],
    td: ['height', 'background-color', 'border-top', 'border-right', 'border-bottom', 'border-left', 'padding'],
    img: ['width', 'height', 'border', 'margin', 'title']
};

// public functions =======================================================

var Export = {};

/**
 * Converts the internal HTML mark-up of the current selection to
 * normalized HTML mark-up that can be exported (e.g. to the clipboard).
 *
 * @param {Editor} docModel
 *  The text model instance.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {String} [options.clipboardId]
 *      A unique id to identify clipboard operation data. Will be attached
 *      to the returned HTML mark-up.
 *  @param {Array} [options.clipboardOperations]
 *      An array with clipboard operations. Will be attached to the
 *      returned HTML mark-up.
 *  @param {String} [options.clipboardOrigin]
 *      A String to determine whether the cliboard data origin is OX Text
 *      or OX Presentation.
 *  @param {String} [options.clipboardOriginId]
 *      An id to identify the clipboard origin
 *
 * @returns {String}
 *  The normalized HTML mark-up for export.
 */
Export.getHTMLFromSelection = function (docModel, options) {
    /**
     * Returns the currently selected contents as HTML mark-up.
     *
     * @returns {String}
     *  The selected content, as HTML mark-up string.
     */
    function getHtmlFromBrowserSelection() {
        var returnValue = $('<div>');
        var allDrawingsInSelection = selection.getAllDrawingsInSelection();

        if (allDrawingsInSelection.length > 0) {
            _.each(allDrawingsInSelection, function (drawing) {
                returnValue.append(parseSelection(docModel, selection, drawing, { type: 'html' }));
                if (_.device('android')) {
                    returnValue.append('<span>&#8203;</span>');
                }
            });
        } else {
            returnValue.append('<div>');
        }

        return returnValue;
    }

    var resultDiv           = $('<div>'),
        selection           = docModel.getSelection(),
        listStyles          = docModel.getListCollection(),
        paraStylesIds       = createParagraphStyleIds(selection, docModel.paragraphStyles),
        clipboardId         = getStringOption(options, 'clipboardId', ''),
        clipboardOperations = getArrayOption(options, 'clipboardOperations', []),
        clipboardOrigin     = getStringOption(options, 'clipboardOrigin', ''),
        clipboardOriginId   = getStringOption(options, 'clipboardOriginId', ''),
        isEncodeBase64      = getBooleanOption(options, 'isEncodeBase64', true),
        html                = docModel.useSlideMode() ? getHtmlFromBrowserSelection() : $('<span>').html(selection.getHtmlFromBrowserSelection()),
        metaElement;

    if (!docModel.useSlideMode()) {
        createOptimizedStructure(html, resultDiv, paraStylesIds, listStyles, isEncodeBase64);
    } else {
        resultDiv = html;
    }
    clearParagraphStyleIds(selection);

    // use the first child to store meta information on
    metaElement = resultDiv.children().first();
    if (metaElement) {
        metaElement.attr('id', 'ox-clipboard-data');
        if (clipboardId.length > 0) {
            metaElement.attr('data-ox-clipboard-id', clipboardId);
        }
        if (clipboardOperations.length > 0) {
            metaElement.attr('data-ox-operations', JSON.stringify(clipboardOperations));
        }
        if (clipboardOrigin.length > 0) {
            metaElement.attr('data-ox-origin', clipboardOrigin);
        }
        if (clipboardOriginId.length > 0) {
            metaElement.attr('data-ox-origin-id', clipboardOriginId);
        }
    }
    if (_.device('android') && resultDiv.children().length === 1) {
        resultDiv.append('<span>&#8203;</span>');
    }
    return resultDiv.html();
};

Export.getPlainTextFromSelection = function (docModel) {
    var selection       = docModel.getSelection(),
        text            = '';

    function getTextFromDrawingSelection() {
        var returnValue = '';

        _.each(selection.getAllDrawingsInSelection(), function (drawing, i) {
            if (i > 0) { returnValue += '\r\n\r\n'; }
            returnValue += parseSelection(docModel, selection, drawing, { type: 'text' });
        });
        return returnValue;
    }

    // iterate through all drawings and get their texts
    text += getTextFromDrawingSelection();

    return text;
};

function getPreparedText(arrElements, options) {
    var type = getStringOption(options, 'type', 'html'),
        text = '',
        i = 0;

    function addText(element) {
        if (!_.isEmpty(element.textContent)) {
            text += element.textContent;
            i++;
        }
    }

    function iterateChildren(arrElements) {
        _.each(arrElements, function (element) {
            var tmp_i = i;
            if ($(element).hasClass('selection') || (type === 'html' && $(element).is('table'))) { return; }

            // open the paragraph-element
            if (type === 'html' && $(element).hasClass('p')) { text += '<p style="margin: 0px; padding: 0px;">'; }

            if (element.children && element.children.length > 0) {
                if (type === 'text' && $(element).hasClass('p') && i > 0) { text += '\r\n'; } // a new line for every paragraph
                iterateChildren(element.children);
            } else {
                addText(element);
            }

            if (type === 'html' && $(element).hasClass('p')) {
                // if there were no text added, add a "placeholder" to create valid html
                if (tmp_i === i) { text += '&#8203;'; }
                // close the paragraph-element
                text += '</p>';
            }
        });
    }

    iterateChildren(arrElements);

    return text;
}

function parseSelection(docModel, selection, drawing, options) {

    const type = options?.type || 'html';
    const browserSelection = window.getSelection();

    // drawing selected OR no text is selected
    if (selection.getSelectionType() === 'drawing' || selection.isTextCursor()) {
        switch (getDrawingType(drawing)) {
            case 'shape':
            case 'connector':
                return extractShape(docModel, drawing, { type });
            case 'table':
                return extractTable(drawing, { type });
            case 'image':
                return extractImage(docModel, drawing, { type });
        }
        return null;
    }

    const result = $('<div>');
    // read all selection ranges
    for (var index = 0; index < browserSelection.rangeCount; index += 1) {
        var content = browserSelection.getRangeAt(index).cloneContents(),
            text = '';
        if (content.children && content.children.length > 0) {
            text += getPreparedText(content.children, { type });
        } else {
            text += $(content).text();
        }
        // result.text(text); // -> simpler solution that retains the line ends -> check for 56580 required
        // escape HTML to avoid XSS
        result.append(escapeHTML(text)); // 56580 (TODO: This fix removes the line breaks, for example see 68240)
    }
    return (type === 'html') ? $('<span>').append(result.html()) : result.text();
}

function extractImage(docModel, drawing, options) {
    var attributes  = drawing.first().data('attributes');

    if (getStringOption(options, 'type', 'html') !== 'html') { return attributes.drawing.name; }

    var width        = convertHmmToLength(attributes.drawing.width, 'px', 1);
    var height       = convertHmmToLength(attributes.drawing.height, 'px', 1);
    var imageUrl     = attributes.image.imageUrl;
    var base64String = attributes.image.imageData;

    if (!base64String && isDocumentImageNode(drawing)) {
        base64String = getBase64FromImageNode(drawing, getMimeTypeFromImageUri(imageUrl));
    }

    var imageSrc  = base64String ? base64String : imageUrl;
    var imageNode = $('<img>', { src: imageSrc, width: width + "px", height: height + "px" });

    return applyFillAndLineStyles(imageNode, docModel, drawing)[0];
}

function extractTable(drawing, options) {
    var sourceTable = ($(drawing).is('table')) ? $(drawing) : $(drawing).find('table'),
        rows        = sourceTable.find('th,tr'),
        table       = $('<table style="border-collapse: collapse;">'),
        text        = '';

    // apply table-styles
    copyStyles(sourceTable, table, 'table');

    rows.each(function (i, row) {
        var rowNode = $('<' + row.nodeName + '>'),
            cells   = $(row).find('td');

        // apply row-styles
        copyStyles(row, rowNode, 'tr');

        var cell_i = 0;
        cells.each(function (j, cell) {
            var cellText    = $(cell).text(),
                cellContent = $('<div class="cellcontent">').text(cellText),
                cellNode    = $('<td>').html(cellContent);

            // apply cell-styles
            copyStyles(cell, cellNode, 'td');
            // apply font-styles
            copyStyles($(cell).find('.p>span'), cellContent, 'span');

            // add td to tr
            rowNode.append(cellNode);

            // add celltext to "plain-text"-content
            text += cellText;
            if (cell_i < (cells.length - 1)) { text += '\t'; }
            cell_i++;
        });

        // add tr to table
        table.append(rowNode);

        // add 'newline' to "plain-text"
        text += '\n';
    });

    return (getStringOption(options, 'type', 'html') === 'html') ? $('<div>').append(table) : text;
}

function extractShape(docModel, drawing, options) {
    var type        = getStringOption(options, 'type', 'html'),
        attributes  = drawing.first().data('attributes'),
        canvas      = drawing.find('canvas'),
        textframe   = drawing.find('.textframe'),
        paragraph   = $(textframe).find('>.p'),
        span        = $(paragraph).find('>span'),
        text        = getPreparedText(drawing[0].children, { type }),
        content     = $('<div class="content">').append(escapeHTML(text)), // #55651 - escape HTML to avoid XSS
        shape       = (canvas.length > 0) ? $('<div class="shape" style="display: block; width: ' + canvas.width() + 'px; height: ' + canvas.height() + 'px;">') : $('<div>');

    copyStyles(paragraph, shape, 'textframe_p');
    copyStyles(span, content, 'span');

    // if we have a preset-shape
    if ((canvas.length > 0) && !(attributes.geometry && attributes.geometry.presetShape && attributes.geometry.presetShape === 'rect')) {
        shape.attr('style', shape.attr('style') + ' background-image: url(\'' + canvas[0].toDataURL() + '\'); background-repeat: no-repeat; background-position: top left;');

    // if we have a "normal" shape (rectangle)
    } else {
        shape = applyFillAndLineStyles(shape, docModel, drawing);
    }

    return (type === 'html') ? shape.append(content) : text;
}

function applyFillAndLineStyles(shape, docModel, drawing) {
    var attributes      = drawing.first().data('attributes'),
        themeModel      = docModel.getThemeModel(),

        fillC           = attributes.fill ? attributes.fill.color : null,
        fillColor       = fillC ? Color.parseJSON(fillC) : null,
        fillColorDesc   = fillColor ? fillColor.resolve('fill', themeModel) : null,

        lineC           = attributes.line ? attributes.line.color : null,
        lineColor       = lineC ? Color.parseJSON(lineC) : null,
        lineColorDesc   = lineColor ? lineColor.resolve('line', themeModel) : null,
        lineWidth       = lineColor ? Math.max(1, convertHmmToLength(attributes.line.width, 'px', 1)) : null,
        lineType        = 'solid';

    // if the shape has a background
    if (fillColorDesc) {
        shape.attr('style', (shape.attr('style') || '') + ' background: ' + fillColorDesc.css + ';');
    }

    // if the shape has a border
    if (lineWidth && lineColorDesc) {
        if (attributes.line.style === 'dotted') {
            lineType        = 'dotted';
        } else if (attributes.line.style === 'dashed') {
            lineType        = 'dashed';
        }

        shape.attr('style', (shape.attr('style') || '') + ' border: ' + lineWidth + 'px ' + lineType + ' ' + lineColorDesc.css + ';');
    }

    return shape;
}

function restoreHyperlinks($node) {
    var
        href        = $node.attr('data-hyperlink-url'),

        elmList     = (isValidURL(href) && [$node]) || $node.find('[data-hyperlink-url]').toArray().filter(function (elm) {
            return isValidURL($(elm).attr('data-hyperlink-url'));
        });

    elmList.forEach(function (elm) {
        var
            $elm  = $(elm),
            $link = $('<a href="' + $elm.attr('data-hyperlink-url') + '"/>');

        $elm.clone().appendTo($link);
        $elm.replaceWith($link);
    });
}

/**
 * Creates a 'normalized' DOM structure based on the source.
 *
 * @param {jQuery} source
 *  The source html. The function assumes that this html was created by the
 *  text application.
 */
function createOptimizedStructure(source, result, paragraphStylesMap, listStyles, isEncodeBase64) {

    var outNode, tbody,
        trow, td, cellContent, colgroup, styles,
        optimized = false, listLevel = -1, listStyle, listLevels = [];

    $(source).children().each(function () {
        var optNode = $(this);

        if (isParagraphNode(optNode)) {
            styles = paragraphStylesMap[optNode.attr('styleId')];
            if (styles && styles.paragraph.listStyleId && styles.paragraph.listStyleId.length && listStyles.getList(styles.paragraph.listStyleId)) {
                listStyle = listStyles.getList(styles.paragraph.listStyleId);
                listLevel = styles.paragraph.listLevel;
                listLevels = createListLevels(result, listLevels, listLevel, listStyle);
                outNode = $('<li>');
            } else {
                // reset list levels
                listLevel = -1;
                listLevels = [];
                outNode = $('<p>');
            }
            copyStyles(optNode, outNode, 'paragraph');
            optimizeParagraphForExport(optNode, outNode, isEncodeBase64);
            if (listLevel === -1) {
                result.append(outNode);
            } else {
                listLevels[listLevel].append(outNode);
            }
            optimized = true;
        } else if (isTableNode(optNode)) {
            optimized = true;
            // reset list levels
            listLevel = -1;
            listLevels = [];
            outNode = $('<table>');
            outNode.css('border-collapse', 'collapse');
            copyStyles(optNode, outNode, 'table');
            colgroup = optNode.find('colgroup');
            if (colgroup.lengh) {
                outNode.append(colgroup);
            }
            tbody = $('<tbody>');
            optNode.find('tr').each(function () {
                trow = $('<tr>');
                $(this).find('td').each(function () {
                    td = $('<td>');
                    td.attr('colspan', this.colSpan);
                    copyStyles(this, td, 'td');
                    cellContent = $(this).find('div.cellcontent');
                    if (cellContent) {
                        createOptimizedStructure(cellContent, td, paragraphStylesMap, listStyles, isEncodeBase64);
                    }
                    trow.append(td);
                });
                tbody.append(trow);
            });
            outNode.append(tbody);
            result.append(outNode);
        }
    });

    // if cannot optimize just append the source
    if (!optimized) {
        result.append(source);
    }

    restoreHyperlinks(result);
}

/**
 * Prepares the parentNode to receive list entries for a the specified
 * list level.
 *
 * @param {jQuery} parentNode
 *  The node which is the parent for all list entries.
 *
 * @param {Array} listLevels
 *  An array filled with jQuery nodes that can be used to add list entries
 *  for up to the specified list level.
 *
 * @param {Integer} upToLevel
 *  The max list level to be used.
 *
 * @param {Lists} listStyle
 *  The list style to be used for the upToLevel list level.
 *
 * @returns {Array}
 *  The new list level array updated to match the upToLevel&listStyle
 *  requirements.
 */
function createListLevels(parentNode, listLevels, upToLevel, listStyle) {
    var i, node;

    if (listLevels.length <= upToLevel) {
        // Extend the list level array and DOM
        for (i = listLevels.length; i <= upToLevel; i++) {
            node = createListElement(upToLevel, listStyle);
            if (i === 0) {
                parentNode.append(node);
            } else {
                listLevels[i - 1].append(node);
            }
            listLevels[i] = node;
        }
    } else {
        // Reduce the list to start a new sub-list
        listLevels = listLevels.slice(0, upToLevel + 1);
    }

    return listLevels;
}

/**
 * Creates a jQuery element to add to the DOM for the specified
 * list level.
 *
 * @param {Integer} level
 *  The list level for which the jQuery element should to be created.
 *
 * @param {List} listStyle
 *  The list style to be used for the list element.
 *
 * @returns {jQuery}
 *  The list element as jQuery.
 */
function createListElement(level, listStyle) {
    var format = listStyle.listLevels[level].numberFormat;
    var levelText = listStyle.listLevels[level].levelText;
    var startValue = listStyle.listLevels[level].listStartValue;
    var node = (format === 'bullet') ? $('<ul>') : $('<ol>');

    if (startValue > 1) {
        node.attr('start', startValue);
    }

    switch (format.toLowerCase()) {
        case 'decimal':
            // do nothing;
            break;
        case 'lowerletter':
            node.attr('type', 'a');
            break;
        case 'upperletter':
            node.attr('type', 'A');
            break;
        case 'lowerroman':
            node.attr('type', 'i');
            break;
        case 'upperroman':
            node.attr('type', 'I');
            break;
        case 'bullet':
            switch (levelText) {
                case '\uf0b7':
                case '\u25CF':  // filled-circle
                case '\u2022':  // small-filled-circle
                    node.css('list-style-type', 'disc');
                    break;
                case '\u25CB':  // circle
                case '\u25E6':  // small-circle
                    node.css('list-style-type', 'circle');
                    break;
                case '\u25A0':  // filled-square
                case '\u25AA':  // small-filled-square
                case '\u25A1':  // square
                case '\u25AB':  // small-square
                    node.css('list-style-type', 'square');
                    break;
                case ' ':
                    node.css('list-style-type', 'none');
                    break;
            }
            break;
    }

    return node;
}

/**
 * Copy specific styles dependent on the type from
 * one element to another one.
 *
 * @param {jQuery|Node} from
 *  The node that is used as source.
 *
 * @param {jQuery|Node} to
 *  The node that receives the styles.
 *
 * @param {String} type
 *  The type of node. Currently supported types are
 *  'paragraph', 'span', 'table', 'td', 'img'.
 */
function copyStyles(from, to, type) {
    var source = $(from), target = $(to);

    _.each(COPY_STYLES[type], function (style) {
        target.css(style, source.css(style));
    });
}

/**
 * Optimize a paragraph containing sub elements for html export.
 *
 * @param {jQuery} paragraph
 *  The paragraph node as jQuery.
 *
 * @param {jQuery} parentNode
 *  The node as jQuery which is the parent for the paragraph.
 */
function optimizeParagraphForExport(paragraph, parentNode, isEncodeBase64) {

    paragraph.children().each(function () {
        var node = $(this), tmpNode = null, img = null, valign = 0;

        if (isSpan(node)) {
            valign = node.css('vertical-align');
            // check for vertical-align which is used for sub/superscript
            if (valign && valign !== 'baseline') {
                valign = parseFloat(valign);
                tmpNode = $((valign < 0) ? '<sub>' : '<sup>');
                node.css('vertical-align', 'baseline');
                parentNode.append(tmpNode);
                tmpNode.append(node);
            } else {
                parentNode.append(node);
            }
        } else if (isImageNode(node)) {
            tmpNode = $('<img>');
            img = node.find('img');
            if (isEncodeBase64 && isDocumentImageNode(node)) {
                tmpNode.attr('src', getBase64FromImageNode(node, getMimeTypeFromImageUri(getUrlParamFromImageNode(node, 'get_filename'))));
            } else {
                tmpNode.attr('src', img.attr('src'));
            }
            // Word needs the size as attributes and cannot cope with with/height in styles!
            tmpNode.attr('width', img.css('width'));
            tmpNode.attr('height', img.css('height'));
            parentNode.append(tmpNode);
        } else if (isDrawingFrame(node)) {
            parentNode.append(node);
        } else if (isTabNode(node)) {
            tmpNode = $('<span>');
            copyStyles(node, tmpNode, 'span');
            if (node[0].textContent) {
                tmpNode.text(node[0].textContent);
            } else {
                ensureExistingTextNode(tmpNode);
            }
            parentNode.append(tmpNode);
        } else if (node.is('br')) {
            parentNode.append(node);
        } else if (isHardBreakNode(node)) {
            parentNode.append($('<br>'));
        }
    });
}

/**
 * Create paragraph style map and unique ids for paragraph nodes.
 *
 * @param {Selection} selection
 *  Current selection.
 *
 * @param {ParagraphStyles} paragraphStyles
 *  The document paragraph styles.
 *
 * @returns {Object}
 *  The paragraph styles accessible via ids.
 */
function createParagraphStyleIds(selection, paragraphStyles) {
    var result,
        paras, len, i,
        id = 10000,
        map = {};

    // visit the paragraphs and tables covered by the text selection
    result = selection.iterateContentNodes(function (contentNode) {

        // mark paragraphs with unique id and store attributes in map
        if (isParagraphNode(contentNode)) {
            map[id] = paragraphStyles.getElementAttributes(contentNode);
            $(contentNode).attr('styleId', id);
            id++;
        // entire table: find all containing paragraph, mark them and store attributes in map
        } else if (isTableNode(contentNode)) {
            paras = $(contentNode).find('div.p');
            len = paras.length;
            for (i = 0; i < len; i++) {
                map[id] = paragraphStyles.getElementAttributes(paras[i]);
                $(paras[i]).attr('styleId', id);
                id++;
            }

        } else {
            return BREAK;
        }

    }, this, { shortestPath: true });

    if (result === BREAK) {
        map = {};
    }

    return map;
}

/**
 * Clear paragraph style ids.
 *
 * @param {Selection} selection
 *  Current selection.
 */
function clearParagraphStyleIds(selection) {

    // visit the paragraphs and tables covered by the text selection
    selection.iterateContentNodes(function (contentNode) {

        // paragraphs may be covered partly
        if (isParagraphNode(contentNode)) {
            $(contentNode).removeAttr('styleId');
        // entire table: generate complete operations array for the table
        } else if (isTableNode(contentNode)) {
            $(contentNode).find('div.p').removeAttr('styleId');
        } else {
            return BREAK;
        }

    }, this, { shortestPath: true });
}

// exports ================================================================

export default Export;
