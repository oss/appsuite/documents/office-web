/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { updateFormatting } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { getFooterWrapperNode, getHeaderWrapperNode, getPageContentNode, getTextDrawingLayerNode,
    isFooterWrapper, isHeaderWrapper } from '@/io.ox/office/textframework/utils/dom';
import { textLogger } from '@/io.ox/office/textframework/utils/textutils';

// class SnapShot =====================================================

/**
 * An instance of this class represents the snapshot handler class that
 * can be used to generate and apply snapshot of the current document.
 *
 * @param {TextApplication} app
 *  The application instance.
 */
export default class Snapshot extends ModelObject {

    // the page node of the text document
    #pageNode;
    // the current operation state number
    #osn = 0;
    // some application specific data in the model
    #modelData;
    // the collector for list style assigned to slides and drawings
    #slideListStyles;
    #drawingListStyles;
    // the nodes for page content and drawing layer
    #contentNode;
    #drawingLayer;
    // the nodes for header wrapper and footer wrapper
    #headerWrapper;
    #footerWrapper;
    // the nodes for master slide layer and layout slide layer
    #masterSlideLayer;
    #layoutSlideLayer;
    // active slide id
    #activeSlideId;
    // the current root node (in which content is deleted, inserted, or gets new attributes)
    #currentRootNode;
    // the comment collection in OX Text
    #clonedCommentCollection;
    // the current comment display mode
    #commentDisplayMode;
    // Spreadsheet app: The text frame node inside the currently active drawing node
    #textFrameNode;
    // an array, that contains objects with the keys 'startPosition' and 'endPosition' for the logical position(s).
    #allPositions;

    constructor(docModel) {

        // base constructor
        super(docModel);

        this.#pageNode = this.docModel.getNode();
        this.#currentRootNode = this.docModel.getCurrentRootNode();

        // initialization -----------------------------------------------------

        this._createSnapshot();
    }

    // private methods ----------------------------------------------------

    /**
     * Helper function that repaints all canvas elements after the cloned drawing node
     * was inserted into the DOM.
     * For performance reasons only those drawings are repainted, that have explicitely
     * set a line or a fill attribute.
     */
    #redrawAllCanvasDrawings() {

        _.each(this.#pageNode.find('.drawing > .content > canvas'), canvasNode => {

            // one drawing node, must be 'jQuerified'
            const drawing = $(canvasNode.parentNode.parentNode);
            // the explicit attributes at the drawing
            const explicitAttrs = getExplicitAttributeSet(drawing);

            if (explicitAttrs && ((explicitAttrs.line && explicitAttrs.line.type !== 'none') || (explicitAttrs.fill && explicitAttrs.fill.type !== 'none'))) {
                // updating only those drawings, that have expicitely set line attributes or fill attributes
                updateFormatting(this.docApp, drawing, this.docModel.drawingStyles.getElementAttributes(drawing));
            }
        });

    }

    /**
     * Creating a snapshot of the current document state.
     */
    @textLogger.profileMethod("Snapshot._createSnapshot()")
    _createSnapshot() { // TODO: Rename to really private "#createSnapShot" when decorators are natively supported

        // the drawing layer node for absolute positioned drawings
        const drawingLayerNode = getTextDrawingLayerNode(this.#pageNode);
        // the header wrapper node
        let headerWrapperNode;
        // the footer wrapper node
        let footerWrapperNode;
        // the master slide layer node
        let masterSlideLayerNode;
        // the layout slide layer node
        let layoutSlideLayerNode;
        // the comment collection instance
        let commentCollection;

        // saving the operation state number
        this.#osn = this.docModel.getOperationStateNumber();

        // all external actions must be put into the queue after creating the snapshot
        this.docModel.setInternalOperationBlocker(true);

        if (this.docApp.isPresentationApp()) { // OX Presentation specific nodes

            // adding all slides into the page, before creating snapshot
            this.docModel.appendAllSlidesToDom();

            // saving the current content node
            this.#contentNode = getPageContentNode(this.#pageNode).clone(true, true);

            this.#activeSlideId = this.docModel.getActiveSlideId();

            // getting the layer nodes for master and layout slides
            masterSlideLayerNode = this.docModel.getMasterSlideLayerNode();
            layoutSlideLayerNode = this.docModel.getLayoutSlideLayerNode();

            if (masterSlideLayerNode && masterSlideLayerNode.length > 0) { this.#masterSlideLayer = masterSlideLayerNode.clone(true, true); }
            if (layoutSlideLayerNode && layoutSlideLayerNode.length > 0) { this.#layoutSlideLayer = layoutSlideLayerNode.clone(true, true); }

            // saving the list styles, that are assigned to slides and drawings. These list styles cannot be restored
            // from the slides data-object, because the list styles are not stored there.
            this.#slideListStyles = this.docModel.getAllSlideListStyleAttributes();
            this.#drawingListStyles = this.docModel.drawingStyles.getAllDrawingListStyleAttributes();

            // saving the values for 'mainSlideCounter', 'activeSlideId' and 'isMasterView'
            this.#modelData = this.docModel.getModelDataObject();

            // saving the logical position(s) to restore drawing selections
            this.#allPositions = this.docModel.getSelection().getAllLogicalPositions();

        } else if (this.docApp.isTextApp()) { // OX Text specific nodes

            // saving the current content node
            this.#contentNode = getPageContentNode(this.#pageNode).clone(true, true);

            // saving a clone of the collection of comment models, if it is not empty
            commentCollection = this.docModel.getCommentCollection();
            if (!commentCollection.isEmpty()) { this.#clonedCommentCollection = commentCollection.clone(); }
            // saving the comment display mode
            this.#commentDisplayMode = this.docModel.getCommentLayer().getDisplayMode();

            // saving the drawing layer node
            if (drawingLayerNode.length > 0) { this.#drawingLayer = drawingLayerNode.clone(true, true); }

            // evaluating the current root node -> check if the first header or last footer need to be exchanged)
            // -> long running stoppable actions in first header or last footer should never occur
            if (isHeaderWrapper(this.#currentRootNode.parent())) {
                // receiving the header wrapper
                headerWrapperNode = getHeaderWrapperNode(this.#pageNode);
                // saving the header wrapper node
                if (headerWrapperNode.length > 0) { this.#headerWrapper = headerWrapperNode.clone(true, true); }
            }

            if (isFooterWrapper(this.#currentRootNode.parent())) {
                // receiving the footer wrapper
                footerWrapperNode = getFooterWrapperNode(this.#pageNode);
                // saving the header wrapper node
                if (footerWrapperNode.length > 0) { this.#footerWrapper = footerWrapperNode.clone(true, true); }
            }
        } else if (this.docApp.isSpreadsheetApp()) {
            // saving the text frame inside the currently active drawing node
            this.#textFrameNode = this.#pageNode.find('.drawing.edit-active > .content > .textframe').clone(true, true);
        }

    }

    // methods ------------------------------------------------------------

    /**
     * Applying the snapshot.
     */
    @textLogger.profileMethod("Snapshot.applySnapshot()")
    apply() {

        // the page content node
        let currentContentNode;

        if (this.docApp.isSpreadsheetApp()) {
            // replacing the text frame inside the active drawing node is sufficient in Spreadsheet app
            currentContentNode = this.#pageNode.find('.drawing.edit-active > .content > .textframe');
            currentContentNode.replaceWith(this.#textFrameNode);
            // setting the OSN
            this.docModel.setOperationStateNumber(this.#osn);
            return; // fast exit for Spreadsheet app
        }

        // replacing the main content node in
        currentContentNode = getPageContentNode(this.#pageNode);
        currentContentNode.replaceWith(this.#contentNode);

        // restoring header and footer wrapper as children of the page (should never be necessary)
        if (this.#headerWrapper && getHeaderWrapperNode(this.#pageNode).length > 0) { getHeaderWrapperNode(this.#pageNode).replaceWith(this.#headerWrapper); }
        if (this.#footerWrapper && getFooterWrapperNode(this.#pageNode).length > 0) { getFooterWrapperNode(this.#pageNode).replaceWith(this.#footerWrapper); }

        // restoring the comment collection model
        if (this.#clonedCommentCollection && this.docApp.isTextApp()) { this.docModel.getCommentCollection().applyCommentCollection(this.#clonedCommentCollection); }
        // restoring the drawing layer node
        if (this.#drawingLayer) { this.docModel.getDrawingLayer().getDrawingLayerNode().replaceWith(this.#drawingLayer); }

        // but removing comment layer and drawing layer if they were created by a paste operation
        if (!this.#drawingLayer && getTextDrawingLayerNode(this.#pageNode).length > 0) { getTextDrawingLayerNode(this.#pageNode).remove(); }

        // restoring the master slide layer and the layout slide layer
        if (this.#masterSlideLayer) { this.docModel.getMasterSlideLayerNode().replaceWith(this.#masterSlideLayer); }
        if (this.#layoutSlideLayer) { this.docModel.getLayoutSlideLayerNode().replaceWith(this.#layoutSlideLayer); }

        // after all nodes are exchanged, it is necessary to update all models and all cached nodes

        // updating cached page content node in page layout object (updating all cached content nodes)
        this.docModel.getPageLayout().refreshPageContentNode();

        // if snapshot is applied while inside active header/footer, references to marginal nodes need to be refreshed, #52113
        if (this.docModel.isHeaderFooterEditState()) {  this.docModel.getPageLayout().refreshActiveMarginalNode(); }

        // triggering that the model will be recreated -> the models need to be rebuilt from scratch
        this.docModel.trigger('document:reloaded', this);

        // setting the OSN
        this.docModel.setOperationStateNumber(this.#osn);

        // setting the complete object of the slide list styles
        if (this.#slideListStyles) { this.docModel.setAllSlideListStyleAttributes(this.#slideListStyles); }

        // setting the object for all drawing list styles
        if (this.#drawingListStyles) { this.docModel.drawingStyles.setAllDrawingListStyleAttributes(this.#drawingListStyles); }

        // setting application specific data into the model
        if (this.#modelData) { this.docModel.setModelDataObject(this.#modelData); }

        // if active slide id is stored, set it again
        if (this.#activeSlideId) { this.docModel.setActiveSlideId(this.#activeSlideId, { forceUpdate: true }); }

        // triggering to listeners that all models are completely recreated
        this.docModel.trigger('document:reloaded:after', this);

        // all canvas elements need to be repainted
        this.#redrawAllCanvasDrawings();

        // external actions must no longer be queued
        this.docModel.setInternalOperationBlocker(false);
    }

    /**
     * Returns the comment display mode that was active, when the snapshot was created.
     *
     * @returns {String}
     *  The comment display mode that was active, when the snapshot was created.
     */
    getCommentDisplayMode() {
        return this.#commentDisplayMode;
    }

    /**
     * Returns the stored allPositions object. This is an array, that contains objects
     * with the keys 'startPosition' and 'endPosition'. If the logical positions are not
     * saved, null is returned.
     *
     * @returns {Object[]|Null}
     */
    getStoredPositions() {
        return this.#allPositions;
    }
}
