/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { is, math, str } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE, containsNode, createSpan, createCanvas, createIcon, parseCssLength, KeyCode, isSymbolFont, getSymbolReplacement } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { compareNodes, getArrayOption, getBooleanOption, getDomNode, findDescendantNode,
    findPreviousSiblingNode, iterateSelectedDescendantNodes } from '@/io.ox/office/tk/utils';
import * as DrawingFrame from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

// constants ==============================================================

// key codes of keys that change the text cursor position backwards
const ARROW_CURSOR_KEYS = _([KeyCode.UP_ARROW, KeyCode.RIGHT_ARROW, KeyCode.DOWN_ARROW, KeyCode.LEFT_ARROW]);

// key codes of keys that change the text cursor position backwards
const BACKWARD_CURSOR_KEYS = _([KeyCode.PAGE_UP, KeyCode.HOME, KeyCode.LEFT_ARROW, KeyCode.UP_ARROW]);

// key codes of keys that change the text cursor position forwards
const FORWARD_CURSOR_KEYS = _([KeyCode.PAGE_DOWN, KeyCode.END, KeyCode.RIGHT_ARROW, KeyCode.DOWN_ARROW]);

// key codes of keys that will be passed directly to the browser
const IGNORABLE_KEYS = _([
    KeyCode.SHIFT, KeyCode.CONTROL, KeyCode.ALT,
    KeyCode.CAPS_LOCK, KeyCode.NUM_LOCK, KeyCode.SCROLL_LOCK,
    KeyCode.BREAK, KeyCode.PRINT, KeyCode.SELECT,
    KeyCode.LEFT_WINDOWS, KeyCode.RIGHT_WINDOWS,
    KeyCode.F5,
    KeyCode.MOZ_COMMAND
]);

/**
 * Provides classes representing DOM points (Point) and ranges
 * (Range), and static helper methods for basic editor DOM
 * manipulation, and access the browser selection.
 */

// class Point ================================================================

/**
 * A DOM text point contains a 'node' attribute referring to a DOM node,
 * and an 'offset' attribute containing an integer offset specifying the
 * position in the contents of the node.
 *
 * @param {Node|jQuery} node
 *  The DOM node selected by this DOM.Point instance. If this object is a
 *  jQuery collection, uses the first DOM node it contains.
 *
 * @param {Number} [offset]
 *  An integer offset relative to the DOM node specifying the position in
 *  the node's contents. If the node is a text node, the offset represents
 *  the character position in the node text. If the node is an element
 *  node, the offset specifies the index of a child node of this node. The
 *  value of the offset may be equal to the text length respectively the
 *  number of child nodes, in this case the DOM point refers to the
 *  position directly after the node's contents. If omitted, this DOM.Point
 *  instance refers to the start of the entire node, instead of specific
 *  contents.
 */
export class Point {

    // the Node element of the point
    node;
    // the offset inside the node
    offset;

    constructor(node, offset) {
        this.node = getDomNode(node);
        this.offset = offset;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a new clone of this DOM point.
     */
    clone() {
        return new Point(this.node, this.offset);
    }

    /**
     * Validates the offset of this DOM point. Restricts the offset to the
     * available index range according to the node's contents, or
     * initializes the offset, if it is missing.
     *
     * If this instance points to a text node, the offset will be
     * restricted to the text in the node, or set to zero if missing.
     *
     * If this instance points to an element node, the offset will be
     * restricted to the number of child nodes in the node. If the offset
     * is missing, it will be set to the index of the node in its siblings,
     * and the node will be replaced by its parent node.
     *
     * @returns {DOM.Point}
     *  A reference to this instance.
     */
    validate() {

        // element: if offset is missing, take own index and refer to the parent node
        if (this.node.nodeType === 1) {
            if (is.number(this.offset)) {
                this.offset = math.clamp(this.offset, 0, this.node.childNodes.length);
            } else {
                this.offset = $(this.node).index();
                this.node = this.node.parentNode;
            }

        // text node: if offset is missing, use zero
        } else if (this.node.nodeType === 3) {
            if (is.number(this.offset)) {
                this.offset = math.clamp(this.offset, 0, this.node.nodeValue.length);
            } else {
                this.offset = 0;
            }
        }

        return this;
    }

    /**
     * Converts this DOM point to a human readable string representation.
     */
    toString() {

        // generates a readable description of the passed node and offset
        function getNodeName(node, offset) {

            // full string representation of this DOM Point
            let result = node.nodeName.toLowerCase();

            if ((node.nodeType === 1) && (node.className.length > 0)) {
                // add class names of an element
                result += '.' + node.className.replace(/ /g, '.');
            } else if (node.nodeType === 3) {
                // add some text of a text node
                result += '"' + node.nodeValue.substr(0, 10) + ((node.nodeValue.length > 10) ? str.ELLIPSIS_CHAR : '') + '"';
            }

            if (is.number(offset)) {
                result += ':' + offset;
            }

            return result;
        }

        if (this.node && this.node.parentNode) {
            return getNodeName(this.node.parentNode, $(this.node).index()) + '>' + getNodeName(this.node, this.offset);
        } else {
            return 'undefined node';
        }
    }

    // static methods ---------------------------------------------------------

    /**
     * Creates and returns a valid DOM.Point instance for the passed DOM node.
     * If the passed node is a text node, the DOM point will refer to its first
     * character, otherwise the DOM point will contain the parent node and the
     * child index of the passed node as offset.
     *
     * @param {Node|jQuery} node
     *  The DOM node selected by the created DOM.Point instance. If this object
     *  is a jQuery collection, uses the first DOM node it contains.
     *
     * @returns {Point}
     *  A new DOM.Point instance referring to the passed node.
     */
    static createPointForNode(node) {
        return new Point(node).validate();
    }

    /**
     * Returns whether the two passed DOM points are equal.
     *
     * @param {DOM.Point} point1
     *  The first DOM point. Must be valid (see DOM.Point.validate() method for
     *  details).
     *
     * @param {DOM.Point} point2
     *  The second DOM point. Must be valid (see DOM.Point.validate() method
     *  for details).
     *
     * @returns {Boolean}
     *  Whether the DOM points are equal.
     */
    static equalPoints(point1, point2) {
        return (point1.node === point2.node) && (point1.offset === point2.offset);
    }

    /**
     * Returns an integer indicating how the two DOM points are located to each
     * other.
     *
     * @param {DOM.Point} point1
     *  The first DOM point. Must be valid (see DOM.Point.validate() method for
     *  details).
     *
     * @param {DOM.Point} point2
     *  The second DOM point. Must be valid (see DOM.Point.validate() method
     *  for details).
     *
     * @returns {Number}
     *  The value zero, if the DOM points are equal, a negative number, if
     *  point1 precedes point2, or a positive number, if point1 follows point2.
     */
    static comparePoints(point1, point2) {

        // Returns the index of the inner node's ancestor in the outer node's
        // children list. 'outerNode' MUST contain 'innerNode'.
        function calculateOffsetInOuterNode(outerNode, innerNode) {
            while (innerNode.parentNode !== outerNode) {
                innerNode = innerNode.parentNode;
            }
            return $(innerNode).index();
        }

        // equal nodes: compare by offset
        if (point1.node === point2.node) {
            return point1.offset - point2.offset;
        }

        // Node in point1 contains the node in point2: point1 is before point2,
        // if offset of point1 (index of its child node) is less than or equal
        // to the offset of point2's ancestor node in the children of point1's
        // node. If offsets are equal, point2 is a descendant of the child node
        // pointed to by point1 and therefore located after point1.
        if (containsNode(point1.node, point2.node)) {
            return (point1.offset <= calculateOffsetInOuterNode(point1.node, point2.node)) ? -1 : 1;
        }

        // Node in point2 contains the node in point1: see above, reversed.
        if (containsNode(point2.node, point1.node)) {
            return (calculateOffsetInOuterNode(point2.node, point1.node) < point2.offset) ? -1 : 1;
        }

        // Neither node contains the other: compare nodes regardless of offset.
        return compareNodes(point1.node, point2.node);
    }

}

// class Range ================================================================

/**
 * A DOM text range represents a half-open range in the DOM tree. It
 * contains 'start' and 'end' attributes referring to DOM point objects.
 *
 * @param {DOM.Point} start
 *  The DOM point where the range starts.
 *
 * @param {DOM.Point} [end]
 *  The DOM point where the range ends. If omitted, uses the start position
 *  to construct a collapsed range (a simple 'cursor').
 */
export class Range {

    // the DOM point where the range starts.
    start;
    // the DOM point where the range ends. If omitted, uses the start position.
    end;

    constructor(start, end) {
        this.start = start;
        this.end = end ?? start.clone();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a new clone of this DOM range.
     */
    clone() {
        return new Range(this.start.clone(), this.end.clone());
    }

    /**
     * Validates the start and end position of this DOM range. See method
     * DOM.Point.validate() for details.
     */
    validate() {
        this.start.validate();
        this.end.validate();
        return this;
    }

    /**
     * Swaps start and end position, if the start position is located after
     * the end position in the DOM tree.
     */
    adjust() {
        if (Point.comparePoints(this.start, this.end) > 0) {
            const tmp = this.start;
            this.start = this.end;
            this.end = tmp;
        }
        return this;
    }

    /**
     * Returns whether the DOM range is collapsed, i.e. start position and
     * end position are equal.
     *
     * @returns {Boolean}
     *  Whether this DOM range is collapsed.
     */
    isCollapsed() {
        return Point.equalPoints(this.start, this.end);
    }

    /**
     * Converts this DOM range to a human readable string representation.
     */
    toString() {
        return '[start=' + this.start + ', end=' + this.end + ']';
    }

    // static methods ---------------------------------------------------------

    /**
     * Returns whether the two passed DOM ranges are equal.
     *
     * @param {DOM.Range} range1
     *  The first DOM range. Must be valid (see DOM.Range.validate() method for
     *  details).
     *
     * @param {DOM.Range} range2
     *  The second DOM range. Must be valid (see DOM.Range.validate() method
     *  for details).
     *
     * @returns {Boolean}
     *  Whether the DOM ranges are equal.
     */
    static equalRanges(range1, range2) {
        return (Point.equalPoints(range1.start, range2.start) && Point.equalPoints(range1.end, range2.end));
    }

    /**
     * Returns whether the two passed DOM ranges have the same offsets (the nodes might be different).
     * This can be used for column selections, where the row might be different, but the offset are
     * constant inside a column.
     *
     * @param {DOM.Range} range1
     *  The first DOM range. Must be valid (see DOM.Range.validate() method for
     *  details).
     *
     * @param {DOM.Range} range2
     *  The second DOM range. Must be valid (see DOM.Range.validate() method
     *  for details).
     *
     * @returns {Boolean}
     *  Whether the DOM ranges have the same offsets.
     */
    static equalRangeOffsets(range1, range2) {
        return (range1.start.offset === range2.start.offset) && (range1.end.offset === range2.end.offset);
    }

    /**
     * Creates a new DOM.Range instance from the passed nodes and offsets.
     *
     * @param {Node|jQuery} startNode
     *  The DOM node used for the start point of the created range. If this
     *  object is a jQuery collection, uses the first DOM node it contains.
     *
     * @param {Number} [startOffset]
     *  The offset for the start point of the created range.
     *
     * @param {Node|jQuery} [endNode]
     *  The DOM node used for the end point of the created range. If this
     *  object is a jQuery collection, uses the first DOM node it contains. If
     *  omitted, creates a collapsed range by cloning the start position.
     *
     * @param {Number} [endOffset]
     *  The offset for the end point of the created range. Not used, if endNode
     *  has been omitted.
     *
     * @returns {DOM.Range}
     *  The new DOM range object.
     */
    static createRange(startNode, startOffset, endNode, endOffset) {
        return new Range(new Point(startNode, startOffset), is.object(endNode) ? new Point(endNode, endOffset) : undefined);
    }

    /**
     * Creates and returns a valid DOM.Range instance for the passed DOM node.
     * If the passed node is a text node, the DOM range will select its entire
     * text, otherwise the DOM range will contain the parent node and the
     * child index of the passed node as start offset, and the next child index
     * as end offset, effectively selecting the entire node.
     *
     * @param {Node|jQuery} node
     *  The DOM node selected by the created DOM.Range instance. If this object
     *  is a jQuery collection, uses the first DOM node it contains.
     *
     * @returns {DOM.Range}
     *  A new DOM.Range instance referring to the passed node.
     */
    static createRangeForNode(node) {
        const range = new Range(Point.createPointForNode(node));
        if (range.end.node.nodeType === 1) {
            range.end.offset += 1;
        } else if (range.end.node.nodeType === 3) {
            range.end.offset = range.end.node.nodeValue.length;
        }
        return range;
    }
}

// key codes ==============================================================

/**
 * Returns whether the passed key code is a cursor navigation key that
 * moves the cursor backwards in the document.
 *
 * @param {Number} keyCode
 *  The key code from a 'keydown' browser event.
 */
export function isBackwardCursorKey(keyCode) {
    return BACKWARD_CURSOR_KEYS.contains(keyCode);
}

/**
 * Returns whether the passed key code is a cursor navigation key that
 * moves the cursor forwards in the document.
 *
 * @param {Number} keyCode
 *  The key code from a 'keydown' browser event.
 */
export function isForwardCursorKey(keyCode) {
    return FORWARD_CURSOR_KEYS.contains(keyCode);
}

/**
 * Returns whether the passed key code is a cursor navigation key.
 *
 * @param {Number} keyCode
 *  The key code from a 'keydown' browser event.
 */
export function isCursorKey(keyCode) {
    return isBackwardCursorKey(keyCode) || isForwardCursorKey(keyCode);
}

/**
 * Returns whether the passed key code is an 'arrow' cursor navigation key.
 *
 * @param {Number} keyCode
 *  The key code from a 'keydown' browser event.
 */
export function isArrowCursorKey(keyCode) {
    return ARROW_CURSOR_KEYS.contains(keyCode);
}

/**
 * Returns whether the passed key code has to be ignored silently.
 *
 * @param {Number} keyCode
 *  The key code from a 'keydown' browser event.
 */
export function isIgnorableKey(keyCode) {
    return IGNORABLE_KEYS.contains(keyCode);
}

// pages ==================================================================

export function handleReadOnly(node) {
    node.attr('contenteditable', !_.browser.IE);
}

/**
 * A jQuery selector that matches elements representing a page.
 */
export const PAGE_NODE_SELECTOR = 'div.page';

/**
 * Creates a new page element.
 *
 * @returns {jQuery}
 *  A page element, as jQuery object.
 */
export function createPageNode() {
    return $('<div class="page formatted-content"><div class="pagecontent"></div></div>');
}

/**
 * Returns whether the passed node is a page element.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a page element.
 */
export function isPageNode(node) {
    return $(node).is(PAGE_NODE_SELECTOR);
}

/**
 * The CSS class used to mark the application content root node.
 */
export const APP_CONTENT_ROOT = 'app-content-root';

/**
 * A jQuery selector that matches nodes representing the application content root.
 */
export const APP_CONTENT_ROOT_SELECTOR = '.' + APP_CONTENT_ROOT;

/**
 * The class name for the page node that is responsible for an increased z-index
 * at all paragraphs and table nodes.
 */
export const INCREASED_CONTENT_CLASS = 'increased-content';

/**
 * The class name for the page content node.
 */
export const PAGECONTENT_NODE_CLASSNAME = 'pagecontent';

/**
 * A jQuery selector that matches elements representing a page content node.
 */
export const PAGECONTENT_NODE_SELECTOR = 'div.pagecontent';

/**
 * The class name for the page content node.
 */
export const POPUP_CONTENT_NODE_CLASSNAME = 'popup-content';

/**
 * A jQuery selector that matches elements representing a page content node.
 */
export const POPUP_CONTENT_NODE_SELECTOR = '.' + POPUP_CONTENT_NODE_CLASSNAME;

/**
 * The class name for the comment layer node.
 */
export const COMMENTLAYER_CLASS = 'commentlayer';

/**
 * The class name for the comment bubble layer node.
 */
export const COMMENTBUBBLELAYER_CLASS = 'commentbubblelayer';

/**
 * A jQuery selector that matches elements representing a comment layer node.
 */
export const COMMENTLAYER_NODE_SELECTOR = 'div.' + COMMENTLAYER_CLASS;

/**
 * A jQuery selector that matches elements representing a comment bubble layer node.
 */
export const COMMENTBUBBLELAYER_NODE_SELECTOR = 'div.' + COMMENTBUBBLELAYER_CLASS;

/**
 * The class name for marking paragraphs that must not be spellchecked, for
 * example template texts in empty text frames.
 */
export const NO_SPELLCHECK_CLASSNAME = 'nospellcheck';

/**
 * The attribute name for the image source property that is saved at the drawing
 * node during fast load or in local storage usage.
 */
export const IMAGESOURCE_ATTR = '__imagesource__';

/**
 * The class name for hiding nodes directly after attaching the html string
 * (fast load or local storage) to the DOM (performance).
 */
export const HIDDENAFTERATTACH_CLASSNAME = 'hiddenafterattach';

/**
 * A jQuery selector for hidden nodes directly after attaching the html string
 * (fast load or local storage) to the DOM (performance).
 */
export const HIDDENAFTERATTACH_SELECTOR = '.' + HIDDENAFTERATTACH_CLASSNAME;

/**
 * The class name for marking the a11y alert node.
 */
export const A11Y_ALERT_CLASSNAME = 'io-ox-office-a11y-alert';

/**
 * A jQuery selector for the a11y alert node.
 */
export const A11Y_ALERT_SELECTOR = '.' + A11Y_ALERT_CLASSNAME;

/**
 * Returns whether the passed node is a page content node that contains all
 * top-level content nodes (paragraphs and tables).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a page content node.
 */
export function isPageContentNode(node) {
    return $(node).hasClass(PAGECONTENT_NODE_CLASSNAME);
}

/**
 * Returns the container node of a page element that contains all top-level
 * content nodes (paragraphs and tables).
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  The container DOM node from the passed page that contains all top-level
 *  content nodes (paragraphs and tables).
 */
export function getPageContentNode(pageNode) {
    return $(pageNode).children(PAGECONTENT_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is the application content root node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is the application content root node.
 */
export function isAppContentRootNode(node) {
    return $(node).is(APP_CONTENT_ROOT_SELECTOR);
}

/**
 * Returns collection of first header wrapper, pagecontent node, and last footer wrapper nodes.
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  Collection of first header wrapper, pagecontent node, and last footer wrapper nodes.
 */
export function getContentChildrenOfPage(pageNode) {
    return $(pageNode).children(PAGE_CHILDREN_CONTENT_SELECTOR);
}

/**
 * Checking, if a specified node is a direct child of the page content node.
 *
 * @param {HTMLElement|jQuery} node
 *  The node that needs to be checked.
 *
 * @returns {Boolean}
 *  Whether a specified node is a direct child of the page content node.
 */
export function isChildOfPageContentNode(node) {
    return isPageContentNode($(node).parent());
}

/**
 * Checking, if a specified node is a direct child of a marginal content node.
 *
 * @param {HTMLElement|jQuery} node
 *  The node that needs to be checked.
 *
 * @returns {Boolean}
 *  Whether a specified node is a direct child of a marginal content node.
 */
export function isChildOfMarginalContentNode(node) {
    return isMarginalContentNode($(node).parent());
}

/**
 * Returns, whether the passed node has the marker for being hidden
 * after loading the document (performance).
 *
 * @returns {Boolean}
 *  Whether the passed node has the marker for being hidden after
 *  loading the document (performance).
 */
export function isHiddenNodeAfterAttach(node) {
    return $(node).is(HIDDENAFTERATTACH_SELECTOR);
}

/**
 * Returns, whether the passed node is the a11y alert node.
 *
 * @returns {Boolean}
 *  Whether the passed node is the a11y alert node.
 */
export function isA11YAlertNode(node) {
    return $(node).is(A11Y_ALERT_SELECTOR);
}

/**
 * Returns whether the passed node is a comment layer node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a comment layer node.
 */
export function isCommentLayerNode(node) {
    return $(node).hasClass(COMMENTLAYER_CLASS);
}

/**
 * Returns whether the passed node is inside the comment layer node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is inside the comment layer node.
 */
export function isInsideCommentLayerNode(node) {
    return $(node).closest(COMMENTLAYER_NODE_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is inside a table cell node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is inside the comment layer node.
 */
export function isInsideTableCellNode(node) {
    return $(node).closest('td').length > 0;
}

/**
 * Returns whether the passed node is a popup-content node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the specified node is a popup-content node.
 */
export function isPopupContentNode(node) {
    return $(node).is(POPUP_CONTENT_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a text frame in a comment node with
 * contenteditable set to true (in OX Text).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the specified node is a popup-content node.
 */
export function isCommentTextframeNode(node) {
    return $(node).is('.comment-text-frame');
}

/**
 * The class name representing an application content node.
 */
export const APPCONTENT_NODE_CLASS = 'app-content';

/**
 * A jQuery selector that matches elements representing an application content node.
 */
export const APPCONTENT_NODE_SELECTOR = 'div.app-content';

/**
 * Returns whether the passed node is an application content node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an application content node.
 */
export function isAppContentNode(node) {
    return $(node).hasClass(APPCONTENT_NODE_CLASS);
}

/**
 * A jQuery selector that matches elements representing a page break node.
 */
export const PAGEBREAK_NODE_SELECTOR = 'div.page-break';

/**
 * Returns whether the passed node contains at least one page break node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains a page break node.
 */
export function hasPageBreakNode(node) {
    return $(node).has(PAGEBREAK_NODE_SELECTOR).length > 0;
}

// paragraphs and tables ==================================================

/**
 * The class name representing a paragraph.
 */
export const PARAGRAPH_NODE_CLASS_NAME = 'p';

/**
 * A jQuery selector that matches elements representing a paragraph.
 */
export const PARAGRAPH_NODE_SELECTOR = 'div.p';

/**
 * A class assigned to an empty paragraph in a list (presentation only).
 */
export const PARAGRAPH_NODE_LIST_EMPTY_CLASS = 'emptylistparagraph';

/**
 * A class assigned to an empty paragraph in a list, that contains the selection (presentation only).
 */
export const PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS = 'emptyselectedlistparagraph';

/**
 * A class assigned to first paragraphs in lists (text only).
 */
export const PARAGRAPH_NODE_IS_FIRST_IN_LIST = 'isfirstinlist';

/**
 * A class assigned to list paragraphs (text only).
 */
export const LIST_PARAGRAPH_MARKER = 'listparagraph';

/**
 * The name of the attribute, in which the list style ID is saved.
 */
export const LIST_PARAGRAPH_ATTRIBUTE = 'data-list-id';

/**
 * A jQuery selector that matches elements representing a border for lists.
 */
export const LIST_BORDER_NODE_SELECTOR = '.page, .drawing, .header, .footer';

/**
 * Returns whether the passed node is a paragraph element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph element.
 */
export function isParagraphNode(node) {
    return $(node).hasClass(PARAGRAPH_NODE_CLASS_NAME);
}

/**
 * Returns whether the passed node is part of a paragraph element.
 * Can be text span, text span container node, drawing frame, comment placeholder, range marker, complex field, hard break node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a part of paragraph element.
 */
export function isPartOfParagraph(node) {
    return isTextSpan(node) || isTextSpanContainerNode(node) || isHardBreakNode(node) || isComplexFieldNode(node) || DrawingFrame.isDrawingFrame(node) || isCommentPlaceHolderNode(node) || isRangeMarkerNode(node) || isBookmarkNode(node);
}

/**
 * Returns whether the passed node a list paragraph element. For this it is checked, if the first
 * element inside the paragraph is a list label node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a list paragraph element.
 */
export function isListParagraphNode(node) {
    return isParagraphNode(node) && $(node).children(LIST_LABEL_NODE_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is a list paragraph element that is the first paragraph
 * in its list and in its level. For every list level, one paragraph has the marker class.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a list paragraph element, that is the first paragraph in its
 *  list.
 */
export function isFirstParagraphInList(node) {
    return isListParagraphNode(node) && $(node).hasClass(PARAGRAPH_NODE_IS_FIRST_IN_LIST);
}

/**
 * Returns whether the passed node is an empty paragraph element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an empty paragraph element. It is empty, if
 *  it only contains an empty text span and a 'br' element.
 */
export function isEmptyParagraph(node) {
    const allChildren = $(node).children();
    return (allChildren.length === 2 && isEmptySpan(allChildren.first()) && allChildren.last().is('br'));
}

/**
 * Returns whether the passed node is an empty paragraph with list label node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is an empty list paragraph element.
 */
export function isEmptyListParagraph(node) {

    // a collector for the text span children inside the paragraph
    let allTextSpans = null;

    if (isListParagraphNode(node)) {
        allTextSpans = $(node).children('span');
        return allTextSpans.length === 1 && allTextSpans.text() === '';
    }

    return false;
}

/**
 * Returns whether the passed node is a paragraph node that starts with a tab node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph node that starts with a tab node.
 */
export function isParagraphNodeStartingWithTab(node) {
    if (isParagraphNode(node)) {
        const children = $(node).children();
        return children.length >= 3 && isEmptySpan(children[0]) && isTabNode(children[1]);
    }
    return false;
}

/**
 * Returns whether the passed node is a paragraph node that starts with a hardbreak node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph node that starts with a hardbreak node.
 */
export function isParagraphNodeStartingWithHardbreak(node) {
    if (isParagraphNode(node)) {
        const children = $(node).children();
        return children.length >= 3 && isEmptySpan(children[0]) && isHardBreakNode(children[1]);
    }
    return false;
}

/**
 * Returns whether the passed node is an empty cell element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an empty cell element.
 */
export function isEmptyCell(node) {
    return ($(node).children().length === 1 && isEmptyParagraph($(node).children().first()));
}

/**
 * Creates a new paragraph element.
 *
 * @returns {jQuery}
 *  A paragraph element, as jQuery object.
 */
export function createParagraphNode() {
    return $('<div class="p">');
}

/**
 * Creates a new paragraph marked as 'implicit'. This is used in
 * empty documents or table cells to allow text insertion in browser.
 *
 * @returns {jQuery}
 *  An implicit paragraph node, as jQuery object.
 */
export function createImplicitParagraphNode() {
    return createParagraphNode().data('implicit', true);
}

/**
 * Flag a paragraph node with data flag 'implicit'. This paragraphs are
 * used in empty documents or table cells to allow text insertion in browser.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be flagged.
 */
export function makeParagraphNodeImplicit(node) {
    $(node).data('implicit', true);
}

/**
 * Returns whether the passed paragraph node is an implicit paragraph
 * node that is used to allow text insertion into document (into empty
 * document or cells). This paragraph nodes were created without sending
 * an operation to the server.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is an implicit paragraph node.
 */
export function isImplicitParagraphNode(node) {
    return $(node).data('implicit') === true;
}

/**
 * Returns whether the passed paragraph node is a paragraph without
 * neighbours (other paragraphs or tables).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph without neighbours.
 */
export function isParagraphWithoutNeighbour(node) {
    return $(node).parent().children(CONTENT_NODE_SELECTOR).length === 1;
}

/**
 * Returns whether the passed paragraph node is a paragraph that is a last
 * paragraph directly behind a table.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph following a table and with no
 *  following sibling.
 */
export function isFinalParagraphBehindTable(node) {
    return ((isTableNode($(node).prev())) && ($(node).nextAll(CONTENT_NODE_SELECTOR).length === 0));
}

/**
 * Returns whether the passed paragraph node is a paragraph that is a last
 * paragraph directly behind a table.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph following a table and with no
 *  following sibling.
 */
export function isFinalParagraphBehindTableInCell(node) {
    return isCellContentNode($(node).parent()) && isFinalParagraphBehindTable(node);
}

/**
 * Returns whether the passed paragraph node is a paragraph that is the first
 * content node (paragraph or table) inside a table cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph that is the first content node
 *  (paragraph or table) inside a table cell.
 */
export function isFirstParagraphInTableInCell(node) {
    return isCellContentNode($(node).parent()) && $(node).prevAll(CONTENT_NODE_SELECTOR).length === 0;
}

/**
 * Returns whether the passed paragraph node is a paragraph that is a last
 * paragraph directly behind a table and that is empty. This paragraph contains
 * only an empty text span and a 'br' element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is an empty paragraph following a table and with no
 *  following sibling.
 */
export function isFinalEmptyParagraphBehindTable(node) {
    return isFinalParagraphBehindTable(node) && isEmptyParagraph(node);
}

/**
 * Returns whether the passed paragraph node is an empty paragraph that is a last
 * paragraph directly behind a table and that is empty and that is positioned inside
 * a table cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is an empty paragraph following a table and with no
 *  following sibling. Furthermore the paragraph is positioned inside a table cell.
 */
export function isFinalEmptyParagraphBehindTableInCell(node) {
    return isCellContentNode($(node).parent()) && isFinalEmptyParagraphBehindTable(node);
}

/**
 * Returns whether the passed paragraph node is a paragraph inside a table cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph inside a table cell.
 */
export function isParagraphInTableCell(node) {
    return isCellContentNode($(node).parent());
}

/**
 * Returns whether the height of the passed paragraph node can be increased and decreased
 * dynamically. This can happen for implicit paragraph nodes that are behind tables or for
 * non-implicit paragraph nodes, that are empty, behind a table, with no following sibling
 * and inside a table cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph, whose height need to be set dynamically.
 */
export function isIncreasableParagraph(node) {
    return ((isImplicitParagraphNode(node)) && (isFinalParagraphBehindTable(node))) || isFinalEmptyParagraphBehindTableInCell(node);
}

/**
 * Returns whether the passed paragraph node is a paragraph that can be merged
 * with a following paragraph.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph that is followed by a further paragraph.
 */
export function isMergeableParagraph(node) {
    if ($(node).next().hasClass('page-break')) {
        return (($(node).next().next().length !== 0) && (isParagraphNode($(node).next().next())));
    } else {
        return (($(node).next().length !== 0) && (isParagraphNode($(node).next())));
    }
}

/**
 * Collecting all nodes below a specified root node that fullfill a specified
 * selector.
 * Additionally the selection can optionally be reduced by a reference node. It
 * is possible to reduce the selection to the nodes before or after the reference
 * node.
 *
 * @param {Node|jQuery} rootNode
 *  The root node, below that the descendant nodes are searched.
 *
 * @param {String} selector
 *  A selector string to filter the descendant nodes.
 *
 * @param {Node|jQuery} [referenceNode]
 *  An optional reference node that can be used to reduce the found node collection
 *  to the range before or after this reference node.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.followingNodes=true]
 *      Optional parameter in conjunction with a specified reference node. If set to
 *      true, the selection is reduced to nodes following the reference node. Otherwise
 *      only nodes before the reference node will become part of the selection.
 *  @param {Boolean} [options.includeReference=true]
 *      Optional parameter in conjunction with a specified reference node. If set to
 *      true, the selection will include the reference node itself. Otherwise it will
 *      not be part of the selection.
 *
 * @returns {jQuery}
 *  A selection that contain all descendants relative to the specified root node,
 *  the selector and optionally a reference node. If not descendant was found, the
 *  jQuery list will be empty.
 */
export function getNodeSelection(rootNode, selector, referenceNode, options) {

    // the jQuery object with all nodes with valid selector
    let allNodes = $(rootNode).find(selector);
    // the index of the referenceNode in the jQuery selection
    let index = 0;
    // whether the reference node shall be included into the searched selection
    let includeReference = true;

    if (referenceNode) {

        index = allNodes.index(referenceNode);

        if (index > -1) {

            includeReference = getBooleanOption(options, 'includeReference', true);

            if (getBooleanOption(options, 'followingNodes', true)) {
                if (!includeReference) { index++; }
                allNodes = allNodes.slice(index);
            } else {
                if (includeReference) { index++; }
                allNodes = allNodes.slice(0, index);
            }
        }
    }

    return allNodes;
}

/**
 * A jQuery selector that matches elements representing a table.
 */
export const TABLE_NODE_SELECTOR = 'table';

/**
 * A jQuery selector that matches elements representing a table with exceeded size.
 */
export const TABLE_SIZE_EXCEEDED_NODE_SELECTOR = 'table.size-exceeded';

/**
 * A jQuery selector that matches placeholder div elements included into an exceeded size table.
 */
export const PLACEHOLDER_SELECTOR = 'div.placeholder';

/**
 * A jQuery selector that matches elements representing a table. This can be a table node itself
 * or a drawing of type 'table'.
 */
export const TABLE_NODE_AND_DRAWING_SELECTOR = TABLE_NODE_SELECTOR + ', ' + DrawingFrame.TABLE_TEXTFRAME_NODE_SELECTOR;

/**
 * Returns whether the passed node is a table element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table element.
 */
export function isTableNode(node) {
    return $(node).is(TABLE_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a table element that is located inside
 * a drawing of type table (in this case it is not counted for the logical
 * position).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table element that is located inside
 *  a drawing of type table.
 */
export function isTableNodeInTableDrawing(node) {
    return isTableNode(node) && $(node).hasClass(DrawingFrame.TABLE_NODE_IN_TABLE_DRAWING_CLASS);
}

/**
 * Returns whether the passed table node is a replacement table for
 * non-editable oversized tables. This table node is created without
 * further operations and takes care of its presentation.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a replacement table for oversized tables.
 */
export function isExceededSizeTableNode(node) {
    return isTableNode(node) && $(node).hasClass('size-exceeded');
}

/**
 * Makes a exceeded size table placeholder from a provided empty table node.
 *
 * @param {Node|jQuery|Null} [tableNode]
 */
export function makeExceededSizeTable(tableNode, overflowElement, value, maxValue) {
    let placeHolder;
    const cell = $('<td>').append(
        placeHolder = $('<div>').addClass('placeholder').append(
            $('<div>').addClass('abs background-icon').append(createIcon('bi:table')),
            $('<p>').text(gt('Table')),
            $('<p>').text(gt('This table is too large to be displayed here.'))
        ));
    const labels = getTableSizeExceededTextLabels(overflowElement, value, maxValue);

    if (labels.countLabel && labels.limitLabel) {
        placeHolder.append($('<p>').text(labels.countLabel + ' ' + labels.limitLabel));
    }

    $(tableNode).addClass('size-exceeded')
        .attr('contenteditable', false)
        .append($('<tr>').append(cell));
}

/**
 * Provides the correct localized labels for a table exceeds size case.
 *
 * @param {String} [type]
 *  Specifies which part of a table exceeded the size. Currently the parts
 *  "rows", "cols", "cells".
 *
 * @param {Integer} [value]
 *  Specifies the current value of the table part which exceeds the limit.
 *
 * @param {Integer} [maxValue]
 *  Specifies the maximal allowed value of the table part which exceeds the
 *  limit.
 */
export function getTableSizeExceededTextLabels(type, value, maxValue) {

    const result = { countLabel: null, limitLabel: null };

    switch (type) {
        case 'rows':
            //#. %1$d is the number of rows in an oversized text table
            result.countLabel = gt.ngettext('The table contains %1$d row.', 'This table contains %1$d rows.', value, value);
            //#. %1$d is the maximum allowed number of rows in a text table
            result.limitLabel = gt.ngettext('Tables are limited to %1$d row.', 'Tables are limited to %1$d rows.', maxValue, maxValue);
            break;
        case 'cols':
            //#. %1$d is the number of columns in an oversized text table
            result.countLabel = gt.ngettext('The table contains %1$d column.', 'This table contains %1$d columns.', value, value);
            //#. %1$d is the maximum allowed number of columns in a text table
            result.limitLabel = gt.ngettext('Tables are limited to %1$d column.', 'Tables are limited to %1$d columns.', maxValue, maxValue);
            break;
        case 'cells':
            //#. %1$d is the number of cells in an oversized text table
            result.countLabel = gt.ngettext('The table contains %1$d cell.', 'This table contains %1$d cells.', value, value);
            //#. %1$d is the maximum allowed number of cells in a text table
            result.limitLabel = gt.ngettext('Tables are limited to %1$d cell.', 'Tables are limited to %1$d cells.', maxValue, maxValue);
    }

    return result;
}

/**
 * Returns whether the passed node is a table node containing the splitting
 * class.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a replacement table for oversized tables.
 */
export function isSplittedTableNode(node) {
    return isTableNode(node) && $(node).hasClass('tb-split-nb');
}

/**
 * Returns whether the passed node is a table node containing at least one merged cell. Merged cells
 * in inner tables are not counted.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table containing at least one merged cell.
 */
export function isTableWithMergedCells(node) {
    let selector = '> tbody > ' + TABLE_ROWNODE_SELECTOR + ' > ' + MERGED_CELL_SELECTOR;
    if (isTableNode(node)) {
        return $(node).find(selector).length > 0;
    } else if (DrawingFrame.isTableDrawingFrame(node)) {
        selector = '> .content > .textframe > ' + TABLE_NODE_SELECTOR + ' ' + selector;
        return $(node).find(selector).length > 0;
    }
    return false;
}

/**
 * Returns whether the passed node is a div node with class 'placeholder' inside a table with
 * an exceeded size.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is an div node with class 'placeholder' inside a replacement table
 *  for oversized tables.
 */
export function isPlaceholderNode(node) {
    return $(node).is(PLACEHOLDER_SELECTOR);
}

/**
 * Returns whether the passed node is a table node that is located in
 * another table.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table node inside another table.
 */
export function isTableInTableNode(node) {
    return isTableNode(node) && isCellContentNode($(node).parent());
}

/**
 * Returns whether the passed node is a table node that is located in
 * another node (table, text frame, ...) so that it is NOT a top level
 * node inside the page content node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table node located inside another node
 *  that is not the page content.
 */
export function isChildTableNode(node) {
    return isTableNode(node) && !$(node).parent().is(PAGECONTENT_NODE_SELECTOR);
}

/**
 * A jQuery selector that matches elements representing a table row.
 */
export const TABLE_ROWNODE_SELECTOR = 'tr';

/**
 * Returns whether the passed node is a table row element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table row element.
 */
export function isTableRowNode(node) {
    return $(node).is(TABLE_ROWNODE_SELECTOR);
}

/**
 * Returns whether the passed node is a table row element inside a first
 * level table element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table row element in a first level table
 *  element.
 */
export function isTopLevelRowNode(node) {
    return isTableRowNode(node) && $(node).parents(TABLE_ROWNODE_SELECTOR).length < 1;
}

/**
 * A class name that matches elements representing a table page break row.
 */
export const TABLE_PAGEBREAK_ROWNODE_CLASSNAME = 'pb-row';

/**
 * A jQuery selector that matches elements representing a table page break row.
 */
export const TABLE_PAGEBREAK_ROWNODE_SELECTOR = '.pb-row';

/**
 * A class name that matches elements representing a repeated, read-only table row.
 */
export const TABLE_REPEATED_ROWNODE_CLASSNAME = 'repeated-row';

/**
 * A jQuery selector that matches elements representing a repeated, read-only table row.
 */
export const TABLE_REPEATED_ROWNODE_SELECTOR = '.repeated-row';

/**
 * A class name that matches elements representing a repeated, read-only table row.
 */
export const TABLE_REPEAT_ROWNODE_CLASSNAME = 'repeat-row';

/**
 * A jQuery selector that matches elements representing a repeated, read-only table row.
 */
export const TABLE_REPEAT_ROWNODE_SELECTOR = '.repeat-row';

/**
 * Returns whether the passed node is a table page break row element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table page break row element.
 */
export function isTablePageBreakRowNode(node) {
    return $(node).hasClass(TABLE_PAGEBREAK_ROWNODE_CLASSNAME);
}

/**
 * Returns whether the passed node is repeated table row element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is repeated table row element.
 */
export function isRepeatedRowNode(node) {
    return $(node).hasClass(TABLE_REPEATED_ROWNODE_CLASSNAME);
}

/**
 * Info: this function is not the same as DOM.isRepeatedRowNode!
 * Returns whether the passed node is table row element to be repeated across table split pages.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is repeated table row element.
 */
export function isRepeatRowNode(node) {
    return $(node).hasClass(TABLE_REPEAT_ROWNODE_CLASSNAME);
}

/**
 * Returns whether the passed node is repeated table row element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is repeated table row element.
 */
export function isFirstRowInRepeatingRowsTable(node) {
    return isRepeatRowNode(node) && !$(node).prev().length; // check if table is having repeating rows property
}

/**
 * Returns whether the passed node is a table node containing a page break
 * row element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table node containing a page break row element.
 */
export function isTableWithPageBreakRowNode(node) {
    return isTableNode(node) && ($(node).find(TABLE_PAGEBREAK_ROWNODE_SELECTOR).length > 0);
}

/**
 * Returns the collection of table rows from the passed table element.
 *
 * @param {HTMLTableElement|jQuery} tableNode
 *  The table DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  A jQuery collection containing all rows of the specified table.
 */
export function getTableRows(tableNode) {

    if (isTableNode(tableNode)) {
        return $(tableNode).children('tbody').children('tr').not(TABLE_PAGEBREAK_ROWNODE_SELECTOR);
    } else if (DrawingFrame.isTableDrawingFrame(tableNode)) {
        return $(tableNode).children('.content:not(.copy)').find('tr');
    } else {
        return $();
    }
}

/**
 * A jQuery selector that matches elements representing a table cell.
 */
export const TABLE_CELLNODE_SELECTOR = 'td';

/**
 * The class name for (horizontally) merged table cells.
 */
export const MERGED_CELL_CLASS = 'mergedcell';

/**
 * A jQuery selector that matches elements representing a (horizontally) merged table cell node.
 */
export const MERGED_CELL_SELECTOR = TABLE_CELLNODE_SELECTOR + '.' + MERGED_CELL_CLASS;

/**
 * Returns whether the passed node is a table cell element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table cell element.
 */
export function isTableCellNode(node) {
    return $(node).is(TABLE_CELLNODE_SELECTOR);
}

/**
 * Creates a new table cell element.
 *
 * @param {jQuery} paragraph
 *  A paragraph node that is inserted into the cellcontent div of the new
 *  table cell.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.plain=false]
 *      If set to true, no resize nodes will be inserted into the cell, and
 *      other special formatting will be omitted (for example, the
 *      'contenteditable' attributes).
 *
 * @returns {jQuery}
 *  A table cell element, as jQuery object.
 */
export function createTableCellNode(paragraph, options) {

    const cellContentNode = $('<div>').addClass('cellcontent');
    const cellChildNode = $('<div>').addClass('cell');
    const cellNode = $('<td>');
    const plain = getBooleanOption(options, 'plain', false);

    if (!plain) {
        cellChildNode.append($('<div>').addClass('resize bottom'), $('<div>').addClass('resize right'));
        cellChildNode.children().toggleClass('touch', TOUCH_DEVICE);
    }
    cellNode.append(cellChildNode.append(cellContentNode.append(paragraph)));

    if (!plain) {
        // -> setting attribute only for IE.
        //    this leads to a yellow border around div.p in Chrome
        // -> Chrome Bug is fixed with: "outline: none !important;"
        handleReadOnly(cellChildNode);
        cellContentNode.attr({ contenteditable: true, 'data-gramm': false, 'data-focus-role': 'cell' });
    }

    return cellNode;
}

/**
 * A jQuery selector that matches elements representing a cell content node.
 */
export const CELLCONTENT_NODE_SELECTOR = 'div.cellcontent';

/**
 * A jQuery selector that matches elements representing a cell node inside a table cell.
 */
export const CELL_NODE_SELECTOR = 'div.cell';

/**
 * Returns whether the passed node is a table cell content element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table cell content element.
 */
export function isCellContentNode(node) {
    return $(node).is(CELLCONTENT_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a table cell node element inside a table cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table cell node element.
 */
export function isCellNode(node) {
    return $(node).is(CELL_NODE_SELECTOR);
}

/**
 * Returns the container node of a table cell that contains all top-level
 * content nodes (paragraphs and tables).
 *
 * @param {HTMLTableCellElement|jQuery} cellNode
 *  The table cell DOM node. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The container DOM node from the passed table cell that contains all
 *  top-level content nodes (paragraphs and tables).
 */
export function getCellContentNode(cellNode) {
    return $(cellNode).find('> * > ' + CELLCONTENT_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a table cell node element that is
 * a start cell of a vertically merged cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table cell node element that is
 * a start cell of a vertically merged cell.
 */
export function isVerticalStartCellNode(node) {
    return isCellNode(node) && $(node).attr('mergeVert') === 'restart';
}

/**
 * Returns whether the passed node is a table cell node element that is
 * a 'continue' cell of a vertically merged cell. So it is not visible.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table cell node element that is
 *  a 'continue' cell of a vertically merged cell, so that it is not
 *  visible.
 */
export function isVerticalContinueCellNode(node) {
    return isCellNode(node) && $(node).attr('mergeVert') === 'continue';
}

/**
 * Returns whether the passed node is a table resize element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table resize element.
 */
export function isResizeNode(node) {
    return $(node).is('div.resize');
}

/**
 * Returns all cell nodes of the passed table. Does not include cells from
 * tables embedded in the passed table.
 *
 * @param {HTMLTableElement|jQuery} tableNode
 *  The table DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  A jQuery collection containing all cells of the passed table.
 */
export function getTableCells(tableNode) {

    if (isTableNode(tableNode)) {
        return $(tableNode).find('> tbody > tr > td');
    } else if (DrawingFrame.isTableDrawingFrame(tableNode)) {
        return $(tableNode).children('.content:not(.copy)').find('tbody > tr > td');
    } else {
        return $();
    }
}

/**
 * A jQuery selector that matches elements representing a table cell
 * or a table row.
 */
export const TABLE_CELL_ROW_NODE_SELECTOR = 'tr, td';

/**
 * Returns whether the passed node is a table row or a table cell element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table row or a table cell element.
 */
export function isTableCellOrRowNode(node) {
    return $(node).is(TABLE_CELL_ROW_NODE_SELECTOR);
}

/**
 * Check, whether the browser selection containing a focus node and an
 * anchor node describes a cell selection.
 *
 * @param {Node|jQuery|Null} focusNode
 *  The focus node of the browser selection.
 *
 * @param {Node|jQuery|Null} anchorNode
 *  The anchor node of the browser selection.
 *
 * @returns {Boolean}
 *  Whether the browser selection containing a focus node and an
 *  anchor node describes a cell selection.
 */
export function isCellRangeSelected(focusNode, anchorNode) {

    // the row containing the anchor node
    let anchorRowNode = null;

    // focus node must be a row
    if (isTableRowNode(focusNode)) {
        // both nodes are rows in the same table
        if (isTableRowNode(anchorNode) && (getDomNode(anchorNode).parentNode === getDomNode(focusNode).parentNode)) { return true; }

        // anchor node can also be a cell content node (only two neighboring cells selected)
        if (isCellContentNode(anchorNode)) {
            anchorRowNode = $(anchorNode).closest(TABLE_ROWNODE_SELECTOR);
            if (anchorRowNode.length > 0 && getDomNode(anchorRowNode).parentNode === getDomNode(focusNode).parentNode) { return true; }
        }
    }

    return false;
}

// text spans, text nodes, text components ================================

// text spans -------------------------------------------------------------

/**
 * This function checks, if a specified node is a text span node. If the text node is missing
 * it adds an empty text node into the span. This is necessary, because jQuerys text-function
 * creates no text node, if it is given an empty string.
 *
 * @param {Node|jQuery|Null} node
 *  The text span element, as jQuery object or html node.
 */
export function ensureExistingTextNode(node) {
    const domNode = node && getDomNode(node);
    if (isTextSpanWithoutTextNode(domNode)) { domNode.appendChild(document.createTextNode('')); }
}

/**
 * This function checks, if a specified node is a text span node inside a tab. If the text node
 * is missing it adds an empty text node into the span. This is necessary, because jQuerys text-function
 * creates no text node, if it is given an empty string.
 *
 * @param {Node|jQuery|Null} node
 *  The text span element, as jQuery object or html node.
 */
export function ensureExistingTextNodeInTab(node) {
    const domNode = node && getDomNode(node);
    if (isTextSpanInTabWithoutTextNode(domNode)) { domNode.appendChild(document.createTextNode('')); }
}

/**
 * This function checks, if a specified node is a text span node inside simple field in ODF.
 * If the text node is missing it adds an empty text node into the span.
 * This is necessary, because jQuerys text-function creates no text node, if it is given an empty string.
 *
 * @param {Node|jQuery|Null} node
 *  The text span element, as jQuery object or html node.
 */
export function ensureExistingTextNodeInField(node) {
    const domNode = node && getDomNode(node);
    if (isTextSpanInFieldWithoutTextNode(domNode)) { domNode.appendChild(document.createTextNode('')); }
}

/**
 * This function checks, if a specified node is a span node inside a field marked with the class
 * 'special-field'. This is for example used in headers for page numbers. If the text node is
 * missing it adds an empty text node into the span.
 *
 * @param {Node|jQuery|Null} node
 *  The text span element, as jQuery object or html node.
 */
export function ensureExistingTextNodeInSpecialField(node) {
    const domNode = node && getDomNode(node);
    if (isSpanInSpecialFieldWithoutTextNode(domNode)) { domNode.appendChild(document.createTextNode('')); }
}

/**
 * Returns whether the passed node is a `<span>` element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element.
 */
export function isSpan(node) {
    return ($(node).is('span'));
}

/**
 * Returns whether the passed node is a `<span>` element containing a
 * single text node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element with a text node.
 */
export function isTextSpan(node) {
    const contents = $(node).contents();
    return ($(node).is('span') && (contents.length === 1) && (contents[0].nodeType === 3));
}

/**
 * Returns whether the passed node is a text span element that extends to more
 * than one line in the containing paragraph.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element with a text node that extends
 *  to more than one line.
 */
export function isMultiLineTextSpan(node) {
    return isTextSpan(node) && $(node).height() > parseCssLength(node, 'lineHeight');
}

/**
 * Returns whether the passed node is a `<span>` whose parent is a `<div>`
 * element with class "hardbreak".
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a hard break span element.
 */
export function isHardBreakSpan(node) {
    const domNode = node && getDomNode(node);
    return $(domNode).is('span') && domNode.parentNode && isHardBreakNode(domNode.parentNode);
}

/**
 * Returns whether the passed node is a `<span>` element containing an
 * empty text node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element with an empty text node.
 */
export function isEmptySpan(node) {
    return (isTextSpan(node) && ($(node).text().length === 0));
}

/**
 * Returns whether the passed node is a `<span>` element representing a
 * text portion (a child element of a paragraph node).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text portion span element.
 */
export function isPortionSpan(node) {
    return isTextSpan(node) && isParagraphNode(getDomNode(node).parentNode);
}

/**
 * Internet Explorer removes empty text nodes from text spans inside
 * table cells.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element without a text node.
 */
export function isTextSpanWithoutTextNode(node) {
    const domNode = node && getDomNode(node);
    return $(domNode).is('span') && ($(domNode).contents().length === 0) && isParagraphNode(domNode.parentNode);
}

/**
 * Returns whether span has empty text nodes inside a tab.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element inside a tab without a text node.
 */
export function isTextSpanInTabWithoutTextNode(node) {
    const domNode = node && getDomNode(node);
    return $(domNode).is('span') && ($(domNode).contents().length === 0) && isTabNode(domNode.parentNode);
}

/**
 * Returns whether span has empty text nodes inside ODF empty field.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span element without a text node.
 */
export function isTextSpanInFieldWithoutTextNode(node) {
    const domNode = node && getDomNode(node);
    return $(domNode).is('span') && ($(domNode).contents().length === 0) && isFieldNode(domNode.parentNode);
}

/**
 * Returns whether the specified node is a span without a text node inside
 * a special field (for example page number in header).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a span without a text node inside a special field.
 */
export function isSpanInSpecialFieldWithoutTextNode(node) {
    const domNode = node && getDomNode(node);
    return $(domNode).is('span') && ($(domNode).contents().length === 0) && isSpecialField(domNode.parentNode);
}

/**
 * Returns whether the passed node is a text node embedded in a text
 * portion span (see DOM.isPortionSpan() method).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text node in a portion span element.
 */
export function isTextNodeInPortionSpan(node) {
    const domNode = node && getDomNode(node);
    return domNode && (domNode.nodeType === 3) && isPortionSpan(domNode.parentNode);
}

/**
 * Returns the first text portion span of the specified paragraph node.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node whose first text portion span will be returned. If
 *  this object is a jQuery collection, uses the first DOM node it
 *  contains.
 *
 * @returns {HTMLElement}
 *  The first text portion span of the paragraph.
 */
export function findFirstPortionSpan(paragraph) {
    return findDescendantNode(paragraph, function () { return isTextSpan(this); }, { children: true });
}

/**
 * Returns the last text portion span of the specified paragraph node.
 *
 * @param {HTMLElement|jQuery} paragraph
 *  The paragraph node whose last text portion span will be returned. If
 *  this object is a jQuery collection, uses the first DOM node it
 *  contains.
 *
 * @returns {HTMLElement}
 *  The last text portion span of the paragraph.
 */
export function findLastPortionSpan(paragraph) {
    return findDescendantNode(paragraph, function () { return isTextSpan(this); }, { children: true, reverse: true });
}

// change tracking ------------------------------------------

/**
 * A jQuery selector that matches elements representing a node with
 * a changes attribute.
 */
export const CHANGETRACK_NODE_SELECTOR = '[data-change-track-inserted="true"], [data-change-track-removed="true"], [data-change-track-modified="true"]';

/**
 * A jQuery selector that matches elements representing a node with
 * the changes attribute 'inserted'.
 */
export const CHANGETRACK_INSERTED_NODE_SELECTOR = '[data-change-track-inserted="true"]';

/**
 * A jQuery selector that matches elements representing a node with
 * the changes attribute 'removed'.
 */
export const CHANGETRACK_REMOVED_NODE_SELECTOR = '[data-change-track-removed="true"]';

/**
 * A jQuery selector that matches elements representing a node with
 * the changes attribute 'modified'.
 */
export const CHANGETRACK_MODIFIED_NODE_SELECTOR = '[data-change-track-modified="true"]';

/**
 * A jQuery selector that matches table rows with the changes attribute 'inserted'.
 */
export const CHANGETRACK_INSERTED_ROW_SELECTOR = 'tr[data-change-track-inserted="true"]';

/**
 * A jQuery selector that matches table rows with the changes attribute 'removed'.
 */
export const CHANGETRACK_REMOVED_ROW_SELECTOR = 'tr[data-change-track-removed="true"]';

/**
 * A jQuery selector that matches table cells with the changes attribute 'removed'.
 */
export const CHANGETRACK_REMOVED_CELL_SELECTOR = 'td[data-change-track-removed="true"]';

/**
 * Returns whether the passed node is a node with a changes
 * attribute.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a node with change tracking attribute.
 */
export function isChangeTrackNode(node) {
    return $(node).is(CHANGETRACK_NODE_SELECTOR) || isChangeTrackInlineNode(node);
}

/**
 * Returns whether the passed node is a node with changes 'inserted'
 * attribute.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a node with changes 'inserted'
 *  attribute.
 */
export function isChangeTrackInsertNode(node) {
    return $(node).is(CHANGETRACK_INSERTED_NODE_SELECTOR) || isChangeTrackInsertInlineNode(node);
}

/**
 * Returns whether the passed node is a node with changes 'removed' attribute.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a node with changes 'removed' attribute.
 */
export function isChangeTrackRemoveNode(node) {
    return $(node).is(CHANGETRACK_REMOVED_NODE_SELECTOR) || isChangeTrackRemoveInlineNode(node);
}

/**
 * Returns whether the passed node is a node with changes 'modified'
 * attribute.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a node with changes 'modified' attribute.
 */
export function isChangeTrackModifyNode(node) {
    return $(node).is(CHANGETRACK_MODIFIED_NODE_SELECTOR) || isChangeTrackModifyInlineNode(node);
}

/**
 * Returns whether the passed node is an inline node with changes attribute
 * at its span (tab, hard break, field, ...).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an inline node with changes attribute set to its span.
 */
export function isChangeTrackInlineNode(node) {
    return isInlineComponentNode(node) && $(getDomNode(node).firstChild).is(CHANGETRACK_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is an inline node with changes 'inserted'
 * attribute at its span (tab, hard break, field, ...).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an inline node with changes 'inserted' attribute
 *  set to its span.
 */
export function isChangeTrackInsertInlineNode(node) {
    return isInlineComponentNode(node) && $(getDomNode(node).firstChild).is(CHANGETRACK_INSERTED_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is an inline node with changes 'removed'
 * attribute at its span (tab, hard break, field, ...).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an inline node with changes 'removed' attribute
 *  set to its span.
 */
export function isChangeTrackRemoveInlineNode(node) {
    return isInlineComponentNode(node) && $(getDomNode(node).firstChild).is(CHANGETRACK_REMOVED_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is an inline node with changes 'modified'
 * attribute at its span (tab, hard break, field, ...).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an inline node with changes 'modified' attribute
 *  set to its span.
 */
export function isChangeTrackModifyInlineNode(node) {
    return isInlineComponentNode(node) && $(getDomNode(node).firstChild).is(CHANGETRACK_MODIFIED_NODE_SELECTOR);
}

// text components: fields, tabs ------------------------------------------

/**
 * A jQuery selector that matches elements representing an inline component
 * inside a paragraph (text components or inline drawing objects).
 */
export const INLINE_COMPONENT_NODE_SELECTOR = 'div.inline';

/**
 * Returns whether the passed node is an editable inline component node in
 * a paragraph. Inline components include text components (e.g. fields,
 * tabs) and inline drawing objects.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an inline component node in a paragraph.
 */
export function isInlineComponentNode(node) {
    return $(node).is(INLINE_COMPONENT_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a `<div>` container with embedded
 * text spans, used as root elements for special text components in a
 * paragraph.
 *
 * Does NOT return `true` for helper nodes that do not represent editable
 * contents of a paragraph (e.g. numbering labels). To check for all helper
 * nodes that contain text spans (also non-editable elements such as
 * numbering labels), use the method `DOM.isTextSpanContainerNode()`
 * instead.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a `<div>` element representing an editable
 *  text component in a paragraph.
 */
export function isTextComponentNode(node) {
    // text component nodes are all inline nodes, except drawings.
    // This includes tabs, fields and hard breaks
    return isInlineComponentNode(node) && !isDrawingFrame(node);
}

/**
 * Returns whether the passed node is a `<div>` container with embedded
 * text spans, used as root elements for special text components in a
 * paragraph.
 *
 * Does NOT return `true` for text nodes contained in helper nodes that do
 * not represent editable contents of a paragraph (e.g. numbering labels).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a DOM text node contained in a `<div>`
 *  element representing an editable text component in a paragraph.
 */
export function isTextNodeInTextComponent(node) {
    node = node ? getDomNode(node) : null;
    return node && node.parentNode && isTextComponentNode(node.parentNode.parentNode);
}

/**
 * Returns whether the passed node is a `<div>` container that is an inline
 * component node, but is not a placeholder for a drawing or a comment and
 * no range marker and no complex field node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an inline component node, that is not a
 *  placeholder for a drawing or a comment.
 */
export function isEditableInlineComponentNode(node) {
    return isInlineComponentNode(node) && !isSubstitutionPlaceHolderNode(node) && !isRangeMarkerNode(node) && !isBookmarkNode(node) && !isComplexFieldNode(node);
}

/**
 * The class name representing a text field.
 */
export const FIELD_NODE_CLASS_NAME = 'field';

/**
 * A jQuery selector that matches elements representing a text field.
 */
export const FIELD_NODE_SELECTOR = 'div.field';

/**
 * CSS class name that matches elements representing special text field:
 * page number type, only in header/footer.
 */
export const SPECIAL_FIELD_NODE_CLASSNAME = 'special-field';

/**
 * A jQuery selector that matches elements representing special text field:
 * page number type, only in header/footer.
 */
export const SPECIAL_FIELD_NODE_SELECTOR = '.special-field';

/**
 * A jQuery selector that matches elements representing a text field with type page-count.
 */
export const PAGECOUNT_FIELD_SELECTOR = '.field.field-page-count, .field.field-NUMPAGES';

/**
 * A jQuery selector that matches elements representing a text field with type page-number.
 */
export const PAGENUMBER_FIELD_SELECTOR = '.field.field-page-number';

/**
 * Returns whether the passed node is an element representing a text field.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing a text field.
 */
export function isFieldNode(node) {
    return $(node).hasClass(FIELD_NODE_CLASS_NAME);
}

/**
 * Returns whether the passed node is element representing complex text field,
 * in header/footer, and with type page number.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing a special text field.
 */
export function isSpecialField(node) {
    return $(node).hasClass(SPECIAL_FIELD_NODE_CLASSNAME);
}

/**
 * Returns a new empty text field element.
 *
 * @returns {jQuery}
 *  A new empty text field element, as jQuery object.
 */
export function createFieldNode() {
    return $('<div>', { contenteditable: false }).addClass('inline field');
}

/**
 * A jQuery selector that matches elements representing a tab.
 */
export const TAB_NODE_SELECTOR = 'div.tab';

/**
 * Returns whether the passed node is a tab element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a is a tab element.
 */
export function isTabNode(node) {
    return $(node).is(TAB_NODE_SELECTOR);
}

/**
 * Returns a new tab element.
 *
 * @returns {jQuery}
 *  A new tab element, as jQuery object.
 */
export function createTabNode() {
    return $('<div>', { contenteditable: false }).addClass('inline tab');
}

/**
 * Returns whether the passed node is a `<span>` whose parent is a `<div>`
 * element with class "tab".
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a tab span element.
 */
export function isTabSpan(node) {
    const domNode = node && getDomNode(node);
    return $(domNode).is('span') && domNode.parentNode && isTabNode(domNode.parentNode);
}

/**
 * A class name representing a hard-break.
 */
export const HARDBREAK_NODE_CLASS_NAME = 'hardbreak';

/**
 * A jQuery selector that matches elements representing a hard-break.
 */
export const HARDBREAK_NODE_SELECTOR = 'div.hardbreak';

/**
 * Returns whether the passed node is a hard-break element
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text node in a text field element.
 */
export function isHardBreakNode(node) {
    return $(node).hasClass(HARDBREAK_NODE_CLASS_NAME);
}

/**
 * Returns a new hard-break element.
 *
 * @param {String} type
 *  The type of the hard-break element. Supported values are
 *  'page', 'column' or 'textWrapping'.
 *
 * @returns {jQuery}
 *  A new hard-break element, as jQuery object.
 */
export function createHardBreakNode(type) {
    return $('<div>', { contenteditable: false }).addClass('inline hardbreak').data('type', type);
}

/**
 * Returns whether the passed node contains a hardbreak element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains a hardbreak element.
 */
export function hasHardbreakNode(node) {
    return $(node).find(HARDBREAK_NODE_SELECTOR).length > 0;
}

// paragraph helper nodes -------------------------------------------------

/**
 * Returns whether the passed node is a `<div>` container with embedded
 * text spans, used as root elements for special text elements in a
 * paragraph.
 *
 * Returns also `true` for helper nodes that do NOT represent editable
 * contents of a paragraph (e.g. numbering labels). To check for container
 * nodes that represent editable components in a paragraph only, use the
 * method `DOM.isTextComponentNode()` instead.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a `<div>` element containing text spans.
 */
export function isTextSpanContainerNode(node) {
    node = node ? getDomNode(node) : null;
    return !!node && $(node).is('div') && isParagraphNode(node.parentNode) && isTextSpan(node.firstChild);
}

/**
 * A jQuery selector that matches elements representing a list label.
 */
export const LIST_LABEL_NODE_SELECTOR = 'div.list-label';

/**
 * Returns whether the passed node is an element representing a list label.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a list label element.
 */
export function isListLabelNode(node) {
    return $(node).is(LIST_LABEL_NODE_SELECTOR);
}

/**
 * Creates a new element representing a list label.
 *
 * @param {String} [text]
 *  The text contents of the list label node.
 *
 * @returns {jQuery}
 *  A new list label node, as jQuery object.
 */
export function createListLabelNode(text) {
    const node = $('<div>').addClass('helper list-label').append(createSpan({ label: text || '' }));

    // Bug 27902: prevent placing text cursor into list label node
    // Bug 26568: IE9 and IE10 show a drawing selection frame when 'contenteditable' is set to false
    // Bug 29546 (still open): this leads to a wrong mouse pointer (move) for the list labels
    if (!_.browser.IE || (_.browser.IE >= 11)) { node.attr('contenteditable', false); }
    return node;
}

/**
 * Returns whether the passed node is a dummy text node that is used in
 * empty paragraphs to preserve an initial element height according to the
 * current font size.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a dummy text node.
 */
export function isDummyTextNode(node) {
    return $(node).data('dummy') === true;
}

/**
 * Creates a dummy text node that is used in empty paragraphs to preserve
 * an initial element height according to the current font size.
 *
 * @returns {jQuery}
 *  A dummy text node, as jQuery object.
 */
export function createDummyTextNode() {
    // TODO: create correct element for current browser
    return $('<br>').data('dummy', true);
}

// drawing nodes ----------------------------------------------------------

/**
 * Returns whether the passed node is the root node of a drawing frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the root node of a drawing frame.
 */
export const isDrawingFrame = DrawingFrame.isDrawingFrame;

/**
 * A jQuery selector that matches nodes representing a drawing frame.
 */
export const DRAWING_NODE_SELECTOR = '.' + DrawingFrame.NODE_CLASS;

/**
 * A jQuery selector that matches nodes representing a text frame content
 * element inside a drawing node. This is reassigned here for convenience
 * reasons to reduce required imports.
 */
export const TEXTFRAMECONTENT_NODE_SELECTOR = DrawingFrame.TEXTFRAMECONTENT_NODE_SELECTOR;

/**
 * A jQuery selector that matches elements representing a floating node
 * inside a paragraph (editable component nodes and helper nodes).
 */
export const FLOATING_NODE_SELECTOR = 'div.float';

/**
 * Returns whether the passed node is a floating node in a paragraph.
 * Floating nodes include floating drawing objects, and their associated
 * helper nodes.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a floating node in a paragraph.
 */
export function isFloatingNode(node) {
    return $(node).is(FLOATING_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is a `<div>` element wrapping a drawing
 * in inline mode.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div element wrapping a drawing and is
 *  rendered inlined.
 */
export function isInlineDrawingNode(node) {
    return DrawingFrame.isDrawingFrame(node) && isInlineComponentNode(node);
}

/**
 * Returns whether the passed node is a `<div>` element wrapping a drawing
 * in floating mode.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div element wrapping a drawing and is
 *  rendered floated.
 */
export function isFloatingDrawingNode(node) {
    return DrawingFrame.isDrawingFrame(node) && isFloatingNode(node);
}

/**
 * Returns whether the passed node (typically a paragraph) contains a floated
 * drawing node as direct child.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains a floated drawing node as direct child.
 */
export function containsFloatingDrawingNode(node) {
    return $(node).children(FLOATING_NODE_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is a `<div>` element wrapping a drawing
 * of type "image".
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div element wrapping a drawing of type
 *  'image'.
 */
export function isImageDrawingNode(node) {
    return $(node).is(DrawingFrame.NODE_SELECTOR) && ($(node).data('type') === 'image');
}

/**
 * Returns whether the passed node is a `<div>` element wrapping a drawing
 * that is not of type "image".
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div element wrapping a drawing that is not
 *  of type 'image'.
 */
export function isNonImageDrawingNode(node) {
    return $(node).is(DrawingFrame.NODE_SELECTOR) && ($(node).data('type') !== 'image');
}

/**
 * Returns whether the passed node is a drawing node that cannot be restored
 * after deletion.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing node that cannot be restored after
 *  deletion.
 */
export function isUnrestorableDrawingNode(node) {

    if (!DrawingFrame.isDrawingFrame(node)) { return false; }

    // unrestorable are all drawing nodes, that are not images, not text frames. Groups are restorable,
    // if they contain only images or text frames.
    if (DrawingFrame.isGroupDrawingFrame(node)) {
        // checking the children of the group
        return _.find($(node).find(DrawingFrame.NODE_SELECTOR), function (drawingNode) {
            return (isNonImageDrawingNode(drawingNode) && !DrawingFrame.isShapeDrawingFrame(drawingNode) && !DrawingFrame.isGroupDrawingFrame(drawingNode)) || DrawingFrame.isWatermarkDrawingNode(drawingNode);
        });
    } else {
        return (isNonImageDrawingNode(node) && !DrawingFrame.isShapeDrawingFrame(node) && !isListLabelNode($(node).parent())) || DrawingFrame.isWatermarkDrawingNode(node);
    }
}

/**
 * Returns whether the passed node is inside a drawing text frame node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is inside a text frame drawing node.
 */
export function isNodeInsideTextFrame(node) {
    return node && $(node).length > 0 && $(node).closest(DrawingFrame.TEXTFRAME_NODE_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is inside an auto resizing drawing text frame node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is inside an auto resizing text frame drawing node.
 */
export function isNodeInsideAutoResizeHightTextFrame(node) {
    return node && $(node).length > 0 && $(node).closest(DrawingFrame.AUTORESIZEHEIGHT_SELECTOR).length > 0;
}

/**
 * Returns whether the specified node is a text frame that contains no text
 * content and that has no (explicit) border specified.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @param {Object} attributes
 *  The attributes of the drawing node.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.ignoreTemplateText=false]
 *      If set to true, a drawing that contains the template text will be handled
 *      as empty drawing. Otherwise a drawing that contains a template text
 *      is NOT empty. The latter is the default.
 *
 * @returns {Boolean}
 *  Whether the specified node is a text frame that contains no text content
 *  and that has no (explicit) border specified.
 */
export function isEmptyTextframeWithoutBorder(node, attributes, options) {
    return isEmptyTextframe(node, options) && !isBorderAttributeSet(attributes);
}

/**
 * Returns whether the specified attribute set (for a drawing) contains
 * border information for the drawing.
 *
 * @param {Object} attributes
 *  The attributes set (of the drawing node).
 *
 * @returns {Boolean}
 *  Whether the specified attributes set describes a border.
 */
export function isBorderAttributeSet(attributes) {
    return attributes && attributes.line && attributes.line.type !== 'none';
}

/**
 * Returns whether the specified node is a text frame that contains no text
 * content.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.ignoreTemplateText=false]
 *      If set to true, a drawing that contains the template text will be handled
 *      as empty drawing. Otherwise a drawing that contains a template text
 *      is NOT empty. The latter is the default.
 *
 * @returns {Boolean}
 *  Whether the specified node is a text frame that contains no text content.
 */
export function isEmptyTextframe(node, options) {

    // whether the text frame contains no content
    let isEmptyTextFrame = false;
    // all paragraphs and all text spans inside the text frame
    let allParagraphs = null;
    let allTextSpans = null;
    // whether a template text shall be ignored
    const ignoreTemplateText = getBooleanOption(options, 'ignoreTemplateText', false);

    if (DrawingFrame.isTextFrameShapeDrawingFrame(node)) {
        if (DrawingFrame.isModifyingDrawingActive(node)) { // there might be a second content node in the drawing
            allParagraphs = $(node).children('.content:not(.copy)').find(PARAGRAPH_NODE_SELECTOR);
        } else {
            allParagraphs = $(node).find(PARAGRAPH_NODE_SELECTOR);
        }

        if (allParagraphs.length === 1) {
            allTextSpans = allParagraphs.children('span');
            isEmptyTextFrame = (!allTextSpans.text() && allTextSpans.length === 1) || (ignoreTemplateText && allTextSpans.hasClass(TEMPLATE_TEXT_CLASS));
            // Info: This check allows more than one text span with class 'templatetext' although this should never happen (spellchecking not allowed)
            // Info: jQuery 'hasClass' returns true, if one of the text spans has class 'templatetext' (a mixture of spans must never happen).
        }
    }

    return isEmptyTextFrame;
}

/**
 * Returns whether the passed node is a drawing node that has a content node that
 * has the class 'DrawingFrame.EMPTYTEXTFRAME_CLASS' assigned (fast detection) or
 * that contains only an implicit paragraph.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing node that has a content node that
 *  has the class 'DrawingFrame.EMPTYTEXTFRAME_CLASS' assigned or that contains
 *  only an implicit paragraph.
 */
export function isDrawingWithOnlyImplicitParagraph(node) {

    // whether the specified node contains only an implicit paragraph
    const isOnlyImplicitParagraph = false;
    // the content node inside the drawing
    let contentNode = null;
    // all paragraphs and all text spans inside the text frame
    let allParagraphs = null;

    if (DrawingFrame.isDrawingFrame(node)) {
        contentNode = DrawingFrame.getContentNode(node);
        if (contentNode.hasClass(DrawingFrame.EMPTYTEXTFRAME_CLASS)) { return true; }
        // checking for paragraphs
        allParagraphs = contentNode.find(PARAGRAPH_NODE_SELECTOR);

        if (allParagraphs.length === 1 && isImplicitParagraphNode(allParagraphs)) { return true; }
    }

    return isOnlyImplicitParagraph;
}

/**
 * Returns whether the passed node is the first direct child inside a
 * text frame. This can be a paragraph or a table.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the first child inside a text frame.
 */
export function isFirstContentNodeInTextframe(node) {

    // the dom node of the specified node
    const domNode = getDomNode(node);

    return domNode && isContentNode(domNode) && !domNode.previousSibling && DrawingFrame.isTextFrameNode(domNode.parentNode);
}

/**
 * Returns whether the passed node is the first text span inside a text
 * frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the first text span inside a text frame.
 */
export function isFirstTextSpanInTextframe(node) {

    // a text frame containing the specified node
    let textFrame = null;
    // the first paragraph in the text frame
    let firstParagraph = null;

    // Check: node is text span and node is inside text frame and node is the first text span in the text frame
    if (isTextSpan(node)) {
        textFrame = DrawingFrame.getClosestTextFrameDrawingNode(node);

        if (textFrame) {
            firstParagraph = textFrame.find(PARAGRAPH_NODE_SELECTOR).first();
            return findFirstPortionSpan(firstParagraph) === getDomNode(node);
        }
    }

    return false;
}

/**
 * Returns whether the passed node is the last text span inside a text
 * frame.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the last text span inside a text frame.
 */
export function isLastTextSpanInTextframe(node) {

    // a text frame containing the specified node
    let textFrame = null;
    // the first paragraph in the text frame
    let lastParagraph = null;

    // Check: node is text span and node is inside text frame and node is the last text span in the text frame
    if (isTextSpan(node)) {
        textFrame = DrawingFrame.getClosestTextFrameDrawingNode(node);

        if (textFrame) {
            lastParagraph = textFrame.find(PARAGRAPH_NODE_SELECTOR).last();
            return findLastPortionSpan(lastParagraph) === getDomNode(node);
        }
    }

    return false;
}

/**
 * Returns whether the specified node is the last DOM node inside a document.
 * This means that it is a table or a paragraph (or a slide) inside the page content node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the last top level node inside the document.
 */
export function isLastTopLevelNodeInDocument(node) {

    // the dom node of the specified node
    const domNode = getDomNode(node);

    return domNode && !domNode.nextSibling && domNode.parentNode && isPageContentNode(domNode.parentNode);
}

/**
 * The class name representing a text span that contains template text.
 */
export const TEMPLATE_TEXT_CLASS = 'templatetext';

/**
 * Css selector representing a text span containing template text.
 */
export const TEMPLATE_TEXT_SELECTOR = '.templatetext';

/**
 * Returns whether the passed node is a text node with default template text.
 *
 * @param {Node} node
 *  The node that will be checked.
 *
 * @returns {Boolean}
 *  Whether the specified node is a text node and the parent has the specified class
 *  to recognize the template text.
 */
export function isTextFrameTemplateTextNode(node) {
    return node && node.nodeType === 3 && node.parentNode && $(node.parentNode).hasClass(TEMPLATE_TEXT_CLASS);
}

/**
 * Returns whether the passed node is a span with default template text.
 *
 * @param {Node} node
 *  The node that will be checked.
 *
 * @returns {Boolean}
 *  Whether the specified node is a text span node and has the specified class
 *  to recognize the template text.
 */
export function isTextFrameTemplateTextSpan(node) {
    return $(node).hasClass(TEMPLATE_TEXT_CLASS);
}

/**
 * Returns whether the passed node is a paragraph with default template text. This
 * template text span is not necessarily the only child. It is possible, that there
 * is also a list label in the paragraph.
 *
 * @param {Node} node
 *  The node that will be checked.
 *
 * @returns {Boolean}
 *  Whether the specified node is a paragraph node that contains a text span
 *  that has the specified class to recognize the template text.
 */
export function isTextFrameTemplateTextParagraph(node) {
    return isParagraphNode(node) && $(node).children(TEMPLATE_TEXT_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is a text span and the first element without previous node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text span and the first element inside its parent node.
 */
export function isFirstTextSpanInParagraph(node) {
    return isTextSpan(node) && $(node).prev().length === 0;
}

/**
 * Returns whether the passed node is a `<div>` element wrapping an image.
 * Simple 'find' for 'img' does not work, because drawing groups can
 * contain several image drawings and images, but are no image drawings.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div element wrapping an image.
 */
export function isImageNode(node) {
    // drawing div contains another div (class content) that contains an image
    return DrawingFrame.isDrawingFrame(node) && (DrawingFrame.getDrawingType(node) === 'image');
}

/**
 * Returns true if the source of the passed image node points to a mail inline image.
 * See: http://oxpedia.org/wiki/index.php?title=HTTP_API#Requesting_an_image
 * The node can be a `<div>` wrapping an image or an `<img>` itself.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed image node points to a mail inline image.
 */
export function isMailInlineImageNode(node) {

    const img = $(node).is('img') ? $(node) : $(node).find('img');
    const src = img.attr('src');

    if (!src) { return false; }

    // check for URL parameters
    return _.every(['/mail/picture', 'id', 'folder', 'uid'], function (param) {
        return src.indexOf(param) !== -1;
    });
}

/**
 * Returns true if the source of the passed image node points to a server document.
 * The node can be a `<div>` wrapping an image or an `<img>` itself.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed image node points to a server document.
 */
export function isDocumentImageNode(node) {

    const img = $(node).is('img') ? $(node) : $(node).find('img');
    const src = img.attr('src');

    if (!src) { return false; }  // replacement data is shown

    // check for base64 data-URL first
    if (src.substring(0, 10) === 'data:image') {
        return false;
    }

    // check for URL parameters
    return _.every(['id', 'folder_id', 'action', 'get_filename'], function (param) {
        return src.indexOf(param) !== -1;
    });
}

/**
 * Returns true if the source of the passed image node contains a base64 data-URL.
 * The node can be a `<div>` wrapping an image or an `<img>` itself.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed image node contains a base64 data-URL.
 */
export function isBase64ImageNode(node) {

    const img = $(node).is('img') ? $(node) : $(node).find('img');
    const src = img.attr('src');

    return src.substring(0, 10) === 'data:image';
}

/**
 * Returns the value of the given URL param of the image src attribute
 *
 * @param {Node|jQuery} node
 *  The image node to get the URL param from.
 *  If this object is a jQuery collection, uses the first DOM node it contains.
 *
 * @param {String} [param]
 *  The URL param
 *
 * @returns {String|Null}
 *  The value of the given URL param or null
 */
export function getUrlParamFromImageNode(node, param) {

    const img = $(node).is('img') ? $(node) : $(node).find('img');
    const src = img.attr('src');
    let reg;
    let match = null;

    if (src && param && param.length) {
        reg = new RegExp('.*[?&]' + param + '=([^&]+)(&|$)');
        match = src.match(reg);
        return (is.array(match) ? match[1] : null);
    }

    return null;
}

/**
 * Returns the base64 encoded data-URL of the given image node
 *
 * @param {Node|jQuery} node
 *  The image node to get the base64 data-URL from.
 *  If this object is a jQuery collection, uses the first DOM node it contains.
 *
 *  @param {String} [mimeType='image/png']
 *   The mime type of the base64 encoded data-URL.
 *   If not given or not supported by the browser 'image/png' is used.
 *
 *  @returns {String}
 *  The base64 encoded data-URL
 */
export function getBase64FromImageNode(node, mimeType) {

    // the image DOM node
    const img = getDomNode($(node).is('img') ? $(node) : $(node).find('img'));
    // the mime type
    const mime = (is.string(mimeType) && mimeType.length) ? mimeType : 'image/png';
    // create an empty canvas element
    const canvas = createCanvas(img.naturalWidth || img.width, img.naturalHeight || img.height);
    // the base64 data URL
    let base64;

    try {

        // copy the image contents to the canvas
        canvas.getContext('2d').drawImage(img, 0, 0);

        // return the data-URL formatted image
        base64 = canvas.toDataURL(mime);

    } catch (ex) {
        globalLogger.warn('DOM.getBase64FromImageNode(): ' + ((ex && ex.message) ? ex.message : 'failed'));
        base64 = 'data:,';
    }

    return base64;
}

/**
 * A jQuery selector that matches elements representing a drawing offset
 * helper node.
 */
export const OFFSET_NODE_SELECTOR = 'div.float.offset';

/**
 * A jQuery selector that matches elements representing an absolutely
 * positioned drawing.
 */
export const ABSOLUTE_DRAWING_SELECTOR = 'div.drawing.absolute';

/**
 * The CSS class used to mark drawings with a relative size, for example
 * a percentage of the page.
 */
export const RELATIVESIZE_CLASS = 'relativesize';

/**
 * The CSS selector, that matches elements representing a drawing with
 * a relative size.
 */
export const RELATIVESIZE_SELECTOR = '.' + RELATIVESIZE_CLASS;

/**
 * A jQuery selector that matches nodes representing the application content root.
 */
export const ABSOLUTE_RELATIVESIZE_DRAWING_SELECTOR = ABSOLUTE_DRAWING_SELECTOR + RELATIVESIZE_SELECTOR;

/**
 * Returns whether the passed node is a `<div>` element for positioning a
 * drawing with a vertical or horizontal offset.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a div element wrapping a drawing.
 */
export function isOffsetNode(node) {
    return $(node).is(OFFSET_NODE_SELECTOR);
}

/**
 * The CSS class used to mark drawing placeholder nodes. This place holder
 * nodes are located in the page content node and safe the position for
 * an absolutely positioned drawing in the drawing layer.
 */
export const DRAWINGPLACEHOLDER_CLASS = 'drawingplaceholder';

/**
 * The data property name for registering the drawing place holder at the absolutely
 * positioned drawing. This works in both directions, because the drawing place
 * holder has a data attribute with this name to the absolutely positioned drawing
 * and vice versa.
 */
export const DRAWINGPLACEHOLDER_LINK = 'drawingplaceholderlink';

/**
 * The data property name for registering the margins (in px) at the absolutely positioned
 * drawings.
 */
export const DRAWING_MARGINS_PIXEL = 'pixelMargins';

/**
 * The data property name for registering the margins (in hmm) at the absolutely positioned
 * drawings.
 */
export const DRAWING_MARGINS_HMM = 'hmmMargins';

/**
 * A jQuery selector that matches elements representing an image place holder.
 */
export const DRAWINGPLACEHOLDER_NODE_SELECTOR = 'div.' + DRAWINGPLACEHOLDER_CLASS;

/**
 * Returns whether the passed node is an element representing an image
 * place holder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing an image placeholder
 *  node.
 */
export function isDrawingPlaceHolderNode(node) {
    return $(node).hasClass(DRAWINGPLACEHOLDER_CLASS);
}

/**
 * Returns whether the passed node (a paragraph) has at least one child
 * image place holder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node has at least one child representing an image
 *  place holder node.
 */
export function hasDrawingPlaceHolderNode(node) {
    return $(node).children(DRAWINGPLACEHOLDER_NODE_SELECTOR).length > 0;
}

/**
 * Searching the place holder node for a specific drawing node. This place
 * holder node exists in the page content for all drawings inside the
 * drawing layer node. This also works vice versa: Searching the drawing
 * node in the drawing layer for a specified place holder. In both cases
 * the 'data' attribute is named DOM.DRAWINGPLACEHOLDER_LINK.
 *
 * @param {Node|jQuery|Null} [node]
 *  The drawing node.
 *
 * @returns {Node}
 *  The place holder node for the drawing node in the drawing layer node
 *  or the drawing in the drawing layer node itself.
 */
export function getDrawingPlaceHolderNode(node) {

    // the node saved in the data object
    let searchedNode = $(node).data(DRAWINGPLACEHOLDER_LINK);
    // an optional header or footer node
    let marginalRoot = null;
    // the ID of the drawing in the drawing layer
    let drawingID = 0;
    // a string to find the related placeholder node
    let selectorString = null;
    // the nodes with selector string inside header or footer
    let foundNodes = null;
    // a selector for the searched dom node
    let searchSelector = null;

    if (searchedNode) { return searchedNode; }

    // Handling headers and footers, where the data object is not used
    if (isDrawingInsideMarginalDrawingLayerNode(node)) {
        marginalRoot = $(node).parent().parent(); // this is a drawing in the text drawing layer
        searchSelector = DRAWINGPLACEHOLDER_NODE_SELECTOR;
    // } else if (DOM.isMarginalNode($(node).parent())) {
    } else if (isParagraphNode($(node).parent())) {
        marginalRoot = getClosestMarginalTargetNode(node); // this is a placeholder node
        searchSelector = ABSOLUTE_DRAWING_SELECTOR;
    }

    if (marginalRoot && marginalRoot.length > 0) {
        drawingID = $(node).attr('data-drawingID');
        selectorString = '[data-drawingID=' + drawingID + ']';
        // finding the place holder and optionally the space maker node, that are located inside the page content node
        foundNodes = $(marginalRoot).find(selectorString);

        if (foundNodes && foundNodes.length > 1) {  // at least drawing in drawing layer, place holder and optionally spacemaker
            searchedNode = _.find(foundNodes, function (node) {
                return $(node).is(searchSelector);
            });

            if (!searchedNode) { globalLogger.warn('Warning: failed to find marginal node with selector: ' + searchSelector); }

        } else {
            globalLogger.warn('Warning: Expected at least two marginal nodes, but found: ' + foundNodes.length);
        }
    } else {
        // outside of header or footer the node must be found from data object
        if (!searchedNode) { globalLogger.warn('Warning: Failed to find place holder node.'); }
    }

    return searchedNode;
}

/**
 * Returns whether the passed node is an element representing a drawing that is
 * absolutely positioned and located inside a paragraph (not in drawing layer).
 * This are the drawings, that are anchored to paragraphs, not to pages.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an absolutely positioned drawing inside a paragraph.
 */
export function isAbsoluteParagraphDrawing(node) {
    return isParagraphNode($(node).parent()) && $(node).is(ABSOLUTE_DRAWING_SELECTOR);
}

/**
 * Returns whether the passed node (typically a paragraph) contains at least one
 * absolutely positioned drawing anchored to the paragraph.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains at least one child representing an absolutely
 *  positioned drawing anchored to a paragraph.
 */
export function hasAbsoluteParagraphDrawing(node) {
    return isParagraphNode(node) && ($(node).has(ABSOLUTE_DRAWING_SELECTOR).length > 0);
}

/**
 * Returns whether the passed node is a paragraph node that starts with a
 * drawing node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a paragraph node starting with a drawing.
 */
export function isParagraphNodeStartingWithDrawing(node) {
    if (!isParagraphNode(node)) { return false; }
    const children = $(node).children();
    const filteredChildren = children.filter(':not(.drawingspacemaker)');
    if (filteredChildren.length < 2) { return false; }
    return isEmptySpan(filteredChildren[0]) && isDrawingFrame(filteredChildren[1]);
}

/**
 * Returns the first text node in a specified paragraph node behind leading absolutely
 * positioned drawing(s) anchored to the paragraph or inline drawings.
 *
 * @param {Node|jQuery|Null} [paraNode]
 *  The paragraph node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Node}
 *  Returns the first text node in a paragraph node behind absolutely positioned
 *  drawing(s) anchored to the paragraph or inline drawings. If there are no drawing
 *  at the beginning of the paragraph, the first text node is returned. If the
 *  specified node is no paragraph, the node is returnred without change.
 */
export function getFirstTextNodeBehindDrawings(paraNode) {
    if (!isParagraphNode(paraNode)) { return paraNode; }
    const children = $(paraNode).children();
    let node = children[0];
    while (node && node.nextSibling && isEmptySpan(node) && isDrawingFrame(node.nextSibling)) { node = node.nextSibling.nextSibling; }
    return node;
}

/**
 * Returns the first text node in a specified paragraph node behind (or before)
 * leading (or trailing) absolutely positioned drawing(s) anchored to the paragraph.
 *
 * @param {Node|jQuery|Null} [paraNode]
 *  The paragraph node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.before=false]
 *      If set to true, the final text node before the the trailing absolutely
 *      positioned drawing(s) is searched.
 *
 * @returns {Node}
 *  Returns the first (or last) text node in a paragraph node behind (or before)
 *  absolutely positioned drawing(s) anchored to the paragraph. If there are no
 *  drawings at the beginning of (or at the end of) the paragraph, the first text
 *  (or last) node is returned. If the specified node is no paragraph, the node is
 *  returned without change.
 */
export function getTextSpanBeforeOrAfterAbsoluteParagraphDrawings(paraNode, options) {
    if (!isParagraphNode(paraNode)) { return paraNode; }
    const children = $(paraNode).children();
    let node = options?.before ? children[children.length - 1] : children[0];
    const searchFunc = options?.before ? "previousSibling" : "nextSibling";
    while (isEmptySpan(node) && isAbsoluteParagraphDrawing(node[searchFunc])) { node = node[searchFunc][searchFunc]; }
    return node;
}

/**
 * Returns whether the passed node is a text span, that has as previous sibling
 * inside its paragraph at least one drawing with text frame.
 *
 * @param {Node|jQuery|Null} [checkNode]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text span, that has at least one drawing with
 *  text frame as previous sibling inside its paragraph.
 */
export function isTextNodeBehindDrawingWithTextframe(checkNode) {

    let node = getDomNode(checkNode);
    let isTextNodeBehindTextFrameDrawing = false;
    let doContinue = true;

    if (!isTextSpan(node)) { return false; }
    if (!node.previousSibling) { return false; }

    while (doContinue && node && node.previousSibling) {
        if (DrawingFrame.isTextFrameShapeDrawingFrame(node.previousSibling)) {
            isTextNodeBehindTextFrameDrawing = true;
            doContinue = false;
        } else {
            node = node.previousSibling;
        }
    }

    return isTextNodeBehindTextFrameDrawing;
}

/**
 * Returns whether the passed node is a text span, that comes into its
 * own paragraph behind a hardbreak node at the beginning of the paragraph.
 *
 * @param {Node|jQuery|Null} paragraph
 *  The paragraph node to be checked.
 *
 * @param {Node|jQuery|Null} [checkNode]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a text span, that comes in its own paragraph
 *  behind a hard break node at the beginning of the pargraph.
 */
export function isTextNodeBehindHardbreakAtParaStart(paragraph, checkNode) {
    const node = getDomNode(checkNode);
    if (!isTextSpan(node)) { return false; }
    if (!node.previousSibling) { return false; }
    return isParagraphNodeStartingWithHardbreak(paragraph);
}

/**
 * The CSS class used to mark drawing space maker nodes.x
 */
export const DRAWING_SPACEMAKER_CLASS = 'drawingspacemaker';

/**
 * The data property name for registering the drawing space maker at the absolutely
 * positioned drawing.
 */
export const DRAWING_SPACEMAKER_LINK = 'drawingspacemakerlink';

/**
 * A jQuery selector that matches elements representing a drawing space maker node.
 */
export const DRAWING_SPACEMAKER_NODE_SELECTOR = 'div.' + DRAWING_SPACEMAKER_CLASS;

/**
 * The data property name for registering the increase of height in pixel into a footer
 * node, after inserting the drawing space maker node.
 */
export const DRAWING_SPACEMAKER_SHIFT = 'drawingSpaceMakerShift';

/**
 * Returns whether the passed node is an element representing a drawing space
 * maker node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing a drawing space maker
 *  node.
 */
export function isDrawingSpaceMakerNode(node) {
    return $(node).hasClass(DRAWING_SPACEMAKER_CLASS);
}

/**
 * Searching the space maker node for a specific drawing node. This space
 * maker node exists in the page content for all drawings inside the
 * drawing layer node, that require space 'below' the drawing.
 *
 * @param {Node|jQuery|Null} [node]
 *  The drawing node.
 *
 * @returns {Node}
 *  The space maker node for the drawing node in the drawing layer node.
 */
export function getDrawingSpaceMakerNode(node) {

    // the node saved in the data object
    let searchedNode = $(node).data(DRAWING_SPACEMAKER_LINK);
    // an optional header or footer node
    let marginalRoot = null;
    // the ID of the drawing in the drawing layer
    let drawingID = 0;
    // a string to find the related placeholder node
    let selectorString = null;
    // the nodes with selector string inside header or footer
    let foundNodes = null;
    // the number of nodes that need to be found
    let minValue = 2;

    if (searchedNode) { return searchedNode; }

    // Handling headers and footers, where the data object is not used
    if (isDrawingInsideMarginalDrawingLayerNode(node)) {
        marginalRoot = $(node).parent().parent(); // this is a drawing in the text drawing layer in header or footer
    }

    // also handling absolute paragraph drawings
    if (!marginalRoot && isAbsoluteParagraphDrawing(node) && isMarginalNode($(node).parent())) {
        marginalRoot = getClosestMarginalTargetNode(node);
        minValue = 1; // only drawing and space maker, no place holder
    }

    if (marginalRoot && marginalRoot.length > 0) {
        drawingID = $(node).attr('data-drawingID');
        selectorString = '[data-drawingID=' + drawingID + ']';
        // finding the optional space maker node
        foundNodes = $(marginalRoot).find(selectorString);

        if (foundNodes && foundNodes.length > minValue) {  // at least drawing in drawing layer, place holder and optionally spacemaker
            searchedNode = _.find(foundNodes, function (node) {
                return isDrawingSpaceMakerNode(node);
            });
        }
    }

    return searchedNode;
}

/**
 * Finding a drawing node inside a specified root node by its drawing ID.
 *
 * @param {jQuery} rootNode
 *  The jQuerified root node in which the drawing is searched.
 *
 * @param {string} drawingId
 *  The drawing ID.
 *
 * @returns {jQuery}
 *  The jQuery collection, that contains 0 or more found drawings.
 */
export function findDrawingNodeById(rootNode, drawingId) {
    return rootNode.find('.drawing[data-drawingID=' + drawingId + ']');
}

/**
 * Returns whether the passed node (a paragraph) has at least one child
 * representing a drawing space maker node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node has at least one child representing a drawing
 *  space maker node.
 */
export function hasDrawingSpaceMakerNode(node) {
    return $(node).children(DRAWING_SPACEMAKER_NODE_SELECTOR).length > 0;
}

/**
 * The CSS class used to mark the drawing layer node.
 */
export const DRAWINGLAYER_CLASS = 'textdrawinglayer';

/**
 * A jQuery selector that matches the drawing layer node.
 */
export const DRAWINGLAYER_NODE_SELECTOR = 'div.' + DRAWINGLAYER_CLASS;

/**
 * Returns whether the passed node is the drawing layer node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is the drawing layer node.
 */
export function isDrawingLayerNode(node) {
    return $(node).hasClass(DRAWINGLAYER_CLASS);
}

/**
 * Returns whether the passed node is a drawing layer node inside a header
 * or footer.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing layer node inside header or footer.
 */
export function isMarginalDrawingLayerNode(node) {
    return isDrawingLayerNode(node) && isHeaderOrFooter($(node).parent());
}

/**
 * Returns the container node of absolute positioned drawings.
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  The container DOM node for all absolute positioned drawings.
 */
export function getTextDrawingLayerNode(pageNode) {
    return $(pageNode).children(DRAWINGLAYER_NODE_SELECTOR);
}

/**
 * Returns whether the passed node is inside the drawing layer node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is located inside the drawing layer node.
 */
export function isInsideDrawingLayerNode(node) {
    return $(node).parents(DRAWINGLAYER_NODE_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is a drawing inside the drawing layer node
 * inside a header or footer.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a drawing node inside the drawing layer node
 *  inside header or footer.
 */
export function isDrawingInsideMarginalDrawingLayerNode(node) {
    return isMarginalDrawingLayerNode($(node).parent());
}

/**
 * Returns the drawing node that is directly located below the drawing
 * layer. The specified node needs to be located inside this drawing.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Returns the top level drawing inside the drawing layer.
 */
export function getTopLevelDrawingInDrawingLayerNode(node) {
    return $(node).parentsUntil(DRAWINGLAYER_NODE_SELECTOR, DrawingFrame.NODE_SELECTOR).last();
}

// manipulate and iterate text spans --------------------------------------

/**
 * Splits the passed text span element into two text span elements. Clones
 * all formatting to the new span element.
 *
 * @param {HTMLSpanElement|jQuery} span
 *  The text span to be split. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @param {Number} offset
 *  The character position where the text span will be split. If this
 *  position is at the start or end of the text in the span, an empty text
 *  span will be inserted.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.append=false]
 *      If set to true, the right part of the text will be inserted after
 *      the passed text span; otherwise the left part of the text will be
 *      inserted before the passed text span. The position of the new text
 *      span may be important when iterating and manipulating a range of
 *      DOM nodes.
 *
 * @returns {jQuery}
 *  The newly created text span element, as jQuery object. Will be located
 *  before or after the passed text span, depending on the 'options.append'
 *  option.
 */
export function splitTextSpan(span, offset, options) {

    // the new span for the split text portion, as jQuery object
    const newSpan = $(span).clone(true);
    // the existing text node (must not be invalidated, e.g. by using jQuery.text())
    const textNode = getDomNode(span).firstChild;
    // text for the left span
    const leftText = textNode.nodeValue.substr(0, offset);
    // text for the right span
    const rightText = textNode.nodeValue.substr(offset);

    // insert the span and update the text nodes
    if (getBooleanOption(options, 'append', false)) {
        newSpan.insertAfter(span);
        textNode.nodeValue = leftText;
        newSpan.text(rightText);
        if (!rightText) { ensureExistingTextNode(newSpan); }
    } else {
        newSpan.insertBefore(span);
        newSpan.text(leftText);
        if (!leftText) { ensureExistingTextNode(newSpan); }
        textNode.nodeValue = rightText;
    }

    // return the new text span
    return newSpan;
}

/**
 * Calls the passed iterator function for all descendant text span elements
 * in a the passed node. As a special case, if the passed node is a text
 * span by itself, it will be visited directly. Text spans can be direct
 * children of a paragraph node (regular editable text portions), or
 * children of other nodes such as text fields or list label nodes.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM node whose descendant text spans will be visited (or which will
 *  be visited by itself if it is a text span). If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param {Function} iterator
 *  The iterator function that will be called for every text span. Receives
 *  the DOM span element as first parameter. If the iterator returns the
 *  Utils.BREAK object, the iteration process will be stopped immediately.
 *
 * @param {Object} [context]
 *  If specified, the iterator will be called with this context (the symbol
 *  'this' will be bound to the context inside the iterator function).
 *
 * @returns {Utils.BREAK | void}
 *  A reference to the Utils.BREAK object, if the iterator has returned
 *  Utils.BREAK to stop the iteration process, otherwise undefined.
 */
export function iterateTextSpans(node, iterator, context) {

    // visit passed text span directly
    if (isTextSpan(node)) {
        return iterator.call(context, getDomNode(node));
    }

    // do not iterate into drawings, they may contain their own paragraphs
    // Bug 30794: also omit list-label nodes as they are especially handled
    // in updateList function in the editor.
    if (!DrawingFrame.isDrawingFrame(node) && !isListLabelNode(node)) {
        return iterateSelectedDescendantNodes(node, function () { return isTextSpan(this); }, iterator, context);
    }
}

// generic container and content nodes ====================================

/**
 * A jQuery selector that matches elements representing a top-level content
 * node (e.g. paragraphs or tables).
 */
export const CONTENT_NODE_SELECTOR = PARAGRAPH_NODE_SELECTOR + ', ' + TABLE_NODE_SELECTOR;

/**
 * Returns whether the passed node is a top-level content node (e.g.
 * paragraphs or tables).
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a top-level content node.
 */
export function isContentNode(node) {
    return $(node).is(CONTENT_NODE_SELECTOR);
}

/**
 * Returns the correct node of a container node that contains its content
 * nodes as direct children. Container nodes include the document page,
 * table cells, or drawing objects containing text.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {jQuery}
 *  The element that contains the child components of the passed nodes as
 *  direct children, as jQuery object.
 */
export function getChildContainerNode(node) {

    // convert to a jQuery object
    node = $(node);

    // page nodes
    if (isPageNode(node)) {
        return getPageContentNode(node);
    }

    // table cell nodes
    if (node.is('td')) {
        return getCellContentNode(node);
    }

    // drawing nodes
    if (DrawingFrame.isDrawingFrame(node)) {

        if (DrawingFrame.isTextFrameShapeDrawingFrame(node)) {
            return DrawingFrame.getTextFrameNode(node);
        } else {
            return DrawingFrame.getContentNode(node);
        }

    }

    // header/footer node - it doesn't have content node, paragraphs are direct children!
    if (isHeaderOrFooter(node)) {
        return getMarginalContentNode(node);
    }

    return $();
}

/**
 * Finding the allowed neighboring node taking into account all nodes
 * that are used for calculating the logical positions. For example as
 * top level nodes only paragraphs and tables are allowed, page-break
 * nodes need to be skipped.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @param {Object} options
 *  A map with additional options controlling the search of the neighbor.
 *  The following options are supported:
 *  @param {Boolean} [options.next=true]
 *      The search direction. If not specified, the next neighboring node
 *      will be searched.
 *
 * @returns {Node|null}
 *  The search element of the specified node. If it cannot be determined
 *  null is returned.
 */
export function getAllowedNeighboringNode(node, options) {

    // determining the direction for searching the neighbor
    const next = getBooleanOption(options, 'next', true);
    // the dom node of the specified node
    const domNode = getDomNode(node);
    // the iterator function
    const iterator = next ? 'nextSibling' : 'previousSibling';
    // the neighboring node
    let neighbor = domNode[iterator] || null;

    while (neighbor && isInvalidNode(neighbor)) {
        neighbor = neighbor[iterator];
    }

    return neighbor;
}

/**
 * Checking, whether a specified node is part of the calculation of
 * logical positions.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the specified node is part of the calculation of
 *  logical positions.
 */
export function isInvalidNode(node) {
    return isPageBreakNode(node) || isListLabelNode(node) || isCommentPlaceHolderNode(node);  // TODO: List needs to be expanded
}

/**
 * Creates a list of allowed DOM nodes (paragraphs and tables currently),
 * and ignores pagebreaks and clearfloating divs
 *
 * @param {HTMLElement|jQuery} childNodes
 *  List of nodes that we want to filter, to contain only allowed ones, such as top level nodes paragraphs and tables
 *
 * @returns {HTMLElement|jQuery}
 *  list of nodes after filtering
 */
export function allowedChildNodes(childNodes) {
    return $(childNodes).filter(PARAGRAPH_NODE_SELECTOR + ',' + TABLE_NODE_SELECTOR);
}

/**
 * Creates a new pagebreak replacement node.
 *
 * @returns {jQuery}
 *  A pagebreak replacement node, as jQuery object.
 */
export function createPagebreakReplacementNode() {
    return $('<div class="pagebreakreplacementnode">');
}

/**
 * jQuery selector for skipping drawing, repeated rows and page break nodes.
 *
 */
export const SKIP_DRAWINGS_AND_P_BREAKS_SELECTOR = '.drawing, .page-break, .pb-row';

// page break nodes ========================================================

/**
 * jQuery selector that matches page break div, used to separate pages in page layout.
 */
export const PAGE_BREAK_SELECTOR = '.page-break';

/**
 * CSS class name that matches page break div, used to separate pages in page layout.
 */
export const PAGE_BREAK_CLASS_NAME = 'page-break';

/**
 * jQuery selector that matches manualy inserted page break by user
 */
export const MANUAL_PAGE_BREAK_SELECTOR = '.manual-page-break';

/**
 * CSS class name that matches manualy inserted page break by user
 */
export const MANUAL_PAGE_BREAK_CLASS_NAME = 'manual-page-break';

/**
 * jQuery selector that matches inner line in page break that marks begining of new page
 */
export const PAGE_BREAK_LINE_SELECTOR = '.inner-pb-line';

/**
 * CSS class name that matches line inside page break, that marks begining of new page
 */
export const PAGE_BREAK_LINE_CLASS_NAME = 'inner-pb-line';

/**
 * jQuery selector name that matches hardbreak with type page, from Microsoft only
 */
export const MS_HARDBREAK_TYPE_PAGE_SELECTOR = '.ms-hardbreak-page';

/**
 * CSS class name that matches hardbreak with type page, from Microsoft only
 */
export const MS_HARDBREAK_TYPE_PAGE_CLASS_NAME = 'ms-hardbreak-page';

/**
 * CSS class name that matches element which contains (is split by) page break.
 */
export const CONTAINS_PAGEBREAK_CLASS_NAME = 'contains-pagebreak';

/**
 * Returns whether the passed node is a page break element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a page break element.
 */
export function isPageBreakNode(node) {
    return $(node).hasClass(PAGE_BREAK_CLASS_NAME);
}

/**
 * Returns whether the passed node is a page break element inserted by user.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a manual page break element.
 */
export function isManualPageBreakNode(node) {
    return $(node).hasClass(MANUAL_PAGE_BREAK_CLASS_NAME);
}

/**
 * Returns whether the passed node is a hardbreak element with type page,
 * inserted in MS Word.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a MS Word page hardbreak element.
 */
export function isMSPageHardbreakNode(node) {
    return $(node).hasClass(MS_HARDBREAK_TYPE_PAGE_CLASS_NAME);
}

/**
 * Returns whether the passed node contains a hardbreak element with type page,
 * inserted in MS Word.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains at least one MS Word page hardbreak element.
 */
export function hasMSPageHardbreakNode(node) {
    return $(node).find(MS_HARDBREAK_TYPE_PAGE_SELECTOR).length > 0;
}

/**
 * Returns whether the passed node is a table element that contains a page break
 * in its first paragraph in the upper left cell.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table node with manual page break element.
 */
export function isTableWithManualPageBreak(node) {
    return isTableNode(node) && $(node).find('.p').first().hasClass(MANUAL_PAGE_BREAK_CLASS_NAME);
}

/**
 * Returns whether node contains manual page break after element attribute,
 * set as a class name.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a table node with manual page break after element.
 */
export function isNodeWithPageBreakAfterAttribute(node) {
    return $(node).hasClass('manual-pb-after');
}

/**
 * Returns whether node contains page break inside itself.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains page break, or not.
 */
export function nodeContainsPageBreak(node) {
    return $(node).hasClass(CONTAINS_PAGEBREAK_CLASS_NAME);
}

// header/footer ==========================================================

export const HEADER_FOOTER_PLACEHOLDER_SELECTOR = '.header-footer-placeholder';

export const HEADER_FOOTER_PLACEHOLDER_CLASSNAME = 'header-footer-placeholder';

export const HEADER_WRAPPER_SELECTOR = '.header-wrapper';

export const HEADER_WRAPPER_CLASSNAME = 'header-wrapper';

export const FOOTER_WRAPPER_SELECTOR = '.footer-wrapper';

export const FOOTER_WRAPPER_CLASSNAME = 'footer-wrapper';

export const MARGINAL_NODE_SELECTOR = '.marginal';

export const MARGINAL_NODE_CLASSNAME = 'marginal';

export const MARGINALCONTENT_NODE_SELECTOR = '.marginalcontent';

export const MARGINALCONTENT_NODE_CLASSNAME = 'marginalcontent';

/**
 * The class name used to specify header nodes.
 */
export const HEADER_CLASSNAME = 'header';

/**
 * The selector used to specify header nodes.
 */
export const HEADER_SELECTOR = '.' + HEADER_CLASSNAME;

/**
 * The class name used to specify footer nodes.
 */
export const FOOTER_CLASSNAME = 'footer';

/**
 * The selector used to specify footer nodes.
 */
export const FOOTER_SELECTOR = '.' + FOOTER_CLASSNAME;

/**
 * Selector used to specify content nodes that are direct children of page node.
 *
 */
export const PAGE_CHILDREN_CONTENT_SELECTOR = HEADER_WRAPPER_SELECTOR + ', ' + PAGECONTENT_NODE_SELECTOR + ', ' + FOOTER_WRAPPER_SELECTOR + ', ' + DRAWINGLAYER_NODE_SELECTOR;

/**
 * Checks if passed node is a header element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a header element.
 */
export function isHeaderNode(node) {
    return $(node).hasClass(HEADER_CLASSNAME);
}

/**
 * Checks if passed node is a footer element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a footer element.
 */
export function isFooterNode(node) {
    return $(node).hasClass(FOOTER_CLASSNAME);
}

/**
 * Checks if passed node is header or footer element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a header or footer element.
 */
export function isHeaderOrFooter(node) {
    return isHeaderNode(node) || isFooterNode(node);
}

/**
 * Checks if passed node is content node inside header or footer element.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a header or footer marginal content element.
 */
export function isMarginalContentNode(node) {
    return $(node).hasClass(MARGINALCONTENT_NODE_CLASSNAME);
}

/**
 * Returns content node of passed header or footer element.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Node|jQuery}
 *  If found, returns content node of passed header or footer element.
 */
export function getMarginalContentNode(node) {
    return $(node).children(MARGINALCONTENT_NODE_SELECTOR);
}

/**
 * Returns the template folder for header and footer.
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  The container DOM node for the header and footer placeholders.
 */
export function getMarginalTemplateNode(pageNode) {
    return $(pageNode).children(HEADER_FOOTER_PLACEHOLDER_SELECTOR);
}

/**
 * Checks if passed node is a header wrapper element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a header wrapper element.
 */
export function isHeaderWrapper(node) {
    return $(node).hasClass(HEADER_WRAPPER_CLASSNAME);
}

/**
 * Returns the header wrapper node, that is a child of the page node.
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  The header wrapper node as a child of the page node.
 */
export function getHeaderWrapperNode(pageNode) {
    return $(pageNode).children(HEADER_WRAPPER_SELECTOR);
}

/**
 * Checks if passed node is a footer wrapper element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a footer wrapper element.
 */
export function isFooterWrapper(node) {
    return $(node).hasClass(FOOTER_WRAPPER_CLASSNAME);
}

/**
 * Returns the footer wrapper node, that is a child of the page node.
 *
 * @param {HTMLElement|jQuery} pageNode
 *  The page DOM node. If this object is a jQuery collection, uses the
 *  first DOM node it contains.
 *
 * @returns {jQuery}
 *  The footer wrapper node as a child of the page node.
 */
export function getFooterWrapperNode(pageNode) {
    return $(pageNode).children(FOOTER_WRAPPER_SELECTOR);
}

/**
 * Checks if passed node is a header or footer wrapper element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a header or footer wrapper element.
 */
export function isHeaderOrFooterWrapper(node) {
    return isHeaderWrapper(node) || isFooterWrapper(node);
}

/**
 * Returns whether the passed node is an element representing a marginal
 * (header&footer) place holder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing a marginal placeholder
 *  node.
 */
export function isMarginalPlaceHolderNode(node) {
    return $(node).hasClass(HEADER_FOOTER_PLACEHOLDER_CLASSNAME);
}

/**
 * Checks if passed node is positioned relative.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is 'relative' positioned.
 */
export function isRelativePositionedNode(node) {
    return $(node).css('position') === 'relative';
}

/**
 * Returning the header or footer node that is wrapping a specified node.
 *
 * @param {Node|jQuery} rootNode
 *  The root node until which the search of header or footer is executed.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 *  @param {Object} [options]
 *  @param {Boolean} [options.footer=false]
 *    If set to true, the specified node is searched in the footer.
 *  @param {Boolean} [options.header=false]
 *    If set to true, the specified node is searched in the header.
 *
 * @returns {jQuery}
 *  A jQuery element, that contains the header or footer node or no node
 *  if the specified node is not inside a header or footer.
 */
export function getMarginalTargetNode(rootNode, node, options) {

    // finding the current node inside a footer
    const findFooter = getBooleanOption(options, 'footer', false);
    // finding the current node inside a footer
    const findHeader = getBooleanOption(options, 'header', false);
    // the selector to find the marginal node
    const marginSelector = findFooter ? 'div.footer' : (findHeader ? 'div.header' : 'div.footer, div.header');

    return $(node).parentsUntil(rootNode, marginSelector);
}

/**
 * Returning the closest header or footer node that is wrapping a specified node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @param {object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.footer=false]
 *    If set to true, the specified node is searched in the footer.
 *  @param {Boolean} [options.header=false]
 *    If set to true, the specified node is searched in the header.
 *
 * @returns {jQuery}
 *  A jQuery element, that contains the closest header or footer node or no
 *  node, if the specified node is not inside a header or footer.
 */
export function getClosestMarginalTargetNode(node, options) {

    // finding the current node inside a footer
    const findFooter = getBooleanOption(options, 'footer', false);
    // finding the current node inside a footer
    const findHeader = getBooleanOption(options, 'header', false);
    // the selector to find the marginal node
    const marginSelector = findFooter ? 'div.footer' : (findHeader ? 'div.header' : 'div.footer, div.header');

    return $(node).closest(marginSelector);
}

/**
 * Returning whether a specified node is inside the header/footer
 * and has the class 'marginal'.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the specified node is located inside header/footer (because
 *  it has class 'marginal'.
 */
export function isMarginalNode(node) {
    return $(node).hasClass(MARGINAL_NODE_CLASSNAME);
}

/**
 * Returning whether specified node is inside the marginal context menu.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked.
 *
 * @returns {Boolean}
 *   Whether specified node is inside the marginal context menu.
 *
 */
export function isInsideMarginalContextMenu(node) {
    return $(node).closest('.marginal-context-menu').length > 0;
}

/**
 * Returning whether a specified node is inside the header/footer
 * template folder (div.header-footer-placeholder).
 *
 * @param {Node|jQuery} rootNode
 *  The root node until which the search of header or footer is executed.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {jQuery}
 *  Whether the specified node is located inside header/footer
 *  template folder.
 */
export function isInsideHeaderFooterTemplateNode(rootNode, node) {
    return $(node).parentsUntil(rootNode, HEADER_FOOTER_PLACEHOLDER_SELECTOR).length > 0;
}

/**
 * Getter for the current target ID of a specified node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The target of the current active root node as string. Returns an empty
 *  string, if there is no target attribute set.
 */
export function getTargetContainerId(node) {
    return $(node).attr('data-container-id') || '';
}

/**
 * setter for the current target ID of a specified node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to which the container id will be assigned.
 *
 * @param {String} id
 *  The container id, that will be assigned.
 */
export function setTargetContainerId(node, id) {
    $(node).attr('data-container-id', id);
}

/**
 * Checks whether node of its type is first occurance in document.
 * Compares passed node with first in document with same target, and checks equality.
 *
 * @param {Node|jQuery} rootNode
 *  Parent root node of passed node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @param {String} [target]
 *  The target of the passed node as string. If missing calculate one from data attributes.
 *
 * @returns {Boolean}
 *  Whether the node is first occurance in document of its type.
 */
export function isFirstMarginalInDocument(rootNode, node, target) {

    target = target || getTargetContainerId(node);
    const $firstNode = rootNode.find('[data-container-id="' + target + '"]').first();

    return $(node)[0] === $firstNode[0];
}

/**
 * Getter the container id of the container node for a specified node located
 * inside a container. If it is not inside a container, null is returned.
 *
 * @param {Node|jQuery} rootNode
 *  The root node until which the search of header or footer is executed.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The target id of the container node as string. Returns null, if no container
 *  node is found.
 */
export function getNodeContainerId(rootNode, node) {

    // the header or footer node, in which this node is located
    const containerNode = getMarginalTargetNode(rootNode, node);

    return (containerNode.length > 0) ? getTargetContainerId(containerNode[0]) : null;
}

// comments ==================================================

/**
 * The CSS class used to mark comment placeholder nodes. This place holder
 * nodes are located in the page content node and safe the position for
 * a comment in the comment layer.
 */
export const COMMENTPLACEHOLDER_CLASS = 'commentplaceholder';

/**
 * The CSS class used to mark comment bubble nodes.
 */
export const COMMENTBUBBLENODE_CLASS = 'commentbubble';

/**
 * The CSS class used to mark presentation comment bubble nodes.
 */
export const PRESENTATION_COMMENTBUBBLENODE_CLASS = 'comment-bubble';

/**
 * A jQuery selector that matches nodes representing a presentation comment bubble.
 */
export const  PRESENTATION_COMMENTBUBBLENODE_SELECTOR = '.' + PRESENTATION_COMMENTBUBBLENODE_CLASS;

export function getPresentationCommentBubbleRootNode(node) {
    return $(node).parent(PRESENTATION_COMMENTBUBBLENODE_SELECTOR)[0]; // {HTMLELementNode|undefined}
}

/**
 * Returns whether the passed node is a presentation comment bubble element.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a presentation comment bubble element.
 */
export function isPresentationCommentBubbleTarget(node) {
    return ($(node).is(PRESENTATION_COMMENTBUBBLENODE_SELECTOR) || !!getPresentationCommentBubbleRootNode(node));
}

/**
 * The CSS class used to mark the page node, if it has a margin for the comment node.
 */
export const COMMENTMARGIN_CLASS = 'commentmargin';

/**
 * A jQuery selector that matches nodes representing a comment bubble node.
 */
export const COMMENTBUBBLENODE_SELECTOR = '.' + COMMENTBUBBLENODE_CLASS;

/**
 * A jQuery selector that matches elements representing a comment place holder.
 */
export const COMMENTPLACEHOLDER_NODE_SELECTOR = 'div.' + COMMENTPLACEHOLDER_CLASS;

/**
 * The CSS class used to mark a comment filter group node.
 */
export const COMMENTFILTERGROUP_CLASS = 'commentfiltergroup';

/**
 * A jQuery selector that matches nodes representing a comment filter group node.
 */
export const COMMENTFILTERGROUP_SELECTOR = '.' + COMMENTFILTERGROUP_CLASS;

/**
 * Returns whether the passed node is an element representing a comment
 * place holder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing a comment placeholder
 *  node.
 */
export function isCommentPlaceHolderNode(node) {
    return $(node).hasClass(COMMENTPLACEHOLDER_CLASS);
}

/**
 * Returns whether the passed node is an element representing a comment
 * place holder node or a drawing place holder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element representing a comment placeholder
 *  node or a drawing place holder node.
 */
export function isSubstitutionPlaceHolderNode(node) {
    return isCommentPlaceHolderNode(node) || isDrawingPlaceHolderNode(node);
}

/**
 * Returns whether the passed node (a paragraph) has at least one child
 * that is a comment place holder node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node has at least one child representing a comment
 *  place holder node.
 */
export function hasCommentPlaceHolderNode(node) {
    return $(node).children(COMMENTPLACEHOLDER_NODE_SELECTOR).length > 0;
}

/**
 * The CSS class used to mark the comment editor node.
 */
export const COMMENT_EDITOR_CLASS = 'commentEditor';

/**
 * A jQuery selector that matches nodes representing a comment editor node.
 */
export const COMMENT_EDITOR_SELECTOR = '.' + COMMENT_EDITOR_CLASS;

// range markers ==================================================

/**
 * The CSS class used to mark range marker nodes.
 */
export const RANGEMARKERNODE_CLASS = 'rangemarker';

/**
 * A jQuery selector that matches nodes representing a range marker node.
 */
export const RANGEMARKERNODE_SELECTOR = '.' + RANGEMARKERNODE_CLASS;

/**
 * The CSS class used to mark range marker nodes at the beginning of the range.
 */
export const RANGEMARKER_STARTTYPE_CLASS = 'rangestart';

/**
 * A jQuery selector that matches nodes representing a range marker start node.
 */
export const RANGEMARKER_STARTTYPE_SELECTOR = '.' + RANGEMARKER_STARTTYPE_CLASS;

/**
 * The CSS class used to mark range marker nodes at the end of the range.
 */
export const RANGEMARKER_ENDTYPE_CLASS = 'rangeend';

/**
 * A jQuery selector that matches nodes representing a range marker end node.
 */
export const RANGEMARKER_ENDTYPE_SELECTOR = '.' + RANGEMARKER_ENDTYPE_CLASS;

/**
 * The CSS class used to mark the range overlay node.
 */
export const RANGEMARKEROVERLAYNODE_CLASS = 'rangemarkeroverlay';

/**
 * Returns whether the passed node is a range marker node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a range marker node.
 */
export function isRangeMarkerNode(node) {
    return $(node).hasClass(RANGEMARKERNODE_CLASS);
}

/**
 * Returns whether the passed node is a range marker start node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a range marker start node.
 */
export function isRangeMarkerStartNode(node) {
    return isRangeMarkerNode(node) && $(node).hasClass(RANGEMARKER_STARTTYPE_CLASS);
}

/**
 * Returns whether the passed node is a range marker end node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a range marker end node.
 */
export function isRangeMarkerEndNode(node) {
    return isRangeMarkerNode(node) && $(node).hasClass(RANGEMARKER_ENDTYPE_CLASS);
}

/**
 * Returns whether the passed node contains at least one range marker node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains a range marker node.
 */
export function hasRangeMarkerNode(node) {
    return $(node).has(RANGEMARKERNODE_SELECTOR).length > 0;
}

/**
 * Getter for the type a specified range node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The type of the specified range node. Returns an empty
 *  string, if there is no type specified.
 */
export function getRangeMarkerType(node) {
    return $(node).attr('data-range-type') || '';
}

/**
 * Getter for the current unique ID of a specified range node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The unique id of the specified range node. Returns an empty
 *  string, if there is no unique id.
 */
export function getRangeMarkerId(node) {
    return $(node).attr('data-range-id') || '';
}

/**
 * Returns whether the passed node is a range marker node of type 'comment'.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is a range marker node of type comment.
 */
export function isCommentRangeMarkerNode(node) {
    return isRangeMarkerNode(node) && getRangeMarkerType() === 'comment';
}

/**
 * Checks if the passed node contains a range marker node with checkBox field character attribute.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains a range marker node with checkBox field character attribute.
 */
export function isRangeStartWithCheckboxAttr(node) {
    const characterAttrs = node && getExplicitAttributes(node, 'character', true);
    const fieldAttrs = characterAttrs && characterAttrs.field;

    return isRangeMarkerStartNode(node) && fieldAttrs && fieldAttrs.formFieldType === 'checkBox';
}

/**
 * Checks if the passed node contains a range marker node with checkBox field character attribute,
 * and checkbox has state enabled (checked).
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains a range marker node with checkBox field character attribute and checked is true.
 */
export function isRangeStartWithCheckboxEnabledState(node) {
    const characterAttrs = node && getExplicitAttributes(node, 'character', true);
    const fieldAttrs = characterAttrs && characterAttrs.field;

    return isRangeMarkerStartNode(node) && fieldAttrs && fieldAttrs.formFieldType === 'checkBox' && fieldAttrs.checked === true;
}

/**
 * Returns whether the passed node with the passed offset is the last
 * text position in front of an range marker end node.
 *
 * @param {Node|Null} node
 *  The DOM node to be checked. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node with the passed offset is the last text
 *  position before a range marker end node.
 */
export function isLastTextPositionBeforeRangeEnd(node, offset) {
    return node.nodeType === 3 && (node.length === offset) && node.parentNode.nextSibling && isRangeMarkerEndNode(node.parentNode.nextSibling);
}

/**
 * Returns whether the passed node with the passed offset is the first
 * text position behind a range marker start node or behind a complex field
 * node.
 *
 * TODO: Handling for comments in ODT, where the range marker start node is the comment node itself
 *
 * @param {Node|Null} node
 *  The DOM node to be checked. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node with the passed offset is the first text
 *  position after a range marker start node or a complex field node.
 */
export function isFirstTextPositionBehindRangeStart(node, offset) {
    return (offset === 0) && (node.nodeType === 3) && node.parentNode.previousSibling && (isRangeMarkerStartNode(node.parentNode.previousSibling) || isComplexFieldNode(node.parentNode.previousSibling));
}

// simple fields ===================================================

/**
 * Getter for the instruction of a specified simple field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The instruction for the specified simple field node. Returns an empty
 *  string, if there is no instruction.
 */
export function getFieldInstruction(node) {
    return $(node).data('type') || '';
}

/**
 * Getter for the date/time format of a specified simple field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The format for the specified simple field node. Returns an empty
 *  string, if there is no format.
 */
export function getFieldDateTimeFormat(node) {
    return $(node).data('dateFormat') || '';
}

/**
 * Getter for the PAGE NUMBER and PAGE COUNT format of a specified simple field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The format for the specified simple field node. Returns an empty
 *  string, if there is no format.
 */
export function getFieldPageFormat(node) {
    return $(node).data('pageNumFormat') || '';
}

/**
 * Checks if passed field node has fixed property,
 * that prevents automatic date value update.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  If the field has fixed date property or not
 */
export function isAutomaticDateField(node) {
    return $(node).attr('data-auto-date') === 'true';
}

/**
 * Checks if passed ODF or simple ooxml field node has fixed property,
 * that prevents automatic date value update.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @param {Boolean} isODF
 *  If document format is odf, or ooxml.
 *
 * @returns {Boolean}
 *  If the field has fixed date property or not
 */
export function isFixedSimpleField(node, isODF) {
    let isFixed = false;
    let nodeAttrs;

    if (isODF) {
        nodeAttrs = (node && is.dict($(node).data().attributes)) ? $(node).data().attributes : false;
        isFixed = $(node).data('text:fixed') === 'true' || (nodeAttrs && nodeAttrs.character && nodeAttrs.character.field && nodeAttrs.character.field['text:fixed'] === 'true');
    } else {
        isFixed = !isAutomaticDateField(node);
    }

    return isFixed;
}

/**
 * Getter for the current unique ID of a specified simple field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The unique id of the specified simple field node. Returns an empty
 *  string, if there is no unique id.
 */
export function getSimpleFieldId(node) {
    return $(node).data('sFieldId') || '';
}

/**
 * Getter for the current unique ID of a specified Slide field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The unique id of the specified simple field node. Returns an empty
 *  string, if there is no unique id.
 */
export function getSlideFieldId(node) {
    return $(node).attr('data-fid') || '';
}

/**
 * Returns whether the passed node contains at least one simple field node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains at least one simple field node.
 */
export function hasSimpleFieldNode(node) {
    return $(node).has(FIELD_NODE_SELECTOR).length > 0;
}

// complex fields ==================================================

/**
 * The CSS class used to mark complex field nodes.
 */
export const COMPLEXFIELDNODE_CLASS = 'complexfield';

/**
 * The CSS class used to mark all nodes that are between a range start node
 * for a complex field and a range end node for a complex field.
 */
export const COMPLEXFIELDMEMBERNODE_CLASS = 'complex-field';

/**
 * A jQuery selector that matches nodes representing a complex field node.
 */
export const COMPLEXFIELDNODE_SELECTOR = '.' + COMPLEXFIELDNODE_CLASS;

/**
 * A jQuery selector that matches nodes that are located between a start range
 * node and an end range node of a complex field.
 */
export const COMPLEXFIELDMEMBERNODE_SELECTOR = '.' + COMPLEXFIELDMEMBERNODE_CLASS;

/**
 * Returns whether the passed node is a complex field node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a complex field node.
 */
export function isComplexFieldNode(node) {
    return $(node).hasClass(COMPLEXFIELDNODE_CLASS);
}

/**
 * Returns whether the passed node is inside a complex field range.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is inside a complex field range.
 */
export function isInsideComplexFieldRange(node) {
    return $(node).hasClass(COMPLEXFIELDMEMBERNODE_CLASS);
}

/**
 * Returns whether the passed node is inside a complex field range
 * of type 'PLACEHOLDER'.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is inside a placeholder complex field range.
 */
export function isInsidePlaceHolderComplexField(node) {

    // the complex field node
    let complexFieldNode = null;
    // the instruction for the complex field
    let fieldInstruction = null;

    if (isInsideComplexFieldRange(node)) {

        complexFieldNode = getPreviousComplexFieldNode(node);

        if (complexFieldNode) {
            fieldInstruction = getComplexFieldInstruction(complexFieldNode);

            if (fieldInstruction && str.trimAll(fieldInstruction).startsWith('PLACEHOLDER ')) {
                return true;
            }
        }
    }

    return false;
}

/**
 * Returns the complex field node the preceeds the passed node as sibling.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose previous matching sibling will be returned. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @returns {Node|Null}
 *  The previous matching complex field sibling node of the passed node;
 *  or null, if no previous sibling node has been found.
 */
export function getPreviousComplexFieldNode(node) {
    return findPreviousSiblingNode(node, COMPLEXFIELDNODE_SELECTOR);
}

/**
 * Returns whether the passed node contains at least one complex field node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node contains at least one complex field node.
 */
export function hasComplexFieldNode(node) {
    return $(node).has(COMPLEXFIELDNODE_SELECTOR).length > 0;
}

/**
 * Getter for the current unique ID of a specified complex field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The unique id of the specified complex field node. Returns an empty
 *  string, if there is no unique id.
 */
export function getComplexFieldId(node) {
    return $(node).attr('data-field-id') || '';
}

/**
 * Getter for the instruction of a specified complex field node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The instruction for the specified complex field node. Returns an empty
 *  string, if there is no instruction.
 */
export function getComplexFieldInstruction(node) {
    return $(node).data('fieldInstruction') || '';
}

/**
 * Getter for the complex field id of a member node of a complex field range.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {String}
 *  The id of the complex field, that contain the specified node between its
 *  start and its end range. Or an empty string, if the node is not member
 *  of a complex field range.
 */
export function getComplexFieldMemberId(node) {
    return $(node).data('fieldId') || '';
}

// bookmark =======================================================

/**
 * The CSS class used to mark bookmark node.
 */
export const BOOKMARKNODE_CLASS = 'bookmark';

/**
 * A jQuery selector that matches nodes representing a bookmark node.
 */
export const BOOKMARKNODE_SELECTOR = '.' + BOOKMARKNODE_CLASS;

/**
 * Returns whether the passed node is a bookmark node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a bookmark node.
 */
export function isBookmarkNode(node) {
    return $(node).hasClass(BOOKMARKNODE_CLASS);
}

/**
 * Returns id of bookmark.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {String}
 */
export function getBookmarkId(node) {
    return $(node).attr('bmId');
}

/**
 * Returns anchor name of the given bookmark node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {String}
 */
export function getBookmarkAnchor(node) {
    return $(node).attr('anchor');
}

/**
 * Returns position type of given bookmark node. Can be 'start' or 'end'.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {String}
 */
export function getBookmarkPosition(node) {
    return $(node).attr('bmPos');
}

/**
 * Returns whether given bookmark node has position 'start'.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 */
export function isBookmarkStartMarker(node) {
    return getBookmarkPosition(node) === 'start';
}

/**
 * Returns whether given bookmark node has position 'end'.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 */
export function isBookmarkEndMarker(node) {
    return getBookmarkPosition(node) === 'end';
}

// spell checker ==================================================

/**
 * The CSS class used to mark spell error nodes.
 */
export const SPELLERRORNODE_CLASS = 'spellerror';

/**
 * A jQuery selector that matches nodes representing a spell error node.
 */
export const SPELLERRORNODE_SELECTOR = '.' + SPELLERRORNODE_CLASS;

/**
 * Returns whether the passed node is a spell error node.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is a spell error node.
 */
export function isSpellerrorNode(node) {
    return $(node).is(SPELLERRORNODE_SELECTOR);
}

// misc =================================================================

/**
 * Returns, if the specified node requires that an artifical cursor is drawn.
 * This happens in Chrome for empty text spans at the end of the paragraph
 * behind selected nodes (26292).
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether this is a node, behind that an artifical cursor needs to be drawn.
 */
export function isCursorSimulationNode(node) {
    return isInlineComponentNode(node) && (DrawingFrame.isDrawingFrame(node) || isSubstitutionPlaceHolderNode(node) || isRangeMarkerNode(node));
}

/**
 * Returns, whether the specified node has a length of 0 for the calculation
 * of the logical positions.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether this is a node, that does not influence the logical position.
 */
export function isZeroLengthNode(node) {
    return isListLabelNode(node) || isPageBreakNode(node) || isDrawingSpaceMakerNode(node);
}

/**
 * Returns, whether the specified node has a length of 1 for the calculation
 * of the logical positions.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether this is a node, that does has a length of 1 for calculating the logical position.
 */
export function isOneLengthNode(node) {
    return isDrawingFrame(node) || isTabNode(node) || isFieldNode(node) || isSubstitutionPlaceHolderNode(node) || isRangeMarkerNode(node);
}

/**
 * Checking, if the specified point describes a first position inside a text span that directly
 * follows the specified node. So the point is the first position behind the node.
 *
 * @param {DOM.Point} point
 *  The point, that will be checked, if it is directly following the specified node. It needs
 *  the properties 'offset' and 'node'.
 *
 * @param {Node|jQuery} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains.
 *
 * @returns {Boolean}
 *  Whether the point describes the first position behind the node.
 */
export function isFirstTextPositionBehindNode(point, node) {
    return point && point.offset === 0 && point.node.nodeType === 3 && point.node.parentNode && (point.node.parentNode.previousSibling === getDomNode(node));
}

/**
 * Returns, whether the specified node can be included inside a word, without
 * creating a word boundary.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether this is a node, that can be included inside one word, without creating
 *  a word boundary.
 */
export function isValidNodeInsideWord(node) {
    return isSubstitutionPlaceHolderNode(node) || isRangeMarkerNode(node) || isDrawingSpaceMakerNode(node);
}

/**
 * An attribute string to replace 'src'
 * it is saved so in local storage
 * and will be replace with 'src' when load again
 */
export const LOCALSTORAGE_SRC_OVERRIDE = '__source__';

/**
 * Returns the parents of the passed node until the endNodes validators
 * stop it or the parent node will be undefined.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Array} [options.endNodes='DOM.APPCONTENT_NODE_SELECTOR']
 *      Validators for iterating the parent nodes.
 *      By default it's the app-content node selector.
 *
 * @returns {Array}
 *  The array with all reached parents of the passed node
 */
export function getDomTree(node, options) {
    const endNodes = getArrayOption(options, 'endNodes', [APPCONTENT_NODE_SELECTOR]);
    let parent = $(node);
    const returnArr = [];

    function check(ele) {
        let returnVal = true;

        _.each(endNodes, function (item) {
            if (is.function(item)) {
                if (item.call(this, ele) === true) {
                    returnVal = false;
                }

            } else {
                if ($.inArray(ele, $(item)) !== -1) {
                    returnVal = false;
                }
            }

        });
        return returnVal;
    }

    while (check(parent.first()[0]) && !_.isUndefined(parent.first()[0])) {
        returnArr.push(parent.first()[0]);
        parent = parent.parent();
    }
    returnArr.push(parent.first()[0]);

    return returnArr;
}

// presentation specific functions ==================================================

/**
 * The CSS class used to mark slide container nodes.
 */
export const SLIDE_CLASS = 'slide';

/**
 * A jQuery selector that matches nodes representing slide container classes.
 */
export const SLIDE_SELECTOR = '.' + SLIDE_CLASS;

/**
 * The CSS class used to mark slide container nodes.
 */
export const SLIDECONTAINER_CLASS = 'slidecontainer';

/**
 * A jQuery selector that matches nodes representing slide container classes.
 */
export const SLIDECONTAINER_SELECTOR = '.' + SLIDECONTAINER_CLASS;

/**
 * The class name representing the background node.
 */
export const BACKGROUNDSLIDE_CLASS = 'backgroundslide';

/**
 * The class name representing the image inside background node.
 */
export const BACKGROUNDIMAGE_CLASS = 'backgroundimage';

/**
 * The class name representing the layout slide layer node.
 */
export const LAYOUTSLIDELAYER_CLASS = 'layoutslidelayer';

/**
 * A jQuery selector that matches nodes representing the layout slide layer node.
 */
export const LAYOUTSLIDELAYER_SELECTOR = '.' + LAYOUTSLIDELAYER_CLASS;

/**
 * The class name representing the master slide layer node.
 */
export const MASTERSLIDELAYER_CLASS = 'masterslidelayer';

/**
 * A jQuery selector that matches nodes representing the master slide layer node.
 */
export const MASTERSLIDELAYER_SELECTOR = '.' + MASTERSLIDELAYER_CLASS;

/**
 * A jQuery selector that matches the layout slide layer and the master slide layer node.
 */
export const LAYOUT_MASTER_LAYER_SELECTOR = LAYOUTSLIDELAYER_SELECTOR + ', ' + MASTERSLIDELAYER_SELECTOR;

/**
 * The class name representing a the template buttons in empty place holder drawings.
 */
export const PLACEHOLDER_TEMPLATE_BUTTON_CLASS = 'placeholdertemplatebutton';

/**
 * The class name representing an image template buttons in empty place holder drawings.
 */
export const IMAGE_TEMPLATE_BUTTON_CLASS = 'imagetemplatebutton';

/**
 * The selector for image template buttons in empty place holder drawings.
 */
export const IMAGE_TEMPLATE_BUTTON_SELECTOR = '.' + IMAGE_TEMPLATE_BUTTON_CLASS;

/**
 * The class name representing a table template button in empty place holder drawings.
 */
export const TABLE_TEMPLATE_BUTTON_CLASS = 'tabletemplatebutton';

/**
 * The selector for table template buttons in empty place holder drawings.
 */
export const TABLE_TEMPLATE_BUTTON_SELECTOR = '.' + TABLE_TEMPLATE_BUTTON_CLASS;

/**
 * The class name representing a table template button in empty place holder drawings.
 */
export const LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS = 'live-preview-background';

/**
 * The selector for table template buttons in empty place holder drawings.
 */
export const LIVE_PREVIEW_SLIDE_BACKGROUND_SELECTOR = '.' + LIVE_PREVIEW_SLIDE_BACKGROUND_CLASS;

/**
 * Creating a new empty slide node (with a paragraph as template)
 *
 * @returns {jQuery}
 *  A slide element, as jQuery object.
 */
export function createSlideNode() {
    return createParagraphNode().addClass(SLIDE_CLASS);
}

/**
 * Returns, whether the specified node is a slide node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether this is a slide node.
 */
export function isSlideNode(node) {
    return $(node).is(SLIDE_SELECTOR);
}

/**
 * Returns, whether the specified node is a container for a slide node.
 *
 * @param {Node|jQuery|Null} node
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether this is a slide container node.
 */
export function isSlideContainerNode(node) {
    return $(node).is(SLIDECONTAINER_SELECTOR);
}

/**
 * Returns whether the passed node is within a slide container node.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is within a slide container node.
 */
export function isInsideSlideContainerNode(node) {
    return $(node).closest(SLIDECONTAINER_SELECTOR).length > 0;
}

/**
 * Returns temporary slide node for background preview, while dialog is open.
 *
 * @param {jQuery} slideNode
 *  current slide node
 *
 * @returns {jQuery}
 *  if found, temporary slide node for background preview.
 */
export function getTempSlideNodeForSlide(slideNode) {
    return $(slideNode).parent().children('.temp-preview-slide');
}

/**
 * Returns whether the passed node is a template button in an empty place holder drawing.
 *
 * @param {Node} node
 *  The node that will be checked.
 *
 * @returns {Boolean}
 *  Whether the specified node is a template button.
 */
export function isPlaceholderTemplateButton(node) {
    return $(node).hasClass(PLACEHOLDER_TEMPLATE_BUTTON_CLASS);
}

/**
 * Returns whether the passed node is a template button for inserting images
 * in an empty place holder drawing.
 *
 * @param {Node} node
 *  The node that will be checked.
 *
 * @returns {Boolean}
 *  Whether the specified node is a template button for inserting images.
 */
export function isImagePlaceholderTemplateButton(node) {
    return $(node).hasClass(IMAGE_TEMPLATE_BUTTON_CLASS);
}

/**
 * Returns whether the passed node is a template button for inserting tables
 * in an empty place holder drawing.
 *
 * @param {Node} node
 *  The node that will be checked.
 *
 * @returns {Boolean}
 *  Whether the specified node is a template button for inserting tables.
 */
export function isTablePlaceholderTemplateButton(node) {
    return $(node).hasClass(TABLE_TEMPLATE_BUTTON_CLASS);
}

/**
 * Returns whether the passed node is set as hyperlink.
 *
 * @param {Node|jQuery|Null} [node]
 *  The DOM node to be checked. If this object is a jQuery collection, uses
 *  the first DOM node it contains. If missing or null, returns false.
 *
 * @returns {Boolean}
 *  Whether the passed node is set as hyperlink.
 */
export function isHyperlinkNode(node) {
    const characterAttrs = node && getExplicitAttributes(node, 'character', true);
    return !!(characterAttrs && is.string(characterAttrs.url));
}

/**
 * Getting the topmost parent node in the DOM for a specified (slide) node.
 *
 * @param {JQuery} node
 *  The node whose top most parent in the DOM is searched.
 *
 * @returns {JQuery}
 *  The jQueryfied topmost parent of the specified node in the DOM.
 */
export function getTopMostParentInDOM(node) {
    let previousRoot = node;
    let currentRoot = previousRoot.parent();
    while (currentRoot && currentRoot.length > 0) {
        previousRoot = currentRoot;
        currentRoot = currentRoot.parent();
    }
    return previousRoot;
}

/**
 * Returns an arbitrary but unambiguous JSON representation of the passed
 * HTML element with some document contents.
 *
 * @param {HTMLElement|JQuery} element
 *  The element to be serialized to JSON.
 *
 * @returns {object}
 *  An unambiguous JSON representation of the passed HTML element.
 */
export function serializeElementToJSON(element) {

    // TODO: real (recursive) JSON structure!
    // TODO: formatting attributes!

    // clone the element, so that children (like TABs) can be modified
    const $element = $(element).clone(true);

    // removing all page breaks. Because of different formatting (fonts), the page breaks
    // can be at different positions inside the document
    // (-> maybe the pagebreaks can be sorted at document end).
    $element.find(PAGEBREAK_NODE_SELECTOR).remove();

    // removing all placeholder texts, because the are localized
    $element.find(TEMPLATE_TEXT_SELECTOR).remove();
    // removing date fields, because the are localized
    $element.find('.field-datetimeFigureOut').remove();
    $element.find('.drawing[data-placeholdertype="dt"]').remove();

    // tabs can have different length in different browsers
    $element.find(TAB_NODE_SELECTOR).each(function () {
        $(this).children().first().text('_TABULATOR_');
    });

    // special fields inside the header footer template might be different (see test 39A) (removing the paragraph -> should be less removal required)
    if ($element.find(HEADER_FOOTER_PLACEHOLDER_SELECTOR + ' ' + SPECIAL_FIELD_NODE_SELECTOR).length) {
        $element.find(HEADER_FOOTER_PLACEHOLDER_SELECTOR + ' ' + SPECIAL_FIELD_NODE_SELECTOR).parent().remove();
    }

    // listlabels might be different after copy/paste
    // -> there are even cases, when there was is a list label only in one document
    $element.find(LIST_LABEL_NODE_SELECTOR).each(function () {
        $(this).empty(); // TODO: Supporting listlabels in checksum
        // $(this).children('span').first().text('_LISTLABEL_');
    });

    let text = '';
    $element.find(CONTENT_NODE_SELECTOR).each(function (index) {
        if (isImplicitParagraphNode(this)) { return; }
        const isTable = isTableNode(this);
        text += index + (isTable ? 't' : ('p' + $(this).text()));
    });

    // replace NBSP (sometimes the diff is only a space versus a NBSP)
    text = text.replace('\xa0', ' ');

    return { text };
}

/**
 * Returns a Unicode replacement for the given list bullet character for a
 * specific symbol font.
 *
 * @param {string} font
 *  The name of the symbol font.
 *
 * @param {string} bullet
 *  The bullet character from the symbol font.
 *
 * @returns {Opt<object>}
 *  If the passed font is a supported (convertible) symbol font, a result
 * object with the following properties:
 *  - {string} char -- The Unicode replacement character.
 *  - {string} font -- The name of the rendering font.
 *  Otherwise, `undefined` will be returned to indicate that no conversion is
 *  needed.
 */
export function getReplacementForBullet(font, bullet) {

    if (!isSymbolFont(font)) {
        return undefined;
    }

    const symbolChar = getSymbolReplacement(font, bullet);
    return {
        char: symbolChar?.char || "\u2022",
        font: symbolChar?.serif ? "Times New Roman" : "Arial"
    };
}
