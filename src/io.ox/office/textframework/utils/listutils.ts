/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fmt } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

// types ------------------------------------------------------------------

/**
 * An object describing the auto detected list symbol.
 */
interface ListSymbolDescription {
    numberFormat?: "bullet" | "decimal" | "upperRoman" | "lowerRoman";
    symbol?: string;
    listStartValue?: number;
    leftString?: string;
    rightString?: string;
}

// constants --------------------------------------------------------------

/**
 * A constant that will be used in the GUI to represent the default style
 * of bullet lists or numbered lists.
 */
export const DEFAULT_LIST_STYLE_ID = "__DEFAULT__";

// static methods ---------------------------------------------------------

/**
 * Formatting a specified number value into a string of specified format.
 * Supported values for the parameter "format" are "lowerletter", "upperletter",
 * "lowerroman" and "upperroman".
 *
 * @param value
 *  One-based number to be converted.
 *
 * @param format
 *  The format, in which the number shall be converted.
 *
 * @returns
 *  The converted specified value. Or the empty string, if no conversion
 *  was done.
 */
export function formatNumber(value: number, format: string): string {

    // checking the specified format
    switch (format.toLowerCase()) {
        case "decimal":     return value.toString();
        case "decimalzero": return fmt.formatInt(value, 10, { digits: 2 });
        case "lowerletter": return fmt.formatAlphabetic(value, { repeat: true, lower: true });
        case "upperletter": return fmt.formatAlphabetic(value, { repeat: true });
        case "lowerroman":  return fmt.formatRoman(value, { depth: 4, lower: true });
        case "upperroman":  return fmt.formatRoman(value, { depth: 4 });
    }

    globalLogger.error(`formatNumber() - unknown number format: ${format}`);
    return "";
}

/**
 * Auto detection of lists from a specified text.
 *
 * @param text
 *  Possible numbering label text
 *
 * @returns
 *  An object containing the properties "numberFormat", "listStartValue"
 *  (numbered lists only) and symbol (bullet lists only). Allowed values
 *  for the numberFormat are "bullet", "decimal", "upperRoman" and
 *  "lowerRoman".
 *  If no list can be detected, an empty object is returned.
 */
export function detectListSymbol(text: string): ListSymbolDescription {

    // the return object used for collecting list information
    const ret: ListSymbolDescription = {};

    // searching in the specified string for "-", "*" or "."

    if (text.length === 1 && (text === "-" || text === "*")) {
        // bullet
        ret.numberFormat = "bullet";
        ret.symbol = text;
    } else if (text.endsWith(".") || text.endsWith(")")) {
        const first = text.substring(0, 1);
        let sub = text.substring(0, text.length - 1);
        if (first === "(") { sub = text.substring(1, text.length - 1); }
        // "sub" should only contain numbers or letters
        const startnumber = parseInt(sub, 10);
        if (startnumber > 0) {
            if (/^\d+$/.test(sub)) { // only numbers allowed in brackets or before dot
                ret.numberFormat = "decimal";
                ret.listStartValue = startnumber;
                ret.leftString = (first === "(") ? first : "";
                ret.rightString = text.substring(text.length - 1);
            }
        } else {
            const result = fmt.parseRoman(text);
            if (result > 0) {
                ret.numberFormat = (text.toLowerCase() === text) ? "lowerRoman" : "upperRoman";
                ret.listStartValue = result;
                ret.leftString = "";
                ret.rightString = text.substring(text.length - 1);
            }
        }
    }

    return ret;
}
