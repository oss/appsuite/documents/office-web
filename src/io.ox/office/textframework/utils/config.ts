/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { getFlag, getInt } from "@/io.ox/office/editframework/utils/editconfig";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/utils/editconfig";

// initialization =============================================================

// ATTENTION: If you change the defaults, please make sure that the server side is also changed
const maxCells = getInt("module/maxTableCells", 1500);
const maxCols = getInt("module/maxTableColumns", 15);
const maxRows = getInt("module/maxTableRows", 1500);

// constants ==================================================================

/**
 * Specifies whether online spelling is enabled.
 */
export const SPELLING_ENABLED = getFlag("module/spellingEnabled", true);

/**
 * The maximum page number that is allowed to be pasted as inline-html content
 * into a newly created e-mail.
 *
 * - If the server configuration item is missing, this fallback defaults to 10.
 * - A server configuration value beneath 0 will be ignored, it defaults to 0.
 * - A server configuration or default value of 0 disables the send-as-html feature.
 */
export const SEND_AS_HTML_MAX_PAGE_COUNT = getInt("module/sendAsHtmlMaxPageCount", 10);

/**
 * The maximum number of cells supported in a text table.
 */
export const MAX_TABLE_CELLS = maxCells;

/**
 * The maximum number of columns supported in a text table.
 */
export const MAX_TABLE_COLUMNS = maxCols;

/**
 * The maximum number of rows supported in a text table.
 */
export const MAX_TABLE_ROWS = maxRows;
