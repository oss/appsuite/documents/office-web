/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";

// constants ==================================================================

export const COMMENT_EDITOR_SELECTOR: string;
export const COMPLEXFIELDMEMBERNODE_CLASS: string;
export const FIELD_NODE_SELECTOR: string;
export const MARGINAL_NODE_CLASSNAME: string;
export const PAGE_NODE_SELECTOR: string;
export const PAGECOUNT_FIELD_SELECTOR: string;
export const PAGENUMBER_FIELD_SELECTOR: string;
export const PARAGRAPH_NODE_SELECTOR: string;
export const TABLE_REPEAT_ROWNODE_CLASSNAME: string;

// functions ==================================================================

export function createFieldNode(): JQuery;
export function createPageNode(): JQuery;
export function ensureExistingTextNode(node: NodeOrJQuery | null): void;
export function findFirstPortionSpan(node: NodeOrJQuery): HTMLElement;
export function getClosestMarginalTargetNode(node: NodeOrJQuery, options?: { footer?: boolean; header?: boolean }): JQuery;
export function getComplexFieldId(node: NodeOrJQuery | null): string;
export function getComplexFieldInstruction(node: NodeOrJQuery | null): string;
export function getComplexFieldMemberId(node: NodeOrJQuery | null): string;
export function getRangeMarkerType(node: NodeOrJQuery | null): string;
export function getFieldDateTimeFormat(node: NodeOrJQuery): string;
export function getFieldInstruction(node: NodeOrJQuery | null): string;
export function getFieldPageFormat(node: NodeOrJQuery): string;
export function getPageContentNode(pageNode: NodeOrJQuery): JQuery;
export function getSimpleFieldId(node: NodeOrJQuery): string;
export function getTargetContainerId(node: NodeOrJQuery | null): string;
export function isA11YAlertNode(node: NodeOrJQuery | null): boolean;
export function isAutomaticDateField(node: NodeOrJQuery | null): boolean;
export function isChangeTrackNode(node: NodeOrJQuery | null): boolean;
export function isComplexFieldNode(node: NodeOrJQuery | null): boolean;
export function isEmptyParagraph(node: NodeOrJQuery | null): boolean;
export function isEmptySpan(node: NodeOrJQuery | null): boolean;
export function isEmptyTextframe(node: NodeOrJQuery, options?: { ignoreTemplateText?: boolean }): boolean;
export function isFieldNode(node: NodeOrJQuery | null): boolean;
export function isFixedSimpleField(node: NodeOrJQuery | null, isODF: boolean): boolean;
export function isFooterWrapper(node: NodeOrJQuery): boolean;
export function isHeaderOrFooter(node: NodeOrJQuery): boolean;
export function isHeaderWrapper(node: NodeOrJQuery): boolean;
export function isInsideComplexFieldRange(node: NodeOrJQuery | null): boolean;
export function isInsideHeaderFooterTemplateNode(rootNode: NodeOrJQuery, node: NodeOrJQuery | null): boolean;
export function isMarginalNode(node: NodeOrJQuery | null): boolean;
export function isParagraphNode(node: NodeOrJQuery): boolean;
export function isRangeMarkerStartNode(node: NodeOrJQuery | null): boolean;
export function isSpan(node: NodeOrJQuery | null): boolean;
export function isSpecialField(node: NodeOrJQuery | null): boolean;
export function isTextSpan(node: NodeOrJQuery | null): boolean;
export function isValidNodeInsideWord(node: NodeOrJQuery | null): boolean;
export function splitTextSpan(node: HTMLSpanElement | JQuery, offset: number, options?: { append?: boolean }): JQuery;

// class Point ================================================================

export class Point {

    node: Node;
    offset?: number;

    constructor(node: NodeOrJQuery, offset?: number);

    clone(point: Point): Point;
    validate(): Point;
    toString(): string;

    static createPointForNode(node: NodeOrJQuery): Point;
    static equalPoints(point1: Point, point2: Point): boolean;
    static comparePoints(point1: Point, point2: Point): number;

}
