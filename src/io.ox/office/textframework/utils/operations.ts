/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { DrawingAttributes } from "@/io.ox/office/drawinglayer/model/drawingmodel";

import type { Position } from "@/io.ox/office/editframework/utils/operations";
import type { PtCharacterAttributeSet, PtChangesAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { OpBorder } from "@/io.ox/office/editframework/utils/border";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/utils/operations";

// types ======================================================================

/**
 * An arbitrary operation with "changes" attributes.
 */
export interface OptChangesOperation {
    attrs?: PtChangesAttributeSet;
}

/**
 * The operation "setAttributes".
 */
export interface SetAttributesOperation {
    name: typeof SET_ATTRIBUTES;
    start: Position;
    end?: Position;
    target?: string;
    attrs?: Dict; // TODO
}


/**
 * The operation "delete".
 */
export interface DeleteOperation {
    name: typeof DELETE;
    start: Position;
    end: Position;
    target?: string;
}

/**
 * The operation "insertText".
 */
export interface InsertTextOperation {
    name: typeof TEXT_INSERT;
    start: Position;
    text: string;
    target?: string;
    attrs?: PtCharacterAttributeSet & PtChangesAttributeSet;
}

/**
 * The operation "insertField".
 */
export interface InsertFieldOperation {
    name: typeof FIELD_INSERT;
    start: Position;
    representation: string;
    type: string;
    target?: string;
    attrs?: PtCharacterAttributeSet & PtChangesAttributeSet;
}

/**
 * The operation "updateField".
 */
export interface UpdateFieldOperation {
    name: typeof FIELD_UPDATE;
    start: Position;
    representation?: string;
    type?: string;
    target?: string;
    attrs?: PtCharacterAttributeSet & PtChangesAttributeSet;
}

/**
 * The drawing attributes only supported in OX Text
 */
export interface TextDrawingAttributes extends DrawingAttributes {
    marginLeft: number;
    marginRight: number;
    marginTop: number;
    marginBottom: number;
    borderLeft: OpBorder;
    borderRight: OpBorder;
    borderTop: OpBorder;
    borderBottom: OpBorder;
    // sizeRelHFrom
    // sizeRelVFrom
    // pctWidth
    // pctHeight
    // pctPosHOffset
    // pctPosVOffset
    inline: boolean;
    anchorHorBase: "margin" | "page" | "column" | "character" | "leftMargin" | "rightMargin" | "insideMargin" | "outsideMargin";
    anchorHorAlign: "left" | "right" | "center" | "inside" | "outside" | "offset";
    anchorHorOffset: number;
    anchorVertBase: "margin" | "page" | "paragraph" | "line" | "topMargin" | "bottomMargin" | "insideMargin" | "outsideMargin";
    anchorVertAlign: "top" | "bottom" | "center" | "inside" | "outside" | "offset";
    anchorVertOffset: number;
    anchorPageNumber: number;
    anchorLayerOrder: number;
    anchorBehindDoc: boolean;
    textWrapMode: "none" | "square" | "tight" | "through" | "topAndBottom";
    textWrapSide: "both" | "left" | "right" | "largest";
}

// constants ==================================================================

export const SET_ATTRIBUTES = "setAttributes";
export const DELETE = "delete";
export const MOVE = "move";

export const TEXT_INSERT = "insertText";
export const FIELD_INSERT = "insertField";
export const FIELD_UPDATE = "updateField";
export const COMPLEXFIELD_INSERT = "insertComplexField";
export const COMPLEXFIELD_UPDATE = "updateComplexField";
export const BOOKMARK_INSERT = "insertBookmark";
export const TAB_INSERT = "insertTab";
export const HARDBREAK_INSERT = "insertHardBreak";
export const COMMENT_INSERT = "insertComment";
export const COMMENT_DELETE = "deleteComment";
export const COMMENT_CHANGE = "changeComment";
export const RANGE_INSERT = "insertRange";

export const PARA_INSERT = "insertParagraph";
export const PARA_SPLIT = "splitParagraph";
export const PARA_MERGE = "mergeParagraph";

export const TABLE_INSERT = "insertTable";
export const ROWS_INSERT = "insertRows";
export const CELLS_INSERT = "insertCells";
export const CELL_SPLIT = "splitCell";
export const CELL_MERGE = "mergeCell";
export const COLUMN_INSERT = "insertColumn";
export const COLUMNS_DELETE = "deleteColumns";
export const TABLE_SPLIT = "splitTable";
export const TABLE_MERGE = "mergeTable";

export const INSERT_LIST = "insertListStyle";
export const DELETE_LIST = "deleteListStyle";
export const INSERT_HEADER_FOOTER = "insertHeaderFooter";
export const DELETE_HEADER_FOOTER = "deleteHeaderFooter";
