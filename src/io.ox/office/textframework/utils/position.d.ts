/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { Position } from "@/io.ox/office/editframework/utils/operations";
import { Point } from "@/io.ox/office/textframework/utils/dom";

// types ======================================================================

/**
 * Optional parameters for the function `getOxoPosition()`.
 */

interface GetOxoPositionOptions {

    /**
     * If set to `true`, the error messages in this function are not written
     * into the console. This is useful, if the correctness of a position is
     * checked. Default value is `false`.
     */
    suppressWarning?: boolean;
}

/**
 * Optional parameters for the function `getWordSelection()`.
 */
interface WordSelectionOptions {

    /**
     * If set to `true`, only the word boundary left of the specified position
     * is searched. In this case, no check to the right is done. Instead the
     * specified logical start position for the search is used as right
     * boundary.
     */
    onlyLeft?: boolean;

    /**
     * If set to `true`, the text span node, that is located at the specified
     * logical position will be included into the return object, if possible.
     */
    returnTextSpan?: boolean;

    /**
     * If set to `true`, all spaces following the word are included into the
     * calculated selection. This can be used for double clicking.
     */
    addFinalSpaces?: boolean;

    /**
     * If set to `true`, the text string is not generated. This can be used for
     * performance reasons, if the string is not required as return value.
     */
    ignoreText?: boolean;

    /**
     * If set to `true`, collect all spans of the word and add it in the return
     * object with name 'nodes'
     */
    returnAllTextSpans?: boolean;

    /**
     * If set to `true`, for RangeMarkerNodes & CommentPlaceHolderNodes
     * whitespaces to the resulting text will be added
     */
    fillPlaceholders?: boolean;
}

/**
 * Result of the function `getWordSelection()`.
 */
export interface WordSelectionResult {

    /**
     * The logical start position of the first character of the word inside the
     * paragraph.
     */
    start: number;

    /**
     * The logical end position of the character BEHIND(!) the word inside the
     * paragraph.
     */
    end: number;

    /**
     * Contains the complete word as string.
     */
    text: string;

    /**
     * Contains a reference to the text span node, that contains the specified
     * position.
     */
    node?: HTMLElement;

    /**
     * A collector with all investigated text spans.
     */
    nodes?: HTMLElement[];
}

// functions ==================================================================

export function getFirstTextPositionInTextFrame(startNode: NodeOrJQuery, drawingFrame: JQuery): Position | null;
export function getLastTextPositionInTextFrame(startNode: NodeOrJQuery, drawingFrame: JQuery): Position | null;
export function getParagraphElement(startnode: NodeOrJQuery, oxoPosition: Position): HTMLElement | null;
export function getParagraphLength(startnode: NodeOrJQuery, oxoPosition: Position, options?: { next?: boolean }): number | undefined;
export function getPositionRangeForNode(startNode: NodeOrJQuery, node: NodeOrJQuery, useRangeMode: boolean): { start: Position; end: Position } | null;
export function getWordSelection(paragraph: NodeOrJQuery, pos: number, charList?: string[], options?: WordSelectionOptions): WordSelectionResult | null;

export function appendNewIndex(position: Position, index: number): Position;
export function decreaseLastIndex(position: Position, decrement?: number): Position;
export function increaseLastIndex(position: Position, increment?: number): Position;
export function isParagraphPosition(startnode: NodeOrJQuery, oxoPosition: Position): boolean;
export function getDOMPosition(startnode: NodeOrJQuery, oxoPosition: Position, forcePositionCounting?: boolean): Point | undefined;
export function getOxoPosition(startNode: NodeOrJQuery, node: NodeOrJQuery, offset?: number, options?: GetOxoPositionOptions): Position | undefined;
export function getCharacterAtDomPoint(point: Point): string | null;
export function getCharacterAtPosition(startnode: NodeOrJQuery, oxoPosition: Position): string | null;
