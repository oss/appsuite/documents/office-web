/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { debounceMethod } from "@/io.ox/office/tk/objects";

import { getDrawingNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getCssTransform, getRotatedDrawingPoints } from '@/io.ox/office/drawinglayer/utils/drawingutils';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';

import { SHOW_REMOTE_SELECTIONS } from '@/io.ox/office/textframework/utils/config';
import { getDOMPosition } from '@/io.ox/office/textframework/utils/position';
import { findFarthest, getBooleanOption, getCSSRectangleFromPoints, getNumberOption } from '@/io.ox/office/textframework/utils/textutils';

// class RemoteSelection ==================================================

export default class RemoteSelection extends ModelObject {

    // the selection object
    #selection;
    // the root node of the document
    #rootNode;
    // slide mode for spreadsheet/presentation
    #slideMode;
    // the display timer of the collaborative overlay
    #overlayTimer;
    // list with all drawings nodes with mouseenter and mouseleave handlers with the namespace 'remoteselection'
    #drawingNodesWithHandlers = [];

    /**
     * @param {TextBaseModel} docModel
     *  The document model instance.
     */
    constructor(docModel) {

        // base constructor ---------------------------------------------------
        super(docModel);

        this.#rootNode = this.docModel.getNode();
        this.#slideMode = this.docModel.useSlideMode();

        // initialization -----------------------------------------------------

        // initialize class members when application is alive
        this.docApp.onInit(() => {
            this.#removeDrawingNodesHandler();
            this.#selection = this.docModel.getSelection();
            // render selection if the user change the slide to see the current selection of the new slide
            this.listenTo(this.docModel, 'change:slide', this.renderCollaborativeSelections);
            // render selection if drawing attributes will be changed
            this.listenTo(this.docModel, 'change:drawing', this.renderCollaborativeSelections);
            // render selection if the attributes of elements have changed (67116)
            this.listenTo(this.docModel, 'docs:attribute:changed', this.renderCollaborativeSelections);
            // render selection if the list of active clients has changed
            this.listenTo(this.docApp, 'docs:users:selection', this.renderCollaborativeSelections);
            this.listenTo(this.docModel, 'update:absoluteElements', this.renderCollaborativeSelections);
            this.listenTo(this.docModel,  'drawingHeight:update', this.renderCollaborativeSelections);
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Draw overlays for the selections of all remote clients.
     *
     * @returns {RemoteSelection}
     *  A reference to this instance.
     */
    renderCollaborativeSelections() {
        this._renderCollaborativeSelectionDebounced();
        return this;
    }

    // private methods --------------------------------------------------------

    /**
     * Draw overlays for a specific remote user's selection debounced.
     */
    @debounceMethod({ delay: 300 })
    _renderCollaborativeSelectionDebounced() {
        if (SHOW_REMOTE_SELECTIONS && this.docModel.showRemoteSelections()) {

            // first, clear all old selection nodes from the DOM
            this.#rootNode.find('.collaborative-overlay').children().remove();

            this.#removeDrawingNodesHandler();

            // render all remote selections
            this.docApp.getActiveClients().forEach(client => this.#renderSelection(client));
        }
    }

    /**
     * Calculate physical positions from logical selection.
     *
     * @param {DOM.Point} anchorPoint
     *  the selection start point
     *
     * @param {DOM.Point} focusPoint
     *  the selection end point.
     *
     * @returns {Object | null}
     *  An selection object containing physical start and end coordinates.
     *  - {Number} Object.anchor - selection start absolute coordinate in pixels
     *  - {Number} Object.focus -  selection end absolute coordinate in pixels
     */
    #calculatePhysicalPosition(anchorPoint, focusPoint) {

        // quit if no logical selection is available
        if (!anchorPoint || !focusPoint) { return null; }

        const zoom = this.docApp.getView().getZoomFactor();

        const getPhysicalPosition = (point1, point2, start) => {
            return getCSSRectangleFromPoints(point1, point2, this.#rootNode, zoom, start);
        };

        return { anchor: getPhysicalPosition(anchorPoint, null, true), focus: getPhysicalPosition(focusPoint), range: getPhysicalPosition(anchorPoint, focusPoint) };
    }

    /**
     * Create a Overlay with the username to show the remote username for a selection.
     *
     * @param {type} username name of the user
     * @param {type} className class name for the overlay
     * @param {String} clientId Id of the client
     */
    #createUserNameOverlay(username, className, clientId) {
        const usernameOverlay = $(document.createElement('div'));
        // draw user name
        usernameOverlay.addClass('collaborative-username ' + className);
        usernameOverlay.attr('data-clientid', clientId);
        usernameOverlay.text(username);

        return usernameOverlay;
    }

    /**
     * Draw overlays for a specific remote user's selection.
     *
     * @param {Object} client
     *  Descriptor of a client editing this document.
     */
    #renderSelection(client) {

        // the selection of the client
        const userData = is.dict(client.userData) ? client.userData : {};
        const userSelections = userData.selections;
        const userTargetSelection = userData.target;
        const userTargetIndex = userData.targetIndex;

        // helper function to determine if selection is on active slide
        const isSelectionOnActiveSlide = userSelection => {
            let state = false;
            const activeSlideId = this.docModel.getActiveSlideId();

            if (this.docModel.isLayoutOrMasterId(activeSlideId)) {
                state = activeSlideId === userData.target;
            } else {
                state = this.docModel.getActiveSlideIndex() === userSelection.start[0];
            }
            return state;
        };

        // draw nothing and quit if:
        // - user is the current user himself
        // - no user data/selection is available.
        if (!client.remote || !userSelections || userSelections.length === 0) {
            return;
        }

        const username = client.userName;
        const overlay = this.#rootNode.find('.collaborative-overlay');
        const className = 'user-' + client.colorIndex;
        const zoom = this.docApp.getView().getZoomFactor();
        // skip the render of drawing selections if one drawing is not on the active slide
        let skipDrawing = false;
        let usernameOverlay;

        // loop through the selections of the user
        userSelections.forEach(userSelection => {

            // special handling if selection is a top level cursor position on a slide
            // -> not drawing the remote cursor on the slide.
            if (skipDrawing || (this.#slideMode && this.#selection.isTopLevelTextCursor(userSelection))) {
                // TODO: Draw remote selection (on slide pane?)
                return;
            }

            // Don't draw selection if is on inactive slide, #46237
            if (this.#slideMode && !isSelectionOnActiveSlide(userSelection)) { return; }

            // OT DEMO -> if there are local operations cached without acknowledge from the server
            //            the OSN cannot lead to an error.
            if (this.docApp.hasLocalActionsWithoutAck()) {
                // OT DEMO: Now it is time to transform the incoming external positions!
                try {
                    if (!userSelection.start) { return; } // happens in testrunner tests
                    const range = userSelection.end && !_.isEqual(userSelection.start, userSelection.end);
                    userSelection.start = this.docApp.transformPositionWithLocalActions(userSelection.start, userTargetSelection);
                    if (range) {
                        userSelection.end = this.docApp.transformPositionWithLocalActions(userSelection.end, userTargetSelection);
                    } else {
                        userSelection.end = [...userSelection.start];
                    }
                } catch (exception) {
                    globalLogger.error('Remote Selection: ' + exception.message);
                    return;
                }
            }

            const activeRootNode = this.docModel.getRootNode(userTargetSelection, userTargetIndex);
            const selectionAnchorPoint = getDOMPosition(activeRootNode, userSelection.start);
            const selectionFocusPoint = getDOMPosition(activeRootNode, userSelection.end);

            if (!selectionAnchorPoint || !selectionFocusPoint) { return; }

            const selectionOverlayGroup = $('<div>').addClass('collaborative-selection-group');

            let drawingNode;
            let rotation = 0;
            let drawingAttrs;
            let setUsernameRotation = false;
            let flipH = false;
            let flipV = false;
            // if the drawing is rotatet only select the frame and not the text
            if (this.#slideMode && userSelection.type !== 'drawing') {
                drawingNode = getDrawingNode($(getDOMPosition(activeRootNode, userSelection.start, true).node));
                if (drawingNode) {
                    drawingAttrs = getExplicitAttributes(drawingNode, 'drawing');
                    rotation = getNumberOption(drawingAttrs, 'rotation', 0);
                }
            }

            // render the selection for drawings
            if (userSelection.type === 'drawing' || rotation > 0) {
                // Test if the drawing is on the aktive slide (Presentation), if not skip the rendering.
                if (this.#slideMode) {
                    const slideID = this.docModel.getActiveSlideId();
                    const layoutOrMaster = this.docModel.isMasterView();
                    if (layoutOrMaster) {
                        if (slideID !== userData.target) {
                            skipDrawing = true;
                        }
                        // TODO select slidePane if the target is layoutOrMaster too
                    } else {
                        if (userData.target || userSelection.start[0] !== this.docModel.getActiveSlideIndex()) {
                            skipDrawing = true;
                        }
                        // TODO select slidePane it the target is not layoutOrMaster
                    }
                    if (skipDrawing) {
                        return;
                    }
                }

                if (!drawingNode) {
                    drawingNode = $(getDOMPosition(activeRootNode, userSelection.start, true).node);
                }

                if (drawingNode.length === 0) {
                    return;
                }
                drawingAttrs = drawingAttrs || getExplicitAttributes(drawingNode, 'drawing');
                rotation = 0;

                flipH = getBooleanOption(drawingAttrs, 'flipH', false);
                flipV = getBooleanOption(drawingAttrs, 'flipV', false);
                rotation = getNumberOption(drawingAttrs, 'rotation', 0);

                const drawingOverlay = document.createElement('div');
                let top = this.#slideMode ? parseFloat(drawingNode.css('top')) : (drawingNode.offset().top - this.#rootNode.offset().top) / zoom;
                let left = this.#slideMode ? parseFloat(drawingNode.css('left')) : (drawingNode.offset().left - this.#rootNode.offset().left) / zoom;

                if (rotation !== 0 && !this.#slideMode) {
                    const rect = getRotatedDrawingPoints({ width: drawingNode.width(), height: drawingNode.height(), top: 0, left: 0 }, -rotation);
                    top -= rect.top;
                    left -= rect.left;
                }

                usernameOverlay = this.#createUserNameOverlay(username, className, client.userId);

                // Set the selection frame position and size attributes
                $(selectionOverlayGroup).css({
                    top: top - 2,
                    left: left - 2,
                    width: parseFloat(drawingNode.css('width')) + 4,
                    height: parseFloat(drawingNode.css('height')) + 4,
                    transform: getCssTransform(rotation, flipH, flipV)
                });

                selectionOverlayGroup.append(drawingOverlay);
                selectionOverlayGroup.append(usernameOverlay);

                // Add 'hover' handler with namespace to show the Username Overlay.
                // The namespace is to remove the event handler without the function.
                this.listenTo(drawingNode, 'mouseenter.remoteselection', () => usernameOverlay.show());
                this.listenTo(drawingNode, 'mouseleave.remoteselection', () => usernameOverlay.hide());

                usernameOverlay.show();
                usernameOverlay.css('display', 'inherit');
                this.#drawingNodesWithHandlers.push(drawingNode);
                drawingOverlay.setAttribute('class', 'selection-overlay drawing ' + className);
                setUsernameRotation = true;
            } else {
                const caretHandle = $('<div>').addClass('collaborative-cursor-handle');
                const caretOverlay = $('<div>').addClass('collaborative-cursor');
                const startNode = $(selectionAnchorPoint.node.parentNode);
                const endNode = $(selectionFocusPoint.node.parentNode);
                const physicalPosition = this.#calculatePhysicalPosition(selectionAnchorPoint, selectionFocusPoint);
                const isRangeSelection = physicalPosition.anchor.top !== physicalPosition.focus.top || physicalPosition.anchor.left !== physicalPosition.focus.left;

                usernameOverlay = this.#createUserNameOverlay(username, className, client.userId);

                // draw user cursors and names
                usernameOverlay.css('top', physicalPosition.focus.height);
                caretOverlay.css({ top: physicalPosition.focus.top, left: physicalPosition.focus.left, height: physicalPosition.focus.height }).addClass(className);
                caretOverlay.append(usernameOverlay, caretHandle);
                caretHandle.hover(() => { $(usernameOverlay).show(); }, () => { usernameOverlay.hide(); });
                usernameOverlay.show();
                usernameOverlay.css('display', 'inherit');
                overlay.append(caretOverlay);

                // if remote selection is in header/footer, fix z-index collision.
                if (client.editor) {
                    if (userTargetSelection) {
                        overlay.addClass('target-overlay');
                    } else {
                        overlay.removeClass('target-overlay');
                    }
                }

                if (isRangeSelection) {

                    // draw collaborative selection area highlighting if we have an 'range' selection
                    const rootNodePos = this.#rootNode.offset(); //both
                    const startNodeCell = startNode.closest('td'); // table
                    const endNodeCell = endNode.closest('td'); // table
                    const isTableSelection = startNodeCell.length && endNodeCell.length && !startNodeCell.is(endNodeCell);

                    // special handling of pure table selection
                    if (isTableSelection) {

                        // handle special mega cool firefox cell selection
                        if (userSelection.type === 'cell') {

                            const ovStartPos = startNodeCell.offset();
                            const ovEndPos = endNodeCell.offset();
                            const ovWidth = (ovEndPos.left  - ovStartPos.left) / zoom + endNodeCell.outerWidth();
                            const ovHeight = (ovEndPos.top  - ovStartPos.top) / zoom + endNodeCell.outerHeight();
                            const ov = document.createElement('div');

                            $(ov).css({
                                top: (ovStartPos.top - rootNodePos.top) / zoom,
                                left: (ovStartPos.left - rootNodePos.left) / zoom,
                                width: ovWidth,
                                height: ovHeight
                            });

                            selectionOverlayGroup.append(ov);

                        } else { // normal table selection (Chrome, IE): iterate cells between start and end

                            const cells = $(findFarthest(this.#rootNode, endNodeCell, 'table')).find('td');
                            const cellsToHighlight = cells.slice(cells.index(startNodeCell), cells.index(endNodeCell) + 1);

                            cellsToHighlight.each(() => {

                                const cellOv = document.createElement('div');
                                const offset = $(this).offset();

                                $(cellOv).css({
                                    top: (offset.top - rootNodePos.top) / zoom,
                                    left: (offset.left - rootNodePos.left) / zoom,
                                    width: $(this).outerWidth(),
                                    height: $(this).outerHeight()
                                });

                                selectionOverlayGroup.append(cellOv);

                            });
                        }
                    } else { // paragraph / mixed selection
                        const startTop = physicalPosition.anchor.top;
                        const startHeight = physicalPosition.anchor.height;
                        const endTop = physicalPosition.focus.top;
                        const endHeight = physicalPosition.focus.height;
                        const startBottom = startTop + startHeight;
                        const endBottom = endTop + endHeight;
                        const isSingleLineSelection = startBottom === endBottom || (endTop >= startTop && endBottom <= startBottom) || (startTop >= endTop && startBottom <= endBottom);
                        let caretHeight;

                        // selection area is in a line
                        if (isSingleLineSelection) {
                            caretHeight = physicalPosition.range.height;

                            const selectionOverlay = document.createElement('div');
                            $(selectionOverlay).css({ top: physicalPosition.range.top, left: physicalPosition.range.left, width: physicalPosition.range.width, height: caretHeight });
                            selectionOverlayGroup.append(selectionOverlay);

                            usernameOverlay.css('top', caretHeight);
                            caretOverlay.css({ top: physicalPosition.range.top, left: physicalPosition.focus.left, height: caretHeight }).addClass(className);
                        } else { // multi line selection area

                            const anchorSelection = physicalPosition.anchor;
                            const focusSelection = physicalPosition.focus;
                            const rangeSelection = physicalPosition.range;

                            const height = (anchorSelection.top + anchorSelection.height) - rangeSelection.top;
                            const headOverlay = document.createElement('div');
                            $(headOverlay).css({
                                top: rangeSelection.top,
                                left: anchorSelection.left,
                                width: anchorSelection.left === rangeSelection.left ?  rangeSelection.width : (rangeSelection.left + rangeSelection.width) - anchorSelection.left,
                                height
                            });

                            const tailOverlay = document.createElement('div');
                            const tailWidth = focusSelection.left - rangeSelection.left;
                            $(tailOverlay).css({
                                top: focusSelection.top,
                                left: rangeSelection.left,
                                width: tailWidth,
                                height: focusSelection.height
                            });

                            const bodyOverlay = document.createElement('div');
                            const bodyTop = rangeSelection.top + height;
                            const bodyHeight = focusSelection.top - bodyTop;
                            $(bodyOverlay).css({
                                top: bodyTop,
                                left: rangeSelection.left,
                                width: rangeSelection.width,
                                height: bodyHeight
                            });

                            caretHeight = focusSelection.height;

                            usernameOverlay.css('top', caretHeight);
                            caretOverlay.css({ top: focusSelection.top, left: rangeSelection.left + tailWidth, height: caretHeight }).addClass(className);

                            selectionOverlayGroup.append(headOverlay, bodyOverlay, tailOverlay);
                        }
                    }

                    selectionOverlayGroup.children().get().forEach(overlayElement => {
                        overlayElement.setAttribute('class', 'selection-overlay ' + className);
                    });
                }
            }

            if (!is.boolean(client.selectionChanged)) {
                usernameOverlay.show(); // 36800
            }
            usernameOverlay.show();
            usernameOverlay.css('display', 'inherit');
            overlay.append(selectionOverlayGroup);
            if (setUsernameRotation) {
                const userHeight = usernameOverlay.height(); //clientSize ? clientSize.height : 0;
                const userWidth = usernameOverlay.width(); //clientSize ? clientSize.width : 0;
                let usernameCSS;
                if (rotation >= 316 || rotation <= 45) {
                    //top
                    usernameCSS = {
                        top: 0,
                        left: 0,
                        transform: getCssTransform(0, flipH, flipV)
                    };
                } else if (rotation >= 136 && rotation <= 225) {
                    // bottom
                    usernameCSS = {
                        top: Math.ceil(parseFloat(drawingNode.css('height')) - userHeight),
                        left: Math.ceil(parseFloat(drawingNode.css('width')) - userWidth - 12),
                        transform: getCssTransform(0, !flipH, !flipV)
                    };
                } else {
                    const isLeft = rotation >= 46 && rotation <= 135 && !flipH && !flipV;
                    const isRight = rotation >= 226 && rotation <= 315;
                    if (isLeft || (isRight && (flipV || flipH))) {
                        usernameCSS = {
                            top: Math.ceil(parseFloat(drawingNode.css('height')) - userWidth / 2 - userHeight + 2),
                            left: Math.ceil(-userWidth / 2 + 2),
                            transform: getCssTransform(90, !flipH || (flipH && flipV), !flipV || (flipH && flipV))
                        };
                    } else {
                        // right
                        usernameCSS = {
                            top:  Math.ceil(userWidth / 2 - userHeight / 2 + 6),
                            left: Math.ceil(-userWidth / 2 + 2 + parseFloat(drawingNode.css('width')) - userHeight),
                            transform: getCssTransform(90, flipH && !flipV, flipV && !flipH)
                        };
                    }
                }
                $(usernameOverlay).css(usernameCSS);
            }
        });

        // show user name for a second
        if (this.#overlayTimer) { this.#overlayTimer.abort(); }

        this.#overlayTimer = this.setTimeout(() => {
            overlay.find('.collaborative-username').fadeOut('fast');
        }, 3000);
    }

    /**
     * Remove the 'Hover' event handlers to show the username tooltip from the selected drawing layer.
     */
    #removeDrawingNodesHandler() {
        // remove the handlers from the drawing nodes
        this.#drawingNodesWithHandlers.forEach(drawingNode => { this.stopListeningTo(drawingNode, 'mouseenter.remoteselection mouseleave.remoteselection'); });
        this.#drawingNodesWithHandlers = [];
    }
}
