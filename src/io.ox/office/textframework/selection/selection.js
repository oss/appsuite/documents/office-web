/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { ary, unicode, itr } from '@/io.ox/office/tk/algorithms';
import { IOS_SAFARI_DEVICE, TOUCH_DEVICE, containsNode, getFocus, setFocus, containsFocus, clearBrowserSelection, KeyCode, hasKeyCode } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { BREAK, CHROME_ON_ANDROID, findClosest, findDescendantNode, findFarthest, findNextNode,
    findNextSiblingNode, findPreviousNode, findPreviousSiblingNode, getArrayOption, getBooleanOption, getDomNode,
    getNodePositionInPage, getOption, isLastDescendant } from '@/io.ox/office/tk/utils';
import { isCheckedButtonNode } from '@/io.ox/office/tk/forms';

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { equalPositions, comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { otLogger } from '@/io.ox/office/editframework/utils/otutils';

import { getMimeTypeFromImageUri } from '@/io.ox/office/drawinglayer/utils/imageutils';
import * as DrawingFrame from '@/io.ox/office/drawinglayer/view/drawingframe';

import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { GROUP, MOVE, NOOP, PARA_INSERT } from '@/io.ox/office/textframework/utils/operations';
import { isF6AcessibilityKeyEvent } from '@/io.ox/office/textframework/utils/keycodeutils';
import * as Position from '@/io.ox/office/textframework/utils/position';
import MultiSelectionMixin from '@/io.ox/office/textframework/selection/multiselectionmixin';
import AndroidSelection from '@/io.ox/office/textframework/selection/androidselection';
import { DEBUG } from '@/io.ox/office/textframework/utils/config';
import { SelectionRangeObserver } from '@/io.ox/office/textframework/selection/selectionrangeobserver';

// constants ==================================================================

// whether the browser selection object supports the collapse() and expand() methods needed to create a backwards selection
var SELECTION_COLLAPSE_EXPAND_SUPPORT = (function () {
    var selection = window.getSelection();
    return _.isFunction(selection.collapse) && _.isFunction(selection.extend);
}());

// if set to true, DOM selection will be logged to console
var LOG_SELECTION = false;

// private functions ==========================================================

/**
 * A jQuery selector for non-empty text spans and in-line component
 * nodes. Empty text spans are allowed as first elements in the paragraph, so
 * that positions before range markers can be reached (41957).
 */
function inlineNodeSelector() {
    return DOM.isEditableInlineComponentNode(this) || (DOM.isTextSpan(this) && (!DOM.isEmptySpan(this) || DOM.isFirstTextSpanInParagraph(this)));
}

/**
 * A jQuery selector for empty and non-empty text spans and in-line component
 * nodes.
 */
function allowEmptyNodeSelector() {
    return DOM.isEditableInlineComponentNode(this) || DOM.isTextSpan(this);
}

/**
 * Returns the first non-empty inline node of the specified paragraph (text
 * component nodes or non-empty text spans) if existing. Otherwise, if the
 * paragraph is empty, returns the trailing text span (following the
 * leading floating drawing objects).
 *
 * @param {HTMLElement | JQuery} paragraph
 *  The paragraph element.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  @param {Boolean} [options.allowEmpty=false]
 *      If set to true, empty text spans will be included into the
 *      search for the first text cursor node. Otherwise only non empty
 *      text spans are allowed. The latter is the default.
 */
function getFirstTextCursorNode(paragraph, options) {
    return findDescendantNode(paragraph, getBooleanOption(options, 'allowEmpty', false) ? allowEmptyNodeSelector : inlineNodeSelector, { children: true }) || DOM.findLastPortionSpan(paragraph);
}

/**
 * Returns the last non-empty inline node of the specified paragraph (text
 * component nodes or non-empty text spans) if existing. Otherwise, if the
 * paragraph is empty, returns the trailing text span (following the
 * leading floating drawing objects).
 */
function getLastTextCursorNode(paragraph) {
    return findDescendantNode(paragraph, inlineNodeSelector, { children: true, reverse: true }) || DOM.findLastPortionSpan(paragraph);
}

// class Selection ============================================================

/**
 * An instance of this class represents a selection in the edited document,
 * consisting of a logical start and end position representing a half-open
 * text range, or a rectangular table cell range.
 *
 * @mixes MultiSelectionMixin
 */
export default class Selection extends ModelObject {

    /**
     * @param {TextBaseModel} docModel
     *  The document model instance.
     */
    constructor(docModel) {

        // base constructor
        super(docModel);

        var // self reference
            self = this,

            // the application instance
            docApp = this.docApp,

            // the root node of the document
            rootNode = docModel.getNode(),

            // slide mode for spreadsheet/presentation
            slideMode = docModel.useSlideMode(),

            // logical start position
            startPosition = [],

            // logical end position (half-open range)
            endPosition = [],

            // whether the current text range has been selected backwards
            backwards = false,

            // whether the last cursor movement was to the left or upwards
            isBackwardCursor = false,

            // after switching the target, the change event must be triggered (even if start and end position stay the same)
            forceSelectionChangeEvent = false,

            // drawing node currently selected, as jQuery collection
            selectedDrawing = $(),

            // drawing node currently beeing cropped
            activeCropDrawing = $(),

            // selecting additionally the text frame, if text is selected
            selectedTextFrameDrawing = $(),

            // previously selected text frame (used for detecting a leave of a text frame)
            previousSelectedTextFrameDrawing = null,

            // table currently selected, as jQuery object
            selectedTable = $(),

            // whether this selection represents a rectangular table cell range
            cellRangeSelected = false,

            // whether the cell range selection covers the entire table
            tableSelected = false,

            // the anchor of a cell range of a cell selection in a table
            anchorCellRange = null,

            // copy of drawing contents with browser selection, for copy&paste
            clipboardNode = null,

            // the original focus() method of the root node DOM element
            originalFocusMethod = rootNode[0].focus,

            // the original focus() method of a non-root node DOM element
            origNonPageFocusMethod = null,

            // Performance: Saving info from insertText operation to simplify scroll calculation
            insertTextPoint = null, isInsertTextOperation = false,

            // Performance: Saving paragraph node, logical paragraph position and text offset in specified operations
            paragraphCache = null,

            // whether the calculated logical (end) position was modified by triggering 'position:calculated'
            selectionExternallyModified = false, endSelectionExternallyModified = false,

            // whether the selection can temporarely not used for iterations, for example during resolving of change track
            isTemporaryInvalidSelection = false,

            // whether a selection problem is externally resolved
            problemExternallyHandled = false,

            // the handler function for moving and resizing drawings
            drawingResizeHandler = null,

            // the handler function for updating size and position of a selection in selection overlay node
            updateOverlaySelectionHandler = null,

            // a store for the current selection state (61568)
            selectionStore = null,

            // whether a drawing is currently moved, resized or rotated
            isTrackingActive = false,

            // a marker for an active triple click event
            isTripleClickActive = false,

            // a timer for the function call of 'processBrowserEvent'
            processBrowserEventTimer = null;

        // mixins
        MultiSelectionMixin.call(this, docModel);
        if (_.browser.Android) { AndroidSelection.call(this, docModel, updateLogicalPosition); }

        // private methods ----------------------------------------------------

        /**
         * Returns the current logical anchor position.
         */
        function getAnchorPosition() {
            return backwards ? endPosition : startPosition;
        }

        /**
         * Returns the current logical focus position.
         */
        function getFocusPosition() {
            return backwards ? startPosition : endPosition;
        }

        /**
         * Returns the first logical position in the document.
         * If the optional parameter node is specified, it returns the
         * first logical position inside this node. This optional
         * parameter can be used for text frames.
         *
         * Info: This function supports an empty selection, that can
         * occur in slide mode after deleting the last slide (45742).
         *
         * @param {HTMLElement|jQuery} [node]
         *  An optional DOM element object whose first logical position
         *  will be searched. If this object is a jQuery collection,
         *  uses the first node it contains.
         *  If it is not specified, the page content node will be
         *  used as default.
         *
         * @param {Object} [options]
         *  Supports all options that are supported by 'getFirstTextCursorNode'.
         *
         * @returns {Number[]|Null}
         *  The first logical position in the document or the specified node.
         *  Or null, if this is the empty selection
         */
        function getFirstPosition(node, options) {

            var // the node, in which the first position is searched
                searchNode = null,
                // the page content node
                pageContentNode = null,
                // the first paragraph inside the specified node
                firstParagraph = null,
                // the first in-line node inside the paragraph (to avoid selection of absolute drawings at para start)
                firstInlineNode = null,
                // whether this is an empty selection in slide mode
                isEmptyPageContent = false;

            if (node) {
                searchNode = node;
            } else {
                pageContentNode = DOM.getPageContentNode(rootNode);

                if (pageContentNode.length > 0) {
                    searchNode = pageContentNode;
                } else {
                    searchNode = rootNode;
                }

                // empty page content node or removed slide in layout master view
                if (slideMode && searchNode.children().length === 0) { isEmptyPageContent = true; }
            }

            if (isEmptyPageContent) { return null; }

            firstParagraph = findDescendantNode(searchNode, DOM.PARAGRAPH_NODE_SELECTOR);
            firstInlineNode = getFirstTextCursorNode(firstParagraph, options);

            return firstInlineNode ? Position.getOxoPosition(rootNode, firstInlineNode, 0) : null;
        }

        /**
         * Returns the last logical position in the document.
         * If the optional parameter node is specified, it returns the
         * last logical position inside this node. This optional parameter
         * can be used for text frames.
         *
         * @param {HTMLElement|jQuery} [node]
         *  An optional DOM element object whose last logical position
         *  will be searched. If this object is a jQuery collection,
         *  uses the first node it contains.
         *  If it is not specified, the page content node will be
         *  used as default.
         *
         * @returns {Array<Number>|Null}
         *  The last logical document position or Null, if it could not be
         *  determined.
         */
        function getLastPosition(node) {

            var // the node, in which the first position is searched
                searchNode = node || (DOM.getPageContentNode(rootNode).length > 0 ? DOM.getPageContentNode(rootNode) : rootNode),
                // the last span inside the specified node
                lastSpan = findDescendantNode(searchNode, function () { return DOM.isPortionSpan(this); }, { reverse: true });

            return lastSpan ? Position.getOxoPosition(rootNode, lastSpan, lastSpan.firstChild.nodeValue.length) : null;
        }

        /**
         * Returns the first logical text cursor position in the document
         * (skips leading floating drawing objects in the first paragraph).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.skipnodes=false]
         *      If set to true, the leading absolute positioned drawings in the
         *      first paragraph are skipped and the logical position behind
         *      these drawings is returned.
         *
         * @returns {Number[]|Null}
         *  The first logical text position in the document.
         *  Or null, if this is the empty selection (presentation only).
         */
        function getFirstTextPosition(options) {

            var // the page content node
                pageContentNode = DOM.getPageContentNode(rootNode),
                // the first in-line node inside the paragraph
                firstInlineNode = null,
                // the selector for finding the first paragraph node
                paraSelector = DOM.PARAGRAPH_NODE_SELECTOR,
                // the first paragraph node
                paragraph = null,
                // the number of leading absolute positioned drawings
                leadingAbsoluteDrawings = 0,
                // the number of absolute positioned drawings that are before the found text span
                prevCount = 0,
                // the logical position
                pos = null;

            // special case for empty presentation documents
            if (slideMode) {
                if (pageContentNode.children().length === 0) { return null; }
                paraSelector += ':not(.slide)'; // not searching text spans that are children of slides
            }

            paragraph = findDescendantNode(pageContentNode.length > 0 ? pageContentNode : rootNode, paraSelector);

            if (paragraph) {

                firstInlineNode = getFirstTextCursorNode(paragraph);

                if (firstInlineNode) {

                    pos = Position.getOxoPosition(rootNode, firstInlineNode, 0);

                    if (getBooleanOption(options, 'skipnodes', false)) {
                        leadingAbsoluteDrawings = Position.getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount(paragraph);
                        if (leadingAbsoluteDrawings > 0) {
                            prevCount = Position.getPreviousAbsoluteDrawingOrDrawingPlaceHolderCount(firstInlineNode); // checking for previous absolute drawings and drawing spacemakers (54008)
                            pos = Position.increaseLastIndex(pos, (leadingAbsoluteDrawings - prevCount));
                        }
                    }
                }
            }

            return pos;
        }

        /**
         * Clears the internal clipboard node.
         */
        function clearClipboardNode() {
            docApp.destroyImageNodes(clipboardNode);
            clipboardNode.empty().data('source-nodes', null);
        }

        /**
         * Checking whether a position is a possible text position.
         * This is a weak, but very fast check.
         * Another check is available in 'this.isValidTextSelection()',
         * that checks, whether the startPosition and the endPosition
         * contain valid positions.
         *
         * @param {Number[]} position
         *  The logical position to be checked.
         *
         * @returns {Boolean}
         *  Whether the given selection is a possible text selection.
         */
        function isPossibleTextSelection(position) {
            // only checking length of position array
            return position.length > 1;  // -> fast solution
            // the precise, but slow solution:
            // var localPos = _.clone(position);
            // localPos.pop();
            // return (Position.getParagraphElement(rootNode, localPos) !== null);
        }

        /**
         * Check, whether the selection was set into a text frame or whether
         * it is no longer a selection inside a text frame (64326).
         * For this two changes the events:
         * 'docs:textframe:enter' and
         * 'docs:textframe:leave' are triggered from the selection.
         */
        function checkTextFrameState() {

            var newSelectedTextFrameDrawing = selectedTextFrameDrawing;
            if (newSelectedTextFrameDrawing.length > 0 && DrawingFrame.isGroupDrawingFrame(newSelectedTextFrameDrawing)) { newSelectedTextFrameDrawing = Position.getClosestDrawingAtPosition(self.getRootNode(), self.getStartPosition()); }
            if (!newSelectedTextFrameDrawing) { newSelectedTextFrameDrawing = $(); }

            // triggering that a text frame node was entered
            if (newSelectedTextFrameDrawing.length > 0 && (!previousSelectedTextFrameDrawing || (previousSelectedTextFrameDrawing !== getDomNode(newSelectedTextFrameDrawing)))) {
                self.trigger('docs:textframe:enter', getDomNode(newSelectedTextFrameDrawing));
            }

            // triggering that a text frame node was left
            if (previousSelectedTextFrameDrawing && (newSelectedTextFrameDrawing.length === 0 || (previousSelectedTextFrameDrawing !== getDomNode(newSelectedTextFrameDrawing)))) {
                self.trigger('docs:textframe:leave', previousSelectedTextFrameDrawing);
            }
        }

        /**
         * Handling an optional selection of the text frame, if this is a (text)
         * selection inside a text frame. In this case this will be a 'double'
         * selection of text and surrounding text frame. This will not lead to
         * a drawing selection. The text selection stays the more important
         * selection.
         *
         * @param {HTMLElement|jQuery} node
         *  The DOM node containing the (text) selection.
         */
        function handleAdditionalTextFrameSelection(node) {

            var // a text frame node, that contains the specified node
                textFrameNode = $(node).closest(DrawingFrame.TEXTFRAME_NODE_SELECTOR),
                // the content node inside the drawing
                contentNode = null,
                // the textframe node inside the drawing
                textframeNode = null;

            selectedTextFrameDrawing = $();

            if (textFrameNode.length > 0) {
                // it is necessary to make a drawing selection
                selectedTextFrameDrawing = $(node).closest(DrawingFrame.NODE_SELECTOR);

                // only groups can be selected, not the drawings inside the group
                if (selectedTextFrameDrawing.hasClass('grouped')) {
                    selectedTextFrameDrawing = DrawingFrame.getFarthestGroupNode(rootNode, selectedTextFrameDrawing) || $();
                }

                // special handling for Mozilla browsers, to avoid, that the text frame
                // content is always in the foreground (36099)
                if ((_.browser.Firefox)) {
                    contentNode = DrawingFrame.getContentNode(selectedTextFrameDrawing);
                    if (_.isString(contentNode.attr('_moz_abspos'))) {
                        contentNode.removeAttr('_moz_abspos');
                    }
                    if (docApp.isSpreadsheetApp()) {
                        textframeNode = contentNode.children('.textframe');
                        if (_.isString(textframeNode.attr('_moz_abspos'))) {
                            textframeNode.removeAttr('_moz_abspos');
                        }
                    }
                }

                // drawing the selection around the drawing
                if (selectedTextFrameDrawing.length > 0) {
                    if (docModel.getSlideTouchMode()) {
                        // do not draw a drawing selection in slideTouchMode, just add the class for the state
                        selectedTextFrameDrawing.addClass('selected');
                    } else {
                        // avoiding new drawing selection, if drawing is resized, moved or rotated
                        if (docModel.isDrawingChangeActive()) { return; }

                        self.drawDrawingSelection(selectedTextFrameDrawing);
                        // -> this is not a real drawing selection, only the border is painted around the drawing
                        // -> the start and end position are not modified.
                        self.setActiveTracking(false);
                        // otherwise activeTracking is still set when switching from drawing selection to text
                        // selection inside drawing.
                    }

                }
            }

        }

        /**
         * Returns an array of DOM ranges representing the current browser
         * selection inside the passed container node.
         *
         * @param {HTMLElement|jQuery} containerNode
         *  The DOM node containing the returned selection ranges. Ranges not
         *  contained in this node will not be included into the result.
         *
         * @returns {Object}
         *  An object that contains a property 'active' with a DOM.Range object
         *  containing the current anchor point in its 'start' property, and
         *  the focus point in its 'end' property. The focus point may precede
         *  the anchor point if selecting backwards with mouse or keyboard. The
         *  returned object contains another property 'ranges' which is an
         *  array of DOM.Range objects representing all ranges currently
         *  selected. Start and end points of these ranges are already adjusted
         *  so that each start point precedes the end point.
         */
        function getBrowserSelection(containerNode) {

            var // the browser selection
                selection = window.getSelection(),
                // the result object
                result = { active: null, ranges: [] },
                // a single range object
                range = null,
                // the limiting points for valid ranges
                containerRange = DOM.Range.createRangeForNode(containerNode);

            // creates a DOM.Range object
            function createRange(startNode, startOffset, endNode, endOffset) {

                var // the range to be pushed into the array
                    range = DOM.Range.createRange(startNode, startOffset, endNode, endOffset),
                    // check that the nodes are inside the root node (with adjusted clone of the range)
                    adjustedRange = range.clone().adjust();

                return ((DOM.Point.comparePoints(containerRange.start, adjustedRange.start) <= 0) && (DOM.Point.comparePoints(adjustedRange.end, containerRange.end) <= 0)) ? range : null;
            }

            if (LOG_SELECTION) { globalLogger.info('Selection.getBrowserSelection(): reading browser selection...'); }

            var rangeCount = selection.rangeCount;
            // get anchor range which preserves direction of selection (focus node
            // may be located before anchor node)

            if (rangeCount >= 1) {
                var compPos = selection.anchorNode.compareDocumentPosition(selection.focusNode);
                // http://stackoverflow.com/questions/9180405/detect-direction-of-user-selection-with-javascript
                var backward = (!compPos && (selection.anchorOffset > selection.focusOffset)) || (compPos === window.Node.DOCUMENT_POSITION_PRECEDING);

                var fromRange = selection.getRangeAt(0);
                var toRange = selection.getRangeAt(rangeCount - 1);

                // fix for Bug 47820
                if (backward) {
                    result.active = createRange(toRange.endContainer, toRange.endOffset, fromRange.startContainer, fromRange.startOffset);
                } else {
                    result.active = createRange(fromRange.startContainer, fromRange.startOffset, toRange.endContainer, toRange.endOffset);
                }
                if (LOG_SELECTION) { globalLogger.log('  anchor=' + result.active); }
            }

            if (LOG_SELECTION) { globalLogger.log('selection.rangeCount=' + rangeCount); }

            // read all selection ranges
            for (var index = 0; index < rangeCount; index += 1) {
                // get the native selection Range object
                range = selection.getRangeAt(index);
                // translate to the internal text range representation
                range = createRange(range.startContainer, range.startOffset, range.endContainer, range.endOffset);
                // push, if range is valid
                if (range) {
                    if (LOG_SELECTION) { globalLogger.log('  ' + result.ranges.length + '=' + range); }
                    result.ranges.push(range.adjust());
                }
            }

            return result;
        }

        /**
         * Info: This must be a private function!
         * This function allows direct manipulation of the logical start and end
         * position. This is only allowed in private functions.
         * This function can be used in combination with the event 'position:calculated'.
         * Listener to this event must use this function to manipulate the logical
         * position(s).
         *
         * @param {Number[]} pos
         *  The new logical position.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.start=true]
         *      If set to true, the logical start position will be overwritten
         *      with the specified logical position. Otherwise the logical end
         *      position will be overwritten.
         *  @param {Boolean} [options.selectionExternallyModified=false]
         *      If set to true, the call of the function restoreBrowserSelection
         *      will be forced, after the 'position:calculated' event was triggered.
         *      This forcing is necessary, when the selection was modified by the
         *      triggered function and if the selection is a backward selection in IE.
         *      Otherwise the new expanded selection will not be visible.
         *      Important:
         *      Unfortunately using this flag has a negative side effect.
         *      It destroys the backward selection with shift-button in IE, because
         *      the 'backwards' information is lost after calling the function
         *      'restoreBrowserSelection'.
         *      Additionally a Chrome browser selection problem can be fixed by reducing
         *      the logical end position temporary by 1. In a multi paragraph selection
         *      the final paragraph selection is not shown correctly.
         */
        function updateLogicalPosition(pos, options) {

            var // whether the start or end position will be updated
                isStart = getBooleanOption(options, 'start', true);

            if (isStart) {
                startPosition = _.clone(pos);
            } else {
                endPosition = _.clone(pos);
            }

            selectionExternallyModified = getBooleanOption(options, 'selectionExternallyModified', false);
            endSelectionExternallyModified = selectionExternallyModified && !isStart;
        }

        /**
         * Sets the browser selection to the passed DOM ranges.
         *
         * @param {HTMLElement|jQuery} containerNode
         *  The DOM node that must contain the passed selection ranges. This
         *  node must be focusable, and will be focused after the browser
         *  selection has been set (except if specified otherwise, see options
         *  below).
         *
         * @param {DOM.Range[]|DOM.Range} ranges
         *  The DOM ranges representing the new browser selection. May be an
         *  array of DOM range objects, or a single DOM range object.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preserveFocus=false]
         *      If set to true, the DOM element currently focused will be
         *      focused again after the browser selection has been set. Note
         *      that doing so will immediately lose the new browser selection,
         *      if focus is currently inside a text input element.
         *  @param {Boolean} [options.processingBrowserEvent=false]
         *      Whether this function was called synchronously from the function
         *     'processBrowserEvent'.
         */
        function setBrowserSelection(containerNode, ranges, options) {

            // OT: Not destroying the local selection or tracking by an external operation
            //     -> but still allowing 'mousedown' selections triggered by 'processBrowserEvent' (DOCS-2309)
            //     -> Check: It might be smarter to block only those calls, triggered by function 'adaptLocalSelectionAfterOT'
            if (docApp.isOTEnabled() && ((docModel.isActiveMouseDownEvent() && !getBooleanOption(options, 'processingBrowserEvent', false)) || isTrackingActive)) { return; }

            var // the browser selection
                selection = window.getSelection(),
                // whether to restore old focus element (always restore, if application is not active)
                restoreFocus = !docApp.getView().isVisible() || getBooleanOption(options, 'preserveFocus', false),
                // the current focus element
                focusNode = $(getFocus());

            if (LOG_SELECTION) { globalLogger.info('Selection.setBrowserSelection(): writing browser selection...'); }

            // Bug 26283: Browsers are picky how to correctly focus and select text
            // in a node that is content-editable: Chrome and IE will change the
            // scroll position of the editable node, if it has been focused
            // explicitly while not having an existing browser selection (or even
            // if the selection is not completely visible). Furthermore, the node
            // will be focused automatically when setting the browser selection.
            // Firefox, on the other hand, wants to have the focus already set
            // before the browser selection can be changed, otherwise it may throw
            // exceptions. Additionally, changing the browser selection does NOT
            // automatically set the focus into the editable node.
            // Performance: This is very expensive and should only be executed, if
            // it is really necessary. Setting text selection after insertOperations
            // should not execute this code.
            if (!docApp.isSpreadsheetApp() && _.browser.Firefox && !options?.simpleTextSelection) {
                setFocus(containerNode);
            }

            // Clear the old browser selection.
            clearBrowserSelection();

            // convert to array
            ranges = ary.wrap(ranges);

            // single range: use attributes of the Selection object (anchor/focus)
            // directly to preserve direction of selection when selecting backwards
            if (SELECTION_COLLAPSE_EXPAND_SUPPORT && (ranges.length === 1) && !$(ranges[0].start.node).is('tr')) {
                if (LOG_SELECTION) { globalLogger.log('  0=' + ranges[0]); }
                try {
                    selection.collapse(ranges[0].start.node, ranges[0].start.offset);
                    selection.extend(ranges[0].end.node, ranges[0].end.offset);
                    ranges = [];
                } catch {
                    if (!(_.browser.Firefox && $(ranges[0].start.node).is('div.clipboard'))) {  // Fix for 26645, no warning required in Firefox
                        globalLogger.warn('Selection.setBrowserSelection(): failed to collapse/expand to range: ' + ranges[0]);
                    }
                    // retry with regular code below
                    selection.removeAllRanges();
                }
            }

            // Performance for Internet Explorer! Alternative way to set selection
            // Can be removed , if IE supports selection API and SELECTION_COLLAPSE_EXPAND_SUPPORT becomes true.
            // Problem: textRange.select(); requires a long time in large documents, similar with
            // selection.addRange(docRange);
            /*
            if (!SELECTION_COLLAPSE_EXPAND_SUPPORT && document.body.createTextRange && (ranges.length === 1) && !$(ranges[0].start.node).is('tr')) {

                // the text range object
                var textRange = document.body.createTextRange();

                if (textRange) {
                    textRange.moveToElementText(ranges[0].start.node.parentNode);
                    textRange.collapse(true);
                    textRange.move('character', ranges[0].start.offset);
                    textRange.select();  // similar time consuming like selection.addRange(docRange);
                    ranges = [];
                }
            }
            */

            // create a multi-selection
            _(ranges).each(function (range, index) {

                var docRange = null;

                if (LOG_SELECTION) { globalLogger.log('  ' + index + '=' + range); }
                try {
                    range.adjust();  // 26574, backward selection
                    docRange = window.document.createRange();
                    docRange.setStart(range.start.node, range.start.offset);
                    docRange.setEnd(range.end.node, range.end.offset);
                    // 'selection.addRange' is time expensive in IE. It must be replaced by 'selection.modify'
                    // for selected operations like insertText, as soon as this is available in IE.
                    // Alternatively 'SELECTION_COLLAPSE_EXPAND_SUPPORT' should be supported, if available.
                    selection.addRange(docRange);
                } catch {
                    globalLogger.error('Selection.setBrowserSelection(): failed to add range to selection: ' + range);
                }
            });

            // No visible browser selection of text spans that are children of slides -> setting browser selection into clip board
            // -> this is the mode for the 'slide selection'
            if (slideMode && (self.isTopLevelTextCursor() || self.isMultiSelection()) && getDomNode(containerNode) !== getDomNode(self.getClipboardNode())) {

                // check, if the user pressed 'F6'
                var isF6Event = docModel.getLastKeyDownEvent() && isF6AcessibilityKeyEvent(docModel.getLastKeyDownEvent());

                if (!isF6Event) {
                    self.setFocusIntoClipboardNode({ specialTouchHandling: true });
                    self.setBrowserSelectionToContents(self.getClipboardNode(), { preserveFocus: true });

                    // setting new values for start and end position
                    startPosition[startPosition.length - 1] = 0;
                    endPosition[endPosition.length - 1] = 0;
                }
            }

            // the Edge browser does not trigger a 'focusin' event after setting the browser selection into a shape or a
            // comment and a popup container is open (57895, 61622)
            // -> forcing a new focus event, so that the container will be closed.
            // -> it is important to set the focus into the textframe node, because this has 'contenteditable' set to true.
            if (_.browser.Edge && isCheckedButtonNode(getFocus())) {
                var textFrame = $(containerNode).find('div.textframe');
                if (textFrame.length > 0) { setFocus(textFrame); }
            }

            // restore the old focus element if specified
            if (restoreFocus && !focusNode.is(containerNode)) {
                setFocus(focusNode);
            }
        }

        /**
         * Returns true if the clipboard contains exactly the given drawings.
         */
        function clipboardContainsDrawings(drawings) {
            var clipboardDrawings = clipboardNode.data('source-nodes') || [];

            if (!_.isArray(drawings) || _.isEmpty(drawings) || (drawings.length !== clipboardDrawings.length)) { return false; }
            return _.every(drawings, function (drawing) {
                return (_.indexOf(clipboardDrawings, drawing) !== -1);
            });
        }

        /**
         * Initializes this selection with the passed start and end points, and
         * validates the browser selection by moving the start and end points
         * to editable nodes.
         *
         * @param {Object} browserSelection
         *  The new browser selection descriptor. See the method
         *  Selection.getBrowserSelection() for details.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preserveFocus=false]
         *      If set to true, the DOM element currently focused will be
         *      focused again after the browser selection has been set. Note
         *      that doing so will immediately lose the new browser selection,
         *      if focus is currently inside a text input element.
         *  @param {Object} [options.event]
         *      The original browser event that has caused the changed
         *      selection.
         *
         *  @param {Object} [newPositions]
         *   An object with a property 'start' and optionally a property 'end',
         *   that describes the logical position of the selection range. If this
         *   optional object is defined, it must be used for the simplified process
         *   of applying the browser selection. This simplified process avoids the
         *   recalculation of logical positions via Position.getTextLevelOxoPosition.
         */
        function applyBrowserSelection(browserSelection, options, newPositions) {

            var // active range from selection, preserving direction
                anchorPoint = null,
                focusPoint = null,
                // adjusted points (start before end)
                startPoint = null, endPoint = null,
                // a modified text node (for replacements of template texts)
                newTextNode = null,
                // whether position is a single cursor
                isCursor = false,
                // selected drawing node
                nodeInfo = null,
                // a temporary helper position
                tmpPosition = null,
                // the browser event passed to this method
                browserEvent = getOption(options, 'event'),
                // browser event from a touch device
                browserTouchEvent = TOUCH_DEVICE && _.isObject(browserEvent),
                // old selection, used to detect changed selection
                oldStartPosition = _.clone(startPosition), oldEndPosition = _.clone(endPosition),
                // the old selection type, used to detect changed selection
                oldType = self.getSelectionType(),
                // whether the browser selection shall be restored
                restoreSelection = getBooleanOption(options, 'restoreSelection', true),
                // whether this function was triggered by an insert operation, for example insertText
                insertOperation = getBooleanOption(options, 'insertOperation', false),
                // whether this function was triggered by an split paragraph operation
                splitOperation = getBooleanOption(options, 'splitOperation', false),
                // whether this function was triggered by an 'simplified text selection' operation, for example insertText, backspace, delete or return
                simpleTextSelection = getBooleanOption(options, 'simpleTextSelection', false),
                // whether this function was triggered by the IOS handling to reset selection after setting paragraph in read-only mode
                isIOSROHandling = getBooleanOption(options, 'simpleTextSelection', false),
                // whether a slide selection shall be set (special flag for performance reasons)
                slideSelection =  getBooleanOption(options, 'slideSelection', false),
                // whether the simplified process can be used to set the new text selection
                simplifiedProcess = !!newPositions,
                // whether the 'change' event must be triggered -> informing remote users about the current selection, even if it was not modified (31832)
                forceTrigger = getBooleanOption(options, 'forceTrigger', false),
                // whether an existing cell selection shall be restored
                keepCellSelection = getBooleanOption(options, 'keepCellSelection', false),
                // whether the change track pop up triggered this change of selection
                keepChangeTrackPopup = getBooleanOption(options, 'keepChangeTrackPopup', false),
                // whether a drawing inside a drawing group is selected
                drawingInGroupSelection = getBooleanOption(options, 'drawingInGroupSelection', false),
                // whether a drawing in the drawing layer is selected
                drawingLayerSelection = getBooleanOption(options, 'drawingLayerSelection', false),
                // whether position is top level (not in tables, text frames, etc. ) and is simple selection
                isTopLevelPosition = null,
                // whether special handling for complex fields of the placeholder is required (39099)
                handlePlaceHolderComplexField = false;

            // checking, that a selection range is not inside and outside a text frame
            // -> modifying one position, so that both are inside the same text frame
            function handleTextFrameSelectionRange() {

                var // an optional text frame for the start position
                    startTextFrame = Position.getClosestDrawingTextframe(rootNode, startPosition),
                    // an optional text frame for the end position
                    endTextFrame = Position.getClosestDrawingTextframe(rootNode, endPosition);

                if (startTextFrame) {
                    if (endTextFrame) {
                        // are start and end position inside the same text frame?
                        if (startTextFrame[0] !== endTextFrame[0]) { endPosition = getLastPosition(startTextFrame); }
                    } else {
                        endPosition = getLastPosition(startTextFrame);
                    }
                } else {
                    if (endTextFrame) { startPosition = getFirstPosition(endTextFrame); }
                }
            }

            if (simplifiedProcess) {

                // check for cell range selection (must be in the same table)
                cellRangeSelected =  false;
                tableSelected = false;

                if (!_.isArray(newPositions.start)) {
                    globalLogger.error('Selection.applyBrowserSelection(): no start position set in newPositions: ' + JSON.stringify(newPositions));
                }

                // if we apply selection on top level elements, we can avoid checks for inside tables and text frames
                isTopLevelPosition = newPositions.start.length === 2 && !newPositions.end;

                startPosition = _.clone(newPositions.start);
                endPosition = newPositions.end ? _.clone(newPositions.end) : _.clone(newPositions.start);

            } else if (keepCellSelection) {

                // simplified behavior for modifying position on remote clients -> this must not destroy local cell selection
                cellRangeSelected = true;

            } else {

                anchorPoint = browserSelection.active.start;
                focusPoint = browserSelection.active.end;

                // check for cell range selection (must be in the same table)
                cellRangeSelected = DOM.isCellRangeSelected(focusPoint.node, anchorPoint.node);

                if (cellRangeSelected) {

                    // cell range selection is always ordered, no need to check for direction
                    backwards = false;

                    // convert multi-selection for cells to rectangular cell selection
                    startPoint = _(browserSelection.ranges).first().start;
                    endPoint = _(browserSelection.ranges).last().end;

                } else {

                    // get range direction (check for real range, DOM.Point.comparePoints() is expensive)
                    isCursor = browserSelection.active.isCollapsed();

                    // Fix for 28344 in combination with special Firefox behavior for ranges ending directly behind list labels (see Position.getTextNodeFromCurrentNode)
                    if ((anchorPoint.node === focusPoint.node) && (anchorPoint.offset === focusPoint.offset - 1) && (DOM.isParagraphNode(anchorPoint.node)) && (DOM.isListLabelNode(anchorPoint.node.childNodes[anchorPoint.offset]))) {
                        // removing listlabels from selection
                        anchorPoint.offset = 1;
                        isCursor = true;
                    }

                    backwards = !isCursor && (DOM.Point.comparePoints(anchorPoint, focusPoint) > 0);
                    tableSelected = false;

                    // adjust start and end position
                    startPoint = backwards ? focusPoint : anchorPoint;
                    endPoint = backwards ? anchorPoint : focusPoint;

                    // checking for text frame template text, that will be removed
                    if (_.browser.IE && DOM.isTextFrameTemplateTextParagraph(anchorPoint.node)) {
                        anchorPoint.node = getDomNode($(anchorPoint.node).children(DOM.TEMPLATE_TEXT_SELECTOR).first()).firstChild; // using the text node instead of paragraph (49854)
                    }

                    if (DOM.isTextFrameTemplateTextNode(anchorPoint.node)) {

                        newTextNode = docModel.removeTemplateTextFromTextSpan(anchorPoint.node);

                        if (newTextNode) {
                            // getDomNode(mySpan).firstChild;
                            startPoint.node = newTextNode;
                            endPoint.node = startPoint.node;

                            startPoint.offset = 0;
                            endPoint.offset = 0;
                        }
                    }

                    // whether this is a selection inside a complex field of type PLACEHOLDER (to be handled in mouseup, not mousedown)
                    handlePlaceHolderComplexField = (!browserEvent || (browserEvent && browserEvent.type !== 'mousedown')) && DOM.isInsidePlaceHolderComplexField(startPoint.node.parentNode);
                }

                // calculate start and end position (always text positions, also in cell range mode)
                startPosition = Position.getTextLevelOxoPosition(startPoint, rootNode, false, !isCursor);
                endPosition = isCursor ? _.clone(startPosition) : Position.getTextLevelOxoPosition(endPoint, rootNode, true, !isCursor);

                // fast validity check, that start position is before end position (36249)
                if (!isCursor && !Position.isValidPositionOrder(startPosition, endPosition)) {
                    tmpPosition = startPosition;
                    startPosition = endPosition;
                    endPosition = tmpPosition;
                    globalLogger.warn('Selection.applyBrowserSelection(): switching of start and end position required');
                }

                // checking if one table is selected completely (Firefox only); selection type is 'cell' if 'cellRangeSelected' is true
                if (cellRangeSelected) { tableSelected = Position.isCompleteTableSelection(rootNode, startPosition, endPosition); }

                // repairing the old value for endPosition, if this is a selected drawing in a drawing group
                if (drawingInGroupSelection || drawingLayerSelection) { endPosition = Position.increaseLastIndex(endPosition); }

                if (!_.isArray(startPosition)) {
                    globalLogger.error('Selection.applyBrowserSelection(): no position for node ' + startPoint);
                }
                if (!_.isArray(endPosition)) {
                    globalLogger.error('Selection.applyBrowserSelection(): no position for node ' + endPoint);
                }
                if (!_.isArray(startPosition) || !_.isArray(endPosition) || !isPossibleTextSelection(startPosition) || !isPossibleTextSelection(endPosition)) {
                    startPosition = getFirstTextPosition();
                    endPosition = _.clone(startPosition);
                    isCursor = true;
                }

                // check for right click in Safari (this range might have been generated by the Safari browser on right click) (61460/61568/61607/61618)
                if (selectionStore) {
                    if (!isCursor && (!_.isEqual(selectionStore.startPosition, startPosition) || !_.isEqual(selectionStore.endPosition, endPosition))) {
                        endPosition = startPosition;
                        isCursor = true;
                    }
                    selectionStore = null; // deleting the selectionStore after using it once
                }
            }

            // check for drawing selection
            if (selectedDrawing.length > 0) {
                nodeInfo = Position.getDOMPosition(rootNode, startPosition, true);
                // Not modifying selection, if this was a click on a drawing inside a drawing group
                if (nodeInfo && DrawingFrame.isDrawingFrame(nodeInfo.node) && $(nodeInfo.node).hasClass('grouped') && selectedDrawing[0] === DrawingFrame.getGroupNode(nodeInfo.node)[0]) {
                    return;
                }

                self.clearDrawingSelection(selectedDrawing);
                if (!slideMode) { docApp.getView().clearVisibleDrawingAnchor(); }
                selectedDrawing = $();
            }

            // saving on optionally selected text frame node (in a group this must be the child drawing that contains the selection)
            previousSelectedTextFrameDrawing = selectedTextFrameDrawing.length > 0 ? getDomNode(selectedTextFrameDrawing) : null;
            if (previousSelectedTextFrameDrawing && DrawingFrame.isGroupDrawingFrame(previousSelectedTextFrameDrawing)) {
                previousSelectedTextFrameDrawing = Position.getClosestDrawingAtPosition(self.getRootNode(), oldStartPosition);
                previousSelectedTextFrameDrawing = previousSelectedTextFrameDrawing ? getDomNode(previousSelectedTextFrameDrawing) : null;
            }

            // check for an additional text frame selection, that is no pure drawing selection
            if (selectedTextFrameDrawing.length > 0) {
                self.clearDrawingSelection(selectedTextFrameDrawing, options);
                if (!slideMode) { docApp.getView().clearVisibleDrawingAnchor(); }
                if (slideMode && (!simplifiedProcess || slideSelection) &&
                    // Without this switch the placeholder text is missing if the user click in a placeholder and then in
                    // an other placeholder
                    (_.browser.IE || (!browserEvent || browserEvent.type !== 'mouseup')) &&
                    !Position.hasSameParentComponent(oldStartPosition, startPosition, 2)) {
                    if (docModel.drawingStyles.handleEmptyPlaceHolderDrawing) {
                        docModel.drawingStyles.handleEmptyPlaceHolderDrawing(selectedTextFrameDrawing);
                    }
                }
                selectedTextFrameDrawing = $();
            }

            // removing a drawing multi selection
            if (self.isMultiSelectionSupported() && self.isMultiSelection()) { self.clearMultiSelection(); }

            // draw a new drawing selection
            if (!simplifiedProcess && !cellRangeSelected && (self.isSingleComponentSelection() || drawingInGroupSelection || drawingLayerSelection)) {
                nodeInfo = nodeInfo || Position.getDOMPosition(rootNode, startPosition, true);
                if (nodeInfo && DrawingFrame.isDrawingFrame(nodeInfo.node)) {
                    selectedDrawing = $(nodeInfo.node);
                    // only groups can be selected, not the drawings inside the group
                    if (selectedDrawing.hasClass('grouped')) {
                        selectedDrawing = DrawingFrame.getFarthestGroupNode(rootNode, selectedDrawing);
                        startPosition = Position.getOxoPosition(rootNode, selectedDrawing, 0);
                        endPosition = Position.getOxoPosition(rootNode, selectedDrawing, 1);
                    }
                    self.drawDrawingSelection(selectedDrawing);
                }
            }

            // update table selection
            // Performance: NOT required for insertText, insertTab, splitParagraph, ..., but might be necessary for backspace, delete and return
            if (!insertOperation && !splitOperation) {
                if (!isTopLevelPosition) {
                    selectedTable.removeClass('selected');
                    selectedTable = $(self.getEnclosingTable({ onlyTableNode: true })).addClass('selected');
                }

                // update the clipboard (clone drawing contents, otherwise clear)
                // Performance: NOT required for insertText
                self.updateClipboardNode();
            }

            // checking, that a selection range is not inside and outside a textframe
            if (!isCursor && !isTopLevelPosition && selectedDrawing.length === 0) { handleTextFrameSelectionRange(); }

            // informing others about the new selection
            self.trigger('position:calculated', updateLogicalPosition, { handlePlaceHolderComplexField });

            // refreshing local variable isCursor might be necessary
            if (handlePlaceHolderComplexField) {
                isCursor = _.isEqual(self.getStartPosition(), self.getEndPosition());
                // updating 'backwards' after switching vom cursor to range; needs to be true for cursorLeft and cursorUp
                if (!isCursor) { backwards = isBackwardCursor; }
            }

            // draw correct browser selection (but not if browser does not
            // support backward selection mode, or on touch devices to keep the
            // text selection menu open after a double tap event)
            if (restoreSelection && !slideSelection && (isCursor || !backwards || SELECTION_COLLAPSE_EXPAND_SUPPORT || selectionExternallyModified)) {
                if (!browserTouchEvent) {
                    options = { ...options, isCursor };
                    self.restoreBrowserSelection(options);
                } else if (self.getSelectionType() === 'text') {
                    //handleAdditionalTextFrameSelection is a newer feature, which is called by restoreBrowserSelection
                    //but not on touch, so we call it now directly
                    startPoint = Position.getDOMPosition(rootNode, startPosition);
                    if (startPoint && startPoint.node) { handleAdditionalTextFrameSelection(startPoint.node); }
                }

                selectionExternallyModified = false; // call of restoreBrowserSelection was forced by 'updateLogicalPosition'
            }

            // checking, if a textframe node was entered or left (using previousSelectedTextFrameDrawing)
            checkTextFrameState();

            // notify listeners
            if ((!_.isEqual(startPosition, oldStartPosition) || !_.isEqual(endPosition, oldEndPosition) || oldType !== self.getSelectionType()) || forceTrigger || forceSelectionChangeEvent) {
                self.trigger('change', { insertOperation, splitOperation, simpleTextSelection, isIOSROHandling, keepChangeTrackPopup, event: browserEvent });
                forceSelectionChangeEvent = false;  // needed after switching the target
            }
            self.trigger('update', { simpleTextSelection, slideSelection, event: browserEvent });
            self.trigger('changecursorinfo', { start: _.copy(startPosition), end: _.copy(endPosition), rootNode: self.getRootNode(), target: docModel.getActiveTarget() });
        }

        /**
         * Changes the current text position or selection by one character or
         * inline component.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.extend=false]
         *      If set to true, the current selection will be extended at the
         *      current focus point. Otherwise, the text cursor will be moved
         *      starting from the current focus point.
         *  @param {Boolean} [options.backwards=false]
         *      If set to true, the selection will be moved back by one
         *      character; otherwise the selection will be moved ahead.
         *  @param {Boolean} [options.verticalCellSelection=false]
         *      If set to true, the selection will be converted from a
         *      text selection to a vertical cell selection. This can be used
         *      in Firefox only, where multi-cell selection is possible.
         */
        function moveTextCursor(options) {

            var // whether to extend the selection
                extend = getBooleanOption(options, 'extend', false),
                // whether to move cursor back
                backwards = getBooleanOption(options, 'backwards', false),
                // whether the selection has to be converted into a vertical cell selection
                verticalCellSelection = getBooleanOption(options, 'verticalCellSelection', false),
                // text node at the current focus position
                focusNodeInfo = Position.getDOMPosition(rootNode, getFocusPosition()),
                // text node at the current anchor position (changes with focus node without SHIFT key)
                anchorNodeInfo = extend ? Position.getDOMPosition(rootNode, getAnchorPosition()) : focusNodeInfo,
                // the text node at the anchor position
                anchorTextNode = anchorNodeInfo && anchorNodeInfo.node && (anchorNodeInfo.node.nodeType === 3) ? anchorNodeInfo.node : null,
                // the text node at the focus position
                focusTextNode = focusNodeInfo && focusNodeInfo.node && (focusNodeInfo.node.nodeType === 3) ? focusNodeInfo.node : null,
                // space for other nodes
                node = null;

            // find the closest following inline node of the passed node
            function findNextNonEmptyInlineNode(node) {
                return findNextSiblingNode(node, inlineNodeSelector);
            }

            // find the closest preceding inline node of the passed node
            function findPreviousNonEmptyInlineNode(node) {
                return findPreviousSiblingNode(node, inlineNodeSelector);
            }

            // checks if node is before special complex field, and returns end range of that field
            function findNextValidNodeAfterSpecialField(node) {
                var nextSib = node.nextSibling,
                    nextNextSib = nextSib ? nextSib.nextSibling : null;

                if (DOM.isMarginalNode(node) && DOM.isRangeMarkerStartNode(nextSib) && (nextNextSib && DOM.isSpecialField(nextNextSib))) {
                    node = nextNextSib.nextSibling;
                    if (DOM.isRangeMarkerEndNode(node)) {
                        return node;
                    }
                }
                return null;
            }

            // check, if the specified point describes a position, that need to
            // be handled in a special way, caused by a preceding end range marker.
            // If the offset is '1', so that it becomes '0' after moving cursor
            // in backward direction, typically the node is changed to the previous
            // node. But not so, if there is a preceding range end marker node.
            // And also not, if the preceding node is a comment place holder node
            // that follows an end range marker node (typical schema in OOXML).
            // And if there is an empty text span between the end range marker node
            // and the following comment place holder node, this is also handled
            // within this function.
            //
            // If this function return 'true' this means, that the node defined by
            // point.node shall stay valid. And only the offset is reduced to 0.
            // Typically the previous node is searched, if offset is reduced to 0.
            function stayBehindPreviousNode(point) {

                var // the parent node of the specified point node
                    parent = null,
                    // the previous node
                    previous = null;

                if (point.offset === 1) { // index must be 1 and new index becomes 0

                    parent = point.node.parentNode;
                    previous = parent.previousSibling;

                    if (!previous) { return false; }

                    // previous node is range marker end node or drawing that is positioned inside the paragraph
                    if (DOM.isRangeMarkerEndNode(previous) || DOM.isAbsoluteParagraphDrawing(previous)) { return true; }

                    if (DOM.isCommentPlaceHolderNode(previous)) {

                        previous = previous.previousSibling;

                        if (!previous) { return false; }

                        // found range marker end node before a comment place holder node
                        if (DOM.isRangeMarkerEndNode(previous)) { return true; }

                        if (DOM.isEmptySpan(previous)) {

                            previous = previous.previousSibling;

                            if (!previous) { return false; }

                            // found range marker end node before an empty text span before a comment place holder node
                            if (DOM.isRangeMarkerEndNode(previous)) { return true; }
                        }
                    }
                }

                return false;
            }

            // find the first inline node or empty node of the following paragraph
            function findFirstInlineNodeInNextParagraph(node) {
                var containerNode = DOM.isPageNode(rootNode) ? DOM.getPageContentNode(rootNode) : rootNode,
                    skipSelector = DOM.SKIP_DRAWINGS_AND_P_BREAKS_SELECTOR,
                    paragraph = findNextNode(containerNode, node, DOM.PARAGRAPH_NODE_SELECTOR, skipSelector);
                return paragraph && getFirstTextCursorNode(paragraph);
            }

            // find the last inline node or empty node of the preceding paragraph
            function findLastInlineNodeInPreviousParagraph(node) {
                var paragraph = findClosest(rootNode, node, DOM.PARAGRAPH_NODE_SELECTOR),
                    containerNode = DOM.isPageNode(rootNode) ? DOM.getPageContentNode(rootNode) : rootNode,
                    skipSelector = DOM.SKIP_DRAWINGS_AND_P_BREAKS_SELECTOR;
                paragraph = paragraph && findPreviousNode(containerNode, paragraph, DOM.PARAGRAPH_NODE_SELECTOR, skipSelector);
                return paragraph && getLastTextCursorNode(paragraph);
            }

            // helper function to jump over leading paragraph aligned drawings of a paragraph
            function skipOverLeadingParagraphDrawings(node) {
                while (node && DOM.isEmptySpan(node) && node.nextSibling && DOM.isAbsoluteParagraphDrawing(node.nextSibling)) {
                    node = node.nextSibling.nextSibling;
                }
                return node;
            }

            // move to start of text span; or to end of text span preceding the inline component
            function jumpBeforeInlineNode(node) {
                if (DOM.isTextSpan(node)) {
                    if (DOM.isEmptySpan(node)) { node = skipOverLeadingParagraphDrawings(node); } // skip over all paragraph aligned drawings
                    // text span: jump to its beginning
                    focusNodeInfo.node = node.firstChild;
                    focusNodeInfo.offset = 0;
                } else if (DOM.isTextSpan(node.previousSibling)) {
                    // jump to end of the span preceding the inline component
                    focusNodeInfo.node = node.previousSibling.firstChild;
                    focusNodeInfo.offset = focusNodeInfo.node.nodeValue.length;
                } else {
                    globalLogger.warn('Selection.moveTextCursor.jumpBeforeInlineNode(): missing text span preceding a component node');
                }
            }

            // skip inline component; move to end or to specific offset of text span
            function jumpOverInlineNode(node, offset) {
                if (DOM.isTextSpan(node)) {
                    // text span: jump to passed offset, or to its end
                    focusNodeInfo.node = node.firstChild;
                    focusNodeInfo.offset = _.isNumber(offset) ? Math.max(0, offset) : focusNodeInfo.node.nodeValue.length;

                } else if (DOM.isTextSpan(DOM.getAllowedNeighboringNode(node))) {
                    // jump to beginning of the span following the inline component
                    // (may be an empty span before floating node)
                    focusNodeInfo.node = DOM.getAllowedNeighboringNode(node).firstChild;
                    focusNodeInfo.offset = 0;
                } else {
                    globalLogger.warn('Selection.moveTextCursor.jumpOverInlineNode(): missing text span following a component node');
                }
            }

            // setting a cell selection instead of a text selection
            function switchToCellSelectionHorz() {

                var // an array of ranges for cell selections
                    ranges = [],
                    // the previous or following row node required for cell selections
                    siblingRow = null,
                    // a temporarily required node
                    helperNode = null;

                // try to find the nearest table cell containing the text node
                focusNodeInfo = DOM.Point.createPointForNode($(focusTextNode).closest(DOM.TABLE_CELLNODE_SELECTOR)[0]);
                anchorNodeInfo = DOM.Point.createPointForNode($(focusTextNode).closest(DOM.TABLE_CELLNODE_SELECTOR)[0]);

                if (backwards) {
                    // selecting the cell and its previous sibling, if available (the previous sibling has to be the active cell)
                    focusNodeInfo.offset += 1;
                    if (anchorNodeInfo.offset > 0) {

                        helperNode = DOM.Point.createPointForNode($(focusTextNode).closest(DOM.TABLE_CELLNODE_SELECTOR)[0]);
                        anchorNodeInfo.offset -= 1;

                        ranges.push({ start: anchorNodeInfo, end: helperNode });
                        ranges.push({ start: helperNode, end: focusNodeInfo });

                        self.setAnchorCellRange(ranges[1]);

                    } else {
                        // the complete row and the previous row need to be selected
                        if ($(focusNodeInfo.node).prev().length > 0) {
                            siblingRow = $(focusNodeInfo.node).prev();
                            ranges.push(new DOM.Range(new DOM.Point(siblingRow, 0), new DOM.Point(siblingRow, siblingRow.children().length)));
                            ranges.push(new DOM.Range(new DOM.Point(focusNodeInfo.node, 0), new DOM.Point(focusNodeInfo.node, $(focusNodeInfo.node).children().length)));
                            self.setAnchorCellRange(new DOM.Range(ranges[1].start, new DOM.Point(ranges[1].start.node, ranges[1].start.offset + 1)));
                        } else {
                            // Fix for 29402, not changing selection
                            if (self.isTextLevelSelection()) {
                                self.setTextSelection(self.getEndPosition(), Position.getFirstPositionInCurrentCell(rootNode, self.getEndPosition()));
                            }
                        }
                    }
                } else {
                    // selecting the cell and its following sibling, if available
                    if (focusNodeInfo.offset + 1 < $(focusNodeInfo.node).children().length) {
                        focusNodeInfo.offset += 2;  // selecting the current and the following cell
                        ranges.push({ start: anchorNodeInfo, end: focusNodeInfo });
                        self.setAnchorCellRange(new DOM.Range(anchorNodeInfo, new DOM.Point(anchorNodeInfo.node, anchorNodeInfo.offset + 1)));
                    } else {
                        // the complete row and the next row need to be selected
                        if ($(focusNodeInfo.node).next().length > 0) {
                            siblingRow = $(focusNodeInfo.node).next();
                            ranges.push(new DOM.Range(new DOM.Point(focusNodeInfo.node, 0), new DOM.Point(focusNodeInfo.node, $(focusNodeInfo.node).children().length)));
                            ranges.push(new DOM.Range(new DOM.Point(siblingRow, 0), new DOM.Point(siblingRow, siblingRow.children().length)));
                            self.setAnchorCellRange(new DOM.Range(new DOM.Point(ranges[0].end.node, ranges[0].end.offset - 1), ranges[0].end.node));
                        }
                    }
                }

                self.setCellSelection(ranges);
            }

            // setting a cell selection instead of a text selection
            function switchToCellSelectionVert() {

                var // an array of ranges for cell selections
                    ranges = [],
                    // the previous or following row node required for cell selections
                    siblingRow = null;

                // try to find the nearest table cell containing the text node
                if ($(focusTextNode).closest(DOM.TABLE_CELLNODE_SELECTOR).length === 0) {
                    // Fix for 29402, not changing selection
                    if (self.isTextLevelSelection()) {
                        if (backwards) {
                            self.setTextSelection(Position.getFirstPositionInCurrentCell(rootNode, self.getEndPosition()), self.getEndPosition());
                        } else {
                            self.setTextSelection(self.getStartPosition(), Position.getLastPositionInCurrentCell(rootNode, self.getStartPosition()));
                        }
                    }
                } else {

                    focusNodeInfo = DOM.Point.createPointForNode($(focusTextNode).closest(DOM.TABLE_CELLNODE_SELECTOR)[0]);

                    if (backwards) {
                        // selecting the cell and its previous sibling in the row above, if available (the previous sibling has to be the active cell)
                        if ($(focusNodeInfo.node).prev().length > 0) {
                            siblingRow = $(focusNodeInfo.node).prev();
                            while (siblingRow.hasClass('pb-row') && siblingRow.prev().length > 0) { siblingRow = siblingRow.prev(); } // skip over page break row
                            ranges.push(new DOM.Range(new DOM.Point(siblingRow, focusNodeInfo.offset), new DOM.Point(siblingRow, focusNodeInfo.offset + 1)));
                            ranges.push(new DOM.Range(focusNodeInfo, new DOM.Point(focusNodeInfo.node, focusNodeInfo.offset + 1)));
                            self.setAnchorCellRange(ranges[ranges.length - 1]);
                        }
                    } else {
                        if ($(focusNodeInfo.node).next().length > 0) {
                            siblingRow = $(focusNodeInfo.node).next();
                            while (siblingRow.hasClass('pb-row') && siblingRow.next().length > 0) { siblingRow = siblingRow.next(); } // skip over page break row
                            ranges.push(new DOM.Range(focusNodeInfo, new DOM.Point(focusNodeInfo.node, focusNodeInfo.offset + 1)));
                            ranges.push(new DOM.Range(new DOM.Point(siblingRow, focusNodeInfo.offset), new DOM.Point(siblingRow, focusNodeInfo.offset + 1)));
                            self.setAnchorCellRange(ranges[0]);
                        }
                    }

                    self.setCellSelection(ranges);
                }
            }

            // check anchor and focus position
            if (!anchorTextNode || !focusTextNode) {

                if (!focusTextNode && focusNodeInfo && focusNodeInfo.node && DOM.isDrawingLayerNode(focusNodeInfo.node.parentNode)) {
                    focusNodeInfo.node = DOM.getDrawingPlaceHolderNode(focusNodeInfo.node).nextSibling.firstChild;
                    focusTextNode = focusNodeInfo && focusNodeInfo.node && (focusNodeInfo.node.nodeType === 3) ? focusNodeInfo.node : null;
                }

                if (!anchorTextNode) {
                    if (extend) {
                        if (anchorNodeInfo && anchorNodeInfo.node && DOM.isDrawingLayerNode(anchorNodeInfo.node.parentNode)) {
                            anchorNodeInfo.node = DOM.getDrawingPlaceHolderNode(anchorNodeInfo.node).nextSibling.firstChild;
                        }
                    } else {
                        anchorNodeInfo = focusNodeInfo;
                    }

                    anchorTextNode = anchorNodeInfo && anchorNodeInfo.node && (anchorNodeInfo.node.nodeType === 3) ? anchorNodeInfo.node : null;
                }

                if (!anchorTextNode || !focusTextNode) {
                    globalLogger.warn('Selection.moveTextCursor(): missing valid text position');
                    return false;
                }
            }

            // update focusNodeInfo according to the passed direction
            if (backwards) {

                if (verticalCellSelection && !cellRangeSelected && Position.isSameTableLevel(rootNode, self.getFocusPosition(), self.getAnchorPosition())) {
                    switchToCellSelectionVert();
                    return true;
                }

                if (!extend && self.hasRange() && self.isTextSelection()) { // DOCS-2932
                    self.setTextSelection(self.getStartPosition()); // switching from selected range to cursor selection
                    return true;
                }

                // move back inside non-empty text span, but not always to the beginning
                if (focusNodeInfo.offset > 1) {
                    focusNodeInfo.offset -= 1;
                    if (unicode.hasSurrogatePair($(focusNodeInfo.node).text(), focusNodeInfo.offset - 1)) { focusNodeInfo.offset--; } // taking care of unicode

                // stay behind range marker end nodes or comment place holder nodes
                } else if (stayBehindPreviousNode(focusNodeInfo)) {
                    focusNodeInfo.offset = 0;  // not changing the node

                // try to find the previous non-empty inline node in the own paragraph
                } else if ((node = findPreviousNonEmptyInlineNode(focusTextNode.parentNode))) {

                    // offset is 1, or preceding node is a text span: move cursor behind the previous inline node
                    if ((focusNodeInfo.offset === 1) || DOM.isTextSpan(node)) {
                        const prevSibling = focusTextNode.parentNode.previousSibling;
                        if (focusNodeInfo.offset === 0 && ($(prevSibling).hasClass('page-break') || DOM.isAbsoluteParagraphDrawing(prevSibling) || DOM.isDrawingPlaceHolderNode(prevSibling) || DOM.isDrawingSpaceMakerNode(prevSibling) || DOM.isRangeMarkerNode(node.nextSibling))) {
                            // cursor is on first position before page break inside paragraph,
                            // and it has to jump to last position on previous page
                            // Check for image place holder node or space maker node is required
                            // for backwards travelling with cursor over node introduced with absolute
                            // positioned drawings.
                            // Also reducing the offset by 1, if there was a jump over a range marker end node.
                            if (DOM.isRangeMarkerNode(node.nextSibling) && DOM.isRangeStartWithCheckboxAttr(node.nextSibling)) {
                                jumpOverInlineNode(node, node.firstChild.nodeValue.length); // rangestart node with checkbox attribute has only empty span between it and rangeend
                            } else {
                                jumpOverInlineNode(node, node.firstChild.nodeValue.length - 1); // TODO: special chars cannot be enumerated with text length of node
                            }

                            if ((focusNodeInfo.node.length === 1) && (focusNodeInfo.offset === 0) && (DOM.isFirstTextPositionBehindRangeStart(focusNodeInfo.node, focusNodeInfo.offset))) {
                                // special handling to jump completely over ranges, that have a length of 1
                                jumpOverRangeMarkerStartNode(focusNodeInfo); // jumping further to the left
                            }

                        } else {
                            jumpOverInlineNode(node);
                        }
                    // offset is 0, skip the previous inline component (jump to end of its preceding inline node)
                    } else {
                        // jump to end of preceding text span
                        jumpOverInlineNode(DOM.getAllowedNeighboringNode(node, { next: false }));

                        // try to find the correct trailing text span of the pre-preceding inline component
                        // (this may jump from the preceding empty span over floating nodes to the trailing
                        // text span of the previous inline component)
                        if (DOM.isEmptySpan(node.previousSibling) && (node = findPreviousNonEmptyInlineNode(node))) {
                            // skipping an inline node: jump to end of preceding text span (try to
                            // find text span following the next preceding component)
                            jumpOverInlineNode(node);
                        }
                    }

                // after first character in paragraph: go to beginning of the first text span
                } else if (focusNodeInfo.offset === 1) {
                    focusNodeInfo.offset = 0;

                // in Mozilla based browsers a cell selection can be created inside tables
                } else if (extend && _.browser.Firefox && !cellRangeSelected &&
                    Position.isPositionInTable(rootNode, getFocusPosition()) &&
                    Position.isFirstPositionInTableCell(rootNode, getFocusPosition()) &&
                    Position.isSameTableLevel(rootNode, getFocusPosition(), self.getAnchorPosition())
                ) {
                    switchToCellSelectionHorz();
                    return true;

                // block cursor traversal, if this is the first position inside a text frame
                } else if (focusNodeInfo.offset === 0 && DOM.isFirstTextSpanInTextframe(focusTextNode.parentNode)) {
                    if (extend) {
                        self.restoreBrowserSelection(); // this call is asynchronous -> avoid uncontrolled horizontal move
                    } else {
                        self.setTextSelection(self.getStartPosition());
                    }
                    return true;

                // try to find the last text position in the preceding paragraph
                } else if ((node = findLastInlineNodeInPreviousParagraph(focusTextNode.parentNode))) {
                    jumpOverInlineNode(node);
                }

            } else {

                if (verticalCellSelection && !cellRangeSelected && Position.isSameTableLevel(rootNode, self.getAnchorPosition(), self.getFocusPosition())) {
                    switchToCellSelectionVert();
                    return true;
                }

                if (!extend && self.hasRange() && self.isTextSelection()) { // DOCS-2932
                    self.setTextSelection(self.getEndPosition()); // switching from selected range to cursor selection
                    return true;
                }

                // move ahead inside non-empty text span (always up to the end of the span)
                if (focusNodeInfo.offset < focusTextNode.nodeValue.length) {
                    focusNodeInfo.offset += 1;
                    if (unicode.hasSurrogatePair($(focusNodeInfo.node).text(), focusNodeInfo.offset - 1)) { focusNodeInfo.offset++; } // taking care of unicode

                    // check for following range marker end nodes
                    if ((focusNodeInfo.offset === focusTextNode.nodeValue.length) && DOM.isLastTextPositionBeforeRangeEnd(focusNodeInfo.node, focusNodeInfo.offset)) {
                        jumpOverRangeMarkerEndNode(focusNodeInfo);
                    }
                // try to jump over special page fields in header/footer
                } else if ((node = findNextValidNodeAfterSpecialField(focusTextNode.parentNode))) {
                    jumpOverInlineNode(node, 1);
                // try to find the next non-empty inline node in the own paragraph
                } else if ((node = findNextNonEmptyInlineNode(focusTextNode.parentNode))) {
                    if (DOM.isRangeMarkerNode(focusTextNode.parentNode.nextSibling) && DOM.isRangeStartWithCheckboxAttr(focusTextNode.parentNode.nextSibling)) {
                        jumpOverInlineNode(node, 0); // rangestart node with checkbox attribute has only empty span between it and rangeend
                    } else {
                        jumpOverInlineNode(node, 1); // skip only the first character of following non-empty text span
                        if (unicode.hasSurrogatePair($(focusNodeInfo.node).text(), focusNodeInfo.offset - 1)) { focusNodeInfo.offset++; } // taking care of unicode
                    }

                    // this first character might be the only character inside a range, so that
                    // the position must be behind the range end marker
                    if (focusNodeInfo.node.length === 1 && DOM.isLastTextPositionBeforeRangeEnd(focusNodeInfo.node, focusNodeInfo.offset)) {
                        jumpOverRangeMarkerEndNode(focusNodeInfo);
                    }

                // in Mozilla based browsers a cell selection can be created inside tables
                } else if (extend && _.browser.Firefox && !cellRangeSelected &&
                    Position.isPositionInTable(rootNode, getFocusPosition()) &&
                    Position.isLastPositionInTableCell(rootNode, getFocusPosition()) &&
                    Position.isSameTableLevel(rootNode, self.getAnchorPosition(), getFocusPosition())
                ) {
                    switchToCellSelectionHorz();
                    return true;

                // block cursor traversal, if this is the last position inside a text frame
                } else if (focusNodeInfo.offset === focusTextNode.nodeValue.length && DOM.isLastTextSpanInTextframe(focusTextNode.parentNode)) {
                    if (extend) {
                        self.restoreBrowserSelection(); // this call is asynchronous -> avoid uncontrolled horizontal move
                    } else {
                        self.setTextSelection(self.getEndPosition());
                    }
                    return true;
                // try to find the first text position in the next paragraph
                } else if ((node = findFirstInlineNodeInNextParagraph(focusTextNode.parentNode))) {
                    jumpBeforeInlineNode(node);
                // cursor right in Chrome at the last position inside the root node (simply stay at current position)
                } else if (_.browser.Chrome && (focusNodeInfo.offset === focusTextNode.nodeValue.length) && !isLastDescendant(rootNode, focusTextNode.parentNode)) {
                    self.restoreBrowserSelection();  // avoid uncontrolled vertical browser scrolling (40052)
                    docApp.getView().recalculateDocumentMargin();
                    return true;
                }
            }

            // update browser selection if focusNodeInfo still valid
            if (focusNodeInfo.node) {
                applyBrowserSelection({ active: new DOM.Range(anchorNodeInfo, focusNodeInfo), ranges: [] });
            }

            return true;
        }

        /**
         * Converts the passed logical text position to a DOM point pointing to
         * a DOM text node as used by the internal browser selection.
         *
         * @param {Number[]} position
         *  The logical position of the target node. Must be the position of a
         *  paragraph child node, either a text span, a text component (fields,
         *  tabs), or a drawing node.
         *
         * @param {Object} [point]
         *  Performance: The point containing node and offset as calculated
         *  from Position.getDOMPosition. Saving the object, that was calculated
         *  before, saves the call of Position.getDOMPosition.
         *
         * @param {Object} [options]
         *  An object containing some additional properties:
         *  @param {Boolean} [options.forceTextPosition=false]
         *      Whether the point for the text position must be a real text in
         *      a text span and not for example a drawing.
         *
         * @returns {DOM.Point|Null}
         *  The DOM.Point object representing the passed logical position, or
         *  null, if the passed position is invalid.
         */
        function getPointForTextPosition(position, point, options) {

            var forceTextPosition = getBooleanOption(options, 'forceTextPosition', false),
                nodeInfo = point ? point : Position.getDOMPosition(rootNode, position, !forceTextPosition),
                node = nodeInfo ? nodeInfo.node : null,
                savedNode = null;

            if (node && node.nodeType === 3 && (point || forceTextPosition)) {
                node = node.parentNode; // saved node could be text node instead of text span
            }

            // check that the position selects a paragraph child node
            if (!node || !DOM.isParagraphNode(node.parentNode)) {
                // this might be a drawing inside a drawing group
                if (node && node.parentNode && DrawingFrame.isGroupContentNode(node.parentNode)) {
                    return new DOM.Point(node, 0);
                } else if (node && node.parentNode && DOM.isDrawingLayerNode(node.parentNode)) {
                    return new DOM.Point(node, 0);
                } else {
                    return null;
                }
            }

            if (DOM.isTextSpan(node)) {
                return new DOM.Point(node.firstChild, nodeInfo.offset);
            }

            savedNode = node;
            node = findPreviousSiblingNode(node, function () { return DOM.isTextSpan(this); });

            if (!node) { // increasing resilience, searching also for text spans without text node (DOCS-3026, DOCS-3028)
                node = findPreviousSiblingNode(savedNode, function () { return DOM.isTextSpanWithoutTextNode(this); });
                if (node) {
                    _.each($(node.parentNode).children('span'), function (span) { DOM.ensureExistingTextNode(span); });
                    globalLogger.warn('Selection.getPointForTextPosition: Found text spans without text node!');
                }
            }

            if (DOM.isTextSpan(node)) {
                return new DOM.Point(node.firstChild, node.firstChild.nodeValue.length);
            }

            return null;
        }

        /**
         * Jumping with a specified point in backward direction over range marker start node(s).
         */
        function jumpOverRangeMarkerStartNode(point) {

            // helper function, that detects preceding range marker start nodes or complex field nodes
            function continueIteration(node) {
                return DOM.isRangeMarkerStartNode(node) || (node.previousSibling && DOM.isEmptySpan(node) && DOM.isRangeMarkerStartNode(node.previousSibling));
            }

            // skipping to the node before the range marker start node (this must be guaranteed by the caller)
            point.node = point.node.parentNode.previousSibling.previousSibling;

            // skipping also over following range marker end node or comment place holder nodes
            while (continueIteration(point.node)) {
                point.node = point.node.previousSibling;
            }

            // this must be a text span node
            point.node = point.node.firstChild;
            point.offset = point.node.length;

        }

        /**
         * Jumping with a specified point in forward direction over range marker end node(s).
         */
        function jumpOverRangeMarkerEndNode(point) {

            // helper function, that detects following range marker nodes or comment place holder nodes
            function continueIteration(node) {
                return DOM.isRangeMarkerEndNode(node) ||
                    DOM.isCommentPlaceHolderNode(node) ||
                    (node.nextSibling && DOM.isEmptySpan(node) && (DOM.isRangeMarkerEndNode(node.nextSibling) || DOM.isCommentPlaceHolderNode(node.nextSibling)));
            }

            // skipping to the node behind the range marker end node (this must be guaranteed by the caller)
            point.node = point.node.parentNode.nextSibling.nextSibling;

            // skipping also over following range marker end node or comment place holder nodes
            while (continueIteration(point.node)) {
                point.node = point.node.nextSibling;
            }

            // this must be a text span node
            point.node = point.node.firstChild;
            point.offset = 0;
        }

        /**
         * Helper function used, for example, when trying to set text selection into fixed height text frame,
         * and position is bellow scrolled position.
         *
         * @param {jQuery} drawing
         */
        function handleFixedHeightTextFrameScrolling(drawing) {
            if (DrawingFrame.isFixedHeightDrawingFrame(drawing)) {
                var textframeNode = DrawingFrame.getTextFrameNode(drawing);
                textframeNode.scrollTop(textframeNode[0].scrollHeight);
            }
        }

        /**
         * OT: After receiving an external operation (insertText, insertTab, delete, ...) it might
         * be necessary to update place holder drawing. Its 'emptyness' might have changed because
         * of the external operation. The drawing might be empty after deleting a character, but
         * also after deleting a (second) paragraph.
         *
         * @param {Object} operation
         *  The external operation that could lead to a modified content and border of a placeholder
         *  drawing.
         */
        function checkEmptyPlaceHolderUpdate(operation) {
            var drawingNode = null;
            if (operation.start && (operation.start.length === 3 || operation.start.length === 4) && _.last(operation.start) === 0) {
                drawingNode = Position.getDrawingElement(docModel.getRootNode(operation.target), operation.start.slice(0, 2));
                if (drawingNode && !self.isSelectedDrawingNode(drawingNode)) {
                    docModel.drawingStyles.handleEmptyPlaceHolderDrawing(drawingNode, null, { remoteUpdate: true });
                }
            }
        }

        /**
         * OT: Switching a selectionState object, that is used for restoring selections after undo
         * or redo from a multi selection state to a selection of a single drawing.
         *
         * @param {Object} selectionState
         *  The selection state object that will be modified within this function.
         */
        function switchSelectionStateFromMultiToSingleSelection(selectionState) {
            if (selectionState.isMultiSelection && selectionState.allPositions && selectionState.allPositions.length === 1) {
                selectionState.isMultiSelection = false;
                selectionState.start = _.copy(selectionState.allPositions[0]);
                selectionState.end = Position.increaseLastIndex(selectionState.start);
                selectionState.allPositions = null;
            }
        }

        /**
         * OT: Switching a selectionState object, that is used for restoring selections after undo
         * or redo from a multi selection state to a 'simple' selection.
         *
         * @param {Object} selectionState
         *  The selection state object that will be modified within this function.
         */
        function switchSelectionStateToSimpleSelection(selectionState) {
            selectionState.isMultiSelection = false;
            selectionState.allPositions = null;
            selectionState.type = 'text';
            selectionState.dir = 'cursor';
            if (docApp.isPresentationApp()) { // setting slide selection
                selectionState.isSlideSelection = true;
                selectionState.start = [selectionState.start[0] || 0, 0]; // setting slide selection
            } else {
                selectionState.isSlideSelection = false;
                selectionState.start = self.getFirstDocumentPosition();
            }
            selectionState.end = _.copy(selectionState.start);
        }

        /**
         * Listener to the event 'transform:selectionstate' from the undoManager.
         * Adapting the selection state saved in the undo action, because this might be modified by external operations.
         * Reverse order for transformPosition like in calculation of undo operations is not required. Also the 'position'
         * operation always needs to be specified in the 'normal' order, so that 'reverse: true' will not be supported.
         *
         * @param {Object} selectionState
         *  A descriptor of the initial selection state that will be used to restore the selection after the undo/redo action.
         *  This selection state is not always specified.
         *
         * @param {Object[]} extActions
         *  An array with those external actions that are required for the transformation of the selection state.
         *
         * TODO: Redo handling
         */
        function transformSelectionState(selectionState, extActions) {

            var selections = null;
            var isMultiSelection = false;
            var decreasedMultiSelection = false;
            var newPositionFound = false; // whether there is a new selection that can be set

            if (selectionState.isMultiSelection) {
                isMultiSelection = true;
                selections = selectionState.allPositions;
            } else {
                selections = [];
                selections.push({ startPosition: selectionState.start, endPosition: selectionState.end });
            }

            try {

                // iterating over all selections
                selections.forEach(function (oneSelection, index) {

                    var localStartPosition =  isMultiSelection ? oneSelection : oneSelection.startPosition; // multi selection has only the start position of each drawing
                    var currStartPosition = _.copy(localStartPosition);
                    var currEndPosition = null;
                    var positionModified = false;
                    var newStartPosition = null;
                    var newEndPosition = null;
                    var isValidPosition = true;
                    var selectionRootNode = null;

                    newStartPosition = docApp.otManager.transformPosition(localStartPosition, extActions, selectionState.target);

                    if (newStartPosition) {

                        newPositionFound = true;
                        positionModified = !_.isEqual(currStartPosition, newStartPosition);

                        if (selectionState.dir === 'cursor') {

                            if (selectionState.isImplicitParagraph) { // taking care of implicit paragraphs (DOCS-2649)
                                selectionRootNode = docModel.getRootNode(selectionState.target);
                                isValidPosition = Position.isValidElementPosition(selectionRootNode, newStartPosition, { allowFinalPosition: true });
                                if (!isValidPosition) {
                                    if (_.isNumber(newStartPosition[newStartPosition.length - 2]) && newStartPosition[newStartPosition.length - 2] > 0) {
                                        newStartPosition[newStartPosition.length - 2] -= 1; // decreasing the paragraph position by 1
                                        isValidPosition = Position.isValidElementPosition(selectionRootNode, newStartPosition, { allowFinalPosition: true });
                                        if (isValidPosition) {
                                            selectionState.isImplicitParagraph = DOM.isImplicitParagraphNode(Position.getParagraphElement(selectionRootNode, _.initial(newStartPosition)));
                                        }
                                    }
                                }
                            }

                            if (isValidPosition) {
                                selectionState.start = newStartPosition;
                                selectionState.end = _.copy(newStartPosition);
                            }

                        } else if (selectionState.type === 'drawing') {

                            if (positionModified) {

                                if (isMultiSelection) {
                                    selectionState.allPositions[index] = newStartPosition;
                                } else {
                                    selectionState.start = newStartPosition;
                                    selectionState.end = Position.increaseLastIndex(newStartPosition);
                                }

                            }

                        } else {

                            currEndPosition = _.copy(oneSelection.endPosition);
                            newEndPosition = docApp.otManager.transformPosition(currEndPosition, extActions, selectionState.target);
                            positionModified = positionModified || !_.isEqual(currEndPosition, newEndPosition);

                            if (positionModified) {
                                selectionState.start = newStartPosition;
                                selectionState.end = newEndPosition;
                            }

                        }

                    } else { // no new start position could be calculated

                        if (isMultiSelection) {
                            // the multi selection needs to be decreased
                            decreasedMultiSelection = true;
                            oneSelection._REMOVED_DRAWING_ = true;
                        } else {
                            switchSelectionStateToSimpleSelection(selectionState);
                        }
                    }
                });

                // adaptions after iterating over all selections
                if (isMultiSelection) {

                    if (decreasedMultiSelection) { // removing one or more drawings from a multi selection
                        selectionState.allPositions = _.reject(selections, function (oneSelection) {
                            return oneSelection._REMOVED_DRAWING_;
                        });
                        if (selectionState.allPositions.length === 1) {
                            switchSelectionStateFromMultiToSingleSelection(selectionState);
                        } else if (selectionState.allPositions.length === 0) {
                            switchSelectionStateToSimpleSelection(selectionState);
                        }
                    }

                    if (newPositionFound && selectionState.allPositions.length > 1) {
                        selectionState.allPositions.sort(comparePositions);
                        selectionState.start = _.copy(selectionState.allPositions[0]);
                        selectionState.end = Position.increaseLastIndex(selectionState.start);
                    }
                }

            } catch (exception) {
                otLogger.error('$badge{Selection} transformSelectionState: ' + exception.message);
            }
        }

        /**
         * OT: Setting the local selection after applying an external operation.
         *
         * In this function only the values for the logical position are recalculated. Therefore
         * 'startPosition' and 'endPosition' get new values assigned. Typically this happens only
         * in 'applyBrowserSelection'. But in this case the logical position is modified only
         * by an external operation. In some cases it might additionally be necessary to restore
         * the browser selection, because it might have vanished. This could happen for example
         * after a split of a paragraph with the current selection in the second part of the
         * paragraph.
         *
         * @param {Object} operation
         *  The external operation that could lead to a modified local logical position.
         */
        function adaptLocalSelectionAfterOT(operation) {

            var isMultiSelection = false;
            var selections = null;
            var doRestoreSelection = false;
            var decreasedMultiSelection = false;

            if (operation.name === NOOP) { return; }

            if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                isMultiSelection = true;
                selections = self.getMultiSelection();
            } else {
                selections = [];
                selections.push({ startPosition: self.getStartPosition(), endPosition: self.getEndPosition() });
            }

            if (docApp.isPresentationApp()) {
                // fast exit for empty selection handling
                if (self.isEmptySelection() && docModel.handledEmptySelection(operation)) { return; }

                // fast exit for slide selections -> avoid increase of [2, 0] by insertDrawing at [2, 0]
                if (self.isSlideSelection() && _.isArray(operation.start) && operation.start.length >= 2) {
                    checkEmptyPlaceHolderUpdate(operation);
                    return;
                }
            }

            // OT: Transforming the current selection because of the applied external operation
            try {

                var currentTarget = docModel.getActiveTarget() || null; // maybe better to use 'self.getRootNodeTarget'
                var keepFocus = false;
                var newPositionFound = false; // whether there is a new selection that can be set
                var forceSimpleSelection = false;

                // iterating over all selections
                selections.forEach(function (oneSelection) {

                    var currStartPosition = _.clone(oneSelection.startPosition);
                    var newStartPosition = docApp.otManager.transformPosition(currStartPosition, operation, currentTarget);
                    var newEndPosition = null;
                    var isValidPosition = true; // whether the calculated position is valid
                    var positionModified = false;
                    var drawingNode = null;

                    // check if the current selection is affected by a group/ungroup operation
                    if (docApp.isPresentationApp()) {
                        if (forceSimpleSelection) { return; } // check only once
                        forceSimpleSelection = docModel.checkForSimpleSelection(operation, currStartPosition, currentTarget);
                        if (forceSimpleSelection) { return; }
                    }

                    if (newStartPosition) {

                        newPositionFound = true;
                        positionModified = !_.isEqual(currStartPosition, newStartPosition);

                        if (self.isTextCursor()) {

                            if (positionModified) {
                                // the old paragraph might have been implicit -> the new position might be invalid (problem immediately after loading and getting external OP)
                                if (operation.name === PARA_INSERT) { isValidPosition = Position.isValidElementPosition(docModel.getCurrentRootNode(), newStartPosition, { allowFinalPosition: true }); }

                                if (isValidPosition) {
                                    startPosition = newStartPosition;
                                    endPosition = _.copy(newStartPosition);
                                }
                            }

                        } else if (self.isDrawingSelection()) {

                            if (positionModified) {
                                startPosition = newStartPosition;
                                endPosition = Position.increaseLastIndex(startPosition);

                                // taking care of multi selection (can only be inside a drawing selection)
                                if (isMultiSelection) {
                                    oneSelection.startPosition = startPosition;
                                    oneSelection.endPosition = endPosition;
                                }

                                if (docApp.isTextApp()) {
                                    // handling drawing selection of absolutely positioned drawings in header/footer
                                    if (docModel.isHeaderFooterEditState()) {
                                        drawingNode = self.getSelectedDrawing();
                                        if (!DOM.isInlineDrawingNode(drawingNode)) {
                                            docModel.executeDelayed(function () {
                                                if (self.isDrawingSelection()) { self.restoreDrawingSelection(drawingNode); }
                                            }, 100);
                                        }
                                    }
                                } else if (docApp.isPresentationApp()) {
                                    // handling a drawing selection that now might be inside a group -> select the group
                                    if (operation.name === GROUP) {
                                        drawingNode = self.getSelectedDrawing();
                                        if (DrawingFrame.isGroupedDrawingFrame(drawingNode)) {
                                            startPosition = _.copy(operation.start);
                                            endPosition = Position.increaseLastIndex(startPosition);
                                        }
                                    }
                                }
                            }

                        } else {

                            var currEndPosition = oneSelection.endPosition;
                            newEndPosition = docApp.otManager.transformPosition(currEndPosition, operation, currentTarget);
                            if (!newEndPosition) { newEndPosition = _.copy(newStartPosition); } // DOCS-2675
                            positionModified = positionModified || !_.isEqual(currEndPosition, newEndPosition);
                            if (positionModified) {
                                startPosition = newStartPosition;
                                endPosition = newEndPosition;
                            }

                        }

                        // setting marker that the (modified) selection need to be restored
                        if (positionModified) { doRestoreSelection = true; }

                    } else if (isMultiSelection) {

                        // the multi selection needs to be decreased
                        decreasedMultiSelection = true;
                        oneSelection._REMOVED_DRAWING_ = true;
                    }
                });

                if (!newPositionFound) {
                    keepFocus = !containsNode(docModel.getNode(), getFocus(), { allowSelf: true }); // not removing any selection outside the page
                    if (docApp.isPresentationApp()) {
                        docModel.changeToValidPosition();
                    } else {
                        self.setTextSelection(self.getFirstDocumentPosition(), undefined, { keepFocus }); // maybe a smarter position can be found
                    }
                    return;
                }

                if (isMultiSelection) {
                    if (decreasedMultiSelection) { // removing one or more drawings from a multi selection
                        self.removeDrawingsFromSelectionByMarker('_REMOVED_DRAWING_'); // a marker must be used, because positions of other drawings might have been modified
                    }

                    if (newPositionFound && operation.name === MOVE) {
                        self.sortMultiSelection(); // move operation can modify order of drawings in multi selection
                    }
                }

                if (docApp.isPresentationApp() && forceSimpleSelection) { docModel.setSimpleSelection(); }

                // making the new selection visible (might be necessary sometimes)
                if (doRestoreSelection) {
                    keepFocus = !containsNode(docModel.getNode(), getFocus(), { allowSelf: true }); // not removing any selection outside the page
                    self.restoreBrowserSelection({ preserveFocus: true, isTemporaryInvalidSelection: true, keepFocus }); // using 'isTemporaryInvalidSelection' to avoid superfluous console errors
                    self.trigger('change', { noscroll: true }); // Informing the listeners about a modified selection, for example to update remote selections
                    self.trigger('changecursorinfo', { start: startPosition, end: endPosition, rootNode: self.getRootNode(), target: currentTarget }); // updating Android selection
                }

                // taking care of content of empty text frames (replacement text and border)
                if (docApp.isPresentationApp()) { checkEmptyPlaceHolderUpdate(operation); }

            } catch (exception) {
                otLogger.error('$badge{Selection} adaptLocalSelectionAfterOT: ' + exception.message);
            }
        }

        /**
         * OT: Generating a test browser selection. This is required, because a simulated mousedown/mouseup
         * via 'dispatchEvent' does not allow the browser default so that the 'real' browser selection cannot
         * be determined with dispatched events. The property 'isTrusted' of the originalEvent inside the
         * specified jQuery event is only true, if the event was triggered by a real user action.
         *
         * @param {jQuery.Event} event
         *  The jQuery event object.
         *
         * @returns {Object|null}
         *  An object that contains a property 'active' with a DOM.Range object containing the current anchor
         *  point in its 'start' property, and the focus point in its 'end' property. The returned object
         *  contains another property 'ranges' which is an array of DOM.Range objects representing all ranges
         *  currently selected.
         *  If the test browser selection cannot be determined, null is returned.
         */
        function getTestBrowserSelection(event) {
            // the logical position of the specified paragraph
            var paraPos = Position.getOxoPosition(rootNode, event.target, 0);
            if (!paraPos) { return null; } // failed to determine the logical position of the event target
            // the text position inside the specified paragraph
            var textPos = 0; // defaulting to first position in paragraph
            if (event.textposition === 'end') { textPos = Position.getParagraphNodeLength(event.target); }
            if (event.textposition === 'middle') { textPos = Math.floor(Position.getParagraphNodeLength(event.target) / 2); }
            // the dom point for the selection
            var domPoint = Position.getDOMPosition(rootNode, Position.appendNewIndex(paraPos, textPos));
            // the dom range for the selection
            var domRange = new DOM.Range(domPoint);
            // returning the calculated browser selection
            return { active: domRange, ranges: [domRange] };
        }

        /**
         * Helper function to set the cursor in Chrome browser correctly after pressing "Cursor down".
         * The Chrome browser sets the cursor into a text frame in the same paragraph.
         *
         * @param {Number[]} position
         *  The logical position before pressing "Cursor down".
         *
         * @returns {Boolean}
         *  Whether the selection was modified within this function.
         */

        function handleJumpIntoTextFrame(pos) {

            let handledEvent = false;
            const paraNode = Position.getParagraphElement(rootNode, _.initial(pos));

            if (paraNode && paraNode.nextSibling) {
                const newPos = Position.getFirstPositionInParagraph(rootNode, Position.increaseLastIndex(_.initial(pos)));
                self.setTextSelection(newPos);
                handledEvent = true;
            }

            return handledEvent;
        }

        /**
         * Helper function to get the text of a text span within a specified range.
         *
         * @param {Node|jQuery} node
         *  The node from which the text might be extracted.
         *
         * @param {Number} start
         *  The start of the text within the text span.
         *
         * @param {Number} length
         *  The length of the text within the text span.
         *
         * @returns {string}
         *  The text of the text span within the specified range.
         */
        function getTextSpanContent(node, start, length) {
            let text = "";
            if ((start >= 0) && (length >= 0) && DOM.isTextSpan(node)) {
                const nodeText = $(node).text();
                if (nodeText) { text = nodeText.slice(start, start + length); }
            }
            return text;
        }

        // methods ------------------------------------------------------------

        /**
         * Returns whether this selection contains a valid start and end
         * position.
         *
         * @returns {boolean}
         */
        this.isValid = function () {
            return !!startPosition && !!endPosition && (startPosition.length > 0) && (endPosition.length > 0);
        };

        /**
         * Returns the current logical start position. The start position is
         * always located before the end position (or, in case of a text
         * cursor, is equal to the end position), regardless of the direction
         * of the selection. To receive positions dependent on the direction,
         * see the methods Selection.getAnchorPosition() and
         * Selection.getFocusPosition().
         *
         * @returns {Number[]}
         *  The logical start position of this selection, as cloned array that
         *  can be changed by the caller.
         */
        this.getStartPosition = function () {
            return _.clone(startPosition);
        };

        /**
         * Returns the current logical end position. The end position is always
         * located behind the start position (or, in case of a text cursor,
         * is equal to the start position), regardless of the direction of the
         * selection. To receive positions dependent on the direction, see the
         * methods Selection.getAnchorPosition() and
         * Selection.getFocusPosition().
         *
         * @returns {Number[]}
         *  The logical end position of this selection, as cloned array that
         *  can be changed by the caller.
         */
        this.getEndPosition = function () {
            return _.clone(endPosition);
        };

        /**
         * Returns the current logical anchor position. The anchor position is
         * the position where the selection of a text range (by mouse or cursor
         * keys) has been started. The anchor position will be located after
         * the focus position if selecting backwards. To receive positions
         * independent from the direction, see the methods
         * Selection.getStartPosition() and Selection.getEndPosition().
         *
         * @returns {Number[]}
         *  The logical anchor position of this selection, as cloned array that
         *  can be changed by the caller.
         */
        this.getAnchorPosition = function () {
            return _.clone(getAnchorPosition());
        };

        /**
         * Returns the current logical focus position. The focus position is
         * the position that changes while modifying the selection of a range
         * (by mouse or cursor keys). The focus position will be located before
         * the anchor position if selecting backwards. To receive positions
         * independent from the direction, see the methods
         * Selection.getStartPosition() and Selection.getEndPosition().
         *
         * @returns {Number[]}
         *  The logical focus position of this selection, as cloned array that
         *  can be changed by the caller.
         */
        this.getFocusPosition = function () {
            return _.clone(getFocusPosition());
        };

        /**
         * Returns whether this selection represents a simple text cursor.
         *
         * @returns {Boolean}
         *  True, if this selection represents a simple text cursor.
         */
        this.isTextCursor = function () {
            return !cellRangeSelected && _.isEqual(startPosition, endPosition);
        };

        /**
         * Returns whether this selection represents a range that covers some
         * document contents. The result is the exact opposite of the method
         * Selection.isTextCursor().
         *
         * @returns {Boolean}
         *  True, if this selection represents a range in the document (a text
         *  range, or a cell range, or a drawing object).
         */
        this.hasRange = function () {
            return !this.isTextCursor();
        };

        /**
         * Returns whether this selection has been created while selecting the
         * document contents backwards (by cursor keys or by mouse).
         *
         * @returns {Boolean}
         *  True, if the selection has been created backwards.
         */
        this.isBackwards = function () {
            return backwards;
        };

        /**
         * Returns the direction of this selection (the relation of start
         * position to end position).
         *
         * @returns {String}
         *  One of the strings 'cursor', 'forwards', or 'backwards'.
         */
        this.getDirection = function () {
            return this.isTextCursor() ? 'cursor' : backwards ? 'backwards' : 'forwards';
        };

        /**
         * Returns the type of this selection as string.
         *
         * @returns {String}
         *  Returns 'text' for a text range or text cursor, or 'cell' for a
         *  rectangular cell range in a table, or 'drawing' for a drawing
         *  selection.
         */
        this.getSelectionType = function () {
            return cellRangeSelected ? 'cell' : (selectedDrawing.length > 0 || (self.isMultiSelectionSupported() && self.isMultiSelection())) ? 'drawing' : 'text';
        };

        /**
         * Saving the current start and end position in a selection store, so that
         * it can be used later, after the new positions are calculated.
         */
        this.storeCurrentSelectionState = function () {
            selectionStore = { startPosition, endPosition };
        };

        /**
         * An external handler might be able to repair an invalid selection. If this is the case,
         * the property 'problemExternallyHandled' can be set to true for a short period of time.
         * Within this time, the function 'resortBrowserSelection' does not show an error on the
         * console.
         *
         * @param {Boolean} value
         *  The new value for the property 'problemExternallyHandled'.
         */
        this.setProblemExternallyHandled = function (value) {
            problemExternallyHandled = value;
        };

        /**
         * Returns whether the current selection is a selection that
         * additionally contains a selected text frame. This typically
         * happens for a text selection inside a text frame drawing.
         *
         * @returns {Boolean}
         *  Whether the current selection additionally contains a selection
         *  of a text frame.
         */
        this.isAdditionalTextframeSelection = function () {
            return selectedTextFrameDrawing.length > 0;
        };

        /**
         * Returns whether the current selection is a pure drawing selection or a
         * multi drawing selection or is at least a selected text frame in a text
         * selection. In the case of a multi drawing selection, the selection type
         * is set to 'drawing' and therefore this function returns true.
         *
         * @returns {Boolean}
         *  Whether the current selection is a pure drawing selection
         *  or is at least a selected text frame in a text selection.
         */
        this.isAnyDrawingSelection = function () {
            return self.getSelectionType() === 'drawing' || self.isAdditionalTextframeSelection();
        };

        /**
         * Switching from an additional drawing selection to a drawing selection. This
         * means, that that drawing that is only selected because of an internal text
         * selection becomes the new selection. The text selection is then no longer
         * valid.
         *
         * This change is typically necessary after finalizing 'resize', 'move', 'rotate'
         * or 'adjust' of a drawing.
         *
         * @returns {Boolean}
         *  Whether the selection switched from text to drawing selection.
         */
        this.switchAdditionalDrawingToDrawingSelection = function () {

            // whether the selection switched from text to drawing selection
            var switched = false;
            // the logical position of the affected drawing
            var drawingPos = null;

            if (selectedTextFrameDrawing.length > 0) {
                drawingPos = Position.getOxoPosition(rootNode, selectedTextFrameDrawing);
                self.setTextSelection(drawingPos, Position.increaseLastIndex(drawingPos));
                switched = true;
            }

            return switched;
        };

        /**
         * Returns whether the current selection is a drawing selection or a selected text frame
         * in a text selection and this is NOT a drawing of type 'table'. In the case of a multi
         * drawing selection, this function returns true, if at least one of the selected drawings
         * it NOT a table drawing node (Presentation only).
         *
         * @returns {Boolean}
         *  Whether the current selection is a pure drawing selection or a selected text frame and
         *  the drawing is not of type 'table'.
         */
        this.isAnyDrawingSelectionExceptTable = function () {
            return self.isAnyDrawingSelection() && !self.isOnlyTableSelection();
        };

        /**
         * Returns whether the current selection is a selection of a drawing group.
         *
         * Info: In the case of a multi drawing selection this functin returns false.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of a drawing group.
         */
        this.isDrawingGroupSelection = function () {
            return self.getSelectionType() === 'drawing' && DrawingFrame.isGroupDrawingFrame(selectedDrawing);
        };

        /**
         * Returns whether the currently selected drawing is of type 'group' or if the drawing, that is
         * additionally selected to the selected text, is a drawing of type 'group'.
         *
         * Info: In the case of a multi drawing selection this functin returns false.
         *
         * @returns {Boolean}
         *  Whether the selection drawing or the additionally selected drawing is of type 'group'.
         */
        this.isAnyDrawingGroupSelection = function () {
            return self.isDrawingGroupSelection() || (self.isAdditionalTextframeSelection() && DrawingFrame.isGroupDrawingFrame(selectedTextFrameDrawing));
        };

        /**
         * Returns whether the current selection is a selection of a drawing group and if this
         * group contains at least one text frame.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of a drawing group and if this
         *  group contains at least one text frame.
         */
        this.isAtLeastOneTextFrameInGroupSelection = function () {
            return self.isDrawingGroupSelection() && selectedDrawing.find(DrawingFrame.TEXTFRAMECONTENT_NODE_SELECTOR).length > 0;
        };

        /**
         * Checking, if this is a drawing selection and the selected drawing is a table drawing (Presentation only).
         * For example required for task 57568.
         *
         * @returns {Boolean}
         *  Whether this is a drawing selection and the selected drawing is a table drawing (Presentation only).
         */
        this.isTableDrawingSelection = function () {
            var tableDrawing = docApp.isPresentationApp() ? self.isDrawingSelection() && DrawingFrame.isTableDrawingFrame(selectedDrawing) : null;
            return !!tableDrawing;
        };

        /**
         * Returns whether the current selection is a drawing selection or a selection inside a drawing
         * and this drawing is a table.
         *
         * @returns {Boolean}
         *  Whether the current selection is a drawing selection or a selection inside a drawing
         *  and this drawing is a table.
         */
        this.isAnyTableDrawingSelection = function () {
            var drawing = self.getAnyDrawingSelection();
            return !!drawing && DrawingFrame.isTableDrawingFrame(drawing);
        };

        /**
         * Returns whether the current selection is selection of drawing allowed to be cropped,
         * namely: drawing selection, not group and not table.
         *
         * @returns {Boolean}
         *  Whether the current selection is allowed selection for drawing crop.
         */
        this.isCropDrawingSelection = function () {
            return docModel.getSelection().isAnyDrawingSelectionExceptTable() && !docModel.getSelection().isAnyDrawingGroupSelection();
        };

        /**
         * Returns a selected drawing or an additional text frame drawing.
         * Or null, if this is neither a drawing selection nor an additional
         * text frame selection.
         *
         * Info: In the case of a multi drawing selection this function returns
         *       null.
         *
         * @returns {jQuery|Null}
         *  A jQuery object containing the currently selected drawing or
         *  additional text frame drawing. Or null, if none of them is
         *  selected or if this is a multi drawing selection.
         */
        this.getAnyDrawingSelection = function () {

            var // the currently selected drawing
                selDrawing = null;

            if (selectedDrawing.length > 0) {
                selDrawing = selectedDrawing;
            } else if (selectedTextFrameDrawing.length > 0) {
                selDrawing = selectedTextFrameDrawing;
            }

            return selDrawing;
        };

        /**
         * Whether the current selection is a selection of type 'text'.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of type 'text'.
         */
        this.isTextSelection = function () {
            return this.getSelectionType() === 'text';
        };

        /**
         * Whether the current selection is a selection of a drawing. This is
         * true if a drawing is selected or if this is a multi drawing selection.
         * But in the case of a text selection with additionally selected
         * text frame drawing, this function returns false.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of a drawing or a
         *  multi drawing selection.
         */
        this.isDrawingSelection = function () {
            return self.getSelectionType() === 'drawing';
        };

        /**
         * Whether the current selection is a selection of type 'cell'. This is
         * currently only supported by the Firefox browser.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of type 'cell'.
         */
        this.isCellSelection = function () {
            return self.getSelectionType() === 'cell';
        };

        /**
         * Whether the current selection is a selection of a text frame.
         *
         * Info: In the case of a multi drawing selection this function returns false.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of a text frame. This does NOT include the additional text frame, that
         *  is selected, if the cursor is positioned inside a text frame.
         */
        this.isTextFrameSelection = function () {
            return self.isDrawingSelection() && DrawingFrame.isTextFrameShapeDrawingFrame(self.getSelectedDrawing());
        };

        /**
         * Returns whether the current selection is a text selection inside a text frame and the additionally
         * selected drawing is a text frame (and not a group), or if a text frame is directly selected as drawing
         * or if in a multi drawing selection at least one selected drawing is a text frame.
         *
         * @param {Object} [options]
         *  An object containing some additional properties:
         *  @param {Boolean} [options.allDrawingTypesAllowed=false]
         *      Whether a selected drawing must be a text frame. Alternatively it is possible, that a 'group' drawing
         *      is selected additionally to a text selection inside a text frame. If 'allDrawingTypesAllowed' is set
         *      to true, this function returns also true, if a 'group' drawing node is selected additionally to a
         *      text selection inside a text frame.
         *  @param {Boolean} [options.allowGroup=false]
         *      Whether the currently selected drawing can be a drawing of type group. In this case this can be a text
         *      selection inside a grouped text frame, so that the group is the additionally selected drawing. Or the
         *      selection is directly on the group, and the group contains at least one grouped text frame.
         *
         * @returns {Boolean}
         *  Whether the current selection is a pure text frame drawing selection
         *  or is at least a selected text frame in a text selection.
         */
        this.isAnyTextFrameSelection = function (options) {

            var // whether the selected drawing must be a text frame
                allDrawingTypesAllowed = getBooleanOption(options, 'allDrawingTypesAllowed', false),
                // whether the selected drawing can also be a group
                allowGroup = getBooleanOption(options, 'allowGroup', false);

            return (self.isAdditionalTextframeSelection() && (allDrawingTypesAllowed || DrawingFrame.isTextFrameShapeDrawingFrame(self.getSelectedTextFrameDrawing()) || (allowGroup && DrawingFrame.isGroupDrawingFrameWithShape(self.getSelectedTextFrameDrawing())))) ||
                (self.getSelectionType() === 'drawing' && (DrawingFrame.isTextFrameShapeDrawingFrame(self.getSelectedDrawing()) || (allowGroup && DrawingFrame.isGroupDrawingFrameWithShape(self.getSelectedDrawing())) || (self.isMultiSelection() && self.isAtLeastOneTextFrameInMultiSelection({ searchInGroup: true }))));
        };

        /**
         * Whether the current selection is a selection of a text frame (a drawing selection, not an additional text frame)
         * or if in a multi drawing seletion at least one drawing is a text frame. In any case a text frame drawing is
         * completely selected as drawing. Additionally this might be a group selection, in which the selected group
         * contains at least one text frame.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection of a text frame drawing or if in a multi drawing seletion at least
         *  one drawing is a text frame.
         */
        this.isAnyTextFrameDrawingSelection = function () {
            return self.isTextFrameSelection() || self.isAtLeastOneTextFrameInMultiSelection() || self.isAtLeastOneTextFrameInGroupSelection();
        };

        /**
         * Whether the current selection is a selection, in which one position is inside a table cell and the other is
         * NOT inside this table cell.
         *
         * @returns {Boolean}
         *  Whether the current selection is a selection, in which one position is inside a table cell and the other is
         *  NOT inside this table cell.
         */
        this.isCellExceedingSelection = function () {
            return Position.isCellExceedingSelection(rootNode, startPosition, endPosition);
        };

        /**
         * Returns whether the selection represents a simple text cursor that is
         * located in a top level node (can be used for slide in presentation app).
         *
         * @param {Object} [selection]
         *  An optional object containing the properties 'start' and 'end'. If this
         *  is specified, it is evaluated. If this is not specified, the current
         *  selection is evaluated.
         *
         * @returns {Boolean}
         *  True, if this selection represents a simple text cursor inside a top
         *  level node.
         */
        this.isTopLevelTextCursor = function (selection) {
            return selection ? (selection.start && _.isEqual(selection.start, selection.end) && selection.start.length === 2) : (startPosition && startPosition.length === 2 && self.isTextCursor());
        };

        /**
         * Returns whether the current selected drawing is supported or not.
         * If the selected drawing is a group of drawings, iterate over all
         * children. If at least one of them is not supported, return false.
         *
         * 'unsupported' is set as class name to specified drawings. This can
         * be for example not supported drawing types.
         *
         * @returns {Boolean}
         *  Whether the selection contains only supported drawings. This means,
         *  that no selected drawing and no child of a selected group drawing
         *  has the class 'unsupported' set.
         */
        this.areOnlySupportedDrawingsSelected = function () {

            var // the collection of selected drawings
                selectedDrawings = self.getSelectedDrawings(),
                // wheter at least one drawing with class 'unsupported' was found
                foundUnsupportedDrawing = false;

            _.each(selectedDrawings, function (oneDrawing) {

                if (!foundUnsupportedDrawing) {
                    if (DrawingFrame.isGroupDrawingFrame(oneDrawing)) {
                        _.each(DrawingFrame.getAllGroupDrawingChildren(oneDrawing), function (drawing) {
                            if (!foundUnsupportedDrawing && DrawingFrame.isUnsupportedDrawing($(drawing))) {
                                foundUnsupportedDrawing = true;
                            }
                        });
                    } else {
                        foundUnsupportedDrawing = DrawingFrame.isUnsupportedDrawing(oneDrawing);
                    }
                }

            });

            return !foundUnsupportedDrawing;
        };

        /**
         * Returns whether the current selection is a drawing selection and
         * the drawing is located inside a text frame.
         *
         * @returns {Boolean}
         *  Whether the current selection is a drawing selection and the drawing
         *  is located inside a text frame.
         */
        this.isDrawingInTextFrameSelection = function () {
            return self.getSelectionType() === 'drawing' && selectedDrawing.length > 0 && selectedDrawing.closest(DrawingFrame.TEXTFRAME_NODE_SELECTOR).length > 0;
        };

        /**
         * Returns whether the current selection is a drawing selection and
         * the drawing is located inside an ODF text frame.
         *
         * @returns {Boolean}
         *  Whether the current selection is a drawing selection and the drawing
         *  is located inside a text frame.
         */
        this.isDrawingInODFTextFrameSelection = function () {

            var // the text frame node inside the drawing node
                textFrameNode = null;

            if (self.getSelectionType() === 'drawing' && selectedDrawing.length > 0) {

                textFrameNode = selectedDrawing.closest(DrawingFrame.TEXTFRAME_NODE_SELECTOR);

                if (textFrameNode && textFrameNode.length > 0 && DrawingFrame.isFullOdfTextframeNode(textFrameNode.closest(DrawingFrame.NODE_SELECTOR))) {
                    return true;
                }
            }

            return false;
        };

        /**
         * Returns whether the current selection is selection inside a paragraph.
         * So currently the selection type must be 'text' or 'drawing'.
         *
         * @returns {Boolean}
         *  Whether the current selection type is 'text' or 'drawing'.
         */
        this.isTextLevelSelection = function () {
            return !cellRangeSelected;
        };

        /**
         * Returns whether the current selection is a cell selection, that covers the
         * complete table (only supported in FF).
         *
         * @returns {Boolean}
         *  Whether the current selection is a cell selection, that covers the
         *  complete table.
         */
        this.isCompleteTableCellSelection = function () {
            return cellRangeSelected && tableSelected;
        };

        /**
         * Resetting an existing selection. This can be used for example during loading the
         * document, where an existing selection must be resetted after the operations were
         * applied. So it is possible to determine, if the user already modified the selection.
         */
        this.resetSelection = function () {
            startPosition = [];
            endPosition = [];
            cellRangeSelected = false;
            tableSelected = false;
        };

        /**
         * Checking, if the current values for startPosition and endPosition
         * represent a valid text position. It can happen, that these values
         * point to a non existing position (bug 28520).
         * Returns whether the current selection is a valid text selection.
         *
         * @returns {Boolean}
         *   Whether the current selection is a valid text selection.
         */
        this.isValidTextSelection = function () {

            var startPos = _.clone(startPosition),
                textPos = startPos.pop(),
                paragraph =  Position.getParagraphElement(rootNode, startPos),
                endPos = null;

            if ((paragraph === null) || (Position.getParagraphNodeLength(paragraph) < textPos)) {
                return false;
            }

            if (!_.isEqual(startPosition, endPosition)) {
                endPos = endPosition ? _.clone(endPosition) : _.clone(startPosition);
                textPos = endPos.pop();
                paragraph =  Position.getParagraphElement(rootNode, endPos);

                if ((paragraph === null) || (Position.getParagraphNodeLength(paragraph) < textPos)) {
                    return false;
                }
            }

            return true;
        };

        /**
         * Checking, whether the current selection describes an inline component
         * node like a tab, field, drawing, ... and nothing else.
         *
         * @returns {Boolean}
         *   Whether the current selection describes an inline component.
         */
        this.isInlineComponentSelection = function () {

            var // the node element at the start position
                startNodeInfo = null;

            if (startPosition && endPosition && _.isEqual(_.initial(startPosition), _.initial(endPosition)) && (_.last(startPosition) + 1 === _.last(endPosition))) {
                startNodeInfo = Position.getDOMPosition(rootNode, startPosition, true);
                if (startNodeInfo && startNodeInfo.node && DOM.isInlineComponentNode(startNodeInfo.node)) {
                    return true;
                }
            }

            return false;
        };

        /**
         * Checking, whether the current selection describes a drawing component
         * and nothing else.
         *
         * @returns {Boolean}
         *   Whether the current selection describes a drawing component and
         *   nothing else.
         */
        this.isDrawingFrameSelection = function () {

            var // the node element at the start position
                startNodeInfo = null;

            if (startPosition && endPosition && _.isEqual(_.initial(startPosition), _.initial(endPosition)) && (_.last(startPosition) + 1 === _.last(endPosition))) {
                startNodeInfo = Position.getDOMPosition(rootNode, startPosition, true);
                if (startNodeInfo && startNodeInfo.node && DOM.isDrawingFrame(startNodeInfo.node)) {
                    return true;
                }
            }

            return false;
        };

        /**
         * Checks, whether the given node is (one of) the selected drawing(s)
         *
         * @param {HTMLElement|jQuery} drawingNode
         *
         * @returns {Boolean}
         *   Whether the node is selected or not
         */
        this.isDrawingSelected = function (drawingNode) {
            return _.contains(selectedDrawing, getDomNode(drawingNode));
        };

        /**
         * Returns whether the start and end position of this selection are
         * located in the same parent component (all array elements but the
         * last are equal).
         *
         * @param {Number} [parentLevel=1]
         *  The number of parent levels. If omitted, the direct parents of the
         *  start and end position will be checked (only the last element of
         *  the position array will be ignored). Otherwise, the specified
         *  number of trailing array elements will be ignored (for example, a
         *  value of 2 checks the grand parents).
         *
         * @returns {Boolean}
         *  Whether the start and end position are located in the same parent
         *  component.
         */
        this.hasSameParentComponent = function (parentLevel) {
            return Position.hasSameParentComponent(startPosition, endPosition, parentLevel);
        };

        /**
         * Returns whether this selection covers exactly one component.
         *
         * @returns {Boolean}
         *  Returns whether the selection is covering a single component. The
         *  start and end position must refer to the same parent component, and
         *  the last array element of the end position must be the last array
         *  element of the start position increased by the value 1.
         */
        this.isSingleComponentSelection = function () {
            return this.hasSameParentComponent() && (_.last(startPosition) === _.last(endPosition) - 1);
        };

        /**
         * Returns whether the selection is top level text cursor in presentation. No drawing or text is selected.
         * Info: Presentation only!
         *
         * @returns {Boolean}
         *  Whether the selection is top level slide selection.
         */
        this.isSlideSelection = function () {
            return slideMode && self.isTopLevelTextCursor();
        };

        /**
         * Returns the logical position of the closest common component
         * containing all nodes covered by this selection (the leading array
         * elements that are equal in the start and end position arrays).
         *
         * @returns {Number[]}
         *  The logical position of the closest common component containing
         *  this selection. May be the empty array if the positions already
         *  differ in their first element.
         */
        this.getClosestCommonPosition = function () {

            var index = 0, length = Math.min(startPosition.length, endPosition.length);

            while ((index < length) && (startPosition[index] === endPosition[index])) {
                index += 1;
            }

            return startPosition.slice(0, index);
        };

        /**
         * Returns the closest paragraph that contains all nodes of this
         * selection completely.
         *
         * @param {Object} [options]
         *  An object containing some additional properties:
         *  @param {Boolean} [options.allowSlide=false]
         *      Whether slides shall be handled like paragraphs (only supported
         *      in OX Presentation).
         *
         * @returns {HTMLElement|Null}
         *  The closest paragraph containing this selection; or null, if the
         *  selection is not contained in a single paragraph.
         */
        this.getEnclosingParagraph = function (options) {

            var // position of closest common parent component containing the selection
                commonPosition = this.getClosestCommonPosition(),
                // the closest paragraph containing the common parent component (or null, if no paragraph can be determined)
                commonParagraph = (commonPosition.length > 0) ? Position.getCurrentParagraph(rootNode, commonPosition) : null;

            if (docApp.isPresentationApp() && commonParagraph && DOM.isSlideNode(commonParagraph) && !getBooleanOption(options, 'allowSlide', false)) {
                commonParagraph = null;
            }

            return commonParagraph;
        };

        /**
         * Returns, whether the current selection is a text selection inside one paragraph (DOCS-3117).
         *
         * @returns {Boolean}
         *  Whether the current selection is a text selection inside one paragraph.
         */
        this.isSelectionInsideOneParagraph = function () {

            var isSelectionInsidePara = false;

            if  (self.isTextSelection()) {
                var startPoint = Position.getDOMPosition(rootNode, _.initial(self.getStartPosition()));
                if (startPoint && startPoint.node && DOM.isParagraphNode(startPoint.node)) {
                    if (self.isTextCursor() || (self.getStartPosition().length === self.getEndPosition().length && _.isEqual(_.initial(self.getStartPosition()), _.initial(self.getEndPosition())))) {
                        isSelectionInsidePara = true;
                    }
                }
            }

            return isSelectionInsidePara;
        };

        /**
         * Returns the closest table that contains all nodes of this selection
         * completely.
         *
         * @param {Object} [options]
         *  An object containing some additional properties:
         *  @param {Boolean} [options.onlyTableNode=false]
         *      Whether only table nodes shall be returned. If not specified, also drawings of type 'table'
         *      will be returned.
         *
         * @returns {HTMLTableElement|Null}
         *  The closest table containing this selection or null, if the selection is not contained in a
         *  single table.
         */
        this.getEnclosingTable = function (options) {

            var // position of closest common parent component containing the selection
                commonPosition = this.getClosestCommonPosition();

            // handling OX Presentation problem with logical position of slide selection
            if (docApp.isPresentationApp() && self.isSlideSelection()) { return null; }

            // the closest table containing the common parent component
            return (commonPosition.length > 0) ? Position.getCurrentTable(rootNode, commonPosition, options) : null;
        };

        /**
         * Provides the text contents from the selection without keeping any
         * paragraph structure.
         *
         * @returns {String|Null}
         *  The text of the current selection or null if no text is available.
         */
        this.getSelectedText = function () {

            let text = null;

            this.iterateNodes(function (node, _pos, start, length) {
                text = text || "";
                text += getTextSpanContent(node, start, length);
            });

            return text;
        };

        /**
         * Helper function to get the text of the selection and keeping the paragraph structure of
         * the selected text.
         *
         * @returns {String}
         *  The selected text with "\n" to keep the paragraph structure.
         */
        this.getStructuredSelectedText = function () {

            let selectedText = "";
            const selectedParagraphs = [];
            const allLanguagesInSelection = [];

            this.iterateContentNodes((contentNode, position, startOffset, endOffset) => {

                const onePara = {};
                onePara.position = position;
                onePara.startOffset = startOffset;
                onePara.endOffset = endOffset;
                let paraText = "";
                const allLanguagesInParagraph = [];
                const allDrawingsInParagraph = [];

                let characterAtts = getExplicitAttributes(contentNode, "character");
                if (characterAtts?.language) {
                    allLanguagesInParagraph.push(characterAtts.language);
                    if (!allLanguagesInSelection.includes(characterAtts?.language)) { allLanguagesInSelection.push(characterAtts.language); }
                }

                Position.iterateParagraphChildNodes(contentNode, function (node, nodeStart, nodeLength, nodeOffset, offsetLength) {
                    if (DOM.isTextSpan(node)) {
                        paraText += getTextSpanContent(node, nodeOffset, offsetLength);
                    } else if (DOM.isInlineComponentNode(node)) {
                        paraText += " ";
                    }
                    characterAtts = getExplicitAttributes(node, "character");
                    if (characterAtts?.language && !allLanguagesInParagraph.includes(characterAtts?.language)) {
                        allLanguagesInParagraph.push(characterAtts.language);
                        if (!allLanguagesInSelection.includes(characterAtts?.language)) { allLanguagesInSelection.push(characterAtts.language); }
                    }
                    if (nodeLength === 1 && (DrawingFrame.isDrawingFrame(node) || DOM.isDrawingPlaceHolderNode(node))) {
                        // const drawingPosition = Position.getOxoPosition(contentNode, node);
                        allDrawingsInParagraph.push(nodeStart); // Check: is nodeStart a reliable oxo position?
                    }
                }, this, { allNodes: true, start: startOffset, end: endOffset });

                onePara.selectedText = paraText;
                onePara.isEmpty = paraText.length === 0;
                onePara.drawings = allDrawingsInParagraph.length > 0 ? allDrawingsInParagraph : null;
                onePara.languages = allLanguagesInParagraph;
                selectedParagraphs.push(onePara);
            });

            // convert array to a string
            selectedParagraphs.forEach((para, index) => {
                selectedText += para.selectedText;
                if (index < selectedParagraphs.length - 1) { selectedText += "\n"; }
            });

            return { selectedText, selectedParagraphs, allLanguagesInSelection };
        };

        /**
         * Returns an object describing the table cell range that is currently
         * selected.
         *
         * @returns {Object|Null}
         *  If this selection is contained completely inside a table, returns
         *  an object containing the following attributes:
         *  - {HTMLTableElement} tableNode: the table element containing the
         *      selection,
         *  - {Number[]} tablePosition: the logical position of the table,
         *  - {Number[2]} firstCellPosition: the logical position of the first
         *      cell, relative to the table (contains exactly two elements:
         *      row, column),
         *  - {Number[2]} lastCellPosition: the logical position of the last
         *      cell, relative to the table (contains exactly two elements:
         *      row, column),
         *  - {Number} width: the number of columns covered by the cell range,
         *  - {Number} height: the number of rows covered by the cell range.
         *  Otherwise, this method returns null.
         */
        this.getSelectedCellRange = function () {

            var // the result object containing all info about the cell range
                result = { tableNode: this.getEnclosingTable() },
                // whether this is a selected table drawing
                isTableDrawingSelection = false;

            if (!result.tableNode && slideMode && self.isDrawingSelection() && DrawingFrame.isTableDrawingFrame(self.getSelectedDrawing())) {
                result = { tableNode: self.getSelectedDrawing() };  // using the table drawing node
                isTableDrawingSelection = true;
            }

            // no table found or this is a slide selection (47505)
            if (!result.tableNode || (slideMode && self.isTopLevelTextCursor())) {
                return null;
            }

            // logical position of the table
            result.tablePosition = Position.getOxoPosition(rootNode, result.tableNode, 0);

            if (isTableDrawingSelection) {
                result.firstCellPosition = [0, 0];
                result.lastCellPosition = Position.getOxoPosition(rootNode, DOM.getTableCells(result.tableNode).last(), 0).slice(2);
            } else {
                // convert selection positions to cell positions relative to table
                if ((startPosition.length < result.tablePosition.length + 2) || (endPosition.length < result.tablePosition.length + 2)) {
                    globalLogger.error('Selection.getSelectedCellRange(): invalid start or end position');
                    return null;
                }
                result.firstCellPosition = startPosition.slice(result.tablePosition.length, result.tablePosition.length + 2);
                result.lastCellPosition = endPosition.slice(result.tablePosition.length, result.tablePosition.length + 2);
            }

            // width and height of the range for convenience
            result.width = result.lastCellPosition[1] - result.firstCellPosition[1] + 1;
            result.height = result.lastCellPosition[0] - result.firstCellPosition[0] + 1;

            return result;
        };

        /**
         * Returns the anchor cell range of a cell selection.
         *
         * @returns {DOM.Range}
         *  A DOM range describing the anchor cell of a cell selection,
         *  or null, if not set.
         */
        this.getAnchorCellRange = function () {
            return anchorCellRange;
        };

        /**
         * Returns the anchor cell range of a cell selection.
         *
         * @param {DOM.Range} range
         *  The DOM range, that describes the position of the anchor
         *  cell in a cell selection.
         */
        this.setAnchorCellRange = function (range) {
            anchorCellRange = range;
        };

        /**
         * Returns the drawing node currently selected.
         *
         * @returns {jQuery}
         *  A jQuery collection containing the currently selected drawing, if
         *  existing; otherwise an empty jQuery collection.
         */
        this.getSelectedDrawing = function () {
            return selectedDrawing;
        };

        /**
         * Returns the type of the drawing node currently selected. This function
         * does not handle multi drawing selections. In this case it is necessary
         * to use 'getSelectedDrawingsTypes'.
         *
         * @returns {String}
         *  The type of the drawing object currently selected ('image',
         *  'chart', 'shape', etc.); or the empty string, if no drawing object
         *  is selected.
         */
        this.getSelectedDrawingType = function () {
            return (selectedDrawing.length > 0) ? DrawingFrame.getDrawingType(selectedDrawing) : '';
        };

        /**
         * Returns the text frame drawing node, that is currently selected.
         *
         * @returns {jQuery}
         *  A jQuery collection containing the currently selected text frame
         *  drawing, if existing; otherwise an empty jQuery collection.
         */
        this.getSelectedTextFrameDrawing = function () {
            return selectedTextFrameDrawing;
        };

        /**
         * Returns the text frame drawing node, that is currently selected. This can
         * be an additionally selected text frame node, if this is a text selection
         * inside a text frame, or a directly selected text frame node, if this is
         * a selection of type 'drawing'.
         *
         * @param {Object} [options]
         *  An object containing some additional properties:
         *  @param {Boolean} [options.forceTextFrame=false]
         *      Whether the returned drawing node must be a text frame. Alternatively
         *      it is possible, that a 'group' drawing is selected additionally to a
         *      text selection inside a text frame. If 'forceTextFrame' is set to true,
         *      not this selected 'group' drawing node is returned, but the not selected
         *      text frame that contains the current text selection.
         *
         * @returns {jQuery}
         *  A jQuery collection containing the currently selected text frame
         *  drawing, if existing; otherwise an empty jQuery collection.
         */
        this.getAnyTextFrameDrawing = function (options) {

            var // whether the selected drawing must be a text frame
                forceTextFrame = getBooleanOption(options, 'forceTextFrame', false),
                // the text frame, that will be returned
                textFrame = $();

            if (self.isAdditionalTextframeSelection()) {
                if (DrawingFrame.isTextFrameShapeDrawingFrame(self.getSelectedTextFrameDrawing())) {
                    textFrame = self.getSelectedTextFrameDrawing();
                } else {
                    if (forceTextFrame) {
                        textFrame = Position.getClosestDrawingTextframe(rootNode, self.getStartPosition());  // finding the text frame, for example inside a group
                        textFrame = textFrame || $();
                    } else {
                        textFrame = self.getSelectedTextFrameDrawing();  // returning a group is also allowed
                    }
                }
            } else if ((self.getSelectionType() === 'drawing' && DrawingFrame.isTextFrameShapeDrawingFrame(self.getSelectedDrawing()))) {
                textFrame = self.getSelectedDrawing();
            }

            return textFrame;
        };

        /**
         * Collecting all drawing nodes in the current selection. If a filter is specified
         * the selection can be reduced to specific drawing nodes.
         *
         * @param {Function} filter
         *  A function that can be used to filter the drawings in the selection.
         *
         * @returns {jQuery[]|[]}
         *  An array that contains all drawing nodes in the selection. Or an empty array
         *  if there are no (filtered) drawings in the selection.
         */
        this.getAllDrawingsInSelection = function (filter) {

            var // the collector for all drawing nodes in the selection
                allDrawings = [];

            self.iterateDrawings(function (oneDrawing) {
                if (!filter || (filter && filter(oneDrawing))) {
                    allDrawings.push($(oneDrawing));
                }
            }, this);

            return allDrawings;
        };

        /**
         * Returns the type of the drawing node(s) currently selected.
         *
         * @returns {String}
         *  The type of the drawing object currently selected ('image', 'shape'
         *  'chart', 'connector', etc.); or the empty string, if no drawing object
         *  is selected or if two or more drawings with different types are selected.
         */
        this.getClosestSelectedDrawingType = function () {
            var types = self.getSelectedDrawingsTypes();
            return (types.size === 1) ? itr.first(types) : '';
        };

        /**
         * Returns the DOM clipboard node used to store a copy of the contents
         * of a selected drawing frame for copy&paste.
         *
         * @returns {jQuery}
         *  The clipboard node, as jQuery object.
         */
        this.getClipboardNode = function () {
            return clipboardNode;
        };

        /**
         * Setting the focus into the clipboard node. This is done, if one or more drawings
         * are selected or if this is a 'slide selection'.
         *
         * @param {Object} [options]
         *  An object containing some additional properties:
         *  @param {Boolean} [options.specialTouchHandling=false]
         *      Whether the slide pane node shall be used instead of the focus node. This
         *      is important to avoid, that the virtual keyboard appears, if the user makes
         *      a 'slide selection'. On touch devices the virtual keyboard shall not appear,
         *      if no object is selected on the slide.
         */
        this.setFocusIntoClipboardNode = function (options) {

            var // whether on touch devices an alternative shall be used to avoid keyboard flickering
                specialTouchHandling = getBooleanOption(options, 'specialTouchHandling', false);

            if (specialTouchHandling && TOUCH_DEVICE && docApp.getView().getSlidePane() && docApp.getView().getSlidePane().getSlidePaneContainer()) {

                // on touch devices, the focus can alternatively be set into the slide pane. The keyboard
                // handling is not supported ('tab', 'ctrl-A', ...), so that the focus is not required in
                // the clipboard node. Copy/paste of drawings is also not supported on touch devices, yet.

                setFocus(docApp.getView().getSlidePane().getSlidePaneContainer());
            } else {

                if (clipboardNode.contents().length === 0) {
                    clipboardNode.text('\xa0');
                }
                setFocus(clipboardNode);
            }
        };

        /**
         * Updates the contents of the clipboard node, according to the current
         * selection.
         */
        this.updateClipboardNode = function () {
            // an array of the selected drawing nodes, or only one if multi selection is not supported
            var allSelectedDrawings = null;

            // inserts a clone of the passed image node into the clipboard
            function copyDrawingNode(drawingNode) {

                var // the source URL of the image
                    sourceUrl = null,
                    // the image node inside the drawing
                    imgNode = null;

                if (DOM.isImageNode(drawingNode)) {

                    imgNode = $(drawingNode).find('img');

                    if (imgNode.length > 0) {

                        // insert the image clone into the clipboard node
                        imgNode = imgNode.clone();
                        // remove CSS position information that may make the image visible
                        imgNode.css({ position: '', top: '', left: '', bottom: '', right: '' });
                        clipboardNode.append(imgNode);

                        // IE needs attributes for width/height instead of styles
                        imgNode.attr({ width: imgNode.width(), height: imgNode.height() });

                        if (DOM.isDocumentImageNode(imgNode)) {

                            // additional attributes to check for copy&paste inside the same editor instance
                            sourceUrl = DOM.getUrlParamFromImageNode(drawingNode, 'get_filename');
                            imgNode.attr('alt', JSON.stringify({
                                altsrc: sourceUrl,
                                sessionId: DOM.getUrlParamFromImageNode(drawingNode, 'session'),
                                fileId: DOM.getUrlParamFromImageNode(drawingNode, 'id')
                            }));

                            // replace source URL with Base64 data URL, doesn't work correctly on IE
                            if (!_.browser.IE) {
                                imgNode.attr('src', DOM.getBase64FromImageNode(imgNode, getMimeTypeFromImageUri(sourceUrl)));
                            }
                        }
                    }
                }
            }

            // get the selected drawings for multi selection or the single selected drawing otherwise
            if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                allSelectedDrawings = self.getAllSelectedDrawingNodes() || [];
            } else {
                allSelectedDrawings = (selectedDrawing.length > 0) ? [selectedDrawing] : [];
            }

            // prevent multiple clones of the same drawing frame
            if (clipboardContainsDrawings(allSelectedDrawings)) {
                return;
            }

            // clear clip board node, register drawing nodes at clip board node
            clearClipboardNode();
            clipboardNode.data('source-nodes', allSelectedDrawings);

            // clone contained image nodes
            _.each(allSelectedDrawings, copyDrawingNode);

            // make sure the clip board node has some content, so that it can get the browser selection
            if (clipboardNode.contents().length === 0) {
                clipboardNode.text('\xa0');
            }
        };

        /**
         * Returns the bounding rectangle of the focus position relative to the
         * entire document page, if this selection is of type 'text'.
         *
         * @param {Object} [options]
         *  An object containing some additional properties. This options are
         *  given to the function "getPointForTextPosition".
         *
         * @returns { DOMRect | null}
         *  Returns a DOMRect object containing read-only left, top, right and bottom
         *  properties describing the border box, in pixels. Or null, if this cannot
         *  be determined or the selection type is not "text".
         */
        this.getFocusPositionBoundRect = function (options) {

            var // the boundaries of the cursor
                boundRect = null,
                // the DOM point of the focus position
                focusPoint = null,
                // the DOM Range object
                docRange = null;

            if (this.getSelectionType() === 'text') {

                // Build DOM Range object from calculated focus point instead
                // of the settings in the core browser selection object (the
                // focusNode and focusOffset attributes). Needed because Chrome
                // reports wrong DOM nodes if cursor is located in an empty
                // paragraph (the browser selection object sometimes returns
                // the last text node of the preceding paragraph instead of the
                // empty text node of the current paragraph).
                // Performance: Special handling for insertText operation, where
                // the node was already saved before
                if (isInsertTextOperation && insertTextPoint) {
                    focusPoint = getPointForTextPosition(null, insertTextPoint);
                } else {
                    focusPoint = getPointForTextPosition(getFocusPosition(), undefined, options);
                }

                if (focusPoint) {

                    // If the cursor points into an empty text span, calculate its
                    // position directly instead of using the getClientRects()
                    // method of the DOM Range object. This is needed because
                    // Chrome has problems calculating the bounding rectangle of an
                    // empty text node.
                    // Zoomed documents in Firefox needs this correction also, but only
                    // if the zoom factor is not 100.
                    // Allowing this for zoom factor 100 in Firefox leads to task 32397
                    if ((_.isString(focusPoint.node.nodeValue) && focusPoint.node.nodeValue.length === 0) || (_.browser.Firefox && docApp.getView().getZoomFactor() !== 1)) {
                        boundRect = getNodePositionInPage(focusPoint.node.parentNode);
                    } else {
                        try {
                            // Build a DOM range object needed to get the actual
                            // position of the focus position between two characters.
                            docRange = window.document.createRange();
                            docRange.setStart(focusPoint.node, focusPoint.offset);
                            docRange.setEnd(focusPoint.node, focusPoint.offset);
                            boundRect = docRange.getClientRects()[0];
                        } catch {
                            globalLogger.error('Selection.getFocusPositionBoundRect(): failed to initialize focus range');
                        }
                    }
                }
            }

            return boundRect;
        };

        // low-level browser selection ----------------------------------------

        /**
         * Returns an array of DOM ranges representing the current browser
         * selection inside the editor root node.
         *
         * @returns {Object}
         *  An object that contains a property 'active' with a DOM.Range object
         *  containing the current anchor point in its 'start' property, and
         *  the focus point in its 'end' property. The focus point may precede
         *  the anchor point if selecting backwards with mouse or keyboard. The
         *  returned object contains another property 'ranges' which is an
         *  array of DOM.Range objects representing all ranges currently
         *  selected. Start and end points of these ranges are already adjusted
         *  so that each start point precedes the end point.
         */
        this.getBrowserSelection = function () {
            return getBrowserSelection(rootNode);
        };

        /**
         * Returns the text covered by the current browser selection as plain
         * text string.
         *
         * @returns {String}
         *  The text covered by the current browser selection.
         */
        this.getTextFromBrowserSelection = function () {
            return window.getSelection().toString();
        };

        /**
         * Returns the currently selected contents as HTML mark-up.
         *
         * @returns {String}
         *  The selected content, as HTML mark-up string.
         */
        this.getHtmlFromBrowserSelection = function () {

            var // the browser selection
                selection = window.getSelection(),
                // the result container
                resultNode = $('<div>');

            // read all selection ranges
            for (var index = 0; index < selection.rangeCount; index += 1) {
                resultNode.append(selection.getRangeAt(index).cloneContents());
            }
            resultNode.find('tr.pb-row').remove(); //filter rows that contains page breaks - they should not be copied

            return resultNode.html();
        };

        /**
         * Sets the browser selection to the passed DOM ranges in the editor
         * root node.
         *
         * @param {DOM.Range[]|DOM.Range} ranges
         *  The DOM ranges representing the new browser selection. May be an
         *  array of DOM range objects, or a single DOM range object. Must be
         *  contained in the editor root node.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preserveFocus=false]
         *      If set to true, the DOM element currently focused will be
         *      focused again after the browser selection has been set. Note
         *      that doing so will immediately lose the new browser selection,
         *      if focus is currently inside a text input element.
         *  @param {Boolean} [options.forceWhenReadOnly=false]
         *      If set to true, the browser selection gets also set in
         *      read only mode which otherwise would be prevented.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.setBrowserSelection = function (ranges, options) {
            // save custom implementation of the focus() method
            var customFocusMethod = rootNode[0].focus;
            // whether to set the browser selection even if in read only mode
            var forceWhenReadOnly = getBooleanOption(options, 'forceWhenReadOnly', false);

            // Prevent popup closing if the document has readonly modus and the editor is typing. Bug 44088
            if (!docApp.isEditable() && !forceWhenReadOnly && !$(document.activeElement).hasClass('page')) {
                return this;
            }
            rootNode[0].focus = originalFocusMethod;
            setBrowserSelection(rootNode, ranges, options);
            rootNode[0].focus = customFocusMethod;
            return this;
        };

        /**
         * Performance: Saving paragraph node, the logical position of the paragraph
         * and the text position during several operations. This values can be reused
         * during cursor setting after this operations or for directly following
         * operations. This reduces Position.getDOMPosition to the final value of
         * the logical position.
         *
         * @param {HTMLElement|jQuery} node
         *  The paragraph element as DOM node or jQuery object.
         *
         * @param {[Number]} pos
         *  The logical position of the paragraph node.
         *
         * @param {Number} offset
         *  The offset inside the paragraph node.
         */
        this.setParagraphCache = function (node, pos, offset) {
            paragraphCache = node ? { node, pos, offset } : null;
        };

        /**
         * Performance: Returning the paragraph node, the logical position of the paragraph
         * and the text position of the logical position of specific operations.
         * This values can be reused during cursor setting after the operation or for
         * following operations. This reduces Position.getDOMPosition to the
         * final value of the logical position.
         *
         * @returns {Object}
         *  An object containing the properties 'node', 'pos' and 'offset', that represent
         *  the paragraph node, the logical position of the paragraph and the offset of
         *  for the text level inside the paragraph. The paragraph node is the most inner
         *  paragraph, so that this paragraph together with the offset describes
         *  the complete cursor position and simplifies the usage of Postion.getDOMPosition
         *  to the last value. The logical paragraph position in the property 'pos' can be
         *  used to check, if in another following operation the paragraph node can be
         *  reused.
         */
        this.getParagraphCache = function () {
            return paragraphCache;
        };

        /**
         * Sets the browser selection to the contents of the passed DOM element
         * node.
         *
         * @param {HTMLElement|jQuery} containerNode
         *  The DOM node whose contents will be completely selected. This node
         *  must be focusable, and will be focused after the browser selection
         *  has been set (except if specified otherwise, see options below).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preserveFocus=false]
         *      If set to true, the DOM element currently focused will be
         *      focused again after the browser selection has been set. Note
         *      that doing so will immediately lose the new browser selection,
         *      if focus is currently inside a text input element.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.setBrowserSelectionToContents = function (containerNode, options) {
            containerNode = getDomNode(containerNode);
            setBrowserSelection(containerNode, DOM.Range.createRange(containerNode, 0, containerNode, containerNode.childNodes.length), options);
            return this;
        };

        /**
         * Performance: Saving point calculated from Position.getDOMPosition for
         * insertText and splitParagraph operations.
         *
         * @param {Object} point
         *  The point calculated from Position.getDOMPosition, containing the
         *  html node element and the offset.
         */
        this.setInsertTextPoint = function (point) {
            insertTextPoint = point;
        };

        /**
         * Performance: Getting point calculated from Position.getDOMPosition for
         * insertText and splitParagraph operations.
         *
         * @returns {Object}
         *  The point calculated from Position.getDOMPosition, containing the
         *  html node element and the offset.
         */
        this.getInsertTextPoint = function () {
            return insertTextPoint;
        };

        /**
         * Performance: Register, whether the current operation is an insertText
         * operation. In this case the function 'getFocusPositionBoundRect' can use
         * the saved DOM in 'insertTextPoint'.
         */
        this.registerInsertText = function (options) {
            isInsertTextOperation = getBooleanOption(options, 'insertOperation', false);
        };

        /**
         * Restoring the selection of a specified drawing node.
         *
         * @param {jQuery|Node} node
         *  The drawing, whose selection is repainted.
         */
        this.restoreDrawingSelection = function (node) {
            DrawingFrame.clearSelection(node);
            self.drawDrawingSelection(node);
        };

        /**
         * Check, whether a specified (drawing) node is selected.
         *
         * @param {jQuery|Node} node
         *  The (drawing) node that is checked, if it is selected.
         *
         * @returns {Boolean}
         *  Whether the specified (drawing) node is selected.
         */
        this.isSelectedDrawingNode = function (node) {
            return $(node).hasClass('selected');
        };

        // selection manipulation ---------------------------------------------

        /**
         * Restores the browser selection according to the current logical
         * selection represented by this instance.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preserveFocus=false]
         *      If set to true, the DOM element currently focused will be
         *      focused again after the browser selection has been set. Note
         *      that doing so will immediately lose the new browser selection,
         *      if focus is currently inside a text input element.
         *  @param {Boolean} [options.keepFocus=false]
         *      If set to true, the DOM element currently focused will not
         *      lose the focus. This is especially important for concurrent
         *      editing, so that for example currently open controls are not
         *      closed by an externally forced selection change.
         *  @param {Boolean} [options.forceWhenReadOnly=false]
         *      If set to true, the browser selection gets also set in read
         *      only mode which otherwise would be prevented. This value is
         *      evalutated in function 'selection.setBrowserSelection()'.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.restoreBrowserSelection = function (options) {

            var // the DOM ranges representing the logical selection
                ranges = [],
                // start and end DOM point for text selection
                startPoint = null, endPoint = null,
                // the DOM element currently focused
                focusNode = $(getFocus()),
                // Performance: whether this is an insertText operation
                insertOperation = getBooleanOption(options, 'insertOperation', false),
                // Performance: whether this is an insertText operation
                splitOperation = getBooleanOption(options, 'splitOperation', false),
                // Performance: whether this is a 'simpleTextSelection' operation (backspace, delete, return, insertText)
                simpleTextSelection = getBooleanOption(options, 'simpleTextSelection', false),
                // Performance: whether this is a cursor selection
                isCursor = getBooleanOption(options, 'isCursor', false),
                // whether the currently focused node must not lose the focus
                keepFocus = getBooleanOption(options, 'keepFocus', false),
                // a local helper position
                localEndPosition = null,
                // logical text position for temporary usage
                localStartPos;

            if (docApp.isInQuit() || keepFocus) {
                return this;
            }

            // Do not preserve focus, if it is inside root node or clipboard
            // (focus may switch nodes depending on new selection type).
            // Bug 28195: IE contains table cells that are content-editable on
            // their own and therefore are focused instead of the root node.
            if ((focusNode.length === 0) || containsFocus(rootNode, { allowSelf: true }) || focusNode.is(clipboardNode)) {
                options = { ...options, preserveFocus: false };
            }

            switch (this.getSelectionType()) {

                // text selection: select text range
                case 'text':
                    // Performance: Simple process for cursor selections, calling Position.getDOMPosition only once
                    if ((insertOperation || splitOperation) && paragraphCache && paragraphCache.node && _.isNumber(paragraphCache.offset)) {
                        startPoint = Position.getDOMPosition(paragraphCache.node, [paragraphCache.offset]);  // Performance: Using saved paragraph node with last oxo position value -> very fast getDOMPosition
                        self.setInsertTextPoint(startPoint); // Performance: Save for later usage
                    } else {
                        startPoint = Position.getDOMPosition(rootNode, startPosition);
                        // Fix for 32002, skipping floated drawings at beginning of document, webkit only, only complete selections (Ctrl-A)
                        if (_.browser.WebKit && !isCursor && _.isEqual(startPosition, getFirstPosition()) && _.isEqual(endPosition, getLastPosition()) &&
                            DOM.isEmptySpan(startPoint.node.parentNode) &&
                            startPoint.node.parentNode.nextSibling &&
                            (DOM.isFloatingDrawingNode(startPoint.node.parentNode.nextSibling) || DOM.isOffsetNode(startPoint.node.parentNode.nextSibling))) {
                            localStartPos = Position.skipFloatedDrawings(rootNode, startPosition); // Skipping floated drawings without modifying the original start position (32002)
                            startPoint = Position.getDOMPosition(rootNode, localStartPos);
                        }

                        // Problem with cursor traversal over drawing place holder
                        if (isCursor &&  startPoint && startPoint.node && DOM.isDrawingLayerNode(startPoint.node.parentNode)) {
                            startPoint.node = DOM.getDrawingPlaceHolderNode(startPoint.node).nextSibling;
                            startPoint.offset = 0;
                        }

                    }
                    if (startPoint && (simpleTextSelection || isCursor)) {
                        ranges.push(new DOM.Range(startPoint, startPoint));
                    } else {
                        if (_.browser.WebKit && selectionExternallyModified && endSelectionExternallyModified) {
                            // special Chrome trick. Otherwise the selection of the final paragraph
                            // in a multi-paragraph selection will not be displayed correctly.
                            localEndPosition = Position.decreaseLastIndex(endPosition);
                        }

                        endPoint = Position.getDOMPosition(rootNode, localEndPosition ? localEndPosition : endPosition);

                        if (_.browser.Safari && !isCursor && docApp.isTextApp()) {

                            // DOCS-4652: Safari browser cannot set startpoint into an empty text node in front of an absolute positioned drawing at the beginning of the document
                            if (startPoint?.node?.nodeType === 3 && startPoint.offset === 0 && startPosition.every(n => n === 0) && DOM.isAbsoluteParagraphDrawing(startPoint.node.parentNode.nextSibling)) {
                                const firstParagraph = Position.getParagraphElement(rootNode, [0]);
                                if (firstParagraph) {
                                    const newTextSpan = DOM.getTextSpanBeforeOrAfterAbsoluteParagraphDrawings(firstParagraph);
                                    if (DOM.isTextSpan(newTextSpan)) { startPoint.node = newTextSpan.firstChild; }
                                }
                            }

                            // DOCS-4714: Safari browser cannot set endpoint into an empty text node behind an absolute positioned drawing at the end of the document
                            if (endPoint?.node?.nodeType === 3 && endPoint.offset === 0) {
                                const textSpan = endPoint.node.parentNode;
                                if (DOM.isEmptySpan(textSpan) && !textSpan.nextSibling && DOM.isAbsoluteParagraphDrawing(textSpan.previousSibling)) {
                                    const para = textSpan.parentNode;
                                    if (para && !para.nextSibling) {
                                        // this is the last position in the document
                                        const newTextSpan = DOM.getTextSpanBeforeOrAfterAbsoluteParagraphDrawings(para, { before: true });
                                        if (DOM.isTextSpan(newTextSpan)) {
                                            endPoint.node = newTextSpan.firstChild;
                                            endPoint.offset = endPoint.node.nodeValue.length;
                                        }
                                    }
                                }
                            }
                        }

                        if (startPoint && endPoint) {
                            ranges.push(new DOM.Range(backwards ? endPoint : startPoint, backwards ? startPoint : endPoint));
                        } else {
                            if (problemExternallyHandled || isTemporaryInvalidSelection) { return this; }
                            globalLogger.error('Selection.restoreBrowserSelection(): missing text selection range');
                        }
                    }

                    // enabling the possibility to select a range, that starts or ends with an absolutely positioned drawing (see 40739)
                    // -> switching from absolute drawing to its placeholder. Otherwise the browser cannot draw a visible selection.
                    if (!isCursor) {
                        if (startPoint && startPoint.node && DOM.isDrawingLayerNode(startPoint.node.parentNode) && endPoint && endPoint.node && endPoint.node.nodeType === 3) {
                            startPoint.node = DOM.getDrawingPlaceHolderNode(startPoint.node);
                        } else if (endPoint && endPoint.node && DOM.isDrawingLayerNode(endPoint.node.parentNode) && startPoint && startPoint.node && startPoint.node.nodeType === 3) {
                            endPoint.node = DOM.getDrawingPlaceHolderNode(endPoint.node);
                        }
                    }

                    // Now the startPoint is available and can be used to check, if next to the text selection, an
                    // additional selection of the text frame is required. Checking this inside applyBrowserSelection()
                    // would be less performant, because it would be necessary to use the startPosition array, to check,
                    // if the logical position is located inside a text frame.
                    if (startPoint && startPoint.node) { handleAdditionalTextFrameSelection(startPoint.node); }

                    break;

                // cell selection: iterate all cells
                case 'cell':
                    this.iterateTableCells(function (cell) {
                        ranges.push(DOM.Range.createRangeForNode(cell));
                    });
                    break;

                // drawing selection: select the clipboard node
                case 'drawing':
                    break;

                default:
                    globalLogger.error('Selection.restoreBrowserSelection(): unknown selection type');
            }

            // Set the new selection ranges. Bug 26283: when a drawing frame is
            // selected, do not focus root node to prevent scroll flickering.
            if (ranges.length > 0) {
                self.setBrowserSelection(ranges, options);

            // Bug #52519
            //      Do NOT grab the focus to the textframework-clipboard-node in Spreadsheet-App
            } else if (!docApp.isSpreadsheetApp()) {
                self.setBrowserSelectionToContents(clipboardNode, options);
            }
            return this;
        };

        /**
         * Handling cursor up and down event inside a text frame. The selection must
         * not leave the text frame.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         *
         * @returns {Boolean}
         *  Whether the event was handled within this function.
         */
        this.handleVerticalCursorKeyInTextFrame = function (event) {

            var // whether the event was handled inside this function
                handledEvent = false,
                // the text frame node containing the current selection
                textFrameNode = null,
                // the old logical position that needs to be checked and the new logical position
                oldPos = null, newPos = null,
                // the paragraph at the specified position
                paraNode = null,
                // if docApp is editable
                isEditable = docApp.isEditable(),
                // start and end DOM Points
                startPosPoint = null, endPosPoint = null,
                // Range object containing start and end Points
                ranges = null;

            // avoiding that a vertical cursor in a text frame leaves the text frame
            if (hasKeyCode(event, 'DOWN_ARROW', 'UP_ARROW') && self.isAdditionalTextframeSelection()) {

                textFrameNode = self.getSelectedTextFrameDrawing();
                oldPos = backwards ? self.getStartPosition() : self.getEndPosition();
                paraNode = Position.getParagraphElement(rootNode, _.initial(oldPos));

                if (hasKeyCode(event, 'DOWN_ARROW')) {
                    if (paraNode && paraNode === getDomNode(textFrameNode.find(DOM.CONTENT_NODE_SELECTOR).last())) {
                        newPos = Position.getLastTextPositionInTextFrame(rootNode, textFrameNode);
                    }
                } else {
                    if (paraNode && paraNode === getDomNode(textFrameNode.find(DOM.CONTENT_NODE_SELECTOR).first())) {
                        newPos = Position.getFirstTextPositionInTextFrame(rootNode, textFrameNode);
                    }
                }

                // setting the new text selection
                if (newPos) {
                    if (event.shiftKey) {
                        if (backwards) {
                            self.setTextSelection(self.getEndPosition(), newPos);
                        } else {
                            self.setTextSelection(self.getStartPosition(), newPos);
                        }
                        startPosPoint = getPointForTextPosition(backwards ? self.getEndPosition() : self.getStartPosition());
                        endPosPoint = getPointForTextPosition(newPos);
                    } else {
                        self.setTextSelection(newPos);
                        startPosPoint = getPointForTextPosition(newPos);
                        endPosPoint = startPosPoint;
                    }
                    if (!isEditable) { // in read-only mode setBrowserSelection is prevented because of #44088, on keypress we need to force it
                        ranges = new DOM.Range(startPosPoint, endPosPoint);
                        setBrowserSelection(rootNode, ranges);
                    }
                    handledEvent = true;
                }
            }

            return handledEvent;
        };

        /**
         * Running the function 'processBrowserEvent' only, if an optionally specified amount of time is over.
         * This is especially important for IOS to avoid too many calls of 'processBrowserEvent', that are
         * triggered by the 'selectionchange' event. It is important to listen to this event on IOS, so that
         * selection changes triggered by the blue selection grabbers are recognized.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         *
         * @param {Object} [options]
         *  The optional parameters supported by 'processBrowserEvent'.
         *
         * @param {Number} [delay]
         *  An optional delay of time that must be over since the last call of 'processBrowserEvent'. If not
         *  specified, a default time is used.
         *
         * @returns {jQuery.Promise}
         *  The promise of 'processBrowserEvent', if this function is called. Otherwise a resolved promise is
         *  returned.
         */
        this.processBrowserEventIfDelayed = function (event, options, delay) {

            var localDelay = _.isNumber(delay) ? delay : 500;

            if (!processBrowserEventTimer || (Date.now() - localDelay > processBrowserEventTimer)) {
                return self.processBrowserEvent(event, options);
            }

            return $.when();
        };

        /**
         * Processes a browser event that will change the current selection.
         * Supported are 'mousedown' events and 'keydown' events originating
         * from cursor navigation keys and 'selectionchange' events.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.fromMouseUpHandler=false]
         *      Whether this function was called from the mouseUpHandler function.
         *  @param {Boolean} [options.readonly=false]
         *      Whether the document is in read-only mode.
         *  @param {Boolean} [options.internalRestart=false]
         *      Whether this function was called internally as restart (IOS only).
         *
         * @returns {jQuery.Promise | void} // TODO: check-void-return
         *  The promise of a deferred object that will be resolved after the
         *  browser has processed the passed event, and this selection instance
         *  has updated itself according to the new browser selection.
         */
        this.processBrowserEvent = function (event, options) {

            var // deferred return value
                def = self.createDeferred(),
                // whether event is a keydown event
                keyDownEvent = event.type === 'keydown',
                // whether event is a keydown event without modifier keys except SHIFT
                simpleKeyDownEvent = keyDownEvent && !event.ctrlKey && !event.altKey && !event.metaKey,
                // whether event is a Left/Right cursor key
                leftRightCursor = hasKeyCode(event, 'LEFT_ARROW', 'RIGHT_ARROW'),
                // whether event is a Left/Right cursor key
                upDownCursor = hasKeyCode(event, 'UP_ARROW', 'DOWN_ARROW'),
                // whethter this is a vertical cursor move
                isVerticalCursor = upDownCursor || hasKeyCode(event, 'PAGE_UP', 'PAGE_DOWN'),
                // original selection, for post-processing
                originalSelection = { start: this.getStartPosition(), end: this.getEndPosition() },
                // whether the document is in read-only mode
                readOnly = getBooleanOption(options, 'readonly', false),
                // the current paragraph node
                para = null;

            // skipping shrinked implicit paragraphs in read-only mode (28563)
            function skipImplicitShrinkedParagraph() {

                // Checking, if the new position is not inside an implicit paragraph with height 0
                var para = Position.getParagraphElement(rootNode, _.initial(startPosition)),
                    eventTriggered = false;

                if (para && DOM.isImplicitParagraphNode(para) && $(para).height() === 0) {
                    // simply skip this paragraph -> triggering this event again
                    $(rootNode).trigger(event);
                    eventTriggered = true;
                }

                return eventTriggered;
            }

            // setting a timer for the call of this function
            processBrowserEventTimer = Date.now();

            // saving backward cursor movement for later setting of 'backwards' for modified selections
            isBackwardCursor = hasKeyCode(event, 'LEFT_ARROW', 'UP_ARROW');

            // handle simple left/right cursor keys (with and without SHIFT) manually
            if (simpleKeyDownEvent && leftRightCursor) {
                // do not move selection manually with SHIFT, if there is no support for backward selection
                if (!event.shiftKey || SELECTION_COLLAPSE_EXPAND_SUPPORT) {
                    if (moveTextCursor({ extend: event.shiftKey, backwards: hasKeyCode(event, 'LEFT_ARROW') })) {
                        def.resolve();
                    } else {
                        def.reject();
                    }

                    if (readOnly && skipImplicitShrinkedParagraph()) {
                        def.resolve();
                        return;
                    }

                    event.preventDefault();
                    return def.promise();
                }

            }

            // ignoring cursor down events at the document end (DOCS-3291)
            if (simpleKeyDownEvent && docApp.isTextApp() && hasKeyCode(event, 'DOWN_ARROW')) {
                para = Position.getParagraphElement(rootNode, _.initial(startPosition));
                if (DOM.isLastTopLevelNodeInDocument(para)) { // the last paragraph in the document
                    var allStartLinePos = Position.getAllFirstTextPositionsInLines(para, docApp.getView().getZoomFactor(), { useClone: true }); // clone required, otherwise cursor travelling is disturbed
                    if (_.isNumber(_.last(allStartLinePos).offset) && _.last(startPosition) >= _.last(allStartLinePos).offset) {
                        event.preventDefault();
                        self.restoreBrowserSelection();
                        def.resolve();
                        return def.promise(); // ignoring this cursor down
                    }
                }
            }

            // fixes for cursor up and down in text application
            if (simpleKeyDownEvent && upDownCursor && docApp.isTextApp()) {
                const siblingFunc = hasKeyCode(event, 'DOWN_ARROW') ? 'nextSibling' : 'previousSibling';
                para = para || Position.getParagraphElement(rootNode, _.initial(startPosition));
                const neighbourPara = para[siblingFunc];
                let selectionSet = false;

                // Firefox jumps vertically over list paragraphs and paragraphs starting with a tab (DOCS-3539)
                if (_.browser.Firefox) {
                    if (neighbourPara && !DOM.isListParagraphNode(para) && (DOM.isListParagraphNode(neighbourPara) || DOM.isParagraphNodeStartingWithTab(neighbourPara))) {
                        self.setTextSelection(Position.getFirstPositionInParagraph(rootNode, Position.getOxoPosition(rootNode, neighbourPara)));
                        selectionSet = true;
                    }
                }

                // Chrome jumps into paragraph aligned drawings and in any drawing with textframe in cursor-up (DOCS-4109)
                if (_.browser.WebKit) {
                    if (neighbourPara && hasKeyCode(event, 'DOWN_ARROW')) {
                        if ((ary.last(this.getStartPosition()) === 0 && DOM.isParagraphNodeStartingWithDrawing(para)) || (DOM.isParagraphNodeStartingWithDrawing(neighbourPara))) {
                            const textNode = DOM.getFirstTextNodeBehindDrawings(neighbourPara);
                            self.setTextSelection(Position.getOxoPosition(rootNode, textNode));
                            selectionSet = true;
                        }
                    } else if (hasKeyCode(event, 'UP_ARROW') && (
                        Position.isTextPositionBehindDrawingWithTextframe(para, ary.last(this.getStartPosition()), { ignoreOffset: true }) || // DOCS-4109
                        Position.isTextPositionBehindHardbreakAtParaStart(para, ary.last(this.getStartPosition()), { ignoreOffset: true })    // DOCS-5202
                    )) {
                        if (neighbourPara) {
                            const textNode = DOM.getFirstTextNodeBehindDrawings(neighbourPara);
                            if (event.shiftKey) {
                                self.setTextSelection(this.getEndPosition(), Position.getOxoPosition(rootNode, textNode));
                            } else {
                                self.setTextSelection(Position.getOxoPosition(rootNode, textNode));
                            }
                            selectionSet = true;
                        } else if (ary.last(this.getStartPosition()) > 0) {
                            // jump or expand to first position in current paragraph
                            if (event.shiftKey) {
                                self.setTextSelection(this.getEndPosition(), [_.initial(this.getStartPosition()), 0]);
                            } else {
                                self.setTextSelection([_.initial(this.getStartPosition()), 0]);
                            }
                            selectionSet = true;
                        }
                    }
                }

                if (selectionSet) {
                    def.resolve();
                    event.preventDefault();
                    return def.promise();
                }
            }

            // Setting cursor to end of document for webkit browsers (32007)
            if (_.browser.WebKit && event.ctrlKey && !event.shiftKey && (event.keyCode === 35)) {
                self.setTextSelection(getLastPosition());
                def.resolve();
                return def.promise();
            }

            // browser needs to process pending events before its selection can be queried
            this.executeDelayed(function () {

                var // the current browser selection
                    browserSelection = self.getBrowserSelection(),
                    // a temporarily required start position
                    start,
                    // a temporarily required information about a position containing node and offset
                    nodeInfo,
                    // whether the browser selection shall be restored
                    restoreSelection = true,
                    // a current cell node in which the event occurred
                    currentCell,
                    // a current cell node in which the event occurred
                    currentCellContentNode,
                    // IE: A helper logical position
                    tempPosition = null,
                    // selector string for fetching page breaks and its children on click, keypress
                    helperSelectorInTable = 'td .page-break, .pb-row td, .pb-row, td .header.inactive-selection, td .footer.inactive-selection, td .inner-pb-line',
                    // helper string selector for fetching non-allowing nodes inside paragraph
                    helperSelectorInParagraph = '.page-break, .inner-pb-line, .header, .footer',
                    outOfBoundariesSelector = '.header.inactive-selection, .footer.inactive-selection, .page, .header-wrapper, .footer-wrapper',
                    // an optionally modified browser selection object
                    modifiedBrowserSel = null,
                    // a modified end position of the selection
                    newLastPosition = null;

                // helper function, that checks if selection received from the browser needs to be modified
                function modifyBrowserSelection() {

                    var $modifiedBrowserSelActStartNode = null,
                        $modifiedBrowserSelActEndNode = null,
                        modifiedBrowserSelActStartNode = null,
                        modifiedBrowserSelActEndNode = null,
                        pos = null,
                        index = null,  // holds position of row with page breaks inside ranges array
                        manualPbNode = $(),
                        $node = null,
                        $msHardbreak = null,
                        firstParagraph = null,
                        lastSpan = null,
                        descNode = null;

                    // click on nodes like page break, header/footer, etc. and check if state is active or inactive
                    function selectionHasForbidenNodes(node) {
                        var parents = node.parentsUntil('.page', '.header, .footer');
                        if (parents.length) {
                            return parents.last().hasClass('inactive-selection');
                        } else {
                            return DOM.isPageBreakNode(node) || node.parentsUntil('.page', DOM.PAGE_BREAK_SELECTOR).length > 0;
                        }
                    }

                    // helper functions for cursor positioning in Firefox in tables with page break
                    function findNextPosInTd(node, pos) {
                        if (!node.hasClass('pb-row')) {
                            node = node.parentsUntil('.pagecontent', '.pb-row');
                        }
                        if (node.next().find('td').eq(pos).length > 0) {
                            return node.next().find('td').eq(pos).find('span').first();
                        } else {
                            return node.next().find('td').last().find('span').first();
                        }
                    }

                    function findPrevPosInTd(node, pos) {
                        if (!node.hasClass('pb-row')) {
                            node = node.parentsUntil('.pagecontent', '.pb-row');
                        }
                        if (node.prev().find('td').eq(pos).length > 0) {
                            return node.prev().find('td').eq(pos).find('.p').last().children('span').first();
                        } else {
                            return node.prev().find('td').last().find('.p').last().children('span').first();
                        }
                    }

                    // If selection start or selection end contains invalid node (such as page break, header, footer...), determine next valid node
                    function modifyStartEndSelection($node) {
                        if (DOM.isPageBreakNode($node)) {
                            if (hasKeyCode(event, 'UP_ARROW')) {
                                if ($node.prev().length > 0) {
                                    return $node.prev();
                                } else if ($node.parent().prev().length > 0) {
                                    return $node.parent().prev();
                                }
                            } else {
                                if ($node.next().length > 0) {
                                    return $node.next();
                                } else if ($node.parent().next().length > 0) {
                                    return $node.parent().next();
                                }
                            }
                        } else if ($node.hasClass('footer') || $node.hasClass('inner-pb-line')) {
                            if (DOM.isPageBreakNode($node.parent())) {
                                if (hasKeyCode(event, 'DOWN_ARROW')) {
                                    if ($node.parent().next().length > 0) {
                                        return $node.parent().next();
                                    }
                                } else {
                                    if ($node.parent().prev().length > 0) {
                                        return $node.parent().prev();
                                    }
                                }
                            }
                        } else if ($node.hasClass('header')) {
                            if (DOM.isPageBreakNode($node.parent())) {
                                if (hasKeyCode(event, 'UP_ARROW')) {
                                    if ($node.parent().prev().length > 0) {
                                        return $node.parent().prev();
                                    }
                                } else {
                                    if ($node.parent().next().length > 0) {
                                        return $node.parent().next();
                                    }
                                }
                            }
                        } else if ($node.is('td') && $node.parentsUntil('.pagecontent', DOM.PAGE_BREAK_SELECTOR).length > 0) {
                            if (hasKeyCode(event, 'DOWN_ARROW')) {
                                if ($node.parentsUntil('.pagecontent', DOM.PAGE_BREAK_SELECTOR).next().length > 0) {
                                    return $node.parentsUntil('.pagecontent', DOM.PAGE_BREAK_SELECTOR).next();
                                }
                            } else {
                                if ($node.parentsUntil('.pagecontent', DOM.PAGE_BREAK_SELECTOR).prev().length > 0) {
                                    return $node.parentsUntil('.pagecontent', DOM.PAGE_BREAK_SELECTOR).prev();
                                }
                            }
                        } else if ($node.parentsUntil('.page', '.header-wrapper').length > 0) {
                            return DOM.getPageContentNode(rootNode).children('.p').first();
                        } else if ($node.parentsUntil('.page', '.footer-wrapper').length > 0) {
                            return DOM.getPageContentNode(rootNode).children('.p').last();
                        }
                        return false;
                    }

                    // Apply modifications on selection, after modifyStartEndSelection function is called
                    function modifyStartEndNodeForSelection($modBrowserSelNode, pos) {

                        var result = modifyStartEndSelection($modBrowserSelNode);

                        if (!result) {
                            // second try with parent node (IE sometimes returns children of header/footer par elements)
                            result = modifyStartEndSelection($modBrowserSelNode.parentsUntil('.page', helperSelectorInParagraph).first());
                        }
                        if (!result) {
                            globalLogger.warn('Selection: No valid node found to skip over page break!');
                            return false;
                        }
                        if (result.is('table')) { // fallback for IE
                            result = result.find('div.p span').first();
                        }
                        if (result.hasClass('p')) { // fix for IE
                            result = result.children('span').first();
                        }

                        if (pos === 'start') {
                            modifiedBrowserSel.active.start.node = (result)[0];
                            modifiedBrowserSel.active.start.offset = 0;
                        } else {
                            modifiedBrowserSel.active.end.node = (result)[0];
                            modifiedBrowserSel.active.end.offset = 0;
                        }
                    }

                    // start of modifyBrowserSelection

                    // Firefox - table on two pages cursor traversal
                    if (($(browserSelection.active.start.node).is(helperSelectorInTable) || $(browserSelection.active.end.node).is(helperSelectorInTable))) {

                        modifiedBrowserSel = _.clone(browserSelection);
                        modifiedBrowserSelActStartNode = modifiedBrowserSel.active.start.node;
                        modifiedBrowserSelActEndNode = modifiedBrowserSel.active.end.node;
                        pos = originalSelection.start[2];

                        if (hasKeyCode(event, 'DOWN_ARROW')) {
                            //TODO: down-arrow in a mousedown event???
                            if ($(browserSelection.active.start.node).is(helperSelectorInTable)) {
                                modifiedBrowserSelActStartNode = findNextPosInTd($(browserSelection.active.start.node), pos);
                                while (modifiedBrowserSelActStartNode.is(DOM.PAGE_BREAK_SELECTOR)) {
                                    modifiedBrowserSelActStartNode = modifiedBrowserSelActStartNode.next();
                                }
                                modifiedBrowserSel.active.start.node = (modifiedBrowserSelActStartNode)[0];
                                modifiedBrowserSel.active.start.offset = [0];
                            }
                            if ($(browserSelection.active.end.node).is(helperSelectorInTable)) {
                                modifiedBrowserSelActEndNode = findNextPosInTd($(browserSelection.active.end.node), pos);
                                while (modifiedBrowserSelActEndNode.is(DOM.PAGE_BREAK_SELECTOR)) {
                                    modifiedBrowserSelActEndNode = modifiedBrowserSelActEndNode.next();
                                }
                                modifiedBrowserSel.active.end.node = (modifiedBrowserSelActEndNode)[0];
                                modifiedBrowserSel.active.end.offset = [0];
                            }
                        } else {
                            if ($(browserSelection.active.start.node).is(helperSelectorInTable)) {
                                modifiedBrowserSelActStartNode = findPrevPosInTd($(browserSelection.active.start.node), pos);
                                while (modifiedBrowserSelActStartNode.is(DOM.PAGE_BREAK_SELECTOR)) {
                                    modifiedBrowserSelActStartNode = modifiedBrowserSelActStartNode.prev();
                                }
                                modifiedBrowserSel.active.start.node = (modifiedBrowserSelActStartNode)[0];
                                modifiedBrowserSel.active.start.offset = [0];
                            }
                            if ($(browserSelection.active.end.node).is(helperSelectorInTable)) {
                                modifiedBrowserSelActEndNode = findPrevPosInTd($(browserSelection.active.end.node), pos);
                                while (modifiedBrowserSelActEndNode.is(DOM.PAGE_BREAK_SELECTOR)) {
                                    modifiedBrowserSelActEndNode = modifiedBrowserSelActEndNode.prev();
                                }
                                modifiedBrowserSel.active.end.node = (modifiedBrowserSelActEndNode)[0];
                                modifiedBrowserSel.active.end.offset = [0];
                            }

                            if ($(browserSelection.active.start.node).hasClass('pb-row')) { // we dont know direction (to move to next or previous node), so we will get it from ranges
                                _.each(browserSelection.ranges, function (range, i) {
                                    if ($(range.start.node).is('tr.pb-row')) {
                                        index = i;
                                    }
                                });
                                if (browserSelection.ranges[index + 1]) {
                                    modifiedBrowserSel.active.start.node = browserSelection.ranges[index + 1].start.node;
                                    modifiedBrowserSel.active.start.offset = browserSelection.ranges[index + 1].start.offset;
                                } else {
                                    modifiedBrowserSel.active.start.node = browserSelection.ranges[index - 1].start.node;
                                    modifiedBrowserSel.active.start.offset = browserSelection.ranges[index - 1].start.offset;
                                }
                            }
                            if ($(browserSelection.active.end.node).hasClass('pb-row')) { // we dont know direction (to move to next or previous node), so we will get it from ranges
                                if (_.isUndefined(index)) {
                                    _.each(browserSelection.ranges, function (range, i) {
                                        if ($(range.end.node).is('tr.pb-row')) {
                                            index = i;
                                        }
                                    });
                                }
                                if (browserSelection.ranges[index + 1]) {
                                    modifiedBrowserSel.active.end.node = browserSelection.ranges[index + 1].end.node;
                                    modifiedBrowserSel.active.end.offset = browserSelection.ranges[index + 1].end.offset;
                                } else {
                                    modifiedBrowserSel.active.end.node = browserSelection.ranges[index - 1].end.node;
                                    modifiedBrowserSel.active.end.offset = browserSelection.ranges[index - 1].end.offset;
                                }
                            }
                            if (!_.isUndefined(index)) { // remove range that contains page break row
                                modifiedBrowserSel.ranges.splice(index, 1);
                            }
                        }

                    } else if (selectionHasForbidenNodes($(browserSelection.active.start.node)) || selectionHasForbidenNodes($(browserSelection.active.end.node))) {

                        modifiedBrowserSel = _.clone(browserSelection);

                        $modifiedBrowserSelActStartNode = $(modifiedBrowserSel.active.start.node);
                        $modifiedBrowserSelActEndNode = $(modifiedBrowserSel.active.end.node);

                        if (selectionHasForbidenNodes($modifiedBrowserSelActStartNode)) {
                            modifyStartEndNodeForSelection($modifiedBrowserSelActStartNode, 'start');
                        }

                        if (selectionHasForbidenNodes($modifiedBrowserSelActEndNode)) {
                            modifyStartEndNodeForSelection($modifiedBrowserSelActEndNode, 'end');
                        }

                    } else if ($(event.target).hasClass('pagecontent') && (event.type === 'mousedown' || event.type === 'mouseup')) { // clicked on manual page break page
                        if (event.type === 'mouseup') {
                            if (!_.browser.Chrome) {
                                modifiedBrowserSel = null;
                            }
                        } else {
                            modifiedBrowserSel = _.clone(browserSelection);

                            if ($(browserSelection.active.start.node).hasClass('manual-page-break')) {
                                manualPbNode = $(browserSelection.active.start.node);
                            } else if ($(browserSelection.active.start.node).parentsUntil('.pagecontent', '.manual-page-break').length) {
                                manualPbNode = $(browserSelection.active.start.node).parentsUntil('.pagecontent', '.manual-page-break');
                            } else if (_.browser.Firefox && $(browserSelection.active.start.node).hasClass('page-break')) {
                                manualPbNode = $(browserSelection.active.start.node).prev();
                            }
                            if (manualPbNode.length) {
                                if (event.clientY < rootNode.children('.pagecontent').children().last()[0].getBoundingClientRect().top && manualPbNode.length) {
                                    if (_.browser.Firefox) {
                                        $node = manualPbNode; // firefox will get the right node, not the one after
                                    } else {
                                        $node = manualPbNode.prev().prev(); // skip over pagebreak
                                        if (DOM.isPageBreakNode($node)) { $node = $node.prev(); } // DOCS-3377
                                    }
                                } else {
                                    $node = rootNode.children('.pagecontent').children().last();
                                }

                                if (!$node.length) {
                                    // setting cursor to first document position, if no node is found
                                    $node = findDescendantNode(DOM.getPageContentNode(rootNode), DOM.PARAGRAPH_NODE_SELECTOR);
                                }

                                descNode = findDescendantNode($node, function () { return DOM.isPortionSpan(this); }, { reverse: true });

                                if (descNode) {
                                    modifiedBrowserSel.active.start.node = descNode;
                                    modifiedBrowserSel.active.end.node = modifiedBrowserSel.active.start.node;
                                    modifiedBrowserSel.active.start.offset = modifiedBrowserSel.active.start.node.firstChild.nodeValue.length;
                                    modifiedBrowserSel.active.end.offset = modifiedBrowserSel.active.start.offset;
                                } else {
                                    modifiedBrowserSel = null; // descNode can be null (54888)
                                }

                            } else {
                                modifiedBrowserSel = null;
                            }
                        }
                    } else if ($(event.target).hasClass('manual-page-break') && event.type === 'mouseup') { // clicked on ms word page break

                        $msHardbreak = $(event.target).find('.ms-hardbreak-page');
                        modifiedBrowserSel = _.clone(browserSelection);

                        if ($msHardbreak.length && $msHardbreak.parent().next().next()[0].getBoundingClientRect().top > event.clientY && event.clientY > $msHardbreak[0].getBoundingClientRect().top + $msHardbreak.height()) {
                            // ms word manual pb handling
                            modifiedBrowserSel.active.start.node = $msHardbreak.parent().prev()[0];
                            modifiedBrowserSel.active.end.node = modifiedBrowserSel.active.start.node;
                            modifiedBrowserSel.active.start.offset = _.last(Position.getOxoPosition(rootNode, modifiedBrowserSel.active.start.node, modifiedBrowserSel.active.start.node.firstChild.nodeValue.length));
                            modifiedBrowserSel.active.end.offset = modifiedBrowserSel.active.start.offset;
                        }
                    } else if ($(browserSelection.active.start.node).is(outOfBoundariesSelector) || $(browserSelection.active.end.node).is(outOfBoundariesSelector)) {

                        modifiedBrowserSel = _.clone(browserSelection);

                        if (hasKeyCode(event, 'UP_ARROW', 'HOME', 'PAGE_UP')) {
                            firstParagraph = findDescendantNode(DOM.getPageContentNode(rootNode), DOM.PARAGRAPH_NODE_SELECTOR);
                            if ($(browserSelection.active.start.node).is(outOfBoundariesSelector)) {
                                modifiedBrowserSel.active.start.node = firstParagraph;
                                modifiedBrowserSel.active.start.offset = 0;
                            }
                            if ($(browserSelection.active.end.node).is(outOfBoundariesSelector)) {
                                modifiedBrowserSel.active.end.node = firstParagraph;
                                modifiedBrowserSel.active.end.offset = 0;
                            }
                        } else if (hasKeyCode(event, 'DOWN_ARROW', 'END', 'PAGE_DOWN')) {
                            lastSpan = findDescendantNode(DOM.getPageContentNode(rootNode), function () { return DOM.isPortionSpan(this); }, { reverse: true });
                            if ($(browserSelection.active.start.node).is(outOfBoundariesSelector)) {
                                modifiedBrowserSel.active.start.node = lastSpan;
                                modifiedBrowserSel.active.start.offset = lastSpan.firstChild.nodeValue.length;
                            }
                            if ($(browserSelection.active.end.node).is(outOfBoundariesSelector)) {
                                modifiedBrowserSel.active.end.node = lastSpan;
                                modifiedBrowserSel.active.end.offset = lastSpan.firstChild.nodeValue.length;
                            }
                            if (_.browser.Chrome && hasKeyCode(event, 'DOWN_ARROW')) {
                                // in Chrome on last document position and arrow down, prevent document scrolls to begining
                                docApp.getView().scrollToChildNode(lastSpan);
                            }
                        } else if (event.type === 'mousedown' || event.type === 'mouseup') {
                            if (modifiedBrowserSel.active.start.node === modifiedBrowserSel.active.end.node) {
                                if ($(event.target).parents('.header, .header-wrapper').length > 0) {
                                    firstParagraph = findDescendantNode(DOM.getPageContentNode(rootNode), DOM.PARAGRAPH_NODE_SELECTOR);
                                    modifiedBrowserSel.active.start.node = modifiedBrowserSel.active.end.node = firstParagraph;
                                    modifiedBrowserSel.active.start.offset = modifiedBrowserSel.active.end.offset = 0;
                                } else if ($(event.target).parents('.footer, .footer-wrapper').length > 0) {
                                    lastSpan = findDescendantNode(DOM.getPageContentNode(rootNode), function () { return DOM.isPortionSpan(this); }, { reverse: true });
                                    modifiedBrowserSel.active.start.node = modifiedBrowserSel.active.end.node = lastSpan;
                                    modifiedBrowserSel.active.start.offset = modifiedBrowserSel.active.end.offset = lastSpan.firstChild.nodeValue.length;
                                }
                            } else if ($(browserSelection.active.end.node).is(outOfBoundariesSelector)) {
                                if ($(browserSelection.active.end.node).parents('.footer, .footer-wrapper').length > 0) {
                                    lastSpan = findDescendantNode(DOM.getPageContentNode(rootNode), function () { return DOM.isPortionSpan(this); }, { reverse: true });
                                    modifiedBrowserSel.active.end.node = lastSpan;
                                    modifiedBrowserSel.active.end.offset = lastSpan.firstChild.nodeValue.length;
                                }
                                if ($(event.target).hasClass('cover-overlay')) {
                                    if ($(event.target).parents('.header, .header-wrapper').length > 0) {
                                        firstParagraph = findDescendantNode(DOM.getPageContentNode(rootNode), DOM.PARAGRAPH_NODE_SELECTOR);
                                        modifiedBrowserSel.active.end.node = firstParagraph;
                                        modifiedBrowserSel.active.end.offset  = 0;
                                    } else if ($(event.target).parents('.footer, .footer-wrapper').length > 0) {
                                        lastSpan = findDescendantNode(DOM.getPageContentNode(rootNode), function () { return DOM.isPortionSpan(this); }, { reverse: true });
                                        modifiedBrowserSel.active.end.node = lastSpan;
                                        modifiedBrowserSel.active.end.offset = lastSpan.firstChild.nodeValue.length;
                                    }
                                }
                            } else if ($(browserSelection.active.start.node).is(outOfBoundariesSelector)) {
                                if ($(browserSelection.active.start.node).parents('.header, .header-wrapper').length > 0) {
                                    firstParagraph = findDescendantNode(DOM.getPageContentNode(rootNode), DOM.PARAGRAPH_NODE_SELECTOR);
                                    modifiedBrowserSel.active.start.node = firstParagraph;
                                    modifiedBrowserSel.active.start.offset  = 0;
                                }
                            }
                        }
                        if (_.browser.Firefox) {
                            docApp.getView().recalculateDocumentMargin();
                        }
                    }
                }

                // helper function that checks, if the vertical scroll position needs to be updated in special scenarios.
                // Returns true, if the process of setting the selection can be terminated, otherwise false.
                function checkVerticalPositions(event) {
                    var pos1 = self.getStartPosition();
                    var localRootNode = docApp.getModel().getCurrentRootNode();
                    var nodeForPos2 = browserSelection.active.isCollapsed() ? browserSelection.active.start.node : browserSelection.active.end.node;
                    var pos2 = Position.getOxoPosition(localRootNode, (nodeForPos2.nodeType === 3 ? nodeForPos2.parentNode : nodeForPos2));

                    if (_.browser.WebKit && docApp.isTextApp() && pos1.length < pos2.length && hasKeyCode(event, 'DOWN_ARROW') && equalPositions(pos1, pos2, pos1.length - 1)) { // preventing Chrome browser from jumping into textframe (DOCS-4436)
                        return handleJumpIntoTextFrame(pos1);
                    }

                    if (!Position.hasSameParentComponent(pos1, pos2, 2)) { // preventing browser from setting cursor in another text frame
                        return self.handleVerticalCursorKeyInTextFrame(event); // see #51398 for more
                    }

                    if (_.browser.WebKit && DOM.isPageNode(localRootNode) && !containsNode(DOM.getPageContentNode(localRootNode), browserSelection.active.start.node)) {
                        // never leave the current root node with cursor down in Chrome (40052)
                        self.restoreBrowserSelection();
                        docApp.getView().recalculateDocumentMargin();
                        return true; // leaving selection setting
                    }

                    return false; // not leaving selection setting
                }

                // helper function that checks, if the browser selection needs to be shifted from the final text
                // position before a range marker end node to the first position behind the range marker node
                // (or even behind the following comment node).
                // also checking if this is the first position inside a text range -> select position before the range
                function checkRangeMarkerEndNodes() {

                    // checking start position of browser selection
                    if (browserSelection.active.start.offset === 0) {
                        if (DOM.isFirstTextPositionBehindRangeStart(browserSelection.active.start.node, browserSelection.active.start.offset)) {
                            jumpOverRangeMarkerStartNode(browserSelection.active.start); // jumping to the left
                        }
                    } else {
                        if (DOM.isLastTextPositionBeforeRangeEnd(browserSelection.active.start.node, browserSelection.active.start.offset)) {
                            jumpOverRangeMarkerEndNode(browserSelection.active.start); // jumping to the right
                        }
                    }

                    // checking end position of browser selection
                    if (browserSelection.active.end.offset === 0) {
                        if (DOM.isFirstTextPositionBehindRangeStart(browserSelection.active.end.node, browserSelection.active.end.offset)) {
                            jumpOverRangeMarkerStartNode(browserSelection.active.end); // jumping to the left
                        }
                    } else {
                        if (DOM.isLastTextPositionBeforeRangeEnd(browserSelection.active.end.node, browserSelection.active.end.offset)) {
                            jumpOverRangeMarkerEndNode(browserSelection.active.end); // jumping to the right
                        }
                    }
                }

                // helper function to handle selection around special fields (page fields in header/footer)
                function checkForSpecialFieldNodes() {
                    var offset, tempNode;

                    tempNode = $(browserSelection.active.start.node).closest(DOM.SPECIAL_FIELD_NODE_SELECTOR);
                    if (tempNode.length) { // browser start node returns text or span node inside special complex field
                        browserSelection.active.start.offset = tempNode.index() - 1;
                        browserSelection.active.start.node = tempNode.parent()[0];
                    } else {
                        offset = browserSelection.active.start.offset;
                        tempNode = browserSelection.active.start.node.childNodes[offset];
                        if (DOM.isSpecialField(tempNode)) { // browser start node returns div node of complex field
                            browserSelection.active.start.offset = offset - 1;
                            tempNode = tempNode.previousSibling;
                        }
                    }
                    if (DOM.isRangeMarkerStartNode(tempNode) && DOM.isSpecialField(tempNode.nextSibling)) {
                        browserSelection.active.start.offset = offset - 1;
                    } else if (DOM.isRangeMarkerEndNode(tempNode) && DOM.isSpecialField(tempNode.previousSibling)) {
                        browserSelection.active.start.offset = offset + 1;
                    }

                    tempNode = $(browserSelection.active.end.node).closest(DOM.SPECIAL_FIELD_NODE_SELECTOR);
                    if (tempNode.length) { // browser END node returns text or span node inside special complex field
                        browserSelection.active.end.offset = tempNode.index() - 1;
                        browserSelection.active.end.node = tempNode.parent()[0];
                    } else {
                        offset = browserSelection.active.end.offset;
                        tempNode = browserSelection.active.end.node.childNodes[offset];
                        if (DOM.isSpecialField(tempNode)) { // browser END node returns div node of complex field
                            browserSelection.active.end.offset = offset - 1;
                            tempNode = tempNode.previousSibling;
                        }
                    }
                    if (DOM.isRangeMarkerStartNode(tempNode) && DOM.isSpecialField(tempNode.nextSibling)) {
                        browserSelection.active.end.offset = offset - 1;
                    } else if (DOM.isRangeMarkerEndNode(tempNode) && DOM.isSpecialField(tempNode.previousSibling)) {
                        browserSelection.active.end.offset = offset + 1;
                    }
                }

                // helper function that checks, in case browser selection fails on page up/down traversals,
                // how to correct position, if possible
                function checkPageUpPageDownTraversal() {
                    var startPos = self.getStartPosition(),
                        originalStartPos = originalSelection.start,
                        compareArrays = comparePositions(startPos, originalStartPos),
                        firefoxCondition = false,
                        pos,
                        pageLayout,
                        pageNum,
                        numPages,
                        beginingElement,
                        newPos;

                    if (_.browser.Firefox) {
                        switch (event.keyCode) {
                            case KeyCode.PAGE_DOWN:
                                firefoxCondition = (compareArrays < 0) || _.isEqual(startPos, self.getFirstDocumentPosition());
                                break;
                            case KeyCode.PAGE_UP:
                                firefoxCondition = (compareArrays > 0) || _.isEqual(startPos, self.getFirstDocumentPosition());
                                break;
                        }
                    }

                    if (hasKeyCode(event, 'PAGE_UP') && _.isEqual(originalStartPos, self.getFirstDocumentPosition())) {
                        // do nothing
                    } else if (hasKeyCode(event, 'PAGE_DOWN') && _.isEqual(originalStartPos, self.getLastDocumentPosition())) {
                        // do nothing
                    } else if (firefoxCondition || _.isEqual(startPos, originalStartPos)) {
                        pos = originalSelection.start;
                        pageLayout = docModel.getPageLayout();
                        pageNum = pageLayout.getPageNumber(pos);
                        numPages = pageLayout.getNumberOfDocumentPages();
                        if (hasKeyCode(event, 'PAGE_DOWN')) {
                            if (pageNum === numPages) {
                                self.setTextSelection(self.getLastDocumentPosition());
                            } else {
                                beginingElement = pageLayout.getBeginingElementOnSpecificPage(pageNum + 1);
                                if (beginingElement) {
                                    beginingElement = beginingElement.parent(); // page brea
                                    if (beginingElement.next().length) {
                                        beginingElement = beginingElement.next(); // first next element
                                    } else if (beginingElement.parent().is('td')) {
                                        beginingElement = beginingElement.closest('tr').next().find('span').first();
                                    }
                                    if (beginingElement.hasClass('p') || beginingElement.is('table')) {
                                        beginingElement = beginingElement.find('span').first();
                                    }
                                    newPos = Position.getOxoPosition(rootNode, beginingElement, 0);
                                    self.setTextSelection(newPos);
                                }
                            }
                        } else if (hasKeyCode(event, 'PAGE_UP')) {
                            if (!pageNum || pageNum < 3) {
                                self.setTextSelection(self.getFirstDocumentPosition());
                            } else {
                                beginingElement = pageLayout.getBeginingElementOnSpecificPage(pageNum - 1);
                                if (beginingElement) {
                                    beginingElement = beginingElement.parent(); // page break
                                    if (beginingElement.next().length) {
                                        beginingElement = beginingElement.next(); // first next element
                                    } else if (beginingElement.parent().is('td')) {
                                        beginingElement = beginingElement.closest('tr').next().find('span').first();
                                    }
                                    if (beginingElement.hasClass('p') || beginingElement.is('table')) {
                                        beginingElement = beginingElement.find('span').first();
                                    }
                                    newPos = Position.getOxoPosition(rootNode, beginingElement, 0);
                                    self.setTextSelection(newPos);
                                }
                            }
                        }
                    }
                }

                // start of delayed anonymous function that handles the browser selection

                // special browser selection handling for testrunner tests
                if (DEBUG && event.forceTestSelection) { browserSelection = getTestBrowserSelection(event); }

                if (browserSelection && browserSelection.active) {

                    // do not modify selection on touch device when starting to select with mouse or touch
                    // -> on touch devices only the 'mouseup' and 'touchend' are evaluated
                    if (!(TOUCH_DEVICE && (event.type === 'mousedown' || event.type === 'touchstart'))) {

                        // checking for range end marker nodes -> setting position behind those range end markers
                        if (leftRightCursor || event.type === 'mousedown' || event.type === 'mouseup') { //#38753, #41957
                            checkForSpecialFieldNodes();
                            checkRangeMarkerEndNodes();
                        }

                        // modifying the browser selection if necessary
                        modifyBrowserSelection();

                        // specific checks for vertical cursor movements
                        if (isVerticalCursor && checkVerticalPositions(event)) { return; }

                        // calculates the new logical selection
                        applyBrowserSelection(modifiedBrowserSel ? modifiedBrowserSel : browserSelection, { event, restoreSelection, processingBrowserEvent: true });

                        // fixing the position after Ctrl-CursorRight, so that cursor stays at end of document (37298)
                        if (hasKeyCode(event, 'RIGHT_ARROW') && event.ctrlKey && self.isTextCursor() && _.isEqual(self.getStartPosition(), self.getFirstDocumentPosition())) {
                            self.setTextSelection(self.getLastDocumentPosition());
                        }

                        // if last line in document and there is changetracking, browser tries to set cursor to ct left sidebar, prevent that and set cursor back to original position
                        if (hasKeyCode(event, 'DOWN_ARROW') && self.isTextCursor() && $(browserSelection.active.start.node).is('.ctdiv')) {
                            self.setTextSelection(originalSelection.start);
                            def.resolve();
                            return;
                        }

                        if (hasKeyCode(event, 'PAGE_DOWN', 'PAGE_UP')) {
                            checkPageUpPageDownTraversal(); // fix for #39003
                        }

                        // Modifying selection, if Ctrl-Shift-Right includes start of following paragraph (40151)
                        if (event.ctrlKey && event.shiftKey && hasKeyCode(event, 'RIGHT_ARROW') && _.last(endPosition) === 0) {
                            newLastPosition = Position.getLastPositionOfPreviousParagraph(rootNode, _.initial(endPosition));
                            if (newLastPosition) { self.setTextSelection(startPosition, newLastPosition); }
                        }

                        // Only for IE: Expanding selection after a double click or triple click (Fix for 29751)
                        if ((self.getSelectionType() === 'text') && (self.isTextCursor())) {
                            if (event.type === 'dblclick') {
                                self.setTextSelection(...Position.getWordBoundaries(rootNode, self.getStartPosition(), { addFinalSpaces: true }));
                            } else if (event.type === 'tripleclick') {
                                self.setTextSelection(Position.appendNewIndex(_.initial(self.getStartPosition()), 0), Position.appendNewIndex(_.initial(self.getStartPosition()), Position.getParagraphLength(rootNode, self.getStartPosition())));
                            }
                        }

                        // Fixing the position after pressing the 'End' key for MS IE and Webkit (26863) and Firefox (28309).
                        // The selection has to be reduced by 1 (if it is not the end of the paragraph) -> task 26863 and 28309
                        if (hasKeyCode(event, 'END') && !event.shiftKey) {
                            start = self.getStartPosition();
                            nodeInfo = Position.getDOMPosition(rootNode, start);

                            if ($(nodeInfo.node).text().charCodeAt(nodeInfo.offset - 1) === 32) {
                                if (start[start.length - 1] < Position.getParagraphLength(rootNode, start)) {
                                    start[start.length - 1] -= 1;
                                    self.setTextSelection(start);
                                }
                            } else if ((_.browser.Firefox) && (nodeInfo.offset === 0) && (nodeInfo.node.parentNode && nodeInfo.node.parentNode.previousSibling && $(nodeInfo.node.parentNode.previousSibling).is(DOM.HARDBREAK_NODE_SELECTOR))) {
                                // Fix for 28544
                                start[start.length - 1] -= 1;
                                self.setTextSelection(start);
                            }
                        }

                        // Fixing in MS IE the handling of shift + 'end', that selects until the end of the document, if pressed
                        // in the last line of a paragraph (Fix for 28218).
                        if (hasKeyCode(event, 'END') && event.shiftKey && _.browser.IE) {
                            if (!Position.hasSameParentComponent(startPosition, endPosition)) {
                                self.setTextSelection(startPosition, Position.getLastPositionInParagraph(rootNode, _.clone(startPosition)));
                            }
                        }

                        // adjust selection, if key event did not have any effect
                        if (keyDownEvent && _.isEqual(originalSelection, { start: startPosition, end: endPosition })) {

                            // we need different keys on OSX - CTRL+Left/Right cursor or on OSX ALT+Left/Right cursor: execute simple left/right step
                            if ((_.browser.MacOS) ? (leftRightCursor && !event.ctrlKey && event.altKey && !event.metaKey) : (leftRightCursor && event.ctrlKey && !event.altKey && !event.metaKey)) {
                                // move cursor to left and right by one step
                                if (moveTextCursor({ extend: event.shiftKey, backwards: hasKeyCode(event, 'LEFT_ARROW') })) {

                                    // Fix for Bug 41333: Extending the selection by a whole word on chrome/safari doesn't work in some situations. So after the cursor was moved with alt/ctrl+shift+left/right, extend the selection to the next word.
                                    if (_.browser.Chrome || _.browser.Safari) {
                                        // when the pressed key was arrow left, extend the selection to the left, to the start from the next word
                                        if (hasKeyCode(event, 'LEFT_ARROW')) {
                                            // set the selection from the position where the selection of the text range has been started, to the first character from the word at the focus position
                                            self.setTextSelection(self.getAnchorPosition(), Position.getWordBoundaries(rootNode, self.getFocusPosition())[0]);

                                        // otherwise extend the selection to the right, to the end from next word
                                        } else {
                                            // set the selection from the start position of the selection to the last character from the word were the selection ends
                                            self.setTextSelection(self.getStartPosition(), Position.getWordBoundaries(rootNode, self.getEndPosition())[1]);
                                        }
                                    }

                                    def.resolve();
                                } else {
                                    def.reject();
                                }
                                return;
                            }

                            // Setting cell selections in Mozilla, if the selection did not change the content is separated into different cells
                            if (_.browser.Firefox && upDownCursor && event.shiftKey && Position.isPositionInTable(rootNode, startPosition) && !cellRangeSelected) {
                                // simple Up/Down cursor: execute simple left/right step
                                if (moveTextCursor({ extend: event.shiftKey, backwards: hasKeyCode(event, 'UP_ARROW'), verticalCellSelection: true })) {
                                    def.resolve();
                                } else {
                                    def.reject();
                                }
                                return;
                            } else if (upDownCursor && !event.ctrlKey && !event.altKey && !event.metaKey) {
                                // simple Up/Down cursor: execute simple left/right step
                                if (moveTextCursor({ extend: event.shiftKey, backwards: hasKeyCode(event, 'UP_ARROW') })) {
                                    def.resolve();
                                } else {
                                    def.reject();
                                }
                                return;
                            }
                            // see #42989
                            if (_.browser.Chrome && !event.ctrlKey && !event.metaKey) {
                                if (hasKeyCode(event, 'END')) {
                                    self.setTextSelection(Position.getLastPositionInParagraph(rootNode, _.clone(startPosition)));
                                }
                                if (hasKeyCode(event, 'HOME')) {
                                    self.setTextSelection(Position.getFirstPositionInParagraph(rootNode, _.clone(startPosition)));
                                }
                            }
                        } else {
                            // setting cell selections in Mozilla, if the content is separated into different cells
                            // Typically this is most often handled with 'verticalCellSelection' in the 'if', because
                            // the selection did not change. But sometimes it changes, and this can be recognized by
                            // the following call of moveTextCursor.
                            if (_.browser.Firefox && upDownCursor && Position.isPositionInTable(rootNode, startPosition) && !cellRangeSelected) {
                                if (event.shiftKey) {
                                    // are start and end position in the same table cell?
                                    if (!_.isEqual(Position.getLastPositionFromPositionByNodeName(rootNode, startPosition, DOM.TABLE_CELLNODE_SELECTOR), Position.getLastPositionFromPositionByNodeName(rootNode, endPosition, DOM.TABLE_CELLNODE_SELECTOR))) {
                                        if (moveTextCursor({ extend: event.shiftKey, backwards: hasKeyCode(event, 'UP_ARROW'), verticalCellSelection: true })) {
                                            def.resolve();
                                        } else {
                                            def.reject();
                                        }
                                        return;
                                    }
                                } else if (DOM.isCellNode(browserSelection.active.start.node)) {
                                    // cursor traversing in FF is broken inside tables (DOCS-4811)
                                    let upInFirstRow = false;
                                    let newPosition = Position.getOxoPosition(rootNode, browserSelection.active.start.node);
                                    if (hasKeyCode(event, 'UP_ARROW') && newPosition.length > 2) {
                                        if (newPosition[newPosition.length - 2] > 0) {
                                            newPosition[newPosition.length - 2] -= 1; // using the cell above in the previous row
                                        } else {
                                            upInFirstRow = true;
                                            newPosition = newPosition.slice(0, -2); // using the previous top level element in front of the table
                                            newPosition[newPosition.length - 1] -= 1;
                                        }
                                    }
                                    const newTextPos = upInFirstRow ? Position.getLastPositionInParagraph(rootNode, newPosition) : (hasKeyCode(event, 'DOWN_ARROW') ? Position.getFirstPositionInCurrentCell(rootNode, newPosition) : Position.getLastPositionInCurrentCell(rootNode, newPosition));
                                    self.setTextSelection(newTextPos);
                                    def.resolve();
                                    return;
                                }
                            }

                            // setting selection in IE using shift and cursor-Right and jumping from one paragraph to the following paragraph
                            if (_.browser.IE && event.shiftKey) {
                                if (hasKeyCode(event, 'RIGHT_ARROW') && Position.isLastPositionInParagraph(rootNode, endPosition)) {
                                    if (moveTextCursor({ extend: true, backwards: false, verticalCellSelection: false })) {
                                        def.resolve();
                                    } else {
                                        def.reject();
                                    }
                                    return;
                                } else if (hasKeyCode(event, 'LEFT_ARROW') && startPosition[startPosition.length - 1] === 0) {
                                    if (moveTextCursor({ extend: true, backwards: true, verticalCellSelection: false })) {
                                        def.resolve();
                                    } else {
                                        def.reject();
                                    }
                                    return;
                                }
                            }
                        }

                        // Fix for 33246, setting cursor behind inline hardbreak in Firefox
                        if (_.browser.Firefox && ((event.type === 'mouseup') || (event.type === 'mousedown')) && event.target && DOM.isParagraphNode(event.target) && !self.hasRange()) {
                            // the current node must follow a div.inline.hardbreak and the selection must have offset of 0
                            start = self.getStartPosition();
                            if (Position.isPositionNeighboringSpecifiedNode(event.target, [_.last(start)], DOM.HARDBREAK_NODE_SELECTOR, { follow: true })) {
                                // -> reduce the current position by '1', so that the cursor is positioned before the hard break
                                start[start.length - 1] -= 1;
                                self.setTextSelection(start);
                            }
                        }

                        // handling unusual browser selection in Edge browser, so that a single mouse click removes a selection range (DOCS-4508)
                        if (_.browser.Edge && event.type === 'mouseup' && self.hasRange() && !isTripleClickActive && _.isEqual(originalSelection, { start: startPosition, end: endPosition })) {
                            if (docModel.isActiveDoubleClickEvent()) {
                                isTripleClickActive = true;
                                self.setTimeout(() => { isTripleClickActive = false; }, 400);
                            } else {
                                self.setTextSelection(self.getEndPosition()); // only when this was a single click, no double and no triple click
                            }
                        }

                        // Skipping not expanded implicit paragraphs in read-only mode during up or down cursor (left or right cursor was already handled before)
                        if (simpleKeyDownEvent && upDownCursor && readOnly && skipImplicitShrinkedParagraph()) {
                            def.resolve();
                            return;
                        }

                        // Enabling change of cursor position (67922, DOCS-2289)
                        if (_.browser.Safari && TOUCH_DEVICE && docApp.isTextApp() && (event.type === 'touchend' || event.type === 'mouseup')) { // } && !self.isDrawingSelection()) {
                            if (getBooleanOption(options, 'fromMouseUpHandler', false)) {
                                delete options.fromMouseUpHandler; // evaluating the selection once more, but only once!
                                options.internalRestart = true; // setting a marker for the second run
                                self.processBrowserEvent(event, options);
                            }
                            // this call of 'restoreBrowserSelection' after the second run dramatically improves the selection behavior as described in task 67922.
                            // Without this call it is very difficult to set a new selection with 'touchend' after a previous 'touchend' (or 'mouseup').
                            if (getBooleanOption(options, 'internalRestart', false)) {
                                this.restoreBrowserSelection();
                            }
                        }
                    }

                    // special IE behavior
                    if (_.browser.IE) {
                        if ((event.type === 'mousedown') && (selectedDrawing.length > 0)) {
                            // deselect drawing node in IE (mousedown event has been ignored, see above)
                            self.clearDrawingSelection(selectedDrawing);
                            selectedDrawing = $();
                            self.updateClipboardNode();
                        } else if ((event.type === 'mousedown') || (event.type === 'mouseup')) {
                            // handling clicks behind last paragraph in table cell node in IE 11 (27287, 29409)
                            currentCell = $(event.target).closest(DOM.TABLE_CELLNODE_SELECTOR);
                            // if parent is row containing page break, don't do anything
                            if (!DOM.isTablePageBreakRowNode(currentCell.parent())) {
                                currentCellContentNode = $(event.target).closest(DOM.CELLCONTENT_NODE_SELECTOR);
                                if ((currentCell.length > 0) && (currentCellContentNode.length === 0)  && !DOM.isExceededSizeTableNode(currentCell.closest(DOM.TABLE_NODE_SELECTOR))) {
                                    // the mouse event happened behind the last paragraph in the cell. Therefore the new position has to be the last position inside the cell
                                    self.setTextSelection(Position.getLastPositionInCurrentCell(rootNode, Position.getOxoPosition(rootNode, currentCell[0], 0)));
                                }
                            }
                            // Handling of selections over several cells in tables in IE (evaluation in mouseup event)
                            if ((event.type === 'mouseup') && (Position.isPositionInTable(rootNode, self.getStartPosition())) && DOM.isCellContentNode(event.target)) {

                                // using event.offsetX and event.offsetY to calculate, if the mouseup event happened in the same table cell
                                // -> if not, a simplified process for selections outside the cell is required
                                if ((event.offsetX < 0) || (event.offsetY < 0) || (event.offsetX > $(event.target.parentNode).width()) || (event.offsetY > $(event.target.parentNode).height())) {

                                    tempPosition = Position.getOxoPositionFromPixelPosition(rootNode, event.pageX, event.pageY);

                                    if (tempPosition && tempPosition.start && (tempPosition.start.length > 0)) {
                                        self.setTextSelection(backwards ? tempPosition.start : self.getStartPosition(), backwards ? self.getEndPosition() : tempPosition.start);
                                    }
                                }
                            }
                        }
                    }

                    def.resolve();

                } else if (selectedDrawing.length > 0) { // if (browserSelection && browserSelection.active) {

                    // IE destroys text selection in clipboard node immediately after
                    // the mouseup event (this cannot be prevented by returning false)
                    if (_.browser.IE && (event.type === 'mouseup') && (selectedDrawing.length > 0)) {
                        self.restoreBrowserSelection(event);
                    } else if (CHROME_ON_ANDROID) {
                        //Fix for Bug 34409
                        self.clearDrawingSelection(selectedDrawing);
                        selectedDrawing = $();
                        self.updateClipboardNode();
                    }

                    def.resolve();
                } else {
                    if (keyDownEvent && _.browser.Chrome && _.isEqual(originalSelection, { start: startPosition, end: endPosition })) {
                        // see #42989
                        if (hasKeyCode(event, 'END')) {
                            self.setTextSelection(Position.getLastPositionInParagraph(rootNode, _.clone(startPosition)));
                            def.resolve();
                            return;
                        }
                        if (hasKeyCode(event, 'HOME')) {
                            self.setTextSelection(Position.getFirstPositionInParagraph(rootNode, _.clone(startPosition)));
                            def.resolve();
                            return;
                        }
                    } else if (slideMode && event.type === 'mouseup' && startPosition && (startPosition.length > 0) && self.isAdditionalTextframeSelection()) {
                        // missing browser selection in mouse up happens also, if a template place holder icon is replaced by a table -> the selection must stay in the table
                        if (DrawingFrame.isTableDrawingFrame(selectedTextFrameDrawing)) {
                            self.setTextSelection(startPosition); // refreshing cursor, so that it blinks in first cell
                        }
                        def.resolve();
                        return;
                    }

                    if (isVerticalCursor) {
                        self.restoreBrowserSelection();  // avoid uncontrolled vertical browser scrolling (40052)
                        if (_.browser.Firefox || _.browser.Chrome) { docApp.getView().recalculateDocumentMargin(); }
                    }

                    // Handling of 'slide' selection
                    if (slideMode && self.isTopLevelTextCursor()) {
                        def.resolve();
                        return;
                    }

                    // at least it is necessary to restore the browser selection in FF and IE to avoid wrong visualization of selection
                    if ((_.browser.Firefox || _.browser.IE) && self.hasRange()) { self.restoreBrowserSelection(); }

                    globalLogger.warn('Selection.processBrowserEvent(): missing valid browser selection. Event type: ' + event.type);
                    def.reject();
                }

            });

            return def.promise();
        };

        /**
         * Applies the passed cell selection.
         *
         * @param {DOM.Range[]} ranges
         *  The DOM ranges of the cell selection.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.setCellSelection = function (ranges) {
            if (ranges.length > 0) {
                // option 'active' required: point to any cell to be able (used to detect cell selection)
                applyBrowserSelection({ active: ranges[0], ranges });
            }
            return this;
        };

        /**
         * Removes the selection node from the specified drawing frame.
         *
         * @param {HTMLElement|jQuery} drawingFrame
         *  The root node of the drawing frame. If this object is a jQuery
         *  collection, uses the first DOM node it contains.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.keepDraggingActive=false]
         *      Whether an active dragging must be kept, when the selection is removed.
         */
        this.clearDrawingSelection = function (drawingFrame, options) {
            DrawingFrame.clearSelection(drawingFrame, options);
            if (this.isCropMode()) { docModel.exitCropMode(); }
        };

        /**
         * Drawing the border around a selected drawing and registering the handler.
         * This is the only function, that is allowed to draw the selection around
         * the drawing.
         *
         * @param {HTMLElement|jQuery} drawing
         *  The drawing node to be selected, as DOM node or jQuery object.
         */
        this.drawDrawingSelection = function (drawing) {
            if (drawingResizeHandler && DrawingFrame.isDrawingFrame(drawing) && !DrawingFrame.isNonSelectableDrawingNode(drawing)) { drawingResizeHandler(docApp, drawing); }
        };

        /**
         * Updating the border around a selected drawing. This function is a shortcut
         * for 'drawDrawingSelection'. It does not redraw the complete selection, but
         * only updates the position and size of an existing drawing selection in the
         * selection overlay node.
         *
         * @param {HTMLElement|jQuery} drawing
         *  The selected drawing node, as DOM node or jQuery object.
         */
        this.updateOverlaySelection = function (drawing) {
            if (drawingResizeHandler && DrawingFrame.isDrawingFrame(drawing) && !DrawingFrame.isNonSelectableDrawingNode(drawing)) { updateOverlaySelectionHandler(docApp, drawing); }
        };

        /**
         * Selects the passed logical text range in the document.
         *
         * @param {Number[]} newStartPosition
         *  The logical position of the first text component in the selection.
         *  Must be the position of a paragraph child node, either a text span,
         *  a text component (fields, tabs), or a drawing node.
         *
         * @param {Number[]} [newEndPosition]
         *  The logical position behind the last text component in the
         *  selection (half-open range). Must be the position of a paragraph
         *  child node, either a text span, a text component (fields, tabs), or
         *  a drawing node. If omitted, sets a text cursor according to the
         *  passed start position.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.backwards=false]
         *      If set to true, the selection will be created backwards. Note
         *      that not all browsers allow to create a reverse DOM browser
         *      selection. Nonetheless, the internal selection direction will
         *      always be set to backwards.
         *  @param {Boolean} [options.simpleTextSelection=false]
         *      If set to true, the selection will be set using newStartPosition
         *      and newEndPosition directly. The logical positions are not converted
         *      to a browserSelection and then calculated back in applyBrowserSelection
         *      using Position.getTextLevelOxoPosition. This is especially important
         *      for the very fast operations like 'insertText' (also used for 'Enter',
         *      'Delete' and 'Backspace').
         *  @param {Boolean} [options.insertOperation=false]
         *      If set to true, this setTextSelection is called from an insert operation,
         *      for example an insertText operation. This is important for performance
         *      reasons, because the cursor setting can be simplified.
         *  @param {Boolean} [options.splitOperation=false]
         *      If set to true, this setTextSelection is called from an 'Enter' operation.
         *      This is important for performance reasons, because the cursor setting can
         *      be simplified.
         *  @param {Boolean} [options.preserveFocus=true]
         *      Whether the focus should be preserved when setting a text selection.
         *  @param {Boolean} [options.keepDraggingActive=false]
         *      After the user clicked the mouse down button on a drawing border, an active
         *      dragging must not be stopped by this selection toggle.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.setTextSelection = function (newStartPosition, newEndPosition, options) {

            var // DOM points for start and end position
                startPoint = null, endPoint = null,
                // whether to create the selection backwards
                selectBackwards = false,
                // the range containing start and end point
                range = null,
                // Performance: Whether the simplified text selection can be used
                simpleTextSelection = getBooleanOption(options, 'simpleTextSelection', false),
                // whether the selection shall be updated after an external operation (then a 'change' must be triggered, task 31832)
                remoteSelectionUpdate = getBooleanOption(options, 'remoteSelectionUpdate', false),
                // whether this call was triggered by the userDataUpdateHandler
                userDataUpdateHandler = getBooleanOption(options, 'userDataUpdateHandler', false),
                // whether the change track pop up triggered this change of selection
                keepChangeTrackPopup = getBooleanOption(options, 'keepChangeTrackPopup', false),
                // the browser event passed to this method
                browserEvent = getOption(options, 'event'),
                // whether a drawing inside a drawing group is selected
                drawingInGroupSelection = false,
                // whether a drawing inside the drawing layer is selected
                drawingLayerSelection = false,
                // whether an existing cell selection must not be destroyed
                keepCellSelection = false,
                // whether this selection change must keep an active dragging
                keepDraggingActive = getBooleanOption(options, 'keepDraggingActive', false),
                // whether an focus should be preserved
                preserveFocus = getBooleanOption(options, 'preserveFocus', true),
                // whether the focus must not be modified
                keepFocus =  getBooleanOption(options, 'keepFocus', false);

            if (simpleTextSelection) {
                applyBrowserSelection(null, { preserveFocus, keepFocus, insertOperation: getBooleanOption(options, 'insertOperation', false), splitOperation: getBooleanOption(options, 'splitOperation', false), slideSelection: getBooleanOption(options, 'slideSelection', false), isIOSROHandling: getBooleanOption(options, 'isIOSROHandling', false), simpleTextSelection, keepChangeTrackPopup }, { start: newStartPosition, end: newEndPosition });
            } else {
                startPoint = _.isArray(newStartPosition) ? getPointForTextPosition(newStartPosition) : null;
                endPoint = _.isArray(newEndPosition) ? getPointForTextPosition(newEndPosition) : startPoint;

                if (startPoint && !endPoint && DrawingFrame.isDrawingFrame(startPoint.node) && DrawingFrame.isGroupContentNode(startPoint.node.parentNode)) {
                    endPoint = new DOM.Point(startPoint.node, 1);
                    drawingInGroupSelection = true;
                } else if (startPoint && startPoint.node && startPoint.node.parentNode && DOM.isDrawingLayerNode(startPoint.node.parentNode) && DOM.isFirstTextPositionBehindNode(endPoint, DOM.getDrawingPlaceHolderNode(startPoint.node))) {
                    endPoint = new DOM.Point(startPoint.node, 1);
                    drawingLayerSelection = true;
                }

                if (startPoint && endPoint) {
                    keepCellSelection = userDataUpdateHandler && self.getSelectionType() === 'cell' && _.isEqual(newStartPosition, this.getStartPosition()) && _.isEqual(newEndPosition, this.getEndPosition());
                    selectBackwards = getBooleanOption(options, 'backwards', false);
                    range = new DOM.Range(selectBackwards ? endPoint : startPoint, selectBackwards ? startPoint : endPoint);
                    if (self.isAdditionalTextframeSelection()) { handleFixedHeightTextFrameScrolling(self.getAnyDrawingSelection()); } // #51398
                    applyBrowserSelection({ active: range, ranges: [range] }, { preserveFocus, keepFocus, forceTrigger: remoteSelectionUpdate, keepCellSelection, keepChangeTrackPopup, drawingInGroupSelection, drawingLayerSelection, isIOSROHandling: getBooleanOption(options, 'isIOSROHandling', false), keepDraggingActive, event: browserEvent });
                } else {
                    var browserStartPoint = getArrayOption(options, 'browserStartPoint');
                    if (remoteSelectionUpdate && browserStartPoint) {
                        // using the selection given by the browser, if restoring the old text selection failed
                        this.setTextSelection(browserStartPoint);
                    } else {
                        globalLogger.warn('Selection.setTextSelection(): expecting text positions, start=' + JSON.stringify(newStartPosition) + ', end=' + JSON.stringify(newEndPosition));
                    }
                }
            }

            return this;
        };

        /**
         * Updating the selection of a remote read-only editor after executing an external operation. The new
         * selection created by the browser is used to calculate and set a new logical start and end position.
         * This cannot be used, if the selection before executing the external operation was a drawing selection
         * or a cell selection.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.preserveFocus=true]
         *      If set to true, the DOM element currently focused will be
         *      focused again after the browser selection has been set. Note
         *      that doing so will immediately lose the new browser selection,
         *      if focus is currently inside a text input element.
         *  @param {Boolean} [options.forceTrigger=true]
         *
         */
        this.updateSelectionAfterBrowserSelection = function (options) {

            var // the current browser selection
                browserSelection = self.getBrowserSelection(),
                preserveFocus = getOption(options, 'preserveFocus', true),
                forceTrigger = getOption(options, 'forceTrigger', true);

            // checking the current browser selection before using it (fix for 32182)
            if (browserSelection && browserSelection.active && browserSelection.active.start && browserSelection.active.end) {
                applyBrowserSelection(browserSelection, { preserveFocus, forceTrigger });
            } else {
                globalLogger.warn('Selection.updateSelectionAfterBrowserSelection(): incomplete browser selection, ignoring remote selection.');
            }
        };

        /**
         * Returns the first logical position in the document.
         *
         * @returns {Number[]}
         *  The first logical document position.
         */
        this.getFirstDocumentPosition = function () {
            return getFirstPosition();
        };

        /**
         * Returns the last logical position in the document.
         *
         * @returns {Number[]}
         *  The last logical document position.
         */
        this.getLastDocumentPosition = function () {
            return getLastPosition();
        };

        /**
         * Returns the logical start and end position in the document as an
         * object with the properties 'start' and 'end'.
         *
         * @returns {Object}
         *  The first and last logical document positions in an object with
         *  the properties 'start' and 'end'.
         */
        this.getDocumentRange = function () {
            return { start: getFirstPosition(), end: getLastPosition() };
        };

        /**
         * Setting the selection after the document was loaded successfully.
         * It is possible, that the selection was already modified during
         * loading the document. Additionally it is possible that the
         * document was scrolled, but the cursor was not set. In the latter
         * case, the scroll position needs to be restored.
         */
        this.selectDocumentLoadPosition = function () {
            // setting selection to top position, but keeping a selection,
            // that might have been set before during fastloading
            if (self.isUndefinedSelection()) {
                return self.setTextSelection(getFirstTextPosition({ skipnodes: true }));
            }
        };

        /**
         * Sets the text cursor to the first available cursor position in the
         * document. Skips leading floating drawings in the first paragraph. If
         * the first content node is a table, selects its first available
         * cell paragraph (may be located in a sub table in the first outer
         * cell).
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.selectTopPosition = function () {

            var // the first text position in the document (can be empty in documents without slides)
                firstTextPosition = getFirstTextPosition({ skipnodes: true });

            return firstTextPosition ? this.setTextSelection(firstTextPosition) : this;
        };

        /**
         * Check, whether the start position of the current selection is
         * the top position in the document.
         *
         * @returns {Boolean}
         *  Whether the start position of the current selection is the top
         *  position in the document.
         */
        this.isTopPositionSelected = function () {
            return _.isEqual(self.getStartPosition(), getFirstTextPosition());
        };

        /**
         * Check, whether the hole document is selected
         *
         * @returns {Boolean}
         *  Whether the hole document is selected
         */
        this.isAllSelected = function () {
            var selectionStartPos   = this.getStartPosition(),
                selectionEndPos     = this.getEndPosition(),
                startPos            = getFirstPosition(),
                endPos              = getLastPosition();

            return (_.isEqual(selectionStartPos, startPos) && _.isEqual(selectionEndPos, endPos));
        };

        /**
         * Check, whether the selection is undefined. This is the case,
         * if the start position is an empty array. This is only valid
         * during loading the document (fast load).
         *
         * @returns {Boolean}
         *  Whether the current selection is undefined.
         */
        this.isUndefinedSelection = function () {
            return (_.isArray(self.getStartPosition()) && _.isEmpty(self.getStartPosition()));
        };

        /**
         * Check, whether the current selection is a cursor selection inside an implicit paragraph.
         *
         * @returns {Boolean}
         *  Whether the current selection is a cursor selection inside an implicit paragraph.
         */
        this.isCursorInImplicitParagraph = function () {
            return self.isTextCursor() && _.last(self.getStartPosition()) === 0 && DOM.isImplicitParagraphNode(Position.getParagraphElement(rootNode, _.initial(self.getStartPosition())));
        };

        /**
         * Setting valid values for startPosition and endPosition, if a multiple drawing
         * selection is active. This can be the logical position of the first drawing
         * inside the multiple drawing selection. Using this function it is guaranteed, that
         * tests like 'isCursor' deliver the correct result for multiple drawing selections.
         *
         * @param {Object[]} multiSelection
         *  The container for the multiple drawing selection.
         */
        this.setValidMultiSelectionValues = function (multiSelection) {
            if (_.isObject(multiSelection[0])) {
                startPosition = multiSelection[0].startPosition;
                endPosition = multiSelection[0].endPosition;
            }
        };

        /**
         * On an iPhone, the "Shift" is activated by the browser after pressing "Enter"
         * or behind "! ", "? " and ". ". In some cases it is necessary to ignore this
         * "shift", that was not explicitely triggered by the user.
         *
         * @returns {Boolean}
         *  Whether a "shift" in the event can be ignored, because it was probably created
         *  by the Safari browser and not by the user.
         */
        this.isSpecialIgnoreShiftPositionOnIPhone = function () {

            if (!IOS_SAFARI_DEVICE) { return false; } // "shift" to be ignored exists only on iPhone (Safari)
            // ignore "shift" in empty paragraphs or when the current selection is at the beginning of the paragraph.
            if (self.hasRange() || ary.last(startPosition) === 0 || Position.getParagraphLength(rootNode, startPosition) === 0) { return true; }
            // ignore "shift" behind "! ", "? " and ". ".
            const startInfo = Position.getDOMPosition(rootNode, startPosition, true);
            if (!startInfo || !startInfo.node) { return false; }
            if (DOM.isTextSpan(startInfo.node) && _.isNumber(startInfo.offset) && startInfo.offset > 1) {
                const text = $(startInfo.node).text();
                if (text.length > 1) {
                    const checkString = text.substring(startInfo.offset - 2, startInfo.offset);
                    if (/^[.!?]\s+$/.test(checkString)) { return true; }
                }
            }

            return false; // in all other cases, the "shift" cannot be ignored.
        };

        /**
         * Selects the entire document or the entire content of a text frame. This is dependent from
         * the current start position.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.selectAll = function () {

            if (DOM.isCommentTextframeNode(getFocus())) { return self; } // do nothing, if the focus is inside the comment editor (DOCS-3345)

            var // whether all drawings can be selected
                // -> is requires support of multiple selection and that the current selection is a slide selection or
                //    a drawing selection or a multiple drawing selection
                selectAllDrawings = slideMode && self.isMultiSelectionSupported() && (self.isTopLevelTextCursor() || self.isDrawingSelection() || self.isMultiSelection()),
                // whether the selection is inside a text frame
                isInsideTextFrame = false,
                // whether the selection did not change (for example ctrl-A inside an empty text frame)
                doNothing = false,
                // the text frame
                textFrame = null,
                // the start and end position of the selection (allowing positions in empty text spans before drawings, comments, ...(38378))
                start = null, end = null,
                // the number of selected drawings
                drawingCount = 0;

            if (selectAllDrawings) {

                // removing existing drawing selections (single or multiple)
                if (self.isMultiSelection()) {
                    self.clearMultiSelection();
                } else if (self.isDrawingSelection()) {
                    self.clearDrawingSelection(selectedDrawing);
                    if (!slideMode) { docApp.getView().clearVisibleDrawingAnchor(); }
                    selectedDrawing = $();
                }

                // selecting all drawings
                drawingCount = self.selectAllDrawingsOnSlide();

                // changing selection positions
                if (self.isSlideSelection() && drawingCount === 0) { doNothing = true; }

                if (!doNothing) {
                    startPosition[startPosition.length - 1] = 0; // should already be the case
                    endPosition[endPosition.length - 1] = 1;  // setting the first drawing as selected
                    self.trigger('change'); // informing the listeners, not using default process
                }

            } else {
                isInsideTextFrame = Position.isPositionInsideTextframe(rootNode, self.getStartPosition());
                textFrame = isInsideTextFrame ? Position.getClosestDrawingTextframe(rootNode, self.getStartPosition()) : null;
                start = getFirstPosition(isInsideTextFrame ? textFrame : null, { allowEmpty: true });
                end = getLastPosition(isInsideTextFrame ? textFrame : null);
                doNothing = _.isEqual(start, end) && _.isEqual(start, self.getStartPosition());
            }

            return selectAllDrawings || doNothing ? self : this.setTextSelection(start, end);
        };

        /**
         * If this selection selects a drawing node, changes the browser
         * selection to a range that starts directly before that drawing node,
         * and ends directly after that drawing.
         *
         * @returns {Selection}
         *  A reference to this instance.
         */
        this.selectDrawingAsText = function () {

            var // the selected drawing, as plain DOM node
                drawingNode = selectedDrawing[0],
                // whether the drawing is in inline mode
                inline = DOM.isInlineDrawingNode(drawingNode),
                // previous text span of the drawing node
                prevTextSpan = inline ? drawingNode.previousSibling : null,
                // next text span of the drawing node (skip following floating drawings)
                nextTextSpan = drawingNode ? findNextSiblingNode(drawingNode, function () { return DOM.isPortionSpan(this); }) : null,
                // DOM points representing the text selection over the drawing
                startPoint = null, endPoint = null;

            if (drawingNode) {

                // remove drawing selection boxes
                self.clearDrawingSelection(selectedDrawing);

                // start point after the last character preceding the drawing
                if (DOM.isPortionSpan(prevTextSpan)) {
                    startPoint = new DOM.Point(prevTextSpan.firstChild, prevTextSpan.firstChild.nodeValue.length);
                }
                // end point before the first character following the drawing
                if (DOM.isPortionSpan(nextTextSpan)) {
                    endPoint = new DOM.Point(nextTextSpan.firstChild, 0);
                }

                // set browser selection (do nothing if no start and no end point
                // have been found - but that should never happen)
                if (startPoint || endPoint) {
                    self.setBrowserSelection(backwards ?
                        new DOM.Range(endPoint || startPoint, startPoint || endPoint) :
                        new DOM.Range(startPoint || endPoint, endPoint || startPoint));
                }
            }

            return this;
        };

        /**
         * Modifies IE selection, after default browser selection is prevented,
         * avoiding resize rectangles (due to IE bug with contenteditable property, see #35678).
         * If header is clicked, returns first next valid node and sets text selection.
         * If footer is clicked, returns first previous valid node and sets selection inside.
         *
         * @param {jQuery} activeRootNode
         *  Root node for the document
         *
         * @param {jQuery} $node
         *  Event's target node, that is clicked on.
         */
        this.updateIESelectionAfterMarginalSelection = function (activeRootNode, $node) {
            var startPos;

            // helper function for IE, to modify browser selection on header&footers
            function modifySelection($node) {
                var $nodeParent = $node.parent();

                function getSiblingFromPageBreakTd($node, direction) {
                    var $nodeParentTr = $node.parentsUntil('.pagecontent', 'tr');

                    if ($nodeParentTr.length > 0) {
                        if (direction === 'prev' && $nodeParentTr.prev().length) {
                            return $nodeParentTr.prev();
                        } else if (direction === 'next' && $nodeParentTr.next().length) {
                            return $nodeParentTr.next();
                        }
                        return false;
                    }
                }

                if (DOM.isPageBreakNode($node)) {
                    if ($node.next().length > 0) {
                        return $node.next();
                    } else if ($nodeParent.next().length > 0) {
                        return $nodeParent.next();
                    }
                } else if (DOM.isFooterNode($node) || $node.hasClass('inner-pb-line')) {
                    if (DOM.isPageBreakNode($nodeParent)) {
                        if ($nodeParent.prev().length > 0) {
                            return $nodeParent.prev();
                        } else {
                            if ($nodeParent.parent().is('td')) {
                                return getSiblingFromPageBreakTd($nodeParent, 'prev');
                            }
                            return $nodeParent.parent().prev();
                        }
                    } else if ($node.parentsUntil('.page', '.footer-wrapper').length > 0) {
                        return DOM.getPageContentNode(activeRootNode).children('.p').last();
                    }
                } else if (DOM.isHeaderNode($node)) {
                    if (DOM.isPageBreakNode($nodeParent)) {
                        if ($nodeParent.next().length > 0) {
                            return $nodeParent.next();
                        } else {
                            if ($nodeParent.parent().is('td')) {
                                return getSiblingFromPageBreakTd($nodeParent, 'next');
                            }
                            return $nodeParent.parent().next();
                        }
                    } else if ($node.parentsUntil('.page', '.header-wrapper').length > 0) {
                        return DOM.getPageContentNode(activeRootNode).children('.p').first();
                    }
                }

                return false;
            }

            if ($node.hasClass('cover-overlay')) {
                $node = $node.parent();
            }

            if ($node.is('.header, .header-wrapper')) {
                $node = modifySelection($node);
                if ($node.is('table, tr')) {
                    $node = $node.find('.p').first();
                }
                if ($node.children().length) {
                    $node = $node.children().first();
                }
                if ($node.length > 0) {
                    startPos = Position.getOxoPosition(activeRootNode, $node, 0);
                    if (startPos) {
                        self.setTextSelection(startPos);
                    } else {
                        globalLogger.warn('Selection.updateIESelectionAfterMarginalSelection: no valid position found!');
                    }
                }
            } else if ($node.is('.footer, .footer-wrapper')) {
                $node = modifySelection($node);
                if ($node.is('table, tr')) {
                    $node = $node.find('.p').last();
                }
                if ($node.children().length) {
                    $node = $node.children().last();
                }
                startPos = Position.getOxoPosition(activeRootNode, $node, 0);
                if (startPos) {
                    self.setTextSelection(startPos);
                } else {
                    globalLogger.warn('Selection.updateIESelectionAfterMarginalSelection: no valid position found!');
                }
            }
        };

        /**
         * Updating the selection of a remote client without write privileges.
         */
        this.updateRemoteSelection = function () {

            var // whether the selection before applying the external operation was a range selection
                wasRange = false,
                // whether a drawing is selected
                drawingSelected = false,
                // whether the browser selection can be evaluated
                useBrowserSelection = true,
                // whether a cell range is selected
                cellSelected = false,
                // old logical start and end positions
                oldStartPosition = null, oldEndPosition = null,
                // the drawing node of a selected drawing
                drawingNode,
                // logical position of the selected drawing
                drawingStartPosition, drawingEndPosition,
                // a new calculated valid logical position
                validPos,
                // the node containing the focus
                focusNode = null;

            // checking if the selection before applying the external operation was a range selection
            if (self.hasRange()) {
                wasRange = true;
                oldStartPosition = _.clone(self.getStartPosition());
                oldEndPosition = _.clone(self.getEndPosition());
                drawingSelected = (self.getSelectionType() === 'drawing');
                cellSelected = (self.getSelectionType() === 'cell');
            }

            if (drawingSelected || cellSelected) { useBrowserSelection = false; }

            // in presentation the root node might no longer exist (someone else removed the master or layout slide, 48291)
            if (slideMode && rootNode.parent().length === 0 && docModel.getIdOfFirstSlideOfActiveView) {
                docModel.setActiveSlideId(docModel.getIdOfFirstSlideOfActiveView()); // selecting the first slide in the view
                return;
            }

            // using the browser selection as new selection
            if (useBrowserSelection) {
                self.updateSelectionAfterBrowserSelection();
                // check if it is necessary to restore a selection range (maybe the browser removed a selection range)
                if (wasRange && !self.hasRange()) {
                    validPos = Position.findValidSelection(rootNode, oldStartPosition, oldEndPosition, self.getLastDocumentPosition(), slideMode);
                    self.setTextSelection(validPos.start, validPos.end, { remoteSelectionUpdate: true });
                }
            } else if (drawingSelected) {

                drawingNode = self.getSelectedDrawing();

                if ($(drawingNode).closest(DOM.PAGECONTENT_NODE_SELECTOR).length > 0) {
                    // is the drawing still in the dom?
                    drawingStartPosition = Position.getOxoPosition(rootNode, drawingNode, 0);
                    drawingEndPosition = Position.increaseLastIndex(drawingStartPosition);

                    if (!_.isEqual(drawingStartPosition, self.getStartPosition())) {
                        self.setTextSelection(drawingStartPosition, drawingEndPosition, { remoteSelectionUpdate: true });
                    }
                } else {
                    validPos = Position.findValidSelection(rootNode, self.getStartPosition(), self.getEndPosition(), self.getLastDocumentPosition(), slideMode);
                    self.setTextSelection(validPos.start, validPos.end, { preserveFocus: false, remoteSelectionUpdate: true });

                    // The div.page must get the focus. Unfortunately this fails in Chrome,
                    // if Chrome does not have the focus.
                    if (!$(getFocus()).is(DOM.PAGE_NODE_SELECTOR)) {
                        focusNode = getFocus();
                        globalLogger.info('Missing browser focus -> changing focus failed. Focus at ' + focusNode.nodeName + '.' + focusNode.className);
                    }
                }

            } else if (cellSelected) {

                // Updating position for cell selection
                self.updateSelectionAfterBrowserSelection();

                // maybe the cell selection was destroyed by the operation -> self.getSelectedCellRange() returns null
                if (!self.getSelectedCellRange()) {
                    validPos = Position.findValidSelection(rootNode, oldStartPosition, oldEndPosition, self.getLastDocumentPosition(), slideMode);
                    self.setTextSelection(validPos.start, validPos.end, { remoteSelectionUpdate: true });
                }

            }
        };

        // iterators ----------------------------------------------------------

        /**
         * Calls the passed iterator function for each table cell, if this
         * selection is located inside a table. Processes a rectangular cell
         * selection (if supported by the browser), otherwise a row-oriented
         * text selection inside a table.
         * This function might also be called, when the selection is not inside
         * a table. In this case it leaves immediately with Utils.BREAK. This
         * happens for example during loading a document (Fix for 29021).
         *
         * @param {Function} iterator
         *  The iterator function that will be called for every table cell node
         *  covered by this selection. Receives the following parameters:
         *      (1) {HTMLTableCellElement} the visited DOM cell element,
         *      (2) {Number[]} its logical position (the last two elements in
         *          this array represent the row and column index of the cell),
         *      (3) {Number} the row offset, relative to the first row
         *          in the rectangular cell range,
         *      (4) {Number} the column offset, relative to the first column
         *          in the rectangular cell range.
         *  The last two parameters will be undefined, if the current selection
         *  is a text range in a table. If the iterator returns the Utils.BREAK
         *  object, the iteration process will be stopped immediately.
         *
         * @param {Object} [context]
         *  If specified, the iterator will be called with this context (the
         *  symbol 'this' will be bound to the context inside the iterator
         *  function).
         *
         * @returns {Utils.BREAK | void}
         *  A reference to the Utils.BREAK object, if the iterator has returned
         *  Utils.BREAK to stop the iteration process, otherwise undefined.
         */
        this.iterateTableCells = function (iterator, context) {

            var // information about the cell range and containing table
                cellRangeInfo = this.getSelectedCellRange(),

                // the DOM cells
                firstCellInfo = null, lastCellInfo = null,
                // current cell, and its logical position
                cellInfo = null, cellNode = null, cellPosition = null,

                // row/column indexes for loops
                row = 0, col = 0;

            // check enclosing table, get its position
            if (!cellRangeInfo) {
                return BREAK;
            }

            // resolve position to closest table cell
            firstCellInfo = Position.getDOMPosition(cellRangeInfo.tableNode, cellRangeInfo.firstCellPosition, true);
            lastCellInfo = Position.getDOMPosition(cellRangeInfo.tableNode, cellRangeInfo.lastCellPosition, true);
            if (!firstCellInfo || !$(firstCellInfo.node).is('td') || !lastCellInfo || !$(lastCellInfo.node).is('td')) {
                globalLogger.error('Selection.iterateTableCells(): no table cells found for cell positions');
                return BREAK;
            }

            // visit all cells for rectangular cell selection mode
            if (cellRangeSelected) {

                // loop over all cells in the range
                for (row = 0; row < cellRangeInfo.height; row += 1) {
                    for (col = 0; col < cellRangeInfo.width; col += 1) {

                        // cell position relative to table
                        cellPosition = [cellRangeInfo.firstCellPosition[0] + row, cellRangeInfo.firstCellPosition[1] + col];
                        cellInfo = Position.getDOMPosition(cellRangeInfo.tableNode, cellPosition);

                        // cellInfo will be undefined, if current position is covered by a merged cell
                        if (cellInfo && $(cellInfo.node).is('td')) {
                            cellPosition = cellRangeInfo.tablePosition.concat(cellPosition);
                            if (iterator.call(context, cellInfo.node, cellPosition, row, col) === BREAK) {
                                return BREAK;
                            }
                        }
                    }
                }

            // otherwise: visit all cells row-by-row (text selection mode)
            } else {

                cellNode = firstCellInfo.node;
                while (cellNode) {

                    // visit current cell
                    cellPosition = Position.getOxoPosition(cellRangeInfo.tableNode, cellNode, 0);
                    row = cellPosition[0] - cellRangeInfo.firstCellPosition[0];
                    col = cellPosition[1] - cellRangeInfo.firstCellPosition[1];
                    cellPosition = cellRangeInfo.tablePosition.concat(cellPosition);
                    if (iterator.call(context, cellNode, cellPosition, row, col) === BREAK) { return BREAK; }

                    // last cell reached
                    if (cellNode === lastCellInfo.node) { return; }

                    // find next cell node (either next sibling, or first child
                    // of next row, but skip embedded tables)
                    cellNode = findNextNode(cellRangeInfo.tableNode, cellNode, 'td', DOM.TABLE_NODE_SELECTOR);
                }

                // in a valid DOM tree, there must always be valid cell nodes until
                // the last cell has been reached, so this point should never be reached
                globalLogger.error('Selection.iterateTableCells(): iteration exceeded end of selection');
                return BREAK;
            }
        };

        /**
         * Calls the passed iterator function for each drawing, if this is a drawing
         * selection or a multiple drawing selection.
         * This function might also be called, when no drawing node is selected. In
         * this case it leaves immediately with Utils.BREAK.
         *
         * @param {Function} iterator
         *  The iterator function that will be called for every drawing node
         *  covered by this selection. Receives the following parameter:
         *      (1) {jQuery} the visited jQueryfied DOM drawing element
         *  If the iterator returns the Utils.BREAK
         *  object, the iteration process will be stopped immediately.
         *
         * @param {Object} [context]
         *  If specified, the iterator will be called with this context (the
         *  symbol 'this' will be bound to the context inside the iterator
         *  function).
         *
         * @returns {Utils.BREAK | void}
         *  A reference to the Utils.BREAK object, if the iterator has returned
         *  Utils.BREAK to stop the iteration process, otherwise undefined.
         */
        this.iterateDrawings = function (iterator, context) {

            var // the container of all drawings
                allDrawings = self.getSelectedDrawings();

            // if undo/redo is running, it might be possible, that not all drawings are attached to the DOM (DOCS-1758)
            if (allDrawings && allDrawings.length && docModel.isUndoRedoRunning()) {
                allDrawings = _.filter(allDrawings, function (oneDrawing) { return getDomNode(oneDrawing).parentNode; });
            }

            // exit if no drawing is selected
            if (!allDrawings || allDrawings.length === 0) { return BREAK; }

            _.each(allDrawings, function (oneDrawing) {
                if (DrawingFrame.isGroupDrawingFrame(oneDrawing)) {
                    _.each($(oneDrawing).find(DrawingFrame.NODE_SELECTOR), function (drawingInGroup) {
                        if (iterator.call(context, drawingInGroup) === BREAK) {
                            return BREAK;
                        }
                    });
                } else {
                    if (iterator.call(context, oneDrawing) === BREAK) {
                        return BREAK;
                    }
                }
            });

        };

        /**
         * Calls the passed iterator function for specific content nodes
         * (tables and paragraphs) selected by this selection instance. It is
         * possible to visit all paragraphs embedded in all covered tables and
         * nested tables, or to iterate on the 'shortest path' by visiting
         * tables exactly once if they are covered completely by the selection
         * range and skipping the embedded paragraphs and sub tables. If the
         * selection range end at the very beginning of a paragraph (before the
         * first character), this paragraph is not considered to be included in
         * the selected range.
         *
         * @param {Function} iterator
         *  The iterator function that will be called for every content node
         *  (paragraphs and tables) covered by this selection. Receives the
         *  following parameters:
         *      (1) {HTMLElement} the visited content node,
         *      (2) {Number[]} its logical position,
         *      (3) {Number|Undefined} the logical index of the first text
         *          component covered by the FIRST paragraph; undefined for
         *          all other paragraphs and tables (may point after the last
         *          existing child text component, if the selection starts at
         *          the very end of a paragraph),
         *      (4) {Number|Undefined} the logical index of the last child text
         *          component covered by the LAST paragraph (closed range);
         *          undefined for all other paragraphs and tables,
         *      (5) {Boolean} whether the selection includes the beginning of
         *          the parent container of the current content node,
         *      (6) {Boolean} whether the selection includes the end of the
         *          parent container of the current content node.
         *  If the selection represents a text cursor, the start position will
         *  exceed the end position by 1. Thus, a text cursor in an empty
         *  paragraph will be represented by the text range [0, -1]. If the
         *  iterator returns the Utils.BREAK object, the iteration process will
         *  be stopped immediately.
         *
         * @param {Object} [context]
         *  If specified, the iterator will be called with this context (the
         *  symbol 'this' will be bound to the context inside the iterator
         *  function).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.shortestPath=false]
         *      If set to true, tables that are covered completely by this
         *      selection will be visited, but their descendant components
         *      (paragraphs and embedded tables) will be skipped in the
         *      iteration process. By default, this method visits all
         *      paragraphs embedded in all tables and their sub tables, but
         *      does not visit the table objects. Has no effect for tables that
         *      contain the end paragraph, because these tables are not fully
         *      covered by the selection. Tables that contain the start
         *      paragraph will never be visited, because they start before the
         *      selection.
         *  @param {Number[]} [options.startPos]
         *      An optional logical start position that can be used to reduce the
         *      iteration range. If not specified, the current start position
         *      of the selection is used.
         *  @param {Number[]} [options.endPos]
         *      An optional logical end position that can be used to reduce the
         *      iteration range. If not specified, the current end position
         *      of the selection is used.
         *  @param {Number} [options.maxIterations=-1]
         *      An optional number for limiting the maximum iteration count for
         *      nodes to be investigated. If the value is set to -1, this limit
         *      is ignored.
         *
         * @returns {Utils.BREAK|Undefined}
         *  A reference to the Utils.BREAK object, if the iterator has returned
         *  Utils.BREAK to stop the iteration process, otherwise undefined.
         */
        this.iterateContentNodes = function (iterator, context, options) {

            // checking, if a selection already exists. This is not the case for example in fast loading process.
            if (self.isUndefinedSelection() || self.isTemporaryInvalidSelection()) { return BREAK; }

            var // the start position, that is used inside this function
                localStartPos = getArrayOption(options, 'startPos', startPosition),
                // the end position, that is used inside this function
                localEndPos = getArrayOption(options, 'endPos', endPosition),

                // start node and offset (pass true to NOT resolve text spans to text nodes)
                startInfo = Position.getDOMPosition(rootNode, localStartPos, true),
                // end node and offset (pass true to NOT resolve text spans to text nodes)
                endInfo = Position.getDOMPosition(rootNode, localEndPos, true),

                // whether to iterate on shortest path (do not traverse into completely covered tables)
                shortestPath = getBooleanOption(options, 'shortestPath', false),

                // max iterations
                maxIterations = getOption(options, 'maxIterations', -1),
                // iterator-counter
                iterateCount = 0,

                // paragraph nodes containing the passed start and end positions
                firstParagraph = null, lastParagraph = null,
                // current content node while iterating
                contentNode = null, parentCovered = false;

            // visit the passed content node (paragraph or table); or table child nodes, if not in shortest-path mode
            function visitContentNode(contentNode, parentCovered) {

                var // each call of the iterator get its own position array (iterator is allowed to modify it)
                    position = Position.getOxoPosition(rootNode, contentNode),
                    // start text offset in first paragraph
                    startOffset = (contentNode === firstParagraph) ? _.last(localStartPos) : undefined,
                    // end text offset in last paragraph (convert half-open range to closed range)
                    endOffset = (contentNode === lastParagraph) ? (_.last(localEndPos) - 1) : undefined;

                // If the localEndPos is an empty paragraph in a cell range selection, the attribute
                // must also be assigned to this end position. But in this case is the endOffset '-1' in the
                // last cell. Concerning to task 27361 the attribute is not assigned to the empty paragraph
                // in the bottom right cell of a table. To fix this, it is in this scenario necessary to
                // increase the endOffset to the value '0'! (Example selection: [0,0,0,0,0] to [0,1,1,0,0])
                // On the other hand, it is important that a selection that ends at the beginning of a
                // paragraph, ignores the attribute assignment for the following paragraph. Example:
                // Selection [0,0] to [1,0]: Paragraph alignment 'center' must not be assigned to
                // paragraph [1].

                if ((cellRangeSelected) && (endOffset < 0)) { endOffset = 0; }  // Task 27361

                // visit the content node, but not the last paragraph, if selection
                // does not start in that paragraph and ends before its beginning
                // (otherwise, it's a cursor in an empty paragraph)
                if ((contentNode === firstParagraph) || (contentNode !== lastParagraph) || (endOffset >= Position.getFirstTextNodePositionInParagraph(contentNode))) {
                    return iterator.call(context, contentNode, position, startOffset, endOffset, parentCovered);
                }
            }

            // find the first content node in passed root node (either table or embedded paragraph depending on shortest-path option)
            function findFirstContentNode(rootNode) {

                // in shortest-path mode, use first table or paragraph in cell,
                // otherwise find first paragraph which may be embedded in a sub table)
                return findDescendantNode(rootNode, shortestPath ? DOM.CONTENT_NODE_SELECTOR : DOM.PARAGRAPH_NODE_SELECTOR);
            }

            // find the next content node in DOM tree (either table or embedded paragraph depending on shortest-path option)
            function findNextContentNode(rootNode, contentNode, lastParagraph) {

                // find next content node in DOM tree (searches in own siblings, AND in other nodes
                // following the parent node, e.g. the next table cell, or paragraphs following the
                // containing table, etc.; but skips drawing nodes that may contain their own paragraphs)
                contentNode = findNextNode(rootNode, contentNode, DOM.CONTENT_NODE_SELECTOR, DOM.SKIP_DRAWINGS_AND_P_BREAKS_SELECTOR);

                // iterate into a table, if shortest-path option is off, or the end paragraph is inside the table
                while (DOM.isTableNode(contentNode) && (!shortestPath || (lastParagraph && containsNode(contentNode, lastParagraph)))) {
                    contentNode = findDescendantNode(contentNode, DOM.CONTENT_NODE_SELECTOR);
                }

                return contentNode;
            }

            // check validity of passed positions
            if (!startInfo || !startInfo.node || !endInfo || !endInfo.node) {
                globalLogger.error('Selection.iterateContentNodes(): invalid selection, cannot find first or last DOM node');
                return BREAK;
            }

            // find first and last paragraph node (also in table cell selection mode)
            firstParagraph = findClosest(rootNode, startInfo.node, DOM.PARAGRAPH_NODE_SELECTOR);
            lastParagraph = findClosest(rootNode, endInfo.node, DOM.PARAGRAPH_NODE_SELECTOR);
            if (!firstParagraph || !lastParagraph) {
                // this can happen for absolutely positioned drawings in drawing layer
                // -> switching from drawing layer node to page content
                if (!firstParagraph && startInfo.node.parentNode && DOM.isDrawingLayerNode(startInfo.node.parentNode)) {
                    firstParagraph = findClosest(rootNode, DOM.getDrawingPlaceHolderNode(startInfo.node), DOM.PARAGRAPH_NODE_SELECTOR);
                }

                if (!lastParagraph && endInfo.node.parentNode && DOM.isDrawingLayerNode(endInfo.node.parentNode)) {
                    lastParagraph = findClosest(rootNode, DOM.getDrawingPlaceHolderNode(endInfo.node), DOM.PARAGRAPH_NODE_SELECTOR);
                }

                if (!firstParagraph || !lastParagraph) {
                    globalLogger.error('Selection.iterateContentNodes(): invalid selection, cannot find containing paragraph nodes');
                    return BREAK;
                }
            }

            // rectangular cell range selection
            if (cellRangeSelected) {

                // entire table selected
                if (shortestPath && tableSelected) {
                    return visitContentNode(this.getEnclosingTable(), false);
                }

                // visit all table cells, iterate all content nodes according to 'shortest-path' option
                return this.iterateTableCells(function (cell) {
                    for (contentNode = findFirstContentNode(cell); contentNode; contentNode = findNextContentNode(cell, contentNode)) {
                        if (visitContentNode(contentNode, true) === BREAK) { return BREAK; }
                    }
                }, this);

            } else if (self.isAnyTextFrameDrawingSelection() && !shortestPath) {
                // iterating over all text frame drawings (one text frame is selected or this
                // is a multiple drawing selection with at least one text frame drawing
                return this.iterateDrawings(function (oneDrawing) {

                    if (DrawingFrame.isTextFrameShapeDrawingFrame(oneDrawing)) {
                        for (contentNode = findFirstContentNode(oneDrawing); contentNode; contentNode = findNextContentNode(oneDrawing, contentNode)) {
                            if (visitContentNode(contentNode, true) === BREAK) { return BREAK; }
                        }
                    }

                }, this);

            }

            // check, if the first paragraph can be expanded to a table, if this table is completely covered
            // This is necessary for deleting complete tables at the beginning of the document, after pressing Ctrl-A.
            // Tables at the end of the document are already fine, because of the following implicit paragraph.
            // 'shortestPath' must be checked, so that setting of attributes still works after Ctrl-A.
            if (shortestPath && _(localStartPos).all(function (value) { return (value === 0); }) && localEndPos[0] > 0 && Position.isPositionInTable(rootNode, localStartPos)) {
                firstParagraph = findFarthest(rootNode, firstParagraph, DOM.TABLE_NODE_SELECTOR);
            }

            // iterate through all paragraphs and tables until the end paragraph has been reached
            contentNode = firstParagraph;
            while (contentNode) {

                // check whether the parent container is completely covered
                parentCovered = !containsNode(contentNode.parentNode, firstParagraph) && !containsNode(contentNode.parentNode, lastParagraph);

                // visit current content node
                if (visitContentNode(contentNode, parentCovered) === BREAK) { return BREAK; }

                // maxIterations is set and reached
                if ((maxIterations !== -1) && (iterateCount + 1 === maxIterations)) { return BREAK; }

                // end of selection, or max iterations reached
                if (contentNode === lastParagraph) { return; }

                // find next content node in DOM tree (next sibling paragraph or
                // table, or first node in next cell, or out of last table cell...)
                contentNode = findNextContentNode(rootNode, contentNode, lastParagraph);
                iterateCount++;
            }

            // in a valid DOM tree, there must always be valid content nodes until end
            // paragraph has been reached, so this point should never be reached
            globalLogger.error('Selection.iterateContentNodes(): iteration exceeded end of selection');
            return BREAK;
        };

        /**
         * Calls the passed iterator function for specific nodes selected by
         * this selection instance. It is possible to visit all child nodes
         * embedded in all covered paragraphs (also inside tables and nested
         * tables), or to iterate on the 'shortest path' by visiting content
         * nodes (paragraphs or tables) exactly once if they are covered
         * completely by this selection and skipping the embedded paragraphs,
         * sub tables, and text contents.
         *
         * @param {Function} iterator
         *  The iterator function that will be called for every node covered by
         *  this selection. Receives the following parameters:
         *      (1) {HTMLElement} the visited DOM node object,
         *      (2) {Number[]} the logical start position of the visited node
         *          (if the node is a partially covered text span, this is the
         *          logical position of the first character covered by the
         *          selection, NOT the start position of the span itself),
         *      (3) {Number} the relative start offset inside the visited node
         *          (usually 0; if the visited node is a partially covered text
         *          span, this offset is relative to the first character in the
         *          span covered by the selection),
         *      (4) {Number} the logical length of the visited node (1 for
         *          content nodes, character count of the covered part of text
         *          spans).
         *  If the iterator returns the Utils.BREAK object, the iteration
         *  process will be stopped immediately.
         *
         * @param {Object} [context]
         *  If specified, the iterator will be called with this context (the
         *  symbol 'this' will be bound to the context inside the iterator
         *  function).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.shortestPath=false]
         *      If set to true, tables and paragraphs that are covered
         *      completely by this selection will be visited directly and once,
         *      and their descendant components will be skipped in the
         *      iteration process. By default, this method visits the child
         *      nodes of all paragraphs and tables embedded in tables. Has no
         *      effect for tables that contain the end paragraph, because these
         *      tables are not fully covered by the selection. Tables that
         *      contain the start paragraph will never be visited, because they
         *      start before the selection.
         *  @param {Boolean} [options.split=false]
         *      If set to true, the first and last text span not covered
         *      completely by this selection will be split before the iterator
         *      function will be called. The iterator function will always
         *      receive a text span that covers the contained text completely.
         *  @param {Number} [options.maxIterations=-1]
         *      An optional number for limiting the maximum iteration count for
         *      nodes to be investigated. If the value is set to -1, this limit
         *      is ignored.
         *
         * @returns {Utils.BREAK|Undefined}
         *  A reference to the Utils.BREAK object, if the iterator has returned
         *  Utils.BREAK to stop the iteration process, otherwise undefined.
         */
        this.iterateNodes = function (iterator, context, options) {

            var // whether to iterate on shortest path (do not traverse into completely covered content nodes)
                shortestPath = getBooleanOption(options, 'shortestPath', false),
                // split partly covered text spans before visiting them
                split = getBooleanOption(options, 'split', false),

                // start node and offset
                startInfo = null;

            // special case 'simple cursor': visit the text span
            if (this.isTextCursor()) {

                if (this.isEmptySelection()) { return BREAK; }  // this can happen in Presentation app

                // start node and offset (pass true to NOT resolve text spans to text nodes)
                startInfo = Position.getDOMPosition(rootNode, startPosition, true);
                if (!startInfo || !startInfo.node) {
                    globalLogger.error('Selection.iterateNodes(): invalid selection, cannot find DOM node at start position ' + JSON.stringify(startPosition));
                    return BREAK;
                }

                // if located at the beginning of a component: use end of preceding text span if available
                if ((startInfo.offset === 0) && DOM.isPortionSpan(startInfo.node.previousSibling)) {
                    startInfo.node = startInfo.node.previousSibling;
                    startInfo.offset = startInfo.node.firstChild.nodeValue.length;
                }

                // visit the text component node (clone, because iterator is allowed to change passed position)
                return iterator.call(context, startInfo.node, _.clone(startPosition), startInfo.offset, 0);
            }

            // iterate the content nodes (paragraphs and tables) covered by the selection
            return this.iterateContentNodes(function (contentNode, position, startOffset, endOffset) {

                var // single-component selection
                    singleComponent = false,
                    // text span at the very beginning or end of a paragraph
                    textSpan = null;

                // visit fully covered content node in 'shortest-path' mode
                if (shortestPath && !_.isNumber(startOffset) && !_.isNumber(endOffset)) {
                    return iterator.call(context, contentNode, position, 0, 1);
                }

                // if selection starts after the last character in a paragraph, visit the last text span
                if (_.isNumber(startOffset) && (startOffset >= Position.getLastTextNodePositionInParagraph(contentNode))) {
                    if ((textSpan = DOM.findLastPortionSpan(contentNode))) {
                        return iterator.call(context, textSpan, position.concat([startOffset]), textSpan.firstChild.nodeValue.length, 0);
                    }
                    globalLogger.error('Selection.iterateNodes(): cannot find last text span in paragraph at position ' + JSON.stringify(position));
                    return BREAK;
                }

                // visit covered text components in the paragraph
                singleComponent = _.isNumber(startOffset) && _.isNumber(endOffset) && (startOffset === endOffset);
                return Position.iterateParagraphChildNodes(contentNode, function (node, nodeStart, nodeLength, nodeOffset, offsetLength) {

                    // skip floating drawings (unless they are selected directly) and helper nodes
                    if (DOM.isTextSpan(node) || DOM.isInlineComponentNode(node) || (singleComponent && DOM.isFloatingDrawingNode(node))) {
                        // create local copy of position, iterator is allowed to change the array
                        return iterator.call(context, node, position.concat([nodeStart + nodeOffset]), nodeOffset, offsetLength);
                    }

                    // options for Position.iterateParagraphChildNodes(): visit empty text spans
                }, this, { allNodes: true, start: startOffset, end: endOffset, split });

                // options for Selection.iterateContentNodes()
            }, this, { shortestPath, maxIterations: getOption(options, 'maxIterations') });

        };

        /**
         * Setter for new rootNode inside selection. Happens when, for ex., when edit mode or header/footer is edited,
         * or return from this edit mode, to normal document edit mode.
         * In future may be expanded to other rootNodes
         *
         * @param {Node|jQuery} newNode
         */
        this.setNewRootNode = function (newNode) {

            var $newNode = $(newNode);

            if (!DOM.isPageNode(rootNode) && origNonPageFocusMethod) {
                rootNode[0].focus = origNonPageFocusMethod; // restoring the original focus function
                origNonPageFocusMethod = null; // no longer required
            }

            if (!DOM.isPageNode(newNode)) {
                origNonPageFocusMethod = $newNode[0].focus; // saving the new original focus function
                // overwrite the focus() method of the passed new root node (DOCS-2990)
                // -> this is done during the initialization only for the page node
                $newNode[0].focus = function () {
                    self.restoreBrowserSelection({ forceWhenReadOnly: true });
                };
            }

            rootNode = $newNode;
            forceSelectionChangeEvent = true;
        };

        /**
         *  Getter for the current root node of the selection.
         *
         * @returns {HTMLElement|jQuery}
         *  The current active root node for the selection.
         */
        this.getRootNode = function () {
            return rootNode;
        };

        /**
         * Getter for the current target ID of the root node of the selection,
         * if there is one.
         *
         * @returns {String}
         *  The target of the current active root node for the selection.
         */
        this.getRootNodeTarget = function () {
            return DOM.getTargetContainerId(rootNode);
        };

        /**
         * Sets the current selection to the first, respectively to the next, drawing.
         * Except drawings in headers/footers.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.backwards=false]
         *      decides, whether the next selected drawing should be before or after
         *      the current
         */
        this.selectNextDrawing = function (options) {

            var // the container for all drawings
                arrDrawings = null,
                // the index specifying the selected drawing
                selectedIndex = 0,
                // the logical start and end positions
                start = null, end = null,
                // whether the next or the previous drawing shall be selected
                backwards = getBooleanOption(options, 'backwards', false);

            // helper function to determine the next or previous drawing index
            function getNewDrawingIndex(oldIndex, maxLength, localBackwards) {

                var newIndex = 0;

                if (localBackwards) {
                    // count one down to select the prev drawing, or restart at zero
                    newIndex = (oldIndex > 0) ? (oldIndex - 1) : (maxLength - 1);
                } else {
                    // count one up to select the next drawing, or restart at zero
                    newIndex = (oldIndex + 1 < maxLength) ? (oldIndex + 1) : 0;
                }

                return newIndex;
            }

            if (slideMode) {

                // using only the drawings on the current slide
                var slide = docModel.getSlideById(docModel.getActiveSlideId());
                arrDrawings = slide.children(DrawingFrame.NODE_SELECTOR);

                if (arrDrawings.length > 0) {
                    selectedIndex = _.indexOf(arrDrawings, selectedDrawing[0]);
                    selectedIndex = getNewDrawingIndex(selectedIndex, arrDrawings.length, backwards);

                    start = Position.getOxoPosition(slide, arrDrawings[selectedIndex], 0);
                    start = docModel.getActiveSlidePosition().concat(start);

                } else {
                    if (docApp.isPresentationApp()) {
                        self.setSlideSelection(); // no drawing available -> keep or make a slide selection (DOCS-2227)
                    }
                    return;
                }

            } else {

                arrDrawings = rootNode.find('.pagecontent > :not(.header, .footer) > .drawing, .pagecontent > :not(.header, .footer) > .drawingplaceholder');

                if (arrDrawings.length > 0) {

                    // if already a drawing is selected, select next one
                    if (selectedDrawing.length > 0) {

                        selectedIndex = _.indexOf(arrDrawings, selectedDrawing[0]);
                        // if the selected drawing is not in the arr, it should be found via the placeholder
                        if (selectedIndex === -1) {
                            selectedIndex = _.indexOf(arrDrawings, $('.drawingplaceholder[data-drawingid="' + selectedDrawing.data('drawingid') + '"]').get(0));
                        }

                        selectedIndex = getNewDrawingIndex(selectedIndex, arrDrawings.length, backwards);
                    }

                    // get start/end positon of drawing
                    start = Position.getOxoPosition(rootNode, arrDrawings[selectedIndex], 0);
                }
            }

            if (start) {
                // the end position is always increased by '1'
                end = Position.increaseLastIndex(start);

                // select drawing
                self.setTextSelection(start, end);

                // scroll to position of drawing
                docApp.getView().scrollToChildNode(arrDrawings[selectedIndex]);
            }
        };

        /**
         * Setting the cursor into the text frame at the last text position inside the text frame.
         * Does nothing, if this is not a drawing or if the selected drawing is not a text frame.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.complete=false]
         *      Whether the complete content inside the text frame shall be selected. This is the
         *      default in the presentation app. In the text app, the cursor is set to the last
         *      text position inside the text frame.
         *
         * @returns {Boolean}
         *  Whether the selection was set into the text frame.
         */
        this.setSelectionIntoTextframe = function (options) {

            var // the new text position
                startPosition = null, endPosition = null,
                // whether the complete content shall be selected
                complete = getBooleanOption(options, 'complete', false),
                // whether the selection was successfully set into the text frame
                setIntoTextFrame = false;

            if (selectedDrawing.length > 0 && DrawingFrame.isTextFrameShapeDrawingFrame(selectedDrawing)) {
                // getting the first text position inside the text frame
                endPosition = Position.getLastTextPositionInTextFrame(docModel.getCurrentRootNode(), selectedDrawing);

                // selecting the complete content inside the text frame
                if (complete) { startPosition = Position.getFirstTextPositionInTextFrame(docModel.getCurrentRootNode(), selectedDrawing); }
            }

            // setting the text selection
            if (!complete && endPosition) {
                self.setTextSelection(endPosition);
                setIntoTextFrame = true;
            } else if (complete && startPosition && endPosition) {
                self.setTextSelection(startPosition, endPosition);
                setIntoTextFrame = true;
            }

            return setIntoTextFrame;
        };

        /**
         * Toggling the selection between a text frame and its content. In the presentation app
         * this is triggered by pressing 'F2'. If a text is selected, the new selection will be
         * the surrounding text frame. And if a text frame is selected, its content will be
         * selected (dependent from the options).
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.complete=false]
         *      Whether the complete content inside the text frame shall be selected. This is the
         *      default in the presentation app. In the text app, the cursor is set to the last
         *      text position inside the text frame.
         *  @param {Boolean} [options.keepDraggingActive=false]
         *      When the user clicked the mouse down button on the drawing border, an active dragging
         *      must not be stopped by a selection toggle (DOCS-3822).
         */
        this.toggleTextFrameAndTextSelection = function (options) {

            var // the logical start position of the text frame
                start = null;

            if (self.isAdditionalTextframeSelection()) {
                // select the text frame itself
                start = Position.getOxoPosition(docModel.getCurrentRootNode(), self.getSelectedTextFrameDrawing(), 0);
                if (start) { self.setTextSelection(start, Position.increaseLastIndex(start), { keepDraggingActive: getBooleanOption(options, 'keepDraggingActive', false) }); }
            } else {
                // jumping into the text frame (if one is selected)
                self.setSelectionIntoTextframe(options);
            }
        };

        /**
         * Setting the selection to one specified drawing node. This must be an descendant of the current
         * root node.
         *
         * @param {Node|jQuery} drawing
         *  The drawing node that shall be selected.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.textSelection=false]
         *      Whether the selection shall be set into the text in the drawing. This is only possible, if
         *      there is a text frame inside the shape. The default value is 'false', so that the drawing
         *      itself if selected and not the text inside.
         */
        this.selectOneSpecificDrawing = function (drawing, options) {
            var pos = null;
            if (!DrawingFrame.isDrawingFrame(drawing)) { return; } // do nothing, if the specified node is not a drawing
            pos = Position.getOxoPosition(docModel.getCurrentRootNode(), drawing, 0);
            if (pos) {
                self.setTextSelection(pos, Position.increaseLastIndex(pos));
                if (getBooleanOption(options, 'textSelection', false)) {
                    self.toggleTextFrameAndTextSelection();
                }
            }
        };

        /**
         * Getting a valid text position, if the current selection is a drawing selection.
         * If this drawing is a text frame, the cursor will be set into the drawing. If this
         * drawing is no text frame, the cursor is set before the drawing.
         *
         * @returns {Array<Number>|null}
         *  The new logical text position. Or null, if could not be determined.
         */
        this.getValidTextPositionAccordingToDrawingFrame = function () {
            // the new text position
            var textPosition = null;
            // selection's start position
            var startPos = self.getStartPosition();
            // oxo position of the selected drawing
            var selDrawingPos = null;

            if (selectedDrawing.length > 0) {

                if (DrawingFrame.isTextFrameShapeDrawingFrame(selectedDrawing)) {

                    if (self.isTableDrawingSelection()) {
                        // setting the selection to the last position into the upper left table cell
                        textPosition = Position.getLastTextPositionInTableCell(rootNode, selectedDrawing.find(DOM.CELL_NODE_SELECTOR)[0]);
                    } else {
                        // setting the selection into the text frame
                        textPosition = Position.getLastTextPositionInTextFrame(rootNode, selectedDrawing);
                    }
                } else {
                    if (DOM.isSlideNode(selectedDrawing.parent())) {
                        // setting the selection before the drawing (application specific!)
                        textPosition = null;
                    } else {
                        // if start position from selection and drawing position are not matching, correct text position.
                        // This usually happens when drawing is part of the group (#54003)
                        selDrawingPos = Position.getOxoPosition(rootNode, selectedDrawing);
                        textPosition = equalPositions(selDrawingPos, startPos) ? startPos : selDrawingPos;
                    }
                }
            }

            return textPosition;
        };

        /**
         * Setting the handler for resizing and moving drawing. This handler is
         * different in the several applications.
         *
         * @param {Function} handler
         *  The handler function for selection drawings and moving and resizing
         *  of drawings.
         */
        this.setDrawingResizeHandler = function (handler) {
            drawingResizeHandler = handler;
        };

        /**
         * Setting the handler for updating the selection frame, that might be located inside
         * the selection overlay node.
         * This function is currently only specified in the text application.
         *
         * @param {Function} handler
         *  The handler function for updating size and position, if this is located inside the
         *  selection overlay node.
         */
        this.setUpdateOverlaySelectionHandler = function (handler) {
            updateOverlaySelectionHandler = handler;
        };

        /**
         * Setting the logical position for the active slide. For the slide at position [3], this
         * selection is done on position [3,0]. All other selections are removed, also the multi
         * drawing selections.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.triggerChange=true]
         *      If set to false, the change of the selection will not cause the 'change' event
         *      to be triggered. This can be used for performance reason in special circumstances.
         */
        this.setSlideSelection = function (options) {

            var // whether the change event shall be triggered
                doTrigger = getBooleanOption(options, 'triggerChange', true),
                // the logical slide position
                slidePosition = null;

            // do nothing, if there is no active slide (maybe it was removed before or is not already inserted yet, 45742)
            // or do nothing, if the view is already removed (DOCS-2438)
            if (docModel.isEmptySlideView() || docModel.getActiveSlideIndex() === -1 || docApp.isInQuit()) { return; }

            slidePosition = Position.appendNewIndex(docModel.getActiveSlidePosition());

            self.setTextSelection(slidePosition, slidePosition, { simpleTextSelection: true, slideSelection: true });

            // set focus and browser selection into clipboard node (but do not set focus in presentation mode)
            if (!docModel.isPresentationMode()) { self.setFocusIntoClipboardNode({ specialTouchHandling: true }); }
            self.setBrowserSelectionToContents(self.getClipboardNode(), { preserveFocus: true });

            if (doTrigger) { self.trigger('change'); }
        };

        /**
         * Clearing all kinds of drawing selections.
         */
        this.clearAllDrawingSelections = function () {

            if (docApp.isInQuit()) {
                return;
            }

            // clearing the selection of the selected drawing
            if (self.getSelectedDrawing().length > 0) {
                self.clearDrawingSelection(selectedDrawing);
                selectedDrawing = $();
            }

            // clearing the selection of an additional text frame selection
            if (self.isAdditionalTextframeSelection()) {
                docModel.addTemplateTextIntoTextSpan(selectedTextFrameDrawing);
                self.clearDrawingSelection(selectedTextFrameDrawing);
                if (docModel.getSlideTouchMode()) { selectedTextFrameDrawing.removeClass('active-edit'); } // optionally ending the edit mode
                selectedTextFrameDrawing = $();
            }

            // clearing a multi selection
            if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                self.clearMultiSelection();
            }

            if (!slideMode) { docApp.getView().clearVisibleDrawingAnchor(); }
        };

        /**
         * Restoring all kinds of drawing selections. Removing the existing
         * drawing selections and draw them again.
         */
        this.restoreAllDrawingSelections = function () {

            var // a container with all selected drawings
                allSelectedDrawings = null;

            if (docApp.isInQuit()) {
                return;
            }

            // clearing the selection of the selected drawing
            if (self.getSelectedDrawing().length > 0) {
                allSelectedDrawings = selectedDrawing;
            }

            // clearing the selection of an additional text frame selection
            if (self.isAdditionalTextframeSelection()) {
                allSelectedDrawings = selectedTextFrameDrawing;
            }

            // clearing a multi selection
            if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                allSelectedDrawings = self.getAllSelectedDrawingNodes();
            }

            if (allSelectedDrawings) {
                _.each(allSelectedDrawings, function (drawingNode) {
                    self.clearDrawingSelection(drawingNode);
                    self.drawDrawingSelection(drawingNode);
                });
            }

        };

        /**
         * Clearing the complete current selection without setting a new selection.
         *
         * Info: This is not valid in OX Text.
         * It can be used in OX Presentation, if the last slide in the active view was deleted.
         * In OX Spreadsheet this function is called, if the user leaves the text edit mode.
         */
        this.setEmptySelection = function (options) {
            startPosition = [];
            endPosition = [];
            backwards = false;
            isBackwardCursor = false;
            selectedDrawing = $();
            selectedTextFrameDrawing = $();
            selectedTable = $();
            cellRangeSelected = false;

            // set focus and browser selection into clipboard node
            if (!(options && options.preserveFocus)) {
                self.setFocusIntoClipboardNode({ specialTouchHandling: true });
                self.setBrowserSelectionToContents(self.getClipboardNode(), { preserveFocus: true });
            }

            self.trigger('change');
        };

        /**
         * Whether the current selection is defined. In Presentation app it can
         * happen, that this selection is not set, if the last document slide is
         * removed.
         *
         * @returns {Boolean}
         *  Whether the current selection is defined.
         */
        this.isEmptySelection = function () {
            return startPosition.length === 0;
        };

        /**
         * Setting the existing selection temporarely disabled or activate it again. This is
         * for example important for long running processes like resolvering of change tracks.
         * If the selection is not disabled, there can be operation success handlers, that
         * read the selection, that might not be valid and therefore show an error in the
         * console.
         *
         * @param {Boolean} value
         *  Allow or disallow the current selection for internal usage.
         */
        this.setTemporaryInvalidSelection = function (value) {
            isTemporaryInvalidSelection = value;
        };

        /**
         * Whether the existing selection can temporarely not be used. This is for
         * example important for operation success handlers that might be called
         * during a long running process like resolving of change tracks.
         *
         * @returns {Boolean}
         *  Whether the current selection is temporarely invalid.
         */
        this.isTemporaryInvalidSelection = function () {
            return isTemporaryInvalidSelection;
        };

        /**
         * Return the selection object to send it to the server
         *
         * @returns {Array<Object>}
         *  An array of objects with "type", "start", "end" properties.
         */
        this.getRemoteSelectionsObject = function () {
            var selections = [];
            if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                self.getMultiSelection().forEach(function (drawing) {
                    selections.push({
                        type: 'drawing',
                        start: _.clone(drawing.startPosition),
                        end: _.clone(drawing.endPosition)
                    });
                });
            } else {
                selections.push({
                    type: self.getSelectionType(),
                    start: self.getStartPosition(),
                    end: self.getEndPosition()
                });
            }
            return selections;
        };

        /**
         * Checks which is the correct root node of given node, and sets it.
         * Three types of root node: comment root node, header/footer, and page root node.
         * Also check bugs #42671 and #45752.
         *
         * @param {Node|jQuery} node
         */
        this.switchToValidRootNode = function (node) {

            var newRootNode = null;

            if (DOM.isMarginalNode(node) || DOM.isMarginalNode($(node).parent())) { // also checking the parent (65802)
                newRootNode = DOM.getClosestMarginalTargetNode(node);
                if (docModel.isHeaderFooterEditState() && docApp.getModel().getActiveTarget() !== DOM.getTargetContainerId(newRootNode)) {
                    docModel.getPageLayout().leaveHeaderFooterEditMode();
                }
                if (!docModel.isHeaderFooterEditState()) {
                    docModel.getPageLayout().enterHeaderFooterEditMode(newRootNode);
                }
                self.setNewRootNode(newRootNode);
            } else {
                if (docModel.isHeaderFooterEditState()) {
                    docModel.getPageLayout().leaveHeaderFooterEditMode();
                }
                docModel.setActiveTarget();
                self.setNewRootNode(docModel.getNode());
            }
        };

        /**
         * Switching an text selection range to the cursor selection using the
         * start position of the range (DOCS-2978).
         *
         * This can be used for example in Safari Browser, so that the 'edit'
         * menu (that sends a 'beforepaste' event when opened) cannot destroy
         * the document. This is possible, when the user selects 'delete' from
         * this edit menu. In this the selected content is removed from the
         * document without any operation.
         */
        this.switchSelectionRangeToCursorSelection = function () {
            if (self.isTextSelection() && self.hasRange()) {
                self.setTextSelection(self.getStartPosition());
            }
        };

        /**
         * Setting a selection range for two logical positions that describe together one surrogate pair.
         * This function can be used from the current logical position if the previous or the next character
         * shall be investigated.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.backward=false]
         *      If set to false, the character left of the current logical position is investigated. Otherwise
         *      the following character right of the current logical position.
         *
         * @returns {Boolean}
         *  Whether the previous or following character is a surrogate pair.
         */
        this.selectUnicodePairIfPossible = function (options) {

            var isSurrogatePair = false;
            var backward = getBooleanOption(options, 'backward', false);
            var testPosition = backward ? Position.increaseLastIndex(startPosition, -2) : _.copy(startPosition);

            var localRootNode = docModel.getCurrentRootNode();
            var dompoint = Position.getDOMPosition(localRootNode, testPosition, true);

            if (dompoint && dompoint.node) {
                if (DOM.isTextSpan(dompoint.node) && _.isNumber(dompoint.offset)) {
                    var text = $(dompoint.node).text();
                    if (text.length > 0) { isSurrogatePair = unicode.hasSurrogatePair(text, dompoint.offset); }
                }
            }

            if (isSurrogatePair) {
                if (backward) {
                    self.setTextSelection(testPosition, startPosition, { simpleTextSelection: true });
                } else {
                    self.setTextSelection(startPosition, Position.increaseLastIndex(startPosition, 2), { simpleTextSelection: true });
                }
            }

            return isSurrogatePair;
        };

        /**
         * TODO
         *
         * @param {*} node
         */
        this.setActiveCropNode = function (node) {
            activeCropDrawing = $(node);
        };

        /**
         * TODO
         */
        this.getActiveCropNode = function () {
            return activeCropDrawing;
        };

        /**
         * TODO
         */
        this.isCropMode = function () {
            return activeCropDrawing.length > 0;
        };

        /**
         * Setting a marker to indicate whether a drawing is currently moved, resized or rotated.
         *
         * @param {Boolean} mode
         *  The tracking mode. If set to true, a drawing is currently moved, resized or rotated. When
         *  finishing the tracking this value must be set to false.
         */
        this.setActiveTracking = function (mode) {
            isTrackingActive = mode;
        };

        // initialization -----------------------------------------------------

        // lazy initialization with access to the application model and view
        docApp.onInit(function () {
            clipboardNode = docApp.getView().createClipboardNode();

            self.listenTo(docApp.getView(), 'change:zoom', function () {
                var drawingNodes = null;

                if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                    drawingNodes = self.getAllSelectedDrawingNodes(); // returns {Node[]|jQuery[]|[]}
                } else {
                    drawingNodes = self.getAnyDrawingSelection(); // returns {jQuery|Null}
                }

                if (drawingNodes && drawingNodes.length) {
                    var
                        zoomFactor    = docApp.getView().getZoomFactor(),
                        zoomValue     = 100 * zoomFactor,
                        scaleHandles  = 1 / zoomFactor;

                    _.each(drawingNodes, function (drawingNode) {
                        var $drawingNode = $(drawingNode);
                        self.clearDrawingSelection($drawingNode);
                        self.drawDrawingSelection($drawingNode);
                        DrawingFrame.updateScaleHandles(drawingNodes, { zoomValue, scaleHandles });
                    });
                }
            });

            // OT: Adapt selection after each and every applied external operation
            self.listenTo(docApp.getModel(), 'operation:external', adaptLocalSelectionAfterOT);

            // OT: Adapting the undo/redo selection state after pressing undo/redo and existing external operations
            // DOCS-1898: do not transform Spreadsheet selection states (different object formats)
            if (!docApp.isSpreadsheetApp()) { self.listenTo(docApp.docModel.undoManager, 'transform:selectionstate', transformSelectionState); }

            // OT: Adapting the selection state of an temporarely inserted comment model in OX Text
            if (docApp.isTextApp()) { self.listenTo(docApp.docModel.getCommentLayer(), 'transform:selectionstate', transformSelectionState); }
        });

        // Bug 26283: Overwrite the focus() method of the passed editor root
        // node. When focusing the editor, the text selection needs to be
        // restored first, or the internal clipboard node will be focused,
        // depending on the current selection type. By overwriting the DOM
        // focus() method, all focus attempts will be covered, also global
        // focus traveling with the F6 key triggered by the UI core.
        rootNode[0].focus = function () {
            self.restoreBrowserSelection({ forceWhenReadOnly: true });
        };

        // initialization after successful import
        this.waitForImportSuccess(function () {
            if ((_.browser.MacOS || _.browser.Firefox) && !TOUCH_DEVICE) {
                // observing text selection ranges, DOCS-2981: Observer required for all browsers on MacOS and FF on all systems (because of F10)
                self.member(new SelectionRangeObserver(docApp)); // members are destroyed, when 'this' is destroyed
            }
        });

        // destroy all class members on destruction
        this.registerDestructor(function () {
            // clear clipboard node and remove it from DOM
            if (clipboardNode) {
                clearClipboardNode();
                clipboardNode.remove();
            }

            // restore original focus() method of the root node DOM element (prevent leaks)
            rootNode[0].focus = originalFocusMethod;
        });
    }
}
