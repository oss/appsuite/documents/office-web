/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { getBooleanOption, getDomNode } from '@/io.ox/office/tk/utils';

import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { clearSelection, getDrawingType, getFarthestGroupNode,
    isDrawingFrame, isGroupDrawingFrame, isGroupDrawingFrameWithShape, isGroupedDrawingFrame,
    isTextFrameShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { ABSOLUTE_DRAWING_SELECTOR, SLIDE_SELECTOR } from '@/io.ox/office/textframework/utils/dom';
import { getDOMPosition, getOxoPosition, getParagraphElement, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';

// class MultiSelectionMixin ==============================================

/**
 * A mix-in class for the multi selection handling of an application.
 *
 * @param {TextBaseModel} docModel
 *  The document model instance.
 */
export default function MultiSelectionMixin(docModel) {

    var // self reference
        self = this,

        // the application instance
        app = docModel.getApp(),

        // slide mode for spreadsheet/presentation
        slideMode = docModel.useSlideMode(),

        // whether the application supports multiple selections
        isMultiSelectionApp = app.isMultiSelectionApplication(),

        // the container for the objects describing a selection with the
        // properties 'startPosition' and 'endPosition'. The selections are
        // sorted corresponding to the start position value.  Furthermore
        // the selection contains the property 'drawing', that contains the
        // jQuerified drawing node. For performance reasons, the selection
        // must also contain the drawing type.
        multipleSelections = [];

    // private methods ----------------------------------------------------

    /**
     * Adding one selection object to the container of all selections. The logical
     * position are sorted.
     *
     * @param {Object} selection
     *  The object describing one selection. This contains the logical start
     *  and the logical end position of the selection. Furthermore it contains
     *  the property 'drawing', that contains the jQuerified drawing node.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    function addOneDrawingSelectionIntoMultiSelection(selection) {
        multipleSelections.push(selection);
        multipleSelections.sort((sel1, sel2) => comparePositions(sel1.startPosition, sel2.startPosition));
        self.setValidMultiSelectionValues(multipleSelections);
        if (!selection.type) { selection.type = getDrawingType(selection.drawing); } // adding the drawing type, for faster access
        return multipleSelections.length;
    }

    /**
     * Removing one selection object from the container of all selections. The logical
     * start position is used to find the selection, that needs to be removed.
     *
     * @param {Number[]} startPosition
     *  The logical start position of the selection that shall be removed.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    function removeOneDrawingSelectionFromMultiSelection(startPosition) {

        multipleSelections = _.reject(multipleSelections, function (oneSelection) {
            return _.isEqual(oneSelection.startPosition, startPosition);
        });

        self.setValidMultiSelectionValues(multipleSelections);
        return multipleSelections.length;
    }

    /**
     * OT: Removing one selection object from the container of all selections.
     * A marker is used to find the selection, that needs to be removed.
     *
     * @param {String} marker
     *  The name of the property that is used to mark the selection for removal.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    function removeOneDrawingSelectionFromMultiSelectionByMarker(marker) {

        multipleSelections = _.reject(multipleSelections, function (oneSelection) {
            return oneSelection[marker];
        });

        self.setValidMultiSelectionValues(multipleSelections);
        return multipleSelections.length;
    }

    /**
     * Check if the current selection is a drawing selection and another drawing
     * than the specified drawing is selected.
     *
     * @param {Node|jQuery} drawing
     *  A drawing node.
     *
     * @returns {Boolean}
     *  Whether the current selection is a drawing selection and another drawing
     *  than the specified drawing is selected.
     */
    function anotherDrawingSelected(drawing) {
        return self.isAnyDrawingSelection() && getDomNode(drawing) !== getDomNode(self.getAnyDrawingSelection());
    }

    /**
     * OT: Checking whether a multi selection needs to be converted into a single selection.
     */
    function checkMultiSelectionState() {

        // the logical start and end position of a drawing
        var startPos = null, endPos = null;

        if (self.isMultiSelection() && self.getMultiSelectionCount() === 1) {

            // saving the selection values of the remaining selected drawing
            startPos = _.clone(multipleSelections[0].startPosition);
            endPos = _.clone(multipleSelections[0].endPosition);

            // clearing multiple selection, setting single drawing selection
            self.clearMultiSelection();

            self.setTextSelection(startPos, endPos);
        }
    }

    // public methods -----------------------------------------------------

    /**
     * Returns whether this application supports multiple selections.
     *
     * @returns {Boolean}
     *  Whether the application supports multiple selections.
     */
    this.isMultiSelectionSupported = function () {
        return isMultiSelectionApp;
    };

    /**
     * Returns whether several drawings are selected.
     *
     * @returns {Boolean}
     *  Whether several drawings are selected.
     */
    this.isMultiSelection = function () {
        return multipleSelections.length > 0;
    };

    /**
     * Returns whether several drawings are selected and none of these
     * drawings is of type 'table'.
     *
     * @returns {Boolean}
     *  Whether several drawings are selected and none of these drawings
     *  is of type 'table'.
     */
    this.isMultiSelectionWithoutTable = function () {
        return self.isMultiSelection() && !self.isAtLeastOneTableInMultiSelection();
    };

    /**
     * Returns the container of selections. This container stores objects
     * containing the properties 'startPosition' and 'endPosition'. The values
     * are the logical positions, that describe one selection.
     *
     * Info: This function returns an array with objects, that contain the
     *       logical positions for the start and the end of each drawing.
     *       If this positions are used for example for the creation of operations,
     *       they must be cloned before usage (TODO).
     *
     * @returns {any[]}
     *  The container for the objects that describe one selection by its
     *  logical start and its logical end position.
     */
    this.getMultiSelection = function () {
        return multipleSelections;
    };

    /**
     * Returns the number of selected drawings in the multi selection.
     *
     * @returns {Number}
     *  The number of selected drawings in the multi selection.
     */
    this.getMultiSelectionCount = function () {
        return multipleSelections.length;
    };

    /**
     * Returns a collector with all drawing nodes in the selection. If this
     * is no multiple drawing selection, an empty array is returned.
     *
     * @returns {Node[]|jQuery[]|[]}
     *  An array with all drawing nodes in the selection. If this
     *  is no multiple drawing selection, an empty array is returned.
     */
    this.getAllSelectedDrawingNodes = function () {

        var // the container for all drawing nodes
            allDrawingNodes = [];

        _.each(multipleSelections, function (drawingSelection) {
            allDrawingNodes.push(self.getDrawingNodeFromMultiSelection(drawingSelection));
        });

        return allDrawingNodes;
    };

    /**
     * Returns, whether in a multi drawing selection at least one text frame
     * shape is seleted.
     *
     * @returns {Boolean}
     *  Whether in a multi drawing selection at least one text frame shape
     *  is seleted.
     */
    this.isAtLeastOneTextFrameInMultiSelection = function (options) {
        var searchInGroup = getBooleanOption(options, 'searchInGroup', false);
        return self.isMultiSelectionSupported() && self.isMultiSelection() && _.isObject(_.find(multipleSelections, function (selection) {
            var drawingNode = self.getDrawingNodeFromMultiSelection(selection);
            if (isTextFrameShapeDrawingFrame(drawingNode) || (searchInGroup && isGroupDrawingFrameWithShape(drawingNode))) {
                return true;
            }
            return false;
        }));
    };

    /**
     * Returns, whether in a multi drawing selection at least one table drawing
     * is seleted.
     *
     * @returns {Boolean}
     *  Whether in a multi drawing selection at least one table drawing is seleted.
     */
    this.isAtLeastOneTableInMultiSelection = function () {
        return self.isMultiSelectionSupported() && self.isMultiSelection() && self.getSelectedDrawingsTypes().has('table');
    };

    /**
     * Returns, whether in a multi drawing selection one table drawings are selected.
     *
     * @returns {Boolean}
     *  Whether in a multi drawing selection only table drawings are seleted.
     */
    this.onlyTableDrawingsInMultiSelection = function () {
        return self.isMultiSelectionSupported() && self.isMultiSelection() && self.isOnlyTableSelection();
    };

    /**
     * Returns whether the current selection (single or multi drawing selection) contains
     * only table drawing nodes.
     *
     * @returns {Boolean}
     *  Whether the current selection contains at least one and only table drawing nodes.
     */
    this.isOnlyTableSelection = function () {
        return self.isOnlyDrawingSelectionOfType('table');
    };

    /**
     * Returns whether the current selection (single or multi drawing selection) contains
     * only image drawing nodes.
     *
     * @returns {Boolean}
     *  Whether the current selection contains at least one and only image drawing nodes.
     */
    this.isOnlyImageSelection = function () {
        return self.isOnlyDrawingSelectionOfType('image');
    };

    /**
     * Returns whether the current selection (single or multi drawing selection) contains
     * only drawing nodes of a specified type.
     *
     * @param {String} drawingType
     *  The drawing type
     *
     * @returns {Boolean}
     *  Whether the current selection contains at least one and only drawing nodes of the
     *  specified type.
     */
    this.isOnlyDrawingSelectionOfType = function (drawingType) {
        var drawingTypes = self.getSelectedDrawingsTypes();
        return drawingTypes.has(drawingType) && (drawingTypes.size === 1);
    };

    /**
     * Returns whether the current selection (single or multi drawing selection) contains
     * no table drawing nodes.
     *
     * @returns {Boolean}
     *  Whether the current selection contains no table drawing nodes.
     */
    this.isDrawingSelectionWithoutTable = function () {
        var drawingTypes = self.getSelectedDrawingsTypes();
        return (drawingTypes.size > 0) && !drawingTypes.has('table'); // there are selected drawing(s), but no table
    };

    /**
     * Returns, whether in a multi drawing selection at least one drawing of type 'group' is selected.
     *
     * @returns {Boolean}
     *  Whether in a multi drawing selection at least one drawing of type 'group' is selected.
     */
    this.isAtLeastOneGroupInMultiSelection = function () {
        return self.isMultiSelectionSupported() && self.isMultiSelection() && self.getSelectedDrawingsTypes().has('group');
    };

    /**
     * Returns all selection objects of drawing nodes of type 'group' from a multiple selection.
     *
     * @returns {Object[]}
     *  An object with all selection objects from a multiple drawing selection that describe
     *  drawings of type 'group'.
     */
    this.getAllDrawingGroupsFromMultiSelection = function () {
        return _.filter(multipleSelections, function (selection) {
            return isGroupDrawingFrame(self.getDrawingNodeFromMultiSelection(selection));
        });
    };

    /**
     * Returns all selection objects of drawing nodes that are not of type 'group' from a multiple selection.
     *
     * @returns {Object[]}
     *  An object with all selection objects from a multiple drawing selection that do not describe
     *  drawings of type 'group'.
     */
    this.getAllNoneDrawingGroupsFromMultiSelection = function () {
        return _.filter(multipleSelections, function (selection) {
            return !isGroupDrawingFrame(self.getDrawingNodeFromMultiSelection(selection));
        });
    };

    /**
     * Returns from a specified selection object of a multiple drawing selection
     * the drawing node. If no selection object exists or the drawing node cannot
     * be found, null is returned.
     *
     * @param {Object} selection
     *  The selection object from a multiple drawing selection.
     *
     * @returns {jQuery|Null}
     *  The drawing node belonging to a multiple drawing selection.
     */
    this.getDrawingNodeFromMultiSelection = function (selection) {
        return selection && selection.drawing ? selection.drawing : null;
    };

    /**
     * Returns from a specified selection object of a multiple drawing selection
     * the logical start position. If no selection object exists or the start
     * position cannot be found, null is returned.
     *
     * @param {Object} selection
     *  The selection object from a multiple drawing selection.
     *
     * @returns {Number[]|Null}
     *  The logical start position belonging to a multiple drawing selection.
     */
    this.getStartPositionFromMultiSelection = function (selection) {
        return selection && selection.startPosition ? _.clone(selection.startPosition) : null;
    };

    /**
     * Returns from a specified selection object of a multiple drawing selection
     * the logical end position. If no selection object exists or the start
     * position cannot be found, null is returned.
     *
     * @param {Object} selection
     *  The selection object from a multiple drawing selection.
     *
     * @returns {Number[]|Null}
     *  The logical end position belonging to a multiple drawing selection.
     */
    this.getEndPositionFromMultiSelection = function (selection) {
        return selection && selection.endPosition ? _.clone(selection.endPosition) : null;
    };

    /**
     * Returns from a specified selection object of a multiple drawing selection
     * the drawing type. If no selection object exists or the type cannot be found,
     * an empty string is returned.
     *
     * @param {Object} selection
     *  The selection object from a multiple drawing selection.
     *
     * @returns {String}
     *  The type of a drawing belonging to a multiple drawing selection.
     */
    this.getDrawingTypeFromMultiSelection = function (selection) {
        return selection && selection.type ? selection.type : '';
    };

    /**
     * Clearing the container for the multiple selections and removing the
     * selection boxes around every drawing.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.triggerChange=true]
     *      Whether the 'change' event shall be triggered directly after the
     *      multi selection is cleared.
     */
    this.clearMultiSelection = function (options) {

        // remove drawing selection boxes
        _.each(multipleSelections, function (oneSelection) {
            self.clearDrawingSelection(oneSelection.drawing);
        });

        // empty collector
        multipleSelections = [];

        // informing the listeners
        if (getBooleanOption(options, 'triggerChange', true)) { self.trigger('change'); }
    };

    /**
     * Selecting all drawings of the active slide. This function is triggered
     * by ctrl-A, if this is a 'slide selection'.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.selectAllDrawingsOnSlide = function () {

        var // the slide position
            slidePos = [_.first(self.getStartPosition())],
            // the slide node
            slide = getParagraphElement(self.getRootNode(), slidePos),
            // the container for all drawings on the slide
            allDrawings = $(slide).children(ABSOLUTE_DRAWING_SELECTOR),
            // the drawing position of a single drawing on the slide
            drawingPos = null;

        if (allDrawings.length === 1) {

            // clearing an existing multi drawing selection
            self.clearAllDrawingSelections();

            drawingPos = slidePos.concat(getOxoPosition(slide, allDrawings[0]));
            self.setTextSelection(drawingPos, increaseLastIndex(drawingPos));

        } else if (allDrawings.length > 1) {

            // clearing an existing multi drawing selection
            self.clearAllDrawingSelections();

            _.each(allDrawings, function (oneDrawing) {

                var // the start position of the drawing
                    startPos = getOxoPosition(slide, oneDrawing),
                    // the end position of the drawing
                    endPos = increaseLastIndex(startPos);

                self.drawDrawingSelection(oneDrawing);
                addOneDrawingSelectionIntoMultiSelection({ drawing: $(oneDrawing), startPosition: slidePos.concat(startPos), endPosition: slidePos.concat(endPos) });
            });

        }

        if (allDrawings.length > 0) {
            // bring clipboard node in sync with the currently selected drawings
            self.updateClipboardNode();
            // focus clipboard node and set the browser selection to it's contents
            self.setFocusIntoClipboardNode({ specialTouchHandling: true });
            self.setBrowserSelectionToContents(self.getClipboardNode(), { preserveFocus: true });
        }

        return allDrawings.length;
    };

    /**
     * Creating a multi drawing selection with the specified drawings nodes.
     * All drawings must be located on the same (and the active) slide.
     *
     * Important: The slide containing the specified drawing nodes must
     *            be activated!
     *
     * @param {jQuery|Node[]} allDrawings
     *  A container with all drawings nodes that shall be selected.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.setMultiDrawingSelection = function (allDrawings) {

        var // the slide position
            slidePos = null,
            // the drawing position
            drawingPos = null,
            // the slide node
            slide = null;

        if (allDrawings.length > 0) {

            slide = $(allDrawings[0]).closest(SLIDE_SELECTOR);
            slidePos = docModel.getSlidePositionById(docModel.getSlideId(slide));

            // clearing an existing multi drawing selection
            self.clearAllDrawingSelections();

            if (allDrawings.length === 1) {
                drawingPos = slidePos.concat(getOxoPosition(slide, allDrawings[0]));
                self.setTextSelection(drawingPos, increaseLastIndex(drawingPos));
            } else {
                _.each(allDrawings, function (oneDrawing) {

                    var // the start position of the drawing
                        startPos = getOxoPosition(slide, oneDrawing),
                        // the end position of the drawing
                        endPos = increaseLastIndex(startPos);

                    self.drawDrawingSelection(oneDrawing);
                    addOneDrawingSelectionIntoMultiSelection({ drawing: $(oneDrawing), startPosition: slidePos.concat(startPos), endPosition: slidePos.concat(endPos) });
                });
            }

            // informing the listeners
            self.trigger('change');
        }

        return allDrawings ? allDrawings.length : 0;
    };

    /**
     * Creating a multi drawing selection with the specified logical drawing positions.
     * All drawings must be located on the same (and the active) slide.
     *
     * Important: The slide containing the specified drawing positions must
     *            be activated!
     *
     * @param {Number[][]} allPositions
     *  A container with all logical positions of the drawings nodes that shall be selected.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.setMultiDrawingSelectionByPosition = function (allPositions) {

        var // the slide position
            slidePos = null,
            // the slide node
            slide = null;

        if (allPositions && allPositions.length > 0) {

            slidePos = [_.first(allPositions[0])];
            slide = getParagraphElement(self.getRootNode(), slidePos);

            // clearing an existing multi drawing selection
            self.clearAllDrawingSelections();

            if (allPositions.length === 1) {
                self.setTextSelection(allPositions[0], increaseLastIndex(allPositions[0]));
            } else {
                _.each(allPositions, function (position) {
                    var // the start position of the drawing
                        oneDrawing = getDOMPosition(slide, [_.last(position)], true),
                        // the end position of the drawing
                        endPos = increaseLastIndex(position);

                    if (oneDrawing && oneDrawing.node) {
                        oneDrawing = $(oneDrawing.node);
                        self.drawDrawingSelection(oneDrawing);
                        addOneDrawingSelectionIntoMultiSelection({ drawing: oneDrawing, startPosition: position, endPosition: endPos });
                    }
                });
            }

            // informing the listeners
            self.trigger('change');
        }

        return allPositions ? allPositions.length : 0;
    };

    /**
     * Checking, whether the selection of a specified drawing needs an update of the
     * multiple drawing selection.
     *
     * @param {HTMLElement|jQuery} drawingNode
     *  The drawing element node, that might require an update of multiple
     *  drawing selection.
     *
     * @returns {Boolean}
     *  Whether the selection of the specified drawing needs to trigger an update of the
     *  multiple drawing selection.
     */
    this.checkMultipleSelection = function (drawingNode) {
        // another drawing already selected -> creating, expanding or removing a multiple drawing selection
        return self.isMultiSelection() || anotherDrawingSelected(drawingNode);
    };

    /**
     * Adding one specified drawing node into the multiple drawing selection. This function
     * includes the drawing of the artificial drawing selection, the creation of the new
     * drawing selection object and the addition of this new selection object into the
     * multiple drawing container.
     *
     * @param {HTMLElement|jQuery} drawingNode
     *  The drawing node that needs to be added into the multiple drawing selection.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.addOneDrawingIntoMultipleSelection = function (drawingNode) {

        var // the active slide node
            slide = docModel.getSlideById(docModel.getActiveSlideId()),
            // the logical position of the active slide
            slidePos = docModel.getActiveSlidePosition(),
            // the logical start position of a drawing
            startPos = getOxoPosition(slide, drawingNode),
            // the logical end position of a drawing
            endPos = increaseLastIndex(startPos);

        self.drawDrawingSelection(drawingNode);
        return addOneDrawingSelectionIntoMultiSelection({ drawing: $(drawingNode), startPosition: slidePos.concat(startPos), endPosition: slidePos.concat(endPos) });
    };

    /**
     * Removing one specified drawing node from the multiple drawing selection. This function
     * includes the removal of the artificial drawing selection, the calculation of the logical
     * position of the drawing and the removal of the selection object of this drawing from the
     * multiple drawing container.
     *
     * @param {HTMLElement|jQuery} drawingNode
     *  The drawing node that needs to be removed from the multiple drawing selection.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.removeOneDrawingFromMultipleSelection = function (drawingNode) {

        var // the active slide node
            slide = docModel.getSlideById(docModel.getActiveSlideId()),
            // the logical position of the active slide
            slidePos = docModel.getActiveSlidePosition(),
            // the logical start position of a drawing
            startPos = getOxoPosition(slide, drawingNode);

        clearSelection(drawingNode);
        return removeOneDrawingSelectionFromMultiSelection(slidePos.concat(startPos));
    };

    /**
     * OT: Removing one or more specified drawing nodes from the multiple drawing selection.
     * All the selections that will be removed have a property 'marker' specified.
     *
     * @param {String} marker
     *  The name of the property that is used to mark the selection for removal.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.removeDrawingsFromSelectionByMarker = function (marker) {
        var drawingcount = removeOneDrawingSelectionFromMultiSelectionByMarker(marker);
        if (drawingcount === 1) { checkMultiSelectionState(); }
        return drawingcount;
    };

    /**
     * OT: After an external move operation it might be necessary to sort the modified
     * local multi selection.
     */
    this.sortMultiSelection = function () {
        multipleSelections.sort((sel1, sel2) => comparePositions(sel1.startPosition, sel2.startPosition));
    };

    /**
     * Handling the multiple drawing selection with respect to the specified
     * drawing node. It is possible, that the drawing is already selected, or that
     * it is not selected. The multiple drawing selection needs to be created,
     * expanded, shrinked or removed.
     *
     * @param {HTMLElement|jQuery} drawingNode
     *  The drawing node that triggers an update of the multiple drawing selection.
     */
    this.handleMultipleSelection = function (drawingNode) {

        var // the jQueryfied drawing node
            $drawingNode = $(drawingNode),
            // the currently selected drawing (or additional text frame drawing)
            selectedDrawing = self.getAnyDrawingSelection(),
            // the logical start and end position of a drawing
            startPos = null, endPos = null,
            // whether the clipboard node should be focused and the browser selection set to it's contents
            focusClipboard = true;

        // handling the group, not a grouped drawing node
        if (isGroupedDrawingFrame(drawingNode)) {
            $drawingNode = getFarthestGroupNode($drawingNode.closest(SLIDE_SELECTOR), drawingNode);
        }

        if (self.isMultiSelection()) {

            // checking, if the specified drawing is already in the selection
            if ($drawingNode.hasClass('selected')) {

                self.removeOneDrawingFromMultipleSelection($drawingNode);

                if (self.getMultiSelectionCount() === 1) {

                    // saving the selection values of the remaining selected drawing
                    startPos = _.clone(multipleSelections[0].startPosition);
                    endPos = _.clone(multipleSelections[0].endPosition);

                    // clearing multiple selection, setting single drawing selection
                    self.clearMultiSelection();

                    self.setTextSelection(startPos, endPos);

                    focusClipboard = false;
                }

            } else {
                self.addOneDrawingIntoMultipleSelection($drawingNode);
            }

        } else {

            // checking, if the specified drawing is already in the selection
            if ($drawingNode.hasClass('selected')) {
                // the one drawing is already selected
                // -> removing the selection
                self.clearAllDrawingSelections();

                // if the drawing was not a text frame, the selection needs to be set into the slide
                if (!self.isAdditionalTextframeSelection()) {
                    if (slideMode) { self.setSlideSelection({ triggerChange: false }); }
                }

            } else {

                // expanding the selection and creating a multiple selection

                // removing all kind of drawing selections
                self.clearAllDrawingSelections();

                // filling two drawings into multiple selection
                self.addOneDrawingIntoMultipleSelection(selectedDrawing);
                self.addOneDrawingIntoMultipleSelection($drawingNode);
            }
        }

        // bring clipboard node in sync with the currently selected drawings
        self.updateClipboardNode();
        // focus clipboard node and set the browser selection to it's contents
        if (focusClipboard) {
            self.setFocusIntoClipboardNode();
            self.setBrowserSelectionToContents(self.getClipboardNode(), { preserveFocus: true });
        }

        // informing the listeners about the new selection
        self.trigger('change');
    };

    /**
     * The start handler for the selection box. If the user clicked into the 'div.slide' node
     * or into the 'app-content-root node', the complete selection must be removed.
     * For the selection box, there is a filter set (PresentationModel.MULTI_SELECTION_BOX_FILTER),
     * so that only events on these nodes cause this function to be executed.
     */
    this.selectionStartHandler = function () {

        // removing any selection, if the user clicked on 'div.slide' or in the 'app-content-root' node.
        // -> setting the 'slide selection' ([3,0] for the slide at position [3]).
        // A trigger of 'change' is not required in start handler, because it can be done in the
        // stop handler.
        if (slideMode) { self.setSlideSelection({ triggerChange: false }); }

        // additionally clear a possible multi selection in the slide pane
        docModel.trigger('slidepane:clearselection');

        // set the virtual focus to the document
        app.getView().setVirtualFocusSlidepane(false);
    };

    /**
     * Selecting all drawings that are located inside a specified rectangle.
     *
     * @param {Object} box
     *  The object describing the rectangle with the properties top, left, width and height.
     *
     * @returns {Number}
     *  The number of selected drawings.
     */
    this.selectAllDrawingsInSelectionBox = function (box) {

        // selecting all drawings on the active slide, that are completely inside
        // the selection box.

        var // the active slide node
            slide = docModel.getSlideById(docModel.getActiveSlideId()),
            // the container for all drawings on the slide (slide must be defined, 56352)
            allDrawings = slide ? slide.children(ABSOLUTE_DRAWING_SELECTOR) : $(),
            // the logical position of the active slide
            slidePos = docModel.getActiveSlidePosition(),
            // a collector for all selected drawings
            allSelectedDrawings = [],
            // the content root node, the base for the position calculation
            contentRootNode = app.getView().getContentRootNode(),
            // the position of the content root node
            rootNodePos = contentRootNode.offset(),
            // the horizontal scroll shift
            scrollLeft = contentRootNode.scrollLeft(),
            // the vertical scroll shift
            scrollTop = contentRootNode.scrollTop(),
            // whether the trigger for the new selection is required
            doTrigger = true,
            // the logical start and end position of a drawing
            startPos = null, endPos = null,
            // whether the clipboard node should be focused and the browser selection set to it's contents
            focusClipboard = true;

        // a helper function to check, if the drawing is completely
        // inside the specified selection box.
        function nodeIsInsideSelectionBox(drawing) {

            var // the position object
                pos = drawing.getBoundingClientRect(),
                // the left position of the drawing
                drawingLeft = pos.left + scrollLeft - rootNodePos.left,
                // the top position of the drawing
                drawingTop = pos.top + scrollTop - rootNodePos.top,
                // the width of the drawing
                width = pos.width,
                // the height of the drawing
                height = pos.height;

            return (box.left <= drawingLeft) &&
                (drawingLeft + width <= box.left + box.width) &&
                (box.top <= drawingTop) &&
                (drawingTop + height <= box.top + box.height);
        }

        // Setting the focus into the clipboard node.
        // This step is necessary to set the focus correctly (into the clip-board), so that a
        // following keydown (like Ctrl-A) will be handled correctly.
        self.setFocusIntoClipboardNode({ specialTouchHandling: true });

        if (allDrawings.length > 0) {

            _.each(allDrawings, function (oneDrawing) {
                if (nodeIsInsideSelectionBox(oneDrawing)) {
                    allSelectedDrawings.push(oneDrawing);
                }
            });

            if (allSelectedDrawings.length > 1) {
                _.each(allSelectedDrawings, function (oneDrawing) {
                    self.addOneDrawingIntoMultipleSelection(oneDrawing);
                });
            } else if (allSelectedDrawings.length === 1) {

                startPos = getOxoPosition(slide, allSelectedDrawings[0]);
                endPos = increaseLastIndex(startPos);

                self.setTextSelection(slidePos.concat(startPos), slidePos.concat(endPos));
                doTrigger = false;
                focusClipboard = false;

            } else {
                // no drawing in selection box -> removing selections from drawings required.
                // The new selection was already set in 'self.selectionStartHandler()' (but not triggered)
                doTrigger = true;
            }

        } else {
            doTrigger = false;
            focusClipboard = false;
        }

        // bring clipboard node in sync with the currently selected drawings
        self.updateClipboardNode();
        // focus clipboard node and set the browser selection to it's contents
        if (focusClipboard) {
            self.setFocusIntoClipboardNode({ specialTouchHandling: true });
            self.setBrowserSelectionToContents(self.getClipboardNode(), { preserveFocus: true });
        }

        // informing the listeners about the new selection
        if (doTrigger) { self.trigger('change'); }

        return allSelectedDrawings.length;
    };

    /**
     * Getting an array containing all logical start or end positions of the
     * multiple selection. If there is no multi selection, an empty array is
     * returned.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.start=true]
     *      Whether the logical start or the end positions shall be returned
     *      in the array.
     *
     * @returns {Number[][]|[]}
     *  An array with all logical start or end positions. Or an empty array,
     *  if there is no multi selection.
     */
    this.getArrayOfLogicalPositions = function (options) {

        var // whether the list of start or end positions shall be returned
            useStart = getBooleanOption(options, 'start', true),
            // the container with all logical positions
            allPositions = [];

        _.each(multipleSelections, function (oneSelection) {
            allPositions.push(useStart ? _.clone(oneSelection.startPosition) : _.clone(oneSelection.endPosition));
        });

        return allPositions;
    };

    /**
     * Getting an array containing all logical start and(!) end positions of the
     * multiple selection or any single selection. The array contains objects that
     * contain the keys 'startPosition' and 'endPosition'.
     * The given logical position can be any logical position. It is not always
     * a drawing or a multi drawing selection.
     *
     * @returns {Object[]}
     *  An array with all logical start and end positions.
     */
    this.getAllLogicalPositions = function () {

        var // the container with all logical positions
            allPositions = [];

        if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
            _.each(multipleSelections, function (oneSelection) {
                allPositions.push({ startPosition: _.clone(oneSelection.startPosition), endPosition: _.clone(oneSelection.endPosition) });
            });
        } else {
            allPositions.push({ startPosition: self.getStartPosition(), endPosition: self.getEndPosition() });
        }

        return allPositions;
    };

    /**
     * Getting an array containing all drawing nodes of the multiple selection.
     * The drawing nodes are sorted corresponding to their logical positions.
     *
     * @returns {jQuery[]}
     *  An array with all jQuerified drawing nodes.
     */
    this.getArrayOfSelectedDrawingNodes = function () {

        var // the container with all jQuerified drawing nodes
            allNodes = [];

        _.each(multipleSelections, function (oneSelection) {
            allNodes.push(oneSelection.drawing);
        });

        return allNodes;
    };

    /**
     * Getting the first drawing node of a multiple selection.
     *
     * @returns {jQuery}
     *  The first drawing node of a multiple selection.
     */
    this.getFirstSelectedDrawingNode = function () {
        return multipleSelections[0].drawing;
    };

    /**
     * Getting a string containing all logical start or end positions of the
     * multiple selection.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.start=true]
     *      Whether the logical start or the end positions shall be converted
     *      into a string.
     *
     * @returns {String}
     *  A string with all logical start or end positions.
     */
    this.getListOfPositions = function (options) {

        var // whether the list of start or end positions shall be returned
            useStart = getBooleanOption(options, 'start', true),
            // the string with all logical positions
            posString = '';

        _.each(multipleSelections, function (oneSelection) {
            var pos = useStart ? oneSelection.startPosition : oneSelection.endPosition;
            posString += '(' + pos.join(',') + '),';

        });

        return posString.substring(0, posString.length - 1);
    };

    /**
     * Whether the drawings in the multi selection can be grouped.
     *
     * @returns {Boolean}
     *  Returns whether the drawings in the multi selection can be grouped.
     */
    this.isGroupableSelection = function () {

        if (!(self.isMultiSelectionSupported() && self.isMultiSelection())) { return false; }

        if (!slideMode) { return true; }

        return !docModel.isAtLeastOneNotGroupableDrawingInSelection();
    };

    /**
     * Whether the selection contains a drawing of type 'group', so that it is possible to
     * 'ungroup' the drawing.
     *
     * @returns {Boolean}
     *  Returns, whether the selection contains a drawing of type 'group', so that it is
     *  possible to 'ungroup' the drawing.
     */
    this.isUngroupableSelection = function () {
        return self.isAnyDrawingGroupSelection() || self.isAtLeastOneGroupInMultiSelection();
    };

    /**
     * Getting an array containing all selected drawings.
     *
     * @param {Object} [options]
     *  An object containing some additional properties:
     *  @param {Boolean} [options.includeTextFrameSelection=true]
     *      Whether additional text frame selections shall be handled like selected drawings. In this case
     *      the selection is a text selection, and the surrounding drawing is 'additionally' selected. If
     *      this option is set to false, only 'pure' drawing selections (single drawing and multi drawing
     *      selection (if supported)) are returned.
     *  @param {Boolean} [options.forceArray=false]
     *      If set to true and no drawing is selected, an empty array is returned instead of null.
     *
     * @returns {jQuery[]|[]|Null}
     *  An array with all drawing nodes in the selection. This can be a single drawing selection or a multi
     *  drawing selection. If no drawing is selected, null is returned.
     */
    this.getSelectedDrawings = function (options) {

        if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
            return self.getAllSelectedDrawingNodes();
        } else if (self.isDrawingSelection()) {
            return [self.getSelectedDrawing()];
        } else if (self.isAdditionalTextframeSelection() && getBooleanOption(options, 'includeTextFrameSelection', true)) {
            return [self.getSelectedTextFrameDrawing()];
        } else {
            return getBooleanOption(options, 'forceArray', false) ? [] : null;
        }
    };

    /**
     * Getting an object containing the types of all selected drawings.
     * If no drawing is selected, null is returned.
     *
     * @param {Object} [options]
     *  An object containing some additional properties:
     *  @param {Boolean} [options.includeTextFrameSelection=true]
     *      Whether additional text frame selections shall be handled like selected drawings. In this case
     *      the selection is a text selection, and the surrounding drawing is 'additionally' selected. If
     *      this option is set to false, only 'pure' drawing selections (single drawing and multi drawing
     *      selection (if supported)) are returned.
     *
     * @returns {Set<DrawingType>}
     *  A set with the types of all selected drawings.
     */
    this.getSelectedDrawingsTypes = function (options) {

        var allTypes = new Set();

        // TODO: Handling of drawings inside selected groups (via option)!?

        if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
            for (const drawingSelection of multipleSelections) {
                allTypes.add(self.getDrawingTypeFromMultiSelection(drawingSelection));
            }
        } else if (self.isDrawingSelection()) {
            allTypes.add(getDrawingType(self.getSelectedDrawing()));
        } else if (self.isAdditionalTextframeSelection() && getBooleanOption(options, 'includeTextFrameSelection', true)) {
            allTypes.add(getDrawingType(self.getSelectedTextFrameDrawing()));
        }

        return allTypes;
    };

    /**
     * Convenience function that supports the selection object of a multi selection also
     * for single drawing selection or an additional text frame seletion.
     * If this is a drawing selection (one drawing or multi drawing selection), this function
     * creates a container that can be used like the container with drawing selections from a
     * multiple drawing selection. This makes is superfluous to distinguish between single
     * drawing selections and multiple drawing selections.
     *
     * Info: In the scenario of a multi selection, this function returns an array with objects,
     *       that contain the logical positions for the start and the end of each drawing.
     *       If this positions are used for example for the creation of operations, they must
     *       be cloned before usage (TODO).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.includeTextFrameSelection=true]
     *      Whether additional text frame selections shall be handled like selected drawings. In this case
     *      the selection is a text selection, and the surrounding drawing is 'additionally' selected. If
     *      this option is set to false, only 'pure' drawing selections (single drawing and multi drawing
     *      selection (if supported)) are returned.
     *  @param {Boolean} [options.forceArray=true]
     *      If set to false and no drawing is selected, null is returned instead of an empty array.
     *
     * @returns {Object[]|Null}
     *  An array containing a selection object for multi and single drawing selections.
     *  If no drawing is selected or this is an additional textframe selection and the option
     *  'includeTextFrameSelection' is set to false, an empty array or Null is returned.
     */
    this.getSelectedDrawingsInfoObject = function (options) {

        if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
            return self.getMultiSelection();
        } else if (self.isDrawingSelection()) {
            return [{ drawing: self.getSelectedDrawing(), startPosition: self.getStartPosition(), endPosition: self.getEndPosition() }];
        } else if (self.isAdditionalTextframeSelection() && getBooleanOption(options, 'includeTextFrameSelection', true)) {
            var drawing = self.getSelectedTextFrameDrawing();
            var drawingPos = getOxoPosition(self.getRootNode(), drawing);
            return [{ drawing, startPosition: drawingPos, endPosition: increaseLastIndex(drawingPos) }];
        } else {
            return getBooleanOption(options, 'forceArray', true) ? [] : null;
        }
    };

    /**
     * Finding the drawing node that belongs to a specified drawing selection node.
     * If only the drawing selection node in the selection overlay node is available,
     * this function returns the corresponding drawing node in the document.
     * If the specified drawing node is not inside the selection overlay node, but
     * is a valid drawing node, it is returnred without changes.
     * If no corresponding drawing node can be found or if the specified node is no
     * drawing node at all, null is returned.
     *
     * @returns {jQuery|null}
     *  The drawing node belonging to the specified selection drawing node. Or the
     *  drawing node itself, if the specified node was no drawing selection node.
     *  Or null, if no valid drawing node could be found belonging to the drawing
     *  selection node.
     */
    this.getDrawingNodeFromDrawingSelection = function (drawingNode) {

        // a drawing node
        var drawing = null;
        // a selected drawing node
        var selectedDrawing = null;

        // helper function to compare the specifed drawing selection node with the selection node of one specified drawing
        function isSameDrawingSelection(oneDrawing) {
            return oneDrawing.data('selection') && getDomNode(drawingNode) === getDomNode(oneDrawing.data('selection'));
        }

        if (drawingNode.hasClass('selectiondrawing')) {
            if (self.isMultiSelectionSupported() && self.isMultiSelection()) {
                drawing = _.find(self.getAllSelectedDrawingNodes(), isSameDrawingSelection);
            } else if (self.isAnyDrawingSelection()) {
                selectedDrawing = self.getAnyDrawingSelection();
                if (isSameDrawingSelection(selectedDrawing)) {
                    drawing = selectedDrawing; // the drawing node, not the drawing selection overlay node
                }
            }
        } else if (isDrawingFrame(drawingNode)) {
            drawing = drawingNode;
        }

        return drawing;
    };

    // initialization -----------------------------------------------------

    this.getDrawingType = function (drawing) {
        return getDrawingType(drawing);
    };
}
