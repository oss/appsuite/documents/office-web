/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { Position } from "@/io.ox/office/editframework/utils/operations";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";
import { Point } from "@/io.ox/office/textframework/utils/dom";

// types ======================================================================

/**
 * Optional parameters for the function `restoreBrowserSelection()`.
 */
interface RestoreBrowserSelectionOptions {

    /**
     * If set to `true`, the DOM element currently focused will be focused
     * again after the browser selection has been set. Note that doing so will
     * immediately lose the new browser selection, if focus is currently inside
     * a text input element. Default value is `false`.
     */
    preserveFocus?: boolean;

    /**
     * If set to `true`, the DOM element currently focused will not lose the
     * focus. This is especially important for concurrent editing, so that for
     * example currently open controls are not closed by an externally forced
     * selection change. Default value is `false`.
     */
    keepFocus?: boolean;

    /**
     * If set to `true`, the browser selection gets also set in read only mode
     * which otherwise would be prevented. This value is evaluated in function
     * `Selection::setBrowserSelection`. Default value is `false`.
     */
    forceWhenReadOnly?: boolean;
}

interface PreserveFocusOptions {

    /**
     * Whether the focus should be preserved when setting a text selection.
     * Default value is `true`.
     */
    preserveFocus?: boolean;
}

/**
 * Optional parameters for the function `Selection#setTextSelection`.
 */
interface SetTextSelectionOptions extends PreserveFocusOptions {

    /**
     * If set to `true`, the selection will be created backwards. Note that not
     * all browsers allow to create a reverse DOM browser selection.
     * Nonetheless, the internal selection direction will always be set to
     * backwards. Default value is `false`.
     */
    backwards?: boolean;

    /**
     * If set to `true`, the selection will be set using newStartPosition and
     * newEndPosition directly. The logical positions are not converted to a
     * browserSelection and then calculated back in applyBrowserSelection using
     * `Position.getTextLevelOxoPosition`. This is especially important for the
     * very fast operations like 'insertText' (also used for 'Enter', 'Delete'
     * and 'Backspace'). Default value is `false`.
     */
    simpleTextSelection?: boolean;

    /**
     * If set to `true`, this setTextSelection is called from an insert
     * operation, for example an insertText operation. This is important for
     * performance reasons, because the cursor setting can be simplified.
     * Default value is `false`.
     */
    insertOperation?: boolean;

    /**
     * If set to `true`, this setTextSelection is called from an 'Enter'
     * operation. This is important for performance reasons, because the cursor
     * setting can be simplified. Default value is `false`.
     */
    splitOperation?: boolean;
}

/**
 * The paragraph cache, that is used for performance reasons
 */
export interface ParagraphCache {
    node: NodeOrJQuery;
    pos?: number;
    offset: number;
}

/**
 * Specifies the direction of a text selection.
 * - "cursor": Cursor selection, start and end position are equal.
 * - "forwards": Range selection, anchor position precedes focus position.
 * - "backwards": Range selection, anchor position follows focus position.
 */
export type SelectionDirection = "cursor" | "forwards" | "backwards";

/**
 * Specifies the type of a text selection.
 * - "text": Top-level text contents.
 * - "cell": Entire cells in a table.
 * - "drawing": Drawing objects selected directly.
 */
export type SelectionType = "text" | "cell" | "drawing";

export interface SelectionChangeEvent {
    event?: Event;
    noscroll?: boolean;
    insertOperation?: boolean;
    splitOperation?: boolean;
    simpleTextSelection?: boolean;
    keepChangeTrackPopup?: boolean;
}

export interface SelectionUpdateEvent {

    /**
     * The optional browser event.
     */
    event?: Event;

    /**
     * Whether the selection is a simple text selection.
     */
    simpleTextSelection?: boolean;

    /**
     * Whether the selection is a slide selection.
     */
    slideSelection?: boolean;
}

export type SelectionPositionCalculatedFn = (position: Position, options: { start: boolean; selectionExternallyModified: boolean }) => void;

export interface SelectionPositionCalculatedOptions {
    handlePlaceHolderComplexField: boolean;
}

export interface ChangeCursorInfoEvent {
    start: Position;
    end: Position;
    rootNode: JQuery;
    target: string;
}

/**
 * Type mapping for the events emitted by `Selection` instances.
 */
export interface SelectionEventMap {

    /**
     * Will be emitted after the selection has changed.
     */
    change: [event?: SelectionChangeEvent];

    /**
     * Will be emitted after the selection has updated (triggers also when
     * selection hasn't changed). Used for example to open popups depending on
     * cursor position (like field format popup).
     */
    update: [event: SelectionUpdateEvent];

    /**
     * Will be emitted after the new logical start and end positions have been
     * calculated.
     */
    "position:calculated": [updateFn: SelectionPositionCalculatedFn, options: SelectionPositionCalculatedOptions];

    /**
     * Will be emitted after the selection has changes (used only for Android)
     */
    changecursorinfo: [event: ChangeCursorInfoEvent];

    /**
     * Will be emitted when the new position is a text position inside a text
     * frame.
     */
    "docs:textframe:enter": [textFrame: HTMLElement];

    /**
     * Will be emitted when the new position is no longer a text position
     * inside the previously selected text frame.
     */
    "docs:textframe:leave": [textFrame: HTMLElement];
}

/**
 * An object with the properties "startPosition" and "endPosition" for the logical
 * positions of the selection.
 */
export interface LogicalPosition {
    startPosition: Position;
    endPosition: Position;
}

/**
 * An object with the properties "position" and "selectedText" for a paragraph
 * inside a selection range.
 */
export interface SelectedParagraphsInfo {

    /**
     * The logical position of the paragraph.
     */
    position: Position;

    /**
     * The startOffset of the selection in the paragraph.
     * If "undefined", the selection does not start in this paragraph.
     */
    startOffset: number | undefined;

    /**
     * The endOffset of the selection in the paragraph.
     * If "undefined", the selection does not end in this paragraph.
     */
    endOffset: number | undefined;

    /**
     * Whether the paragraph is empty and contains no content.
     */
    isEmpty: boolean;

    /**
     * Whether the selected text inside this paragraph can be translated.
     */
    isTranslatable?: boolean;

    /**
     * The translation of the selected text.
     */
    translation?: string;

    /**
     * The selected text inside this paragraph.
     */
    selectedText: string;

    /**
     * An array with all languages used as character attributes inside the
     * paragraph.
     */
    languages: string[];

    /**
     * An array with the logical positions of all drawings inside a paragraph.
     * If there are no drawings inside the paragraph, the value is "null".
     */
    drawings: Opt<number[]>;
}

// class Selection ============================================================

export default class Selection extends ModelObject<TextBaseModel, SelectionEventMap> {

    constructor(docModel: TextBaseModel);

    clearMultiSelection(): void;
    drawDrawingSelection(node: NodeOrJQuery): void;
    getAllLogicalPositions(): LogicalPosition[];
    getAnchorPosition(): Position;
    getDirection(): SelectionDirection;
    getEnclosingParagraph(options?: { allowSlide?: boolean }): HTMLElement | null;
    getEndPosition(): Position;
    getFocusPosition(): Position;
    getInsertTextPoint(): Point;
    getParagraphCache(): ParagraphCache | null;
    getRootNode(): NodeOrJQuery;
    getRootNodeTarget(): string;
    getSelectedDrawing(): JQuery;
    getSelectedText(): string | null;
    getStructuredSelectedText(): { selectedText: string; selectedParagraphs: SelectedParagraphsInfo[]; allLanguagesInSelection: string[] };
    getSelectedTextFrameDrawing(): JQuery;
    getSelectionType(): SelectionType;
    getStartPosition(): Position;
    hasRange(): boolean;
    isAdditionalTextframeSelection(): boolean;
    isBackwards(): boolean;
    isCellSelection(): boolean;
    isDrawingSelection(): boolean;
    isTextFrameSelection(): boolean;
    isTextCursor(): boolean;
    isTextSelection(): boolean;
    isValid(): boolean;
    iterateNodes(iterator: (node: HTMLElement, pos: Position, start: number, length: number) => void, context?: unknown): void; // TODO: Precision can be improved
    restoreBrowserSelection(options?: RestoreBrowserSelectionOptions): void;
    setEmptySelection(options?: PreserveFocusOptions): void;
    setMultiDrawingSelectionByPosition(positions: Position[]): void;
    setNewRootNode(node: NodeOrJQuery): void;
    setTextSelection(startPosition: Position, endPosition?: Position, options?: SetTextSelectionOptions): void;
    switchToValidRootNode(node: NodeOrJQuery): void;
}
