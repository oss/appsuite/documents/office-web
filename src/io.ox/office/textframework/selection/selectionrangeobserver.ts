/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { getFocus } from "@/io.ox/office/tk/dom";
import { EObject } from "@/io.ox/office/tk/objects";
import ErrorCode from "@/io.ox/office/baseframework/utils/errorcode";
import { ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR } from "@/io.ox/office/baseframework/utils/clienterror";
import type { TextBaseApplication } from "@/io.ox/office/textframework/app/application";
import { PAGE_NODE_SELECTOR, COMMENT_EDITOR_SELECTOR } from "@/io.ox/office/textframework/utils/dom";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";
import type Selection from "@/io.ox/office/textframework/selection/selection";

// types ======================================================================

/**
 * Configuration for a simplified mouse event that contains only the type property.
 */
interface SimplifiedMouseEvent {

    /**
     * The type of event.
     */
    type: string;
}

// private functions ==========================================================

/**
 * Helper function to determine wheter the focus is currently inside the page node.
 */
function focusIsInsidePage(): boolean {
    const focusNode = getFocus();
    return focusNode ? ($(focusNode).closest(PAGE_NODE_SELECTOR).length > 0) : false;
}

/**
 * Helper function to determine wheter the focus is currently inside a valid position
 * outside the page. This can be the comment editor or a form control.
 */
function isValidFocusOutsidePage(): boolean {
    const focusNode = getFocus();
    return focusNode ? ($(focusNode).closest(COMMENT_EDITOR_SELECTOR).length > 0 || $(focusNode).is("input.form-control")) : false;
}

// class SelectionRangeObserver ===============================================

/**
 * Represents and conserves a specific state of a sheet model.
 */
export class SelectionRangeObserver extends EObject {

    // properties -------------------------------------------------------------

    readonly #docApp: TextBaseApplication;
    readonly #docModel: TextBaseModel;
    readonly #selection: Selection;
    readonly #officeMainWindowNode: JQuery; // the node with the class "io-ox-office-main"
    #selectionWatchTimer: Opt<Abortable>;
    #secondRunTimer: Opt<Abortable>;
    #observerFocusNode: Element | null = null;
    #isActiveMouseDown = false;

    // constructor ------------------------------------------------------------

    constructor(textBaseApp: TextBaseApplication) {
        super();

        this.#docApp = textBaseApp;
        this.#docModel = textBaseApp.docModel;
        this.#selection = this.#docModel.getSelection();

        this.#officeMainWindowNode = this.#docApp.getWindowNode();

        this.listenTo(this.#selection, "change", this.#observeSelectionRange);
    }

    protected override destructor(): void {
        this.#stopSelectionWatch();
        super.destructor();
    }

    // private methods --------------------------------------------------------

    /**
     * After a selection change it might be required to observe the existing
     * selection range. It might be modified by non-supported DOM manipulations
     * (DOCS-2978, DOCS-2981)
     */
    #observeSelectionRange(): void {
        this.#stopSelectionWatch();
        if (this.#selection.isTextSelection() && this.#selection.hasRange() && focusIsInsidePage()) {
            this.#startSelectionWatch();
        }
    }

    /**
     * Start to observe an existing selection range.
     */
    #startSelectionWatch(): void {
        // checking every second, if the selection is still valid and corresponds to the saved browser selection
        if (!this.#selectionWatchTimer) {
            this.#observerFocusNode = getFocus();
            this.#registerGlobalSelectionRestoreHandler();
            this.#selectionWatchTimer = this.setInterval(this.#watchSelectionRepeated, 1000);
        }
    }

    /**
     * Stop to observe an existing selection range.
     */
    #stopSelectionWatch(): void {
        if (this.#selectionWatchTimer) {
            this.#selectionWatchTimer.abort();
            this.#selectionWatchTimer = undefined;
            this.#secondRunTimer?.abort();
            this.#secondRunTimer = undefined;
            this.#setActiveMouseDown({ type: "mouseup" }); // setting 'isActiveMouseDown' to false
            this.#deregisterGlobalSelectionRestoreHandler();
        }
    }

    /**
     * After a mouseup event, it might be necessary to restore the browser selection.
     * This is especially required on Safari browser (DOCS-3000, DOCS-3002).
     * Not 'stealing' the browser selection from another valid focus position.
     */
    #checkBrowserRangeSelection(): void {
        const browserSelection = window.getSelection();
        if (browserSelection?.isCollapsed && this.#selection.isTextSelection() && this.#selection.hasRange() && !isValidFocusOutsidePage()) {
            this.#selection.restoreBrowserSelection();
        }
    }

    /**
     * Check, whether this is an active mousedown without mouseup.
     * This is especially required on Safari browser (DOCS-3000, DOCS-3002).
     */
    #setActiveMouseDown(event: MouseEvent | SimplifiedMouseEvent): void {
        this.#isActiveMouseDown = event.type === "mousedown";
    }

    /**
     * Registering the handler functions for mousedown and mouseup to avoid reload by accident.
     * This is especially required on Safari browser (DOCS-3000, DOCS-3002).
     */
    #registerGlobalSelectionRestoreHandler(): void {
        this.listenTo(this.#officeMainWindowNode, "mousedown mouseup", this.#setActiveMouseDown);
        this.listenTo(this.#officeMainWindowNode, "mouseup", this.#checkBrowserRangeSelection);
    }

    /**
     * Deregistering the handler functions for mousedown and mouseup to avoid reload by accident.
     * This is especially required on Safari browser (DOCS-3000, DOCS-3002).
     */
    #deregisterGlobalSelectionRestoreHandler(): void {
        this.stopListeningTo(this.#officeMainWindowNode, "mousedown mouseup", this.#setActiveMouseDown);
        this.stopListeningTo(this.#officeMainWindowNode, "mouseup", this.#checkBrowserRangeSelection);
    }

    /**
     * Checking, if the text selection range still corresponds with the browser selection.
     * The browser selection might be collapsed, if the focus was changed in the meantime.
     */
    #watchSelectionRepeated(): void {
        const browserSelection = window.getSelection();
        if (browserSelection?.isCollapsed && !this.#isActiveMouseDown && this.#selection.isTextSelection() && this.#selection.hasRange() && getFocus() === this.#observerFocusNode) {
            if (!this.#secondRunTimer) {
                this.#secondRunTimer = this.setTimeout(this.#watchSelectionRepeated, 200); // the error has to occur twice to avoid false alarm (especially when the focus changes)
            } else {
                this.#stopSelectionWatch();
                this.#selection.setTextSelection(this.#selection.getStartPosition()); // avoiding to start with selection range after reload
                this.#docApp.trigger("docs:reload", new ErrorCode(ERROR_CLIENT_INVALID_DOM_MANIPULATION_ERROR));
            }
        } else if (this.#secondRunTimer) {
            this.#secondRunTimer.abort();
            this.#secondRunTimer = undefined;
        }
    }

}
