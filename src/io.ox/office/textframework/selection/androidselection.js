/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { getFocus, setFocus, setInputSelection, KeyCode, hasKeyCode, isIMEInputKey, isShiftKey, Font } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { getCSSRectangleFromPoints, getWindowHeight, getScreenHeight } from '@/io.ox/office/textframework/utils/textutils';
import { DELETE, PARA_INSERT, TEXT_INSERT } from '@/io.ox/office/textframework/utils/operations';
import { getDOMPosition, getWordSelection, getWordBoundEndChars, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { PARAGRAPH_NODE_SELECTOR, isCommentTextframeNode, isCursorKey, isEmptyParagraph, isImplicitParagraphNode, isRangeMarkerNode,
    isSubstitutionPlaceHolderNode, isTextSpan, splitTextSpan } from '@/io.ox/office/textframework/utils/dom';

// constants ==============================================================

var NBSP = '\xa0';
var DUMMYLEN = 50;

//first letter is workaround for Camel-Case-Behavior on some devices (Bug 40066)
var INITTEXT = 'G' + ' '.repeat(DUMMYLEN - 1);

var ENDCHARS = getWordBoundEndChars();

var MONO_FONT = new Font('monospace', 20); // size in points
var LETTER_WIDTH = MONO_FONT.getTextWidth('A');

// mix-in class AndroidSelection ==========================================

function AndroidSelection(docModel, updateLogicalPosition) {

    var self = this;

    // the application instance
    var app = docModel.getApp();

    // slide mode for spreadsheet/presentation
    var slideMode = docModel.useSlideMode();

    var positionOx = null;

    var wordFrom = null;
    var wordTo = null;
    var word = null;

    var div = $('<div class="edit-overlay">');
    var textarea = null;
    var caret = null;
    var highlight = null;
    var singleArray = [0];

    var restoreFN = $.noop;

    // private methods ----------------------------------------------------

    function updateHighlight(from, to) {
        var rootNode = docModel.getSelection().getRootNode();
        var point1 = getDOMPosition(rootNode, from);
        var point2 = getDOMPosition(rootNode, to);

        if (!point1) {
            globalLogger.warn('AndroidSelection: updateHighlight, missing DOM point for position, from=' + JSON.stringify(from));
            return;
        }

        var imeRect = getRectangleFromPoints(point1, point2);

        highlight.css({
            top: imeRect.top + imeRect.height + 1,
            left: imeRect.left,
            width: imeRect.width
        });

    }
    function isSoftKeyboardOpen() {
        return (getWindowHeight() / getScreenHeight()) < 0.7;
    }

    function getRectangleFromPoints(point1, point2) {
        // the root node of the document
        var rootNode = docModel.getNode();

        var offsetTop = parseFloat(rootNode.css('margin-top'));
        // use only top because page transform-origin is "center top"

        var rect = getCSSRectangleFromPoints(point1, point2, rootNode, 1, true);
        rect.left +=  (app.getView().getZoomFactor() < 0.99 ? 3 : 0); // magic number '3' is needed because of 'recalculateDocumentMargin' in presentation/view.js
        rect.top += offsetTop;
        return rect;
    }

    function splitTextSpanLocal(para, pos) {
        singleArray[0] = _.last(pos);

        if (singleArray[0] === 0) {
            var firstSpan = para.children().first();
            if (!firstSpan.is('span') || !firstSpan[0].innerHTML.length) {
                return firstSpan[0];
            } else {
                var newSpan = firstSpan.clone(true);
                fillTextInSpan(newSpan[0], '');
                para.prepend(newSpan);
                return newSpan[0];
            }
        }

        var domPos = getDOMPosition(para[0], singleArray);
        if (!domPos) {
            globalLogger.error('no result', para.children().length, pos, para[0]);
            return null;
        }

        if (domPos.node.nodeValue.length === domPos.offset) {
            return domPos.node.parentElement;
        } else {
            return splitTextSpan(domPos.node.parentElement, domPos.offset)[0];
        }
    }

    function fillTextInSpan(span, text) {
        span.innerHTML = '';
        span.appendChild(document.createTextNode(text));
    }

    function changeSelection(selectionInfo) {

        var start = _.clone(selectionInfo.start);
        var end = _.clone(selectionInfo.end);
        // the selection object
        var selection = docModel.getSelection();

        positionOx = null;

        // var visibility = 'hidden';
        var imeVis = 'hidden';

        if (app.isEditable()) {

            var selType = window.getSelection().type;
            if (!selType || selType === 'None') {
                selType = isSoftKeyboardOpen();
            }

            if (_.isEqual(start, end) && selType) {

                var oldPara = $(getDOMPosition(selectionInfo.rootNode, _.initial(start)).node);
                var wordSelection = getWordSelection(oldPara, _.last(start), ENDCHARS, { fillPlaceholders: true });
                if (wordSelection) {
                    var i = 0;
                    for (i = 0; i < wordSelection.text.length; i++) {
                        if (wordSelection.text[0] === ' ') {
                            wordSelection.text = wordSelection.text.substring(1, wordSelection.text.length);
                            wordSelection.start++;
                        } else {
                            break;
                        }
                    }
                    for (i = wordSelection.text.length - 1; i >= 0; i--) {
                        if (wordSelection.text[wordSelection.text.length - 1] === ' ') {
                            wordSelection.text = wordSelection.text.substring(0, wordSelection.text.length - 1);
                            wordSelection.end--;
                        } else {
                            break;
                        }
                    }

                    wordSelection.text = wordSelection.text.replace(/\s/g, '\u200B');

                    wordFrom = _.clone(start);
                    wordFrom[wordFrom.length - 1] = wordSelection.start;

                    wordTo = _.clone(start);
                    wordTo[wordTo.length - 1] = wordSelection.end;

                    if (_.last(wordFrom) < _.last(wordTo)) {
                        word = wordSelection.text;

                        imeVis = 'visible';
                        updateHighlight(wordFrom, wordTo);
                    } else {
                        wordFrom = _.clone(start);
                        wordTo =  _.clone(start);
                        word = '';
                    }
                } else {
                    wordFrom = _.clone(start);
                    wordTo =  _.clone(start);
                    word = '';
                }

                positionOx = { start, end, para: oldPara, rootNode: selectionInfo.rootNode, target: selectionInfo.target };

                selectTextArea();

                handleTextAreaPos();
            }

            // no blinking cursor, if a drawing is selected or this is a slide selection (52922)
            if (docModel.useSlideMode()) { caret.toggleClass('noblinkingcursor', selection.isSlideSelection() || selection.isDrawingSelection()); }

            // setting marker when there is a range selection. In this case the keyboard button should not be visible (DOCS-3060)
            app.getWindowNode().toggleClass('range-selection', selType === "Range");
        }

        // textarea.css('visibility', visibility);
        highlight.css('visibility', imeVis);
    }

    function handleTextAreaPos() {
        // not edit mode -> return
        if (positionOx === null) { return; }

        var start = getDOMPosition(positionOx.rootNode, positionOx.start);
        if (!start) {
            globalLogger.warn(positionOx.start + ' does not lead to a DomPos!');
            return;
        }

        var end = null;
        if (_.last(positionOx.start) > 0) {
            // workaround for empty paragraph -> lead to zero size range rectangle
            end = start;
        }
        var rect = getRectangleFromPoints(start, end);

        // the height calculation in 'getRectangleFromPoints' requires a zoom correction for empty text frames (DOCS-3062)
        if (!end && isEmptyParagraph($(start.node).closest(PARAGRAPH_NODE_SELECTOR))) {
            rect.height *= app.getView().getZoomFactor();
        }

        caret.css({
            left: rect.left,
            top: rect.top,
            width: 2,
            height: rect.height
        });
        textarea.css({
            left: rect.left - 4,
            top: rect.top - 10,
            width: 8,
            minWidth: 8,
            height: rect.height + 20,
            zIndex: 100
        });
    }

    function isPassThrough(event, press) {
        if (event.ctrlKey || event.altKey || event.metaKey) { return true; }

        if (press) {
            return event.keyCode > 0 && !isIMEInputKey(event);
        } else if (hasKeyCode(event, 'TAB')) {
            event.preventDefault();
            return true;
        } else if (event.shiftKey && isCursorKey(event.keyCode)) {
            restoreFN();
            event.preventDefault();
            return true;
        } else if (hasKeyCode(event, 'BACKSPACE')) {
            event.preventDefault();
            return true;
        } else if (isCursorKey(event.keyCode) || hasKeyCode(event, 'ENTER', 'DELETE')) {
            return true;
        } else if (event.keyCode > 0 && !isIMEInputKey(event)) {
            return true;
        } else {
            return false;
        }
    }

    function passThrough(event) {
        if (isPassThrough(event, true)) {
            handlePassThrough(event);
        }
    }

    function handlePassThrough(event) {
        var rootNode = docModel.getNode();

        if (!event.shiftKey && hasKeyCode(event, 'UP_ARROW', 'DOWN_ARROW')) {
            event.preventDefault();
            /*
            FIXME
            if (event.type === 'keydown' && oldPara) {
                var offset = hasKeyCode(event, 'UP_ARROW') ? -1 : 1;

                var cursorSpan = oldPara.find('.' + CURSOR);
                var w = $(window);

                var height = cursorSpan.height();
                offset *= height * 1.5;

                var pos = cursorSpan.offset();

                var top = pos.top - w.scrollTop();
                var left = pos.left + cursorSpan.width() - w.scrollLeft();

                var newOxo = getOxoPositionFromPixelPosition(rootNode, left, top + height / 2 + offset);

                if (!newOxo) {
                    if (oldStart.length > 2) {
                        return;
                    }
                    newOxo = { start: [] };
                    if (hasKeyCode(event, 'UP_ARROW')) {
                        newOxo.start[0] = Math.max(0, oldStart[0] - 1);
                    } else {
                        newOxo.start[0] = oldStart[0] + 1;
                    }
                    newOxo.start.push(0);
                }

                self.setTextSelection(newOxo.start);
            }*/
        } else if (!event.shiftKey && hasKeyCode(event, 'LEFT_ARROW', 'RIGHT_ARROW')) {
            docModel.handleAndroidTyping();
            event.preventDefault();
            event.preventDefault = $.noop;
            rootNode.trigger(event);
        } else {
            docModel.handleAndroidTyping();
            event.preventDefault = $.noop;
            rootNode.trigger(event);
        }
        return true;
    }

    function simulateEvent(event, keyCode) {

        updateLogicalPosition(positionOx.start, { start: true });
        updateLogicalPosition(positionOx.end, { start: false });

        textarea.val(INITTEXT);
        setInputSelection(textarea[0], DUMMYLEN);

        if (event.type === 'keyup') {
            handlePassThrough(new $.Event('keydown', { keyCode }));
            handlePassThrough(new $.Event('keypress', { keyCode, charCode: keyCode }));
            handlePassThrough(new $.Event('keyup', { keyCode }));
        }
    }

    // merge new attributes old attributes, if they are not specified in the set of old attributes
    function mergeAttrsIfMissing(oldAttrs, newAttrs) {
        if (!oldAttrs) { return newAttrs; }
        return { ...newAttrs, ...oldAttrs };
    }

    function changedText(event) {

        // whether this is a 'keyup' event
        var isKeyUp = event.type === 'keyup';
        var cursorIndex = getCursorIndex();

        if (isPassThrough(event)) {
            handlePassThrough(event);
            if (!isShiftKey(event)) { restoreFN(); } // check shiftKey, because otherwise keyboard might be closed
            return;
        }

        var text = getText();
        // console.warn('text', text.length, '"' + text + '"');

        if (cursorIndex < 0) {
            // handling backspace
            simulateEvent(event, KeyCode.BACKSPACE);
            return;
        } else if (!text.length) {
            docModel.handleAndroidTyping();
            return;
        } else if (text === ' ' && cursorIndex === 1) {
            // handling space
            simulateEvent(event, KeyCode.SPACE);
            return;
        } else if (text.indexOf('\n') >= 0) {
            // handling enter
            simulateEvent(event, KeyCode.ENTER);
            return;
        }

        if (text.length > 0) {

            var preAttrs = docModel.getPreselectedAttributes() || undefined;
            var attrs = preAttrs;
            var changeTrack = docModel.getChangeTrack();
            var undoOperations = [];
            var redoOperations = [];
            var implParagraph = false;

            if (changeTrack.isActiveChangeTracking()) {
                if (!attrs) {
                    attrs = {};
                } else {
                    attrs = _.copy(attrs, true);
                }
                attrs.changes = { inserted: changeTrack.getChangeTrackInfo() };
            }

            // adding character attributes for the first character in the paragraph (DOCS-4211, DOCS-4830)
            if (positionOx && positionOx.start) {
                const additionalCharacterAttrs = docModel.collectCharacterAttributesForInsertOperation(docModel.getSelection().getRootNode(), positionOx.start);
                if (additionalCharacterAttrs) {
                    attrs = attrs || {};
                    attrs.character = mergeAttrsIfMissing(attrs.character, additionalCharacterAttrs);
                }
            }

            if (positionOx && positionOx.para && isImplicitParagraphNode(positionOx.para[0])) {
                positionOx.para.data('implicit', false);
                var insertParaOp = { name: PARA_INSERT, start: _.initial(wordFrom) };
                docModel.extendPropertiesWithTarget(insertParaOp, positionOx.target);
                redoOperations.push(insertParaOp);
                implParagraph = true; // generating undo later, taking care of deleting text before paragraph
            }

            var insertOp = { name: TEXT_INSERT, start: _.clone(wordTo), text };
            if (attrs) { insertOp.attrs = attrs; }
            docModel.extendPropertiesWithTarget(insertOp, positionOx.target);
            redoOperations.push(insertOp);

            var startWord = _.clone(wordFrom);
            var endWord = null;
            var offset = 0;

            if (_.last(wordFrom) !== _.last(wordTo)) {
                endWord = _.clone(wordTo);
                offset = makeDelOps(redoOperations, startWord, increaseLastIndex(wordTo, -1));

                var insStart = increaseLastIndex(wordFrom, text.length);
                var undoDeleteOp = { name: TEXT_INSERT, start: _.copy(insStart), text: word };
                if (preAttrs) { undoDeleteOp.attrs = preAttrs; }
                docModel.extendPropertiesWithTarget(undoDeleteOp, positionOx.target);
                undoOperations.push(undoDeleteOp);
            } else {
                endWord = _.clone(wordFrom);
            }

            makeDelOps(undoOperations, startWord, endWord);

            if (implParagraph) { makeDelOps(undoOperations, _.initial(wordFrom)); }

            // TODO: Check, if it is it necessary that the operations are applied in 'keydown' and in 'keyup'?
            // Fix for Undo: Registering the undo operations only after 'keyup'.

            // sending operations to server (without applying them locally) and registering them in the undo
            // stack (but only, if this is a keyup event)
            sendOperations(docModel, changeTrack, undoOperations, redoOperations, isKeyUp);

            var leftNoIME = _.contains(ENDCHARS, text[cursorIndex - 1]);

            applyOperations(redoOperations);

            // var pos = _.last(wordTo) - text.length + cursorIndex;
            var pos = _.last(wordFrom) + cursorIndex;
            positionOx.start[positionOx.start.length - 1] = pos;
            positionOx.end[positionOx.end.length - 1] = pos;

            updateLogicalPosition(positionOx.start, { start: true });
            updateLogicalPosition(positionOx.end, { start: false });

            if (leftNoIME) {
                word = text.substring(cursorIndex);
                wordFrom[wordFrom.length - 1] = pos;
                wordTo[wordTo.length - 1] = _.last(wordFrom) + word.length;

                selectTextArea();
            } else {
                word = text;
                wordTo[wordTo.length - 1] = _.last(wordFrom) + text.length;
            }

            updateHighlight(wordFrom, wordTo);
            handleTextAreaPos();

            if (attrs) {
                docModel.paragraphStyles.updateElementFormatting(positionOx.para);
            }
            if (offset > 0) {
                changeSelection({ start:  _.clone(positionOx.start), end: _.clone(positionOx.end), rootNode: docModel.getSelection().getRootNode(), target: docModel.getActiveTarget() });
            }
        }
        docModel.handleAndroidTyping();
    }

    function makeDelOps(ops, start, end) {
        var offset = 0;
        var deleteOp = null;
        if (end) {
            var startPos = getDOMPositionLocal(start);
            var endPos = getDOMPositionLocal(end);
            if (startPos.nodeIndex === endPos.nodeIndex) {
                deleteOp = { name: DELETE, start: _.copy(start), end: _.copy(end) };
                docModel.extendPropertiesWithTarget(deleteOp, positionOx.target);
                ops.push(deleteOp);
            } else {

                for (var i = _.last(end); i >= _.last(start); i--) {
                    var pos = _.initial(start);
                    pos.push(i);

                    var domPos = getDOMPositionLocal(pos);
                    if (isRangeMarkerNode(domPos.node) || isSubstitutionPlaceHolderNode(domPos.node)) {
                        offset++;
                    } else {
                        deleteOp = { name: DELETE, start: _.copy(pos), end: _.copy(pos) };
                        docModel.extendPropertiesWithTarget(deleteOp, positionOx.target);
                        ops.push(deleteOp);
                    }
                }
            }
        } else {
            deleteOp = { name: DELETE, start: _.copy(start) };
            docModel.extendPropertiesWithTarget(deleteOp, positionOx.target);
            ops.push(deleteOp);
        }
        return offset;
    }

    function sendOperations(docModel, changeTrack, undoOperations, redoOperations, isKeyUp) {
        if (changeTrack.isActiveChangeTracking()) {
            var info = changeTrack.getChangeTrackInfo();
            _.each(undoOperations, function (op) {
                addChangeTrackInfo(op, info);
            });
            _.each(redoOperations, function (op) {
                addChangeTrackInfo(op, info);
            });
        }
        docModel.sendOperations(redoOperations); // sends the passed operations to the server without applying them locally.
        if (isKeyUp) { docModel.getUndoManager().addUndo(undoOperations, redoOperations); } // registering the operations on undo stack.
    }

    function getDOMPositionLocal(pos) {
        if (!pos) { return; }

        singleArray[0] = _.last(pos);
        var node = getDOMPosition(positionOx.para, singleArray, true);
        if (node) {
            // node.node = node.node.parentElement;
            node.nodeIndex = $(node.node).index();
            return node;
        }
        throw new Error('nothing found');
    }

    function applyOperations(ops) {

        _.each(ops, function (op) {

            var start = getDOMPositionLocal(op.start);
            var text;

            if (op.name === DELETE) {
                var end = getDOMPositionLocal(op.end);

                if (start.nodeIndex === end.nodeIndex) {
                    text = start.node.innerText;
                    text = text.substring(0, start.offset) + text.substring(end.offset + 1);
                    fillTextInSpan(start.node, text);
                } else {
                    var startText = start.node.innerText;
                    fillTextInSpan(start.node, startText.substring(0, start.offset));
                    var endText = end.node.innerText;
                    fillTextInSpan(end.node, endText.substring(end.offset + 1));
                    var children = positionOx.para.children();
                    if ((end.nodeIndex - start.nodeIndex) > 1) {
                        for (var ni = start.nodeIndex + 1; ni < end.nodeIndex; ni++) {
                            positionOx.para[0].removeChild(children[ni]);
                        }
                    }
                }

            } else if (op.name === TEXT_INSERT) {
                var opText = op.text;
                if (opText[opText.length - 1] === ' ') {
                    opText = opText.substring(0, opText.length - 1) + NBSP;
                }

                if (!start.offset && start.nodeIndex > 0 && isTextSpan(start.node.previousSibling)) {
                    start.nodeIndex--;
                    start.node = start.node.previousSibling;
                    start.offset = start.node.innerHTML.length;
                }

                var attrs = op.attrs;
                text = start.node.innerText;
                text = text.substring(0, start.offset) + opText + text.substring(start.offset);
                fillTextInSpan(start.node, text);

                var splitStart = _.last(op.start);
                var splitEnd = splitStart - start.offset;
                var counter = start.offset;

                for (var i = splitStart; i >= splitEnd; i--) {
                    counter--;
                    if (_.contains(ENDCHARS, text[counter])) {

                        var before = splitTextSpanLocal(positionOx.para, [i]);

                        if (before) {
                            var beforeText = before.innerText;
                            if (_.last(beforeText) === NBSP) {
                                fillTextInSpan(before, beforeText.substring(0, beforeText.length - 1) + ' ');
                            }
                        }
                        break;
                    }
                }

                handlePreselectedAttrs(attrs, start); //fix me wrong node
            }
        });

    }

    function addChangeTrackInfo(op, info) {
        var attrs = op.attrs;
        if (attrs) {
            if (attrs.changes) {
                return;
            }
            attrs = _.copy(attrs, true);
        } else {
            attrs = {};
        }

        var name;
        if (op.name === TEXT_INSERT) {
            name = 'inserted';
        } else {
            name = 'deleted';
        }
        attrs.changes = {};
        attrs.changes[name] = info;

    }

    function handlePreselectedAttrs(attrs, domPos) {

        if (attrs) {
            attrs = { attributes: attrs };
            $(domPos.node).data(attrs);
        }
    }

    textarea = $('<textarea class="edit-textarea">');
    caret = $('<div class="edit-caret"></div>');
    highlight = $('<div class="edit-highlight"></div>');

    function selectTextArea() {

        // not edit mode -> return
        if (positionOx === null || docModel.isPresentationMode()) { return; }

        if (document.activeElement !== textarea[0]) {
            setFocus(textarea);
        }
        textarea.val(INITTEXT + word);
        setInputSelection(textarea[0], DUMMYLEN + (_.last(positionOx.start) - _.last(wordFrom)));
    }

    function getCursorIndex() {
        return textarea[0].selectionStart - DUMMYLEN;
    }

    function getText() {
        return textarea.val().substring(DUMMYLEN).replace(/\\u200B/g, '');
    }

    var processSelectUpdate = (function () {

        var always = function () {
            if (!docModel) { return; }

            var selecttype = window.getSelection().type;
            // selecting
            var rootNode = docModel.getSelection().getRootNode();
            if (getFocus() === rootNode[0] || (slideMode && $(getFocus()).is('.drawing')) || (!slideMode && $(getFocus()).is('.textframe'))) {
                // listener hangs directly on document, but we only want the events of editdiv
                if (selecttype !== 'None') {
                    self.updateSelectionAfterBrowserSelection({ preserveFocus: false, forceTrigger: false }); // prpbrowserevent
                }

                // hide caret when a selection range is visible
                if (selecttype === 'Range') {
                    caret.css('visibility', 'hidden');
                    highlight.css('visibility', 'hidden');

                    // close keyboard when a range is selected and the keyboard is open -> ime backspace problem
                    if (isSoftKeyboardOpen()) {
                        docModel.trigger('selection:possibleKeyboardClose', { forceClose: true });
                    }

                } else {
                    caret.css('visibility', 'visible');
                }

            // writing
            } else if (getFocus() === textarea[0]) {
                if (window.getSelection().anchorNode === div[0]) {
                    // focus really in textarea
                    if (selecttype === 'Range') {
                        if (_.last(wordFrom) < _.last(wordTo)) {
                            restoreFN();
                            self.setTextSelection(wordFrom, wordTo);
                        }
                    }
                }
            }
        };
        var notAlways =  _.throttle(always, 250);

        return function (evt) {

            var selecttype = window.getSelection().type;

            if (evt.type === 'selectstart') {
                if (selecttype === 'Range') {
                    // the following 'preventDefault' makes it impossible to change the selection in OX Presentation,
                    // if there is a text selection range. Therefore it should not be called in this case.
                    var selection = docModel.getSelection();
                    var avoidPreventDefault = app.isPresentationApp() && selection.isTextSelection() && !selection.isSlideSelection() && selection.hasRange();

                    if (!avoidPreventDefault) {
                        // user seems to call "select all", but android chrome does not really support that feature when contenteditable=true and begins with scrolling :(
                        evt.preventDefault();
                        return;
                    }
                }
                always();
            } else {
                notAlways();
            }
        };
    }());

    // initialization -----------------------------------------------------

    textarea.on('keyup keydown', changedText);
    textarea.on('keypress', passThrough);

    self.on('changecursorinfo', changeSelection);

    this.setInterval(function () {
        caret.toggleClass('androidblink');
    }, 500);

    this.waitForImportSuccess(function () {
        var docView = app.getView();

        restoreFN = _.bind(self.restoreBrowserSelection, self);

        textarea.on('cut copy paste', function (evt) {
            // TODO maybe update oxoposition to be sure that it's correct?
            docModel[evt.type](evt);
        });

        textarea.css({
            letterSpacing: -LETTER_WIDTH + 'px',
            fontFamily: MONO_FONT.family,
            fontSize: MONO_FONT.size + 'pt',
            lineHeight: MONO_FONT.size + 'pt'
        });

        textarea.addClass('transparent-selection');

        div.append(textarea);
        div.append(caret);
        div.append(highlight);

        docModel.getNode().parent().append(div);

        docModel.listenTo($(document), 'selectstart selectionchange select', processSelectUpdate);

        this.listenToWindowResizeWhenVisible(function () {

            // too early
            if (positionOx === null) { return; }

            handleTextAreaPos();
            if (app.getWindowNode().hasClass('soft-keyboard-off')) {
                // do nothing
            } else {
                if ($('.io-ox-office-main.popup-container:not(.context-menu)').length > 0) {
                    // do nothing, wait for popup close
                } else if (!isSoftKeyboardOpen() && !docView.keyboardJustOpened() && !isCommentTextframeNode($(getFocus()))) {
                    docModel.trigger('selection:possibleKeyboardClose');
                }
            }
        });

        docModel.on('selection:focusToEditPage', selectTextArea);
        docModel.listenTo(docView, 'change:zoom', function () {
            if (wordFrom && wordTo) { updateHighlight(wordFrom, wordTo); }
            handleTextAreaPos();
        });

        // DOCS-3056: In Spreadsheet it is sometimes required, to set the focus into the textarea node
        docModel.listenTo(docModel.getSelection(), 'change', function () {
            var selection = null;
            if (app.isSpreadsheetApp()) {
                selection = docModel.getSelection();
                if (selection.isAdditionalTextframeSelection() && selection.isTextCursor() && _.last(selection.getStartPosition()) === 0) {
                    // empty shape or empty paragraph -> forcing the focus into the text area
                    // it is sometimes in the textframe or the clipboard after entering text mode or pressing return to insert a new paragraph
                    docModel.executeDelayed(selectTextArea, 0);
                    if (!$(getFocus()).is('.edit-textarea')) { // the focus might have been set into the clipboard
                        docModel.executeDelayed(selectTextArea, 500);
                    }
                }
            }
        });

    });

    this.listenTo(app, 'docs:beforequit', function () {
        textarea.remove();
        div.remove();
    });
}

// exports ================================================================

export default AndroidSelection;
