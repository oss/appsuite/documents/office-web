/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { CLEAR_ATTRIBUTES, findSelectionRange } from '@/io.ox/office/textframework/components/hyperlink/hyperlink';
import { isInsideComplexFieldRange, isTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { DELETE, SET_ATTRIBUTES, TEXT_INSERT } from '@/io.ox/office/textframework/utils/operations';
import { getDOMPosition, increaseLastIndex, isValidElementRange } from '@/io.ox/office/textframework/utils/position';
import { HyperlinkDialog } from '@/io.ox/office/textframework/view/dialogs';
import { getExplicitAttributes } from '@/io.ox/office/editframework/utils/attributeutils';
import { setFocus } from '@/io.ox/office/tk/dom';

function mixinHyperlink(BaseClass) {

    // mix-in class HyperlinkMixin ============================================

    /**
     * A mix-in class for the hyperlink handling in the document model.
     *
     * @param {EditApplication} app
     *  The application instance.
     */

    class HyperlinkMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Inserting a hyperlink during pasting.
         *
         * @param {String} url
         *  The URL of the hyperlink.
         *
         * @param {String} text
         *  The text for the hyperlink.
         *
         * @param {Number[]} [dropPosition]
         *  An optional logical position at which the text will be inserted. If not specified
         *  the current selection is used.
         */
        insertHyperlinkDirect(url, text, dropPosition) {

            const generator = this.createOperationGenerator();
            const hyperlinkStyleId = this.useSlideMode() ? null : this.getDefaultUIHyperlinkStylesheet();

            if (url && url.length > 0) {

                this.getUndoManager().enterUndoGroup(() => {

                    // helper function to insert hyperlink after selected content is removed
                    const doInsertHyperlink = () => {

                        const newText = text || url;
                        // created operation
                        let newOperation = null;
                        // target string property of operation
                        const target = this.getActiveTarget();
                        // the logical start and end positions
                        let start = null, end = null;
                        // the selection object
                        const selection = this.getSelection();

                        // reading start and end position (after calling deleteSelected())
                        start = dropPosition ? dropPosition.start : selection.getStartPosition();

                        // insert new text
                        this.doCheckImplicitParagraph(start);
                        newOperation = { text: newText, start: _.clone(start) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(TEXT_INSERT, newOperation);

                        // Calculate end position of new text
                        // will be used for setAttributes operation
                        end = increaseLastIndex(start, newText.length);

                        if (this.characterStyles.isDirty(hyperlinkStyleId)) {
                            // insert hyperlink style to document
                            this.generateInsertStyleOp(generator, 'character', hyperlinkStyleId);
                        }

                        newOperation = {
                            attrs: { styleId: hyperlinkStyleId, character: { url } },
                            start: _.clone(start),
                            end: _.clone(end)
                        };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(SET_ATTRIBUTES, newOperation);

                        // apply all collected operations
                        this.applyOperations(generator);
                    };

                    // delete selected range (this creates new operations)
                    if (this.getSelection().hasRange()) {
                        return this.deleteSelected()
                            .done(() => {
                                doInsertHyperlink();
                            });
                    }

                    doInsertHyperlink();
                    return $.when();
                });
            }
        }

        /**
         * Shows the hyperlink dialog for the current selection, and returns a
         * promise that allows to wait for the dialog.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         */
        insertHyperlinkDialog() {

            // the selection object
            const selection = this.getSelection();
            // the generator object for the operations
            const generator = this.createOperationGenerator();
            let text = '', url = '';
            let startPos = null;
            let start = selection.getStartPosition();
            let end = selection.getEndPosition();
            // target string for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // currently active root node
            const activeRootNode = this.getCurrentRootNode(target);
            // created operation
            let newOperation = null;
            // the dialog promise
            let promise = null;
            // a modified selection object
            let newSelection = null;
            // whether a specified selection was already expanded
            let alreadyExpanded = false;
            // the active target when the dialog opens
            const savedTarget = target;

            // helper function to expand the given selection
            const expandSelection = () => {
                newSelection = findSelectionRange(this, selection);
                if (newSelection.start !== null && newSelection.end !== null) {
                    startPos = selection.getStartPosition();
                    start[start.length - 1] = newSelection.start;
                    end[end.length - 1] = newSelection.end;
                    selection.setTextSelection(start, end); // this also handles range markers!
                    start = selection.getStartPosition(); // reload start position, to guarantee range marker handling (42095)
                    end = selection.getEndPosition();     // reload end position, to guarantee range marker handling (42095)
                }
            };

            // helper function to expand selection range inside complex fields, if they have already an URL specified.
            const expandRangeInsideComplexField = () => {

                // the start point at the selections start position
                const startPoint = getDOMPosition(activeRootNode, selection.getStartPosition());

                if (startPoint && startPoint.node && startPoint.node.parentNode && isTextSpan(startPoint.node.parentNode) && isInsideComplexFieldRange(startPoint.node.parentNode)) {

                    // further check, if the text span also already has an url defined
                    const charAttrs = getExplicitAttributes(startPoint.node.parentNode, 'character');
                    if (charAttrs.url) {
                        selection.setTextSelection(selection.getStartPosition());  // removing selection range
                        expandSelection();
                    }
                }
            };

            if (!selection.hasRange()) {
                expandSelection();
                alreadyExpanded = true;
            }

            // avoiding that hyperlinks that expand over complex fields are splitted inside the complex
            // field. This can happen, if the new selection is only a part inside a complex field. In this
            // case the new hyperlink has to expand to the range of the old hyperlink.
            // The filter cannot handle the case, in which a hyperlink starts outside of a complex field
            // and ends inside a complex field.
            if (selection.hasRange() && !alreadyExpanded) { expandRangeInsideComplexField(); }

            if (!this.hasEnclosingParagraph() && !selection.isSlideSelection()) {
                return $.Deferred().reject();
            }

            // use range to retrieve text and possible url
            if (selection.hasRange()) {

                // Find out the text/url of the selected text to provide them to the
                // hyperlink dialog
                selection.iterateNodes((node, _pos, start, length) => {
                    if ((start >= 0) && (length >= 0) && isTextSpan(node)) {
                        const nodeText = $(node).text();
                        if (nodeText) {
                            text = text.concat(nodeText.slice(start, start + length));
                        }
                        if (url.length === 0) {
                            const charAttributes = this.characterStyles.getElementAttributes(node).character;
                            if (charAttributes.url && charAttributes.url.length > 0) {
                                url = charAttributes.url;
                            }
                        }
                    }
                });
            }

            // show hyperlink dialog
            promise = new HyperlinkDialog(this.#docApp.getView(), url, text).show();

            return promise.done(data => {
                // set url to selected text
                const hyperlinkStyleId = this.useSlideMode() ? null : this.getDefaultUIHyperlinkStylesheet();
                const url = data.url;
                let oldAttrs = null;
                let attrs = null;
                let hyperlinkNode = null;
                let insertedLink = false;
                // the change track object
                const changeTrack = this.getChangeTrack();

                // checking, if a predefined selection was modified in the meantime
                if (this.#docApp.isOTEnabled()) {
                    if (savedTarget !== this.getActiveTarget()) { return; } // even the complete target was removed in the meantime
                    if (!_.isEqual(start, selection.getStartPosition()) || !_.isEqual(end, selection.getEndPosition())) {
                        start = selection.getStartPosition(); // reload start position modified by OT
                        end = selection.getEndPosition();     // reload end position modified by OT
                    }
                }

                this.getUndoManager().enterUndoGroup(() => {

                    // the start position of a selected range
                    let rangeStart = null;
                    // the end position of a selected range
                    let rangeEnd = null;
                    // whether a selected range can be deleted after inserted a modified text
                    let deleteRangeAfterInsert = false;
                    // the operation attributes object
                    let operationProperties = null;
                    // whether the end position must already exist
                    let endMustExist = true;

                    if (data.url === null && data.text === null) {
                        // remove hyperlink
                        // setAttribute uses a closed range therefore -1
                        attrs = CLEAR_ATTRIBUTES;

                        if (changeTrack.isActiveChangeTracking()) {
                            hyperlinkNode = getDOMPosition(activeRootNode, selection.getEndPosition()).node;
                            if (hyperlinkNode && (hyperlinkNode.nodeType === 3)) { hyperlinkNode = hyperlinkNode.parentNode; }
                            // Expanding operation for change tracking with old explicit attributes
                            oldAttrs = changeTrack.getOldNodeAttributes(hyperlinkNode);
                            // adding the old attributes, author and date for change tracking
                            if (oldAttrs) {
                                oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
                                attrs.changes = { modified: oldAttrs };
                            }
                        }
                    } else {
                        // insert new text frame in Presentation if selection is not set in any existing, see #47413
                        if (this.useSlideMode() && !selection.isAdditionalTextframeSelection() && !selection.isDrawingSelection()) {
                            if (selection.isMultiSelection()) { selection.clearMultiSelection(); }
                            this.insertTextFrame();
                            start = selection.getStartPosition();
                        }

                        // if the data.text is a emtpy string, a default value is set (fix for Bug 40313)
                        if (data.text === '') {
                            data.text = data.url;
                        }

                        // insert/change hyperlink
                        if (data.text !== text) {

                            // text has been changed
                            if (selection.hasRange()) {
                                start = selection.getStartPosition();
                                rangeStart = _.clone(start);
                                rangeEnd = selection.getEndPosition();

                                if (_.isEqual(_.initial(rangeStart), _.initial(rangeEnd))) {  // single paragraph selection
                                    deleteRangeAfterInsert = true;  // deleting later to keep attributes (48157)
                                } else {
                                    this.deleteSelected();
                                }
                            }

                            if (changeTrack.isActiveChangeTracking()) {
                                attrs = attrs || {};
                                attrs.changes = { inserted: changeTrack.getChangeTrackInfo() };
                                insertedLink = true;
                            }

                            // insert new text
                            this.doCheckImplicitParagraph(start);
                            newOperation = { text: data.text, start: _.clone(start) };
                            if (attrs) { newOperation.attrs = attrs; }
                            this.extendPropertiesWithTarget(newOperation, target);
                            generator.generateOperation(TEXT_INSERT, newOperation);

                            endMustExist = false; // -> the end position will be generated with insertText operation

                            // -> deleting a range -> this must happen after inserting, because otherwise the attributes can be lost (48157)
                            if (deleteRangeAfterInsert) {
                                rangeStart = increaseLastIndex(rangeStart, data.text.length); // adapting the start position of the removed the range
                                rangeEnd = increaseLastIndex(rangeEnd, data.text.length - 1);  // adapting also the end position
                                operationProperties = { start: _.copy(rangeStart), end: _.copy(rangeEnd) };
                                this.extendPropertiesWithTarget(operationProperties, target);
                                generator.generateOperation(DELETE, operationProperties); // generating a single delete operation
                            }

                            // Calculate end position of new text, will be used for setAttributes operation
                            end = increaseLastIndex(start, data.text.length);
                        }

                        if (this.characterStyles.isDirty(hyperlinkStyleId)) {
                            // insert hyperlink style to document
                            this.generateInsertStyleOp(generator, 'character', hyperlinkStyleId);
                        }

                        attrs = { styleId: hyperlinkStyleId, character: { url } };

                        if (changeTrack.isActiveChangeTracking() && !insertedLink) {
                            hyperlinkNode = getDOMPosition(activeRootNode, end).node;
                            if (hyperlinkNode && (hyperlinkNode.nodeType === 3)) { hyperlinkNode = hyperlinkNode.parentNode; }
                            // Expanding operation for change tracking with old explicit attributes
                            oldAttrs = changeTrack.getOldNodeAttributes(hyperlinkNode);
                            // adding the old attributes, author and date for change tracking
                            if (oldAttrs) {
                                oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
                                attrs.changes = { modified: oldAttrs };
                            }
                        }
                    }

                    end[end.length - 1] -= 1;
                    newOperation = {
                        attrs,
                        start: _.clone(start),
                        end: _.clone(end)
                    };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(SET_ATTRIBUTES, newOperation);

                    // OT: The position might have been destroyed in the meantime -> avoid invalid operations
                    // -> a smarter process in the future might update the positions in the operation(s), if possible.
                    if (this.#docApp.isOTEnabled() && !isValidElementRange(this.getCurrentRootNode(target), start, endMustExist ? end : start, { allowFinalPosition: true })) { return; }

                    // apply all collected operations
                    this.applyOperations(generator);

                }, this); // enterUndoGroup()

            }).always(() => {
                if (startPos) {
                    selection.setTextSelection(startPos);
                }
            });
        }

        /**
         * Removing a hyperlink at the current selection.
         */
        removeHyperlink() {

            // the selection object
            const selection = this.getSelection();
            // the generator object for the operations
            const generator = this.createOperationGenerator();
            let startPos = null;
            const start = selection.getStartPosition();
            const end = selection.getEndPosition();
            let oldAttrs = null;
            let attrs = null;
            let hyperlinkNode = null;
            // target string for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // currently active root node
            const activeRootNode = this.getCurrentRootNode(target);
            // created operation
            let newOperation = null;
            // a modified selection object
            let newSelection = null;
            // the change track object
            const changeTrack = this.getChangeTrack();

            // Bug 47546: The context menu with options for removing and editing hyperlinks is only opened
            // when there is no range selected. In some situations in OSX a text range selection is applied to a
            // hyperlink while this context menu opens. This selection contains in most cases and depending on
            // the browser only a part from the hyperlink and therefore only a part of the hyperlink would be
            // removed. To be sure that the whole hyperlink is removed, clear the textSelection, so that
            // the text selection for the hyperlink is determined again with the following 'findSelectionRange()'.
            if (_.browser.MacOS && (_.browser.Chrome || _.browser.Safari)) { selection.setTextSelection(start, start); }

            if (!selection.hasRange()) {
                newSelection = findSelectionRange(this, selection);
                if (newSelection.start !== null && newSelection.end !== null) {
                    startPos = selection.getStartPosition();
                    start[start.length - 1] = newSelection.start;
                    end[end.length - 1] = newSelection.end;
                    selection.setTextSelection(start, end);
                }
            }

            if (selection.hasRange() && this.hasEnclosingParagraph()) {

                attrs = CLEAR_ATTRIBUTES;

                if (changeTrack.isActiveChangeTracking()) {
                    hyperlinkNode = getDOMPosition(activeRootNode, selection.getEndPosition()).node;
                    if (hyperlinkNode && (hyperlinkNode.nodeType === 3)) { hyperlinkNode = hyperlinkNode.parentNode; }
                    // Expanding operation for change tracking with old explicit attributes
                    oldAttrs = changeTrack.getOldNodeAttributes(hyperlinkNode);
                    // adding the old attributes, author and date for change tracking
                    if (oldAttrs) {
                        oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
                        attrs.changes = { modified: oldAttrs };
                    }
                }

                // remove hyperlink
                // setAttribute uses a closed range therefore -1
                end[end.length - 1] -= 1;

                newOperation = {
                    attrs,
                    start: _.clone(start),
                    end: _.clone(end)
                };
                this.extendPropertiesWithTarget(newOperation, target);
                generator.generateOperation(SET_ATTRIBUTES, newOperation);

                // apply the operations (undo group is created automatically)
                this.applyOperations(generator);

                this.executeDelayed(() => {
                    this.#docApp.getView().grabFocus();
                    if (startPos) {
                        selection.setTextSelection(startPos);
                    }
                    // in Edge browser it is necessary, to set the focus into the textframe (62401)
                    if (_.browser.Edge && selection.isAdditionalTextframeSelection()) {
                        const textFrame = selection.getSelectedTextFrameDrawing().find('div.textframe');
                        if (textFrame.length > 0) { setFocus(textFrame); }
                    }
                });
            }
        }

        // private methods ----------------------------------------------------

    }

    return HyperlinkMixin;
}

// exports ====================================================================

export const XHyperlink = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XTableOperation>}
     *  The extended class with hyperlink functionality.
     */
    mixin: mixinHyperlink
};
