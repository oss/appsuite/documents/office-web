/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { FILTER_MODULE_NAME } from '@/io.ox/office/tk/utils/io';
import { isCharacterFontThemed, isColorThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import { convertToEM, getBooleanOption, getDomNode, getObjectOption } from '@/io.ox/office/textframework/utils/textutils';
import { LIST_LABEL_NODE_SELECTOR, PARAGRAPH_NODE_LIST_EMPTY_CLASS, PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS,
    PARAGRAPH_NODE_SELECTOR, createListLabelNode, findFirstPortionSpan, isEmptyParagraph, getReplacementForBullet } from '@/io.ox/office/textframework/utils/dom';

// mix-in class UpdateListsMixin ======================================

/**
 * A mix-in class for the document model providing the handler functions
 * that keep lists up-to-date.
 *
 * Triggers the follwing events:
 * - 'listformatting:done': The debounced list formatting is done.
 *
 * @param {Application} app
 *  The application containing this instance.
 */
export default function UpdateListsMixin(app) {

    var // self reference for local functions
        self = this,
        // a collector for all paragraph nodes that need to be updated
        paragraphs = $(),
        // whether only the current paragraph needs to be updated
        onlySingleParagraph = false;

    // private methods ----------------------------------------------------

    /**
     * Direct callback for 'updateListsDebounced'. This is triggered every time, when a list update
     * is required. It allows registration of paragraphs, for that a list update must be done. Typically
     * this paragraph and all its neighbors need to be updated. Using the option 'singleParagraph' it
     * can be forced, that the neighbors will not be updated.
     *
     * @param {HTMLElement|jQuery} [paragraph]
     *  Optional paragraph or set of paragraphs, that need to be updated. If not specified
     *  all paragraphs in the document will to be updated.
     *
     * @param {Object} options
     *  Optional parameters:
     *  @param {Boolean} [options.singleParagraph=false]
     *   If set to true, only the one specified paragraph is updated. Otherwise
     *   all paragraphs in the drawing that contains the specified paragraph are updated. 'singleParagraph'
     *   is useful, if a bullet type is modified or some character attributes in a paragraph are modified.
     */
    function registerListUpdate(paragraph, options) {

        // whether only the specified paragraph needs to be updated
        onlySingleParagraph = getBooleanOption(options, 'singleParagraph', false);

        // store the new paragraph in the collection (jQuery keeps the collection unique)
        if (paragraph) {
            paragraphs = paragraphs.add(paragraph);
        }
    }

    /**
     * The deferred callback for the list update.
     *
     * @returns {jQuery.Promise}
     *  A promise that is resolved, when all paragraphs are updated. Asynchronous updating of
     *  lists happens only during loading the document.
     */
    function execUpdateLists() {

        // the collector for the paragraphs that need to be updated
        const allParagraps = onlySingleParagraph ? paragraphs : paragraphs.parent().children(PARAGRAPH_NODE_SELECTOR);
        // a promise that is resolved, when all paragraphs are updated.
        // This happens only asynchronous, when the document is loaded
        const updateListPromise = self.updateLists(allParagraps);

        paragraphs = $();
        onlySingleParagraph = false;

        return updateListPromise;
    }

    // public methods -----------------------------------------------------

    /**
     * Updates all paragraphs that are part of any bullet or numbering
     * list or update all specified paragraphs.
     *
     * @param {jQuery} [paragraphs]
     *  Optional set of paragraphs, that need to be updated. If not specified
     *  all paragraphs in the document need to be updated. This happens after
     *  the document was loaded successfully. In this case the option 'async'
     *  needs to be set to true.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.async=false]
     *      If set to true, all lists are updated asynchronously. This
     *      should happen only once when the document is loaded.
     *  @param {Object} [options.baseAttributes]
     *      An attribute set with all attribute values of the ancestor of
     *      the passed element, merged from style sheet and explicit
     *      attributes, keyed by attribute family. Used internally while
     *      updating the formatting of all child elements of a node.
     *
     * @returns {jQuery.Promise}
     *  A promise that is resolved, when all paragraphs are updated.
     */
    this.updateLists = function (paragraphs, options) {

        var // a collector for all numbers for numbered list items
            listItemCounter = {},
            // check, if for the current level a start value is already defined
            isStartValueOnLevelSet = {},
            // list of all paragraphs in the document
            paragraphNodes = null,
            // whether the paragraphs are specified
            predefinedParagraphs = !_.isUndefined(paragraphs),
            // caller may pass precalculated base attributes, e.g. of a parent DOM element
            baseAttributes = getObjectOption(options, 'baseAttributes', null),
            // the deferred, whose promise will be returned
            def = null,
            // the target chain, that is required to resolve the correct theme
            target = null;

        function updateListInParagraph(para) {

            var // the attributes at the paragraph
                elementAttributes = self.paragraphStyles.getElementAttributes(para, { baseAttributes }),
                // the paragraph attributes at the paragraph
                paraAttributes = elementAttributes.paragraph,
                // the paragraph level
                listLevel = paraAttributes.level,
                // an existing list label at the paragraph
                oldLabel = null,
                // whether tab stops need to be recalculated at the paragraph
                updateParaTabstops = false,
                // during loading the document, the counter has to be resetted for every first paragraph in text frame
                // -> Important: There can be paragraphs from several drawings in 'paragraphs', if a layout slide was modified(!)
                resetListItemCounter = $(para).prevAll(PARAGRAPH_NODE_SELECTOR).length === 0,
                // the character and font name that are used for displaying
                displayChar = null, displayFontName = null,
                // the numbering type
                numType = null,
                // whether the paragraph has a list label
                noListLabel = true,
                // an url for a list item bitmap
                imageUrl = null,
                // the id of the numbered list
                listCounterId =  null,
                // the index of the sub levels
                subLevelIdx = 0,
                // a text string
                textChar = '',
                // the list label node
                numberingElement = null,
                // the first text span in the paragraph
                span = null,
                // the character attributes of the text span following the list label node
                followingTextAttributes = null,
                // the list span inside the list label node
                listSpans = null,
                // whether the paragraph is empty
                isEmptyParagraphLocal = false,
                // must call more often the model
                docModel = app.getModel();

            // helper function to count the number for the list items in numbered lists
            // (only used for numbered list paragraphs)
            function countListLevel() {

                if (resetListItemCounter) {  // resetting for each new drawing
                    listItemCounter = {};
                    isStartValueOnLevelSet = {};
                }

                if (!listItemCounter[listCounterId]) {
                    listItemCounter[listCounterId] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                }

                if (!noListLabel) {
                    if (_.isNumber(paraAttributes.bullet.startAt)) {
                        if (isStartValueOnLevelSet[listLevel]) {
                            if (app.isODF() || !isEmptyParagraphLocal) { listItemCounter[listCounterId][listLevel]++; } // there is already a start value defined
                        } else {
                            listItemCounter[listCounterId][listLevel] = paraAttributes.bullet.startAt;  // this is the first paragraph with start value
                            isStartValueOnLevelSet[listLevel] = true;  // a new start value for this level is set
                        }
                    } else {
                        if (app.isODF() || !isEmptyParagraphLocal) { listItemCounter[listCounterId][listLevel]++; }
                        isStartValueOnLevelSet[listLevel] = false;  // no current start value for this level
                    }
                }

                // reset all sub-levels depending on their 'levelRestartValue' attribute
                subLevelIdx = listLevel + 1;

                _.each(_.keys(listItemCounter), function (counterId) {

                    var index = subLevelIdx;
                    for (; index < 10; index++) {
                        listItemCounter[counterId][index] = 0;
                        isStartValueOnLevelSet[index] = false;  // no current start value for all sub levels
                    }

                    // restart at the same level for all other list IDs
                    if (counterId !== listCounterId) {
                        listItemCounter[counterId][listLevel] = 0;
                    }
                });

            }

            // handling the count list level for paragraphs, that are not numbered lists. This
            // can be bullet lists or no lists at all -> at that level and lower, all lists are
            // restarted.
            function restartCountListLevel(level) {

                _.each(_.keys(listItemCounter), function (counterId) {

                    var index = level;
                    for (; index < 10; index++) {
                        listItemCounter[counterId][index] = 0;
                        isStartValueOnLevelSet[index] = false;
                    }
                });
            }

            // updating the paragraph
            oldLabel = $(para).children(LIST_LABEL_NODE_SELECTOR);
            updateParaTabstops = oldLabel.length > 0;
            oldLabel.remove();

            // After removing the label, it can be checked, if the paragraph is empty
            // -> PARAGRAPH_NODE_LIST_EMPTY_CLASS might already be set by the 'selectionChangeHandler'
            isEmptyParagraphLocal = $(para).hasClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS) || isEmptyParagraph(para);
            if (isEmptyParagraphLocal) {
                $(para).addClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS); // -> adding a class to the paragraph to mark it as empty
                if (!app.isODF() && (getDomNode(self.getSelectedParagraph()) === getDomNode(para))) {
                    $(para).addClass(PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS); // -> adding a class to the paragraph to mark it as empty and selected
                }
            }
            // resetting counter for non numbering lists
            if (paraAttributes.bullet.type !== 'numbering') { restartCountListLevel(_.isNumber(listLevel) ? listLevel : 0); }

            if (_.isNumber(listLevel) && listLevel >= 0) {

                noListLabel = paraAttributes.bullet.type ? paraAttributes.bullet.type === 'none' : true;
                imageUrl = paraAttributes.bullet.type === 'bitmap' ? paraAttributes.bullet.imageUrl : null;

                // preparing counting of list item numbers
                if (paraAttributes.bullet.type === 'numbering') {
                    listCounterId = paraAttributes.bullet.numType;  // different counter for every different numbering type
                } else {
                    resetListItemCounter = true; // restart counting after every non-numbering paragraph (TODO: Only in same level?)
                }

                if (listLevel !== -1 && listLevel < 10) {

                    if (paraAttributes.bullet.type === 'numbering') { countListLevel(); }

                    updateParaTabstops = true;

                    if ((paraAttributes.bullet.type === 'character') && _.isString(paraAttributes.bulletFont.name)) {

                        // check if a replacement is required for displaying the character correctly
                        const replacement = getReplacementForBullet(paraAttributes.bulletFont.name, paraAttributes.bullet.character);

                        if (replacement) {
                            displayChar = replacement.char;
                            displayFontName = replacement.font;
                        } else {
                            displayChar = paraAttributes.bullet.character;
                            displayFontName = paraAttributes.bulletFont.name || null;
                        }

                        textChar = paraAttributes.bullet.character ? displayChar : ''; // setting the character that is used inside the list label

                    } else if (paraAttributes.bullet.type === 'numbering') {
                        numType = paraAttributes.bullet.numType || 'arabicPeriod';
                        textChar = self.createNumberedListItem((isEmptyParagraphLocal && !app.isODF()) ? (listItemCounter[listCounterId][listLevel] + 1) : listItemCounter[listCounterId][listLevel], numType);
                        displayFontName = paraAttributes.bulletFont.name ? paraAttributes.bulletFont.name : null;
                        if (app.isODF()) { displayFontName = null; } // not using specified font in numbered lists in ODT (55671)
                    }

                    numberingElement = createListLabelNode(noListLabel ? '' : textChar);

                    // setting a marker for visibility in ODP files
                    if (app.isODF()) { numberingElement.addClass('isodf'); }

                    span = findFirstPortionSpan(para);
                    followingTextAttributes = self.characterStyles.getElementAttributes(span, { baseAttributes }).character;
                    listSpans = numberingElement.children('span');

                    if (imageUrl) {

                        var absUrl = app.getServerModuleUrl(FILTER_MODULE_NAME, { action: 'getfile', get_filename: imageUrl }, { uid: false });
                        var image = $('<div>', { contenteditable: false });
                        var img = $('<img>', { src: absUrl });
                        var baseLine = null;
                        var isEMConverted = false;

                        if (!target && isCharacterFontThemed(followingTextAttributes)) {
                            target = docModel.getThemeTargets(para);
                        }

                        if (paraAttributes.bulletSize.type === 'followText') {
                            baseLine = 0.8 * docModel.getRenderFont(followingTextAttributes, 1, target).getBaseLineOffset();
                        } else if (paraAttributes.bulletSize.type === 'percent') {
                            baseLine = convertToEM(Math.round((paraAttributes.bulletSize.size / 100) * 0.8 * followingTextAttributes.fontSize), 'pt'); // using factor 0.8 looks more precise
                            isEMConverted = true;
                        } else {
                            isEMConverted = true;
                            baseLine = convertToEM(paraAttributes.bulletSize.size, 'pt');
                        }

                        // see 47859/48025/55478/55510
                        if (isEMConverted) {
                            image.css('height', baseLine);
                            img.css('height', baseLine);
                        } else {
                            image.css('height', convertToEM(baseLine, 'px'));
                            img.css('height', convertToEM(baseLine, 'px'));
                        }

                        image.addClass('drawing')
                            .addClass('bulletdrawing')
                            .data('url', imageUrl)
                            .append(
                                $('<div>').append(img)
                            );

                        numberingElement.prepend(image);

                    } else if (!noListLabel) {

                        if (paraAttributes.bulletSize.type === 'followText') {
                            listSpans.css('font-size', convertToEM(followingTextAttributes.fontSize, 'pt'));
                        } else if (paraAttributes.bulletSize.type === 'percent') {
                            listSpans.css('font-size', convertToEM(Math.round((paraAttributes.bulletSize.size / 100) * followingTextAttributes.fontSize), 'pt'));
                        } else {
                            listSpans.css('font-size', convertToEM(paraAttributes.bulletSize.size, 'pt')); // TODO: Is the size given in 'pt' ?
                        }

                        if (paraAttributes.bulletFont.followText) {
                            listSpans.css('font-family', self.getCssFontFamily(followingTextAttributes.fontName));
                        } else {
                            if (displayFontName) { listSpans.css('font-family', self.getCssFontFamily(displayFontName)); }
                        }

                        if (isColorThemed(followingTextAttributes.color) && (!target || !predefinedParagraphs)) {
                            target = docModel.getThemeTargets(para); // target chain should be identical for all predefined paragraphs
                        }

                        if (paraAttributes.bulletColor.followText) {
                            listSpans.css('color', self.getCssColor(followingTextAttributes.color, 'text', target));
                        } else {
                            listSpans.css('color', self.getCssColor(paraAttributes.bulletColor.color, 'text', target));
                        }

                        // in numbering also bold and italic are adapted from the following text span
                        if (paraAttributes.bullet.type === 'numbering' && paraAttributes.bulletFont.followText) {
                            listSpans.css('font-weight', $(span).css('font-weight'));
                            listSpans.css('font-style', $(span).css('font-style'));
                        }

                        self.characterStyles.updateElementLineHeight(listSpans, paraAttributes.lineHeight, followingTextAttributes);
                    }

                    // setting distance between margin, numbering element and text (effects only the numbering element!)
                    if (!noListLabel && paraAttributes.indentFirstLine < 0) {
                        numberingElement.css('min-width', (-paraAttributes.indentFirstLine / 100) + 'mm');
                    }

                    if (-paraAttributes.indentFirstLine > paraAttributes.indentLeft) {
                        numberingElement.css('margin-left', (-paraAttributes.indentLeft - paraAttributes.indentFirstLine) / 100 + 'mm');
                    }

                    if (!noListLabel) { $(para).prepend(numberingElement); } // adding the list label

                }
            }

            if (updateParaTabstops) {
                self.paragraphStyles.updateTabStops(para);
            }
        }

        // receiving list of all document paragraphs (excluding slides)
        paragraphNodes = paragraphs ? paragraphs : self.getCurrentRootNode().find(PARAGRAPH_NODE_SELECTOR).not('.slide');

        if (getBooleanOption(options, 'async', false)) {
            def = self.iterateArraySliced(paragraphNodes, updateListInParagraph);
        } else {
            paragraphNodes.each(function () { updateListInParagraph(this); });
            def = $.when();
        }

        return def.promise().always(function () {
            // informing the listeners, that the list formatting is done
            self.trigger('listformatting:done');
        });
    };

    // the debounced list handler function
    this.updateListsDebounced = this.debounce(registerListUpdate, execUpdateLists, { delay: "animationframe" });
}
