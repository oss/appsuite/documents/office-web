/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';

import { is, str } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { BREAK, convertLengthToHmm, getBooleanOption, getIntegerOption, getNodeName, getObjectOption,
    getStringOption, iterateDescendantNodes, iterateSelectedDescendantNodes } from '@/io.ox/office/tk/utils';

import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { OperationGenerator } from '@/io.ox/office/editframework/model/operation/generator';

import { getMimeTypeFromImageUri } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { NODE_SELECTOR, getDrawingType, isDrawingFrame, isTextFrameShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';

import * as Op from '@/io.ox/office/textframework/utils/operations';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, increaseLastIndex, iterateParagraphChildNodes } from '@/io.ox/office/textframework/utils/position';
import { getCharacterAttributesFromEmptyTextSpan } from '@/io.ox/office/textframework/utils/textutils';

// class TextOperationGenerator ===========================================

/**
 * An instance of this class contains an operations array and provides
 * methods to generate operations for various element nodes.
 *
 * @param {TextModel} docModel
 *  The document model containing this generator.
 */
export default class TextOperationGenerator extends OperationGenerator {

    constructor(docModel) {

        // base constructor
        super(docModel);

        // private properties
        this._slideMode = docModel.useSlideMode();
        this._attrPool = docModel.defAttrPool;
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates and appends a new operation with a target property to the
     * operations array.
     *
     * @param {String} name
     *  The name of the operation.
     *
     * @param {Object|Null} [properties]
     *  Additional properties that will be stored in the operation.
     *
     * @param {Object} [options]
     *  Optional parameters. Supports all options of the public method
     *  OperationGenerator.generateOperation(), and the following additional
     *  options:
     *  @param {String} [options.target]
     *      The identifier of the target to be added as 'target' property to
     *      the generated operation. If omitted or an empty string, no target
     *      property will be added to the operation.
     *
     * @returns {Operation}
     *  The created JSON operation object.
     */
    generateOperationWithTarget(name, properties, options) {
        const target = options && options.target;
        if (target) { properties = _.extend({}, properties, { target }); }
        return this.generateOperation(name, properties, options);
    }

    /**
     * Creates and appends a new operation to the operations array. Adds
     * explicit attributes of the passed node to the 'attrs' option of the new
     * operation.
     *
     * @param {HTMLElement|jQuery} element
     *  The element node that may contain explicit formatting attributes. If
     *  this object is a jQuery collection, uses the first node it contains.
     *
     * @param {String} name
     *  The name of the operation.
     *
     * @param {Object|Null} [properties]
     *  Additional properties that will be stored in the operation.
     *
     * @param {Object} [options]
     *  Optional parameters. Supports all options of the public method
     *  OperationGenerator.generateOperationWithTarget(), and the following
     *  additional options:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute set of the generated operation.
     *  @param {Object} [options.drawingListStyles]
     *      If specified, this property will be object containing list style
     *      attributes of the drawing.
     *  @param {Object} [options.slideListStyles]
     *      If specified, this property will be object containing list style
     *      attributes of the slide.
     *  @param {Boolean} [options.getNewDrawingId=false]
     *      If set to true, new id for the drawing will be created.
     *
     * @returns {Operation}
     *  The created operation JSON object.
     */
    generateOperationWithAttributes(element, name, properties, options) {

        // explicit attributes of the passed node
        const attributes = getExplicitAttributeSet(element);
        // a specified property, that shall be ignored in the setAttributes operation
        const ignoreFamily = getStringOption(options, 'ignoreFamily', '');
        // object containing list style attributes of the drawing
        const drawingListStyles = getObjectOption(options, 'drawingListStyles', {});
        // object containing the slide list style attributes
        const slideListStyles = getObjectOption(options, 'slideListStyles', null);
        // the active application
        const app = this.docModel.docApp;
        // restoring character attributes at paragraphs
        const characterAttributes = (!app.isODF() && options && options.isParagraph) ? getCharacterAttributesFromEmptyTextSpan(element) : null;
        // a new drawing ID
        let newDrawingId = null;
        // Whether to use new drawing id, and overwrite existing one that is copied, to keep them unique
        const getNewDrawingId = options && options.getNewDrawingId;
        // the document file descriptor
        const fileDescriptor = app.getFileDescriptor();
        // Need width of parent to calculate width of inner table
        let parentWidth;

        // receiving additional attributes from empty paragraph
        if (characterAttributes) { attributes.character = characterAttributes; }

        // deleting the specified families from the attributes
        if (attributes && ignoreFamily.length > 0) {
            for (const family of str.splitTokens(ignoreFamily)) {
                delete attributes[family];
            }
        }

        // create a copy of the operation properties, ensure an existing object
        properties = { ...properties };

        // add the 'attrs' entry if there are attributes (but do not overwrite merged attributes, if they are already saved in properties)
        if (!_.isEmpty(attributes) && _.isEmpty(properties.attrs)) {
            properties.attrs = attributes;
        }

        if (!_.isEmpty(drawingListStyles)) {
            properties.attrs = properties.attrs || {};
            properties.attrs = _.extend(_.copy(drawingListStyles, true), properties.attrs); // always create a clone for drawing list styles
        }

        // adding the slide list styles, if available
        if (slideListStyles) {
            properties.attrs = properties.attrs || {};
            properties.attrs.listStyles = slideListStyles;
        }
        if (properties.attrs && getNewDrawingId) {
            // overwrite drawing id with the new one to keep them unique
            if (properties.attrs.drawing) {
                newDrawingId = this.docModel.getNewDrawingId();
                if (options.drawingIdsTransTable) { options.drawingIdsTransTable.push([newDrawingId, properties.attrs.drawing.id]); }
                properties.attrs.drawing.id = newDrawingId;
            }
            // remove linked drawing ids, as they are not valid anymore (or replace them (if possible))
            if (properties.attrs.connector) {
                if (options.drawingIdsTransTable) {
                    options.drawingIdsTransTable.foundConnector = true; // setting a marker at the collector -> evaluation follows
                } else {
                    properties.attrs.connector.startId = null;
                    properties.attrs.connector.startIndex = null;
                    properties.attrs.connector.endId = null;
                    properties.attrs.connector.endIndex = null;
                }
            }
        }

        // add additinal attributes to identify whether copy&paste takes place between two documents or inside the same document
        if (properties.attrs && properties.attrs.fill && properties.attrs.fill.type === 'bitmap') {
            _.extend(properties.attrs.fill, { fileId: fileDescriptor.id, sessionId: ox.session });
        }

        // Check of odfAutoWidth set in `generateTableOperations()` to make the width if inner table greater than parent cell width
        if (element.odfAutoWidth) {
            parentWidth = $(element).parent().width();
            // adding 10hmm so that the width is larger than the width of the table cell. This will trigger width: "auto" (refer tablestyles.js)
            properties.attrs.table.width = convertLengthToHmm(parentWidth, 'px') + 10;
        }

        // push the operation
        return this.generateOperationWithTarget(name, properties, options);
    }

    /**
     * Generates the 'setAttributes' operation needed to set the explicit
     * formatting attributes of the passed element node. If the passed node
     * does not contain any explicit attributes, no operation will be generated.
     *
     * @param {HTMLElement|jQuery} element
     *  The element node whose formatting attributes will be converted to an
     *  operation. If this object is a jQuery collection, uses the first node
     *  it contains.
     *
     * @param {Object|Null} [properties]
     *  Additional properties that will be stored in the operation.
     *
     * @param {Object} [options]
     *  Optional parameters. Supports all options of the public method
     *  OperationGenerator.generateOperationWithTarget(), and the following
     *  additional options:
     *  @param {String} [options.clearFamily]
     *      If specified, a style family for which additional formatting
     *      attributes with null values will be inserted into the operation.
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute set of the generated operation.
     *  @param {Boolean} [options.allAttributes=false]
     *      If true, merged attributes will be used instead of explicit
     *      attributes.
     */
    generateSetAttributesOperation(element, properties, options) {

        // explicit or merged attributes of the passed node
        let elementAttributes = null;
        // the style families for generated null attributes
        const clearFamily = options && options.clearFamily;
        // a specified property, that shall be ignored in the setAttributes operation
        const ignoreFamily = options && options.ignoreFamily;
        // a specified property, that determines whether to use merged attributes instead of explicit attributes
        const allAttributes = options && options.allAttributes;

        if (allAttributes && this.#isPlaceHolderDrawing(element)) {
            // merged attributes of the passed node
            elementAttributes = this.docModel.drawingStyles.getElementAttributes(element);

        } else if (allAttributes && this.#isPlaceHolderDrawing($(element).parents(NODE_SELECTOR))) {
            // merged attributes of the passed node
            if (DOM.isParagraphNode(element)) {
                elementAttributes = this.docModel.paragraphStyles.getElementAttributes(element, { skipPoolDefaults: true });
            } else if (DOM.isTextSpan(element)) {
                // for text spans only the explicit attributes must be copied/pasted. Otherwise after pasting
                // the inheritance from template placeholder drawings is broken (DOCS-2214).
                elementAttributes = getExplicitAttributeSet(element);
            }

        } else {
            // explicit attributes of the passed node
            elementAttributes = getExplicitAttributeSet(element);
        }

        // insert null values for all attributes registered for the specified style family
        let attributeSet = null;
        if (clearFamily) {
            const styleCollection = this.docModel.getStyleCollection(clearFamily);
            attributeSet = styleCollection.buildNullAttributeSet({ style: true });
        } else {
            attributeSet = {};
        }

        // merge the explicit attributes of the passed element
        this._attrPool.extendAttrSet(attributeSet, elementAttributes);

        // deleting the specified families from the attributes
        if (attributeSet && ignoreFamily) {
            const allFamilies = _.keys(attributeSet);
            _.each(ignoreFamily.split(' '), oneFamily => {
                if (_.contains(allFamilies, oneFamily)) { delete attributeSet[oneFamily]; }
            });
        }

        // no attributes, no operation
        if (!_.isEmpty(attributeSet)) {
            properties = _.extend({}, properties, { attrs: attributeSet });
            this.generateOperationWithTarget(Op.SET_ATTRIBUTES, properties, options);
        }
    }

    /**
     * Generates all operations needed to recreate the child nodes of the
     * passed paragraph.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph element whose content nodes will be converted to
     *  operations. If this object is a jQuery collection, uses the first node
     *  it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed paragraph node. The generated
     *  operations will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Number} [options.start]
     *      The logical index of the first character to be included into the
     *      generated operations. By default, operations will include all
     *      contents from the beginning of the paragraph.
     *  @param {Number} [options.end]
     *      The logical index of the last character to be included into the
     *      generated operations (closed range). By default, operations will
     *      include all contents up to the end of the paragraph.
     *  @param {Boolean} [options.clear=false]
     *      If set to true, a 'setAttributes' operation will be generated for
     *      the first 'insertText' operation that clears all character
     *      attributes of the inserted text. This prevents that applying the
     *      operations at another place in the document clones the character
     *      formatting of the target position.
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {Number} [options.targetOffset]
     *      If set to a number, the logical positions in the operations
     *      generated for the child nodes will start at this offset. If
     *      omitted, the original node offset will be used in the logical
     *      positions.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     *  @param {Boolean} [options.getDrawingListStyles=false]
     *      If specified, for drawings inside paragraph list styles will be
     *      fetched.
     *  @param {Boolean} [options.getNewDrawingId=false]
     *      If specified, for drawings new drawing IDs are generated.
     *  @param {Boolean} [options.allAttributes=false]
     *      If true, merged attributes will be used instead of explicit
     *      attributes.
     */
    generateParagraphChildOperations(paragraph, position, options) {

        // start of text range to be included in the operations
        const rangeStart = getIntegerOption(options, 'start');
        // end of text range to be included in the operations
        const rangeEnd = getIntegerOption(options, 'end');
        // start position of text nodes in the generated operations
        let targetOffset = getIntegerOption(options, 'targetOffset');
        // used to merge several text portions into the same operation
        let lastTextOperation = null;
        // a specified property, that shall be ignored in the setAttributes operation
        const ignoreFamily = getStringOption(options, 'ignoreFamily', '');
        // a specified property, that shall send target string with operation
        const target = getStringOption(options, 'target', '');
        // fetch list style properties for drawings
        const getDrawingListStyles = getBooleanOption(options, 'getDrawingListStyles', false);
        // a specified property, that determines whether to use merged attributes instead of explicit attributes
        const allAttributes = getBooleanOption(options, 'allAttributes', false);
        // a specified property, that determines whether to use merged attributes instead of explicit attributes
        const clearAttributes = getBooleanOption(options, 'clear', false);
        // formatting ranges for text portions, must be applied after the contents
        let attributeRanges = [];
        // attributes passed to the first insert operation to clear all formatting
        let allClearAttributes = null;

        // adding character attributes that need to be removed
        const resetCharacterAttributes = (span, insertOperation) => {

            // not simply removing all character attributes, but only those, that are really set in the spans (55504).
            _.each(getExplicitAttributes(span, 'character'), (_value, oneAttribute) => {
                insertOperation.attrs = insertOperation.attrs || {};
                insertOperation.attrs.character = insertOperation.attrs.character || {};
                insertOperation.attrs.character[oneAttribute] = null;
            });
        };

        // collecting those character attributes, that need to be cleared
        const getSpecificCharacterClearAttributes = (span, isSpanStart) => {

            // not simply removing all character attributes, but only those, that are really set in the span and optinally the previous span (55504).
            let requiredAttrs = _.keys(getExplicitAttributes(span, 'character'));

            if (isSpanStart && span.previousSibling) { // also remove character attributes from previous span (if it exists)
                requiredAttrs = requiredAttrs.concat(_.keys(getExplicitAttributes(span.previousSibling, 'character')));
            }

            const newCharacterAttributes = requiredAttrs.length > 0 ? {} :  null;
            _.each(requiredAttrs, oneAttribute => {
                newCharacterAttributes[oneAttribute] = null;
            });

            if (!allClearAttributes) { allClearAttributes = {}; }

            if (newCharacterAttributes) {
                allClearAttributes.character = newCharacterAttributes;
            } else {
                delete allClearAttributes.character;
            }

            return allClearAttributes;
        };

        // generates the specified operation, adds that attributes in clearAttributes on first call
        const generateOperationWithClearAttributes = (name, properties, textSpan, isSpanStart) => {

            // add the character attributes that will be cleared on first insertion operation
            if (clearAttributes) {
                properties.attrs = getSpecificCharacterClearAttributes(textSpan, isSpanStart);
                allClearAttributes = null;
            }

            return this.generateOperationWithTarget(name, properties, options);
        };

        // merging neighbouring ranges, so that less operations are generated (DOCS-4163)
        const combineAttributeRanges = allRanges => {

            const combinedRanges = [];

            allRanges.forEach(nextRange => {

                const lastRange = _.last(combinedRanges);

                if (lastRange) {

                    const lastEndPostion = lastRange.position.end ? lastRange.position.end : lastRange.position.start;

                    if (_.isEqual(nextRange.position.start, increaseLastIndex(lastEndPostion))) {

                        const lastAttrs = getExplicitAttributeSet(lastRange.node);
                        const nextAttrs = getExplicitAttributeSet(nextRange.node);

                        // ignoring the spellerror attribute
                        if (lastAttrs?.character?.spellerror) { delete lastAttrs?.character?.spellerror; }
                        if (nextAttrs?.character?.spellerror) { delete nextAttrs?.character?.spellerror; }

                        if (_.isEqual(lastAttrs, nextAttrs)) {
                            // expanding the already existing range
                            lastRange.position.end = nextRange.position.end ? nextRange.position.end : nextRange.position.start;
                            // do not add the new range
                            return;
                        }
                    }
                }

                combinedRanges.push(nextRange);
            });

            return combinedRanges;
        };

        // clear all attributes of the first inserted text span
        if (clearAttributes) {
            allClearAttributes = this.docModel.characterStyles.buildNullAttributeSet({ style: true });
        }

        // process all content nodes in the paragraph and create operations
        iterateParagraphChildNodes(paragraph, (node, nodeStart, _nodeLength, offsetStart, offsetLength) => {

            // logical start index of the covered part of the child node
            const startIndex = _.isNumber(targetOffset) ? targetOffset : (nodeStart + offsetStart);
            // logical end index of the covered part of the child node (closed range)
            const endIndex = startIndex + offsetLength - 1;
            // logical start position of the covered part of the child node
            const startPosition = appendNewIndex(position, startIndex);
            // logical end position of the covered part of the child node
            const endPosition = appendNewIndex(position, endIndex);
            // text of a portion span
            let text = null;
            // type of the drawing or the hard break or the field or the range marker
            let type = null;
            // id and position of a text marker range
            let rangeId = null, rangePos = null;
            // whether the clearing of all attributes is required
            let isSpanStart = false;
            // the field attributes
            const attrs = {};

            // operation to create a (non-empty) generic text portion
            if (DOM.isTextSpan(node)) {

                // nothing to do for template texts in empty text frames
                if (DOM.isTextFrameTemplateTextSpan(node)) { return; }

                // extract the text covered by the specified range
                text = node.firstChild.nodeValue.substr(offsetStart, offsetLength);
                // append text portions to the last 'insertText' operation
                if (lastTextOperation) {
                    lastTextOperation.text += text;
                    if (clearAttributes) { resetCharacterAttributes(node, lastTextOperation); } // also removing attributes
                } else {
                    if (text.length > 0) {
                        isSpanStart = offsetStart === 0; // clearing of attributes only required for first character in text span
                        text = text.replace(/\xa0/g, ' ');  // DOCS-2134, replacing non breakable spaces
                        lastTextOperation = generateOperationWithClearAttributes(Op.TEXT_INSERT, { start: _.copy(startPosition), text }, node, isSpanStart);
                    }
                }
                attributeRanges.push({ node, position: _.extend({ start: _.copy(startPosition) }, (text.length > 1) ? { end: _.copy(endPosition) } : null) });

            } else {

                // anything else than plain text will be inserted, forget last text operation
                lastTextOperation = null;

                // operation to create a text field
                if (DOM.isFieldNode(node)) {
                    // extract text of all embedded spans representing the field
                    text = $(node).text();
                    attrs.character = {};
                    attrs.character.field = {};
                    $.each($(node).data(), (name, value) => {
                        if (name !== 'type') {
                            if (name !== 'sFieldId' && name !== 'attributes') { // #45332 - dont send unnecessary and invalid properties
                                attrs.character.field[name] = value;
                            }
                        } else {
                            type = value;
                        }
                    });
                    this.generateOperationWithTarget(Op.FIELD_INSERT, { start: _.copy(startPosition), representation: text, type, attrs }, options);
                    // attributes are contained in the embedded span elements
                    attributeRanges.push({ node: node.firstChild, position: { start: _.copy(startPosition) } });

                // operation to create a tabulator
                } else if (DOM.isTabNode(node)) {
                    generateOperationWithClearAttributes(Op.TAB_INSERT, { start: _.copy(startPosition) });
                    // attributes are contained in the embedded span elements
                    attributeRanges.push({ node: node.firstChild, position: { start: _.copy(startPosition) } });

                // operation to create a hard break
                } else if (DOM.isHardBreakNode(node)) {
                    // reading type of hard break from the node
                    type = $(node).data('type');
                    generateOperationWithClearAttributes(Op.HARDBREAK_INSERT, { start: _.copy(startPosition), type });
                    // attributes are contained in the embedded span elements
                    attributeRanges.push({ node: node.firstChild, position: { start: _.copy(startPosition) } });

                // operation to create a drawing (including its attributes)
                } else if (isDrawingFrame(node) || DOM.isDrawingPlaceHolderNode(node)) {
                    // switching to node in drawing layer
                    if (DOM.isDrawingPlaceHolderNode(node)) { node = DOM.getDrawingPlaceHolderNode(node); }
                    // skip drawing nodes that cannot be restored (inserting one space because of position counting inside paragraph)
                    // -> but never insert a text direct into a slide
                    if (DOM.isUnrestorableDrawingNode(node) && !this._slideMode) {
                        generateOperationWithClearAttributes(Op.TEXT_INSERT, { start: _.copy(startPosition), text: ' ' });
                        return;
                    }
                    // generate operations for the drawing
                    const drawingOptions = { ignoreFamily, getDrawingListStyles };
                    drawingOptions.target = getStringOption(options, 'target', null);
                    drawingOptions.getNewDrawingId = getBooleanOption(options, 'getNewDrawingId', false);
                    drawingOptions.drawingIdsTransTable = options.drawingIdsTransTable;
                    this.generateDrawingOperations(node, _.copy(startPosition), drawingOptions);

                } else if (DOM.isCommentPlaceHolderNode(node)) {
                    // generate operations for the drawing
                    this.generateCommentOperations(node, _.copy(startPosition), { ignoreFamily, target });

                } else if (DOM.isRangeMarkerNode(node)) {

                    rangeId = DOM.getRangeMarkerId(node);
                    rangePos = DOM.isRangeMarkerStartNode(node) ? 'start' : (DOM.isRangeMarkerEndNode(node) ? 'end' : undefined);
                    type = DOM.getRangeMarkerType(node);
                    this.generateOperationWithTarget(Op.RANGE_INSERT, { start: _.copy(startPosition), id: rangeId, type, position: rangePos }, options);
                    attributeRanges.push({ node, position: { start: _.copy(startPosition) } });

                } else if (DOM.isComplexFieldNode(node)) {
                    const isSpecialField = DOM.isSpecialField(node);
                    let includeComplexField = true; // check, whether the delete operation includes the (special) complex field
                    if (isSpecialField && _.isNumber(rangeStart) && _.isNumber(nodeStart) && rangeStart > nodeStart && rangeStart === startIndex) { includeComplexField = false; } // only the text inside the complex field is affected by this delete operation
                    if (includeComplexField) { this.generateOperationWithTarget(Op.COMPLEXFIELD_INSERT, { start: _.copy(startPosition), instruction: DOM.getComplexFieldInstruction(node) }, options); }
                    // special fields have content inside complex field div node, and lenght for oxo positioning is stored in data as "length"
                    if (isSpecialField) {
                        let fieldTxtLen = $(node).data('length');
                        fieldTxtLen = (_.isNumber(fieldTxtLen) && fieldTxtLen > 0) ? (fieldTxtLen - 1) : 1;
                        const textStart = includeComplexField ? increaseLastIndex(startPosition, 1) : startPosition;
                        this.generateOperationWithTarget(Op.TEXT_INSERT, { start: _.copy(textStart), text: '1'.repeat(fieldTxtLen) }, options);
                    }
                    attributeRanges.push({ node, position: { start: _.copy(startPosition) } });
                } else if (DOM.isBookmarkNode(node)) {
                    this.generateOperationWithTarget(Op.BOOKMARK_INSERT, { start: _.copy(startPosition), id: DOM.getBookmarkId(node), anchorName: DOM.getBookmarkAnchor(node), position: DOM.getBookmarkPosition(node) }, options);
                } else {
                    globalLogger.error('TextOperationGenerator.generateParagraphChildOperations(): unknown content node');
                    return BREAK;
                }
            }

            // custom target offset: advance offset by covered node length
            if (_.isNumber(targetOffset)) {
                targetOffset += offsetLength;
            }

        }, this, { start: rangeStart, end: rangeEnd });

        // if it's presentation paragraph inside placeholder drawing and it has only template text, generate possible character attrs
        if (!DOM.isSlideNode(paragraph) && this.#isPlaceHolderDrawing($(paragraph).parents(NODE_SELECTOR))) {
            const $templateTextSpan = $(paragraph).find(DOM.TEMPLATE_TEXT_SELECTOR);
            if ($templateTextSpan.length) {
                attributeRanges.push({ node: $templateTextSpan, position: { start: _.copy(position) } });
            }
        }

        // Generate 'setAttribute' operations after all contents have been
        // created via 'insertText', 'insertField', etc. Otherwise, these
        // operations would clone the attributes of the last text portion
        // instead of creating a clean text node as expected in this case.
        const attrOptions = { ignoreFamily, allAttributes };
        attrOptions.target = getStringOption(options, 'target', null);
        if (!ignoreFamily && !allAttributes) { attributeRanges = combineAttributeRanges(attributeRanges); }
        attributeRanges.forEach(range => {
            this.generateSetAttributesOperation(range.node, range.position, attrOptions);
        });
    }

    /**
     * Generates all operations needed to recreate the passed paragraph.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph element whose contents will be converted to operations.
     *  If this object is a jQuery collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed paragraph node. The generated
     *  operations will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     *  @param {Boolean} [options.allAttributes=false]
     *      If true, merged attributes will be used instead of explicit
     *      attributes.
     *  @param {Boolean} [options.doNotCreateParagraphChildOperations=false]
     *      If true operations for paragraph childs are not generated
     */
    generateParagraphOperations(paragraph, position, options) {

        // a locally used options object
        let localOptions = null;
        // the options for the operation
        let operationOptions = { start: _.copy(position) };
        // the name of the operation
        let operationName = Op.PARA_INSERT;
        // an object collecting data for the operation
        let operationInfo = null;
        // the element attributes
        let elementAttrs = null;
        // a specified property, that shall be ignored in the setAttributes operation
        const ignoreFamily = options && options.ignoreFamily;
        // a specified property, that determines whether to use merged attributes instead of explicit attributes
        const allAttributes = options && options.allAttributes;

        if (!options) { options = {}; }

        // operations to create the paragraph element and formatting
        if (DOM.isSlideNode(paragraph)) {
            // getting the target for the operation
            if ($(paragraph).data('target')) {
                localOptions = _.clone(options);
                localOptions.target = $(paragraph).data('target'); // only setting target for Operations.SLIDE_INSERT
            } else {
                localOptions = {};
            }
            if (is.function(this.docModel.getInsertOperationNameForNode)) {
                operationInfo = this.docModel.getInsertOperationNameForNode(paragraph, localOptions);
                operationName = operationInfo.name;
                if (_.isNumber(operationInfo.index)) { operationOptions.start = operationInfo.index; }
                if (localOptions.id) { operationOptions.id = localOptions.id; } // reusing the node ID, if possible
            }
            this.generateOperationWithAttributes(paragraph, operationName, operationOptions, localOptions);

            if (operationInfo?.insertThemeOp) { this.insertOperations(operationInfo.insertThemeOp); } // restoring also an optional insertTheme operation
        } else {
            options.isParagraph = true;

            // explicit attributes do not represent the entire attribute set of paragraphs inside placeholder drawings, so use merged element attributes instead
            if (allAttributes && this.#isPlaceHolderDrawing($(paragraph).parents(NODE_SELECTOR))) {
                elementAttrs = this.docModel.paragraphStyles.getElementAttributes(paragraph, { skipPoolDefaults: true });
                if (!_.isEmpty(elementAttrs)) {
                    // deleting the specified families from the attributes
                    if (ignoreFamily) {
                        _.each(ignoreFamily.split(' '), oneFamily => {
                            if (elementAttrs[oneFamily]) { delete elementAttrs[oneFamily]; }
                        });
                    }

                    operationOptions = _.extend({ attrs: elementAttrs }, operationOptions);
                }
            }

            this.generateOperationWithAttributes(paragraph, operationName, operationOptions, options);
        }

        // process all content nodes in the paragraph and create operations
        if (!options.doNotCreateParagraphChildOperations) {
            this.generateParagraphChildOperations(paragraph, position, options);
        }
    }

    /**
     * Generates all operations needed to recreate the passed table cell.
     *
     * @param {HTMLTableCellElement|jQuery} cellNode
     *  The table cell element that will be converted to operations. If this
     *  object is a jQuery collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed table cell. The generated operations
     *  will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     */
    generateTableCellOperations(cellNode, position, options) {

        // operation to create the table cell element
        this.generateOperationWithAttributes(cellNode, Op.CELLS_INSERT, { start: _.copy(position), count: 1 }, options);

        // generate operations for the contents of the cell
        this.generateContentOperations(cellNode, position, options);
    }

    /**
     * Generates all operations needed to recreate the passed table row.
     *
     * @param {HTMLTableRowElement|jQuery} rowNode
     *  The table row element that will be converted to operations. If this
     *  object is a jQuery collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed table row. The generated operations
     *  will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     */
    generateTableRowOperations(rowNode, position, options) {

        // operation to create the table row element
        this.generateOperationWithAttributes(rowNode, Op.ROWS_INSERT, { start: _.copy(position) }, options);

        // generate operations for all cells
        position = appendNewIndex(position);
        iterateSelectedDescendantNodes(rowNode, 'td', cellNode => {
            this.generateTableCellOperations(cellNode, position, options);
            position = increaseLastIndex(position);
        }, this, { children: true });
    }

    /**
     * Generates all operations needed to recreate the passed table.
     *
     * @param {HTMLTableElement|jQuery} tableNode
     *  The table element that will be converted to operations. If this object
     *  is a jQuery collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed table node. The generated operations
     *  will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     */
    generateTableOperations(tableNode, position, options) {

        // check if current table node has autoWidth
        let isAutoWidth;
        // check if current node is inside another table cell
        let isInnerTable;
        // whether the table node is a table node inside a drawing of type 'table'
        const isTableDrawing = DOM.isTableNodeInTableDrawing(tableNode);

        // [issue: 63852] - only in .odt file inner table breaks after deletion of the containing row, then undo to restore;
        if (this.docModel.docApp.isODF()) {
            isAutoWidth = tableNode.style.width === '100%';
            isInnerTable = tableNode.parentNode && DOM.isCellContentNode(tableNode.parentNode); // tableNode.parentNode.className.indexOf('cellcontent') > -1;

            if (isAutoWidth && isInnerTable) {
                tableNode.odfAutoWidth = true;
            }
        }

        // operation to create the table element (not necessary for drawings of type 'table')
        if (!isTableDrawing) {
            this.generateOperationWithAttributes(tableNode, Op.TABLE_INSERT, { start: _.copy(position) }, options);
            position = appendNewIndex(position);
        }

        // generate operations for all rows
        DOM.getTableRows(tableNode).get().forEach(node => {
            this.generateTableRowOperations(node, position, options);
            position = increaseLastIndex(position);
        }, this);
    }

    /**
     * Generates all operations needed to recreate the passed drawing group.
     *
     * @param {HTMLElement|jQuery} drawingNode
     *  The drawing element whose contents will be converted to operations. If
     *  this object is a jQuery collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed drawing node. The generated
     *  operations will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     *  @param {Boolean} [options.allAttributes=false]
     *      If true, merged attributes will be used instead of explicit
     *      attributes.
     *  @param {Boolean} [options.getDrawingListStyles=false]
     *      If specified, for drawings inside paragraph list styles will be
     *      fetched.
     *  @param {Boolean} [options.getNewDrawingId=false]
     *      If set to true, new id for the drawing will be created.
     *  @param {String[][]]} [options.drawingIdsTransTable]
     *      A translation table for drawing IDs, that is filled within this
     *      operation generation process and can be evaluated from the caller.
     */
    generateDrawingOperations(drawingNode, position, options) {

        // the type of the drawing
        const type = getDrawingType(drawingNode);
        // the operation options
        let operationOptions = { start: _.copy(position), type };
        // the element attributes
        let elementAttrs = null;
        // a specified property, that shall be ignored in the setAttributes operation
        let ignoreFamily = getStringOption(options, 'ignoreFamily', '');
        // a specified property, that determines whether to use merged attributes instead of explicit attributes
        const allAttributes = options && options.allAttributes;

        if (type === 'image') {
            if (allAttributes && this.#isPlaceHolderDrawing(drawingNode)) {
                elementAttrs = this.docModel.drawingStyles.getElementAttributes(drawingNode);
                if (elementAttrs) {
                    ignoreFamily += ' shape geometry'; // no 'shape' and 'geometry' attributes for images
                    // deleting the specified families from the attributes
                    if (ignoreFamily.length > 0) {
                        _.each(ignoreFamily.split(' '), oneFamily => {
                            if (elementAttrs[oneFamily]) { delete elementAttrs[oneFamily]; }
                        });
                    }
                    operationOptions = _.extend({ attrs: elementAttrs }, operationOptions);
                }
            }

            // special image handling: We need to take care that images are always readable for the target instance
            this.generateImageOperationWithAttributes(drawingNode, operationOptions, options);
            return;
        }

        // explicit attributes do not represent the entire attribute set of placeholder drawings, so use merged element attributes instead
        if (allAttributes && (type === 'shape') && this.#isPlaceHolderDrawing(drawingNode)) {
            elementAttrs = this.docModel.drawingStyles.getElementAttributes(drawingNode, { skipPoolDefaults: true });
            if (!_.isEmpty(elementAttrs)) {
                // deleting the specified families from the attributes
                if (ignoreFamily.length > 0) {
                    _.each(ignoreFamily.split(' '), oneFamily => {
                        if (elementAttrs[oneFamily]) { delete elementAttrs[oneFamily]; }
                    });
                }
                operationOptions = _.extend({ attrs: elementAttrs }, operationOptions);
            }
        }
        if (this._slideMode && options && options.getDrawingListStyles) {
            // fetch list styles of the drawing
            options.drawingListStyles = this.docModel.drawingStyles.getAllListStylesAtDrawing(drawingNode);
        }

        this.generateOperationWithAttributes(drawingNode, Op.INSERT_DRAWING, operationOptions, options);

        // the drawing list styles shall not be used for generating the content of the drawing (for example for paragraphs)
        if (options.drawingListStyles) { delete options.drawingListStyles; }

        if (type === 'group') {
            // generate operations for the contents of the text frame
            this.generateDrawingGroupChildOperations(drawingNode, _.copy(position), options);
        } else if (((type === 'shape' || type === 'connector') && isTextFrameShapeDrawingFrame(drawingNode)) || type === 'table') {
            // generate operations for the contents of the text frame
            this.generateContentOperations(drawingNode, _.copy(position), options);
        }
    }

    /**
     * Generates all operations needed to recreate the passed comment.
     *
     * @param {HTMLElement|jQuery} commentNode
     *  The comment placeholder node whose contents will be converted to operations. If
     *  this object is a jQuery collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed drawing node. The generated
     *  operations will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     */
    generateCommentOperations(commentNode, position, options) {

        // restoring author, date and id
        const id = DOM.getTargetContainerId(commentNode);
        const commentCollection = this.docModel.getCommentCollection();
        const commentModel = commentCollection.getById(id);
        const commentAuthor = commentModel.getAuthor();
        const operationOptions = { start: _.copy(position), id, author: commentAuthor };

        const userId = commentModel.getAuthorId();
        const date = commentModel.getDate();
        const parentId = commentModel.getParentId();
        const providerId = commentModel.getAuthorProvider();
        const restorable = commentModel.isRestorable();

        if (parentId) { operationOptions.parentId = parentId; } /// also using parentId in ODF, if the value is available
        if (date) { operationOptions.date = date; } // date must be specified or ignored, but no empty string, 55221
        if (userId !== '') { operationOptions.userId = userId; }
        if (restorable === false) { operationOptions.restorable = false; }
        if (providerId) { operationOptions.providerId = providerId; }

        if (commentModel.getText()) { operationOptions.text = commentModel.getText(); }
        if (commentModel.getMentions()) { operationOptions.mentions = commentModel.getMentions(); }
        if (!operationOptions.text) { operationOptions.text = ''; } // insertComment must contain text property (DOCS-2655)

        // saving all current children at the insertComment operation (DOCS-2684)
        if (!commentModel.isReply() && commentCollection.hasChild(commentModel)) { operationOptions.children = commentCollection.getAllChildIds(id); }

        this.generateOperationWithAttributes(commentNode, Op.COMMENT_INSERT, operationOptions, options);
    }

    /**
     * Generates a usable image operation for an internal & external URL.
     *
     * @param {HTMLElement|jQuery} imageNode
     *  The image drawing element. If this object is a jQuery collection,
     *  uses the first node it contains.
     *
     * @param {Object} [operationOptions]
     *  Additional options that will be stored in the operation.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     *
     * @returns {Operation}
     *  The created operation JSON object.
     */
    generateImageOperationWithAttributes(imageNode, operationOptions, options) {

        // the new operation
        const operation = this.generateOperationWithAttributes(imageNode, Op.INSERT_DRAWING, _.extend({ type: 'image' }, operationOptions), options);
        // the image URL from the attributes map
        const imageUrl = operation.attrs && operation.attrs.image && operation.attrs.image.imageUrl;

        // set image attributes so they can be used in the same instance or between different instances
        if (imageUrl && DOM.isDocumentImageNode(imageNode)) {
            _.extend(operation.attrs.image, {
                imageData: DOM.getBase64FromImageNode(imageNode, getMimeTypeFromImageUri(imageUrl)),
                sessionId: ox.session,
                fileId: DOM.getUrlParamFromImageNode(imageNode, 'id')
            });
        }

        return operation;
    }

    /**
     * Generates all operations needed to recreate the children of the passed
     * drawing group.
     *
     * @param {HTMLElement|jQuery} drawingGroupNode
     *  The drawing group element whose contents will be converted to
     *  operations. If this object is a jQuery collection, uses the first node
     *  it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed drawing group node. The generated
     *  operations will contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     */
    generateDrawingGroupChildOperations(drawingGroupNode, position, options) {

        // generate operations for all drawings inside the drawing group
        position = appendNewIndex(position);
        iterateSelectedDescendantNodes($(drawingGroupNode).children().first(), '.drawing', drawingNode => {
            this.generateDrawingOperations(drawingNode, position, options);
            position = increaseLastIndex(position);
        }, this, { children: true });
    }

    /**
     * Generates all operations needed to recreate the contents of the passed
     * root node. Root nodes are container elements for text paragraphs and
     * other first-level content nodes (e.g. tables). Examples for root nodes
     * are the entire document root node, table cells, or text shapes. Note
     * that the operation to create the root node itself will NOT be generated.
     *
     * @param {HTMLElement|jQuery} rootNode
     *  The root node containing the content nodes that will be converted to
     *  operations. The passed root node may contain an embedded node that
     *  serves as parent for all content nodes. If this object is a jQuery
     *  collection, uses the first node it contains.
     *
     * @param {Array<Number>} position
     *  The logical position of the passed node. The generated operations will
     *  contain positions starting with this address.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.ignoreFamily]
     *      A space-separated list of families that will not be inserted into
     *      the attribute sets of the generated operations.
     *  @param {String} [options.target]
     *      If specified, this property will be sent as target with operation.
     */
    generateContentOperations(rootNode, position, options) {

        // the container node (direct parent of the target content nodes)
        const containerNode = DOM.getChildContainerNode(rootNode);

        // iterate all child elements of the root node and create operations
        position = appendNewIndex(position);
        iterateDescendantNodes(containerNode, node => {

            if (DOM.isParagraphNode(node)) {
                // skip implicit paragraph nodes without increasing the position
                if (DOM.isImplicitParagraphNode(node)) { return; }
                // operations to create a paragraph
                this.generateParagraphOperations(node, position, options);
            } else if (DOM.isTableNode(node)) {
                // skip table nodes with exceeded size without increasing the position
                if (DOM.isExceededSizeTableNode(node)) { return; }
                // operations to create a table with its structure and contents
                this.generateTableOperations(node, position, options);
            } else if (DOM.isMarginalDrawingLayerNode(node)) {
                // skip over text drawing layer in header/footer
            } else {
                globalLogger.error('TextOperationGenerator.generateContentOperations(): unexpected node "' + getNodeName(node) + '" at position ' + JSON.stringify(position) + '.');
                // continue with next child node (do not increase position)
                return;
            }

            // increase last element of the logical position (returns a clone)
            position = increaseLastIndex(position);

        }, this, { children: true });
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether the passed DOM drawing frame is a placeholder that will
     * not be processed by this generator.
     *
     * @param {jQuery|HTMLElement} drawingFrame
     *  The DOM drawing frame to be checked.
     *
     * @returns {Boolean}
     *  Whether the passed DOM drawing frame is a placeholder.
     */
    #isPlaceHolderDrawing(drawingFrame) {
        return !!this.docModel.isPlaceHolderDrawing && this.docModel.isPlaceHolderDrawing(drawingFrame);
    }
}
