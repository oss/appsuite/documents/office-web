/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';

import { fun, math } from '@/io.ox/office/tk/algorithms';
import { IOS_SAFARI_DEVICE, containsNode, getFocus, setFocus, clearBrowserSelection } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { applyImageTransformation, hasUrlImageExtension, insertDataUrl, insertURL, isBase64, isImageMimeType, isSupportedImageMimeType } from '@/io.ox/office/drawinglayer/utils/imageutils';

import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { getElementStyleId, getExplicitAttributes, getExplicitAttributeSet, isColorThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import { opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';
import { getWidthForPreset } from '@/io.ox/office/editframework/utils/border';
import { hasSupportedProtocol, isValidURL } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { StyleCache } from '@/io.ox/office/editframework/model/operation/stylecache';

import { CLEAR_ATTRIBUTES } from '@/io.ox/office/textframework/components/hyperlink/hyperlink';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';
import { ABSOLUTE_DRAWING_SELECTOR, PARAGRAPH_NODE_SELECTOR, TABLE_CELLNODE_SELECTOR, getDrawingPlaceHolderNode,
    getTableRows, isCommentTextframeNode, isDrawingPlaceHolderNode, isExceededSizeTableNode, isImplicitParagraphNode, isInsideCommentLayerNode,
    isListLabelNode, isPageNode, isParagraphNode, isTableNode } from '@/io.ox/office/textframework/utils/dom';
import Export from '@/io.ox/office/textframework/utils/export';
import * as Op from '@/io.ox/office/textframework/utils/operations';
import { appendNewIndex, decreaseLastIndex, getDOMPosition, getLastNodeFromPositionByNodeName, getOxoPosition, getOxoPositionFromPixelPosition,
    getParagraphElement, getParagraphLength, getParagraphNodeLength, getSelectedElement, increaseLastIndex, isParagraphPosition, isPositionInsideTextframe,
    isValidPositionOrder } from '@/io.ox/office/textframework/utils/position';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import { BREAK, convertLengthToHmm, extendOptions, findNextNode, getBooleanOption, getClipboardData,
    getCSSPositionFromPoint, getNodeName, getObjectOption, insertHiddenNodes, parseAndSanitizeHTML,
    removeFalsyItemsInArray } from '@/io.ox/office/textframework/utils/textutils';

// constants ==============================================================

// attribute families that support color attributes
const COLOR_SUPPORT_ATTRIBUTE_FAMILIES = ['character', 'paragraph', 'fill', 'line', 'table', 'cell'];

// attribute families that don't support color attributes
const NON_COLOR_SUPPORT_ATTRIBUTE_FAMILIES = ['page', 'document', 'changes', 'drawing', 'connector', 'shape', 'geometry', 'chart', 'series', 'axis', 'legend', 'row', 'image', 'text'];

// a 1x1 pixel placeholder image
const PLACEHOLDER_IMAGE_URL = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
// drawing attributes for a 1x1 pixel placeholder image
const PLACEHOLDER_IMAGE_DRAWING_ATTRIBUTES = { drawing: { width: 1, height: 1 }, image: { imageData: PLACEHOLDER_IMAGE_URL } };

function mixinClipBoardHandler(BaseClass) {

    // mix-in class ClipBoardHandlerMixin ======================================

    /**
     * A mix-in class for the clip board specific functions.
     */

    class ClipBoardHandlerMixin extends BaseClass {

        // the next free list style number part
        #listStyleNumber = 1;
        // a global generator that holds all delete operations to finally remove the spaces added by replacement operations
        #deleteReplacementsGenerator = null;
        // the application object
        #docApp = null;
        // the handler for the 'dragover' event
        processDragOver = $.noop;
        // determines whether the origin of the clipboard data is OX Text or OX Presentation
        #clipboardOrigin = '';
        // an id to identify the origin of the clipboard data
        #clipboardOriginId = '';

        constructor(docApp, ...baseCtorArgs) {

            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;

            /**
             * Handler for 'dragover' event.
             * - draws a overlay caret on the 'dragover' event, which indicates in which position
             * the dragged content should be inserted to.
             *
             * @param {jQuery.Event} event
             *  The browser event sent via dragOver
             */
            this.processDragOver = fun.do(event => { // TODO DOCS-4959 -> alternative to constructor?

                // the dragOver event
                let dragOverEvent = event;

                const directCallback = event => {
                    dragOverEvent = event;
                    dragOverEvent.originalEvent.dataTransfer.dropEffect = 'copy';
                };

                const deferredCallback = () => {

                    // the page node
                    const pageNode = this.getNode();
                    let activeRootNode = pageNode; // at beginning active root is page node, if h&f is active, change it later

                    // inactive selection in header&footer
                    if (dragOverEvent.target.classList.contains('inactive-selection') || $(dragOverEvent.target).parents('.inactive-selection').length > 0) {
                        return;
                    }
                    // header/footer edit state is active
                    if (this.isHeaderFooterEditState()) {
                        if (!dragOverEvent.target.classList.contains('marginal')) {
                            return; // return if target is outside of marginal root node
                        }
                        activeRootNode = this.getSelection().getRootNode(); // root node that is currently active, needed for getting DOM position
                    }

                    const caretOxoPosition = getOxoPositionFromPixelPosition(activeRootNode, dragOverEvent.originalEvent.clientX, dragOverEvent.originalEvent.clientY);
                    if (!caretOxoPosition || caretOxoPosition.start.length === 0) { return; }

                    const dropCaret = pageNode.find('.drop-caret');
                    const zoom = this.#docApp.getView().getZoomFactor() * 100;
                    const caretPoint = getDOMPosition(activeRootNode, caretOxoPosition.start);
                    const caretParent = caretPoint && caretPoint.node && caretPoint.node.parentNode;
                    const caretCoordinate = getCSSPositionFromPoint(caretPoint, pageNode, zoom);
                    let caretLineHeight;

                    // adapt drop caret height to the content it is hovering, and reposition the caret
                    if (caretParent) {
                        caretLineHeight = $(caretParent).css('lineHeight');
                        dropCaret.css('height', caretLineHeight);
                    }
                    if (caretCoordinate && _.isNumber(caretCoordinate.top) && _.isNumber(caretCoordinate.left)) {
                        dropCaret.css({
                            top: caretCoordinate.top + 'px',
                            left: caretCoordinate.left + 'px'
                        });
                    }

                    dropCaret.show();
                };

                return this.debounce(directCallback, deferredCallback, { delay: 50, maxDelay: 100 });
            });

        }

        // public methods -----------------------------------------------------

        /**
         * Getting the clipboard data origin.
         * Returns "io.ox/office/text", "io.ox/office/presentation" or an empty String.
         *
         * @returns {String}
         *  The clipboard data origin.
         */
        getClipboardOrigin() {
            return this.#clipboardOrigin;
        }

        /**
         * Returns true if the clipboard data origin is OX Text.
         *
         * @returns {Boolean}
         *  Whether the origin is OX Text.
         */
        isClipboardOriginText() {
            return (this.#clipboardOrigin.toLowerCase().indexOf('io.ox/office/text') !== -1);
        }

        /**
         * Returns true if the clipboard data origin format is ODF.
         *
         * @returns {Boolean}
         *  Whether the origin format is ODF.
         */
        isClipboardOriginODFFormat() {
            return (this.#clipboardOrigin.toLowerCase().indexOf('odf') !== -1);
        }

        /**
         * Returns true if the clipboard data origin is equal to current application clipboard origin.
         *
         * @returns {Boolean}
         *  Whether the clipboard origin is equal to current one.
         */
        isPasteToSameDocumentType() {
            return this.getClipboardOrigin() === this.#getCurrentClipboardOrigin();
        }

        /**
         * Returns true if the clipboard data origin is OX Presentation.
         *
         * @returns {Boolean}
         *  Whether the origin is OX Presentation.
         */
        isClipboardOriginPresentation() {
            return (this.#clipboardOrigin.toLowerCase().indexOf('io.ox/office/presentation') !== -1);
        }

        /**
         * Setting the clipbard data origin.
         *
         * @param {String} [origin]
         *  The clipboard data origin.
         */
        setClipboardOrigin(origin) {
            if (_.isString(origin) && !_.isEmpty(origin)) {
                this.#clipboardOrigin = origin;
            } else {
                this.#clipboardOrigin = this.#getCurrentClipboardOrigin();
            }
        }

        /**
         * Returns an id that identifies the origin of the clipboard data.
         *
         * @returns {String}
         *  The clipboard data origin id.
         */
        getClipboardOriginId() {
            return this.#clipboardOriginId;
        }

        /**
         * Set the id to identify the clipboard origin.
         *
         * @param {String} id
         *  The clipboard data origin id.
         */
        setClipboardOriginId(id) {
            if (_.isString(id) && !_.isEmpty(id)) {
                this.#clipboardOriginId = id;
            } else {
                this.#clipboardOriginId = this.#getCurrentClipboardOriginId();
            }
        }

        /**
         * Returns true if the clipboard copy and paste takes place on the same document.
         *
         * @returns {Boolean}
         *  Whether the copy and paste takes place on the same document.
         */
        isCopyPasteOnSameDocument() {
            return (this.getClipboardOriginId() === this.#getCurrentClipboardOriginId());
        }

        /**
         * Sets the browser focus to the clipboard node, and selects all its
         * contents.
         *
         * @param {jQuery} clipboardNode
         */
        grabClipboardFocus(clipboardNode) {

            // the browser selection
            const  selection = window.getSelection();
            // a browser selection range object
            let  docRange = null;

            // Clear the old browser selection.
            // Bug 28515, bug 28711: IE fails to clear the selection (and to modify
            // it afterwards), if it currently points to a DOM node that is not
            // visible anymore (e.g. the 'Show/hide side panel' button). Workaround
            // is to move focus to an editable DOM node which will cause IE to update
            // the browser selection object. The target container node cannot be used
            // for that, see comments above for bug 26283. Using another focusable
            // node (e.g. the body element) is not sufficient either. Interestingly,
            // even using the (editable) clipboard node does not work here. Setting
            // the new browser selection below will move the browser focus back to
            // the application pane.
            clearBrowserSelection();

            // set the browser selection
            try {
                docRange = window.document.createRange();
                docRange.setStart(clipboardNode[0], 0);
                docRange.setEnd(clipboardNode[0], clipboardNode[0].childNodes.length);
                selection.removeAllRanges();
                selection.addRange(docRange);
            } catch (ex) {
                globalLogger.error('ClipBoardHandlerMixin.grabClipboardFocus(): failed to select clipboard node: ' + ex);
            }
        }

        /**
         * Parses the clipboard div, creates operations and applies them asynchronously.
         *
         * @param {jQuery} clipboard
         *  The clipboard div.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the operations have been applied;
         *  or rejected, if edit rights where lost or applying is canceled.
         */
        parseHTMLClipboardAndCreateOperations(clipboard) {
            return this.#createOperationsFromExternalClipboard(this.parseClipboard(clipboard));
        }

        /**
         * Copies the current selection into the internal clipboard and deletes
         * the selection.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        cut(event) {

            // DOCS-3345: nothing to do, if the focus is inside the comment editor
            if (isCommentTextframeNode(getFocus())) { return $.when(); }

            // the clipboard event data
            const clipboardData = getClipboardData(event);
            // the selection object
            const selection = this.getSelection();

            // set the internal clipboard data and
            // add the data to the clipboard event if the browser supports the clipboard api
            this.copy(event);

            // if the browser supports the clipboard api, use the copy function to add data to the clipboard
            // the iPad supports the api, but cut doesn't work
            if (clipboardData && !IOS_SAFARI_DEVICE) {

                // prevent default cut handling for desktop browsers, but not for touch devices
                if (_.device('desktop')) {
                    event.preventDefault();
                }

                if (this.#isSlideCopied()) {
                    if (this.isSlideSelectionDeletable(this.getSlidePaneSelection())) {
                        this.deleteMultipleSlides(this.getSlidePaneSelection());
                    }
                    return;
                } else {
                    // delete current selection
                    return this.deleteSelected({ deleteKey: true }).done(() => {
                        selection.setTextSelection(selection.getStartPosition()); // setting the cursor position
                    });
                }
            }

            return this.executeDelayed(() => {
                if (this.#isSlideCopied()) {
                    if (this.isSlideSelectionDeletable(this.getSlidePaneSelection())) {
                        this.deleteMultipleSlides(this.getSlidePaneSelection());
                    }
                } else {
                    // focus and restore browser selection
                    selection.restoreBrowserSelection();
                    // delete restored selection
                    return this.deleteSelected({ deleteKey: true }).done(() => {
                        selection.setTextSelection(selection.getStartPosition()); // setting the cursor position
                    });
                }
            });
        }

        /**
         * Copies the current selection into the internal clipboard and
         * attaches the clipboard data to the copy event, if the browser
         * supports the clipboard api.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.forceOperationGeneration=false]
         *   Whether operations must be generated. This is currently only
         *   required for Testrunner tests (required after DOCS-2982).
         */
        copy(event, options) {

            // the clipboard event data
            const clipboardData = getClipboardData(event);
            // the clipboard operations
            let operations = null;
            // the selection object
            const selection = this.getSelection();
            // start of current selection
            const start = selection.getStartPosition();
            // end of current selection
            const end = selection.getEndPosition();
            // information about the cell range and containing table
            let cellRangeInfo;
            // is the cell defined by the start position the first cell in the row
            let isFirstCell;
            // is the cell defined by the end position the last cell in the row
            let isLastCell;
            // flag to determine if focus should be returned to slidepane or not
            let returnFocusToSidepane = false;

            // DOCS-2982 Text size lost when using context menu 'Copy'
            // If the user use the contextmenu to copy a text with Chrome and Edge(Chrome) the attributes are lost if
            // the clipboard DIV is used. The fix is to trigger the 'normal' OS copy event and read the clipboardData from the event.
            // This also saves us a second way (clipboard DIV) to copy a text.
            if (!clipboardData && document.queryCommandSupported && document.queryCommandSupported('copy') && !getBooleanOption(options, 'forceOperationGeneration', false)) {
                if (!this.#docApp.isPresentationApp()) {
                    selection.restoreBrowserSelection({ forceWhenReadOnly: true });
                }
                document.execCommand('copy');
                return;
            }

            if (_.browser.IE && (end[0] - start[0]) > 200) {
                // bigger selection take more time to copy,
                // in IE it can take more than 10seconds,
                // so we warn the user
                // eslint-disable-next-line no-alert
                const proceed = window.confirm(gt('The selected text is very long. It will take some time to be copied. Do you really want to continue?'));
                if (!proceed) { return; }
            }

            // the target has a marker, that this event handler can ignore it (inside the comment pane, DOCS-2663)
            if (this.#docApp.isTextApp() && event && $(event.target).is('.ignorepageevent')) { return; }

            // generate a new unique id to identify the clipboard operations
            this.setClipboardId(ox.session + ':' + _.uniqueId());
            // set clipboard data origin and data origin id
            this.setClipboardOrigin();
            this.setClipboardOriginId();

            switch (selection.getSelectionType()) {

                case 'text':
                    if (this.#docApp.isTextApp()) {
                        // OX Text
                        cellRangeInfo = selection.getSelectedCellRange();
                        isFirstCell = $(getLastNodeFromPositionByNodeName(this.getNode(), start, TABLE_CELLNODE_SELECTOR)).prev().length === 0;
                        isLastCell = $(getLastNodeFromPositionByNodeName(this.getNode(), end, TABLE_CELLNODE_SELECTOR)).next().length === 0;

                        // if the selected range is inside the same table or parent table and
                        // the start position is the first cell in the start row and the end position
                        // is the last cell in the end row use table selection otherwise, use text selection.
                        if (cellRangeInfo && isFirstCell && isLastCell && !_.isEqual(cellRangeInfo.firstCellPosition, cellRangeInfo.lastCellPosition)) {
                            operations = this.#copyCellRangeSelection();
                        } else {
                            operations = this.#copyTextSelection();
                        }

                    } else if (this.#docApp.isPresentationApp()) {
                        // OX Presentation
                        if (selection.isAdditionalTextframeSelection() && selection.hasRange()) {
                            operations = this.#copyTextSelection();
                        } else if (selection.isAdditionalTextframeSelection()) {
                            operations = this.#copyDrawingSelection();
                        } else {
                            operations = this.#copySlides();
                            returnFocusToSidepane = true;
                        }

                    } else if (this.#docApp.isSpreadsheetApp()) {
                        // OX Spreadsheet
                        operations = this.#copyTextSelection();
                    }
                    break;

                case 'drawing':
                    if (this.#docApp.isPresentationApp()) {
                        operations = this.#copyDrawingSelection();
                    } else {
                        operations = this.#copyTextSelection();
                    }
                    break;

                case 'cell':
                    operations = this.#copyCellRangeSelection();
                    break;

                default:
                    this.setClipboardId('');
                    globalLogger.error('Editor.copy(): unsupported selection type: ' + selection.getSelectionType());
            }

            this.setClipboardOperations(this.#prepareThemeColorsForCopy(operations));

            // If it's not presentation and no footer or header is in edit mode, hide the footer/header to prevent copy the footer/header text into the clipboard
            let pageBreaks;
            if (!this.useSlideMode() && !$('.page .pagecontent .page-break .footer[contenteditable=true], .page .pagecontent .page-break .header[contenteditable=true]').length) {
                pageBreaks = $('.page .pagecontent .page-break');
                pageBreaks.hide();
                pageBreaks.children().hide();
            }

            // html clipboard data cleaned up for export
            const htmlExportData = Export.getHTMLFromSelection(this, { clipboardId: this.getClipboardId(), clipboardOperations: this.getClipboardOperations(), clipboardOrigin: this.getClipboardOrigin(), clipboardOriginId: this.getClipboardOriginId() });
            // set clipboard debug pane content
            this.trigger('debug:clipboard', htmlExportData);

            // if browser supports clipboard api add data to the event
            if (clipboardData) {
                // add operation data
                clipboardData.setData('text/ox-operations', JSON.stringify(this.getClipboardOperations()));

                // add operation data origin
                clipboardData.setData('text/ox-origin', this.getClipboardOrigin());

                // add id to identify clipboard origin
                clipboardData.setData('text/ox-origin-id', _.cid(this.#docApp.getFileDescriptor()));

                // add plain text and html of the current browser selection
                if (this.useSlideMode()) {
                    clipboardData.setData('text/plain', Export.getPlainTextFromSelection(this).replace(/\xa0/g, ' ')); // DOCS-2134, replacing non breakable spaces
                } else {
                    clipboardData.setData('text/plain', selection.getTextFromBrowserSelection().replace(/\xa0/g, ' ')); // DOCS-2134, replacing non breakable spaces
                }
                clipboardData.setData('text/html', htmlExportData.replace(/&nbsp;/g, ' ')); // DOCS-2134, replacing non breakable spaces

                // prevent default copy handling for desktop browsers, but not for touch devices
                event.preventDefault();

                if (returnFocusToSidepane) { // can only be true in Presentation app
                    this.executeDelayed(() => {
                        clearBrowserSelection();
                        setFocus(this.#docApp.getView().getSlidePane().getSlidePaneContainer());
                    });
                }
            }
            if (pageBreaks) {
                pageBreaks.show();
                pageBreaks.children().show();
            }
        }

        /**
         * Deletes the current selection and pastes the internal clipboard to
         * the resulting cursor position.
         *
         * @param {Number[]} dropPosition
         *  An optional logical position used for pasting the internal clipboard
         *  content.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        pasteInternalClipboard(dropPosition) {

            // check if clipboard contains something
            if (!this.hasInternalClipboard()) { return $.when(); }

            // don't paste content if the presentation hasn't at least one slide
            if (this.useSlideMode() && this.isEmptySlideView && this.isEmptySlideView()) { return $.when(); }

            // make sure that only one paste call is processed at the same time
            if (this.checkSetClipboardPasteInProgress()) { return $.when(); }

            // Group all executed operations into a single undo action.
            // The undo manager returns the return value of the callback function.
            return this.getUndoManager().enterUndoGroup(() => {

                // the deferred to keep the undo group open until it is resolved or rejected
                const undoDef = this.createDeferred();
                // target position to paste the clipboard contents to
                let anchorPosition = null;
                // the generated paste operations with transformed positions
                let operations = null;
                // the generator for stylesheet operations that need to be executed first
                const styleCache = new StyleCache(this);
                // the list style map
                const listStyleMap = {};
                // a snapshot object
                let snapshot = null;
                // whether the user aborted the pasting action
                let userAbort = false;
                // whether there is a selection range before pasting
                let hasRange = false;
                // the logical range position
                let rangeStart = null, rangeEnd = null;
                // the list collection object
                const listCollection = this.getListCollection();
                // whether a operation was removed from the list of operations
                let operationRemoved = false;
                // the selection object
                const selection = this.getSelection();
                // whether the insert list operation is the first list
                let firstInsertListOperation = true;
                // array of objects that keeps track of old and new drawing ids, used to refresh connector links
                const drawingIdsTransTable = [];

                // builds the list style map from insertListStyle operations, mapping the list style ids
                // of the source document to the list style ids of the destination document
                const createListStyleMap = operation => {

                    let listStyleId;

                    if ((operation.name === Op.INSERT_LIST) && _.isObject(operation.listDefinition)) {
                        // deep copy the operation before changing
                        operation = _.copy(operation, true);

                        if (firstInsertListOperation) {
                            // check if the previous paragraph has the same definition and use it or create a new id
                            listStyleId = listCollection.getListId(operation.listDefinition);
                            firstInsertListOperation = false;
                        }
                        if (!listStyleId) {
                            listStyleId = this.#getFreeListStyleId();
                        }

                        listStyleMap[operation.listStyleId] = listStyleId;
                        operation.listStyleId = listStyleId;

                        // handling the base style id (that cannot be sent to the server)
                        if (operation.baseStyleId) {

                            // if the base style Id is the same, do not send any operation (filter cannot handle base style id)
                            if (operation.baseStyleId === listCollection.getBaseStyleIdFromListStyle(operation.listStyleId)) {
                                operationRemoved = true;  // needs to be marked for cleanup
                                return null;
                            }

                            // but never send baseStyleId information to the server because filter cannot handle it correctly
                            delete operation.baseStyleId;
                        }

                        // make sure every list level has a listStartValue set
                        _.each(operation.listDefinition, listLevelDef => {
                            if (!('listStartValue' in listLevelDef)) {
                                listLevelDef.listStartValue = 1;
                            }
                        });
                    }

                    return operation;
                };

                // transforms a position being relative to [0,0] to a position relative to anchorPosition
                const transformPosition = position => {

                    // the resulting position
                    let resultPosition = null;

                    if ((position[0] === 0) && (position.length > 1)) {
                        // adjust text/drawing offset for first paragraph
                        resultPosition = anchorPosition.slice(0, -1);
                        resultPosition.push(anchorPosition[anchorPosition.length - 1] + position[1]);
                        resultPosition = resultPosition.concat(position.slice(2));
                    } else {
                        // adjust paragraph offset for following paragraphs
                        resultPosition = anchorPosition.slice(0, -2);
                        resultPosition.push(anchorPosition[anchorPosition.length - 2] + position[0]);
                        resultPosition = resultPosition.concat(position.slice(1));
                    }

                    return resultPosition;
                };

                // checks the anchorPosition for pasting. If this is a text position and the pasting starts with insertParagraph,
                // the anchorPosition must be adjusted to the beginning of the (following) paragraph. Otherwise the function
                // 'transformPosition' might create invalid text operations (DOCS-2453).
                const checkAnchorPosition = (anchorPosition, operations) => {

                    let localAnchorPosition = anchorPosition;
                    const localRootNode = this.getSelection().getRootNode();
                    let testPosition = null;

                    if (operations.length && operations[0].name === Op.PARA_INSERT) {
                        if (_.last(localAnchorPosition) > 0 && isParagraphPosition(localRootNode, _.initial(localAnchorPosition))) {
                            localAnchorPosition[localAnchorPosition.length - 1] = 0; // setting anchor to the beginning of the paragraph
                            if (localAnchorPosition.length > 1) {
                                testPosition = _.copy(localAnchorPosition);
                                testPosition[localAnchorPosition.length - 2] += 1; // checking for a next paragraph
                                if (isParagraphPosition(localRootNode, _.initial(testPosition))) {
                                    localAnchorPosition = testPosition;
                                }
                            }
                        }
                    }

                    return localAnchorPosition;
                };

                // if the operation references a style try to add the style sheet to the document
                const addMissingStyleSheet = operation => {
                    // the default lateral table definiton
                    const defaultTableDefinition = this.getDefaultLateralTableDefinition();
                    // the table stylesheet id
                    let tableStyleId = null;

                    // insert table in OX Text
                    const isInsertTable = operation => {
                        return (_.isObject(operation) && _.isObject(operation.attrs) && (operation.name === Op.TABLE_INSERT));
                    };

                    // insert table in OX Presentation
                    const isInsertDrawingTable = operation => {
                        return (_.isObject(operation) && _.isObject(operation.attrs) && (operation.name === Op.INSERT_DRAWING) && (operation.type === 'table'));
                    };

                    if (isInsertTable(operation) || isInsertDrawingTable(operation)) {

                        // check if table style sheet id is given and if it can be used
                        if (this.tableStyles.containsStyleSheet(operation.attrs.styleId) && (this.isCopyPasteOnSameDocument() || this.#docApp.isPresentationApp() || !this.#docApp.isODF())) {
                            // the table style sheet collection contains the style sheet and copy&paste takes place on the same document or target Presentation or target is OOXML
                            tableStyleId = operation.attrs.styleId;

                        } else {
                            // use the default table style sheet id and adopt the given operation
                            tableStyleId = defaultTableDefinition.styleId;
                            operation.attrs.styleId = tableStyleId;
                        }

                        // generate operation to insert a dirty table style into the document
                        this.tableStyles.generateMissingStyleSheetOperations(styleCache, tableStyleId);
                    }
                };

                // apply list style mapping to setAttributes and insertParagraph operations
                const mapListStyles = operation => {

                    if ((operation.name === Op.SET_ATTRIBUTES || operation.name === Op.PARA_INSERT) &&
                        operation.attrs && operation.attrs.paragraph && _.isString(operation.attrs.paragraph.listStyleId) &&
                        listStyleMap[operation.attrs.paragraph.listStyleId]) {

                        // apply the list style id from the list style map to the operation
                        operation.attrs.paragraph.listStyleId = listStyleMap[operation.attrs.paragraph.listStyleId];
                    }
                };

                const isDrawing = operation => {
                    return (operation && (operation.name === Op.INSERT_DRAWING) && operation.attrs && operation.attrs.drawing);
                };

                const isOperationWithinGroup = (_operation, idx, operationList) => {
                    return (
                        (idx >= 1) &&
                        _.isArray(operationList) &&
                        operationList.some(o7n => { return (o7n.type === 'group'); })
                    );
                };

                //  Since Bug #62839 is reported for copying from text-application to text-application,
                //  the beneath commented additional approach does not yet need to be implemented ...
                //
                //  But it should be implemented as soon as one is going to fix the most recently created
                //  Bug #64370 that targets copying from presentation to text.
                //
                // // - will target drawing operations more precisely as it has been done by the former fix for Bug #61595.
                // // - the hereby additionally provided new approach also considers Bug #62839.
                // const createListOfDrawingGroups = operationList => {
                //     window.console.log('+++ createListOfDrawingGroups :: operationList : ', operationList);
                // }

                // make drawing attributes modifications
                const mapDrawing = (operation, idx, operationList/*, drawingGroupList*/) => {
                    if (isDrawing(operation)) {

                        // changes all drawing from floating to inline (only in Text application)
                        if (this.#docApp.isTextApp() && !operation.attrs.drawing.inline) {

                            _.extend(operation.attrs.drawing, { inline: true, anchorHorBase: null, anchorHorAlign: null, anchorHorOffset: null, anchorVertBase: null, anchorVertAlign: null, anchorVertOffset: null, textWrapMode: null, textWrapSide: null });

                            // bug 61595 - Text doesn't use left and top
                            // new approach, that also considers Bug 62839.
                            if (!isOperationWithinGroup(operation, idx, operationList))  {
                                delete operation.attrs.drawing.left;
                                delete operation.attrs.drawing.top;
                            }
                        }
                        // replace copied id with new, unique one
                        if (this.#docApp.isPresentationApp()) {
                            const newId = this.getNewDrawingId();
                            drawingIdsTransTable.push([newId, operation.attrs.drawing.id]);
                            operation.attrs.drawing.id = newId;
                        }
                    }
                };

                // for ODF documents replace all theme colors with the fallback color
                const mapThemeColor = operation => {

                    const handleColor = subtree => {
                        // the current element of the sub tree
                        let element = null;

                        for (const key in subtree) {
                            element = subtree[key];

                            if (key === 'color' || key === 'fillColor') {
                                if (isColorThemed(element) && element.fallbackValue) {
                                    subtree[key] = opRgbColor(element.fallbackValue);
                                }
                            } else if (_.isObject(element) && !this.#isNonColorAttributeFamily(key)) {
                                handleColor(element);
                            }
                        }
                    };

                    if (this.#docApp.isODF() && this.#hasAttributeFamilyWithColor(operation)) {
                        handleColor(operation.attrs);
                    }
                };

                // set default hyperlink style if given one is not available
                const mapHyperlinkStyle = operation => {

                    // hyperlink style sheet id
                    let hyperlinkStyleId = null;

                    const isInsertHyperlink = operation => {
                        return (operation.name === Op.SET_ATTRIBUTES || operation.name === Op.TEXT_INSERT) &&
                            operation.attrs && operation.attrs.character && !_.isEmpty(operation.attrs.character.url);
                    };

                    if (!this.useSlideMode() && isInsertHyperlink(operation)) {

                        if (this.characterStyles.containsStyleSheet(operation.attrs.styleId)) {
                            hyperlinkStyleId = operation.attrs.styleId;
                        } else {
                            hyperlinkStyleId = this.getDefaultUIHyperlinkStylesheet();
                            operation.attrs.styleId = hyperlinkStyleId;
                        }

                        // generate operation to insert a dirty hyperlink style
                        this.characterStyles.generateMissingStyleSheetOperations(styleCache, hyperlinkStyleId);
                    }
                };

                // transforms the passed operation relative to anchorPosition
                const transformOperation = (operation, idx, operationList) => {

                    // clone the operation to transform the positions (no deep clone,
                    // as the position arrays will be recreated, not modified inplace)
                    operation = _.clone(operation);

                    // transform position of operation (Text: But not, if a target is defined (for example in comments))
                    // -> in slide mode this function needs to be called always, also for master and layout slides ()
                    if (_.isArray(operation.start) && (this.useSlideMode() || !operation.target)) {
                        // start may exist but is relative to position then
                        operation.start = transformPosition(operation.start);
                        // attribute 'end' only with attribute 'start'
                        if (_.isArray(operation.end)) {
                            operation.end = transformPosition(operation.end);
                        }
                        addMissingStyleSheet(operation);
                    }

                    // map list style ids from source to destination document
                    mapListStyles(operation);

                    // change drawing from floating to inline - fix for bug #32873, and replace drawing id if needed
                    mapDrawing(operation, idx, operationList/*, createListOfDrawingGroups(operationList)*/);

                    // for ODF documents replace all theme colors with the fallback color
                    mapThemeColor(operation);

                    // set default hyperlink style if given one is not available
                    mapHyperlinkStyle(operation);

                    return operation;
                };

                // removes operations that are not allowed in slide mode
                const removeInvalidSlideModeOperation = operation => {
                    // #47759 - in pptx only style sheets of type table are supported.
                    if (operation.name === Op.INSERT_STYLESHEET && operation.type !== 'table') {
                        operationRemoved = true;  // needs to be marked for cleanup
                        return null;
                    }

                    return operation;
                };

                // helper function to check, if the operations can be pasted to the anchor position into OX Text
                const isForbiddenPastingText = (anchorPosition, clipboardOperations) => {

                    // helper function to find insertDrawing operations with type 'shape'
                    const containsInsertShapeDrawing = operations => {
                        return _.find(operations, operation => {
                            return operation.name && operation.name === 'insertDrawing' && operation.type && operation.type === 'shape';
                        });
                    };

                    // helper function to find specified operations in the list of operations
                    const containsSpecifiedOperations = (operations, operationList) => {
                        return _.find(operations, operation => {
                            return operation.name && _.contains(operationList, operation.name);
                        });
                    };

                    // helper function to find insert drawing shape operation with nested insert table operation
                    const containsTableInsidePresetOrCustomShape = operations => {
                        // the drawing shape positions
                        const shapePositions = [];

                        // returns true if the position is inside a drawing.
                        const isInsideDrawingShapePosition = position => {
                            // check position if it starts with one of the collected drawing positions
                            if ((!_.isArray(position)) || (shapePositions.length < 1)) { return false; }
                            return _.some(shapePositions, rootPosition => {
                                return this.#arrayStartsWith(position, rootPosition);
                            });
                        };

                        return _.some(operations, operation => {
                            // whether there is a nested table in a shape
                            let found = false;

                            // check for preset or custom shape operations
                            if (this.#isPresetOrCustomShapeOperation(operation)) {
                                shapePositions.push(operation.start);
                            } else if ((operation.name === Op.TABLE_INSERT) && isInsideDrawingShapePosition(operation.start)) {
                                found = true;
                            }

                            return found;
                        });
                    };

                    // pasting text frames into text frames or comments is not supported
                    if (containsInsertShapeDrawing(clipboardOperations)) {
                        if (this.isCommentFunctionality()) {
                            // inform the user, that this pasting is not allowed
                            this.#docApp.getView().yell({ type: 'info', message: gt('Pasting shapes into comments is not supported.') });
                            return true;
                        } else if (isPositionInsideTextframe(this.getCurrentRootNode(), anchorPosition)) {
                            // inform the user, that this pasting is not allowed
                            this.#docApp.getView().yell({ type: 'info', message: gt('Pasting shapes into text frames is not supported.') });
                            return true;
                        }
                    }

                    // pasting tables or drawings into 'shapes with text content' in odf or into comments in odf is not supported
                    if (containsSpecifiedOperations(clipboardOperations, ['insertDrawing', 'insertTable']) && (this.isReducedOdfTextframeFunctionality() || this.isOdfCommentFunctionality())) {
                        // inform the user, that this pasting is not allowed
                        this.#docApp.getView().yell({ type: 'info', message: gt('Pasting content into this object is not supported.') });
                        return true;
                    }

                    // pasting images is not supported in any shape or textframe in odf, #57470
                    if (this.#docApp.isODF() && selection.isAdditionalTextframeSelection() && containsSpecifiedOperations(clipboardOperations, ['insertDrawing'])) {
                        // inform the user, that this pasting is not allowed
                        this.#docApp.getView().yell({ type: 'info', message: gt('Pasting content into this object is not supported.') });
                        return true;
                    }

                    // pasting fields into comments in odf is not supported
                    if (containsSpecifiedOperations(clipboardOperations, ['insertField']) && this.isOdfCommentFunctionality()) {
                        // inform the user, that this pasting is not allowed
                        this.#docApp.getView().yell({ type: 'info', message: gt('Pasting fields into comments is not supported.') });
                        return true;
                    }

                    // pasting lists into comments in odf is not supported
                    if (containsSpecifiedOperations(clipboardOperations, ['insertListStyle']) && this.isOdfCommentFunctionality()) {
                        // inform the user, that this pasting is not allowed
                        this.#docApp.getView().yell({ type: 'info', message: gt('Pasting lists into comments is not supported.') });
                        return true;
                    }

                    // tables inside shapes are not supported in odf
                    if (this.#docApp.isODF() && containsTableInsidePresetOrCustomShape(clipboardOperations)) {
                        // inform the user, that this pasting is not allowed
                        this.#docApp.getView().yell({ type: 'info', message: gt('Pasting content into this object is not supported.') });
                        return true;
                    }

                    return false;
                };

                // helper function to check, if the operations can be pasted to the anchor position into any OX Documents application
                const isForbiddenPastingGeneral = (_anchorPosition, clipboardOperations) => {
                    // returns true when operations inserts a table or a drawing of type table
                    const isInsertTable = operation => {
                        return ((operation.name === Op.TABLE_INSERT) || ((operation.name === Op.INSERT_DRAWING && operation.type === 'table')));
                    };

                    // helper function to find insert drawing table operation inside groups
                    const containsTableInsideGroup = operations => {
                        // the positions of group drawings
                        const groupPositions = [];

                        // returns true if the operation is nested inside a group drawing
                        const isInsideGroup = operation => {
                            if (!operation.start || groupPositions.length < 1) { return false; }
                            // check if operation.start begins with one of the collected group positions
                            return _.some(groupPositions, groupPosition => {
                                return this.#arrayStartsWith(operation.start, groupPosition);
                            });
                        };

                        return _.some(operations, operation => {
                            // whether there is a table inside a group
                            let found = false;

                            // store group position; used to check if operation is nested inside a group drawing
                            if (operation.type === 'group') {
                                groupPositions.push(operation.start);
                            } else if (isInsertTable(operation) && isInsideGroup(operation)) {
                                found = true;
                            }

                            return found;
                        });
                    };

                    // tables inside groups are not supported in ooxml
                    if (!this.#docApp.isODF() && containsTableInsideGroup(clipboardOperations)) {
                        // inform the user, that this pasting is not allowed
                        this.#docApp.getView().yell({ type: 'info', message: gt('Pasting groups with tables is not supported.') });
                        return true;
                    }

                    return false;
                };

                // finding a valid text cursor position after pasting. The default position is
                // determined from the last operation of pasting.
                const getFinalCursorPosition = (defaultPosition, operations) => {

                    // the determined cursor position
                    let cursorPosition = defaultPosition;
                    // the searched split paragraph operation
                    let searchOperation = null;
                    // searched range end operation of type field
                    let rangeEndFieldOperation = null;
                    // last operation
                    let lastOperation = null;
                    // searching for a splitting of paragraph
                    const searchOperationName = Op.PARA_SPLIT;
                    // the position after splitting a paragraph
                    let splitPosition = null;

                    if (operations && operations.length > 0) {
                        // setting the cursor after pasting.
                        // setting cursor after pasting complex field as last operation
                        lastOperation = _.last(operations);
                        // taking care of pasting range end of complex field as last operation, (#43699)
                        rangeEndFieldOperation = lastOperation.name === Op.RANGE_INSERT && lastOperation.type === 'field' && lastOperation.position === 'end';
                        if (rangeEndFieldOperation) {
                            cursorPosition = increaseLastIndex(lastOperation.start);
                        } else {
                            // Also taking care of splitted paragraphs (36471) -> search last split paragraph operation (but only inside correct target (DOCS-1671))
                            searchOperation = _.find(operations.reverse(), operation => {
                                return operation.name && operation.name === searchOperationName && operation.target === lastOperation.target;
                            });
                        }
                    }

                    if (searchOperation) {
                        // is this position after split behind the defaultPosition? Then is should be used
                        splitPosition = _.clone(searchOperation.start);
                        splitPosition.pop();  // the paragraph position
                        splitPosition = increaseLastIndex(splitPosition);
                        splitPosition.push(0);  // setting cursor to start of second part of splitted paragraph
                        if (isValidPositionOrder(cursorPosition, splitPosition)) { cursorPosition = splitPosition; }
                    }

                    return cursorPosition;
                };

                // returns the page break attributes for the paragraph at the given position
                const getParagraphPageBreakAttributes = position => {
                    const result = {};
                    let paragraphNode = null;
                    let attributes = null;

                    if (!this.useSlideMode()) {
                        paragraphNode = getParagraphElement(this.getNode(), _.initial(position));
                        attributes = getExplicitAttributeSet(paragraphNode);

                        if (attributes && attributes.paragraph && attributes.paragraph.pageBreakBefore) {
                            result.pageBreakBefore = true;
                        }
                        if (attributes && attributes.paragraph && attributes.paragraph.pageBreakAfter) {
                            result.pageBreakAfter = true;
                        }
                    }

                    return result;
                };

                // generate a set attribute operation to restore paragraph page breaks
                const generatePageBreakOperation = (position, pageBreakAttrs) => {
                    if (this.useSlideMode() || !_.isArray(position) || !_.isObject(pageBreakAttrs)) {
                        return null;
                    }

                    let result = null;

                    if (pageBreakAttrs.pageBreakBefore || pageBreakAttrs.pageBreakAfter) {
                        result = this.createOperationGenerator().generateOperation(Op.SET_ATTRIBUTES, {
                            target: this.getActiveTarget(),
                            start: _.initial(position),
                            attrs: { paragraph: pageBreakAttrs }
                        });
                    }

                    return result;
                };

                // returns the global replacement delete operations sorted and in a reversed order
                const getDeleteReplacementsOperations = () => {
                    // the delete replacement operations
                    let deleteReplacementsOperations = this.#deleteReplacementsGenerator.getOperations();

                    // the operations must be sorted, not only reversed (52508)
                    deleteReplacementsOperations.sort((a, b) => comparePositions(a.start, b.start));
                    // the replacement delete operations need to be processed in reverse order
                    deleteReplacementsOperations.reverse();
                    // transforms the operations relative to anchorPosition
                    deleteReplacementsOperations = _.map(deleteReplacementsOperations, transformOperation);

                    return deleteReplacementsOperations;
                };

                // applying the paste operations
                const doPasteInternalClipboard = () => {

                    // the apply actions promise
                    let applyPromise = null;
                    // the newly created operations
                    let newOperations = null;
                    // the current target node
                    const target = this.getActiveTarget();
                    // whether the clipboard contains drawings
                    const isDrawingInClipboard = this.#checkClipboardForDrawings(this.getClipboardOperations());
                    // whether the clipboard contains tables
                    const isTableInClipboard = this.#checkClipboardForTables(this.getClipboardOperations());
                    // whether the clipboard contains only insert image
                    const isOnlyInsertImageInClipboard = this.#isOnlyInsertImageOperations(this.getClipboardOperations());
                    // the target position for drawings pasted from OX Text
                    const textPasteDrawingTarget = selection.isTopLevelTextCursor() ? { top: 0, left: 0 } : this.#getMinPositionForSelectedDrawings();
                    // the page break attributes of the paste target paragraph
                    let targetParagraphPageBreakAttributes = null;
                    // set attribute operation to restore paragraph page breaks
                    let targetParagraphPageBreakOperation = null;
                    // whether the paste operations are valid for pasting content into ODT comments (53840)
                    let isAllowedCommentContent = true;
                    // whether the pasting happens into the same document type. For example from odt to odt or from docx to docx
                    const isPasteToSameDocumentType = this.isPasteToSameDocumentType();

                    // init replacement delete operations generator
                    this.#deleteReplacementsGenerator = this.createOperationGenerator();

                    // determine anchor position for pasting
                    anchorPosition = this.#getAnchorPositionForPaste({
                        dropPosition,
                        isDrawingInClipboard,
                        isTableInClipboard,
                        isOnlyInsertImageInClipboard,
                        internalClipboard: true
                    });

                    // the position for the drop operation
                    if (anchorPosition.length >= 2) {

                        if (this.#docApp.isTextApp()) {
                            // checking, if the content inside a comment is allowed in destination (53840)
                            // -> never paste comments into different document type (54362)
                            isAllowedCommentContent = isPasteToSameDocumentType && this.getCommentLayer().checkTextCommentContent(this.getClipboardOperations());
                            // check for pasting comments into comments
                            if (this.getActiveTarget() || selection.isAdditionalTextframeSelection() || !isAllowedCommentContent) {
                                this.setClipboardOperations(this.getCommentLayer().handleCommentOperationsForPasting(this.getClipboardOperations(), isPasteToSameDocumentType, this.#deleteReplacementsGenerator));
                            }
                            // checking for missing range markers (only OOXML)
                            if (!this.#docApp.isODF()) { this.setClipboardOperations(this.getCommentLayer().handleMissingRangeMarkersForPasting(this.getClipboardOperations(), isPasteToSameDocumentType, this.#deleteReplacementsGenerator)); }
                            // doing some checks, if the content can be pasted into the destination element in OX Text.
                            if (isForbiddenPastingText(anchorPosition, this.getClipboardOperations())) {
                                return $.when();
                            }
                        }

                        // doing some general checks, if the content can be pasted into the destination element of any OX Document application.
                        if (isForbiddenPastingGeneral(anchorPosition, this.getClipboardOperations())) {
                            return $.when();
                        }

                        targetParagraphPageBreakAttributes = getParagraphPageBreakAttributes(anchorPosition);

                        globalLogger.info('Editor.pasteInternalClipboard()');
                        this.setBlockKeyboardEvent(true);

                        // creating a snapshot
                        snapshot ??= new Snapshot(this);

                        // show a nice message with cancel button
                        this.#docApp.getView().enterBusy({
                            warningLabel: gt('Sorry, pasting from clipboard will take some time.'),
                            cancelHandler: () => {
                                userAbort = true;  // user aborted the pasting process
                                // restoring the document state
                                if (snapshot && !snapshot.destroyed) { snapshot.apply(); }
                                // calling abort function for operation promise
                                this.#docApp.enterBlockOperationsMode(() => {
                                    if (applyPromise && applyPromise.abort) {
                                        applyPromise.abort();
                                    }
                                });
                            }
                        });

                        // map the operations
                        operations = listCollection ? _(this.getClipboardOperations()).map(createListStyleMap) : this.getClipboardOperations();
                        if (this.useSlideMode()) { operations = operations.map(removeInvalidSlideModeOperation); }
                        if (operationRemoved) { operations = removeFalsyItemsInArray(operations); }

                        // map operations from OX Text to OX Presentation
                        if (this.useSlideMode() && (!isDrawingInClipboard || this.isClipboardOriginText())) {
                            operations = this.#removeCommentContentOperations(operations);
                            operations = this.#makeTextFromTableOperations(operations);
                            operations = this.#transformOperationsToSlideMode(operations);
                            anchorPosition = checkAnchorPosition(anchorPosition, operations);
                        }

                        // map operations from OX Presentation to OX Text
                        if (!this.useSlideMode() && this.isClipboardOriginPresentation()) {
                            operations = this.#transformOperationsToText(operations);
                        }

                        // in operations of the Presentation app the information of the source slide must not be used, 52965
                        if (this.isClipboardOriginPresentation()) {
                            _.each(operations, operation => {
                                if (operation.target) { delete operation.target; }
                            });
                        }

                        // transcode field operations between OOXML and ODF, also between text and presentation apps
                        if (!isPasteToSameDocumentType) {
                            operations = this.#transcodeFieldOperations(operations);
                        }

                        // transcode image bullets to default text bullets
                        if (!this.isCopyPasteOnSameDocument()) {
                            operations = this.#transcodeListOperations(operations);
                        }

                        // remove any bookmark operations, as they are not needed on paste
                        operations = this.#removeBookmarkOperations(operations);

                        // shifting the positions of the new inserted drawings
                        if (this.#docApp.isPresentationApp() && isDrawingInClipboard) {
                            this.#shiftDrawingPositions(operations, textPasteDrawingTarget);
                        }

                        operations = _(operations).map(transformOperation);

                        // DOCS-4211
                        if (this.hasOoxmlTextCharacterFilterSupport()) { this.removeHardCharacterAttributesAtOperation(operations[0]); }

                        // when pasting connectors linked to drawings with new ids, keep links by refreshing them in connector, #55784
                        if (this.#docApp.isPresentationApp() && this.isClipboardOriginPresentation() && isDrawingInClipboard) {
                            this.refreshConnectorLinks(operations, drawingIdsTransTable);
                        }

                        // concat with newly generated operations
                        newOperations = styleCache.getOperations();
                        if (newOperations.length) {
                            operations = newOperations.concat(operations);
                        }

                        // generate a set attribute operation to restore paragraph page breaks
                        targetParagraphPageBreakOperation = generatePageBreakOperation(anchorPosition, targetParagraphPageBreakAttributes);
                        if (targetParagraphPageBreakOperation) {
                            operations = operations.concat(targetParagraphPageBreakOperation);
                        }

                        // adding change track information, if required
                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            this.getChangeTrack().handleChangeTrackingDuringPaste(operations);
                        } else {
                            this.getChangeTrack().removeChangeTrackInfoAfterSplitInPaste(operations);
                        }

                        // handling target positions (for example inside comments)
                        if (this.#docApp.isTextApp()) { this.getCommentLayer().handlePasteOperationTarget(operations); }
                        this.getFieldManager().handlePasteOperationIDs(operations);

                        // setting property autoResizeHeight explicitely to false, if not already specified (56351)
                        if (this.#docApp.isODF() && this.#docApp.isTextApp()) { this.#checkODTTextProperties(operations); }

                        this.doCheckImplicitParagraph(anchorPosition);

                        // add delete replacement operations to the clipboard operations
                        operations = operations.concat(getDeleteReplacementsOperations());

                        // if pasting into header or footer or comment, extend operations with target
                        if (target) {
                            _.each(operations, operation => {
                                if (operation.name !== Op.INSERT_STYLESHEET) {
                                    operation.target = target;
                                }
                            });
                        }

                        // set clipboard debug pane content
                        this.trigger('debug:clipboard', operations);

                        // apply operations
                        applyPromise = this.applyOperationsAsync(operations)
                            .progress(progress => {
                                this.#docApp.getView().updateBusyProgress(progress);
                            });

                    } else {
                        globalLogger.warn('Editor.pasteInternalClipboard(): invalid cursor position');
                        applyPromise = $.Deferred().reject();
                    }

                    return applyPromise;
                };

                // reset id generation for this.#getFreeListStyleId()
                this.#resetListStyleId();

                // special handling for selected drawings
                if (this.isDrawingSelected()) {
                    if (selection.isTextFrameSelection()) {
                        selection.setSelectionIntoTextframe(); // if a text frame is selected, the new content is inserted into the text frame
                    } else if (!this.useSlideMode()) {
                        selection.setTextSelection(selection.getStartPosition()); // if a drawing is selected in text mode, the new content is inserted before the drawing
                    }
                }

                hasRange = selection.hasRange();

                // creating a snapshot
                if (hasRange) {
                    snapshot = new Snapshot(this);
                    rangeStart = _.clone(this.getSelection().getStartPosition());
                    rangeEnd = _.clone(this.getSelection().getEndPosition());
                }

                // delete current selection, but in presentation app only text selections inside shapes
                ((this.useSlideMode() && !this.getSelection().isAdditionalTextframeSelection()) ? $.when() : (this.deleteSelected({ alreadyPasteInProgress: true, snapshot })))
                    .then(doPasteInternalClipboard)
                    .always(() => {
                        // Text only: Check for implicit paragraphs in shapes (53983)
                        if (!this.#docApp.isODF() && this.#docApp.isTextApp()) { this.checkInsertedEmptyTextShapes(operations); }
                        // no longer blocking page break calculations (40107)
                        this.setBlockOnInsertPageBreaks(false);
                        // leaving busy mode
                        this.leaveAsyncBusy();
                        // close undo group
                        undoDef.resolve();
                        // deleting the snapshot
                        if (snapshot) { snapshot.destroy(); }
                        // setting the cursor range after cancel by user
                        if (userAbort && hasRange) { this.getSelection().setTextSelection(rangeStart, rangeEnd); }

                    }).done(() => {
                        const lastOperation = _.last(operations);
                        const finalCursorPos = getFinalCursorPosition(this.getLastOperationEnd(), operations);
                        let selectionSet = false;

                        if (this.useSlideMode()) {
                            selectionSet = this.#selectTopLevelDrawings(operations); // selecting all inserted top level drawings (no grouped drawings) (58992)
                        }

                        if (!selectionSet) {
                            // select the previously inserted image in all applications (DOCS-3385)
                            if (lastOperation && lastOperation.name === Op.INSERT_DRAWING && lastOperation.type === 'image') {
                                this.getSelection().setTextSelection(decreaseLastIndex(finalCursorPos), finalCursorPos);
                            } else {
                                this.getSelection().setTextSelection(finalCursorPos);
                            }
                        }
                    });

                return undoDef.promise();

            }, this); // enterUndoGroup()
        }

        /**
         * Pastes the clipboard containing copied slides behind the position of last selected slide.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        pasteSlidesInternal() {

            let operations = this.getClipboardOperations();
            const firstOp = operations && _.first(operations);
            const isFirstOpInsertSlide = firstOp && firstOp.name === 'insertSlide';
            const isFirstOpInsertLayout = firstOp && firstOp.name === 'insertLayoutSlide';

            // check if clipboard contains something
            if (!this.hasInternalClipboard()) { return $.when(); }

            // don't paste slides if it's not Presentation app
            if (!this.useSlideMode()) { return $.when(); }

            // don't allow pasting of standard slides into master view, and vice versa
            if (this.isMasterView()) {
                if (isFirstOpInsertSlide) {
                    return $.when();
                }
            } else {
                if (isFirstOpInsertLayout) {
                    return $.when();
                }
            }

            // make sure that only one paste call is processed at the same time
            if (this.checkSetClipboardPasteInProgress()) { return $.when(); }

            // Group all executed operations into a single undo action.
            // The undo manager returns the return value of the callback function.
            return this.getUndoManager().enterUndoGroup(() => {

                // the deferred to keep the undo group open until it is resolved or rejected
                const undoDef = this.createDeferred();
                // target position to paste the clipboard contents to
                let anchorPosition = null;
                // operation generator for additional insert style sheets operations
                const generator = this.createOperationGenerator();
                // a snapshot object
                let snapshot = null;
                // used for target of elements on pasted new layout slide
                let nextLayoutId = null;
                // start position of layout slide
                let layoutSlideStart = null;
                // id of the current file
                const currentFileId = this.#docApp.getFileDescriptor().id;

                // transforms a position being relative to [0,0] to a position relative to anchorPosition
                const transformPosition = position => {

                    // the resulting position
                    const resultPosition = position;

                    resultPosition.shift();
                    resultPosition.unshift(anchorPosition);

                    return resultPosition;
                };

                // transforms the passed operation relative to anchorPosition
                const transformOperation = operation => {

                    // clone the operation to transform the positions (no deep clone,
                    // as the position arrays will be recreated, not modified inplace)
                    operation = _.clone(operation);

                    if (operation.name === 'insertLayoutSlide') {
                        if (!nextLayoutId) {
                            nextLayoutId = this.getNextCustomLayoutId();
                        } else {
                            nextLayoutId = String(parseInt(nextLayoutId, 10) + 1); // increase manually next layout Id, since the operations are not applied yet
                        }
                        operation.id = nextLayoutId;
                        operation.target = this.isMasterSlideId(this.getActiveSlideId()) ? this.getActiveSlideId() : this.getMasterSlideId(this.getActiveSlideId());
                        layoutSlideStart = _.isNull(layoutSlideStart) ? anchorPosition : layoutSlideStart;
                        operation.start = layoutSlideStart;
                        layoutSlideStart += 1;
                        anchorPosition = 0; // layout slides have start pos always 0
                    } else if (isFirstOpInsertLayout) {
                        operation.target  = nextLayoutId;
                    }

                    if (operation.name === 'insertSlide') {
                        anchorPosition += 1; // increase position for new slide
                        operation.start = [anchorPosition];
                        if (operation.options) {
                            if (operation.options.fileId !== currentFileId) { // copy/paste slides between different documents, fetch layout id from layout type
                                operation.target = this.getLayoutIdFromType(operation.options.type);
                            }
                            delete operation.options; // clean up helper values before generating operation
                        }
                    }

                    // transform position of operation
                    if (_.isArray(operation.start)) {
                        // start may exist but is relative to position then
                        operation.start = transformPosition(operation.start);
                        // attribute 'end' only with attribute 'start'
                        if (_.isArray(operation.end)) {
                            operation.end = transformPosition(operation.end);
                        }
                    }

                    const op = _.clone(operation);
                    delete op.name;

                    return operation;
                };

                // applying the paste operations
                const doPasteSlidesInternal = () => {

                    // the apply actions promise
                    let applyPromise = null;
                    // the newly created operations
                    let newOperations = null;

                    // paste clipboard to current cursor position
                    anchorPosition = this.getSlidePaneSelection() && this.getSlidePaneSelection().sort((a, b) => { return a - b; });
                    anchorPosition = _.last(anchorPosition);
                    if (_.isNumber(anchorPosition)) {
                        globalLogger.info('Editor.pasteSlidesInternal()');
                        this.setBlockKeyboardEvent(true);

                        // creating a snapshot
                        snapshot ??= new Snapshot(this);

                        // show a message with cancel button
                        this.#docApp.getView().enterBusy({
                            warningLabel: gt('Sorry, pasting slides from clipboard will take some time.'),
                            cancelHandler: () => {
                                // restoring the document state
                                if (snapshot && !snapshot.destroyed) { snapshot.apply(); }
                                // calling abort function for operation promise
                                this.#docApp.enterBlockOperationsMode(() => {
                                    if (applyPromise && applyPromise.abort) {
                                        applyPromise.abort();
                                    }
                                });
                            }
                        });

                        // map the operations
                        operations = _(operations).map(transformOperation);

                        // concat with newly generated operations
                        newOperations = generator.getOperations();
                        if (newOperations.length) {
                            operations = newOperations.concat(operations);
                        }

                        // set clipboard debug pane content
                        this.trigger('debug:clipboard', operations);

                        // apply operations
                        applyPromise = this.applyOperationsAsync(operations)
                            .progress(progress => {
                                this.#docApp.getView().updateBusyProgress(progress);
                            });

                    } else {
                        globalLogger.warn('Editor.pasteInternalClipboard(): invalid cursor position');
                        applyPromise = $.Deferred().reject();
                    }

                    return applyPromise;
                };

                doPasteSlidesInternal()
                    .always(() => {
                        // leaving busy mode
                        this.leaveAsyncBusy();
                        // close undo group
                        undoDef.resolve();
                        // deleting the snapshot
                        if (snapshot) { snapshot.destroy(); }
                    }).done(() => {
                        this.changeToSlide(nextLayoutId || this.getSlideIdByPosition(anchorPosition));
                        this.executeDelayed(() => {
                            clearBrowserSelection();
                            setFocus(this.#docApp.getView().getSlidePane().getSlidePaneContainer());
                        });
                    });

                return undoDef.promise();

            }, this); // enterUndoGroup()
        }

        /**
         * Handler function for the events 'paste' and 'beforepaste'.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         */
        paste(event) {

            // the clipboard event data
            const clipboardData = getClipboardData(event);
            // the list items of the clipboard event data
            const items = clipboardData && clipboardData.items;
            // the list of mime types of the clipboard event data
            const types = clipboardData && clipboardData.types;
            // the operation data from the internal clipboard
            let eventData;
            // the operation data origin
            let eventDataOrigin;
            // the operation data origin id
            let eventDataOriginId;
            // the file reader
            let reader;
            // the list item of type text/html
            let htmlEventItem = null;
            // the list item of type text/plain
            let textEventItem = null;
            // the file list item
            let fileEventItem = null;
            // the file data object
            let fileObject = null;
            // the event URL data
            let urlEventData = null;
            // the focused node
            const focusNode = getFocus();

            // do nothing, if the paste happens into a comment
            if (isCommentTextframeNode(focusNode)) { return false; }

            if (event.type === 'beforepaste' && _.browser.Safari) { // 'beforepaste' must not be handled in Safari browser
                return false;
            }

            // bug 44261
            // we had a case where a paste event could not be canceld by calling prevent default in the key down handler,
            // so we need to check for blocked keyboard events here as well.
            if (this.getBlockKeyboardEvent()) {
                event.preventDefault();
                return false;
            }

            if (!this.#docApp.isEditable()) {
                //paste via burger-menu in FF and Chrome must be handled
                this.#docApp.rejectEditAttempt();
                event.preventDefault();
                return;
            }

            // handles the result of reading file data from the file blob received from the clipboard data api
            const onLoadHandler = evt => {
                const data = evt && evt.target && evt.target.result;
                if (isBase64(data)) {
                    this.#createOperationsFromExternalClipboard([{ operation: Op.INSERT_DRAWING, data, type: 'image', depth: 0 }]);
                } else {
                    this.#docApp.rejectEditAttempt('image');
                }
            };

            // returns true if the html clipboard has a matching clipboard id set
            const isHtmlClipboardIdMatching = html => {
                return ($(html).find('#ox-clipboard-data').attr('data-ox-clipboard-id') === this.getClipboardId());
            };

            // returns the operations attached to the html clipboard, or null
            const getHtmlAttachedOperations = html => {
                let operations;

                try {
                    operations = JSON.parse($(html).find('#ox-clipboard-data').attr('data-ox-operations') || '{}');
                } catch (e) {
                    globalLogger.warn('getHtmlAttachedOperations', e);
                    operations = null;
                }

                return operations;
            };

            // returns the origin attached to the html clipboard, or an empty String
            const getHtmlAttachedOrigin = html => {
                return ($(html).find('#ox-clipboard-data').attr('data-ox-origin') || '');
            };

            // returns the origin id attached to the html clipboard, or an empty String
            const getHtmlAttachedOriginId = html => {
                return ($(html).find('#ox-clipboard-data').attr('data-ox-origin-id') || '');
            };

            // sanitizes the mso-list CSS styles used by copy&paste from MS Office
            // Browsers like Firefox remove invalid CSS attribute names like 'mso-list'
            // and even remove valid attributes where the content doesn't match with the attribute.
            // Therefore the 'mso-list' attribute is replaced with the 'content' attribute.
            // And the content is changed from e.g. 'l0 level3 lfo1' to 'l0-level3-lfo1'.
            const sanitizeMsoListParagraphs = html => {
                if (!_.isString(html)) { return null; }
                // check if a MS List paragraph is present, look for class MsoListParagraphCxSpFirst, MsoListParagraphCxSpMiddle, or MsoListParagraphCxSpLast.
                if (html.indexOf('MsoListParagraphCxSp') === -1) { return html; }
                // replace mso-list CSS attributes
                return html.replace(/mso-list:(l\d+)\s+(level\d+)\s+(lfo\d+)/gi, 'content:"$1-$2-$3";');
            };

            // if the browser supports the clipboard api, look for operation data
            // from the internal clipboard to handle as internal paste.
            if (clipboardData) {
                eventData = clipboardData.getData('text/ox-operations');
                eventDataOrigin = clipboardData.getData('text/ox-origin');
                eventDataOriginId = clipboardData.getData('text/ox-origin-id');

                // don't use operations for text part of spreadsheet
                if (eventData && !this.#docApp.isSpreadsheetApp()) {
                    // prevent default paste handling for desktop browsers, but not for touch devices
                    if (_.device('desktop')) {
                        event.preventDefault();
                    }

                    // set the operations from the event to be used for the paste
                    this.setClipboardOperations((eventData.length > 0) ? JSON.parse(eventData) : []);
                    this.setClipboardOrigin(eventDataOrigin);
                    this.setClipboardOriginId(eventDataOriginId);

                    if (this.#isSlideCopied()) {
                        this.pasteSlidesInternal();
                    } else {
                        this.pasteInternalClipboard();
                    }
                    return;
                }

                // check if clipboardData contains a html item
                htmlEventItem = _.find(items, item => { return item.type.toLowerCase() === 'text/html'; });

                // Chrome doesn't paste images into a (content editable) div, check if clipboardData contains an image item
                fileEventItem = _.find(items, item => { return item.type.toLowerCase().indexOf('image') !== -1; });

                // check if we have a mime type to get an URL from
                urlEventData = clipboardData.getData(_.find(types, type => { return type.toLowerCase().indexOf('text/uri-list') !== -1; }));

                // check if clipboardData contains a plain item
                textEventItem = _.find(items, item => { return item.type.toLowerCase() === 'text/plain'; });

                if (htmlEventItem || textEventItem) {
                    (htmlEventItem || textEventItem).getAsString(content => {
                        let ops;

                        // sanitize the mso-list CSS styles used by copy&paste from MS Office
                        content = sanitizeMsoListParagraphs(content);

                        // set clipboard debug pane content
                        this.trigger('debug:clipboard', content);

                        // clean up html to put only harmless html into the <div>
                        const div = $('<div>').html(parseAndSanitizeHTML(content));

                        if (htmlEventItem) {
                            if (isHtmlClipboardIdMatching(div) && !this.#docApp.isSpreadsheetApp()) {
                                // if the clipboard id matches it's an internal copy & paste
                                // but don't use operations for text part of spreadsheet
                                if (this.#isSlideCopied()) {
                                    this.pasteSlidesInternal();
                                } else {
                                    this.pasteInternalClipboard();
                                }

                            } else {
                                this.setClipboardOrigin(getHtmlAttachedOrigin(div));
                                this.setClipboardOriginId(getHtmlAttachedOriginId(div));

                                ops = getHtmlAttachedOperations(div);
                                if (_.isArray(ops) && !this.#docApp.isSpreadsheetApp()) {
                                    // it's not an internal copy & paste, but we have clipboard operations piggy backed to use for clipboardOperations
                                    // but don't use operations for text part of spreadsheet
                                    this.setClipboardOperations(ops);
                                    if (this.#isSlideCopied()) {
                                        this.pasteSlidesInternal();
                                    } else {
                                        this.pasteInternalClipboard();
                                    }

                                } else {
                                    // we need to put the whole content to the DOM
                                    // otherwise, no "computed styles" will be added
                                    insertHiddenNodes(div);

                                    // use html clipboard
                                    ops = this.parseClipboard(div);

                                    // remove clipboard-content from DOM
                                    div.remove();

                                    this.#createOperationsFromExternalClipboard(ops);
                                }
                            }
                        } else {
                            //text only
                            //BUG 48910: Impossible to paste XML strings into OX Text
                            //BUG 50384: pasting > leads to &gt
                            ops = this.parseClipboardText(_.unescapeHTML(div.html()));
                            this.#createOperationsFromExternalClipboard(ops);
                        }
                    });

                } else if (fileEventItem) {
                    // Bug 50897 - fileEventItem.getAsFile() returns null even if image file item is present
                    fileObject = fileEventItem.getAsFile();
                    if (fileObject) {
                        reader = new window.FileReader();
                        reader.onload = onLoadHandler;
                        reader.readAsDataURL(fileObject);
                    }

                } else if (urlEventData && hasUrlImageExtension(urlEventData)) {
                    this.#createOperationsFromExternalClipboard([{ operation: Op.INSERT_DRAWING, data: urlEventData, depth: 0 }]);
                }

                if (htmlEventItem || fileEventItem || urlEventData || textEventItem) {
                    // prevent default paste handling of the browser
                    event.preventDefault();
                    return;
                }
            }

            // append the clipboard div to the body and place the cursor into it
            const clipboard = this.#docApp.getView().createClipboardNode();

            // focus and select the clipboard container node
            //this.getSelection().setBrowserSelectionToContents(clipboard);
            this.grabClipboardFocus(clipboard);

            // read pasted data
            this.executeDelayed(() => {

                let clipboardData;
                let operations;

                // set the focus back
                this.#docApp.getView().grabFocus();

                if (isHtmlClipboardIdMatching(clipboard) && this.hasInternalClipboard() && !this.#docApp.isSpreadsheetApp()) {
                    // if the clipboard id matches it's an internal copy & paste
                    // but don't use operations for text part of spreadsheet
                    if (this.#isSlideCopied()) {
                        this.pasteSlidesInternal();
                    } else {
                        this.pasteInternalClipboard();
                    }
                } else {
                    // look for clipboard origin
                    this.setClipboardOrigin(getHtmlAttachedOrigin(clipboard));
                    this.setClipboardOriginId(getHtmlAttachedOriginId(clipboard));

                    // look for clipboard operations
                    operations = getHtmlAttachedOperations(clipboard);
                    if (_.isArray(operations) && !this.#docApp.isSpreadsheetApp()) {
                        // it's not an internal copy & paste, but we have clipboard operations piggy backed to use for clipboardOperations
                        // but don't use operations for text part of spreadsheet
                        this.setClipboardOperations(operations);
                        if (this.#isSlideCopied()) {
                            this.pasteSlidesInternal();
                        } else {
                            this.pasteInternalClipboard();
                        }
                    } else {
                        // set clipboard debug pane content
                        this.trigger('debug:clipboard', clipboard);
                        // use html clipboard
                        clipboardData = this.parseClipboard(clipboard);
                        this.#createOperationsFromExternalClipboard(clipboardData);
                    }
                }

                // remove the clipboard node
                clipboard.remove();
                clearBrowserSelection();  // Bug #49804 - in order to set a new browser selection, we need to call selection.removeAllRanges() before.

                if (this.#docApp.getView().getSlidePane && this.useSlideMode()) {
                    setFocus(this.#docApp.getView().getSlidePane().getSlidePaneContainer());
                } else {
                    this.#docApp.getView().grabFocus();
                }
            });
        }

        /**
         * Removes all attributes that should not be used with clipboard operations.
         * Doesn't create a copy, modifies the given operations.
         *
         * @param {Array} operations
         *  The array with the clipboard operations to clean.
         *
         * @returns {Array}
         *  The array with the cleaned clipboard operations.
         */
        cleanClipboardOperations(operations) {
            // for copy/paste slides presentation family should be preserved
            const firstOp = operations && _.first(operations);
            const isPasteSlideMode = firstOp && (firstOp.name === 'insertSlide' || firstOp.name === 'insertLayoutSlide');

            const hasBulletAttrs = operation => {
                return (operation.attrs && operation.attrs.paragraph && _.isObject(operation.attrs.paragraph.bullet) && !_.isEmpty(operation.attrs.paragraph.bullet));
            };

            _.each(operations, operation => {
                if (_.isObject(operation.attrs)) {
                    // delete empty URL attribute to avoid generation of a hyperlink
                    if (operation.attrs.character && _.isEmpty(operation.attrs.character.url)) {
                        delete operation.attrs.character.url;
                    }

                    // remove presentation family for insert drawing operations. copied placeholder drawings are pasted as default drawings
                    if ((operation.name === Op.INSERT_DRAWING) && (operation.attrs.presentation) && !isPasteSlideMode) {
                        delete operation.attrs.presentation;
                    }

                    // sanitize empty bullet definitions
                    if (operation.attrs.paragraph && _.isObject(operation.attrs.paragraph.bullet) && _.isEmpty(operation.attrs.paragraph.bullet)) {
                        operation.attrs.paragraph.bullet.type = 'none';
                    }
                }

                // ODF uses a different default for bullets. therefore if no attributes are set the bullet type needs to be explicitely set to none
                if (this.#docApp.isODF() && !this.isPasteToSameDocumentType() && (operation.name === Op.PARA_INSERT) && !hasBulletAttrs(operation)) {
                    operation.attrs = _.extend({}, operation.attrs);
                    operation.attrs.paragraph = _.extend({ bullet: { type: 'none' } }, operation.attrs.paragraph);
                }
            });

            return operations;
        }

        /**
         * Handler function to process drop events from the browser.
         *
         * @param {Object} event
         *  The drop event sent by the browser.
         */
        processDrop(event) {

            event.preventDefault();

            if (!this.#docApp.isEditable()) {
                return false;
            }
            // inactive selection in header&footer
            if (event.target.classList.contains('inactive-selection') || $(event.target).parents('.inactive-selection').length > 0) {
                this.getNode().find('.drop-caret').remove();
                return false;
            }

            // don't drop content if the presentation hasn't at least one slide
            if (this.#docApp.isPresentationApp() && this.isEmptySlideView()) {
                this.getNode().find('.drop-caret').remove();
                return false;
            }

            const activeRootNode = this.getSelection().getRootNode();

            // make sure drop target is inside activeRootNode, otherwise getOxoPositionFromPixelPosition() would fail
            if (!containsNode(activeRootNode, event.target, { allowSelf: true })) {
                return false;
            }

            const files = event.originalEvent.dataTransfer.files;
            const dropX = event.originalEvent.clientX;
            const dropY = event.originalEvent.clientY;
            let dropPosition = getOxoPositionFromPixelPosition(activeRootNode, dropX, dropY);

            // #52874 - use current selection start as fallback if no oxo position can be determined from drop position
            if (!dropPosition || !_.isArray(dropPosition.start) || _.isEmpty(dropPosition.start)) {
                dropPosition = this.getSelection().getStartPosition();
            }

            // validating the drop position. This must be a text position. The parent must be a paragraph
            if (dropPosition && dropPosition.start && !getParagraphElement(activeRootNode, _.initial(dropPosition.start))) {
                if (getParagraphElement(activeRootNode, dropPosition.start)) {
                    dropPosition.start.push(0);
                } else {
                    return false;
                }
            }

            if (!files || files.length === 0) {

                // try to find out what type of data has been dropped
                const types = event.originalEvent.dataTransfer.types;
                let detectedDropDataType = null, div = null, url = null, text = null;
                let assuranceLevel = 99, lowerCaseType = null, operations = null, ops = null;

                if (types && types.length > 0) {

                    _(types).each(type => {

                        lowerCaseType = type.toLowerCase();

                        if (lowerCaseType === 'text/ox-operations') {
                            operations = event.originalEvent.dataTransfer.getData(type);
                            // set the operations from the event to be used for the paste
                            this.setClipboardOperations((operations.length > 0) ? JSON.parse(operations) : []);
                            assuranceLevel = 0;
                            detectedDropDataType = 'operations';

                        } else if (lowerCaseType === 'text/html') {

                            const html = event.originalEvent.dataTransfer.getData(type);
                            if (html && assuranceLevel > 1) {
                                // clean up html to put only harmless html into the <div>
                                div = $('<div>').html(parseAndSanitizeHTML(html));
                                if (div.children().length > 0) {
                                    // Unfortunately we sometimes get broken html from Firefox (D&D from Chrome).
                                    // So it's better to use the plain text part.
                                    assuranceLevel = 1;
                                    detectedDropDataType = 'html';
                                }
                            }
                        } else if (lowerCaseType === 'text/uri-list' || lowerCaseType === 'url') {

                            const list = event.originalEvent.dataTransfer.getData(type);
                            if (list && (list.length > 0) && (assuranceLevel > 3)) {
                                assuranceLevel = 3;
                                detectedDropDataType = 'link';
                                url = list;
                            }
                        } else if (lowerCaseType === 'text/x-moz-url') {
                            // FF sometimes (image D&D Chrome->FF) provides only this type
                            // instead of text/uri-list so we are forced to support it, too.
                            const temp = event.originalEvent.dataTransfer.getData(type);
                            // x-moz-url is defined as link + '\n' + caption
                            if (temp && temp.length > 0 && (assuranceLevel > 2)) {
                                const array = temp.split('\n');
                                url = array[0];
                                if (array.length > 1) {
                                    text = array[1];
                                }
                                assuranceLevel = 2;
                                detectedDropDataType = 'link';
                            }
                        } else if (lowerCaseType === 'text/plain' || lowerCaseType === 'text') {

                            const plainText = event.originalEvent.dataTransfer.getData(type);
                            if (plainText && (assuranceLevel > 4)) {
                                assuranceLevel = 4;
                                detectedDropDataType = 'text';
                                text = plainText;
                            }
                        }
                    });
                } else {
                    // IE sometimes don't provide any types but they are accessible getData()
                    // So try to check if we have a Url to check for.
                    url = event.originalEvent.dataTransfer.getData('Url');
                    if (url && url.length > 0) {
                        detectedDropDataType = 'link';
                    }
                }

                if (detectedDropDataType === 'operations') {
                    // use clipboard code to insert operations to document
                    this.pasteInternalClipboard(dropPosition);
                } else if (detectedDropDataType === 'html') {
                    if (div && div.children().length > 0) {
                        // drag&drop detected html
                        ops = this.parseClipboard(div);
                        this.#createOperationsFromExternalClipboard(ops, dropPosition);
                    }
                } else if (detectedDropDataType === 'link') {
                    // insert detected hyperlink
                    const setText = text || url;
                    if (setText && setText.length) {
                        if (hasUrlImageExtension(url) || isBase64(url)) {
                            // text part of spreadsheet allows only plain text
                            if (!this.#docApp.isSpreadsheetApp()) {
                                this.insertImageURL(url, { position: (this.useSlideMode() ? null : dropPosition.start) });
                            }

                        } else {
                            ops = [
                                { operation: Op.TEXT_INSERT, data: setText },
                                { operation: 'insertHyperlink', data: url, length: setText.length }
                            ];
                            this.#createOperationsFromExternalClipboard(ops, dropPosition);
                        }
                    }
                } else if (detectedDropDataType === 'text') {
                    if (text && text.length > 0) {
                        this.#insertPlainTextFormatted(text, dropPosition);
                    }
                } else {
                    // fallback try to use 'text' to at least get text
                    text = event.originalEvent.dataTransfer.getData('text');
                    if (text && text.length > 0) {
                        this.#insertPlainTextFormatted(text, dropPosition);
                    }
                }
            } else {
                if (this.#docApp.isTextApp() && (isInsideCommentLayerNode(event.target))) {
                    // inserting images into comments not possible
                    this.getNode().find('.drop-caret').remove();
                    return false;
                }
                this.#processDroppedImages(event, dropPosition);
            }

            // always clean caret on every drop
            this.executeDelayed(() => {
                this.getNode().find('.drop-caret').remove();
                // set selection to dropped position
                if (dropPosition) {
                    this.getSelection().setTextSelection(dropPosition.start);
                }
            }, 500);

            return false;
        }

        /**
         * Handler to process dragStart event from the browser.
         *
         * @param {jQuery.event} event
         *  The browser event sent via dragStart
         */
        processDragStart(event) {

            // the data transfer object
            const dataTransfer = event && event.originalEvent && event.originalEvent.dataTransfer;
            // the data transfer operations
            let dataTransferOperations = null;
            // the selection object
            const selection = this.getSelection();

            switch (selection.getSelectionType()) {

                case 'text':
                case 'drawing':
                    dataTransferOperations = this.#copyTextSelection();
                    break;

                case 'cell':
                    dataTransferOperations = this.#copyCellRangeSelection();
                    break;

                default:
                    globalLogger.error('Editor.processDragStart(): unsupported selection type: ' + selection.getSelectionType());
            }

            // if browser supports DnD api add data to the event
            if (dataTransfer) {
                // add operation data
                if (!_.browser.IE) {
                    if (dataTransferOperations) {
                        dataTransfer.setData('text/ox-operations', JSON.stringify(dataTransferOperations));
                    }
                    // add plain text and html of the current browser selection
                    dataTransfer.setData('text/plain', selection.getTextFromBrowserSelection());
                    dataTransfer.setData('text/html', Export.getHTMLFromSelection(this));
                } else {
                    // IE just supports 'Text' & Url. Text is more generic
                    dataTransfer.setData('Text', selection.getTextFromBrowserSelection());
                }
            }
        }

        /**
         * Handler for 'dragenter' event of the editor root node.
         * - create and appends drop caret, if user drags over editor root node
         *
         * @param {jQuery.Event} event
         *  The browser event sent via dragEnter
         */
        processDragEnter(event) {

            // the page node
            const pageNode = this.getNode();

            event.originalEvent.dataTransfer.dropEffect = 'copy';

            if (pageNode.find(event.originalEvent.target).length > 0 || isPageNode(event.originalEvent.target)) {
                const collabOverlay = pageNode.find('.collaborative-overlay');
                let dropCaret = pageNode.find('.drop-caret');

                if (dropCaret.length === 0) {
                    dropCaret = $('<div>').addClass('drop-caret');
                    collabOverlay.append(dropCaret);
                }
            }
        }

        /**
         * Handler for 'dragleave' event of the editor root node.
         * - clears drop caret, if user drags out of the editor root node.
         *
         * @param {jQuery.Event} event
         *  The browser event sent via dragLeave
         */
        processDragLeave(event) {
            if (isPageNode(event.originalEvent.target)) {
                this.getNode().find('.drop-caret').remove();
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Returns the logical position for pasting new content according to
         * the drop position, the current selection and the operations to paste.
         *
         * @param {Object} options
         *  The options that have impact on the paste anchor position:
         *      @param {Object} options.dropPosition
         *      The position for the drop operation.
         *      @param {Boolean} options.isDrawingInClipboard
         *      Whether the clipboard data contains drawing operations.
         *      @param {Boolean} options.isTableInClipboard
         *      Whether the clipboard data contains table operations.
         *      @param {Boolean} options.isOnlyInsertImageInClipboard
         *      Whether the clipboard data contains only insert image operations.
         *      @param {Boolean} options.isInternalClipboard
         *      Whether the paste is handled via internal or external clipboard
         *
         * @returns {Number[]}
         *  The logical anchor position for pasting.
         */
        #getAnchorPositionForPaste(options) {

            // the position for the drop operation
            const dropPosition = getObjectOption(options, 'dropPosition', null);
            // whether the clipboard data contains drawing operations
            const isDrawingInClipboard = getBooleanOption(options, 'isDrawingInClipboard', false);
            // whether the clipboard data contains table operations
            const isTableInClipboard = getBooleanOption(options, 'isTableInClipboard', false);
            // whether the clipboard data consists of insert image operations only
            const isOnlyInsertImageInClipboard = getBooleanOption(options, 'isOnlyInsertImageInClipboard', false);
            // whether the paste is handled via internal or external clipboard
            const isInternalClipboard = getBooleanOption(options, 'isInternalClipboard', true);
            // the selection object
            const selection = this.getSelection();
            // whether a drop position is given
            const hasDropPosition = dropPosition && _.isArray(dropPosition.start) && !_.isEmpty(dropPosition.start);
            // whether one or more groups or more then one drawings are selected
            const isMultiOrGroupSelection = this.#isMultiSelectionOrGroupSelected();
            // whether the current selection is inside an existing table
            const isSelectionInsideTable = (selection.getEnclosingTable() !== null);
            // whether the current selection is a place holder without text support
            const isPlaceHolderWithoutTextSelection = !!this.isPlaceHolderWithoutTextDrawing && this.isPlaceHolderWithoutTextDrawing(selection.getSelectedDrawing());
            // the resulting anchor position
            let anchorPosition = hasDropPosition ? dropPosition.start : selection.getStartPosition();
            // the generator for additional operations
            const generator = this.createOperationGenerator();
            // the clipboard operations
            let operations = this.getClipboardOperations();

            if (this.#docApp.isPresentationApp()) {
                // determine anchor position for Presentation
                if (isOnlyInsertImageInClipboard && (selection.isTopLevelTextCursor() || this.#isPlaceHolderDrawingSelection())) {
                    // if the clipboard contains only insert image operations, replace place holder drawing when available or paste to top level.
                    // therefore keep the selection to determine the place holder drawings and set the anchor for the top level paste.
                    anchorPosition = this.getNextAvailablePositionInActiveSlide();

                } else if (selection.isTopLevelTextCursor() || isDrawingInClipboard || isMultiOrGroupSelection || isPlaceHolderWithoutTextSelection) {
                    // do not append into a group or a multi selection
                    // paste to top level if the clipboard contains drawings.
                    // do not append into placeholder that don't support text
                    anchorPosition = this.getNextAvailablePositionInActiveSlide();
                    if (isDrawingInClipboard || isMultiOrGroupSelection || isPlaceHolderWithoutTextSelection) {
                        selection.setTextSelection(anchorPosition);
                    }

                } else if (selection.isTextFrameSelection()) {
                    // if a text frame is selected, the new content is inserted into the text frame
                    selection.setSelectionIntoTextframe();
                    anchorPosition = selection.getStartPosition();
                }

            } else if (this.#docApp.isTextApp()) {
                // determine anchor position for Text

                // when pasting a table and the paste target is not a paragraph start position
                // prepend the operations with an additional split paragraph operation
                // but only if the paste target is inside a table or the first clipboard operation is insert table
                if (isInternalClipboard && isTableInClipboard && !this.#isParagraphStartPosition(anchorPosition) && (isSelectionInsideTable || this.#isFirstOperationInsertTable(operations))) {
                    // the operations are based to position [0], so start at [-1] in order to prepend the other operations
                    generator.generateOperation(Op.PARA_SPLIT, { start: [-1, _.last(anchorPosition)] });

                    // set anchor position at the start of the newly inserted paragraph
                    anchorPosition[anchorPosition.length - 2] += 1;
                    anchorPosition[anchorPosition.length - 1]  = 0;

                    // prepend the new operation to the clipboard operations
                    operations = generator.getOperations().concat(operations);
                    this.setClipboardOperations(operations);
                }

            } else if (this.#docApp.isSpreadsheetApp()) {
                // determine anchor position for Spreadsheet
                if (!isPositionInsideTextframe(this.getNode(), anchorPosition)) {
                    // set selection into the text frame
                    if (selection.setSelectionIntoTextframe()) {
                        this.#docApp.getView().enterTextEditMode('drawing', { restart: true }); // activating required (snapshot), 53758
                        anchorPosition = selection.getStartPosition();
                    } else {
                        globalLogger.info('Clipboard - #getAnchorPositionForPaste() - could not set anchor position into text frame. Using anchor position:', anchorPosition);
                    }
                }
            }

            return anchorPosition;
        }

        /**
         * Returns true if the given position is at a paragraph start
         */
        #isParagraphStartPosition(position) {
            return _.isArray(position) && (_.last(position) === 0);
        }

        /**
         * Resets the internal list style number which forces 'this.#getFreeListStyleId'
         * to sync with the list collection.
         */
        #resetListStyleId() {

            const listCollection = this.getListCollection();
            let sFreeId = null;

            if (listCollection && listCollection.getFreeListId) {
                sFreeId = listCollection.getFreeListId();
                this.#listStyleNumber = parseInt(sFreeId.slice(1, sFreeId.length), 10);
            }
            if (!_.isNumber(this.#listStyleNumber)) {
                this.#listStyleNumber = 1;
            }
        }

        /**
         * Returns the next free list style id.
         * The list collection is only aware of the list styles that have been
         * applied by an 'insertList' operation, so the style ids for newly
         * generated operations need to be handled separately.
         *
         * @returns {String}
         *  The next free list style id.
         */
        #getFreeListStyleId() {
            const listCollection = this.getListCollection();
            let sFreeId = 'L';

            while (listCollection && listCollection.hasListStyleId('L' + this.#listStyleNumber)) {
                this.#listStyleNumber++;
            }
            sFreeId += this.#listStyleNumber;
            this.#listStyleNumber++;
            return sFreeId;
        }

        /**
         * Check, whether the clipboard operations contain only drawings (or content
         * inside drawings).
         * In the case of internal clipboard, a mixture of drawing and text selection
         * is not possible. Therefore the clipboard contains only drawings, if at least
         * one 'insertDrawing' operation is included in the list of operations. This
         * can include text operations in the drawing (text frame).
         * In the case of the external clipboard it is possible, that there is a text
         * selection next to a drawing selection. But this is not handled within this
         * function.
         *
         * @returns {Boolean}
         *  Whether only drawings are top level elements in the clip board.
         */
        #checkClipboardForDrawings(operations) {
            return _.filter(operations, operation => { return operation.name === Op.INSERT_DRAWING; }).length > 0;
        }

        /**
         * Returns true if the given operations contain insert table.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Boolean}
         *  Whether the operations contain insert table.
         */
        #checkClipboardForTables(operations) {
            return _.any(operations, operation => { return operation.name === Op.TABLE_INSERT; });
        }

        /**
         * Returns true if the first of given operations is insert table.
         */
        #isFirstOperationInsertTable(operations) {
            const first = _.first(operations);
            return (first && first.name === Op.TABLE_INSERT);
        }

        /**
         * Returns true if the given operations are only insertDrawing of type image.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Boolean}
         *  Whether the operations are only insertDrawing of type image.
         */
        #isOnlyInsertImageOperations(operations) {
            return _.every(operations, operation => { return operation.name === Op.INSERT_DRAWING && operation.type === 'image'; });
        }

        /**
         * If drawings are pasted from OX Text, they should not generate additional drawings in OX Presentation that come
         * from setAttributes operations of the paragraph node in OX Text. In OX Text all drawings are children of a paragraph.
         * In OX Presentation the drawings are children of the slide.
         *
         * Maybe this function becomes more generic in the future, to handle more than this special case.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         */
        #isOnlyInsertDrawingOperationInSetAttributesContainer(operations) {
            if (operations.length >= 2 && operations[0].name === Op.SET_ATTRIBUTES && operations[1].name === Op.INSERT_DRAWING) {
                if (operations[0].start.length < operations[1].start.length && comparePositions(operations[0].start, operations[1].start, operations[0].start.length) === 0) {
                    if (operations.length === 2) {
                        return true;
                    } else {
                        // are all following operations children of the drawing? (DOCS-3100)
                        const drawingPos = operations[1].start;
                        if (!_.find(operations.slice(2), op => { return !op.start || comparePositions(op.start, drawingPos, drawingPos.length) !== 0; })) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /**
         * Returns true if operation inserts a preset or custom shape.
         *
         * @param {Array} operation
         *  The clipboard operation to check.
         *
         * @returns {Boolean}
         *  Whether the operation inserts a preset or custom shape.
         */
        #isPresetOrCustomShapeOperation(operation) {
            return operation && operation.attrs && (operation.name === Op.INSERT_DRAWING) && (operation.type === 'shape') && _.isObject(operation.attrs.geometry);
        }

        /**
         * Returns true if more than one drawing or a group is selected.
         *
         * @returns {Boolean}
         *  Whether the selection is a multi selection or group selection.
         */
        #isMultiSelectionOrGroupSelected() {
            // the selection object
            const selection = this.getSelection();
            return (selection.isMultiSelectionSupported() && selection.isMultiSelection()) || selection.isAnyDrawingGroupSelection();
        }

        /**
         * Returns whether the passed drawing is a placeholder.
         *
         * @returns {Boolean}
         *  Whether the passed drawing is a placeholder.
         */
        #isPlaceHolderDrawing(drawingFrame) {
            return !!this.isPlaceHolderDrawing && this.isPlaceHolderDrawing(drawingFrame);
        }

        /**
         * Returns true if a place holder drawing is selected.
         *
         * @returns {Boolean}
         *  Whether a holder drawing is selected.
         */
        #isPlaceHolderDrawingSelection() {
            return this.#isPlaceHolderDrawing(this.getSelection().getAnyDrawingSelection());
        }

        /**
         * Convenience function to select all top level drawings after pasting.
         *
         * @param {Object[]} operations
         *  The array with the clipboard operations.
         *
         * @returns {Boolean}
         *  Whether the selection was set within this function. This is the case, if at
         *  least one top level drawing was inserted during pasting.
         */
        #selectTopLevelDrawings(operations) {

            const allPositions = [];
            let selectionSet = false;

            _.each(operations, op => { // no grouped drawings, only top level -> copy/paste of several slides
                if (op.name === Op.INSERT_DRAWING && op.start.length === 2) {
                    allPositions.push(op.start);
                }
            });

            if (allPositions.length > 0) {
                // selection all top level inserted drawings (never the content of the drawings)
                this.getSelection().setMultiDrawingSelectionByPosition(allPositions);
                selectionSet = true;
            }

            return selectionSet;
        }

        /**
         * Returns true if the array begins with the sub array.
         *
         * @param {Array} array
         *  The array to check if it begins with the sub array.
         *
         * @param {Array} sub
         *  The sub array to check the given array with.
         *
         * @returns {Boolean}
         *  Whether the array starts with the sub array.
         */
        #arrayStartsWith(array, sub) {
            if (!_.isArray(array) || !_.isArray(sub)) { return false; }
            return _.every(sub, (v, i) => {
                return array[i] === v;
            });
        }

        /**
         * Returns all drawing nodes that are present on the currently selected slide.
         *
         * @returns {jQuery}
         *  A collection with all drawing nodes of the selected slide.
         */
        #getDrawingNodesOfCurrentSlide() {

            // the active slide node
            const slide = this.getActiveSlide ? this.getActiveSlide() : null;

            return slide ? slide.children(ABSOLUTE_DRAWING_SELECTOR) : $();
        }

        /**
         * Returns the drawing attributes for the given drawing node.
         *
         * @param {jQuery|Node} drawingNode
         *  The drawing node that will get new values assigned.
         *
         * @returns {Object}
         *  The attribute set if existing, otherwise an empty object.
         */
        #getDrawingAttributes(drawingNode) {

            let attrs = null;

            if (this.#isPlaceHolderDrawing(drawingNode)) {
                // the merged attributes of the specified place holder drawing
                attrs = this.drawingStyles.getElementAttributes(drawingNode);
            } else {
                // the explicit attributes at the specified drawing
                attrs = getExplicitAttributeSet(drawingNode);
            }

            return attrs;
        }

        /**
         * Calculates the minimum top/left position of the selected drawings.
         *
         * @returns {Object}
         *  An object with the minimum top and left position.
         */
        #getMinPositionForSelectedDrawings() {
            return this.#getMinPositionForDrawings(this.getSelection().getSelectedDrawings());
        }

        /**
         * Calculates the minimum top/left position of the drawing selection
         * defined by the given drawing nodes.
         *
         * @param {Array} drawingNodes
         *  The selected drawing nodes.
         *
         * @returns {Object}
         *  An object with the minimum top and left position.
         */
        #getMinPositionForDrawings(drawingNodes) {
            const position = {};

            _.each(drawingNodes, node => {
                const attrs = this.#getDrawingAttributes(node);
                if (attrs && attrs.drawing) {
                    // exchanges the top, left position if they are smaller than the current
                    if ((attrs.drawing.top > 0) && (!('top' in position) || (attrs.drawing.top < position.top))) {
                        position.top = attrs.drawing.top;
                    }
                    if ((attrs.drawing.left > 0) && (!('left' in position) || (attrs.drawing.left < position.left))) {
                        position.left = attrs.drawing.left;
                    }
                }
            });

            // make sure to have defaults
            return _.extend({ top: 0, left: 0 }, position);
        }

        /**
         * Returns default attributes for text frames which need to be inserted if plain text is pasted to the top level.
         *
         * @returns {Object}
         *  The default attributes object
         */
        #getDefaultDrawingAttributes() {
            // the default width of the textframe in hmm
            const defaultWidth = 7500;
            // the default drawing attributes
            const drawingAttrs = extendOptions({ width: defaultWidth }, this.getInsertDrawingAttibutes && this.getInsertDrawingAttibutes());
            // the default border attributes
            const lineAttrs = { color: Color.BLACK, style: 'single', type: 'none', width: getWidthForPreset('thin') };
            // the default fill color attributes
            const fillAttrs = { color: Color.AUTO, type: 'solid' };
            // the default attributes
            const attrs = { drawing: drawingAttrs, shape: { autoResizeHeight: true, noAutoResize: false }, line: lineAttrs, fill: fillAttrs };
            // adding styleId (only for ODT)
            if (this.#docApp.isODF()) { attrs.styleId = 'Frame'; }

            return attrs;
        }

        /**
         * Special handling for property 'autoResizeHeight' (56351) in ODT Text application.
         *
         * @param {Object[]} operations
         *  The array with the clipboard operations.
         */
        #checkODTTextProperties(operations) {

            _.each(operations, operation => {
                if (operation.name === Op.INSERT_DRAWING) {
                    if (!operation.attrs) { operation.attrs = {}; }
                    if (!operation.attrs.shape) { operation.attrs.shape = {}; }

                    // setting property autoResizeHeight explicitely to false, if not already specified
                    if (!_.has(operation.attrs.shape, 'autoResizeHeight')) {
                        operation.attrs.shape.autoResizeHeight = false;
                    }
                }
            });
        }

        /**
         * Moving the pasted drawing, so that it does not cover the original drawing
         * completely.
         * Info: This function can only be called in slide mode.
         */
        #shiftDrawingPositions(operations, anchor) {

            // the top and left offset to place the drawings with
            const offset = 500;
            // the position to place the next drawing when pasted from OX Text
            const currentPosition = anchor || { top: 0, left: 0 };
            // all drawings on the slide
            const allDrawings = this.#getDrawingNodesOfCurrentSlide();
            // all attributes of the drawings on the slide
            const allDrawingAttrs = _.map(allDrawings, oneDrawing => this.#getDrawingAttributes(oneDrawing));
            // the default drawing attributes
            const defaultAttrs = this.#getDefaultDrawingAttributes();

            // returns true if the given operation is a drawing and not inside a group.
            const isRootLevelDrawing = operation => {
                return (operation && operation.name === Op.INSERT_DRAWING && operation.start && operation.start.length === 2);
            };

            // returns true if the given position overlaps one of the existing drawings
            const isPositionOverlappingExistingDrawing = position => {
                // check the attributes of all drawings if position overlaps
                const result = _.any(allDrawingAttrs, attrs => {
                    const top = attrs.drawing.top;
                    const left = attrs.drawing.left;
                    const bottom = attrs.drawing.top + offset;
                    const right = attrs.drawing.left + offset;
                    // the overlapping rectangle is defined by the position plus an offset in width and height
                    return (position.top >= top && position.top < bottom && position.left >= left && position.left < right);
                });

                return result;
            };

            // get a drawing position that doesn't overlap existing drawings
            const getNonOverlappingDrawingPosition = operation => {
                let position = null;

                if (this.isClipboardOriginPresentation()) {
                    position = {
                        top:  (operation.attrs && operation.attrs.drawing && operation.attrs.drawing.top) || 0,
                        left: (operation.attrs && operation.attrs.drawing && operation.attrs.drawing.left) || 0
                    };
                } else {
                    position = currentPosition;
                    currentPosition.left += offset;
                    currentPosition.top  += offset;
                }

                while (isPositionOverlappingExistingDrawing(position)) {
                    position.top += offset;
                    position.left += offset;
                }

                return position;
            };

            // assures that the drawing has all necessary attributes
            const sanitizeDrawingAtrributes = operation => {
                if (!_.isObject(operation.attrs) || !_.isObject(operation.attrs.drawing)) {
                    // if no attributes given use the default attributes
                    operation.attrs = defaultAttrs;
                } else if (!_.isNumber(operation.attrs.drawing.width)) {
                    // fix drawing width and set auto height
                    operation.attrs.drawing.width = defaultAttrs.drawing.width;

                    if (!_.isNumber(operation.attrs.drawing.height)) {
                        operation.attrs.shape = _.extend({ autoResizeHeight: true, noAutoResize: false }, operation.attrs.shape);
                    }
                }
            };

            // shift positions of insert drawing operations
            _.each(operations, operation => {
                let position = null;

                if (isRootLevelDrawing(operation) && !this.isPlaceHolderAttributeSet(operation.attrs)) {
                    // new non overlapping position
                    position = getNonOverlappingDrawingPosition(operation);
                    // set new position
                    if (position && operation.attrs && operation.attrs.drawing) {
                        operation.attrs.drawing.top = position.top;
                        operation.attrs.drawing.left = position.left;
                    }
                }

                if (operation.name === Op.INSERT_DRAWING && operation.type === 'shape') {
                    sanitizeDrawingAtrributes(operation);
                }
            });
        }

        /**
         * Maps OX Text list number formats to OX Presentation list numbering types
         *
         * @param {String} numberFormat
         *  The list level number format.
         *
         * @param {String} levelText
         *  The text for the list level.
         *
         * @returns {String}
         *  The OX Presentation list numbering type.
         */
        #getNumberingType(numberFormat, levelText) {
            let numberingType;
            // the first part of the numbering type is defined by the number format
            switch (numberFormat) {
                case 'lowerRoman':
                    numberingType = 'romanLc';
                    break;
                case 'upperRoman':
                    numberingType = 'romanUc';
                    break;
                case 'lowerLetter':
                    numberingType = 'alphaLc';
                    break;
                case 'upperLetter':
                    numberingType = 'alphaUc';
                    break;
                case 'decimal':
                default:
                    numberingType = 'arabic';
                    break;
            }
            // the second part of the numbering type is defined by the level text
            if (/^\(%\d+\)/.test(levelText)) {
                numberingType += 'ParenBoth';
            } else if (/^%\d+\)/.test(levelText)) {
                numberingType += 'ParenR';
            } else {
                numberingType += 'Period';
            }
            return numberingType;
        }

        /**
         * Returns OX Presentation paragraph list attributes from the given MS Word list level definition.
         *
         * @param {Object} levelDefinition
         *  The MS Word list level definition.
         *  @param {String} levelDefinition.numberFormat
         *      The list number format (MS Word).
         *  @param {String} levelDefinition.levelText
         *      The text for the list level (MS Word).
         *  @param {String} levelDefinition.fontFamily
         *      The level text font.
         *  @param {Number} levelDefinition.indentFirstLine
         *      The first line text indent in 1/100 of millimeters.
         *  @param {Number} levelDefinition.indentLeft
         *      The left indent in 1/100 of millimeters.
         *  @param {String} levelDefinition.numberPosition
         *      The number position.
         *
         * @returns {Object}
         *  The paragraph list attributes:
         *  - {Number} indentFirstLine
         *      The first line text indent in 1/100 of millimeters.
         *  - {Number} indentLeft
         *      The left indent in 1/100 of millimeters.
         *  - {Object} bullet
         *      The bullet definition
         *      - {String} bullet.type
         *          The bullet type, either 'character' or 'numbering'.
         *      - {String} bullet.character
         *          The character for bullets of type 'character'.
         *      - {String} bullet.numType
         *          The number format definition for bullets of type 'numbering'.
         *  - {Object} bulletFont
         *      The font definition for the bullet.
         *      - {String} bulletFont.name
         *          The font name.
         *      - {Boolean} bulletFont.followText
         *          If true, the bullet is as big as the following text height.
         */
        #getSlideModeListParagraphAttributesFromLevelDefinition(levelDefinition) {
            // the resulting paragraph attributes
            const result = {};
            let numType = null;
            let startAt = null;

            if (levelDefinition) {
                // indentation
                if (_.isNumber(levelDefinition.indentLeft)) {
                    result.indentLeft = levelDefinition.indentLeft;
                }
                if (_.isNumber(levelDefinition.indentFirstLine)) {
                    result.indentFirstLine = levelDefinition.indentFirstLine;
                }
                // number format
                if (levelDefinition.numberFormat === 'bullet') {
                    // the definition of the bullet style
                    result.bullet = { type: 'character', character: levelDefinition.levelText || '\u25CF' };
                    result.bulletFont = { followText: false, name: levelDefinition.fontName || 'Times New Roman' };
                } else {
                    // the definition of the numbering style
                    numType = this.#getNumberingType(levelDefinition.numberFormat, levelDefinition.levelText);
                    startAt = (_.isNumber(levelDefinition.listStartValue)) ? levelDefinition.listStartValue : 1;

                    result.bullet = { type: 'numbering', numType, startAt };
                    result.bulletFont = { followText: true };
                }
            }

            return result;
        }

        /**
         * Returns OX Presentation paragraph list attributes from the given HTML list style type.
         *
         * @param {String} type
         *  The HTML list style type.
         *
         * @param {Number} listLevel
         *  The list level.
         *
         * @returns {Object}
         *  The paragraph list attributes:
         *  - {Number} indentFirstLine
         *      The first line text indent in 1/100 of millimeters.
         *  - {Number} indentLeft
         *      The left indent in 1/100 of millimeters.
         *  - {Object} bullet
         *      The bullet definition
         *      - {String} bullet.type
         *          The bullet type, either 'character' or 'numbering'.
         *      - {String} bullet.character
         *          The character for bullets of type 'character'.
         *      - {String} bullet.numType
         *          The number format definition for bullets of type 'numbering'.
         *  - {Object} bulletFont
         *      The font definition for the bullet.
         *      - {String} bulletFont.name
         *          The font name.
         *      - {Boolean} bulletFont.followText
         *          If true, the bullet is as big as the following text height.
         */
        #getSlideModeListParagraphAttributesFromHTMLType(type, listLevel) {
            // default value for the list paragraph left indent
            const indentLeftDefault = 1500;
            // default indent for the first line of a list paragraph
            const indentFirstLineDefault = -1500;
            // the resulting paragraph attributes
            const result = { bulletFont: { followText: true } };

            // set default indents
            if (_.isNumber(listLevel) && listLevel >= 0) {
                result.indentLeft = (listLevel + 1) * indentLeftDefault;
                result.indentFirstLine = indentFirstLineDefault;
            }

            // set bullet or numbering attributes
            switch (type) {
                case 'decimal':
                case 'decimal-leading-zero':
                case '1':
                default:
                    result.bullet = { type: 'numbering', numType: 'arabicPeriod' };
                    break;

                case 'A':
                case 'upper-alpha':
                case 'upper-latin':
                    result.bullet = { type: 'numbering', numType: 'alphaUcPeriod' };
                    break;

                case 'a':
                case 'lower-alpha':
                case 'lower-latin':
                    result.bullet = { type: 'numbering', numType: 'alphaLcPeriod' };
                    break;

                case 'I':
                case 'upper-roman':
                    result.bullet = { type: 'numbering', numType: 'romanUcPeriod' };
                    break;

                case 'i':
                case 'lower-roman':
                    result.bullet = { type: 'numbering', numType: 'romanLcPeriod' };
                    break;

                case 'disc':
                    result.bullet = { type: 'character', character: '\uf0b7' };
                    result.bulletFont = { followText: false, name: 'Symbol' };
                    break;

                case 'circle':
                    result.bullet = { type: 'character', character: '\u25CB' };
                    result.bulletFont = { followText: false, name: 'Times New Roman' };
                    break;

                case 'square':
                    result.bullet = { type: 'character', character: '\u25A0' };
                    result.bulletFont = { followText: false, name: 'Times New Roman' };
                    break;

                case 'none':
                    result.bullet = { type: 'character', character: ' ' };
                    result.bulletFont = { followText: false, name: 'Times New Roman' };
                    break;
            }

            return result;
        }

        /**
         * Transforms OX Text operations to be used with slide mode, e.g. OX Presentation.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #transformOperationsToSlideMode(operations) {
            // the operations generator
            const generator = this.createOperationGenerator();
            // the selection object
            const selection = this.getSelection();
            // whether the selected drawing is a drawing that contains place holder attributes
            const isPlaceHolderDrawingSelected = this.#isPlaceHolderDrawing(selection.getAnyTextFrameDrawing({ forceTextFrame: true }));
            // the default attributes for new drawings
            const attrs = this.#getDefaultDrawingAttributes();
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // the current operation to transform
            let operation = null;
            // the newly created operation
            let newOperation = null;
            // the index of the currently transformed operation
            let index = 0;
            // the resulting slide mode operations
            const resultOperations = [];
            // the new base position for root level operations
            const rootLevelStart = [0, 0];
            // the new base position for drawings
            let drawingStart = [0, 0];
            // the new base position for text frames
            let textFrameStart = [0, 0];
            // the positions of group drawings
            const groupPositions = [];
            // the new base position for groups
            let groupStart = [0, 0];
            // the list definitions taken from insertListStyle operations
            const listDefinitions = {};

            // check for operations first
            if (!operations || operations.length < 1) { return []; }
            // do nothing if paste target is not OX Text
            if (!this.useSlideMode()) { return operations; }

            // transforms the operation positions for a top level paste.
            const transformPositionsToTopLevel = operation => {

                const newOperation = _.copy(operation, true); // deep clone required

                if (operation.start) {
                    // for root level operations just add the start prefix
                    if (operation.start.length < 3) {
                        newOperation.start = rootLevelStart.concat(operation.start);

                        if (operation.end) {
                            newOperation.end = rootLevelStart.concat(operation.end);
                        }

                    } else {
                        // for deeper level operations replace the start prefix
                        newOperation.start.splice(0, 2); // -> requires deep clone of operation, otherwise operation.start is modified (54205)
                        newOperation.start = textFrameStart.concat(newOperation.start);

                        if (operation.end) {
                            newOperation.end.splice(0, 2);
                            newOperation.end = textFrameStart.concat(newOperation.end);
                        }
                    }
                }

                return newOperation;
            };

            // transforms the operation positions for a group paste.
            const transformPositionsToGroup = operation => {
                const newOperation = _.clone(operation);

                if (operation.start) {
                    // replace the start prefix
                    newOperation.start.splice(0, 2);
                    newOperation.start = groupStart.concat(newOperation.start);

                    if (operation.end) {
                        newOperation.end.splice(0, 2);
                        newOperation.end = groupStart.concat(newOperation.end);
                    }
                }

                return newOperation;
            };

            // returns true if the operation is nested inside a group drawing
            const isInsideGroup = operation => {
                if (!operation || !operation.start || groupPositions.length < 1) { return false; }
                // check if operation.start begins with one of the collected group positions
                return _.some(groupPositions, groupPosition => {
                    return this.#arrayStartsWith(operation.start, groupPosition);
                });
            };

            // returns true if the operation is a direct child of the group
            const isDirectGroupChild = operation => {
                if (!operation || !operation.start || groupPositions.length < 1) { return false; }

                // check if operation.start begins with one of the collected group positions
                return _.some(groupPositions, groupPosition => {
                    if (!this.#arrayStartsWith(operation.start, groupPosition)) { return false; }
                    return (operation.start.length === (groupPosition.length + 1));
                });
            };

            // returns true if the operation contains a list definition
            const hasListDefinition = operation => {
                return (operation && _.isString(operation.listStyleId) && !_.isEmpty(operation.listStyleId) && _.isObject(operation.listDefinition));
            };

            // returns true if the operation contains list paragraph attributes
            const hasListAttributes = operation => {
                return (operation && operation.attrs && operation.attrs.paragraph && operation.attrs.paragraph.listStyleId && operation.attrs.styleId === this.getDefaultUIParagraphListStylesheet());
            };

            // transform paragraph list attributes to slide mode
            const transformListAttributes = operation => {
                // the paragraph attributes of the operation
                const paragraph = operation && operation.attrs && operation.attrs.paragraph;
                // the list definition for the given list style id
                let listDefinition = null;
                // the definition for the current list level
                let levelDefinition = null;
                // the resulting paragraph attributes
                const result = {};

                if (_.isObject(paragraph)) {
                    // transform listLevel to level
                    if (_.isNumber(paragraph.listLevel)) {
                        result.level = paragraph.listLevel;
                    }
                    // provide only list level when pasting into place holder drawings
                    if (!isPlaceHolderDrawingSelected) {
                        // look for list definition
                        listDefinition = listDefinitions[paragraph.listStyleId];
                        // look for level definition
                        if (_.isObject(listDefinition) && _.isNumber(paragraph.listLevel)) {
                            levelDefinition = listDefinition['listLevel' + paragraph.listLevel];
                        }
                        // transform style informations, get indentation and definition of the numbering or bullet style
                        _.extend(result, this.#getSlideModeListParagraphAttributesFromLevelDefinition(levelDefinition));
                    }

                    // delete OX Text style id
                    delete operation.attrs.styleId;
                    // replace paragraph attributes
                    operation.attrs.paragraph = result;
                }

                return operation;
            };

            // transform or remove attributes that are not allowed in slide mode
            const sanitizeAttributes = operation => {
                if (!_.isObject(operation.attrs)) { return operation; }

                // the paragraph attributes of the operation
                const paragraph = operation.attrs.paragraph;

                // #51186 PP does not understand the tabstops as they are suppoted in Word. The 'dot' cannot be specified in PP and the tabStop position is set to its default value.
                if (_.isObject(paragraph) && _.isArray(paragraph.tabStops)) {
                    _.each(paragraph.tabStops, tabStop => {
                        if (tabStop && tabStop.pos) {
                            delete tabStop.pos;
                        }
                        if (tabStop && tabStop.fillChar) {
                            delete tabStop.fillChar;
                        }
                    });
                }

                return operation;
            };

            // returns true if the operation inserts a drawing of type shape
            const isInsertDrawingShape = operation => {
                return (operation && operation.name === Op.INSERT_DRAWING && operation.type === 'shape');
            };

            // returns true if the operation inserts a paragraph
            const isInsertParagraph = operation => {
                return (operation && operation.name === Op.PARA_INSERT);
            };

            // special handling for images pasted from OX Text. This drawing are always included into a paragraph
            if (this.#isOnlyInsertDrawingOperationInSetAttributesContainer(operations)) { operations.splice(0, 1); } // DOCS-2358

            if (this.#isOnlyInsertImageOperations(operations)) {
                //
                // special handling when pasting only images
                //
                // modifying positions for the clipboard operations
                for (; index < operations.length; index++) {
                    // the original operation
                    operation = operations[index];

                    newOperation = _.clone(operation);
                    newOperation.start = drawingStart;
                    resultOperations.push(newOperation);
                    // update start index for new drawings
                    drawingStart = increaseLastIndex(drawingStart, 1);
                }

            } else if (selection.isTopLevelTextCursor()) {
                //
                // no text frame selected, paste target is the slide
                //
                // take the first two operations and make sure they insert a text frame with a paragraph
                if (isInsertDrawingShape(operations[0]) &&  isInsertParagraph(operations[1])) {
                    // take text frame operation already present
                    resultOperations.push(_.clone(operations[0]));
                    // take paragraph operation already present
                    resultOperations.push(transformPositionsToTopLevel(operations[1]));
                    // skip these operations later in the loop
                    index += 2;

                } else {
                    // add a drawing of type shape
                    newOperation = { attrs, start: _.copy(rootLevelStart), type: 'shape' };
                    this.extendPropertiesWithTarget(newOperation, target);
                    newOperation = generator.generateOperation(Op.INSERT_DRAWING, newOperation);
                    resultOperations.push(_.clone(newOperation));

                    // add a paragraph into the shape
                    newOperation = { start: appendNewIndex(rootLevelStart) };
                    if (this.#docApp.isODF()) { newOperation.attrs = { paragraph: { bullet: { type: 'none' } } }; } // 52740
                    this.extendPropertiesWithTarget(newOperation, target);
                    newOperation = generator.generateOperation(Op.PARA_INSERT, newOperation);
                    resultOperations.push(_.clone(newOperation));
                }

                // modifying positions for the clipboard operations
                for (; index < operations.length; index++) {
                    // the original operation
                    operation = operations[index];

                    // check if operation is nested inside a group
                    if (isInsideGroup(operation)) {
                        // check insert image operations for their positions:
                        // images are allowed as direct child of a group, but are forbidden at deeper positions (like an image iside a text frame inside a group).
                        if ((operation.name === Op.INSERT_DRAWING) && (operation.type === 'image') && !isDirectGroupChild(operation))  {
                            // replace insert image with insert text operation to keep positions consistent,
                            newOperation = transformPositionsToGroup({ start: _.copy(operation.start), text: ' ' });
                            this.extendPropertiesWithTarget(newOperation, target);
                            newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                            resultOperations.push(_.clone(newOperation));

                            // update start index for new drawings
                            drawingStart = increaseLastIndex(drawingStart, 1);
                            // insert the image at the next valid root level position
                            newOperation = _.clone(operation);
                            newOperation.start = drawingStart;
                            resultOperations.push(newOperation);

                        } else if (operation.name === Op.INSERT_LIST) {
                            // store list definition to transform list paragraphs
                            if (hasListDefinition(operation)) {
                                listDefinitions[operation.listStyleId] = operation.listDefinition;
                            }

                        } else if ((operation.name === Op.COMPLEXFIELD_INSERT) || (operation.name === Op.COMMENT_INSERT) || (operation.name === Op.RANGE_INSERT) || (operation.name === Op.BOOKMARK_INSERT)) {
                            // replace complex field, comment and range operations with insert text operation to keep positions consistent
                            newOperation = transformPositionsToGroup({ start: _.copy(operation.start), text: '\u2060' });
                            this.extendPropertiesWithTarget(newOperation, target);
                            newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                            resultOperations.push(_.clone(newOperation));

                            // create a delete operation to finally remove replacement spaces again
                            newOperation = transformPositionsToGroup({ start: _.copy(operation.start) });
                            this.extendPropertiesWithTarget(newOperation, target);
                            this.#deleteReplacementsGenerator.generateOperation(Op.DELETE, newOperation);

                        } else {
                            newOperation = transformPositionsToGroup(operation);
                            if (hasListAttributes(operation)) {
                                newOperation = transformListAttributes(newOperation);
                            }
                            newOperation = sanitizeAttributes(newOperation);

                            resultOperations.push(newOperation);
                        }

                    } else {
                        // if we find an operation that inserts a drawing we need to update the base positions for drawings and for text frames.
                        if (operation.name === Op.INSERT_DRAWING) {
                            // replace insert drawing with insert text operation to keep positions consistent,
                            // but only if the drawing's origin is OX Text
                            if (this.isClipboardOriginText()) {
                                newOperation = transformPositionsToTopLevel({ start: _.copy(operation.start), text: ' ' });
                                this.extendPropertiesWithTarget(newOperation, target);
                                newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                                resultOperations.push(_.clone(newOperation));
                            }

                            // update start index for new drawings
                            drawingStart = increaseLastIndex(drawingStart, 1);
                            // update start index for new text frames
                            if (operation.type === 'shape') {
                                textFrameStart = _.clone(drawingStart);
                            }

                            // store group position; used to check if operation is nested inside a group drawing
                            if (operation.type === 'group') {
                                groupPositions.push(operation.start);
                                groupStart =  _.clone(drawingStart);
                            }

                            newOperation = _.clone(operation);
                            newOperation.start = drawingStart;
                            resultOperations.push(newOperation);

                        } else if (operation.name === Op.INSERT_LIST) {
                            // store list definition to transform list paragraphs
                            if (hasListDefinition(operation)) {
                                listDefinitions[operation.listStyleId] = operation.listDefinition;
                            }

                        } else if ((operation.name === Op.COMPLEXFIELD_INSERT) || (operation.name === Op.COMMENT_INSERT) || (operation.name === Op.RANGE_INSERT) || (operation.name === Op.BOOKMARK_INSERT)) {
                            // replace complex field, comment and range operations with insert text operation to keep positions consistent
                            newOperation = transformPositionsToTopLevel({ start: _.copy(operation.start), text: '\u2060' });
                            this.extendPropertiesWithTarget(newOperation, target);
                            newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                            resultOperations.push(_.clone(newOperation));

                            // create a delete operation to finally remove replacement spaces again
                            newOperation = { start: _.clone(newOperation.start) }; // reusing the already generated position (54205)
                            this.extendPropertiesWithTarget(newOperation, target);
                            this.#deleteReplacementsGenerator.generateOperation(Op.DELETE, newOperation);

                        } else {
                            newOperation = transformPositionsToTopLevel(operation);
                            if (hasListAttributes(operation)) {
                                newOperation = transformListAttributes(newOperation);
                            }
                            newOperation = sanitizeAttributes(newOperation);

                            resultOperations.push(newOperation);
                        }
                    }
                }

            } else if (selection.isAnyTextFrameSelection()) {
                //
                // a text frame is selected, paste target for text is that frame
                //
                // modifying positions for the clipboard operations
                for (; index < operations.length; index++) {
                    // the original operation
                    operation = operations[index];

                    if (operation.name === Op.INSERT_LIST) {
                        // store list definition to transform list paragraphs
                        if (hasListDefinition(operation)) {
                            listDefinitions[operation.listStyleId] = operation.listDefinition;
                        }

                    } else if ((operation.name === Op.COMPLEXFIELD_INSERT) || (operation.name === Op.COMMENT_INSERT) || (operation.name === Op.RANGE_INSERT) || (operation.name === Op.BOOKMARK_INSERT)) {
                        // replace complex field, comment and range operations with insert text operation to keep positions consistent
                        newOperation = { start: _.copy(operation.start), text: '\u2060' };
                        this.extendPropertiesWithTarget(newOperation, target);
                        newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                        resultOperations.push(_.clone(newOperation));

                        // create a delete operation to finally remove replacement spaces again
                        newOperation = { start: _.copy(operation.start) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        this.#deleteReplacementsGenerator.generateOperation(Op.DELETE, newOperation);

                    } else {
                        newOperation = _.clone(operation);
                        if (hasListAttributes(operation)) {
                            newOperation = transformListAttributes(newOperation);
                        }
                        newOperation = sanitizeAttributes(newOperation);

                        resultOperations.push(newOperation);
                    }
                }
            }

            return resultOperations;
        }

        /**
         * Removes all operations from the clipboard data that are used to generate comment content.
         * The insertComment and insertRange operations need to be left in place for later handling.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #removeCommentContentOperations(operations) {
            // comment target
            let commentTargets = [];
            // the resulting operations
            const resultOperations = [];

            // returns true if the operation target is a comment id
            const isCommentOperation = operation => {
                if (!operation) { return false; }

                return _.some(commentTargets, target => {
                    return operation.target === target;
                });
            };

            _.each(operations, operation => {
                if (operation.name === Op.COMMENT_INSERT || (operation.name === Op.RANGE_INSERT && operation.type === 'comment')) {
                    if (_.isString(operation.id) && !_.isEmpty(operation.id) && _.indexOf(resultOperations, operation.id) === -1) {
                        commentTargets.push(operation.id);
                        commentTargets = _.uniq(commentTargets);
                    }
                    resultOperations.push(operation);

                } else if (!isCommentOperation(operation)) {
                    resultOperations.push(operation);
                }

            });

            return resultOperations;
        }

        /**
         * Replaces a table including sub tables with a paragraph.
         * Therefore removes all table concerning operations, replacing them
         * with insert paragraph and insert text operations.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #makeTextFromTableOperations(operations) {
            // the operations generator
            const generator = this.createOperationGenerator();
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // the table root positions
            const tableRoots = [];
            // the position for the resulting text
            let textPosition = [0, 0];
            // the newly created operation
            let newOperation = null;
            // the resulting operations
            const resultOperations = [];

            // returns true if the position is inside a table.
            const isTablePosition = position => {
                // check position if it starts with one of the collected table root positions
                if ((!_.isArray(position)) || (tableRoots.length < 1)) { return false; }
                return _.some(tableRoots, rootPosition => {
                    return this.#arrayStartsWith(position, rootPosition);
                });
            };

            _.each(operations, operation => {
                // check for insert table, operations that are inside the table and all the rest
                if (operation.name === Op.TABLE_INSERT) {
                    // check for root or sub table
                    if ((tableRoots.length === 0) || !this.#arrayStartsWith(operation.start, textPosition.slice(0, -1))) {
                        // create insert paragraph operation to replace insert table operation
                        newOperation = { start: _.clone(operation.start) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        newOperation = generator.generateOperation(Op.PARA_INSERT, newOperation);
                        resultOperations.push(_.clone(newOperation));
                        // set position for insert text operations
                        textPosition = appendNewIndex(operation.start);
                    }
                    // remember table position
                    tableRoots.push(operation.start);

                } else if (isTablePosition(operation.start)) {
                    // shift position of insert text operation from the table into the target paragraph
                    if (operation.name === Op.TEXT_INSERT) {
                        newOperation = _.clone(operation);
                        newOperation.start = _.clone(textPosition);
                        newOperation.text += ' ';
                        resultOperations.push(newOperation);
                        // update text position
                        textPosition = increaseLastIndex(textPosition, newOperation.text.length);
                    }

                } else {
                    // non table operations
                    resultOperations.push(operation);
                }

            });

            return resultOperations;
        }

        /**
         * Transforms OX Presentation operations to be used with OX Text.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #transformOperationsToText(operations) {

            // returns the width of the selected paragraph
            const getSelectedParagraphWidth = () => {
                // the selected paragraph node
                const paraNode = getParagraphElement(this.getNode(), _.initial(selection.getStartPosition()));
                let paraWidth = null;

                if (paraNode) {
                    paraWidth = convertLengthToHmm($(paraNode).width(), 'px');
                }

                return paraWidth;
            };

            // the generator to create replacement operations for the given ones
            const generator = this.createOperationGenerator();
            // the generator for stylesheet operations that need to be executed before the transformed ones
            const styleCache = new StyleCache(this);
            // the generator for operations that need to be executed after the transformed ones
            const appendGenerator = this.createOperationGenerator();
            // the default border attributes
            const lineAttrs = { line: { color: Color.BLACK, style: 'single', type: 'solid', width: 26 } };
            // the default fill color attributes
            const fillAttrs = { fill: { color: Color.WHITE, type: 'solid' } };
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // the selection object
            const selection = this.getSelection();
            // the list collection object
            const listCollection = this.getListCollection();
            // OX Text default paragraph list style sheet
            const listParaStyleId = this.getDefaultUIParagraphListStylesheet();
            // indicates if the insert paragraph list style sheet operation needs to added
            let addParagraphListStyle = false;
            // the list definitions taken from insertParagraph operations
            const listDefinitions = {};
            // the default width for drawings
            const defaultWidth = 7500;
            // the default height for drawings
            const defaultHeight = 5000;
            // the width of the parent paragraph
            const maxWidth = getSelectedParagraphWidth() || defaultWidth;
            // the table stylesheet id
            let tableStyleId = null;
            // the positions of table drawings
            const tablePositions = [];
            // the new table base position
            let tableStart = [0];
            // the newly created operation
            let newOperation = null;
            // the resulting text mode operations
            const resultOperations = [];

            // returns true if the operation contains list paragraph attributes
            const hasListAttributes = operation => {
                return (operation.attrs && operation.attrs.paragraph && operation.attrs.paragraph.bullet);
            };

            // returns a key string for the given position
            const getKeyForPosition = position => {
                let key = null;

                if (_.isArray(position)) {
                    key = position.join(',');
                    if (_.isEmpty(key)) {
                        key = 'root';
                    }
                }

                return key;
            };

            // returns a list entry for the given position
            const getListEntryForPosition = position => {
                const key = getKeyForPosition(position);
                return listDefinitions[key] || null;
            };

            // getting a list definition for a drawing position
            const getListDefinitionForPosition = position => {
                const entry = getListEntryForPosition(position);
                return (entry && entry.listDefinition) || null;
            };

            // getting a list style id for a drawing position
            const getListStyleIdForPosition = position => {
                const entry = getListEntryForPosition(position);
                return (entry && entry.listStyleId) || null;
            };

            // setting a new list definition for the position
            const setListDefinitionForPosition = (position, definition) => {
                const key = getKeyForPosition(position);

                if (key && !listDefinitions[key]) {
                    listDefinitions[key] = {
                        listDefinition: definition,
                        listStyleId: this.#getFreeListStyleId()
                    };
                }
            };

            // replaces a list level definition of the given list definition with data defined in the given attrs
            const addLevelToListDefinition = (definition, attrs) => {
                if (!definition || !attrs || !attrs.paragraph) { return; }

                // the list level
                const level = attrs.paragraph.level || 0;
                // the list level definition
                const leveDefinition = definition['listLevel' + level];
                // the type of the numbered list
                const numType = attrs.paragraph.bullet.numType || '';
                // the number format part of the numbered list
                let numFormatPart = null;
                // the level text part of the numbered list
                let levelTextPart = null;
                // the result of parsing the numbered list type
                let result = null;

                if (!leveDefinition) { return; }

                // indentFirstLine and indentLeft - always use the OX Text defaults

                // fontName
                if (attrs.paragraph.bulletFont && _.isString(attrs.paragraph.bulletFont.name)) {
                    leveDefinition.fontName = attrs.paragraph.bulletFont.name;
                }
                // numberFormat and levelText
                if (attrs.paragraph.bullet.type === 'numbering') {
                    // parse numType
                    result = numType.match(/(arabic|alphaLc|alphaUc|romanLc|romanUc)(Period|ParenR|ParenBoth)/);
                    numFormatPart = (result && result[1]) || null;
                    levelTextPart = (result && result[2]) || null;

                    // set number format
                    switch (numFormatPart) {
                        case 'alphaLc':
                            leveDefinition.numberFormat = 'lowerLetter';
                            break;
                        case 'alphaUc':
                            leveDefinition.numberFormat = 'upperLetter';
                            break;
                        case 'romanLc':
                            leveDefinition.numberFormat = 'lowerRoman';
                            break;
                        case 'romanUc':
                            leveDefinition.numberFormat = 'upperRoman';
                            break;
                        case 'arabic':
                        default:
                            leveDefinition.numberFormat = 'decimal';
                            break;
                    }

                    // set level text
                    switch (levelTextPart) {
                        case 'ParenR':
                            leveDefinition.levelText = '%' + (level + 1) + ')';
                            break;
                        case 'ParenBoth':
                            leveDefinition.levelText = '(%' + (level + 1) + ')';
                            break;
                        case 'Period':
                        default:
                            leveDefinition.levelText = '%' + (level + 1) + '.';
                            break;
                    }

                } else {
                    leveDefinition.numberFormat = 'bullet';
                    if (attrs.paragraph.bullet.type === 'character') {
                        leveDefinition.levelText = attrs.paragraph.bullet.character || '\u2022';
                    } else {
                        leveDefinition.levelText = ' '; // no symbol
                    }
                }
            };

            // returns true if the position is inside a table drawing
            const isTablePosition = position => {
                if (!_.isArray(position) || tablePositions.length < 1) { return false; }
                // check if given position begins with one of the collected table positions
                return _.some(tablePositions, tablePosition => {
                    return this.#arrayStartsWith(position, tablePosition);
                });
            };

            // transforms the operation position if necessary
            const transformPosition = position => {
                let result = _.clone(position);

                // transform table position from drawing level to root position
                if (isTablePosition(position)) {
                    result.splice(0, 2);
                    result = tableStart.concat(result);
                }

                return result;
            };

            // copies the operation and transforms the position if necessary
            const copyOperation = operation => {
                const resultOp = _.copy(operation, true);

                if (_.isArray(operation.start)) {
                    resultOp.start = transformPosition(operation.start);
                }
                if (_.isArray(operation.end)) {
                    resultOp.end = transformPosition(operation.end);
                }

                return resultOp;
            };

            // reset id generation for this.#getFreeListStyleId()
            this.#resetListStyleId();

            _.each(operations, operation => {
                // the list definition of a list paragraph
                let listDefinition = null;
                // the position where the current list definition is applied to
                let rootPosition = null;
                // the operation attributes
                const attrs = operation.attrs;
                // whether a table style was found in the local collection of table styles
                let tableStyleSheetFound = false;
                // set target
                if (target) {
                    operation.target = target;
                }

                if ((operation.name === Op.PARA_INSERT || operation.name === Op.SET_ATTRIBUTES) && (hasListAttributes(operation))) {

                    // create a deep copy before changing the operation
                    newOperation = copyOperation(operation);

                    // add insert paragraph list style sheet operation
                    addParagraphListStyle = true;

                    // handle list paragraph
                    rootPosition = operation.start.slice(0, -1);
                    listDefinition = getListDefinitionForPosition(rootPosition);

                    if (!listDefinition) {
                        // create a new list definition (bullet or numbering)
                        listDefinition = listCollection.getDefaultListDefinition(attrs.paragraph.bullet.type);

                        // register new list definition
                        setListDefinitionForPosition(rootPosition, listDefinition);
                    }

                    // replace default with current level definition
                    addLevelToListDefinition(listDefinition, attrs);

                    // set OX Text list attrs
                    newOperation.attrs.styleId = listParaStyleId;
                    newOperation.attrs.paragraph.listLevel = attrs.paragraph.level || 0;
                    newOperation.attrs.paragraph.listStyleId = getListStyleIdForPosition(rootPosition);

                    // remove OX Presentation list attrs
                    delete newOperation.attrs.paragraph.level;
                    delete newOperation.attrs.paragraph.indentLeft;
                    delete newOperation.attrs.paragraph.indentFirstLine;
                    delete newOperation.attrs.paragraph.bullet;
                    delete newOperation.attrs.paragraph.bulletFont;

                    resultOperations.push(newOperation);

                } else if (operation.name === Op.FIELD_INSERT) {
                    // replace insert field operations with insert text operation to keep positions consistent
                    newOperation = { start: transformPosition(operation.start) };
                    this.extendPropertiesWithTarget(newOperation, target);

                    if (_.isString(operation.representation) && !_.isEmpty(operation.representation)) {
                        // create a replacement for the insert field operation using the first character of the representation
                        newOperation.text = _.first(operation.representation);
                        newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                        resultOperations.push(_.clone(newOperation));

                        // create insert text operation for the remaining part of the representation
                        if (operation.representation.length > 1) {
                            newOperation = { start: increaseLastIndex(newOperation.start, 1), text: operation.representation.slice(1) };
                            this.extendPropertiesWithTarget(newOperation, target);
                            appendGenerator.generateOperation(Op.TEXT_INSERT, newOperation);
                        }

                    } else {
                        // create a replacement for the insert field operation using a replacement space char
                        newOperation.text = '\u2060';
                        newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                        resultOperations.push(_.clone(newOperation));

                        // create a delete operation to finally remove replacement spaces again
                        newOperation = { start: transformPosition(operation.start) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        this.#deleteReplacementsGenerator.generateOperation(Op.DELETE, newOperation);
                    }

                } else if (operation.name === 'insertSlide' || operation.name === 'insertLayoutSlide') {
                    // replace insertSlide or insertLayoutSlide operations with insertParagraph operations.
                    newOperation = { start: _.clone(operation.start), attrs: { paragraph: { pageBreakBefore: true } } };
                    this.extendPropertiesWithTarget(newOperation, target);
                    newOperation = generator.generateOperation(Op.PARA_INSERT, newOperation);
                    resultOperations.push(_.clone(newOperation));

                } else if (operation.name === Op.INSERT_DRAWING && operation.type === 'table') {
                    // store position of drawing table
                    tablePositions.push(operation.start);

                    // update table base position
                    tableStart = increaseLastIndex(tableStart, 1);

                    // replace insert drawing table operations with insert text operation to keep positions consistent
                    newOperation = { start: _.copy(operation.start), text: '\u2060' };
                    this.extendPropertiesWithTarget(newOperation, target);
                    newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                    resultOperations.push(_.copy(newOperation, true));

                    // create a delete operation to finally remove replacement spaces again
                    newOperation = { start: _.copy(operation.start) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    this.#deleteReplacementsGenerator.generateOperation(Op.DELETE, newOperation);

                    // create insert table operation
                    tableStyleSheetFound = this.tableStyles.containsStyleSheet(operation.attrs.styleId);
                    tableStyleId = tableStyleSheetFound ? operation.attrs.styleId : this.getDefaultUITableStylesheet();
                    newOperation = {
                        start: transformPosition(operation.start),
                        attrs: {
                            styleId: tableStyleId,
                            table: _.copy(operation.attrs.table, true)
                        }
                    };

                    if (!tableStyleSheetFound) { // not using border properties from whole table, if the fallback style is used (DOCS-3037)
                        _.each(TableStyles.getBorderProps(), borderProp => { delete newOperation.attrs.table[borderProp]; });
                    }

                    if (operation.attrs.drawing && _.isNumber(operation.attrs.drawing.width)) {
                        newOperation.attrs.table.width = Math.min(operation.attrs.drawing.width, maxWidth);
                    }
                    this.extendPropertiesWithTarget(newOperation, target);
                    newOperation = generator.generateOperation(Op.TABLE_INSERT, newOperation);
                    resultOperations.push(newOperation);

                    // generate an insertStyleSheet operation
                    this.tableStyles.generateMissingStyleSheetOperations(styleCache, tableStyleId);

                } else if (operation.name === Op.INSERT_DRAWING) {
                    // create a deep copy before changing the operation
                    newOperation = copyOperation(operation);
                    newOperation.attrs.drawing = newOperation.attrs.drawing || {};

                    // remove 'connector' family from attributes
                    if (newOperation.attrs.connector) {
                        delete newOperation.attrs.connector;
                    }

                    // remove 'table' family from attributes
                    if (newOperation.attrs.table) {
                        delete newOperation.attrs.table;
                    }

                    // ODF doesn't support preset and custom shapes yet - bug #47593
                    if (this.#docApp.isODF() && this.#isPresetOrCustomShapeOperation(operation)) {
                        // adding styleId
                        newOperation.attrs.styleId = 'Frame';
                        // remove geometry attribute set
                        delete newOperation.attrs.geometry;
                        // set fill and line attributes to defaults
                        _.extend(newOperation.attrs, fillAttrs, lineAttrs);

                    } else {
                        // auto height for text frames
                        if (newOperation.type === 'shape' && (!newOperation.attrs.geometry || !newOperation.attrs.geometry.presetShape || newOperation.attrs.geometry.presetShape === 'rect')) {
                            newOperation.attrs.shape = newOperation.attrs.shape || {};
                            _.extend(newOperation.attrs.shape, { autoResizeHeight: true, noAutoResize: false });
                        }
                    }

                    // fix drawing width and height
                    if (!_.isNumber(newOperation.attrs.drawing.width)) {
                        _.extend(newOperation.attrs.drawing, { width: defaultWidth, height: defaultHeight });

                    } else if (newOperation.attrs.drawing.width > maxWidth) {
                        newOperation.attrs.drawing.width = maxWidth;
                    }

                    resultOperations.push(newOperation);

                } else {
                    resultOperations.push(copyOperation(operation));
                }
            });

            // add insert paragraph list style sheet operation
            if (addParagraphListStyle) {
                this.paragraphStyles.generateMissingStyleSheetOperations(styleCache, listParaStyleId);
            }

            // generate the 'insertList' operations for the collected list definitions
            for (const entry in listDefinitions) {
                styleCache.generateOperation(Op.INSERT_LIST, {
                    listStyleId: listDefinitions[entry].listStyleId,
                    listDefinition: listDefinitions[entry].listDefinition
                });
            }

            // concatenate prepend, result and append operations
            appendGenerator.reverseOperations();
            styleCache.appendOperations(resultOperations).appendOperations(appendGenerator);
            return styleCache.getOperations();
        }

        /**
         * Transforms field operations between DOCX and ODF
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #transcodeFieldOperations(operations) {

            // the generator to create replacement operations for the given ones
            const generator = this.createOperationGenerator();
            // the generator for operations that need to be executed after the transformed ones
            const appendGenerator = this.createOperationGenerator();
            // target for operation - if exists, it's for ex. header or footer
            let target = this.getActiveTarget();
            // the newly created operation
            let newOperation = null;
            // the resulting text mode operations
            const resultOperations = [];

            _.each(operations, operation => {
                // use operation-specified target
                if (operation.target) { target = operation.target; }

                if (operation.name === Op.FIELD_INSERT) {
                    // replace insert field operations with insert text operation to keep positions consistent
                    newOperation = { start: _.copy(operation.start) };
                    this.extendPropertiesWithTarget(newOperation, target);

                    if (_.isString(operation.representation) && !_.isEmpty(operation.representation)) {
                        // create a replacement for the insert field operation using the first character of the representation
                        newOperation.text = _.first(operation.representation);
                        newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                        resultOperations.push(_.clone(newOperation));

                        // create insert text operation for the remaining part of the representation
                        if (operation.representation.length > 1) {
                            newOperation = { start: increaseLastIndex(newOperation.start, 1), text: operation.representation.slice(1) };
                            this.extendPropertiesWithTarget(newOperation, target);
                            appendGenerator.generateOperation(Op.TEXT_INSERT, newOperation);
                        }

                    } else {
                        // create a replacement for the insert field operation using a replacement space char
                        newOperation.text = '\u2060';
                        newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                        resultOperations.push(_.clone(newOperation));

                        // -> using no delete operations, because the document type is modified (54362)
                    }

                } else if ((operation.name === Op.COMPLEXFIELD_INSERT) || (operation.name === Op.RANGE_INSERT && operation.type === 'field')) {
                    // replace complex field and range operations with insert text operation to keep positions consistent
                    newOperation = { start: _.copy(operation.start), text: '\u2060' };
                    this.extendPropertiesWithTarget(newOperation, target);
                    newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                    resultOperations.push(_.clone(newOperation));

                    // -> using no delete operations, because the document type is modified (54362)

                } else if (operation.name === Op.SET_ATTRIBUTES && operation.attrs && operation.attrs.character && operation.attrs.character.field) {
                    newOperation = _.copy(operation, true);
                    delete newOperation.attrs.character.field;
                    resultOperations.push(newOperation);

                } else {
                    resultOperations.push(_.copy(operation, true));
                }

            });

            // concatenate prepend, result and append operations
            appendGenerator.reverseOperations();
            return resultOperations.concat(appendGenerator.getOperations());
        }

        /**
         * Transforms list specific operations.
         *  Replaces image bullets with default text bullets.
         *
         * @param {Array} operations
         *  An array with the clipboard operations (changes the operations directly).
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #transcodeListOperations(operations) {
            _.each(operations, operation => {
                const paragraph = operation && operation.attrs && operation.attrs.paragraph;

                // replace image bullets with default text bullet
                if (paragraph && paragraph.bullet && paragraph.bullet.type === 'bitmap') {
                    paragraph.bullet = { type: 'character', character: '\u25CF' };
                    paragraph.bulletFont = { followText: false, name: 'Times New Roman' };
                }
            });

            return operations;
        }

        /**
         * Removes any bookmark clipboard generated operation,
         * first with inserting replacement space charater, and then coresponding delete operation.
         *
         * @param {Array} operations
         *  An array with the clipboard operations.
         *
         * @returns {Array}
         *  An array with the resulting operations.
         */
        #removeBookmarkOperations(operations) {
            // the generator to create replacement operations for the given ones
            const generator = this.createOperationGenerator();
            // target for operation - if exists, it's for ex. header or footer
            let target = this.getActiveTarget();
            // the newly created operation
            let newOperation = null;
            // the resulting text mode operations
            const resultOperations = [];

            _.each(operations, operation => {
                if (operation.name === Op.BOOKMARK_INSERT) {
                    // use operation-specified target
                    if (operation.target) { target = operation.target; }

                    // replace insert bookmark operations with insert text operation to keep positions consistent
                    // #66073# but don't set a target here, this would make transformOperation() skip this operation
                    // without adopting it to the paste anchor position
                    newOperation = { start: _.copy(operation.start) };

                    // create a replacement for the insert bookmark operation using a replacement space char
                    newOperation.text = '\u2060';
                    newOperation = generator.generateOperation(Op.TEXT_INSERT, newOperation);
                    resultOperations.push(_.clone(newOperation));

                    // create a delete operation to finally remove replacement spaces again
                    // #66073# but don't set a target here, this would make transformOperation() skip this operation
                    // without adopting it to the paste anchor position
                    if (!this.getPageLayout().isIdOfMarginalNode(target)) { // no delete op inside header/footer, as this may be inside special fields
                        newOperation = { start: _.copy(operation.start) };
                        this.#deleteReplacementsGenerator.generateOperation(Op.DELETE, newOperation);
                    }
                } else {
                    resultOperations.push(_.copy(operation, true));
                }
            });

            return resultOperations;
        }

        /**
         * Processing drop of images.
         *
         * @param {jQuery.Event} event
         *  The jQuery browser event object.
         *
         * @param {Number[]} dropPosition
         *  The position for the drop operation.
         */
        #processDroppedImages(event, dropPosition) {

            const images = event.originalEvent.dataTransfer.files;

            // text part of spreadsheet allows only plain text
            if (this.#docApp.isSpreadsheetApp()) { return; }

            // handle image date received from readClientFile()
            const handleImageData = imgData => {
                this.insertImageURL(imgData, { position: (this.useSlideMode() ? null : dropPosition.start) });
            };

            //checks if files were dropped from the browser or the file system
            if (!images || images.length === 0) {
                handleImageData(event.originalEvent.dataTransfer.getData('text'));
                return;
            }

            for (let i = 0; i < images.length; i++) {
                const img = images[i];

                // skip non images and images with unsupported MIME types
                if (!isSupportedImageMimeType(img.type)) {
                    // show error for unsupported image MIME types
                    if (isImageMimeType(img.type)) {
                        this.#docApp.rejectEditAttempt('image');
                    }
                    continue;
                }

                this.#docApp.readClientFile(img).done(handleImageData);
            }
        }

        /**
         * Returns true if the operation attributes contain a color supporting attribute family.
         *
         * @returns {Boolean}
         *  Whether the given operation attributes have a color supporting family.
         */
        #hasAttributeFamilyWithColor(operation) {
            if (!_.isObject(operation.attrs)) {
                return false;
            }
            return COLOR_SUPPORT_ATTRIBUTE_FAMILIES.some(family => {
                return (family in operation.attrs);
            });
        }

        /**
         * Returns true if the given attribute family doesn't support color attributes.
         *
         * @returns {Boolean}
         *  Whether the given attribute family supports color attributes.
         */
        #isNonColorAttributeFamily(family) {
            return NON_COLOR_SUPPORT_ATTRIBUTE_FAMILIES.some(item => {
                return (item === family);
            });
        }

        /**
         * Adds a fallback color to all operation attributes with theme colors.
         * Doesn't create a copy, modifies the given operations.
         *
         * @param {Array} operations
         *  The array with the clipboard operations to handle.
         *
         * @returns {Array}
         *  The array with the resulting clipboard operations.
         */
        #prepareThemeColorsForCopy(operations) {

            // add fallback color to the theme color
            const handleColor = subtree => {

                _.each(subtree, (color, key) => {
                    if (key === 'color' || key === 'fillColor') {
                        if (isColorThemed(color)) {
                            const auto = (key === 'fillColor') ? 'fill' : 'line';
                            color.fallbackValue = this.parseAndResolveColor(color, auto).hex;
                        }
                    } else if (_.isObject(color) && !this.#isNonColorAttributeFamily(key)) {
                        handleColor(color);
                    }
                });
            };

            _.each(operations, operation => {
                if (this.#hasAttributeFamilyWithColor(operation)) {
                    handleColor(operation.attrs);
                }
            });

            return operations;
        }

        /**
         * Generates operations needed to copy the current text selection to
         * the internal clipboard.
         *
         * @returns {Array}
         *  The operations array that represents the current selection.
         */
        #copyTextSelection() {

            // the generator for stylesheet operations that need to be executed first
            const styleCache = new StyleCache(this);
            // the operations generator
            const generator = this.createOperationGenerator();
            // zero-based index of the current content node
            let targetPosition = 0;
            // result of the iteration process
            let result = null;
            // OX Text default paragraph list style sheet
            const listParaStyleId = this.getDefaultUIParagraphListStylesheet();
            // indicates if we had to add a paragraph style
            let listParaStyleInserted = false;
            // the applied list style ids
            const listStyleIds = [];
            // the attribute property for the change track
            const changesFamily = 'changes';
            // a logical helper start position, if a text frame is selected
            let startPosition = null;
            // attributes of the contentNode
            let attributes;
            // a new created operation
            let newOperation = null;
            // the selection object
            const selection = this.getSelection();

            // in the case of a text cursor selection inside a selected text frame, the text frame itself can be used for copying
            if (!this.#docApp.isSpreadsheetApp() && selection.getSelectionType() === 'text' && !this.hasSelectedRange() && selection.isAdditionalTextframeSelection()) {
                startPosition = getOxoPosition(this.getNode(), selection.getSelectedTextFrameDrawing(), 0);
                selection.setTextSelection(startPosition, increaseLastIndex(startPosition));
            }

            // ignoring empty selections
            if (selection.isTextCursor()) { return []; }

            // visit the paragraphs and tables covered by the text selection
            result = selection.iterateContentNodes((contentNode, position, startOffset, endOffset) => {

                // whether this is a selection inside one paragraph -> no handling of paragraph attributes (42759)
                const isSingleParagraphSelection = _.isNumber(startOffset) && _.isNumber(endOffset);
                // the list collection object
                const listCollection = this.getListCollection();

                // paragraphs may be covered partly
                if (isParagraphNode(contentNode)) {

                    // if we have a list add the list paragraph style and the list style
                    if (!isSingleParagraphSelection && this.paragraphStyles.containsStyleSheet(listParaStyleId)) {

                        if (!listParaStyleInserted) {
                            this.paragraphStyles.generateMissingStyleSheetOperations(styleCache, listParaStyleId);
                            listParaStyleInserted = true;
                        }

                        attributes = this.paragraphStyles.getElementAttributes(contentNode);
                        if (listCollection && attributes.paragraph && attributes.paragraph.listStyleId && !_.contains(listStyleIds, attributes.paragraph.listStyleId)) {
                            newOperation = listCollection.getListOperationFromListStyleId(attributes.paragraph.listStyleId);
                            if (newOperation) {  // check for valid operation (38007)
                                styleCache.generateOperation(Op.INSERT_LIST, newOperation);
                                listStyleIds.push(attributes.paragraph.listStyleId);
                            }
                        }

                    }

                    // first or last paragraph: generate operations for covered text components
                    if (_.isNumber(startOffset) || _.isNumber(endOffset)) {

                        // some selections might cause invalid logical positions (38095)
                        if (_.isNumber(startOffset) && _.isNumber(endOffset) && (endOffset < startOffset)) { return BREAK; }

                        // special handling for Firefox and IE when selecting list paragraphs.
                        // if the selection includes the end position of a non list paragraph
                        // above the list, we don't add an empty paragraph to the document
                        // fix for bug 29752
                        if ((_.browser.Firefox || _.browser.IE) &&
                            getParagraphLength(this.getNode(), position) === startOffset &&
                            !isListLabelNode(contentNode.firstElementChild) &&
                            findNextNode(this.getNode(), contentNode, PARAGRAPH_NODE_SELECTOR) &&
                            isListLabelNode(findNextNode(this.getNode(), contentNode, PARAGRAPH_NODE_SELECTOR).firstElementChild)) {
                            return;
                        }

                        // generate a splitParagraph and setAttributes operation for
                        // contents of first paragraph (but for multiple-paragraph
                        // selections only)
                        if (!_.isNumber(endOffset)) {
                            generator.generateOperation(Op.PARA_SPLIT, { start: [targetPosition, 0] });
                        }

                        // handling for all paragraphs, also the final (36825). But ignoring paragraph attributes in single paragraph selections (42759)
                        if (!isSingleParagraphSelection) {
                            generator.generateSetAttributesOperation(contentNode, { start: [targetPosition] }, { clearFamily: 'paragraph', ignoreFamily: changesFamily + ' character', allAttributes: true });
                        }

                        // operations for the text contents covered by the selection
                        generator.generateParagraphChildOperations(contentNode, [targetPosition], { start: startOffset, end: endOffset, targetOffset: 0, clear: true, ignoreFamily: changesFamily, allAttributes: true });

                    } else {

                        // skip embedded implicit paragraphs
                        if (isImplicitParagraphNode(contentNode)) { return; }

                        // generate operations for the paragraph only, the paragraph childs are generated without ignoring the character family later
                        generator.generateParagraphOperations(contentNode, [targetPosition], { ignoreFamily: changesFamily + ' character', allAttributes: true, doNotCreateParagraphChildOperations: true });

                        // process all content nodes in the paragraph and create operations
                        generator.generateParagraphChildOperations(contentNode, [targetPosition], { ignoreFamily: changesFamily, allAttributes: true });
                    }

                // entire table: generate complete operations array for the table (if it is not an exceeded-size table)
                } else if (isTableNode(contentNode)) {

                    // skip embedded oversized tables
                    if (isExceededSizeTableNode(contentNode)) { return; }

                    // generate operations for entire table
                    generator.generateTableOperations(contentNode, [targetPosition], { ignoreFamily: changesFamily });

                } else {
                    globalLogger.error('Editor.#copyTextSelection(): unknown content node "' + getNodeName(contentNode) + '" at position ' + JSON.stringify(position));
                    return BREAK;
                }

                targetPosition += 1;

            }, this, { shortestPath: true });

            // return operations, if iteration has not stopped on error
            return (result === BREAK) ? [] : styleCache.getOperations().concat(generator.getOperations());
        }

        /**
         * Generates operations needed to copy the current cell range selection
         * to the internal clipboard.
         *
         * @returns {Array}
         *  The operations array that represents the current selection.
         */
        #copyCellRangeSelection() {

            // the operations generator
            const generator = this.createOperationGenerator();
            // the selection object
            const selection = this.getSelection();
            // information about the cell range
            const cellRangeInfo = selection.getSelectedCellRange();
            // merged attributes of the old table
            let oldTableAttributes = null;
            // explicit attributes for the new table
            let newTableAttributes = null;
            // all rows in the table
            let tableRowNodes = null;
            // relative row offset of last visited cell
            let lastRow = -1;
            // the attribute property for the change track
            const changesFamily = 'changes';
            // result of the iteration process
            let result = null;

            // generates operations for missing rows and cells, according to lastRow/lastCol
            const generateMissingRowsAndCells = (row/*, col*/) => {

                // generate new rows (repeatedly, a row may be covered completely by merged cells)
                while (lastRow < row) {
                    lastRow += 1;
                    generator.generateOperationWithAttributes(tableRowNodes[lastRow], Op.ROWS_INSERT, { start: [1, lastRow], count: 1, insertDefaultCells: false }, { ignoreFamily: changesFamily });
                }

                // TODO: detect missing cells, which are covered by merged cells outside of the cell range
                // (but do not generate cells covered by merged cells INSIDE the cell range)
            };

            if (!cellRangeInfo) {
                globalLogger.error('Editor.#copyCellRangeSelection(): invalid cell range selection');
                return [];
            }

            // split the paragraph to insert the new table between the text portions
            generator.generateOperation(Op.PARA_SPLIT, { start: [0, 0] });

            // generate the operation to create the new table
            oldTableAttributes = this.tableStyles.getElementAttributes(cellRangeInfo.tableNode);
            newTableAttributes = getExplicitAttributeSet(cellRangeInfo.tableNode);
            newTableAttributes.table = newTableAttributes.table || {};
            newTableAttributes.table.tableGrid = oldTableAttributes.table.tableGrid.slice(cellRangeInfo.firstCellPosition[1], cellRangeInfo.lastCellPosition[1] + 1);
            if (newTableAttributes && newTableAttributes[changesFamily]) { delete newTableAttributes[changesFamily]; }
            generator.generateOperation(Op.TABLE_INSERT, { start: [1], attrs: newTableAttributes });

            // all covered rows in the table
            tableRowNodes = getTableRows(cellRangeInfo.tableNode).slice(cellRangeInfo.firstCellPosition[0], cellRangeInfo.lastCellPosition[0] + 1);

            // visit the cell nodes covered by the selection
            result = selection.iterateTableCells((cellNode, _position, row, col) => {

                // generate operations for new rows, and for cells covered by merged cells outside the range
                generateMissingRowsAndCells(row, col);

                // generate operations for the cell
                generator.generateTableCellOperations(cellNode, [1, row, col], { ignoreFamily: changesFamily });
            });

            // missing rows at bottom of range, covered completely by merged cells (using relative cellRangeInfo, task 30839)
            generateMissingRowsAndCells(cellRangeInfo.lastCellPosition[0] - cellRangeInfo.firstCellPosition[0], cellRangeInfo.lastCellPosition[1] - cellRangeInfo.firstCellPosition[1] + 1);

            // return operations, if iteration has not stopped on error
            return (result === BREAK) ? [] : generator.getOperations();
        }

        /**
         * Generates operations needed to copy the current drawing selection to
         * the internal clipboard.
         *
         * @returns {Array}
         *  The operations array that represents the current selection.
         */
        #copyDrawingSelection() {
            // the operations generator
            const generator = this.createOperationGenerator();
            // the attribute property for the change track
            const changesFamily = 'changes';
            // the start position for the drawing
            let startPosition = [0, 0];
            // the selected drawing nodes
            let drawingNodes = null;
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();

            // the selected drawing nodes
            drawingNodes = this.getSelection().getSelectedDrawings();
            // generate operations
            _.each(drawingNodes, node => {

                let copyDrawing = true; // whether a drawing will become part of the clipboard

                // switching to node in drawing layer
                if (isDrawingPlaceHolderNode(node)) { node = getDrawingPlaceHolderNode(node); }

                if (this.#docApp.isPresentationApp() && this.isEmptyPlaceHolderDrawing(node)) { copyDrawing = false; } // not copying empty place holder drawings

                if (copyDrawing) {
                    // generate operations for the drawing (including its attributes)
                    generator.generateDrawingOperations(node, startPosition, { ignoreFamily: changesFamily, target, allAttributes: true });

                    // set start position for next drawing
                    startPosition = increaseLastIndex(startPosition, 1);
                }
            });

            // the resulting operations
            return generator.getOperations();
        }

        /**
         * Generates operations needed to copy the current selected slide(s) selection to
         * the internal clipboard.
         *
         * @returns {Array}
         *  The operations array that represents the current selection.
         */
        #copySlides() {
            // the operations generator
            const generator = this.createOperationGenerator();
            // the selected slide(s) in slide pane
            let selectedSlides = null;
            // zero-based position of the current slide
            let position = 0;
            // id of the current file
            const fileId = this.#docApp.getFileDescriptor().id;

            // the selected selected slide(s)
            selectedSlides = this.getSlidePaneSelection();
            // sort selected slides ascending
            if (_.isArray(selectedSlides)) {
                selectedSlides.sort((a, b) => { return a - b; });
            }
            // generate operations
            _.each(selectedSlides, slideIndex => {
                const slideId = this.getIdOfSlideOfActiveViewAtIndex(slideIndex);
                const slideNode = this.getSlideById(slideId);
                const parentId = this.isLayoutOrMasterId(slideId) ? this.getMasterSlideId(slideId) : this.getLayoutSlideId(slideId);
                let operation;
                const options = {};
                const slideAttrs = getExplicitAttributeSet(slideNode);

                // insert slide op
                if (this.isMasterView()) {
                    operation = { start: [position], target: parentId, id: null, attrs: _.extend({ slide: { type: this.getSlideFamilyAttributeForSlide(slideId, 'slide', 'type') } }, slideAttrs) }; // start, target and id needs to be calculated on paste
                    generator.generateOperation('insertLayoutSlide', operation);
                    options.getDrawingListStyles = true;
                } else {
                    // start needs to be calculated on paste, target also
                    operation = { start: [position], target: parentId, options: { type: this.getSlideFamilyAttributeForSlide(parentId, 'slide', 'type'), fileId } };
                    if (!_.isEmpty(slideAttrs)) {
                        operation.attrs = slideAttrs;
                    }
                    generator.generateOperation('insertSlide', operation);
                }

                // generate operations for the drawing (including its attributes)
                generator.generateParagraphChildOperations(slideNode, [position], options);
                // increase position for the next slide
                position++;
            });

            // the resulting operations
            return generator.getOperations();
        }

        /**
         * Creates operations from the clipboard data returned by parseClipboard(...)
         * and applies them asynchronously.
         *
         * @param {Array} clipboardData
         *  The clipboard data array to create operations from.
         *
         * @param {Number[]} dropPosition
         *  An optional logical position used for pasting the content.
         */
        #createOperationsFromExternalClipboard(clipboardData, dropPosition) {

            // don't paste content if the presentation hasn't at least one slide
            if (this.useSlideMode() && this.isEmptySlideView && this.isEmptySlideView()) { return $.when(); }

            // make sure that only one paste call is processed at the same time
            if (this.checkSetClipboardPasteInProgress()) { return $.when(); }

            // returns true if the clipboard data contains drawing operations
            const containsDrawings = clipboardData => {
                return _.some(clipboardData, entry => { return (entry.operation === Op.INSERT_DRAWING || entry.operation === Op.TABLE_INSERT); });
            };

            // returns true if the given entry is an insert image operation
            const isInsertImage = entry => {
                return (entry && entry.operation === Op.INSERT_DRAWING && entry.type === 'image');
            };

            // returns true if the given entry is an insert image operation and has an URL set which is not a web-kit fake URL.
            const isInsertImageWithValidUrl = entry => {
                return (isInsertImage(entry) && _.isString(entry.data) && !_.isEmpty(entry.data) && !isWebkitFakeUrlError(entry.data));
            };

            const isInsertTable = entry => {
                return (entry && entry.operation === 'insertTable');
            };

            // returns true if the given entry is an insert hyperlink operation
            const isInsertHyperlink = entry => {
                return (entry && entry.operation === 'insertHyperlink');
            };

            // returns true if all clipboard data entries are insert image operations
            const containsOnlyImages = (clipboardData, options) => {
                const ignoreHyperlinks = getBooleanOption(options, 'ignoreHyperlinks', false);

                return _.every(clipboardData, (entry, index, entries) => {
                    // check for insert image operations, ignore insert hyperlink after insert image operations
                    return ((isInsertImage(entry)) || (ignoreHyperlinks && isInsertHyperlink(entry) && isInsertImage(entries[index - 1])));
                });
            };

            // the generator for stylesheet operations that need to be executed first
            const styleCache = new StyleCache(this);
            // the operations generator
            const generator = this.createOperationGenerator();
            // the selection object
            const selection = this.getSelection();
            // the anchor position to paste new drawings
            const drawingPasteAnchor = selection.isTopLevelTextCursor() ? { top: 0, left: 0 } : this.#getMinPositionForSelectedDrawings();
            // whether the clipboard data contains only text or also drawings
            const isDrawingInClipboard = containsDrawings(clipboardData);
            // whether all operations are insert image
            const isOnlyInsertImageInClipboard = containsOnlyImages(clipboardData, { ignoreHyperlinks: true });
            // the operation start position
            let start = this.#getAnchorPositionForPaste({
                dropPosition,
                isDrawingInClipboard,
                isTableInClipboard: isDrawingInClipboard,
                isOnlyInsertImageInClipboard,
                isInternalClipboard: false
            });
            // the operation end position
            let end = _.clone(start);
            // all valid logical place holder positions for inserting an image
            const imagePlaceHolderPositions = (isOnlyInsertImageInClipboard && this.#docApp.isPresentationApp()) ? this.getAllValidPlaceHolderPositionsForInsertDrawing('pic') : [];
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // used to cancel operation preparing in iterateArraySliced
            let cancelled = false;
            // the ratio of operation generation to apply the operations
            const generationRatio = 0.2;
            // current element container node
            const rootNode = this.getCurrentRootNode(target);
            // whether the selection represents a simple text cursor inside a top level node
            const isTopLevelTextCursor = selection.isTopLevelTextCursor();
            // whether the current selection is a text selection inside a text frame
            const isAnyTextFrameSelection = selection.isAnyTextFrameSelection();
            // whether the current selection is a place holder drawing
            const isPlaceHolderSelection = this.#isPlaceHolderDrawingSelection();
            // the list collection object
            const listCollection = this.getListCollection();
            // the current paragraph style sheet id
            let styleId = null;
            // OX Text default paragraph list style sheet id
            const listParaStyleId = (this.useSlideMode()) ? null : this.getDefaultUIParagraphListStylesheet();
            // the list style sheet id
            let listStyleId = null;
            // indicates if previous operations inserted a list
            let listInserted = false;
            // the highest list level supported by the current document type (OOXML => 8, ODF => 9)
            const maxListLevel = this.#docApp.isODF() ? 9 : 8;
            // OX Text hyperlink style sheet id
            const hyperlinkStyleId = (this.useSlideMode()) ? null : this.getDefaultUIHyperlinkStylesheet();
            // indicates if the previous operation was insertHyperlink
            let hyperLinkInserted = false;
            // the default drawing attributes
            const insertDrawingAttrs = this.#getDefaultDrawingAttributes();

            // the operation after insertHyperlink needs to remove the hyperlink style again
            const removeHyperLinkStyle = (start, end, nextEntry) => {

                if (hyperLinkInserted && (!nextEntry || nextEntry.operation !== 'insertHyperlink')) {
                    // generate the 'setAttributes' operation to remove the hyperlink style
                    generator.generateOperation(Op.SET_ATTRIBUTES, {
                        attrs: CLEAR_ATTRIBUTES,
                        start: _.clone(start),
                        end: decreaseLastIndex(end)
                    });
                    hyperLinkInserted = false;
                }
            };

            // checks the given position for a text span with hyperlink attributes
            const isHyperlinkAtPosition = position => {
                const span = getSelectedElement(rootNode, decreaseLastIndex(position), 'span');
                const characterAttrs = getExplicitAttributes(span, 'character', true);

                return !!(characterAttrs && _.isString(characterAttrs.url));
            };

            // handle implicit paragraph
            const doHandleImplicitParagraph = start => {
                const position = _.clone(start);
                let paragraph;

                if (position.pop() === 0) {  // is this an empty paragraph?
                    paragraph = getParagraphElement(rootNode, position);
                    if ((paragraph) && (isImplicitParagraphNode(paragraph)) && (getParagraphNodeLength(paragraph) === 0)) {
                        // creating new paragraph explicitely
                        generator.generateOperation(Op.PARA_INSERT, {
                            start: _.copy(position)
                        });
                    }
                }
            };

            // returns the next valid drawing position according to the given position
            const getSlideModeDrawingPosition = position => {
                let result = null;

                if (_.isArray(position)) {
                    if (position.length > 2) {
                        result  = position.splice(0, 2);
                        result = increaseLastIndex(result);
                    } else {
                        result = _.clone(position);
                    }
                }

                return result;
            };

            // returns true if the list definition is a MS Word list definition
            const isMSListDefinition = listDefinition => {
                return _.some(listDefinition, level => {
                    return (_.isString(level.numberFormat) && !_.isEmpty(level.numberFormat));
                });
            };

            // returns the attributes for a list paragraph in slide mode
            const getSlideModeListAttributes = entry => {
                if (!this.useSlideMode() || !entry) { return {}; }

                // the current list level, restricted to 0 - 8 for OOXML and 0 - 9 for ODF
                const listLevel = math.clamp(entry.listLevel, 0, maxListLevel);
                // the list definition
                const listDefinition = entry.type || null;
                // the definition for the current list level
                const levelDefinition = (listDefinition && listDefinition[listLevel]) || null;
                // the list paragraph attributes for the list level
                let listLevelAttributes = null;
                // the resulting list paragraph attributes
                const paragraph = { level: listLevel };

                // when pasting into place holder drawings, list level is the only attribute to provide
                if (listDefinition && !this.#isPlaceHolderDrawing(selection.getAnyTextFrameDrawing({ forceTextFrame: true }))) {
                    // get list paragraph attributes from MS Word or HTML list definition
                    if (isMSListDefinition(listDefinition)) {
                        listLevelAttributes = this.#getSlideModeListParagraphAttributesFromLevelDefinition(levelDefinition);
                    } else {
                        listLevelAttributes = this.#getSlideModeListParagraphAttributesFromHTMLType(levelDefinition, listLevel);
                    }

                    _.extend(paragraph, listLevelAttributes);
                }

                return { paragraph };
            };

            // returns true for web-kit fake URLs.
            // Note: it's not possible to access data of a web-kit fake URL.
            const isWebkitFakeUrl = url => {
                return (_.isString(url) && ((url.substring(0, 15) === 'webkit-fake-url') || (url.substring(0, 7) === 'file://')));
            };

            // returns true for web-kit fake URLs and shows an error.
            const isWebkitFakeUrlError = url => {
                if (isWebkitFakeUrl(url)) {
                    this.#docApp.rejectEditAttempt('image');
                    return true;
                }
                return false;
            };

            // Insert text frame with an initial paragraph
            // Slide Mode
            // Operation: insertDrawing, insertParagraph
            const insertTextFrameWithParagraph = position => {

                // create text frame
                let operationObj = {
                    start: _.clone(position),
                    type: 'shape',
                    attrs: _.copy(insertDrawingAttrs, true) // this.#shiftDrawingPositions() later changes top and left position, so a deep copy is needed here
                };

                this.extendPropertiesWithTarget(operationObj, target);

                generator.generateOperation(Op.INSERT_DRAWING, operationObj);

                // add paragraph to text frame
                position = appendNewIndex(position, 0);
                operationObj = { start: _.copy(position) };
                if (this.#docApp.isODF()) { operationObj.attrs = { paragraph: { bullet: { type: 'none' } } }; } // DOCS-4237
                this.extendPropertiesWithTarget(operationObj, target);

                generator.generateOperation(Op.PARA_INSERT, operationObj);

                // set the first valid text position in the new paragraph of the text frame
                end = appendNewIndex(position, 0);
            };

            // Insert a new paragraph
            // Text Mode
            // Operation: splitParagraph
            const splitParagraphTextMode = entry => {

                const lastPos = start.length - 1;
                let attributes = null;
                let styleAttributes = null;

                // generate the 'insertParagraph' operation
                generator.generateOperation(Op.PARA_SPLIT, {
                    start: _.clone(start)
                });

                end[lastPos - 1] += 1;
                end[lastPos] = 0;

                // set attributes only if it's not a list paragraph, these are handled in 'insertListElement'
                if (entry.listLevel === -1) {
                    // use the nextStyleId if the current paragraph style defines one
                    styleAttributes = this.paragraphStyles.getStyleSheetAttributeMap(styleId, 'paragraph');
                    styleId = styleAttributes.paragraph?.nextStyleId;

                    if (listInserted) {
                        // remove list style
                        if ((!_.isString(styleId)) || (styleId === this.getDefaultUIParagraphListStylesheet())) {
                            styleId = this.getDefaultUIParagraphStylesheet();
                        }
                        attributes = { styleId, paragraph: { listStyleId: null, listLevel: -1 } };
                        listInserted = false;

                    } else if (_.isString(styleId)) {
                        attributes = { styleId };
                    }

                    if (_.isString(styleId)) {
                        // generate operation to insert a dirty paragraph style into the document
                        this.paragraphStyles.generateMissingStyleSheetOperations(styleCache, styleId);

                        // generate the 'setAttributes' operation
                        generator.generateOperation(Op.SET_ATTRIBUTES, {
                            start: _.copy(end.slice(0, -1)),
                            attrs: attributes
                        });
                    }
                }

                return null;
            };

            const insertParagraph = () => {
                start = start.splice(0, 1);
                start = increaseLastIndex(start);

                generator.generateOperation(Op.PARA_INSERT, {
                    start: _.clone(start)
                });

                start = appendNewIndex(start, 0);
                end = start;
                return null;
            };

            // Insert a new paragraph
            // Slide Mode
            // Operation: splitParagraph
            const splitParagraphSlideMode = entry => {
                const lastPos = start.length - 1;

                // handle non-list paragraphs only, list paragraphs are handled in 'insertListElement'
                if (entry.listLevel === -1) {
                    generator.generateOperation(Op.PARA_SPLIT, {
                        start: _.clone(start)
                    });

                    end[lastPos - 1] += 1;
                    end[lastPos] = 0;
                }
                return null;
            };

            // Insert a new paragraph
            // Sheet Mode
            // Operation: splitParagraph
            const splitParagraphSheetMode = () => {
                const lastPos = start.length - 1;

                generator.generateOperation(Op.PARA_SPLIT, {
                    start: _.clone(start)
                });

                end[lastPos - 1] += 1;
                end[lastPos] = 0;

                return null;
            };

            // Insert plain text
            // Text Mode and Slide Mode
            // Operation: insertText
            const insertText = (entry, nextEntry) => {
                const lastPos = start.length - 1;

                if (entry.data) {
                    generator.generateOperation(Op.TEXT_INSERT, {
                        text: entry.data,
                        start: _.clone(start),
                        attrs: { changes: { inserted: null, removed: null, modified: null } } // 41152
                    });

                    end[lastPos] += entry.data.length;
                }

                removeHyperLinkStyle(start, end, nextEntry);
                return null;
            };

            // Set attributes
            // "textOnlyClipboard" (<- currently only for spreadsheet)
            // Operation: setAttributes
            const setAttributes = entry => {

                const lastPos = start.length - 1;
                const from = _.clone(start);
                const to = _.clone(start);

                from[lastPos]  -= entry.data.len;
                to[lastPos]    -= 1;

                if (entry.data) {
                    generator.generateOperation(Op.SET_ATTRIBUTES, {
                        start:  _.copy(from),
                        end:    _.copy(to),
                        attrs:  entry.data.attrs
                    });
                }

                return null;
            };

            // Insert a tab
            // Text Mode and Slide Mode
            // Operation: insertTab
            const  insertTab = (_entry, nextEntry) => {

                const lastPos = start.length - 1;
                const attributes = this.getPreselectedAttributes();

                generator.generateOperation(Op.TAB_INSERT,
                    _.isObject(attributes) ? { start: _.clone(start), attrs: attributes } : { start: _.clone(start) }
                );

                end[lastPos] += 1;
                removeHyperLinkStyle(start, end, nextEntry);

                return null;
            };

            // Insert an image
            // Text Mode
            // Operation: insertDrawing
            const insertDrawingTextMode = (entry, nextEntry) => {

                const resultDef = this.createDeferred();
                const lastPos = start.length - 1;

                if (isInsertImageWithValidUrl(entry)) {
                    // get media url
                    let promise = isBase64(entry.data) ? insertDataUrl(this.#docApp, entry.data) : insertURL(this.#docApp, entry.data);

                    promise = promise.then(async result => {

                        const size = await this.getImageSize(result.url);
                        const attrSet = {
                            drawing: { ...size, ...this.getDefaultDrawingMargins() },
                            image: { imageUrl: result.url }
                        };
                        applyImageTransformation(attrSet.drawing, result.transform);

                        // exit silently if we lost the edit rights
                        if (!this.#docApp.isEditable()) { return; }

                        // generate the 'insertDrawing' operation
                        generator.generateOperation(Op.INSERT_DRAWING, {
                            start: _.clone(start),
                            type: 'image',
                            attrs: attrSet
                        });

                        // set end position
                        end[lastPos] += 1;
                        removeHyperLinkStyle(start, end, nextEntry);
                    });

                    // catch all errors
                    promise = promise.catch(() => {
                        this.#docApp.rejectEditAttempt('image');
                    });

                    // always resolve to continue processing paste operations
                    promise.always(() => {
                        resultDef.resolve();
                    });

                } else {
                    // always resolve to continue processing paste operations
                    resultDef.resolve();
                }

                return resultDef;
            };

            // Insert an image or a text frame (a text frame is added after an image as new text target)
            // Slide Mode
            // Operation: insertDrawing
            const insertDrawingSlideMode = (entry, nextEntry) => {

                const resultDef = this.createDeferred();
                let position = null;

                if (entry.type === 'shape') {
                    // get valid drawing position for the text frame
                    position = getSlideModeDrawingPosition(start);
                    // create text frame and add a paragraph
                    insertTextFrameWithParagraph(position);
                    resultDef.resolve();

                } else if (isInsertImageWithValidUrl(entry)) {

                    // get media url
                    let promise = isBase64(entry.data) ? insertDataUrl(this.#docApp, entry.data) : insertURL(this.#docApp, entry.data);

                    // in case of success insert the image
                    promise = promise.then(async result => {
                        const attrSet = {
                            drawing: await this.getImageSize(result.url),
                            image: { imageUrl: result.url }
                        };
                        applyImageTransformation(attrSet.drawing, result.transform);
                        return attrSet;
                    });

                    // in case of an error insert a placeholder image
                    promise = promise.catch(() => {
                        this.#docApp.rejectEditAttempt('image');
                        return PLACEHOLDER_IMAGE_DRAWING_ATTRIBUTES;
                    });

                    promise.then(attrSet => {
                        // exit silently if we lost the edit rights
                        if (!this.#docApp.isEditable()) { return; }

                        // get valid drawing position for the image
                        position = getSlideModeDrawingPosition(start);

                        // generate the 'insertDrawing' operation
                        generator.generateOperation(Op.INSERT_DRAWING, {
                            start: _.copy(position),
                            type: 'image',
                            attrs: attrSet
                        });

                        // set end position and insert text frame
                        end = increaseLastIndex(position);
                        removeHyperLinkStyle(position, end, nextEntry);

                        if (nextEntry && !isInsertImage(nextEntry) && !isInsertTable(nextEntry)) {
                            // create text frame and add a paragraph
                            position = increaseLastIndex(position);
                            insertTextFrameWithParagraph(position);
                        }

                        // always resolve to continue processing paste operations
                        resultDef.resolve();
                    });

                } else {
                    if (nextEntry && !isInsertImage(nextEntry) && !isInsertTable(nextEntry) && !isInsertHyperlink(nextEntry)) {
                        position = getSlideModeDrawingPosition(start);
                        // create text frame and add a paragraph
                        insertTextFrameWithParagraph(position);
                    }
                    // nothing to do, but always resolve to continue processing paste operations
                    resultDef.resolve();
                }

                return resultDef;
            };

            // Insert an image, special handling when the clipboard data contains only images
            // Slide Mode
            // Operation: insertDrawing
            const insertImageSlideMode = entry => {

                const resultDef = this.createDeferred();

                if (!isOnlyInsertImageInClipboard || !isInsertImageWithValidUrl(entry)) {
                    // nothing to do, but always resolve to continue processing paste operations
                    return resultDef.resolve();
                }

                // get media url
                let promise = isBase64(entry.data) ? insertDataUrl(this.#docApp, entry.data) : insertURL(this.#docApp, entry.data);

                promise = promise.then(async result => {

                    // the logical position of the image
                    let position = null;
                    // image size
                    let size = await this.getImageSize(result.url);

                    // the attributes for the image
                    const attrSet = {
                        drawing: { ...this.getDefaultDrawingMargins() },
                        image: { imageUrl: result.url },
                        line: { type: 'none' }
                    };
                    applyImageTransformation(attrSet.drawing, result.transform);

                    // exit silently if we lost the edit rights
                    if (!this.#docApp.isEditable()) { return; }

                    // check if a valid place holder position is available
                    const placeHolder = imagePlaceHolderPositions.shift();
                    if (placeHolder) {
                        // replace place holder with image

                        position = placeHolder.position;
                        const rectangle = placeHolder.rectangle;
                        const explicitAttrs = placeHolder.attributes;
                        const replacePicPlaceHoder = explicitAttrs?.presentation?.phType === 'pic';

                        // generate the 'delete' operation to remove the place holder
                        generator.generateOperation(Op.DELETE, {
                            start: _.copy(position),
                            target
                        });

                        if (explicitAttrs) {
                            // expanding the drawing attributes with the explicit attributes of the existing place holder drawing
                            _.extend(attrSet, explicitAttrs); // simply overwriting existing drawing attributes 'name', 'left' and 'top'
                            // ... but not sending phSize to the server
                            if (attrSet.presentation?.phSize) { delete attrSet.presentation.phSize; }

                            // inserting an image into the image place holder requires a cropping of the drawing
                            if (replacePicPlaceHoder) {
                                const cropAttrs = this.calculateCroppingValue(rectangle, size);
                                if (cropAttrs?.value) {
                                    if (cropAttrs.isHorizontal) {
                                        attrSet.image.cropLeft = attrSet.image.cropRight = cropAttrs.value;
                                    } else {
                                        attrSet.image.cropTop = attrSet.image.cropBottom = cropAttrs.value;
                                    }
                                }
                            } else {
                                // in content body place holder the inserted image must not be larger than the drawing
                                const newSize = this.checkImageSizeInDrawing(rectangle, size);
                                if (newSize) { size = newSize; }
                            }
                        }

                        // expanding the drawing attributes with the default position and size of the image
                        // -> but not for drawing place holders of type 'pic'
                        if (!replacePicPlaceHoder) {

                            Object.assign(attrSet.drawing, size); // adding width and height to the drawing attributes

                            if (rectangle) {
                                attrSet.drawing.top = rectangle.top + (0.5 * (rectangle.height - attrSet.drawing.height));
                                attrSet.drawing.left = rectangle.left + (0.5 * (rectangle.width - attrSet.drawing.width));
                            }
                        }

                        // no change of aspect ratio
                        attrSet.drawing.aspectLocked = true;

                    } else {
                        // get valid slide level position for the image

                        position = getSlideModeDrawingPosition(start);
                        // adding width and height to the drawing attributes
                        Object.assign(attrSet.drawing, size);
                        // set end position
                        end = increaseLastIndex(position);
                        // adapting the image size to the slide size
                        if (this.#docApp.isPresentationApp() && this.adaptImageSizeAndPositionToSlide) {
                            this.adaptImageSizeAndPositionToSlide(attrSet.drawing);
                        }
                    }

                    // generate the 'insertDrawing' operation
                    generator.generateOperation(Op.INSERT_DRAWING, {
                        start: _.copy(position),
                        type: 'image',
                        attrs: attrSet,
                        target
                    });
                });

                promise = promise.catch(() => {
                    this.#docApp.rejectEditAttempt('image');
                });

                // always resolve to continue processing paste operations
                promise.always(() => {
                    resultDef.resolve();
                });

                return resultDef;
            };

            // Insert a hyperlink
            // Text Mode and Slide Mode
            // Operation: setAttributes
            const insertHyperlink = entry => {

                const lastPos = start.length - 1;
                let startPosition = null;
                let endPosition = null;
                let attributes = null;

                // check for valid position and valid URL data
                if (entry.data && _.isNumber(entry.length) && (entry.length > 0) && hasSupportedProtocol(entry.data) && isValidURL(entry.data)) {
                    // prepare attributes
                    attributes = { character: { url: entry.data } };

                    // no hyperlinkStyleId in slide mode
                    if (!this.useSlideMode()) {
                        attributes.styleId = hyperlinkStyleId;

                        // generate operation to insert a dirty character style into the document
                        if (_.isString(hyperlinkStyleId)) {
                            this.characterStyles.generateMissingStyleSheetOperations(styleCache, hyperlinkStyleId);
                        }
                    }

                    // the text for the hyperlink has already been inserted and the start position is right after this text,
                    // so the start for the hyperlink attribute is the start position minus the text length
                    startPosition = _.clone(start);
                    startPosition[lastPos] -= entry.length;
                    if (startPosition[lastPos] < 0) {
                        startPosition[lastPos] = 0;
                    }

                    // Task 35689, position of last character in range
                    endPosition = _.clone(end);
                    endPosition[lastPos] -= 1;

                    if (endPosition[lastPos] >= 0) { // avoiding invalid setAttribute operations

                        // generate the 'setAttributes' operation
                        generator.generateOperation(Op.SET_ATTRIBUTES, {
                            attrs: attributes,
                            start: _.copy(startPosition),
                            end: _.copy(endPosition)
                        });

                        hyperLinkInserted = true;
                    }
                }

                return null;
            };

            // Insert a list style with a list definition generated from HTML data
            // Text Mode
            // Operation: insertListStyle
            const insertListTextMode = entry => {

                let listOperation = null;

                // for the list root level insert list style with all style levels
                if (listCollection && entry.listLevel === -1) {
                    listOperation = listCollection.getListOperationFromHtmlListTypes(entry.type);
                    listStyleId = this.#getFreeListStyleId();

                    // generate the 'insertList' operation
                    generator.generateOperation(Op.INSERT_LIST, {
                        listStyleId,
                        listDefinition: listOperation.listDefinition
                    });
                }

                return null;
            };

            // Insert a list style with a list definition generated from a MS list definition
            // Text Mode
            // Operation: insertListStyle
            const insertMSListTextMode = entry => {

                let listOperation = null;

                if (listCollection) {
                    listOperation = listCollection.getListOperationFromListDefinition(entry.data);
                    listStyleId = this.#getFreeListStyleId();

                    // generate the 'insertList' operation
                    generator.generateOperation(Op.INSERT_LIST, {
                        listStyleId,
                        listDefinition: listOperation.listDefinition
                    });
                }

                return null;
            };

            // Insert a new item into the list. The paragraph was already inserted so just set the list attributes here
            // Text Mode
            // Operation: setAttributes
            const insertListElementTextMode = entry => {

                if (listStyleId && _.isNumber(entry.listLevel)) {

                    // generate operation to insert a dirty paragraph style into the document
                    if (_.isString(listParaStyleId)) {
                        this.paragraphStyles.generateMissingStyleSheetOperations(styleCache, listParaStyleId);
                    }

                    // generate the 'setAttributes' operation
                    generator.generateOperation(Op.SET_ATTRIBUTES, {
                        start: _.copy(start.slice(0, -1)),
                        attrs: { styleId: listParaStyleId, paragraph: { listStyleId, listLevel: entry.listLevel } }
                    });

                    listInserted = true;
                }

                return null;
            };

            // Insert a new item into the list. Insert paragraph with list attributes
            // Slide Mode
            // Operation: insertParagraph
            const insertListElementSlideMode = entry => {

                let position = null;

                if (_.isNumber(entry.listLevel)) {
                    // get position for the new paragraph
                    position = start.slice(0, -1);
                    position = increaseLastIndex(position);

                    // generate the 'insertParagraph' operation
                    generator.generateOperation(Op.PARA_INSERT, {
                        start: _.copy(position),
                        attrs: getSlideModeListAttributes(entry)
                    });

                    // the first valid text position in the new paragraph
                    end = appendNewIndex(position, 0);

                    listInserted = true;
                }

                return null;
            };

            // Insert an additional paragraph into a list item, from the second paragraph on they are no bullets or numberings
            // Text Mode and Slide Mode
            // Operation: insertHardBreak
            const insertListParagraph = () => {

                const lastPos = start.length - 1;

                generator.generateOperation(Op.HARDBREAK_INSERT, {
                    start: _.clone(start)
                });

                end[lastPos] += 1;

                return null;
            };

            const insertTableTextMode = entry => {
                const gridArray = [];
                const currentParagraphPos = start[1];

                _(entry.size).times(() => { gridArray.push(1000); });

                start = start.splice(0, 1);

                // something is in the current paragraph (use a new one)
                if (currentParagraphPos > 0) {
                    start = increaseLastIndex(start);
                }

                // insert table caption into a separate paragraph
                if (!_.isEmpty(entry.caption)) {
                    generator.generateOperation(Op.PARA_INSERT, {
                        start: _.clone(start)
                    });

                    generator.generateOperation(Op.TEXT_INSERT, {
                        text: entry.caption,
                        start: appendNewIndex(start, 0),
                        attrs: { changes: { inserted: null, removed: null, modified: null } } // 41152
                    });

                    start = increaseLastIndex(start);
                }

                this.generateInsertStyleOp(generator, /*family*/ 'table', /*styleId*/ this.getDefaultUITableStylesheet(), /*setDefault*/ true);
                generator.generateOperation(Op.TABLE_INSERT, {
                    start: _.copy(start),
                    attrs: {
                        table: {
                            tableGrid: gridArray,
                            width: 'auto',
                            exclude: ['lastRow', 'lastCol', 'bandsVert']
                        },
                        styleId: this.getDefaultUITableStylesheet()
                    }
                });

                end = start;
                return null;
            };

            const insertTableSlideMode = entry => {

                const gridArray = [];
                const currentParagraphPos = start[3];

                _(entry.size).times(() => { gridArray.push(1000); });

                start = start.splice(0, 2);

                // something is in the current paragraph (use a new one)
                if (currentParagraphPos > 0) {
                    start = increaseLastIndex(start);
                }

                this.generateInsertStyleOp(generator, /*family*/ 'table', /*styleId*/ this.getDefaultUITableStylesheet(), /*setDefault*/ true);
                generator.generateOperation(Op.INSERT_DRAWING, {
                    attrs: {
                        drawing: {
                            width: 8000,
                            left: 5000,
                            top: 3000,
                            noGroup: true
                        },
                        styleId: this.getDefaultUITableStylesheet(),
                        table: {
                            tableGrid: gridArray,
                            width: 'auto',
                            exclude: ['bandsVert', 'firstCol', 'lastCol', 'lastRow']
                        }
                    },
                    start: _.copy(start),
                    type: 'table'
                });

                end = start;
                return null;
            };

            const insertRow = mode => {

                const length = (mode === 'text') ? 2 : 3;

                if (start.length >= length) {
                    start = start.splice(0, length);
                    start = increaseLastIndex(start);
                } else {
                    while (start.length < length) {
                        start = appendNewIndex(start, 0);
                    }
                }

                generator.generateOperation(Op.ROWS_INSERT, {
                    start: _.copy(start),
                    insertDefaultCells: true
                });

                end = appendNewIndex(start, 0);
                return null;
            };

            const prepareCell = mode => {

                const textPosLen = (mode === 'text') ? 5 : 6;
                const paraPosLen = (mode === 'text') ? 4 : 5;

                if (start.length >= paraPosLen) {
                    start = start.splice(0, (paraPosLen - 1));
                }

                while (start.length < paraPosLen) {
                    start = appendNewIndex(start, 0);
                }

                if (start.length < textPosLen) {
                    while (start.length < textPosLen) {
                        start = appendNewIndex(start, 0);
                    }
                }

                end = _.clone(start);
                return null;
            };

            const insertCellData = entry => {

                const lastPos = start.length - 1;

                if (entry.data) {
                    generator.generateOperation(Op.TEXT_INSERT, {
                        text: entry.data,
                        start: _.clone(start)
                    });

                    start[lastPos] += entry.data.length;
                }

                end = _.clone(start);

                return null;
            };

            const finalizeCell = mode => {
                const cellPosLen = (mode === 'text') ? 3 : 4;

                // set "cursor" in the next cell
                start = start.splice(0, cellPosLen);
                start = increaseLastIndex(start);

                end = _.clone(start);
                return null;
            };

            const getOperationGenerationCallback = () => {
                if (this.#docApp.isTextApp()) {
                    return generateTextModeOperationCallback;
                } else if (this.#docApp.isPresentationApp()) {
                    return generateSlideModeOperationCallback;
                } else if (this.#docApp.isSpreadsheetApp()) {
                    return generateSheetModeOperationCallback;
                }

                return _.noop;
            };

            // parse entry and generate operations for text mode
            const generateTextModeOperationCallback = (entry, index, dataArray) => {
                let def = null;
                const nextEntry = dataArray[index + 1] || null;

                // next operation's start is previous operation's end
                start = _.clone(end);
                end = _.clone(end);

                // cancel the clipboard paste if we loose the edit rights or if the 'cancel' button is clicked.
                if (cancelled || !this.#docApp.isEditable()) { return BREAK; }
                switch (entry.operation) {

                    case Op.PARA_INSERT:
                        if (entry.type && (entry.type === 'leaveTable' || entry.type === 'enclosingTable')) {
                            def = insertParagraph(entry);
                        } else {
                            def = splitParagraphTextMode(entry);
                        }
                        break;

                    case Op.TEXT_INSERT:
                        def = insertText(entry, nextEntry);
                        break;

                    case Op.TAB_INSERT:
                        def = insertTab(entry, nextEntry);
                        break;

                    case Op.INSERT_DRAWING:
                        def = insertDrawingTextMode(entry, nextEntry);
                        break;

                    case 'insertHyperlink':
                        def = insertHyperlink(entry);
                        break;

                    case 'insertList':
                        def = insertListTextMode(entry);
                        break;

                    case 'insertMSList':
                        def = insertMSListTextMode(entry);
                        break;

                    case 'insertListElement':
                        def = insertListElementTextMode(entry);
                        break;

                    // needed for additional paragraph inside a list element, from the second paragraph on they have no bullet or numbering
                    case 'insertListParagraph':
                        def = insertListParagraph();
                        break;

                    case Op.TABLE_INSERT:
                        def = insertTableTextMode(entry);
                        break;

                    case 'insertRow':
                        def = insertRow(/* mode = */'text');
                        break;

                    case 'prepareCell':
                        def = prepareCell(/* mode = */'text');
                        break;

                    case 'insertCellData':
                        def = insertCellData(entry);
                        break;

                    case 'finalizeCell':
                        def = finalizeCell(/* mode = */'text');
                        break;

                    case Op.SET_ATTRIBUTES:
                        def = setAttributes(entry);
                        break;

                    default:
                        globalLogger.log('this.#createOperationsFromExternalClipboard(...) - unhandled operation: ' + entry.operation);
                }

                return def;

            }; // generateTextModeOperationCallback

            // parse entry and generate operations for slide mode
            const generateSlideModeOperationCallback = (entry, index, dataArray) => {
                let def = null;
                const nextEntry = dataArray[index + 1] || null;

                // next operation's start is previous operation's end
                start = _.clone(end);
                end = _.clone(end);

                // cancel the clipboard paste if we loose the edit rights or if the 'cancel' button is clicked.
                if (cancelled || !this.#docApp.isEditable()) { return BREAK; }

                // if no text frame selected, the paste target is the slide. if a text frame is selected, paste target is that frame
                if (isTopLevelTextCursor || isAnyTextFrameSelection || isPlaceHolderSelection) {
                    switch (entry.operation) {

                        case Op.PARA_INSERT:
                            if (entry.type && entry.type === 'leaveTable') {
                                start = start.splice(0, 2);
                                start = increaseLastIndex(start);
                                if (nextEntry && (nextEntry.operation === Op.TEXT_INSERT || nextEntry.operation === Op.PARA_INSERT || nextEntry.operation === 'insertList')) {
                                    def = insertTextFrameWithParagraph(start);
                                } else {
                                    end = _.clone(start);
                                }
                            } else {
                                def = splitParagraphSlideMode(entry);
                            }
                            break;

                        case Op.TEXT_INSERT:
                            def = insertText(entry, nextEntry);
                            break;

                        case Op.TAB_INSERT:
                            def = insertTab(entry, nextEntry);
                            break;

                        case Op.INSERT_DRAWING:
                            if (isOnlyInsertImageInClipboard) {
                                def = insertImageSlideMode(entry);
                            } else if (isTopLevelTextCursor) {
                                def = insertDrawingSlideMode(entry, nextEntry);
                            }
                            break;

                        case 'insertHyperlink':
                            def = insertHyperlink(entry);
                            break;

                        case 'insertListElement':
                            def = insertListElementSlideMode(entry);
                            break;

                        // needed for additional paragraph inside a list element, from the second paragraph on they have no bullet or numbering
                        case 'insertListParagraph':
                            def = insertListParagraph();
                            break;

                        case Op.TABLE_INSERT:
                            def = insertTableSlideMode(entry);
                            break;

                        case 'insertRow':
                            def = insertRow(/* mode = */'slide');
                            break;

                        case 'prepareCell':
                            def = prepareCell(/* mode = */'slide');
                            break;

                        case 'insertCellData':
                            def = insertCellData(entry);
                            break;

                        case 'finalizeCell':
                            def = finalizeCell(/* mode = */'slide');
                            break;

                        case Op.SET_ATTRIBUTES:
                            def = setAttributes(entry);
                            break;

                        default:
                            globalLogger.log('this.#createOperationsFromExternalClipboard(...) - unhandled operation: ' + entry.operation);
                    }
                }

                return def;

            }; // generateSlideModeOperationCallback

            // parse entry and generate operations for sheet mode
            const generateSheetModeOperationCallback = (entry, index, dataArray) => {
                let def = null;
                const nextEntry = dataArray[index + 1] || null;

                // next operation's start is previous operation's end
                start = _.clone(end);
                end = _.clone(end);

                // cancel the clipboard paste if we loose the edit rights or if the 'cancel' button is clicked.
                if (cancelled || !this.#docApp.isEditable()) { return BREAK; }

                // if a text frame is selected, paste target is that frame
                if (isAnyTextFrameSelection) {
                    switch (entry.operation) {

                        case Op.PARA_INSERT:
                        case 'insertListElement':
                            def = splitParagraphSheetMode();
                            break;

                        case Op.TEXT_INSERT:
                        case 'insertCellData':
                            def = insertText(entry, nextEntry);
                            break;

                        case Op.TAB_INSERT:
                            def = insertTab(entry, nextEntry);
                            break;

                            // case 'insertHyperlink':
                            //     def = insertHyperlink(entry);
                            //     break;

                        // needed for additional paragraph inside a list element, from the second paragraph on they have no bullet or numbering
                        case 'insertListParagraph':
                            def = insertListParagraph();
                            break;
                    }
                }

                return def;

            }; // generateSheetModeOperationCallback

            // init the paragraph style sheet id
            styleId = getElementStyleId(getLastNodeFromPositionByNodeName(rootNode, start, PARAGRAPH_NODE_SELECTOR));

            // make sure to remove the hyperlink style if we paste directly after a hyperlink
            hyperLinkInserted = isHyperlinkAtPosition(start);

            // reset id generation for this.#getFreeListStyleId()
            this.#resetListStyleId();

            // add a 'para-insert' when we stand within a table
            if (selection.getEnclosingTable() && _.some(clipboardData, item => { return (item.operation === Op.TABLE_INSERT); })) {
                clipboardData.unshift({
                    operation: Op.PARA_INSERT,
                    type: 'enclosingTable'
                });
            // to paste at the cursor position don't create a paragraph as first operation
            } else if (clipboardData.length > 1 && clipboardData[0].operation === Op.PARA_INSERT) {
                clipboardData.shift();
            } else if (clipboardData.length > 2 && clipboardData[0].operation === 'insertList' && clipboardData[1].operation === Op.PARA_INSERT) {
                clipboardData.splice(1, 1);
            }

            if (this.#docApp.isPresentationApp()) {
                // check if we need an initial text frame for the pasted data
                if ((clipboardData.length > 0) && !isAnyTextFrameSelection && !isInsertImage(clipboardData[0]) && !isInsertTable(clipboardData[0])) {

                    clipboardData.unshift({
                        operation: Op.INSERT_DRAWING,
                        type: 'shape',
                        depth: -1
                    });
                }
            }

            return this.getUndoManager().enterUndoGroup(() => {

                // the deferred to keep the undo group open until it is resolved or rejected
                const undoDef = this.createDeferred();
                // the apply actions promise
                let applyPromise = null;
                // the generate operations deferred
                let generateDef = null;
                // the generated operations
                let operations = null;
                // a snapshot object
                let snapshot = null;
                // whether there is a selection range before pasting
                const hasRange = selection.hasRange();
                // the selection range before pasting
                let rangeStart = null, rangeEnd = null;
                // the promise for the deletion and generating and applying of operations
                let completePromise = null;

                // creating a snapshot
                if (hasRange) {
                    snapshot = new Snapshot(this);
                    rangeStart = _.clone(selection.getStartPosition());
                    rangeEnd = _.clone(selection.getEndPosition());
                }

                // delete current selection, but in presentation app only text selections inside shapes (has its own progress bar)
                completePromise = ((this.useSlideMode() && !this.getSelection().isAdditionalTextframeSelection()) ? $.when() : this.deleteSelected({ alreadyPasteInProgress: true, snapshot }))
                    .then(() => {

                        // create operation to replace an implicit paragraph with a real one
                        doHandleImplicitParagraph(start);

                        // creating a snapshot
                        snapshot ??= new Snapshot(this);

                        this.#docApp.getView().enterBusy({
                            warningLabel: gt('Sorry, pasting from clipboard will take some time.'),
                            cancelHandler: () => {
                                cancelled = true;
                                // restoring the old document state
                                if (snapshot && !snapshot.destroyed) { snapshot.apply(); }
                                this.#docApp.enterBlockOperationsMode(() => {
                                    if (applyPromise && applyPromise.abort) {
                                        applyPromise.abort();
                                    }
                                });
                            }
                        });

                        // generate operations

                        generateDef = this.iterateArraySliced(clipboardData, getOperationGenerationCallback());

                        // add progress handling
                        generateDef.progress(progress => {
                            // update the progress bar according to progress of the operations promise
                            this.#docApp.getView().updateBusyProgress(generationRatio * progress);
                        });

                        return generateDef;
                    })
                    .then(() => {
                        // cancel the clipboard paste if we loose the edit rigths or if the 'cancel' button is clicked.
                        if (cancelled || !this.#docApp.isEditable()) { return $.Deferred().reject(); }
                    })
                    .then(() => {
                        // the currently active target
                        const target = this.getActiveTarget();

                        // cancel the clipboard paste if we loose the edit rigths or if the 'cancel' button is clicked.
                        if (cancelled || !this.#docApp.isEditable()) { return $.Deferred().reject(); }

                        operations = styleCache.getOperations().concat(generator.getOperations());

                        // adding the information about the change tracking to the operations
                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            this.getChangeTrack().handleChangeTrackingDuringPaste(operations);
                        } else {
                            this.getChangeTrack().removeChangeTrackInfoAfterSplitInPaste(operations);
                        }

                        // handling target positions (for example inside comments)
                        if (this.#docApp.isTextApp()) { this.getCommentLayer().handlePasteOperationTarget(operations); }
                        this.getFieldManager().handlePasteOperationIDs(operations);

                        // setting property autoResizeHeight explicitely to false, if not already specified (56351)
                        if (this.#docApp.isODF() && this.#docApp.isTextApp()) { this.#checkODTTextProperties(operations); }

                        // if pasting into header or footer that are currently active, extend operations with target
                        if (target) {
                            _.each(operations, operation => {
                                if (operation.name !== Op.INSERT_STYLESHEET) {
                                    operation.target = target;
                                }
                            });
                        }

                        // shifting the positions of the new inserted drawings
                        if (isDrawingInClipboard && this.#docApp.isPresentationApp()) {
                            this.#shiftDrawingPositions(operations, drawingPasteAnchor);
                        }

                        // apply generated operationsapplyOperations
                        applyPromise = this.applyOperationsAsync(operations)
                            .progress(progress => {
                                // update the progress bar according to progress of the operations promise
                                this.#docApp.getView().updateBusyProgress(generationRatio + (1 - generationRatio) * progress);
                            });

                        return applyPromise;

                    });

                // handler for 'done', that is not triggered, if "this" is in destruction (document is closed, 42567)
                this.onFulfilled(completePromise, () => {

                    const lastOperation = _.last(operations);
                    if (lastOperation && lastOperation.name === Op.INSERT_DRAWING && lastOperation.type === 'image') {
                        this.getSelection().setTextSelection(decreaseLastIndex(end), end); // selecting a drawing, DOCS-3405
                    } else {
                        selection.setTextSelection(end);
                    }
                });

                // handler for 'always', that is not triggered, if "this" is in destruction (document is closed, 42567)
                this.onSettled(completePromise, () => {
                    if (cancelled && hasRange) { selection.setTextSelection(rangeStart, rangeEnd); }
                    // no longer blocking page break calculations (40107)
                    this.setBlockOnInsertPageBreaks(false);
                    // leaving the blocked async mode
                    this.leaveAsyncBusy();
                    // close undo group
                    undoDef.resolve();
                    // deleting the snapshot
                    if (snapshot) { snapshot.destroy(); }
                });

                return undoDef.promise();

            }); // enterUndoGroup()
        }

        /**
         * Uses a plain text string and interprets control characters, e.g.
         * \n, \t to have a formatted input for the editor.
         *
         * @param {String} plainText
         *  The plain text with optional control characters.
         *
         * @param {Number[]} [dropPosition]
         *  An optional position for the drop operation.
         */
        #insertPlainTextFormatted(plainText, dropPosition) {

            let lines, insertParagraph = false;
            const result = [];

            if (_.isString(plainText) && plainText.length > 0) {
                if (this.getSelection().hasRange() && dropPosition.start) {
                    this.getSelection().setTextSelection(dropPosition.start); // clear selection range, #43275
                }

                lines = plainText.match(/[^\r\n]+/g);

                _(lines).each(line => {
                    if (insertParagraph) {
                        result.push({ operation: Op.PARA_INSERT, depth: 0, listLevel: -1 });
                    }
                    result.push({ operation: Op.TEXT_INSERT, data: line, depth: 0 });
                    insertParagraph = true;
                });

                this.#createOperationsFromExternalClipboard(result, dropPosition);
            }
        }

        /**
         * Helper function that tells if user is going to paste slide(s), or more precisely,
         * if first operation in clipboard operations is insertSlide or insertLayoutSlide.
         *
         * @returns {Boolean}
         */
        #isSlideCopied() {

            if (!this.useSlideMode()) { return false; }

            const pasteOps = this.getClipboardOperations();
            const firstOp = pasteOps && _.first(pasteOps);

            return firstOp && (firstOp.name === 'insertSlide' || firstOp.name === 'insertLayoutSlide');
        }

        /**
         * Returns clipboard origin string, containing application name and document format combined in one string.
         *
         * @returns {String}
         *  The clipboard origin string.
         */
        #getCurrentClipboardOrigin() {
            return this.#docApp.moduleName + "/" + this.#docApp.getFileFormat();
        }

        /**
         * Generates the clipboard origin id from the document file descriptor.
         *
         * @returns {String}
         *  The clipboard origin id.
         */
        #getCurrentClipboardOriginId() {
            return _.cid(this.#docApp.getFileDescriptor());
        }
    }

    return ClipBoardHandlerMixin;
}

// exports ====================================================================

export const XClipBoardHandler = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XClipBoardHandler>}
     *  The extended class with clipboard handler functionality.
     */
    mixin: mixinClipBoardHandler
};
