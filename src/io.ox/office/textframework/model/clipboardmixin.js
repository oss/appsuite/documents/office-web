/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';
import 'unorm'; // Polyfill for String.prototype.normalize()

import { convertCssLength, convertCssLengthToHmm, resolveCssColor } from '@/io.ox/office/tk/dom';

import { opRgbColor } from '@/io.ox/office/editframework/utils/color';
import { getMimeTypeFromImageUri } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { getBase64FromImageNode, getUrlParamFromImageNode, isDocumentImageNode, isMailInlineImageNode,
    isTextSpan, getReplacementForBullet } from '@/io.ox/office/textframework/utils/dom';
import { INSERT_DRAWING, PARA_INSERT, SET_ATTRIBUTES, TAB_INSERT, TEXT_INSERT } from '@/io.ox/office/textframework/utils/operations';

// constants ==================================================================

// regExp to parse list label text for decimal numbering
const DECIMAL_RE = /^\d+[.)]/;
// regExp to parse list label text for roman numbering
const UPPER_ROMAN_RE = /^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})[.)]/;
const LOWER_ROMAN_RE = /^m{0,4}(cm|cd|d?C{0,3})(xc|xl|l?x{0,3})(ix|iv|v?i{0,3})[.)]/;
// regExp to parse list label text for alpha numbering
const UPPER_LETTER_RE = /^[A-Z]+[.)]/;
const LOWER_LETTER_RE = /^[a-z]+[.)]/;

// private functions ==========================================================

function toOpColor(cssColor) {
    const rgba = cssColor ? resolveCssColor(cssColor) : null;
    // DOCS-2975: Ignore colors with transparency completely
    return (rgba[3] === 255) ? opRgbColor(rgba.hex3) : null;
}

function getCharAttrs(childNode) {

    const styles = window.getComputedStyle(childNode.parentNode);
    const textColor = styles.getPropertyValue('color');
    const fillColor = styles.getPropertyValue('background-color');
    const fontSize = styles.getPropertyValue('font-size');
    const fontWeight = styles.getPropertyValue('font-weight');
    const fontStyle = styles.getPropertyValue('font-style');
    const textDeco = styles.getPropertyValue('text-decoration');

    return {
        color:     toOpColor(textColor),
        fillColor: toOpColor(fillColor),
        fontSize:  fontSize ? convertCssLength(fontSize, 'pt') : null,
        bold:      ((fontWeight === 'bold') || (parseInt(fontWeight, 10) > 400)) || null,
        italic:    (fontStyle === 'italic') || null,
        strike:    textDeco.includes('line-through') ? 'single' : null,
        underline: textDeco.includes('underline') || null
    };
}

function mixinClipBoard(BaseClass) {

    // mix-in class ClipBoardMixin ============================================

    /**
     * Mix-in class for the class Editor that provides extensions for
     * dealing with clipboard functionality.
     */

    class ClipBoardMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        // Text parser --------------------------------------------------------

        /**
         * Parses the pasted clipboard text.
         *
         * @param {String} text
         *
         * @returns {Array}
         *  The clipboard data array to create operations from.
         */
        parseClipboardText(text) {

            const ops = [];
            let paragraphs;

            if (_.isString(text) && (text.length > 0)) {
                // Unicode normalization
                text = text.normalize();

                paragraphs = text.split(/\r\n?|\n/);
                _.each(paragraphs, para => {
                    const splitted = para.match(/[^\t]+|\t/g);
                    // insert paragraph
                    ops.push({ operation: PARA_INSERT, listLevel: -1 });
                    // insert tab or text
                    _.each(splitted, part => {
                        if (part === '\t') {
                            ops.push({ operation: TAB_INSERT });
                        } else {
                            ops.push({ operation: TEXT_INSERT, data: part });
                        }
                    });
                });
            }

            return ops;
        }

        // HTML parser --------------------------------------------------------

        /**
         * Parses the clipboard div for pasted text content.
         *
         * @param {jQuery} clipboard
         *  The clipboard div.
         *
         * @returns {Array}
         *  The clipboard data array to create operations from.
         */
        parseClipboard(clipboard) {

            let result = [];
            const fileId = this.#docApp.getFileDescriptor().id;
            let acceptTextNodes = true;
            let skipTextNodes = false;
            // the definition off all MS list styles
            let msListDefinitions = null;

            const findTextNodes = (current, depth, listLevel, tableLevel, type) => { // TODO DOCS-4959

                // the length (of chars, tabs, drawings...) to be added to the oxo position
                let insertLength = 0;

                for (const child of current.childNodes) {

                    // determines if the node gets parsed recursively or is skipped
                    let nextLevel = true;
                    // contains the last operation
                    const lastOp = _.last(result);

                    if (child.nodeType === 3 && acceptTextNodes && !skipTextNodes) {
                        // handle non-whitespace characters and non-breaking spaces only
                        // except for our own text portion spans
                        if (isTextSpan(child.parentNode) || (/\S|\xa0/.test(child.nodeValue))) {
                            // text node content splitted by tab and text
                            const splitted = child.nodeValue.match(/[^\t]+|\t/g) || '';
                            for (let j = 0; j < splitted.length; j++) {
                                if (splitted[j] === '\t') {
                                    // --- tab ---
                                    result.push({ operation: TAB_INSERT, depth });
                                    insertLength += 1;
                                } else {
                                    // --- text ---
                                    // Unicode normalization
                                    let text = splitted[j].normalize();
                                    // replace '\r' and '\n' with space to fix pastes from aoo,
                                    // replace invisible control characters and reduce space sequences to a single spaces
                                    text = text.replace(/[\r\n]/g, ' ').replace(/[\x00-\x1f]/g, '').replace(/ +/g, ' ');
                                    insertLength += text.length;
                                    if (tableLevel >= 0) {
                                        result.push({ operation: 'insertCellData', data: text });
                                    } else {
                                        result.push({ operation: TEXT_INSERT, data: text, depth });
                                        const attrs = { character: getCharAttrs(child) };
                                        result.push({ operation: SET_ATTRIBUTES, data: { len: text.length, attrs }, depth });
                                    }
                                }
                            }
                        }

                    } else if ((child.nodeType === 8) && child.nodeValue) {
                        // <!--EndFragment--> marks the end of the content to parse
                        // special handling for Win 8, do no longer accept text nodes after an <!--EndFragment-->
                        if (child.nodeValue.toLowerCase() === 'endfragment') {
                            acceptTextNodes = false;
                        }
                        // <!--[if !supportLists]--> marks the start of the list replacement content
                        if (child.nodeValue.toLowerCase().indexOf('!supportlists') > -1) {
                            skipTextNodes = true;
                        }
                        // <!--[endif]--> marks the end of the replacement content
                        if (child.nodeValue.toLowerCase().indexOf('endif') > -1) {
                            skipTextNodes = false;
                        }

                    } else {
                        // insert paragraph for <div>, heading tags and for
                        //      <br> tags not nested inside a <div>, <p> or <li>
                        //      <p> tags not nested inside a <li>

                        if ($(child.parentNode).is('a') && ($(child).is('div') || $(child).is('span') || $(child).is('br'))) {
                            insertLength += findTextNodes(child, depth + 1, listLevel, tableLevel, type);
                            // these children in links lead to empty following paragraphs, that receive setAttributes operations
                            nextLevel = false;

                        } else if (
                            $(child).is('div, h1, h2, h3, h4, h5, h6') ||
                            (
                                ($(child).is('br') && !$(child.parentNode).is('p, div, li')) ||
                                ($(child.parentNode).hasClass('clipboard') && !$(child).is('style, pre, ol, ul, table, img') && $(child.parentNode).children(':first').is(child))
                            ) ||
                            ($(child).is('p') && !$(child.parentNode).is('li'))
                        ) {
                            if (this.isMSListParagraph(child)) {
                                // read MS list definition styles if not yet done
                                if (!msListDefinitions) {
                                    msListDefinitions = this.getMSListDefinitions(clipboard) || this.getMSFallBackListDefinitions(clipboard);
                                }

                                // ms list paragraph info on style id and list level
                                const msParagraphLevelInfo = this.getMSParagraphLevelInfo(child);
                                // a single MS list style deinition
                                const msListStyle = msListDefinitions && msParagraphLevelInfo && msParagraphLevelInfo.styleId && msListDefinitions[msParagraphLevelInfo.styleId];
                                if (msListStyle) {
                                    if (!msListStyle.inserted) {
                                        // insert MS list style definition
                                        result.push({ operation: 'insertMSList', depth, listLevel: msParagraphLevelInfo.level, data: msListStyle });
                                        msListStyle.inserted = true;
                                    }

                                    // insert paragraph and list element for current list level
                                    result.push({ operation: PARA_INSERT, depth, listLevel: msParagraphLevelInfo.level });
                                    result.push({ operation: 'insertListElement', depth, listLevel: msParagraphLevelInfo.level, type: msListStyle });

                                } else {
                                    // special handling for pasting list paragraphs without bullet or numbering
                                    result.push({ operation: 'insertListParagraph', depth, listLevel: 0 });
                                }

                            } else if (tableLevel === -1 && (!lastOp || (lastOp && lastOp.operation !== PARA_INSERT && lastOp.type !== 'leaveTable'))) {
                                result.push({ operation: PARA_INSERT, depth, listLevel });
                            }

                        } else if ($(child).is('p') && $(child.parentNode).is('li') && ($(child.parentNode).children('p').first().get(0) !== child)) {
                            // special handling for pasting lists from Word, the second <p> doesn't have a bullet or numbering
                            // <ol>
                            //   <li>
                            //     <p><span>foo</span></p>      =>   1. foo
                            //     <p><span>bar</span></p>              bar
                            //   </li>
                            // </ol>
                            result.push({ operation: 'insertListParagraph', depth, listLevel });

                        } else if ($(child).is('span')) {
                            // don't parse <span> elements with 'mso-list:Ignore' style
                            if (this.isMSListIgnore(child)) {
                                nextLevel = false;
                            }

                        } else if ($(child).is('img')) {

                            // we don't support images within tables
                            if (tableLevel === -1) {

                                // additional data of the current child node
                                let childData;

                                // the img alt tag optionally stores the document media URL, the session id and the file id
                                try {
                                    childData = JSON.parse($(child).attr('alt'));
                                } catch {
                                    childData = {};
                                }
                                if (ox.session === childData.sessionId && fileId === childData.fileId && childData.altsrc) {
                                    // The image URL points to a document and the copy&paste action
                                    // takes place inside the same editor instance, so we use relative document media URL.
                                    result.push({ operation: INSERT_DRAWING, type: 'image', data: childData.altsrc, depth });

                                } else if (isDocumentImageNode(child)) {
                                    // The image URL points to a document, but copy&paste is done from one editor instance to another
                                    // and the image src has not been replaced with a base64 data URL (e.g. IE 9 does not support this).
                                    // So the base64 data URL can be generated from the image node as long as the session is valid.
                                    result.push({
                                        operation: INSERT_DRAWING,
                                        type: 'image',
                                        data: getBase64FromImageNode(child, getMimeTypeFromImageUri(getUrlParamFromImageNode(child, 'get_filename'))),
                                        depth
                                    });

                                } else if (isMailInlineImageNode(child)) {
                                    // The image URL points to a mail inline image. Replace the url with a base64 data URL.
                                    result.push({
                                        operation: INSERT_DRAWING,
                                        type: 'image',
                                        data: getBase64FromImageNode(child),
                                        depth
                                    });

                                } else {
                                    // The image src is an external web URL or already a base64 data URL
                                    result.push({ operation: INSERT_DRAWING, type: 'image', data: child.src, depth });

                                }
                                insertLength += 1;
                            } else {
                                // placeholder for removed table
                                result.push({ operation: 'insertCellData', data: ' ' });
                            }

                        } else if ($(child).is('style, title')) {
                            // don't parse <style> and <title> elements
                            nextLevel = false;

                        }  else if ($(child).is('a')) {
                            if ($(child).children('img').length === 0) {
                                // before we can add the operation for inserting the hyperlink we need to add the insertText operation
                                // so we first parse the next recursion level
                                const len = findTextNodes(child, depth + 1, listLevel, tableLevel, type);
                                // and then add the hyperlink
                                result.push({ operation: 'insertHyperlink', data: child.href, length: len, depth });
                            } else {
                                insertLength += findTextNodes(child, depth + 1, listLevel, tableLevel, type);
                            }

                            // we already traversed the tree, so don't do it again
                            nextLevel = false;

                        } else if ($(child).is('ol, ul')) {
                            // for list root level create a new array, otherwise add to current
                            if (listLevel === -1) { type = []; }
                            // add current list level type
                            type[listLevel + 1] = $(child).css('list-style-type') || $(child).attr('type') || ($(child).is('ol') ? 'decimal' : 'disc');
                            // insert list entry for current list level
                            result.push({ operation: 'insertList', depth, listLevel, type });
                            // look for deeper list levels
                            insertLength += findTextNodes(child, depth + 1, listLevel + 1, tableLevel, type);
                            // we already traversed the tree, so don't do it again
                            nextLevel = false;

                        } else if ($(child).is('li')) {
                            // insert paragraph and list element for current list level
                            result.push({ operation: PARA_INSERT, depth, listLevel });
                            result.push({ operation: 'insertListElement', depth, listLevel, type });

                        } else if ($(child).is('table')) {
                            if (tableLevel === -1) {
                                if ($(child).find('td').length > 1) {
                                    const size = this.#calcTableSize(child);
                                    const caption = $(child).find('caption').text();

                                    result.push({ operation: 'insertTable', size, caption });
                                    insertLength += findTextNodes(child, depth + 1, listLevel, tableLevel + 1, type);
                                    nextLevel = false;
                                    result.push({ operation: PARA_INSERT, type: 'leaveTable' });
                                }
                            } else {
                                // placeholder for removed table
                                result.push({ operation: 'insertCellData', data: ' ' });
                                // stop traversing nested tables!
                                nextLevel = false;
                            }

                        } else if ($(child).is('tr')) {
                            if (tableLevel !== -1) {
                                result.push({ operation: 'insertRow' });
                            } else {
                                //split para
                                result.push({ operation: PARA_INSERT, depth });
                            }

                        } else if ($(child).is('td') || $(child).is('th')) {
                            if (tableLevel !== -1) {
                                result.push({ operation: 'prepareCell', depth });
                                insertLength += findTextNodes(child, depth + 1, listLevel, tableLevel, type);
                                result.push({ operation: 'finalizeCell', depth });
                                nextLevel = false;

                            } else {
                                //insert tab
                                if ($(child).is('td:not(:first-of-type)')) {
                                    if (lastOp && lastOp.operation === TEXT_INSERT) {
                                        lastOp.data = lastOp.data.replace(/^\s*/, '');
                                    }
                                    result.push({ operation: TAB_INSERT, depth });
                                }
                            }
                        } else if ($(child).is('caption')) {
                            if (tableLevel !== -1) {
                                // stop traversing table captions, they are handled within insert table
                                nextLevel = false;
                            }
                        } else if ($(child).is('pre')) {
                            // parse preformatted text
                            const textOps = this.parseClipboardText($(child).text());
                            result = result.concat(textOps);
                            // all deeper levels already handled, don't traverse the tree
                            nextLevel = false;
                        }

                        if (nextLevel) {
                            insertLength += findTextNodes(child, depth + 1, listLevel, tableLevel, type);
                        }
                    }
                }

                return insertLength;

            };

            findTextNodes(/*node*/ clipboard.get(0), /*depth*/ 0, /*listLevel*/ -1, /*tableLevel*/ -1, /*type*/ undefined);

            // console.warn('paste result', result);
            return result;
        }

        // MS Word list pasting -----------------------------------------------

        /**
         * Returns true if the origin of the given HTML data is MS Word.
         *
         * @param {Node|jQuery} html
         *  The HTML data to check.
         *
         * @returns {Boolean}
         */
        isMSWordHtml(html) {
            return (($(html).find('meta[content*=Word]').length > 0) || ($(html).filter('meta[content*=Word]').length > 0));
        }

        /**
         * Returns true if the paragraph element is a MS Word list paragraph.
         *
         * @param {Node|jQuery} paragraph
         *  The HTML list paragraph.
         *
         * @returns {Boolean}
         */
        isMSListParagraph(paragraph) {
            return ($(paragraph).is('.MsoListParagraphCxSpFirst, .MsoListParagraphCxSpMiddle, .MsoListParagraphCxSpLast'));
        }

        /**
         * Returns true if the HTML element is a MS Office list replacement.
         * We look for 'mso-list:Ignore' style first, but since some browsers remove it,
         * we also check if the previous node is the <!--[if !supportLists]--> comment node.
         *
         * @param {Node|jQuery} element
         *  The HTML element.
         *
         * @returns {Boolean}
         */
        isMSListIgnore(element) {

            const node = (element instanceof $) ? element[0] : element;
            const style = node && element.getAttribute('style');
            if (style && style.indexOf('mso-list:Ignore') > -1) {
                return true;
            }

            const prevNodeValue = node && node.previousSibling && (node.previousSibling.nodeType === 8) && node.previousSibling.nodeValue;
            if (prevNodeValue && prevNodeValue.indexOf('!supportLists') > -1) {
                return true;
            }

            return false;
        }

        /**
         * Parses the given MS Word list paragraph for the list style id and the list level.
         *
         * @param {Node|jQuery} paragraph
         *  The MS Word list paragraph.
         *
         * @returns {Object|null}
         *              {String} styleId
         *                  The list style id
         *              {Number} level
         *                  The zero based list level
         *              {Number} lfo
         *                  The MS list formatting attribute
         */
        getMSParagraphLevelInfo(paragraph) {

            const originalRegEx = /mso-list:(l\d+)\s+level(\d+)\s+lfo(\d+)/i;
            const sanitizeRegEx = /content:\s*"(l\d+)-level(\d+)-lfo(\d+)"/i;
            const style = $(paragraph).attr('style');
            let parts;

            // first check for the sanitized mso-list / content attribute
            parts = sanitizeRegEx.exec(style);
            // then check for the original mso-list attribute
            if (!_.isArray(parts) || !parts[1] || !parts[2]) {
                parts = originalRegEx.exec(style);
            }
            if (!_.isArray(parts) || !parts[1] || !parts[2]) { return null; }

            return { styleId: parts[1].toLowerCase(), level: this.#listLevelStringToZeroBasedLevel(parts[2]), lfo: parseInt(parts[3], 10) };
        }

        /**
         * Parses the given HTML for MS Word list style definitions
         * and returns them as object structure with the style id as the first
         * and the list level as the second object level.
         *
         * @param {Node|jQuery} html
         *  The HTML data to parse.
         *
         * @returns {Object}
         *              {Object} styleId
         *                  {Object} level
         *                      {String} numberFormat
         *                          The MS list level number format.
         *                      {String} levelText
         *                          The text for the list level.
         *                      {String} fontFamily
         *                          The level text font.
         *                      {Number} indentFirstLine
         *                          The first line text indent in 1/100 of millimeters.
         *                      {Number} indentLeft
         *                          The left indent in 1/100 of millimeters.
         *                      {String} numberPosition
         *                          The number position.
         */
        getMSListDefinitions(html) {

            const stylesText = $(html).find('style').text();
            const result = {};
            // parse list styles out of all styles
            const listStyles = stylesText.match(/@list\s+l\d+:level\d+\s*\{[^}]*\}/ig);
            if (!_.isArray(listStyles)) { return null; }

            _.each(listStyles, item => {

                // parse style id and list level
                let parts = item.match(/@list\s+(l\d+):level(\d+)/i);
                if (_.isArray(parts) && parts[1] && parts[2]) {

                    const styleId = parts[1].toLowerCase();
                    const listLevel = this.#listLevelStringToZeroBasedLevel(parts[2]);

                    if (!result[styleId]) {
                        result[styleId] = {};
                    }
                    if (!result[styleId][listLevel]) {
                        result[styleId][listLevel] = {};
                    }

                    // parse number format, default to 'decimal'
                    parts = item.match(/mso-level-number-format:\s*(\S+);/i);
                    const numberFormat = _.isArray(parts) && parts[1];

                    // parse level text, or generate it from number format and list level
                    parts = item.match(/mso-level-text:(\S+.*);/i);
                    const levelText = _.isArray(parts) && parts[1];

                    // parse font family
                    parts = item.match(/font-family:\s*(\S+.*\S+);/i);
                    const fontFamily = _.isArray(parts) && parts[1];

                    parts = this.createMSNumberFormatAndLevelText(numberFormat, listLevel, levelText, fontFamily);
                    result[styleId][listLevel].numberFormat = parts.numberFormat;
                    result[styleId][listLevel].levelText = parts.levelText;
                    if (parts.fontFamily) { result[styleId][listLevel].fontFamily = parts.fontFamily; }

                    // parse text indent and convert to 1/100 millimeters
                    parts = item.match(/text-indent:\s*(\S+);/i);
                    if (_.isArray(parts) && parts[1]) {
                        result[styleId][listLevel].indentFirstLine = convertCssLengthToHmm(parts[1]);
                    }

                    // parse margin left and convert to 1/100 millimeters
                    parts = item.match(/margin-left:\s*(\S+);/i);
                    if (_.isArray(parts) && parts[1]) {
                        result[styleId][listLevel].indentLeft = convertCssLengthToHmm(parts[1]);
                    }

                    // parse number position
                    parts = item.match(/mso-level-number-position:\s*(\S+);/i);
                    if (_.isArray(parts) && parts[1]) {
                        result[styleId][listLevel].numberPosition = parts[1];
                    }
                }

            });

            return result;
        }

        /**
         * Parses the given HTML for MS Word list paragraphs and generates
         * fall back style definitions out of the list replacements.
         * Returns them as object structure with the style id as the first
         * and the list level as the second object level.
         *
         * @param {Node|jQuery} html
         *  The HTML data to parse.
         *
         * @returns {Object}
         *              {Object} styleId
         *                  {Object} level
         *                      {String} numberFormat
         *                          The MS list level number format.
         *                      {String} levelText
         *                          The text for the list level.
         *                      {String} fontFamily
         *                          The level text font.
         *                      {Number} indentFirstLine
         *                          The first line text indent in 1/100 of millimeters.
         *                      {Number} indentLeft
         *                          The left indent in 1/100 of millimeters.
         */
        getMSFallBackListDefinitions(html) {

            const paragraphs = $(html).find('p');
            const result = {};

            const hasListDefinition = (styles, styleId, listLevel) => {
                if (!styles) { return false; }
                if (!styles[styleId]) { return false; }
                if (!styles[styleId][listLevel]) { return false; }
                if (!_.isString(styles[styleId][listLevel].levelText)) { return false; }
                if (!_.isString(styles[styleId][listLevel].numberFormat)) { return false; }
                return true;
            };

            _.each(paragraphs, p => {

                if (this.isMSListParagraph(p)) {

                    const msParagraphLevelInfo = this.getMSParagraphLevelInfo(p);

                    if (msParagraphLevelInfo && _.isString(msParagraphLevelInfo.styleId) && _.isNumber(msParagraphLevelInfo.level)) {

                        const styleId = msParagraphLevelInfo.styleId;
                        const listLevel = msParagraphLevelInfo.level;

                        if (!hasListDefinition(result, styleId, listLevel)) {

                            let text;
                            let levelText;

                            // get alt text from image bullet, or level text from bullet / numbering
                            const image = $(p).find('img');
                            if (image.length > 0) {
                                text = image.attr('alt') || '\u2022';
                            } else {
                                text = $(p).text().match(/\s*(\S*)\s+(\S*)/);
                                text = (_.isArray(text) && _.isString(text[1])) ? text[1] : '';
                            }

                            const numberFormat = this.getNumberFormatFromListLabelText(text);
                            const fontFamily = $(p).children().first().css('font-family');

                            if (!result[styleId]) {
                                result[styleId] = {};
                            }
                            if (!result[styleId][listLevel]) {
                                result[styleId][listLevel] = {};
                            }

                            switch (numberFormat) {

                                case 'decimal':
                                case 'lowerRoman':
                                case 'upperRoman':
                                case 'upperLetter':
                                case 'lowerLetter':
                                    levelText = '%' + (listLevel + 1) + '.';
                                    break;

                                case 'bullet':
                                    levelText = text;
                                    break;

                                default:
                                    levelText = text;
                            }

                            const parts = this.createMSNumberFormatAndLevelText(numberFormat, listLevel, levelText, fontFamily);
                            result[styleId][listLevel].numberFormat = parts.numberFormat;
                            result[styleId][listLevel].levelText = parts.levelText;
                            if (parts.fontFamily) {
                                result[styleId][listLevel].fontFamily = parts.fontFamily;
                            }
                        }
                    }
                }
            });

            return result;
        }

        /**
         * Creates the MS list level number format from the CSS number format and
         * creates a level text if none is given.
         *
         * @param {String} numberFormat
         *  The CSS number format String.
         *
         * @param {Number} listLevel
         *  The zero based list level.
         *
         * @param {String} [levelText]
         *  The level text to use instead of a generated one.
         *
         * @param {String} [fontFamily]
         *  The font family for mapping symbols to Unicode bullets.
         *
         * @returns {Object}
         *              {String} numberFormat
         *                  The MS list level number format String.
         *              {String} levelText
         *                  The level text that was given or that has been generated.
         */
        createMSNumberFormatAndLevelText(numberFormat, listLevel, levelText, fontFamily) {

            const result = {};

            numberFormat =  _.isString(numberFormat) ? numberFormat.toLowerCase() : 'decimal';
            levelText = this.#cleanLevelText(levelText);

            switch (numberFormat) {

                case 'decimal':
                case '1':
                    result.numberFormat = 'decimal';
                    result.levelText = _.isString(levelText) ? levelText : '%' + (listLevel + 1) + '.';
                    break;

                case 'decimal-leading-zero':
                    result.numberFormat = 'decimal';
                    result.levelText = _.isString(levelText) ? levelText : '0%' + (listLevel + 1) + '.';
                    break;

                case 'A':
                case 'upper-alpha':
                case 'alpha-upper':
                case 'upper-latin':
                case 'upperletter':
                    result.numberFormat = 'upperLetter';
                    result.levelText = _.isString(levelText) ? levelText : '%' + (listLevel + 1) + '.';
                    break;

                case 'a':
                case 'lower-alpha':
                case 'alpha-lower':
                case 'lower-latin':
                case 'lowerletter':
                    result.numberFormat = 'lowerLetter';
                    result.levelText = _.isString(levelText) ? levelText : '%' + (listLevel + 1) + '.';
                    break;

                case 'I':
                case 'upper-roman':
                case 'roman-upper':
                case 'upperroman':
                    result.numberFormat = 'upperRoman';
                    result.levelText = _.isString(levelText) ? levelText : '%' + (listLevel + 1) + '.';
                    break;

                case 'i':
                case 'lower-roman':
                case 'roman-lower':
                case 'lowerroman':
                    result.numberFormat = 'lowerRoman';
                    result.levelText = _.isString(levelText) ? levelText : '%' + (listLevel + 1) + '.';
                    break;

                case 'disc':
                case 'circle':
                case 'square':
                case 'bullet':
                    result.numberFormat = 'bullet';
                    if (_.isString(fontFamily)) {
                        const replacement = getReplacementForBullet(fontFamily, levelText);
                        if (replacement) {
                            result.levelText = replacement.char;
                            result.fontFamily = replacement.font;
                        }

                    } else if (_.isString(levelText)) {
                        result.levelText = levelText;

                    } else {
                        switch (numberFormat) {
                            case 'disc':
                            case 'bullet':
                                result.levelText = '\uf0b7';
                                break;
                            case 'circle':
                                result.levelText = '\u25CB';
                                break;
                            case 'square':
                                result.levelText = '\u25A0';
                                break;

                            default:
                                result.levelText = '\uf0b7';
                        }
                    }
                    break;

                case 'none':
                    result.numberFormat = 'bullet';
                    result.levelText = _.isString(levelText) ? levelText : ' ';
                    break;

                case 'image':
                    result.numberFormat = 'bullet';
                    result.levelText = '\u2022';
                    break;

                default:
                    result.numberFormat = 'decimal';
                    result.levelText = _.isString(levelText) ? levelText : '%' + (listLevel + 1) + '.';
            }

            return result;
        }

        /**
         * Returns the number format for the given list label.
         *
         * @param {String} text
         *  The list label.
         *
         * @returns {String}
         *  The number format.
         */
        getNumberFormatFromListLabelText(text) {

            if (text.length > 0) {

                if (DECIMAL_RE.test(text)) {
                    return 'decimal';
                }
                if (UPPER_ROMAN_RE.test(text)) {
                    return 'upperRoman';
                }
                if (LOWER_ROMAN_RE.test(text)) {
                    return 'lowerRoman';
                }
                if (UPPER_LETTER_RE.test(text)) {
                    return 'upperLetter';
                }
                if (LOWER_LETTER_RE.test(text)) {
                    return 'lowerLetter';
                }
            }

            return 'bullet';
        }

        // private methods ----------------------------------------------------

        /**
         * Converts a list level String to a zero based number.
         *
         * @param {String} levelString
         *  The list level as one based String
         *
         * @returns {Number}
         *  The list level as zero based Number
         */
        #listLevelStringToZeroBasedLevel(levelString) {

            let level = parseInt(levelString, 10);
            level = (level > 0) ? (level - 1) : 0;

            return level;
        }

        /**
         * Removes ' and \ chars from given String.
         *
         * @param {String} text
         *  The String to clean.
         *
         * @returns {String|null}
         *  The cleaned String or null.
         */
        #cleanLevelText(text) {
            if (!_.isString(text)) { return null; }
            return text.replace(/["\\]/g, '');
        }

        #calcTableSize(child) {
            const rows = $(child).find('tr');
            const rowCount = rows.length;
            const whileCount = (rowCount > 50) ? 50 : rowCount;
            let size = 0;
            let i = 0;

            while (i < whileCount) {
                const row = $(rows[i]);
                let cols = row.find('>td,>th').length;
                const colSpan = $(row).attr('colspan') - 1;

                if (colSpan >= 1) { cols += colSpan; }
                if (cols > size) { size = cols; }
                i++;
            }

            return size;
        }
    }
    return ClipBoardMixin;
}

// exports ====================================================================

export const XClipBoard = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XClipBoard>}
     *  The extended class with clipboard functionality.
     */
    mixin: mixinClipBoard
};
