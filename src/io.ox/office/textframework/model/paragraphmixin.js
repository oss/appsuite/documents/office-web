/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { containsNode } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';

import { CANVAS_NODE_SELECTOR, getClosestTextFrameDrawingNode } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { CONTENT_NODE_SELECTOR, DRAWING_SPACEMAKER_NODE_SELECTOR, LIST_LABEL_NODE_SELECTOR, PARAGRAPH_NODE_LIST_EMPTY_CLASS,
    PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS, containsFloatingDrawingNode, createParagraphNode, ensureExistingTextNode,
    hasAbsoluteParagraphDrawing, hasCommentPlaceHolderNode, hasComplexFieldNode, hasDrawingPlaceHolderNode, hasRangeMarkerNode,
    hasSimpleFieldNode, isDrawingFrame, isDummyTextNode, isEmptySpan, isImplicitParagraphNode, isListLabelNode,
    isParagraphNode, isSlideNode, isTextFrameTemplateTextParagraph, isTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { DELETE, PARA_INSERT, PARA_MERGE, PARA_SPLIT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { appendNewIndex, getDOMPosition, getLeadingAbsoluteDrawingCount, getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount, getParagraphElement,
    getParagraphNodeLength, getParagraphLength, increaseLastIndex, removeUnusedDrawingOffsetNodes } from '@/io.ox/office/textframework/utils/position';
import { getCharacterAttributesFromEmptyTextSpan, getDomNode, handleParagraphIndex, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';
import SpellChecker from '@/io.ox/office/textframework/components/spellcheck/spellchecker';

function mixinParagraph(BaseClass) {

    // mix-in class ParagraphMixin ============================================

    /**
     * A mix-in class for the paragraph handling in the document model.
     */

    class ParagraphMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * After splitting a paragraph with place holders in the new paragraph (that
         * was cloned before), it is necessary to update the place holder nodes in the
         * models. Also range marker and complex field models need to be updated.
         *
         * @param {Node|jQuery} para
         *  The paragraph node.
         */
        updateAllModels(para) {

            // whether the existence of the components in the specified node shall be checked
            const noCheck = !isParagraphNode(para);

            // also update the links of the drawings in the drawing layer to their placeholder
            if (noCheck || hasDrawingPlaceHolderNode(para)) { this.getDrawingLayer().repairLinksToPageContent(para); }
            // also update the links of the comments in the comment layer to their placeholder
            if (this.#docApp.isTextApp() && (noCheck || hasCommentPlaceHolderNode(para))) { this.getCommentLayer().repairLinksToPageContent(para); }
            // also update the collected range markers
            if (noCheck || hasRangeMarkerNode(para)) { this.getRangeMarker().updateRangeMarkerCollector(para); }
            // also update the collected complex fields
            if (noCheck || hasComplexFieldNode(para)) { this.getFieldManager().updateComplexFieldCollector(para); }
            // also update the collected simple fields
            if (noCheck || hasSimpleFieldNode(para)) { this.getFieldManager().updateSimpleFieldCollector(para); }
        }

        /**
         * Inserting a paragraph into the document.
         *
         * @param {Number[]} start
         *  The logical position of the paragraph to be inserted.
         */
        insertParagraph(start) {

            // the attributes of the inserted paragraph
            const attrs = {};
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // created operation
            let newOperation = null;
            // the change track object
            const changeTrack = this.getChangeTrack();

            // handling change tracking
            if (changeTrack.isActiveChangeTracking()) { attrs.changes = { inserted: changeTrack.getChangeTrackInfo() }; }

            newOperation = { name: PARA_INSERT, start: _.clone(start), attrs };
            this.extendPropertiesWithTarget(newOperation, target);

            // applying operation
            this.applyOperations(newOperation);
        }

        /**
         * Creating the operation for splitting a paragraph. A check is necessary, if the paragraph
         * contains leading floated drawings. In this case it might be necessary to update the
         * specified split position. For example the specified split position is [0,1]. If there
         * is a floated drawing at position [0,0], it is necessary to split the paragraph at
         * the position [0,0], so that the floated drawing is also moved into the following
         * paragraph.
         *
         * @param {Number[]} position
         *  The logical split position.
         *
         * @param {Node|jQuery|Null} [para]
         *  The paragraph that will be splitted. If this object is a jQuery collection, uses
         *  the first DOM node it contains. If missing, it will be determined from the specified
         *  logical position. This parameter can be used for performance reasons.
         */
        splitParagraph(position, para) {

            // currently active root node
            const activeRootNode = this.getCurrentRootNode();
            // the operations generator
            const generator = this.createOperationGenerator();
            // the paragraph node, if not specified as parameter
            const paragraph = para || getParagraphElement(activeRootNode, position.slice(0, -1));
            // the last position of the specified logical split position
            const offset = _.last(position);
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // the explicit paragraph attributes
            let expAttrs = null;
            // the generated operations
            let operationOptions = {};
            // optionally existing preselected attributes
            const preselectedAttrs = this.getPreselectedAttributes();
            // the logical paragraph position
            let paraPos = null;

            // moving the leading absolute drawings to the new paragraph
            if (hasAbsoluteParagraphDrawing(paragraph) && offset > 0 && offset <= getLeadingAbsoluteDrawingCount(paragraph)) {
                position[position.length - 1] = 0;
            }

            operationOptions = { start: _.clone(position) };
            this.extendPropertiesWithTarget(operationOptions, target);

            generator.generateOperation(PARA_SPLIT, _.copy(operationOptions, true));

            // the second paragraph must not have pageBreakBefore attribute set to true (53110)
            if (this.#docApp.isODF()) {
                expAttrs = getExplicitAttributes(paragraph, 'paragraph');
                if (expAttrs && expAttrs.pageBreakBefore) {
                    operationOptions = { start: increaseLastIndex(_.initial(position)), attrs: { paragraph: { pageBreakBefore: null } } }; // removing the pageBreakBefore attribute
                    this.extendPropertiesWithTarget(operationOptions, target);
                    generator.generateOperation(SET_ATTRIBUTES, _.copy(operationOptions, true));
                }
            }

            // taking care of preselected attributes by storing them at the following paragraph (DOCS-3230)
            if (preselectedAttrs) {
                paraPos = _.initial(position);
                operationOptions = { start: increaseLastIndex(paraPos), attrs: preselectedAttrs };
                this.extendPropertiesWithTarget(operationOptions, target);
                generator.generateOperation(SET_ATTRIBUTES, _.copy(operationOptions, true));
            }

            this.applyOperations(generator);
        }

        /**
         * Creating the operation for merging two paragraphs.
         *
         * @param {Number[]} position
         *  The logical merge position.
         *
         * @param {Number} len
         *  The length of the paragraph.
         */
        mergeParagraph(position, len) {

            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            const operationOptions = { name: PARA_MERGE, start: _.clone(position), paralength: (len ? len : 0) };

            this.extendPropertiesWithTarget(operationOptions, target);
            this.applyOperations(operationOptions);
        }

        // operation handler --------------------------------------------------

        /**
         * The handler for the splitParagraph operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the paragraph was splitted successfully.
         */
        splitParagraphHandler(operation, external) {

            if (!external && this.undoManager.isUndoEnabled()) {

                const paragraphPos = _.initial(operation.start);
                const undoOperation = { name: PARA_MERGE, start: _.copy(paragraphPos), paralength: _.last(operation.start) };

                this.extendPropertiesWithTarget(undoOperation, operation.target);
                this.undoManager.addUndo(undoOperation, operation);
            }
            return this.#implSplitParagraph(operation.start, operation.target, external);
        }

        /**
         * The handler for the mergeParagraph operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the paragraphs were merged successfully.
         */
        mergeParagraphHandler(operation, external) {

            // container root node of paragraph
            const rootNode = this.getRootNode(operation.target);
            // the paragraph that will be merged with its next sibling
            const paragraphInfo = getDOMPosition(rootNode, operation.start, true);
            // current and next paragraph, as jQuery objects
            let thisParagraph = null, nextParagraph = null;
            // text position at end of current paragraph, logical position of next paragraph
            let paraEndPosition = null, nextParaPosition = null;
            // first child node of next paragraph
            let firstChildNode = null;
            // the undo/redo operations
            const generator = this.undoManager.isUndoEnabled() ? this.createOperationGenerator() : null;
            // the character attributes
            let characterAttributes = null;
            // the list label elements at the following paragraph
            let listLabelInNextParagraph = null;
            // undo operation for passed operation
            let undoOperation = {};

            // get current paragraph
            if (!paragraphInfo || !isParagraphNode(paragraphInfo.node)) {
                globalLogger.warn('Editor.mergeParagraph(): no paragraph found at position ' + JSON.stringify(operation.start));
                return false;
            }
            thisParagraph = $(paragraphInfo.node);

            // start node from where to run page break calculations
            const currentElement = thisParagraph;
            paraEndPosition = appendNewIndex(operation.start, getParagraphLength(rootNode, operation.start));

            // get next paragraph
            nextParagraph = thisParagraph.next();

            // but make sure next paragraph is only valid node, and not pagebreak for example
            while (nextParagraph.length > 0 && !nextParagraph.is(CONTENT_NODE_SELECTOR)) {
                nextParagraph = nextParagraph.next();
            }

            if (!isParagraphNode(nextParagraph)) {
                globalLogger.warn('Editor.mergeParagraph(): no paragraph found after position ' + JSON.stringify(operation.start));
                return false;  // forcing an error, if there is no following paragraph
            }
            nextParaPosition = increaseLastIndex(operation.start);

            // generate undo/redo operations
            if (generator && !external) {
                undoOperation = { start: _.copy(paraEndPosition), target: operation.target };
                this.extendPropertiesWithTarget(undoOperation, operation.target);
                generator.generateOperation(PARA_SPLIT, undoOperation);

                // restoring character attributes at the empty paragraphs again
                if (!this.#docApp.isODF()) {
                    characterAttributes = getCharacterAttributesFromEmptyTextSpan(nextParagraph);
                    if (characterAttributes && !_.isEmpty(characterAttributes)) { this.paragraphStyles.setElementAttributes(nextParagraph, { character: characterAttributes }); }
                }

                // Info: This Operation is multiplied in repeating undo-redo process
                undoOperation = { start: _.copy(nextParaPosition) };
                this.extendPropertiesWithTarget(undoOperation, operation.target);
                generator.generateSetAttributesOperation(nextParagraph, undoOperation, { clearFamily: 'paragraph' });
                this.undoManager.addUndo(generator.getOperations(), operation);
            }

            // remove dummy text node from current paragraph
            if (isDummyTextNode(thisParagraph[0].lastChild)) {
                $(thisParagraph[0].lastChild).remove();
            }

            // remove list label node from next paragraph (taking care of list update (35447)
            listLabelInNextParagraph = nextParagraph.children(LIST_LABEL_NODE_SELECTOR);
            if (listLabelInNextParagraph.length > 0) {
                listLabelInNextParagraph.remove();
                if (this.handleTriggeringListUpdate(nextParagraph, { checkSplitInNumberedList: true })) {
                    nextParagraph.data('splitInNumberedList', 'true'); // handling a merge like a split for update of lists
                }
            }

            // append all children of the next paragraph to the current paragraph, delete the next paragraph
            firstChildNode = nextParagraph[0].firstChild;
            thisParagraph.append(nextParagraph.children());
            nextParagraph.remove();

            // when merging par that contains MS hardbreak with type page, mark merged paragraph
            if (thisParagraph.find('.ms-hardbreak-page').length) {
                thisParagraph.addClass('manual-page-break contains-pagebreak');
            }

            // remove one of the sibling text spans at the concatenation point,
            // if one is empty; otherwise try to merge equally formatted text spans
            if (isTextSpan(firstChildNode) && isTextSpan(firstChildNode.previousSibling)) {
                if (isEmptySpan(firstChildNode)) {
                    $(firstChildNode).remove();
                } else if (isEmptySpan(firstChildNode.previousSibling)) {
                    $(firstChildNode.previousSibling).remove();
                } else {
                    mergeSiblingTextSpans(firstChildNode);
                }
            }

            // refresh DOM
            if (this.isImportFinished()) {
                this.implParagraphChangedSync(thisParagraph); // increasing stability, for example testrunner TEST_18
            } else {
                this.implParagraphChanged(thisParagraph);
            }

            // checking the z-index of paragraph
            if (this.#docApp.isTextApp() && this.#docApp.getModel().isIncreasedDocContent()) { handleParagraphIndex(thisParagraph); }

            // checking paragraph attributes for list styles and updating lists, if required
            if (this.handleTriggeringListUpdate(thisParagraph, { checkSplitInNumberedList: true })) {
                thisParagraph.data('splitInNumberedList', 'true'); // handling a merge like a split for update of lists
            }

            // new cursor position at merge position
            this.setLastOperationEnd(_.clone(paraEndPosition));

            if ($(currentElement).data('lineBreaksData')) {
                $(currentElement).removeData('lineBreaksData'); //we changed paragraph layout, cached line breaks data needs to be invalidated
            }
            //render pagebreaks after merge
            this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(thisParagraph));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            // only for text, there are no paragraph borders and color in presentation
            if (!this.useSlideMode()) {
                // only needed for updating paragraphs that have a color or a border
                this.paragraphStyles.updateParagraphBorderAndColorNodes(thisParagraph.next());
            }

            return true;
        }

        /**
         * The handler for the insertParagraph operation.
         *
         * @param {OperationContext} context
         *  The context object wrapping the document operation.
         *
         * @throws {OperationError}
         *  If the paragraph could not be inserted successfully.
         */
        insertParagraphHandler(context) {

            // the new paragraph
            const paragraph = createParagraphNode();
            // the insertion position of the new paragraph
            const paraStart = context.getPos('start');
            // the target root node
            const target = context.optStr('target');
            // the attributes of the operation
            let attrs = context.optDict('attrs');
            // an attribute helper object
            let modifiedAttrs = null;
            // the undo operation
            let undoOperation = null;
            // whether an undo operation shall be created (not doing so for implicit paragraphs)
            const noUndo = context.optBool('noUndo');

            // checking valid paragraph operation for presentation app (48608)
            if (this.useSlideMode()) { context.ensure(paraStart.length >= 3, 'invalid start position'); }

            // insert the new paragraph into the root node addressed by the operation
            context.ensure(this.insertContentNode(_.clone(paraStart), paragraph, target), 'cannot create new paragraph');

            // insert required helper nodes
            this.validateParagraphNode(paragraph);

            // removing a following implicit paragraph (for example in a table cell)
            // -> exchanging an implicit paragraph with a non-implicit paragraph
            if (isImplicitParagraphNode(paragraph.next())) {
                paragraph.next().remove();
            }

            // generate undo/redo operations
            if (!context.external && !noUndo && this.undoManager.isUndoEnabled()) {
                undoOperation = { name: DELETE, start: _.copy(paraStart) };
                this.extendPropertiesWithTarget(undoOperation, target);
                this.undoManager.addUndo(undoOperation, context);
            }

            // apply the passed paragraph attributes
            if (attrs) {

                // No list style in comments in odt: 38829 (simply removing the list style id)
                if (this.#docApp.isODF() && this.#docApp.isTextApp() && target && attrs.paragraph && attrs.paragraph.listStyleId && this.getCommentLayer().isCommentId(target)) {
                    attrs = _.clone(attrs);
                    attrs.paragraph = _.clone(attrs.paragraph);
                    delete attrs.paragraph.listStyleId;
                }

                if (this.#docApp.isODF() || this.hasOoxmlTextCharacterFilterSupport()) { // in docx files the character attributes must be assigned to paragraphs (DOCS-4211)
                    if (!_.isEmpty(attrs)) { this.paragraphStyles.setElementAttributes(paragraph, attrs); }
                } else {

                    // assigning character attributes to text spans, not to paragraphs (41250)
                    if ('character' in attrs) {

                        modifiedAttrs = _.copy(attrs, true);  // not modifying the original object

                        // saving the character attributes at the paragraph node, so that they can be used in clearAttributes operation
                        paragraph.data('origCharAttrs', _.copy(attrs.character, true));

                        // setting the character attributes to the text span inside the paragraph
                        _.each(paragraph.children('span'), node => {
                            if (isTextSpan(node)) { this.characterStyles.setElementAttributes(node, attrs); }
                        });

                        // removing the character attributes, so that they are not assigned to the paragraph
                        delete modifiedAttrs.character;

                        if (!_.isEmpty(modifiedAttrs)) {
                            this.paragraphStyles.setElementAttributes(paragraph, modifiedAttrs);
                        }
                    }

                    // saving attributes at paragraph node
                    if (!modifiedAttrs && !_.isEmpty(attrs)) {
                        this.paragraphStyles.setElementAttributes(paragraph, attrs);
                    }
                }
            }

            // set cursor to beginning of the new paragraph
            // But not, if this was triggered by an insertColumn or insertRow operation (Task 30859)
            if (!this.isGUITriggeredOperation()) {
                this.setLastOperationEnd(appendNewIndex(paraStart));
            }

            // register paragraph for deferred formatting, especially empty paragraphs
            // or update them immediately after the document import has finished (Task 28370)
            if (this.isImportFinished()) {
                this.implParagraphChangedSync(paragraph);

                // only for text, there are no paragraph borders and color in presentation
                if (!this.useSlideMode()) {
                    // only needed for updating paragraphs that have a color or a border
                    this.paragraphStyles.updateParagraphBorderAndColorNodes(paragraph.next());
                }

            } else {
                this.implParagraphChanged(paragraph);
            }

            // updating lists, if required
            if (this.handleTriggeringListUpdate(paragraph, { paraInsert: true })) {
                // mark this paragraph for later list update. This is necessary, because it does not yet contain
                // the list label node (DOM.LIST_LABEL_NODE_SELECTOR) and is therefore ignored in updateLists.
                paragraph.data('updateList', 'true');
            }

            // table parents required for page breaks
            let currentElement = paragraph[0];
            const parents = paragraph.parents('table');
            if (parents.length > 0) {
                currentElement = parents.last();
            }
            // render pagebreaks after insert
            this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(paragraph));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            // Performance: Saving paragraph info for following operations
            this.getSelection().setParagraphCache(paragraph, _.clone(paraStart), 0);

            // #39486
            if (target) {
                this.getPageLayout().markParagraphAsMarginal(paragraph, target);
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Splitting a paragraph at a specified logical position.
         *
         * @param {Number[]} position
         *  The logical position at which the paragraph shall be splitted.
         *
         * @param {String} target
         *  The target for this operation.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the paragraph has been splitted successfully.
         */
        #implSplitParagraph(position, target, external) {

            // the last value of the position array
            const offset = _.last(position);
            // the position of the 'old' paragraph
            const paraPosition = position.slice(0, -1);
            // container root node of paragraph
            const rootNode = this.getRootNode(target);
            // the 'old' paragraph node
            const paragraph = (this.useParagraphCache() && !target && this.getParagraphCache()) || getParagraphElement(rootNode, paraPosition);
            // the position of the 'new' paragraph
            const newParaPosition = increaseLastIndex(paraPosition);
            // the 'new' paragraph node
            const newParagraph = paragraph ? $(paragraph).clone(true).find('div.page-break, ' + DRAWING_SPACEMAKER_NODE_SELECTOR).remove().end().insertAfter(paragraph) : null; //remove eventual page breaks before inserting to DOM
            // a specific position
            let startPosition = null;
            // the length of the paragraph
            const paraLength = paragraph ? getParagraphNodeLength(paragraph) : -1;
            // whether the paragraph is splitted at the end
            const isLastPosition = (paraLength === offset);
            // whether the paragraph contains floated drawings
            const hasFloatedChildren = paragraph ? containsFloatingDrawingNode(paragraph) : false;
            // Performance: Saving global paragraph cache
            const paragraphCacheSafe = (this.useParagraphCache() && this.getParagraphCache()) || null;
            // Performance: Saving data for list updates
            let paraAttrs = null;
            // whether the split paragraph contains selected drawing(s)
            let updateSelectedDrawing = false;
            // whether the document is in read-only mode
            const readOnly = !this.#docApp.isEditable();
            // the list label node inside a paragraph
            let listLabelNode = null;
            // the old drawing start position before splitting the paragraph
            let drawingStartPosition = null, index = 0;
            // the last text span of the paragraph
            let finalTextSpan = null, lastTextSpan = null;
            // whether the current selection is no longer valid after an external split paragraph operation
            let currentPositionIsInvalid = false;
            // the selection object
            const selection = this.getSelection();
            // the spell checker object
            const spellChecker = this.getSpellChecker();
            // some inherited character attributes
            let inheritedCharacterAttrs = null;

            // return with error, if paragraph could not be found or offset is bigger than the length of the paragraph
            if (!paragraph || offset > paraLength) { return false; }

            // also not allowing splitting of slides in presentation app
            if (paragraph && this.useSlideMode() && isSlideNode(paragraph)) { return false; }

            // handling of empty placeholder drawings in presentation app
            if (external && paraLength === 0 && this.#docApp.isPresentationApp()) { this.#handleParagraphSplitInEmptyPlaceHolder(paragraph, newParagraph); }

            // checking if a selected drawing is affected by this split
            if (readOnly && selection.getSelectionType() === 'drawing') {
                // is the drawing inside the splitted paragraph and is the drawing behind the split?
                if (containsNode(paragraph, selection.getSelectedDrawing())) {
                    // split happens at position 'position'. Is this before the existing selection?
                    if (comparePositions(position, selection.getStartPosition(), position.length) < 0) {
                        updateSelectedDrawing = true;
                        drawingStartPosition = _.clone(selection.getStartPosition());
                    }
                }
            }

            // caching current paragraph/table (if paragraph split inside table) for page break calculation
            const currentElement = (position.length > 2) ? $(paragraph).parents('table').last() : $(paragraph);

            // Performance: If the paragraph is splitted at the end, the splitting process can be simplified
            if (isLastPosition && paraLength > 0 && isTextSpan(newParagraph.children(':last'))) {
                if (this.#docApp.isTextApp()) { //inheritance over drawings (DOCS-4187)
                    lastTextSpan = getDomNode(newParagraph.children(':last'));
                    if (isEmptySpan(lastTextSpan) && lastTextSpan.previousSibling && isDrawingFrame(lastTextSpan.previousSibling)) {
                        inheritedCharacterAttrs = this.getInheritInlineCharacterAttributes(lastTextSpan);
                    }
                }
                // the final textSpan must be reused to save character attributes
                finalTextSpan = newParagraph.children(':last').clone(true).text('');
                if (isListLabelNode(newParagraph.children(':first'))) { listLabelNode = newParagraph.children(':first'); }
                newParagraph.empty().end().prepend(finalTextSpan);
                ensureExistingTextNode(finalTextSpan);  // must be done after insertion into the DOM
                if (listLabelNode) { newParagraph.prepend(listLabelNode); }
                startPosition = appendNewIndex(newParaPosition);
                // inheritance over drawings (DOCS-4187)
                if (inheritedCharacterAttrs && !_.isEmpty(inheritedCharacterAttrs)) {
                    this.characterStyles.setElementAttributes(newParagraph.children(':first'), { character: inheritedCharacterAttrs });
                }

            } else {

                // delete trailing part of original paragraph
                if (offset !== -1) {
                    if (this.useParagraphCache()) { this.setParagraphCache(paragraph); } // Performance: implDeleteText can use cached paragraph
                    this.implDeleteText(position, appendNewIndex(paraPosition, -1), { allowEmptyResult: true, keepDrawingLayer: true }, target);
                    // updating absolute paragraph drawings in the second paragraph
                    this.getDrawingLayer().registerUpdateElement(newParagraph);
                    if (hasFloatedChildren) {
                        // delete all image divs that are no longer associated with following floating drawings
                        removeUnusedDrawingOffsetNodes(paragraph);
                    }
                    if (offset === 0) { // if split is at the beginning of paragraph, remove page breaks immediately for better user experience
                        $(paragraph).find('div.page-break').remove();
                        // handle place holder nodes for drawings and comment
                        this.updateAllModels(newParagraph);
                        // DOCS-4427, checking character attributes at empty paragraphs
                        if (this.#docApp.isTextApp()) { this.#updateCharacterAttributesAfterSplit(paragraph, getDomNode(newParagraph)); }
                    }
                }

                // delete leading part of new paragraph
                startPosition = appendNewIndex(newParaPosition);
                if (offset > 0) {
                    if (this.useParagraphCache()) { this.setParagraphCache(newParagraph); } // Performance: implDeleteText cannot use cached paragraph
                    this.implDeleteText(startPosition, increaseLastIndex(startPosition, offset - 1), { keepDrawingLayer: true }, target);
                    if (hasFloatedChildren) {
                        // delete all empty text spans in cloned paragraph before floating drawings
                        // TODO: implDeleteText() should have done this already
                        removeUnusedDrawingOffsetNodes(newParagraph);
                    }
                    // handle place holder nodes for drawings and comment
                    this.updateAllModels(newParagraph);
                }
                // drawings in the new paragraph need a repaint of the canvas border (also if offset is 0)
                this.trigger('drawingHeight:update', newParagraph.find(CANVAS_NODE_SELECTOR), { external });
            }

            // update formatting of the paragraphs
            if (this.isImportFinished()) {
                target = target || '';
                currentPositionIsInvalid = external && position[0] <= startPosition[0] && target === this.getActiveTarget();
                selection.setTemporaryInvalidSelection(currentPositionIsInvalid); // avoiding selection warnings in console after remove splitParagraph operation -> keeping browser selection valid (DOCS-2988)
                this.implParagraphChangedSync($(paragraph)); // increasing stability, for example testrunner TEST_18
                this.implParagraphChangedSync($(newParagraph));
                if (currentPositionIsInvalid) { selection.setTemporaryInvalidSelection(false); }
            } else {
                this.implParagraphChanged(paragraph);
                this.implParagraphChanged(newParagraph);
            }

            // checking the z-index of paragraph
            if (this.#docApp.isTextApp() && this.#docApp.getModel().isIncreasedDocContent()) {
                handleParagraphIndex(paragraph);
                handleParagraphIndex(newParagraph);
            }

            // only for text, there are no paragraph borders and color in presentation
            if (!this.useSlideMode()) {
                // only needed for updating paragraphs that have a color or a border
                this.paragraphStyles.updateParagraphBorderAndColorNodes($(newParagraph).next());
            }

            // invalidating an optional cache with spell results synchronously
            if (spellChecker.hasSpellResultCache(paragraph)) {
                spellChecker.clearSpellResultCache(paragraph);
                spellChecker.clearSpellResultCache(newParagraph);
                // setting marker for next spellcheck process (DOCS-3826)
                $(paragraph).data(SpellChecker.HANDLE_EMPTY_SPELL_RESULT, 'true');
                $(newParagraph).data(SpellChecker.HANDLE_EMPTY_SPELL_RESULT, 'true');
            }

            if (this.useParagraphCache()) { this.setParagraphCache(paragraphCacheSafe); } // Performance: Restoring global paragraph cache

            // checking paragraph attributes for list styles
            if (this.handleTriggeringListUpdate(paragraph, { checkSplitInNumberedList: true })) {
                $(newParagraph).data('splitInNumberedList', 'true');
            }

            // checking paragraph attributes for list styles
            paraAttrs = getExplicitAttributeSet(paragraph);

            // !!!Notice - We do not send operation for removing this attribute, it is by default set to false for any paragraph in filter
            if (paraAttrs && paraAttrs.paragraph && paraAttrs.paragraph.pageBreakBefore && paraAttrs.paragraph.pageBreakBefore === true) {
                this.paragraphStyles.setElementAttributes(newParagraph, { paragraph: { pageBreakBefore: false } });
                newParagraph.removeClass('manual-page-break');
            }
            // !!!Notice - We do not send operation for removing this attribute, it is by default set to false for any paragraph in filter
            if (paraAttrs && paraAttrs.paragraph && paraAttrs.paragraph.pageBreakAfter && paraAttrs.paragraph.pageBreakAfter === true) {
                this.paragraphStyles.setElementAttributes(newParagraph, { paragraph: { pageBreakAfter: false } });
                newParagraph.removeClass('manual-pb-after');
            }

            // Removing 'listStartValue' at paragraph for next paragraph without operation
            if (this.#docApp.isODF() && paraAttrs && paraAttrs.paragraph && _.isNumber(paraAttrs.paragraph.listStartValue)) {
                this.paragraphStyles.setElementAttributes(newParagraph, { paragraph: { listStartValue: null } });
            }

            // block page breaks render if operation is targeted
            if (target) {
                this.setBlockOnInsertPageBreaks(true);
            }

            if (!isLastPosition && currentElement.data('lineBreaksData')) {
                currentElement.removeData('lineBreaksData'); //we changed paragraph layout, cached line breaks data needs to be invalidated
            }
            if (newParagraph.data('lineBreaksData')) {
                newParagraph.removeData('lineBreaksData'); // for the cloned paragraph also
            }

            // removing selected classes from the new paragraph (48180)
            newParagraph.removeClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS + ' ' + PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS);

            // quitFromPageBreak = true; // quit from any currently running pagebreak calculus - performance optimization
            this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(paragraph));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            this.setLastOperationEnd(increaseLastIndex(startPosition, getLeadingAbsoluteDrawingOrDrawingPlaceHolderCount(newParagraph)));

            // Performance: Saving paragraph info for following operations
            selection.setParagraphCache(newParagraph, _.clone(newParaPosition), 0);

            if (updateSelectedDrawing) {
                // selecting the previously selected drawing again
                index = position.length - 1;
                drawingStartPosition[index] -= position[index];  // new text position inside paragraph
                drawingStartPosition[index - 1]++;  // new paragraph position
                selection.setTextSelection(drawingStartPosition, increaseLastIndex(drawingStartPosition));
            }

            return true;
        }

        /**
         * Transferring the character attributes, when a paragraph is splitted at its first position (DOCS-4427).
         *
         * @param {HTMLElement} firstPara
         *  The first paragraph DOM element.
         *
         * @param {HTMLElement} secondPara
         *  The second paragraph DOM element.
         */
        #updateCharacterAttributesAfterSplit(firstPara, secondPara) {

            if (this.hasOoxmlTextCharacterFilterSupport()) { // new processing in OX Text OOXML, DOCS-4211
                if ($(firstPara).children().length === 1 && isEmptySpan(firstPara.firstElementChild)) {
                    if ($(secondPara).children('span').length > 1) {
                        // removing the character attributes from the last text span
                        const lastCharAttrs = getExplicitAttributes(secondPara.lastElementChild, 'character');
                        if (lastCharAttrs) {
                            for (const attr in lastCharAttrs) { lastCharAttrs[attr] = null; }
                            this.characterStyles.setElementAttributes(firstPara.firstElementChild, { character: lastCharAttrs });
                        }
                    }

                    // setting the character attributes from the first text span (DOCS-4211) -> sending in OP with first character in the paragraph
                    const firstCharAttrs = getExplicitAttributes(secondPara.firstElementChild, 'character');
                    if (firstCharAttrs) { this.characterStyles.setElementAttributes(firstPara.firstElementChild, { character: firstCharAttrs }); }
                }
            } else { // behavior before DOCS-4211
                if ($(firstPara).children().length === 1 && isEmptySpan(firstPara.firstElementChild)  && $(secondPara).children('span').length >= 1) {
                    // removing the character attributes from the last text span
                    // TODO: This is only necessary for "$(secondPara).children('span').length > 1", not ">=". But today this is the better roundtrip.
                    const lastCharAttrs = getExplicitAttributes(secondPara.lastElementChild, 'character');
                    if (lastCharAttrs) {
                        for (const attr in lastCharAttrs) { lastCharAttrs[attr] = null; }
                        this.characterStyles.setElementAttributes(firstPara.firstElementChild, { character: lastCharAttrs });
                    }
                }
            }
        }

        /**
         * After splitting an empty paragraph in OX Presentation it might be necessary to
         * remove the placeholder text nodes.
         *
         * @param {Node|jQuery} firstPara
         *  The first part of the splitted paragraph node.
         *
         * @param {Node|jQuery} secondPara
         *  The second part of the splitted paragraph node.
         */
        #handleParagraphSplitInEmptyPlaceHolder(firstPara, secondPara) {
            if (isTextFrameTemplateTextParagraph(firstPara)) {
                this.removeTemplateTextFromTextSpan($(firstPara).children('.templatetext'));
                this.removeTemplateTextFromTextSpan($(secondPara).children('.templatetext'));
            }
        }
    }

    return ParagraphMixin;
}

// exports ====================================================================

export const XParagraph = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XParagraph>}
     *  The extended class with paragraph operation functionality.
     */
    mixin: mixinParagraph
};
