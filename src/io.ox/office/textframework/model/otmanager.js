/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, json } from '@/io.ox/office/tk/algorithms';

import { clonePosition, equalPositions, comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { isOperationRemoved, setOperationRemoved, reduceAttributeSets } from '@/io.ox/office/editframework/utils/otutils';
import { BaseOTManager } from '@/io.ox/office/editframework/model/operation/otmanager';
import * as Op from '@/io.ox/office/textframework/utils/operations';

// re-exports =================================================================

export * from '@/io.ox/office/editframework/model/operation/otmanager';

// constants ==================================================================

/**
 * Alias name for operations inserting a single element at character position
 * (inside a paragraph).
 */
export const OT_INSERT_CHAR_ALIAS = 'insertChar';

/**
 * Alias name for operations that insert a first-class component into the
 * document (a paragraph or a table).
 */
export const OT_INSERT_COMP_ALIAS = 'insertComp';

/**
 * Alias name for operations that merge two first-class components in the
 * document (paragraphs or tables).
 */
export const OT_MERGE_COMP_ALIAS = 'mergeComp';

/**
 * Alias name for operations that split a first-class component in the document
 * (a paragraph or a table).
 */
export const OT_SPLIT_COMP_ALIAS = 'splitComp';

// private constants ----------------------------------------------------------

/**
 * The names of all operations that insert content into a paragraph. Will be
 * aliased to the synthetic operation name `OT_INSERT_CHAR_ALIAS`.
 */
const INSERT_CHAR_OPS = [
    Op.TEXT_INSERT,
    Op.FIELD_INSERT,
    Op.BOOKMARK_INSERT,
    Op.TAB_INSERT,
    Op.HARDBREAK_INSERT,
    Op.RANGE_INSERT
];

/**
 * The names of all operations that insert a first-class component into the
 * document (a paragraph or a table). Will be aliased to the synthetic
 * operation name `OT_INSERT_COMP_ALIAS`.
 */
const INSERT_COMP_OPS = [
    Op.PARA_INSERT,
    Op.TABLE_INSERT
];

/**
 * The names of all operations that merge two first-class components in the
 * document (paragraphs or tables). Will be aliased to the synthetic operation
 * name `OT_MERGE_COMP_ALIAS`.
 */
const MERGE_COMP_OPS = [
    Op.PARA_MERGE,
    Op.TABLE_MERGE
];

/**
 * The names of all operations that split a first-class component in the
 * document (a paragraph or a table). Will be aliased to the synthetic
 * operation name `OT_SPLIT_COMP_ALIAS`.
 */
const SPLIT_COMP_OPS = [
    Op.PARA_SPLIT,
    Op.TABLE_SPLIT
];

// public functions ===========================================================

/**
 * Creates a clone of the passed logical position and increases the last
 * element of the array by the specified value. The passed array object
 * will not be changed.
 *
 * @param {Position} position
 *  The initial logical position.
 *
 * @param {Number} [increment=1]
 *  The value that will be added to the last element of the position.
 *
 * @returns {Array<Number>}
 *  A clone of the passed logical position, with the last element increased.
 */
export function increaseLastIndex(position, increment) {
    position = clonePosition(position);
    position[position.length - 1] += (is.number(increment) ? increment : 1);
    return position;
}

/**
 * Check, whether the logical start position of specified reference operation is removed by the start or end
 * position of a specified delete operation.
 * Example:
 * 'delete start: [2]' removes an 'insertText start: [2,6]'
 * 'delete start: [1] end: [2]' removes an 'insertText start: [2,6]'
 * The second example is important, because the insertText position is 'behind' the end position, if the
 * function 'comparePositions' is used.
 *
 * @param {Object} deleteOp
 *  A delete operation. It must contain a start position and can contain an end position.
 *
 * @param {Object} startOp
 *  A reference operation. It must contain a start position.
 *
 * @returns {Boolean}
 *  Whether the start position of the reference operation is removed by the specified start and end positions
 *  of the delete operation.
 */
export function ancestorRemoved(deleteOp, startOp) {
    return isAncestorPos(deleteOp.start, startOp.start) || (deleteOp.end && isAncestorPos(deleteOp.end, startOp.start));
}

/**
 * Check, whether the specified logical position posA describes an ancestor of the logical position posB.
 *
 * @param {Number[]} posA
 *  A logical position.
 *
 * @param {Number[]} posB
 *  A logical position.
 *
 * @returns {Boolean}
 *  Whether the specified logical position posA describes an ancestor of the logical position posB.
 */
export function isAncestorPos(posA, posB) {
    return (posA.length < posB.length && equalPositions(posA, posB, posA.length));
}

/**
 * Check, whether the specified operations have completely identical ranges. Both operations must
 * have a property named 'start'. Optionally a property named 'end' is evaluated.
 *
 * @param {Object} op1
 *  An operation with a 'start' property and optionally an 'end' property.
 *
 * @param {Object} op2
 *  An operation with a 'start' property and optionally an 'end' property.
 *
 * @returns {Boolean}
 *  Whether the specified operations have completely identical ranges, without overlap.
 */
export function identicalRanges(op1, op2) {

    let isIdentical = false;

    const compareEnd = op1.end || op2.end; // no check of 'end' required, if both end positions are not defined

    const op1Start = op1.start;
    const op2Start = op2.start;
    let op1End;
    let op2End;

    if (compareEnd) {
        op1End = op1.end ? op1.end : op1.start;
        op2End = op2.end ? op2.end : op2.start;
    }

    if (equalPositions(op1Start, op2Start) && (!compareEnd || equalPositions(op1End, op2End))) {
        isIdentical = true;
    }

    return isIdentical;
}

/**
 * Check, whether the specified operations have completely separated ranges, without an overlap. Both
 * operations must have a property named 'start'. Optionally a property named 'end' is evaluated.
 *
 * This function also handles problematic case like: delete [3][4] and delete [4,1][4,3] (ancestors)
 *
 * @param {Object} op1
 *  An operation with a 'start' property and optionally an 'end' property.
 *
 * @param {Object} op2
 *  An operation with a 'start' property and optionally an 'end' property.
 *
 * @returns {Boolean}
 *  Whether the specified operations have completely separated ranges, without overlap.
 */
export function separateRanges(op1, op2) {

    let isSeparated = false;

    const op1Start = op1.start;
    const op2Start = op2.start;
    const op1End = op1.end ? op1.end : op1.start;
    const op2End = op2.end ? op2.end : op2.start;

    if ((comparePositions(op1End, op2Start) < 0 && !isAncestorPos(op1End, op2Start)) || (comparePositions(op2End, op1Start) < 0 && !isAncestorPos(op2End, op1Start))) {
        isSeparated = true;
    }

    return isSeparated;
}

/**
 * Check, whether the specified operations have surrounding ranges. This means that the range of
 * one operation is completely inside the range of the other operation. This is also the case, if
 * both ranges are identical.
 *
 * This function also handles problematic case like: delete [4] that surrounds delete [4,1][4,3] (ancestors)
 *
 * @param {Object} op1
 *  An operation with a 'start' property and optionally an 'end' property.
 *
 * @param {Object} op2
 *  An operation with a 'start' property and optionally an 'end' property.
 *
 * @returns {Number}
 *  Returns 1, if the range of op1 surrounds the range of op2 (or if both are equal).
 *  Returns -1, if the range of op2 surrounds the range of op1.
 *  Returns 0, if no range surrounds the other range.
 */
export function surroundingRanges(op1, op2) {

    let isSurrounding = 0;

    // helper function to compare two operations in specific order
    const doSurround = (opA, opB) => {

        let surrounds = false;

        const opAStart = opA.start;
        const opBStart = opB.start;
        const opAEnd = opA.end ? opA.end : opA.start;
        const opBEnd = opB.end ? opB.end : opB.start;

        if (comparePositions(opAStart, opBStart) <= 0 && (comparePositions(opAEnd, opBEnd) >= 0 || isAncestorPos(opAEnd, opBEnd))) {
            surrounds = true;
        }

        return surrounds;
    };

    if (doSurround(op1, op2)) {
        isSurrounding = 1;
    } else if (doSurround(op2, op1)) {
        isSurrounding = -1;
    }

    return isSurrounding;
}

/**
 * Calculating a 'delete' array, that can be used to modify a specified position 'pos'.
 * The logical position 'pos' has to be behind the specified start and end positions,
 * so that its values are reduced caused by the removal of the content between start
 * and end position.
 *
 * Examples:
 * start: [0,4], end: [0,9], pos: [0,20]  -> returned array: [0,6]
 * start: [0,4], end: [0,9], pos: [1,20]  -> returned array: [0,0]
 * start: [0,4], end: [1,9], pos: [1,20]  -> returned array: [0,9]
 * start: [0], end: [1,9], pos: [1,20]    -> returned array: [1,9]
 *
 * @param {number[]} start
 *  The logical start position (of the delete operation).
 *
 * @param {number[]} end
 *  The logical end position (of the delete operation).
 *
 * @param {number[]} pos
 *  The logical position for that the 'delete' array is calculated.
 *
 * @returns {number[]}
 *  And array with the length of the specified logical position 'pos', that can be
 *  used to calculate the modified position, after the delete operation is applied.
 */
export function calculateDeleteArray(start, end, pos) {

    const posLength = pos.length;
    const lastStartIndex = start.length - 1;
    const lastEndIndex = end.length - 1;
    let sameParent = true; // whether start and end position have same parent during iteration
    let relevant = true; // whether the diff is relevant for the specified position
    const diffArray = [];

    for (let index = 0; index < posLength; index += 1) {

        let diff = 0;

        if (relevant) {
            if (sameParent) {

                if (is.number(end[index]) && is.number(start[index])) {
                    if (end[index] > start[index]) {
                        diff = end[index] - start[index] - 1;
                        if (index === lastStartIndex) { diff++; }
                        if (index === lastEndIndex) { diff++; } // not if this is the same
                    } else if (index === lastStartIndex && index === lastEndIndex && end[index] === start[index]) {
                        diff = 1; // one single character, if both positions are identical
                    }
                }

                if (!is.number(start[index]) || !is.number(end[index]) || end[index] !== start[index]) { sameParent = false; } // check if start and end position still have the same parent

                if (!is.number(end[index]) || pos[index] !== end[index]) { relevant = false; } // check if end position and specified position still have the same parent

            } else {

                diff = is.number(end[index]) ? end[index] + 1 : 0; // example: [2,5] to [3,5] for position [3,7] (pos [3,5] means, that 6 characters are removed, therefore '+1')

                if (!is.number(end[index]) || pos[index] !== end[index]) { relevant = false; } // check if end position and specified position still have the same parent
            }
        }

        diffArray[index] = diff;
    }

    return diffArray;
}

/**
 * Returns the number of characters inserted by the passed insert operation
 * (any operation aliased to `OT_INSERT_CHAR_ALIAS`).
 *
 * @param {Operation} op
 *  The JSON document operation.
 *
 * @returns {number}
 *  The number of characters inserted by the passed insert operation.
 */
export function getInsertCharLength(op) {
    return (op.name === Op.TEXT_INSERT) ? op.text.length : 1;
}

/**
 * Check, whether a specified array of numbers contains only directly following numbers or if
 * there is a gap between two following numbers. Furthermore it is expected, that the numbers
 * in the array are sorted.
 *
 * This function is required for evaluating group and ungroup operations
 *
 * @param {number[]} arr
 *  A (sorted) number array.
 *
 * @returns {Boolean}
 *  Whether the specified array of numbers contains a gap between two directly following numbers.
 */
export function isGapInArray(arr) {

    let hasGap = false;
    let previousValue = -1;

    arr.forEach(value => {
        if (previousValue >= 0 && !hasGap) {
            if (value - previousValue > 1) { hasGap = true; }
        }
        previousValue = value;
    });

    return hasGap;
}

/**
 * Generating an object that contains the result of a handler function. This object
 * can have the properties 'externalOpsBefore', 'externalOpsAfter', 'localOpsBefore',
 * 'localOpsAfter', that have as values arrays of new generated operations.
 *
 * @returns {Object|null}
 *  The object with the new generated operations. Or null, if no new operation is
 *  generated.
 */
export function getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter) {

    if (!externalOpsBefore && !externalOpsAfter && !localOpsBefore && !localOpsAfter) { return null; }

    const result = {};

    if (externalOpsBefore) { result.externalOpsBefore = externalOpsBefore; }
    if (externalOpsAfter) { result.externalOpsAfter = externalOpsAfter; }
    if (localOpsBefore) { result.localOpsBefore = localOpsBefore; }
    if (localOpsAfter) { result.localOpsAfter = localOpsAfter; }

    return result;
}

// class OTManager ========================================================

/**
 * An instance of this class represents the Operational Transformation (OT) manager.
 * This manager handles external operations, if there are local operations that are
 * not yet acknowledged by the server. In this case the logical positions of the
 * external operations need to be adapted.
 * Additionally the logical position of the local operations without acknowledgement
 * by the server need to be modified by two reasons:
 * 1. If they are not already sent to the server, they are transformed on client side,
 *    so that the server does not need to do it. This is especially important, if a
 *    client is only allowed to send his operations to the server after receiving the
 *    acknowledgement for his previous sent operations.
 * 2. Following external operations must be transformed with the modified local
 *    operations.
 */
export class TextBaseOTManager extends BaseOTManager {

    constructor(config) {

        // base constructor
        super(config);

        // initialization
        this.registerTransformations({
            delete: {
                delete: this.handleDeleteDelete,
                deleteColumns: this.handleDeleteColumnsDelete,
                insertCells: (...args) => this.handleInsertRowsDelete(...args),
                insertChar: this.handleInsertCharDelete,
                insertColumn: this.handleInsertColumnDelete,
                insertComp: this.handleInsertCompDelete,
                insertRows: (...args) => this.handleInsertRowsDelete(...args),
                mergeComp: this.handleMergeCompDelete,
                position: this.handleDeleteExtPosition,
                setAttributes: this.handleSetAttrsDelete,
                splitComp: this.handleSplitCompDelete,
                updateField: this.handleSetAttrsDelete
            },
            deleteColumns: {
                delete: this.handleDeleteColumnsDelete,
                deleteColumns: this.handleDeleteColumnsDeleteColumns,
                insertCells: (...args) => this.handleInsertCellsDeleteColumns(...args), // arrow function, DOCS-4422
                insertChar: this.handleDeleteColumnsInsertChar,
                insertColumn: this.handleInsertColumnDeleteColumns,
                insertComp: this.handleInsertCompDeleteColumns,
                insertRows: this.handleDeleteColumnsInsertRows,
                mergeComp: this.handleMergeCompDeleteColumns,
                position: this.handleDeleteColumnsExtPosition,
                setAttributes: this.handleSetAttrsDeleteColumns,
                splitComp: this.handleDeleteColumnsSplitComp,
                updateField: this.handleSetAttrsDeleteColumns
            },
            insertCells: {
                delete: (...args) => this.handleInsertRowsDelete(...args),
                deleteColumns: (...args) => this.handleInsertCellsDeleteColumns(...args),
                insertCells: this.handleInsertRowsInsertRows,
                insertChar: this.handleInsertRowsInsertChar,
                insertColumn: this.handleInsertCellsInsertColumn,
                insertComp: this.handleInsertCompInsertRows,
                insertRows: this.handleInsertCellsInsertRows,
                mergeComp: this.handleMergeCompInsertRows,
                position: this.handleInsertRowsExtPosition,
                setAttributes: this.handleSetAttrsInsertRows,
                splitParagraph: this.handleInsertRowsSplitPara,
                splitTable: this.handleInsertRowsSplitTable,
                updateField: this.handleSetAttrsInsertRows
            },
            insertChar: {
                delete: this.handleInsertCharDelete,
                deleteColumns: this.handleDeleteColumnsInsertChar,
                insertCells: this.handleInsertRowsInsertChar,
                insertChar: this.handleInsertCharInsertChar,
                insertColumn: this.handleInsertColumnInsertChar,
                insertComp: this.handleInsertCompInsertChar,
                insertRows: this.handleInsertRowsInsertChar,
                mergeComp: this.handleMergeCompInsertChar,
                position: this.handleInsertCharExtPosition,
                setAttributes: this.handleSetAttrsInsertChar,
                splitParagraph: this.handleSplitParaInsertChar,
                splitTable: this.handleSplitTableInsertChar,
                updateField: this.handleSetAttrsInsertChar
            },
            insertColumn: {
                delete: this.handleInsertColumnDelete,
                deleteColumns: this.handleInsertColumnDeleteColumns,
                insertCells: this.handleInsertCellsInsertColumn,
                insertChar: this.handleInsertColumnInsertChar,
                insertColumn: this.handleInsertColumnInsertColumn,
                insertComp: this.handleInsertCompInsertColumn,
                insertRows: this.handleInsertRowsInsertColumn,
                mergeComp: this.handleMergeCompInsertColumn,
                position: this.handleInsertColumnExtPosition,
                setAttributes: this.handleSetAttrsInsertColumn,
                splitComp: this.handleInsertColumnSplitComp,
                updateField: this.handleSetAttrsInsertColumn
            },
            insertComp: {
                delete: this.handleInsertCompDelete,
                deleteColumns: this.handleInsertCompDeleteColumns,
                insertCells: this.handleInsertCompInsertRows,
                insertChar: this.handleInsertCompInsertChar,
                insertColumn: this.handleInsertCompInsertColumn,
                insertComp: this.handleInsertCompInsertComp,
                insertRows: this.handleInsertCompInsertRows,
                mergeComp: this.handleMergeCompInsertComp,
                position: this.handleInsertCompExtPosition,
                setAttributes: this.handleSetAttrsInsertComp,
                splitParagraph: this.handleInsertCompSplitPara,
                splitTable: this.handleSplitTableInsertPara,
                updateField: this.handleSetAttrsInsertComp
            },
            insertRows: {
                delete: (...args) => this.handleInsertRowsDelete(...args),
                deleteColumns: this.handleDeleteColumnsInsertRows,
                insertCells: this.handleInsertCellsInsertRows,
                insertChar: this.handleInsertRowsInsertChar,
                insertColumn: this.handleInsertRowsInsertColumn,
                insertComp: this.handleInsertCompInsertRows,
                insertRows: this.handleInsertRowsInsertRows,
                mergeComp: this.handleMergeCompInsertRows,
                position: this.handleInsertRowsExtPosition,
                setAttributes: this.handleSetAttrsInsertRows,
                splitParagraph: this.handleInsertRowsSplitPara,
                splitTable: this.handleInsertRowsSplitTable,
                updateField: this.handleSetAttrsInsertRows
            },
            mergeComp: {
                delete: this.handleMergeCompDelete,
                deleteColumns: this.handleMergeCompDeleteColumns,
                insertCells: this.handleMergeCompInsertRows,
                insertChar: this.handleMergeCompInsertChar,
                insertColumn: this.handleMergeCompInsertColumn,
                insertComp: this.handleMergeCompInsertComp,
                insertRows: this.handleMergeCompInsertRows,
                mergeComp: this.handleMergeCompMergeComp,
                setAttributes: this.handleSetAttrsMergeComp,
                splitComp: this.handleSplitCompMergeComp,
                updateField: this.handleSetAttrsMergeComp
            },
            mergeParagraph: {
                position: this.handleMergeParaExtPosition
            },
            mergeTable: {
                position: this.handleMergeTableExtPosition
            },
            setAttributes: {
                delete: this.handleSetAttrsDelete,
                deleteColumns: this.handleSetAttrsDeleteColumns,
                insertCells: this.handleSetAttrsInsertRows,
                insertChar: this.handleSetAttrsInsertChar,
                insertColumn: this.handleSetAttrsInsertColumn,
                insertComp: this.handleSetAttrsInsertComp,
                insertRows: this.handleSetAttrsInsertRows,
                mergeComp: this.handleSetAttrsMergeComp,
                position: null,
                setAttributes: this.handleSetAttrsSetAttrs,
                splitParagraph: this.handleSetAttrsSplitPara,
                splitTable: this.handleSetAttrsSplitTable,
                updateField: null
            },
            splitComp: {
                delete: this.handleSplitCompDelete,
                deleteColumns: this.handleDeleteColumnsSplitComp,
                insertColumn: this.handleInsertColumnSplitComp,
                mergeComp: this.handleSplitCompMergeComp
            },
            splitParagraph: {
                insertCells: this.handleInsertRowsSplitPara,
                insertChar: this.handleSplitParaInsertChar,
                insertComp: this.handleInsertCompSplitPara,
                insertRows: this.handleInsertRowsSplitPara,
                position: this.handleSplitParaExtPosition,
                setAttributes: this.handleSetAttrsSplitPara,
                splitParagraph: this.handleSplitParaSplitPara,
                splitTable: this.handleSplitParaSplitTable,
                updateField: this.handleSetAttrsSplitPara
            },
            splitTable: {
                insertCells: this.handleInsertRowsSplitTable,
                insertChar: this.handleSplitTableInsertChar,
                insertComp: this.handleSplitTableInsertPara,
                insertRows: this.handleInsertRowsSplitTable,
                position: this.handleSplitTableExtPosition,
                setAttributes: this.handleSetAttrsSplitTable,
                splitParagraph: this.handleSplitParaSplitTable,
                splitTable: this.handleSplitTableSplitTable,
                updateField: this.handleSetAttrsSplitTable
            },
            updateField: {
                delete: this.handleSetAttrsDelete,
                deleteColumns: this.handleSetAttrsDeleteColumns,
                insertCells: this.handleSetAttrsInsertRows,
                insertChar: this.handleSetAttrsInsertChar,
                insertColumn: this.handleSetAttrsInsertColumn,
                insertComp: this.handleSetAttrsInsertComp,
                insertRows: this.handleSetAttrsInsertRows,
                mergeComp: this.handleSetAttrsMergeComp,
                position: null,
                setAttributes: null,
                splitParagraph: this.handleSetAttrsSplitPara,
                splitTable: this.handleSetAttrsSplitTable,
                updateField: this.handleUpdateFieldUpdateField
            }
        });

        this.registerAliasName(OT_INSERT_CHAR_ALIAS, INSERT_CHAR_OPS);
        this.registerAliasName(OT_INSERT_COMP_ALIAS, INSERT_COMP_OPS);
        this.registerAliasName(OT_MERGE_COMP_ALIAS, MERGE_COMP_OPS);
        this.registerAliasName(OT_SPLIT_COMP_ALIAS, SPLIT_COMP_OPS);
    }

    // handler functions for one operation and a specified position

    /**
     * Handling the specified position with the insertChar operations.
     */
    handleInsertCharExtPosition(insertOp, posOp) {
        const lastIndex = insertOp.start.length - 1;
        if (posOp.start.length >= insertOp.start.length && equalPositions(insertOp.start, posOp.start, lastIndex)) {
            if (insertOp.start[lastIndex] <= posOp.start[lastIndex]) {
                posOp.start[lastIndex] += getInsertCharLength(insertOp); // the position of the external operation needs to be increased
            }
        }
    }

    /**
     * Handling the specified position with the split paragraph operation.
     */
    handleSplitParaExtPosition(splitOp, posOp) {

        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let length = 0;

        if (splitOp.start.length <= posOp.start.length) {
            length = splitOp.start.length; // both positions have the same length
            if (length === 2 || equalPositions(splitOp.start, posOp.start, length - 2)) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = splitOp.start.length - 2;
                textIndex = paraIndex + 1;
                if (splitOp.start[paraIndex] < posOp.start[paraIndex]) {
                    posOp.start[paraIndex]++; // increasing position of insert operation
                } else if (splitOp.start[paraIndex] === posOp.start[paraIndex] && splitOp.start[textIndex] <= posOp.start[textIndex]) {
                    posOp.start[paraIndex]++;
                    posOp.start[textIndex] -= splitOp.start[textIndex];
                }
            }
        }
    }

    /**
     * Handling the specified position with the merge paragraph operation.
     */
    handleMergeParaExtPosition(mergeOp, posOp) {

        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let length = 0;

        if (mergeOp.start.length <= posOp.start.length - 1) {
            length = mergeOp.start.length; // both positions have the paragraph at the same level
            if (mergeOp.start.length === 1 || equalPositions(mergeOp.start, posOp.start, length - 1)) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = length - 1;
                textIndex = paraIndex + 1;
                if (mergeOp.start[paraIndex] < posOp.start[paraIndex] - 1) {
                    posOp.start[paraIndex]--; // simply decreasing position of insert operation
                } else if (mergeOp.start[paraIndex] === posOp.start[paraIndex] - 1) { // handling mergeParagraph at [0] with insertText [1,14]
                    // the merge happens always before the insert position
                    posOp.start[paraIndex]--;
                    posOp.start[textIndex] += (mergeOp.paralength ? mergeOp.paralength : 0);
                }
            }
        }
    }

    /**
     * Handling the specified position with the insert operation.
     */
    handleInsertCompExtPosition(insertOp, posOp) {

        // the length of the logical position of the insert operation
        let insertLength = 0;
        // the index of the paragraph position inside the logical position
        let paraIndex = 0;

        if (comparePositions(insertOp.start, posOp.start) <= 0) {
            // the paragraphs must have the same parent to influence the external position
            insertLength = insertOp.start.length;
            paraIndex = insertLength - 1;

            if (paraIndex === 0 || (posOp.start.length >= insertLength && equalPositions(posOp.start, insertOp.start, insertLength - 1))) {
                posOp.start[paraIndex]++;
            }
        }
    }

    /**
     * Handling the specified position with the insertRows/insertCells operation.
     * insertRows: { start:[1, 1], count: 1, referenceRow: 0 }
     * insertCells: { start:[1, 1, 1], count: 1 }
     */
    handleInsertRowsExtPosition(insertOp, posOp) {

        // the length of the logical position of the insertRows operation
        let insertLength = 0;
        // the index of the row/cell position inside the logical position
        let lastIndex = 0;
        // the number of inserted rows
        const count = insertOp.count || 1;

        if (comparePositions(insertOp.start, posOp.start) <= 0) {
            // the table must be the parent for both positions to influence the external position
            insertLength = insertOp.start.length;
            lastIndex = insertLength - 1;

            if ((posOp.start.length >= insertLength && equalPositions(posOp.start, insertOp.start, insertLength - 1))) {
                if (posOp.start[lastIndex] >= insertOp.start[lastIndex]) { // position inside a following row/cell
                    posOp.start[lastIndex] += count;
                }
            }
        }
    }

    /**
     * Handling the specified position with the insertColumn operation.
     *
     * insertColumn: { start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     *
     * Only positions inside the same table are affected, if a column is inserted.
     * Allowed values for the insertMode: 'before' and 'behind'
     *
     * If a table or a row is selected, the insertColumn operation has no influence to the
     * selection -> 'posOp.start.length > insertLength + 1'
     *
     * Merged cells might be problematic. It must be possible to switch reliable from gridPosition
     * to logical position
     * -> Solution: insertColumn is only used, if the table does not contain mergedCells. In all
     *              other cases, insertCells is used.
     *
     * CHECK: The logical (column) position is only increased, if the gridPosition is smaller than
     *        the logical (column) position.
     */
    handleInsertColumnExtPosition(insertOp, posOp) {

        // the length of the logical position of the insertColumn operation
        let insertLength = 0;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';

        if (comparePositions(insertOp.start, posOp.start) < 0) {
            // the table must be the parent for both positions to influence the external position
            insertLength = insertOp.start.length;

            if ((posOp.start.length > (insertLength + 1) && equalPositions(posOp.start, insertOp.start, insertLength))) {
                // the logical position is at least two positions longer than the table specified in insertColumn operation
                tableIndex = insertLength - 1;
                columnIndex = tableIndex + 2;
                if ((posOp.start[columnIndex] > insertOp.gridPosition) || (mode === 'before' && posOp.start[columnIndex] === insertOp.gridPosition)) {
                    posOp.start[columnIndex]++;
                }
            }
        }
    }

    /**
     * Handling the specified position with the insertColumn operation.
     *
     * insertColumn: { start: [2], startGrid: 1, endGrid: 2 }
     *
     * Only positions inside the same table are affected, if a column is removed.
     *
     * If a table or a row is selected, the deleteColumns operation has no influence to the
     * selection -> 'posOp.start.length > deleteLength + 1'
     *
     * Merged cells might be problematic. It must be possible to switch reliable from gridPosition
     * to logical position
     * -> Solution: insertColumn is only used, if the table does not contain mergedCells. In all
     *              other cases, insertCells is used.
     */
    handleDeleteColumnsExtPosition(deleteOp, posOp) {

        // the length of the logical position of the deleteColumns operation
        let deleteLength = 0;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the number of deleted columns
        const deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;

        if (comparePositions(deleteOp.start, posOp.start) < 0) {
            // the table must be the parent for both positions to influence the external position
            deleteLength = deleteOp.start.length;

            if ((posOp.start.length > (deleteLength + 1) && equalPositions(posOp.start, deleteOp.start, deleteLength))) {
                // the logical position is at least two positions longer than the table specified in deleteColumn operation
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (posOp.start[columnIndex] >= deleteOp.startGrid) {
                    if (posOp.start[columnIndex] > deleteOp.endGrid) {
                        posOp.start[columnIndex] -= deleteColumns;
                    } else {
                        setOperationRemoved(posOp); // invalidate the position, not possible to determine position
                    }
                }
            }
        }
    }

    /**
     * Handling the specified position with the delete operation.
     * If the external position is before the delete range, nothing need to be modified.
     * If the external position is behind the delete range, the position need to be shifted.
     * If the external position is inside the delete range, the position need to be set to the start of the delete operation.
     *
     * The position of the delete operation can have an arbitrary length.
     * The delete operation can have an end position, but this is not always the case.
     *
     */
    handleDeleteExtPosition(deleteOp, posOp) {

        // the length of the start position of the delete operation
        const deleteLength = deleteOp.start.length;
        // whether there is a range to delete
        const isRange = deleteOp.end && !equalPositions(deleteOp.start, deleteOp.end);
        // the last index
        let lastIndex = 0;

        if (!isRange) {

            // the length of the position of the delete operation must be shorter or have the same length as the external position
            if (deleteLength <= posOp.start.length) {
                // all positions must be identical except the last
                // -> if the last two positions are not identical, the delete position does not influence the specified position
                lastIndex = deleteLength - 1;

                if (lastIndex === 0 || equalPositions(deleteOp.start, posOp.start, lastIndex)) {
                    if (deleteOp.start[lastIndex] < posOp.start[lastIndex]) {
                        posOp.start[lastIndex]--; // decrease the value of the specified position
                    } else if (deleteOp.start[lastIndex] === posOp.start[lastIndex]) {
                        setOperationRemoved(posOp); // not possible to determine position
                    }
                }
            }
        } else {
            // there is a range for deletion -> start postion and end position are not equal

            // 1. the specified position is before the delete start position -> nothing to do
            if (comparePositions(posOp.start, deleteOp.start) < 0) { return; } // no change of external position

            // 2. the specified position is in the deletion range -> set it to start value or undefined
            if (ancestorRemoved(deleteOp, posOp) || (deleteOp.end && comparePositions(posOp.start, deleteOp.end) <= 0)) {
                setOperationRemoved(posOp); // invalidate the position
                return;
            }

            // 3. the specified position is behind the delete end position -> it must be modified.

            // modifying the specified logical position
            const posLength = posOp.start.length;
            const deleteArray = calculateDeleteArray(deleteOp.start, deleteOp.end, posOp.start);

            // calculating the new values for the external position
            for (let index = 0; index < posLength; index += 1) { posOp.start[index] -= deleteArray[index]; }
        }
    }

    /**
     * Handling the specified position with the mergeTable operation.
     */
    handleMergeTableExtPosition(mergeOp, posOp) {

        // the length of the logical position of the merge operation
        let mergeLength = 0;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the row position inside the logical position
        let rowIndex = 0;
        // the number of rows of the merged table
        const rowCount = mergeOp.rowcount ? mergeOp.rowcount : 1;

        if (comparePositions(mergeOp.start, posOp.start) <= 0) {
            // the paragraphs must have the same parent to influence the external position
            mergeLength = mergeOp.start.length;
            tableIndex = mergeLength - 1;
            rowIndex = tableIndex + 1;

            // the external position can be behind the table or inside the splitted table (there is always a delete paragraph operation before)
            if (tableIndex === 0 || (posOp.start.length >= mergeLength && equalPositions(posOp.start, mergeOp.start, mergeLength - 1))) {
                if (posOp.start[tableIndex] > (mergeOp.start[tableIndex] + 1)) {
                    posOp.start[tableIndex]--;
                } else if (posOp.start[tableIndex] === (mergeOp.start[tableIndex] + 1)) {
                    posOp.start[tableIndex]--;
                    posOp.start[rowIndex] += rowCount;
                }
            }
        }
    }

    /**
     * Handling the specified position with the splitTable operation.
     */
    handleSplitTableExtPosition(splitOp, posOp) {

        // the length of the logical position of the split operation
        let splitLength = 0;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the row position inside the logical position
        let rowIndex = 0;

        if (comparePositions(splitOp.start, posOp.start) <= 0) {
            // the paragraphs must have the same parent to influence the external position
            splitLength = splitOp.start.length;
            tableIndex = splitLength - 2;
            rowIndex = splitLength - 1;

            // the external position can be behind the table or inside the splitted table
            if (tableIndex === 0 || (posOp.start.length >= (splitLength - 1) && equalPositions(posOp.start, splitOp.start, splitLength - 2))) {
                if (posOp.start[tableIndex] > splitOp.start[tableIndex]) {
                    posOp.start[tableIndex]++;
                } else if (posOp.start[tableIndex] === splitOp.start[tableIndex] && posOp.start[rowIndex] >= splitOp.start[rowIndex]) {
                    posOp.start[tableIndex]++;
                    posOp.start[rowIndex] -= splitOp.start[rowIndex];
                }
            }
        }
    }

    // handler functions for two operations

    /**
     * Handling two insertText operations.
     * Insert operations do only influence each other, if they are inside the same paragraph.
     */
    handleInsertCharInsertChar(localOp, extOp) {

        // the length of the local logical position
        const localLength = localOp.start.length;
        // the length of the external logical position
        const extLength = extOp.start.length;
        // the paragraph position inside the logical position
        let textIndex = 0;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;

        // is the position of the local operation in the same paragraph as the external operation?
        if (localLength === extLength) {
            if (equalPositions(localOp.start, extOp.start, localLength - 1)) {
                textIndex = localLength - 1;
                if (localOp.start[textIndex] < extOp.start[textIndex]) { // INFO: On server side this is implemented with '<='
                    extOp.start = increaseLastIndex(extOp.start, getInsertCharLength(localOp)); // the position of the external operation needs to be increased
                } else {
                    localOp.start = increaseLastIndex(localOp.start, getInsertCharLength(extOp)); // the position of the internal operation needs to be increased
                }
            }
        } else {
            shortOp = localLength < extLength ? localOp : extOp;
            longOp = localLength < extLength ? extOp : localOp;
            textIndex = shortOp.start.length - 1;

            if (equalPositions(shortOp.start, longOp.start, textIndex)) {
                if (shortOp.start[textIndex] <= longOp.start[textIndex]) {
                    longOp.start[textIndex] += getInsertCharLength(shortOp);
                }
            }
        }
    }

    /**
     * Handling of two setAttributes operations.
     * In this case the positions of the external setAttributes need to be adapted, if there is
     * an overlap of two concurrent setAttributes operations.
     *
     * The local operation is never modified. The external operation (sent from the server) is
     * modified. It is necessary to change the attributes, but also the logical positions must
     * be adapted. Additionally it is possible, that two, or even three external operations are
     * generated.
     *
     * -> What happens with this transformation on server side? On the server side, the external
     *    operation (the operation sent from the client) is never modified. Therefore only the
     *    stack of locally cloned operations must be modified -> but for what is this useful
     *    in the case of setAttributes operation?
     *
     * Example:
     *   The external operation set to a specific range a red background color, the local operation
     *   to another specific range a green background color. If these ranges overlap, the local
     *   operation has to win. The reason is, that the external operation is already applied on
     *   the server, while the local operation will be applied soon.
     */
    handleSetAttrsSetAttrs(localOp, extOp) {

        // whether the two delete ranges surround each other
        let surrounding = false;
        // a new operation might be required
        let newOperation = null;
        // a new operation with reduced attribute set might be required
        let newReducedOperation = null;
        // the length of the logical position
        let posLength = 0;
        // the last index of the logical position
        let lastIndex = 0;

        if (separateRanges(localOp, extOp)) { return; } // nothing to do

        // a copy of the attributes at the external operation
        const reducedExtAttrs = _.copy(extOp.attrs, true);
        // reducing the set of the external attributes
        const extModified = reduceAttributeSets(localOp.attrs, reducedExtAttrs); // the object with the external attributes might be modififed
        // whether there are remaining external attributes
        const emptyExtAttributes = extModified && _.isEmpty(reducedExtAttrs);
        // whether a new operation shall be inserted before an existing operation
        let insertBefore = false;

        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        // TODO: Handling of change track attributes!

        if (extModified) {
            if (identicalRanges(localOp, extOp))  {
                if (emptyExtAttributes) {
                    setOperationRemoved(extOp); // setting marker at external operation -> not applying operation locally
                } else {
                    extOp.attrs = reducedExtAttrs; // only the reduced part of the external attributes is locally applied
                }
            } else {

                // TODO: Checking existence of reduced attributes

                surrounding = surroundingRanges(localOp, extOp);

                if (surrounding) {

                    if (surrounding === 1) {
                        // local surrounds external operation
                        if (extOp.start.length === localOp.start.length) {
                            // -> not removing external operation, if the attribute is set locally at [3], but externally more specific from [3,4] to [3,6]
                            if (emptyExtAttributes) {
                                setOperationRemoved(extOp); // setting marker at external operation
                            } else {
                                extOp.attrs = reducedExtAttrs; // only the reduced part of the external attributes is locally applied
                            }
                        }
                    } else {
                        // position of external operation surrounds position of local operation
                        // -> at maxium three external operations are required:
                        //    - one leading range with full set of attributes
                        //    - one overlapping range with reduced set of attributes
                        //    - one trailing range with full set of attributes
                        posLength = extOp.start.length;
                        lastIndex = posLength - 1;

                        // assuming all setAttributes positions are on the same level (same length)
                        if (extOp.end && localOp.end && extOp.end.length === posLength && localOp.start.length === posLength && localOp.end.length === posLength) {

                            if (extOp.start[lastIndex] < localOp.start[lastIndex] && extOp.end[lastIndex] > localOp.end[lastIndex]) {
                                // external surrounds local operation -> generating a second (external) operation for the two external ranges
                                // It is (probably) not necessary to modify the local operation (and the stack of local operations)
                                newOperation = json.deepClone(extOp); // generating new operation for the 'end' range
                                newOperation.start = increaseLastIndex(localOp.end);
                                externalOpsAfter = [newOperation];
                                extOp.end = increaseLastIndex(localOp.start, -1);

                            } else if (extOp.start[lastIndex] < localOp.start[lastIndex]) {
                                extOp.end = increaseLastIndex(localOp.start, -1);
                            } else if (extOp.end[lastIndex] > localOp.end[lastIndex]) {
                                extOp.start = increaseLastIndex(localOp.end);
                                insertBefore = true;
                            }

                            // a further new external operation is required for the range of the local operation, but with reduced attribute set
                            if (!emptyExtAttributes) {
                                newReducedOperation = json.deepClone(localOp);
                                newReducedOperation.attrs = reducedExtAttrs;
                                if (insertBefore) {
                                    externalOpsBefore = [newReducedOperation];
                                } else {
                                    if (externalOpsAfter) {
                                        externalOpsAfter.unshift(newReducedOperation);
                                    } else {
                                        externalOpsAfter = [newReducedOperation];
                                    }
                                }
                            }

                        } else {
                            // If a text attribute is set at the paragraph [3], the more specific setting of a concurrent attribute
                            // from [3,2] to [3,6] will overwrite it -> no further operation required.
                        }
                    }

                } else {
                    // overlapping ranges -> 3 ranges:
                    //     - part only covered by localOp
                    //     - part only covered by extOp
                    //     - part covered by both operations (local operation wins)

                    // -> the external operation needs to get a shrinked range
                    if (comparePositions(localOp.start, extOp.start) <= 0) {
                        // modifying the external operation in that way, that only the missing part is changed in the local document
                        if (localOp.end.length === extOp.start.length) {
                            const extOpStartOrig = _.copy(extOp.start, true);
                            extOp.start = increaseLastIndex(localOp.end);

                            // a further new external operation is required for the common range of the operations, but with reduced attribute set
                            if (!emptyExtAttributes) {
                                newReducedOperation = json.deepClone(localOp);
                                newReducedOperation.attrs = reducedExtAttrs;
                                newReducedOperation.start = extOpStartOrig;
                                externalOpsBefore = [newReducedOperation]; // inserting before the current setAttributes operation
                            }
                        }
                    } else {
                        // modifying the external operation in that way, that only the missing part is removed from the local document
                        if (localOp.start.length === extOp.end.length) {
                            const extOpEndOrig = _.copy(extOp.end, true);
                            extOp.end = increaseLastIndex(localOp.start, -1);

                            // a further new external operation is required for the common range of the operations, but with reduced attribute set
                            if (!emptyExtAttributes) {
                                newReducedOperation = json.deepClone(localOp);
                                newReducedOperation.attrs = reducedExtAttrs;
                                newReducedOperation.end = extOpEndOrig;
                                externalOpsAfter = [newReducedOperation]; // inserting behind the current setAttributes operation
                            }
                        }
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling two operations of type updateField.
     *
     * { name: 'updateField', opl: 1, osn: 1, type: 'abc', representation: '123' }
     *
     * These two operations influence each other only, if they have the same start position.
     */
    handleUpdateFieldUpdateField(localOp, extOp) {

        if (equalPositions(localOp.start, extOp.start)) {

            if (localOp.type === extOp.type && localOp.representation === extOp.representation) {
                // if type and representation are the same, both operations can be ignored
                setOperationRemoved(extOp);
                setOperationRemoved(localOp);
            } else {
                // different type or representation: not applying external operation (this must be handled differently on server side)
                setOperationRemoved(extOp);
            }
        }
    }

    /**
     * Handling two operations of type insertParagraph or insertTable.
     */
    handleInsertCompInsertComp(localOp, extOp) {

        // the length of the local logical position
        const localLength = localOp.start.length;
        // the length of the external logical position
        const extLength = extOp.start.length;
        // the paragraph index of the logical position
        let paraIndex = 0;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;

        if (localLength === extLength) {
            if (localLength === 1 || equalPositions(localOp.start, extOp.start, localLength - 1)) {
                paraIndex = localLength - 1;
                if (localOp.start[paraIndex] < extOp.start[paraIndex]) { // on server side this is implemented with '<='
                    extOp.start[paraIndex]++;
                } else {
                    localOp.start[paraIndex]++;
                }
            }
        } else {
            // both positions have NOT the same length
            shortOp = localLength < extLength ? localOp : extOp;
            longOp = localLength < extLength ? extOp : localOp;

            paraIndex = shortOp.start.length - 1;

            if (paraIndex === 0 || equalPositions(shortOp.start, longOp.start, paraIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (shortOp.start[paraIndex] <= longOp.start[paraIndex]) {
                    longOp.start[paraIndex]++; // increase the paragraph position of the operation with the longer position
                }
            }
        }
    }

    /**
     * Handling two paragraph split operations.
     * Paragraph split operations influence each other, if:
     * - both paragraphs have the same (or none) parent.
     * - the paragraph with the longer logical position has the other paragraph as ancestor.
     */
    handleSplitParaSplitPara(localOp, extOp) {

        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let length = 0;

        if (localOp.start.length === extOp.start.length) {
            length = localOp.start.length; // both positions have the same length
            if (length === 2 || equalPositions(localOp.start, extOp.start, length - 2)) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = localOp.start.length - 2;
                textIndex = paraIndex + 1;
                if (localOp.start[paraIndex] < extOp.start[paraIndex]) {
                    extOp.start[paraIndex]++; // increasing external operation paragraph position
                } else if (localOp.start[paraIndex] > extOp.start[paraIndex]) {
                    localOp.start[paraIndex]++; // increasing internal operation paragraph position
                } else if (localOp.start[paraIndex] === extOp.start[paraIndex]) {
                    // both splits happened in the same paragraph -> did the local split happen before the external split
                    if (localOp.start[textIndex] < extOp.start[textIndex]) { // same position increases the internal operation (INFO: On server side this is implemented with '<=')
                        extOp.start[paraIndex]++;
                        extOp.start[textIndex] -= localOp.start[textIndex];
                    } else if (localOp.start[textIndex] >= extOp.start[textIndex]) {
                        localOp.start[paraIndex]++;
                        localOp.start[textIndex] -= extOp.start[textIndex];
                    }
                }
            }
        } else {
            // both positions have NOT the same length
            const shortOp = localOp.start.length < extOp.start.length ? localOp : extOp; // this has never changed
            const longOp = localOp.start.length < extOp.start.length ? extOp : localOp; // only this might have changed

            paraIndex = shortOp.start.length - 2;
            textIndex = paraIndex + 1;

            if (paraIndex === 0 || equalPositions(shortOp.start, longOp.start, paraIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (shortOp.start[paraIndex] < longOp.start[paraIndex]) {
                    longOp.start[paraIndex]++; // increase the paragraph position of the operation with the longer position
                } else if (shortOp.start[paraIndex] === longOp.start[paraIndex] && shortOp.start[textIndex] <= longOp.start[textIndex]) {
                    longOp.start[paraIndex]++;
                    longOp.start[textIndex] -= shortOp.start[textIndex];
                }
            }
        }
    }

    /**
     * Handling two table split operations.
     * Table split operations influence each other, if:
     * - both tables have the same (or none) parent.
     * - the table with the longer logical position has the other table as ancestor.
     */
    handleSplitTableSplitTable(localOp, extOp) {

        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the length of the logical position(s)
        let length = 0;

        if (localOp.start.length === extOp.start.length) {
            length = localOp.start.length; // both positions have the same length
            if (length === 2 || equalPositions(localOp.start, extOp.start, length - 2)) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                tableIndex = localOp.start.length - 2;
                rowIndex = tableIndex + 1;
                if (localOp.start[tableIndex] < extOp.start[tableIndex]) {
                    extOp.start[tableIndex]++; // increasing external operation table position
                } else if (localOp.start[tableIndex] > extOp.start[tableIndex]) {
                    localOp.start[tableIndex]++; // increasing internal operation table position
                } else if (localOp.start[tableIndex] === extOp.start[tableIndex]) {
                    // both splits happened in the same table -> did the local split happen before the external split
                    if (localOp.start[rowIndex] < extOp.start[rowIndex]) { // same position increases the internal operation (INFO: On server side this is implemented with '<=')
                        extOp.start[tableIndex]++;
                        extOp.start[rowIndex] -= localOp.start[rowIndex];
                    } else if (localOp.start[rowIndex] >= extOp.start[rowIndex]) {
                        localOp.start[tableIndex]++;
                        localOp.start[rowIndex] -= extOp.start[rowIndex];
                    }
                }
            }
        } else {
            // both positions have NOT the same length
            const shortOp = localOp.start.length < extOp.start.length ? localOp : extOp; // this has never changed
            const longOp = localOp.start.length < extOp.start.length ? extOp : localOp; // only this might have changed

            tableIndex = shortOp.start.length - 2;
            rowIndex = tableIndex + 1;

            if (tableIndex === 0 || equalPositions(shortOp.start, longOp.start, tableIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (shortOp.start[tableIndex] < longOp.start[tableIndex]) {
                    longOp.start[tableIndex]++; // increase the paragraph position of the operation with the longer position
                } else if (shortOp.start[tableIndex] === longOp.start[tableIndex] && shortOp.start[rowIndex] <= longOp.start[rowIndex]) {
                    longOp.start[tableIndex]++;
                    longOp.start[rowIndex] -= shortOp.start[rowIndex];
                }
            }
        }
    }

    /**
     * Handling two paragraph merge operations or two table merge operations.
     * Merge operations influence each other, if:
     * - both paragraphs/tables have the same (or none) parent.
     * - the paragraph/table with the longer logical position has the other paragraph as ancestor.
     *
     * This function can also be used, if localOp and extOp are different. It is supported that one
     * operation is a mergeTable operation and the other one is a mergeParagraph operation.
     */
    handleMergeCompMergeComp(localOp, extOp) {

        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the paragraph inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let length = 0;
        // the name of the property containing the length of the leading paragraph/table
        let lengthProperty = null;
        // the name of the property containing the length of the leading paragraph/table of the shorter operation
        let shortLengthProperty = null;

        if (localOp.start.length === extOp.start.length) {
            length = localOp.start.length; // both positions have the same length
            if (length === 1 || equalPositions(localOp.start, extOp.start, length - 1)) {
                // both positions are top level (length === 1) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = localOp.start.length - 1; // the paragraph index is always the last index
                lengthProperty = localOp.name === Op.PARA_MERGE ? 'paralength' : 'rowcount';
                if (localOp.start[paraIndex] < extOp.start[paraIndex]) {
                    extOp.start[paraIndex]--; // decreasing external operation paragraph position
                    if (extOp.start[paraIndex] === localOp.start[paraIndex]) { extOp[lengthProperty] += localOp[lengthProperty]; }
                } else if (localOp.start[paraIndex] > extOp.start[paraIndex]) {
                    localOp.start[paraIndex]--; // decreasing internal operation paragraph position
                    if (extOp.start[paraIndex] === localOp.start[paraIndex]) { localOp[lengthProperty] += extOp[lengthProperty]; }
                } else if (localOp.start[paraIndex] === extOp.start[paraIndex]) {
                    // both merges happened in the same paragraph -> they are identical and can be ignored
                    setOperationRemoved(extOp); // setting marker at external operation
                    setOperationRemoved(localOp); // setting marker at local operation
                }
            }
        } else {
            // both positions have NOT the same length
            const shortOp = localOp.start.length < extOp.start.length ? localOp : extOp; // this has never changed
            const longOp = localOp.start.length < extOp.start.length ? extOp : localOp; // only this might have changed

            paraIndex = shortOp.start.length - 1;
            textIndex = paraIndex + 1;

            if (paraIndex === 0 || equalPositions(shortOp.start, longOp.start, paraIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (shortOp.start[paraIndex] === longOp.start[paraIndex] - 1) {
                    shortLengthProperty = shortOp.name === Op.PARA_MERGE ? 'paralength' : 'rowcount';
                    longOp.start[paraIndex]--; // decrease the paragraph position of the operation with the longer position
                    longOp.start[textIndex] += (shortOp[shortLengthProperty] ? shortOp[shortLengthProperty] : 0);
                } else if (shortOp.start[paraIndex] < longOp.start[paraIndex]) {
                    longOp.start[paraIndex]--; // decrease the paragraph position of the operation with the longer position
                }
            }
        }
    }

    /**
     * Handling two insertRows/insertCells operations.
     */
    handleInsertRowsInsertRows(localOp, extOp) {

        // the length of the local logical position
        const localLength = localOp.start.length;
        // the length of the external logical position
        const extLength = extOp.start.length;
        // the row/cell index of the logical position
        let rowIndex = 0;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;
        // the number of inserted rows/cells
        let rowCount = 1;

        if (localLength === extLength) {
            if (equalPositions(localOp.start, extOp.start, localLength - 1)) {
                rowIndex = localLength - 1;
                if (localOp.start[rowIndex] < extOp.start[rowIndex]) { // this is implemented on server side with '<='
                    rowCount = localOp.count || 1;
                    extOp.start[rowIndex] += rowCount;
                    if ('referenceRow' in extOp && extOp.referenceRow >= localOp.start[rowIndex]) { extOp.referenceRow += rowCount; }
                } else {
                    rowCount = extOp.count || 1;
                    localOp.start[rowIndex] += rowCount;
                    if ('referenceRow' in localOp && localOp.referenceRow >= extOp.start[rowIndex]) { localOp.referenceRow += rowCount; }
                }
            }
        } else {
            // both positions have NOT the same length
            shortOp = localLength < extLength ? localOp : extOp;
            longOp = localLength < extLength ? extOp : localOp;

            rowIndex = shortOp.start.length - 1;

            if (equalPositions(shortOp.start, longOp.start, rowIndex)) { // both positions must be inside the same table
                // the shorter row is in a top level table (index is 1) or the parent of the row of the shorter op is an ancestor also for the longer op
                if (shortOp.start[rowIndex] <= longOp.start[rowIndex]) {
                    rowCount = shortOp.count || 1;
                    longOp.start[rowIndex] += rowCount; // increase the row position of the operation with the longer position
                }
            }
        }
    }

    /**
     * Handling two insertColumn operations.
     *
     * Info: The table grid must be merged, if both operations are applied to the same table.
     *
     * Example:
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     */
    handleInsertColumnInsertColumn(localOp, extOp) {

        // the length of the local logical position
        const localLength = localOp.start.length;
        // the length of the external logical position
        const extLength = extOp.start.length;
        // the table index of the logical position
        let tableIndex = 0;
        // the column index of the logical position
        let columnIndex = 0;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;
        // the specification, where the column is inserted for the local operation
        const localMode = localOp.insertMode || 'behind';
        // the specification, where the column is inserted for the external operation
        const extMode = extOp.insertMode || 'behind';
        // the new table grid that contains the grid after both operations are applied
        let newTableGrid = null;

        // the table grid(s) of the operations must be adapted, because there is one additional column locally and on server side
        const calculateMergedTableGrid = tableGrid => {
            const localInsertedIndex = localOp.gridPosition;
            const tableGridLength = tableGrid.length;
            const insertIndex = localMode === 'behind' ? localInsertedIndex + 1 : localInsertedIndex;
            const columnWidth = tableGridLength > localInsertedIndex ? tableGrid[localInsertedIndex] : tableGrid[tableGridLength - 1];

            tableGrid.splice(insertIndex, 0, columnWidth);
            return tableGrid;
        };

        if (localLength === extLength) {
            if (equalPositions(localOp.start, extOp.start, localLength)) { // both operations in the same table

                tableIndex = localLength - 1;
                columnIndex = tableIndex + 2;

                if (localOp.gridPosition > extOp.gridPosition) {
                    localOp.gridPosition++;
                } else if (localOp.gridPosition < extOp.gridPosition) {
                    extOp.gridPosition++;
                } else {
                    // both grid positions are equal
                    if (localMode === extMode) {
                        localOp.gridPosition++; // increasing the local position
                    } else if (localMode === 'behind') {
                        localOp.gridPosition++; // increasing the local position
                    } else if (extMode === 'behind') {
                        extOp.gridPosition++; // increasing the external position
                    }
                }

                newTableGrid = calculateMergedTableGrid(json.deepClone(extOp.tableGrid)); // using the external grid as base for the new merged grid

                // assigning the new table grid to both operations.
                // - The external operation needs the new grid to be applied locally with the already applied local operation.
                // - The local operation needs the new grid for further external operations and especially, if it is sent to the server
                //   later, because on server side the external operation is already applied.
                extOp.tableGrid = newTableGrid;
                localOp.tableGrid = json.deepClone(newTableGrid);
            }
        } else {
            // both positions have NOT the same length
            shortOp = localLength < extLength ? localOp : extOp;
            longOp = localLength < extLength ? extOp : localOp;

            tableIndex = shortOp.start.length - 1;
            columnIndex = tableIndex + 2;

            if (equalPositions(shortOp.start, longOp.start, shortOp.start.length)) { // both positions must be inside the same table
                if ((longOp.start[columnIndex] > shortOp.gridPosition) || (shortOp.insertMode === 'before' && longOp.start[columnIndex] === shortOp.gridPosition)) {
                    longOp.start[columnIndex]++;
                }
            }
        }
    }

    /**
     * Handling two deleteColumns operations.
     *
     * TODO: Check, if the table grid must be merged, if both operations are applied to the same table.
     *
     * Example:
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     */
    handleDeleteColumnsDeleteColumns(localOp, extOp) {

        // the length of the local logical position
        const localLength = localOp.start.length;
        // the length of the external logical position
        const extLength = extOp.start.length;
        // the table index of the logical position
        let tableIndex = 0;
        // the column index of the logical position
        let columnIndex = 0;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;
        // the number of deleted columns of the local operation
        let localDeleteColumns = 1;
        // the number of deleted columns of the external operation
        let extDeleteColumns = 1;
        // the number of deleted columns
        let deleteColumns = 0;

        if (localLength === extLength) {
            if (equalPositions(localOp.start, extOp.start, localLength)) { // both operations in the same table

                tableIndex = localLength - 1;
                columnIndex = tableIndex + 2;

                if (localOp.startGrid === extOp.startGrid && localOp.endGrid === extOp.endGrid) { // identical ranges
                    setOperationRemoved(localOp); // setting marker at internal operation!
                    setOperationRemoved(extOp); // setting marker at external operation
                } else if (localOp.endGrid < extOp.startGrid) { // local range before external range
                    localDeleteColumns = localOp.endGrid - localOp.startGrid + 1;
                    extOp.startGrid -= localDeleteColumns;
                    extOp.endGrid -= localDeleteColumns;
                } else if (extOp.endGrid < localOp.startGrid) { // external range before local range
                    extDeleteColumns = extOp.endGrid - extOp.startGrid + 1;
                    localOp.startGrid -= extDeleteColumns;
                    localOp.endGrid -= extDeleteColumns;
                } else if (localOp.startGrid >= extOp.startGrid && localOp.endGrid <= extOp.endGrid) { // local range inside external range
                    localDeleteColumns = localOp.endGrid - localOp.startGrid + 1;
                    extOp.endGrid -= localDeleteColumns;
                    setOperationRemoved(localOp); // setting marker at internal operation!
                } else if (extOp.startGrid >= localOp.startGrid && extOp.endGrid <= localOp.endGrid) { // external range inside local range
                    extDeleteColumns = extOp.endGrid - extOp.startGrid + 1;
                    localOp.endGrid -= extDeleteColumns;
                    setOperationRemoved(extOp); // setting marker at external operation
                } else if (localOp.startGrid <= extOp.startGrid) { // overlapping ranges (local range before external range)
                    // example: local [1..7], extern [3..16]
                    deleteColumns = localOp.endGrid - localOp.startGrid + 1; // 7 (number of deleted columns by local operation)
                    localOp.endGrid = extOp.startGrid - 1; // [1..2]
                    extOp.startGrid = localOp.startGrid; // 1
                    extOp.endGrid -= deleteColumns; // 9
                } else if (localOp.startGrid > extOp.startGrid) { // overlapping ranges (local range after external range)
                    deleteColumns = extOp.endGrid - extOp.startGrid + 1; // number of deleted columns by external operation
                    extOp.endGrid = localOp.startGrid - 1;
                    localOp.startGrid = extOp.startGrid;
                    localOp.endGrid -= deleteColumns;
                }

                // newTableGrid = calculateMergedTableGrid(clone(extOp.tableGrid)); // TODO: Is is necessary to calculate a new table grid?
                // assigning the new table grid to both operations.
                // - The external operation needs the new grid to be applied locally with the already applied local operation.
                // - The local operation needs the new grid for further external operations and especially, if it is sent to the server
                //   later, because on server side the external operation is already applied.
                // extOp.tableGrid = newTableGrid;
                // localOp.tableGrid = clone(newTableGrid);
            }
        } else {
            // both positions have NOT the same length
            shortOp = localLength < extLength ? localOp : extOp;
            longOp = localLength < extLength ? extOp : localOp;

            tableIndex = shortOp.start.length - 1;
            columnIndex = tableIndex + 2;

            if (equalPositions(shortOp.start, longOp.start, tableIndex + 1) && (longOp.start[columnIndex] >= shortOp.startGrid)) {
                if (longOp.start[columnIndex] > shortOp.endGrid) {
                    deleteColumns = shortOp.endGrid - shortOp.startGrid + 1;
                    longOp.start[columnIndex] -= deleteColumns;
                } else {
                    // the insert operation cannot be applied, because its column is already removed
                    setOperationRemoved(longOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                }
            }
        }
    }

    /**
     * Handling two delete operations.
     *
     * The following scenarios are considered;
     *  Case 1: The two delete ranges are completely separated.
     *  Case 2: The two delete ranges are identical.
     *  Case 3: One delete range is completely included in the other range.
     *  Case 4: The two delete ranges are overlapping partly.
     */
    handleDeleteDelete(localOp, extOp) {

        // the array containing the shift of position
        let deleteArray;
        // an index counter
        let index = 0;
        // an optionally existing end position of a delete operation
        let endPosition;
        // whether the two delete ranges surround each other
        let surrounding = false;

        if (separateRanges(localOp, extOp)) {

            if (comparePositions(localOp.start, extOp.start) < 0) {
                endPosition = localOp.end ? localOp.end : localOp.start;
                deleteArray = calculateDeleteArray(localOp.start, endPosition, extOp.start);
                for (index = 0; index < extOp.start.length; index += 1) { extOp.start[index] -= deleteArray[index]; }

                if (extOp.end) {
                    deleteArray = calculateDeleteArray(localOp.start, endPosition, extOp.end);
                    for (index = 0; index < extOp.end.length; index += 1) { extOp.end[index] -= deleteArray[index]; }
                }
            } else {
                endPosition = extOp.end ? extOp.end : extOp.start;
                deleteArray = calculateDeleteArray(extOp.start, endPosition, localOp.start);
                for (index = 0; index < localOp.start.length; index += 1) { localOp.start[index] -= deleteArray[index]; }

                if (localOp.end) {
                    deleteArray = calculateDeleteArray(extOp.start, endPosition, localOp.end);
                    for (index = 0; index < localOp.end.length; index += 1) { localOp.end[index] -= deleteArray[index]; }
                }
            }

        } else if (identicalRanges(localOp, extOp))  {

            // setting marker at both operations
            setOperationRemoved(localOp); // setting marker at internal operation!
            setOperationRemoved(extOp); // setting marker at external operation

        } else {

            surrounding = surroundingRanges(localOp, extOp);

            if (surrounding) {

                if (surrounding === 1) {
                    // local surrounds external operation -> shrinking the local operation range and ignoring the external operation
                    if (localOp.end) {
                        endPosition = extOp.end ? extOp.end : extOp.start;
                        deleteArray = calculateDeleteArray(extOp.start, endPosition, localOp.end);
                        for (index = 0; index < localOp.end.length; index += 1) { localOp.end[index] -= deleteArray[index]; }
                    }
                    setOperationRemoved(extOp); // setting marker at external operation
                } else {
                    // external surrounds local operation -> ignoring the local operation and shrinking the external operation range
                    if (extOp.end) {
                        endPosition = localOp.end ? localOp.end : localOp.start;
                        deleteArray = calculateDeleteArray(localOp.start, endPosition, extOp.end);
                        for (index = 0; index < extOp.end.length; index += 1) { extOp.end[index] -= deleteArray[index]; }
                    }
                    setOperationRemoved(localOp); // setting marker at internal operation
                }

            } else {
                // overlapping ranges -> 3 new ranges:
                //     - part only covered by localOp
                //     - part only covered by extOp
                //     - part covered by both operations (content already deleted by localOp)

                const origLocalStart = json.deepClone(localOp.start);
                const origLocalEnd = json.deepClone(localOp.end);

                // -> both operations need to get shrinked ranges
                if (comparePositions(localOp.start, extOp.start) <= 0) {

                    // shrinking the range of the local operation -> leaving only the part covered only by the local operation
                    // recalculating the range of the external operation, that can be applied to the local document
                    localOp.end = increaseLastIndex(extOp.start, -1); // TODO: Not reliable (both must have same length and same parent, fails at position 0)
                    // modifying the external operation in that way, that only the missing part is removed from the local document
                    extOp.start = increaseLastIndex(origLocalEnd);

                    // shifting the two positions of the external operation to the front
                    deleteArray = calculateDeleteArray(origLocalStart, origLocalEnd, extOp.start);
                    for (index = 0; index < extOp.start.length; index += 1) { extOp.start[index] -= deleteArray[index]; }
                    deleteArray = calculateDeleteArray(origLocalStart, origLocalEnd, extOp.end);
                    for (index = 0; index < extOp.end.length; index += 1) { extOp.end[index] -= deleteArray[index]; }

                } else {

                    const origExtStart = json.deepClone(extOp.start);
                    const origExtEnd = json.deepClone(extOp.end);

                    localOp.start = increaseLastIndex(extOp.end); // TODO the common part becomes part of the external operation
                    // modifying the external operation in that way, that only the missing part is removed from the local document
                    extOp.end = increaseLastIndex(origLocalStart, -1);

                    // shifting the two positions of the local operation to the front
                    deleteArray = calculateDeleteArray(origExtStart, origExtEnd, localOp.start);
                    for (index = 0; index < localOp.start.length; index += 1) { localOp.start[index] -= deleteArray[index]; }
                    deleteArray = calculateDeleteArray(origExtStart, origExtEnd, localOp.end);
                    for (index = 0; index < localOp.end.length; index += 1) { localOp.end[index] -= deleteArray[index]; }
                }
            }
        }
    }

    /**
     * Handling of insertParagraph operation with an insertChar operation.
     * insertParagraph operation and insertChar operation influence each other, if:
     * - both paragraphs have the same (or none) parent.
     * - the paragraph of the insertChar operation has the longer logical position.
     *
     * Important: This function can also be used for insertTable and insertChar.
     */
    handleInsertCompInsertChar(localOp, extOp) {

        // the insert operation
        const insertParaOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the insert operation
        const insertCharOp = (localOp === insertParaOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the insert operation
        const insertParaLength = insertParaOp.start.length;
        // the default length of the insertChar operation
        let insertCharLength = 2;

        if (comparePositions(insertParaOp.start, insertCharOp.start) <= 0) {
            // the paragraphs must have the same parent to influence the insertChar position
            paraIndex = insertParaLength - 1;
            if (paraIndex === 0 || (insertCharOp.start.length > insertParaLength && equalPositions(insertCharOp.start, insertParaOp.start, paraIndex))) {
                insertCharOp.start[paraIndex]++;
            }
        } else {
            // the paragraph might be inside a drawing
            insertCharLength = insertCharOp.start.length;
            textIndex = insertCharLength - 1;
            if (insertCharLength < insertParaLength && equalPositions(insertCharOp.start, insertParaOp.start, insertCharLength - 1)) {
                if (insertCharOp.start[textIndex] <= insertParaOp.start[textIndex]) {
                    insertParaOp.start[textIndex] += getInsertCharLength(insertCharOp);
                }
            }
        }
    }

    /**
     * Handling of insertParagraph/insertTable operation with an insertRows operation.
     */
    handleInsertCompInsertRows(localOp, extOp) {

        // the insert operation
        const insertParaOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the insertRows operation
        const insertRowsOp = (localOp === insertParaOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the length of the insert operation
        const insertParaLength = insertParaOp.start.length;
        // the index of the row/cell inside the logical position
        let rowIndex = 0;
        // the length of the logical position of the insertRows/insertCells operation
        const insertRowsLength = insertRowsOp.start.length;
        // the number of inserted rows/cells
        const rowCount = insertRowsOp.count || 1;

        if (comparePositions(insertRowsOp.start, insertParaOp.start) <= 0) {
            // the table must be the parent for both positions to influence the insert position
            rowIndex = insertRowsLength - 1;

            if ((insertParaLength > insertRowsLength && equalPositions(insertParaOp.start, insertRowsOp.start, insertRowsLength - 1))) {
                if (insertParaOp.start[rowIndex] >= insertRowsOp.start[rowIndex]) { // position inside a following row/cell
                    insertParaOp.start[rowIndex] += rowCount;
                }
            }
        } else {

            paraIndex = insertParaLength - 1;

            if (paraIndex === 0 || (insertRowsLength > insertParaLength && equalPositions(insertRowsOp.start, insertParaOp.start, insertParaLength - 1))) {
                insertRowsOp.start[paraIndex]++;
            }
        }
    }

    /**
     * Handling of insertCells operation with an insertRows operation.
     */
    handleInsertCellsInsertRows(localOp, extOp) {

        // the insertRows operation
        const insertRowsOp = localOp.name === Op.ROWS_INSERT ? localOp : extOp;
        // the insertCells operation
        const insertCellsOp = localOp.name === Op.CELLS_INSERT ? localOp : extOp;
        // the length of the logical position of the insertRows operation
        const insertRowsLength = insertRowsOp.start.length;
        // the length of the logical position of the insertCells operation
        const insertCellsLength = insertCellsOp.start.length;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the index of the cell inside the logical position
        let cellIndex = 0;
        // the number of inserted rows
        const rowCount = insertRowsOp.count || 1;
        // the number of inserted cells
        const cellCount = insertCellsOp.count || 1;

        if (comparePositions(insertRowsOp.start, insertCellsOp.start) <= 0) {
            // the table must be the parent for both positions to influence the insertCells position
            rowIndex = insertRowsLength - 1;
            if ((insertCellsLength > insertRowsLength && equalPositions(insertCellsOp.start, insertRowsOp.start, rowIndex))) {
                if (insertCellsOp.start[rowIndex] >= insertRowsOp.start[rowIndex]) { // position inside a following row
                    insertCellsOp.start[rowIndex] += rowCount;
                }
            }
        } else {
            cellIndex = insertCellsLength - 1;
            if (insertRowsLength > insertCellsLength && equalPositions(insertRowsOp.start, insertCellsOp.start, cellIndex)) {
                insertRowsOp.start[cellIndex] += cellCount;
            }
        }
    }

    /**
     * Handling of insertParagraph/insertTable operation with an insertColumn operation.
     *
     * Example:
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     */
    handleInsertCompInsertColumn(localOp, extOp) {

        // the inser operation
        const insertParaOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the insertColumn operation
        const insertColumnOp = (localOp === insertParaOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the length of the insert operation
        const insertParaLength = insertParaOp.start.length;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the column inside the logical position
        let columnIndex = 0;
        // the length of the logical position of the insertColumn operation
        const insertColumnLength = insertColumnOp.start.length;
        // the specification, where the column is inserted
        const mode = insertColumnOp.insertMode || 'behind';

        if (comparePositions(insertColumnOp.start, insertParaOp.start) < 0) {
            // the table must be the parent for both positions to influence the insert position
            if ((insertParaLength > insertColumnLength + 1 && equalPositions(insertParaOp.start, insertColumnOp.start, insertColumnLength))) {
                tableIndex = insertColumnLength - 1;
                columnIndex = tableIndex + 2;
                if ((insertParaOp.start[columnIndex] > insertColumnOp.gridPosition) || (mode === 'before' && insertParaOp.start[columnIndex] === insertColumnOp.gridPosition)) {
                    insertParaOp.start[columnIndex]++;
                }
            }
        } else {
            paraIndex = insertParaLength - 1;
            if (paraIndex === 0 || (insertColumnLength > insertParaLength && equalPositions(insertColumnOp.start, insertParaOp.start, insertParaLength - 1))) {
                insertColumnOp.start[paraIndex]++;
            }
        }
    }

    /**
     * Handling of insertParagraph/insertTable operation with a deleteColumns operation.
     *
     * Example:
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     */
    handleInsertCompDeleteColumns(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the insertColumn operation
        const deleteOp = (localOp === insertOp) ? extOp : localOp;
        // the length of the insert operation
        const insertParaLength = insertOp.start.length;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the column inside the logical position
        let columnIndex = 0;
        // the length of the logical position of the deleteColumns operation
        const deleteLength = deleteOp.start.length;
        // the number of deleted columns
        let deleteColumns = 1;

        if (comparePositions(deleteOp.start, insertOp.start) < 0) {
            // the table must be the parent for both positions to influence the insert position
            if ((insertParaLength > deleteLength + 1 && equalPositions(insertOp.start, deleteOp.start, deleteLength))) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (insertOp.start[columnIndex] >= deleteOp.startGrid) {
                    if (insertOp.start[columnIndex] > deleteOp.endGrid) {
                        deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                        insertOp.start[columnIndex] -= deleteColumns;
                    } else {
                        // the insert operation cannot be applied, because its column is already removed
                        setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                    }
                }
            }
        } else {
            paraIndex = insertParaLength - 1;
            if (paraIndex === 0 || (deleteLength > insertParaLength && equalPositions(deleteOp.start, insertOp.start, insertParaLength - 1))) {
                deleteOp.start[paraIndex]++;
            }
        }
    }

    /**
     * Handling of insertRows operation with an insertColumn operation.
     *
     * Example:
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     *
     * Both operations only influence each other only in the same table, but not on the same table level.
     * The influenced operation must be at least a table in a table.
     */
    handleInsertRowsInsertColumn(localOp, extOp) {

        // the insertRows operation
        const insertRowsOp = localOp.name === Op.ROWS_INSERT ? localOp : extOp;
        // the insertColumn operation
        const insertColumnOp = localOp.name === Op.COLUMN_INSERT ? localOp : extOp;
        // the length of the logical position of the insertRows operation
        const insertRowsLength = insertRowsOp.start.length;
        // the length of the logical position of the insertColumn operation
        const insertColumnLength = insertColumnOp.start.length;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the index of the column inside the logical position
        let columnIndex = 0;
        // the specification, where the column is inserted
        const mode = insertColumnOp.insertMode || 'behind';
        // number of rows to be inserted
        const rowCount = insertRowsOp.count || 1;

        // both operations do not influence each other on same table level
        if (insertColumnLength + 1 === insertRowsLength) { return; }

        if (insertColumnLength < insertRowsLength) {
            // the table must be the parent for both positions to influence each other
            if (equalPositions(insertRowsOp.start, insertColumnOp.start, insertColumnLength)) {
                tableIndex = insertColumnLength - 1;
                columnIndex = tableIndex + 2;
                if ((insertRowsOp.start[columnIndex] > insertColumnOp.gridPosition) || (mode === 'before' && insertRowsOp.start[columnIndex] === insertColumnOp.gridPosition)) {
                    insertRowsOp.start[columnIndex]++; // inserting row(s) in table in table
                }
            }
        } else {
            // the table must be the parent for both positions to influence each other
            if (equalPositions(insertRowsOp.start, insertColumnOp.start, insertRowsLength - 1)) {
                rowIndex = insertRowsLength - 1;
                if ((insertRowsOp.start[rowIndex] <= insertColumnOp.start[rowIndex])) {
                    insertColumnOp.start[rowIndex] += rowCount; // inserting column in table in table
                }
            }
        }
    }

    /**
     * Handling of insertRows operation with a deleteColumns operation.
     *
     * Example:
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     *
     * Both operations only influence each other only in the same table, but not on the same table level.
     * The influenced operation must be at least a table in a table.
     */
    handleDeleteColumnsInsertRows(localOp, extOp) {

        // the insertRows operation
        const insertOp = localOp.name === Op.ROWS_INSERT ? localOp : extOp;
        // the deleteColumns operation
        const deleteOp = localOp.name === Op.COLUMNS_DELETE ? localOp : extOp;
        // the length of the logical position of the insertRows operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the deleteColumns operation
        const deleteLength = deleteOp.start.length;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the index of the column inside the logical position
        let columnIndex = 0;
        // number of rows to be inserted
        const rowCount = insertOp.count || 1;
        // the number of deleted columns
        let deleteColumns = 1;

        // both operations do not influence each other on same table level
        if (deleteLength + 1 === insertLength) { return; }

        if (deleteLength < insertLength) {
            // the table must be the parent for both positions to influence each other
            if (equalPositions(insertOp.start, deleteOp.start, deleteLength)) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (insertOp.start[columnIndex] >= deleteOp.startGrid) {
                    if (insertOp.start[columnIndex] > deleteOp.endGrid) {
                        deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                        insertOp.start[columnIndex] -= deleteColumns;
                    } else {
                        // the insert operation cannot be applied, because its column is already removed
                        setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                    }
                }
            }
        } else {
            // the table must be the parent for both positions to influence each other
            if (insertLength === 1 || equalPositions(insertOp.start, deleteOp.start, insertLength - 1)) {
                rowIndex = insertLength - 1;
                if ((insertOp.start[rowIndex] <= deleteOp.start[rowIndex])) {
                    deleteOp.start[rowIndex] += rowCount;
                }
            }
        }
    }

    /**
     * Handling of insertColumn operation with a deleteColumns operation.
     *
     * Example:
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     *
     * Both operations only influence each other only in the same table, but not on the same table level.
     * The influenced operation must be at least a table in a table.
     *
     * TODO: It might be necessary to modify the 'tableGrid' property.
     */
    handleInsertColumnDeleteColumns(localOp, extOp) {

        // the insertColumns operation
        const insertOp = localOp.name === Op.COLUMN_INSERT ? localOp : extOp;
        // the deleteColumns operation
        const deleteOp = localOp.name === Op.COLUMNS_DELETE ? localOp : extOp;
        // the length of the logical position of the insertRows operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the deleteColumns operation
        const deleteLength = deleteOp.start.length;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the column inside the logical position
        let columnIndex = 0;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';
        // the number of deleted columns
        let deleteColumns = 1;

        if (deleteLength === insertLength) {
            if (equalPositions(insertOp.start, deleteOp.start, deleteLength)) { // same table -> only modifying attributes 'startGrid', 'endGrid', 'gridPosition'
                if (insertOp.gridPosition <= deleteOp.startGrid) {
                    deleteOp.startGrid++;
                    deleteOp.endGrid++;
                } else if ((insertOp.gridPosition <= deleteOp.endGrid)) {
                    deleteOp.endGrid++;
                    // the insert operation cannot be applied, because its column is already removed
                    setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                } else {
                    deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                    insertOp.gridPosition -= deleteColumns; // shifting the insert operation
                }
            }
        } else if (deleteLength < insertLength) {
            if (equalPositions(insertOp.start, deleteOp.start, deleteLength)) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (insertOp.start[columnIndex] >= deleteOp.startGrid) {
                    if (insertOp.start[columnIndex] > deleteOp.endGrid) {
                        deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                        insertOp.start[columnIndex] -= deleteColumns;
                    } else {
                        // the insert operation cannot be applied, because its column is already removed
                        setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                    }
                }
            }
        } else { // insertLength is smaller than deleteLength
            if (equalPositions(insertOp.start, deleteOp.start, insertLength)) {
                tableIndex = insertLength - 1;
                columnIndex = tableIndex + 2;
                if (is.number(deleteOp.start[columnIndex]) && (insertOp.gridPosition < deleteOp.start[columnIndex] || (mode === 'before' && insertOp.gridPosition === deleteOp.start[columnIndex]))) {
                    deleteOp.start[columnIndex]++;
                }
            }
        }
    }

    /**
     * Handling of insertCells operation with an insertColumn operation.
     *
     * Example:
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     *
     * Both operations only influence each other only in the same table.
     */
    handleInsertCellsInsertColumn(localOp, extOp) {

        // the insertCells operation
        const insertCellsOp = localOp.name === Op.CELLS_INSERT ? localOp : extOp;
        // the insertColumn operation
        const insertColumnOp = localOp.name === Op.COLUMN_INSERT ? localOp : extOp;
        // the length of the logical position of the insertCells operation
        const insertCellsLength = insertCellsOp.start.length;
        // the length of the logical position of the insertColumn operation
        const insertColumnLength = insertColumnOp.start.length;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the column inside the logical position
        let cellIndex = 0;
        // the specification, where the column is inserted
        const mode = insertColumnOp.insertMode || 'behind';
        // number of cells to be inserted
        const cellCount = insertCellsOp.count || 1;

        if (insertColumnLength < insertCellsLength) {
            // the table must be the parent for both positions to influence each other
            if (equalPositions(insertCellsOp.start, insertColumnOp.start, insertColumnLength)) {
                tableIndex = insertColumnLength - 1;
                cellIndex = tableIndex + 2;
                if ((insertCellsOp.start[cellIndex] > insertColumnOp.gridPosition) || (mode === 'before' && insertCellsOp.start[cellIndex] === insertColumnOp.gridPosition)) {
                    insertCellsOp.start[cellIndex]++;
                }
            }
        } else {
            // the row must be the parent for both positions to influence each other,
            // so that the insertColumn operation needs to be changed.
            if (equalPositions(insertCellsOp.start, insertColumnOp.start, insertCellsLength - 1)) {
                cellIndex = insertCellsLength - 1;
                if ((insertCellsOp.start[cellIndex] <= insertColumnOp.start[cellIndex])) {
                    insertColumnOp.start[cellIndex] += cellCount; // inserting column in table in table
                }
            }
        }
    }

    /**
     * Handling of insertCells operation with a deleteColumns operation.
     *
     * Example:
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     *
     * Both operations only influence each other only in the same table.
     */
    handleInsertCellsDeleteColumns(localOp, extOp) {

        // the insertCells operation
        const insertOp = localOp.name === Op.CELLS_INSERT ? localOp : extOp;
        // the deleteColumns operation
        const deleteOp = localOp.name === Op.COLUMNS_DELETE ? localOp : extOp;
        // the length of the logical position of the insertCells operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the insertColumn operation
        const deleteLength = deleteOp.start.length;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the column inside the logical position
        let cellIndex = 0;
        // number of cells to be inserted
        const cellCount = insertOp.count || 1;
        // the number of deleted columns
        let deleteColumns = 1;

        if (deleteLength < insertLength) {
            // the table must be the parent for both positions to influence each other
            if (equalPositions(insertOp.start, deleteOp.start, deleteLength)) {
                tableIndex = deleteLength - 1;
                cellIndex = tableIndex + 2;

                if (insertOp.start[cellIndex] >= deleteOp.startGrid) {
                    if (insertOp.start[cellIndex] > deleteOp.endGrid) {
                        deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                        insertOp.start[cellIndex] -= deleteColumns;
                    } else {
                        // the insert operation cannot be applied, because its column is removed
                        setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                    }
                }
            }
        } else {
            // the row must be the parent for both positions to influence each other,
            // so that the deleteColumns operation needs to be changed.
            if (equalPositions(insertOp.start, deleteOp.start, insertLength - 1)) {
                cellIndex = insertLength - 1;
                if ((insertOp.start[cellIndex] <= deleteOp.start[cellIndex])) {
                    deleteOp.start[cellIndex] += cellCount; // inserting column in table in table
                }
            }
        }
    }

    /**
     * Handling of insertParagraph/insertTable and splitParagraph.
     */
    handleInsertCompSplitPara(localOp, extOp) {

        // the insert operation
        const insertParaOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the insertRows operation
        const splitParaOp = (insertParaOp === localOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the insert operation
        const insertParaLength = insertParaOp.start.length;
        // the length of the logical position of the insertRows operation
        const splitParaLength = splitParaOp.start.length;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;

        if (splitParaLength === insertParaLength + 1) {
            if (insertParaLength === 1 || equalPositions(splitParaOp.start, insertParaOp.start, insertParaLength - 1)) {
                paraIndex = insertParaLength - 1;
                if (insertParaOp.start[paraIndex] <= splitParaOp.start[paraIndex]) {
                    splitParaOp.start[paraIndex]++;
                } else {
                    insertParaOp.start[paraIndex]++;
                }
            }
        } else {
            // both positions have NOT the same length
            shortOp = insertParaLength < (splitParaLength - 1) ? insertParaOp : splitParaOp;
            longOp = insertParaLength < (splitParaLength - 1) ? splitParaOp : insertParaOp;

            paraIndex = (shortOp === insertParaOp) ? shortOp.start.length - 1 : shortOp.start.length - 2;
            textIndex = paraIndex + 1;

            if (paraIndex === 0 || equalPositions(shortOp.start, longOp.start, paraIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (shortOp.start[paraIndex] < longOp.start[paraIndex]) {
                    longOp.start[paraIndex]++; // increase the paragraph position of the operation with the longer position
                } else if (shortOp.start[paraIndex] === longOp.start[paraIndex]) {
                    if (shortOp === splitParaOp && shortOp.start[textIndex] <= longOp.start[textIndex]) {
                        longOp.start[paraIndex]++; // the insert operation might be in a text frame
                        longOp.start[textIndex] -= shortOp.start[textIndex];
                    } else if (shortOp === insertParaOp) {
                        longOp.start[paraIndex]++;
                    }
                }
            }
        }
    }

    /**
     * Handling of insertParagraph/insertTable and mergeParagraph/mergeTable.
     *
     * Problematic case:
     * Local 'mergeParagraph [0]' and external 'insertTable [1]' or also 'mergeParagraph [0]'
     * and external 'insertParagraph [1]'  (any possible combination with paragraph and table)
     * -> the server would have to merge paragraph at [0] and paragraph at [2] (or table at[0]
     *    with table at [2]).
     * -> Solution: Making a local split to undo the merge operation before inserting the new
     *    table or paragraph.
     */
    handleMergeCompInsertComp(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the merge operation
        const mergeOp = (insertOp === localOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the insert operation
        const insertParaLength = insertOp.start.length;
        // the length of the logical position of the merge operation
        const mergeParaLength = mergeOp.start.length;
        // the operation with the shorter length of the logical position
        let shortOp = null;
        // the operation with the longer length of the logical position
        let longOp = null;
        // a new operation that might be required
        let newOperation = null;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';
        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        if (mergeParaLength === insertParaLength) {
            if (insertParaLength === 1 || equalPositions(mergeOp.start, insertOp.start, insertParaLength - 1)) {
                paraIndex = insertParaLength - 1;
                if (insertOp.start[paraIndex] <= mergeOp.start[paraIndex]) {
                    mergeOp.start[paraIndex]++;
                } else if (insertOp.start[paraIndex] === mergeOp.start[paraIndex] + 1) {
                    // local mergeParagraph [0] and remote insertTable [1]

                    // splitting paragraph again and then insert new paragraph/table
                    newOperation = json.deepClone(mergeOp);
                    newOperation.name = mergeOp.name ===  Op.PARA_MERGE ? 'splitParagraph' : 'splitTable';
                    newOperation.start.push(newOperation[lengthProperty]);
                    delete newOperation[lengthProperty];

                    if (localOp === mergeOp) {
                        externalOpsBefore = [newOperation]; // inserting before the current external operation
                    } else {
                        // the split operation must also be added to the local operation stack
                        localOpsBefore = [newOperation]; // inserting before the current insert operation
                    }

                    // ignoring the merge operation locally and externally
                    setOperationRemoved(mergeOp);
                } else {
                    insertOp.start[paraIndex]--;
                }
            }
        } else {
            // both positions have NOT the same length
            shortOp = insertParaLength < mergeParaLength ? insertOp : mergeOp;
            longOp = insertParaLength < mergeParaLength ? mergeOp : insertOp;

            paraIndex = shortOp.start.length - 1;
            textIndex = paraIndex + 1;

            if (paraIndex === 0 || equalPositions(shortOp.start, longOp.start, paraIndex)) {
                // the shorter paragraph is top level (index is 0) or the parent of the paragraph of the shorter op is an ancestor also for the longer op
                if (shortOp.start[paraIndex] <= longOp.start[paraIndex]) {
                    if (shortOp === mergeOp) {
                        if (shortOp.start[paraIndex] < longOp.start[paraIndex]) {
                            longOp.start[paraIndex]--;
                            if (shortOp.start[paraIndex] === longOp.start[paraIndex]) { // insert in a text frame (already decreased yet)
                                longOp.start[textIndex] += (shortOp[lengthProperty] ? shortOp[lengthProperty] : 0);
                            }
                        }
                    } else {
                        longOp.start[paraIndex]++;
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of insertCells/insertRows and mergeParagraph/mergeTable.
     */
    handleMergeCompInsertRows(localOp, extOp) {

        // the merge operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the insert operation for cells, rows or table
        const insertOp = (mergeOp === localOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the index of the row/cell inside the logical position
        let rowIndex = 0;
        // the length of the insert operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the merge operation
        const mergeLength = mergeOp.start.length;
        // number of rows/cells to be inserted
        const rowCount = insertOp.count || 1;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';

        if (comparePositions(insertOp.start, mergeOp.start) <= 0) {
            // the table must be the parent for both positions
            rowIndex = insertLength - 1;

            if ((mergeLength >= insertLength && equalPositions(insertOp.start, mergeOp.start, insertLength - 1))) {
                if (mergeOp.start[rowIndex] >= insertOp.start[rowIndex]) {
                    mergeOp.start[rowIndex] += rowCount;
                }
            }
        } else {
            paraIndex = mergeLength - 1;
            textIndex = paraIndex + 1;

            if ((paraIndex === 0) || (insertLength >= mergeLength && equalPositions(insertOp.start, mergeOp.start, paraIndex))) {
                if (mergeOp.start[paraIndex] < insertOp.start[paraIndex]) {
                    insertOp.start[paraIndex]--;
                    if (mergeOp.start[paraIndex] === insertOp.start[paraIndex]) { // paragraph position already decreased
                        insertOp.start[textIndex] += mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0;
                        if (!isParaMerge && insertLength === (mergeLength + 1) && ('referenceRow' in insertOp) && mergeOp[lengthProperty]) { // adapting the reference row for insertRows operation
                            insertOp.referenceRow += mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0;
                            if (insertOp.referenceRow < 0) { delete insertOp.referenceRow; }
                        }
                    }
                }
            }
        }
    }

    /**
     * Handling of insertColumn and mergeParagraph/mergeTable.
     */
    handleMergeCompInsertColumn(localOp, extOp) {

        // the merge operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the insertColumn operation
        const insertOp = (mergeOp === localOp) ? extOp : localOp;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the insertColumn operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the mergr operation
        const mergeLength = mergeOp.start.length;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';
        // the last value of the merge position
        const lastMergePos = mergeOp.start[mergeLength - 1];
        // the last value of the insert position
        const lastInsertPos = insertOp.start[insertLength - 1];
        // checking whether the table that has the insertColumn also needs to be merged
        const tableMerge = !isParaMerge && insertLength === mergeLength && (insertLength === 1 || equalPositions(mergeOp.start, insertOp.start, insertLength - 1)) && (lastMergePos === lastInsertPos || lastMergePos === lastInsertPos - 1);
        // whether the merge affects the same table as the insertColumn
        let sameTable = false;
        // a new operation that might be required
        let newOperation = null;
        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        if (tableMerge) {

            tableIndex = insertLength - 1;
            sameTable = lastMergePos === lastInsertPos;

            if (sameTable) {
                // the merge is in the same table: mergeTable [3] and insertColumn [3]
                // -> an additional operation: 'insertColumn [4]' before 'mergeTable [3]' is required
                newOperation = json.deepClone(insertOp);
                newOperation.start[tableIndex]++;
                if (mergeOp === localOp) {
                    localOpsBefore = [newOperation];
                } else {
                    externalOpsBefore = [newOperation];
                }
            } else {
                // the merge is in the previous table: mergeTable [3] and insertColumn [4]
                newOperation = json.deepClone(insertOp);
                // 1. the insertColumn [4] operation must be modified, because of the mergeTable [3] operation
                insertOp.start[tableIndex]--; // insertColumn [3] instead of insertColumn [4] -> this affects both tables
                // 2. an additional operation: 'insertColumn [3]' before 'mergeTable [3]' is required
                newOperation.start[tableIndex]--;
                if (mergeOp === localOp) {
                    localOpsBefore = [newOperation];
                } else {
                    externalOpsBefore = [newOperation];
                }
            }
        } else {
            if (comparePositions(insertOp.start, mergeOp.start) <= 0) {
                // the table must be the parent for both positions
                if ((mergeOp.start.length > insertLength + 1 && equalPositions(mergeOp.start, insertOp.start, insertLength))) {
                    tableIndex = insertLength - 1;
                    columnIndex = tableIndex + 2;
                    if ((mergeOp.start[columnIndex] > insertOp.gridPosition) || (mode === 'before' && mergeOp.start[columnIndex] === insertOp.gridPosition)) {
                        mergeOp.start[columnIndex]++;
                    }
                }
            } else {
                paraIndex = mergeLength - 1;
                textIndex = paraIndex + 1;

                if ((paraIndex === 0) || (insertLength >= mergeLength && equalPositions(insertOp.start, mergeOp.start, paraIndex))) {
                    if (mergeOp.start[paraIndex] < insertOp.start[paraIndex]) {
                        insertOp.start[paraIndex]--;
                        if (mergeOp.start[paraIndex] === insertOp.start[paraIndex]) { // the table might be inside a text frame
                            insertOp.start[textIndex] += mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0;
                        }
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of deleteColumns and mergeParagraph/mergeTable.
     */
    handleMergeCompDeleteColumns(localOp, extOp) {

        // the insertRows operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the deleteColumns operation
        const deleteOp = (mergeOp === localOp) ? extOp : localOp;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the deleteColumns operation
        const deleteLength = deleteOp.start.length;
        // the length of the logical position of the merge operation
        const mergeLength = mergeOp.start.length;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';
        // the last value of the merge position
        const lastMergePos = mergeOp.start[mergeLength - 1];
        // the last value of the delete position
        const lastDeletePos = deleteOp.start[deleteLength - 1];
        // checking whether the table that has the deleteColumns also needs to be merged (with the following table or caused by the previous table)
        const tableMerge = !isParaMerge && deleteLength === mergeLength && (deleteLength === 1 || equalPositions(mergeOp.start, deleteOp.start, deleteLength - 1)) && (lastMergePos === lastDeletePos || lastMergePos === lastDeletePos - 1);
        // whether the merge affects the same table as the deleteColumns
        let sameTable = false;
        // a new operation that might be required
        let newOperation = null;
        // the number of deleted columns
        let deleteColumns = 1;
        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        if (tableMerge) {

            tableIndex = deleteLength - 1;
            sameTable = lastMergePos === lastDeletePos;

            if (sameTable) {
                // the merge is in the same table: mergeTable [3] and deleteColumns [3]
                // -> an additional operation: 'deleteColumns [4]' before 'mergeTable [3]' is required
                newOperation = json.deepClone(deleteOp);
                newOperation.start[tableIndex]++;
                if (mergeOp === localOp) {
                    localOpsBefore = [newOperation]; // adding a new internal deleteColumns operation to the container of actions before mergeTable
                } else {
                    externalOpsBefore = [newOperation]; // inserting a new deleteColumns [4] operation before the current external merge operation
                }
            } else {
                // the merge is in the previous table: mergeTable [3] and deleteColumns [4]
                newOperation = json.deepClone(deleteOp);
                // 1. the deleteColumns [4] operation must be modified, because of the mergeTable [3] operation
                deleteOp.start[tableIndex]--; // deleteColumns [3] instead of deleteColumns [4] -> this affects both tables
                // 2. an additional operation: 'deleteColumns [3]' before 'mergeTable [3]' is required
                newOperation.start[tableIndex]--;
                if (mergeOp === localOp) {
                    localOpsBefore = [newOperation]; // adding a new internal deleteColumns operation to the container of actions before mergeTable
                } else {
                    externalOpsBefore = [newOperation]; // inserting a new deleteColumns [4] operation before the current external merge operation
                }
            }
        } else {
            if (comparePositions(deleteOp.start, mergeOp.start) <= 0) {
                // the table must be the parent for both positions
                if ((mergeOp.start.length > deleteLength + 1 && equalPositions(mergeOp.start, deleteOp.start, deleteLength))) {
                    tableIndex = deleteLength - 1;
                    columnIndex = tableIndex + 2;
                    if (mergeOp.start[columnIndex] >= deleteOp.startGrid) {
                        if (mergeOp.start[columnIndex] > deleteOp.endGrid) {
                            deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                            mergeOp.start[columnIndex] -= deleteColumns;
                        } else {
                            // the merge operation cannot be applied, because its column is already removed
                            setOperationRemoved(mergeOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                        }
                    }
                }
            } else {
                paraIndex = mergeLength - 1;
                textIndex = paraIndex + 1;

                if ((paraIndex === 0) || (deleteLength >= mergeLength && equalPositions(deleteOp.start, mergeOp.start, paraIndex))) {
                    if (mergeOp.start[paraIndex] < deleteOp.start[paraIndex]) {
                        deleteOp.start[paraIndex]--;
                        if (mergeOp.start[paraIndex] === deleteOp.start[paraIndex]) { // the table might be inside a text frame
                            deleteOp.start[textIndex] += mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0;
                        }
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of insertRows and splitParagraph.
     */
    handleInsertRowsSplitPara(localOp, extOp) {

        // the insertRows operation
        const insertOp = (localOp.name === Op.ROWS_INSERT || localOp.name === Op.CELLS_INSERT) ? localOp : extOp;
        // the splitParagraph operation
        const splitParaOp = localOp.name === Op.PARA_SPLIT ? localOp : extOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the index of the row/cell inside the logical position
        let rowIndex = 0;
        // the length of the insertRows/insertCells operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the splitParagraph operation
        const splitParaLength = splitParaOp.start.length;
        // number of rows/cells to be inserted
        const rowCount = insertOp.count || 1;

        if (comparePositions(insertOp.start, splitParaOp.start) <= 0) {
            // the table must be the parent for both positions
            rowIndex = insertLength - 1;

            if ((splitParaLength >= insertLength && equalPositions(insertOp.start, splitParaOp.start, insertLength - 1))) {
                if (splitParaOp.start[rowIndex] >= insertOp.start[rowIndex]) {
                    splitParaOp.start[rowIndex] += rowCount;
                }
            }
        } else {
            paraIndex = splitParaLength - 2;
            textIndex = paraIndex + 1;

            if ((paraIndex === 0) || (insertLength >= splitParaLength && equalPositions(insertOp.start, splitParaOp.start, paraIndex))) {
                if (splitParaOp.start[paraIndex] < insertOp.start[paraIndex]) {
                    insertOp.start[paraIndex]++;
                } else if (splitParaOp.start[paraIndex] === insertOp.start[paraIndex] && splitParaOp.start[textIndex] <= insertOp.start[textIndex]) {
                    insertOp.start[paraIndex]++;
                    insertOp.start[textIndex] -= splitParaOp.start[textIndex];
                }
            }
        }
    }

    /**
     * Handling of insertColumn and splitParagraph/splitTable.
     */
    handleInsertColumnSplitComp(localOp, extOp) {

        // the split operation
        const splitOp = this.hasAliasName(localOp, OT_SPLIT_COMP_ALIAS) ? localOp : extOp;
        // the insertRows operation
        const insertOp = (splitOp === localOp) ? extOp : localOp;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the insertColumn operation
        const insertLength = insertOp.start.length;
        // the length of the logical position of the split operation
        const splitLength = splitOp.start.length;
        // whether this is a merge of a paragraph (and not a table)
        const isParaSplit = splitOp.name === Op.PARA_SPLIT;
        // checking whether the table that has the insertColumn also needs to be splitted
        const tableSplit = !isParaSplit && insertLength === splitLength - 1 && equalPositions(splitOp.start, insertOp.start, insertLength);
        // a new operation that might be required
        let newOperation = null;
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        if (tableSplit) {
            // new insertColumn operation required
            tableIndex = insertLength - 1;

            // the split is in the same table: splitTable [3, 5] and insertColumn [3]
            // -> an additional operation: 'insertColumn [4]' after 'splitTable [3, 5]' is required
            newOperation = json.deepClone(insertOp);
            newOperation.start[tableIndex]++;
            if (splitOp === localOp) {
                externalOpsAfter = [newOperation]; // inserting a new insertColumn [4] operation after the current external insertColumn [3] operation
            } else {
                localOpsAfter = [newOperation]; // adding a new internal insertColumn [4] operation to the container of actions after insertColumn [3]
            }
        } else {
            if (comparePositions(insertOp.start, splitOp.start) <= 0) {
                // the table must be the parent for both positions
                if ((splitOp.start.length > insertLength + 1 && equalPositions(splitOp.start, insertOp.start, insertLength))) {
                    tableIndex = insertLength - 1;
                    columnIndex = tableIndex + 2;
                    if ((splitOp.start[columnIndex] > insertOp.gridPosition) || (mode === 'before' && splitOp.start[columnIndex] === insertOp.gridPosition)) {
                        splitOp.start[columnIndex]++;
                    }
                }
            } else {
                paraIndex = splitLength - 2;
                textIndex = paraIndex + 1;

                if ((paraIndex === 0) || (insertLength >= (splitLength - 1) && equalPositions(insertOp.start, splitOp.start, paraIndex))) {
                    if (splitOp.start[paraIndex] < insertOp.start[paraIndex]) {
                        insertOp.start[paraIndex]++;
                    } else if (splitOp.start[paraIndex] === insertOp.start[paraIndex] && splitOp.start[textIndex] <= insertOp.start[textIndex]) {
                        insertOp.start[paraIndex]++;
                        insertOp.start[textIndex] -= splitOp.start[textIndex];
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of deleteColumns and splitParagraph/splitTable.
     */
    handleDeleteColumnsSplitComp(localOp, extOp) {

        // the split operation
        const splitOp = this.hasAliasName(localOp, OT_SPLIT_COMP_ALIAS) ? localOp : extOp;
        // the insertRows operation
        const deleteOp = (splitOp === localOp) ? extOp : localOp;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the insertColumn operation
        const deleteLength = deleteOp.start.length;
        // the length of the logical position of the split operation
        const splitLength = splitOp.start.length;
        // whether this is a merge of a paragraph (and not a table)
        const isParaSplit = splitOp.name === Op.PARA_SPLIT;
        // checking whether the table that has the deleteColumns also needs to be splitted
        const tableSplit = !isParaSplit && deleteLength === splitLength - 1 && equalPositions(splitOp.start, deleteOp.start, deleteLength);
        // a new operation that might be required
        let newOperation = null;
        // the number of deleted columns
        let deleteColumns = 1;
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        if (tableSplit) {
            // new deleteColumns operation required
            tableIndex = deleteLength - 1;

            // the split is in the same table: splitTable [3, 5] and deleteColumns [3]
            // -> an additional operation: 'deleteColumns [4]' after 'splitTable [3, 5]' is required
            newOperation = json.deepClone(deleteOp);
            newOperation.start[tableIndex]++;
            if (splitOp === localOp) {
                externalOpsAfter = [newOperation]; // inserting a new deleteColumns [4] operation after the current external deleteColumns [3] operation
            } else {
                localOpsAfter = [newOperation]; // adding a new internal deleteColumns [4] operation to the container of actions after deleteColumns [3]
            }
        } else {
            if (comparePositions(deleteOp.start, splitOp.start) <= 0) {
                // the table must be the parent for both positions
                if ((splitLength > deleteLength + 1 && equalPositions(splitOp.start, deleteOp.start, deleteLength))) {
                    tableIndex = deleteLength - 1;
                    columnIndex = tableIndex + 2;
                    if (splitOp.start[columnIndex] >= deleteOp.startGrid) {
                        if (splitOp.start[columnIndex] > deleteOp.endGrid) {
                            deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                            splitOp.start[columnIndex] -= deleteColumns;
                        } else {
                            // the split operation cannot be applied, because its column is already removed
                            setOperationRemoved(splitOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                        }
                    }
                }
            } else {
                paraIndex = splitLength - 2;
                textIndex = paraIndex + 1;

                if ((paraIndex === 0) || (deleteLength >= (splitLength - 1) && equalPositions(deleteOp.start, splitOp.start, paraIndex))) {
                    if (splitOp.start[paraIndex] < deleteOp.start[paraIndex]) {
                        deleteOp.start[paraIndex]++;
                    } else if (splitOp.start[paraIndex] === deleteOp.start[paraIndex] && splitOp.start[textIndex] <= deleteOp.start[textIndex]) {
                        deleteOp.start[paraIndex]++;
                        deleteOp.start[textIndex] -= splitOp.start[textIndex];
                    }
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of insertRows/insertCells operation with an insertChar operation.
     * insertRows/insertCells operation and insertChar operation influence each other, if:
     * - the row/cell is inserted into a table at a position before insertChar in the same table/row.
     */
    handleInsertRowsInsertChar(localOp, extOp) {

        // the insertRows operation
        const insertRowsOp = (localOp.name === Op.ROWS_INSERT || localOp.name === Op.CELLS_INSERT) ? localOp : extOp;
        // the insert operation
        const insertCharOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the index of the row/cell inside the logical position
        let rowIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position of the insertRows/insertCells operation
        const insertRowsLength = insertRowsOp.start.length;
        // the number of inserted rows/cells
        const rowCount = insertRowsOp.count || 1;
        // the default length of the insertChar operation
        let insertCharLength = 2;

        if (comparePositions(insertRowsOp.start, insertCharOp.start) <= 0) {
            // the table must be the parent for both positions to influence the external position
            rowIndex = insertRowsLength - 1;

            if ((insertCharOp.start.length >= insertRowsLength && equalPositions(insertCharOp.start, insertRowsOp.start, insertRowsLength - 1))) {
                if (insertCharOp.start[rowIndex] >= insertRowsOp.start[rowIndex]) { // position inside a following row/cell
                    insertCharOp.start[rowIndex] += rowCount;
                }
            }
        } else {
            // the table might be inside a drawing
            insertCharLength = insertCharOp.start.length;
            textIndex = insertCharLength - 1;
            if (insertCharLength < insertRowsLength && equalPositions(insertCharOp.start, insertRowsOp.start, insertCharLength - 1)) {
                if (insertCharOp.start[textIndex] <= insertRowsOp.start[textIndex]) {
                    insertRowsOp.start[textIndex] += getInsertCharLength(insertCharOp);
                }
            }
        }
    }

    /**
     * Handling of insertColumn operation with an insertChar operation.
     * insertColumn operation and insertChar operation influence each other, if:
     * - the column is inserted into a table at a position left of insertChar in the same table.
     *
     * Example:
     * { name: insertColumn, start: [2], tableGrid: [374,374,374], gridPosition: 2, insertMode: 'behind' }
     */
    handleInsertColumnInsertChar(localOp, extOp) {

        // the insertColumn operation
        const insertColumnOp = localOp.name === Op.COLUMN_INSERT ? localOp : extOp;
        // the insert operation
        const insertCharOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position of the insertColumn operation
        const insertColumnLength = insertColumnOp.start.length;
        // the default length of the insertChar operation
        let insertCharLength = 0;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the specification, where the column is inserted
        const mode = insertColumnOp.insertMode || 'behind';

        if (comparePositions(insertColumnOp.start, insertCharOp.start) < 0) {
            if ((insertCharOp.start.length > insertColumnLength + 1 && equalPositions(insertCharOp.start, insertColumnOp.start, insertColumnLength))) {
                tableIndex = insertColumnLength - 1;
                columnIndex = tableIndex + 2;
                if ((insertCharOp.start[columnIndex] > insertColumnOp.gridPosition) || (mode === 'before' && insertCharOp.start[columnIndex] === insertColumnOp.gridPosition)) {
                    insertCharOp.start[columnIndex]++;
                }
            }
        } else {
            // the table might be inside a drawing
            insertCharLength = insertCharOp.start.length;
            textIndex = insertCharLength - 1;
            if (insertCharLength < insertColumnLength && equalPositions(insertCharOp.start, insertColumnOp.start, insertCharLength - 1)) {
                if (insertCharOp.start[textIndex] <= insertColumnOp.start[textIndex]) {
                    insertColumnOp.start[textIndex] += getInsertCharLength(insertCharOp);
                }
            }
        }
    }

    /**
     * Handling of deleteColumn operation with an insertChar operation.
     * deleteColumn operation and insertChar operation influence each other, if:
     * - the column is deleted inside a table at a position left of insertChar in the same table.
     * - the column is deleted in a table in a textframe, whose position is changed by the insertChar operation.
     *
     * Example:
     * { name: deleteColumns, start: [2], startGrid: 2, endGrid: 3 }
     */
    handleDeleteColumnsInsertChar(localOp, extOp) {

        // the deleteColumns operation
        const deleteOp = localOp.name === Op.COLUMNS_DELETE ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position of the deleteColumns operation
        const deleteLength = deleteOp.start.length;
        // the default length of the insertChar operation
        let insertCharLength = 0;
        // the index of the table position inside the logical position
        let tableIndex = 0;
        // the index of the column position inside the logical position
        let columnIndex = 0;
        // the number of deleted columns
        let deleteColumns = 1;

        if (comparePositions(deleteOp.start, insertOp.start) < 0) {
            if ((insertOp.start.length > deleteLength + 1 && equalPositions(insertOp.start, deleteOp.start, deleteLength))) {
                tableIndex = deleteLength - 1;
                columnIndex = tableIndex + 2;
                if (insertOp.start[columnIndex] >= deleteOp.startGrid) {
                    if (insertOp.start[columnIndex] > deleteOp.endGrid) {
                        deleteColumns = deleteOp.endGrid - deleteOp.startGrid + 1;
                        insertOp.start[columnIndex] -= deleteColumns;
                    } else {
                        // the insert operation cannot be applied, because its column is already removed
                        setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                    }
                }
            }
        } else {
            // the table might be inside a drawing
            insertCharLength = insertOp.start.length;
            textIndex = insertCharLength - 1;
            if (insertCharLength < deleteLength && equalPositions(insertOp.start, deleteOp.start, insertCharLength - 1)) {
                if (insertOp.start[textIndex] <= deleteOp.start[textIndex]) {
                    deleteOp.start[textIndex] += getInsertCharLength(insertOp);
                }
            }
        }
    }

    /**
     * Handling of paragraph split operation with an insert operation.
     * Paragraph split operations and insert operation influence each other, if:
     * - both paragraphs have the same (or none) parent.
     * - the paragraph of the insert operation has the longer logical position and has the paragraph of the split operation as ancestor.
     */
    handleSplitParaInsertChar(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the split operation
        const splitOp = (insertOp === localOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let posLength = 0;
        // the default length of the insertChar operation
        let insertCharLength = 2;

        // TODO: This fake ensure needs to be removed (forcing reload in OX Presentation test 23)
        // if (localOp === splitOp && insertOp.start[0] === 0 && splitOp.start[0] === 0 && insertOp.start.length === 4 && splitOp.start.length === 4) {
        //     this.ensure(insertOp.start[0] !== splitOp.start[0], 'ERROR: OT conflict, this is a fake error for testing reasons.'); // Reload required
        // }

        if (splitOp.start.length <= insertOp.start.length) {
            posLength = splitOp.start.length; // both positions have the same length
            if (posLength === 2 || equalPositions(splitOp.start, insertOp.start, posLength - 2)) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                textIndex = splitOp.start.length - 1;
                paraIndex = textIndex - 1;
                if (splitOp.start[paraIndex] < insertOp.start[paraIndex]) {
                    insertOp.start[paraIndex]++; // increasing position of insert operation
                } else if (splitOp.start[paraIndex] === insertOp.start[paraIndex]) {
                    // did the split happen before or after the text insertion?
                    if (splitOp.start[textIndex] <= insertOp.start[textIndex]) {
                        insertOp.start[paraIndex]++;
                        insertOp.start[textIndex] -= splitOp.start[textIndex];
                    } else {
                        splitOp.start[textIndex] += getInsertCharLength(insertOp);
                    }
                }
            }
        } else {
            // the splitted paragraph might be inside a drawing
            insertCharLength = insertOp.start.length;
            textIndex = insertCharLength - 1;
            if (equalPositions(insertOp.start, splitOp.start, insertCharLength - 1)) {
                if (insertOp.start[textIndex] <= splitOp.start[textIndex]) {
                    splitOp.start[textIndex] += getInsertCharLength(insertOp);
                }
            }
        }
    }

    /**
     * Handling of table split operation with an insert operation.
     */
    handleSplitTableInsertChar(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the split operation
        const splitOp = (insertOp === localOp) ? extOp : localOp;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the default length of the insertChar operation
        let insertCharLength = 2;

        if (splitOp.start.length <= insertOp.start.length) {

            rowIndex = splitOp.start.length - 1;
            tableIndex = rowIndex - 1;

            if (tableIndex === 0 || equalPositions(splitOp.start, insertOp.start, tableIndex)) {
                if (splitOp.start[tableIndex] < insertOp.start[tableIndex]) {
                    insertOp.start[tableIndex]++; // increasing position of insert operation at the index of the table
                } else if (splitOp.start[tableIndex] === insertOp.start[tableIndex]) {
                    // did the split happen before or after the text insertion?
                    if (splitOp.start[rowIndex] <= insertOp.start[rowIndex]) {
                        insertOp.start[tableIndex]++;
                        insertOp.start[rowIndex] -= splitOp.start[rowIndex];
                    }
                }
            }
        } else {
            // the splitted table might be inside a drawing
            insertCharLength = insertOp.start.length;
            textIndex = insertCharLength - 1;
            if (equalPositions(insertOp.start, splitOp.start, insertCharLength - 1)) {
                if (insertOp.start[textIndex] <= splitOp.start[textIndex]) {
                    splitOp.start[textIndex] += getInsertCharLength(insertOp);
                }
            }
        }
    }

    /**
     * Handling of table split operation with an insert paragraph operation.
     */
    handleSplitTableInsertPara(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the split operation
        const splitOp = (insertOp === localOp) ? extOp : localOp;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the index of the text inside the logical position
        let paraIndex = 0;

        if (splitOp.start.length <= insertOp.start.length) {

            rowIndex = splitOp.start.length - 1;
            tableIndex = rowIndex - 1;

            if (tableIndex === 0 || equalPositions(splitOp.start, insertOp.start, tableIndex)) {
                if (splitOp.start[tableIndex] < insertOp.start[tableIndex]) {
                    insertOp.start[tableIndex]++; // increasing position of insert operation at the index of the table
                } else if (splitOp.start[tableIndex] === insertOp.start[tableIndex]) {
                    if (splitOp.start[rowIndex] <= insertOp.start[rowIndex]) {
                        insertOp.start[tableIndex]++;
                        insertOp.start[rowIndex] -= splitOp.start[rowIndex];
                    }
                }
            }
        } else {
            paraIndex = insertOp.start.length - 1;
            if (paraIndex === 0 || equalPositions(insertOp.start, splitOp.start, paraIndex)) {
                if (insertOp.start[paraIndex] <= splitOp.start[paraIndex]) {
                    splitOp.start[paraIndex]++;
                } else {
                    if (splitOp.start.length === insertOp.start.length + 1) { // must be same level
                        insertOp.start[paraIndex]++;
                    }
                }
            }
        }
    }

    /**
     * Handling splitTable and splitParagraph operations.
     * The split operations influence each other, if:
     * - both paragraph/table have the same (or none) parent.
     * - the paragraph/table with the longer logical position has the other paragraph/table as ancestor.
     */
    handleSplitParaSplitTable(localOp, extOp) {

        // the index of the paragraph/table inside the logical position
        let paraIndex = 0;
        // the index of the text/row inside the logical position
        let textRowIndex = 0;
        // the length of the logical position(s)
        let length = 0;

        if (localOp.start.length === extOp.start.length) {
            length = localOp.start.length; // both positions have the same length
            if (length === 2 || equalPositions(localOp.start, extOp.start, length - 2)) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = localOp.start.length - 2;
                if (localOp.start[paraIndex] < extOp.start[paraIndex]) {
                    extOp.start[paraIndex]++; // increasing external operation paragraph/table position
                } else if (localOp.start[paraIndex] > extOp.start[paraIndex]) {
                    localOp.start[paraIndex]++; // increasing internal operation paragraph/table position
                }
            }
        } else {
            // both positions have NOT the same length
            const shortOp = localOp.start.length < extOp.start.length ? localOp : extOp; // this has never changed
            const longOp = localOp.start.length < extOp.start.length ? extOp : localOp; // only this might have changed

            paraIndex = shortOp.start.length - 2;
            textRowIndex = paraIndex + 1;

            if (paraIndex === 0 || equalPositions(shortOp.start, longOp.start, paraIndex)) {
                // the shorter paragraph/table is top level (index is 0) or the parent of the paragraph/table of the shorter op is an ancestor also for the longer op
                if (shortOp.start[paraIndex] < longOp.start[paraIndex]) {
                    longOp.start[paraIndex]++; // increase the paragraph/table position of the operation with the longer position
                } else if (shortOp.start[paraIndex] === longOp.start[paraIndex] && shortOp.start[textRowIndex] <= longOp.start[textRowIndex]) {
                    longOp.start[paraIndex]++;
                    longOp.start[textRowIndex] -= shortOp.start[textRowIndex];
                }
            }
        }
    }

    /**
     * Handling of table split operation with an insertRows/insertCells operation.
     * Example:
     * { name: 'insertRows', start: [1, 2], count: 1, referenceRow: 2, opl: 1, osn: 1 }] }
     * { name: 'splitTable', start: [2, 1], opl: 1, osn: 1 }
     */
    handleInsertRowsSplitTable(localOp, extOp) {

        // the split operation
        const splitOp = localOp.name === Op.TABLE_SPLIT ? localOp : extOp;
        // the insert operation
        const insertOp = (localOp.name === Op.ROWS_INSERT || localOp.name === Op.CELLS_INSERT) ? localOp : extOp;
        // the index of the table inside the logical position
        let tableIndex = 0;
        // the index of the row inside the logical position
        let rowIndex = 0;
        // the number of inserted rows/cells
        const rowCount = insertOp.count || 1;

        if (splitOp.start.length <= insertOp.start.length) {

            rowIndex = splitOp.start.length - 1;
            tableIndex = rowIndex - 1;

            if (tableIndex === 0 || equalPositions(splitOp.start, insertOp.start, tableIndex)) {
                if (splitOp.start[tableIndex] < insertOp.start[tableIndex]) {
                    insertOp.start[tableIndex]++; // increasing position of insert operation at the index of the table
                } else if (splitOp.start[tableIndex] === insertOp.start[tableIndex]) {
                    if (splitOp.start[rowIndex] <= insertOp.start[rowIndex]) {
                        insertOp.start[tableIndex]++;
                        insertOp.start[rowIndex] -= splitOp.start[rowIndex];
                        if ('referenceRow' in insertOp && splitOp.start.length === insertOp.start.length) {
                            insertOp.referenceRow -= splitOp.start[rowIndex];
                            if (insertOp.referenceRow < 0) { delete insertOp.referenceRow; }
                        }
                    } else {
                        if ('referenceRow' in insertOp && splitOp.start.length === insertOp.start.length && insertOp.referenceRow >= splitOp.start[rowIndex]) {
                            delete insertOp.referenceRow; // reference row is in the "other" part of the table
                        }
                        if (splitOp.start.length === insertOp.start.length && insertOp.name === Op.ROWS_INSERT) { // must be same table level
                            splitOp.start[rowIndex] += rowCount;
                        }
                    }
                }
            }
        } else {
            rowIndex = insertOp.start.length - 1; // the index of row/cell in the insert operation
            if (equalPositions(insertOp.start, splitOp.start, rowIndex)) {
                if (insertOp.start[rowIndex] <= splitOp.start[rowIndex]) {
                    splitOp.start[rowIndex] += rowCount;
                }
            }
        }
    }

    /**
     * Handling of setAttributes operation with a split table operation.
     * setAttributes operations and table split operations influence each other, if:
     * - the split table operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted. But both only, if these two positions are affected by the split of the table.
     * -> the setAttributes operation never influences the position of the split table operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     *
     * Info: If a complete table gets an attribute and this table is splitted, there must be
     *       a setAttributes operation for every table. A setAttributes, that covers more than
     *       one table (setAttributes from [1] to [2]) might then be influenced by a following
     *       insertParagraph, so that it is expanded from [1] to [3], so that table attributes
     *       would be assigned to the new inserted paragraph.
     */
    handleSetAttrsSplitTable(localOp, extOp) {

        // the insert operation
        const splitOp = localOp.name === Op.TABLE_SPLIT ? localOp : extOp;
        // the set attributes operation
        const attrsOp = splitOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical start position of the attributes operation
        const startLength = attrsOp.start.length;
        // the length of the logical position of the split operation
        const posLength = splitOp.start.length;
        // the index of the table inside the logical position
        const tableIndex = posLength - 2;
        // the index of the text inside the logical position
        const rowIndex = tableIndex + 1;
        // a new operation that might be required
        let newOperation = null;
        // whether the splitted table gets attributes assigned (in this case the end position is not specified or the same as the start position)
        const isSplitInAttributedTable = attrsOp.start && (!attrsOp.end || equalPositions(attrsOp.start, attrsOp.end)) && startLength === posLength - 1 && equalPositions(splitOp.start, attrsOp.start, posLength - 1);
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        allPos.forEach((attrsPos, index) => {

            if (posLength <= attrsPos.length && (posLength === 2 || equalPositions(splitOp.start, attrsPos, posLength - 2))) {
                // both positions are top level (length === 2) or the tables have the same parent (inside cell, text frame, ...)
                if (splitOp.start[tableIndex] < attrsPos[tableIndex]) {
                    attrsPos[tableIndex]++; // increasing position of setAttributes operation
                } else if (splitOp.start[tableIndex] === attrsPos[tableIndex]) {
                    if (splitOp.start[rowIndex] <= attrsPos[rowIndex]) {
                        // the split happens before the range of setAttributes
                        attrsPos[tableIndex]++;
                        attrsPos[rowIndex] -= splitOp.start[rowIndex];
                    }
                }
            } else {
                // both positions have or have NOT the same length (setAttributes length is pretty arbitrary, must not be a text selection)
                if (tableIndex === 0 || (is.number(attrsPos[tableIndex]) && equalPositions(splitOp.start, attrsPos, tableIndex))) {

                    if (isSplitInAttributedTable) {
                        if (index === 0) {
                            // setAttributes operation must stay inside one table -> new operation for following table required
                            newOperation = json.deepClone(attrsOp);
                            newOperation.start[tableIndex]++;
                            if (newOperation.end) { newOperation.end[tableIndex]++; }

                            if (attrsOp === localOp) {
                                localOpsAfter = [newOperation]; // adding a new internal setAttributes operation to the container of actions
                            } else {
                                externalOpsAfter = [newOperation]; // inserting behind the current external operation
                            }
                        }
                    } else {
                        // the split table is top level (index is 0) or the parent of the table of the split operation is an ancestor of the attributes op
                        if (splitOp.start[tableIndex] === attrsPos[tableIndex]) {
                            if (index === 1 || !attrsOp.end) { // this is the final paragraph
                                attrsOp.end = attrsOp.end || json.deepClone(attrsOp.start);
                                attrsOp.end[tableIndex]++; // Example: setAttributes(start: [6]) and splitParagraph at (6,5) -> setAttributes start:[6], end:[7]
                            }
                        } else if (splitOp.start[tableIndex] < attrsPos[tableIndex]) {
                            attrsPos[tableIndex]++; // increase the table position of the setAttributes operation
                        }
                    }
                }
            }
        });

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling a mergeParagraph/mergeTable operation with an insert operation.
     * Paragraph merge operations and insert operation influence each other, if:
     *  - merge paragraph influences the insert operation, if its paragraph position is behind the paragraph position of the merge operation
     *  - insert operation will influence merge paragraph operation in the future, if it contains the text position (not yet available).
     */
    handleMergeCompInsertChar(localOp, extOp) {

        // the merge operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let posLength = 0;
        // the default length of the insertChar operation
        let insertCharLength = 2;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';

        if (mergeOp.start.length <= insertOp.start.length - 1) {
            posLength = mergeOp.start.length; // both paragraph positions have the same length (insert contains additionally the text position)
            if (posLength === 1 || equalPositions(mergeOp.start, insertOp.start, posLength - 1)) {
                // both positions are top level (length === 1 resp. 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = mergeOp.start.length - 1;
                textIndex = paraIndex + 1;
                if (mergeOp.start[paraIndex] < (insertOp.start[paraIndex] - 1)) { // handling mergeParagraph at [1] and insertText at [3,14], not [2,14]
                    insertOp.start[paraIndex]--; // decreasing position of insert operation
                } else if (mergeOp.start[paraIndex] === (insertOp.start[paraIndex] - 1)) { // handling mergeParagraph at [1] and insertText at [2,14]
                    // the merge is always before the text insertion
                    insertOp.start[paraIndex]--;
                    insertOp.start[textIndex] += (mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0);
                } else if (mergeOp.start[paraIndex] === insertOp.start[paraIndex]) {
                    if (isParaMerge && (insertOp.start.length === mergeOp.start.length + 1) && insertOp.start[textIndex] <= mergeOp[lengthProperty]) {
                        // increasing the 'paralength' property (but only if the text is inserted into the paragraph and not into a drawing in the paragraph)
                        mergeOp[lengthProperty] += getInsertCharLength(insertOp);
                    } else {
                        // this should never happen
                    }
                }
            }
        } else {
            // the merged paragraph/table might be inside a drawing
            insertCharLength = insertOp.start.length;
            textIndex = insertCharLength - 1;
            if (equalPositions(insertOp.start, mergeOp.start, insertCharLength - 1)) {
                if (insertOp.start[textIndex] <= mergeOp.start[textIndex]) {
                    mergeOp.start[textIndex] += getInsertCharLength(insertOp);
                }
            }
        }
    }

    /**
     * Handling a split paragraph (or splitTable) operation together with a merge paragraph operation.
     * Paragraph split operation and paragraph merge operation influence each other, if:
     *  - split paragraph operation influences the merge operation, if the split is in a previous paragraph or inside the same paragraph.
     *  - merge paragraph influences the split operation, if its paragraph position is behind the paragraph position of the merge operation.
     */
    handleSplitCompMergeComp(localOp, extOp) {

        // the merge operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the split operation
        const splitOp = (mergeOp === localOp) ? extOp : localOp;
        // the index of the paragraph/table inside the logical position
        let paraIndex = 0;
        // the index of the text/row inside the logical position
        let textIndex = 0;
        // the length of the logical position(s)
        let posLength = 0;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';

        if (mergeOp.start.length === splitOp.start.length - 1) {
            posLength = mergeOp.start.length; // both paragraph positions have the same length, splitParagraph contains additionally the text position
            if (posLength === 1 || equalPositions(mergeOp.start, splitOp.start, posLength - 1)) {
                // both positions are top level (length === 1 resp. 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = posLength - 1;
                textIndex = posLength;
                if (mergeOp.start[paraIndex] < (splitOp.start[paraIndex] - 1)) { // handling mergeParagraph at [1] and splitParagraph at [3,14], not [2,14]
                    splitOp.start[paraIndex]--; // decreasing position of split operation
                } else if (mergeOp.start[paraIndex] === (splitOp.start[paraIndex] - 1)) { // handling mergeParagraph at [1] and splitParagraph at [2,14]
                    // the merge is always before the split operation in the next paragraph
                    splitOp.start[paraIndex]--;
                    splitOp.start[textIndex] += (mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0);
                } else if (mergeOp.start[paraIndex] === splitOp.start[paraIndex]) { // handling mergeParagraph at [1] (length 8) and splitParagraph at [1,4]
                    if (mergeOp[lengthProperty] >= splitOp.start[textIndex]) {
                        mergeOp.start[paraIndex]++;
                        mergeOp[lengthProperty] -= splitOp.start[textIndex];
                    } else {
                        // this is a case, that never should happen: mergeParagraph at [1] with length 4 and splitParagraph at [1,8]
                    }
                } else {
                    mergeOp.start[paraIndex]++; // in all other cases the index at the paragraph for the merge operation needs to be increased because of the split operation
                }
            }
        } else {
            // both paragraph positions have NOT the same level -> both operations can influence each other
            if (mergeOp.start.length < (splitOp.start.length - 1)) {
                paraIndex = mergeOp.start.length - 1;
                textIndex = paraIndex + 1;

                if (paraIndex === 0 || equalPositions(mergeOp.start, splitOp.start, paraIndex)) {
                    // the merge paragraph is top level (index is 0) or the parent of the paragraph of the merge operation is an ancestor of the split op
                    if (mergeOp.start[paraIndex] < splitOp.start[paraIndex] - 1) {
                        splitOp.start[paraIndex]--; // decrease the paragraph position of the split operation
                    } else if (mergeOp.start[paraIndex] === splitOp.start[paraIndex] - 1) {
                        splitOp.start[paraIndex]--;
                        splitOp.start[textIndex] += (mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0);
                    }
                }
            } else {
                // the split operation might influence a longer merge operation
                paraIndex = splitOp.start.length - 2;
                textIndex = paraIndex + 1;

                if (paraIndex === 0 || equalPositions(mergeOp.start, splitOp.start, paraIndex)) {
                    // the split paragraph is top level (index is 0) or the parent of the paragraph of the split operation is an ancestor of the merge op
                    if (splitOp.start[paraIndex] < mergeOp.start[paraIndex]) {
                        mergeOp.start[paraIndex]++; // increase the paragraph position of the merge operation
                    } else if (splitOp.start[paraIndex] === mergeOp.start[paraIndex] && splitOp.start[textIndex] <= mergeOp.start[textIndex]) {
                        mergeOp.start[paraIndex]++; // merge inside a text frame
                        mergeOp.start[textIndex] -= splitOp.start[textIndex];
                    }
                }
            }
        }
    }

    /**
     * Handling of setAttributes operation with an insert operation.
     * setAttributes operations and insert operation influence each other, if:
     * - the insert operation is before or inside the range affected by the setAttributes operation. In
     *   this case the start position might be shifted, and the end position must be shifted.
     *   But both only, if they are positioned inside the same paragraph as the insert operation.
     * -> the setAttributes operation never influences the position of the insert operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     */
    handleSetAttrsInsertChar(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the set attributes operation
        const attrsOp = insertOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical position of the insert operation
        const posLength = insertOp.start.length;
        // the index of the text in the insert operation
        let textIndex = 0;

        allPos.forEach((attrsPos, index) => {
            if (posLength <= attrsPos.length && equalPositions(insertOp.start, attrsPos, posLength - 1)) {
                textIndex = posLength - 1;
                if (insertOp.start[textIndex] <= attrsPos[textIndex]) { // insert shifts the setAttributes operation
                    attrsPos[textIndex] += getInsertCharLength(insertOp); // the start/end position of the attributes operation needs to be increased
                    if (index === 0) { attrsOp.start = attrsPos; }
                    if (index === 1) { attrsOp.end = attrsPos; }
                }
            }
        });
    }

    /**
     * Handling of setAttributes operation with a split paragraph operation.
     * setAttributes operations and paragraph split operations influence each other, if:
     * - the split paragraph operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted. But both only, if these two positions are affected by the split of the paragraph.
     * -> the setAttributes operation never influences the position of the split paragraph operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     */
    handleSetAttrsSplitPara(localOp, extOp) {

        // the insert operation
        const splitOp = localOp.name === Op.PARA_SPLIT ? localOp : extOp;
        // the set attributes operation
        const attrsOp = splitOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical start position of the attributes operation
        const startLength = attrsOp.start.length;
        // the length of the logical position of the split operation
        const posLength = splitOp.start.length;
        // the index of the paragraph inside the logical position
        const paraIndex = posLength - 2;
        // the index of the text inside the logical position
        const textIndex = paraIndex + 1;
        // a new operation that might be required
        let newOperation = null;
        // whether the split happens inside the (text) attribute range (requires a second operation)
        const splitInAttrs = attrsOp.start && attrsOp.end && attrsOp.start.length === posLength && attrsOp.end.length === posLength && attrsOp.start[textIndex] < splitOp.start[textIndex] && attrsOp.end[textIndex] >= splitOp.start[textIndex];
        // whether the splitted paragraph gets attributes assigned (in this case the end position is not specified or the same as the start position)
        const isSplitInAttributedParagraph = attrsOp.start && (!attrsOp.end || equalPositions(attrsOp.start, attrsOp.end)) && startLength === posLength - 1 && equalPositions(splitOp.start, attrsOp.start, posLength - 1);
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        allPos.forEach((attrsPos, index) => {

            if (posLength <= attrsPos.length && (posLength === 2 || equalPositions(splitOp.start, attrsPos, posLength - 2))) {
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                if (splitOp.start[paraIndex] < attrsPos[paraIndex]) {
                    attrsPos[paraIndex]++; // increasing position of setAttributes operation
                } else if (splitOp.start[paraIndex] === attrsPos[paraIndex]) {
                    if (splitInAttrs) {
                        // the split happens inside the range of setAttributes
                        if (index === 0) {
                            // setAttributes operation must stay inside one paragraph -> new operation for following paragraph required
                            newOperation = json.deepClone(attrsOp);
                            attrsOp.end[textIndex] = splitOp.start[textIndex] - 1;
                            newOperation.start[paraIndex]++;
                            newOperation.end[paraIndex]++;
                            newOperation.start[textIndex] = 0;
                            newOperation.end[textIndex] -= splitOp.start[textIndex];

                            if (attrsOp === localOp) {
                                localOpsAfter = [newOperation]; // adding a new internal setAttributes operation to the container of actions
                            } else {
                                externalOpsAfter = [newOperation]; // inserting behind the current external operation
                            }
                        }
                    } else if (splitOp.start[textIndex] <= attrsPos[textIndex]) {
                        // the split happens before the range of setAttributes
                        attrsPos[paraIndex]++;
                        attrsPos[textIndex] -= splitOp.start[textIndex];
                    }
                }
            } else {
                // both positions have or have NOT the same length (setAttributes length is pretty arbitrary, must not be a text selection)
                if (paraIndex === 0 || (is.number(attrsPos[paraIndex]) && equalPositions(splitOp.start, attrsPos, paraIndex))) {

                    if (isSplitInAttributedParagraph) {
                        if (index === 0) {
                            // setAttributes operation must stay inside one paragraph -> new operation for following paragraph required
                            newOperation = json.deepClone(attrsOp);
                            newOperation.start[paraIndex]++;
                            if (newOperation.end) { newOperation.end[paraIndex]++; }

                            if (attrsOp === localOp) {
                                localOpsAfter = [newOperation]; // adding a new internal setAttributes operation to the container of actions
                            } else {
                                externalOpsAfter = [newOperation]; // inserting behind the current external operation
                            }
                        }
                    } else {
                        // the split paragraph is top level (index is 0) or the parent of the paragraph of the split operation is an ancestor of the attributes op
                        if (splitOp.start[paraIndex] === attrsPos[paraIndex]) {
                            if (index === 1 || !attrsOp.end) { // this is the final paragraph
                                attrsOp.end = attrsOp.end || json.deepClone(attrsOp.start);
                                attrsOp.end[paraIndex]++; // Example: setAttributes(start: [6]) and splitParagraph at (6,5) -> setAttributes start:[6], end:[7]
                            }
                        } else if (splitOp.start[paraIndex] < attrsPos[paraIndex]) {
                            attrsPos[paraIndex]++; // increase the paragraph position of the setAttributes operation
                        }
                    }
                }
            }
        });

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling of setAttributes operation with a insertParagraph or insertTable operation.
     * setAttributes operations and insertParagraph operations influence each other, if:
     * - the insert paragraph operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted. But both only, if these two positions are affected by the insertion of the paragraph.
     * -> the setAttributes operation never influences the position of the insertParagraph operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     */
    handleSetAttrsInsertComp(localOp, extOp) {

        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the set attributes operation
        const attrsOp = insertOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical position of the split operation
        const posLength = insertOp.start.length;
        // the index of the paragraph inside the logical position
        const paraIndex = posLength - 1;

        allPos.forEach(attrsPos => {
            if (paraIndex === 0 || (is.number(attrsPos[paraIndex]) && equalPositions(insertOp.start, attrsPos, paraIndex))) {
                // the insert operation is top level (index is 0) or the parent of the paragraph of the insert operation is an ancestor of the attributes op
                if (insertOp.start[paraIndex] <= attrsPos[paraIndex]) {
                    attrsPos[paraIndex]++; // increase the paragraph position of the setAttributes operation
                }
            }
        });
    }

    /**
     * Handling of setAttributes operation with an insertRows/insertCells operation.
     * setAttributes operations and insertRows/insertCells operations influence each other, if:
     * - the insertRows/Cells operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted. But both only, if these two positions are affected by the insertion of the rows.
     * -> the setAttributes operation never influences the position of the insertRows operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     */
    handleSetAttrsInsertRows(localOp, extOp) {

        // the insert operation
        const insertOp = (localOp.name === Op.ROWS_INSERT || localOp.name === Op.CELLS_INSERT) ? localOp : extOp;
        // the set attributes operation
        const attrsOp = insertOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical position of the split operation
        const posLength = insertOp.start.length;
        // the index of the rows/cells inside the logical position
        const rowIndex = posLength - 1;
        // the number of inserted rows/cells
        const rowCount = insertOp.count || 1;

        allPos.forEach(attrsPos => {
            // both positions have or have NOT the same length (setAttributes length is pretty arbitrary, must not be a text selection)
            if (attrsPos.length >= posLength && equalPositions(insertOp.start, attrsPos, rowIndex)) {
                // the insert operation is top level (index is 0) or the parent of the rows of the insert operation (the table) is an ancestor of the attributes op
                if (insertOp.start[rowIndex] <= attrsPos[rowIndex]) {
                    attrsPos[rowIndex] += rowCount; // increase the row position of the setAttributes operation
                }
            }
        });
    }

    /**
     * Handling of setAttributes operation with an insertColumn operation.
     * setAttributes operations and insertColumn operations influence each other, if:
     * - the insertColumn operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted. But both only, if these two positions are affected by the insertion of the column.
     * -> the setAttributes operation never influences the position of the insertColumn operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     */
    handleSetAttrsInsertColumn(localOp, extOp) {

        // the insert operation
        const insertOp = localOp.name === Op.COLUMN_INSERT ? localOp : extOp;
        // the set attributes operation
        const attrsOp = insertOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical position of the insert operation
        const insertLength = insertOp.start.length;
        // the index of the table inside the logical position
        const tableIndex = insertLength - 1;
        // the index of the column inside the logical position
        const columnIndex = tableIndex + 2;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';

        allPos.forEach(attrsPos => {
            // both positions have or have NOT the same length (setAttributes length is pretty arbitrary, must not be a text selection)
            if (equalPositions(insertOp.start, attrsPos, insertLength)) {
                if (attrsOp.start.length === insertLength) {
                    // check for tableGrid property
                    if (attrsOp.attrs && attrsOp.attrs.table && attrsOp.attrs.table.tableGrid) {
                        delete attrsOp.attrs.table.tableGrid;
                        if (_.isEmpty(attrsOp.attrs.table)) {
                            delete attrsOp.attrs.table;
                        }
                    }
                } else  if ((is.number(attrsPos[columnIndex]))) {
                    // the insert operation is top level (index is 0) or the parent of the rows of the insert operation (the table) is an ancestor of the attributes op
                    if ((attrsPos[columnIndex] > insertOp.gridPosition) || (mode === 'before' && attrsPos[columnIndex] === insertOp.gridPosition)) {
                        attrsPos[columnIndex]++;
                    }
                }
            }
        });
    }

    /**
     * Handling of setAttributes operation with a deleteColumns operation.
     * setAttributes operations and deleteColumns operations influence each other, if:
     * - the deleteColumns operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted. But both only, if these two positions are affected by the deletion of the columns.
     * -> the setAttributes operation never influences the position of the insertColumn operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     *
     * TODO: Is it possible, that one setAttributes operation affects several cells? It might be
     *       possible, that only the start or only the end position is affected by deleting the
     *       column.
     *       A setAttributes operation for one table or one row is not affected, because it would
     *       never be necessary to modify the selection range caused by a deleteColumns operation.
     *       But a selection from [1, 2, 3] to [1, 2, 4] inside one setAttributes operation would
     *       need to be adapted, if the column at grid 3 or grid 4 is deleted. Therefore it is
     *       better, if one setAttributes operation is generated for every cell.
     */
    handleSetAttrsDeleteColumns(localOp, extOp) {

        // the insert operation
        const deleteOp = localOp.name === Op.COLUMNS_DELETE ? localOp : extOp;
        // the set attributes operation
        const attrsOp = deleteOp === extOp ? localOp : extOp;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical position of the delete operation
        const deleteLength = deleteOp.start.length;
        // the index of the table inside the logical position
        const tableIndex = deleteLength - 1;
        // the index of the column inside the logical position
        const columnIndex = tableIndex + 2;
        // the index of the start grid
        const startGridProperty = deleteOp.startGrid;
        // the number of deleted columns
        const deleteColumns = deleteOp.endGrid - startGridProperty + 1;

        allPos.forEach(attrsPos => {
            // both positions have or have NOT the same length (setAttributes length is pretty arbitrary, must not be a text selection)
            if (equalPositions(deleteOp.start, attrsPos, deleteLength)) {
                if (attrsOp.start.length === deleteLength) {
                    // check for tableGrid property
                    if (attrsOp.attrs && attrsOp.attrs.table && attrsOp.attrs.table.tableGrid) {
                        attrsOp.attrs.table.tableGrid.splice(startGridProperty, deleteColumns);
                    }
                } else if ((is.number(attrsPos[columnIndex]))) {
                    if (attrsPos[columnIndex] >= deleteOp.startGrid) {
                        if (attrsPos[columnIndex] > deleteOp.endGrid) {
                            attrsPos[columnIndex] -= deleteColumns;
                        } else {
                            // the setAttributes operation cannot be applied, because the column is already removed
                            // -> it must be safe, that one setAttributes operation does not affect several cells
                            setOperationRemoved(attrsOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                        }
                    }
                }
            }
        });
    }

    /**
     * Handling of setAttributes operation with a mergeParagraph operation.
     * setAttributes operations and mergeParagraph operations influence each other, if:
     * - the merge paragraph operation is before or inside the range affected by the setAttributes
     *   operation. In this case the start position might be shifted, and the end position must be
     *   shifted.
     * -> the setAttributes operation never influences the position of the split paragraph operation.
     *
     * Info: A setAttribute operation contains not necessarily a text selection.
     * Info: A setAttribute operation does not necessarily contain an end position.
     */
    handleSetAttrsMergeComp(localOp, extOp) {

        // the merge operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the set attributes operation
        const attrsOp = (mergeOp === localOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the index of the text inside the logical position
        let textIndex = 0;
        // the collector for the logical positions of the attributes operation
        const allPos = attrsOp.end ? [attrsOp.start, attrsOp.end] : [attrsOp.start];
        // the length of the logical position of the merge operation
        const posLength = mergeOp.start.length;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';

        allPos.forEach(attrsPos => {

            if ((posLength === attrsPos.length - 1) && (posLength === 1 || equalPositions(mergeOp.start, attrsPos, posLength - 1))) {
                // merge position length is one shorter than setAttributes position length, setAttributes contains a text selection (because mergeOp is always a paragraph position)
                // both positions are top level (length === 2) or the paragraphs have the same parent (inside cell, text frame, ...)
                paraIndex = posLength - 1;
                textIndex = paraIndex + 1;
                if (mergeOp.start[paraIndex] < attrsPos[paraIndex] - 1) {
                    attrsPos[paraIndex]--; // decreasing position of setAttributes operation
                } else if (mergeOp.start[paraIndex] === attrsPos[paraIndex] - 1) {
                    // the split happens in the paragraph directly before the setAttributes position
                    attrsPos[paraIndex]--;
                    attrsPos[textIndex] += mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0;
                }
            } else {
                // both positions have or have NOT the same length for the common paragraph (setAttributes length is pretty arbitrary, must not be a text selection)
                paraIndex = posLength - 1;
                textIndex = paraIndex + 1;
                if (paraIndex === 0 || (is.number(attrsPos[paraIndex]) && equalPositions(mergeOp.start, attrsPos, paraIndex))) {
                    // the merge paragraph is top level (index is 0) or the parent of the paragraph of the merge operation is an ancestor of the attributes op
                    if (mergeOp.start[paraIndex] < attrsPos[paraIndex] - 1) {
                        attrsPos[paraIndex]--; // decrease the paragraph position of the setAttributes operation
                    } else if (mergeOp.start[paraIndex] === attrsPos[paraIndex] - 1) {
                        attrsPos[paraIndex]--;
                        if (is.number(attrsPos[textIndex])) { attrsPos[textIndex] += mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0; }
                    } else if (mergeOp.start[paraIndex] === attrsPos[paraIndex]) {
                        // What happens in setAttributes([6]) and mergeParagraph at (6,5), for example for background color of paragraph?
                        // -> this should not be a problem: First merge and following setting of background color has the same result as
                        //    first setting background color and then merge the modified paragraph with the following paragraph
                        // -> nothing to do
                    }
                }
            }
        });
    }

    /**
     * Handling a delete and an insert operation.
     *
     * Important: If the positions are equal, an insert operation shifts a delete operation, not vice versa.
     *
     * If the delete operation has a range, there are 3 different scenarios:
     *
     * Case 1: insert is before the start position (or equal to the start position) -> insert shifts both delete positions.
     * Case 2: insert is between start and end position -> insert operation needs to be removed.
     * Case 3: insert is after the end position -> delete shifts the insert position.
     */
    handleInsertCharDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_CHAR_ALIAS) ? localOp : extOp;
        // the length of the start position of the insert operation
        const insertCharLength = insertOp.start.length;
        // the collector for the logical positions of the delete operation
        let allPos = null;

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {
            let textIndex = 0;
            // is the position of the insert operation in the same paragraph as the start/end position of the delete operation?
            if ((insertCharLength <= deletePos.length) && equalPositions(insertOp.start, deletePos, insertCharLength - 1)) {
                textIndex = insertCharLength - 1;
                if (insertOp.start[textIndex] <= deletePos[textIndex]) { // insert shifts delete, if the positions are equal
                    deletePos[textIndex] += getInsertCharLength(insertOp); // the start/end position of the delete operation needs to be increased
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                }
            }
        };

        // Case 1: insert is before the start position (or equal to the start position) -> insert shifts both delete positions.
        // Case 2: insert is between start and end position -> insert operation needs to be removed.
        // Case 3: insert is after the end position -> delete shifts the insert position.

        if (comparePositions(insertOp.start, deleteOp.start) <= 0) {
            // 1. the specified position is before the delete start position (or it is the delete start position)
            allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];
            allPos.forEach((deletePos, index) => { modifyDeletePosition(deletePos, index); });
        } else if (ancestorRemoved(deleteOp, insertOp) || (deleteOp.end && comparePositions(insertOp.start, deleteOp.end) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (deleteOp.end) { modifyDeletePosition(deleteOp.end, 1); } // only modifying the end position
            setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, insertOp.start);
            // calculating the new values for the external position
            for (let index = 0; index < insertOp.start.length; index += 1) { insertOp.start[index] -= deleteArray[index]; }
        }
    }

    /**
     * Handling a delete and an splitParagraph (and splitTable) operation.
     */
    handleSplitCompDelete(localOp, extOp) {

        // the split operation
        const splitOp = this.hasAliasName(localOp, OT_SPLIT_COMP_ALIAS) ? localOp : extOp;
        // the delete operation
        const deleteOp = (splitOp === localOp) ? extOp : localOp;
        // the index of the paragraph/table inside the logical position
        let paraIndex = 0;
        // the index of the text/row inside the logical position
        let textIndex = 0;
        // the collector for the logical positions of the delete operation
        let allPos = null;
        // whether a merge operation is required additonally to the modified delete operation
        let mergeRequired = false;
        // a copy of the original delete operation
        let origDelete = null;
        // whether this is a paragraph operation (and no table operation)
        const isParagraphOperation = splitOp.name === Op.PARA_SPLIT;
        // the container for new external operations executed before the current external operation
        const externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        let externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        const localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        let localOpsAfter = null;

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {

            paraIndex = splitOp.start.length - 2;
            textIndex = paraIndex + 1;

            if (paraIndex === 0 || (is.number(deletePos[paraIndex]) && equalPositions(splitOp.start, deletePos, paraIndex))) {
                // the split paragraph is top level (index is 0) or the parent of the paragraph of the split operation is an ancestor of the delete op
                if (splitOp.start[paraIndex] === deletePos[paraIndex]) {
                    // the split position is before the delete.start position. The delete position might be longer
                    if (is.number(deletePos[textIndex])) {
                        if (splitOp.start[textIndex] <= deletePos[textIndex]) {
                            deletePos[paraIndex]++;
                            deletePos[textIndex] -= splitOp.start[textIndex];
                            if (index === 0) { deleteOp.start = deletePos; }
                            if (index === 1) { deleteOp.end = deletePos; }
                            // this is a text position for the end of the delete operation -> following paragraph merge operation is required
                            // because a delete from [0,5] to [0,8] does only delete the text content and does not merge the paragraphs.
                            mergeRequired = true;
                        }
                    } else {
                        deletePos[paraIndex]++; // a split in [1,3] leads to a delete of paragraph [2]
                        if (index === 0) { deleteOp.start = deletePos; }
                        if (index === 1) { deleteOp.end = deletePos; }
                    }
                } else if (splitOp.start[paraIndex] <= deletePos[paraIndex]) {
                    deletePos[paraIndex]++; // increase the paragraph position of the delete operation
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                }
            }
        };

        // helper function -> the split happened inside the deleted range -> adding a merge operation to the local stack of operations!
        const addMergeOperation = deleteStartPos => {

            paraIndex = splitOp.start.length - 2;
            textIndex = paraIndex + 1;

            if (!is.number(deleteStartPos[paraIndex])) { return; } // check, when this happens

            const mergeOperation = json.deepClone(splitOp);
            const lengthProperty = isParagraphOperation ? 'paralength' : 'rowcount';
            mergeOperation.name = isParagraphOperation ? 'mergeParagraph' : 'mergeTable';
            mergeOperation.start = deleteStartPos.slice(0, paraIndex + 1); // using all indices from '0' to 'paraIndex + 1' (returned by slice)
            mergeOperation[lengthProperty] = is.number(deleteStartPos[textIndex]) ? deleteStartPos[textIndex] : 0;

            // adding a new internal merge operation to the container of actions
            if (localOp === deleteOp) {
                localOpsAfter = [mergeOperation];
            } else {
                externalOpsAfter = [mergeOperation];
            }
        };

        // helper function to check if the deleted range is inside one paragraph. In this case an additional merge operation has to be generated.
        // -> this function does not check, if this are really text positions. Therefore a previous check for text positions is required.
        const isDeleteInsideParagraph = deleteOp => {
            return  deleteOp.start && deleteOp.end && deleteOp.start.length === deleteOp.end.length && equalPositions(deleteOp.start, deleteOp.end, deleteOp.start.length - 1);
        };

        // Case 1: split is before the start position (or equal to the start position) -> split shifts both delete positions.
        // Case 2: split is between start and end position -> split operation needs to be removed (also handling: delete removes the parent of split (delete[2] and splitParagraph[2,5]))
        // Case 3: split is after the end position -> delete shifts the split position.

        if (comparePositions(splitOp.start, deleteOp.start) <= 0) {
            // 1. the split position is before the delete start position (or it is exactly the delete start position)

            allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];

            allPos.forEach((oneDeletePos, index) => {
                modifyDeletePosition(oneDeletePos, index);
            });

        } else if (ancestorRemoved(deleteOp, splitOp) || (deleteOp.end && comparePositions(splitOp.start, deleteOp.end) <= 0)) {
            // 2. the split position is inside the deletion range
            if (!deleteOp.end) { deleteOp.end = json.deepClone(deleteOp.start); } // example: 'delete start:[1]' can be handled like 'delete start: [1] end: [1]'
            origDelete = json.deepClone(deleteOp); // creating a clone of the original delete operation, before it is modified
            modifyDeletePosition(deleteOp.end, 1);
            // the merge is required, if a new paragraph is generated, but text positions are used inside one paragraph: [0,1] -> [0,8] goes to [0,1] -> [1,2]
            // the merge is not required, if a new paragraph is generated, but the text positions are in different paragraphs: [0,1] -> [3,1] goes to [0,1] -> [4,1]
            // the merge is not required, if a new paragraph is generated, but no text positions are used: [0,1] -> [3] goes to [0,1] -> [4]
            if (mergeRequired && isDeleteInsideParagraph(origDelete)) { addMergeOperation(deleteOp.start); } // sometimes adding a mergeParagraph operation, so that following external operations are counted correctly
            setOperationRemoved(splitOp); // setting marker that the operation is not applied locally and not sent to the server
        } else {
            // 3. the split position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, splitOp.start);

            // calculating the new values for the external position
            for (let index = 0; index < splitOp.start.length; index += 1) { splitOp.start[index] -= deleteArray[index]; }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling a delete and an insertParagraph/insertTable operation.
     */
    handleInsertCompDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the insert operation
        const insertOp = this.hasAliasName(localOp, OT_INSERT_COMP_ALIAS) ? localOp : extOp;
        // the logical position of the insert operation
        const insertPos = insertOp.start;
        // the length of the position of the insert operation
        const insertPosLength = insertPos.length;
        // the index of the paragraph inside the logical position
        const paraIndex = insertPosLength - 1;
        // the collector for the logical positions of the delete operation
        let allPos = null;

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {

            if (paraIndex === 0 || (is.number(deletePos[paraIndex]) && equalPositions(insertOp.start, deletePos, paraIndex))) {
                // the insert operation is top level (index is 0) or the parent of the paragraph of the insert operation is an ancestor of the delete op
                if (insertOp.start[paraIndex] <= deletePos[paraIndex]) {
                    deletePos[paraIndex]++;
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                }
            }
        };

        // Case 1: insert is before the start position (or equal to the start position) -> insert shifts both delete positions.
        // Case 2: insert is between start and end position -> insert operation needs to be removed
        // Case 3: insert is after the end position -> delete shifts the insert position.

        if (comparePositions(insertPos, deleteOp.start) <= 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)

            allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];

            allPos.forEach((oneDeletePos, index) => {
                modifyDeletePosition(oneDeletePos, index);
            });

        } else if (ancestorRemoved(deleteOp, insertOp) || (deleteOp.end && comparePositions(insertPos, deleteOp.end) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (deleteOp.end) { modifyDeletePosition(deleteOp.end, 1); } // expanding the delete operation
            setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, insertPos);

            for (let index = 0; index < insertPosLength; index += 1) { insertOp.start[index] -= deleteArray[index]; }
        }
    }

    /**
     * Handling a delete and an insertRows/insertCells operation.
     */
    handleInsertRowsDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the insertRows/insertCells operation
        const insertOp = (localOp.name === Op.ROWS_INSERT || localOp.name === Op.CELLS_INSERT) ? localOp : extOp;
        // the logical position of the insertRows/insertCells operation
        const insertPos = insertOp.start;
        // the length of the position of the insertRows/insertCells operation
        const insertPosLength = insertPos.length;
        // the index of the row/cell inside the logical position
        const rowIndex = insertPosLength - 1;
        // the number of inserted rows/cells
        const rowCount = insertOp.count || 1;
        // the collector for the logical positions of the delete operation
        let allPos = null;

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {

            if (is.number(deletePos[rowIndex]) && equalPositions(insertOp.start, deletePos, rowIndex)) {
                // the parent of the paragraph of the insert operation is an ancestor of the delete op
                if (insertOp.start[rowIndex] <= deletePos[rowIndex]) {
                    deletePos[rowIndex] += rowCount; // increase the row position of the delete operation
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                }
            }
        };

        // Case 1: insertRows is before the start position (or equal to the start position) -> insertRows shifts both delete positions.
        // Case 2: insertRows is between start and end position -> insert operation needs to be removed
        // Case 3: insertRows is after the end position -> delete shifts the insertRows position.

        if (comparePositions(insertPos, deleteOp.start) <= 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)

            allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];

            allPos.forEach((oneDeletePos, index) => {
                modifyDeletePosition(oneDeletePos, index);
            });

        } else if (ancestorRemoved(deleteOp, insertOp) || (deleteOp.end && comparePositions(insertPos, deleteOp.end) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (deleteOp.end) { modifyDeletePosition(deleteOp.end, 1); } // expanding the delete operation
            setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, insertPos);

            for (let index = 0; index < insertPosLength; index += 1) { insertOp.start[index] -= deleteArray[index]; }

            // handling the reference row correctly (DOCS-4278)
            if (is.number(insertOp.referenceRow) && deleteArray[rowIndex] > 0) {
                if (endPosition[rowIndex] < insertOp.referenceRow) {
                    insertOp.referenceRow -= deleteArray[rowIndex]; // reducing the value for the reference row
                } else if (deleteOp.start[rowIndex] <= insertOp.referenceRow) {
                    delete insertOp.referenceRow; // the reference row was deleted
                }
            }
        }
    }

    /**
     * Handling a delete and an insertColumn operation.
     */
    handleInsertColumnDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the insertColumn operation
        const insertOp = localOp.name === Op.COLUMN_INSERT ? localOp : extOp;
        // the logical position of the insertRows operation
        const insertPos = insertOp.start;
        // the length of the position of the insertRows operation
        const insertPosLength = insertPos.length;
        // the index of the table inside the logical position
        const tableIndex = insertPosLength - 1;
        // the index of the column inside the logical position
        const columnIndex = tableIndex + 2;
        // the collector for the logical positions of the delete operation
        let allPos = null;
        // the specification, where the column is inserted
        const mode = insertOp.insertMode || 'behind';

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {

            if (is.number(deletePos[columnIndex]) && equalPositions(insertOp.start, deletePos, tableIndex)) {
                // the parent of the paragraph of the insert operation is an ancestor of the delete op
                if ((deletePos[columnIndex] > insertOp.gridPosition) || (mode === 'before' && deletePos[columnIndex] === insertOp.gridPosition)) {
                    deletePos[columnIndex]++; // increase the column position of the delete operation
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                }
            }
        };

        // Case 1: insertColumn is before the start position (or equal to the start position) -> insertColumn shifts both delete positions.
        // Case 2: insertColumn is between start and end position -> insert operation needs to be removed
        // Case 3: insertColumn is after the end position -> delete shifts the insertColumn position.

        if (comparePositions(insertPos, deleteOp.start) < 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)

            allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];

            allPos.forEach((oneDeletePos, index) => {
                modifyDeletePosition(oneDeletePos, index);
            });

        } else if (equalPositions(insertOp.start, deleteOp.start) || ancestorRemoved(deleteOp, insertOp) || (deleteOp.end && comparePositions(insertPos, deleteOp.end) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (deleteOp.end) { modifyDeletePosition(deleteOp.end, 1); } // expanding the delete operation
            setOperationRemoved(insertOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else {
            // 3. the insert position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, insertPos);

            for (let index = 0; index < insertPosLength; index += 1) { insertOp.start[index] -= deleteArray[index]; }
        }
    }

    /**
     * Handling a delete and a delectColumns operation.
     */
    handleDeleteColumnsDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the insertColumn operation
        const deleteColumnsOp = localOp.name === Op.COLUMNS_DELETE ? localOp : extOp;
        // the logical position of the insertRows operation
        const deleteColumnsPos = deleteColumnsOp.start;
        // the length of the position of the insertRows operation
        const deleteColumnsPosLength = deleteColumnsPos.length;
        // the index of the table inside the logical position
        const tableIndex = deleteColumnsPosLength - 1;
        // the index of the column inside the logical position
        const columnIndex = tableIndex + 2;
        // the collector for the logical positions of the delete operation
        let allPos = null;
        // the number of deleted columns
        let deleteColumns = 1;

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {

            if (is.number(deletePos[columnIndex]) && equalPositions(deleteColumnsOp.start, deletePos, tableIndex)) {
                // the parent of the paragraph of the insert operation is an ancestor of the delete op
                if (deletePos[columnIndex] >= deleteColumnsOp.startGrid) {
                    if (deletePos[columnIndex] > deleteColumnsOp.endGrid) {
                        deleteColumns = deleteColumnsOp.endGrid - deleteColumnsOp.startGrid + 1;
                        deletePos[columnIndex] -= deleteColumns;
                        if (index === 0) { deleteOp.start = deletePos; }
                        if (index === 1) { deleteOp.end = deletePos; }
                    } else {
                        // the delete operation cannot be applied, because its column is already removed
                        // -> it must be safe, that one delete operation does not affect several cells (with different cell position for start and end property)
                        setOperationRemoved(deleteOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
                    }
                }
            }
        };

        // Case 1: deleteColumns is before the start position (or equal to the start position) -> deleteColumns shifts both delete positions.
        // Case 2: deleteColumns is between start and end position -> deleteColumns operation needs to be removed (example: table was completely removed)
        // Case 3: deleteColumns is after the end position -> delete shifts the deleteColumns position.

        if (comparePositions(deleteColumnsPos, deleteOp.start) < 0) {
            // 1. the insert position is before the delete start position (or it is exactly the delete start position)

            allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];

            allPos.forEach((oneDeletePos, index) => {
                if (!isOperationRemoved(deleteOp)) { modifyDeletePosition(oneDeletePos, index); }
            });

        } else if (equalPositions(deleteColumnsOp.start, deleteOp.start) || ancestorRemoved(deleteOp, deleteColumnsOp) || (deleteOp.end && comparePositions(deleteColumnsPos, deleteOp.end) <= 0)) {
            // 2. the insert position is inside the deletion range
            if (deleteOp.end) { modifyDeletePosition(deleteOp.end, 1); } // expanding the delete operation
            setOperationRemoved(deleteColumnsOp); // setting marker that this operation is not used, not applied locally and not sent to the server.
        } else {
            // 3. the deleteColumns position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, deleteColumnsPos);

            for (let index = 0; index < deleteColumnsPosLength; index += 1) { deleteColumnsOp.start[index] -= deleteArray[index]; }
        }
    }

    /**
     * Handling a delete and a paragraph merge operation.
     */
    handleMergeCompDelete(localOp, extOp) {

        // the merge operation
        const mergeOp = this.hasAliasName(localOp, OT_MERGE_COMP_ALIAS) ? localOp : extOp;
        // the delete operation
        const deleteOp = (mergeOp === localOp) ? extOp : localOp;
        // the index of the paragraph inside the logical position
        let paraIndex = 0;
        // the last index
        let lastIndex = 0;
        // the collector for the logical positions of the delete operation
        let allPos = null;
        // a merge position that includes the text position for the merge
        let mergePos = null;
        // a required new operation
        let newOperation = null;
        // whether this is a merge of a paragraph (and not a table)
        const isParaMerge = mergeOp.name === Op.PARA_MERGE;
        // the name of the property containing the length of the leading paragraph/table
        const lengthProperty = isParaMerge ? 'paralength' : 'rowcount';
        // the container for new external operations executed before the current external operation
        let externalOpsBefore = null;
        // the container for new external operations executed after the current external operation
        const externalOpsAfter = null;
        // the container for new local operations executed before the current local operation
        let localOpsBefore = null;
        // the container for new local operations executed after the current local operation
        const localOpsAfter = null;

        // helper function to modify the start/end position of the delete operation
        const modifyDeletePosition = (deletePos, index) => {

            paraIndex = mergeOp.start.length - 1;
            lastIndex = paraIndex + 1;

            if (paraIndex === 0 || (is.number(deletePos[paraIndex]) && equalPositions(mergeOp.start, deletePos, paraIndex))) {
                // the merge paragraph is top level (index is 0) or the parent of the paragraph of the merge operation is an ancestor of the delete op
                if (mergeOp.start[paraIndex] === deletePos[paraIndex] - 1) {
                    deletePos[paraIndex]--;
                    if (is.number(deletePos[lastIndex])) { deletePos[lastIndex] += (mergeOp[lengthProperty] ? mergeOp[lengthProperty] : 0); }
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                } else if (mergeOp.start[paraIndex] < deletePos[paraIndex] - 1) {
                    deletePos[paraIndex]--; // decrease the paragraph position of the delete operation
                    if (index === 0) { deleteOp.start = deletePos; }
                    if (index === 1) { deleteOp.end = deletePos; }
                } else if (index === 1 && mergeOp.start.length === deletePos.length && mergeOp.start[paraIndex] === deletePos[paraIndex]) {
                    // the complete paragraph of the merge operation was removed (delete end position is [3] and merge also in [3])
                    deletePos.push(mergeOp[lengthProperty] - 1);
                    deleteOp.end = deletePos;
                }
            }
        };

        // helper function to create a delete range, if only the paragraph of the merge operation is removed
        const generateRangeInDeleteOp = deleteOp => {
            deleteOp.end = json.deepClone(deleteOp.start);
            deleteOp.start.push(0); // always deleting from the beginning of the paragraph
        };

        // helper function that checks, if a remote merge operation can be ignored
        const isRemovalOfFollowingParagraph = (deleteOp, mergeOp) => {
            const lastIdx = deleteOp.start.length - 1;
            return deleteOp.start.length === mergeOp.start.length && equalPositions(deleteOp.start, mergeOp.start, lastIdx) && deleteOp.start[lastIdx] - 1 === mergeOp.start[lastIdx];
        };

        // helper function that checks if the merge is still required. This is the case for delete operations
        // like delete [0,4] to [1,2], because this operation does NOT remove a single paragraph, it only removes text.
        // Therefore an external merge operation cannot be ignored.
        const mergeRequired = (deleteOp, mergeOp) => {

            let mergeRequired = false;
            const deleteStartLength = deleteOp.start.length;
            const deleteEndLength = deleteOp.end && deleteOp.end.length;
            const mergeStartLength = mergeOp.start.length;

            if (deleteStartLength === deleteEndLength && deleteStartLength === mergeStartLength + 1) {
                const paraIndex = mergeStartLength - 1;
                if (deleteOp.end && deleteOp.start[paraIndex] === deleteOp.end[paraIndex] - 1) { // the direct following paragraph
                    mergeRequired = true;
                }
            }
            return mergeRequired;
        };

        // Case 1: merge is before the start position (or equal to the start position) -> merge shifts both delete positions.
        // Case 2: merge is between start and end position -> split operation needs to be removed (also handling: delete removes the parent of split (delete[2] and mergeParagraph[2,5,6,7]))
        // Case 3: merge is after the end position -> delete shifts the merge position.

        // a merge position that includes the text position
        mergePos = json.deepClone(mergeOp.start);
        mergePos.push(mergeOp[lengthProperty]); // -> adding the paragraph length, so that a text position is generated

        if (comparePositions(mergePos, deleteOp.start) <= 0) {
            // 1. the merge position is before the delete start position (or it is exactly the delete start position)

            // special case: If a full following paragraph is removed, the mergeOperation can be ignored
            if (isRemovalOfFollowingParagraph(deleteOp, mergeOp)) {
                setOperationRemoved(mergeOp); // setting marker for removal -> but the delete operation gets a previous split operation

                // adding a new split operation in front of the delete operation
                newOperation = json.deepClone(deleteOp);
                newOperation.name = mergeOp.name === Op.PARA_MERGE ? 'splitParagraph' : 'splitTable';
                newOperation.start = _.copy(mergeOp.start);
                newOperation.start.push(mergeOp[lengthProperty]); // split at [1, 5], if there was a mergeOperation at [1] with length 5
                delete newOperation.end;

                if (extOp === deleteOp) {
                    externalOpsBefore = [newOperation]; // inserting before the current external operation
                } else {
                    localOpsBefore = [newOperation]; // inserting before the current local operation
                }
            } else {
                allPos = deleteOp.end ? [deleteOp.start, deleteOp.end] : [deleteOp.start];

                allPos.forEach((oneDeletePos, index) => {
                    modifyDeletePosition(oneDeletePos, index);
                });
            }

        } else if (ancestorRemoved(deleteOp, { start: mergePos }) || (deleteOp.end && comparePositions(mergePos, deleteOp.end) <= 0)) {
            // 2. the merge position is inside the deletion range
            const isMergeRequired = mergeRequired(deleteOp, mergeOp);
            if (!deleteOp.end && deleteOp.start.length === mergeOp.start.length) { generateRangeInDeleteOp(deleteOp); } // this might be the case for delete start [1] and mergeParagraph [1]
            if (deleteOp.end) { modifyDeletePosition(deleteOp.end, 1); } // shrinking the delete operation
            if (!isMergeRequired) { setOperationRemoved(mergeOp); } // setting marker for removal

        } else {
            // 3. the merge position is behind the delete end position -> its position must be modified.
            const endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
            const deleteArray = calculateDeleteArray(deleteOp.start, endPosition, mergePos);

            // calculating the new values for the external position
            lastIndex = mergeOp.start.length - 1;

            for (let index = 0; index < mergeOp.start.length + 1; index += 1) {
                const isLast = (index > lastIndex);
                if (isLast) {
                    mergeOp[lengthProperty] -= deleteArray[index]; // modifying the paralength/rowcount attribute, not the start position
                } else {
                    mergeOp.start[index] -= deleteArray[index];
                }
            }
        }

        return getResultObject(externalOpsBefore, externalOpsAfter, localOpsBefore, localOpsAfter);
    }

    /**
     * Handling a delete and a setAttributes operation.
     *
     * Within this function the delete operation is not modified.
     *
     * The following scenarios are considered;
     *  Case 1: The two ranges are completely separated.
     *  Case 2: The two ranges are identical.
     *  Case 3: One range is completely included in the other range.
     *  Case 4: The two ranges are overlapping partly.
     */
    handleSetAttrsDelete(localOp, extOp) {

        // the delete operation
        const deleteOp = localOp.name === Op.DELETE ? localOp : extOp;
        // the set attributes operation
        const attrsOp = deleteOp === extOp ? localOp : extOp;
        // the array containing the shift of position
        let deleteArray;
        // an index counter
        let index = 0;
        // an optionally existing end position of a delete operation
        let endPosition;
        // whether the two delete ranges surround each other
        let surrounding = false;

        if (separateRanges(deleteOp, attrsOp)) {

            if (comparePositions(deleteOp.start, attrsOp.start) < 0) {
                endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
                deleteArray = calculateDeleteArray(deleteOp.start, endPosition, attrsOp.start);
                for (index = 0; index < attrsOp.start.length; index += 1) { attrsOp.start[index] -= deleteArray[index]; }

                if (attrsOp.end) {
                    deleteArray = calculateDeleteArray(deleteOp.start, endPosition, attrsOp.end);
                    for (index = 0; index < attrsOp.end.length; index += 1) { attrsOp.end[index] -= deleteArray[index]; }
                }
            }

        } else if (identicalRanges(deleteOp, attrsOp))  {
            // delete range is identical with setAttributes range -> setAttributes can be ignored
            setOperationRemoved(attrsOp); // setting marker at setAttributes operation!
        } else {

            surrounding = surroundingRanges(deleteOp, attrsOp);

            if (surrounding) {

                if (surrounding === 1) {
                    // delete surrounds setAttributes operation -> setAttributes can be ignored
                    setOperationRemoved(attrsOp); // setting marker at setAttributes operation
                } else {
                    // setAttributes surrounds the delete operation -> shrinking the setAttributes operation range
                    if (attrsOp.end) {
                        endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
                        deleteArray = calculateDeleteArray(deleteOp.start, endPosition, attrsOp.end);
                        for (index = 0; index < attrsOp.end.length; index += 1) { attrsOp.end[index] -= deleteArray[index]; }
                    }
                }

            } else {
                // overlapping ranges
                // -> 3 new ranges:
                //     - part only covered by attrsOp
                //     - part only covered by deleteOp
                //     - part covered by both operations

                // -> both operations need to get shrinked ranges
                if (comparePositions(deleteOp.start, attrsOp.start) <= 0) {

                    // shrinking the range of the setAttributes operation
                    endPosition = deleteOp.end ? deleteOp.end : deleteOp.start;
                    attrsOp.start = increaseLastIndex(endPosition);
                    // shifting the two positions of the setAttributes operation to the front
                    deleteArray = calculateDeleteArray(deleteOp.start, endPosition, attrsOp.start);
                    for (index = 0; index < attrsOp.start.length; index += 1) { attrsOp.start[index] -= deleteArray[index]; }
                    deleteArray = calculateDeleteArray(deleteOp.start, endPosition, attrsOp.end);
                    for (index = 0; index < attrsOp.end.length; index += 1) { attrsOp.end[index] -= deleteArray[index]; }

                } else {

                    // modifying the setAttributes operation in that way, that only the missing part is removed from the local document
                    attrsOp.end = increaseLastIndex(deleteOp.start, -1);
                }
            }
        }
    }

}
