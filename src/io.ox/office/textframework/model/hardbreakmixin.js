/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { getClosestTextFrameDrawingNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { createHardBreakNode, isManualPageBreakNode, isParagraphNode, isTableNode,
    splitTextSpan } from '@/io.ox/office/textframework/utils/dom';
import { DELETE, HARDBREAK_INSERT, PARA_INSERT, PARA_SPLIT, SET_ATTRIBUTES,
    TABLE_SPLIT } from '@/io.ox/office/textframework/utils/operations';
import { getContentNodeElement, getFirstPositionInCurrentCell, getFirstPositionInParagraph,
    increaseLastIndex } from '@/io.ox/office/textframework/utils/position';

function mixinHardBreak(BaseClass) {

    // mix-in class HardBreakMixin ============================================

    /**
     * A mix-in class for the hard break handling in the document model.
     * And additionally for the manual page break handling.
     */

    class HardBreakMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Inserting a hard line break, that leads to a new line inside a paragraph. This
         * function can be called from the side pane with a button or with 'Shift+Enter' from
         * within the document.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        insertHardBreak() {

            // the undo manager object
            const undoManager = this.getUndoManager();

            // the undo manager returns the return value of the callback function
            return undoManager.enterUndoGroup(() => {

                // the selection object
                const selection = this.getSelection();

                const doInsertHardBreak = () => {
                    const start = selection.getStartPosition();
                    const operation = { name: HARDBREAK_INSERT, start: _.copy(start) };
                    // target for operation - if exists, it's for ex. header or footer
                    const target = this.getActiveTarget();

                    this.extendPropertiesWithTarget(operation, target);
                    this.doCheckImplicitParagraph(start);
                    // modifying the attributes, if changeTracking is activated
                    if (this.getChangeTrack().isActiveChangeTracking()) {
                        operation.attrs = operation.attrs || {};
                        operation.attrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null };
                    }
                    // adding character attributes for the first character in the paragraph (DOCS-4211)
                    const additionalCharacterAttrs = this.collectCharacterAttributesForInsertOperation(this.getCurrentRootNode(this.getActiveTarget()), start);
                    if (additionalCharacterAttrs) {
                        operation.attrs = operation.attrs || {};
                        operation.attrs.character = additionalCharacterAttrs;
                    }

                    this.applyOperations(operation);
                };

                if (selection.hasRange()) {
                    return this.deleteSelected()
                        .done(() => {
                            doInsertHardBreak();
                            selection.setTextSelection(this.getLastOperationEnd()); // finally setting the cursor position
                        });
                }

                doInsertHardBreak();
                selection.setTextSelection(this.getLastOperationEnd()); // finally setting the cursor position
                return $.when();

            }, this);

        }

        /**
         * Inserting a manual page break, that leads to a new page inside a document. This
         * function can be called from the toolbar with a button or with 'CTRL+Enter' from
         * within the document.
         *
         * @returns {jQuery.Promise | void} // TODO: check-void-return
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        insertManualPageBreak() {

            // the undo manager object
            const undoManager = this.getUndoManager();

            // no page break in header/footer or comments or text frames
            if (this.getSelection().isAdditionalTextframeSelection() || this.isTargetFunctionality()) { return; }

            // the undo manager returns the return value of the callback function
            return undoManager.enterUndoGroup(() => {

                // the selection object
                const selection = this.getSelection();
                // start position of selection
                let start = selection.getStartPosition();
                // copy of start position, prepared for modifying
                const positionOfCursor = _.clone(start);
                // last index in positionOfCursor array, used for incrementing cursor position
                const lastIndex = positionOfCursor.length - 1;
                // the page node
                const editdiv = this.getNode();
                // first level element, paragraph or table
                const firstLevelNode = getContentNodeElement(editdiv, start.slice(0, 1));
                // object passed to generateOperation method
                let operationOptions;
                // operations generator
                const generator = this.createOperationGenerator();

                const doInsertManualPageBreak = () => {

                    // the attributes for the paragraph between the tables
                    const paraAttrs = {};
                    // the change track object
                    const changeTrack = this.getChangeTrack();

                    // refreshing start position, that might be modified by deleteSelected()
                    start = selection.getStartPosition();

                    // different behavior for inserting page break inside paragraphs and tables
                    if (isParagraphNode(firstLevelNode)) {

                        // first split paragraph
                        generator.generateOperation(PARA_SPLIT, { start: _.copy(start) });
                        this.doCheckImplicitParagraph(start);
                        //update cursor position value after paragraph split
                        positionOfCursor[lastIndex - 1] += 1;
                        positionOfCursor[lastIndex] = 0;
                        // set paragraph attribute page break
                        operationOptions = { start: _.copy(positionOfCursor.slice(0, -1)), attrs: { paragraph: { pageBreakBefore: true } } };

                        if (changeTrack.isActiveChangeTracking()) {
                            operationOptions.attrs.changes = { inserted: changeTrack.getChangeTrackInfo(), removed: null };
                        }
                        generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                    } else if (isTableNode(firstLevelNode)) {

                        const firstParagraphPosHelper = positionOfCursor.slice(0, 3);
                        // we always apply pageBreakBefore attribute to the first paragraph in table - because of compatibility with MS Word
                        firstParagraphPosHelper[2] = 0;
                        let firstParagraphPosInFirstCell = getFirstPositionInCurrentCell(editdiv, firstParagraphPosHelper);
                        // before applying paragraph attribute, we need to check if it's implicit paragraph, #34624
                        // #35328 - table is not yet split, and we dont check impl paragraph for cursor position, but for first par in row, where we apply pageBreakBefore attr
                        this.doCheckImplicitParagraph(firstParagraphPosInFirstCell);

                        // different behavior for odt and msword page breaks in table
                        if (this.#docApp.isODF()) {
                            if (start[0] === 0 || (isManualPageBreakNode(firstLevelNode))) {
                                // if table is at first position in document, or there is already page break applied to table, ODF ignores operation
                                return;
                            }

                            // #35446 - odf applies pageBreakBefore attr always on first cell in table, and doesn't split table
                            firstParagraphPosInFirstCell = getFirstPositionInParagraph(editdiv, positionOfCursor.slice(0, 1));
                            this.doCheckImplicitParagraph(firstParagraphPosInFirstCell);
                            // odf specific, set attr to table, too, #35585
                            operationOptions = { start: _.copy(firstParagraphPosInFirstCell.slice(0, -1)), attrs: { paragraph: { pageBreakBefore: true } } };
                            generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                        } else {
                            // split table with page break, but only if it's not first row. Otherwise, just insert new paragraph before table
                            if (positionOfCursor[1] !== 0) {
                                generator.generateOperation(TABLE_SPLIT, { start: _.copy(positionOfCursor.slice(0, 2)) }); // passing row position of table closest to pagecontent node (top level table)

                                firstParagraphPosInFirstCell[0] += 1;
                                firstParagraphPosInFirstCell[1] = 0;

                                operationOptions = { start: _.copy(firstParagraphPosInFirstCell.slice(0, -1)), attrs: { paragraph: { pageBreakBefore: true } } };
                                generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                                // Insert empty paragraph in between split tables.
                                // Adding change track information only to the inserted paragraph and only, if
                                // change tracking is active.
                                // -> the splitted table cannot be marked as inserted. Rejecting this change track
                                // requires automatic merge of the splitted table, if possible.
                                if (changeTrack.isActiveChangeTracking()) {
                                    paraAttrs.changes = { inserted: changeTrack.getChangeTrackInfo(), removed: null };
                                }

                                // update position of cursor
                                positionOfCursor[0] += 1;

                                // #36130 - avoid error in console: Selection.restoreBrowserSelection(): missing text selection range
                                selection.setTextSelection(getFirstPositionInParagraph(editdiv, start.slice(0, 1)));

                                // insert empty paragraph in between split tables
                                generator.generateOperation(PARA_INSERT, { start: _.copy(positionOfCursor.slice(0, 1)), attrs: paraAttrs });

                                // update position of cursor
                                positionOfCursor[0] += 1;
                                positionOfCursor[1] = 0;

                            } else {
                                operationOptions = { start: _.copy(firstParagraphPosInFirstCell.slice(0, -1)), attrs: { paragraph: { pageBreakBefore: true } } };
                                generator.generateOperation(SET_ATTRIBUTES, operationOptions);

                                // if we apply page break to element that already has that property, set to paragraph that is inserted before it
                                if (isManualPageBreakNode(firstLevelNode)) {
                                    paraAttrs.paragraph = { pageBreakBefore: true };
                                }

                                // Insert empty paragraph in between split tables.
                                // Adding change track information only to the inserted paragraph and only, if
                                // change tracking is active.
                                // -> the splitted table cannot be marked as inserted. Rejecting this change track
                                // requires automatic merge of the splitted table, if possible.
                                if (changeTrack.isActiveChangeTracking()) {
                                    paraAttrs.changes = { inserted: changeTrack.getChangeTrackInfo(), removed: null };
                                }

                                // insert empty paragraph in between split tables
                                generator.generateOperation(PARA_INSERT, { start: _.copy(positionOfCursor.slice(0, 1)), attrs: paraAttrs });

                                // update position of cursor
                                positionOfCursor[0] += 1;

                                // #36130 - avoid error in console: Selection.restoreBrowserSelection(): missing text selection range
                                selection.setTextSelection(getFirstPositionInParagraph(editdiv, positionOfCursor.slice(0, 1)));
                            }
                        }
                    }

                    this.applyOperations(generator);
                };

                if (selection.hasRange() && !isTableNode(firstLevelNode)) { // #35346 don't delete if table is selected
                    return this.deleteSelected()
                        .done(() => {
                            doInsertManualPageBreak();
                            selection.setTextSelection(positionOfCursor); // finally setting the cursor position
                        });
                }

                doInsertManualPageBreak();
                selection.setTextSelection(positionOfCursor); // finally setting the cursor position

                // waiting for new pagination to scroll to the new position (DOCS-1945)
                this.waitForEvent(this, 'pagination:finished', 1000).then(() => { this.#docApp.getView().scrollToSelection(); });

                return $.when();

            }, this);

        }

        // operation handler --------------------------------------------------

        /**
         * The handler for the insertHardBreak operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the hard break was inserted successfully.
         */
        insertHardBreakHandler(operation, external) {

            // the undo manager
            const undoManager = this.getUndoManager();

            if (!this.#implInsertHardBreak(operation.start, operation.type, operation.attrs, operation.target)) {
                return false;
            }

            if (undoManager.isUndoEnabled() && !external) {
                const undoOperation = { name: DELETE, start: _.copy(operation.start) };
                this.extendPropertiesWithTarget(undoOperation, operation.target);
                undoManager.addUndo(undoOperation, operation);
            }

            return true;
        }

        // private methods ----------------------------------------------------

        /**
         * Inserts a hard break component into the document DOM.
         *
         * @param {Number[]} start
         *  The logical start position for the new tabulator.
         *
         * @param {string} type
         *
         * @param {Object} [attrs]
         *  Attributes to be applied at the new hard-break component, as map of
         *  attribute maps (name/value pairs), keyed by attribute family.
         *
         * @param {string[]} [target]
         *
         * @returns {Boolean}
         *  Whether the tabulator has been inserted successfully.
         */
        #implInsertHardBreak(start, type, attrs, target) {

            // text span that will precede the hard-break node
            const span = this.prepareTextSpanForInsertion(start, {}, target);
            // new text span for the hard-break node
            let hardbreakSpan = null;
            // element from which we calculate pagebreaks
            const currentElement = span ? span.parentNode : '';
            // the hard break type
            const hardBreakType = type || 'textWrapping';  // defaulting to 'textWrapping'

            if (!span) { return false; }

            // expanding attrs to disable change tracking, if not explicitely set
            attrs = this.checkChangesMode(attrs);

            // split the text span to get initial character formatting for the hard-break (handling inheritance)
            hardbreakSpan = splitTextSpan(span, 0);

            // insert a hard-break container node before the addressed text node, move
            // the hard-break span element into the hard-break container node
            // Fix for 29265: Replacing '.empty()' of hardbreakSpan with '.contents().remove().end()'
            // to get rid of empty text node in span in IE.

            // TODO: implementing hard break type 'column'
            // -> fallback to text node with one space, so that counting is still valid.
            if ((hardBreakType === 'page') || (hardBreakType === 'column')) {
                hardbreakSpan.contents().remove().end().text('\xa0');  // non breakable space
                hardbreakSpan.addClass('ms-hardbreak-page');
                $(currentElement).addClass('manual-page-break');
            } else {
                hardbreakSpan.contents().remove().end().append($('<br>'));
            }

            createHardBreakNode(hardBreakType).append(hardbreakSpan).insertAfter(span);

            // apply the passed tab attributes
            if (_.isObject(attrs)) {
                this.characterStyles.setElementAttributes(hardbreakSpan, attrs);
            }

            // validate paragraph, store new cursor position
            this.implParagraphChanged(span.parentNode);
            this.setLastOperationEnd(increaseLastIndex(start));

            // don't call explicitly page break rendering, if target for header/footer comes with operation
            if (target) {
                this.setBlockOnInsertPageBreaks(true);
            }

            if ($(currentElement).data('lineBreaksData')) {
                $(currentElement).removeData('lineBreaksData'); //we changed paragraph layout, cached line breaks data needs to be invalidated
            }

            // call for debounced render of pagebreaks
            this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(span.parentNode));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            // immediately updating element formatting to underline the tab correctly
            // -> This is not necessary with localStorage and fastLoad during loading
            if (this.isImportFinished() && this.getChangeTrack().isActiveChangeTracking()) {
                this.paragraphStyles.updateElementFormatting(span.parentNode);
            }

            return true;
        }
    }

    return HardBreakMixin;
}

// exports ====================================================================

export const XHardBreak = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XHardBreak>}
     *  The extended class with hard break and page break functionality.
     */
    mixin: mixinHardBreak
};
