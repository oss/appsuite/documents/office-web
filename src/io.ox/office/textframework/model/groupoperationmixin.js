/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';

import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { getRotatedDrawingPoints, isAngleInButterflyArea } from '@/io.ox/office/drawinglayer/utils/drawingutils';

import * as DrawingFrame from '@/io.ox/office/drawinglayer/view/drawingframe';
import { SLIDE_SELECTOR } from '@/io.ox/office/textframework/utils/dom';
import { GROUP, UNGROUP } from '@/io.ox/office/textframework/utils/operations';
import { getDOMPosition, getOxoPosition, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';
import { convertLengthToHmm, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';

function mixinGroupOperation(BaseClass) {

    // mix-in class GroupOperationMixin =======================================

    /**
     * A mix-in class for the document model class providing the group
     * operation handling used in a presentation and text document. This
     * includes the grouping and ungrouping of drawings.
     *
     * @param {EditApplication} app
     *  The application instance.
     */
    class GroupOperationMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Generating a 'group' operation for the selected drawings.
         */
        groupDrawings() {

            // the operations generator
            const generator = this.createOperationGenerator();
            // the selection object
            const selection = this.getSelection();
            // a collector for all logical positions of the selected drawings
            let allPositions = null;
            // a collector for all drawing indices of the selected drawings
            let drawingIndices = null;
            // created operation
            let newOperation = null;
            // the drawing attributes
            let drawingAttrs = null;

            if (selection.isMultiSelectionSupported() && selection.isMultiSelection()) {

                // collecting the logical positions of all selected drawings
                allPositions = selection.getArrayOfLogicalPositions();

                drawingIndices = _.map(allPositions, pos => { return _.last(pos); });

                // calculating the position (left, top, width, height) for the drawing of type 'group' -> this is application specific
                if (this.useSlideMode()) {
                    drawingAttrs = this.#getGroupDrawingPositionAttributes(this.getSelection().getArrayOfSelectedDrawingNodes());
                    drawingAttrs.id = this.getNewDrawingId();
                }

                // creating the operation for grouping the selected drawings
                newOperation = { start: _.clone(allPositions[0]), drawings: drawingIndices };

                if (drawingAttrs) { newOperation.attrs = { drawing: drawingAttrs }; }
                generator.generateOperation(GROUP, newOperation);

                // applying operation
                this.applyOperations(generator);

                // clearing the existing multi selection (no trigger of selection change required)
                selection.clearMultiSelection({ triggerChange: false });

                // selecting the grouped drawing
                selection.setTextSelection(allPositions[0], increaseLastIndex(allPositions[0]));

                // an additional trigger is required to set correct selection type 'drawing' (f.e. required for remote selection)
                selection.trigger('change');
            }
        }

        /**
         * Generating an 'ungroup' operation for the selected drawing(s) of type group.
         */
        ungroupDrawings() {

            // the operations generator
            const generator = this.createOperationGenerator();
            // created operation
            let newOperation = null;
            // the selection object
            const selection = this.getSelection();
            // the logical position of the selected drawing
            let start = selection.isDrawingSelection() ? selection.getStartPosition() : null;
            // a selected drawing group node (drawing selection or additional drawing selection)
            let drawingNode = null;
            // the content node inside the selected drawing(s)
            let contentNode = null;
            // the number of drawing children inside a selected group
            let childrenCount = 0;
            // the logical positions of the ungrouped drawings
            let positions = null;
            // an offset for the logical position caused by previous ungrouping
            let offset = 0;
            // the group selections of a multiple selection
            let allDrawingGroupSelections = null;
            // collector for all non group drawing selections
            let allNonGroupSelections = null;
            // collector for all drawing node / position pairs
            let allGroupDrawings = null;
            // a collector for all ungrouped drawings(to create the following selection)
            let allUngroupedDrawingNodes = $();

            // getting the selected drawing group
            if (selection.isMultiSelectionSupported() && selection.isMultiSelection()) {
                allDrawingGroupSelections = selection.getAllDrawingGroupsFromMultiSelection();

                if (!allDrawingGroupSelections || allDrawingGroupSelections.length === 0) { return; } // no drawing group selected

                allGroupDrawings = [];

                _.each(allDrawingGroupSelections, sel => {
                    drawingNode = selection.getDrawingNodeFromMultiSelection(sel);
                    start = selection.getStartPositionFromMultiSelection(sel);
                    allGroupDrawings.push({ node: drawingNode, start });
                });

                // saved all non groups, so that they can be selected again after ungrouping
                allNonGroupSelections = selection.getAllNoneDrawingGroupsFromMultiSelection();
                _.each(allNonGroupSelections, sel => { allUngroupedDrawingNodes = allUngroupedDrawingNodes.add(selection.getDrawingNodeFromMultiSelection(sel)); });

            } else if (selection.isDrawingSelection()) {
                drawingNode = selection.getSelectedDrawing();
                start = selection.getStartPosition();
                if (!DrawingFrame.isGroupDrawingFrame(drawingNode)) { return; } // no drawing group selected
                if (this.useSlideMode() && start.length > 2) { start = getOxoPosition(this.getCurrentRootNode(), drawingNode, 0); }
                allGroupDrawings = [{ node: drawingNode, start }];
            } else if (selection.isAdditionalTextframeSelection()) {
                drawingNode = selection.getSelectedTextFrameDrawing();
                start = getOxoPosition(this.getCurrentRootNode(), drawingNode, 0);
                if (!DrawingFrame.isGroupDrawingFrame(drawingNode)) { return; } // no drawing group selected
                allGroupDrawings = [{ node: drawingNode, start }];
            }

            // Iterating over all selected drawing groups
            _.each(allGroupDrawings, groupDrawing => {

                // the logical position of the group drawing node
                let groupPos = _.clone(groupDrawing.start);
                // all child drawings in the drawing group
                let allChildDrawings = null;

                if (offset > 0) { groupPos = increaseLastIndex(groupPos, offset); }

                // calculating all new positions (required for undo!)
                positions = [];
                contentNode = DrawingFrame.getContentNode(groupDrawing.node);
                allChildDrawings = contentNode.children(DrawingFrame.NODE_SELECTOR);
                childrenCount = allChildDrawings.length;

                _.each(_.range(childrenCount), index => {
                    positions.push(increaseLastIndex(groupPos, index));
                });

                // creating the operation for grouping the selected drawings
                newOperation = { start: _.clone(groupPos), drawings: _.map(positions, pos => { return _.last(pos); }) };
                generator.generateOperation(UNGROUP, newOperation);

                // setting the offset for a following ungroup operation
                offset += (childrenCount - 1);

                allUngroupedDrawingNodes = allUngroupedDrawingNodes.add(allChildDrawings);
            });

            // applying operation
            this.applyOperations(generator);

            // selecting all ungrouped drawings
            selection.setMultiDrawingSelection(allUngroupedDrawingNodes);
        }

        // operation handler --------------------------------------------------

        /**
         * Handler for a 'group' operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the drawings were grouped successfully.
         */
        groupDrawingsHandler(operation, external) {

            if (!this.#implGroupDrawings(operation.start, operation.target, operation.drawings, operation.attrs, operation.childAttrs, external)) { return false; }

            if (this.getUndoManager().isUndoEnabled() && !external) {
                const undoOperation = { name: UNGROUP, start: _.copy(operation.start), target: operation.target, drawings: operation.drawings, attrs: operation.attrs };
                this.getUndoManager().addUndo(undoOperation, operation);
            }

            return true;
        }

        /**
         * Handler for an 'ungroup' operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the drawings were ungrouped successfully.
         */
        ungroupDrawingsHandler(operation, external) {

            // a collector for the drawing positions inside the group
            // -> this needs to be collected for an undo of a rotated drawing group
            const drawingChildAttrs = [];
            // a collector for the drawing attributes of the drawing of type group
            const drawingGroupAttrs = [];

            if (!this.#implUngroupDrawings(operation.start, operation.target, operation.drawings, drawingGroupAttrs, drawingChildAttrs, external)) { return false; }

            if (this.getUndoManager().isUndoEnabled() && !external) {
                const undoOperation = { name: GROUP, start: _.copy(operation.start), target: operation.target, drawings: operation.drawings, attrs: (drawingGroupAttrs.length > 0) ? drawingGroupAttrs[0] : null, childAttrs: (drawingChildAttrs.length > 0) ? drawingChildAttrs : null };
                this.getUndoManager().addUndo(undoOperation, operation);
            }

            return true;
        }

        // private methods ----------------------------------------------------

        /**
         * Handler for grouping the specified drawings.
         *
         * @param {Number[]} start
         *  The logical position of the grouped drawing.
         *
         * @param {String} target
         *  The id of the layout slide, that is the base for the specified slide.
         *
         * @param {Number[][]} drawings
         *  The logical positions of all drawings that will be grouped.
         *
         * @param {Object} attrs
         *  The drawing attributes. Especially the position and the size of the
         *  drawing group.
         *
         * @param {Object[]} [childAttrs]
         *  The drawing attributes of the grouped drawings. Especially the 'top'
         *  and 'left' values cannot be calculated for drawing that will be grouped
         *  into a rotated drawing group. Therefore this parameter is only necessary
         *  for rotated drawing groups. And such an operation can only be created
         *  as an undo of an ungrouping of a rotated drawing group.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the slide has been inserted successfully.
         */
        #implGroupDrawings(start, target, drawings, attrs, childAttrs, external) {

            // grouping the drawings at the logical positions
            // -> inserting the new drawing of type 'group'
            // -> shifting all drawings into the drawing of type 'group'

            // In OX Presentation all drawings are on the same slide. In OX Text the
            // drawings can be positioned anywhere.

            // text span that will precede the drawing group
            let span = null;
            // deep copy of attributes, because they are modified in webkit browsers
            let attributes = _.copy(attrs, true);
            // the target node for the logical positions
            let targetNode = null;
            // new drawing node
            let drawingNode = null;
            // the content node inside the new drawing node
            let contentNode = null;
            // the function used to insert the new drawing frame
            const insertFunction = 'insertAfter';
            // an array with all jQuerified drawing nodes.
            let allChildDrawings = null;
            // the logical slide position
            const slidePos = _.initial(start);

            try {
                span = this.prepareTextSpanForInsertion(start, {}, target);
            } catch {
            }

            if (!span) { return false; }

            // For the user with edit rights, the drawings are already selected. They
            // can directly be taken from the selection. All other clients need to find
            // the drawings with their logical positions.
            // -> but not if undo/redo is running
            if (this.#docApp.isEditable() && !this.isUndoRedoRunning() && !external) {
                allChildDrawings = this.getSelection().getArrayOfSelectedDrawingNodes();
            } else {
                // collecting all drawings, before the new group drawing is inserted
                targetNode = this.getRootNode(target);
                allChildDrawings = [];
                _.each(drawings, drawingPosition => {
                    // -> collect all drawings in 'allChildDrawings'
                    const position = _.clone(slidePos);
                    position.push(drawingPosition);
                    const oneDrawing = getDOMPosition(targetNode, position, true);
                    if (oneDrawing && oneDrawing.node && DrawingFrame.isDrawingFrame(oneDrawing.node)) {
                        allChildDrawings.push($(oneDrawing.node));
                    } else {
                        return false; // invalid logical position
                    }
                });
            }

            // insert the drawing with default settings between the two text nodes (store original URL for later use)
            drawingNode = DrawingFrame.createDrawingFrame('group')[insertFunction](span);
            contentNode = DrawingFrame.getContentNode(drawingNode);

            // Shifting all children into the group container
            // iterating over all child drawing nodes
            _.each(allChildDrawings, drawing => {
                const prevSpan = drawing.prev();
                drawing.addClass(DrawingFrame.GROUPED_NODE_CLASS);
                contentNode.append(drawing);
                mergeSiblingTextSpans(prevSpan, true); // merge with next span
            });

            // apply the passed drawing attributes
            if (_.isObject(attributes)) {
                const drawingAttrs = attributes.drawing;
                // store child attributes in group root node
                if (drawingAttrs) {
                    drawingNode.data('childGroupAttrs', {
                        childWidth: drawingAttrs.width,
                        childHeight: drawingAttrs.height,
                        childLeft: drawingAttrs.left,
                        childTop: drawingAttrs.top
                    });
                }

                attributes = this.#getChildAttributes(attributes); // 55950

                // assigning the attributes to the group (no rotation!)
                this.drawingStyles.setElementAttributes(drawingNode, attributes);

                if (childAttrs) {
                    _.each(allChildDrawings, (drawing, counter) => {
                        this.drawingStyles.setElementAttributes(drawing, childAttrs[counter]);
                    });

                    // updating the new group once more (45853)
                    this.drawingStyles.updateElementFormatting(drawingNode);
                }
            }

            return true;
        }

        /**
         * If there is a 'group' operation generated by the client, this operation does not contain the drawing
         * attributes 'childLeft', 'childTop', 'childWidth' and 'childHeight'. The filter generates this values,
         * so that the client receives them, when a document is (re-)loaded.
         * In task 55950 a group generated on client side is duplicated via copy and paste. In this case, there
         * is no operation of type 'group'. Instead there is an 'insertDrawing' with drawing type 'group'. This
         * operation must have values for 'childLeft', 'childTop', 'childWidth' and 'childHeight'. Otherwise the
         * drawings will not be displayed correctly in PowerPoint (this is a pptx task). Therefore these properties
         * are added within this function simply by using the values from 'left', 'top', 'width' and 'height'.
         *
         * Important:
         * This addition of attributes must happen after the client received the operation. If the client sends
         * the attributes 'childLeft', 'childTop', 'childWidth' and 'childHeight' to the filter, the groups are
         * also not displayed correctly in PowerPoint. This might be caused by the fact, that in pptx file format
         * are additional offset values used for the drawings.
         *
         * If this additional attributes are assigned to the group drawing 'implicitely', they can be used by
         * copy/paste code, so that the generated insertDrawing operations for 'group' drawings can handle this
         * attributes correctly.
         *
         * @param {Object} attributes
         *  The attribute set for the drawing node of type 'group'.
         *
         * @returns {Object}
         *  The copied and modified set of attributes for the group drawing.
         */
        #getChildAttributes(attributes) {

            // the attribute set received by the 'group' operation
            const attrs = _.copy(attributes, true);
            // the drawing family of the attributes
            let drawingAttrs = null;

            if (!attrs.drawing) { attrs.drawing = {}; }

            drawingAttrs = attrs.drawing;

            // adding childLeft, childTop, childWidth and childHeight simply by using the values
            // of left, top, width and height (?!)
            if (drawingAttrs) {
                if (!drawingAttrs.childLeft) { drawingAttrs.childLeft = drawingAttrs.left || 0; }
                if (!drawingAttrs.childTop) { drawingAttrs.childTop = drawingAttrs.top || 0; }
                if (!drawingAttrs.childWidth) { drawingAttrs.childWidth = drawingAttrs.width || 0; }
                if (!drawingAttrs.childHeight) { drawingAttrs.childHeight = drawingAttrs.height || 0; }
            }

            return attrs;
        }

        /**
         * Handler for ungrouping the specified drawing.
         *
         * @param {Number[]} start
         *  The logical position of the drawing.
         *
         * @param {String} target
         *  The id of the layout slide, that is the base for the specified slide.
         *
         * @param {Number[][]} drawings
         *  The (new) logical positions for all drawings that will be ungrouped. The
         *  number of logical positions must be the same as the number of child drawings
         *  in the group.
         *
         * @param {Object[]} groupAttrsContainer
         *  A collector for the drawing attributes of the drawing of type 'group'.
         *
         * @param {Object[]} childAttrsContainer
         *  A collector for the drawing attributes of the grouped drawings. This
         *  is necessary to create the undo operation for a rotated drawing group.
         *
         * @returns {Boolean}
         *  Whether the slide has been inserted successfully.
         */
        #implUngroupDrawings(start, target, drawings, groupAttrsContainer, childAttrsContainer) {

            // the target node for the logical positions
            const targetNode = this.getRootNode(target);
            // the drawing group node
            let drawingGroupNode = getDOMPosition(targetNode, start, true);
            // the content node inside the new drawing group node
            let contentNode = null;
            // all drawing nodes.
            let allChildDrawings = null;
            // the span before the drawing group node
            let prevSpan = null;
            // a counter
            let counter = 0;
            // a collector for the attributes of the child drawings
            let allNewDrawingAttributes = null;
            // the logical slide position
            const slidePos = _.initial(start);
            // the last value of the logical position of the grouped drawing
            const drawingPos = _.last(start);

            // the specified drawing must be a group drawing node
            if (!(drawingGroupNode && drawingGroupNode.node && DrawingFrame.isGroupDrawingFrame(drawingGroupNode.node))) { return false; }

            drawingGroupNode = $(drawingGroupNode.node);

            contentNode = DrawingFrame.getContentNode(drawingGroupNode);

            allChildDrawings = contentNode.children(DrawingFrame.NODE_SELECTOR);

            // compare the number of drawings with the number of specified logical positions
            if (drawings && drawings.length !== allChildDrawings.length) { return false; }  // invalid operation

            if (!drawings) {
                drawings = [];
                _.each(allChildDrawings, () => {
                    drawings.push(drawingPos + counter);
                    counter++;
                });
            }

            allNewDrawingAttributes = this.#getChildDrawingAttrsAfterUngroup(drawingGroupNode, allChildDrawings, groupAttrsContainer, childAttrsContainer);

            // detaching the group from the DOM
            prevSpan = drawingGroupNode.prev();
            drawingGroupNode.detach();
            mergeSiblingTextSpans(prevSpan, true); // merge with next span

            // all children need to get new explicit attributes for 'top', 'left', 'width', 'height'
            // and 'rotation' corresponding to the position and size of the drawing of type 'group'.
            this.#assignDrawingAttributesToDrawings(allChildDrawings, allNewDrawingAttributes);

            // iterating over all child drawings
            _.each(allChildDrawings, (drawingNode, index) => {

                // the logical position to insert the drawing
                const pos = _.clone(slidePos);
                // the text span, after that the drawing can be inserted
                let span = null;

                pos.push(drawings[index]);
                span = this.prepareTextSpanForInsertion(pos, {}, target);

                if (span) {
                    $(drawingNode).removeClass(DrawingFrame.GROUPED_NODE_CLASS).insertAfter(span);
                    this.drawingStyles.updateElementFormatting(drawingNode);
                    if (DrawingFrame.isAutoResizeHeightDrawingFrame(drawingNode)) { $(drawingNode).data('drawingHeight', $(drawingNode).height()); } // #49890
                } else {
                    return false;
                }
            });

            // finally the (empty) drawing group node (and its selection) can be removed
            if (drawingGroupNode.data('selection')) { DrawingFrame.removeDrawingSelection(drawingGroupNode); }
            drawingGroupNode.remove();

            return true;
        }

        /**
         * Getting the position attributes for a drawing group, that is specified by all its
         * children. This is used to determine the position of a new created drawing group.
         *
         * @param {jQuery|jQuery[]|Node[]} drawings
         *  A drawing container over that can be iterated.
         *
         * @returns {Object}
         *  An object containing the properties 'left', 'top', 'width' and 'height'.
         */
        #getGroupDrawingPositionAttributes(drawings) {

            const attrs = { left: -1, top: -1, width: -1, height: -1 };
            let right = -1, bottom = -1;

            _.each(drawings, drawing => {
                // explicit attributes (is this sufficient)
                // -> Place holder drawings cannot be grouped -> no 'inherited' positions!
                let explicitAttrs = getExplicitAttributeSet(drawing);

                if (explicitAttrs.drawing) {

                    explicitAttrs = explicitAttrs.drawing;
                    if ('rotation' in explicitAttrs) { // #50006
                        // for odf is always unrotated shape position, for ooxml depends on angle, if it's 0 or 90 deg
                        let angle = 0;
                        if (!this.#docApp.isODF() && isAngleInButterflyArea(explicitAttrs.rotation, explicitAttrs.flipH, explicitAttrs.flipV)) {
                            angle = 90;
                        }
                        explicitAttrs = _.extend(explicitAttrs, getRotatedDrawingPoints(explicitAttrs, angle));
                    }

                    if (!_.isNumber(explicitAttrs.left)) { explicitAttrs.left = 0; } // setting the default value (47463)
                    if (!_.isNumber(explicitAttrs.top)) { explicitAttrs.top = 0; } // setting the default value

                    if ((_.isNumber(explicitAttrs.left)) && ((attrs.left === -1) || (explicitAttrs.left < attrs.left))) { attrs.left = explicitAttrs.left; }
                    if ((_.isNumber(explicitAttrs.top)) && ((attrs.top === -1) || (explicitAttrs.top < attrs.top))) { attrs.top = explicitAttrs.top; }

                    if (_.isNumber(explicitAttrs.left) && _.isNumber(explicitAttrs.width)) {
                        if (right === -1 || right < (explicitAttrs.left + explicitAttrs.width)) {
                            right = explicitAttrs.left + explicitAttrs.width;
                        }
                    }

                    if (_.isNumber(explicitAttrs.top) && _.isNumber(explicitAttrs.height)) {
                        if (bottom === -1 || bottom < (explicitAttrs.top + explicitAttrs.height)) {
                            bottom = explicitAttrs.top + explicitAttrs.height;
                        }
                    }
                }
            });

            // switching to default value 0
            if (attrs.left === -1) { attrs.left = 0; }
            if (attrs.top === -1) { attrs.top = 0; }
            attrs.width = right - attrs.left + 1;
            attrs.height = bottom - attrs.top + 1;

            return attrs;
        }

        /**
         * Saving the specified drawing attributes in a collector.
         *
         * @param {Object[]} container
         *  A container for the drawing attributes of the grouped drawings and the drawing
         *  group itself. This 'collecting' is necessary to create the undo operation for
         *  a (rotated) drawing group.
         *
         * @param {Object} drawingAttrs
         *  The explicit drawing attributes at a drawing.
         */
        #saveRequiredDrawingAttributes(container, drawingAttrs) {

            // the object for saving the drawing attributes
            const savedDrawingAttrs = {};
            // the properties that need to be saved. For the group node it is important that the values
            // for 'childLeft', 'childTop', 'childWidth' and 'childHeight' are saved (58249)
            const props = ['left', 'top', 'width', 'height', 'rotation', 'flipH', 'flipV', 'childLeft', 'childTop', 'childWidth', 'childHeight'];

            _.each(props, prop => {
                if (_.isNumber(drawingAttrs[prop])) { savedDrawingAttrs[prop] = drawingAttrs[prop]; }
                // rotation must be set explicitely
                if (prop === 'rotation' && !_.isNumber(savedDrawingAttrs[prop])) { savedDrawingAttrs[prop] = 0; }
                // flip properties are Boolean, and must be set also explicitely
                if ((prop === 'flipH' || prop === 'flipV') && _.isBoolean(drawingAttrs[prop])) {
                    savedDrawingAttrs[prop] = drawingAttrs[prop];
                }
            });

            container.push({ drawing: savedDrawingAttrs });
        }

        /**
         * A function that gets all the drawing attributes of all child drawings
         * of a specified drawing group. These drawing attributes needs to be
         * assigned to the child drawings, after the drawing group is removed.
         *
         * @param {jQuery} drawingGroupNode
         *  The drawing group node.
         *
         * @param {jQuery} allChildDrawings
         *  The container with all drawing children of the drawing group node.
         *
         * @param {Object[]} groupAttrsContainer
         *  A container for the drawing attributes of the grouped drawings and the
         *  drawing group itself. This 'collecting' is necessary to create the undo
         *  operation for a (rotated) drawing group.
         *
         * @param {Object[]} childAttrsContainer
         *  A collector for the drawing attributes of the grouped drawings. This
         *  is necessary to create the undo operation for a rotated drawing group.
         *
         * @returns {Object[]}
         *  A container that contains the drawing attributes in the order of the
         *  specified child drawings.
         */
        #getChildDrawingAttrsAfterUngroup(drawingGroupNode, allChildDrawings, groupAttrsContainer, childAttrsContainer) {

            // a collector for all drawing attributes of the child drawings
            const allNewDrawingAttributes = [];
            // an optional rotation angle of the group
            const groupRotationAngle = DrawingFrame.getDrawingRotationAngle(this, drawingGroupNode) || 0;
            // the jQuery offset of the complete slide
            const slideOffset = drawingGroupNode.closest(SLIDE_SELECTOR).offset();
            // the current zoom factor
            const zoomFactor = this.#docApp.getView().getZoomFactor();
            // group horizontal flip property
            const groupFlipH = DrawingFrame.isFlippedHorz(drawingGroupNode);
            // group vertical flip property
            const groupFlipV = DrawingFrame.isFlippedVert(drawingGroupNode);
            // difference in horizontal and vertical flip of the drawing group
            const groupFlipXor = groupFlipH !== groupFlipV;

            // reset transformation of group parent draiwng, if needed
            if (groupRotationAngle || groupFlipH || groupFlipV) { $(drawingGroupNode).css({ transform: 'rotate(0deg)' }); }

            // reading value of upper left corner of the unrotated drawing
            const groupDrawingOffset = $(drawingGroupNode).offset();
            const newParentDrawingAttrs = {
                left: convertLengthToHmm((groupDrawingOffset.left - slideOffset.left) / zoomFactor, 'px'),
                top: convertLengthToHmm((groupDrawingOffset.top - slideOffset.top) / zoomFactor, 'px'),
                width: convertLengthToHmm($(drawingGroupNode).width(), 'px'),
                height: convertLengthToHmm($(drawingGroupNode).height(), 'px'),
                rotation: groupRotationAngle,
                flipH: groupFlipH,
                flipV: groupFlipV
            };

            if (groupRotationAngle || groupFlipH || groupFlipV) {
                // first run - save child attrs before ungroup if transformation is applied
                _.each(allChildDrawings, drawing => {

                    // the rotation angle of one child drawing
                    const drawingAngle = DrawingFrame.getDrawingRotationAngle(this, drawing) || 0;
                    // the jQuerified drawing
                    const $drawing = $(drawing);
                    // the jQuery offset of one drawing
                    let drawingOffset = null;
                    // the explicit drawing attributes of the child
                    let drawingAttrs = null;
                    // drawing horizontal flip property
                    const drawingFlipH = DrawingFrame.isFlippedHorz(drawing);
                    // drawing vertical flip property
                    const drawingFlipV = DrawingFrame.isFlippedVert(drawing);

                    if (drawingAngle || drawingFlipH || drawingFlipV) { $(drawing).css({ transform: '' }); } // rotating back the child drawing

                    // reading value of upper left corner of the unrotated drawing
                    drawingOffset = $drawing.offset();
                    drawingAttrs = {
                        left: convertLengthToHmm((drawingOffset.left - groupDrawingOffset.left) / zoomFactor, 'px') + newParentDrawingAttrs.left,
                        top: convertLengthToHmm((drawingOffset.top - groupDrawingOffset.top) / zoomFactor, 'px') + newParentDrawingAttrs.top,
                        width: convertLengthToHmm($drawing.width(), 'px'),
                        height: convertLengthToHmm($drawing.height(), 'px'),

                        rotation: drawingAngle,
                        flipH: drawingFlipH,
                        flipV: drawingFlipV
                    };
                    DrawingFrame.setCssTransform(drawing, drawingAngle, drawingFlipH, drawingFlipV);

                    this.#saveRequiredDrawingAttributes(childAttrsContainer, drawingAttrs);
                });

                // rotating back the parent drawing
                DrawingFrame.setCssTransform($(drawingGroupNode), groupRotationAngle, groupFlipH, groupFlipV);
            }
            // saving the original positions in the gropuAttrsContainer
            this.#saveRequiredDrawingAttributes(groupAttrsContainer, newParentDrawingAttrs);

            // second run - iterating over all child drawings
            _.each(allChildDrawings, drawing => {

                // the rotation angle of one child drawing
                const drawingAngle = DrawingFrame.getDrawingRotationAngle(this, drawing) || 0;
                // the negative drawing angle of the group
                let completeDrawingAngle = 0;
                // the jQuerified drawing
                const $drawing = $(drawing);
                // the jQuery offset of one drawing
                let drawingOffset = null;
                // drawing horizontal flip property
                const drawingFlipH = DrawingFrame.isFlippedHorz(drawing);
                // drawing vertical flip property
                const drawingFlipV = DrawingFrame.isFlippedVert(drawing);
                // combined values for flip horizontal of drawing itself and group flip in one variable
                const mergedFlipH = drawingFlipH !== groupFlipH;
                // combined values for flip vertical of drawing itself and group flip in one variable
                const mergedFlipV = drawingFlipV !== groupFlipV;
                // final calculated angle of the drawing
                let finalDrawingAngle = 0;

                if (drawingAngle) { $(drawing).css({ transform: 'rotate(0deg)' }); } // rotating back the child drawing
                if (groupRotationAngle || groupFlipH || groupFlipV) {
                    completeDrawingAngle = groupFlipXor ? -(360 - groupRotationAngle) : -groupRotationAngle;
                    // assigning the negative group rotation to the child drawing so that there is no remaining rotation
                    if (completeDrawingAngle !== 0) { DrawingFrame.setCssTransform(drawing, completeDrawingAngle, mergedFlipH, mergedFlipV); }
                }

                // reading value of upper left corner of the unrotated drawing
                drawingOffset = $drawing.offset();
                // transfering angle to the drawing after ungroup as a combination of group angle (dependant on group flip) and drawing angle
                finalDrawingAngle = groupFlipXor ? groupRotationAngle - drawingAngle : groupRotationAngle + drawingAngle;
                if (finalDrawingAngle < 0) { finalDrawingAngle += 360; }
                finalDrawingAngle = math.modulo(finalDrawingAngle, 360);

                allNewDrawingAttributes.push({
                    left: convertLengthToHmm((drawingOffset.left - slideOffset.left) / zoomFactor, 'px'),
                    top: convertLengthToHmm((drawingOffset.top - slideOffset.top) / zoomFactor, 'px'),
                    width: convertLengthToHmm($drawing.width(), 'px'),
                    height: convertLengthToHmm($drawing.height(), 'px'),
                    rotation: finalDrawingAngle,
                    flipH: mergedFlipH,
                    flipV: mergedFlipV
                });
            });

            return allNewDrawingAttributes;
        }

        /**
         * Assigning new drawing attributes to the children of a group, when the group is 'ungrouped'.
         *
         * @param {jQuery} allChildDrawings
         *  The container with all drawing children of the drawing group node.
         *
         * @param {Object[]} drawingAttrs
         *  An object with the drawing attributes for all child drawings. The order must be the same
         *  as in the 'allChildDrawings' container.
         */
        #assignDrawingAttributesToDrawings(allChildDrawings, drawingAttrs) {
            _.each(allChildDrawings, (drawing, counter) => {
                this.drawingStyles.setElementAttributes(drawing, { drawing: drawingAttrs[counter] });
            });
        }
    }

    return GroupOperationMixin;
}

// exports ====================================================================

export const XGroupOperation = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XGroupOperation>}
     *  The extended class with group operation functionality.
     */
    mixin: mixinGroupOperation
};
