/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getBooleanOption, getDomNode, getNumberOption } from '@/io.ox/office/tk/utils';
import { CONTENT_NODE_SELECTOR, EMPTYTEXTFRAME_CLASS, EMPTYTEXTFRAMEBORDER_CLASS, EMPTY_THICKBORDER_CLASS, isTextFrameNode } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { PARAGRAPH_NODE_LIST_EMPTY_CLASS, PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS, PARAGRAPH_NODE_SELECTOR, isEmptyListParagraph,
    isImplicitParagraphNode, isParagraphNode } from '@/io.ox/office/textframework/utils/dom';
import { formatNumber, DEFAULT_LIST_STYLE_ID } from '@/io.ox/office/textframework/utils/listutils';
import { PARA_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { getOxoPosition, getParagraphElement, getParagraphNodeLength, increaseLastIndex } from '@/io.ox/office/textframework/utils/position';

// constants ==============================================================

// all predefined list styles for the GUI controls, mapped by list identifier
var PREDEFINED_LISTSTYLES = (function () {

    /**
     * Creates and returns a style definition for a bullet list with
     * the same bullet character in all list levels.
     *
     * @param {String} listKey
     *  A unique key for the bullet list style.
     *
     * @param {String} listCharacter
     *  The bullet character for all list levels.
     *
     * @param {string} fontName
     *
     * @param {Number} sortIndex
     *  The sorting index used in drop-down lists in GUI controls.
     *
     * @param {String} tooltip
     *  A tool tip for the list style used in GUI from controls.
     *
     * @returns {Object}
     *  An object with an attribute 'definition' and the string
     *  attributes 'listKey', 'listLabel' and 'tooltip'.
     */
    function createBulletListStyle(listKey, listCharacter, fontName, sortIndex, tooltip) {
        return {
            definition: {
                bullet: { type: 'character', character: listCharacter },
                bulletFont: { followText: false, name: fontName }
            },
            listKey,
            listLabel: listCharacter,
            sortIndex,
            tooltip
        };
    }

    /**
     * Creates and returns a style definition for a numbering list.
     *
     * @param {String} numberFormat
     *  The number format. Allowed are all values supported in 'formatNumber'.
     *
     * @param {String} textBefore
     *  The text string that is left of the number.
     *
     * @param {String} textAfter
     *  The text string that is right of the number.
     *
     * @param {String} numType
     *  The numbering type. This value is used as list key and it is used for the
     *  property 'numType' in generated operations.
     *
     * @param {Number} sortIndex
     *  The sorting index used in drop-down lists in GUI controls.
     *
     * @param {String} tooltip
     *  A tool tip for the list style used in GUI from controls.
     *
     * @returns {Object}
     *  An object with an attribute 'definition' and the string
     *  attributes 'listKey', 'format', 'textBefore', 'textAfter',
     *  'listLabel' and 'tooltip'.
     */
    function createNumberedListStyle(numberFormat, textBefore, textAfter, numType, sortIndex, tooltip) {
        return {
            definition: {
                bullet: { type: 'numbering', numType },
                bulletFont: { followText: true }
            },
            listKey: numType,
            format: numberFormat,
            textBefore,
            textAfter,
            listLabel: numberFormat ? (textBefore + formatNumber(1, numberFormat) + textAfter) : '',
            sortIndex,
            tooltip
        };
    }

    // generate and return the array with all predefined list styles
    return {
        L20000: createBulletListStyle('filled-circle',       '\u25CF', 'Times New Roman', 11, /*#. symbol in bullet lists (filled) */ gt('Filled circle')),
        L20001: createBulletListStyle('small-filled-circle', '\u2022', 'Times New Roman', 12, /*#. symbol in bullet lists (filled) */ gt('Small filled circle')),
        L20002: createBulletListStyle('circle',              '\u25CB', 'Times New Roman', 13, /*#. symbol in bullet lists (not filled) */ gt('Circle')),
        L20003: createBulletListStyle('small-circle',        '\u25E6', 'Times New Roman', 14, /*#. symbol in bullet lists (not filled) */ gt('Small circle')),
        L20004: createBulletListStyle('filled-square',       '\u25A0', 'Times New Roman', 21, /*#. symbol in bullet lists (filled) */ gt('Filled square')),
        L20005: createBulletListStyle('small-filled-square', '\u25AA', 'Times New Roman', 22, /*#. symbol in bullet lists (filled) */ gt('Small filled square')),
        L20006: createBulletListStyle('square',              '\u25A1', 'Times New Roman', 23, /*#. symbol in bullet lists (not filled) */ gt('Square')),
        L20007: createBulletListStyle('small-square',        '\u25AB', 'Times New Roman', 24, /*#. symbol in bullet lists (not filled) */ gt('Small square')),
        L20008: createBulletListStyle('filled-diamond',      '\u2666', 'Times New Roman', 31, /*#. symbol in bullet lists (filled) */ gt('Filled diamond')),
        L20009: createBulletListStyle('diamond',             '\u25CA', 'Times New Roman', 32, /*#. symbol in bullet lists (not filled) */ gt('Diamond')),
        L20010: createBulletListStyle('filled-triangle',     '\u25BA', 'Times New Roman', 41, /*#. symbol in bullet lists (filled) */ gt('Filled triangle')),
        L20011: createBulletListStyle('chevron',             '>',      'Times New Roman', 42, /*#. symbol in bullet lists */ gt('Chevron')),
        L20012: createBulletListStyle('arrow',               '\u2192', 'Times New Roman', 51, /*#. symbol in bullet lists */ gt('Arrow')),
        L20013: createBulletListStyle('dash',                '\u2013', 'Times New Roman', 52, /*#. symbol in bullet lists */ gt('Dash')),
        L20014: createBulletListStyle('black-diamond',       '\u2756', 'Arial',           53, /*#. symbol in bullet lists */ gt('Black diamond')),
        L20015: createBulletListStyle('arrow-head',          '\u27a2', 'Arial',           54, /*#. symbol in bullet lists */ gt('Arrow head')),
        L20016: createBulletListStyle('check-mark',          '\u2713', 'Arial',           55, /*#. symbol in bullet lists */ gt('Check mark')),

        alphaLcPeriod:    createNumberedListStyle('lowerLetter', '',  '.', 'alphaLcPeriod',    21, /*#. label style in numbered lists */ gt('Small Latin letters')),
        alphaLcParenR:    createNumberedListStyle('lowerLetter', '',  ')', 'alphaLcParenR',    22, /*#. label style in numbered lists */ gt('Small Latin letters')),
        alphaLcParenBoth: createNumberedListStyle('lowerLetter', '(', ')', 'alphaLcParenBoth', 23, /*#. label style in numbered lists */ gt('Small Latin letters')),
        alphaUcPeriod:    createNumberedListStyle('upperLetter', '',  '.', 'alphaUcPeriod',    31, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        alphaUcParenR:    createNumberedListStyle('upperLetter', '',  ')', 'alphaUcParenR',    32, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        alphaUcParenBoth: createNumberedListStyle('upperLetter', '(', ')', 'alphaUcParenBoth', 33, /*#. label style in numbered lists */ gt('Capital Latin letters')),
        romanLcPeriod:    createNumberedListStyle('lowerRoman',  '',  '.', 'romanLcPeriod',    41, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        romanLcParenR:    createNumberedListStyle('lowerRoman',  '',  ')', 'romanLcParenR',    42, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        romanLcParenBoth: createNumberedListStyle('lowerRoman',  '(', ')', 'romanLcParenBoth', 43, /*#. label style in numbered lists */ gt('Small Roman numbers')),
        romanUcPeriod:    createNumberedListStyle('upperRoman',  '',  '.', 'romanUcPeriod',    51, /*#. label style in numbered lists */ gt('Capital Roman numbers')),
        romanUcParenR:    createNumberedListStyle('upperRoman',  '',  ')', 'romanUcParenR',    52, /*#. label style in numbered lists */ gt('Capital Roman numbers')),
        romanUcParenBoth: createNumberedListStyle('upperRoman',  '(', ')', 'romanUcParenBoth', 53, /*#. label style in numbered lists */ gt('Capital Roman numbers')),
        arabicPeriod:     createNumberedListStyle('decimal',     '',  '.', 'arabicPeriod',     11, /*#. label style in numbered lists */ gt('Decimal numbers')),
        arabicParenR:     createNumberedListStyle('decimal',     '',  ')', 'arabicParenR',     12, /*#. label style in numbered lists */ gt('Decimal numbers')),
        arabicParenBoth:  createNumberedListStyle('decimal',     '(', ')', 'arabicParenBoth',  13, /*#. label style in numbered lists */ gt('Decimal numbers'))
    };

}()); // end of PREDEFINED_LISTSTYLES local scope

// mix-in class ListHandlerMixin ======================================

/**
 * A mix-in class for the document model class providing functionality
 * for the list handling used in a presentation document.
 */
export default function ListHandlerMixin() {

    var // self reference for local functions
        self = this,
        // the collector for the default list styles
        // -> this will be used in non place holder drawings like shapes or text frames
        defaultTextListStyles = null,
        // the paragraph node that contains the selection
        selectedParagraph = $();

    // private methods ----------------------------------------------------

    /**
     * Creates a non-default list either with decimal numbers or bullets that
     * are available at the GUI.
     *
     * @param {String} listStyleId
     *  The id of the list style.
     *
     * @param {String} type
     *  The list type: 'numbering' or 'bullet'.
     *
     * @param {Array[]} [allParagraphs]
     *  An optional collector for all paragraphs, that will get the list attributes.
     *  Supported is currently an array of logical positions for each paragraph or an
     *  array of the paragraph nodes.
     *  If not specified, the current selection is used.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.expandNumberList=false]
     *      Whether the current selection shall be expanded so that all neighboring
     *      numbering paragraphs on the same list level get the same new numbering
     *      type, even if they are not part of the selection.
     *  @param {Boolean} [options.listStartValue=null]
     *      An optional start value for a numbered list, that is recognized by the
     *      list auto detection.
     */
    function createSelectedListStyle(listStyleId, type, allParagraphs, options) {

        // if the new list style id shall be set in a fully specified drawing (like place holder 'body') it is
        // possible to avoid the properties 'indentLeft' and 'identFirstLine'. But if these values are not
        // already specified in the corresponding list level, it is necessary to add them to the operation.
        // sufficient to set the

        var // the operations generator
            generator = self.createOperationGenerator(),
            // the list type
            // listType = type === 'bullet' ? 'character' : 'numbering';
            // the selected text frame node (also finding text frames inside groups)
            drawing = null,
            // an object with all list level attributes
            allListLevelAttributes = null,
            // whether a new 'bullet' attribute has to be assigned to the paragraph
            addBulletAttribute = true,
            // the paragraph attributes for the operation
            paraAttrs = null,
            // whether the default list style need to be created
            createDefault = (listStyleId === DEFAULT_LIST_STYLE_ID),
            // whether all paragraphs on the current level and with current number type shall be modified
            expandNumberList = getBooleanOption(options, 'expandNumberList', false),
            // an optional start value, recognized by auto detection
            startValue = getNumberOption(options, 'listStartValue', null),
            // whether the small list indents shall be used
            useSmallIndents = (self.getApp().isODF() || self.getApp().isSpreadsheetApp());

        // the handler function for each paragraph to add the list attributes
        // 1. Parameter: The paragraph node
        // 2. Parameter: The logical paragraph position (optional)
        function addListToParagraph(paragraph, position) {

            var // all explicit attributes
                explicitAttrs = getExplicitAttributeSet(paragraph),
                // the explicit paragraph attributes
                explicitParaAttrs = (explicitAttrs && explicitAttrs.paragraph) ? explicitAttrs.paragraph : null,
                // the paragraph attributes for the operation
                localParaAttrs = _.copy(paraAttrs, true),
                // the attributes of the paragraph
                elementAttributes = null,
                // the paragraph attributes of the paragraph
                paragaraphAttributes = null,
                // whether the paragraph is already a list paragraph
                isListParagraph = false,
                // whether the paragraph is an implicit paragraph
                isImplicitParagraph = false;

            if (addBulletAttribute) {
                // it is necessary to handle an existing left indent -> otherwise the paragraph might shift to the left
                isListParagraph = self.isListParagraph(explicitParaAttrs);

                if (isListParagraph) {
                    if (_.isNumber(localParaAttrs.indentLeft)) { delete localParaAttrs.indentLeft; }
                    if (_.isNumber(localParaAttrs.indentFirstLine)) { delete localParaAttrs.indentFirstLine; }
                } else {
                    if (explicitParaAttrs && _.isNumber(explicitParaAttrs.indentLeft)) {
                        localParaAttrs.indentLeft = _.isNumber(localParaAttrs.indentLeft) ? localParaAttrs.indentLeft : 0;
                        localParaAttrs.indentLeft += explicitParaAttrs.indentLeft;
                    } else if (explicitParaAttrs && _.isNumber(explicitParaAttrs.level)) {
                        // scenario: setting a new list item to a paragraph inside text frame
                        elementAttributes = self.paragraphStyles.getElementAttributes(paragraph);
                        paragaraphAttributes = elementAttributes.paragraph;

                        if (paragaraphAttributes && _.isNumber(paragaraphAttributes.indentLeft)) {
                            localParaAttrs.indentLeft += paragaraphAttributes.indentLeft;
                        }
                    }
                }
            } else {
                if (type === 'bullet' && explicitParaAttrs && _.isNumber(explicitParaAttrs.level)) {
                    // scenario: setting a new list item to a paragraph that has set bullet type to 'none'
                    elementAttributes = self.paragraphStyles.getElementAttributes(paragraph);
                    paragaraphAttributes = elementAttributes.paragraph;

                    if (paragaraphAttributes && paragaraphAttributes.bullet && paragaraphAttributes.bullet.type && paragaraphAttributes.bullet.type === 'none') {
                        if (paragaraphAttributes.bullet.character) {
                            localParaAttrs.bullet = { type: 'character', character: paragaraphAttributes.bullet.character };
                        }
                    }
                }
            }

            // saving an existing start value for a numbered list
            if (type === 'numbering' && explicitParaAttrs && explicitParaAttrs.bullet && _.isNumber(explicitParaAttrs.bullet.startAt)) {
                localParaAttrs.bullet.startAt = explicitParaAttrs.bullet.startAt;
            } else if (_.isNumber(startValue)) {
                localParaAttrs.bullet.startAt = startValue;
            }

            // in ODP format the client must send the bullet size for numbered lists (52147)
            if (self.getApp().isODF() && type === 'numbering') { localParaAttrs.bulletSize = { type: 'percent', size: 100 }; }

            // receiving the logical position of the paragraph, if not specified as parameter
            if (!position) { position = getOxoPosition(self.getCurrentRootNode(self.getActiveTarget()), paragraph, 0); }

            isImplicitParagraph = isImplicitParagraphNode(paragraph);

            if (self.getApp().isODF() && isImplicitParagraph) {
                // saving the attributes as explicit attributes at the implicit paragraphs
                self.paragraphStyles.setElementAttributes(paragraph, { paragraph: localParaAttrs });
                self.updateListsDebounced(paragraph);

            } else {
                // handling of implicit paragraphs (in tables)
                if (isImplicitParagraph && getParagraphNodeLength(paragraph) === 0) {
                    generator.generateOperation(PARA_INSERT, { start: _.copy(position) });
                }

                // generate the 'setAttributes' operation for each paragraph
                generator.generateOperation(SET_ATTRIBUTES, { start: _.copy(position), attrs: { paragraph: localParaAttrs } });
            }
        }

        // redefining the default bullet style
        if (createDefault && type === 'numbering') {
            createDefault = false;  // for numbering lists never rely on defaults, that are defined at place holder
            listStyleId = ListHandlerMixin.DEFAULT_NUMBERING_TYPE;
        }

        // generating the operations
        if (createDefault || listStyleId in PREDEFINED_LISTSTYLES) {

            if (self.paragraphStyles.getAllAvailableListStyleAttributes) {
                drawing = self.getSelection().getAnyTextFrameDrawing({ forceTextFrame: true });
                allListLevelAttributes = self.paragraphStyles.getAllAvailableListStyleAttributes(drawing);
            }

            if (allListLevelAttributes) {
                if ((type === 'bullet' && isBulletListDefinition(allListLevelAttributes)) || (type === 'numbering' && isNumberingListDefinition(allListLevelAttributes))) {
                    // for example in the body place holder
                    addBulletAttribute = false; // -> adding information for 'indentLeft' and 'indentFirstLine' not required -> defined in place holder
                    // -> but removing 'indentLeft' and 'indentFirstLine' is required!
                } else {

                    // for example in the title place holder or inside a shape or inside a text frame
                    addBulletAttribute = true; // -> setting 'indentLeft' and 'indentFirstLine' is required
                }
            }

            if (createDefault) { // the default list style (can only be true for bullet lists, not numbering lists)
                if (addBulletAttribute) {
                    paraAttrs = _.copy(PREDEFINED_LISTSTYLES[ListHandlerMixin.DEFAULT_BULLET_LISTSTYLE].definition, true);
                } else {
                    paraAttrs = { bullet: null };
                    // -> is always a bullet list defined at place holder drawing?
                }

            } else { // the specified list style
                paraAttrs = _.copy(PREDEFINED_LISTSTYLES[listStyleId].definition, true);
            }

            if (addBulletAttribute) {
                paraAttrs.indentLeft = useSmallIndents ? ListHandlerMixin.PREDEFINED_LEFT_INDENT_DEFAULT_SMALL : ListHandlerMixin.PREDEFINED_LEFT_INDENT_DEFAULT_LARGE; // TODO: Should be specific to header or shape
                paraAttrs.indentFirstLine = useSmallIndents ? ListHandlerMixin.PREDEFINED_INDENT_FIRST_LINE_DEFAULT_SMALL : ListHandlerMixin.PREDEFINED_INDENT_FIRST_LINE_DEFAULT_LARGE; // TODO: Should be specific to header or shape

                // TODO: In PowerPoint the indentLeft and indentFirstLine are dependent from the font size (in title)
            } else {
                paraAttrs.indentLeft = null;  // simply using the default value, that is defined at the place holder
                paraAttrs.indentFirstLine = null;
            }

            // using allParagraphs to set the same number type to all numbered lists on the same level
            if (expandNumberList) { allParagraphs = getAllNeighboringNumberParagraphs(); }

            if (allParagraphs) {
                _.each(allParagraphs, function (paraPos) {

                    var // target for operation
                        target = self.getActiveTarget(),
                        // the paragraph node at the start position
                        paragraphNode = _.isArray(paraPos) ? getParagraphElement(self.getCurrentRootNode(target), paraPos) : paraPos;

                    if (paragraphNode) { addListToParagraph(paragraphNode, _.isArray(paraPos) ? paraPos : null); }
                });
            } else {
                // iterating over all paragraphs in the selection
                self.getSelection().iterateContentNodes(addListToParagraph);
            }

            // apply all collected operations
            self.applyOperations(generator);
        }

    }

    /**
     * Check, whether the specified list attribute contain a list definition for the specified
     * list type ('character' or 'numbering').
     *
     * Ignoring the specific level for performance reasons. Simply check, if one level contains
     * the bullet information.
     *
     * @param {Object} allListLevelAttributes
     *  The object containing the definitions of all list levels.
     *
     * @param {String} listType
     *  The type of list to be checked. This can only be the two values 'character' or 'numbering'.
     *
     * @returns {Boolean}
     *  Whether the attribute set with all list levels describe a list of the specified type.
     */
    function isSpecificListDefinition(allListLevelAttributes, listType) {

        var // whether the specified list is a bullet list
            isListDef = false;

        _.each(_.keys(allListLevelAttributes), function (level) {

            var // the attributes specified for one level
                listLevelAttributes = allListLevelAttributes[level];

            // using the paragraph attributes, if available
            listLevelAttributes = listLevelAttributes && listLevelAttributes.paragraph ? listLevelAttributes.paragraph : null;

            if (listLevelAttributes && listLevelAttributes.bullet && listLevelAttributes.bullet.type && listLevelAttributes.bullet.type === listType) {
                isListDef = true;
            }

        });

        return isListDef;
    }

    /**
     * Check, whether the specified list attribute contains a list definition of a
     * bullet list.
     *
     * @param {Object} allListLevelAttributes
     *  The object containing the definitions of all list levels.
     *
     * @returns {Boolean}
     *  Whether the attribute set with all list levels describe a 'bullet' list.
     */
    function isBulletListDefinition(allListLevelAttributes) {
        return isSpecificListDefinition(allListLevelAttributes, 'character');
    }

    /**
     * Check, whether the specified list attribute contains a list definition of a
     * numbering list.
     *
     * @param {Object} allListLevelAttributes
     *  The object containing the definitions of all list levels.
     *
     * @returns {Boolean}
     *  Whether the attribute set with all list levels describe a 'numbering' list.
     */
    function isNumberingListDefinition(allListLevelAttributes) {
        return isSpecificListDefinition(allListLevelAttributes, 'numbering');
    }

    /**
     * Receiving the list level and the type ('numbering', 'character' or 'none') for a
     * specified paragraph. If the paragraph has no list level specified, the value -1
     * is returned.
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node
     *
     * @returns {Object}
     *  An object containing the properties 'bulletType' and 'level'. The 'bulletType'
     *  can get the values 'numbering', 'character' or 'none'. If no value is specified,
     *  it is set to null. If the list level cannot be determined, it is set to -1.
     */
    function getParagraphListInfo(paragraph) {

        var // the attributes at the paragraph
            attrs = self.paragraphStyles.getElementAttributes(paragraph),
            // the paragraph attributes
            paraAttrs = attrs && attrs.paragraph,
            // the bullet type
            bulletType = paraAttrs && paraAttrs.bullet && paraAttrs.bullet.type;

        return { bulletType: bulletType || null, level: (paraAttrs && _.isNumber(paraAttrs.level)) ? paraAttrs.level : -1 };
    }

    /**
     * Receiving the numbering type (property 'numType') of a numbered paragraph. If this
     * is not defined (for example in non-numbering paragraphs, null is returned.
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node
     *
     * @returns {String|Null}
     *  The numbering type of the specified paragraph, if this is a numbering paragraph.
     *  Otherwise null is returned.
     */
    function getNumberedParagraphNumberingType(paragraph) {

        var // the attributes at the paragraph
            attrs = self.paragraphStyles.getElementAttributes(paragraph),
            // the paragraph attributes
            paraAttrs = attrs && attrs.paragraph;

        return (paraAttrs && paraAttrs.bullet && paraAttrs.bullet.type && paraAttrs.bullet.numType) || null;
    }

    /**
     * Receiving an optionally defined start value for a numbering list at a specified paragraph. If this
     * is not defined (for example in non-numbering paragraphs, '-1' is returned.
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node
     *
     * @returns {Number}
     *  The start value for the specified paragraph, if this is a defined value at a numbering paragraph.
     *  Otherwise -1 is returned.
     */
    function getNumberedParagraphStartValue(paragraph) {

        var // the attributes at the paragraph
            attrs = getExplicitAttributeSet(paragraph),  // checking only explicit attributes
            // the paragraph attributes
            paraAttrs = attrs && attrs.paragraph;

        return paraAttrs && paraAttrs.bullet && _.isNumber(paraAttrs.bullet.startAt) ? paraAttrs.bullet.startAt : -1;
    }

    /**
     * Checking, whether a specified attribute set with the property 'paragraph' describes a
     * numbering paragraph.
     *
     * @param {Object} attrs
     *  The attribute set.
     *
     * @returns {Boolean}
     *  Whether the specified attribute set with the property 'paragraph' describes a
     *  numbering paragraph.
     */
    function isNumberingListStyleAttributeSet(attrs) {
        return attrs && attrs.paragraph && attrs.paragraph.bullet && attrs.paragraph.bullet.type === 'numbering';
    }

    /**
     * Trying to find the best suited numbering type for a specified paragraph after
     * a change to a new level. It is checked, if a valid numbering type can be found
     * at the neighboring paragraphs. First it is searched in top direction, then
     * in bottom direction.
     * A bullet type paragraph or a paragraph without list level stops the iteration.
     *
     * @param {Node|jQuery} paragraph
     *  The paragraph node.
     *
     * @param {Number} level
     *  The new list level of the paragraph.
     *
     * @returns {Object}
     *  An object containing the properties 'numType' and 'startValue' for the paragraph
     *  at the specified level. Both values can be null, if they cannot be determined.
     */
    function getBulletNumberingType(paragraph, level) {

        var  // the level of the current paragraph
            currentLevel = 0,
            // the type of the current paragraph
            currentType = null,
            // whether the iteration shall continue
            doContinue = true,
            // the current paragraph node
            currentParagraph = getDomNode(paragraph),
            // the bullet numbering type
            newNumType = null,
            // the start value at the new level
            newStartValue = -1,
            // a list info object with properties 'bulletType' and 'level'
            listInfo = null;

        // iterating backwards
        while (doContinue && !newNumType && currentParagraph && currentParagraph.previousSibling && isParagraphNode(currentParagraph.previousSibling)) {

            listInfo = getParagraphListInfo(currentParagraph.previousSibling);
            currentLevel = listInfo.level;
            currentType = listInfo.bulletType;

            if (currentLevel === level && currentType === 'numbering') {
                newNumType = getNumberedParagraphNumberingType(currentParagraph.previousSibling);
                newStartValue = getNumberedParagraphStartValue(currentParagraph.previousSibling);
                doContinue = false;
            } else if (currentLevel > level) {
                currentParagraph = currentParagraph.previousSibling; // paragraphs with higher level are ignored
            } else {
                doContinue = false; // paragraphs with lower level stop the iteration
            }
        }

        if (!newNumType) {
            currentParagraph = getDomNode(paragraph);
            doContinue = true;
        }

        // iteration forwards
        while (doContinue && !newNumType && currentParagraph && currentParagraph.nextSibling && isParagraphNode(currentParagraph.nextSibling)) {

            listInfo = getParagraphListInfo(currentParagraph.nextSibling);
            currentLevel = listInfo.level;
            currentType = listInfo.bulletType;

            if (currentLevel === level && currentType === 'numbering') {
                newNumType = getNumberedParagraphNumberingType(currentParagraph.nextSibling);
                newStartValue = getNumberedParagraphStartValue(currentParagraph.nextSibling);
                doContinue = false;
            } else if (currentLevel > level) {
                currentParagraph = currentParagraph.nextSibling; // paragraphs with higher level are ignored
            } else {
                doContinue = false; // paragraphs with lower level stop the iteration
            }
        }

        return { numType: newNumType || null, startValue: _.isNumber(newStartValue) ? newStartValue : null };
    }

    /**
     * Collecting all neighboring numbering paragraphs of the current selection. Only the
     * start position of the selection is used to define the starting paragraph. Collected
     * are all paragraph that use a common number counting. This means, the paragraphs
     * have the same level and there is no paragraph with lower level or with no level
     * in between.
     *
     * @returns {Node[]}
     *  A sorted array with all collected paragraph nodes.
     */
    function getAllNeighboringNumberParagraphs() {

        var // the collector for all paragraphs
            allParagraphs = null,
            // the selection object
            selection = self.getSelection(),
            // the current paragraph node
            currentParagraph = null,
            // the paragraph node at the start position (only handling start position)
            startParagraph = getParagraphElement(selection.getRootNode(), _.initial(selection.getStartPosition())),
            // the level of the start paragraph
            startLevel = 0,
            // the level of the current paragraph
            currentLevel = 0,
            // an object with the list information for a paragraph
            listInfo = null,
            // the bullet type of the current paragraph
            currentType = null,
            // whether the iteration shall continue
            doContinue = true;

        if (startParagraph) {

            listInfo = getParagraphListInfo(startParagraph);
            startLevel = listInfo.level;
            currentType = listInfo.bulletType;

            if (startLevel > -1 && currentType === 'numbering') {

                allParagraphs = [];
                currentParagraph = startParagraph;
                allParagraphs.push(currentParagraph);

                // iterating backwards
                while (doContinue && currentParagraph && currentParagraph.previousSibling && isParagraphNode(currentParagraph.previousSibling)) {

                    listInfo = getParagraphListInfo(currentParagraph.previousSibling);
                    currentLevel = listInfo.level;
                    currentType = listInfo.bulletType;

                    if (currentLevel === startLevel && currentType === 'numbering') {
                        currentParagraph = currentParagraph.previousSibling;
                        allParagraphs.unshift(currentParagraph);
                    } else if (currentLevel > startLevel) {
                        currentParagraph = currentParagraph.previousSibling; // paragraphs with higher level are ignored
                    } else {
                        doContinue = false; // paragraphs with lower level stop the iteration
                    }
                }

                currentParagraph = startParagraph;
                doContinue = true;

                // iteration forwards
                while (doContinue && currentParagraph && currentParagraph.nextSibling && isParagraphNode(currentParagraph.nextSibling)) {

                    listInfo = getParagraphListInfo(currentParagraph.nextSibling);
                    currentLevel = listInfo.level;
                    currentType = listInfo.bulletType;

                    if (currentLevel === startLevel && currentType === 'numbering') {
                        currentParagraph = currentParagraph.nextSibling;
                        allParagraphs.push(currentParagraph);
                    } else if (currentLevel > startLevel) {
                        currentParagraph = currentParagraph.nextSibling; // paragraphs with higher level are ignored (also with bullets or no list item)
                    } else {
                        doContinue = false; // paragraphs with lower level stop the iteration
                    }
                }
            }
        }

        return allParagraphs;
    }

    /**
     * Searching the paragraphs that directly follow a specified paragraph at the specified position
     * with specified level and numbering type. All the paragraphs with the same numbering type and
     * level.
     *
     * @param {Node|jQuery} para
     *  The paragraph node.
     *
     * @param {Number[]} pos
     *  The logical position of the specified paragraph.
     *
     * @param {Number} level
     *  The paragraph level.
     *
     * @param {String} numType
     *  The paragraphs numbering type.
     *
     * @returns {Number[][]}
     *  An array of the logical positions of all paragraphs, that directly follow the specified
     *  paragraph and have the same level and numbering type.
     */
    function getFollowingNumberingParagraphs(para, pos, level, numType) {

        var // the logical position of the following numbering paragraphs with same level
            allParagraphPositions = [],
            // the paragraph node
            currentParagraph = getDomNode(para),
            // the current logical position
            currentPos = _.copy(pos),
            // the numbering type of the current paragraph
            currentNumType = null,
            // an object with the list information for a paragraph
            listInfo = null,
            // the level of the current paragraph
            currentLevel = null,
            // the bullet type of the current paragraph
            currentType = null,
            // whether the iteration shall continue
            doContinue = true;

        while (doContinue && currentParagraph && currentParagraph.nextSibling && isParagraphNode(currentParagraph.nextSibling)) {

            listInfo = getParagraphListInfo(currentParagraph.nextSibling);
            currentLevel = listInfo.level;
            currentType = listInfo.bulletType;

            currentPos = increaseLastIndex(currentPos);

            if (currentLevel === level && currentType === 'numbering') {
                currentNumType = getNumberedParagraphNumberingType(currentParagraph.nextSibling);
                if (!currentNumType || currentNumType === numType) {
                    doContinue = false; // nothing to do, no numbering type or numbering type is already the same
                } else {
                    allParagraphPositions.push(currentPos);
                    currentParagraph = currentParagraph.nextSibling;
                }
            } else if (currentLevel > level) {
                currentParagraph = currentParagraph.nextSibling; // paragraphs with higher level are ignored
            } else {
                doContinue = false; // paragraphs with lower level stop the iteration
            }
        }

        return allParagraphPositions;
    }

    /**
     * Check, whether a specified ID is the ID of a numbering list style.
     *
     * @param {String} id
     *  The ID, that will be checked.
     *
     * @returns {Boolean}
     *  Whether the specified ID is the ID of a numbering list style.
     */
    function isNumberingListStyle(id) {

        var // whether the id specified a numbering list
            isNumberingStyle = false,
            // the list style object
            listStyle = null;

        if (id in PREDEFINED_LISTSTYLES) {
            listStyle = PREDEFINED_LISTSTYLES[id];
            isNumberingStyle = listStyle && listStyle.definition && listStyle.definition.bullet && listStyle.definition.bullet.type && listStyle.definition.bullet.type === 'numbering';
        }

        return isNumberingStyle;
    }

    /**
     * Finding the number list ID for a specified set of number format (decimal, lowerRoman, ...),
     * left value and right value.
     *
     * @param {String} left
     *  The left string for the numbered auto list (only '(' is supported yet).
     *
     * @param {String} right
     *  The right string for the numbered auto list.
     *
     * @param {String} numberFormat
     *  The number format string. Supported values are 'decimal', 'lowerRoman', ... .
     *
     * @returns {String|Null}
     *  The list ID for the specified number format, left and right string. If no ID could be
     *  found, null is returned.
     */
    function findAutoListId(left, right, numberFormat) {

        var // the id of the list style
            id = null;

        id = _.find(PREDEFINED_LISTSTYLES, function (value) {
            return value.format && value.format === numberFormat && value.textBefore === left && value.textAfter === right;
        });

        return id ? id.listKey : null;
    }

    /**
     * Check whether a specified change of list style is a change from one numbering list
     * style to another numbering list style. And additionally, if the selection is inside
     * one single paragraph. If this all is true, the numbered list style will be changed
     * for all neighboring paragraphs, that have the same level.
     *
     * @param {String} newStyleId
     *  The ID of the new paragraph list style.
     *
     * @param {String} oldStyleId
     *  The ID of the old paragraph list style.
     *
     * @returns {Boolean}
     *  Whether a specified change of list style is a change from one numbering list style
     *  to another numbering list style. And additionally the selection is inside one single
     *  paragraph.
     */
    function isNumberChangeInOneParagraph(newStyleId, oldStyleId) {
        // both parameter are number lists and the selection is inside one paragraph

        var // whether this is a change of the number type
            isNumberChange = false;

        if (newStyleId && oldStyleId && isNumberingListStyle(newStyleId) && isNumberingListStyle(oldStyleId)) {
            isNumberChange = _.isEqual(_.initial(self.getSelection().getStartPosition()), _.initial(self.getSelection().getEndPosition()));
        }

        return isNumberChange;
    }

    /**
     * Removes bullet/numbered list from selected paragraph(s). But keeps the list level.
     */
    function removeListAttributes() {

        var // the operations generator
            generator = self.createOperationGenerator();

        // iterating over all paragraphs in the selection
        self.getSelection().iterateContentNodes(function (paragraph, position) {

            var // all explicit attributes
                explicitAttrs = getExplicitAttributeSet(paragraph),
                // the explicit paragraph attributes
                explicitParaAttrs = explicitAttrs && explicitAttrs.paragraph ? explicitAttrs.paragraph : null,
                // the complete element attributes
                elementAttributes = null,
                // the complete paragraph attibutes
                paragaraphAttributes = null,
                // the paragraph attributes for the operation
                paraAttrs = { bullet: { type: 'none' }, bulletSize: null, bulletColor: null, bulletFont: null };

            // taking care of the complete bullet object (not just modifying one value)
            if (explicitParaAttrs && explicitParaAttrs.bullet) {
                paraAttrs.bullet = explicitParaAttrs.bullet;
                paraAttrs.bullet.type = 'none';
            }

            // in ODP files it can happen that this is an implicit paragraph -> no generation of operations
            if (isImplicitParagraphNode(paragraph)) { return; } // in LO the bullet in an implicit paragraph is not removed

            if (explicitParaAttrs && _.isNumber(explicitParaAttrs.indentFirstLine)) {

                // TODO: receiving the left indent corresponding to the level

                if (_.isNumber(explicitParaAttrs.indentLeft)) {
                    // reducing 'indentLeft' by adding negative 'indentFirstLine'
                    paraAttrs.indentLeft = explicitParaAttrs.indentLeft + explicitParaAttrs.indentFirstLine;

                    // TODO: It might be necessary to handle the level, to set precise value of indentLeft
                    // -> the above correction fails on increased levels
                }

                // removing 'indentFirstLine' completely
                paraAttrs.indentFirstLine = 0;

            } else {

                // removing the list item in a place holder drawing, where indentLeft and indentFirstLine are
                // inherited from layout drawings.

                paraAttrs.indentFirstLine = 0; // setting hard to 0, so that inherited indent first line is not used

                elementAttributes = self.paragraphStyles.getElementAttributes(paragraph);
                paragaraphAttributes = elementAttributes.paragraph;

                if (paragaraphAttributes && paragaraphAttributes.indentLeft && paragaraphAttributes.indentFirstLine) {
                    paraAttrs.indentLeft = paragaraphAttributes.indentLeft + paragaraphAttributes.indentFirstLine;
                }
            }

            // generate the 'setAttributes' operation for each paragraph
            generator.generateOperation(SET_ATTRIBUTES, { start: _.copy(position), attrs: { paragraph: paraAttrs } });
        });

        // apply all collected operations
        self.applyOperations(generator);
    }

    /**
     * Adding the default font size that is used by PP (Arial, 18 pt, black) to the
     * default text list styles (if not specified via operation). This is used for example,
     * if a new text frame is inserted.
     */
    function addDefaultValuesToTextListStyles() {
        _.each(_.keys(defaultTextListStyles), function (key) {
            if (!defaultTextListStyles[key].character) {
                defaultTextListStyles[key].character = { fontSize: ListHandlerMixin.DEFAULT_FONT_SIZE };
            } else if (!_.isNumber(defaultTextListStyles[key].character.fontSize)) {
                defaultTextListStyles[key].character.fontSize = ListHandlerMixin.DEFAULT_FONT_SIZE;
            }
        });
    }

    /**
     * Spreadsheet specific handler for the selection 'change' event.
     * In OX Presentation the handler is already attached in model.js.
     */
    function selectionChangeHandler() {
        self.handleListItemVisibility(); // handling the visibility of the list items in empty paragraphs
    }

    // public methods -----------------------------------------------------

    /**
     * Item getter function for a list style identifier.
     *
     * @param {String} listType
     *  The type of the list styles: either 'bullet' or 'numbering'.
     *
     * @param {Object} attributes
     *  The merged attribute set of the selected paragraphs.
     *
     * @returns {String|Null}
     *  If unambiguous, the list style identifier contained in the passed
     *  paragraph attributes, if the type of that list matches the list
     *  type specified in the item user data ('bullets' or 'numbering'),
     *  otherwise an empty string. If ambiguous, returns the value null.
     *
     *  Info: If a list type is specifid, but the list style identifier cannot
     *  be determined, null must be returned.
     */
    this.getListStyleId = function (listType, attributes) {

        // in ambiguous attributes, list type cannot be determined (bullet and numbering will be amboguous)
        if (attributes.bullet === null) { return null; }

        // should not happen, just for safety
        if (!_.isObject(attributes.bullet)) {
            globalLogger.error('ListHandlerMixin.getListStyleId(): invalid bullet value, object expected');
            return '';
        }

        switch (listType) {

            // bullet lists: find predefined list style by bullet character and font settings
            case 'bullet':
                // bullet font may be ambiguous
                if (!_.isObject(attributes.bulletFont)) { return null; }
                // check, if this is a bullet list
                if (attributes.bullet.type !== 'character') { return ''; } // no bullet list
                // search in predefined lists
                var char = attributes.bullet.character;
                var font = attributes.bulletFont.name;
                return _.findKey(PREDEFINED_LISTSTYLES, function (listStyle) {
                    var def = listStyle.definition;
                    return (def.bullet.type === 'character') && (def.bullet.character === char) && !def.bulletFont.followText && (def.bulletFont.name === font);
                }) || null;

            // numbered lists: list style identifier is contained in the 'bullet' attribute
            case 'numbering':
                // check, if this is a numbering list
                if (attributes.bullet.type !== 'numbering') { return ''; } // no numbering list

                var numType = attributes.bullet.numType;
                return (typeof numType === 'string') ? numType : null;

            default:
                globalLogger.error('ListHandlerMixin.getListStyleId(): unexpected list type');
        }

        // empty string (no list active)
        return '';
    };

    /**
     * Item getter function for a list style identifier. The difference to
     * the above function 'getListStyleId' is, that this function returns
     * an empty string, if the bullet type is set to 'none'. Therefore the
     * two list styles 'noBulletListItem' and 'noNumberListItem' are
     * handled differently.
     *
     * @param {String} listType
     *  The type of the list styles: either 'bullet' or 'numbering'.
     *
     * @param {Object} attributes
     *  The (merged) attribute set of the selected paragraphs.
     *
     * @returns {String|Null}
     *  If unambiguous, the list style identifier contained in the passed
     *  paragraph attributes, if the type of that list matches the list
     *  type specified in the item user data ('bullets' or 'numbering'),
     *  otherwise an empty string. If ambiguous, returns the value null.
     */
    this.getEffectiveListStyleId = function (listType, attributes) {

        if ((!attributes.bullet) || (attributes.bullet && attributes.bullet.type === 'none')) { return ''; }

        return self.getListStyleId(listType, attributes);
    };

    /**
     * Item setter function for a list style identifier.
     *
     * @param {String} listType
     *  The type of the list styles: either 'bullet' or 'numbering'.
     *
     * @param {String} listStyleId
     *  The new list style identifier. The value of the special constant
     *  DEFAULT_LIST_STYLE_ID can be used to toggle the default
     *  list style.
     *
     * @param {String} oldStyleId
     *  The old list style identifier.
     */
    this.setListStyleId = function (listType, listStyleId, oldStyleId) {

        // simulate toggle behavior for default list style
        if (listStyleId === DEFAULT_LIST_STYLE_ID) {

            if (oldStyleId === '') { // empty string is the case without list
                createSelectedListStyle(listStyleId, listType);
            } else {
                removeListAttributes(); // removing list attributes, even if oldStyleId is null
            }
        } else if (listStyleId === '') {
            removeListAttributes();
        } else {
            // list level may be null, will fall back to level 0 then...
            createSelectedListStyle(listStyleId, listType, null, { expandNumberList: isNumberChangeInOneParagraph(listStyleId, oldStyleId) });
        }
    };

    /**
     * Generating the list style key from a specified paragraph level.
     *
     * @param {Number} level
     *  The paragraph level.
     *
     * @returns {String}
     *  The key used in list styles corresponding to the specified paragraph level.
     */
    this.generateListKeyFromLevel = function (level) {
        return _.isNumber(level) ? 'l' + (level + 1) : null;
    };

    /**
     * Public helper function to remove list attributes, that is used after
     * 'backspace' was pressed.
     */
    this.removeListAttributes = function () {
        removeListAttributes();
    };

    /**
     * Getting the maximum list level that is available in the drawing that contains
     * the selection.
     *
     * @returns {Number}
     *  The maximum list level available in the drawing that contains the selection.
     */
    this.getMaxListLevel = function () {

        var // the maximum value for the list level
            maxValue = 0,
            // the selected text frame node (also finding text frames inside groups)
            drawing = null,
            // an object with all list level attributes
            allListLevelAttributes = null;

        if (self.getApp().isSpreadsheetApp()) {
            maxValue = ListHandlerMixin.DEFAULT_SPREADSHEET_MAXLISTLEVEL;
        } else {
            drawing = self.getSelection().getAnyTextFrameDrawing({ forceTextFrame: true });
            allListLevelAttributes = self.paragraphStyles.getAllAvailableListStyleAttributes ? self.paragraphStyles.getAllAvailableListStyleAttributes(drawing, { nameOnly: true }) : null;
            maxValue = allListLevelAttributes ? (_.keys(allListLevelAttributes).length - 1) : 0;
        }

        return maxValue;
    };

    /**
     * Returns all predefined bullet list styles.
     *
     * @returns {Object}
     *  A map with list style identifiers as keys, and objects as values
     *  containing the attributes 'definition' with the list style definition,
     *  'listLabel' containing the bullet text, and 'tooltip' containing a GUI
     *  tool tip string for the list style.
     */
    this.getPredefinedBulletListStyles = function () {

        var // the resulting bullet list styles, mapped by list identifier
            bulletListStyles = {};

        _(PREDEFINED_LISTSTYLES).each(function (listStyle, listStyleId) {
            if (listStyle.definition.bullet.type === 'character') {
                bulletListStyles[listStyleId] = listStyle;
            }
        });

        return bulletListStyles;
    };

    /**
     * Returns all predefined numbered list styles.
     *
     * @returns {Object}
     *  A map with list style identifiers as keys, and objects as values
     *  containing the attributes 'definition' with the list style definition,
     *  'listlabel' containing a string with the number 1 formatted according
     *  to the list style, and 'tooltip' containing a GUI tool tip string for
     *  the list style.
     */
    this.getPredefinedNumberedListStyles = function () {

        var // the resulting numbered list styles, mapped by list identifier
            numberedListStyles = {};

        _(PREDEFINED_LISTSTYLES).each(function (listStyle, listStyleId) {
            if (listStyle.definition.bullet.type === 'numbering') {
                numberedListStyles[listStyleId] = listStyle;
            }
        });

        return numberedListStyles;
    };

    /**
     * Changing the level of the selected paragraph(s).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.increase=true]
     *      Whether the indentation level of the selected paragraph(s) shall be
     *      increased or decreased.
     *  @param {Boolean} [options.validatedLevel=false]
     *      Whether it is necessary to validate the list level. This is necessary,
     *      if the increase/decrease was triggered by a 'tab' key press.
     *      Using the tool bar the validity is already checked by the availability
     *      of the buttons (but only for the first selected paragraph).
     */
    this.changeListIndent = function (options) {

        // iterating over all selected paragraphs

        var // the operations generator
            generator = self.createOperationGenerator(),
            // whether the list indent shall be increased or decreased
            doIncrease = getBooleanOption(options, 'increase', true),
            // the new indent level
            newIndent = 0,
            // the selection object
            selection = self.getSelection(),
            // the maximum allowed level
            validatedLevel = getBooleanOption(options, 'validatedLevel', false),
            // the drawing node, in which the paragraph is located
            drawing = null,
            // the list style attributes defined at the layout slides and drawings
            listLayoutAttributes = null,
            // whether the first paragraph of the selection is handled
            isFirstParagraph = true,
            // whether the final paragraph in the selection got a new numbering type
            lastGotNewNumberingType = false,
            // saving the operation options for paragraphs behind the selection
            paraOperationOptions = null,
            // the logical positions of the paragraphs following the selection
            allFollowingParagraphPositionsWithSameLevel = null,
            // the final paragraph in the selection and its logical position and indent
            finalParagraph = null, finalPosition = null, finalIndent = 0, finalNumType = null;

        // iterating over all paragraphs in the selection
        selection.iterateContentNodes(function (paragraph, position) {

            var // the application
                app = self.getApp(),
                // the explicit attributes
                explicitAttrs = getExplicitAttributeSet(paragraph),
                // the explicit paragraph attributes
                paraAttrs = explicitAttrs && explicitAttrs.paragraph ? explicitAttrs.paragraph : null,
                // the paragraphs current indent level
                currentIndent = paraAttrs && _.isNumber(paraAttrs.level) ? paraAttrs.level : 0,
                // the options for the setAttributes operation
                operationOptions = null,
                // the old value for the left indentation, that is explicitely set at the paragraph
                oldIndentLeft = paraAttrs && _.isNumber(paraAttrs.indentLeft) ? paraAttrs.indentLeft : null,
                // the new value for the left indentation
                newIndentLeft = null,
                // the maximum allowed list level for the selected paragraph(s)
                maxLevel = 0,
                // the old key and the new key for the list levels
                oldListKey = null, newListKey = null,
                // the difference of leftIndent in the layout styles
                oldLayoutLeftIndent = 0, newLayoutLeftIndent = 0, differentLayoutLeftIndent = 0,
                // the numbering type of the paragraph in its current level and in its new level
                currentNumType = null, newNumType = null,
                // the bullet object used in the operation
                bulletValue = null,
                // the numbering type object with propertiess 'numType' and 'startValue'
                bulletNumberingType = null,
                // whether an operation must be generated
                generateOperation = true;

            // resetting marker
            lastGotNewNumberingType = false;

            // fast process for only one paragraph
            if (_.isNumber(currentIndent)) {

                // calculating the new indent
                newIndent = doIncrease ? currentIndent + 1 : currentIndent - 1;

                if (!validatedLevel) {
                    if (self.getApp().isSpreadsheetApp()) {
                        maxLevel = ListHandlerMixin.DEFAULT_SPREADSHEET_MAXLISTLEVEL;
                    } else {
                        drawing = drawing || $(paragraph).closest('div.drawing'); // TODO: Group handling?
                        if (!listLayoutAttributes && self.paragraphStyles.getAllAvailableListStyleAttributes) {
                            listLayoutAttributes = self.paragraphStyles.getAllAvailableListStyleAttributes(drawing);
                        }
                        maxLevel = listLayoutAttributes ? (_.keys(listLayoutAttributes).length - 1) : 0;
                    }
                }

                if (validatedLevel || (newIndent >= 0 && newIndent <= maxLevel)) { // creating new operation only for valid list levels

                    operationOptions = { start: _.copy(position), attrs: { paragraph: { level: newIndent } } };

                    // getting the drawing, to check, if it is a place holder drawing or not
                    drawing = drawing || $(paragraph).closest('div.drawing'); // TODO: Group handling

                    if (!_.isNumber(oldIndentLeft) && self.getApp().isSpreadsheetApp()) { oldIndentLeft = 0; }

                    // if 'indentLeft' is specified at the paragraph, this needs to be adapted
                    // -> the new value for 'indentLeft' can be calculated with the layout styles.
                    if (_.isNumber(oldIndentLeft)) {
                        if (!listLayoutAttributes && self.paragraphStyles.getAllAvailableListStyleAttributes) {
                            listLayoutAttributes = self.paragraphStyles.getAllAvailableListStyleAttributes(drawing);
                        }
                        oldListKey = self.generateListKeyFromLevel(currentIndent);
                        newListKey = self.generateListKeyFromLevel(newIndent);
                        oldLayoutLeftIndent = listLayoutAttributes && listLayoutAttributes[oldListKey] && listLayoutAttributes[oldListKey].paragraph && listLayoutAttributes[oldListKey].paragraph.indentLeft ? listLayoutAttributes[oldListKey].paragraph.indentLeft : 0;
                        newLayoutLeftIndent = listLayoutAttributes && listLayoutAttributes[newListKey] && listLayoutAttributes[newListKey].paragraph && listLayoutAttributes[newListKey].paragraph.indentLeft ? listLayoutAttributes[newListKey].paragraph.indentLeft : 0;
                        differentLayoutLeftIndent = oldLayoutLeftIndent - newLayoutLeftIndent;
                        if (differentLayoutLeftIndent < 0) { differentLayoutLeftIndent = -differentLayoutLeftIndent; }

                        // handling for Spreadsheet application
                        if (differentLayoutLeftIndent === 0 && self.getApp().isSpreadsheetApp()) { differentLayoutLeftIndent = ListHandlerMixin.PREDEFINED_LEFT_INDENT_DEFAULT_SMALL; }

                        newIndentLeft = doIncrease ? oldIndentLeft + differentLayoutLeftIndent : oldIndentLeft - differentLayoutLeftIndent;
                        operationOptions.attrs.paragraph.indentLeft = newIndentLeft;

                        if (listLayoutAttributes && self.getApp().isODF()) { operationOptions.attrs.paragraph.indentFirstLine = listLayoutAttributes[newListKey].paragraph.indentFirstLine; }
                    }

                    // trying to find a good number list ID for numbered lists
                    if (isNumberingListStyleAttributeSet(explicitAttrs)) {
                        currentNumType = explicitAttrs.paragraph.bullet.numType;
                        bulletNumberingType = getBulletNumberingType(paragraph, newIndent);
                        newNumType = bulletNumberingType.numType;

                        if (newNumType && newNumType !== currentNumType) {
                            bulletValue = _.clone(explicitAttrs.paragraph.bullet);
                            bulletValue.numType = newNumType;
                            operationOptions.attrs.paragraph.bullet = bulletValue;
                        }

                        // setting marker for paragraphs behind the selection
                        lastGotNewNumberingType = true;
                        // and saving the operation options, node and logical position
                        paraOperationOptions = _.copy(operationOptions, true);
                        finalParagraph = paragraph;
                        finalPosition = position;
                        finalIndent = newIndent;
                        finalNumType = newNumType;

                        // setting the start value
                        if (_.isNumber(bulletNumberingType.startValue) && bulletNumberingType.startValue > -1) {
                            // setting the value of the new paragraph
                            operationOptions.attrs.paragraph.bullet = operationOptions.attrs.paragraph.bullet || _.clone(paraAttrs.bullet);
                            operationOptions.attrs.paragraph.bullet.startAt = bulletNumberingType.startValue;

                        } else if (paraAttrs && paraAttrs.bullet && _.isNumber(paraAttrs.bullet.startAt)) {
                            // removing the value of the current paragraph
                            operationOptions.attrs.paragraph.bullet = operationOptions.attrs.paragraph.bullet || _.clone(paraAttrs.bullet);
                            operationOptions.attrs.paragraph.bullet.startAt = null;
                        }
                    } else {
                        // sending information about the bullet type in ODP, if it is an explicit attribute (DOCS-4439)
                        if (app.isODF() && app.isPresentationApp() && paraAttrs && paraAttrs.bullet && !operationOptions.attrs.paragraph.bullet) {
                            operationOptions.attrs.paragraph.bullet = _.clone(paraAttrs.bullet);
                        }
                    }

                    // avoid to generate operations for implicit paragraphs in ODP
                    if (app.isODF() && app.isPresentationApp() && isImplicitParagraphNode(paragraph)) {
                        self.paragraphStyles.setElementAttributes(paragraph, operationOptions.attrs);
                        generateOperation = false; // no operation
                    }

                    // generate the 'setAttributes' operation
                    if (generateOperation) { generator.generateOperation(SET_ATTRIBUTES, operationOptions); }
                }
            }

            if (isFirstParagraph) {
                isFirstParagraph = false;
                validatedLevel = false; // only the first paragraph was validated in 'isListIndentChangeable'
            }
        });

        // the directly following paragraphs can also get the new value for numbering type, if they are
        // also numbering paragraphs and share the same level
        if (lastGotNewNumberingType && paraOperationOptions) {
            allFollowingParagraphPositionsWithSameLevel = getFollowingNumberingParagraphs(finalParagraph, finalPosition, finalIndent, finalNumType);
            // setting the global options saved in 'paraOperationOptions' to all following paragraphs
            _.each(allFollowingParagraphPositionsWithSameLevel, function (paraPos) {
                var newOptions = _.copy(paraOperationOptions, true);
                newOptions.start = paraPos;
                generator.generateOperation(SET_ATTRIBUTES, newOptions);
            });
        }

        // apply all collected operations
        this.applyOperations(generator);
    };

    /**
     * Creates a new list after pressing 'Enter'. In this case the list auto detection found
     * a valid list structure.
     *
     * @param {String} type
     *  The list type: 'numbering' or 'bullet'
     *
     * @param {Object} options
     *  The detected options that are used to create the new list style.
     */
    this.createList = function (type, options) {

        var // the list id of the automatically created list
            listId = null,
            // the logical position of the first paragraph
            paraPos = null,
            // an object with additional options
            localOptions = null;

        if (type === 'bullet' && options.symbol) {
            listId = ListHandlerMixin.AUTODETECTION_BULLET_LISTSTYLE[options.symbol];
            paraPos = _.initial(options.startPosition);
            createSelectedListStyle(listId, type, [paraPos, increaseLastIndex(paraPos)]);
        } else if (type === 'numbering' && options.numberFormat) {

            listId = findAutoListId(options.left || '', options.right || '', options.numberFormat);

            if (listId) {
                if (_.isNumber(options.listStartValue)) { localOptions = { listStartValue: options.listStartValue }; }
                paraPos = _.initial(options.startPosition);
                createSelectedListStyle(listId, type, [paraPos, increaseLastIndex(paraPos)], localOptions);
            }
        }

    };

    /**
     * Whether the list level of the currently selected paragraph(s) can be increased
     * or decreased.
     *
     * It is only the first paragraph of the selection investigated. If the selection
     * contains additional paragraphs, that cannot be modified, no operations are
     * generated for those paragraphs. But this function 'isListIndentChangeable' must
     * be performant, because it is used for updating the buttons in the UI.
     *
     * @param {Object} [paraAttrs]
     *  explicit attributes of paragraph
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.increase=true]
     *      Whether the indentation level of the selected paragraph(s) shall be
     *      increased or decreased.
     */
    this.isListIndentChangeable = function (paraAttrs, options) {

        // whether the level shall be increased or decreased
        var doIncrease = getBooleanOption(options, 'increase', true);
        // the paragraphs current indent level
        var currentIndent = paraAttrs && _.isNumber(paraAttrs.level) ? paraAttrs.level : 0;

        if ((!doIncrease && currentIndent > 0) || (doIncrease && currentIndent < self.getMaxListLevel())) { return true; }

        return false;
    };

    /**
     * Setting the default text list styles. The values are content of the
     * "changeConfig" operation.
     *
     * @param {Object} listStyles
     *  The default text list styles. This must be an object with the keys in the format
     *  'l<number>', for example 'l3'. The value for such a level is an object, that
     *  contains as members the 'paragraph' or 'character' family attributes.
     */
    this.setDefaultTextListStyles = function (listStyles) {
        defaultTextListStyles = listStyles;

        // there are some hard coded default values, that need to be added here (48628)
        addDefaultValuesToTextListStyles();
    };

    /**
     * Receiving the complete container for the default text list styles. These are
     * used in non-placeholder drawings, like text frames or shapes.
     *
     * @returns {Object|Null}
     *  The container for the default text list styles. Or null, if this is not
     *  defined (in the "changeConfig" operation).
     */
    this.getDefaultTextListStyles = function () {
        return defaultTextListStyles;
    };

    /**
     * Receiving the container for one specified level for the default text list styles.
     * These styles are used in non placeholder drawings, like text frames or shapes.
     *
     * @param {Number|String} level
     *  The key that specifies the level. This can be a number, that is converted to the
     *  key-string-format. In the model for the default text list styles the allowed keys
     *  are 'l1' or 'l3'. The level that is used in the operations is a zero-based number.
     *  So level 0 will be converted to 'l1'.
     *
     * @returns {Object|Null}
     *  The container for the default text list styles for one specified level. Or null,
     *  if this is not defined (in the "changeConfig" operation).
     */
    this.getDefaultTextListStylesForLevel = function (level) {

        var // the key that is used for the default list styles (l1 - l9)
            key = _.isNumber(level) ? self.generateListKeyFromLevel(level) : level;

        return (defaultTextListStyles && defaultTextListStyles[key]) ? defaultTextListStyles[key] : null;
    };

    /**
     * Check, whether the specified paragraph attribute set specifies a paragraph with
     * defined bullet type. The bullet can be a character, a number or a bitmap.
     *
     * @param {Object} paragraphAttrs
     *  The paragraph attribute set.
     *
     * @returns {Boolean}
     *  Whether the specified paragraph attribute set specifies a paragraph with defined
     *  bullet type.
     */
    this.isListParagraph = function (paragraphAttrs) {
        return paragraphAttrs && paragraphAttrs.bullet && paragraphAttrs.bullet.type && paragraphAttrs.bullet.type && paragraphAttrs.bullet.type !== 'none';
    };

    /**
     * Check, whether the specified paragraph attribute set specifies a paragraph with
     * defined bullet numbering type.
     *
     * @param {Object} paragraphAttrs
     *  The paragraph attribute set.
     *
     * @returns {Boolean}
     *  Whether the specified paragraph attribute set specifies a paragraph with defined
     *  bullet numbering type.
     */
    this.isNumberedListParagraph = function (paragraphAttrs) {
        return (paragraphAttrs && paragraphAttrs.bullet && paragraphAttrs.bullet.type && paragraphAttrs.bullet.type && paragraphAttrs.bullet.type === 'numbering') || false;
    };

    /**
     * Check, whether at a specified position the list auto detection shall be executed. In
     * the text application or spreadsheet application there are not limitations yet.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph node.
     *
     * @returns {Boolean}
     *  Whether the list auto detection can be executed in the specified paragraph.
     */
    this.isAutoDetectionPosition = function (paragraph) {
        return self.autoDetectionPositionCheck ? self.autoDetectionPositionCheck(paragraph) : true;
    };

    /**
     * Check, whether a specified string at a specified position in a paragraph with a specified
     * text length can be used to create a list automatically.
     *
     * @param {Number[]} position
     *  The logical position.
     *
     * @param {Number} paragraphLength
     *  The length of the text in the paragraph.
     *
     * @param {String} paraText
     *  The text in the paragraph.
     *
     * @returns {Boolean}
     *  Whether the specified string can be used to create a list automatically.
     */
    this.isListAutoDetectionString = function (position, paragraphLength, paraText) {

        return (_.last(position) > (paraText.indexOf('. ') + 1)) && (
            ((paraText.indexOf('. ') >= 0) && (paragraphLength > 3) && (paraText.indexOf('. ') < (paraText.length - 2))) ||
            ((paraText.indexOf(') ') >= 0) && (paragraphLength > 3) && (paraText.indexOf(') ') < (paraText.length - 2))) ||
            ((paraText.indexOf(' ') >= 0) && (paragraphLength > 2) && (paraText.indexOf('* ') === 0 || paraText.indexOf('- ') === 0))
        );
    };

    /**
     * Creating the item for a numbered list.
     *
     * @param {Number} number
     *  The number to be converted.
     *
     * @param {String} numberingType
     *  The format, in which the number shall be converted. Supported are
     *  all formats, that are supported from 'formatNumber'.
     *
     * @returns {String}
     *  The converted specified value. Or the empty string, if no conversion
     *  was done.
     */
    this.createNumberedListItem = function (number, numberingType) {

        var // the description of the numbering type
            listStyle = null,
            // the numbering type
            numType = numberingType || ListHandlerMixin.DEFAULT_NUMBERING_TYPE,
            // the text string for the list item
            text = '';

        if (!PREDEFINED_LISTSTYLES[numType]) { numType = ListHandlerMixin.DEFAULT_NUMBERING_TYPE; }

        listStyle = PREDEFINED_LISTSTYLES[numType];

        // converting the number
        text = formatNumber(number, listStyle.format);

        // creating the full text
        text = text ? listStyle.textBefore + text + listStyle.textAfter : '';

        return text;
    };

    /**
     * Handling the visibility of the bullet or numbering items in empty list paragraphs.
     * If the paragraph is empty the item must not be visible. But if the selection is
     * inside the paragraph, it becomes partly visible.
     */
    this.handleListItemVisibility = function () {

        var // the paragraph containing the new selection
            newSelectedParagraph = null,
            // whether the paragraph with the old selection was an empty list paragraph
            oldEmptyListParagraph = false,
            // whether the paragraph with the new selection is an empty list paragraph
            newEmptyListParagraph = false,
            // the merged paragraph attributes
            paragraphAttributes = null,
            // the selection object
            selection = self.getSelection(),
            // the selected paragraph
            selectedParagraph = self.getSelectedParagraph(),
            // whether the old selected paragraph is an empty list paragraph now
            isEmptyListParagraphLocal = false,
            // the drawing content node that contains the old paragraph
            drawingContent = null,
            // the parent of the paragraph
            paraParent = null,
            // the drawing node
            drawing = null,
            // whether this is an ODF app
            isODF = self.getApp().isODF();

        // getting the new paragraph
        if (selection.getSelectionType() === 'text' && !selection.hasRange() && !selection.isEmptySelection()) {
            newSelectedParagraph = getParagraphElement(selection.getRootNode(), _.initial(selection.getStartPosition()));

            if (isODF && newSelectedParagraph && selectedParagraph.length > 0 && selectedParagraph.parent().length === 0) {
                // no longer in the DOM, problem of handling for implicit paragraphs in ODF
                oldEmptyListParagraph = selectedParagraph.hasClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS); // was it empty before?
                selectedParagraph = $(newSelectedParagraph);
            }
        }

        // handling the old selected paragraph
        if (selectedParagraph.length > 0) {
            isEmptyListParagraphLocal = isEmptyListParagraph(selectedParagraph);
            oldEmptyListParagraph = oldEmptyListParagraph || selectedParagraph.hasClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS);
            selectedParagraph.toggleClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS, isEmptyListParagraphLocal);
            if (!isODF) { selectedParagraph.removeClass(PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS); }
            // also checking, if the flag can be removed from the text frame
            // -> otherwise this would happen, if the selection is no longer inside the drawing
            if (oldEmptyListParagraph && !isEmptyListParagraphLocal) {
                drawingContent = selectedParagraph.closest(CONTENT_NODE_SELECTOR);
                if (drawingContent.hasClass(EMPTYTEXTFRAME_CLASS)) {
                    drawingContent.removeClass(EMPTYTEXTFRAME_CLASS);
                    drawingContent.removeClass(EMPTYTEXTFRAMEBORDER_CLASS);
                    drawingContent.removeClass(EMPTY_THICKBORDER_CLASS);
                    drawing = drawingContent.parent();
                    drawing.removeClass(EMPTYTEXTFRAMEBORDER_CLASS);
                    drawing.removeClass(EMPTY_THICKBORDER_CLASS);
                }
                // Presentation specific update
                if (self.drawingStyles.handleImageTemplateContainer && isTextFrameNode(selectedParagraph.parent())) { // only for top level text, for example not in tables
                    drawing = drawing || drawingContent.parent();
                    self.drawingStyles.handleImageTemplateContainer(drawing, false); // remove an image template container
                }
            }
        }

        // handling the new selected paragraph
        if (newSelectedParagraph) {
            selectedParagraph = $(newSelectedParagraph);

            if (isEmptyListParagraph(selectedParagraph) || (isODF && isImplicitParagraphNode(selectedParagraph))) {
                selectedParagraph.addClass(PARAGRAPH_NODE_LIST_EMPTY_CLASS);
                if (!isODF) { selectedParagraph.addClass(PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS); }
                newEmptyListParagraph = true;
            }

            self.setSelectedParagraph(selectedParagraph);
        }

        // handling a switch of class 'PARAGRAPH_NODE_LIST_EMPTY_CLASS' -> in this case the list update needs to be triggered
        if (oldEmptyListParagraph !== newEmptyListParagraph) {
            // update list only, if the list is a numbered list
            paragraphAttributes = self.paragraphStyles.getElementAttributes(selectedParagraph);

            if (paragraphAttributes && paragraphAttributes.paragraph && self.isNumberedListParagraph(paragraphAttributes.paragraph)) {
                self.updateListsDebounced(selectedParagraph);
            }

            if (newEmptyListParagraph) {
                paraParent = selectedParagraph.parent();
                if (self.drawingStyles.handleImageTemplateContainer && isTextFrameNode(paraParent) && paraParent.children(PARAGRAPH_NODE_SELECTOR).length === 1) { // only for top level text, for example not in tables
                    drawingContent = drawingContent || selectedParagraph.closest(CONTENT_NODE_SELECTOR);
                    drawing = drawingContent.parent();
                    self.drawingStyles.handleImageTemplateContainer(drawing, true); // insert an image template container
                }
            }
        }

    };

    /**
     * Getting the paragraph that contains the current selection.
     *
     * @returns {jQuery}
     *  The paragraph that contains the current selection. If there is
     *  no paragraph, the jQuery collection contains no element.
     */
    this.getSelectedParagraph = function () {
        return selectedParagraph;
    };

    /**
     * Setting the paragraph that contains the current selection.
     *
     * @param {jQuery} [para]
     *  Setting the paragraph that contains the current selection. If the
     *  paragraph is not specified, the global 'selectedParagraph' is set
     *  to an empty jQuery collection.
     */
    this.setSelectedParagraph = function (para) {
        selectedParagraph = para || $();
    };

    // initialization -----------------------------------------------------

    // handling selection change events in Spreadsheet
    if (self.getApp().isSpreadsheetApp()) {
        self.getSelection().on('change', selectionChangeHandler);
    }
}

// constants --------------------------------------------------------------

/**
 * A constant containing the ID of the default bullet list style.
 */
ListHandlerMixin.DEFAULT_BULLET_LISTSTYLE = 'L20001';

/**
 * The default numbering type for numbered lists.
 */
ListHandlerMixin.DEFAULT_NUMBERING_TYPE = 'arabicPeriod';

/**
 * An object containing the connection from character to list style IDs.
 */
ListHandlerMixin.AUTODETECTION_BULLET_LISTSTYLE = {
    '-': 'L20013',
    '*': 'L20001'
};

/**
 * A constant containing the default value for the paragraph left indent,
 * if a list item is inserted.
 */
ListHandlerMixin.PREDEFINED_LEFT_INDENT_DEFAULT_LARGE = 1500;

/**
 * A constant containing the default value for the paragraph left indent,
 * if a list item is inserted.
 */
ListHandlerMixin.PREDEFINED_LEFT_INDENT_DEFAULT_SMALL = 600;

/**
 * A constant containing the negative indent for the first line of a paragraph,
 * if a list item is inserted.
 */
ListHandlerMixin.PREDEFINED_INDENT_FIRST_LINE_DEFAULT_LARGE = -1500;

/**
 * ODF: A constant containing the negative indent for the first line of a paragraph,
 * if a list item is inserted.
 */
ListHandlerMixin.PREDEFINED_INDENT_FIRST_LINE_DEFAULT_SMALL = -600;

/**
 * A constant that will be used to specify the default font size, that is hard
 * coded in PP.
 */
ListHandlerMixin.DEFAULT_FONT_SIZE = 18;

/**
 * A constant that is used to specify the maximum number of list levels in the
 * Spreadsheet application.
 */
ListHandlerMixin.DEFAULT_SPREADSHEET_MAXLISTLEVEL = 8;
