/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ox from "$/ox";

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { math, fun, json } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, TOUCH_DEVICE, IOS_SAFARI_DEVICE, containsNode, createSpan, getFocus, setFocus, hideFocus, containsFocus, KeyCode, isIMEInputKey, matchKeyCode } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { transformOpColor, opRgbColor, Color } from '@/io.ox/office/editframework/utils/color';
import { comparePositions } from '@/io.ox/office/editframework/utils/operations';
import { otLogger } from '@/io.ox/office/editframework/utils/otutils';
import { getElementStyleId, getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { EditModel } from '@/io.ox/office/editframework/model/editmodel';
import { resolvePresetBorder } from '@/io.ox/office/drawinglayer/utils/drawingutils';

import { getWidthForPreset } from '@/io.ox/office/editframework/utils/border';
import { DEBUG, MAX_TABLE_CELLS, MAX_TABLE_COLUMNS, MAX_TABLE_ROWS } from '@/io.ox/office/textframework/utils/config';
import * as Op from '@/io.ox/office/textframework/utils/operations';
import { canMergeNeighboringOperations, convertHmmToLength, convertLengthToHmm, findNextNode,
    getArrayOption, getBooleanOption, getDomNode, getEmptyDrawingBackgroundAttributes, getFunctionOption, getNodeName,
    getNumberOption, getObjectOption, handleParagraphIndex, isHangulText, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';
import { applyImageTransformation, getFileUrl, insertDataUrl, insertURL, isAbsoluteUrl, isBase64, postProcessBackgroundImageOperationAttributes,
    postProcessOperationAttributes } from '@/io.ox/office/drawinglayer/utils/imageutils';
import * as DrawingFrame from '@/io.ox/office/drawinglayer/view/drawingframe';
import { clearCropFrame, cropFillFitSpace as cropFillFitSpaceImport, drawCropFrame, refreshCropFrame as refreshCropFrameImport } from '@/io.ox/office/textframework/components/drawing/imagecropframe';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import Selection from '@/io.ox/office/textframework/selection/selection';
import RemoteSelection from '@/io.ox/office/textframework/selection/remoteselection';
import { getColumnCount, getRowCount } from '@/io.ox/office/textframework/components/table/table';
import { CLEAR_ATTRIBUTES } from '@/io.ox/office/textframework/components/hyperlink/hyperlink';
import TextOperationGenerator from '@/io.ox/office/textframework/model/operationgenerator';
import { findValidSelection, getClosestDrawingAtPosition, getClosestDrawingTextframe, getContentNodeElement,
    getCurrentParagraph, getDOMPosition, getFirstPositionInParagraph, getLastNodeFromPositionByNodeName,
    getLastPositionInParagraph, getOxoPosition, getOxoPositionFromPixelPosition, getParagraphElement,
    getParagraphNodeLength, getPixelPositionToRootNodeOffset, getVerticalPagePixelPosition,
    getWordBoundaries, hasSameParentComponent, increaseLastIndex, isEmptyParagraph, isPositionInsideDrawingGroup,
    isPositionInTable as isPositionInTableImport, isValidElementPosition, isValidTextPosition, iterateParagraphChildNodes,
    positionsInTowCellsInSameTable } from '@/io.ox/office/textframework/utils/position';
import RangeMarker from '@/io.ox/office/textframework/components/rangemarker/rangemarker';
import ChangeTrack from '@/io.ox/office/textframework/components/changetrack/changetrack';
import DrawingLayer from '@/io.ox/office/textframework/components/drawing/drawinglayer';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';
import { XClipBoard } from '@/io.ox/office/textframework/model/clipboardmixin';
import { XClipBoardHandler } from '@/io.ox/office/textframework/model/clipboardhandlermixin';
import { XStorageOperation } from '@/io.ox/office/textframework/model/storageoperationmixin';
import { XStringConverter } from '@/io.ox/office/textframework/model/stringconvertermixin';
import { XTableOperation } from '@/io.ox/office/textframework/model/tableoperationmixin';
import { XGroupOperation } from '@/io.ox/office/textframework/model/groupoperationmixin';
import { XAttributeOperation } from '@/io.ox/office/textframework/model/attributeoperationmixin';
import { XDeleteOperation } from '@/io.ox/office/textframework/model/deleteoperationmixin';
import { XParagraph } from '@/io.ox/office/textframework/model/paragraphmixin';
import { XHardBreak } from '@/io.ox/office/textframework/model/hardbreakmixin';
import { XHyperlink } from '@/io.ox/office/textframework/model/hyperlinkmixin';
import { XKeyHandling } from '@/io.ox/office/textframework/model/keyhandlingmixin';
import PageLayout from '@/io.ox/office/textframework/components/pagelayout/pagelayout';
import SearchHandler from '@/io.ox/office/textframework/components/searchhandler/searchhandler';
import SpellChecker from '@/io.ox/office/textframework/components/spellcheck/spellchecker';

// constants ==================================================================

// style attributes for heading 1 -6 based on latent styles
const HEADINGS_CHARATTRIBUTES = [
    { color: transformOpColor(Color.ACCENT1, { shade: 74902 }), bold: true, fontSize: 14 },
    { color: Color.ACCENT1, bold: true, fontSize: 13 },
    { color: Color.ACCENT1, bold: true },
    { color: Color.ACCENT1, bold: true, italic: true },
    { color: transformOpColor(Color.ACCENT1, { shade: 49804 }) },
    { color: transformOpColor(Color.ACCENT1, { shade: 49804 }), italic: true }
];

const DEFAULT_PARAGRAPH_DEFINTIONS = { default: true, styleId: 'Standard', styleName: 'Normal' };

// style attributes for lateral table style
const DEFAULT_LATERAL_TABLE_DEFINITIONS = { default: true, styleId: 'TableGrid', styleName: 'Table Grid', uiPriority: 59 };
const DEFAULT_LATERAL_TABLE_ATTRIBUTES = {
    wholeTable: {
        paragraph: { lineHeight: { type: 'percent', value: 100 }, marginBottom: 0 },
        table: {
            borderTop:        { color: Color.AUTO, width: 17, style: 'single' },
            borderBottom:     { color: Color.AUTO, width: 17, style: 'single' },
            borderInsideHor:  { color: Color.AUTO, width: 17, style: 'single' },
            borderInsideVert: { color: Color.AUTO, width: 17, style: 'single' },
            borderLeft:       { color: Color.AUTO, width: 17, style: 'single' },
            borderRight:      { color: Color.AUTO, width: 17, style: 'single' },
            paddingBottom: 0,
            paddingTop: 0,
            paddingLeft: 190,
            paddingRight: 190
        }
    }
};

// style attributes for lateral table style, used for ODF
// because in ODF 'TableGrid' can already exist and it has no borders defined
const DEFAULT_LATERAL_TABLE_OX_DEFINITIONS = { default: true, styleId: 'TableGridOx', styleName: 'Table Grid Ox', uiPriority: 59 };
const DEFAULT_LATERAL_TABLE_OX_ATTRIBUTES = {
    wholeTable: {
        paragraph: { lineHeight: { type: 'percent', value: 100 }, marginBottom: 0 },
        table: {
            borderTop:        { color: Color.AUTO, width: 17, style: 'single' },
            borderBottom:     { color: Color.AUTO, width: 17, style: 'single' },
            borderInsideHor:  { color: Color.AUTO, width: 17, style: 'single' },
            borderInsideVert: { color: Color.AUTO, width: 17, style: 'single' },
            borderLeft:       { color: Color.AUTO, width: 17, style: 'single' },
            borderRight:      { color: Color.AUTO, width: 17, style: 'single' },
            paddingBottom: 0,
            paddingTop: 0,
            paddingLeft: 190,
            paddingRight: 190
        }
    }
};

// style attributes for lateral table style
const DEFAULT_LATERAL_TABLE_DEFINITIONS_PRESENTATION = { default: true, styleId: '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}', styleName: 'Medium Style 2 - Accent 1', uiPriority: 59 };

// style attributes for lateral comment style
const DEFAULT_LATERAL_COMMENT_DEFINITIONS = { default: false, styleId: 'annotation text', styleName: 'annotation text', parent: 'Standard', uiPriority: 99 };
const DEFAULT_LATERAL_COMMENT_ATTRIBUTES = { character: { fontSize: 10 }, paragraph: { lineHeight: { type: 'percent', value: 100 }, marginBottom: 0 } };

// style attributes for lateral header style
const DEFAULT_LATERAL_HEADER_DEFINITIONS = { default: false, styleId: 'Header', styleName: 'Header', parent: 'HeaderFooter', uiPriority: 99 };

// style attributes for lateral footer style
const DEFAULT_LATERAL_FOOTER_DEFINITIONS = { default: false, styleId: 'Footer', styleName: 'Footer', parent: 'HeaderFooter', uiPriority: 99 };

const DEFAULT_HYPERLINK_DEFINTIONS = { default: false, styleId: 'Hyperlink', styleName: 'Hyperlink', uiPriority: 99 };
const DEFAULT_HYPERLINK_CHARATTRIBUTES = { color: _.extend({ fallbackValue: '0080C0' }, Color.HYPERLINK), underline: true };

const DEFAULT_DRAWING_MARGINS = { marginTop: 317, marginLeft: 317, marginBottom: 317, marginRight: 317 };

const DEFAULT_DRAWING_DEFINITION = { default: true, styleId: 'default_drawing_style', styleName: 'Default Graphic Style', uiPriority: 50 };
const DEFAULT_DRAWING_ATTRS = { line: { color: opRgbColor('3465a4') } };

const DEFAULT_DRAWING_TEXTFRAME_DEFINITION = { default: false, styleId: 'Frame', uiPriority: 60 };
const DEFAULT_DRAWING_TEXTFRAME_ATTRS = {
    line: { style: 'solid', type: 'solid', width: 2, color: Color.BLACK },
    shape: { paddingLeft: 150, paddingTop: 150, paddingRight: 150, paddingBottom: 150 },
    drawing: { anchorVertAlign: 'top', marginLeft: 201, indentLeft: 201, marginTop: 201, marginRight: 201, indentRight: 201, marginBottom: 201, textWrapMode: 'square', textWrapSide: 'both', anchorHorAlign: 'center', anchorHorBase: 'column', anchorVertBase: 'paragraph' }
};
const DEFAULT_DRAWING_TEXTFRAME_ATTRS_ODF_SLIDE = { // 52317
    line: { style: 'solid', type: 'solid', width: 2, color: Color.BLACK },
    shape: { paddingLeft: 250, paddingTop: 125, paddingRight: 250, paddingBottom: 125 },
    drawing: { anchorVertAlign: 'top', marginLeft: 201, indentLeft: 201, marginTop: 201, marginRight: 201, indentRight: 201, marginBottom: 201, textWrapMode: 'square', textWrapSide: 'both', anchorHorAlign: 'center', anchorHorBase: 'column', anchorVertBase: 'paragraph' }
};

// list of operations that insert content into a paragraph, DOCS-4211
const INSERT_PARA_CONTENT_OPERATIONS = [Op.TEXT_INSERT, Op.TAB_INSERT, Op.INSERT_DRAWING, Op.BOOKMARK_INSERT, Op.RANGE_INSERT, Op.HARDBREAK_INSERT, Op.COMMENT_INSERT, Op.COMPLEXFIELD_INSERT];

// if set to true, events caught by the editor will be logged to console
const LOG_EVENTS = false;

// Performance: List of operation names, that cache the paragraph in the selection
const PARAGRAPH_CACHE_OPERATIONS = {
    [Op.TEXT_INSERT]: true,
    [Op.PARA_INSERT]: true,
    [Op.PARA_SPLIT]: true,
    [Op.SET_ATTRIBUTES]: true
};

// vars =======================================================================

// internal clipboard
let clipboardOperations = [];
let clipboardId = '';

// private global functions ===================================================

/**
 * Returns whether the passed document operations refer to the same parent
 * document component (i.e. their 'start' properties contain equal indexes
 * except for the last array element), and refer to the same document
 * target (i.e. their 'target' properties are equal).
 *
 * @param {Object} operation1
 *  The first document operation to be checked.
 *
 * @param {Object} operation2
 *  The second document operation to be checked.
 *
 * @returns {Boolean}
 *  Whether the passed document operations refer to the same parent
 *  document component.
 */
function operationsHaveSameParentComponent(operation1, operation2) {
    return _.isArray(operation1.start) && _.isArray(operation2.start) &&
        hasSameParentComponent(operation1.start, operation2.start) &&
        (operation1.target === operation2.target);
}

/**
 * Returns whether the passed undo action is an 'insertText' action (an
 * action with a 'insertText' redo operation and a 'deleteText' undo
 * operation).
 *
 * @param {UndoAction} action
 *  The action to be tested.
 *
 * @param {Boolean} single
 *  If true, the action must contain exactly one 'deleteText' undo
 *  operation and exactly one 'insertText' redo operation. Otherwise, the
 *  last operations of each of the arrays are checked, and the arrays may
 *  contain other operations.
 */
function isInsertTextAction(action, single) {
    return (single ? (action.undoOperations.length === 1) : (action.undoOperations.length >= 1)) && (_.first(action.undoOperations).name === Op.DELETE) &&
        (single ? (action.redoOperations.length === 1) : (action.redoOperations.length >= 1)) && (_.last(action.redoOperations).name === Op.TEXT_INSERT);
}

/**
 * Tries to merge the passed undo actions. Merging works only if both
 * actions represent a single 'insertText' operation, and the passed action
 * appends a single character directly after the text of this action.
 *
 * @param {UndoAction} lastAction
 *  The existing action that will be extended if possible.
 *
 * @param {UndoAction} nextAction
 *  The new action that will be tried to merge into the existing action.
 *
 * @returns {Boolean}
 *  Whether the passed new undo action has been merged successfully into
 *  the existing undo action.
 */
function mergeUndoActionHandler(lastAction, nextAction) {

    // check if this and the passed action is an 'insertText' action
    const validActions = isInsertTextAction(lastAction, false) && isInsertTextAction(nextAction, true);
    // the redo operation of this action and the passed action
    const lastRedo = _.last(lastAction.redoOperations);
    const nextRedo = nextAction.redoOperations[0];

    // check that the operations are valid for merging the actions
    if (validActions && (nextRedo.text.length >= 1) && operationsHaveSameParentComponent(lastRedo, nextRedo)) {

        // check that the new action adds the character directly after the text of this action and does not change the attributes
        // check that the last character of this action is not a space character (merge actions word by word)
        // check that the operations can be merged corresponding to their attributes
        if ((_.last(lastRedo.start) + lastRedo.text.length === _.last(nextRedo.start)) && (lastRedo.text.substr(-1) !== ' ') && canMergeNeighboringOperations(lastRedo, nextRedo)) {
            // merge undo operation (delete one more character)
            lastAction.undoOperations[0].end[lastAction.undoOperations[0].end.length - 1] += nextRedo.text.length;
            // merge redo operation (add the character)
            lastRedo.text += nextRedo.text;
            return true;
        }
    }
    return false;
}

// class Editor ===========================================================

/**
 * The textframework editor model. Contains and manages the entire DOM of the edited
 * text document. Implements execution of all supported operations.
 *
 * Triggers the events supported by the base class EditModel, and the
 * following additional events:
 * - 'selection': When the editor selection has been changed. Event
 *      handlers receive a reference to the selection object (instance of
 *      class Selection).
 * - 'changeTrack:stateInfo': When the change tracking state of the document
 *      is modified. This event is triggered with the option 'state: true' after
 *      loading the document and the document contains change tracked elements
 *      or after receiving an operation with change track attributes.
 *      The event is triggered with 'state: false', if no change tracked element was
 *      found inside the document.
 * - 'drawingHeight:update': When the height of a drawing is changed. This can
 *      be used to update the borders of the drawing and surrounding groups.
 * - 'update:absoluteElements': When the document is modified in that way, that
 *      it is necessary to update all absolute positioned elements.
 * - 'cacheBuffer:flush': When edit rights are transfered, and there are still
 *      not registerd operations in buffer, like for update of date fields after load.
 * - 'change:pageSettings': This event is triggered after a change of the documents
 *      page settings.
 *
 * @mixes XClipboard
 * @mixes XClipBoardHandler
 * @mixes XStringConverter
 * @mixes XStorageOperation
 * @mixes XTableOperation
 * @mixes XGroupOperation
 * @mixes XAttributeOperation
 * @mixes XDeleteOperation
 * @mixes XParagraph
 * @mixes XHardBreak
 * @mixes XHyperlink
 * @mixes XKeyHandling
 *
 * @param {EditApplication} app
 *  The application containing this editor instance.
 *
 * @param {Object} [initOptions]
 *  Optional parameters passed to the base class constructor. Additionally,
 *  the following options are supported:
 *  @param {Boolean} [initOptions.spellingSupported=false]
 *      Whether the spell checker will be enabled by this instance.
 */
export default class Editor extends XKeyHandling.mixin(XHyperlink.mixin(XHardBreak.mixin(XParagraph.mixin(XDeleteOperation.mixin(XAttributeOperation.mixin(XGroupOperation.mixin(XTableOperation.mixin(XStorageOperation.mixin(XStringConverter.mixin(XClipBoardHandler.mixin(XClipBoard.mixin(EditModel)))))))))))) {

    // the root element for the document contents
    #editdiv = DOM.createPageNode().attr({ contenteditable: true, 'data-gramm': false, tabindex: 0, 'data-focus-role': 'page', role: 'main' }).addClass('user-select-text noI18n f6-target');
    // the undo manager of this document
    #undoManager = null;
    // the logical selection, synchronizes with browser DOM selection
    #selection = null;
    // the remote selection handling collaborative overlays
    #remoteSelection = null;
    // the change track handler
    #changeTrack = null;
    // the handler object for fields
    #fieldManager = null;
    // the drawing layer for all drawings assigned to the page
    #drawingLayer = null;
    // the comment layer for all comments
    #commentLayer = null;
    // the handler for all range markers in the document
    #rangeMarker = null;
    // instance handling visual page layout, like page breaks, headers/footers, etc.
    #pageLayout = null;
    // instance handling search functions
    #searchHandler = null;
    // the spell checker object
    #spellChecker = null;
    // shortcuts for other format containers
    #listCollection = null;
    // values needed for pagebreaks calculus
    #pageAttributes;
    #pagePaddingLeft;
    #pageWidth;
    #pbState = true;
    #quitFromPageBreak = false;
    #blockOnInsertPageBreaks = false;
    // the list of all known authors
    #authorsList;
    // whether the user selects 'View in draft mode'. This value is not set, when draft mode
    // is force on small devices. See TODO at function 'isDraftMode()'.
    #draftModeState = false;
    // attributes that were set without a selection and are only set for a single character
    #preselectedAttributes = null;
    // whether the preselected attributes must not be set to null (for example after splitParagraph)
    #keepPreselectedAttributes = false;
    // whether mouse down occured in the this.#editdiv (and no mouse up followed)
    #activeMouseDownEvent = false;
    // whether a double click event is currently handled
    #activeDoubleClickEvent = false;
    // whether the directly following mouseup event can be ignored
    #ignoreFollowingMouseUpEventHandler = false;
    // whether mouse down occured in the comments side pane
    #mouseDownInCommentsPane = false;
    // whether mouse down occured in a comments bubble node
    #mouseDownInCommentsBubbleNode = false;
    // whether a click event shall be ignored
    #ignoreClickEvent = false;
    // whether a group of operations requires, that the undo stack must be deleted
    #deleteUndoStack = false;
    // whether an operation is part of an undo group
    #isInUndoGroup = false;
    #undoRedoRunning = false;
    // whether local operations are generated and applied for the header/footer without entering edit mode of header/footer
    #isInactiveLocalHeaderFooterOp = false;
    // the promise that controls an already active (deferred) text input
    #inputTextPromise = null;
    // a paragraph node, that is implicit and located behind a table, might temporarily get an increased height
    #increasedParagraphNode = null;
    // all paragraphs (and their background-color) that are artificially highlighted
    #highlightedParagraphs = [];
    // all cells that are artificially highlighted
    #highlightedCells = [];
    // whether an update of element formatting is required
    #requiresElementFormattingUpdate = true;
    // whether an operation was triggered by a GUI action (for table operations)
    #guiTriggeredOperation = false;
    // whether the keyboard events need to be blocked, for example during pasting
    // of internal clipboard
    #blockKeyboardEvent = false;
    // whether the IME block is active (Safari only)
    #imeBlock = false;
    // a marker for IME handler function (Safari only)
    #safariIMESyncCall = false;
    // whether an internal or external clipboard paste is currently running
    #pasteInProgress = false;
    // the current event from the keyDown handler
    #lastKeyDownEvent = null;
    // the last key down key code
    #lastKeyDownKeyCode = null;
    // the last key down modifiers
    #lastKeyDownModifiers = null;
    // whether a tab processing was started by the keydown event
    #isKeyDownTabHandling = false;
    // whether several repeated key down events without keypress occured
    #repeatedKeyDown = false;
    // a timer for the keyup event handler
    #processKeyUpTimer = null;
    // the logical position where the operation ends, can be used for following cursor positioning
    #lastOperationEnd; // need to decide: Should the last operation modify this member, or should the selection be passed up the whole call chain?!
    // whether an undo shall set the selection range after applying the operation
    #useSelectionRangeInUndo = false;
    // indicates an active ime session
    #imeActive = false;
    // ime objects now stored in a queue as we can have more than
    // one ime processing at a time. due to asynchronous processing
    #imeStateQueue = [];
    // ime update cache
    #imeUpdateText = null;
    // ime update text must be used
    #imeUseUpdateText = false;
    // mutation observer
    #editDivObserver = null;
    // iPad read-only paragraph
    #roIOSParagraph = null;
    // collects information for debounced page break insertion
    #currentProcessingNodeCollection = [];
    // collects information for debounced automatic resizing of text frame nodes
    #currentProcessingTextFrameNode = null;
    // collects information for debounced target node update
    #targetNodesForUpdate = $();
    // collects information for debounced repeated table rows update
    #tableRepeatNodes = $();
    // whether the paragraph cache can be used (only for internal operations)
    #useParagraphCache = false;
    // Performance: Caching the synchronously used paragraph element
    #paragraphCache = null;
    // handling double click and triple click in IE
    // whether there is a double click event waiting for a third click
    #doubleClickEventWaiting = false;
    // whether the third click occured after a double click, activating the tripleclick event
    #tripleClickActive = false;
    // the period of time, that a double click event 'waits' for a further click (in ms).
    #doubleClickTimeOut = 60;
    // Android Chrome Code
    #androidTimeouter = null;
    // special flag for the very slow android chrome
    #androidTyping = false;
    // global var holding info are header and footer in state of editing
    #headerFooterEditState = false;
    // global var holding container root node for header and footer state
    #$headFootRootNode = $();
    // reference to parent node of header footer root node
    #$parentOfHeadFootRootNode = $();
    // reference to index of header footer root node, if parent is detached
    #rootNodeIndex = null;
    // string holding current editable element's target id
    #activeTarget = '';
    // whether the document was loaded with in 'fastLoad' process
    #fastLoadImport = false;
    // whether the document was loaded with in 'localStorage' process
    #localStorageImport = false;
    // whether there are additional operations loaded from the server during loading document from local storage
    #externalLocalStorageOperationsExist = false;
    // The document state, when the selection changes into a textframe.
    #enterState = null;
    // whether a current change of selection is triggered by a remote client
    #updatingRemoteSelection = false;
    // public listeners for IPAD workaround, all important listeners on this.#editdiv are save, that we can reuse them
    #listenerList = null;
    // the maximum number of top level nodes that defines a large selection (that need to be handled asynchronously)
    #MAX_TOP_LEVEL_NODES = 100;
    // whether the minimum time for a move operation shall be ignored. This is required in Unit tests.
    #ignoreMinMoveTime = false;
    // whether the document is free of change track nodes
    // -> this can be set to 'true' earliest in '#documentLoaded' after call of 'this.#changeTrack.updateSideBar()'
    #ctFreeDoc = false;
    // a counter for checking, if it is required to set 'modifying: false' to the operations that will be sent (DOCS-2144)
    #modifiyingCounter = 0;
    // the target string that can be used to avoid automatic target setting at operations. This string must not be used as target.
    #operationTargetBlocker = 'TARGET_BLOCKER_VALUE';
    // a cache for the size of the currently selected drawing
    #savedDrawingSize = null;
    // a store that contains paragraphs, which needs to be formatted with colored margins or borders later
    #paragraphQueueForColoredMarginAndBorder = [];
    // dumping the passed event object to the browser console.
    #dumpEventObject = null;
    // debounced functions, that will be set after successful document load
    #implParagraphChanged = $.noop;
    #implTableChanged = $.noop;
    #insertPageBreaksDebounced = $.noop;
    #updateEditingHeaderFooterDebounced = $.noop;
    #updateRepeatedTableRowsDebounced = $.noop;
    #updateHighligtedRangesDebounced = $.noop;
    #updateDrawingsDebounced = $.noop;

    // Handler for updating change tracks debounced. It invalidates the existing side bar. This needs to be done after operations or after 'refresh:layout' event.
    #updateChangeTracksDebounced = $.noop;
    // Handler for updating change tracks debounced. It does not invalidate the existing side bar. This can happen for example after 'scroll' events.
    #updateChangeTracksDebouncedScroll = $.noop;

    constructor(app, initOptions) {

        // base constructor ---------------------------------------------------

        super(app, _.extend({
            mergeUndoActionHandler,
            operationsFinalizer: (...args) => this.#finalizeOperations(...args),
            storageOperationCollector: (...args) => this.#saveStorageOperationHandler(...args)
        }, initOptions));

        this.#dumpEventObject = LOG_EVENTS ? event => {
            globalLogger.log('type=' + event.type + ' keyCode=' + event.keyCode + ' charCode=' + event.charCode + ' shift=' + event.shiftKey + ' ctrl=' + event.ctrlKey + ' alt=' + event.altKey + ' data=' + event.originalEvent.data);
        } : $.noop;

        // initialization (registering the handler functions for the operations) --------

        this.registerOperationHandler(Op.CHANGE_CONFIG, context => {

            // the attribute set from the passed operation
            const attributes = _.clone(context.getDict('attrs'));

            //only undo for page attributes
            if (attributes.page && !context.external) {
                const oldPageAttributes = this.pageStyles.getElementAttributes(this.#editdiv).page;
                for (const key in oldPageAttributes) {
                    if (!(key in attributes.page)) {
                        delete oldPageAttributes[key];
                    }
                }
                const undoOp = { name: Op.CHANGE_CONFIG, attrs: { page: oldPageAttributes } };
                this.#undoManager.addUndo(undoOp, context);
            }

            this.applyChangeConfigOperation(context);

            // update local pageAttributes
            this.setPageAttributes(this.pageStyles.getElementAttributes(this.#editdiv));

            if (context && context.operation && context.operation.attrs && context.operation.attrs.authors) {
                this.setAuthorsList(context.operation.attrs.authors);
                // informing listeners, that the list of authors was updated
                this.trigger('authorList:update', context.operation.attrs.authors);
            }
        });

        this.registerPlainOperationHandler(Op.MOVE, (operation, external) => {
            if (this.#undoManager.isUndoEnabled() && !external) {
                // Todo: Ignoring 'end', only 'start' === 'end' is supported
                const undoOperation = { name: Op.MOVE, start: _.copy(operation.to), end: _.copy(operation.to), to: _.copy(operation.start) };

                this.#extendPropertiesWithTarget(undoOperation, operation.target);
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return this.#implMove(operation.start, operation.end, operation.to, operation.target);
        });

        this.registerOperationHandler(Op.TEXT_INSERT, context => {

            const start = context.getPos('start');
            const text = context.getStr('text');
            const attrs = context.optDict('attrs');
            const target = context.optStr('target');
            const noUndo = context.optBool('noUndo');

            context.ensure(this.#implInsertText(start, text, attrs, target, context.external), 'cannot insert text');

            if (this.#undoManager.isUndoEnabled() && !context.external && !noUndo) {
                const end = increaseLastIndex(start, text.length - 1);
                const undoOperation = { name: Op.DELETE, start: _.copy(start), end: _.copy(end) };

                this.#extendPropertiesWithTarget(undoOperation, target);
                this.#undoManager.addUndo(undoOperation, context);
            }
        });

        this.registerPlainOperationHandler(Op.FIELD_INSERT, (operation, external) => {

            if (!this.#implInsertField(operation.start, operation.type, operation.representation, operation.attrs, operation.target)) {
                return false;
            }

            if (this.#undoManager.isUndoEnabled() && !external) {
                const undoOperation = { name: Op.DELETE, start: _.copy(operation.start) };

                this.#extendPropertiesWithTarget(undoOperation, operation.target);
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return true;
        });

        this.registerPlainOperationHandler(Op.FIELD_UPDATE, (operation, external) => {

            if (!this.#implUpdateField(operation.start, operation.type, operation.representation, operation.attrs, operation.target, external)) {
                return false;
            }
            // undo is added in impl function
            return true;
        });

        this.registerPlainOperationHandler(Op.TAB_INSERT, (operation, external) => {

            if (!this.#implInsertTab(operation.start, operation.attrs, operation.target)) {
                return false;
            }

            if (this.#undoManager.isUndoEnabled() && !external) {
                const undoOperation = { name: Op.DELETE, start: _.copy(operation.start) };
                this.#extendPropertiesWithTarget(undoOperation, operation.target);
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return true;
        });

        this.registerPlainOperationHandler(Op.INSERT_DRAWING, (operation, external) => {

            if (!this.#implInsertDrawing(operation.type, operation.start, operation.attrs, operation.target)) {
                return false;
            }

            if (this.#undoManager.isUndoEnabled() && !external) {
                // not registering for undo if this is a drawing inside a drawing group (36150)
                const undoOperation = isPositionInsideDrawingGroup(this.getRootNode(operation.target), operation.start) ? null : { name: Op.DELETE, start: _.copy(operation.start) };

                if (undoOperation) {
                    this.#extendPropertiesWithTarget(undoOperation, operation.target);
                }
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return true;
        });

        this.registerPlainOperationHandler(Op.INSERT_LIST, (operation, external) => {
            if (!external) { this.#undoManager.addUndo({ name: Op.DELETE_LIST, listStyleId: operation.listStyleId }, operation); }
            this.#listCollection.insertList(operation);
        });

        this.registerPlainOperationHandler(Op.DELETE_LIST, (operation, external) => {
            this.#listCollection.deleteList(operation.listStyleId, external);
        });

        this.registerPlainOperationHandler(Op.INSERT_HEADER_FOOTER, (operation, external) => {
            if (this.#undoManager.isUndoEnabled() && !external) {
                const undoOperation = { name: Op.DELETE_HEADER_FOOTER, id: operation.id };

                this.#undoManager.addUndo(undoOperation, operation);
            }
            return this.#implInsertHeaderFooter(operation.id, operation.type, operation.attrs, external);
        });

        this.registerPlainOperationHandler(Op.RANGE_INSERT, (operation, external) => {

            // the undo operation
            let undoOperation = null;

            if (!this.#implInsertRange(operation.start, operation.id, operation.type, operation.position, operation.attrs, operation.target)) {
                return false;
            }

            if (this.#undoManager.isUndoEnabled() && !external) {
                undoOperation = { name: Op.DELETE, start: _.copy(operation.start) };
                this.#extendPropertiesWithTarget(undoOperation, operation.target);
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return true;
        });

        this.registerPlainOperationHandler(Op.COMPLEXFIELD_INSERT, (operation, external) => {

            // the undo operation
            let undoOperation = null;

            if (!this.#implInsertComplexField(operation.start, operation.instruction, operation.attrs, operation.target)) {
                return false;
            }

            if (this.#undoManager.isUndoEnabled() && !external) {
                undoOperation = { name: Op.DELETE, start: _.copy(operation.start) };
                this.#extendPropertiesWithTarget(undoOperation, operation.target);
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return true;
        });

        this.registerPlainOperationHandler(Op.COMPLEXFIELD_UPDATE, (operation, external) => {

            if (!this.#implUpdateComplexField(operation.start, operation.instruction, operation.attrs, operation.target, external)) {
                return false;
            }
            // undo is added in impl function

            return true;
        });

        this.registerPlainOperationHandler(Op.BOOKMARK_INSERT, (operation, external) => {

            // the undo operation
            let undoOperation = null;

            if (!this.#implInsertBookmark(operation.start, operation.id, operation.anchorName, operation.position, operation.attrs, operation.target)) {
                return false;
            }

            if (this.#undoManager.isUndoEnabled() && !external) {
                undoOperation = { name: Op.DELETE, start: _.copy(operation.start) };
                this.#extendPropertiesWithTarget(undoOperation, operation.target);
                this.#undoManager.addUndo(undoOperation, operation);
            }
            return true;
        });

        this.registerPlainOperationHandler(Op.DELETE_HEADER_FOOTER, (operation, external) => {

            const generator = this.#undoManager.isUndoEnabled() ? this.createOperationGenerator() : null;
            const containerNode = this.getRootNode(operation.id);
            // must be empty, so that generateContentOperations makes operations for all content inside node
            const position = [];
            let options = null;
            let type = null;
            let undoOperation = null;

            // undo/redo generation
            if (this.#undoManager.isUndoEnabled() && !external) {
                if (generator) {
                    options = { target: operation.id };

                    containerNode.children('.cover-overlay').remove();
                    // check if there are special fields for restoring before generating op
                    this.#fieldManager.checkRestoringSpecialFieldsInContainer(containerNode);

                    generator.generateContentOperations(containerNode, position, options);
                    this.#undoManager.addUndo(generator.getOperations());
                }

                type = this.#pageLayout.getTypeFromId(operation.id);
                if (type) {
                    undoOperation = { name: Op.INSERT_HEADER_FOOTER, id: operation.id, type };
                    this.#undoManager.addUndo(undoOperation, operation);
                } else {
                    return;
                }

            }

            return this.#implDeleteHeaderFooter(operation.id);
        });

        this.registerPlainOperationHandler(Op.TABLE_INSERT, this.insertTableHandler);
        this.registerPlainOperationHandler(Op.ROWS_INSERT, this.insertRowHandler);
        this.registerPlainOperationHandler(Op.COLUMN_INSERT, this.insertColumnHandler);
        this.registerPlainOperationHandler(Op.SET_ATTRIBUTES, this.setAttributesHandler);
        this.registerPlainOperationHandler(Op.DELETE, this.deleteHandler);
        this.registerPlainOperationHandler(Op.COLUMNS_DELETE, this.deleteColumnsHandler);
        this.registerPlainOperationHandler(Op.TABLE_SPLIT, this.tableSplitHandler);
        this.registerPlainOperationHandler(Op.TABLE_MERGE, this.tableMergeHandler);
        this.registerPlainOperationHandler(Op.CELL_MERGE, this.cellMergeHandler);
        this.registerPlainOperationHandler(Op.CELLS_INSERT, this.cellInsertHandler);
        this.registerPlainOperationHandler(Op.PARA_SPLIT, this.splitParagraphHandler);
        this.registerPlainOperationHandler(Op.PARA_MERGE, this.mergeParagraphHandler);
        this.registerOperationHandler(Op.PARA_INSERT, this.insertParagraphHandler);
        this.registerPlainOperationHandler(Op.HARDBREAK_INSERT, this.insertHardBreakHandler);
        this.registerPlainOperationHandler(Op.GROUP, this.groupDrawingsHandler);
        this.registerPlainOperationHandler(Op.UNGROUP, this.ungroupDrawingsHandler);

        // initialization -----------------------------------------------------

        // setting the undo manager
        this.#undoManager = this.getUndoManager();

        // the constructor of the CommentLayer class used by this document
        const CommentLayerClass = getFunctionOption(initOptions, 'commentLayerClass');

        // creating several new objects
        this.#selection = this.member(new Selection(this));
        this.#remoteSelection = this.member(new RemoteSelection(this));
        this.#changeTrack = this.member(new ChangeTrack(this, this.#editdiv));
        this.#drawingLayer = this.member(new DrawingLayer(this, this.#editdiv));
        if (CommentLayerClass) { this.#commentLayer = this.member(new CommentLayerClass(this)); }
        this.#rangeMarker = this.member(new RangeMarker(this));
        this.#pageLayout = this.member(new PageLayout(this, this.#editdiv));
        this.#searchHandler = this.member(new SearchHandler(this));
        this.#spellChecker = this.member(new SpellChecker(this, initOptions));

        // initialize document contents
        this.docApp.onInit((...args) => this.#initDocument(...args));

        // Creating debounced function for updating drawing nodes.
        // Info: This function is important for performance reasons.
        // Example: After modifying the paragraphs inside a drawing, 'this.#implParagraphChangedSync' is called for every paragraph.
        //          This function triggers an update of the surrounding drawing. But this should happen only once, not five
        //          times.
        this.#updateDrawingsDebounced = fun.do(() => {

            // all drawing nodes that need to be updated
            let allDrawings = $();

            // Direct callback: Called every time when implParagraphChanged() has been called.
            // Parameter 'paragraph': HTMLElement|jQuery|Number[]
            // The paragraph element as DOM node or jQuery object, or the logical position of the paragraph or any of its child components.
            const registerDrawingNode = drawingNode => {
                // store the drawing in the collection (jQuery keeps the collection unique)
                if (drawingNode) {
                    allDrawings = allDrawings.add(drawingNode);
                }
            };

            // deferred callback: called once, after current script ends
            const updateAllDrawings = () => {

                const modifiedDrawings = this.docApp.isPresentationApp() ? [] : null;

                _.each(allDrawings, oneDrawing => {

                    if (DrawingFrame.isAutoResizeHeightDrawingFrame(oneDrawing)) {
                        this.#updateDrawings($(oneDrawing));
                        if (DrawingFrame.isGroupedDrawingFrame(oneDrawing)) {
                            DrawingFrame.updateDrawingGroupHeight(this.docApp, oneDrawing);
                        }
                        if (modifiedDrawings) { modifiedDrawings.push(oneDrawing); }
                    } else if (this.useSlideMode()) {
                        if (DrawingFrame.isAutoTextHeightDrawingFrame(oneDrawing)) {
                            // updating the font size dynamically inside the drawing node (only supported in slide mode)
                            if (this.updateDynFontSizeDebounced && !this.isClipboardPasteInProgress()) {
                                this.updateDynFontSizeDebounced(oneDrawing);
                                if (modifiedDrawings) { modifiedDrawings.push(oneDrawing); }
                            }
                        } else if (DrawingFrame.isGroupDrawingFrame(oneDrawing)) {
                            // generic formatting of group drawing frame after change of child drawing attributes
                            DrawingFrame.updateFormatting(this.docApp, oneDrawing, this.drawingStyles.getElementAttributes(oneDrawing));
                            if (modifiedDrawings) { modifiedDrawings.push(oneDrawing); }
                        }
                    } else if (this.docApp.isTextApp()) {
                        const attrs = getExplicitAttributeSet(oneDrawing);
                        const geometryAttrs = (attrs && attrs.geometry) || null;
                        DrawingFrame.handleTextframeOverflow($(oneDrawing), this.docApp.isODF(), geometryAttrs);
                    }
                });

                // updating the side pane might be required in Presentation app
                if (modifiedDrawings && modifiedDrawings.length > 0) { this.updateAllAffectedSlidesInSlidePane(modifiedDrawings); }

                allDrawings = $(); // reset the collector
            };

            // create and return the debounced method
            return this.debounce(registerDrawingNode, updateAllDrawings, { delay: 100, maxDelay: 500 });
        });

        // initialize selection after import
        this.waitForImport(function () {
            if (!this.useSlideMode()) { this.#selection.selectDocumentLoadPosition(); } // in presentation a 'slide selection' is set
            this.docApp.getView().grabFocus({ afterImport: true });
        });

        // more initialization after successful import
        this.waitForImportSuccess(this.#documentLoaded);

        // disable browser editing capabilities if import fails
        this.waitForImportFailure(() => {
            this.#editdiv.attr('contenteditable', false);
        });

        // Generating 'implParagraphChanged' after successful document load.
        // Has to be called every time after a paragraph was changed.
        this.waitForImportSuccess(() => {

            // all paragraph nodes that need to be updated
            let paragraphs = $();

            // Direct callback: Called every time when implParagraphChanged() has been called.
            // Parameter 'paragraph': HTMLElement|jQuery|Number[]
            // The paragraph element as DOM node or jQuery object, or the logical position of the paragraph or any of its child components.
            const registerParagraph = paragraph => {
                if (_.isArray(paragraph)) {
                    paragraph = getCurrentParagraph(this.getCurrentRootNode(), paragraph);
                }
                // store the new paragraph in the collection (jQuery keeps the collection unique)
                if (paragraph) {
                    // reset to 'not spelled'
                    this.#spellChecker.resetDirectly(paragraph);
                    paragraphs = paragraphs.add(paragraph);
                }
            };

            // deferred callback: called once, after current script ends
            const updateParagraphs = () => {
                this.#implParagraphChangedSync(paragraphs);
                paragraphs = $();
            };

            // create and return the deferred implParagraphChanged() method
            this.#implParagraphChanged = this.debounce(registerParagraph, updateParagraphs, { delay: 200, maxDelay: 1000 });
        });

        // Generating 'this.#implTableChanged' after successful document load.
        // Has to be called every time after a table was changed. It recalculates the position
        // of each cell in the table and sets the corresponding attributes. This can be set for
        // the first or last column or row, or even only for the south east cell.
        this.waitForImportSuccess(() => {

            // all table nodes that need to be updated
            let tables = $();

            // direct callback: called every time when this.#implTableChanged() has been called
            // @param {HTMLTableElement|jQuery} table
            const registerTable = table => {
                // store the new table in the collection (jQuery keeps the collection unique)
                tables = tables.add(table);
            };

            // deferred callback: called once, after current script ends
            const updateTables = () => {

                if (this.docApp.isOTEnabled()) {
                    let marginalTablesAdded = false;
                    _.each(tables, table => {
                        // in the meantime the header could be activated -> updating all tables in the active header/footer, too (67148)
                        if (!marginalTablesAdded && this.isHeaderFooterEditState() && DOM.isMarginalNode(table) && this.#pageLayout.getHeaderFooterPlaceHolder().find(table)) {
                            tables = tables.add(this.getCurrentRootNode().find(DOM.TABLE_NODE_SELECTOR));
                            marginalTablesAdded = true;
                        }
                    });
                }

                tables.get().forEach(table => {
                    // the table may have been removed from the DOM in the meantime
                    // -> just checking, if the table is still somewhere below the page
                    if (containsNode(this.#editdiv, table)) {
                        this.tableStyles.updateElementFormatting(table).done(() => { this.trigger('tableUpdate:after', table); });
                    }
                });
                tables = $();
            };

            // create and return the deferred this.#implTableChanged() method
            this.#implTableChanged = this.debounce(registerTable, updateTables);
        });

        if (!this.useSlideMode()) {

            this.waitForImportSuccess(() => {

                // defining function for debounced setting of page breaks
                this.#insertPageBreaksDebounced = this.debounce(this.#registerPageBreaksInsert, () => {
                    if (this.#currentProcessingTextFrameNode) { this.trigger('drawingHeight:update', $(this.#currentProcessingTextFrameNode)); }
                    this.#currentProcessingTextFrameNode = null;
                    if (this.getBlockOnInsertPageBreaks()) { // if operation is inside header/footer, block page breaks. They will be triggered from other function if necessary.
                        this.setBlockOnInsertPageBreaks(false);
                        this.trigger('update:absoluteElements');  // updating comments and absolutely positioned drawings
                    } else {
                        this.#pageLayout.insertPageBreaks();
                    }
                    // If you increase the delay you must increase the delay of the renderCollaborativeSelectionDebounced delay of the remoteselection
                }, { delay: 200 });

                // Debouncing distribution of updated data from editing node to all other headers&footers with same type
                this.#updateEditingHeaderFooterDebounced = this.debounce(this.#registerTargetNodesUpdate, this.#sendTargetNodesForUpdate, { delay: 200 });

                // Debouncing updating of repeated rows content inside table
                this.#updateRepeatedTableRowsDebounced = this.debounce(this.#registerRepeatedNodesUpdate, this.#sendRepeatedNodesForUpdate, { delay: 500 });

                // public handler function for updating comment ranges debounced. This is
                // required, if a comment range is visualized (with hover) and then it is
                // written inside this comment range. This happens after the event
                // 'paragraphUpdate:after' was triggered.
                this.#updateHighligtedRangesDebounced = this.debounce(() => this.#rangeMarker.updateHighlightedRanges(), { delay: 100 });
            });

        } else {

            this.waitForImportSuccess(() => {
                // defining function for debounced reacting of height changes of content inside text frames
                // -> in slide mode, it is not necessary to update page breaks, but to trigger the event 'drawingHeight:update'
                this.#insertPageBreaksDebounced = this.debounce(this.#registerPageBreaksInsert, () => {
                    if (this.#currentProcessingTextFrameNode) { this.trigger('drawingHeight:update', $(this.#currentProcessingTextFrameNode)); }
                    this.#currentProcessingTextFrameNode = null;
                }, { delay: 200 });
            });
        }

        // restore selection after undo/redo operations
        this.listenTo(this.#undoManager, 'undo:before redo:before', () => {
            this.#undoRedoRunning = true;
            this.setBlockKeyboardEvent(true); // block keyboard events while processing undo operations
        });

        // restore selection after undo/redo operations
        this.listenTo(this.#undoManager, 'undo:after', (operations, options) => this.#undoRedoAfterHandler(false, operations, options));
        this.listenTo(this.#undoManager, 'redo:after', (operations, options) => this.#undoRedoAfterHandler(true, operations, options));

        // undogroup:open : checking if the operations in a group can be undone
        this.listenTo(this.#undoManager, 'undogroup:open', () => {
            this.#deleteUndoStack = false;  // default: undo stack must not be deleted
            this.#isInUndoGroup = true;
        });

        // undogroup:closed : delete undo stack, if required
        this.listenTo(this.#undoManager, 'undogroup:close', () => {
            if (this.#deleteUndoStack) { this.#undoManager.clearUndoActions(); }
            this.#deleteUndoStack = false;
            this.#isInUndoGroup = false;
        });

        // remove highlighting before changing the DOM which invalidates the positions in highlightRanges
        this.listenTo(this, 'operations:before', (operations, external) => {

            // checking operations for change track attributes
            if (!this.#changeTrack.isSideBarHandlerActive() && this.#changeTrack.hasChangeTrackOperation(operations)) {
                this.trigger('changeTrack:stateInfo', { state: true });
            }

            // reset read-only paragraph
            if (this.#roIOSParagraph) {
                if (this.docApp.isEditable()) { $(this.#roIOSParagraph).removeAttr('contenteditable'); }
                this.#roIOSParagraph = null;
            }

            // clear preselected attributes before applying any operations
            if (!this.#keepPreselectedAttributes) { this.#preselectedAttributes = null; }

            // Insert drawing operations with a document URL as imageUrl attribute also have the imageData attribute set.
            // Before the operation is applied we need to determine which attribute to use and which to remove.
            if (!external) {
                _(operations).each(operation => {
                    if (operation.name === Op.INSERT_DRAWING && (operation.type === 'image' || operation.type === 'ole') && operation.attrs && operation.attrs.image) {
                        postProcessOperationAttributes(operation.attrs.image, this.docApp.getFileDescriptor().id);
                    } else if (operation.name === Op.INSERT_DRAWING && operation.type === 'shape' && operation.attrs && operation.attrs.fill && operation.attrs.fill.type === 'bitmap') {
                        postProcessBackgroundImageOperationAttributes(operation.attrs.fill, this.docApp);
                    }
                });
            }
        });

        this.listenTo(this, 'operations:success', (operations, external) => {
            // saving information about the type of operation
            this.#useSelectionRangeInUndo = (_.last(operations).name === Op.SET_ATTRIBUTES);

            // this.#useSelectionRangeInUndo also needs to be true, if after setAttributes an operation like deleteListStyle comes (32005)
            // -> maybe there needs to be a list of operations that can be ignored like 'Operations.DELETE_LIST' following the setAttributes operation
            // -> leaving this simple now because of performance reasons
            if ((!this.#useSelectionRangeInUndo) && (operations.length > 1) && (_.last(operations).name === Op.DELETE_LIST) && (operations[operations.length - 2].name === Op.SET_ATTRIBUTES)) {
                this.#useSelectionRangeInUndo = true;
            }

            // update change track sidebar after successfully applied operation
            this.#updateChangeTracksDebounced();

            // In read-only mode update the selection after an external operation (and after the document is loaded successfully)
            // -> maybe this need to be deferred
            if (!this.docApp.isEditable() && this.isImportFinished()) {
                this.#updatingRemoteSelection = true;
                this.#selection.updateRemoteSelection();
                this.#updatingRemoteSelection = false;
            }

            // an update for headers/footers after applying external operations with target might be required,
            // if the target is not the active target (67442).
            // Typically the update of header/footer is triggered by the event 'paragraphUpdate:after' that is
            // triggered inside the often called function 'this.#implParagraphChangedSync'. But in some cases the updating
            // of a paragraph is not required, and therefore this generic process is required, too.
            if (external && this.docApp.isTextApp()) {
                const targetCollector = {};
                _.each(operations, operation => {
                    if (operation.target && operation.target !== this.getActiveTarget()) {
                        targetCollector[operation.target] = 1; // collecting all targets
                    }
                });
                _.keys(targetCollector).forEach(target => {
                    const templateNode = this.#pageLayout.getHeaderFooterPlaceHolder().children('[data-container-id="' + target + '"]');
                    if (templateNode.length > 0) { this.#updateEditingHeaderFooterDebounced(templateNode); }
                    // if there are drawings inside the marginal node, an update of drawing formatting might be required (66982)
                    // const marginalNode = this.#pageLayout.getFirstMarginalByIdInDoc(target);
                    // if (marginalNode.length && !DOM.isMarginalPlaceHolderNode(marginalNode.parent())) {
                    //     this.#pageLayout.enterHeaderFooterEditMode(marginalNode); // -> triggers drawing formatting
                    //     this.#pageLayout.leaveHeaderFooterEditMode();
                    //     // -> restore the previous selection
                    // }
                });
            }

        });

        // Register the listener for the 'changeTrack:stateInfo' event. This event is
        // triggered with the option 'state: true' after loading the document and the
        // document contains change tracked elements or after receiving an operation
        // with change track attributes.
        // The event is triggered with 'state: false', if no change tracked element was
        // found inside the document.
        this.listenTo(this, 'changeTrack:stateInfo', options => {

            // whether the document contains change tracking content
            const state = getBooleanOption(options, 'state', false);

            if (state) {
                this.#ctFreeDoc = false;
                if (!this.#changeTrack.isSideBarHandlerActive() && !SMALL_DEVICE) {
                    this.#changeTrack.setSideBarHandlerActive(true);
                    this.#updateChangeTracksDebounced = this.debounce(() => { this.#changeTrack.updateSideBar({ invalidate: true }); }, { delay: 500, maxDelay: 1500 });
                    this.#updateChangeTracksDebouncedScroll = this.debounce(() => { this.#changeTrack.updateSideBar({ invalidate: false }); }, { delay: 500, maxDelay: 1500 });
                }
            } else {
                this.#ctFreeDoc = true;
                if (this.#changeTrack.isSideBarHandlerActive()) {
                    this.#changeTrack.setSideBarHandlerActive(false);
                    this.#updateChangeTracksDebounced = $.noop;
                    this.#updateChangeTracksDebouncedScroll = $.noop;
                }
            }

        });

        // Register the listener for the events 'paragraphUpdate:after' and
        // 'tableUpdate:after'. These events are triggered, when the promise
        // updating of paragraphs or tables has finished. The listener receives
        // the formatted node. This is required, because of task 36495. The
        // parameter nodes contains one node or a list of nodes that can be handled
        // in this event handler (Node|Node[]|jQuery).
        for (const eventType of ['paragraphUpdate:after', 'tableUpdate:after', 'drawingHeight:update']) {
            this.on(eventType, (nodes, options) => {

                _.each($(nodes), node => {

                    let drawingNode = null;

                    if (eventType === 'paragraphUpdate:after' && getBooleanOption(options, 'paragraphChanged', false) && DOM.isNodeInsideTextFrame(node)) {
                        drawingNode = DrawingFrame.getDrawingNode(node);
                        this.#updateDrawingsDebounced(drawingNode); // updating the drawing debounced, not for every paragraph inside the drawing synchronously
                    } else if (DOM.isImageDrawingNode(node) || DrawingFrame.isCanvasNode(node) || DOM.isNodeInsideTextFrame(node) || DrawingFrame.isShapeDrawingFrame(node)) {
                        drawingNode = DrawingFrame.getDrawingNode(node);
                        this.#updateDrawings(drawingNode);
                        if (DrawingFrame.isGroupedDrawingFrame(drawingNode) && DrawingFrame.isAutoResizeHeightDrawingFrame(drawingNode)) {
                            DrawingFrame.updateDrawingGroupHeight(this.docApp, drawingNode);
                        }

                        // updating the font size dynamically inside the drawing node (only supported in slide mode)
                        // -> but not, if the event (for example 'drawingheight:update') was triggered by an external operation
                        if (this.updateDynFontSizeDebounced && !this.isClipboardPasteInProgress() && !getBooleanOption(options, 'suppressFontSize', false) && !getBooleanOption(options, 'external', false)) {
                            this.updateDynFontSizeDebounced(drawingNode); // Attention -> this call generates operations!
                        }
                    }
                    if (eventType !== 'drawingHeight:update' && this.#pageLayout.isHeaderFooterActivated()) { // #37250 - Looping request when exit header edit mode
                        if (DOM.isMarginalNode(node)) {
                            this.#updateEditingHeaderFooterDebounced(DOM.getMarginalTargetNode(this.#editdiv, node));
                        } else if (DOM.isHeaderOrFooter(node)) {
                            this.#updateEditingHeaderFooterDebounced(node);
                        } else if ($(node).find(DOM.TAB_NODE_SELECTOR).length && this.#pageLayout.getHeaderFooterPlaceHolder().find(node).length && !this.isUndoRedoRunning()) {
                            // if updated paragraph is inside headerfooter, but in template node, and it has tabs inside,
                            // redistribute to all visible headerfooter nodes
                            this.#pageLayout.replaceAllTypesOfHeaderFooters();
                        }
                    }
                    if (DOM.isCellContentNode($(node).parent()) && DOM.isFirstRowInRepeatingRowsTable($(node).closest(DOM.TABLE_REPEAT_ROWNODE_SELECTOR))) {
                        this.#updateRepeatedTableRowsDebounced(node);
                    }

                    // updating an visualized (comment) range, if necessary
                    this.#updateHighligtedRangesDebounced();
                });
            });
        }

        // Register the handler for document reload event. This happens, if the
        // user cancels a long running action.
        this.on('document:reloaded', this.#documentReloadHandler);

        // Register the handler for the 'docs:editmode' event.
        this.listenTo(this.docApp, 'docs:editmode', this.#changeEditModeHandler);

        this.on('ot:collision', data => {
            otLogger.warn('$badge{Editor} Collision detected: ' + data.message || '');
        });

        this.on('docs:ot:collision', this.#otCollisionHandler);

        // scroll to cursor, forward selection change events to own listeners
        this.listenTo(this.#selection, "change", this.#selectionChangeHandler);

        // Register selectionchange handler to get notification if user changes
        // selection via handles. No other way to detect these selection changes on
        // mobile Safari.
        if (IOS_SAFARI_DEVICE) {
            this.listenToDOMWhenVisible($(document), 'selectionchange', event => {

                // do nothing, if this happens between keydown und keyup event or directly after a keyup event
                if (this.#lastKeyDownEvent || Date.now() - 500 < this.#processKeyUpTimer) { return; }

                const sel = window.getSelection();

                // special OX Presentation behavior in ios
                if (this.getSlideTouchMode()) {
                    if (sel && sel.rangeCount && !this.getSelection().isSlideSelection()) {
                        // Check that the selection is inside our this.#editdiv node. We are listing to the whole document!
                        if (containsNode(this.#editdiv, sel.getRangeAt(0).startContainer)) { this.#selection.processBrowserEvent(event); }
                    }
                } else {

                    if (this.docApp.isTextApp() && sel && sel.rangeCount) {
                        // Check that the selection is inside our this.#editdiv node. We are listing to the whole document!
                        if (containsNode(this.#editdiv, sel.getRangeAt(0).startContainer)) {
                            this.#selection.processBrowserEventIfDelayed(event);
                        } else {
                            // when the user pressed "All", the selection is outside (in the a11y container)
                            if (sel.getRangeAt(0).startOffset === 0 && containsNode(this.#editdiv, sel.getRangeAt(0).endContainer)) {
                                this.selectAll(); // DOCS-5321
                            }
                        }
                    }

                    // behaviour... but there seems not to be a better way. Triggered when pressed on 'done' to close the softKeyboard in ios
                    if (sel.anchorNode === null) {
                        let isActiveComment = false;
                        if (this.docApp.isTextApp() && this.#commentLayer.isCommentEditorActive()) { isActiveComment = true; }
                        if (!isActiveComment) {
                            this.trigger('selection:possibleKeyboardClose');
                        }
                    }
                }
            });
        }

        /**
         * Registering the handler for taking care of special fields BEFORE resolving changetracked nodes.
         */
        this.listenTo(this, 'specialFieldsCT:before', this.#specialFieldsCTbeforeHandler);

        /**
         * Registering the handler for taking care of special fields AFTER resolving changetracked nodes.
         */
        this.listenTo(this, 'specialFieldsCT:after', this.#specialFieldsCTafterHandler);

        // registering keypress handler at document for Chrome to avoid problems with direct keypress to document
        // -> using this.listenToDOMWhenVisible the handler is registered with 'show' and deregistered
        // with 'hide' event of the app automatically and does not influence other apps.
        if (_.browser.WebKit) { this.listenToDOMWhenVisible($(document), 'keypress', this.#processKeypressOnDocument); }

        // hybrid edit mode ('mousedown touchstart' need to be registered specific for each application)
        this.#listenerList = {
            keydown: _.bind(this.getPageProcessKeyDownHandler(), this),
            keypress: _.bind(this.getPageProcessKeyPressHandler(), this),
            keyup: _.bind(this.#processKeyUp, this),
            compositionstart: _.bind(this.#processCompositionStart, this),
            compositionupdate: _.bind(this.#processCompositionUpdate, this),
            compositionend: _.bind(this.#processCompositionEnd, this),
            textInput: _.bind(this.#processTextInput, this),
            input: _.bind(this.#processInput, this),
            'mouseup touchend': _.bind(this.#processMouseUp, this),
            dragstart: _.bind(this.processDragStart, this),
            drop: _.bind(this.processDrop, this),
            dragover: _.bind(this.processDragOver, this),
            dragenter: _.bind(this.processDragEnter, this),
            dragleave: _.bind(this.processDragLeave, this),
            cut: _.bind(this.cut, this),
            copy: _.bind(this.copy, this),
            'paste beforepaste': _.bind(this.paste, this)     // for IE we need to handle 'beforepaste', on all other browsers 'paste'
        };

        this.#editdiv.on(this.#listenerList); // TODO: no more "bind" required after using "listenTo", but "this.#listenerList" is exported

        if (SMALL_DEVICE) { this.#editdiv.on('touchstart touchmove touchend', DOM.TABLE_NODE_SELECTOR, event => this.#processTouchEventsForTableDraftMode(event)); }

        // Fix for 29751: Support double click for word selection
        if (_.browser.WebKit) { this.listenTo(this.#editdiv, "dblclick", this.#processDoubleClick); }

        // header/footer editing on doubleclick
        this.#editdiv.on('dblclick', '.header.inactive-selection, .footer.inactive-selection, .cover-overlay', event => this.#processHeaderFooterEdit(event));

        // user defined fields
        this.#editdiv.on('mouseenter', '.field[class*=user-field]', event => this.#pageLayout.createTooltipForField(event));
        this.#editdiv.on('mouseleave', '.field[class*=user-field]', event => this.#pageLayout.destroyTooltipForField(event));

        // mouseup events can be anywhere -> binding to $(document)
        this.listenTo($(document), 'mouseup touchend', this.#processMouseUpOnDocument);

        // register handler for changes of page settings
        this.on('change:pageSettings', this.#updatePageDocumentFormatting);

        // register handler for cleaning processing nodes collector for page breaks and after canceling snapshoot (#53275)
        this.on('empty:processingNodesCollector document:reloaded:after', () => {
            this.#currentProcessingNodeCollection = []; // empty the collection
        });

        // if image crop mode is active on losing edit rights, exit
        this.listenTo(this.docApp, 'docs:editmode:leave', () => {
            if (this.#selection.isCropMode()) { this.exitCropMode(); }
        });

        // destroy all class members on destruction
        this.registerDestructor(() => {
            this.#editdiv.remove(); // remove page node from document
        });

    }

    // public methods -----------------------------------------------------

    /**
     * Creates and returns an initialized operation generator instance for
     * text operations (instance of `TextOperationGenerator`).
     *
     * @param {GeneratorConfig} [options]
     *  Optional parameters that will be passed to the constructor of the
     *  operations generator.
     *
     * @returns {TextOperationGenerator}
     *  A new operation generator instance for text operations.
     */
    createOperationGenerator(options) {
        return new TextOperationGenerator(this, options);
    }

    /**
     * Returns the root DOM element representing this editor.
     *
     * @returns {jQuery}
     *  The root node containing the document model and representation, as
     *  jQuery object.
     */
    getNode() {
        return this.#editdiv;
    }

    isEmptyPage() { // quite expensive, please handle with care.

        const $pageContent  = DOM.getPageContentNode(this.#editdiv);
        const elmHeaderWrapper = this.#editdiv.children(DOM.HEADER_WRAPPER_SELECTOR)[0];
        const elmFooterWrapper = this.#editdiv.children(DOM.FOOTER_WRAPPER_SELECTOR)[0];

        //  please have a look into
        //
        //  - http://caniuse.com/#feat=innertext
        //  - http://perfectionkills.com/the-poor-misunderstood-innerText/
        //
        return (
            (!!elmHeaderWrapper && ($.trim(elmHeaderWrapper.innerText) === '')) &&
            (!!elmFooterWrapper && ($.trim(elmFooterWrapper.innerText) === '')) &&
            ($.trim($pageContent[0].innerText) === '')
        );
    }

    /**
     * Selects the entire document.
     *
     * @returns {Editor}
     *  A reference to this instance.
     */
    selectAll() {
        this.#selection.selectAll();
        return this;
    }

    /**
     * Returning the drawing layer, that handles all drawings
     * located inside the drawing layer node.
     *
     * @returns {Object}
     *  A reference to the drawing layer object.
     */
    getDrawingLayer() {
        return this.#drawingLayer;
    }

    /**
     * Returning the comment layer object, that handles all comments
     * in the document.
     *
     * @returns {Object}
     *  A reference to the comment layer object.
     */
    getCommentLayer() {
        return this.#commentLayer;
    }

    /**
     * Returning the range marker object, that handles all range marker
     * nodes in the document.
     *
     * @returns {Object}
     *  A reference to the range marker object.
     */
    getRangeMarker() {
        return this.#rangeMarker;
    }

    /**
     * Returning the complex field object, that handles all complex fields
     * in the document.
     *
     * @returns {Object}
     *  A reference to the complex field object.
     */
    getFieldManager() {
        return this.#fieldManager;
    }

    /**
     * Setting the handler for field manager. This handler is different in the
     * several applications.
     *
     * @param {FieldManager | SlideFieldManager} fieldManager
     *  The application specific field manager for updating lists.
     */
    setFieldManager(fieldManager) {
        this.#fieldManager = this.member(fieldManager);
    }

    /**
     * Returning the change track object, that handles all change
     * tracks in the document.
     *
     * @returns {Object}
     *  A reference to the change track object.
     */
    getChangeTrack() {
        return this.#changeTrack;
    }

    /**
     * Getter for pageLayout object instance.
     *
     * @returns {Object}
     *  A reference to the page layout object.
     */
    getPageLayout() {
        return this.#pageLayout;
    }

    /**
     * Getting the blocker target, that avoids, that the active target is automatically
     * added to the operations in the 'this.#finalizeOperations' handler. This blocker is
     * removed in the 'this.#finalizeOperations' handler, so that the operation will not
     * have a target at all. If another target than the active target shall be used
     * in an operation, it must be set during the creation of the operation.
     *
     * @returns {String}
     *  A blocker target string that cannot be used as a valid target. It is automatically
     *  removed in the 'this.#finalizeOperations' handler.
     */
    getOperationTargetBlocker() {
        return this.#operationTargetBlocker;
    }

    /**
     * Setting the handler for number formatter. This handler is different in the
     * several applications.
     */
    setNumberFormatter(numberFormatter) {
        this.numberFormatter = this.member(numberFormatter);
    }

    /**
     * Whether the document was loaded from local storage.
     *
     * @returns {Boolean}
     *  Returns whether the document was loaded from local storage.
     */
    isLocalStorageImport() {
        return this.#localStorageImport;
    }

    /**
     * Whether the document was loaded with 'fast load' process.
     *
     * @returns {Boolean}
     *  Returns whether the document was loaded with 'fast load' process.
     */
    isFastLoadImport() {
        return this.#fastLoadImport;
    }

    /**
     * Setting the global variable 'this.#localStorageImport'.
     *
     * @param {Boolean} value
     *  The value for 'this.#localStorageImport'.
     */
    setLocalStorageImport(value) {
        this.#localStorageImport = value;
    }

    /**
     * Setting the global variable 'this.#externalLocalStorageOperationsExist'.
     * This is set to true, if there are additional operations sent from
     * the server during loading document with local storage.
     * This reduces the performance, because more parts of the document
     * need to be formatted.
     *
     * @param {Boolean} value
     *  The value for 'this.#externalLocalStorageOperationsExist'.
     */
    setExternalLocalStorageOperationsExist(value) {
        this.#externalLocalStorageOperationsExist = value;
    }

    /**
     * Whether there were additional operations sent from the server during
     * loading the document from the local storage.
     *
     * @returns {Boolean}
     *  Returns whether there were additional operations sent from the
     *  server during loading the document from the local storage.
     */
    externalLocalStorageOperationsExist() {
        return this.#externalLocalStorageOperationsExist;
    }

    /**
     * Setting the global variable 'this.#fastLoadImport'.
     *
     * @param {Boolean} value
     *  The value for 'this.#fastLoadImport'.
     */
    setFastLoadImport(value) {
        this.#fastLoadImport = value;
    }

    /**
     * Whether the document is free of change track info
     */
    isCTFreeDoc() {
        return this.#ctFreeDoc;
    }

    /**
     * Getting the global 'clipboardOperations'.
     *
     * @returns {Object[]}
     *  The list with operations in the clip board.
     */
    getClipboardOperations() {
        return clipboardOperations;
    }

    /**
     * Setting the global value of 'clipbardOperations'.
     *
     * @param {Object[]} ops
     *  The list with operations in the clip board.
     */
    setClipboardOperations(ops) {
        clipboardOperations = this.cleanClipboardOperations(ops);
    }

    /**
     * Setting the value for the global clipboardId.
     *
     * @param {String} value
     *  The new value for the global clipboard id.
     */
    setClipboardId(value) {
        clipboardId = value;
    }

    /**
     * Returns the value of global clipboardId.
     *
     * @returns {String}
     *  Returns the global clipboardId.
     */
    getClipboardId() {
        return clipboardId;
    }

    /**
     * Public helper function for calling the private function.
     * Clean up after the busy mode of a local client with edit prileges. After the
     * asynchronous operations were executed, the busy mode can be left.
     */
    leaveAsyncBusy() {
        return this.#leaveAsyncBusy();
    }

    /**
     * Public helper function.
     * Calculates the size of the image defined by the given url,
     * limiting the size to the paragraph size of the current selection.
     *
     * @param {String} url
     *  The image url or base64 data url
     *
     * @returns {jQuery.Promise}
     *  A promise where the resolve result contains the size object
     *  including width and height in Hmm {width: 123, height: 456}
     */
    getImageSize(url) {
        return this.#getImageSize(url);
    }

    /**
     * Public helper function.
     * Has to be called after changing the structure of a paragraph node, if
     * the update has to be done immediately (synchronously).
     *
     * @param {jQuery} paragraphs
     *  All paragraph nodes that need to be updated included into a
     *  jQuery object
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.suppressFontSize=false]
     *      If set to true, the dynamic font size calculation is NOT
     *      started during formatting of a complete slide.
     *  @param {Boolean} [options.restoreSelection=true]
     *      If set to false, the selection will not be restored via
     *      restoreBrowserSelection. This is useful, if the DOM was
     *      modfified without setting the new selection.
     */
    implParagraphChangedSync(paragraphs, options) {
        return this.#implParagraphChangedSync(paragraphs, options);
    }

    /**
     * Getting the object with the preselected attributes. This are attributes that
     * were set without a selection and are only set for a single character.
     *
     * @returns {Object}
     *  An object, that contains the preselected attributes.
     */
    getPreselectedAttributes() {
        return this.#preselectedAttributes;
    }

    /**
     * Whether the preselected attributes must not be set to null
     * (for example after splitParagraph).
     *
     * @returns {Boolean}
     *  Whether the preselected attributes must not be set to null.
     */
    keepPreselectedAttributes() {
        return this.#keepPreselectedAttributes;
    }

    /**
     * Setting the global value, whether the preselected attributes
     * must not be set to null (for example after splitParagraph).
     *
     * @param {Boolean} value
     *  Whether the preselected attributes must not be set to null.
     */
    setKeepPreselectedAttributes(value) {
        this.#keepPreselectedAttributes = value;
    }

    /**
     * Adding the explicit attributes of a specified family of a specified node
     * to the preselected attributes.
     *
     * @param {Node|jQuery|Number[]} node
     *  The node (mostly paragraph node), whose descendants are investigated.
     *
     * @param {Number|Number[]} start
     *  The number or complete logical position to find the child node relative to
     *  the specified node.
     *
     * @param {String} family
     *  The family for the explicit attributes. Only the attributes of the specified
     *  family are saved in the explicit attributes.
     */
    addCharacterAttributesToPreselectedAttributes(node, start, family) {

        // the dom point at the specified position
        const domPos = getDOMPosition(node, _.isNumber(start) ? [start] : start, true);
        // the explicit attributes of the specified child node
        let attrs = null;
        // the attributes of the specified family
        let familyAttrs = null;

        if (domPos && domPos.node) {

            attrs = getExplicitAttributes(domPos.node, family);

            if (attrs) {
                familyAttrs = {};
                familyAttrs[family] = attrs;
                this.addPreselectedAttributes(familyAttrs);
            }
        }

    }

    /**
     * Adds the passed attributes to the set of preselected attributes that
     * will be applied to the next inserted text contents without moving
     * the text cursor or executing any other action.
     *
     * @param {Object} attributes
     *  A character attribute set that will be added to the set of
     *  preselected attributes.
     *
     * @returns {Editor}
     *  A reference to this instance.
     */
    addPreselectedAttributes(attributes) {
        this.#preselectedAttributes = this.#preselectedAttributes || {};
        this.defAttrPool.extendAttrSet(this.#preselectedAttributes, attributes);
        return this;
    }

    /**
     * Whether local operations are generated and applied for the header/footer without
     * entering edit mode of header/footer.
     *
     * @returns {Boolean}
     *  Returns whether local operations are generated and applied for the header/footer
     *  without entering edit mode of header/footer.
     */
    isInactiveLocalHeaderFooterOp() {
        return this.#isInactiveLocalHeaderFooterOp;
    }

    /**
     * Setting that local operations are generated and applied for the header/footer without
     * entering edit mode of header/footer.
     *
     * @param {Boolean} value
     *  The state for generating and applying local operations for the header/footer without
     *  entering edit mode of header/footer.
     */
    setInactiveLocalHeaderFooterOp(value) {
        this.#isInactiveLocalHeaderFooterOp = value;
    }

    /**
     * Whether undo or redo operations are currently running.
     *
     * @returns {Boolean}
     *  Returns whether undo or redo operations are currently running.
     */
    isUndoRedoRunning() {
        return this.#undoRedoRunning;
    }

    /**
     * Whether an operation is part of an undo group.
     *
     * @returns {Boolean}
     *  Returns whether an operation is part of an undo group.
     */
    isInUndoGroup() {
        return this.#isInUndoGroup;
    }

    /**
     * Setting the promise that controls an already active (deferred) text input.
     *
     * @param {Object|Null} value
     *  The new value for the global this.#inputTextPromise.
     */
    setInputTextPromise(value) {
        this.#inputTextPromise = value;
    }

    /**
     * Getting the promise that controls an already active (deferred) text input.
     *
     * @returns {Object|Null}
     *  The global this.#inputTextPromise.
     */
    getInputTextPromise() {
        return this.#inputTextPromise;
    }

    /**
     * Whether the paragraph cache can be used.
     *
     * @returns {Boolean}
     *  Returns whether the paragraph cache can be used.
     */
    useParagraphCache() {
        return this.#useParagraphCache;
    }

    /**
     * Setting, whether the paragraph cache can be used.
     *
     * @param {Boolean} value
     *  Whether the paragraph cache can be used.
     */
    setUseParagraphCache(value) {
        this.#useParagraphCache = value;
    }

    /**
     * Returns the content of the paragraph cache. Or null, if there is no content. It
     * is important to return the node, because it might be assigned directly.
     *
     * @returns {Node|Null}
     *  Returns the content of the paragraph cache. Or null, if there is no content.
     */
    getParagraphCache() {
        return this.#paragraphCache;
    }

    /**
     * Setting the paragraph cache node.
     *
     * @param {Node|Null} node
     *  Setting the content of the paragraph cache.
     */
    setParagraphCache(node) {
        this.#paragraphCache = node;
    }

    /**
     * Setting the global variable this.#lastOperationEnd. This is necessary,
     * because more and more operation handlers are handled in external
     * modules and they need to set the end of their operation.
     *
     * @param {Number[]} pos
     *  The logical position.
     */
    setLastOperationEnd(pos) {
        this.#lastOperationEnd = pos;
    }

    /**
     * Getting the global variable this.#lastOperationEnd. This is necessary,
     * because more and more operation are generated in external
     * modules and they need to set the selection after applying the
     * operations.
     *
     * @returns {Array<Number>}
     *  The logical position.
     */
    getLastOperationEnd() {
        return this.#lastOperationEnd;
    }

    /**
     * Setting the key down event.
     *
     * @param {jQuery.Event} event
     *  The key down event.
     */
    setLastKeyDownEvent(event) {
        this.#lastKeyDownEvent = event;
    }

    /**
     * Getting the keydown event.
     *
     * @returns {?jQuery.Event}
     *  The keydown event.
     */
    getLastKeyDownEvent() {
        return this.#lastKeyDownEvent;
    }

    /**
     * Setting the last key code from key down event.
     *
     * @param {Number} keyCode
     *  The key code number.
     */
    setLastKeyDownKeyCode(keyCode) {
        this.#lastKeyDownKeyCode = keyCode;
    }

    /**
     * Getting the last key code from key down event.
     *
     * @returns {Number}
     *  The key code number.
     */
    getLastKeyDownKeyCode() {
        return this.#lastKeyDownKeyCode;
    }

    /**
     * Setting that tab handling is active or not active.
     *
     * @param {Boolean} value
     *  Whether a tab key down event is handled or not.
     */
    setKeyDownTabHandling(value) {
        this.#isKeyDownTabHandling = value;
    }

    /**
     * Returns whether a tab key down event is handled or not.
     *
     * @returns {Number}
     *   Whether a tab key down event is handled or not.
     */
    isKeyDownTabHandling() {
        return this.#isKeyDownTabHandling;
    }

    /**
     * Setting the last key modifiers from the key down event
     *
     * @param {Boolean} ctrlKey
     *  The state of the ctrl key
     *
     * @param {Boolean} shiftKey
     *  The state of the shift key
     *
     * @param {Boolean} altKey
     *  The tate of the alt key
     *
     * @param {Boolean} metaKey
     *  The state of the meta key
     */
    setLastKeyDownModifiers(ctrlKey, shiftKey, altKey, metaKey) {
        this.#lastKeyDownModifiers = { ctrlKey, shiftKey, altKey, metaKey };
    }

    /*
        * Retrieves the last modifiers from the key down event
        *
        * @returns {Object}
        *  The key modifiers as stored in the last key down event
        */
    getLastKeyDownModifiers() {
        return this.#lastKeyDownModifiers;
    }

    /**
     * Setting the repeated key down flag.
     *
     * @param {Boolean} value
     *  The flag for repeated key down.
     */
    setRepeatedKeyDown(value) {
        this.#repeatedKeyDown = value;
    }

    /**
     * Whether the key down event was triggered several times
     * without keypress event in between (46659).
     *
     * @returns {Boolean}
     *  Whether the key down event was triggered several times
     *  without keypress event.
     */
    isRepeatedKeyDown() {
        return this.#repeatedKeyDown;
    }

    /**
     * Returns whether the undo stack must be deleted. The value is saved
     * in the global 'this.#deleteUndoStack'.
     *
     * @returns {Boolean}
     *  Returns whether the undo stack must be deleted.
     */
    deleteUndoStack() {
        return this.#deleteUndoStack;
    }

    /**
     * Setting that the undo stack must be deleted. The value is saved
     * in the global 'this.#deleteUndoStack'.
     *
     * @param {Boolean} value
     *  Whether the undo stack must be deleted.
     */
    setDeleteUndoStack(value) {
        this.#deleteUndoStack = value;
    }

    /**
     * Getting the list of artificially highlighted paragraph nodes.
     *
     * @returns {HTMLElement[]}
     *  The list of artificially highlighted paragraphs.
     */
    getHighlightedParagraphs() {
        return this.#highlightedParagraphs;
    }

    /**
     * Getting the list of artificially highlighted cell nodes.
     *
     * @returns {HTMLElement[]}
     *  The list of artificially highlighted cells.
     */
    getHighlightedCells() {
        return this.#highlightedCells;
    }

    /**
     * Getting the implicit paragraph node with increased height.
     *
     * @returns {HTMLElement}
     *  The implicit paragraph node with increase height.
     */
    getIncreasedParagraphNode() {
        return this.#increasedParagraphNode;
    }

    /**
     * Setting the global implicit paragraph node with increase height.
     *
     * @param {HTMLElement} node
     *  The implicit paragraph node with increase height.
     */
    setIncreasedParagraphNode(node) {
        this.#increasedParagraphNode = node;
    }

    /**
     * Setting the global pageAttributes.
     *
     * @param {Object} value
     *  The page attributes object.
     */
    setPageAttributes(value) {
        this.#pageAttributes = value;
    }

    /**
     * Getting the global pageAttributes.
     *
     * @returns {Object}
     *  The page attributes object.
     */
    getPageAttributes() {
        return this.#pageAttributes;
    }

    /**
     * Setting the maximum page height.
     *
     * @param {Number} _value
     *  The maximum page height.
     */
    setPageMaxHeight(_value) {
        //pageMaxHeight = value;
    }

    /**
     * Setting the page's left padding.
     *
     * @param {Number} value
     *  The page's left padding.
     */
    setPagePaddingLeft(value) {
        this.#pagePaddingLeft = value;
    }

    /**
     * Getting the page's left padding.
     *
     * @returns {Number}
     *  The page's left padding.
     */
    getPagePaddingLeft() {
        return this.#pagePaddingLeft;
    }

    /**
     * Setting the page's top padding.
     *
     * @param {Number} _value
     *  The page's top padding.
     */
    setPagePaddingTop(_value) {
        //pagePaddingTop = value;
    }

    /**
     * Setting the page's bottom padding.
     *
     * @param {Number} _value
     *  The page's bottom padding.
     */
    setPagePaddingBottom(_value) {
        //pagePaddingBottom = value;
    }

    /**
     * Setting the page's width.
     *
     * @param {Number} value
     *  The page's width.
     */
    setPageWidth(value) {
        this.#pageWidth = value;
    }

    /**
     * Getting the page's width.
     *
     * @returns {Number}
     *  The page's width.
     */
    getPageWidth() {
        return this.#pageWidth;
    }

    /**
     * Specifying whether the minimum time for a valid move operation shall be ignored.
     * This minimum time is checked, to distinguish move operations from only selecting
     * drawing. This check must not be done in unit tests.
     *
     * @param {Boolean} value
     *  Setting, if the minimum move time shall be ignored.
     */
    setIgnoreMinMoveTime(value) {
        this.#ignoreMinMoveTime = value;
    }

    /**
     * Returning whether the minimum move time shall be ignored. This minimum time is checked,
     * to distinguish move operations from only selecting drawing. This check must not be done
     * in unit tests.
     *
     * @returns {Boolean}
     *  Whether the minimum move time shall be ignored.
     */
    ignoreMinMoveTime() {
        return this.#ignoreMinMoveTime;
    }

    /**
     * Increasing the global 'modifyingCounter' by 1.
     */
    increaseModifyingCounter() {
        this.#modifiyingCounter++;
    }

    /**
     * Decreasing the global 'modifyingCounter' by 1.
     */
    decreaseModifyingCounter() {
        this.#modifiyingCounter--;
    }

    /**
     * Getter for the global 'modifyingCounter'. If its value is greater than 0, the operations
     * that are sent to the backend get in #finalizeOperations a marker 'modifying: false', so
     * that the backend does not generate a new file version (DOCS-2144).
     *
     * @returns {Number}
     *  The current value of the global 'modifyingCounter'.
     */
    getModifyingCounter() {
        return this.#modifiyingCounter;
    }

    /**
     * Getter for the global this.#doubleClickEventWaiting.
     *
     * @returns {Boolean}
     *  The global variable this.#doubleClickEventWaiting.
     */
    getDoubleClickEventWaiting() {
        return this.#doubleClickEventWaiting;
    }

    /**
     * Setter for the global this.#tripleClickActive.
     *
     * @param {Boolean} value
     *  The new value for the global variable this.#tripleClickActive.
     */
    setTripleClickActive(value) {
        this.#tripleClickActive = value;
    }

    /**
     * Setter for the global this.#activeMouseDownEvent.
     *
     * @param {Boolean} value
     *  The new value for the global variable this.#activeMouseDownEvent.
     */
    setActiveMouseDownEvent(value) {
        this.#activeMouseDownEvent = value;
    }

    /**
     * Getter for the global this.#activeMouseDownEvent.
     *
     * @returns {Boolean} value
     *  Whether the user currently has mousedown pressed and mouseup
     *  does not happen yet.
     */
    isActiveMouseDownEvent() {
        return this.#activeMouseDownEvent;
    }

    /**
     * Setter for the global this.#activeDoubleClickEvent.
     *
     * @param {Boolean} value
     *  The new value for the global variable this.#activeDoubleClickEvent.
     */
    setActiveDoubleClickEvent(value) {
        this.#activeDoubleClickEvent = value;
    }

    /**
     * Getter for the global this.#activeDoubleClickEvent.
     *
     * @returns {Boolean} value
     *  Whether the user has double clicked on the document.
     */
    isActiveDoubleClickEvent() {
        return this.#activeDoubleClickEvent;
    }

    /**
     * Setter for the global this.#ignoreFollowingMouseUpEventHandler.
     */
    ignoreFollowingMouseUpEventHandler() {
        this.#ignoreFollowingMouseUpEventHandler = true;
    }

    /**
     * Setter for the global 'this.#mouseDownInCommentsPane'.
     *
     * @param {Boolean} value
     *  The new value for the global variable this.#mouseDownInCommentsPane.
     */
    setMouseDownInCommentsPane(value) {
        this.#mouseDownInCommentsPane = value;
    }

    /**
     * Setter for the global 'this.#mouseDownInCommentsBubbleNode'.
     *
     * @param {Boolean} value
     *  The new value for the global variable this.#mouseDownInCommentsBubbleNode.
     */
    setMouseDownInCommentsBubbleNode(value) {
        this.#mouseDownInCommentsBubbleNode = value;
    }

    /**
     * Setter for the global 'this.#ignoreClickEvent'.
     *
     * @param {Boolean} value
     *  The new value for the global variable this.#ignoreClickEvent.
     */
    ignoreClickEvent(value) {
        this.#ignoreClickEvent = value;
    }

    /**
     * Getter for the global 'this.#ignoreClickEvent'.
     *
     * @returns {Boolean}
     *  The value of the global variable this.#ignoreClickEvent.
     */
    isIgnoreClickEvent() {
        return this.#ignoreClickEvent;
    }

    // public helper functions that are required by the presentation application
    // TODO: This functions need to be removed, if presentation and text share common base

    validateParagraphNode(element) {
        return this.#validateParagraphNode(element);
    }

    implParagraphChanged(nodes) {
        return this.#implParagraphChanged(nodes);
    }

    /**
     * Debounced function for updating registered drawings.
     *
     * @param {HTMLElement|jQuery} nodes
     *  The drawing nodes to be registered for debounced formatting.
     */
    updateDrawingsDebounced(nodes) {
        return this.#updateDrawingsDebounced(nodes);
    }

    implTableChanged(nodes) {
        return this.#implTableChanged(nodes);
    }

    checkChangesMode(attrs) {
        return this.#checkChangesMode(attrs);
    }

    handleTriggeringListUpdate(node, options) {
        return this.#handleTriggeringListUpdate(node, options);
    }

    insertContentNode(position, node, target) {
        return this.#insertContentNode(position, node, target);
    }

    deletePageLayout() {
        this.#pageLayout.insertPageBreaks = $.noop;
    }

    disableDrawingLayer() {
        this.#drawingLayer.stopUpdatingAbsoluteElements();
    }

    getParagraphCacheOperation() {
        return PARAGRAPH_CACHE_OPERATIONS;
    }

    getMaxTopLevelNodes() {
        return this.#MAX_TOP_LEVEL_NODES;
    }

    applyTextOperationsAsync(generator, label, options) {
        return this.#applyTextOperationsAsync(generator, label, options);
    }

    getRoIOSParagraph() {
        return this.#roIOSParagraph;
    }

    setRoIOSParagraph(value) {
        this.#roIOSParagraph = value;
    }

    isImeActive() {
        return this.#imeActive;
    }

    dumpEventObject(event) {
        return this.#dumpEventObject(event);
    }

    // end of functions introduced for presentation app

    /**
     * Returns true if a clipboard paste is still in progress, otherwise false.
     * And then sets the state to true.
     * and the processing of this paste
     *
     * @returns {Boolean}
     *  Wether a clipboard paste is in progress
     */
    checkSetClipboardPasteInProgress() {
        const state = this.#pasteInProgress;
        this.#pasteInProgress = true;
        return state;
    }

    /**
     * Removing the artificial selection of empty paragraphs
     */
    removeArtificalHighlighting() {
        _.each(this.#highlightedParagraphs, element => { $(element).removeClass('selectionHighlighting'); });
        this.#highlightedParagraphs = [];
        _.each(this.#highlightedCells, element => { $(element).removeClass('cellSelectionHighlighting'); });
        this.#highlightedCells = [];
    }

    /**
     * Creates and returns an implicit paragraph node, that can be directly
     * included into the dom (validated and updated).
     *
     * @param {Object} [paraAttrs]
     *  Optional attributes that can be assigned to the implicit paragraph.
     *
     * @returns {HTMLElement}
     *  The implicit paragraph
     */
    getValidImplicitParagraphNode(paraAttrs) {
        const paragraph = DOM.createImplicitParagraphNode();
        if (paraAttrs) { this.paragraphStyles.setElementAttributes(paragraph, paraAttrs); }
        this.#validateParagraphNode(paragraph);
        this.paragraphStyles.updateElementFormatting(paragraph);
        return paragraph;
    }

    /**
     * Get the list of all known authors
     *
     * @returns {Array}
     *  The author array.
     */
    getAuthorsList() {
        return this.#authorsList;
    }

    /**
     * Sets the list of all known authors
     *
     * @param {Array} authors
     *  A new list of all authors
     */
    setAuthorsList(authors) {
        this.#authorsList = authors;
    }

    /**
     * Prepares a 'real' paragraph after insertion of text, tab, drawing, ...
     * by exchanging an 'implicit' paragraph (in empty documents, empty cells,
     * behind tables, ...) with the help of an operation. Therefore the server is
     * always informed about creation and removal of paragraphs and implicit
     * paragraphs are only required for user input in the browser.
     *
     * @param {Number[]} position
     *  The logical text position.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.ignoreLength=false]
     *      If set to true, no check for the paragraph length is done. Normally
     *      a implicit paragraph must be empty. In special cases this check
     *      must be omitted.
     */
    doCheckImplicitParagraph(position, options) {
        this.#handleImplicitParagraph(position, options);
    }

    /**
     * Public API function for the private function with the same name.
     * Shows a warning dialog with Yes/No buttons before deleting document
     * contents that cannot be restored.
     *
     * @param {String} title
     *  The title of the dialog box.
     *
     * @param {String} message
     *  The message text shown in the dialog box.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the Yes button has been pressed,
     *  or rejected if the No button has been pressed.
     */
    showDeleteWarningDialog(title, message) {

        // disable modifications of div.page node (bug 36435)
        this.#editdiv.attr('contenteditable', false);

        // show the query dialog
        const promise = this.docApp.docView.showQueryDialog(title, message, { width: 650 });

        // enable modifications of div.page node (bug 36435)
        return promise.always(() => { this.#editdiv.attr('contenteditable', true); });
    }

    /**
     * Check, wheter a given paragraph style id is adding list style information
     * to a paragraph (bullet or numbering list).
     *
     * @param {String} styleId
     *  The paragraph style id, that will be checked for a list style id.
     *
     * @returns {Boolean}
     *  Whether the specified styleId contains a list style id.
     */
    isParagraphStyleWithListStyle(styleId) {
        const styleAttrs = this.paragraphStyles.getStyleSheetAttributeMap(styleId);
        return !!(styleAttrs?.paragraph && (styleAttrs.paragraph.listStyleId || styleAttrs.paragraph.listLevel));
    }

    /**
     * Check, wheter a given paragraph node describes a paragraph that
     * is part of a list (bullet or numbering list).
     *
     * @param {HTMLElement|jQuery} node
     *  The paragraph whose attributes will be checked. If this object is a
     *  jQuery collection, uses the first DOM node it contains.
     *
     * @param {Object} [attributes]
     *  An optional map of attribute maps (name/value pairs), keyed by attribute.
     *  It this parameter is defined, the parameter 'paragraph' can be omitted.
     *
     * @returns {Boolean}
     *  Whether the content node is a list paragraph node.
     */
    isListStyleParagraph(node, attributes) {
        return this.#isListStyleParagraph(node, attributes);
    }

    // dumps profiling information to browser console
    dumpProfilingInformation() {

        if (!DEBUG) { return; }

        const  dumpCount = (nodeKey, countObject, actionVerb) => {
            const nodeCount = nodeCountMap[nodeKey];
            const callCount = countObject.DBG_COUNT || 0;
            if (nodeCount || callCount) {
                let nodeName = nodeKey.replace('_', ' ');
                if (nodeCount > 1) { nodeName += 's'; }
                let message = nodeCount + ' ' + nodeName + ' ' + actionVerb + ' ' + callCount + ' times';
                if (nodeCount && callCount) { message += ' (' + math.roundp(callCount / nodeCount, 0.01) + ' per node)'; }
                this.docApp.launchTracker.info('$badge{Editor} updateDocumentFormatting: ' + message);
            }
        };

        const nodeCountMap = {
            text_span: this.#editdiv.find('span').filter((_idx, oneSpan) => { return DOM.isPortionSpan(oneSpan) || DOM.isTextComponentNode(oneSpan.parentNode); }).length,
            paragraph: this.#editdiv.find(DOM.PARAGRAPH_NODE_SELECTOR).length,
            table: this.#editdiv.find(DOM.TABLE_NODE_SELECTOR).length,
            table_cell: this.#editdiv.find('td').length,
            drawing: this.#editdiv.find(DrawingFrame.NODE_SELECTOR).length
        };

        dumpCount('text_span', this.characterStyles, 'formatted');
        dumpCount('paragraph', this.paragraphStyles, 'formatted');
        dumpCount('table', this.tableStyles, 'formatted');
        dumpCount('table_cell', this.tableCellStyles, 'formatted');
        dumpCount('drawing', this.drawingStyles, 'formatted');
        dumpCount('paragraph', this, 'validated');
    }

    /**
     * collects all attrs for assigned styleId out of family-stylecollection
     * and writes all data into the assigned generator
     */
    generateInsertStyleOp(generator, family, styleId, setDefault) {

        const styleCollection = this.getStyleCollection(family);
        const parentId = styleCollection.getParentId(styleId);
        const uiPriority = styleCollection.getUIPriority(styleId);
        const hidden = styleCollection.isHidden(styleId);

        const operProps = {
            type: family,
            styleId,
            styleName: styleCollection.getName(styleId),
            attrs: styleCollection.getStyleSheetAttributeMap(styleId)
        };

        if (parentId) { operProps.parent = parentId; }
        if (_.isNumber(uiPriority)) { operProps.uiPriority = uiPriority; }
        if (hidden === true) { operProps.hidden = true; }
        if (setDefault === true) { operProps.default = true; }

        // adding property 'fallbackValue' to themed colors inside the specified attributes
        this.addFallbackValueToThemeColors(operProps.attrs);

        generator.generateOperation(Op.INSERT_STYLESHEET, operProps);
        styleCollection.setDirty(styleId, false);
    }

    /**
     * Returns whether the internal clipboard contains operations.
     */
    hasInternalClipboard() {
        return clipboardOperations.length > 0;
    }

    /**
     * Returning a list of objects describing the extensions that are necessary
     * to load a document from the local storage.
     * One object can contain the following properties:
     *  extension: This is the required string extension, that is used to save
     *      one layer (page content, drawing layer, ...) in the local
     *      storage.
     *  optional:  Whether this is an optional value from local storage (like
     *      drawing layer) or a required value (like page content).
     *  additionalOptions: Some additional options that can be set to influence
     *      the loading process.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.allApps=false]
     *      If set to true the local storage extensions of all apps are collected.
     *      Default is false, so that only application specific extensions are
     *      returned.
     *
     * @returns {Object[]}
     *  An array with objects describing the strings that are used in the local
     *  storage.
     */
    getSupportedStorageExtensions(options) {

        // the list of all supported extensions for the local registry
        const allExtensions = [];
        // whether the list shall contain all extensions of all apps
        const allApps = getBooleanOption(options, 'allApps', false);

        // the page content layer
        allExtensions.push({ extension: '', optional: 'false' });

        if (this.docApp.isTextApp() || allApps) {
            // the drawing layer
            allExtensions.push({ extension: '_DL', optional: 'true', additionalOptions: { drawingLayer: true } });
            // header&footer layer
            allExtensions.push({ extension: '_HFP', optional: 'true', additionalOptions: { headerFooterLayer: true } });
            // the comment layer
            allExtensions.push({ extension: '_CL', optional: 'true', additionalOptions: { commentLayer: true } });
        }

        if (this.docApp.isPresentationApp() || allApps) {
            // the master slide layer
            if (!this.docApp.isODF()) { allExtensions.push({ extension: '_MSL', optional: 'false', additionalOptions: { masterSlideLayer: true } }); }
            // the layout slide layer
            allExtensions.push({ extension: '_LSL', optional: 'false', additionalOptions: { layoutSlideLayer: true } });
        }

        return allExtensions;
    }

    // ==================================================================
    // HIGH LEVEL EDITOR API which finally results in Operations
    // and creates Undo Actions.
    // Public functions, that are called from outside the editor
    // and generate operations.
    // ==================================================================

    /**
     * special flag for the very slow android chrome
     */
    isAndroidTyping() {
        return this.#androidTyping;
    }

    /**
     *
     */
    handleAndroidTyping() {
        this.#androidTyping = true;
        if (this.#androidTimeouter) {
            window.clearTimeout(this.#androidTimeouter);
        }
        this.#androidTimeouter = this.executeDelayed(() => {
            this.#androidTyping = false;
        }, 1000);
    }

    /**
     * Inserting a drawing group node into the document. This group can be
     * used as container for drawing frames
     * TODO: This code cannot be triggered via GUI yet. It is currently ony
     * available for testing reasons.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the dialog asking the user, if
     *  he wants to delete the selected content (if any) has been closed
     *  with the default action; or rejected, if the dialog has been
     *  canceled. If no dialog is shown, the promise is resolved
     *  immediately.
     */
    insertDrawingGroup() {

        // whether the text frame is included into a drawing group node
        let insertIntoGroup = false;
        // the position of the text frame inside a group node
        let groupPosition = 0;
        // helper nodes
        let startNodeInfo = null, contentNode = null;

        // the undo manager returns the return value of the callback function
        return this.#undoManager.enterUndoGroup(() => {

            // helper function, that generates the operations
            const doInsertDrawingGroup = () => {

                // the operations generator
                const generator = this.createOperationGenerator();
                const start = this.#selection.getStartPosition();
                const attrs = { drawing: { width: 6000, height: 3000 } };

                // special code for inserting into group
                if (insertIntoGroup) {
                    start.push(groupPosition);  // adding the position inside the group -> TODO: Remove asap
                } else {
                    this.#handleImplicitParagraph(start);
                }

                // modifying the attributes, if changeTracking is activated
                if (this.#changeTrack.isActiveChangeTracking()) {
                    attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
                }

                // text frames are drawings of type shape
                generator.generateOperation(Op.INSERT_DRAWING, { attrs, start: _.copy(start), type: 'group' });

                // apply all collected operations
                this.applyOperations(generator);
            };

            // -> integrating a text frame into a drawing frame of type 'group' by selecting the drawing group
            if (this.#selection.hasRange() && this.#selection.isDrawingFrameSelection()) {
                startNodeInfo = getDOMPosition(this.#editdiv, this.#selection.getStartPosition(), true);
                if (startNodeInfo && startNodeInfo.node && DrawingFrame.isGroupDrawingFrame(startNodeInfo.node)) {
                    contentNode = $(startNodeInfo.node).children().first();
                    // checking the number of children inside the content node
                    groupPosition = DrawingFrame.isPlaceHolderNode(contentNode) ? 0 : contentNode.children().length;
                    insertIntoGroup = true;
                }
            }

            if (!insertIntoGroup && this.#selection.hasRange()) {
                return this.deleteSelected()
                    .done(() => {
                        doInsertDrawingGroup();
                    });
            }

            doInsertDrawingGroup();
            return $.when();

        }, this);  // enterUndoGroup()
    }

    /**
     * Handling the property 'aspectLocked' of selected drawing nodes.
     * The handling from drawings in a single selection and a multiselection is different.
     *
     * @param {Boolean} state
     *  Whether the property 'aspectLocked' of selected drawings shall be
     *  enabled or disabled.
     */
    handleDrawingLockRatio(state) {

        // the operations generator
        const generator = this.createOperationGenerator();
        // the options for the setAttributes operation
        const operationOptions = {};
        // selected drawing nodes
        let drawingNodes = null;

        // multiselection
        if (this.#selection.isMultiSelectionSupported() && this.#selection.isMultiSelection()) {

            _.each(this.#selection.getMultiSelection(), drawingItem => {

                // collecting the attributes for the operation
                operationOptions.attrs =  {};
                operationOptions.attrs.drawing = { aspectLocked: state };
                operationOptions.start = _.clone(drawingItem.startPosition);

                // generate the 'setAttributes' operation
                generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);
            });

            drawingNodes = this.#selection.getAllSelectedDrawingNodes();

        // normal selection
        } else {

            drawingNodes = this.#selection.getAnyDrawingSelection();

            // collecting the attributes for the operation
            operationOptions.attrs =  {};
            operationOptions.attrs.drawing = { aspectLocked: state };

            if (this.#selection.isAdditionalTextframeSelection()) {
                operationOptions.start = getOxoPosition(this.getCurrentRootNode(), drawingNodes, 0);
            } else {
                operationOptions.start = this.#selection.getStartPosition();
            }

            // generate the 'setAttributes' operation
            generator.generateOperation(Op.SET_ATTRIBUTES, operationOptions);

            drawingNodes = [drawingNodes];

        }
        // apply all collected operations
        this.applyOperations(generator);

        // refreshing the drawing selection
        _.each(drawingNodes, drawing => {
            DrawingFrame.clearSelection(drawing);
            this.#selection.drawDrawingSelection(drawing);
        });

    }

    /**
     * Checking whether currently selected drawing nodes have 'aspectLocked' enabled or not.
     * The handling from drawings in a single selection and a multiselection is different.
     *
     * @returns {Boolean}
     *  Whether a currently selected drawing node has 'aspectLocked' enabled. If selected drawings are in a
     *  a multiselection and have different states, null is returned as a flag for an ambiguous state.
     */
    isLockedRatioDrawing() {

        // whether the selected drawing has a locked aspect ratio
        let lockState = false;
        // if a drawing with an locked ratio is found in a multiselection
        let foundLockedDrawings = false;
        // if a drawing with an unlocked ratio is found in a multiselection
        let foundUnlockedDrawings = false;
        // attribute from the current node
        let attrs;

        // multiselection: check all nodes in the selection for the 'aspectLocked' attribute to compute the 'lockState' later
        if (this.#selection.isMultiSelectionSupported() && this.#selection.isMultiSelection()) {
            _.each(this.#selection.getMultiSelection(), drawingItem => {

                attrs = getExplicitAttributeSet(drawingItem.drawing);

                // exist check
                if (attrs && attrs.drawing) {
                    if (attrs.drawing.aspectLocked) {
                        foundLockedDrawings = true;
                    } else {
                        foundUnlockedDrawings = true;
                    }
                }
            });

            // compute lockState for multiselection:
            // (1) there are locked and unlocked items,
            // (2) only locked items
            // (3) only unlocked items
            if (foundLockedDrawings && foundUnlockedDrawings) {
                // 'ambiguous' state, check the checkbox element for details
                lockState = null;
            } else if (foundLockedDrawings) {
                lockState = foundLockedDrawings;
            } else if (foundUnlockedDrawings) {
                lockState = false;
            }

        // normal selection
        } else {

            attrs = getExplicitAttributeSet(this.#selection.getAnyDrawingSelection());
            if (attrs && attrs.drawing && attrs.drawing.aspectLocked) { lockState = true; }

        }

        return lockState;
    }

    /**
     * Checking whether a currently selected text frame node or a specified drawing node has
     * 'autoResizeHeight' enabled or not.
     *
     * @param {Node|jQuery} [drawingNode]
     *  An optional drawing node, that shall be investigated. If not specified, a selected
     *  drawing node is used instead.
     *
     * @returns {Boolean}
     *  Whether a a currently selected text frame node has 'autoResizeHeight' enabled. If no
     *  text frame is selected or specified, false is returned.
     */
    isAutoResizableTextFrame(drawingNode) {

        // the selected text frame node (also finding text frames inside groups)
        const textFrame = drawingNode || this.#selection.getAnyTextFrameDrawing({ forceTextFrame: true });

        return !!textFrame && (textFrame.length > 0) && DrawingFrame.isAutoResizeHeightDrawingFrame(textFrame);
    }

    /**
     * Checking whether a currently selected text frame node or a specified drawing node has
     * 'autoResizeText' enabled or not.
     *
     * @param {Node|jQuery} [drawingNode]
     *  An optional drawing node, that shall be investigated. If not specified, a selected
     *  drawing node is used instead.
     *
     * @returns {Boolean}
     *  Whether a a currently selected text frame node has 'autoResizeText' enabled. If no
     *  text frame is selected or specified, false is returned.
     */
    isAutoTextHeightTextFrame(drawingNode) {

        // the selected text frame node (also finding text frames inside groups)
        const textFrame = drawingNode || this.#selection.getAnyTextFrameDrawing({ forceTextFrame: true });

        return !!textFrame && (textFrame.length > 0) && DrawingFrame.isAutoTextHeightDrawingFrame(textFrame);
    }

    /**
     * Getting the auto resize state for the selected drawing(s). Allowed values are 'autofit',
     * 'autotextheight' or 'noautofit'.
     *
     * @returns {String|Null}
     *  The auto resize state of the selected drawing(s). If no unique state can be determined,
     *  Null is returned.
     */
    getDrawingAutoResizeState() {

        // the auto resize state of the selected drawings
        let state = null;
        // a collector for all resize types of the selected drawings
        let allResizeTypes = null;
        // one resize type of one selected drawing
        let oneType = null;
        // a selected drawing node
        let selectedDrawing = null;

        // helper function to receive the resize state for one specified drawing
        const handleOneDrawingState = drawingNode => {
            const node = $(drawingNode);
            return this.isAutoResizableTextFrame(node) ? 'autofit' : (this.isAutoTextHeightTextFrame(node) ? 'autotextheight' : 'noautofit');
        };

        if (this.#selection.isMultiSelection()) {
            allResizeTypes = {};
            _.each(this.#selection.getAllSelectedDrawingNodes(), oneDrawing => {
                if (DrawingFrame.isTextFrameShapeDrawingFrame(oneDrawing)) { // only checking valid drawings. TODO: Groups
                    oneType = handleOneDrawingState(oneDrawing);
                    allResizeTypes[oneType] = 1;
                } else if (DrawingFrame.isGroupDrawingFrameWithShape(oneDrawing)) {
                    _.each(DrawingFrame.getAllGroupDrawingChildren(oneDrawing), child => {
                        if (DrawingFrame.isTextFrameShapeDrawingFrame(child)) {
                            oneType = handleOneDrawingState(child);
                            allResizeTypes[oneType] = 1;
                        }
                    });
                }
            });
            state = (_.keys(allResizeTypes).length === 1) ? oneType : null;
        } else {

            selectedDrawing = this.#selection.getSelectedDrawing();

            if (selectedDrawing.length  > 0 && DrawingFrame.isGroupDrawingFrame(selectedDrawing)) { // the group drawing itself is selected
                allResizeTypes = {};
                _.each(DrawingFrame.getAllGroupDrawingChildren(selectedDrawing), oneDrawing => {
                    if (DrawingFrame.isTextFrameShapeDrawingFrame(oneDrawing)) {
                        oneType = handleOneDrawingState(oneDrawing);
                        allResizeTypes[oneType] = 1;
                    }
                });
                state = (_.keys(allResizeTypes).length === 1) ? oneType : null;
            } else {
                state = handleOneDrawingState(this.#selection.getAnyTextFrameDrawing({ forceTextFrame: true }));
            }

        }

        return state;
    }

    /**
     * Adding a default template text into a specified drawing, if the drawing is
     * an empty text frame and a template text is available.
     *
     * @param {Node|jQuery} drawing
     *  The DOM node to be checked. If this object is a jQuery collection, uses
     *  the first DOM node it contains.
     *
     * @param {Object} [options]
     *  Some additional options that are supported by the model function 'getDefaultTextForTextFrame'.
     *  Additionally supported:
     *  @param {Boolean} [options.ignoreTemplateText=false]
     *      This option is handled within DOM.isEmptyTextFrame to define, that text frames
     *      that contain template text are also empty.
     *
     * @returns {Boolean}
     *  Whether the default template text was inserted into the drawing.
     */
    addTemplateTextIntoTextSpan(drawing, options) {

        // the text span inside an empty text frame
        let textSpan = null;
        // the paragraph node
        let paragraph = null;
        // the default text
        let text = null;
        // whether the default text was inserted
        let insertedText = false;

        if (this.getDefaultTextForTextFrame && DOM.isEmptyTextframe(drawing, options)) {

            text = this.getDefaultTextForTextFrame(drawing, options); // this is application specific

            if (text) {
                textSpan = $(drawing).find('div.p > span');
                textSpan.text(text);
                textSpan.addClass(DOM.TEMPLATE_TEXT_CLASS);
                paragraph = textSpan.parent();
                paragraph.removeClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS); // removing the marker for empty paragraphs (if it was set)
                paragraph.addClass(DOM.NO_SPELLCHECK_CLASSNAME); // setting marker for avoiding spell checking of template text
                insertedText = true;
            }
        }

        return insertedText;
    }

    /**
     * Removing template text in a text frame, if there is the default
     * text active inside the text frame.
     *
     * @param {Node|jQuery} oldTextNode
     *  The text node or the text span that will be checked, if it contains
     *  the default text.
     *
     * @returns {Node|Null}
     *  The new empty text node, if the old text node was removed. Otherwise
     *  null is returned.
     */
    removeTemplateTextFromTextSpan(oldTextNode) {

        // the new text node in the span
        let newTextNode = null;
        // the dom node
        const domNode = getDomNode(oldTextNode);
        // whether the parameter oldTextNode is a text node (not a span)
        const isTextNode = domNode.nodeType === 3;
        // the text span node
        const mySpan = isTextNode ? domNode.parentNode : domNode;

        if (!isTextNode) { oldTextNode = domNode.firstChild; }

        if (mySpan && $(mySpan).hasClass(DOM.TEMPLATE_TEXT_CLASS)) {
            $(mySpan).removeClass(DOM.TEMPLATE_TEXT_CLASS);
            newTextNode = document.createTextNode(''); // generating a new empty text node
            mySpan.removeChild(oldTextNode);
            mySpan.appendChild(newTextNode);
            $(mySpan).parent().removeClass(DOM.NO_SPELLCHECK_CLASSNAME); // removing spell check marker
        }

        // Info: Is it necessary to remove neighboring template text spans, too? This should never happen.
        //       It was triggered by er spellchecking.

        return newTextNode;
    }

    /**
     * Inserting a text frame drawing node of type 'shape' into the document.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the dialog asking the user, if
     *  he wants to delete the selected content (if any) has been closed
     *  with the default action; or rejected, if the dialog has been
     *  canceled. If no dialog is shown, the promise is resolved
     *  immediately.
     */
    insertTextFrame() {

        // the undo manager returns the return value of the callback function
        return this.#undoManager.enterUndoGroup(() => {

            // helper function, that generates the operations
            const doInsertTextFrame = () => {

                // the operations generator
                const generator = this.createOperationGenerator();
                // the default width of the textframe in hmm
                const defaultWidth = 5000;
                // the width of the parent paragraph
                let paraWidth = 0;
                // the current cursor position
                const start = this.#selection.getStartPosition();
                // the default border attributes
                const lineAttrs = { color: Color.BLACK, style: 'single', type: 'solid', width: getWidthForPreset('thin') };
                // the default fill color attributes
                const fillAttrs = { color: { ...Color.BACK1, fallbackValue: 'FFFFFF' }, type: 'solid' };
                // the default attributes (wordWrap is required for 62582)
                const attrs = { drawing: { width: defaultWidth }, shape: { autoResizeHeight: true, noAutoResize: false, wordWrap: true }, line: lineAttrs, fill: fillAttrs };
                // the position of the first paragraph inside the text frame
                const paraPos = _.clone(start);
                // a required style ID for style handling
                let parentStyleId = null;
                // the paragraph node, in which the text frame will be inserted
                let paraNode = null;
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();
                // created operation
                let newOperation = null;

                this.#handleImplicitParagraph(start);

                // checking the width of the parent paragraph (36836)
                paraNode = getParagraphElement(this.getRootNode(target), _.initial(start));

                if (paraNode) {
                    paraWidth = convertLengthToHmm($(paraNode).width(), 'px');
                    if (paraWidth < defaultWidth) {
                        attrs.drawing.width = paraWidth;
                    }
                }

                paraPos.push(0);

                // modifying the attributes, if changeTracking is activated
                if (this.#changeTrack.isActiveChangeTracking()) {
                    attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
                }

                // adding character attributes for the first character in the paragraph (DOCS-4211)
                const additionalCharacterAttrs = this.collectCharacterAttributesForInsertOperation(this.getCurrentRootNode(target), start);
                if (additionalCharacterAttrs) {
                    attrs.character = additionalCharacterAttrs;
                }

                // Adding styleId (only for ODT)
                if (this.docApp.isODF()) { attrs.styleId = 'Frame'; }

                // handling the required styles for the text frame
                if (_.isString(attrs.styleId) && this.drawingStyles.isDirty(attrs.styleId)) {

                    // checking the parent style of the specified style
                    parentStyleId = this.drawingStyles.getParentId(attrs.styleId);

                    if (_.isString(parentStyleId) && this.drawingStyles.isDirty(parentStyleId)) {
                        // inserting parent of text frame style to document
                        this.generateInsertStyleOp(generator, 'drawing', parentStyleId, true);
                        this.drawingStyles.setDirty(parentStyleId, false);
                    }

                    // insert text frame style to document
                    this.generateInsertStyleOp(generator, 'drawing', attrs.styleId);
                }

                // text frames are drawings of type shape
                newOperation = { attrs, start: _.copy(start), type: 'shape' };
                this.#extendPropertiesWithTarget(newOperation, target);
                generator.generateOperation(Op.INSERT_DRAWING, newOperation);

                // add a paragraph into the shape, so that the cursor can be set into the text frame
                // -> an operation is required for this step, so that remote clients are also prepared (-> no implicit paragraph)
                // TODO: Change tracking of this paragraph?
                newOperation = { start: _.clone(paraPos) };
                this.#extendPropertiesWithTarget(newOperation, target);
                generator.generateOperation(Op.PARA_INSERT, newOperation);

                // reducing distance of paragraphs inside the text frame
                newOperation = { start: _.clone(paraPos), attrs: { paragraph: { marginBottom: 0 } } };
                this.#extendPropertiesWithTarget(newOperation, target);
                generator.generateOperation(Op.SET_ATTRIBUTES, newOperation);

                // apply all collected operations
                this.applyOperations(generator);

                // setting cursor into text frame
                this.#selection.setTextSelection(this.#lastOperationEnd);
            };

            if (this.#selection.hasRange()) {
                return this.deleteSelected().done(() => { doInsertTextFrame(); });
            }

            doInsertTextFrame();
            return $.when();

        }, this);  // enterUndoGroup()
    }

    /**
     * Generating an operation to assign a new anchor to a drawing. Supported anchor
     * values are 'inline', 'paragraph' and 'page'.
     *
     * @param {String} anchor
     *  A string describing the new anchor position of the drawing.
     */
    anchorDrawingTo(anchor) {

        // the drawing position
        let start = null;
        // the pixel position of the drawing relative to the page node
        let pixelPos = null;
        // the drawing node info object
        let startNodeInfo = null;
        // an optional footer offset in px
        let footerOffset = 0;
        // the page number of the drawing
        let pageNumber = 1;
        // an options object
        const options = {
            anchorHorBase: null,
            anchorHorAlign: null,
            anchorVertBase: null,
            anchorVertAlign: null
        };
        // currently active root node
        const activeRootNode = this.getCurrentRootNode();
        // drawing attributes required for the order of the drawing
        let drawingOrderAttrs = null;
        // a helper variable for the drawing node
        let drawingNode = null;

        if (this.isDrawingSelected()) {
            start = this.#selection.getStartPosition();
        } else if (this.getSelection().isAdditionalTextframeSelection()) {
            start = getOxoPosition(activeRootNode, this.getSelection().getSelectedTextFrameDrawing(), 0);
        } else {
            return;
        }

        startNodeInfo = getDOMPosition(activeRootNode, start, true);

        if (startNodeInfo && startNodeInfo.node && DrawingFrame.isDrawingFrame(startNodeInfo.node)) {

            if (anchor === 'inline') {
                // set anchor inline into the text
                options.inline = true;

                // setting other values to their defaults
                options.anchorHorAlign = null;
                options.anchorVertAlign = null;
                options.anchorHorOffset = null;
                options.anchorVertOffset = null;
                options.anchorHorBase = null;
                options.anchorVertBase = null;

                options.anchorBehindDoc = null;
                options.anchorLayerOrder = null;

            } else {

                drawingNode = startNodeInfo.node;

                // set Anchor to page, margin or paragraph
                if (anchor === 'page' || anchor === 'margin') {

                    pixelPos = getPixelPositionToRootNodeOffset(activeRootNode, startNodeInfo.node, this.docApp.getView().getZoomFactor() * 100);

                    if (DOM.isHeaderNode(activeRootNode)) {
                        pageNumber = 1;  // in header or footer the page number is not required
                    } else if (DOM.isFooterNode(activeRootNode)) {
                        pageNumber = 1;  // in header or footer the page number is not required
                        // if this is a footer, the height between page start and footer need to be added
                        footerOffset = convertHmmToLength(this.#pageLayout.getPageAttribute('height'), 'px', 1) - Math.round($(activeRootNode).outerHeight(true));
                        pixelPos.y += footerOffset;
                    } else {
                        if (DOM.isDrawingLayerNode(startNodeInfo.node.parentNode)) {
                            drawingNode = DOM.getDrawingPlaceHolderNode(drawingNode); // using previous sibling (see template doc)
                        }
                        pageNumber = this.#pageLayout.getPageNumber(drawingNode); // maybe this drawing is not on page 1
                        if (pageNumber > 1) { pixelPos.y -= getVerticalPagePixelPosition(activeRootNode, this.#pageLayout, pageNumber, this.docApp.getView().getZoomFactor() * 100); }
                    }

                    if (anchor === 'margin') {
                        // handling top and left margin
                        pixelPos.x -= convertHmmToLength(this.#pageLayout.getPageAttribute('marginLeft'), 'px', 1);
                        pixelPos.y -= convertHmmToLength(this.#pageLayout.getPageAttribute('marginTop'), 'px', 1);
                    }

                    options.anchorHorBase = anchor;
                    options.anchorVertBase = anchor;

                    options.anchorHorOffset = convertLengthToHmm(pixelPos.x, 'px');
                    options.anchorVertOffset = convertLengthToHmm(pixelPos.y, 'px');

                } else {
                    options.anchorHorBase = 'column';
                    options.anchorVertBase = 'paragraph';

                    options.anchorHorOffset = 0;
                    options.anchorVertOffset = 0;
                }

                options.inline = false;
                options.anchorHorAlign = 'offset';
                options.anchorVertAlign = 'offset';

                // getting the operation attributes for 'anchorBehindDoc' and 'anchorLayerOrder', so that the drawing is positioned at the top (54735)
                // -> but this must never lead to a drawing behind the text (DOCS-3270)
                if (drawingNode && DOM.isInlineComponentNode(drawingNode)) {
                    drawingOrderAttrs = this.getDrawingLayer().getDrawingOrderAttributes('front', drawingNode);
                    if (drawingOrderAttrs && !drawingOrderAttrs.anchorBehindDoc) { _.extend(options, drawingOrderAttrs); }
                }
            }

            this.setAttributes('drawing', { drawing: options });

            // the selection needs to be repainted. Otherwise it is not possible
            // to move the drawing to the page borders.
            DrawingFrame.clearSelection(startNodeInfo.node);
            this.#selection.drawDrawingSelection(startNodeInfo.node);
        }

    }

    /**
     * Generating an operation to assign a border to a drawing.
     *
     * @param {String} preset
     *  A string describing the new border of the drawing.
     */
    setDrawingBorder(preset) {

        // new line attributes
        let lineAttrs = null;
        // the selected drawings of a multi drawing selection
        let selectedDrawings = null;
        // the positions of the selected drawings of a multi drawing selection
        let allDrawingPositions = null;

        // helper function to generate operation(s)
        const generateDrawingBorderOperation = localLineAttrs => {

            // the attributes of the currently selected drawing
            const oldLineAttrs = this.getAttributes('drawing').line;

            if (localLineAttrs.type !== 'none' && (!oldLineAttrs || !oldLineAttrs.color || (oldLineAttrs.color.type === 'auto'))) {
                localLineAttrs.color = Color.BLACK;
            }

            this.setAttributes('drawing', { line: localLineAttrs }, { drawingFilterRequired: true });
        };

        // there must be a drawing selection
        if ((!this.#selection.isAnyDrawingSelection()) || (!this.isDrawingSelected() && !this.#selection.isAdditionalTextframeSelection())) { return; }

        lineAttrs = resolvePresetBorder(preset);

        // if no border color is available, push default black (but not for multi drawing selections (52421))
        if (!this.#selection.isMultiSelection()) {
            generateDrawingBorderOperation(_.copy(lineAttrs, true));
        } else {

            selectedDrawings = this.#selection.getMultiSelection(); // handling for multi drawing selection 57897
            allDrawingPositions = [];

            this.#undoManager.enterUndoGroup(() => {

                _.each(selectedDrawings, oneSelection => {

                    const startPosition = this.#selection.getStartPositionFromMultiSelection(oneSelection);
                    const endPosition = this.#selection.getEndPositionFromMultiSelection(oneSelection);

                    allDrawingPositions.push(startPosition);

                    // setting each drawing selection individually, so that this.setAttributes can still be used
                    this.#selection.setTextSelection(startPosition, endPosition);

                    generateDrawingBorderOperation(_.copy(lineAttrs, true));

                });
            });

            // restoring the multi selection
            this.#selection.setMultiDrawingSelectionByPosition(allDrawingPositions);
        }

    }

    /**
     * Generating an operation to assign lineending to a line/connector.
     *
     * @param {String} endings
     *  A string describing the new head and tail type of the line/connector.
     */
    setLineEnds(endings) {
        // there must be a drawing selection
        if (!this.getSelection().isAnyDrawingSelection()) { return; }

        if (!this.isDrawingSelected() && !this.getSelection().isAdditionalTextframeSelection()) {
            return;
        }

        const oldLineAttrs = this.getAttributes('drawing').line;
        const end = endings.split(':');
        const head = (end[0] !== 'none') ? end[0] : null;
        const tail = (end[1] !== 'none') ? end[1] : null;
        const lineAttrs = {};

        if (oldLineAttrs.headEndType !== head) { lineAttrs.headEndType = head; }
        if (oldLineAttrs.tailEndType !== tail) { lineAttrs.tailEndType = tail; }

        if (!_.isEmpty(lineAttrs)) { this.setAttributes('drawing', { line: lineAttrs }); }
    }

    /**
     * Generating an operation to assign a color to a drawing border.
     *
     * @param {Object} color
     *  An object describing the new border color of the drawing.
     */
    setDrawingBorderColor(color) {

        // there must be a drawing selection
        if (!this.getSelection().isAnyDrawingSelection()) { return; }

        if (!this.isDrawingSelected() && !this.getSelection().isAdditionalTextframeSelection()) {
            return;
        }

        if (color.type === 'auto') {
            this.setAttributes('drawing', { line: { type: 'none' } }, { drawingFilterRequired: true });
        } else {
            this.setAttributes('drawing', { line: { type: 'solid', color } }, { drawingFilterRequired: true });
        }
    }

    /**
     * Generating an operation to assign a fill color to a drawing.
     *
     * Info: In slide mode the filter does not accept color 'auto'. In this
     *       case it is handled like a removal of any background, if the
     *       user selects 'auto'.
     *
     * @param {Object} color
     *  An object describing the new fill color of the drawing.
     */
    setDrawingFillColor(color) {

        // there must be a drawing selection
        if (!this.getSelection().isAnyDrawingSelection()) { return; }

        // the fill attributes for the drawing
        const fillAttrs = {};

        if (color.type === 'auto') {
            if (this.useSlideMode()) {
                fillAttrs.type = null;
                fillAttrs.color = null; // DOCS-4392
                if (this.docApp.isODF()) { fillAttrs.type = 'none'; }
            } else {
                fillAttrs.type = 'none';
                fillAttrs.color = Color.AUTO;
            }
        } else {
            fillAttrs.color = color;
            fillAttrs.type = 'solid';
        }

        // adding property 'fallbackValue' to themed colors inside the specified attributes
        const attrSet = { fill: fillAttrs };
        this.addFallbackValueToThemeColors(attrSet);
        this.setAttributes('drawing', attrSet, { drawingFilterRequired: true });
    }

    /**
     * Generating an operation assigning transparency to a drawing.
     *
     * @param {Object} fillAttrs
     *  An object describing the new transparency of the drawing.
     */
    setDrawingBgImageTransparency(fillAttrs) {

        // there must be a drawing selection
        // if (!this.getSelection().isAnyDrawingSelection()) { return; }

        this.setAttributes('drawing', { fill: fillAttrs }, { drawingFilterRequired: true });
    }

    setDrawingTransparency() {
        // there must be a drawing selection
        if (!this.getSelection().isAnyDrawingSelection()) { return; }

        // the fill attributes for the drawing
        const fillAttrs = getEmptyDrawingBackgroundAttributes().fill;

        this.setAttributes('drawing', { fill: fillAttrs }, { drawingFilterRequired: true });
    }

    /**
     * Handles any drawing (shape/image) cropping properties.
     *
     * @param {Boolean} state
     *  Set/unset Crop mode.
     */
    handleDrawingCrop(state) {
        if (state) {
            const drawing = this.#selection.getAnyDrawingSelection();
            const attrs = getExplicitAttributeSet(drawing);
            if (attrs) {
                const isImageType = this.#selection.getSelectedDrawingType() === 'image';
                const contentNode = DrawingFrame.getContentNode(drawing);
                const cropNode = $('<div class="drawing crop">');
                const zoomFactor = this.docApp.getView().getZoomFactor();
                // const presetShapeId = (attrs.geometry && attrs.geometry.presetShape) || null;
                // const options = { noFill: true };
                // const shapeSize = { width: drawing.width(), height: drawing.height() };
                // const widthPx = convertHmmToLength(attrs.drawing.width, 'px', 1);
                // const heightPx = convertHmmToLength(attrs.drawing.height, 'px', 1);

                this.#selection.setActiveCropNode(drawing);

                if (isImageType) {
                    const imgClone = contentNode.find('img').clone();
                    const imgPosLeft = parseInt(imgClone.css('left'), 10);
                    const imgPosTop = parseInt(imgClone.css('top'), 10);
                    const imgWidth = parseInt(imgClone.css('width'), 10);
                    const imgHeight = parseInt(imgClone.css('height'), 10);
                    cropNode.css({ marginLeft: imgPosLeft, marginTop: imgPosTop, width: imgWidth, height: imgHeight }).append(imgClone.css({ opacity: 0.5, left: 0, top: 0, position: 'absolute' }));
                } else { // shape with bitmap fill
                    // const previewCanvas = DrawingFrame.drawPreviewShape(this.docApp, drawing, shapeSize, presetShapeId, options);
                    // contentNode.append(previewCanvas);
                    // const bitmapUrlAttr = attrs.fill.bitmap.imageUrl;
                    // const calcPosSize = attrs.fill.bitmap.stretching ? DrawingUtils.getStrechedCoords(attrs.fill.bitmap.stretching, widthPx, heightPx) : attrs.drawing;
                    // const imageUrl = ImageUtil.getFileUrl(this.docApp, bitmapUrlAttr);
                    // const fakeDiv = $('<img>').attr('src', imageUrl).css({
                    //     width: calcPosSize.width,
                    //     height: calcPosSize.height,
                    //     opacity: 0.5,
                    //     position: 'absolute',
                    //     left: 0,
                    //     top: 0
                    // });
                    // cropNode.css({ width: calcPosSize.width, height: calcPosSize.height, left: calcPosSize.left, top: calcPosSize.top }).append(fakeDiv);
                }
                contentNode.append(cropNode);
                drawCropFrame(drawing, cropNode, zoomFactor);

            } else {
                globalLogger.warn('editor.handleDrawingCrop(): no explicit attributes for drawing: ', drawing);
            }
        } else {
            this.exitCropMode();
        }
    }

    /**
     * Exit image crop mode and clean up cropped drawing.
     */
    exitCropMode() {
        const cropNode = this.#selection.getActiveCropNode();
        clearCropFrame(cropNode);
        this.#selection.setActiveCropNode($());
    }

    /**
     * Publicly available method from model to get selected node and refresh it's crop frame.
     */
    refreshCropFrame() {
        const cropNode = this.#selection.getActiveCropNode();
        refreshCropFrameImport(cropNode);
    }

    /**
     * Public method for fill or fit types of image crop,
     * accesable via dropdown toolbar menu.
     *
     * @param {String} type
     *  Can be 'fill' or 'fit'.
     */
    cropFillFitSpace(type) {
        const drawing = this.getSelection().getSelectedDrawing();
        const explAttrs = getExplicitAttributeSet(drawing);

        cropFillFitSpaceImport(this, explAttrs, type).then(cropOps => {
            if (cropOps && !_.isEmpty(cropOps)) {
                const opProperties = { start: this.getSelection().getStartPosition(), attrs: { image: cropOps } };
                const generator = this.createOperationGenerator();
                generator.generateOperation(Op.SET_ATTRIBUTES, opProperties);
                this.applyOperations(generator);
            }
        });
    }

    /**
     * Shows an insert image dialog from drive, local or URL dialog
     *
     * @param {ImageDescriptor} imageDesc
     *  The object containing the image attributes. This are 'url' or 'substring'.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Number[]} [options.position=null]
     *   The logical position at that the drawing shall be inserted.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved or rejected after the image is inserted or
     *  if an error occured during inserting the image.
     */
    insertImageURL(imageDesc, options) {

        let def = null;
        let url = null;
        let name = null;
        let attrs = null;

        // the url of an image, that exceeds the size limit
        var exceededSizeUrl = "";

        if (!imageDesc) {
            def = this.createRejectedPromise();
        } else {

            attrs = { drawing: {}, image: {}, line: { type: 'none' } };
            url = (imageDesc.url) ? imageDesc.url : imageDesc;
            name = (imageDesc.name) ? imageDesc.name : null;

            if (this.docApp.isODF() && this.docApp.isTextApp()) { attrs.shape = { autoResizeHeight: false }; } // 56351: Setting explicit autoResizeHeight property for ODT
            if (this.docApp.isTextApp()) { attrs.drawing.inline = true; } // images are inserted inline in OX Text (DOCS-1722)

            if (isBase64(url)) {
                // base64 to media URL
                def = insertDataUrl(this.docApp, url);

            } else if (isAbsoluteUrl(url)) {
                // external URl to media URL
                def = insertURL(this.docApp, url);

            } else {
                def = $.when({ url, name, transform: imageDesc.transform });
            }
        }

        def = def.then(result => {
            // media url and file name
            attrs.image.imageUrl = result.url;
            attrs.drawing.name = name || result.name;
            applyImageTransformation(attrs.drawing, result.transform);
            exceededSizeUrl = result.exceedsSizeLimit ? result.url : "";

            // image size
            return this.getImageSize(result.url);
        });

        def = def.then(size => {
            // the apply operations deferred for inserting the image
            const applyDef = this.createDeferred();
            // the apply operations result
            let result = false;
            // created operation
            let newOperation = null;
            // selection before insert the image
            const startSelection = this.#selection.getStartPosition();
            // the logical position of the inserted drawing, given position or start selection
            const start = getArrayOption(options, 'position', startSelection, true);

            /**
             * Setting the cursor position (selecting the inserted drawing), after the image was loaded.
             * If the image needs long time to load and the user change the position the selection will not
             * changed to the image.
             */
            const setPostionAfterLoad = () => {
                if (_.isEqual(startSelection, this.#selection.getStartPosition())) {
                    this.#selection.setTextSelection(_.copy(start), increaseLastIndex(start));
                }
            };

            // exit silently if we lost the edit rights
            if (!this.docApp.isEditable()) {
                return applyDef.resolve();
            }

            this.one('image:loaded', setPostionAfterLoad);

            this.#undoManager.enterUndoGroup(() => {

                _.extend(attrs.drawing, size, DEFAULT_DRAWING_MARGINS);

                this.#handleImplicitParagraph(start);

                // modifying the attributes, if changeTracking is activated
                if (this.#changeTrack.isActiveChangeTracking()) {
                    attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
                }

                // adding character attributes for the first character in the paragraph (DOCS-4211)
                const additionalCharacterAttrs = this.collectCharacterAttributesForInsertOperation(this.getCurrentRootNode(this.getActiveTarget()), start);
                if (additionalCharacterAttrs) {
                    attrs = attrs || {};
                    attrs.character = additionalCharacterAttrs;
                }

                newOperation = {
                    name: Op.INSERT_DRAWING,
                    start: _.copy(start),
                    type: 'image',
                    attrs
                };
                this.#extendPropertiesWithTarget(newOperation, this.getActiveTarget());

                result = this.applyOperations(newOperation);
            }); // enterUndoGroup()

            if (!result) {
                this.stopListeningTo(this, 'image:loaded', setPostionAfterLoad);

                // setting the cursor position (selecting the inserted drawing)
                this.#selection.setTextSelection(_.copy(start), increaseLastIndex(start));
            }

            return result ? applyDef.resolve() : applyDef.reject();
        });

        def.fail((reason) => {
            if (reason === "SIZE_DETECTION_FAILED" && exceededSizeUrl && /^http:/.test(exceededSizeUrl) && /^https:/.test(ox.abs)) {
                this.docApp.rejectEditAttempt('imageHttp');
            } else {
                this.docApp.rejectEditAttempt('image');
            }
        });

        return def;
    }

    /**
     * Inserting a tab stopp.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved if the dialog has been closed with
     *  the default action; or rejected, if the dialog has been canceled.
     *  If no dialog is shown, the promise is resolved immediately.
     */
    insertTab() {

        // the undo manager returns the return value of the callback function
        return this.#undoManager.enterUndoGroup(() => {

            const doInsertTabAndSetCursor = () => {

                const start = this.#selection.getStartPosition();
                const operation = { name: Op.TAB_INSERT, start: _.copy(start) };
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();

                this.#extendPropertiesWithTarget(operation, target);
                this.#handleImplicitParagraph(start);
                if (_.isObject(this.#preselectedAttributes)) { operation.attrs = this.#preselectedAttributes; }
                // modifying the attributes, if changeTracking is activated
                if (this.#changeTrack.isActiveChangeTracking()) {
                    operation.attrs = operation.attrs || {};
                    operation.attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
                }

                // adding character attributes for the first character in the paragraph (DOCS-4211)
                const additionalCharacterAttrs = this.collectCharacterAttributesForInsertOperation(this.getCurrentRootNode(target), start);
                if (additionalCharacterAttrs) {
                    operation.attrs = operation.attrs || {};
                    operation.attrs.character = this.#mergeAttrsIfMissing(operation.attrs.character, additionalCharacterAttrs);
                }

                this.applyOperations(operation);

                if (IOS_SAFARI_DEVICE && this.isHeaderFooterEditState()) {
                    //header footer workaround (Bug 39976)
                    this.applyOperations({ name: Op.TEXT_INSERT, text: ' ', start: increaseLastIndex(start) });
                    this.deleteRange(increaseLastIndex(start));
                } else {
                    this.#selection.setTextSelection(this.#lastOperationEnd);
                }
            };

            // Special handling for selected drawing frames (62509) -> if insertTab button is pressed, a tab is inserted into the shape
            if (this.#selection.hasRange() && this.#selection.isDrawingFrameSelection()) {
                const modifiedSelection = this.#selection.getValidTextPositionAccordingToDrawingFrame();
                if (modifiedSelection) { this.#selection.setTextSelection(modifiedSelection); } // setting the new selection
            }

            if (this.#selection.hasRange()) {
                return this.deleteSelected().done(() => { doInsertTabAndSetCursor(); });
            }

            doInsertTabAndSetCursor();

            return $.when();

        }, this);

    }

    insertText(text, position, attrs, forcedTarget) {

        // the target for the operation (the forced target is used for the IME mode (DOCS-2852))
        const target = forcedTarget || this.getActiveTarget();
        // current element container node
        const rootNode = this.getCurrentRootNode(target);

        // In IE and on Android text input in slide selection happens sometimes (focus not set fast enough into clipboard)
        if ((_.browser.IE || _.browser.Android) && this.useSlideMode() && position.length === 2) {
            globalLogger.error('Editor.insertText: Invalid operation: ' + JSON.stringify(position) + ' : ' + text);
        }

        // adding changes attributes -> but for performance reasons only, if the change tracking is active!
        if (this.#changeTrack.isActiveChangeTracking()) {
            attrs = attrs || {};
            attrs.changes = { inserted: this.#changeTrack.getChangeTrackInfo(), removed: null };
        }

        // adding character attributes for the first character in the paragraph (DOCS-4211)
        const additionalCharacterAttrs = this.collectCharacterAttributesForInsertOperation(rootNode, position);
        if (additionalCharacterAttrs) {
            attrs = attrs || {};
            attrs.character = this.#mergeAttrsIfMissing(attrs.character, additionalCharacterAttrs);
        }

        this.#undoManager.enterUndoGroup(() => {

            const operation = { name: Op.TEXT_INSERT, text, start: _.clone(position) };

            this.#handleImplicitParagraph(position);
            if (_.isObject(attrs)) { operation.attrs = _.copy(attrs, true); }
            this.#guiTriggeredOperation = true;  // Fix for 30587, remote client requires synchronous attribute setting
            this.applyOperations(operation);
            this.#guiTriggeredOperation = false;

            // Bug 25077: Chrome cannot set curser after a trailing SPACE
            // character, need to validate the paragraph node immediately
            // (which converts trailing SPACE characters to NBSP)
            // -> only do this, if it is really necessary (performance)
            if (_.browser.WebKit && (text === ' ')) {
                this.#validateParagraphNode(getParagraphElement(rootNode, position.slice(0, -1)));
            }

        }, this);
    }

    /**
     * Collecting the character attributes that are attached to an empty span inside an empty
     * paragraph. This character attributes are attached, when a previous paragraph is splitted
     * at the end and the last span of the paragraph had some hard character attributes.
     *
     * In this scenario the filter has no span inside the empty "new" paragraph, so that the hard
     * character attributes cannot be saved. Therefore the client must send this hard character
     * attributes with an operation, when the first element in the "new" paragraph is created.
     * This happens with an "insertText" or "insertTab" operation.
     *
     * Performance: This function is called for the generation of every "insertText" or "insertTab"
     * operation. To avoid the DOM access, the first check is, that the position ends with "0". Only
     * then the explicit character attributes are searched. But maybe in the future the check for
     * "0" is no longer possible (maybe caused by leading absolute drawings?). Therefore there is
     * the additional check "this.checkUnsentCharacterAttributes()". This checks a variable, that
     * is set in OX Text after every selection change. After any insertText operation, this variable
     * is set to false, because it is not necessary to check for unsent character attributes,
     * because the paragraph cannot be empty.
     *
     * DOCS-4211, only for Text Application and only for OOXML
     *
     * @param {Node} rootNode
     *  The root node corresponding to the logical position.
     *
     * @param {Array<Number>} position
     *  The logical position of the inserted element.
     *
     * @returns {Object|Null}
     *  An object with the character attributes, that are assigned to the empty text span.
     *  Or null, if there are no hard character attributes.
     */
    collectCharacterAttributesForInsertOperation(rootNode, position) {

        let attrs = null;

        // Performance: Only allowing DOM access, if the position ends with "0" and this.checkUnsentCharacterAttributes() returns true
        // if (_.last(position) !== 0 || !this.docApp.isTextApp() || !this.docApp.isOOXML() || !this.checkUnsentCharacterAttributes()) { return attrs; }
        if (!this.hasOoxmlTextCharacterFilterSupport() || !this.checkUnsentCharacterAttributes()) { return attrs; }

        // Performance: This should not be reached during typing -> this.checkUnsentCharacterAttributes() must be as restrictive as possible
        //              Checking "_.last(position) !== 0" is no longer valid because of paragraphs that contain only drawings (DOCS-4178)

        // check for character attributes in the span and the paragraph
        const paragraph = getParagraphElement(rootNode, _.initial(position));

        // TODO: Performance: Not calculating the full length of the paragraph, but only checking, if it is an empty paragraph or if it
        //                    contains only drawings

        if (!DOM.isImplicitParagraphNode(paragraph) && isEmptyParagraph(paragraph, { ignoreDrawings: true })) {
            // checking the first text span after a splitParagraph operation
            const span = DOM.findFirstPortionSpan(paragraph);
            const spanAttrs = getExplicitAttributeSet(span);
            if (spanAttrs.character) {
                attrs = attrs || {};
                attrs = _.copy(spanAttrs.character, true);
            }
        }

        return attrs;
    }

    /**
     * Collecting the character attributes that are attached to the last text span of a
     * paragraph. This character attributes must be collected for insertText operations,
     * that are created after an optional splitParagraph operation.
     *
     * See DOCS-5185: insertText operations are generated, before the splitParagraph
     * operation is applied.
     *
     * @param {Node} rootNode
     *  The root node corresponding to the logical position.
     *
     * @param {Array<Number>} position
     *  The logical position of the splitParagraph operation.
     *
     * @returns {Object|Null}
     *  An object with the character attributes. Or null, if there are no hard character
     *  attributes.
     */
    getCharacterAttributesFromFinalSpan(rootNode, position) {

        let attrs = null;

        // check for character attributes in the span and the paragraph
        const paragraph = getParagraphElement(rootNode, _.initial(position));

        if (paragraph && !DOM.isImplicitParagraphNode(paragraph)) {
            // checking the last text span (before a splitParagraph operation)
            const span = DOM.findLastPortionSpan(paragraph);
            const spanAttrs = getExplicitAttributeSet(span);
            if (spanAttrs.character) {
                attrs = attrs || {};
                attrs = _.copy(spanAttrs.character, true);
            }
        }

        return attrs;
    }

    /**
     * Removing the character attributes that are attached to an empty span inside an empty
     * paragraph. This character attributes are attached, when a previous paragraph is splitted
     * at the end and the last span of the paragraph had some hard character attributes.
     *
     * In this scenario the filter has no span inside the empty "new" paragraph, so that the hard
     * character attributes cannot be saved. Therefore the client must remove this hard character
     * attributes with an operation, when the first element in the "new" paragraph is created.
     * This happens with an "insertText" or "insertTab" operation.
     *
     * DOCS-4211, only for Text Application and only for OOXML
     *
     * @param {Node} rootNode
     *  The root node corresponding to the logical position.
     *
     * @param {Array<Number>} position
     *  The logical position of the inserted element.
     *
     * @returns {Object|Null}
     *  An object with the character attributes, that are explicitely removed from the empty text
     *  span with an operation.
     *  Or null, if there are no hard character attributes.
     */
    getRemoveCharacterAttributesForInsertOperation(rootNode, position) {
        const attrs = this.collectCharacterAttributesForInsertOperation(rootNode, position);
        if (attrs) { for (const attr in attrs) { attrs[attr] = null; } }
        return attrs;
    }

    /**
     * Modifying an existing operation, so that character attributes are added to remove
     * existing character attributes at a text span. This character attributes are attached
     * to the span, when a previous paragraph is splitted at the end and the last span of the
     * paragraph had some hard character attributes.
     *
     * In this scenario the filter has no span inside the empty "new" paragraph, so that the hard
     * character attributes cannot be saved. Therefore the client must remove this hard character
     * attributes with an operation, when the first element in the "new" paragraph is created.
     * This happens with an "insertText" or "insertTab" operation.
     *
     * This function is called, when new insert is pasted or a TOC is inserted. In this scenarios
     * existing hard character attributes shall not be used.
     *
     * DOCS-4211
     *
     * @param {Object} operation
     *  The operation that might be modified within this function.
     *
     * @param {CharacterAttribtutes} [unsentCharacterAttrs]
     *  An optional set of character attributes. If this is specified, the attributes are used and
     *  not determined by the current position. This is especially useful for groups of operations,
     *  in which the new positions even do not exist in the time of operation creation.
     */
    removeHardCharacterAttributesAtOperation(operation, unsentCharacterAttrs) {
        if (operation && INSERT_PARA_CONTENT_OPERATIONS.includes(operation.name) && operation.start && _.last(operation.start) === 0) {
            // removing explicitely character attributes for the first character in the paragraph (DOCS-4211)
            const additionalCharacterAttrs = unsentCharacterAttrs ? unsentCharacterAttrs : this.getRemoveCharacterAttributesForInsertOperation(this.getCurrentRootNode(this.getActiveTarget()), operation.start);
            if (additionalCharacterAttrs) {
                operation.attrs = operation.attrs || {};
                operation.attrs.character = this.#mergeAttrsIfMissing(operation.attrs.character, additionalCharacterAttrs);
            }
        }
    }

    /**
     * Assigning a set of specified character attributes to a specified set of operations. Especially
     * to the insert operations, that insert content at the first position in a paragraph.
     * If the parameter "removeAttrs" is set to true, the specified character attributes are not set
     * but remvoved.
     *
     * DOCS-4211
     *
     * @param {Array<Object>} operations
     *  An array with operations that might be modified within this function.
     *
     * @param {CharacterAttribtutes} unsentCharacterAttrs
     *  A set of attributes that are added to the given operations, if required.
     *
     * @param {Boolean} [removeAttrs=false]
     *  If set to true, the specified attributes are not set, but removed within the operation.
     */
    assignCharacterAttrsToInsertOperations(operations, unsentCharacterAttrs, removeAttrs) {
        if (!operations || !unsentCharacterAttrs) { return; }
        const characterAttrs = unsentCharacterAttrs;
        if (removeAttrs) { for (const attr in characterAttrs) { characterAttrs[attr] = null; } }
        operations.forEach(op => { this.removeHardCharacterAttributesAtOperation(op, characterAttrs); });
    }

    // style sheets and formatting attributes -----------------------------

    /**
     * Returns the collection of all list definitions.
     */
    getListCollection() {
        return this.#listCollection;
    }

    /**
     * Setting the application specific list collection.
     */
    setListCollection(collection) {
        this.#listCollection = this.member(collection);
    }

    /**
     * Returns the current selection.
     */
    getSelection() {
        return this.#selection;
    }

    /**
     * Returns the remote selection.
     */
    getRemoteSelection() {
        return this.#remoteSelection;
    }

    /**
     * Returns whether the current selection selects any text. This
     * includes the rectangular table cell selection mode.
     */
    isTextSelected() {
        return this.#selection.getSelectionType() !== 'drawing';
    }

    /**
     * Returns whether the editor contains a selection range (text,
     * drawings, or table cells) instead of a simple text cursor.
     */
    hasSelectedRange() {
        return this.#selection.hasRange();
    }

    /**
     * Returns whether the editor contains a selection within a
     * single paragraph or not.
     */
    hasEnclosingParagraph() {
        return this.#selection.getEnclosingParagraph() !== null;
    }

    // PUBLIC TABLE METHODS

    /**
     * Returns whether the editor contains a selection within a
     * table or not.
     */
    isPositionInTable() {
        return !_.isNull(this.#selection.getEnclosingTable());
    }

    /**
     * Returns whether the editor contains a selection over one or multiple
     * cells within a table
     */
    isCellRangeSelected() {
        // Firefox has beautiful cell-range-selection
        if (_.browser.Firefox) {
            const currentSelection = this.#selection.getBrowserSelection();
            return (currentSelection && currentSelection.active) ? DOM.isCellRangeSelected(currentSelection.active.start.node, currentSelection.active.end.node) : false;

        // every other browser has to check the selection
        } else {
            return positionsInTowCellsInSameTable(this.getCurrentRootNode(), this.#selection.getStartPosition(), this.#selection.getEndPosition());
        }
    }

    /**
     * Returns the number of columns, if the editor contains a selection within a
     * table.
     */
    getNumberOfColumns() {
        const table = this.#selection.getEnclosingTable() || (this.#selection.isTableDrawingSelection() && this.#selection.getSelectedDrawing());
        return table && getColumnCount(table);
    }

    /**
     * Returns the number of rows, if the editor contains a selection within a
     * table.
     */
    getNumberOfRows() {
        const table = this.#selection.getEnclosingTable() || (this.#selection.isTableDrawingSelection() && this.#selection.getSelectedDrawing());
        return table && getRowCount(table);
    }

    /**
     * Returns whether a further row can be added, if the editor contains a
     * selection within a table. The maximum number of rows and cells can
     * be defined in the configuration.
     */
    isRowAddable() {

        // the number of rows in the current table
        const rowCount = this.getNumberOfRows();
        // the number of columns in the current table
        const colCount = this.getNumberOfColumns();

        // check that cursor is located in a table, check maximum row count and maximum cell count
        return _.isNumber(rowCount) && _.isNumber(colCount) &&
            (rowCount < MAX_TABLE_ROWS) &&
            ((rowCount + 1) * colCount <= MAX_TABLE_CELLS);
    }

    /**
     * Returns whether a further column can be added, if the editor contains a selection within a
     * table. The maximum number of columns and cells can be defined in the configuration.
     */
    isColumnAddable() {

        // the number of rows in the current table
        const rowCount = this.getNumberOfRows();
        // the number of columns in the current table
        const colCount = this.getNumberOfColumns();

        // check that cursor is located in a table, check maximum column count and maximum cell count
        return _.isNumber(rowCount) && _.isNumber(colCount) &&
            (colCount < MAX_TABLE_COLUMNS) &&
            ((colCount + 1) * rowCount <= MAX_TABLE_CELLS);
    }

    // PUBLIC DRAWING METHODS

    /**
     * Returns whether the current selection selects one or more drawings.
     *
     * @returns {Boolean}
     */
    isDrawingSelected() {
        return this.#selection.getSelectionType() === 'drawing';
    }

    /**
     * Returns whether the current selection is a text selection inside a shape
     * in an odf document. These 'shapes with text' need to be distinguished from
     * 'classical' text frames in odf format, because much less functionality is
     * available. It is also handled correctly, if the selection is inside a grouped
     * shape.
     *
     * @returns {Boolean}
     */
    isReducedOdfTextframeFunctionality() {
        const selectedTextFrame = this.#selection.getSelectedTextFrameDrawing();

        return this.docApp.isODF() && this.#selection.isAdditionalTextframeSelection() && (
            DrawingFrame.isReducedOdfTextframeNode(selectedTextFrame) ||
            (DrawingFrame.isGroupDrawingFrame(selectedTextFrame) && DrawingFrame.isReducedOdfTextframeNode(getClosestDrawingAtPosition(this.#selection.getRootNode(), this.#selection.getStartPosition())))
        );
    }

    /**
     * Returns whether the current selection is a text selection inside a 'classic'
     * text frame in an odf document. These 'classic' text frames need to be
     * distinguished from 'shapes with text', because more functionality is
     * available. It is also handled correctly, if the selection is inside a grouped
     * shape.
     *
     * @returns {Boolean}
     */
    isFullOdfTextframeFunctionality() {
        const selectedTextFrame = this.#selection.getSelectedTextFrameDrawing();

        return this.docApp.isODF() && this.#selection.isAdditionalTextframeSelection() && (
            DrawingFrame.isFullOdfTextframeNode(selectedTextFrame) ||
            (DrawingFrame.isGroupDrawingFrame(selectedTextFrame) && DrawingFrame.isFullOdfTextframeNode(getClosestDrawingAtPosition(this.#selection.getRootNode(), this.#selection.getStartPosition())))
        );
    }

    /**
     * Returns true if specified node is inside a text frame node inside a drawing.
     * This function also returns true, if the specified node is a text frame node
     * itself.
     *
     * @param {jQuery|Node} node
     *  The node, that is checked.
     *
     * @returns {Boolean}
     *  Whether the specified node is inside a text frame node in a drawing or is
     *  the text frame node itself.
     */
    isTextframeOrInsideTextframe(node) {
        return $(node).parentsUntil(DOM.PAGE_NODE_SELECTOR, DrawingFrame.TEXTFRAME_NODE_SELECTOR).length > 0;
    }

    /**
     * Returns true if specified node is inside a text frame node inside a drawing.
     * This function does not (!) returns true, if the specified node is a text
     * frame node itself.
     *
     * @param {jQuery|Node} node
     *  The node, that is checked.
     *
     * @returns {Boolean}
     *  Whether the specified node is inside a text frame node in a drawing.
     */
    isNodeInsideTextframe(node) {
        return !DrawingFrame.isTextFrameNode(node) && $(node).parentsUntil(DOM.PAGE_NODE_SELECTOR, DrawingFrame.TEXTFRAME_NODE_SELECTOR).length > 0;
    }

    /**
     * Returns whether the current selection is a selection inside a comment
     * in an odf document.
     *
     * @returns {Boolean}
     */
    isOdfCommentFunctionality() {
        return this.docApp.isODF() && this.isCommentFunctionality();
    }

    /**
     * Returns whether the current selection is a selection for inserting a shape.
     * -> only main document or header/footer is active.
     * In all other cases (for example text selection inside a text frame) a valid
     * text position can be calculated.
     *
     * @returns {Boolean}
     */
    isInsertShapePosition() {
        return !this.getActiveTarget() || this.isHeaderFooterEditState();
    }

    /**
     * Returns whether the current selection is a selection inside a comment.
     *
     * TODO: target is no longer supported for comments starting with the
     *       commenteditor for mentions.
     *
     * @returns {Boolean}
     */
    isCommentFunctionality() {
        return false;
    }

    /**
     * Returns whether the current selection is a selection inside header/footer.
     *
     * @returns {Boolean}
     */
    isHeaderFooterFunctionality() {
        return this.getActiveTarget() ? this.isHeaderFooterEditState() : false;
    }

    /**
     * Returns whether the current selection is a selection inside a comment or a header.
     *
     * @returns {Boolean}
     */
    isTargetFunctionality() {
        return this.getActiveTarget() ? this.isHeaderFooterEditState() : false;
    }

    /**
     * Returns whether the current selection selects text, not cells and
     * not drawings.
     *
     * @returns {Boolean}
     */
    isTextOnlySelected() {
        return this.#selection.getSelectionType() === 'text';
    }

    /**
     * Returns whether clipboard paste is in progress.
     *
     * @returns {Boolean}
     */
    isClipboardPasteInProgress() {
        return this.#pasteInProgress;
    }

    /**
     * Returns the default lateral heading character styles
     *
     * @returns {Array}
     */
    getDefaultHeadingCharacterStyles() {
        const styles = json.deepClone(HEADINGS_CHARATTRIBUTES);
        for (const style of styles) {
            style.color.fallbackValue = this.parseAndResolveColor(style.color, 'text').hex;
        }
        return styles;
    }

    /**
     * Returns the default lateral paragraph style
     *
     * @returns {Object}
     */
    getDefaultParagraphStyleDefinition() {
        return DEFAULT_PARAGRAPH_DEFINTIONS;
    }

    /**
     * Returns the default lateral table definiton
     *
     * @returns {Object}
     */
    getDefaultLateralTableDefinition() {
        return this.useSlideMode() ? DEFAULT_LATERAL_TABLE_DEFINITIONS_PRESENTATION : DEFAULT_LATERAL_TABLE_DEFINITIONS;
    }

    /**
     * Returns the default lateral table attributes
     *
     * @returns {Object}
     */
    getDefaultLateralTableAttributes() {
        return this.useSlideMode() ? TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent1', 'light1', 'dark1', this.getThemeModel()) : DEFAULT_LATERAL_TABLE_ATTRIBUTES;
    }

    /**
     * Returns the default lateral table definiton for ODF only
     *
     * @returns {Object}
     */
    getDefaultLateralTableODFDefinition() {
        return DEFAULT_LATERAL_TABLE_OX_DEFINITIONS;
    }

    /**
     * Returns the default lateral table attributes for ODF only
     *
     * @returns {Object}
     */
    getDefaultLateralTableODFAttributes() {
        return DEFAULT_LATERAL_TABLE_OX_ATTRIBUTES;
    }

    /**
     * Returns default lateral hyperlink definition.
     *
     * @returns {Object}
     */
    getDefaultLateralHyperlinkDefinition() {
        return DEFAULT_HYPERLINK_DEFINTIONS;
    }

    /**
     * Returns default lateral hyperlink character attributes.
     *
     * @returns {Object}
     */
    getDefaultLateralHyperlinkAttributes() {
        return DEFAULT_HYPERLINK_CHARATTRIBUTES;
    }

    /**
     * Returns the default lateral drawing definition.
     *
     * @returns {Object}
     */
    getDefaultDrawingDefintion() {
        return DEFAULT_DRAWING_DEFINITION;
    }

    /**
     * Returns the default drawing attributes.
     *
     * @returns {Object}
     */
    getDefaultDrawingAttributes() {
        return DEFAULT_DRAWING_ATTRS;
    }

    /**
     * Returns the default drawing margins.
     *
     * @returns {Object}
     */
    getDefaultDrawingMargins() {
        return DEFAULT_DRAWING_MARGINS;
    }

    /**
     * Returns the default drawing textframe definition.
     *
     * @returns {Object}
     */
    getDefaultDrawingTextFrameDefintion() {
        return DEFAULT_DRAWING_TEXTFRAME_DEFINITION;
    }

    /**
     * Returns the default drawing text frame attributes.
     *
     * @returns {Object}
     */
    getDefaultDrawingTextFrameAttributes() {
        return (this.useSlideMode() && this.docApp.isODF()) ? DEFAULT_DRAWING_TEXTFRAME_ATTRS_ODF_SLIDE : DEFAULT_DRAWING_TEXTFRAME_ATTRS;
    }

    /**
     * Returns the default lateral comment definitions.
     *
     * @returns {Object}
     */
    getDefaultCommentTextDefintion() {
        return DEFAULT_LATERAL_COMMENT_DEFINITIONS;
    }

    /**
     * Returns the default lateral header definitions.
     *
     * @returns {Object}
     */
    getDefaultHeaderTextDefinition() {
        return DEFAULT_LATERAL_HEADER_DEFINITIONS;
    }

    /**
     * Returns the default lateral footer definitions.
     *
     * @returns {Object}
     */
    getDefaultFooterTextDefinition() {
        return DEFAULT_LATERAL_FOOTER_DEFINITIONS;
    }

    /**
     * Returns the default comment text attributes.
     *
     * @returns {Object}
     */
    getDefaultCommentTextAttributes() {
        return DEFAULT_LATERAL_COMMENT_ATTRIBUTES;
    }

    /**
     * Returns the document default paragraph stylesheet id
     *
     * @returns {String|Null}
     */
    getDefaultUIParagraphStylesheet() {
        let styleNames = [];
        let styleId = null;

        if (this.docApp.isODF()) {
            styleNames = this.paragraphStyles.getStyleSheetNames();
            _(styleNames).each((_name, id) => {
                const lowerId = id.toLowerCase();
                if (lowerId === 'standard' || lowerId === 'normal') { styleId = id; }
            });

            if (!styleId) { styleId = this.paragraphStyles.getDefaultStyleId(); }
        } else {
            styleId = this.paragraphStyles.getDefaultStyleId();
        }

        return styleId;
    }

    /**
     * Returns the document default table stylesheet id
     * which can be used to set attributes for a new
     * table.
     *
     * @returns {String|Null}
     */
    getDefaultUITableStylesheet() {
        const styleNames = this.tableStyles.getStyleSheetNames();
        let highestUIPriority = 99;
        let tableStyleId = null;

        _(styleNames).each((name, id) => {
            const uiPriority = this.tableStyles.getUIPriority(id);

            if (_.isNumber(uiPriority) && (uiPriority < highestUIPriority)) {
                tableStyleId = id;
                highestUIPriority = uiPriority;
            }
        });

        return tableStyleId;
    }

    /**
     * Returns the document default hyperlink stylesheet id.
     *
     * @returns {String|Null}
     */
    getDefaultUIHyperlinkStylesheet() {
        const styleNames = this.characterStyles.getStyleSheetNames();
        let hyperlinkId = null;

        _(styleNames).find((name, id) => {
            const lowerName = name.toLowerCase();
            if (lowerName.indexOf('hyperlink') >= 0) {
                hyperlinkId = id;
                return true;
            }
            return false;
        });

        return hyperlinkId;
    }

    /**
     * Returns the document default paragraph list stylesheet id.
     *
     * @returns {String|Null}
     */
    getDefaultUIParagraphListStylesheet() {
        const styleNames = this.paragraphStyles.getStyleSheetNames();
        let paragraphListId = null;

        _(styleNames).find((name, id) => {
            const lowerName = name.toLowerCase();
            if (lowerName.indexOf('list paragraph') === 0 || lowerName.indexOf('listparagraph') === 0) {
                paragraphListId = id;
                return true;
            }
        });

        return paragraphListId;
    }

    /**
     * Finding a good text color, when the user selects "auto". The filter cannot save
     * "auto" as text color in shapes (OX Presentation and OX Spreadsheet, ooxml).
     * Therefore this function returns a rgb value that can be used instead and that can
     * be saved by the filter (DOCS-3248).
     *
     * @returns {Object} textcolor
     *  A color object with the properties 'type' and 'value'.
     */
    findTextAutoColorForTextframe() {

        // the text color
        let textcolor = Color.BLACK;
        // an array containing all affected fill colors
        const allFillColors = [];
        // the set of drawing attributes
        let drawingAttrs = null;
        // the drawing that contains the text selection
        const drawing = this.#selection.isDrawingSelection() ? this.#selection.getSelectedDrawing() : getClosestDrawingTextframe(this.getCurrentRootNode(), this.#selection.getStartPosition());

        if (drawing && drawing.length) {
            drawingAttrs = this.drawingStyles.getElementAttributes(drawing); // the full attribute set for the selected drawing
            if (drawingAttrs.fill.type === 'solid') {
                allFillColors.push(drawingAttrs.fill.color);
                textcolor = opRgbColor(this.parseAndResolveTextColor(Color.AUTO, allFillColors).hex);
            }
        }

        return textcolor;
    }

    /**
     * Optimizes the actions before they will be sent to the server.
     * Especially consecutive 'insertText' operations can be merged. This
     * function is a callback that is started from 'sendActions' in the
     * application.
     *
     * @param {Array<Object>} actions
     *  An array with all actions to be optimized.
     *
     * @returns {Array}
     *  An array with optimized actions.
     */
    optimizeActions(actions) {

        // the resulting merged actions
        const newActions = [];

        // merging insertText operations, process all passed actions and reduce number of actions
        actions.forEach(nextAction => {

            // the 'insertText' operations to be merged (either variable may become null)
            const lastTextOp = this.#getSingleInsertTextOperation(_.last(newActions));
            const nextTextOp = this.#getSingleInsertTextOperation(nextAction);

            // check that the operations are valid for merging:
            // - both operations must refer to the same paragraph in the same document target
            // - next operation has no attributes and the previous operation is not a change track operation,
            //      or the following two operations both are change track operations
            if (lastTextOp && nextTextOp && operationsHaveSameParentComponent(lastTextOp, nextTextOp) && canMergeNeighboringOperations(lastTextOp, nextTextOp)) {

                // the text contents of the last and next operation
                const lastText = lastTextOp.text;
                const nextText = nextTextOp.text;

                // the distance between start character indexes of the last and next operation
                const distance = _.last(nextTextOp.start) - _.last(lastTextOp.start);

                // new text can be inserted between existing text (splice new text into the preceding text),
                // this includes the special case that the new text will be appended to the preceding text
                if ((distance >= 0) && (distance <= lastText.length)) {

                    // splice the new text into the existing text
                    lastTextOp.text = lastText.slice(0, distance) + nextText + lastText.slice(distance);

                    // increasing the operation length
                    if (lastTextOp.opl && nextTextOp.opl) {
                        lastTextOp.opl += nextTextOp.opl;
                    }

                    // setting marker at 'removed' operation (to be filtered in debug console)
                    nextTextOp.dbg_merged = true;

                    // informing listeners about this merge
                    if (DEBUG) { this.trigger('operationtrace:merged:internal', nextTextOp); }

                    // setting marker at removed operation/action (this is required for the OT collection of these operations)
                    nextAction._OPTIMIZED_OPERATION_ = 1;

                    // do not add the new action to the result array
                    return;
                }
            }

            newActions.push(nextAction);
        });

        return newActions;
    }

    /**
     * A handler this is called before a document is reloaded. For OX Text and OX Presentation this handler return
     * the current selection, so that it can be restored after successful reload.
     *
     * @returns {Object}
     *  Returning an object that contains the current logical start position, the current logical end position and
     *  the currently active target.
     */
    prepareReloadDocument() {
        const docView = this.docApp.docView;
        return {
            selection: this.getSelectionState(),
            zoomType: docView.zoomState.type,
            zoomFactor: docView.zoomState.factor,
            scrollPosition: docView.saveScrollPosition ? docView.saveScrollPosition() : null,
            commentDisplayMode: this.docApp.isTextApp() ? (this.#commentLayer.getBeforeReloadDisplayMode() || this.#commentLayer.getDisplayMode()) : null
        };
    }

    /**
     * Getting the global property 'this.#requiresElementFormattingUpdate'.
     *
     * @returns {Boolean}
     *  Returning the value of the global property 'this.#requiresElementFormattingUpdate'.
     */
    requiresElementFormattingUpdate() {
        return this.#requiresElementFormattingUpdate;
    }

    /**
     * Setting the global property 'this.#requiresElementFormattingUpdate'.
     *
     * @param {Boolean} state
     *  Whether an update of element formatting is required.
     */
    setRequiresElementFormattingUpdate(state) {
        this.#requiresElementFormattingUpdate = state;
    }

    /**
     * Getting the global property 'this.#guiTriggeredOperation'.
     *
     * @returns {Boolean}
     *  Returning the value of the global property 'this.#guiTriggeredOperation'.
     */
    isGUITriggeredOperation() {
        return this.#guiTriggeredOperation;
    }

    /**
     * Setting the global property 'this.#guiTriggeredOperation'.
     *
     * @param {Boolean} state
     *  Whether the running process was triggered locally via GUI.
     */
    setGUITriggeredOperation(state) {
        this.#guiTriggeredOperation = state;
    }

    /**
     * Returns wheter editor is in page breaks mode, or without them.
     *
     * @returns {Boolean}
     *
     */
    isPageBreakMode() {
        return this.#pbState;
    }

    /**
     * Returns whether editor is in draft mode (limited functionality mode for small devices).
     *
     * TODO: The value of 'this.#draftModeState' is not handled by small devices! Small devices use
     *       draft mode, but this is forced and not handled by the state 'this.#draftModeState'. This
     *       state is only set, when the user selects 'View in draft mode' from the 'View' top
     *       bar pane. For small devices a further check of 'SMALL_DEVICE' is typically
     *       required. The latter should be made superfluous in the future, so that 'this.#draftModeState'
     *       is also set correctly on small devices.
     *
     * @returns {boolean}
     */
    isDraftMode() {
        return this.#draftModeState;
    }

    /**
     * Method that toggles and simulates Draftmode in web browser.
     *
     * @param {boolean} state
     *  The new state of the draft mode..
     */
    toggleDraftMode(state) {

        if (this.#draftModeState === state) { return this; }
        this.#draftModeState = state;

        this.docApp.docView.$contentRootNode.toggleClass('draft-mode', state);
        DOM.getPageContentNode(this.#editdiv).css({ width: '100%', backgroundColor: '', transform: '', transformOrigin: '' });
        this.docApp.docView.changeZoom('fixed', 1, { force: true, clean: true });
        this.togglePageBreakMode(!state);
        this.toggleForceInlineDrawings(!state);
    }

    /**
     * Turn page breaks on or off. Removes them from document, and makes cleanup.
     *
     * @param {Boolean} state
     *   New state. If false, toggles the page aligned drawings to inline drawings.
     *
     * @returns {Editor}
     *   A reference to this instance.
     */
    toggleForceInlineDrawings(state) {

        if (state) { // leaving draft-mode
            // update formatting of all drawings that have the marker that they are forced to be inline (the page aligned drawings)
            // and all other absolute positioned drawings (the paragraph aligned drawings)
            const formattingNodes = DOM.getPageContentNode(this.#editdiv).find(DrawingFrame.FORCE_INLINE_SELECTOR + ',' + DOM.ABSOLUTE_DRAWING_SELECTOR);

            // updating all collected drawings
            _.each(formattingNodes, drawing => { this.drawingStyles.updateElementFormatting(drawing); });
            this.trigger('update:absoluteElements');  // updating absolutely positioned drawings
        } else { // entering draft mode
            // updating all collected drawings in drawing layer (page aligned drawings)
            _.each(this.#drawingLayer.getDrawings(), drawing => {
                this.drawingStyles.updateElementFormatting(drawing);
                // problem sometimes occurs with 'position: absolute' at image node
                if (DrawingFrame.getDrawingType(drawing) === 'image') { $(drawing).find('img').css('position', ''); }
            });

            // also removing space maker nodes. These come from paragraph aligned drawings that are forced to be inline via CSS.
            DOM.getPageContentNode(this.#editdiv).find(DOM.DRAWING_SPACEMAKER_NODE_SELECTOR).remove();
        }

        return this;
    }

    /**
     * Turn page breaks on or off. Removes them from document, and makes cleanup.
     *
     * @param {Boolean} state
     *   New state. If false, toggles page breaks off.
     *
     * @returns {Editor}
     *   A reference to this instance.
     */
    togglePageBreakMode(state) {

        let splittedParagraphs;
        const pageContentNode = DOM.getPageContentNode(this.#editdiv);
        let spans;

        if (this.#pbState === state) { return this; }
        this.#pbState = state;

        if (!this.#pbState) {
            pageContentNode.find('.page-break').remove();
            pageContentNode.find('.pb-row').remove();
            pageContentNode.find('.tb-split-nb').removeClass('tb-split-nb');
            pageContentNode.find('.break-above').removeClass('break-above');
            pageContentNode.find('.last-on-page').removeClass('last-on-page');
            this.#editdiv.children('.header-wrapper, .footer-wrapper').addClass('hiddenVisibility');

            splittedParagraphs = pageContentNode.find('.contains-pagebreak');
            if (splittedParagraphs.length > 0) {
                _.each(splittedParagraphs, splittedItem => {
                    spans = $(splittedItem).find('.break-above-span').removeClass('break-above-span');
                    $(splittedItem).children('.break-above-drawing').removeClass('break-above-drawing');
                    this.#validateParagraphNode(splittedItem);
                    if (spans.length > 0) {
                        _.each(spans, span => {
                            mergeSiblingTextSpans(span);
                            mergeSiblingTextSpans(span, true);
                        });
                    }
                    $(splittedItem).removeClass('contains-pagebreak');
                });
            }
            pageContentNode.css({ paddingBottom: '' });
        } else {
            this.#editdiv.children('.header-wrapper, .footer-wrapper').removeClass('hiddenVisibility');
            this.#pageLayout.callInitialPageBreaks();
        }
        this.docApp.getView().recalculateDocumentMargin();
        return this;
    }

    /**
     * Setter for global state - if headers and footers are currently edited
     *
     *  @param {Boolean} state
     */
    setHeaderFooterEditState(state) {
        if (this.#headerFooterEditState !== state) {
            this.#headerFooterEditState = state;
        }
    }

    /**
     * Getter for global state of editing or not headers and footers.
     *
     * @returns {Boolean}
     *  Global state of editing or not headers and footer.
     */
    isHeaderFooterEditState() {
        return this.#headerFooterEditState;
    }

    /**
     * Checks if rendering of page breaks layout should be skipped or not.
     *
     * @returns {Boolean}
     *  Whether should rendering of the page breaks be skipped or not.
     */
    skipPageLayoutRendering() {
        return !this.isPageBreakMode() || SMALL_DEVICE || this.isDraftMode() || this.isTargetFunctionality();
    }

    /**
     * Setter for header/footer container root node
     *
     *  @param {jQuery} $node
     */
    setHeaderFooterRootNode($node) {
        this.#$headFootRootNode = $node;
        this.#rootNodeIndex = this.#editdiv.find('.header, .footer').filter('[data-container-id="' + this.#$headFootRootNode.attr('data-container-id') + '"]').index(this.#$headFootRootNode);
        this.#$parentOfHeadFootRootNode = this.#$headFootRootNode.parent();
        this.#selection.setNewRootNode($node);
    }

    /**
     * Getter for the parent node of the header/footer container root node
     *
     * @returns {jQuery}
     *  The parent node of the header/footer root node
     */
    getParentOfHeaderFooterRootNode() {
        return this.#$parentOfHeadFootRootNode;
    }

    /**
     * Getter for header/footer container root node
     *
     * @param {String} [target]
     *  if exists, find node by target id
     *
     * @returns {jQuery}
     */
    getHeaderFooterRootNode(target) {
        // if node is detached from DOM, fetch from parent reference in DOM
        if (target || !this.#$headFootRootNode.parent().parent().length) {
            // if it comes from operation, it will have target argument
            if (target) {
                if (target === this.#$headFootRootNode.attr('data-container-id')) { // if target is same as cached node reference, use this node
                    if (this.isHeaderFooterEditState()) {
                        return this.#$headFootRootNode;
                    }
                    this.#$headFootRootNode = this.#editdiv.find('.header, .footer').filter('[data-container-id="' + target + '"]').eq(this.#rootNodeIndex);
                } else { // otherwise use first node with that target

                    // leaving current edit state, because another header/footer is selected
                    this.#pageLayout.leaveHeaderFooterEditMode(this.#$headFootRootNode, this.#$headFootRootNode.parent());

                    // finding new header/footer
                    this.#$headFootRootNode = this.#editdiv.find('.header, .footer').filter('[data-container-id="' + target + '"]').eq(0);
                }
            } else {
                if (this.docApp.isEditable()) {
                    if (!this.isHeaderFooterEditState()) {
                        globalLogger.warn('Editor.getHeaderFooterRootNode failed to fetch valid node!');
                        return this.#editdiv;
                    }
                    this.#$headFootRootNode = this.#editdiv.find('.header, .footer').filter('[data-container-id="' + this.#$headFootRootNode.attr('data-container-id') + '"]').eq(this.#rootNodeIndex);
                } else {
                    // remote clients need to fetch template node
                    this.#$headFootRootNode = this.#pageLayout.getHeaderFooterPlaceHolder().children('[data-container-id="' + target + '"]').first();
                }
            }
            if (!this.#$headFootRootNode.length) {
                globalLogger.warn('Editor.getHeaderFooterRootNode not found!');
                return this.#editdiv;
            }

            this.#pageLayout.enterHeaderFooterEditMode(this.#$headFootRootNode);

            //this.#selection.resetSelection();
            this.#selection.setTextSelection(this.#selection.getFirstDocumentPosition());
        }

        return this.#$headFootRootNode;
    }

    /**
     * Getter method for currently active root container node.
     * This function is used by OX Text.
     * It is overwritten by OX Presentation.
     *
     * @param {String} [target]
     *  ID of root container node
     *
     * @returns {jQuery}
     *  Found node
     */
    getCurrentRootNode(target) {
        // target for operation - if exists, it's for ex. header or footer
        const currTarget = target || this.getActiveTarget();

        // container root node of header/footer
        return currTarget ? (this.isImportFinished() ? this.getHeaderFooterRootNode(target) : this.#pageLayout.getHeaderFooterPlaceHolder().children('[data-container-id="' + target + '"]')) : this.#editdiv;
    }

    /**
     * Getter method for root container node with passed target,
     * or if target is not passed, default this.#editdiv node.
     *
     * Info:
     * This function is used by OX Text.
     * It is overwritten by OX Presentation.
     *
     * @param {String} [target]
     *  ID of root container node
     *
     * @param {Number} [index]
     *  Cardinal number of target node in the document from top to bottom.
     *
     * @returns {jQuery}
     *  Found node
     */
    getRootNode(target, index) {

        if (!target) { return this.#editdiv; }

        // headers&footers
        return this.#pageLayout.getRootNodeTypeHeaderFooter(target, index);
    }

    /**
     * Public method interface for private function #extendPropertiesWithTarget.
     *
     * @param {Object} object
     *   object that is being extended
     *
     * @param {Array<String>} target
     *   Id referencing to header or footer node
     */
    extendPropertiesWithTarget(object, target) {
        return this.#extendPropertiesWithTarget(object, target);
    }

    /**
     * This function is also available as private function. Check is
     * necessary, if this really must be a public function. Using
     * revealing pattern here.
     *
     * Prepares the text span at the specified logical position for
     * insertion of a new text component or character. Splits the text span
     * at the position, if splitting is required. Always splits the span,
     * if the position points between two characters of the span.
     * Additionally splits the span, if there is no previous sibling text
     * span while the position points to the beginning of the span, or if
     * there is no next text span while the position points to the end of
     * the span.
     *
     * @param {Number[]} position
     *  The logical text position.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.isInsertText=false]
     *      If set to true, this function was called from #implInsertText.
     *  @param {Boolean} [options.useCache=false]
     *      If set to true, the paragraph element saved in the selection can
     *      be reused.
     *  @param {Boolean} [options.allowDrawingGroup=false]
     *      If set to true, the element can also be inserted into a drawing
     *      frame of type 'group'.
     *
     * @param {String|Node} [target]
     *  ID of root node or the root node itself.
     *
     * @returns {HTMLSpanElement|Null}
     *  The text span that precedes the passed offset. Will be the leading
     *  part of the original text span addressed by the passed position, if
     *  it has been split, or the previous sibling text span, if the passed
     *  position points to the beginning of the span, or the entire text
     *  span, if the passed position points to the end of a text span and
     *  there is a following text span available or if there is no following
     *  sibling at all.
     *  Returns null, if the passed logical position is invalid.
     */
    prepareTextSpanForInsertion(position, options, target) {
        return this.#prepareTextSpanForInsertion(position, options, target);
    }

    /**
     * Method to check whether page break calculation are blocked and returned from it.
     *
     * @returns {Boolean}
     *  True, if calculation should stop running.
     */
    checkQuitFromPageBreaks() {
        return this.#quitFromPageBreak;
    }

    /**
     * Set block on running loops inside page breaks calculation, and immediately returns from it.
     * It is called from functions that have bigger priority than page breaks: such as text insert and delete.
     *
     * @param {Boolean} state
     *  New state
     */
    setQuitFromPageBreaks(state) {
        this.#quitFromPageBreak = state;
    }

    /**
     * Check if debounced method should skip insertPageBreaks function, but run other piggybacked, like updateAbsolutePositionedDrawings.
     * This is different from checkQuitFromPageBreaks, as it block page breaks inside debounced method.
     *
     * @returns {Boolean}
     *  True, if debounced method should skip call of insertPageBreaks.
     */
    getBlockOnInsertPageBreaks() {
        return this.#blockOnInsertPageBreaks;
    }

    /**
     * Sets block on pageLayout.insertPageBreak function, that is evaluated inside this.#insertPageBreaksDebounced.
     * This is different from setQuitFromPageBreaks, as it block page breaks inside debounced method,
     * not inside insertPageBreaks function.
     *
     * @param {Boolean} state
     *  New state
     */
    setBlockOnInsertPageBreaks(state) {
        if (this.isImportFinished()) {
            this.#blockOnInsertPageBreaks = state;
        }
    }

    /**
     * Getter to fetch global variable node currentProcessingNode,
     * which is set during direct call of inserting page breaks, #registerPageBreaksInsert.
     *
     * @returns {jQuery|Node}
     */
    getCurrentProcessingNode() {

        let firstInCollection;
        let firstIndex;
        let lastInCollection;
        let lastIndex;
        let currentProcessingNode = null;

        // group of lot of operations, e.g. setAttributes,
        // we need to start pagination not from last, but from first (higher in DOM, with lower index)
        if (this.#currentProcessingNodeCollection.length) {
            firstInCollection = this.#currentProcessingNodeCollection[0];
            lastInCollection = _.last(this.#currentProcessingNodeCollection);
            firstIndex = $(firstInCollection).index();
            lastIndex = $(lastInCollection).index();
            if (firstIndex > -1 && lastIndex > -1) { // both elements are in DOM
                currentProcessingNode = lastIndex < firstIndex ? lastInCollection : firstInCollection;
            } else {
                if (firstIndex < 0 && lastIndex > -1) { // first elem is not in DOM & last is
                    currentProcessingNode = lastInCollection;
                }
                if (lastIndex < 0 && firstIndex > -1) { // last el is not in DOM & first is
                    currentProcessingNode = firstInCollection;
                }
            }
        }

        return currentProcessingNode;
    }

    /**
     * Public method to get access to debounced insertPageBreaks call.
     *
     * @param {jQuery|Node} [node]
     *  Node from where to start page breaks calculation (top-down direction).
     *
     * @param {jQuery} [textframe]
     *  An optional text frame node.
     */
    insertPageBreaks(node, textframe) {
        this.#insertPageBreaksDebounced(node, textframe);
    }

    /**
     * Getter for global variable containing target of operation (header or footer target id).
     *
     * @returns {String}
     *  this.#activeTarget
     */
    getActiveTarget() {
        return this.#activeTarget;
    }

    /**
     * Setter for target of operation (header or footer target id).
     *
     * @param {String} value
     *  sets passed value to this.#activeTarget
     */
    setActiveTarget(value) {
        this.#activeTarget = value || '';
    }

    /**
     * Getter for the keyboard block property.
     */
    getBlockKeyboardEvent() {
        return this.#blockKeyboardEvent;
    }

    /**
     * Setting a blocker for the incoming keyboard events. This is necessary
     * for long running processes.
     */
    setBlockKeyboardEvent(value) {
        this.#blockKeyboardEvent = value;
        this.setInternalOperationBlocker(value); // OT: Blocking or allowing external operations
    }

    /**
     * Wheter the IME blocker is currently active (Safari browser only, DOCS-2146).
     */
    isIMEBlockActive() {
        return this.#imeBlock;
    }

    /**
     * Setting a blocker that is set in IME mode on Safari. This is required, because there must not
     * be any operation generation between '#processCompositionEnd' and the asynchronously called
     * '#postProcessCompositionEnd'. Sometimes the Safari browser allows, that the 'keydown' and
     * 'keypress' handlers are executed between 'processcompositionend' and 'postprocesscompositionend'.
     * To avoid this behavior, this IME blocker is used (DOCS-2146).
     *
     * @param {Boolean} value
     *  Activate or deactivate the imeBlock.
     */
    setIMEBlock(value) {
        this.#imeBlock = value;
    }

    /**
     * Whether the function '#postProcessCompositionEnd' was already called with the parameter
     * 'this.#safariIMESyncCall'. This check is required for Safari only (DOCS-2146).
     */
    wasSafariIMESyncCall() {
        return this.#safariIMESyncCall;
    }

    /**
     * Setting a marker at the function 'postprocesscompositionend' that it was already called before
     * the asynchronous call triggered in '#processCompositionEnd' runs. Then this function is not
     * executed twice. This check is required for Safari only (DOCS-2146).
     *
     * @param {Boolean} value
     *  Setting or removing the marker for the function call.
     */
    setSafariIMESyncCall(value) {
        this.#safariIMESyncCall = value;
    }

    /**
     * Helper function to avoid that the keyboard closes too often in touch devices because of
     * focus changes. This happens especially often on Android devices in OX Presentation.
     *
     * @returns {Boolean}
     *  Whether the focus must not be changed.
     */
    forceToKeepFocus() {
        return _.browser.Android && TOUCH_DEVICE && this.docApp.isPresentationApp();
    }

    /**
     * public listeners for IPAD workaround:
     * so we can unlisten all function on this.#editdiv
     * and assign them new on editdivs parent
     */
    getListenerList() {
        return this.#listenerList;
    }

    /**
     * Check, whether the start position of the current selection is inside
     * the specified (paragraph) node. In this case the start position has
     * to start with the logical position of the node.
     *
     * @param {Node|jQuery} [node]
     *  The DOM node to be checked. If this object is a jQuery collection,
     *  uses the first DOM node it contains.
     *
     * @returns {Boolean}
     *  Whether the start of the selection is located inside the specified
     *  node.
     */
    selectionInNode(node) {

        // the node corresponding to the current selection
        const selectionPoint = getDOMPosition(this.getCurrentRootNode(), this.#selection.getStartPosition());
        // whether the selection is inside the specified node
        let selectionInsideNode = false;

        if (selectionPoint && selectionPoint.node) {
            selectionInsideNode = containsNode(node, selectionPoint.node, { allowSelf: true });
        }

        return selectionInsideNode;
    }

    /**
     * Helper function that can be called, if a new valid text position is required. This
     * might be the case after some content is deleted, for example after accepting or
     * rejecting a change track.
     * Before setting a new text selection it is checked, if the current selection is
     * invalid.
     */
    setValidSelectionIfRequired() {

        let validPos = 0;

        if (!isValidTextPosition(this.getCurrentRootNode(), this.#selection.getStartPosition())) {
            validPos = findValidSelection(this.getCurrentRootNode(), this.#selection.getStartPosition(), this.#selection.getEndPosition(), this.#selection.getLastDocumentPosition(), this.useSlideMode());
            this.#selection.setTextSelection(validPos.start, validPos.end);
        }
    }

    /**
     * Public method to access this.#updateChangeTracksDebounced function outside of Editor class.
     */
    updateChangeTracksDebounced() {
        this.#updateChangeTracksDebounced();
    }

    /**
     * Public method to access this.#updateChangeTracksDebouncedScroll function outside of Editor class.
     */
    updateChangeTracksDebouncedScroll() {
        this.#updateChangeTracksDebouncedScroll();
    }

    /**
     * Public method to access this.#updateEditingHeaderFooterDebounced function outside of Editor class.
     */
    updateEditingHeaderFooterDebounced(node) {
        return this.#updateEditingHeaderFooterDebounced(node);
    }

    /**
     * Public method to access this.#updateRepeatedTableRowsDebounced function outside of Editor class.
     */
    updateRepeatedTableRowsDebounced(node) {
        return this.#updateRepeatedTableRowsDebounced(node);
    }

    /**
     * Unique collection for updating of marginal nodes is emptied, which leads to canceling of debounced update.
     */
    cancelDebouncedUpdatingHeaders() {
        this.#targetNodesForUpdate = $();
    }

    /**
     * Public method to check if there is pending debounced call to header update.
     */
    isPendingDebouncedHeaderUpdate() {
        return this.#targetNodesForUpdate.length > 0;
    }

    /**
     * Public method to access searchHandler outside of Editor class.
     */
    getSearchHandler() {
        return this.#searchHandler;
    }

    /**
     * Public method to access spellchecker outside of Editor class.
     */
    getSpellChecker() {
        return this.#spellChecker;
    }

    /**
     * Updating the models for drawings in drawing layer, comments, complex fields or range
     * markers. If a node like a paragraph, a table row, a table cell, a complete table or a
     * header or footer is removed, it is necessary to check, if the collections of drawing
     * layer, comments, range markers or complex fields are affected.
     * Additionally the drawings and comments inside their layers need to be removed, if a
     * placeholder is removed.
     *
     * @param {HTMLElement|jQuery} parentNode
     *  The node (paragraph, table, row, cell, ...) that will be checked for specific elements
     *  that are stored in models.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.marginal=false]
     *      If set to true, this updating of models was triggered by a change of header or footer.
     */
    updateCollectionModels(parentNode, options) {

        // whether this is a marginal model update
        const marginal = getBooleanOption(options, 'marginal', false);

        if (this.useSlideMode()) {
            if (this.drawingStyles.deletePlaceHolderAttributes) {
                this.drawingStyles.deletePlaceHolderAttributes(parentNode);
            }
        }

        // checking, if the paragraph contains comment place holder nodes. In this case the comment
        // needs to be removed from comment layer, too.
        if (this.docApp.isTextApp() && (!marginal || this.docApp.isODF())) { this.#commentLayer.removeAllInsertedCommentsFromCommentLayer(parentNode); }

        // removing an optional drawing selection (DOCS-2815)
        if (this.docApp.isTextApp()) {
            for (const drawingNode of $(parentNode).find(DrawingFrame.NODE_SELECTOR).get()) {
                this.#selection.clearDrawingSelection(drawingNode);
            }
        }

        // checking, if the paragraph contains drawing place holder nodes. In this case the drawings
        // needs to be removed from drawing layer, too.
        if (!marginal) { this.#drawingLayer.removeAllInsertedDrawingsFromDrawingLayer(parentNode); }

        // checking, if the paragraph contains additional range marker nodes, that need to be removed, too.
        this.#rangeMarker.removeAllInsertedRangeMarker(parentNode);

        // checking, if the paragraph contains additional field nodes, that need to be removed.
        this.#fieldManager.removeAllFieldsInNode(parentNode);

        // updating the slide model
        if (DOM.isSlideNode(parentNode)) { this.removeFromSlideModel(DOM.getTargetContainerId(parentNode)); }
    }

    /**
     * Adding an paragraph object to the 'this.#paragraphQueueForColoredMarginAndBorder'.
     * This store contains paragraphs, for which the background color
     * and border formatting needs to be checked.
     *
     * @param {jQuery} paragraph
     *  A paragraph that needs to be updated for correct color and border formatting.
     *
     * @param {Object} [mergedAttributes=null]
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    addParagraphToStore(paragraph, mergedAttributes) {
        // add to queue (set attributes to null when not specified)
        this.#paragraphQueueForColoredMarginAndBorder.push({ paragraph, mergedAttributes: mergedAttributes || null });
    }

    /**
     * Starting to format all paragraphs that are in 'this.#paragraphQueueForColoredMarginAndBorder'.
     * The queue is cleared afterwards.
     *
     * Note: There are two cases: 1) the queue was filled at undoRedo, 2) the queue was filled document loading
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.filledAtUndoRedo=false]
     *      Whether the 'this.#paragraphQueueForColoredMarginAndBorder' was filled at undoRedo.
     */
    startParagraphMarginColorAndBorderFormating(options) {

        const undo = getBooleanOption(options, 'filledAtUndoRedo', false);

        this.#paragraphQueueForColoredMarginAndBorder.forEach(paraObj => {
            // when the queue was filled at undoRedo use this
            if (undo) {
                this.implParagraphChanged(paraObj.paragraph);
            //... at document loading use this
            } else {
                this.paragraphStyles.applyElementFormatting(paraObj.paragraph, paraObj.mergedAttributes, { duringImport: true });
            }
        });

        // clear the queue
        this.#paragraphQueueForColoredMarginAndBorder = [];
    }

    /**
     * Create the operations for the new page attributes.
     *
     * @param {Object} pageAttrs the attributes to set
     * @param {Object} initialPageAttrs the initial page attributes
     */
    setPaperFormat(pageAttrs, initialPageAttrs) {

        const newPageAttrs = {};

        _.each(pageAttrs, (value, key) => {
            if (value !== initialPageAttrs[key]) {
                newPageAttrs[key] = _.isNumber(value) ? math.roundp(value, 10) : value;
            }
        });

        if (!_.isEmpty(newPageAttrs)) {
            if (this.useSlideMode()) {

                return this.executeDelayed(() => { this.reformatDocument(newPageAttrs); });

            } else {

                this.#addPageOrientationIfRequired(newPageAttrs, pageAttrs);

                return this.createAndApplyOperations(generator => {

                    generator.generateOperation(Op.CHANGE_CONFIG, { attrs: { page: newPageAttrs } });

                    const createTabStopOp = styleId => {

                        if (this.paragraphStyles.isDirty(styleId)) { return; }

                        const styleAttrs = this.paragraphStyles.getStyleSheetAttributeMap(styleId);
                        if (styleAttrs.paragraph && styleAttrs.paragraph.tabStops) {

                            const oldWidth = initialPageAttrs.width - initialPageAttrs.marginLeft - initialPageAttrs.marginRight;
                            const newWidth = pageAttrs.width - pageAttrs.marginLeft - pageAttrs.marginRight;

                            _.each(styleAttrs.paragraph.tabStops, tabStop => {
                                const relPos = tabStop.pos / oldWidth;
                                tabStop.pos = Math.round(relPos * newWidth);
                            });

                            generator.generateOperation(Op.CHANGE_STYLESHEET, { styleId, attrs: styleAttrs, type: 'paragraph' });
                        }
                    };

                    // Helper function to update all drawings that have a size that is related to the page size
                    const updateDrawingsWithRelativeSize = () => {

                        // leaving an optionally activated header or footer edit mode
                        this.getPageLayout().leaveHeaderFooterAndSetSelection();

                        // generating operations for all drawings that have a relative size (to the page)
                        _.each(this.#collectRelativeSizeDrawingNodes(), drawingNode => {

                            // the marginal target node of the drawing
                            const targetNode = DOM.getMarginalTargetNode(this.getNode(), drawingNode);
                            // an optional target
                            const target = targetNode.length > 0 ? DOM.getTargetContainerId(targetNode[0]) : undefined;
                            // the root node of the drawing
                            const rootNode = targetNode.length > 0 ? targetNode : this.getNode();
                            // the logical position of the drawing
                            const drawingPos = getOxoPosition(rootNode, drawingNode, 0);
                            // the new drawing size after the change of the page size
                            let newDrawingSize = null;
                            // the attributes of the drawing
                            const drawingAttrs = this.drawingStyles.getElementAttributes(drawingNode);

                            if (drawingAttrs.drawing.sizeRelHFrom === 'page' && _.isNumber(initialPageAttrs.width) && _.isNumber(pageAttrs.width) && _.isNumber(drawingAttrs.drawing.pctWidth) && initialPageAttrs.width !== pageAttrs.width) {
                                newDrawingSize = { width: Math.round((drawingAttrs.drawing.pctWidth / 100000) * pageAttrs.width) };
                            }

                            if (drawingAttrs.drawing.sizeRelVFrom === 'page' && _.isNumber(initialPageAttrs.height) && _.isNumber(pageAttrs.height) && _.isNumber(drawingAttrs.drawing.pctHeight) && initialPageAttrs.height !== pageAttrs.height) {
                                newDrawingSize = newDrawingSize || {};
                                newDrawingSize.height = Math.round((drawingAttrs.drawing.pctHeight / 100000) * pageAttrs.height);
                            }

                            if (newDrawingSize) {
                                generator.generateOperation(Op.SET_ATTRIBUTES, { start: _.copy(drawingPos), target, attrs: { drawing: newDrawingSize }, relativesize: true }); // TODO: target handling
                                if (target) { this.setInactiveLocalHeaderFooterOp(true); }
                            }
                        });
                    };

                    createTabStopOp(this.getDefaultHeaderTextDefinition().styleId);
                    createTabStopOp(this.getDefaultFooterTextDefinition().styleId);

                    // update all drawings with relative size and offset (to the page)
                    updateDrawingsWithRelativeSize();

                }, { undoMode: 'skip' }).then(() => {
                    this.setBlockOnInsertPageBreaks(false);
                    this.setInactiveLocalHeaderFooterOp(false);
                });
            }

        }

        return $.when();
    }

    /**
     * Setting those states that are required when a drawing is currently modified
     * (resize, move, rotate).
     * If the state is true, update of drawings (change of drawing height, font size,
     * ...) is not always possible and external operations need to be blocked.
     *
     * @param {Boolean} state
     *  The new state for handling of currently modified drawings.
     */
    setDrawingChangeOTHandling(state) {
        if (this.docApp.isOTEnabled()) {
            this.setDrawingChangeActive(state);
            this.setInternalOperationBlocker(state); // blocking external operations
        }
    }

    /**
     * Searching a border style that fits into the table, when new borders are
     * inserted via the border style picker (DOCS-2440).
     *
     * @param {Obejct} tableAttrs
     *  The set of attributes of the family 'table' of the table, that contains
     *  the selection.
     *
     * @param {Object} defaultBorder
     *  The border that will be used, if not better border was found within this
     *  function.
     *
     * @returns {Object}
     *  A border style that fits to the table containing the selection. If none
     *  is found, the specified default border is returned.
     */
    getDefaultBorderStyle(tableAttrs, defaultBorder) {

        // the border style that will be calculated within this function
        let borderStyle = null;
        // the table node that contains the selection
        let table = null;
        // the style ID of the table
        let styleId = null;
        // the attributes of the table style
        let styleAttributes = null;
        // the cell attributes of the table style
        let cellStyleAttributes = null;
        // the table attributes of the table style
        let tableStyleAttributes = null;
        // a saved border style saved at the table after loading (odt only)
        let savedBorderStyle = null;

        // step 1: Checking for existing borders in the table -> using the first found already used border
        const CELL_BORDER_NAMES = ['borderTop', 'borderBottom', 'borderLeft', 'borderRight', 'borderInsideHor', 'borderInsideVert'];

        _.each(CELL_BORDER_NAMES, borderName => {
            if (!borderStyle && tableAttrs[borderName] && tableAttrs[borderName].style !== 'none') {
                borderStyle = tableAttrs[borderName];
            }
        });

        // step 2: Analyzing the table style -> searching for border styles for the whole table
        if (!borderStyle) {

            table = this.#selection.getEnclosingTable() || (this.#selection.isTableDrawingSelection() && this.#selection.getSelectedDrawing());

            if (table) {
                styleId = getElementStyleId(table);
                if (styleId) {
                    styleAttributes = this.tableStyles.getStyleSheetAttributeMap(styleId);
                    // evaluating only 'wholeTable' and 'cell' attributes
                    if (styleAttributes.wholeTable && styleAttributes.wholeTable.cell) { // cell attributes for whole table
                        cellStyleAttributes = styleAttributes.wholeTable.cell;
                        _.each(CELL_BORDER_NAMES, borderName => {
                            if (!borderStyle && cellStyleAttributes[borderName] && cellStyleAttributes[borderName].style !== 'none') {
                                borderStyle = cellStyleAttributes[borderName];
                            }
                        });
                    }

                    if (!borderStyle && styleAttributes.wholeTable && styleAttributes.wholeTable.table) { // table attributes for whole table
                        tableStyleAttributes = styleAttributes.wholeTable.table;
                        _.each(CELL_BORDER_NAMES, borderName => {
                            if (!borderStyle && tableStyleAttributes[borderName] && tableStyleAttributes[borderName].style !== 'none') {
                                borderStyle = tableStyleAttributes[borderName];
                            }
                        });
                    }
                }
            }
        }

        // step 3 (odt only, DOCS-2448)
        if (!borderStyle && this.docApp.isODF() && this.docApp.isTextApp() && table && $(table).data('odtBorderStyle')) {
            savedBorderStyle = JSON.parse($(table).data('odtBorderStyle'));
            if (savedBorderStyle && savedBorderStyle.style && savedBorderStyle.style !== 'none') {
                borderStyle = savedBorderStyle;
            }
        }

        // step 4: Taking the suggested default border
        if (!borderStyle) { borderStyle = defaultBorder; }

        return borderStyle;
    }

    /**
     * Public helper function to call '#postProcessCompositionEnd' synchronously. This is required
     * only for the Safari browser, if the 'keydown' handler is called between '#processCompositionEnd'
     * and '#postProcessCompositionEnd'. This leads to a broken document (DOCS-2146). This problems can
     * be avoided by running this function synchronously at the beginning of the 'keydown' handler.
     *
     * For the options parameter see at the private function '#postProcessCompositionEnd'.
     */
    postProcessCompositionEnd(options) {
        this.#postProcessCompositionEnd(options); // calling private function with marker
    }

    // ==================================================================
    // END of Editor API
    // ==================================================================

    // ==================================================================
    // Private functions
    // ==================================================================

    /**
     * Sets the state whether a cipboard paste is in progress
     *
     * @param {Boolean} state
     *  The clipboard pasting state
     */
    #setClipboardPasteInProgress(state) {
        this.#pasteInProgress = state;
    }

    /**
     * Returns an 'insertText' operation from the passed action, if it
     * exists and is the only operation in the action.
     *
     * @param {Object} action
     *  A document action with an operations array.
     *
     * @returns {Object|Null}
     *  An 'insertText' operation from the passed action, if it exists and
     *  is the only operation in the action; otherwise null.
     */
    #getSingleInsertTextOperation(action) {
        const singleOp = (_.isObject(action) && _.isArray(action.operations) && (action.operations.length === 1)) ? action.operations[0] : null;
        return (singleOp && (singleOp.name === Op.TEXT_INSERT)) ? singleOp : null;
    }

    /**
     * OT: Handler for OT Collisions.
     * Trying to restore the browser selection.
     */
    #otCollisionHandler() {
        const startPos = this.#selection.getStartPosition();
        const endPos = this.#selection.getEndPosition();
        otLogger.info('$badge{Editor} Collision: otCollisionHandler');
        otLogger.info('$badge{Editor} Collision: Resetting selection: ' + JSON.stringify(startPos) + ' : ' + JSON.stringify(endPos));
        otLogger.info('$badge{Editor} Collision: otCollisionHandler');
        this.#selection.restoreBrowserSelection(); // useless, if setTextSelection is called after applyOperations
        this.executeDelayed(() => {
            otLogger.info('$badge{Editor} Collision: Setting selection asynchronously: ' + JSON.stringify(startPos) + ' : ' + JSON.stringify(endPos));
            this.#selection.setTextSelection(startPos, endPos); // trying to repair the selection asynchronously
        }, 0);
    }

    /**
     * Checks stored character styles of a document and adds 'missing'
     * character styles (e.g. hyperlink).
     */
    #insertMissingCharacterStyles() {
        const styleNames = this.characterStyles.getStyleSheetNames();
        const parentId = this.characterStyles.getDefaultStyleId();
        let hyperlinkMissing = true;

        _(styleNames).find(name => {
            const lowerName = name.toLowerCase();
            if (lowerName.indexOf('hyperlink') >= 0) {
                hyperlinkMissing = false;
                return true;
            }
            return false;
        });

        if (hyperlinkMissing) {
            const hyperlinkAttr = { character: this.getDefaultLateralHyperlinkAttributes() };
            const hyperlinkDef = this.getDefaultLateralHyperlinkDefinition();
            this.characterStyles.createDirtyStyleSheet(hyperlinkDef.styleId, hyperlinkDef.styleName, hyperlinkAttr, { parent: parentId, priority: hyperlinkDef.uiPriority });
        }
    }

    /**
     * Check the stored paragraph styles of a document and adds 'missing'
     * heading / default and other paragraph styles.
     */
    #insertMissingParagraphStyles() {

        // the number extensions for the supported headings
        let headings = [0, 1, 2, 3, 4, 5];
        // the paragraph style names
        const styleNames = this.paragraphStyles.getStyleSheetNames();
        // the default paragraph style id
        let parentId = this.paragraphStyles.getDefaultStyleId();
        // whether a default style is already defined after loading
        const hasDefaultStyle = _.isString(parentId) && (parentId.length > 0);
        // the id of the paragraph list style
        let paragraphListStyleId = null;
        // the id of the paragraph comment style
        let commentStyleId = null;
        // the id of the paragraph header style
        let headerStyleId = null;
        // the id of the paragraph footer style
        let footerStyleId = null;
        // the default style id of a visible paragraph
        let visibleParagraphDefaultStyleId = null;
        // the default style id of a hidden paragraph
        let hiddenDefaultStyle = false;
        // the default paragraph definition
        let defParaDef;
        // the comment paragraph definition
        let commentDef = null;
        // the comment paragraph attributes
        let commentAttrs = null;
        // the header paragraph definition
        let headerDef = null;
        // the footer paragraph definition
        let footerDef = null;
        // header/footer style tab position right
        const rightTabPos = this.#pageLayout.getPageAttribute('width') - this.#pageLayout.getPageAttribute('marginLeft') - this.#pageLayout.getPageAttribute('marginRight');
        // header/footer style tab position center
        const centerTabPos = rightTabPos / 2;
        // the header paragraph attributes
        let headerAttrs = null;
        // the footer paragraph attributes
        let footerAttrs = null;

        if (!hasDefaultStyle) {
            // add a missing default paragraph style
            defParaDef = this.getDefaultParagraphStyleDefinition();
            this.paragraphStyles.createDirtyStyleSheet(defParaDef.styleId, defParaDef.styleName, {}, { priority: 1, defStyle: defParaDef.default });
            parentId = defParaDef.styleId;
            visibleParagraphDefaultStyleId = parentId;
        } else {
            hiddenDefaultStyle = this.paragraphStyles.isHidden(parentId);
            if (!hiddenDefaultStyle) {
                visibleParagraphDefaultStyleId = parentId;
            }
        }

        // Search through all paragraph styles to find out what styles
        // are missing.
        _(styleNames).each((name, id) => {
            const styleAttributes = this.paragraphStyles.getStyleAttributeSet(id).paragraph;
            const outlineLvl = styleAttributes.outlineLevel;
            const lowerName = name.toLowerCase();
            const lowerId = id.toLowerCase();

            if (_.isNumber(outlineLvl) && (outlineLvl >= 0 && outlineLvl < 6)) {
                headings = _(headings).without(outlineLvl);
            }

            if (!paragraphListStyleId) {
                if (lowerName.indexOf('list paragraph') === 0 || lowerName.indexOf('listparagraph') === 0) {
                    paragraphListStyleId = id;
                }
            }

            if (!commentStyleId) {
                if (lowerName.indexOf('annotation text') === 0) {
                    commentStyleId = id;
                }
            }

            if (!headerStyleId) {
                if (lowerName.indexOf('header') === 0) {
                    headerStyleId = id;
                }
            }

            if (!footerStyleId) {
                if (lowerName.indexOf('footer') === 0) {
                    footerStyleId = id;
                }
            }

            // Check and store the headings style which is used
            // by odf documents as seed for other heading styles.
            if (hiddenDefaultStyle && !visibleParagraphDefaultStyleId) {
                if (lowerId === 'standard' || lowerId === 'normal') {
                    visibleParagraphDefaultStyleId = id || parentId;
                }
            }
        });

        // add the missing paragraph heading styles using predefined values
        if (headings.length > 0) {
            const defaultCharStyles = this.getDefaultHeadingCharacterStyles();
            const headingsParentId = hiddenDefaultStyle ? visibleParagraphDefaultStyleId : parentId;
            const headingsNextId = hiddenDefaultStyle ? visibleParagraphDefaultStyleId : parentId;

            _(headings).each(level => {
                const attr = {};
                const charAttr = defaultCharStyles[level];
                attr.character = charAttr;
                attr.paragraph = { outlineLevel: level, nextStyleId: headingsNextId };
                this.paragraphStyles.createDirtyStyleSheet('heading' + (level + 1), 'heading ' + (level + 1), attr, { parent: headingsParentId, priority: 9 });
            });
        }

        // add missing paragraph list style
        if (!paragraphListStyleId) {
            this.paragraphStyles.createDirtyStyleSheet(
                'ListParagraph',
                'List Paragraph',
                { paragraph: { indentLeft: 1270, contextualSpacing: true, nextStyleId: 'ListParagraph' } },
                { parent: hiddenDefaultStyle ? (visibleParagraphDefaultStyleId || parentId) : parentId, priority: 34 });
        }

        // add missing comment style
        if (!commentStyleId) {
            commentDef = this.getDefaultCommentTextDefintion();
            commentAttrs = this.getDefaultCommentTextAttributes();
            this.paragraphStyles.createDirtyStyleSheet(commentDef.styleId, commentDef.styleName, commentAttrs, { parent: commentDef.parent, priority: commentDef.uiPriority, defStyle: commentDef.default });
        }

        // add missing header style
        if (!headerStyleId) {
            headerDef = this.getDefaultHeaderTextDefinition();
            headerAttrs = { paragraph: { tabStops: [{ value: 'center', pos: centerTabPos }, { value: 'right', pos: rightTabPos }] } };
            this.paragraphStyles.createDirtyStyleSheet(headerDef.styleId, headerDef.styleName, headerAttrs, { parent: headerDef.parent, priority: headerDef.uiPriority, defStyle: headerDef.default });
        }

        // add missing footer style
        if (!footerStyleId) {
            footerDef = this.getDefaultFooterTextDefinition();
            footerAttrs = { paragraph: { tabStops: [{ value: 'center', pos: centerTabPos }, { value: 'right', pos: rightTabPos }] } };
            this.paragraphStyles.createDirtyStyleSheet(footerDef.styleId, footerDef.styleName, footerAttrs, { parent: footerDef.parent, priority: footerDef.uiPriority, defStyle: footerDef.default });
        }
    }

    /**
     * Check the stored drawing styles of a document and adds 'missing' style. This is
     * currently only used for odt documents, where the insertion of a text frame requires
     * a corresponding drawing style.
     */
    #insertMissingDrawingStyles() {

        // the names of the drawing styles
        const styleNames = this.drawingStyles.getStyleSheetNames();
        // the default style Id
        let parentId = this.drawingStyles.getDefaultStyleId();
        // whether a default style is already defined
        const hasDefaultStyle = _.isString(parentId) && (parentId.length > 0);
        // whether the style for the text frames is missing
        let textframestyleMissing = true;
        // the drawing style definition
        let drawingDef = null;
        // the drawing style attributes
        let drawingAttrs = null;

        // adding the default style, if required
        if (!hasDefaultStyle) {
            drawingDef = this.getDefaultDrawingDefintion();
            drawingAttrs = this.getDefaultDrawingAttributes();
            this.drawingStyles.createDirtyStyleSheet(drawingDef.styleId, drawingDef.styleName, drawingAttrs, { priority: drawingDef.uiPriority, defStyle: drawingDef.default });
            parentId = drawingDef.styleId;
        }

        // checking the style used for text frames
        _(styleNames).find(name => {
            const lowerName = name.toLowerCase();
            if (lowerName.indexOf('frame') >= 0) {
                textframestyleMissing = false;
                return true;
            }
            return false;
        });

        if (textframestyleMissing) {
            drawingDef = this.getDefaultDrawingTextFrameDefintion();
            drawingAttrs = this.getDefaultDrawingTextFrameAttributes();
            // Info: Setting option hidden to true. Otherwise this style will replace the style 'default_drawing_style', so that for example line colors are modified.
            this.drawingStyles.createDirtyStyleSheet(drawingDef.styleId, drawingDef.styleId, drawingAttrs, { parent: parentId, priority: drawingDef.uiPriority, defStyle: drawingDef.default, hidden: true });
        }

    }

    // ====================================================================
    // Private functions for the hybrid edit mode
    // ====================================================================

    #processMouseUp(event) {

        // whether the document is opened in read-only mode
        const readOnly = !this.docApp.isEditable();
        // a drawing node and a tab node
        let drawing = null, drawingNode = null, tabNode = null, fieldNode = null;
        // logical positions
        let startPosition = null, oxoPosition = null, fieldPos = null;
        // whether this event was triggered by a right click
        const rightClick = (event.button === 2);
        // the currently active target
        let localActiveTarget = null;
        // whether a comment is active
        let isActiveComment = false;
        // the current root node
        let currentRootNode = null;

        this.setActiveDoubleClickEvent(false);

        // the mouse down already occured in the comments side pane -> will not be handled here
        if (this.#mouseDownInCommentsPane) { return true; }

        // do nothing in this mouseup handler, if mousedown happened on a bubble node (DOCS-3082)
        // -> the target of mouseup might have changed, if the opened comment shifts the document.
        // -> it is not possible to rely on the event.target of mouseup event.
        if (this.#mouseDownInCommentsBubbleNode) {
            this.setMouseDownInCommentsBubbleNode(false);
            // avoiding that the just opened comment is closed immediately by click handler
            // -> order: mousedown, mouseup, click
            this.ignoreClickEvent(true); // DOCS-3101
            this.executeDelayed(() => { this.ignoreClickEvent(false); }, 100);
            return true;
        }

        this.#activeMouseDownEvent = false;

        // ignoring this mouse up event handler (specified from the mousedown handler, DOCS-3423)
        if (this.#ignoreFollowingMouseUpEventHandler) {
            this.#ignoreFollowingMouseUpEventHandler = false;
            return true;
        }

        // in Presentation app on touch devices the edit mode might be activated
        if (this.docApp.isPresentationApp() && this.getSlideTouchMode()) {
            this.handleTouchEditMode(event);
            return;
        }

        // handle mouse events in edit mode only
        if (readOnly) {
            this.#selection.processBrowserEvent(event);
            return;
        }

        tabNode = $(event.target).closest(DOM.TAB_NODE_SELECTOR);
        if (tabNode.length) {
            localActiveTarget = this.getActiveTarget();
            isActiveComment = localActiveTarget && this.#commentLayer.isCommentId(localActiveTarget);

            if (isActiveComment || (this.#selection.getBrowserSelection().active && this.#selection.getBrowserSelection().active.isCollapsed())) { // hasRange cannot be used, as endPosition is still not set
                currentRootNode = this.getCurrentRootNode();

                // tabulator handler to decide if cursor goes before or after tab
                if (tabNode.offset().left + tabNode.width() / 2 < event.clientX) {
                    startPosition = getOxoPosition(currentRootNode, tabNode.next(), 0);
                    this.#selection.setTextSelection(startPosition);
                    return;
                } else {
                    startPosition = getOxoPosition(currentRootNode, tabNode, 0);
                    this.#selection.setTextSelection(startPosition);
                    return;
                }
            }
        }
        fieldNode = $(event.target).closest(DOM.FIELD_NODE_SELECTOR);
        if (!rightClick && fieldNode.length && this.#selection.getSelectionType() === 'text') { // #47584
            startPosition = this.#selection.getStartPosition();
            fieldPos = getOxoPosition(this.getCurrentRootNode(), fieldNode, 0);
            if (comparePositions(fieldPos, startPosition) < 0) {
                this.#selection.setTextSelection(fieldPos, startPosition);
            } else {
                this.#selection.setTextSelection(startPosition, increaseLastIndex(fieldPos));
            }
        }

        if (this.#selection.getSelectionType() === 'drawing') {
            drawingNode = $(event.target).closest(DrawingFrame.NODE_SELECTOR);

            if (rightClick && drawingNode.length === 0) {
                oxoPosition = getOxoPositionFromPixelPosition(this.#editdiv, event.pageX, event.pageY);
                this.#selection.setTextSelection(oxoPosition.start);
            } else if (!this.useSlideMode()) {
                // mouse up while drawing selected: selection does not change,
                // but scroll drawing completely into the visible area (not for presentation app, 49354)
                drawing = this.#selection.getSelectedDrawing(); // empty for multiple selection (if supported)

                if (drawing.length > 0) {
                    // on small devices scrolling to the drawing might be disturbing (67946)
                    const avoidScrolling = this.docApp.isTextApp() && SMALL_DEVICE && event.type === 'touchend';
                    if (!avoidScrolling) { this.docApp.getView().scrollToChildNode(drawing); }
                }
            }

            if (IOS_SAFARI_DEVICE) {
                // on the iPad when deselecting an image, processBrowserEvent is called in mouseDown,
                // but applyBrowserSelection is not. So we need to call processBrowserEvent also in mouseUp
                // to finally call applyBrowserSelection.
                // Info: This conflicts with following 'restoreBrowserSelection' (because of the asynchronity
                //       in 'processBrowserEvent'), so that 'restoreBrowserSelection' must not be called
                //       for IOS.
                //
                // DOCS-5318: Do not call "processBrowserEvent" when this is the "touchend" of a just selected
                // drawing. In this scenario the drawing is immediately deselected again, when the soft keyboard
                // is open.
                if (!drawingNode.length) { this.#selection.processBrowserEvent(event, { fromMouseUpHandler: true }); }
            } else if (_.browser.IE === 11 && !this.useSlideMode()) {
                // Avoiding windows frame around the drawing by repainting OX Text drawing frame
                // TODO: Is this still necessary?
                // -> removing this for text frames, because text selection will be disturbed
                if (!DrawingFrame.isTextFrameShapeDrawingFrame(drawing)) {
                    DrawingFrame.clearSelection(drawing);
                    this.#selection.drawDrawingSelection(drawing);
                }
            }

            // taking care that the browser selection is in the clipboard node, so that copy/paste is enabled (DOCS-1583).
            // -> this can only be triggered in mouseup, because setBrowserSelection is not updated during active mousedown.
            if (this.docApp.isOTEnabled()) {
                this.#selection.setActiveTracking(false);
                if (!IOS_SAFARI_DEVICE) { this.#selection.restoreBrowserSelection(); } // not on IOS, because it makes drawing deselection impossible
            }

        } else {
            if (rightClick && !this.#selection.hasRange() && !this.#selection.isMultiSelection()) {
                if (this.docApp.isPresentationApp() && DOM.isSlideNode(event.target)) { // shortcut for slide selections, without using the pixel API
                    if (_.browser.Safari) { return; } // context menu when clicked on slide in Safari gets killed by setTextSelection
                    oxoPosition = { start: _.copy(this.getActiveSlidePosition()) };
                    oxoPosition.start.push(0);
                } else {
                    oxoPosition = getOxoPositionFromPixelPosition(this.#editdiv, event.pageX, event.pageY); // using pixel API
                }

                if (this.docApp.isPresentationApp() && oxoPosition && oxoPosition.start && _.last(oxoPosition.start) > 0 && this.handleTemplateTextForNode(event.target)) { // taking care of template texts, that might not be replaced yet (61534)
                    oxoPosition.start[oxoPosition.start.length - 1] = 0;
                }
                if (oxoPosition) { this.#selection.setTextSelection(oxoPosition.start); }
            } else if (rightClick && this.#selection.hasRange() && this.#selection.getSelectionType() === 'text') {
                // context menu gets killed by focusChangeHandler if processBrowserEvent is called in this case, #49518
            } else {
                // calculate logical selection from browser selection, after browser has processed the mouse event
                this.#selection.processBrowserEvent(event, { fromMouseUpHandler: true });
            }
        }
    }

    /**
     * Event handler for scrolling tables horizontaly in draft mode
     */

    #startXpos = 0; // initial pageX value on touchstart for calculating event offset
    #currentXpos = 0; // temp pageX value got on touchmove
    #tableCssLeft = 0;

    #processTouchEventsForTableDraftMode(event) {

        const pageContentWidth = this.#editdiv.find(DOM.PAGECONTENT_NODE_SELECTOR).width();
        const currentTable = $(event.target).parents(DOM.TABLE_NODE_SELECTOR).last();
        const currentTableWidth = currentTable.width();

        switch (event.type) {
            case 'touchstart':
                if (event.originalEvent && event.originalEvent.changedTouches) {
                    this.#startXpos = event.originalEvent.changedTouches[0].pageX;
                }
                this.#tableCssLeft = parseInt(currentTable.css('left'), 10);
                break;
            case 'touchmove':
                if (event.originalEvent && event.originalEvent.changedTouches) {
                    this.#currentXpos = (event.originalEvent.changedTouches[0].pageX - this.#startXpos);
                }
                if (Math.abs(this.#currentXpos) < pageContentWidth / 2) {
                    currentTable.css({ left: this.#tableCssLeft + this.#currentXpos });
                } else {
                    currentTable.css({ left: (this.#currentXpos > 0) ? 0 : (pageContentWidth - currentTableWidth) });
                }
                break;
            case 'touchend':
                this.#tableCssLeft = parseInt(currentTable.css('left'), 10);
                if (this.#tableCssLeft > 0) {
                    currentTable.css({ left: 0 });
                } else if (this.#tableCssLeft < pageContentWidth - currentTableWidth) {
                    currentTable.css({ left: pageContentWidth - currentTableWidth });
                }
                this.#currentXpos = 0;
                break;
        }
    }

    #processHeaderFooterEdit(event) {
        if (!this.docApp.isEditable() || !this.isImportFinished()) {
            return;
        }
        event.stopPropagation();

        let $currTarget = $(event.currentTarget);
        let $refToParentNode = $();

        if ($currTarget.hasClass('cover-overlay')) {
            $currTarget = $currTarget.parents('.header, .footer');
        }

        if (this.isHeaderFooterEditState()) {
            if (this.getHeaderFooterRootNode()[0] === $currTarget[0]) {
                return;
            } else {
                this.#pageLayout.leaveHeaderFooterEditMode(this.getHeaderFooterRootNode());

                // store parent element as a reference (element itself is going to be replaced)
                $refToParentNode = $currTarget.parent();

                // must be direct call
                this.#pageLayout.updateEditingHeaderFooter();
            }
        }
        if (DOM.isHeaderOrFooter($currTarget)) {

            if ($refToParentNode.length > 0) {
                $currTarget = $refToParentNode.children('[data-container-id="' + $currTarget.attr('data-container-id') + '"]');
            }
            if (!$currTarget.length) {
                // from edit mode of one h/f we doubleclicked on other, which is not created yet
                $currTarget = $(event.currentTarget);
                if ($currTarget.hasClass('cover-overlay')) {
                    $currTarget = $currTarget.parents('.header, .footer');
                }
            }
            if (DOM.isHeaderOrFooter($currTarget)) {
                this.#pageLayout.enterHeaderFooterEditMode($currTarget);

                this.#selection.setNewRootNode($currTarget);
                this.#selection.setTextSelection(this.#selection.getFirstDocumentPosition());
            }
        } else {
            globalLogger.warn('Editor.processHeaderFooterEdit(): Failed to fetch header or footer');
        }
    }

    #processMouseUpOnDocument(event) {

        if (this.#activeMouseDownEvent) {
            this.#activeMouseDownEvent = false;
            this.#processMouseUp(event);
        }
    }

    // Registering keypress handler at the document for Chrome
    // to avoid problems with direct keypress to document
    // -> This handler is triggered once without event, therefore
    // the check for the event is required.
    #processKeypressOnDocument(event) {

        // Fix for the following scenario:
        // - local client uses Chrome browser
        // - local client selects drawing, without have edit privileges
        // - local client removes the focus from the Chrome browser
        // - remote client removes the selected drawing
        // - local client set browser focus by clicking on top of browser (or side pane)
        // - local client can type a letter direct with keypress into the document
        //   -> the letter is inserted into the document, no operation, no read-only warning
        // After removing the drawing in the remote client, the focus cannot be set correctly
        // in the local client, because the browser does not have the focus. Therefore the focus
        // remains in the clipboard and is then switched to the body element.

        const readOnly = !this.docApp.isEditable();

        // special handling for the following scenario:
        // - there is an event (once this handler is called without event)
        // - the target of the event was the body. So we cannot conflict with events triggered in the side pane, that bubble up to the body.
        // - the document is in read-only mode -> this might be removed in the future, if more than one user is able to edit the document.
        // - this edit application is visible
        // - the event is not triggered a second time -> this event is branded with the property '_triggeredAgain' to avoid endless loop.
        // - the currently focused element is the body element (which is a second test similar to the check of the event target).
        if (event && $(event.target).is('body') && readOnly && this.docApp.isActive() && !event._triggeredAgain && $(getFocus()).is('body')) {
            event.preventDefault();
            // Trigger the same event again at the div.page once more (-> read-only warning will be displayed)
            event.target = this.#editdiv[0];
            event._triggeredAgain = true;
            this.#editdiv.trigger(event);
        }
    }

    // Fix for 29751: IE supports double click for word selection
    #processDoubleClick(event) {

        // the boundaries of the double-clicked word
        let wordBoundaries = null;

        this.setActiveDoubleClickEvent(true);

        if (_.browser.IE) {

            if (DOM.isTextSpan(event.target)) {
                this.#doubleClickEventWaiting = true;
                // Executing the code deferred, so that a triple click becomes possible.
                this.executeDelayed(() => {
                    // modifying the double click to a triple click, if a further mousedown happened
                    if (this.#tripleClickActive) { event.type = 'tripleclick'; }
                    this.#doubleClickEventWaiting = false;
                    this.#tripleClickActive = false;
                    this.#selection.processBrowserEvent(event);
                }, this.#doubleClickTimeOut);

            }
        } else if (_.browser.WebKit) { // 39357
            if (DOM.isTextSpan(event.target) && DOM.isListParagraphNode(event.target.parentNode) && _.last(this.#selection.getStartPosition()) > 0) {
                wordBoundaries = getWordBoundaries(this.getCurrentRootNode(), this.#selection.getStartPosition(), { addFinalSpaces: true });
                if (_.last(wordBoundaries[0]) === 0) { this.#selection.setTextSelection(...wordBoundaries); }
            }
        }
    }

    /**
     * Handler for the keyUp event.
     *
     * @param {jQuery.Event} event
     *  A jQuery keyboard event object.
     */
    #processKeyUp(event) {

        if (_.browser.Edge && matchKeyCode(event, 'TAB', { shift: null }) && !this.isKeyDownTabHandling()) { // 62404
            // try to repair the broken DOM, if the current paragraph ends with a non-breakable space (this code can be removed after Edge has switched to Chromium engine)
            const start = this.#selection.getStartPosition();
            start.pop();
            const paragraph = getParagraphElement(this.getCurrentRootNode(), start);
            const lastNode = paragraph ? paragraph.lastChild : null;

            if (lastNode && DOM.isTextSpan(lastNode)) {
                let text = $(lastNode).text();
                if (text.length > 0 && /\xa0$/.test(text)) {
                    globalLogger.warn('Trying to repair the DOM. Problem caused by unhandled tab keydown event.');
                    text = text.replace(/\xa0$/, ''); // removing the trailing non-breakable space that was inserted by unhandled tab-keydown-event
                    const textNode = getDomNode(lastNode).firstChild;
                    textNode.nodeValue = text;
                }
            }
        }

        this.setKeyDownTabHandling(false);

        if (IOS_SAFARI_DEVICE) { this.#processKeyUpTimer = Date.now(); } // setting a timer, to avoid superfluous selection updates on IOS

        // to be able to distinguish tap on ipad's suggestion word, which triggers only textInput event,
        // from tap on normal letter from virtual keyboard, clear cached event value on key up event.
        this.#lastKeyDownEvent = null;
        this.#dumpEventObject(event);
    }

    /**
     * Handler for the 'compositionstart' event.
     * The event is dispatched before a text composition system begins a new
     * composition session and before the DOM is modified due to the composition
     * process.
     *
     * @param {jQuery.Event} event
     *  The 'compositionstart' event
     */
    #processCompositionStart(event) {
        //globalLogger.log('#processCompositionStart');

        // ime state object
        const imeState = {};
        // root node
        const rootNode = this.getCurrentRootNode();
        // whether this is a FireFox browser
        const isFireFox = _.browser.Firefox;
        // the browser selection
        const browserSel = isFireFox ? window.getSelection() : null;

        // helper function to check
        const isInvalidSelectionNode = node => {
            return DOM.isPageNode(node) || DOM.isHeaderOrFooterWrapper(node) || DOM.isHeaderOrFooter(node);
        };

        this.#dumpEventObject(event);
        if (_.browser.Android) { return; }

        // restoring the browser selection in Firefox (version > 49), because the browser selection
        // might have been modified (by the browser) before '#processCompositionStart'.
        if (isFireFox && (isInvalidSelectionNode(browserSel.anchorNode) || isInvalidSelectionNode(browserSel.focusNode))) { this.#selection.restoreBrowserSelection(); }

        // no text input in read-only mode (54729) (fix fails for Firefox)
        if (!this.docApp.isEditable() && !isFireFox) {
            this.#selection.setFocusIntoClipboardNode();
            event.preventDefault();
            event.stopPropagation();
        }

        this.#imeUseUpdateText = false;
        if (this.#imeStateQueue.length) {
            //fix for Bug 35543 - 3-set korean on chrome, compositionStart comes directly after compositionEnd,
            //but we have deferred the #postProcessCompositionEnd, so we have to abort it and call it directly
            const last = _.last(this.#imeStateQueue);
            const delayed = last.delayed;
            if (delayed) {
                delayed.abort();
                this.#postProcessCompositionEnd();
            }
        }

        // delete current selection first
        this.deleteSelected();

        this.#imeActive = true;
        // determin the span where the IME text will be inserted into
        imeState.imeStartPos = this.#selection.getStartPosition();
        imeState.target = this.getActiveTarget();
        // the span to be used for special webkit treatment
        const insertSpan = $(getLastNodeFromPositionByNodeName(rootNode, imeState.imeStartPos, 'span'));

        if (IOS_SAFARI_DEVICE && this.#lastKeyDownEvent) {
            //Bug 42457
            //IME start can come on an Asian keyboard in two ways:
            //1. press just a -> IME starts for replace a -> (we get two sign, this here is the workaround for that)
            //2. just press on a Asian sign, all is fine, IME works a expected

            const before = increaseLastIndex(this.#selection.getStartPosition(), -1);
            if (_.last(before) >= 0) {
                this.applyOperations({ name: Op.DELETE, start: _.copy(before), end: _.copy(before) });
                imeState.imeStartPos = before;
            }
        }

        // if the span is empty webkit browsers replace the content of the paragraph node
        // to avoid this we add a non-breaking space charater here which will be removed in #processCompositionEnd()
        // this workaround is not needed for Firefox and IE and would lead to runtime errors
        if (_.browser.WebKit && (insertSpan.text().length === 0)) {
            imeState.imeAdditionalChar = true;
            insertSpan.text('\xa0');
            this.#selection.setBrowserSelection(DOM.Range.createRange(insertSpan, 1, insertSpan, 1));
        } else {
            imeState.imeAdditionalChar = false;
        }

        // Special code for iPad. When we detect a compositionstart we have to
        // remove the last char from the activeInputText. Unfortunately we only
        // know now that the char should be processed by the composition system (IME).
        // Therefore we have to remove this char from the active input text.
        if (IOS_SAFARI_DEVICE) {
            // check and abort running input text promise
            if (this.#inputTextPromise) {
                this.#inputTextPromise.abort();
            }
            // remove latest character from active input text
            if (this.getActiveInputText()) {
                this.setActiveInputText(this.getActiveInputText().slice(0, -1));
            }
        }
        // now store the current state into our queue
        this.#imeStateQueue.push(imeState);

        // all external actions must be put into the queue after creating the snapshot
        this.setInternalOperationBlocker(true);
    }

    /**
     * Handler for the 'compositionupdate' event.
     * The event is dispatched during a composition session when a text composition
     * system updates its active text passage with a new character.
     *
     * @param {jQuery.Event} event
     *  The 'compositionupdate' event
     */
    #processCompositionUpdate(event) {

        if (_.browser.Android) { return; }

        //globalLogger.log('#processCompositionUpdate');
        this.#dumpEventObject(event);
        // Fix for 34726: Store the latest update text for later use.
        this.#imeUpdateText = event.originalEvent.data;
        //globalLogger.log('#processCompositionUpdate: imeUpdateText=' + this.#imeUpdateText);
    }

    /**
     * Handler for the 'compositionend' event.
     * The event is dispatched after the text composition system completes or cancels
     * the current composition session (e.g., the IME is closed, minimized, switched
     * out of focus, or otherwise dismissed, and the focus switched back to the user
     * agent).
     * Be careful: There are browsers which violate the w3c specification (e.g. Chrome
     * and Safari) and send the compositionend event NOT when the composition system
     * completes, but at a moment where DOM manipulations are NOT allowed.
     *
     * @param {jQuery.Event} event
     *  The 'compositionend' event
     */
    #processCompositionEnd(event) {
        if (_.browser.Android) { return; }
        if (this.#imeStateQueue.length === 0) { return; } // DOCS-4599

        this.#dumpEventObject(event);
        //globalLogger.log('#processCompositionEnd');

        // current ime state
        const imeState = _.last(this.#imeStateQueue);

        // store the original composition end event
        imeState.event = event;
        imeState.imeUpdateText = this.#imeUpdateText;

        // #51390#
        // Reset ime active flag to make sure that calling further
        // functions can work correctly (there are now functions that
        // check the IME activity and do nothing if there is a composition).
        this.#imeActive = false;

        // #48733 The Chrome implementation of their composition processing must have
        // changed considerably. Due to the fact that we implement the composition code
        // based on behavior & assumptions this now breaks. At least from Chrome 53 the
        // compositionend event is now sent AFTER the last DOM changes were made. This
        // leads to bad behavior in our code as we use asynchronous processing.
        // We now use synchronous processing and can remove some special code for Mac/Chrome.
        if (_.browser.Safari && _.device('macos')) {
            // For Safari we have to use the old, bad way to delay the process
            // the composition end event. This is also necessary for Windows where
            // some IME implementations send two or more input events which are
            // not always behind the DOM manipulation
            imeState.delayed = this.executeDelayed(this.#postProcessCompositionEnd);
            // there must not be any operation generation between '#processCompositionEnd' and '#postProcessCompositionEnd' (DOCS-2146)
            this.setIMEBlock(true); // allowing no operation generation until this blocker is removed in '#postProcessCompositionEnd'
        } else if (IOS_SAFARI_DEVICE) {
            //Ipad can process 'compositionend' synchronously, but to separate "composition" from "just insert text" we use that delay
            imeState.delayed = this.executeDelayed(this.#postProcessCompositionEnd);
        } else {
            // all other desktop browser can process 'compositionend' synchronously.
            this.#postProcessCompositionEnd();
        }

        this.#imeUseUpdateText = false;
    }

    /**
     * Post processing for the event handler '#processCompositionEnd'.
     *
     * @param {Object} options
     *  Optional parameters:
     *  @param {Boolean} [options.safariIMESync=false]
     *      Whether this function was triggered by the 'keydown' handler, when the IME block was
     *      active. This happens sometimes in Safari browser. In this case the 'keydown' handler
     *      generates operations between '#processCompositionEnd' and '#postProcessCompositionEnd'.
     *      This leads to a broken document (DOCS-2146). This problems can be avoided by running
     *      this function synchronously at the beginning of the 'keydown' handler.
     */
    #postProcessCompositionEnd(options) {
        //globalLogger.log('#postProcessCompositionEnd');

        // handling for Safari browser to avoid operation generation between #processCompositionEnd and #postProcessCompositionEnd (DOCS-2146)
        if (_.browser.Safari) {
            if (getBooleanOption(options, 'safariIMESync', false)) {
                this.setSafariIMESyncCall(true); // setting a marker to avoid following asynchronous call
            } else if (this.wasSafariIMESyncCall()) {
                this.setSafariIMESyncCall(false);
                return; // nothing more to do, function already ran triggered by 'keydown' event
            }
            this.setIMEBlock(false); // this IME block is only required between '#processCompositionEnd' and '#postProcessCompositionEnd' (DOCS-2146)
        }

        // pull the first state from the queue
        const imeState = this.#imeStateQueue.shift();
        // retrieve the IME text from the compositonend event or last good update event
        // see #49194# for more information
        let imeText = this.#imeUseUpdateText ? imeState.imeUpdateText : imeState.event.originalEvent.data;
        // determine the start position stored in the imeState
        const startPosition = imeState.imeStartPos ? _.clone(imeState.imeStartPos) : this.#selection.getStartPosition();
        // determine the target for start position stored in the imeState
        const imeTarget = imeState.target ? imeState.target : this.getActiveTarget();
        // the end position of the ime inserted text
        let endPosition;

        // DOCS-4597, when starting a new text document on windows with ime in the chrome browser a
        // "font" element might be inserted into the document. This font element needs to be removed.
        if (_.isEqual(startPosition, [0, 0])) {
            this.setTimeout(() => {
                const paragraphNode = getParagraphElement(this.getCurrentRootNode(this.getActiveTarget()), [0]);
                const fontNode = paragraphNode.querySelector("font");

                if (fontNode) {
                    fontNode.replaceWith(...fontNode.childNodes);
                }
            }, 0);
        }

        if (_.browser.Safari && _.device('macos')) {
            // Fix for 34726: This is a workaround for a special behaviour of
            // MacOS X. The keyboard switches to composition, if the user presses
            // the acute accent key. If this key is pressed twice, the composition is started,
            // ended and started again. This collides with the asynchronous processing
            // of the composition end due to broken 'compositionend' notification of
            // webkit/blink based browers. So the asynchronous code is executed while
            // the browser started a new composition. Manipulating the DOM while in
            // composition stops the composition immediately and 'compositionend'
            // provides an empty string in event.originalEvnt.data. Therefore we use
            // the last 'compositionupdate' text to get the correct text.
            imeText = (imeText.length === 0) ? imeState.imeUpdateText : imeText;
        }

        if (imeText.length > 0) {

            // remove the inserted IME text from the DOM
            // and also remove the non-breaking space if it has been added in '#processCompositionStart'
            endPosition = increaseLastIndex(startPosition, imeText.length - (imeState.imeAdditionalChar ? 0 : 1));
            this.implDeleteText(startPosition, endPosition, null, imeTarget);

            // We have to make sure that we always convert a implicit
            // paragraph. In some cases we have overlapping composition
            // events, which can produce an implicit pararaph with content
            // (inserted by the IME directly). So for this special
            // case we have to omit the length check in #handleImplicitParagraph
            this.#handleImplicitParagraph(startPosition, { ignoreLength: true });

            // insert the IME text provided by the event data
            this.insertText(imeText, startPosition, this.#preselectedAttributes, imeTarget); // imeTarget is used for DOCS-2852

            // set the text selection after the inserted IME text
            this.#selection.setTextSelection(imeState.imeAdditionalChar ? endPosition : increaseLastIndex(endPosition));

            // during the IME input we prevented calling 'restoreBrowserSelection', so we need to call it now
            this.#selection.restoreBrowserSelection();
        } else if ((imeText.length === 0) && imeState.imeAdditionalChar) {
            // #48775#
            // We have to remove the "special" nbsp, even if we don't
            // have any composition text. Otherwise an additional character is
            // inside the DOM!!
            this.implDeleteText(startPosition, startPosition, null, imeTarget);
            // We have to make sure that we always convert a implicit
            // paragraph. In some cases we have overlapping composition
            // events, which can produce an implicit pararaph with content
            // (inserted by the IME directly). So for this special
            // case we have to omit the length check in #handleImplicitParagraph
            this.#handleImplicitParagraph(startPosition, { ignoreLength: true });
            // set the text selection after the inserted IME text
            this.#selection.setTextSelection(startPosition);
            // during the IME input we prevented calling 'restoreBrowserSelection', so we need to call it now
            this.#selection.restoreBrowserSelection();
        }

        if (this.docApp.isOTEnabled()) {
            this.setInternalOperationBlocker(false); // external actions must no longer be queued
        }
    }

    #isImeActive() {
        return this.#imeActive || this.#imeStateQueue.length > 0;
    }

    #processTextInput(event) {
        const aLastKeyDownEvent = this.getLastKeyDownEvent();
        let startPosition;
        // whether the text can be inserted at the specified position
        let validPosition = true;
        //globalLogger.log('#processTextInput');

        if (this.docApp.isTextApp() && $(event.target).is('.ignorepageevent')) { return; } // the target has a marker, that page event handler can ignore it (inside the comment pane)

        this.#dumpEventObject(event);

        // Evaluating MacOS marker for multiple directly following keydown events (46659)
        // This check should only be done when no IME is active (49285)
        if (_.browser.MacOS && _.isNumber(this.getLastKeyDownKeyCode()) && !this.#isImeActive()) {
            if (this.isRepeatedKeyDown()) {
                // textinput event occurred directly after keydown and keyup without keypress
                // -> 'this.#lastKeyDownKeyCode' was not deleted in keypress
                this.setLastKeyDownKeyCode(null);
                this.setRepeatedKeyDown(false);
                event.preventDefault();
                return false;
            } else if ((this.getLastKeyDownKeyCode() === KeyCode.SPACE) && (this.getLastKeyDownModifiers().ctrlKey && this.getLastKeyDownModifiers().metaKey)) {
                this.setLastKeyDownKeyCode(null);
                this.setRepeatedKeyDown(false);
                event.preventDefault();
                return false;
            }
        }

        if (!this.#isImeActive() && _.browser.Chrome && (
            ((aLastKeyDownEvent === null) && _.isString(event.originalEvent.data) && event.originalEvent.data.length === 1) ||
            (aLastKeyDownEvent && isIMEInputKey(aLastKeyDownEvent) && _.isString(event.originalEvent.data) && (event.originalEvent.data.length === 1))
        )) {
            // This is again a very bad hacking solution for a broken Chrome behaviour with
            // certain IME-implementations, e.g. on Windows with MS Cangjie & on Linux with
            // SunPinyin & Cangjie 3 or 5.
            // 1.) Chrome provides a keydown-event with keycode 229, but there is no composition-event.
            // Only a textinput-event provides any information about the user-input.
            // 2.) There is only a "textInput" which is FOLLOWED by a input, keydown, keyup
            // event. This is a very bad behaviour and we also try to detect it.
            // Therefore this code tries to detect this magically and to save the
            // situation (#49585, see also Chromium issue: 658141)
            // ATTENTION: This fix must be removed as soon the Chromium test resolved the
            // broken behaviour. So 658141 should be monitored.
            startPosition = this.#selection.getStartPosition();

            // avoid typing into the slide selection ()
            if (this.useSlideMode() && startPosition.length === 2) { validPosition = false; }

            if (validPosition) {
                this.insertText(event.originalEvent.data, startPosition, this.#preselectedAttributes);
                this.#selection.setTextSelection(increaseLastIndex(startPosition), null, { keepFocus: this.forceToKeepFocus() });
            }

            // prevent default handling which inserts the text into the DOM
            event.preventDefault();
            return false;
        }

        if (IOS_SAFARI_DEVICE && !this.#isImeActive()) {
            const data = event.originalEvent.data;
            // Special code: The prevent default cancels the insertion of a suggested text.
            // Unfortunately the text is inserted for a short fraction and afterwards replaced
            // by the old text. Due to this process the cursor position is sometimes incorrectly
            // set. Previous code tried to restore the latest old cursor position, which was leading to internal errors or broken roundtrip.
            if (data.length && !this.#lastKeyDownEvent) {
                // detect that user taped on word suggestion, and not on actuall letter:
                // if there is only textInput event, and no keyDown and keyUp.
                // Workaround: instead of trying to set cursor position while element is or is not in the dom,
                // close the ios keyboard by setting focus to a hidden node.
                hideFocus(); // #40185
            } else if (data.length && this.#lastKeyDownEvent.keyCode === 0 && isHangulText(data)) {
                // Korean keyboard has no feedback for IME-Mode
                hideFocus(); // #42667
            }
            event.preventDefault();
            return false;
        }

        if (_.browser.MacOS && !this.#isImeActive()) {
            // Bug 48829 emojis in macos' IME mode
            const orgData = event.originalEvent.data;
            if (orgData.length && !this.#lastKeyDownEvent) {
                event.preventDefault();
                return false;
            }
        }
    }

    #processInput(event) {
        this.#dumpEventObject(event);
    }

    /**
     * Sets a paragraph temporarily to "contentEditable=false".
     *
     * paragraph {HTMLElement|null}
     *  The paragraph in that contentEditable is temporarily set to false.
     */
    #setIOSROParagraph(paragraph) {

        if (paragraph && this.#selection.hasRange()) {

            paragraph.contentEditable = 'false';
            this.#roIOSParagraph = paragraph;

            // setting contentEditable to false for 500 ms at the paragraph containing the selection avoids the context menu
            setTimeout(() => {
                // selection must be recreated after setting contentEditable to false
                this.#selection.setTextSelection(this.#selection.getStartPosition(), this.#selection.getEndPosition(), { keepFocus: this.forceToKeepFocus(), isIOSROHandling: true });
            }, 0);
            setTimeout(() => {
                paragraph.contentEditable = 'true';
                this.#roIOSParagraph = null;
            }, 500);
        }
    }

    /**
     * Central place to make changes to all sychnronously applied operations. Before the generated operations
     * are applied to the operation handler, this is the point, where global changes for all operations can be
     * made.
     *
     * @param {Array} operations
     *  An array containing all operations that are investigated to be finalized.
     */
    #finalizeOperations(operations) {

        operations.forEach(operation => {

            // adding the target to each operation, if it is not already added to the operation
            // and if it is a non-empty string
            if (operation.name && operation.start && !operation.target && this.#activeTarget && !this.#undoRedoRunning) {
                operation.target = this.#activeTarget;
            } else if (operation.target && operation.target === this.getOperationTargetBlocker()) {
                // removing the blocker for the operation target
                delete operation.target;
            }

            // setting a marker, that the operation does not require a new file version (DOCS-2144)
            if (this.getModifyingCounter() > 0) { operation.modifying = false; }
        });

        return operations;
    }

    /**
     * Load performance: Collecting selected operations in collector for saving them in local storage. This
     * makes it possible to load documents without any operations or fast load string, because all required
     * data is saved in the local storage.
     *
     * @param {Object} operation
     *  An operation object.
     */
    #saveStorageOperationHandler(operation) {
        return this.saveStorageOperation(operation);
    }

    /**
     * Utility method for extending object with target property,
     * but only if its defined.
     *
     * @param {Object} object
     *  object that is being extended
     *
     * @param {String[]} target
     *  Id referencing to header or footer node
     */
    #extendPropertiesWithTarget(object, target) {
        if (target) {
            object.target = target;
        }
    }

    #initDocument() {

        // container for the top-level paragraphs
        const pageContentNode = DOM.getPageContentNode(this.#editdiv);
        // the initial paragraph node in an empty document
        const paragraph = DOM.createImplicitParagraphNode();

        // create empty page with single paragraph
        pageContentNode.empty().append(paragraph);
        this.#validateParagraphNode(paragraph);

        // register keyboard event handlers on the clipboard node to handle key strokes when
        // a drawing is selected in which case the browser selection is inside the clipboard node
        this.#selection.getClipboardNode().on({
            keydown: this.docApp.isPresentationApp() ? _.bind(this.getPageProcessKeyDownHandlerClipboardNode(), this) : _.bind(this.getPageProcessKeyDownHandler(), this),
            keypress: this.docApp.isPresentationApp() ? _.bind(this.getPageProcessKeyPressHandlerClipboardNode(), this) : _.bind(this.getPageProcessKeyPressHandler(), this),
            cut: _.bind(this.cut, this),
            copy: _.bind(this.copy, this),
            paste: _.bind(this.paste, this)
        });

        // registering the handlers for entering or leaving a text frame selection
        this.listenTo(this.#selection, 'docs:textframe:leave', this.#textframeLeaveHandler);
        this.listenTo(this.#selection, 'docs:textframe:enter', this.#textframeEnterHandler);
    }

    /**
     * Called when all initial document operations have been processed.
     * Can be used to start post-processing tasks which need a fully
     * processed document.
     */
    #documentLoaded() {

        if (this.#changeTrack.updateSideBar()) { this.trigger('changeTrack:stateInfo', { state: true }); }
        this.#insertCollaborativeOverlay();
        this.#insertDrawingSelectionOverlay();

        this.#insertMissingCharacterStyles();
        this.#insertMissingParagraphStyles();
        this.tableStyles.insertMissingTableStyles();
        if (this.docApp.isODF()) { this.#insertMissingDrawingStyles(); }

        // Special observer for iPad/Safari mobile. Due to missing events to detect
        // and process DOM changes we have to add special code to handle these cases.
        if (IOS_SAFARI_DEVICE) {
            const MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

            this.#editDivObserver = new MutationObserver(mutations => {
                if (!this || !this.isProcessingOperations) {
                    this.#editDivObserver.disconnect();
                    return;
                }
                if (!this.isProcessingOperations()) {
                    // Optimization: we only check DOM mutations when we don't process our own
                    // operations.

                    mutations.forEach(mutation => {
                        let i = 0;

                        if (mutation.addedNodes.length > 0) {

                            for (i = 0; i < mutation.addedNodes.length; i++) {
                                const node = mutation.addedNodes[i];

                                if (node.nodeType === 1 && node.tagName === 'IMG' && node.className === '-webkit-dictation-result-placeholder') {
                                    // We have detected a Siri input. Switch model to read-only/disconnect and disconnect observer
                                    this.docApp.rejectEditAttempt('siri');
                                    this.#editDivObserver.disconnect();
                                }
                            }
                        }
                    });
                }
            });

            this.#editDivObserver.observe(this.#editdiv[0], { childList: true, subtree: true });

            if (this.docApp.isPresentationApp()) {

                // Prevent text suggestions which are available via context menu on iPad/iPhone.
                this.listenTo(this.#selection, 'change', options => {
                    // nothing to do, if this "change" was triggered by setting the paragraph in read-only mode
                    if (getBooleanOption(options, "isIOSROHandling", false)) { return; }
                    // Performance: Ignoring handler for 'simpleTextSelection' operations
                    if (!getBooleanOption(options, 'simpleTextSelection', false)) {
                        if (this.#selection.getSelectionType() === 'text' && this.docApp.isEditable()) {
                            const paragraph = this.#selection.getEnclosingParagraph();
                            if (paragraph) { this.#setIOSROParagraph(paragraph); }
                        }
                    }
                });

                // show own contextmenu after a single click on a selection range
                this.listenTo(this.#editdiv, 'touchend', () => {
                    if (this.#selection.hasRange() && this.#selection.getSelectionType() === 'text' && this.docApp.isEditable()) {
                        setTimeout(() => {
                            if (this.#selection.hasRange() && this.#selection.getSelectionType() === 'text' && this.docApp.isEditable()) {
                                const paragraph = this.#selection.getEnclosingParagraph();
                                if (paragraph) { this.#setIOSROParagraph(paragraph); }
                            }
                        }, 100);
                    }
                });

            }
        }
    }

    /**
     * Removes empty text nodes from the passed paragraph, checks whether
     * it needs a dummy text node, and converts consecutive white-space
     * characters.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph element to be validated. If this object is a jQuery
     *  collection, uses the first DOM node it contains.
     */
    #validateParagraphNode(paragraph) {

        // current sequence of sibling text nodes
        let siblingTextNodes = [];
        // array of arrays collecting all sequences of sibling text nodes
        const allSiblingTextNodes = [siblingTextNodes];
        // whether the paragraph contains any text
        let hasText = false;
        // whether the last child node is the dummy element
        let hasLastDummy = false;
        // whether we have to add or preserve a dummy element for hard-breaks or template text spans
        let mustAddOrPreserveDummy = false;

        if (this.isImeActive()) {
            // #51390: Never manipulate the DOM when a composition is active
            return;
        }

        this.DBG_COUNT = (this.DBG_COUNT || 0) + 1;

        // convert parameter to a DOM node
        paragraph = getDomNode(paragraph);

        // whether last node is the dummy node
        hasLastDummy = DOM.isDummyTextNode(paragraph.lastChild);

        // remove all empty text spans which have sibling text spans, and collect
        // sequences of sibling text spans (needed for white-space handling)
        iterateParagraphChildNodes(paragraph, node => {

            // visit all text spans embedded in text container nodes (fields, tabs, ... (NOT numbering labels))
            if (DOM.isTextSpan(node) || DOM.isTextComponentNode(node)) {
                DOM.iterateTextSpans(node, span => {
                    if (DOM.isEmptySpan(span)) {
                        // remove this span, if it is an empty portion and has a sibling text portion (should not happen anymore)
                        if (DOM.isTextSpan(span.previousSibling) || DOM.isTextSpan(span.nextSibling)) {
                            globalLogger.warn('Editor.#validateParagraphNode(): empty text span with sibling text span found');
                            $(span).remove();
                        }
                        // otherwise simply ignore the empty span
                    } else {
                        if (hasLastDummy && DOM.isTextFrameTemplateTextSpan(span)) { mustAddOrPreserveDummy = true; }
                        // append text node to current sequence
                        siblingTextNodes.push(span.firstChild);
                    }
                });

            // anything else (no text span or text container node): start a new sequence of text nodes
            } else {
                allSiblingTextNodes.push(siblingTextNodes = []);
            }

        }, undefined, { allNodes: true });

        // Special case for hyperlink formatting and empty paragraph (just one empty textspan)
        // Here we always don't want to have the hyperlink formatting, we hardly reset these attributes
        let url;
        if (DOM.isTextSpan(paragraph.firstElementChild) && paragraph.children.length === 1 && paragraph.firstElementChild.textContent.length === 0) {
            url = this.characterStyles.getElementAttributes(paragraph.firstElementChild).character.url;
            if ((url !== null) && (url.length > 0)) {
                this.characterStyles.setElementAttributes(paragraph.firstElementChild, CLEAR_ATTRIBUTES);
            }
        } else if (DOM.isListLabelNode(paragraph.firstElementChild)) {

            if (paragraph.children.length === 2 && paragraph.firstElementChild.nextSibling.textContent.length === 0) {
                url = this.characterStyles.getElementAttributes(paragraph.firstElementChild.nextSibling).character.url;
                if ((url !== null) && (url.length > 0)) {
                    this.characterStyles.setElementAttributes(paragraph.firstElementChild.nextSibling, CLEAR_ATTRIBUTES);
                }
            }

            if (!this.docApp.isTextApp()) {
                if (DOM.isEmptyListParagraph(paragraph)) { // handling empty list paragraph in OX Presentation and OX Spreadsheet (DOCS-1884)
                    if (!$(paragraph).hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_SELECTED_CLASS) && !$(paragraph).hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)) {
                        $(paragraph).addClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS); // setting marker for list item visibility in empty paragraph
                        this.#handleTriggeringListUpdate(paragraph); // numbered updating lists might be required
                    }
                } else { // handling non empty list paragraph in OX Presentation and OX Spreadsheet (DOCS-1884)
                    if ($(paragraph).hasClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS)) {
                        $(paragraph).removeClass(DOM.PARAGRAPH_NODE_LIST_EMPTY_CLASS); // removing marker for list item visibility in non-empty paragraph
                        this.#handleTriggeringListUpdate(paragraph); // numbered updating lists might be required
                    }
                }
            }
        }

        // Special case for hard-breaks. Check for a hard-break as a node before the last child which
        // is not a dummy node. We need to have a dummy node for a hard-break at the end of the
        // paragraph to support correct formatting & cursor travelling.
        if (paragraph.children.length >= 2) {
            let prevNode = paragraph.lastChild.previousSibling;

            if (paragraph.children.length >= 3 && hasLastDummy) {
                // Second case: We have a dummy node and must preserve it.
                // prevNode is in that case the previous node of prev node.
                // (hard-break) - (span) - (br)
                prevNode = prevNode.previousSibling;
            }

            if (prevNode && DOM.isHardBreakNode(prevNode)) {
                mustAddOrPreserveDummy = true;
            }
        }

        // Convert consecutive white-space characters to sequences of SPACE/NBSP
        // pairs. We cannot use the CSS attribute white-space:pre-wrap, because
        // it breaks the paragraph's CSS attribute text-align:justify. Process
        // each sequence of sibling text nodes for its own (the text node
        // sequences may be interrupted by other elements such as hard line
        // breaks, drawings, or other objects).
        // TODO: handle explicit NBSP inserted by the user (when supported)
        _(allSiblingTextNodes).each(siblingTextNodes => {

            // the complete text of all sibling text nodes
            let text = '';
            // offset for final text distribution
            let offset = 0;

            // collect the complete text in all text nodes
            _(siblingTextNodes).each(textNode => { text += textNode.nodeValue; });

            // ignore empty sequences
            if (text.length > 0) {
                hasText = true;

                // process all white-space contained in the text nodes
                text = text
                    // normalize white-space (convert to SPACE characters)
                    .replace(/\s/g, ' ')
                    // text in the node sequence cannot start with a SPACE character
                    .replace(/^ /, '\xa0')
                    // convert SPACE/SPACE pairs to SPACE/NBSP pairs
                    .replace(/ {2}/g, ' \xa0')
                    // text in the node sequence cannot end with a SPACE character
                    .replace(/ $/, '\xa0');

                // distribute converted text to the text nodes
                _(siblingTextNodes).each(textNode => {
                    const length = textNode.nodeValue.length;
                    textNode.nodeValue = text.substr(offset, length);
                    offset += length;
                });
            }
        });

        // determine hasLastDummy again to be sure that there is, after some
        // possible modifications before, a dummy text node
        hasLastDummy = DOM.isDummyTextNode(paragraph.lastChild);

        // insert an empty text span if there is no other content (except the dummy node)
        if (!paragraph.hasChildNodes() || (hasLastDummy && (paragraph.childNodes.length === 1))) {
            $(paragraph).prepend(createSpan({ label: '' }));
        }

        // append dummy text node if the paragraph contains no text,
        // or remove it if there is any text
        if (!hasText && !hasLastDummy) {
            $(paragraph).append(DOM.createDummyTextNode());
        } else if (!hasLastDummy && mustAddOrPreserveDummy) {
            // we need to add a dummy text node after a last hard-break
            $(paragraph).append(DOM.createDummyTextNode());
        } else if (hasText && hasLastDummy && !mustAddOrPreserveDummy) {
            $(paragraph.lastChild).remove();
        }
    }

    /**
     * Draws the border(s) of drawings. Needed for localStorage case to draw lines into the canvas
     * or for automatically resized text frames or text frames within groups.
     *
     * @param {jQuery|Node[]} drawingNodes
     *  Collector (jQuery or array) for all drawing nodes, whose border canvas need to be
     *  repainted.
     */
    #updateDrawings(drawingNodes) {

        _.each(drawingNodes, node => {

            if (this.isDrawingChangeActive()) { return; } // no update of drawing that is currently resized, moved, ...

            // one drawing node, must be 'jQuerified'
            const drawing = DrawingFrame.isDrawingFrame(node) ? $(node) : DrawingFrame.getDrawingNode(node);
            // the attributes of the drawing (also using styles (38058))
            const drawingAttrs = this.drawingStyles.getElementAttributes(drawing);

            if (this.docApp.isPresentationApp() && DOM.isHiddenNodeAfterAttach(drawing.parent())) { return; } // Performance: No update of drawings before slide formatting starts

            // update is required for table background color (48370)
            if (this.useSlideMode() && DrawingFrame.isTableDrawingFrame(node)) {
                this.tableStyles.updateTableDrawingBackground(node);
            }

            let changedVisibility = false;
            if (this.docApp.isPresentationApp() && this.drawingStyles.requiresFormatVisibility(drawingAttrs)) {
                changedVisibility = this.handleContainerVisibility(drawing, { makeVisible: true });
            }

            DrawingFrame.updateFormatting(this.docApp, drawing, drawingAttrs);

            if (changedVisibility) { this.handleContainerVisibility(drawing, { makeVisible: false }); }

            // After an update of formatting the selection needs to be repainted (otherwise event handlers are missing, 64283)
            if (this.#selection.isSelectedDrawingNode(node) && node.parentNode !== null) {
                // but never do this for the drawings in inactive header/footer nodes (DOCS-2812)
                if (!this.docApp.isTextApp() || !DOM.isMarginalNode(node) || containsNode(this.getCurrentRootNode(), node)) {
                    this.#selection.restoreDrawingSelection(node);
                }
            }
        });
    }

    /**
     * Calculates the size of the image defined by the given url,
     * limiting the size to the paragraph size of the current selection.
     *
     * @param {String} url
     *  The image url or base64 data url
     *
     * @returns {jQuery.Promise}
     *  A promise where the resolve result contains the size object
     *  including width and height in Hmm {width: 123, height: 456}
     */
    #getImageSize(url) {

        // the result deferred
        const def = this.createDeferred();
        // the image for size rendering
        const image = $('<img>');
        // the clipboard holding the image
        let clipboard = null;

        if (!url) { return def.reject(); }

        const absUrl = getFileUrl(this.docApp, url);

        // append the clipboard div to the body and place the image into it
        clipboard = this.docApp.getView().createClipboardNode();
        clipboard.append(image);

        // if you set the load handler BEFORE you set the .src property on a new image, you will reliably get the load event.
        image.one('load', () => {

            let width, height, para, maxWidth, maxHeight, factor, start;
            let localPageAttributes = null;
            // a text frame node containing the image
            let textFrameDrawing = null;

            // helper function for calculating drawing width and height
            const resolveImageSize = () => {
                if (this.docApp.isTextApp()) { // -> adapting the size of the drawing to the paragraph (only in OX Text!)
                    // maybe the paragraph is not so big
                    start = this.#selection.getStartPosition();
                    start.pop();
                    para = getParagraphElement(this.getRootNode(this.getActiveTarget()), start);
                    if (para) {
                        maxWidth = convertLengthToHmm($(para).outerWidth(), 'px');
                        localPageAttributes = this.pageStyles.getElementAttributes(this.#editdiv);
                        // reading page attributes, they are always available -> no need to check existence
                        maxHeight = localPageAttributes.page.height - localPageAttributes.page.marginTop - localPageAttributes.page.marginBottom;

                        // adapting width and height also to text frames with fixed height (36329)
                        if (DOM.isNodeInsideTextFrame(para)) {
                            textFrameDrawing = DrawingFrame.getClosestTextFrameDrawingNode(para);
                            if (DrawingFrame.isFixedHeightDrawingFrame(textFrameDrawing)) {
                                maxHeight = convertLengthToHmm(DrawingFrame.getTextFrameNode(textFrameDrawing).height(), 'px');
                            }
                        }

                        if (width > maxWidth) {
                            factor = math.roundp(maxWidth / width, 0.01);
                            width = maxWidth;
                            height = Math.round(height * factor);
                        }

                        if ((maxHeight) && (height > maxHeight)) {
                            factor = math.roundp(maxHeight / height, 0.01);
                            height = maxHeight;
                            width = Math.round(width * factor);
                        }
                    }
                }

                def.resolve({ width, height });
            };

            // Workaround for a strange Chrome behavior, even if we use .one() Chrome fires the 'load' event twice.
            // One time for the image node rendered and the other time for a not rendered image node.
            // We check for the rendered image node
            if (containsNode(clipboard, image)) {
                width = convertLengthToHmm(image.width(), 'px');
                height = convertLengthToHmm(image.height(), 'px');

                if (width === 0 && height === 0) {
                    // IE performance problem: The browser sometimes returns wrong values -> waiting a half second (56730)
                    this.executeDelayed(() => {
                        width = convertLengthToHmm(image.width(), 'px');
                        height = convertLengthToHmm(image.height(), 'px');
                        resolveImageSize();
                    }, 500);
                } else {
                    resolveImageSize();
                }
            }
        })
        .one('error', () => {
            globalLogger.warn('Editor.getImageSize(): image load error');
            def.reject("SIZE_DETECTION_FAILED");
        })
        .attr('src', absUrl);

        // always remove the clipboard again
        def.always(() => {
            clipboard.remove();
        });

        return def.promise();
    }

    // ====================================================================
    // IMPLEMENTATION FUNCTIONS
    // Private methods, that are called from method applyOperations().
    // The operations itself are never generated inside an impl*-function.
    // ====================================================================

    /**
     * Has to be called after changing the structure of a paragraph node, if
     * the update has to be done immediately (synchronously).
     *
     * @param {jQuery} paragraphs
     *  All paragraph nodes that need to be updated included into a
     *  jQuery object
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.suppressFontSize=false]
     *      If set to true, the dynamic font size calculation is NOT started
     *  @param {Boolean} [options.restoreSelection=true]
     *      If set to false, the selection will not be restored via
     *      restoreBrowserSelection. This is useful, if the DOM was
     *      modfified without setting the new selection.
     */
    #implParagraphChangedSync(paragraphs, options) {

        paragraphs.get().forEach(paragraph => {

            // the style handler for updating attributes
            let styleHandler = null;
            // an optional page break node inside the paragraph -> this should not be formatted (DOCS-3330)
            let pageBreakNode = null;
            // a temporary replacement node that must be inserted for the page break node
            let replacementNode = null;

            // the paragraph may have been removed from the DOM in the meantime
            if (containsNode(this.#editdiv, paragraph)) {
                this.#spellChecker.resetDirectly(paragraph); // reset to 'not spelled'
                this.#validateParagraphNode(paragraph);
                styleHandler = (this.useSlideMode() && DOM.isSlideNode(paragraph)) ? this.slideStyles : this.paragraphStyles;

                // replacing a page-break inside the paragraph
                if ($(paragraph).hasClass('contains-pagebreak')) {
                    pageBreakNode = $(paragraph).children('.page-break');
                    if (pageBreakNode.length === 1) {
                        replacementNode = DOM.createPagebreakReplacementNode();
                        replacementNode.insertAfter(pageBreakNode);
                        pageBreakNode.detach();
                    }
                }

                styleHandler.updateElementFormatting(paragraph)
                    .done(() => {
                        // -> maybe drawing formatting is required only for selected drawings? (TODO) -> check for implicit or empty paragraph?
                        options = options || {};
                        options.paragraphChanged = true; // setting marker, so that the caller can be recognized in the handler function

                        // appending the page break node to the DOM again after formatting
                        if (pageBreakNode && replacementNode) {
                            pageBreakNode.insertAfter(replacementNode);
                            replacementNode.remove();
                        }

                        this.trigger('paragraphUpdate:after', paragraph, options); // -> Performance critical
                    });
            }
        });

        // paragraph validation changes the DOM, restore selection
        // -> Not restoring browser selection, if the edit rights are not available (Task 29049)
        // -> Not restoring browser selection, if this function was started by debounced slide formatting (with 'suppressFontSize')
        // -> Not restoring browser selection, if 'restoreSelection' is explicitely set to false (52750)
        // -> Not restoring browser selection, if this function was started inside block of async operations (#50555)
        // -> Not restoring browser selection, if the focus is currently not in the page (example: user wants to insert a table)
        // -> Not restoring browser selection, if the focus is currently in a comment text frame (OX Text)
        if (!this.#imeActive && this.docApp.isEditable() && !getBooleanOption(options, 'suppressFontSize', false) && getBooleanOption(options, 'restoreSelection', true) && !this.getBlockKeyboardEvent()) {
            // Concurrent editing: External operation must not change the focus node, if this is not inside the page (this.docApp content node (67064)) or in a textframe
            // Concurrent editing: External operation must not lead to a restoring of browser selection, if the active drawing is currently moved, resized, ...
            const focusNode = getFocus();
            if (!this.docApp.isOTEnabled() || (containsFocus(this.docApp.docView.$appContentNode, { allowSelf: true }) && !DOM.isCommentTextframeNode(focusNode) && !DOM.isPopupContentNode(focusNode) && !this.isDrawingChangeActive())) {
                this.#selection.restoreBrowserSelection({ preserveFocus: true, keepFocus: this.forceToKeepFocus() });
            }
        }
    }

    /**
     * After paragraph modifications it might be necessary to update lists.
     * If this is necessary and which performance properties are required,
     * is checked within this function.
     *
     * @param {Node|jQuery|Null} [node]
     *  The DOM paragraph node to be checked. If this object is a jQuery
     *   collection, uses the first DOM node it contains. If missing or null,
     *   no list update is triggered and false is returned.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.checkSplitInNumberedList=false]
     *      If set to true, an additional check is executed, to make special
     *      performance improvements for (numbered) lists (32583). In this case
     *      a special marker is set at the following paragraph, if this function
     *      returns 'true'. In this case only all following paragraphs need to
     *      be updated. If this is a bullet list, no list update has to be done
     *      at all.
     *      This flag is used by splitParagraph and mergeParagraph handler.
     *  @param {Boolean} [options.paraInsert=false]
     *      If set to true, the 'updateList' data attribute is set at the (new)
     *      paragraph. This is necessary, if the paragraph does not already contain
     *      the list label node (DOM.LIST_LABEL_NODE_SELECTOR) and is therefore
     *      ignored in updateLists. Additionally 'updateListsDebounced' is 'informed
     *      about this setting. All this is important for performance reasons.
     *  @param {Boolean} [options.forceListLabelUpdate=false]
     *      If set to true, an optionally existing list label must be updated. This
     *      is especially important for bullet lists (DOCS-3422).
     *
     * @returns {Boolean}
     *  Whether the calling function can executed some specific code. This is
     *  only used for special options.
     */
    #handleTriggeringListUpdate(node, options) {

        // paragraph attributes object
        let paragraphAttrs = null;
        // the paragraph list style id
        let listStyleId = null;
        // the paragraph list level
        let listLevel = null;
        // whether a marker for a node is required
        let runSpecificCode = false;
        // whether a split happened inside a numbered list (performance)
        let splitInNumberedList = false;
        // whether the 'paraInsert' value needs to be set for updateListsDebounced
        const paraInsert = getBooleanOption(options, 'paraInsert', false);
        // whether the property 'splitInNumberedList' needs to be checked
        const checkSplitInNumberedList = getBooleanOption(options, 'checkSplitInNumberedList', false);
        // whether the list label must be updated (also required for bullet lists)
        const forceListLabelUpdate = getBooleanOption(options, 'forceListLabelUpdate', false);

        // nothing to do, if the node is not a paragraph
        if (!DOM.isParagraphNode(node)) { return false; }

        // do not update lists during import
        const updateListsDebounced = this.isImportFinished() ? this.updateListsDebounced.bind(this) : fun.undef;

        // receiving the paragraph attributes
        // fix for Bug 37594: changed from explicit attrs to merged attrs,
        // because some para stylesheets have list styles inside
        paragraphAttrs = this.paragraphStyles.getElementAttributes(node);

        if (this.useSlideMode()) {
            if (paragraphAttrs.paragraph && 'level' in paragraphAttrs.paragraph) {
                updateListsDebounced(node);
            }
        } else if (this.#isListStyleParagraph(null, paragraphAttrs)) {
            if (paragraphAttrs && paragraphAttrs.paragraph) {
                listStyleId = paragraphAttrs.paragraph.listStyleId;
                listLevel = paragraphAttrs.paragraph.listLevel;

                if (forceListLabelUpdate) {
                    updateListsDebounced(node);
                    return false;
                }

                if (checkSplitInNumberedList) { // check for split/merge of paragraphs
                    // marking paragraph for performance reasons, if this is a numbered list
                    if (this.#listCollection && this.#listCollection.isNumberingList(listStyleId, listLevel)) {
                        runSpecificCode = true;
                        splitInNumberedList = true;
                    } else {
                        // and do nothing if this is a split/merge in a bullet list (check 61923)
                        return false;
                    }
                }

                if (paraInsert) { runSpecificCode = true; }
            }

            // triggering list update debounced
            updateListsDebounced(null, { useSelectedListStyleIDs: true, listStyleId, listLevel, paraInsert, splitInNumberedList });
        }

        return runSpecificCode;
    }

    /**
     * After changing the page settings it is necessary to update specific elements. This are especially drawings
     * at paragraphs and tab stops. Drawings that are anchored to the page are updated by the event 'update:absoluteElements'
     * that comes later. Tables do not need to be updated, if their width is correctly set in percentage or 'auto' If they
     * have a fixed width, this must not be changed.
     */
    #updatePageDocumentFormatting() {

        // updating tables and drawings at paragraphs (in page and header / footer)
        const pageContentNode = DOM.getPageContentNode(this.#editdiv);
        // all paragraph and all drawings
        let formattingNodes = pageContentNode.find(DrawingFrame.NODE_SELECTOR + ', ' + DOM.PARAGRAPH_NODE_SELECTOR);
        // header and footer container nodes needs to be updated also, if there is content inside them
        const headerFooterFormattingNodes = this.#pageLayout.getHeaderFooterPlaceHolder().find(DrawingFrame.NODE_SELECTOR + ', ' + DOM.PARAGRAPH_NODE_SELECTOR);

        // update of tables is not required: width 'auto' (that is '100%') and percentage values works automatically

        // add content of comment nodes
        formattingNodes = formattingNodes.add(headerFooterFormattingNodes);

        // updating all elements
        _.each(formattingNodes, element => {

            if (DrawingFrame.isDrawingFrame(element) && DOM.isFloatingNode(element)) {
                // Only handle drawings that are anchored to paragraphs
                // -> absolute positioned drawings are updated later via 'update:absoluteElements'
                this.drawingStyles.updateElementFormatting(element);
                // also updating all paragraphs inside text frames (not searching twice for the drawings)
                this.#implParagraphChanged($(element).find(DOM.PARAGRAPH_NODE_SELECTOR));

            } else if (DOM.isParagraphNode(element)) {

                // update the size of all tab stops in this paragraph (but only if the paragraph contains tabs (Performance))
                if ($(element).find(DOM.TAB_NODE_SELECTOR).length > 0) {
                    this.paragraphStyles.updateTabStops(element);
                }

            }
        });
    }

    /**
     * Prepares a 'real' paragraph after insertion of text, tab, drawing, ...
     * by exchanging an 'implicit' paragraph (in empty documents, empty cells,
     * behind tables, ...) with the help of an operation. Therefore the server is
     * always informed about creation and removal of paragraphs and implicit
     * paragraphs are only required for user input in the browser.
     *
     * @param {Number[]} position
     *  The logical text position.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.ignoreLength=false]
     *      If set to true, no check for the paragraph length is done. Normally
     *      a implicit paragraph must be empty. In special cases this check
     *      must be omitted.
     */
    #handleImplicitParagraph(position, options) {

        // the searched implicit paragraph
        let paragraph = null;
        // the new created paragraph
        let newParagraph = null;
        // ignore length option
        const ignoreLength = getBooleanOption(options, 'ignoreLength', false);
        // target for operation - if exists, it's for ex. header or footer
        const target = this.getActiveTarget();
        // current element container node
        const rootNode = this.getCurrentRootNode(target);
        // created operation
        let operation = null;
        // optional attributes for the insertParagraph operation
        let attrs = null;

        position = _.clone(position);
        if (position.pop() === 0) {  // is this an empty paragraph?
            paragraph = (this.#useParagraphCache && this.#paragraphCache) || getParagraphElement(rootNode, position);
            if ((paragraph) && (DOM.isImplicitParagraphNode(paragraph)) && ((getParagraphNodeLength(paragraph) === 0) || ignoreLength)) {
                // adding attributes to the paragraph, if required
                if (this.getReplaceImplicitParagraphAttrs) { attrs = this.getReplaceImplicitParagraphAttrs(paragraph); } // checking for attributes before removing the implicit paragraph
                // removing implicit paragraph node
                $(paragraph).remove();
                // creating new paragraph explicitely
                operation = { name: Op.PARA_INSERT, start: _.copy(position), noUndo: true };
                if (attrs && !_.isEmpty(attrs)) { operation.attrs = attrs; }
                this.#extendPropertiesWithTarget(operation, target);
                this.applyOperations(operation);
                // Setting attributes to new paragraph immediately (task 25670)
                newParagraph = getParagraphElement(rootNode, position);
                this.paragraphStyles.updateElementFormatting(newParagraph);
                // using the new paragraph as global cache
                if (this.#useParagraphCache && this.#paragraphCache) { this.#paragraphCache = newParagraph; }
                // if implicit paragraph was marked as marginal, mark newly created also
                if (DOM.isMarginalNode(paragraph)) {
                    $(newParagraph).addClass(DOM.MARGINAL_NODE_CLASSNAME);
                }
            }
        }
    }

    /**
     * Prepares the text span at the specified logical position for
     * insertion of a new text component or character. Splits the text span
     * at the position, if splitting is required. Always splits the span,
     * if the position points between two characters of the span.
     * Additionally splits the span, if there is no previous sibling text
     * span while the position points to the beginning of the span, or if
     * there is no next text span while the position points to the end of
     * the span.
     *
     * @param {Number[]} position
     *  The logical text position.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.isInsertText=false]
     *      If set to true, this function was called from #implInsertText.
     *  @param {Boolean} [options.useCache=false]
     *      If set to true, the paragraph element saved in the selection can
     *      be reused.
     *  @param {Boolean} [options.allowDrawingGroup=false]
     *      If set to true, the element can also be inserted into a drawing
     *      frame of type 'group'.
     *
     * @param {String|Node} [target]
     *  ID of root node or the root node itself.
     *
     * @returns {HTMLSpanElement|Null}
     *  The text span that precedes the passed offset. Will be the leading
     *  part of the original text span addressed by the passed position, if
     *  it has been split, or the previous sibling text span, if the passed
     *  position points to the beginning of the span, or the entire text
     *  span, if the passed position points to the end of a text span and
     *  there is a following text span available or if there is no following
     *  sibling at all.
     *  Returns null, if the passed logical position is invalid.
     */
    #prepareTextSpanForInsertion(position, options, target) {

        // node info at passed position (DOM text node level)
        let nodeInfo = null;
        // whether this text span is required for inserting text (performance)
        const isInsertText = getBooleanOption(options, 'isInsertText', false);
        // whether the cached paragraph can be reused (performance)
        const useCache = getBooleanOption(options, 'useCache', false);
        // the character attributes of a previous inline component
        let inlineAttrs = null;
        // the node, in which the content will be inserted
        let insertNode = null;
        // whether attributes are inherited from a left node
        let leftInheritance = false;

        if (useCache && !target) {
            nodeInfo = getDOMPosition(this.#selection.getParagraphCache().node, [_.last(position)]);
        } else {
            if (target) {
                if (_.isString(target)) {
                    nodeInfo = getDOMPosition(this.getRootNode(target), position);

                    if (!nodeInfo) {
                        nodeInfo = getDOMPosition(this.getRootNode(target), position);
                    }
                } else {
                    nodeInfo = getDOMPosition(target, position);
                }
            } else {
                nodeInfo = getDOMPosition(this.#editdiv, position);
            }
        }

        // check that the parent is a text span
        if (!nodeInfo || !nodeInfo.node || !DOM.isPortionSpan(nodeInfo.node.parentNode)) {

            // maybe the nodeInfo is a drawing in the drawing layer
            if (nodeInfo && nodeInfo.node && DOM.isDrawingLayerNode(nodeInfo.node.parentNode)) {
                nodeInfo.node = DOM.getDrawingPlaceHolderNode(nodeInfo.node).previousSibling.firstChild; // using previous sibling (see template doc)
                nodeInfo.offset = 0;
            }

            if (!nodeInfo || !nodeInfo.node || !DOM.isPortionSpan(nodeInfo.node.parentNode)) {
                globalLogger.warn('Editor.#prepareTextSpanForInsertion(): expecting text span at position ' + JSON.stringify(position) + ' Target: ' + target);
                if (target && this.docApp.isTextApp()) { this.#commentLayer.logModelState(target); } // debugging the problem in comment layer
                return null;
            }
        }
        nodeInfo.node = nodeInfo.node.parentNode;

        // return current span, if offset points to its end
        // without following node or with following text span
        if (nodeInfo.offset === nodeInfo.node.firstChild.nodeValue.length) {
            if ((isInsertText && !nodeInfo.node.nextSibling) || DOM.isTextSpan(nodeInfo.node.nextSibling)) {
                if (isInsertText && (nodeInfo.offset === 0) && nodeInfo.node.previousSibling) {
                    // inheriting character attributes from previous inline component node, for example 'tabulator'
                    inlineAttrs = this.getInheritInlineCharacterAttributes(nodeInfo.node);
                    if (inlineAttrs && !_.isEmpty(inlineAttrs)) {
                        this.characterStyles.setElementAttributes(nodeInfo.node, { character: inlineAttrs });
                    }
                }
                return nodeInfo.node;
            }
        }

        // do not split at beginning with existing preceding text span
        if ((nodeInfo.offset === 0) && nodeInfo.node.previousSibling) {
            if (DOM.isTextSpan(nodeInfo.node.previousSibling)) {
                return nodeInfo.node.previousSibling;
            } else if (isInsertText) {
                // adding inherited character attributes from previous inline nodes
                inlineAttrs = this.getInheritInlineCharacterAttributes(nodeInfo.node);
                if (inlineAttrs && !_.isEmpty(inlineAttrs)) { leftInheritance = true; }
            }
        }

        // inheriting from an inline node right of the text span at the beginning of a paragraph or behind a drawing node
        // -> but inheritance from left is more important than inheritance from right
        if (isInsertText && !leftInheritance && nodeInfo.offset === 0 &&  nodeInfo.node.firstChild.nodeValue.length === 0 && (!nodeInfo.node.previousSibling || DOM.isDrawingFrame(nodeInfo.node.previousSibling)) && nodeInfo.node.nextSibling) {
            inlineAttrs = this.getInheritInlineCharacterAttributes(nodeInfo.node, { fromNextSibling: true });
        }

        // checking for template text in empty text frames (problem with undo, that inserts text in not-selected empty text frame)
        if (DOM.isTextFrameTemplateTextSpan(nodeInfo.node)) { this.removeTemplateTextFromTextSpan(nodeInfo.node); }

        // otherwise, split the span
        insertNode = DOM.splitTextSpan(nodeInfo.node, nodeInfo.offset)[0];

        // checking, if there are attributes inherited from previous inline components
        if (isInsertText && inlineAttrs && !_.isEmpty(inlineAttrs)) {
            this.characterStyles.setElementAttributes(insertNode, { character: inlineAttrs });
        }

        return insertNode;
    }

    /**
     * Helper function to insert the changes mode property into the
     * attribute object. This is necessary for performance reasons:
     * The 'mode: null' property and value shall not be sent with
     * every operation.
     * On the other this is necessary, so that an inserted text/tab/
     * field/hardbreak/... can be included into a change track span,
     * without using changeTrack. So the inserted text/tab.... must
     * not be marked as change tracked.
     * -> This is different to other character attributes.
     *
     * @param {Object} [attrs]
     *  Attributes transfered with the operation. These are checked
     *  for change track information and expanded, if necessary.
     *
     * @returns {Object}
     *  Attributes object, that is expanded with the change track
     *  attributes, if required.
     */
    #checkChangesMode(attrs) {

        // if no attributes are defined, set empty changes attributes
        if (!attrs) { return { changes: { inserted: null, removed: null, modified: null } }; }

        if (!attrs.changes) {
            // a new object containing attributes
            const newAttrs = _.copy(attrs, true);
            // if attributes are defined, but no change track attributes, also set empty changes attributes
            newAttrs.changes = { inserted: null, removed: null, modified: null };
            return newAttrs;
        }

        return attrs;
    }

    /**
     * Inserts a simple text portion into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new text portion.
     *
     * @param {String} text
     *  The text to be inserted.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new text portion, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {String} [target]
     *  The target corresponding to the specified logical start position.
     *
     * @param {Boolean} [external]
     *  Will be set to true, if the invocation of this method originates
     *  from an external operation.
     *
     * @returns {Boolean}
     *  Whether the text portion has been inserted successfully.
     */
    #implInsertText(start, text, attrs, target, external) {

        // text span that will precede the field
        let span = null;
        // new new text span
        let newSpan = null, newSpanNode = null;
        // variables for page breaks rendering
        let currentElement = null, currentHeight = null, directParagraph = null, textFrameNode = null;
        // the height of the paragraph or table in that the text is inserted
        let prevHeight = null, prevDirectParaHeight = null;
        // whether the height of a paragraph, table or dynamic text frame has changed
        let updatePageBreak = false, updateTextFrame = false;
        // whether the height of a node can be changed by this operation handler
        let fixedHeight = false;
        // whether the span for text insertion is a spell error span
        let isSpellErrorSpan = false;
        // the paragraph node
        let paragraphNode = null;
        // the previous operation before this current operation
        const lastOperation = this.getLastOperation();
        // whether the cache can be reused (performance)
        const useCache = (!target && lastOperation && PARAGRAPH_CACHE_OPERATIONS[lastOperation.name] &&
            this.#selection.getParagraphCache() && _.isEqual(_.initial(start), this.#selection.getParagraphCache().pos) && _.isUndefined(lastOperation.target));

        // helper function to determine a node, that can be used for measuring changes of height
        // triggered by the text insertion
        const setValidHeightNode = () => {

            currentElement = paragraphNode;

            // measuring heights to trigger update of page breaks or auto resizing text frames
            if (!$(currentElement.parentNode).hasClass('pagecontent')) {

                // check, if the node inside a text frame
                textFrameNode = DrawingFrame.getDrawingNode(currentElement.parentNode);

                if (textFrameNode && textFrameNode.length > 0) {

                    // check, if the text frame node needs to grow dynamically
                    if (DrawingFrame.isAutoResizeHeightDrawingFrame(textFrameNode)) {
                        directParagraph = currentElement;
                        prevDirectParaHeight = currentElement.offsetHeight;

                        // finding a top level node for page break calculation
                        if (DOM.isInsideDrawingLayerNode(currentElement)) {
                            currentElement = DOM.getTopLevelDrawingInDrawingLayerNode(currentElement);
                            currentElement = DOM.getDrawingPlaceHolderNode(currentElement);
                        }

                        // searching for the top level paragraph or table
                        currentElement = $(currentElement).parents(DOM.PARAGRAPH_NODE_SELECTOR).last()[0];  // its a div.p inside paragraph

                        if (currentElement && !$(currentElement.parentNode).hasClass('pagecontent')) {
                            currentElement = $(currentElement).parents(DOM.TABLE_NODE_SELECTOR).last()[0]; // its a text frame inside table(s)
                        }

                    } else {
                        // the height of the text frame will not change! It is not automatically resized.
                        fixedHeight = true;
                    }

                } else if (target) {
                    if (!$(currentElement.parentNode).attr('data-container-id')) {
                        currentElement = $(currentElement).parents(DOM.TABLE_NODE_SELECTOR).last()[0]; //its a div.p inside table(s)
                    }
                    // TODO parent DrawingFrame
                } else {
                    currentElement = $(currentElement).parents(DOM.TABLE_NODE_SELECTOR).last()[0]; // its a div.p inside table(s)
                }
            }
        };

        // starting with preparing a text span for text insertion
        span = this.#prepareTextSpanForInsertion(start, { isInsertText: true, useCache }, target);

        if (!span) { return false; }

        paragraphNode = span.parentNode;
        isSpellErrorSpan = DOM.isSpellerrorNode(span);

        // do not write text into an implicit paragraph, improvement for task 30906
        if (DOM.isImplicitParagraphNode(paragraphNode)) { return false; }

        // handling for paragraphs behind tables in tables, whose height is explicitely set to 0
        if ((this.#undoRedoRunning || external) && $(paragraphNode).height() === 0) { $(paragraphNode).css('height', ''); }

        // finding the correct element for measuring height changes caused by this text insertion
        setValidHeightNode();

        prevHeight = currentElement ? currentElement.offsetHeight : 0;

        // Splitting text span is only required, if attrs are available
        if (attrs) {
            attrs = this.#checkChangesMode(attrs); // expanding attrs to disable change tracking, if not explicitely set
            // split the text span again to get actual character formatting for
            // the span, and insert the text
            newSpan = DOM.splitTextSpan(span, span.firstChild.nodeValue.length, { append: true }).contents().remove().end().text(text);
            if (!text) { DOM.ensureExistingTextNode(newSpan); } // empty text should never happen

            // apply the passed text attributes
            if (_.isObject(attrs)) {
                this.characterStyles.setElementAttributes(newSpan, attrs);
            }

            // removing spell error attribute
            if (isSpellErrorSpan) { this.#spellChecker.clearSpellErrorAttributes(newSpan); }

            // removing empty neighboring text spans (merging fails -> avoiding warning in console)
            newSpanNode = getDomNode(newSpan);
            if (DOM.isChangeTrackNode(newSpanNode) || (attrs.character && attrs.character.anchor)) {
                if (DOM.isEmptySpan(newSpanNode.previousSibling)) { $(newSpanNode.previousSibling).remove(); }
                if (DOM.isEmptySpan(newSpanNode.nextSibling)) { $(newSpanNode.nextSibling).remove(); }
            }

        } else {

            // Performance: Simply adding new text into existing node
            // -> But not for change tracking and not if this is an empty span
            if (isSpellErrorSpan || DOM.isChangeTrackNode(span) || DOM.isEmptySpan(span)) {
                attrs = this.#checkChangesMode(); // expanding attrs to disable change tracking, if not explicitely set
                newSpan = DOM.splitTextSpan(span, span.firstChild.nodeValue.length, { append: true }).contents().remove().end().text(text);
                this.characterStyles.setElementAttributes(newSpan, attrs);
                if (isSpellErrorSpan) { this.#spellChecker.clearSpellErrorAttributes(newSpan); }
            } else {
                span.firstChild.nodeValue += text;
                newSpan = $(span);
            }
        }

        // try to merge with preceding and following span
        mergeSiblingTextSpans(newSpan, true);
        mergeSiblingTextSpans(newSpan);

        // validate paragraph, store new cursor position
        // paste task 55767 -> synchronous formatting is required, if attributes are specified in this operation (to remove emtpy spans)
        if (this.#guiTriggeredOperation || !this.isImportFinished() || (this.#pasteInProgress && !_.isObject(attrs))) {
            this.#implParagraphChanged(newSpan.parent());  // Performance and task 30587: Local client can defer attribute setting, Task 30603: deferring also during loading document
        } else {
            if (this.docApp.isOTEnabled()) {
                this.#implParagraphChanged(newSpan.parent()); // OT: Avoiding to lose the focus permanently
            } else {
                this.#implParagraphChangedSync(newSpan.parent()); // Performance and task 30587: Remote client must set attributes synchronously
            }
        }

        if (target) { this.setBlockOnInsertPageBreaks(true); }

        this.#quitFromPageBreak = true; // quit from any currently running pagebreak calculus - performance optimization

        // checking changes of height for top level elements (for page breaks) and for direct paragraph (auto resize text frame)
        if (!fixedHeight) {
            if (directParagraph && prevDirectParaHeight !== directParagraph.offsetHeight) {
                updateTextFrame = true;
            } else {
                textFrameNode = null;
            }

            if ($(currentElement).data('lineBreaksData')) {
                $(currentElement).removeData('lineBreaksData'); //we changed paragraph layout, cached line breaks data needs to be invalidated
            }

            currentHeight = (currentElement && currentElement.offsetHeight) || 0;
            if (prevHeight !== currentHeight) {
                updatePageBreak = true;
            }
            if (DOM.isEmptySpan(span) && DOM.isHardBreakNode(newSpan.prev())) { // #39468 - also update pb when typing after ms page hardbreak
                updatePageBreak = true;
            }

            if (updatePageBreak || updateTextFrame) {
                this.#insertPageBreaksDebounced(currentElement, textFrameNode);
                if (!this.#pbState) { this.docApp.getView().recalculateDocumentMargin(); }
            }
        }

        if (DrawingFrame.isNoWordWrapDrawingFrame(textFrameNode)) {
            this.#insertPageBreaksDebounced(currentElement, textFrameNode);
            if (!this.#pbState) { this.docApp.getView().recalculateDocumentMargin(); }
        }

        this.#lastOperationEnd = increaseLastIndex(start, text.length);

        // Performance: Saving paragraph info for following operations
        this.#selection.setParagraphCache(newSpan.parent(), _.clone(_.initial(this.#lastOperationEnd)), _.last(this.#lastOperationEnd));

        return true;
    }

    /**
     * Inserts a text field component into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new text field.
     *
     * @param {String} type
     *  A property describing the field type.
     *
     * @param {String} representation
     *  A fallback value, if the placeholder cannot be substituted with a
     *  reasonable value.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new text field, as map of attribute
     *  maps (name/value pairs), keyed by attribute family.
     *
     * @returns {Boolean}
     *  Whether the text field has been inserted successfully.
     */
    #implInsertField(start, type, representation, attrs, target) {
        return this.#fieldManager.implInsertField(start, type, representation, attrs, target);
    }

    /**
     * Inserts a horizontal tabulator component into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new tabulator.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new tabulator component, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {Object} [target]
     *  If exists, defines node, to which start position is related.
     *  Used primarily to address headers/footers.
     *
     * @returns {Boolean}
     *  Whether the tabulator has been inserted successfully.
     */
    #implInsertTab(start, attrs, target) {

        // text span that will precede the field
        const span = this.#prepareTextSpanForInsertion(start, {}, target);
        // new text span for the tabulator node
        let tabSpan = null;
        // element from which we calculate pagebreaks
        const currentElement = span ? span.parentNode : '';

        if (!span) { return false; }

        const isSpellErrorSpan = DOM.isSpellerrorNode(span);

        // tab must not be inserted into the slide (52416)
        if (this.useSlideMode() && start.length === 2) { return false; }

        // expanding attrs to disable change tracking, if not explicitely set
        attrs = this.#checkChangesMode(attrs);

        // split the text span to get initial character formatting for the tab
        tabSpan = DOM.splitTextSpan(span, 0);

        if (isSpellErrorSpan) { // DOCS-3825
            this.#spellChecker.clearSpellErrorAttributes(tabSpan);
            const nextSpan = span.nextSibling;
            if (DOM.isEmptySpan(nextSpan) && DOM.isSpellerrorNode(nextSpan)) { this.#spellChecker.clearSpellErrorAttributes(nextSpan); }
        }

        if (_.browser.IE) {
            // Prevent resizing rectangle provided by IE for tab div even
            // set to contenteditable=false. Therefore we need to set IE
            // specific unselectable=on.
            tabSpan.attr('unselectable', 'on');
        } else if (_.browser.iOS && _.browser.Safari) {
            // Safari mobile allows to visit nodes which has contenteditable=false, therefore
            // we have to use a different solution. We use -webkit-user-select: none to force
            // Safari mobile to jump over the text span with the fill characters.
            tabSpan.css('-webkit-user-select', 'none');
        }

        // new tabulator node
        const newTabNode = DOM.createTabNode();

        // Bug #51172:
        //      cursor jumps one line down while inserting a tab-stop
        if (_.browser.Chrome) {
            const brElement = _.filter($(span).siblings('br'), ele => { return $(ele).data('dummy'); });
            if (brElement.length === 1) {
                $(brElement).remove();
            }
        }

        // insert a tab container node before the addressed text node, move
        // the tab span element into the tab container node
        newTabNode.append(tabSpan).insertAfter(span);

        // apply the passed tab attributes
        if (_.isObject(attrs)) {
            this.characterStyles.setElementAttributes(tabSpan, attrs);
        }

        // adding inherited character attributes
        this.inheritInlineCharacterAttributes(newTabNode, attrs?.character);

        // it might be necessary to transfer the hard character attributes to a previous empty text span (DOCS-3244)
        if (attrs && attrs.character) { this.checkTransferOfInlineCharacterAttributes(tabSpan, attrs.character); }

        // validate paragraph, store new cursor position
        this.#implParagraphChanged(span.parentNode);
        this.#lastOperationEnd = increaseLastIndex(start);

        // don't call explicitly page break rendering, if target for header/footer comes with operation
        if (target) { this.setBlockOnInsertPageBreaks(true); }

        // we changed paragraph layout, cached line breaks data needs to be invalidated
        if ($(currentElement).data('lineBreaksData')) { $(currentElement).removeData('lineBreaksData'); }
        // call for debounced render of pagebreaks
        // TODO: Check if paragraph height was modified
        this.#quitFromPageBreak = true; // quit from any currently running pagebreak calculus - performance optimization

        this.#insertPageBreaksDebounced(currentElement, DrawingFrame.getClosestTextFrameDrawingNode(span.parentNode));
        if (!this.#pbState) { this.docApp.getView().recalculateDocumentMargin(); }

        // immediately updating element formatting to underline the tab correctly
        // -> This is not necessary with localStorage and fastLoad during loading
        if (this.isImportFinished() && this.#changeTrack.isActiveChangeTracking()) {
            this.paragraphStyles.updateElementFormatting(span.parentNode);
        }

        return true;
    }

    /**
     * Inserts a complex field into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new complex field.
     *
     * @param {String} instruction
     *  The instruction string of the complex field.
     *
     * @param {Object} attrs
     *  The attributes of the complex field.
     *
     * @param {String} [target]
     *  The target corresponding to the specified logical start position.
     *
     * @returns {Boolean}
     *  Whether the complex field has been inserted successfully.
     */
    #implInsertComplexField(start, instruction, attrs, target) {
        return this.#fieldManager.insertComplexFieldHandler(start, instruction, attrs, target);
    }

    /**
     * Inserts a bookmark into the document DOM.
     *
     * @param {Number[]} start
     *  The logical start position for the new bookmark.
     *
     * @param {string} id
     *
     * @param {String} anchorName
     *  The anchor string of the bookmark.
     *
     * @param {String} position
     *  String marking start or end type of bookmark.
     *
     * @param {Object} attrs
     *  The attributes of the bookmark.
     *
     * @param {String} [target]
     *  The target corresponding to the specified logical start position.
     *
     * @returns {Boolean}
     *  Whether the bookmark has been inserted successfully.
     */
    #implInsertBookmark(start, id, anchorName, position, attrs, target) {

        // the text span that will precede the bookmark
        const span = this.#prepareTextSpanForInsertion(start, {}, target);
        // the jQueryfied bookmark node
        const newBookmarkNode = $('<div class="bookmark inline">').attr({ anchor: anchorName, bmPos: position, bmId: id });

        // insert a bookmark container node before the addressed text node, move
        // the bookmark span element into the bookmark container node
        newBookmarkNode.insertAfter(span);

        // adding inherited character attributes
        this.inheritInlineCharacterAttributes(newBookmarkNode, attrs?.character);

        // it might be necessary to transfer the hard character attributes to a previous empty text span (DOCS-3244)
        if (attrs?.character) { this.checkTransferOfInlineCharacterAttributes(newBookmarkNode, attrs.character); }

        // validate paragraph, store new cursor position
        this.#implParagraphChanged(span?.parentNode);
        this.#lastOperationEnd = increaseLastIndex(start);

        return true;
    }

    /**
     * Updates a complex field node with new instruction.
     *
     * @param {Number[]} start
     *  The logical start position for the new complex field.
     *
     * @param {String} instruction
     *  The instruction string of the complex field.
     *
     * @param {object} attrs
     *
     * @param {String} target
     *  The target corresponding to the specified logical start position.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the complex field has been inserted successfully.
     */
    #implUpdateComplexField(start, instruction, attrs, target, external) {
        return this.#fieldManager.updateComplexFieldHandler(start, instruction, attrs, target, external);
    }

    /**
     * Updates a simple field node with new properties.
     *
     * @param {Number[]} start
     *  The logical start position for the new simple field.
     *
     * @param {String} type
     *  Type of the simple field.
     *
     * @param {String} representation
     *  Content of the field that's updated.
     *
     * @param {object} attrs
     *
     * @param {String} target
     *  The target corresponding to the specified logical start position.
     *
     * @param {Boolean} external
     *  Whether this is an external operation.
     *
     * @returns {Boolean}
     *  Whether the simple field has been inserted successfully.
     */
    #implUpdateField(start, type, representation, attrs, target, external) {
        return this.#fieldManager.updateSimpleFieldHandler(start, type, representation, attrs, target, external);
    }

    /**
     * Inserts a range marker node into the document DOM. These ranges can be
     * used for several features like comments, complex fields, ...
     *
     * @param {Number[]} start
     *  The logical start position for the new range marker.
     *
     * @param {String} id
     *  The unique id of the range marker. This can be used to identify the
     *  corresponding node, that requires this range. This can be a comment
     *  node for example.
     *
     * @param {String} type
     *  The type of the range marker.
     *
     * @param {String} position
     *  The position of the range marker. Currently supported are 'start'
     *  and 'end'.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new range component, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {String} [target]
     *  The target corresponding to the specified logical start position.
     *
     * @returns {Boolean}
     *  Whether the range marker has been inserted successfully.
     */
    #implInsertRange(start, id, type, position, attrs, target) {
        return this.#rangeMarker.insertRangeHandler(start, id, type, position, attrs, target);
    }

    /**
     * Inserts a drawing component into the document DOM.
     *
     * @param {String} type
     *  The type of the drawing. Supported values are 'shape', 'group',
     *  'image', 'diagram', 'chart', 'ole', 'horizontal_line', 'undefined'
     *
     * @param {Number[]} start
     *  The logical start position for the new tabulator.
     *
     * @param {Object} [attrs]
     *  Attributes to be applied at the new drawing component, as map of
     *  attribute maps (name/value pairs), keyed by attribute family.
     *
     * @param {Object} [target]
     *  If exists, defines node, to which start position is related.
     *  Used primary to address headers/footers.
     *
     * @returns {Boolean}
     *  Whether the drawing has been inserted successfully.
     */
    #implInsertDrawing(type, start, attrs, target) {

        // text span that will precede the field
        let span = null;
        // deep copy of attributes, because they are modified in webkit browsers
        const attributes = _.copy(attrs, true);
        // new drawing node
        let drawingNode = null;
        // root node container
        const rootNode = this.getRootNode(target);
        // image aspect ratio
        const currentElement = getContentNodeElement(rootNode, start.slice(0, -1), { allowDrawingGroup: true });
        // whether the drawing is inserted into a drawing group
        let insertIntoDrawingGroup = false;
        // the function used to insert the new drawing frame
        let insertFunction = 'insertAfter';
        // the number of children in the drawing group
        let childrenCount = 0;

        // helper function to insert a drawing frame into an existing drawing group
        const addDrawingFrameIntoDrawingGroup = () => {

            childrenCount = DrawingFrame.getGroupDrawingCount(currentElement);

            if (_.isNumber(childrenCount)) {
                if (childrenCount === 0) {
                    if (_.last(start) === 0) {
                        span = $(currentElement).children().first(); // using 'span' for the content element in the drawing group
                        insertFunction = 'appendTo';
                    }
                } else {
                    if (_.last(start) === 0) {
                        // inserting before the first element
                        span = DrawingFrame.getGroupDrawingChildren(currentElement, 0);
                        insertFunction = 'insertBefore';
                    } else if (_.last(start) <= childrenCount) {
                        // inserting after the span element
                        span = DrawingFrame.getGroupDrawingChildren(currentElement, _.last(start) - 1);
                    }
                }
                insertIntoDrawingGroup = true;
            }
        };

        try {
            span = this.#prepareTextSpanForInsertion(start, {}, target);
        } catch {
            // do nothing, try to repair missing text spans
        }

        // check, if the drawing is inserted into a drawing group
        if (!span && DrawingFrame.isGroupDrawingFrame(currentElement)) { addDrawingFrameIntoDrawingGroup(); }

        if (!span) { return false; }

        // expanding attrs to disable change tracking, if not explicitely set
        attrs = this.#checkChangesMode(attrs);

        // insert the drawing with default settings between the two text nodes (store original URL for later use)
        drawingNode = DrawingFrame.createDrawingFrame(type)[insertFunction](span);

        if (type === 'horizontal_line') { drawingNode.addClass('horizontal-line'); }

        // during the loading phase, the class "inline" can be added immediately (required for attribute inheritance, DOCS-4167)
        if (!this.isImportFinished() && attributes?.drawing?.inline) { drawingNode.addClass('inline'); }

        // apply the passed drawing attributes
        if (_.isObject(attributes)) { this.drawingStyles.setElementAttributes(drawingNode, attributes); }

        // validate paragraph, store new cursor position
        if (!insertIntoDrawingGroup) { this.#implParagraphChanged(span.parentNode); }

        this.#lastOperationEnd = increaseLastIndex(start);

        // don't call explicitly page break rendering, if target for header/footer comes with operation
        if (target) { this.setBlockOnInsertPageBreaks(true); }
        if ($(currentElement).data('lineBreaksData')) {
            $(currentElement).removeData('lineBreaksData'); // we changed paragraph layout, cached line breaks data needs to be invalidated
        }
        this.#insertPageBreaksDebounced(currentElement, insertIntoDrawingGroup ? null : DrawingFrame.getClosestTextFrameDrawingNode(span.parentNode));
        if (!this.#pbState) { this.docApp.getView().recalculateDocumentMargin(); }

        // marking drawing as marginal for external operations (58739)
        if (!this.docApp.isEditable() && DOM.isMarginalNode(span.parentNode)) { drawingNode.addClass(DOM.MARGINAL_NODE_CLASSNAME); }

        return true;
    }

    /**
     * In case of OT, the header/footer might have been activated in the meantime
     *
     * @param {HTMLElement|jQuery} singleDrawingNode
     *  One dawing node that needs to be updated.
     *
     * @returns {Boolean}
     *  The drawing node that really needs to be updated. In the case of an activated
     *  header or footer, this is no longer the node in the placeholder template.
     */
    #checkActiveHFDrawingNode(singleDrawingNode) {

        let drawingNode = singleDrawingNode;

        if (this.isHeaderFooterEditState() && DOM.isMarginalNode(singleDrawingNode.parent()) && this.#pageLayout.getHeaderFooterPlaceHolder().find(singleDrawingNode)) {
            // comparing the targets of the marginal nodes
            const targetNode = DOM.getClosestMarginalTargetNode(singleDrawingNode);
            const target = targetNode.attr('data-container-id');

            if (this.getActiveTarget() === target) {
                // finding the correct drawing node inside the active header/footer
                const allDrawingsInTemplateTarget = targetNode.find(DOM.DRAWING_NODE_SELECTOR);
                const index = allDrawingsInTemplateTarget.index(singleDrawingNode);
                if (index >= 0) {
                    const activeDrawingNodes = this.getCurrentRootNode().find(DOM.DRAWING_NODE_SELECTOR);
                    if (activeDrawingNodes.length > index) {
                        drawingNode = activeDrawingNodes.get(index);
                    }
                }
            }
        }

        return drawingNode;
    }

    /**
     * Inserts the passed content node at the specified logical position.
     *
     * @param {Number[]} position
     *  The logical position of the new content node.
     *
     * @param {HTMLElement|jQuery} node
     *  The new content node. If this object is a jQuery collection, uses
     *  the first node it contains.
     *
     * @param {String} [target]
     *  If exists, this string determines class name of container node.
     *  Usually is used for headers/footers
     *
     * @returns {Boolean}
     *  Whether the content node has been inserted successfully.
     */
    #insertContentNode(position, node, target) {

        const origPosition = _.clone(position);
        const index = _.last(position);
        let contentNode = null;
        let insertBefore = true;
        let insertAtEnd = false;
        let parentNode = null;
        let rootNode = null;

        const getContainerNode = position => {

            // logical position of the paragraph container node
            const parentPosition = position.slice(0, -1);
            // the container node for the paragraph
            const containerInfo = getDOMPosition(rootNode, parentPosition, true);
            // the parent container node
            let containerNode = null;

            // resolve component node to the correct node that contains the
            // child content nodes (e.g. resolve table cell elements to the
            // embedded div.cellcontent elements, or drawing elements to the
            // embedded div.content elements)
            if (containerInfo && containerInfo.node) {
                containerNode = DOM.getChildContainerNode(containerInfo.node)[0];

                // preparing text frame node, if this is a placeholder node inside a drawing frame of type 'shape'
                // if (DrawingFrame.isPlaceHolderNode(containerNode) && DrawingFrame.isShapeDrawingFrame(containerInfo.node)) {
                if (DrawingFrame.isShapeDrawingFrame(containerInfo.node)) {

                    // the child node can be a placeholder or an empty content node (empty during loading)
                    if (DrawingFrame.isPlaceHolderNode(containerNode) || DrawingFrame.isEmptyDrawingContentNode(containerNode)) {
                        // preparing text frame structure in drawing
                        containerNode = DrawingFrame.prepareDrawingFrameForTextInsertion(containerInfo.node);
                        // update formatting, otherwise styles (border, background, ...) will be lost
                        // -> but waiting until the paragraph is inserted (executing delayed), so that also handling for empty
                        //    place holder drawings is done
                        if (this.isImportFinished()) {
                            this.executeDelayed(() => {
                                let singleDrawingNode = DrawingFrame.getDrawingNode(containerNode);
                                if (this.docApp.isOTEnabled()) { singleDrawingNode = this.#checkActiveHFDrawingNode(singleDrawingNode); }
                                this.drawingStyles.updateElementFormatting(singleDrawingNode);
                            });
                        }
                        // #36327 - TF: Auto sizing for TF doesn't respect page size (not for OX Presentation)
                        if (!this.useSlideMode()) { containerNode.css('max-height', this.#pageLayout.getDefPageActiveHeight({ convertToPixel: true }) - 15); } // 15 px is rounding for borders and inner space of textframe
                    }
                }
            }

            return containerNode;
        };

        // setting the correct root node dependent from an optional target
        rootNode = this.getRootNode(target);

        // trying to find existing paragraphs, tables or the parent element
        if (index > -1) {

            try {
                contentNode = getContentNodeElement(rootNode, position);
            } catch {
                contentNode = null;  // trying to repair in the following code
            }

            // content node does not exist -> maybe the new paragraph/table shall be added to the end of the document
            if (!contentNode) {

                if (index === 0) {
                    // trying to find parent node, because this is the first paragraph/table in document or table cell or text frame or comment
                    parentNode = getContainerNode(position);

                    if (!parentNode) {
                        globalLogger.warn('Editor.#insertContentNode(): cannot find parent node at position ' + JSON.stringify(position));
                        return false;
                    }
                    insertBefore = false;
                    insertAtEnd = true;

                } else {
                    // Further try to find an existing element, behind that the new paragraph/table shall be appended
                    position[position.length - 1]--;
                    contentNode = getContentNodeElement(rootNode, position, { allowImplicitParagraphs: false });

                    if (contentNode) {
                        insertBefore = false;
                    } else {
                        globalLogger.warn('Editor.#insertContentNode(): cannot find content node at position ' + JSON.stringify(origPosition));
                        return false;
                    }
                }
            }
        } else if (index === -1) { // Definition: Adding paragraph/table to the end if index is -1
            if (target) {
                parentNode = rootNode;
            } else {
                parentNode = getContainerNode(position);
            }
            if (!parentNode) {
                globalLogger.warn('Editor.#insertContentNode(): cannot find parent node at position ' + JSON.stringify(position));
                return false;
            }
            insertBefore = false;
            insertAtEnd = true;
        } else {
            globalLogger.warn('Editor.#insertContentNode(): invalid logical position ' + JSON.stringify(position));
            return false;
        }

        // modify the DOM
        if (insertAtEnd) {
            $(node).first().appendTo(parentNode);
        } else if (insertBefore) {
            $(node).first().insertBefore(contentNode);
        } else {
            $(node).first().insertAfter(contentNode);
        }

        return true;
    }

    /**
     * Creates and inserts Header or footer with passes id and type
     *
     * @param {String} id
     * @param {String} type
     * @param {Object} attrs
     *
     * @returns {Boolean}
     */
    #implInsertHeaderFooter(id, type, attrs, external) {
        this.#pageLayout.implCreateHeaderFooter(id, type, attrs, this.#undoRedoRunning, external);
        return true;
    }

    /**
     * Deletes and removes from DOM node with passed id
     *
     * @param {String} id
     *
     * @returns {Boolean}
     */
    #implDeleteHeaderFooter(id) {
        return this.#pageLayout.implDeleteHeaderFooter(id);
    }

    #implMove(_start, _end, _to, target) {

        const start = _.copy(_start, true);
        const to = _.copy(_to, true);
        const activeRootNode = this.getRootNode(target);
        const sourcePos = getDOMPosition(activeRootNode, start, true);
        const destPos = getDOMPosition(activeRootNode, to, true);
        let insertBefore = true;
        let splitNode = false;
        // a text span before the moved node before moving the node
        let prevTextSpan = 0;

        // Fix for 28634 -> Moving a drawing to position with tab or hard break
        // Fix for DOCS-4605: Also using previous sibling for rangemarker nodes
        if (DOM.isHardBreakNode(destPos.node) || DOM.isTabNode(destPos.node) || DOM.isFieldNode(destPos.node) || DOM.isRangeMarkerNode(destPos.node)) {
            destPos.node = destPos.node.previousSibling;
        } else if (DrawingFrame.isDrawingFrame(destPos.node) && DOM.isDrawingLayerNode(destPos.node.parentNode)) {
            destPos.node = DOM.getDrawingPlaceHolderNode(destPos.node).previousSibling;
        }

        if (destPos.offset === 0) {
            insertBefore = true;
        } else if ((destPos.node.length) && (destPos.offset === (destPos.node.length - 1))) {
            insertBefore = false;
        } else if ((DrawingFrame.isDrawingFrame(destPos.node)) && (destPos.offset === 1)) {
            insertBefore = false;
        } else {
            splitNode = true;  // splitting node is required
            insertBefore = false;  // inserting after new created text node
        }

        if ((sourcePos) && (destPos)) {

            const sourceNode = sourcePos.node;
            let destNode = destPos.node;
            let useOffsetDiv = true;
            const offsetDiv = sourceNode.previousSibling;
            let doMove = true;
            let oldPara = null;

            if ((sourceNode) && (destNode)) {

                if (!DrawingFrame.isDrawingFrame(sourceNode)) {
                    doMove = false; // supporting only drawings at the moment
                    globalLogger.warn('Editor.#implMove(): moved  node is not a drawing: ' + getNodeName(sourceNode));
                } else {
                    // also move the offset divs
                    if ((!offsetDiv) || (!DOM.isOffsetNode(offsetDiv))) { useOffsetDiv = false; }
                }

                if (doMove) {

                    if (splitNode) {
                        destNode = DOM.splitTextSpan(destNode, destPos.offset + 1)[0];
                    } else {
                        if (destNode.nodeType === 3) { destNode = destNode.parentNode; }
                    }

                    // using empty span as reference for inserting new components
                    if ((DrawingFrame.isDrawingFrame(destNode)) && (DOM.isOffsetNode(destNode.previousSibling))) {
                        destNode = destNode.previousSibling;  // switching temporary to offset
                    }

                    // there can be empty text spans before the destination node
                    if ((DOM.isTextSpan(destNode)) || (DrawingFrame.isDrawingFrame(destNode)) || (DOM.isOffsetNode(destNode))) {
                        while (DOM.isEmptySpan(destNode.previousSibling)) {
                            destNode = destNode.previousSibling;
                        }
                    }

                    if ((insertBefore) && (DOM.isTextSpan(destNode))) {
                        destNode = DOM.splitTextSpan(destNode, 0)[0]; // taking care of empty text span before drawing
                        insertBefore = false;  // insert drawing behind new empty text span
                    }

                    // removing empty text spans behind or after the source node
                    if ((sourceNode.previousSibling) && (sourceNode.nextSibling)) {
                        if ((DOM.isTextSpan(sourceNode.previousSibling)) && (DOM.isEmptySpan(sourceNode.nextSibling))) {
                            $(sourceNode.nextSibling).remove();
                        } else if ((DOM.isEmptySpan(sourceNode.previousSibling)) && (DOM.isTextSpan(sourceNode.nextSibling))) {
                            $(sourceNode.previousSibling).remove();
                        }
                    }

                    if ((sourceNode.previousSibling) && (sourceNode.previousSibling.previousSibling) && (sourceNode.nextSibling) && (DOM.isOffsetNode(sourceNode.previousSibling))) {
                        if ((DOM.isTextSpan(sourceNode.previousSibling.previousSibling)) && (DOM.isEmptySpan(sourceNode.nextSibling))) {
                            $(sourceNode.nextSibling).remove();
                        } else if ((DOM.isEmptySpan(sourceNode.previousSibling.previousSibling)) && (DOM.isTextSpan(sourceNode.nextSibling))) {
                            $(sourceNode.previousSibling.previousSibling).remove();
                        }
                    }

                    // saving the parent paragraphs for later handling
                    if ($(sourceNode).data('textWrapMode') === 'none') { oldPara = sourceNode.parentNode; }

                    // saving the old previous text span for later merge
                    if (sourceNode.previousSibling && DOM.isTextSpan(sourceNode.previousSibling)) { prevTextSpan = sourceNode.previousSibling; }

                    // moving the drawing
                    if (insertBefore) {
                        $(sourceNode).insertBefore(destNode);
                    } else {
                        $(sourceNode).insertAfter(destNode);
                    }

                    // moving also the corresponding div before the moved drawing
                    if (useOffsetDiv) { $(offsetDiv).insertBefore(sourceNode); }

                    // merging text spans, if possible
                    if (prevTextSpan) { mergeSiblingTextSpans(prevTextSpan, true); }

                    this.#implParagraphChanged(to);

                    // checking z-index of the source and the destination paragraph
                    if (oldPara && this.docApp.isTextApp() && this.isIncreasedDocContent()) {
                        handleParagraphIndex(oldPara);
                        handleParagraphIndex(sourceNode.parentNode); // this check is only required for undo operation
                    }
                }
            }
        }

        return true;
    }

    /**
     * Check, wheter a given paragraph node describes a paragraph that
     * is part of a list (bullet or numbering list).
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph whose attributes will be checked. If this object is a
     *  jQuery collection, uses the first DOM node it contains.
     *
     * @param {Object} [attributes]
     *  An optional map of attribute maps (name/value pairs), keyed by attribute.
     *  It this parameter is defined, the parameter 'paragraph' can be omitted.
     *
     * @returns {Boolean}
     *  Whether the content node is a list paragraph node.
     */
    #isListStyleParagraph(paragraph, attributes) {

        // the attributes directly assigned to the paragraph
        const paraAttrs = attributes || getExplicitAttributeSet(paragraph);

        // shortcut, not handling style attributes
        if (paraAttrs && paraAttrs.paragraph && paraAttrs.paragraph.listStyleId) { return true; }

        // checking paragraph style
        if (attributes && attributes.styleId && this.isParagraphStyleWithListStyle(attributes.styleId)) { return true; }

        return false;
    }

    /**
     * After reloading a document, some updates are necessary in the model.
     * For example refreshing the collector for artificial paragraphs (IE and
     * Firefox) or cells (none Firefox).
     */
    #documentReloadHandler() {

        let paragraphs = null;
        let cells = null;

        if (!_.isEmpty(this.#highlightedParagraphs) && (_.browser.Firefox || _.browser.IE) && this.#selection.hasRange()) {
            paragraphs = DOM.getPageContentNode(this.#editdiv).find('div.selectionHighlighting');
            this.#highlightedParagraphs = $.makeArray(paragraphs); // converting to array
        }

        if (!_.isEmpty(this.#highlightedCells) && !_.browser.Firefox && this.#selection.hasRange()) {
            cells = DOM.getPageContentNode(this.#editdiv).find('td.cellSelectionHighlighting');
            this.#highlightedCells = $.makeArray(cells); // converting to array
        }
    }

    /**
     * The handler this is triggered after a change of the edit privileges.
     */
    #changeEditModeHandler(editMode) {

        // set CSS class for formatting dependent on edit mode (mouse pointers)
        this.#editdiv.toggleClass('edit-mode', editMode);

        // Fix for 30992, setting valid text position after receiving edit rights
        if (editMode) {

            if (this.useSlideMode()) {
                this.#selection.restoreAllDrawingSelections();
            } else {

                if (isValidTextPosition(this.#editdiv, this.#selection.getStartPosition())) {
                    if (isValidTextPosition(this.#editdiv, this.#selection.getEndPosition())) {
                        this.#selection.setTextSelection(this.#selection.getStartPosition(), this.#selection.getEndPosition());
                    } else {
                        this.#selection.setTextSelection(this.#selection.getStartPosition());
                    }
                } else if (this.#selection.isDrawingSelection()) {
                    this.#selection.restoreAllDrawingSelections();
                } else if ((!_.isEmpty(this.#selection.getStartPosition())) && (isValidTextPosition(this.#editdiv, getFirstPositionInParagraph(this.#editdiv, [this.#selection.getStartPosition()[0]])))) {
                    this.#selection.setTextSelection(getFirstPositionInParagraph(this.#editdiv, [this.#selection.getStartPosition()[0]]));
                } else if (this.#lastOperationEnd && isValidTextPosition(this.#editdiv, this.#lastOperationEnd)) {
                    this.#selection.setTextSelection(this.#lastOperationEnd);
                } else {
                    this.#selection.selectTopPosition();
                }
            }
        }

        // in read-only mode change drawing selection to text selection
        if (!editMode) {

            if (this.useSlideMode()) {
                this.#selection.restoreAllDrawingSelections();
            } else {
                if (this.#selection.isDrawingSelection()) {
                    this.#selection.restoreAllDrawingSelections();
                } else {
                    this.#selection.selectDrawingAsText();
                }

                if (this.isHeaderFooterEditState()) {
                    this.#pageLayout.leaveHeaderFooterEditMode();
                    this.#selection.setNewRootNode(this.#editdiv);
                    this.#selection.setTextSelection(this.#selection.getFirstDocumentPosition()); // #43083
                }
            }
        }

        // We are not able to disable the context menu in read-only mode on iPad
        // or other touch devices. Therefore we just set contenteditable dependent
        // on the current editMode.
        if (TOUCH_DEVICE) {
            // prevent virtual keyboard on touch devices in read-only mode
            this.#editdiv.attr('contenteditable', editMode);
        }

        // disable IE table manipulation handlers in edit mode
        // getDomNode(this.#editdiv).onresizestart = () => { return false; };
        // The resizestart event does not appear to bubble in IE9+, so we use the selectionchange event to bind
        // the resizestart handler directly once the user selects an object (as this is when the handles appear).
        // The MS docs (http://msdn.microsoft.com/en-us/library/ie/ms536961%28v=vs.85%29.aspx) say it's not
        // cancelable, but it seems to work in practice.

        // The oncontrolselect event fires when the user is about to make a control selection of the object.
        // Non-standard event defined by Microsoft for use in Internet Explorer.
        // This event fires before the element is selected, so inspecting the selection object gives no information about the element to be selected.
        if (_.browser.IE) {
            getDomNode(this.#editdiv).oncontrolselect = e => {
                if (DOM.isImageNode(e.srcElement)) {
                    // if an image was selected, return browser selection to the image clipboard
                    this.#selection.restoreBrowserSelection();
                }
                // return false to suppress IE default size grippers
                return false;
            };
        }

        // focus back to editor
        this.docApp.getView().grabFocus();
    }

    /**
     * The handler function that is triggered if the selection changes into a textframe.
     *
     * @param {Node} drawingNode
     *  The drawing node that contains the new selection. In a group this is the child drawing node.
     */
    #textframeEnterHandler(drawingNode) {
        this.#enterState = this.getOperationStateNumber();

        // saving width and height of the drawing
        this.#savedDrawingSize = this.#getDrawingDimensions(drawingNode);
    }

    /**
     * The handler function that is triggered if the selection is no longer inside a textframe.
     *
     * @param {Node} drawingNode
     *  The drawing node that no longer contains the selection. In a group this is the child drawing node.
     */
    #textframeLeaveHandler(drawingNode) {

        if (!this.isProcessingExternalOperations()) { // never send operations, while external OPs are processed -> recursive call (DOCS-4431)
            if (this.docApp.isEditable() && this.#enterState !== this.getOperationStateNumber()) { // there was at least one operation
                if (DrawingFrame.isAutoResizeHeightDrawingFrame(drawingNode)) {

                    // the current target
                    const target = this.getActiveTarget();
                    // the attributes operation to inform filter about drawing height
                    let operation = null;
                    // current element container node
                    const rootNode = this.getCurrentRootNode(target);
                    // the logical position of the table drawing
                    const pos = getOxoPosition(rootNode, drawingNode);

                    if (pos && isValidElementPosition(rootNode, pos, { allowFinalPosition: true })) {

                        const newDrawingSize = this.#getDrawingDimensions(drawingNode);
                        const drawingSizeChanged = !this.#savedDrawingSize || this.#savedDrawingSize.width !== newDrawingSize.width || this.#savedDrawingSize.height !== newDrawingSize.height;

                        if (drawingSizeChanged) {
                            operation = {
                                name: Op.SET_ATTRIBUTES,
                                noUndo: true,
                                start: _.copy(pos),
                                attrs: { drawing: newDrawingSize },
                                target: target ? target : this.getOperationTargetBlocker()
                            };
                            this.sendOperations(operation); // only sending operation, not applying it locally
                        }
                    }
                }
            }
        }

        this.#savedDrawingSize = null;
    }

    /**
     * A function that returns the dimensions of a drawing node
     *
     * @param {Node} drawingNode
     * The drawing node whose current size (px) should be converted to the size that is we use for operations
     * (taking into account that the drawingNode can be child of a group)
     *
     * @returns {object} size
     * returns the converted pixel size of the drawing node, normally this is 100thmm but in case that the
     * drawing node is child of a group the dimension of the parent group is used.
     *
     */
    #getDrawingDimensions(drawingNode) {

        const size = { width: convertLengthToHmm($(drawingNode).width(), 'px'), height: convertLengthToHmm($(drawingNode).height(), 'px') };
        if (DrawingFrame.isGroupedDrawingFrame(drawingNode)) {
            const parentNode = DrawingFrame.getGroupNode(drawingNode);
            const parentSize = { width: convertLengthToHmm($(parentNode).width(), 'px'), height: convertLengthToHmm($(parentNode).height(), 'px') };
            const parentAttrs = getExplicitAttributes(parentNode, 'drawing');

            if (parentAttrs.childWidth && parentSize.width > 0) {
                size.width *= (parentAttrs.childWidth / parentSize.width);
            }

            if (parentAttrs.childHeight && parentSize.height > 0) {
                size.height *= (parentAttrs.childHeight / parentSize.height);
            }
        }
        return size;
    }

    /**
     * Listener to the 'change' event triggered by the selection.
     *
     * @param {Object} options
     *  Optional parameters:
     *  @param {Boolean} [options.insertOperation=false]
     *      Whether this function was triggered by an insert operation, for example 'insertText'.
     *  @param {Boolean} [options.splitOperation=false]
     *      Whether this function was triggered by an split paragraph operation.
     *  @param {Boolean} [options.keepChangeTrackPopup=false]
     *      Whether the change track pop up triggered this change of selection.
     *  @param {jQuery.Event} [options.event=null]
     *      The original browser event passed to this method
     */
    #selectionChangeHandler(options) {

        // whether an implicit paragraph behind a table has to be increased
        let increaseParagraph = false;
        let domNode = null, paraNode = null, rowNode = null, tableNode = null;
        let explicitAttributes = null;
        let startPosition = null;
        let nodeInfo = null;
        const insertOperation = getBooleanOption(options, 'insertOperation', false);
        const splitOperation = getBooleanOption(options, 'splitOperation', false);
        const keepChangeTrackPopup = getBooleanOption(options, 'keepChangeTrackPopup', false);
        const noscroll = getBooleanOption(options, 'noscroll', false);
        const browserEvent = getObjectOption(options, 'event', null);
        const readOnly = !this.docApp.isEditable();
        // selection hightlighting: dom point of start and end position
        let startNodeInfo = null, endNodeInfo = null;
        // selection hightlighting: paragraph dom nodes
        let nextParagraph = null, startParagraph = null, endParagraph = null;
        // whether the iteration reached the end paragraph
        let reachedEndParagraph = false;
        // helper nodes for explicit paragraphs inside text frames
        let drawing = null, drawingBorder = null;
        // target for operation - if exists, it's for ex. header or footer
        const target = this.getActiveTarget();
        // current element container node
        const thisRootNode = this.getCurrentRootNode(target);

        // handling height of implicit paragraphs, but not for insertText operations or splitting of paragraphs
        // and also not in readonly mode (28563)
        if (!insertOperation && !splitOperation && !readOnly) {

            startPosition = this.#selection.getStartPosition();
            nodeInfo = getDOMPosition(thisRootNode, startPosition);

            // if this is the last row of a table, a following implicit paragraph has to get height zero or auto
            if (isPositionInTableImport(thisRootNode, startPosition)) {
                domNode = nodeInfo.node;
                paraNode = $(domNode).closest(DOM.PARAGRAPH_NODE_SELECTOR);
                rowNode = $(paraNode).closest(DOM.TABLE_ROWNODE_SELECTOR);
                tableNode = $(rowNode).closest(DOM.TABLE_NODE_SELECTOR);

                if (paraNode !== null) {
                    if (paraNode.get(0) === this.#increasedParagraphNode) {
                        increaseParagraph = true;  // avoid, that height is set to 0, if cursor is in this paragraph
                        if (paraNode.prev().length === 0) {  // maybe the previous table was just removed
                            paraNode.css('height', '');  // the current paragraph always has to be increased
                            this.#increasedParagraphNode = null;
                        }
                    } else if (DOM.isIncreasableParagraph(paraNode)) {
                        if (this.#increasedParagraphNode !== null) { $(this.#increasedParagraphNode).css('height', 0); } // useful for tables in tables in tables in ...
                        paraNode.css('height', '');
                        increaseParagraph = true;   // cursor travelling from right/button directly into the explicit paragraph
                        this.#increasedParagraphNode = paraNode.get(0);
                        // in Internet Explorer it is necessary to restore the browser selection, so that no frame is drawn around the paragraph (28132)
                        if (_.browser.IE) { this.#selection.restoreBrowserSelection(); }
                    } else if ((DOM.isChildTableNode(tableNode)) && ($(rowNode).next().length === 0) && (DOM.isIncreasableParagraph($(tableNode).next()))) {
                        if (this.#increasedParagraphNode !== null) { $(this.#increasedParagraphNode).css('height', 0); } // useful for tables in tables in tables in ...
                        $(tableNode).next().css('height', '');
                        increaseParagraph = true;
                        this.#increasedParagraphNode = $(tableNode).next().get(0);
                    } else if ((paraNode.prev().length === 0) && (paraNode.height() === 0)) {
                        // if the cursor was not in the table, when the inner table was deleted -> there was no this.#increasedParagraphNode behind the table
                        paraNode.css('height', '');  // the current paragraph always has to be increased, maybe a previous table just was deleted (undo)
                    }
                }
            } else {

                // check if the currently increased paragraph behind a table is clicked (for example inside a text frame)
                if (this.#increasedParagraphNode) {
                    domNode = nodeInfo.node;
                    paraNode = domNode && $(domNode).closest(DOM.PARAGRAPH_NODE_SELECTOR);
                    if (paraNode && getDomNode(paraNode) === this.#increasedParagraphNode) {
                        increaseParagraph = true;  // avoid, that height is set to 0, if cursor is in this paragraph
                    }
                }
            }

            // is the increased paragraph node inside a text frame?
            drawing = $(this.#increasedParagraphNode).closest(DrawingFrame.NODE_SELECTOR);

            // if the implicit paragraph no longer has to get an increase height
            if (!increaseParagraph && this.#increasedParagraphNode) {
                // can the increased paragraph node still be decreased (not if it is no longer increasable)
                if (DOM.isIncreasableParagraph(this.#increasedParagraphNode)) { $(this.#increasedParagraphNode).css('height', 0); }
                this.#increasedParagraphNode = null;

            }

            // check, if the drawing border needs to be repainted (canvas)
            if (drawing.length > 0) {
                drawingBorder = DrawingFrame.getCanvasNode(drawing);
                if (drawingBorder.length > 0) { this.trigger('drawingHeight:update', drawing); }
            }

        }

        // in Internet Explorer it is necessary to restore the browser selection, so that no frame is drawn around the paragraph (28132),
        // this part for the read only mode
        if (readOnly && _.browser.IE && !isPositionInTableImport(thisRootNode, this.#selection.getStartPosition())) {
            nodeInfo = getDOMPosition(thisRootNode, this.#selection.getStartPosition());
            if (nodeInfo) {
                paraNode = $(nodeInfo.node).closest(DOM.PARAGRAPH_NODE_SELECTOR);
                if ((DOM.isImplicitParagraphNode(paraNode)) && (DOM.isFinalParagraphBehindTable(paraNode))) {
                    paraNode.css('height', '');
                    this.#selection.restoreBrowserSelection();
                }
            }
        }

        // clear preselected attributes when selection changes
        if (!this.#keepPreselectedAttributes) { this.#preselectedAttributes = null; }

        // Special behaviour for cursor positions directly behind fields.
        // -> the character attributes of the field have to be used
        // -> using this.#preselectedAttributes
        if (!insertOperation && nodeInfo && (nodeInfo.offset === 0) && nodeInfo.node && nodeInfo.node.parentNode && DOM.isFieldNode(nodeInfo.node.parentNode.previousSibling)) {
            explicitAttributes = getExplicitAttributeSet(nodeInfo.node.parentNode.previousSibling.firstChild);
            this.addPreselectedAttributes(explicitAttributes);
        }

        // removing highlighting of selection of empty paragraphs in Firefox and IE
        if (!_.isEmpty(this.#highlightedParagraphs) || !_.isEmpty(this.#highlightedCells)) { this.removeArtificalHighlighting(); }

        // highlighting selection of empty paragraphs in Firefox and IE
        // fix for Bug 47820
        if ((_.browser.Firefox || _.browser.IE) && this.#selection.hasRange() && this.#selection.getSelectionType() !== 'cell') {
            startNodeInfo = getDOMPosition(thisRootNode, this.#selection.getStartPosition());
            endNodeInfo = getDOMPosition(thisRootNode, this.#selection.getEndPosition());

            if (startNodeInfo && startNodeInfo.node && endNodeInfo && endNodeInfo.node) {
                startParagraph = getDomNode($(startNodeInfo.node).closest(DOM.PARAGRAPH_NODE_SELECTOR));
                endParagraph = getDomNode($(endNodeInfo.node).closest(DOM.PARAGRAPH_NODE_SELECTOR));

                if (startParagraph && endParagraph && startParagraph !== endParagraph) {
                    nextParagraph = startParagraph;

                    while (nextParagraph && !reachedEndParagraph) {

                        if (nextParagraph) {

                            // only highlight empty paragraphs
                            if (getParagraphNodeLength(nextParagraph) === 0) {
                                // collecting all modified paragraphs in an array
                                this.#highlightedParagraphs.push(nextParagraph);
                                // modify the background color of the empty paragraph to simulate selection
                                // Task 35375: Using class, so that background-color is not included into copy/paste
                                $(nextParagraph).addClass('selectionHighlighting');
                            }

                            if (nextParagraph === endParagraph) {
                                reachedEndParagraph = true;
                            } else {
                                nextParagraph = findNextNode(thisRootNode, nextParagraph, DOM.PARAGRAPH_NODE_SELECTOR);
                            }
                        }
                    }
                }
            }
        }

        // highlighting table cells in the selection, if this is not a Firefox browser
        if (!_.browser.Firefox && !TOUCH_DEVICE && this.#selection.hasRange() && this.#selection.isTextSelection()) {
            this.#selection.iterateTableCells(cellNode => { this.#highlightedCells.push(cellNode); });
            this.#highlightedCells.forEach(cellNode => { $(cellNode).addClass('cellSelectionHighlighting'); });
        }

        // forward selection change events to own listeners (view and controller)
        this.trigger('selection', this.#selection, { insertOperation, browserEvent, keepChangeTrackPopup, updatingRemoteSelection: this.#updatingRemoteSelection, noscroll });
    }

    /**
     * Listener to the events 'undo:after' and 'redo:after'.
     *
     * @param {boolean} isRedo
     *  Whether to handle undo operations (`false`) or redo operations (`true`).
     *
     * @param {Array<Object>} operations
     *  An array containing the undo or redo operations.
     *
     * @param {Object} [options]
     *  Additional options.
     */
    #undoRedoAfterHandler(isRedo, operations, options) {

        if (this.destroyed) { return; } // the model might have been destroyed in the meantime

        let para = null;
        let newParagraph = null;
        let pageContentNode = null;
        const lastOperation = _.last(operations);
        // the seection state to be applied
        let selectionState = options && options.selectionState;
        // whether the saved selection state can be used (not in redo (53773))
        let useSelectionState = !!(selectionState && selectionState.start && selectionState.end && !isRedo);
        // the target belonging to the logical position
        const target = useSelectionState ? selectionState.target : (lastOperation && lastOperation.target); // handle empty lastOperation (41502)
        const isCreateDeleteHeaderFooterOp = lastOperation ? (lastOperation.name === 'insertHeaderFooter' || lastOperation.name === 'deleteHeaderFooter') : false;
        const $rootNode = this.getRootNode(target);
        let localRootNode = $rootNode;
        // a temporary logical position
        let tempLastOperationEnd = null;
        // holder for filtered set attributes operation for rotation of drawings
        let rotationOps = null;
        // a collector of all affected targets during this undo/redo
        const targetCollector = [];
        // whether the page size was modified in undo/redo
        let pageSizeChanged = false;

        // helper function to activate the header/footer edit mode after undo
        const activateHeaderFooterEditMode = () => {
            if (this.docApp.isOTEnabled() && $rootNode.parent().hasClass(DOM.HEADER_FOOTER_PLACEHOLDER_CLASSNAME)) {
                this.#undoRedoRunning = false;
                localRootNode = this.getRootNode(target); // receiving the header/footer node inside the page
                this.#undoRedoRunning = true;
            }

            if (!localRootNode.parent().hasClass(DOM.HEADER_FOOTER_PLACEHOLDER_CLASSNAME)) {
                this.#pageLayout.enterHeaderFooterEditMode(localRootNode);
                this.#selection.setNewRootNode(localRootNode);
                this.docApp.getView().scrollToChildNode(localRootNode);
            }
        };

        // unblock keyboard events again
        this.setBlockKeyboardEvent(false);

        // not blocking page break calculations after undo (40169)
        this.setBlockOnInsertPageBreaks(false);

        if (this.docApp.isTextApp()) {

            // group of undo operations, replace headers&footers after all operations are finnished
            if (_.findWhere(operations, { name: 'insertHeaderFooter' })) {
                this.#pageLayout.markAllUndoRedoNodesAsMarginal(_.where(operations, { name: 'insertHeaderFooter' }));
                this.#pageLayout.replaceAllTypesOfHeaderFooters();
                if (!this.docApp.isOTEnabled()) { this.#pageLayout.updateFormattingOfDrawingsInPage(_.where(operations, { name: 'insertHeaderFooter' })); } // 66982, only for the active editor in none-OT-mode
                this.#pageLayout.updateStartHeaderFooterStyle();
                this.setBlockOnInsertPageBreaks(false); // release block on insertPageBreaks to register debounced call
                this.#insertPageBreaksDebounced();
            }
            // odf needs also replacing of headers after delete (switching type)
            if (this.docApp.isODF() && _.findWhere(operations, { name: 'deleteHeaderFooter' })) {
                this.#pageLayout.replaceAllTypesOfHeaderFooters();
            }

            // return from edit mode of header/footer if undo/redo operation doesnt have target,
            // or is not create or delete headerFooter operation
            if (this.isHeaderFooterEditState() && !(target || isCreateDeleteHeaderFooterOp)) {
                this.#pageLayout.leaveHeaderFooterEditMode();
                this.#selection.setNewRootNode(this.#editdiv); // restore original rootNode
            } else if (target) {
                if (this.#pageLayout.isIdOfMarginalNode(target)) {
                    // jump into editing mode of header/footer with given target (first occurance in doc)
                    if (this.isHeaderFooterEditState()) {
                        // do nothing if we are already inside that node
                        if (this.getHeaderFooterRootNode()[0] !== $rootNode[0]) {
                            // first leave current node, and enter another
                            this.#pageLayout.leaveHeaderFooterEditMode(this.getHeaderFooterRootNode());
                            this.#pageLayout.updateEditingHeaderFooter(); // must be direct call
                            activateHeaderFooterEditMode();
                        }
                    } else {
                        activateHeaderFooterEditMode();
                        if (this.docApp.isODF()) { this.executeDelayed(this.#pageLayout.updatePageSimpleFields.bind(this.#pageLayout), 600); } // DOCS-2861
                    }
                }
            }

            // mark new inserted nodes in header/footer as 'marginal' (DOCS-1626)
            operations.forEach(op => { if (op.target && !_.contains(targetCollector, op.target)) { targetCollector.push(op.target); } });
            if (targetCollector.length) { this.#pageLayout.markAllTargetNodesAfterUndoRedoAsMarginal(targetCollector); }

            // check for a change of page size
            pageSizeChanged = operations.find(op => op.name === Op.CHANGE_CONFIG && (_.isNumber(op.attrs?.page?.width) || _.isNumber(op.attrs?.page?.height)));
        }

        // #31971, #32322
        pageContentNode = DOM.getPageContentNode(this.#editdiv);
        if (pageContentNode.find(DOM.CONTENT_NODE_SELECTOR).length === 0 && pageContentNode.find(DOM.PAGE_BREAK_SELECTOR).length > 0) { //cleanup of dom
            pageContentNode.find(DOM.PAGE_BREAK_SELECTOR).remove();

            newParagraph = DOM.createImplicitParagraphNode();
            pageContentNode.append(newParagraph);
            this.#validateParagraphNode(newParagraph);
            this.#implParagraphChanged(newParagraph);
        }

        // setting text selection to specified 'selectionState' or 'this.#lastOperationEnd' after undo/redo.
        // -> Spreadsheet can also use this.#lastOperationEnd, until a better process is available.
        if (!this.docApp.isPresentationApp() && !(options && options.preventSelectionChange)) {

            // trying to get a selection state for a redo operation (not relying on this.#lastOperationEnd)
            if (!useSelectionState && isRedo && !_.isEmpty(operations)) {
                if (_.every(operations, op => { return op.name === Op.SET_ATTRIBUTES; })) { // only setAttributes operations
                    selectionState = { start: _.copy(_.first(operations).start), end: increaseLastIndex(_.last(operations).end) };
                    useSelectionState = true;
                }
            }

            // using the predefined selection state, if it is specified
            if (useSelectionState) {
                this.docApp.getView().grabFocus(); // required to activate the selection range observer
                this.#selection.setTextSelection(selectionState.start, selectionState.end);
            } else if (this.#lastOperationEnd) {

                if (_.browser.IE) {
                    // 29397: Not using implicit paragraph in IE
                    para = getLastNodeFromPositionByNodeName($rootNode, this.#lastOperationEnd, DOM.PARAGRAPH_NODE_SELECTOR);
                    if (DOM.isImplicitParagraphNode(para) && para.previousSibling) {
                        tempLastOperationEnd = getLastPositionInParagraph($rootNode, getOxoPosition($rootNode, para.previousSibling, 0));
                    }

                    if (tempLastOperationEnd) { this.#lastOperationEnd = _.copy(tempLastOperationEnd); }
                }

                // undo might have inserted a table -> checking this.#lastOperationEnd (Firefox)
                if (!getLastNodeFromPositionByNodeName($rootNode, this.#lastOperationEnd, DOM.PARAGRAPH_NODE_SELECTOR)) {
                    this.#lastOperationEnd = getFirstPositionInParagraph($rootNode, this.#lastOperationEnd);
                }

                // setting selection before calling 'this.#implParagraphChangedSync' (35350)
                if (this.#lastOperationEnd) { this.#selection.setTextSelection(this.#lastOperationEnd); }

                // Avoiding cursor jumping, if new paragraph is empty (32454), indicated by setting cursor to position ending with '0'
                if (_.last(this.#lastOperationEnd) === 0) {
                    para = para || getLastNodeFromPositionByNodeName($rootNode, this.#lastOperationEnd, DOM.PARAGRAPH_NODE_SELECTOR);
                    if (para && !DOM.isSlideNode(para)) { this.#implParagraphChangedSync($(para)); }
                }
            }
        }

        // check if it is undo/redo operation of drawing rotate, and update corespodning resize mouse pointers
        rotationOps = _.find(operations, a => { return a.attrs && a.attrs.drawing && !_.isUndefined(a.attrs.drawing.rotation); });
        if (rotationOps) {
            const selectedDrawings = this.#selection.getAllDrawingsInSelection();
            if (selectedDrawings) {
                _.each(selectedDrawings, drawingNode => {
                    const angle = DrawingFrame.getDrawingRotationAngle(this, drawingNode) || 0;
                    DrawingFrame.updateResizersMousePointers(drawingNode, angle);
                });
            }
        }

        if (pageSizeChanged) { // can only be true in OX Text
            // leaving an optionally activated header or footer edit mode
            this.getPageLayout().leaveHeaderFooterAndSetSelection();
            // update all drawings with the class "relativesize", because the setDocumentAttributes operation is typically the last in "undo"
            _.each(this.#collectRelativeSizeDrawingNodes(), drawingNode => { this.drawingStyles.updateElementFormatting(drawingNode); });
            // handling page breaks (undo in DOCS-3813_B)
            this.setTimeout(() => { this.insertPageBreaks(); }, 750);
        }

        // this must be executed, never leave function before!
        this.#undoRedoRunning = false;

        // in Edge browser it is necessary, to set the focus into the textframe of the shape or the comment node
        if (_.browser.Edge && this.docApp.isTextApp()) {
            let textFrame = null;
            if (this.#selection.isAdditionalTextframeSelection()) {
                textFrame = this.#selection.getSelectedTextFrameDrawing().find('div.textframe');
            }
            if (textFrame && textFrame.length > 0) { setFocus(textFrame); }
        }

        // after undo/redo, start formatting the paragraph colors and border
        this.startParagraphMarginColorAndBorderFormating({ filledAtUndoRedo: true });

        // Text only: Check for implicit paragraphs in shapes (53983)
        if (!this.docApp.isODF() && this.docApp.isTextApp()) { this.checkInsertedEmptyTextShapes(operations); }

        // hide old/invalid change track popups after undo/redo (not valid for all applications)
        this.docApp.docView.hideChangeTrackPopup();
    }

    /**
     * Callback for the listener 'specialFieldsCT:before'.
     * Finds special fields among change-tracked nodes, and restores them to original state before applying operations.
     */
    #specialFieldsCTbeforeHandler(trackingNodes) {

        let complexFieldChildren = null;
        let nodeIndex = 0;

        _.each(trackingNodes, (trackingNode, index) => {
            if (DOM.isSpecialField(trackingNode)) {
                const firstChild = $(trackingNode).children().first();
                const insertChild = DOM.isChangeTrackNode(firstChild) && !$(trackingNodes).is(firstChild); // change tracked node, that is not inside the collection
                if (insertChild) { trackingNodes.splice(index + 1, 0, getDomNode(firstChild)); }
                if ($(trackingNode).children().length > 1) { complexFieldChildren = $(trackingNode).children(); } // saving children before restauration of complex field
                this.#fieldManager.restoreSpecialField(trackingNode);
                this.#fieldManager.addToTempSpecCollection(trackingNode);
            } else if (DOM.isSpecialField(trackingNode.parentNode)) {
                const fieldNode = trackingNode.parentNode;
                if ($(fieldNode).children().length > 1) { complexFieldChildren = $(fieldNode).children(); } // saving children before restauration of complex field
                this.#fieldManager.restoreSpecialField(fieldNode);
                this.#fieldManager.addToTempSpecCollection(fieldNode);
            }
        });

        // the child spans of the complex field need to be removed from the collection of tracking nodes
        // -> except the first one that was moved outside the complex field node
        if (complexFieldChildren) {
            _.each(complexFieldChildren, (node, index) => {
                if (index > 0) { // not deleting the first child
                    nodeIndex = trackingNodes.index(node);
                    trackingNodes.splice(nodeIndex, 1);
                }
            });
        }
    }

    /**
     * Callback for the listener 'specialFieldsCT:after'.
     * Finds special fields among change-tracked nodes, and returns them to special state after applying operations.
     */
    #specialFieldsCTafterHandler() {
        _.each(this.#fieldManager.getSpecFieldsFromTempCollection(), trackingNode => {
            const instruction = DOM.getComplexFieldInstruction(trackingNode);
            const fieldType = (/NUMPAGES/i).test(instruction) ? 'NUMPAGES' : 'PAGE';
            const id = DOM.getComplexFieldId(trackingNode);
            const target = this.#fieldManager.getMarginalComplexFieldsTarget(id);

            this.#fieldManager.convertToSpecialField(id, target, fieldType);
        });
        this.#fieldManager.emptyTempSpecCollection();
    }

    /**
     * Collecting data about event that triggered inserting page breaks
     *
     * @param {HTMLElement|jQuery} currentNode
     *  DOM node where the cursor position is - we process DOM from that element downward
     *
     * @param {jQuery} [textFrameNode]
     *  An optional text frame node.
     */
    #registerPageBreaksInsert(currentNode, textFrameNode) {

        // guarantee, that node is a direct child of page content node
        if (textFrameNode && !DOM.isChildOfPageContentNode(currentNode) && !DOM.isChildOfMarginalContentNode(currentNode)) {
            currentNode = $(currentNode).parentsUntil(DOM.PAGECONTENT_NODE_SELECTOR, DOM.CONTENT_NODE_SELECTOR, DOM.MARGINALCONTENT_NODE_SELECTOR).last();
        }

        this.#currentProcessingNodeCollection.push(currentNode);
        if (textFrameNode && $(textFrameNode).length > 0) { this.#currentProcessingTextFrameNode = textFrameNode; }
    }

    /**
     * Collecting nodes for update of all other header/footer nodes of same type
     *
     * @param {jQuery} targetNode
     *  header or footer node that is beeing updated
     *
     */
    #registerTargetNodesUpdate(targetNode) {
        if (targetNode) { this.#targetNodesForUpdate = this.#targetNodesForUpdate.add(targetNode); }
    }

    /**
     * Distribute collected target header-footer nodes for updating.
     */
    #sendTargetNodesForUpdate() {
        _.each(this.#targetNodesForUpdate, targetNode => {
            this.#pageLayout.updateEditingHeaderFooter({ givenNode: targetNode, isDebounced: true });
        });
        this.#targetNodesForUpdate = $();
    }

    /**
     * Collecting nodes inside first row of table with repeating row property.
     * This unique collection is used in deffered callback to update connected cloned rows inside table.
     *
     * @param {jQuery} node
     *  header or footer node that is beeing updated
     *
     */
    #registerRepeatedNodesUpdate(node) {
        if (node) { this.#tableRepeatNodes = this.#targetNodesForUpdate.add(node); }
    }

    /**
     * Distribute collected nodes inside first row of table with repeating row property, to the cloned rows of that table
     */
    #sendRepeatedNodesForUpdate() {
        _.each(this.#tableRepeatNodes, node => {
            this.#pageLayout.updateRepeatedTableRows(node);
        });
        this.#tableRepeatNodes = $();
    }

    /**
     * Inserts a collaborative overlay element.
     */
    #insertCollaborativeOverlay() {
        this.#editdiv.append($('<div>').addClass('collaborative-overlay').attr('contenteditable', false));
    }

    /**
     * Inserts an overlay element for the drawing selection.
     */
    #insertDrawingSelectionOverlay() {
        this.#editdiv.append($('<div>').addClass('drawingselection-overlay').attr('contenteditable', false));
    }

    /**
     * Clean up after the busy mode of a local client with edit prileges. After the
     * asynchronous operations were executed, the busy mode can be left.
     */
    #leaveAsyncBusy() {
        if (!this.docApp.isInQuit()) {
            // closing the dialog
            this.docApp.docView.leaveBusy();
            this.docApp.docView.grabFocus();
            // end of gui triggered operation
            this.setGUITriggeredOperation(false);
            // allowing keyboard events again
            this.setBlockKeyboardEvent(false);
            // always leaving clip board paste mode, also this is not always required
            this.#setClipboardPasteInProgress(false);
            // restore the original selection (this is necessary for directly following key press)
            this.#selection.restoreBrowserSelection();
        }
    }

    /**
     * Asynchronous execution of operations from the local client with edit privileges.
     * This is necessary for the operations that depend on a (huge) selection. This is
     * very important for setAttributes() and deleteSelected(). At least the applying
     * of operations needs to be done asynchronously.
     *
     * @param {OperationGenerator} generator
     *  The generated operations.
     *
     * @param {String} label
     *  The label that is shown while applying the operations.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.showProgress=true]
     *      Whether the progress bar shall be handled inside this function. This is not
     *      necessary, if there is already an outer function that handles the progress
     *      bar. The latter is for example the case for deleting the selection before
     *      pasting the content of the external clipboard.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after all actions have been applied
     *  successfully, or rejected immediately after applying an action has
     *  failed. See `EditModel.applyOperationsAsync()` for more information.
     */
    #applyTextOperationsAsync(generator, label, options) {

        // the promise for the asynchronous execution of operations
        let operationsPromise = null;
        // whehter the visibility of the progress bar shall be handled by this function
        // -> this is typically not necessary during pasting, because there is already a visible progress bar
        const showProgress = getBooleanOption(options, 'showProgress', true);
        // closing progress bar at the end, if not a user abort occurred
        const leaveOnSuccess = getBooleanOption(options, 'leaveOnSuccess', false);
        // an optional start value for the progress bar
        const progressStart = getNumberOption(options, 'progressStart', 0);

        // setting a default label, if not specified from caller
        if (!label) { label = gt('Sorry, applying changes will take some time.'); }

        // blocking keyboard input during applying of operations
        this.setBlockKeyboardEvent(true);

        // Wait for a second before showing the progress bar (entering busy mode).
        // Users applying minimal or 'normal' amount of change tracks will not see this progress bar at all.
        if (showProgress) {
            this.docApp.docView.enterBusy({
                cancelHandler: () => { if (operationsPromise) { operationsPromise.abort(); } },
                delay: 1000,
                warningLabel: label
            });
            this.docApp.docView.grabFocus();
        }

        // fire apply actions asynchronously
        operationsPromise = this.applyOperationsAsync(generator);

        // handle the result of change track operations
        operationsPromise.progress(progress => {
            // update the progress bar according to progress of the operations promise
            this.docApp.getView().updateBusyProgress(progressStart + progress * (1 - progressStart));
        });

        // handler for 'always', that is not triggered, if this is in destruction (document is closed, 42567)
        this.onSettled(operationsPromise, () => { if (showProgress || leaveOnSuccess) { this.#leaveAsyncBusy(); } });

        return operationsPromise;
    }

    /**
     * If the width or the height of the page has changed, it might be possible, that the page orientation has
     * changed, too. This orientation change must be part of the operation (DOCS-3180).
     *
     * @param {Object} newPageAttrs
     *  The page attributes changed by the user. This object might be modified within this function.
     *
     * @param {Object} pageAttrs
     *  The full set of new page attributes.
     */
    #addPageOrientationIfRequired(newPageAttrs, pageAttrs) {
        if (!_.isUndefined(newPageAttrs.width) || !_.isUndefined(newPageAttrs.height)) {
            if (pageAttrs.width > pageAttrs.height && pageAttrs.orientation === 'portrait') {
                newPageAttrs.orientation = 'landscape';
            } else if (pageAttrs.height > pageAttrs.width && pageAttrs.orientation === 'landscape') {
                newPageAttrs.orientation = 'portrait';
            }
        }
    }

    /**
     * Searching all drawings with classes "relativesize" and "absolute" in the page content
     * node, in the text drawing layer node and in the template folder for the header/footer.
     * Not every drawing in the template folder is visible in the document, so that the operations
     * must be applied to the drawings in the template folder.
     *
     * @returns {jQuery}
     *  A jQuery collection with all drawings with relative size in the document. If there is no
     *  such drawing, an empty jQuery collection is returned.
     */
    #collectRelativeSizeDrawingNodes() {
        const contentChildren = DOM.getPageContentNode(this.getNode()).add(DOM.getMarginalTemplateNode(this.getNode())).add(DOM.getTextDrawingLayerNode(this.getNode()));
        return contentChildren.find(DOM.ABSOLUTE_RELATIVESIZE_DRAWING_SELECTOR);
    }

    /**
     * Helper function to merge attributes from a new set of (character) attributes
     * into an old set. But the new attributes are only added, if they do NOT exist
     * in the object with the old attributes.
     *
     * @param {Object|Null} oldAttrs
     *  An object with the (character) attributes.
     *
     * @param {Object} newAttrs
     *  An object with new (character) attributes. This are only used, if the attributes
     *  are not set in the object with the old attributes.
     *
     * @returns {Object}
     *  An object that contains the old attributes and optionally the new attributes.
     */
    #mergeAttrsIfMissing(oldAttrs, newAttrs) {
        if (!oldAttrs) { return newAttrs; }
        return { ...newAttrs, ...oldAttrs };
    }

}
