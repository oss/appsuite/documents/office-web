/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { IOS_SAFARI_DEVICE, hasKeyCode, matchKeyCode, isEscapeKey, isIMEInputKey, isCopyKeyEvent, isPasteKeyEvent, isClipboardKeyEvent, isSelectAllKeyEvent } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { isTextFrameShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { checkForHyperlinkText, insertHyperlink } from '@/io.ox/office/textframework/components/hyperlink/hyperlink';
import { changeCellSelectionHorz, changeCellSelectionHorzIE, changeCellSelectionVert,
    mergeableTables, resolveCellSelection, updownCursorTravel, updownExceededSizeTable } from '@/io.ox/office/textframework/components/table/table';
import { LIST_LABEL_NODE_SELECTOR, MANUAL_PAGE_BREAK_CLASS_NAME, MANUAL_PAGE_BREAK_SELECTOR, PARAGRAPH_NODE_SELECTOR, RANGEMARKER_STARTTYPE_SELECTOR,
    getAllowedNeighboringNode, getRangeMarkerId, getRangeMarkerType, isArrowCursorKey, isChangeTrackNode, isCursorKey, isFinalParagraphBehindTableInCell,
    isFirstContentNodeInTextframe, isIgnorableKey, isImplicitParagraphNode, isInlineDrawingNode, isInsideComplexFieldRange, isManualPageBreakNode,
    isMergeableParagraph, isParagraphNode, isSlideNode, isTableNode } from '@/io.ox/office/textframework/utils/dom';
import { DELETE, PARA_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { detectListSymbol } from '@/io.ox/office/textframework/utils/listutils';
import { findPreviousNode, isHangulText } from '@/io.ox/office/textframework/utils/textutils';
import { getPrintableChar, isBrowserShortcutKeyEvent, isF6AcessibilityKeyEvent, isSearchKeyEvent } from '@/io.ox/office/textframework/utils/keycodeutils';
import { getElementStyleId, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';

function mixinKeyHandling(BaseClass) {

    // mix-in class KeyHandlingMixin ==========================================

    /**
     * A mix-in class for the keyboard handling in the document model.
     */

    class KeyHandlingMixin extends BaseClass {

        // the period of time, that a text input is deferred, waiting for a further text input (in ms).
        #inputTextTimeout = 2;
        // the maximum number of characters that are buffered before an insertText operation is created.
        #maxTextInputChars = 3;
        // the content, that will be printed in the current active text input process
        #activeInputText = null;
        // a deferred that is resolved, after the text is inserted. This is required for the undo group and must be global.
        #insertTextDef = null;

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Getting the handler function that is used for processing keydown
         * events on the page node.
         *
         * @returns {Function}
         *  The handler function for the 'keydown' event.
         */
        getPageProcessKeyDownHandler() {
            return this.#pageProcessKeyDownHandler;
        }

        /**
         * Getting the handler function that is used for processing keydown
         * events on the clipboard node.
         *
         * @returns {Function}
         *  The handler function for the 'keydown' event at clipboard node.
         */
        getPageProcessKeyDownHandlerClipboardNode() {
            return event => {
                if (!this.#docApp.getView().hasSlidepaneVirtualFocus()) {
                    this.#pageProcessKeyDownHandler(event);
                }
            };
        }

        /**
         * Getting the handler function that is used for processing keypressed
         * events on the page node.
         *
         * @returns {Function}
         *  The handler function for the 'keypressed' event.
         */
        getPageProcessKeyPressHandler() {
            return this.#pageProcessKeyPressHandler;
        }

        /**
         * Getting the handler function that is used for processing keypressed
         * events on the clipboard node.
         *
         * @returns {Function}
         *  The handler function for the 'keydown' event at clipboard node.
         */
        getPageProcessKeyPressHandlerClipboardNode() {
            return event => {
                if (!this.#docApp.getView().hasSlidepaneVirtualFocus()) {
                    this.#pageProcessKeyPressHandler(event);
                }
            };
        }

        /**
         * Setting the content of 'this.#activeInputText', that is the
         * collector for the inserted characters.
         *
         * @param {String} value
         *  The new value of the character collector
         */
        setActiveInputText(value) {
            this.#activeInputText = value;
        }

        /**
         * Getting the content of 'this.#activeInputText', that is the
         * collector for the inserted characters.
         *
         * @returns {String}
         */
        getActiveInputText() {
            return this.#activeInputText;
        }

        // private methods ----------------------------------------------------

        /**
         * Helper function for synchronous callbacks to switch between synchronous and
         * asynchronous handling of key input.
         * In OT process, the operation generation has to be synchronous to avoid, that
         * external operations are disturbing the operation generation. Otherwise the
         * key inputs are collected and the operation is generated with a delay specified
         * in 'this.#inputTextTimeout'.
         *
         * This function uses the parameters as they are specified in '#executeDelayed'.
         *
         * @returns {jQuery.Promise|Null}
         *  A promise that will be resolved after the callback function has been
         *  executed. If the callback is executed synchronously, null is returned.
         */
        #doSyncOrAsyncCall(callback, delay) {
            if (this.#docApp.isOTEnabled()) {
                callback.call(this);
                return null;
            } else {
                return this.executeDelayed(callback, delay);
            }
        }

        /**
         * modify current listlevel - if any - depending on the increase flag
         */
        #implModifyListLevel(currentlistLevel, increase, minLevel) {

            this.getUndoManager().enterUndoGroup(() => {

                if (this.useSlideMode()) {
                    this.changeListIndent({ increase });
                } else {

                    const levelKey = 'listLevel';
                    if (increase && currentlistLevel < 8) {
                        currentlistLevel += 1;
                        this.setAttribute('paragraph', levelKey, currentlistLevel);
                    } else if (!increase && currentlistLevel > minLevel) {
                        currentlistLevel -= 1;
                        this.setAttribute('paragraph', levelKey, currentlistLevel);
                        if (currentlistLevel < 0 && !this.useSlideMode()) {
                            this.setAttribute('paragraph', 'listStyleId', null);
                        }
                    }
                }
            });
        }

        /**
         * Removes pageBreakBefore paragraph attribute from passed table element. It is called when
         * cursor is in first position inside first table paragraph and BACKSPACE is pressed, or
         * when DELETE is pressed on cursor placed in paragraph's last position right above table
         *
         * @param {jQuery|DOM} table - table element that has paragraph with pageBreakBefore attribute,
         *  and has to be removed.
         *
         */
        #removePageBreakBeforeAttribute(table) {

            let paragraphPos;
            let operation;
            // the page node
            const editdiv = this.getNode();

            const paragraphWithManualPageBreak = $(table).find(MANUAL_PAGE_BREAK_SELECTOR);

            if (paragraphWithManualPageBreak.length > 0) {
                paragraphPos = Position.getOxoPosition(editdiv, paragraphWithManualPageBreak);

                operation = { name: SET_ATTRIBUTES, attrs: { paragraph: { pageBreakBefore: false } }, start: _.copy(paragraphPos) };
                this.applyOperations(operation);
            } else {
                if (this.#docApp.isODF()) {
                    paragraphPos = Position.getFirstPositionInParagraph(editdiv, Position.getOxoPosition(editdiv, table));
                    operation = { name: SET_ATTRIBUTES, attrs: { paragraph: { pageBreakBefore: null } }, start: _.copy(paragraphPos) };
                    this.applyOperations(operation);
                } else {
                    // #35358 - if we delete paragraph, we cannot remove attr from it, so just clean up table marker for displaying
                    $(table).removeClass(MANUAL_PAGE_BREAK_CLASS_NAME);
                }
            }
        }

        /**
         * Helper function to repair problems with "f6-target" problems, when the focus in OX Presentation in the
         * clipboard node or in OX Text in the page. In OX Text the commentlayer is a child of the page, so that
         * "f6-target" does not work correctly and the focus must be set explicitely. In OX Presentation there are
         * problems with the slide selection, when the focus is inside the clipboard node.
         *
         * @returns {boolean}
         *  Whether the focus was handled within this function.
         */
        #handleF6TargetFocusProblems() {
            let focusHandled = false;
            if (this.#docApp.docView.commentsPane?.isVisible()) {
                this.setTimeout(() => { this.#docApp.docView.commentsPane.setFocusIntoCommentsPane(); }, 0);
                focusHandled = true;
            } else if (this.#docApp.isPresentationApp()) {
                if (this.#docApp.docView.collaboratorMenu.isVisible()) {
                    this.setTimeout(() => { this.#docApp.docView.collaboratorMenu.grabFocus(); }, 0);
                    focusHandled = true;
                }
                // handling for the operations pane might be added here
            }
            return focusHandled;
        }

        /**
         * "Backspace" and "Delete" should not delete an unselected drawing immediately. Instead it should
         * select a drawing, if required. This is more user friendly, because the cursor is not always visible
         * in OX Text (DOCS-4188).
         *
         * @param {Number[]} startPosition
         *  The logical start position.
         *
         * @param {Number[]} endPosition
         *  The logical end position.
         */
        #onlyDrawingSelected(startPosition, endPosition) {

            let selectedDrawing = false;

            if (this.#docApp.isTextApp() && _.isEqual(startPosition, endPosition)) {
                const deleteNode = Position.getDOMPosition(this.getCurrentRootNode(), startPosition, true);
                if (deleteNode && deleteNode.node && isInlineDrawingNode(deleteNode.node) && !this.getSelection().isDrawingSelection()) {
                    this.getSelection().setTextSelection(startPosition, Position.increaseLastIndex(startPosition));
                    selectedDrawing = true;
                }
            }

            return selectedDrawing;
        }

        /**
         * Handler for the keyDown event.
         *
         * @param {jQuery.Event} event
         *  A jQuery keyboard event object.
         */
        #pageProcessKeyDownHandler(event) {

            const readOnly = !this.#docApp.isEditable();
            let isCellSelection = false;
            let currentBrowserSelection = null;
            let checkPosition = null;
            let returnObj = null;
            let startPosition;
            let endPosition;
            let paraLen;
            const activeRootNode = this.getCurrentRootNode();
            // the selection object
            const selection = this.getSelection();
            // the page layout object
            const pageLayout = this.getPageLayout();
            // the range marker object
            const rangeMarker = this.getRangeMarker();
            // keys combination used to jump into header/footer on diff OS
            let keyCombo = null;
            // the ruler pane of the document
            let rulerPane = null;
            // is keycode is left or right arrow
            const leftRightArrow = hasKeyCode(event, 'LEFT_ARROW', 'RIGHT_ARROW');
            // control varible used for bugfix #58296 in Crome for rulerpane
            let rulerPaneTempHidden = false;

            if (this.#docApp.isTextApp() && $(event.target).is('.ignorepageevent')) { return; } // the target has a marker, that page event handler can ignore it (inside the comment pane)

            if (this.isFormatPainterActive()) {
                this.setFormatPainterState(false);
            }

            // the presentation mode is active -> accepting several cursor keys (cursor key, escape, ctrl-shift-f)
            if (this.#docApp.isPresentationApp() && this.isPresentationMode()) {
                this.getPresentationModeManager().handleKeydownInPresentationMode(event);
                if (isBrowserShortcutKeyEvent(event)) { return; }
                event.preventDefault();
                return;
            }

            if (selection.isCropMode()) {
                // TODO check keys ESC, block typing...
                if (isEscapeKey(event)) {
                    this.exitCropMode();
                    return;
                }
                if (!isCursorKey(event.keyCode)) {
                    event.preventDefault();
                    return;
                }
            }

            // F6 handling with open comment layer in OX Text and OX Presentation (does not work correctly using class f6-target)
            if (isF6AcessibilityKeyEvent(event) && (this.#docApp.isTextApp() || this.#docApp.isPresentationApp()) && this.#handleF6TargetFocusProblems()) { return; }

            // disable text input into slide, if slide mode is active (no preventDefault -> this breaks pasting)
            if (this.useSlideMode() && selection.isTopLevelTextCursor() && !isEscapeKey(event) && !isF6AcessibilityKeyEvent(event) && !matchKeyCode(event, 'TAB', { shift: null })) {
                if (isIgnorableKey(event.keyCode) || isBrowserShortcutKeyEvent(event)) { return; } // 50901
                if (isCursorKey(event.keyCode) && this.setActiveSlideByCursorEvent) { this.setActiveSlideByCursorEvent(event); }
                if (!isPasteKeyEvent(event)) { event.preventDefault(); } // still enabling pasting into slide root
                return;
            }

            // disabling all kind of text modifications in master view in place holder drawings of ODF documents
            if (this.isODFReadOnyPlaceHolderDrawingProcessing && !isCursorKey(event.keyCode) && this.isODFReadOnyPlaceHolderDrawingProcessing()) {
                event.preventDefault();
                return;
            }

            // prevent browser's navigation history alt+arrow shortcut, if it is used later as drawing rotation
            if (this.useSlideMode() && (selection.isDrawingSelection() || selection.isMultiSelection()) && (event.altKey && leftRightArrow)) {
                event.preventDefault();
            }

            // saving the keyDown event for later usage in processKeyPressed
            this.setLastKeyDownEvent(event);

            // registering MacOS marker for multiple directly following keydown events (46659)
            if (_.browser.MacOS && !isCursorKey(event.keyCode)) {
                if (_.isNumber(this.getLastKeyDownKeyCode()) && this.getLastKeyDownKeyCode() === event.keyCode) {
                    this.setRepeatedKeyDown(true);
                }
                this.setLastKeyDownKeyCode(event.keyCode);
                this.setLastKeyDownModifiers(event.ctrlKey, event.shiftKey, event.altKey, event.metaKey);
            }

            // open the change track popup, when available, with 'ALT' + 'DOWN_ARROW' combination
            if (matchKeyCode(event, 'DOWN_ARROW', { alt: true })) {
                this.#docApp.docView.showChangeTrackPopup();
                return;
            }

            // close the change track popup, when available, with 'ALT' + 'UP_ARROW' combination
            if (matchKeyCode(event, 'UP_ARROW', { alt: true })) {
                this.#docApp.docView.hideChangeTrackPopup();
                return;
            }

            if (this.getRoIOSParagraph()) {
                if (this.#docApp.isEditable()) {
                    $(this.getRoIOSParagraph()).removeAttr('contenteditable');
                }
                this.setRoIOSParagraph(null);
            }

            if (this.getBlockKeyboardEvent()) {
                event.preventDefault();
                return false;
            }

            if (_.browser.Safari && this.isIMEBlockActive()) { // calling 'postProcessCompositionEnd' synchronously (DOCS-2146)
                this.postProcessCompositionEnd({ safariIMESync: true });
            }

            // special handling for IME input on IE
            // having a browser selection that spans up ak range,
            // the IME input session start event (compositionstart) is omitted.
            // Normally we have to call deleteSelected() to remove the selection within
            // compositionstart. Unfortunately IE is NOT able to work correctly if the
            // browser selection is changed during a IME session. Therefore this code
            // cancels the IME session whenever we detect a selected range to preserve
            // a consistent document state.
            // TODO: We have to find a better solution to support IME and selections.
            if (_.browser.IE && isIMEInputKey(event) && selection.hasRange()) {
                event.preventDefault();
                return false;
            }

            if (isIgnorableKey(event.keyCode) || isBrowserShortcutKeyEvent(event)) {
                return;
            }

            this.dumpEventObject(event);

            // 'escape' converts a cell selection into a text selection
            if (isEscapeKey(event) && (selection.getSelectionType() === 'cell')) {
                resolveCellSelection(selection, activeRootNode);
                event.preventDefault();
                return false;
            }

            // resetting values for table cell selection, if no 'shift' is pressed
            if ((selection.getAnchorCellRange() !== null) && !event.shiftKey) {
                selection.setAnchorCellRange(null);
            }

            if (isCursorKey(event.keyCode)) {

                if (this.#docApp.isTextApp()) {
                    rulerPane = this.#docApp.docView.rulerPane;
                    if (_.browser.Chrome && rulerPane?.isVisible() && hasKeyCode(event, 'PAGE_UP', 'PAGE_DOWN')) { //#58296
                        rulerPane.$el.addClass('temp-hide-ruler');
                        rulerPaneTempHidden = true;
                    }
                }

                // deferring all cursor position calculations, because insertText is also deferred (39358)

                this.#doSyncOrAsyncCall(() => {

                    const isTextFrameSelectionInSlideMode = selection.isAdditionalTextframeSelection() && this.useSlideMode();

                    isCellSelection = (selection.getSelectionType() === 'cell');

                    if (isCellSelection) {
                        currentBrowserSelection = selection.getBrowserSelection();
                    }

                    // Setting cursor up and down in tables on MS IE or Webkit
                    if (hasKeyCode(event, 'UP_ARROW', 'DOWN_ARROW') && !event.shiftKey && !event.ctrlKey) {
                        // Setting cursor up and down in tables on MS IE or Webkit
                        if ((_.browser.IE || _.browser.WebKit) && this.isPositionInTable()) {
                            if (updownCursorTravel(event, selection, activeRootNode, { up: hasKeyCode(event, 'UP_ARROW'), down: hasKeyCode(event, 'DOWN_ARROW'), readonly: readOnly })) {
                                return;
                            }
                        }
                        // Setting cursor up and down before and behind exceeded-size tables in Firefox (27395)
                        if (_.browser.Firefox) {
                            if (updownExceededSizeTable(event, selection, activeRootNode, { up: hasKeyCode(event, 'UP_ARROW'), down: hasKeyCode(event, 'DOWN_ARROW') })) {
                                return;
                            }
                        }
                    }

                    // Changing table cell selections in FireFox when shift is used with arrow keys.
                    // Switching from text selection to cell selection already happened in 'selection.moveTextCursor()'.
                    if (_.browser.Firefox && isArrowCursorKey(event.keyCode) && event.shiftKey && isCellSelection && this.isPositionInTable()) {

                        event.preventDefault();

                        if (hasKeyCode(event, 'UP_ARROW', 'DOWN_ARROW')) {
                            changeCellSelectionVert(this.#docApp, selection, currentBrowserSelection, { backwards: hasKeyCode(event, 'UP_ARROW') });
                        }

                        if (hasKeyCode(event, 'LEFT_ARROW', 'RIGHT_ARROW')) {
                            changeCellSelectionHorz(this.#docApp, selection, currentBrowserSelection, { backwards: hasKeyCode(event, 'LEFT_ARROW') });
                        }

                        return;
                    }

                    if (_.browser.IE && isArrowCursorKey(event.keyCode) && event.shiftKey &&
                        (this.isPositionInTable() || Position.isPositionInTable(activeRootNode, selection.getEndPosition()) || Position.isPositionInTable(activeRootNode, selection.getStartPosition()))) {

                        if (hasKeyCode(event, 'LEFT_ARROW', 'RIGHT_ARROW')) {
                            if (changeCellSelectionHorzIE(this.#docApp, activeRootNode, selection, currentBrowserSelection, { backwards: hasKeyCode(event, 'LEFT_ARROW') })) {
                                event.preventDefault();
                                return;
                            }
                        }

                    }
                    // jumping to header/footer on Mac and other OS
                    keyCombo = _.browser.MacOS ? { ctrl: true, meta: true } : { ctrl: true, alt: true };

                    if (matchKeyCode(event, 'PAGE_UP', keyCombo)) { // Go to header (if exists, if not, create it first) on the page where cursor is
                        event.preventDefault();
                        if (!this.isHeaderFooterEditState() && !readOnly) {
                            pageLayout.jumpToHeaderOnCurrentPage();
                            return;
                        }
                    } else if (matchKeyCode(event, 'PAGE_DOWN', keyCombo)) { // Go to footer (if exists, if not, create it first) on the page where cursor is
                        event.preventDefault();
                        if (!this.isHeaderFooterEditState() && !readOnly) {
                            pageLayout.jumpToFooterOnCurrentPage();
                            return;
                        }
                    } else if (matchKeyCode(event, 'HOME', { ctrlOrMeta: true }) && isTextFrameSelectionInSlideMode) {
                        event.preventDefault();
                        selection.setTextSelection(Position.getFirstTextPositionInTextFrame(activeRootNode, selection.getAnyTextFrameDrawing()));
                        return;
                    } else if (matchKeyCode(event, 'HOME', { ctrlOrMeta: true, shift: true }) && isTextFrameSelectionInSlideMode) {
                        event.preventDefault();
                        selection.setTextSelection(Position.getFirstTextPositionInTextFrame(activeRootNode, selection.getAnyTextFrameDrawing()), selection.getEndPosition());
                        return;
                    } else if (matchKeyCode(event, 'END', { ctrlOrMeta: true }) && isTextFrameSelectionInSlideMode) {
                        event.preventDefault();
                        selection.setTextSelection(Position.getLastTextPositionInTextFrame(activeRootNode, selection.getAnyTextFrameDrawing()));
                        return;
                    } else if (matchKeyCode(event, 'END', { ctrlOrMeta: true, shift: true }) && isTextFrameSelectionInSlideMode) {
                        event.preventDefault();
                        selection.setTextSelection(selection.getStartPosition(), Position.getLastTextPositionInTextFrame(activeRootNode, selection.getAnyTextFrameDrawing()));
                        return;
                    }

                    // handling vertical cursor key events inside text frames is done in selection class
                    // in checkVerticalPositions, it's corrected after browser tries to set selection

                    // any navigation key: change drawing selection to text selection before or behind the drawing (OX Text)
                    if (!this.useSlideMode() && selection.isDrawingSelection()) { selection.selectDrawingAsText(); }

                    // moving selected drawing(s) in slide mode
                    if (this.useSlideMode() && (selection.isDrawingSelection() || selection.isMultiSelection()) && this.handleDrawingOperations(event)) { return; }

                    // changing the slide always with page up and page down, independent from the current selection
                    if (this.handleSlideChangeByPageUpDown && this.handleSlideChangeByPageUpDown(event)) { return; }

                    // let browser process the key event (this may create a drawing selection)
                    selection.processBrowserEvent(event, { readonly: readOnly }); // processing is dependent from read-only mode

                    // #58296
                    if (rulerPaneTempHidden) {
                        this.#docApp.docView.rulerPane.$el.removeClass('temp-hide-ruler');
                        rulerPaneTempHidden = false;
                    }
                }, this.#inputTextTimeout);

                return;
            }

            // invoke copy-method in IE manually when we have a TextCursor within a TextFrame (shape) and pressing "copy", cause it won't be done automatically
            if (_.browser.IE && this.useSlideMode() && isCopyKeyEvent(event) && selection.isTextCursor() && selection.isAdditionalTextframeSelection()) {
                this.copy(event);
            }

            // handle just cursor, copy, search, selectall and the global F6 accessibility events if in read only mode
            if (readOnly && !isCopyKeyEvent(event) && !isF6AcessibilityKeyEvent(event) && !matchKeyCode(event, 'TAB', { shift: null }) && !isEscapeKey(event) && !isSearchKeyEvent(event) && !isSelectAllKeyEvent(event)) {
                if (!this.isImportFinished()) {
                    this.#docApp.rejectEditAttempt('loadingInProgress');
                } else {
                    this.#docApp.rejectEditAttempt();
                }
                event.preventDefault();
                return;
            }

            // avoiding OT Collisions, if there are currently external operations applied (TODO: Informing the user with a yell?)
            if (this.#docApp.isOTEnabled() && (this.isProcessingExternalOperations() || this.isProcessingActions())) {
                this.trigger('ot:collision', { message: 'Suppressing keydown event!' });
                event.preventDefault();
                return;
            }

            if (hasKeyCode(event, 'DELETE') && !event.shiftKey) {

                event.preventDefault();

                // if there is an active 'inputTextPromise', it has to be set to null now, so that
                // following text input is included into a new deferred
                this.setInputTextPromise(null);

                // Executing the code for 'DELETE' deferred. This is necessary because of deferred input of text. Defer time is 'this.#inputTextTimeout'
                this.#doSyncOrAsyncCall(() => {

                    let helperNode;
                    // the change track object
                    const changeTrack = this.getChangeTrack();
                    // a local helper for lastOperationEnd
                    let lastOperationEndLocal = null;
                    // whether the following character is a unicode surrogate pair
                    let isSurrogatePair = false;

                    startPosition = selection.getStartPosition();

                    if (event.ctrlKey) {

                        const storedEndPos = Position.getRangeForDeleteWithCtrl(this.getCurrentRootNode(), startPosition, false);
                        selection.setTextSelection(startPosition, storedEndPos, { simpleTextSelection: true });

                    } else if (!selection.hasRange()) {

                        // DOCS-2909: Deleting a range, if this is a unicode position
                        isSurrogatePair = selection.selectUnicodePairIfPossible({ backward: false });
                        if (isSurrogatePair) { startPosition = selection.getStartPosition(); }
                    }

                    if (selection.hasRange()) {
                        if (this.#docApp.isODF()) { this.setKeepPreselectedAttributes(true); } // 52649
                        this.deleteSelected({ deleteKey: true })
                            .done(() => {
                                startPosition = selection.getStartPosition(); // refresh required after deleteSelected()
                                selection.setTextSelection((this.getLastOperationEnd() && (startPosition.length === this.getLastOperationEnd().length)) ? startPosition : this.getLastOperationEnd(), null, { simpleTextSelection: true });
                                if (this.#docApp.isODF()) { this.setKeepPreselectedAttributes(false); } // 52649
                            });
                    } else {
                        endPosition = selection.getEndPosition();

                        returnObj = Position.skipDrawingsAndTables(_.clone(startPosition), _.clone(endPosition), activeRootNode, { backwards: false });
                        paraLen = returnObj.paraLen;
                        startPosition = returnObj.start;
                        endPosition = returnObj.end;

                        if (returnObj && returnObj.selectComplexField) {
                            helperNode = rangeMarker.getEndMarker(getRangeMarkerId(returnObj.node));
                            if (helperNode) {
                                startPosition = selection.getStartPosition();
                                endPosition = Position.getOxoPosition(activeRootNode, helperNode);
                                selection.setTextSelection(startPosition, endPosition);
                            } else {
                                globalLogger.error('editor.processKeyDown(): handling DELETE before complex field failed to fetch valid node!');
                            }
                            return;
                        }

                        // handling for grouped drawings, only ODF (58986, 58987) ->  not generating invalid operation
                        if (this.#docApp.isODF() && this.#docApp.isPresentationApp() && Position.isGroupedDrawingPosition(this.getCurrentRootNode(), startPosition)) { return; }

                        if (startPosition[startPosition.length - 1] < paraLen) {
                            if (!this.#onlyDrawingSelected(startPosition, endPosition)) {
                                this.deleteRange(startPosition, endPosition, { setTextSelection: false, handleUnrestorableContent: true, deleteKey: true })
                                    .always(() => {
                                        // If change tracking is active, lastOperationEnd is set inside deleteRange
                                        if (changeTrack.isActiveChangeTracking() && _.isArray(this.getLastOperationEnd()) && !_.isEqual(this.getLastOperationEnd(), startPosition)) {
                                            selection.setTextSelection(this.getLastOperationEnd(), null, { simpleTextSelection: true });
                                        } else {
                                            selection.setTextSelection(startPosition, null, { simpleTextSelection: true });
                                        }

                                        // list update in presentation app necessary, if the last character in the paragraph was removed
                                        if (this.useSlideMode() && paraLen === 1 && _.last(startPosition) === 0 && _.isEqual(startPosition, endPosition)) {
                                            this.handleListItemVisibility(); // last character removed without changing selection
                                        }
                                    });
                            }
                        } else {
                            const mergeselection = _.clone(startPosition);
                            const characterPos = mergeselection.pop();
                            const nextParagraphPosition = _.copy(mergeselection);

                            nextParagraphPosition[nextParagraphPosition.length - 1] += 1;

                            const domPos = Position.getDOMPosition(activeRootNode, nextParagraphPosition);
                            let nextIsTable = false;
                            let isLastParagraph = false;

                            if (domPos) {
                                if (isTableNode(domPos.node)) {
                                    nextIsTable = true;
                                }
                            } else {
                                nextParagraphPosition[nextParagraphPosition.length - 1] -= 1;
                                isLastParagraph = true;
                            }

                            if (isMergeableParagraph(Position.getParagraphElement(activeRootNode, mergeselection))) {
                                if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(domPos.node)) {  // not really deleting paragraph, but marking for deletion
                                    this.applyOperations({
                                        name: SET_ATTRIBUTES,
                                        start: _.copy(nextParagraphPosition),  // the latter paragraph needs to be marked for removal
                                        attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } }
                                    });
                                    // Setting the cursor to the correct position, if no character was deleted
                                    lastOperationEndLocal = _.clone(mergeselection);
                                    lastOperationEndLocal[lastOperationEndLocal.length - 1] += 1;
                                    lastOperationEndLocal = Position.appendNewIndex(lastOperationEndLocal);
                                    this.setLastOperationEnd(lastOperationEndLocal);

                                } else {
                                    if (paraLen === 0 && !isManualPageBreakNode(domPos.node)) {  // Simply remove an empty paragraph
                                        this.deleteRange(_.initial(_.clone(startPosition)), null, { deleteKey: true });
                                        // updating lists, if required. This is the case, if the deleted paragraph is part of a list
                                        this.handleTriggeringListUpdate(domPos.node.previousSibling);
                                    } else { // Merging two paragraphs
                                        this.mergeParagraph(mergeselection, paraLen);
                                    }
                                }
                            }

                            if (nextIsTable) {
                                if (characterPos === 0) {
                                    // removing empty paragraph
                                    const localPos = _.clone(startPosition);
                                    localPos.pop();
                                    if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(domPos.node)) {  // not really deleting paragraph, but marking for deletion
                                        this.applyOperations({
                                            name: SET_ATTRIBUTES,
                                            start: _.copy(localPos),  // the latter paragraph needs to be marked for removal
                                            attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } }
                                        });
                                        // Setting the cursor to the correct position, if no character was deleted
                                        lastOperationEndLocal = _.clone(localPos);
                                        lastOperationEndLocal[lastOperationEndLocal.length - 1] += 1;
                                        lastOperationEndLocal = Position.getFirstPositionInParagraph(activeRootNode, lastOperationEndLocal);
                                        this.setLastOperationEnd(lastOperationEndLocal);

                                    } else {
                                        this.getUndoManager().enterUndoGroup(() => {

                                            // the current paragraph
                                            const currentParagraph = Position.getContentNodeElement(activeRootNode, startPosition.slice(0, -1));
                                            // the previous allowed neighbour of the paragraph
                                            const prevNeighbour = getAllowedNeighboringNode(currentParagraph, { next: false });
                                            // the next allowed neighbour of the paragraph
                                            const nextNeighbour = getAllowedNeighboringNode(currentParagraph);
                                            // the logical position of the previous neighbour node
                                            let prevNeighbourPos = null;

                                            // remove paragraph explicitely
                                            if (!isImplicitParagraphNode(currentParagraph)) { // first handle if implicit paragraph at that position
                                                this.deleteRange(localPos, null, { deleteKey: true });
                                                nextParagraphPosition[nextParagraphPosition.length - 1] -= 1;
                                            } else {
                                                $(currentParagraph).remove();
                                            }

                                            if (nextNeighbour && isTableNode(nextNeighbour)) {

                                                // check if table has paragraph with manual page break set, and remove it
                                                if (isManualPageBreakNode(nextNeighbour)) {
                                                    this.#removePageBreakBeforeAttribute(nextNeighbour);
                                                }

                                                // if both neighbours are tables, and it's possible, merge them
                                                if (prevNeighbour && isTableNode(prevNeighbour) && mergeableTables(prevNeighbour, nextNeighbour) && !this.#docApp.isODF()) {
                                                    prevNeighbourPos = Position.getOxoPosition(activeRootNode, prevNeighbour);
                                                    this.mergeTable(prevNeighbourPos, { next: true });
                                                }
                                            }

                                            return $.when();
                                        }, this); // end of enterUndoGroup()
                                    }
                                }
                                startPosition = Position.getFirstPositionInParagraph(activeRootNode, nextParagraphPosition);
                            } else if (isLastParagraph) {

                                if (Position.isPositionInTable(activeRootNode, nextParagraphPosition)) {

                                    returnObj = Position.getFirstPositionInNextCell(activeRootNode, nextParagraphPosition);
                                    startPosition = returnObj.position;
                                    const endOfTable = returnObj.endOfTable;
                                    if (endOfTable && startPosition) {
                                        startPosition[startPosition.length - 1] += 1;
                                        startPosition = Position.getFirstPositionInParagraph(activeRootNode, startPosition);
                                    }
                                }
                            }

                            if (this.getChangeTrack().isActiveChangeTracking() && _.isArray(this.getLastOperationEnd()) && !_.isEqual(this.getLastOperationEnd(), startPosition)) {
                                selection.setTextSelection(this.getLastOperationEnd(), this.getLastOperationEnd(), { simpleTextSelection: true });
                            } else if (startPosition) {
                                selection.setTextSelection(startPosition, null, { simpleTextSelection: true });
                            }
                        }
                    }

                }, this.#inputTextTimeout);

            } else if (hasKeyCode(event, 'BACKSPACE') && !event.altKey) {

                if (IOS_SAFARI_DEVICE && this.isImeActive()) {
                    //Bug 42457
                    //IPAD directly hangs up an reloads the page :(
                    this.setInputTextPromise(null);
                    return;
                }

                const imeSyncActive = this.isImeActive();
                event.preventDefault();

                // if there is an active 'inputTextPromise', it has to be set to null now, so that
                // following text input is included into a new deferred
                this.setInputTextPromise(null);

                // Executing the code for 'BACKSPACE' deferred. This is necessary because of deferred input of text. Defer time is 'this.#inputTextTimeout'
                const handleBack = () => {

                    let helperNode;
                    let isSurrogatePair = false;

                    if (this.isImeActive() && !imeSyncActive) {
                        // #51390: Never process BACKSPACE key asynchronously if we are within a composition
                        return;
                    }

                    startPosition = selection.getStartPosition();

                    if (event.ctrlKey) {
                        const storedEndPos = Position.getRangeForDeleteWithCtrl(this.getCurrentRootNode(), startPosition, true);
                        selection.setTextSelection(storedEndPos, startPosition, { simpleTextSelection: true });
                        startPosition = storedEndPos;

                    } else if (startPosition[startPosition.length - 1] > 1 && !selection.hasRange()) {

                        // DOCS-2909: Deleting a range, if this is a unicode position
                        isSurrogatePair = selection.selectUnicodePairIfPossible({ backward: true });
                        if (isSurrogatePair) { startPosition = selection.getStartPosition(); }
                    }

                    if (selection.hasRange()) {
                        if (this.#docApp.isODF()) { this.setKeepPreselectedAttributes(true); } // 52649
                        this.deleteSelected({ deleteKey: true })
                            .done(() => {
                                startPosition = selection.getStartPosition(); // refresh required after deleteSelected()
                                selection.setTextSelection(startPosition, null, { simpleTextSelection: true });
                                if (this.#docApp.isODF()) { this.setKeepPreselectedAttributes(false); } // 52649
                            });
                    } else {
                        returnObj = Position.skipDrawingsAndTables(_.clone(startPosition), _.clone(selection.getEndPosition()), activeRootNode, { backwards: true });
                        if (returnObj && returnObj.selectComplexField) {
                            helperNode = rangeMarker.getStartMarker(getRangeMarkerId(returnObj.node));
                            if (helperNode) {
                                startPosition = Position.getOxoPosition(activeRootNode, helperNode);
                                endPosition = selection.getStartPosition();
                                selection.setTextSelection(startPosition, endPosition);
                            } else {
                                globalLogger.error('editor.processKeyDown(): handling BACKSPACE behind complex field failed to fetch valid node!');
                            }
                            return;
                        }

                        if (returnObj.start === null || returnObj.end === null) { // #33154 backspace on table with position [0, 0] and exceeded size limit
                            return;
                        }

                        startPosition = returnObj.start;
                        endPosition = returnObj.end;

                        // handling for grouped drawings, only ODF (58986, 58987) ->  not generating invalid operation
                        if (this.#docApp.isODF() && this.#docApp.isPresentationApp() && Position.isGroupedDrawingPosition(this.getCurrentRootNode(), startPosition)) { return; }

                        if (startPosition[startPosition.length - 1] > 0) {

                            startPosition[startPosition.length - 1] -= 1;
                            endPosition[endPosition.length - 1] -= 1;
                            if (this.#docApp.isODF()) { this.setKeepPreselectedAttributes(true); } // 52649

                            if (!this.#onlyDrawingSelected(startPosition, endPosition)) {
                                this.deleteRange(startPosition, endPosition, { setTextSelection: false, handleUnrestorableContent: true, deleteKey: true })
                                    .fail(() => {
                                        // keeping the current selection
                                        startPosition[startPosition.length - 1] += 1;
                                    })
                                    .always(() => {
                                        selection.setTextSelection(startPosition, null, { simpleTextSelection: true });
                                        if (this.#docApp.isODF()) { this.setKeepPreselectedAttributes(false); }
                                    });
                            }

                        } else if (startPosition[startPosition.length - 2] >= 0) {

                            const paragraph = Position.getLastNodeFromPositionByNodeName(activeRootNode, startPosition, PARAGRAPH_NODE_SELECTOR);
                            let listLevel = -1;
                            const atParaStart = startPosition[startPosition.length - 1] === 0;
                            let styleId = null;
                            let elementAttributes = null;
                            let styleAttributes = null;
                            let paraAttributes = null;

                            if (!_(startPosition).all(value => { return (value === 0); })) {

                                startPosition[startPosition.length - 2] -= 1;
                                startPosition.pop();  // -> removing last value from startPosition !

                                const length = Position.getParagraphLength(activeRootNode, startPosition);
                                const domPos = Position.getDOMPosition(activeRootNode, startPosition);
                                let prevIsTable = false;
                                let paraBehindTable = null;
                                let localStartPos = null;
                                let fromStyle = false;
                                let prevPara;

                                if ((domPos) && (isTableNode(domPos.node))) {
                                    prevIsTable = true;
                                }

                                if (atParaStart) {

                                    elementAttributes = this.paragraphStyles.getElementAttributes(paragraph);
                                    paraAttributes = elementAttributes.paragraph;

                                    if (this.useSlideMode()) {
                                        if (_.isNumber(paraAttributes.level) && paraAttributes.level > -1) {
                                            // removing a bullet, if it exists
                                            if (paraAttributes.bullet && paraAttributes.bullet.type && paraAttributes.bullet.type !== 'none') {
                                                this.removeListAttributes();
                                                this.handleTriggeringListUpdate(paragraph);
                                                return;
                                            }
                                        }
                                    } else {

                                        styleId = elementAttributes.styleId;
                                        listLevel = paraAttributes.listLevel;
                                        styleAttributes = this.paragraphStyles.getStyleAttributeSet(styleId).paragraph;

                                        if (paraAttributes.listStyleId !== '') {
                                            fromStyle = paraAttributes.listStyleId === styleAttributes.listStyleId;
                                        }

                                        if (fromStyle) {
                                            // works in OOX as listStyle 0 is a non-numbering style
                                            // will not work in OpenDocument
                                            this.setAttribute('paragraph', 'listStyleId', 'L0');
                                            return;
                                        } else if (listLevel >= 0) {
                                            this.#implModifyListLevel(listLevel, false, -1);
                                            if (listLevel === 0 && _.isNumber(paraAttributes.indentFirstLine)) { this.setAttribute('paragraph', 'indentFirstLine', null); } // DOCS-2278
                                            return;
                                        } else if (styleId === this.getDefaultUIParagraphListStylesheet()) {
                                            this.setAttributes('paragraph', { styleId: this.getDefaultUIParagraphStylesheet() });
                                            // invalidate previous paragraph to recalculate bottom distance
                                            prevPara = findPreviousNode(activeRootNode, paragraph, PARAGRAPH_NODE_SELECTOR);
                                            if (prevPara) {
                                                this.paragraphStyles.updateElementFormatting(prevPara);
                                            }
                                            return;
                                        }
                                    }
                                }

                                if (startPosition[startPosition.length - 1] >= 0) {
                                    if (!prevIsTable) {
                                        if (this.getChangeTrack().isActiveChangeTracking() && !this.getChangeTrack().isInsertNodeByCurrentAuthor(paragraph)) {
                                            localStartPos = _.clone(startPosition);
                                            localStartPos[localStartPos.length - 1] += 1;
                                            this.applyOperations({
                                                name: SET_ATTRIBUTES,
                                                start: _.copy(localStartPos),  // the latter paragraph is marked for removal
                                                attrs: { changes: { removed: this.getChangeTrack().getChangeTrackInfo() } }
                                            });
                                        } else {
                                            const paraLen = Position.getParagraphLength(activeRootNode, startPosition);
                                            if (paraLen === 0 && !isManualPageBreakNode(paragraph)) {
                                                // simply remove first paragraph, if it is empty
                                                this.deleteRange(startPosition, null, { deleteKey: true });
                                            } else {
                                                // merge two following paragraphs, if both are not empty
                                                this.mergeParagraph(startPosition, paraLen);
                                            }
                                        }
                                    } else {

                                        checkPosition = _.clone(startPosition);
                                        checkPosition[checkPosition.length - 1] += 1; // increasing paragraph again
                                        //checkPosition.push(0);

                                        // if it is not an implicit paragraph now, it can be removed via operation (but not a final paragraph behind a table in a cell)
                                        paraBehindTable = Position.getParagraphElement(activeRootNode, checkPosition);
                                        if (paraBehindTable && Position.getParagraphNodeLength(paraBehindTable) === 0 && !isImplicitParagraphNode(paraBehindTable) && !isFinalParagraphBehindTableInCell(paraBehindTable)) {
                                            // remove paragraph explicitely or mark it as removed
                                            if (this.getChangeTrack().isActiveChangeTracking() && !this.getChangeTrack().isInsertNodeByCurrentAuthor(paragraph)) {
                                                this.applyOperations({
                                                    name: SET_ATTRIBUTES,
                                                    start: _.copy(checkPosition),  // the latter paragraph is marked for removal
                                                    attrs: { changes: { removed: this.getChangeTrack().getChangeTrackInfo() } }
                                                });
                                            } else {
                                                this.getUndoManager().enterUndoGroup(() => {
                                                    const prevNeighbour = getAllowedNeighboringNode(paraBehindTable, { next: false });
                                                    const nextNeighbour = getAllowedNeighboringNode(paraBehindTable);
                                                    const prevNeighbourPos = Position.getOxoPosition(activeRootNode, prevNeighbour);
                                                    // remove paragraph explicitely
                                                    this.applyOperations({ name: DELETE, start: _.copy(checkPosition) });
                                                    // check if table has paragraph with manual page break set, and remove it
                                                    if (isTableNode(nextNeighbour) && isManualPageBreakNode(nextNeighbour)) {
                                                        this.#removePageBreakBeforeAttribute(nextNeighbour);
                                                    }
                                                    // if both neighbours are tables, and it's possible, merge them
                                                    if (isTableNode(prevNeighbour) && isTableNode(nextNeighbour) && mergeableTables(prevNeighbour, nextNeighbour) && !this.#docApp.isODF()) {
                                                        this.mergeTable(prevNeighbourPos, { next: true });
                                                    }
                                                    return $.when();
                                                }, this); // end of enterUndoGroup()
                                            }
                                        }
                                    }
                                }

                                if (prevIsTable) {
                                    startPosition = Position.getLastPositionInParagraph(activeRootNode, startPosition, { ignoreImplicitParagraphs: true });
                                } else {
                                    const isFirstPosition = startPosition[startPosition.length - 1] < 0;
                                    if (isFirstPosition) {
                                        if (isFirstContentNodeInTextframe(paragraph)) {
                                            return; // simply ignore 'backspace' at start position of text frame
                                        } else if (Position.isPositionInTable(activeRootNode, startPosition)) {

                                            returnObj = Position.getLastPositionInPrevCell(activeRootNode, startPosition, { ignoreImplicitParagraphs: true });
                                            startPosition = returnObj.position;
                                            const beginOfTable = returnObj.beginOfTable;
                                            if (beginOfTable) {
                                                const table = Position.getContentNodeElement(activeRootNode, startPosition);
                                                if (!table && Position.isGroupedDrawingPosition(activeRootNode, startPosition)) { return; } // table not found, if it is a drawing table (59803)
                                                if (isFirstContentNodeInTextframe(table)) {
                                                    return; // simply ignore 'backspace' at start position of table
                                                }
                                                // check if table has paragraph with manual page break set, and remove it
                                                if (isManualPageBreakNode(table)) {
                                                    this.#removePageBreakBeforeAttribute(table);
                                                }
                                                // update position of cursor
                                                startPosition[startPosition.length - 1] -= 1;
                                                startPosition = Position.getLastPositionInParagraph(activeRootNode, startPosition, { ignoreImplicitParagraphs: true });
                                            }
                                        } else {
                                            startPosition.push(length);
                                        }
                                    } else {
                                        startPosition.push(length);
                                    }
                                }
                            } else {
                                // The position contains only '0's -> Backspace at the beginning of the document -> handle paragraphs with list style.
                                elementAttributes = this.paragraphStyles.getElementAttributes(paragraph);
                                styleId = elementAttributes.styleId;
                                paraAttributes = elementAttributes.paragraph;
                                listLevel = paraAttributes.listLevel;
                                styleAttributes = this.paragraphStyles.getStyleAttributeSet(styleId).paragraph;

                                if (this.useSlideMode()) {
                                    if (_.isNumber(paraAttributes.level) && paraAttributes.level > -1) {
                                        // removing a bullet, if it exists
                                        if (paraAttributes.bullet && paraAttributes.bullet.type && paraAttributes.bullet.type !== 'none') {
                                            this.removeListAttributes();
                                            this.handleTriggeringListUpdate(paragraph);
                                            return;
                                        }
                                    }
                                } else {
                                    if ((paraAttributes.listStyleId !== '') && (paraAttributes.listStyleId === styleAttributes.listStyleId)) {
                                        // works in OOX as listStyle 0 is a non-numbering style will not work in OpenDocument
                                        this.setAttribute('paragraph', 'listStyleId', 'L0');
                                    } else if (listLevel >= 0) {
                                        this.#implModifyListLevel(listLevel, false, -1);
                                        if (listLevel === 0 && _.isNumber(paraAttributes.indentFirstLine)) { this.setAttribute('paragraph', 'indentFirstLine', null); } // DOCS-2278
                                    } else if (styleId === this.getDefaultUIParagraphListStylesheet()) {
                                        this.setAttributes('paragraph', { styleId: this.getDefaultUIParagraphStylesheet() });
                                    }
                                }
                            }

                            selection.setTextSelection(startPosition, null, { simpleTextSelection: true });
                        }
                    }

                };
                if (_.browser.Android) {
                    //it has to be syncron on android, otherwise it can overjump some letters
                    handleBack();
                } else {
                    this.#doSyncOrAsyncCall(handleBack, this.#inputTextTimeout);
                }

            } else if ((!event.metaKey !== !event.ctrlKey) && !event.altKey) {

                if (hasKeyCode(event, 'ENTER')) {
                    // insert manual page break, but not inside text frames
                    if (selection.isAnyDrawingSelection()) {
                        event.preventDefault();
                        return;
                    }
                    this.insertManualPageBreak();
                }

                // prevent browser from evaluating the key event, but allow cut, copy and paste events
                if (!isClipboardKeyEvent(event)) {
                    event.preventDefault();
                }

                //this.clearPreselectedAttributes();
            } else if (matchKeyCode(event, 'TAB', { shift: null })) {

                event.preventDefault();

                this.setKeyDownTabHandling(true); // setting marker for tab handling (62404)

                if (this.isPositionInTable()) {

                    // if there is an active 'inputTextPromise', it has to be set to null now, so that
                    // following text input is included into a new deferred
                    this.setInputTextPromise(null);

                    // Executing the code for 'TAB' deferred. This is necessary because of deferred input of text.
                    // Defer time is 'this.#inputTextTimeout'
                    this.#doSyncOrAsyncCall(() => {

                        if (event.shiftKey) {
                            // Jump into first position of previous cell. Do not jump, if there is no previous cell
                            returnObj = Position.getFirstPositionInPreviousCell(activeRootNode, selection.getStartPosition());
                            if (!returnObj.beginOfTable) {
                                selection.setTextSelection(returnObj.position);
                            }
                        } else {
                            // Jumping into the next table cell or, if this is already the last cell of a table, insert a new row.
                            returnObj = Position.getFirstPositionInNextCell(activeRootNode, selection.getStartPosition());

                            if (returnObj.endOfTable) {

                                if (readOnly) {
                                    this.#docApp.rejectEditAttempt();
                                    event.preventDefault();
                                    return;
                                }

                                if (this.isRowAddable()) {
                                    this.insertRow();
                                    selection.setTextSelection(this.getLastOperationEnd());
                                } else {
                                    this.#docApp.getView().rejectEditTextAttempt('tablesizerow'); // checking table size (26809)
                                }
                            } else {
                                selection.setTextSelection(returnObj.position);
                            }
                        }

                    }, this.#inputTextTimeout);

                // select next/prev drawing
                } else if (this.isDrawingSelected() || (this.useSlideMode() && selection.isTopLevelTextCursor())) {
                    this.#docApp.getView().executeControllerItem('document/selectDrawing', { backwards: matchKeyCode(event, 'TAB', { shift: true }) });

                } else {

                    if (readOnly) {
                        this.#docApp.rejectEditAttempt();
                        event.preventDefault();
                        return;
                    }

                    // (shift)Tab: Change list indent (if in list) when selection is at first position in paragraph
                    const paragraph = Position.getLastNodeFromPositionByNodeName(activeRootNode, selection.getStartPosition(), PARAGRAPH_NODE_SELECTOR);
                    let mustInsertTab = true;  // always true, independent from shift-key

                    if (!selection.hasRange() && paragraph && _.last(selection.getStartPosition()) === Position.getFirstTextNodePositionInParagraph(paragraph)) {

                        const elementAttributes = this.paragraphStyles.getElementAttributes(paragraph);
                        const styleId = elementAttributes.styleId;
                        const paraAttributes = elementAttributes.paragraph;
                        let listLevel = paraAttributes.listLevel;
                        const styleAttributes = this.paragraphStyles.getStyleAttributeSet(styleId).paragraph;

                        if (paraAttributes.listStyleId) {
                            mustInsertTab = false;
                            const fromStyle = listLevel === -1 || paraAttributes.listStyleId === styleAttributes.listStyleId;
                            if (listLevel === -1) {
                                listLevel = paraAttributes.listLevel;
                            }
                            if (listLevel !== -1) {

                                if (!fromStyle) {
                                    this.#implModifyListLevel(listLevel, !event.shiftKey, 0);
                                } else {
                                    // numbering via paragraph style (e.g. outline numbering)
                                    const newStyleId = this.getListCollection().findPrevNextStyle(paraAttributes.listStyleId, styleId, event.shiftKey);
                                    if (newStyleId) {
                                        this.setAttributes('paragraph', { styleId: newStyleId });
                                    }
                                }
                            }
                        } else if (this.useSlideMode() && this.isListParagraph(paraAttributes)) {
                            mustInsertTab = false;
                            this.#implModifyListLevel(paraAttributes.level, !event.shiftKey, 0, true);
                        }

                        event.stopPropagation(); // otherwise the drawing will change (in OX Spreadsheet)
                    }
                    if (mustInsertTab && paragraph) {
                        // if there is an active 'inputTextPromise', it has to be set to null now, so that
                        // following text input is included into a new deferred
                        this.setInputTextPromise(null);

                        // Executing the code for 'TAB' deferred. This is necessary because of deferred input of text.
                        // Defer time is 'this.#inputTextTimeout'
                        this.#doSyncOrAsyncCall(this.insertTab, this.#inputTextTimeout);

                        event.stopPropagation(); // otherwise the drawing will change (in OX Spreadsheet)
                    }
                }
            } else if (isEscapeKey(event)) {

                const selectionBox = _.isFunction(this.getSelectionBox) ? this.getSelectionBox() : null;
                // cancelling an active selection box
                if (selectionBox && (selectionBox.isCancelModeActive() || selectionBox.isSelectionBoxActive())) {
                    selectionBox.cancelSelectionBox();
                    return false;
                }

                // deselect drawing node before the search bar
                if (this.isDrawingSelected()) {
                    selection.setTextSelection(selection.getEndPosition());
                    return false;
                }

                // deselect the paragraph in the text frame. Set the selection to the text frame itself.
                if (selection.isAdditionalTextframeSelection()) {
                    const start = Position.getOxoPosition(this.getCurrentRootNode(), selection.getSelectedTextFrameDrawing(), 0);
                    const end = Position.increaseLastIndex(start);

                    // select drawing
                    selection.setTextSelection(start, end);
                    return false;
                }

                if (this.isHeaderFooterEditState()) {
                    // leave edit state
                    pageLayout.leaveHeaderFooterAndSetCursor(activeRootNode, activeRootNode.parent());
                    return false;
                }
                // else: let ESCAPE key bubble up the DOM tree
            } else if (matchKeyCode(event, 'ENTER') && selection.getSelectionType() === 'drawing') {

                if (isTextFrameShapeDrawingFrame(selection.getSelectedDrawing())) {
                    event.preventDefault();
                    this.#docApp.getView().executeControllerItem('document/setCursorIntoTextframe');
                }
            } else if (matchKeyCode(event, 'F9')) {
                if (selection.hasRange()) {
                    this.getFieldManager().updateHighlightedFieldSelection();
                } else if (this.getFieldManager().isHighlightState()) {
                    this.getFieldManager().updateHighlightedField();
                }
            }

        }

        /**
         * Handler for the keyPress event, following the keyDown event.
         *
         * Info: Do not use comparisons with event.keyCode inside this.#pageProcessKeyPressHandler!
         *  The event from processKeyDown, that was executed before this this.#pageProcessKeyPressHandler
         *  is stored in the variable 'lastKeyDownEvent'. Please use this, if you want
         *  to make comparisons with keyCodes.
         *
         * Addition: Try to use this event ONLY for information about character
         *  that is beeing pressed on keyboard. For getting information about physical key being pressed,
         *  please use function processKeyDown! For combination of modifier keys and characters,
         *  use lastKeyDownEvent for modifier, and event for chars!
         *
         * @param {jQuery.Event} event
         *  A jQuery keyboard event object.
         */
        #pageProcessKeyPressHandler(event) {

            // Editor mode
            const readOnly = !this.#docApp.isEditable();
            // the char element
            let c = null;
            let hyperlinkSelection = null;
            // currently active root node
            const activeRootNode = this.getCurrentRootNode();
            // the last keydown event
            const lastKeyDownEvent = this.getLastKeyDownEvent();
            // the selection object
            const selection = this.getSelection();
            // whether an additional operation for removal of hyperlinks is required.
            let removeHyperLinkAttributes = false;

            // Helper function for collecting and inserting characters. This function is typically executed synchronously,
            // but in special cases with a selection containing unrestorable content, it is executed deferred.
            const doInsertCharacter = () => {

                // the string to be inserted with insert text operation
                let insertText = null;
                // the logical start position of the selection
                let startPosition = null;

                // merging input text, if there is already a promise and there are not already 5 characters deferred
                if (this.getInputTextPromise() && this.#activeInputText && this.#activeInputText.length < this.#maxTextInputChars) {
                    // deleting existing deferred for insert text and creating a new deferred
                    // containing the new and the old text
                    this.getInputTextPromise().abort();
                    this.#activeInputText += c;
                } else {
                    // creating a completely new deferred for insert text
                    this.setInputTextPromise(null);
                    this.#activeInputText = c;
                }

                insertText = this.#activeInputText;
                // Calling insertText deferred. Restarting, if there is further text input in the next 'this.#inputTextTimeout' ms.
                this.setInputTextPromise(this.#doSyncOrAsyncCall(() => {
                    startPosition = selection.getStartPosition();

                    // whether the text can be inserted at the specified position
                    let validPosition = true;

                    // check if a modified selection is not a slide selection and the drawing is already selected (happens on IE)
                    if (this.useSlideMode() && (startPosition.length === 2 || !selection.isAdditionalTextframeSelection())) { validPosition = false; }

                    if (validPosition) {

                        this.insertText(insertText, startPosition, this.getPreselectedAttributes());

                        // set cursor behind character
                        selection.setTextSelection(Position.increaseLastIndex(startPosition, insertText.length), null, { simpleTextSelection: !removeHyperLinkAttributes, insertOperation: true });
                    }

                    // Setting to null, so that new timeouts can be started.
                    // If there is already a running new inputTextPromise (fast typing), then this is also set to null, so that it cannot
                    // add further characters. But this is not problematic. Example: '123 456' -> After typing space, the inputTextPromise
                    // is set to null. Then it can happen that '4' and '5' are typed, and then this deferred function is executed. In this
                    // case the inputTextPromise for the '45' is terminated, even though there would be sufficient time to add the '6' too.
                    // But this is not important, because this concatenation is only required for faster type feeling.
                    this.setInputTextPromise(null);
                    // Resolving the this.#insertTextDef for the undoGroup. This is very important, because otherwise there could be
                    // pending actions that will never be sent to the server (33226). Every new character in 'processKeyPressed'
                    // needs the same 'this.#insertTextDef', because this is registered for the undoGroup with the return value of
                    // enterUndoGroup. A first character registers the promise of the 'this.#insertTextDef' and a second very fast
                    // following character must use the same (already created) this.#insertTextDef. It is finally resolved
                    // during the execution of this deferred function. What happens, if there is already a new inputTextPromise?
                    // Again the example '123 456'. After typing '123 ' the inputTextPromise is set to null. Then '456' is typed
                    // and collected. Then this deferred function is executed for '123 ', deletes the inputTextPromise (so that
                    // '456' is also completed) and resolves the this.#insertTextDef, so that it will be set to null (via the 'always'
                    // function). This makes also the '456' a complete block with resolved undoGroup. If later this function is
                    // executed for '456', the this.#insertTextDef might already be set to null. But in this case it simply does not
                    // need to be resolved, because this already happened at the previous run of the function with '123 '.
                    // The correct undoGroups are anyhow created by merging following insertText operations (mergeUndoActionHandler).
                    if (this.#insertTextDef) {  // -> the this.#insertTextDef might be already resolved in previous call of this function
                        this.#insertTextDef.resolve();  // resolving the deferred for the undo group
                    }
                }, this.#inputTextTimeout));

                // After a space, always a new text input promise is required -> text will be written.
                // Setting inputTextPromise to null, makes it unreachable for abort() .
                if (c === ' ') { this.setInputTextPromise(null); }
            };

            if (this.#docApp.isTextApp() && $(event.target).is('.ignorepageevent')) { return; } // the target has a marker, that page event handler can ignore it (inside the comment pane)

            // resetting MacOS marker for multiple directly following keydown events (46659)
            if (_.browser.MacOS) {
                this.setLastKeyDownKeyCode(null);
                this.setLastKeyDownModifiers(null);
                this.setRepeatedKeyDown(false);
                if (_.browser.Safari && event.key && (event.key.length > 1) && (event.charCode === event.key.charCodeAt(0))) {
                    // # 51454 - Text input on Mac/Safari doesn't work as expected using composing keys ´, `, ^
                    // BE CAREFUL: This workaround is based on behavior and NOT on specification. That Safari put
                    // two characters in the property 'key' is also strange. Therefore this workaround is doomed
                    // to fail, if somone in WebKit/Safari changes the behavior.
                    event.charCode = event.key.charCodeAt(1);
                }
            }

            // disable text input into slide, if slide mode is active (no preventDefault -> this breaks pasting)
            if (this.useSlideMode() && selection.isTopLevelTextCursor()) { return; }

            // disabling all kind of text modifications in master view in place holder drawings of ODF documents
            if (this.isODFReadOnyPlaceHolderDrawingProcessing && this.isODFReadOnyPlaceHolderDrawingProcessing()) {
                event.preventDefault();
                return;
            }

            // Android Chrome Code
            if (_.browser.Android && event.keyCode === 0) { return; }
            if (_.browser.Android && hasKeyCode(event, 'ENTER')) { event.preventDefault(); }

            this.dumpEventObject(event);

            if (lastKeyDownEvent && isIgnorableKey(lastKeyDownEvent.keyCode)) {
                return;
            }

            if (IOS_SAFARI_DEVICE && lastKeyDownEvent && lastKeyDownEvent.keyCode === 0 && isHangulText(event.originalEvent.data)) {
                // #42667 Korean keyboard has no feedback for IME-Mode
                event.preventDefault();
                return;
            }

            // Fix for 32910: Client reports internal error if user types as soon as possible after pasting lengthly content with lists
            if (this.getBlockKeyboardEvent()) {
                event.preventDefault();
                return false;
            }

            if (lastKeyDownEvent && isCursorKey(lastKeyDownEvent.keyCode)) {
                // Fix for 32008: CTRL+POS1/END create special characters on Mac/Chrome
                if (_.browser.WebKit && _.browser.MacOS && _.browser.Chrome && lastKeyDownEvent.ctrlKey) {
                    if ((lastKeyDownEvent.keyCode === 35) || (lastKeyDownEvent.keyCode === 36)) {
                        event.preventDefault();
                    }
                }
                return;
            }

            // needs to be checked on both keydown and keypress events
            if (isBrowserShortcutKeyEvent(event)) {
                if (!_.device('macos') && event.metaKey && !event.ctrlKey) { event.preventDefault(); } // 51465, allowing metaKey only on Mac
                return;
            }

            // TODO: Check if unnecessary! In most browsers returned already at processKeyDown!
            // handle just cursor, copy, escape, search and the global F6 accessibility events if in read only mode
            if (readOnly && (
                !lastKeyDownEvent || (
                    !isCopyKeyEvent(lastKeyDownEvent) &&
                    !isF6AcessibilityKeyEvent(lastKeyDownEvent) &&
                    !matchKeyCode(lastKeyDownEvent, 'TAB', { shift: null }) &&
                    !isEscapeKey(lastKeyDownEvent)
                )
            ) && !isSearchKeyEvent(event)
            ) {
                if (!this.isImportFinished()) {
                    this.#docApp.rejectEditAttempt('loadingInProgress');
                } else {
                    this.#docApp.rejectEditAttempt();
                }
                event.preventDefault();
                return;
            }

            // avoiding OT Collisions, if there are currently external operations applied (TODO: Informing the user with a yell?)
            if (this.#docApp.isOTEnabled() && (this.isProcessingExternalOperations() || this.isProcessingActions())) {
                this.trigger('ot:collision', { message: 'Suppressing keypress event!' });
                event.preventDefault();
                return;
            }

            // Special behavior for the iPad! Due to strange event order in mobile Safari we have to
            // pass the keyPressed event to the browser. We use the textInput event to cancel the DOM
            // modification instead. The composition system needs the keyPressed event to start a
            // a new composition.
            if (!IOS_SAFARI_DEVICE) {
                // prevent browser from evaluating the key event, but allow cut, copy and paste events
                if (!lastKeyDownEvent || !isClipboardKeyEvent(lastKeyDownEvent)) {
                    event.preventDefault();
                }
            } else {
                // We still get keydown/keypressed events during a composition session in mobile Safari.
                // According to the W3C specification this shouldn't happen. Therefore we have to prevent
                // to call our normal keyPressed code.
                if (this.isImeActive()) {
                    return;
                }
            }

            c = getPrintableChar(event);

            // TODO
            // For now (the prototype), only accept single chars, but let the browser process, so we don't need to care about DOM stuff
            // TODO: But we at least need to check if there is a selection!!!

            if (lastKeyDownEvent && (!lastKeyDownEvent.ctrlKey || (lastKeyDownEvent.ctrlKey && lastKeyDownEvent.altKey && !lastKeyDownEvent.shiftKey)) && !lastKeyDownEvent.metaKey && (c.length === 1)) {

                // do nothing in the case of a drawing multi selection or in the case of a slide selection
                if ((selection.isMultiSelectionSupported() && selection.isMultiSelection()) || (this.useSlideMode() && selection.isTopLevelTextCursor())) {
                    return;
                }

                this.getUndoManager().enterUndoGroup(() => {

                    // whether the this.#insertTextDef was created
                    let createdInsertTextDef = false;
                    // an optional modified selection
                    let modifiedSelection = null;
                    // a logical position
                    let startPosition = null;

                    // Setting this.#insertTextDef, if it is still null.
                    // Because of the undoGroup this this.#insertTextDef must be global (33226). It cannot be local and be created
                    // in every run inside this 'undoManager.enterUndoGroup'.

                    if (!this.#insertTextDef) {
                        createdInsertTextDef = true;
                        this.#insertTextDef = this.createDeferred().always(() => {
                            this.#insertTextDef = null;
                        });
                    }

                    if (selection.hasRange()) {

                        // Special handling for selected drawing frames
                        if (selection.isDrawingFrameSelection()) {

                            // not deleting the drawing -> adding the content into or before the drawing instead of deleting it
                            modifiedSelection = selection.getValidTextPositionAccordingToDrawingFrame();

                            if (modifiedSelection) {
                                // setting the new selection
                                selection.setTextSelection(modifiedSelection);
                                // inserting the character into the text frame
                                doInsertCharacter();
                            } else {
                                // did not find valid selection inside the text frame (there might be no paragraph inside)
                                createdInsertTextDef = false;
                                this.#insertTextDef = null;
                            }

                        } else {

                            // saving predefined character attributes at the start position
                            this.setKeepPreselectedAttributes(true);

                            this.deleteSelected({ saveStartAttrs: true })
                                .done(() => {
                                    this.setKeepPreselectedAttributes(false);
                                    // inserting the character with taking care of existing selection range
                                    // -> this might lead to a deferred character insertion
                                    doInsertCharacter();
                                })
                                .fail(() => {
                                    if (this.#insertTextDef) {
                                        this.#insertTextDef.resolve();
                                    }
                                    this.setKeepPreselectedAttributes(false);
                                });
                        }
                    } else {
                        // check left text to support hyperlink auto correction
                        if (event.charCode === 32) {
                            startPosition = selection.getStartPosition();
                            hyperlinkSelection = checkForHyperlinkText(this, selection.getEnclosingParagraph(), startPosition);
                            if (hyperlinkSelection !== null) {
                                removeHyperLinkAttributes = true;
                            }
                        }

                        // inserting the character without taking care of existing selection range
                        doInsertCharacter();
                    }

                    // returning a promise for the undo group (in the OT case, this.#insertTextDef is already set to null, 68198)
                    return (createdInsertTextDef && this.#insertTextDef) ? this.#insertTextDef.promise() : $.when();

                }) // enterUndoGroup
                    .done(() => {

                        if (hyperlinkSelection !== null) {
                            // perform auto stuff after undo group, so that first undo only undoes auto stuff
                            this.#doSyncOrAsyncCall(() => {
                                this.getUndoManager().enterUndoGroup(() => {
                                    if (!hyperlinkSelection.isStartOfParagraph) {
                                        insertHyperlink(
                                            this,
                                            hyperlinkSelection.start,
                                            hyperlinkSelection.end,
                                            (hyperlinkSelection.url === null) ? hyperlinkSelection.text : hyperlinkSelection.url,
                                            this.getActiveTarget(),
                                            { removeHyperLinkAttributes }
                                        );
                                    }
                                }, null, { preventSelectionChange: true });
                            }, this.#inputTextTimeout);
                        }

                    });

            } else if (c.length > 1) {
                // TODO?
            } else {

                if (lastKeyDownEvent && matchKeyCode(lastKeyDownEvent, 'ENTER', { shift: null })) {

                    // if there is an active 'inputTextPromise', it has to be set to null now, so that
                    // following text input is included into a new deferred
                    this.setInputTextPromise(null);

                    // 'Enter' key is possible with and without shift-key. With shift-key a hard break is inserted.
                    // A new paragraph shall also be inserted on iPhones, when shift is enabled in an empty paragraph or after "! ", "? " or ". ".
                    if (!lastKeyDownEvent.shiftKey || selection.isSpecialIgnoreShiftPositionOnIPhone()) {

                        // Executing the code for 'ENTER' deferred. This is necessary because of deferred input of text.
                        // Defer time is 'this.#inputTextTimeout'
                        this.#doSyncOrAsyncCall(() => {

                            // whether the selection has a range (saving state, because the selection will be removed by deleteSelected())
                            const hasSelection = selection.hasRange();

                            // Helper function for executing the 'Enter' key event. This function is typically executed synchronously,
                            // but in special cases with a selection containing unrestorable content, it is executed deferred.
                            const doHandleEnterKey = () => {

                                // the logical start position of the selection
                                const startPosition = selection.getStartPosition();
                                // the index of the last value in the logical start position
                                const lastValue = startPosition.length - 1;
                                // a new logical position
                                let newPosition = _.clone(startPosition);
                                // an object containing the start and end position of a hyperlink
                                let hyperlinkSelection = null;
                                // a logical table position
                                let localTablePos = null;
                                // whether the current position is the start position in a paragraph
                                const isFirstPositionInParagraph = (startPosition[lastValue] === 0);
                                // whether the current position is the start position of the first paragraph in the document or table cell
                                const isFirstPositionOfFirstParagraph = (isFirstPositionInParagraph && (startPosition[lastValue - 1] === 0));
                                // the paragraph element addressed by the passed logical position
                                const paragraph = Position.getLastNodeFromPositionByNodeName(activeRootNode, startPosition, PARAGRAPH_NODE_SELECTOR);
                                // Performance: Whether a simplified selection can be used
                                const isSimpleTextSelection = true;
                                let isSplitOperation = true;
                                // the paragraph attributes
                                let paragraphAttrs = null;
                                let listLevel;
                                let paragraphLength;
                                let endOfParagraph;
                                let split;
                                let paraText;
                                let labelText;

                                // Local helper function, to check if cursor is currently placed inside complex field node,
                                // so the list formatting can be ignored if true
                                const isCursorInsideCxField = () => {
                                    const domPos = Position.getDOMPosition(selection.getRootNode(), selection.getStartPosition());
                                    let node = null;

                                    if (domPos && domPos.node) {
                                        node = domPos.node;
                                        if (node.nodeType === 3) {
                                            node = node.parentNode;
                                        }
                                    }
                                    return isInsideComplexFieldRange(node);
                                };

                                // Local helper for checking if first non-empty paragraph child contains - and is not complex field, #42999
                                const isCxFieldFirstInPar = () => {
                                    const fieldRangeMarker = $(paragraph).children(RANGEMARKER_STARTTYPE_SELECTOR).first();
                                    if (fieldRangeMarker.length && getRangeMarkerType(fieldRangeMarker) === 'field') {
                                        const rangeMarkerStartPos = Position.getOxoPosition(activeRootNode, fieldRangeMarker);
                                        if (rangeMarkerStartPos && _.last(rangeMarkerStartPos) === 0) {
                                            return true;
                                        }
                                    }
                                    return false;
                                };

                                if (this.useSlideMode() && _.isFunction(this.isForcedHardBreakPosition) && this.isForcedHardBreakPosition(paragraph)) {
                                    hyperlinkSelection = checkForHyperlinkText(this, paragraph, startPosition); // 48216
                                    // at some positions hard break must be inserted instead of splitting the paragraph
                                    const ret = this.insertHardBreak();
                                    if (hyperlinkSelection !== null) {
                                        this.getUndoManager().enterUndoGroup(() => {
                                            insertHyperlink(this, hyperlinkSelection.start, hyperlinkSelection.end, hyperlinkSelection.url, this.getActiveTarget());
                                        }, null, { preventSelectionChange: true });
                                    }
                                    return ret;
                                }

                                // check for a possible hyperlink text
                                hyperlinkSelection = checkForHyperlinkText(this, paragraph, startPosition);
                                const numAutoCorrect = {};

                                if (isFirstPositionInParagraph &&
                                    isFirstPositionOfFirstParagraph &&
                                    (lastValue >= 4) &&
                                    Position.isPositionInTable(activeRootNode, [0]) &&
                                    startPosition.every(value => { return (value === 0); })
                                ) {
                                    //at first check if a paragraph has to be inserted before the current table
                                    this.insertParagraph([0]);
                                    // Setting attributes to new paragraph immediately (task 25670)
                                    this.paragraphStyles.updateElementFormatting(Position.getParagraphElement(activeRootNode, [0]));
                                    newPosition = [0, 0];
                                } else if (
                                    isFirstPositionInParagraph &&
                                    isFirstPositionOfFirstParagraph &&
                                    (lastValue >= 4) &&
                                    (
                                        (localTablePos = Position.getTableBehindTablePosition(activeRootNode, startPosition)) || // Inserting an empty paragraph between to tables
                                        (localTablePos = Position.getTableAtCellBeginning(activeRootNode, startPosition)) // Inserting an empty paragraph at beginning of table cell
                                    )
                                ) {
                                    // Inserting an empty paragraph between to tables
                                    this.insertParagraph(localTablePos);
                                    // Setting attributes to new paragraph immediately (task 25670)
                                    this.paragraphStyles.updateElementFormatting(Position.getParagraphElement(activeRootNode, localTablePos));
                                    newPosition = Position.appendNewIndex(localTablePos);
                                } else {
                                    // demote or end numbering instead of creating a new paragraph
                                    paragraphAttrs = this.paragraphStyles.getElementAttributes(paragraph).paragraph;
                                    listLevel = paragraphAttrs.listLevel;
                                    paragraphLength = paragraph ? Position.getParagraphNodeLength(paragraph) : -1;
                                    endOfParagraph = paragraphLength === _.last(startPosition);
                                    split = true;

                                    if (!hasSelection && listLevel >= 0 && paragraphLength === 0) {
                                        listLevel--;
                                        this.getUndoManager().enterUndoGroup(() => {
                                            if (listLevel < 0) {
                                                //remove list label and update paragraph
                                                $(paragraph).children(LIST_LABEL_NODE_SELECTOR).remove();
                                                this.setAttributes('paragraph', { styleId: this.getDefaultUIParagraphStylesheet(), paragraph: { listStyleId: null, listLevel: -1 } });
                                                if (_.isNumber(paragraphAttrs.indentFirstLine)) { this.setAttribute('paragraph', 'indentFirstLine', null); } // DOCS-2278
                                                this.implParagraphChanged(paragraph);
                                            } else {
                                                this.setAttribute('paragraph', 'listLevel', listLevel);
                                            }
                                        });
                                        split = false;
                                    }

                                    if (paragraph && this.useSlideMode() && isSlideNode(paragraph)) {
                                        split = false; // #61158 - prevent split on a slide paragraphs
                                    }

                                    if (paragraph && split === true) {

                                        if (!hasSelection && paragraphLength > 2 && !this.isListParagraph(paragraphAttrs) && !isCursorInsideCxField()) {
                                            // detect Numbering/Bullet labels at paragraph start

                                            if (paragraph !== undefined && !isCxFieldFirstInPar() && this.isAutoDetectionPosition(paragraph)) {
                                                this.validateParagraphNode(paragraph); // 52264, avoiding check of non-breakable space
                                                paraText = paragraph.textContent;
                                                // Task 30826 -> No automatic list generation, if split happens inside the first '. ' substring
                                                if (this.isListAutoDetectionString(startPosition, paragraphLength, paraText)) {
                                                    // Fix for 29508: Not detecting '123. ' or ' xmv. ' as list
                                                    // Fix for 29732: Detecting '- ' and '* ' automatically as list
                                                    labelText = paraText.split(' ', 1)[0];
                                                    numAutoCorrect.listDetection = detectListSymbol(labelText);
                                                    if (numAutoCorrect.listDetection.numberFormat !== undefined) {
                                                        numAutoCorrect.startPosition = _.clone(startPosition);
                                                        numAutoCorrect.startPosition[numAutoCorrect.startPosition.length - 1] = 0;
                                                        numAutoCorrect.endPosition = selection.getEndPosition();
                                                        numAutoCorrect.endPosition[numAutoCorrect.endPosition.length - 1] = labelText.length;
                                                    }
                                                }
                                            }
                                        }

                                        newPosition[lastValue - 1] += 1;
                                        newPosition[lastValue] = 0;

                                        this.getUndoManager().enterUndoGroup(() => {

                                            const localParaPos = _.clone(startPosition);
                                            const styleAttrs = this.characterStyles.getElementAttributes(paragraph.lastChild).character;
                                            const attrs = {};
                                            let newParagraph = null;
                                            let paraAttrs = null;
                                            let charAttrs = null;
                                            let paraStyleId = null;
                                            let isListParagraph = false;
                                            let followingParagraph = null;
                                            let styleAttributes;

                                            if (paragraphLength > 0 && endOfParagraph && styleAttrs?.url) {

                                                // Special handling: Inserting new paragraph after return at end of paragraph after hyperlink (task 30742)
                                                paraAttrs = getExplicitAttributeSet(paragraph);
                                                charAttrs = getExplicitAttributeSet(paragraph.lastChild);
                                                paraStyleId = getElementStyleId(paragraph);
                                                isListParagraph = this.isListStyleParagraph(null, paraAttrs);

                                                if (charAttrs && charAttrs.character && charAttrs.character.url) { delete charAttrs.character.url; }
                                                if (paraStyleId) { attrs.styleId = paraStyleId; }
                                                if (paraAttrs.paragraph && !_.isEmpty(paraAttrs.paragraph)) { attrs.paragraph = paraAttrs.paragraph; }
                                                if (charAttrs.character && !_.isEmpty(charAttrs.character)) { attrs.character = charAttrs.character; }
                                                // use only paragraph attributes in list paragraphs as character attributes, otherwise merge character attributes, task 30794
                                                if (paraAttrs.character && !_.isEmpty(paraAttrs.character)) {
                                                    if (isListParagraph) {
                                                        attrs.character = paraAttrs.character;
                                                    } else {
                                                        attrs.character = _.extend(paraAttrs.character, attrs.character || {});
                                                    }
                                                }

                                                // checking for 'nextStyleId'
                                                styleAttributes = this.paragraphStyles.getStyleSheetAttributeMap(paraStyleId);
                                                if (styleAttributes.paragraph && styleAttributes.paragraph.nextStyleId) { attrs.styleId = styleAttributes.paragraph.nextStyleId; }

                                                // modifying the attributes, if changeTracking is activated
                                                if (this.getChangeTrack().isActiveChangeTracking()) {
                                                    attrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo() };
                                                }

                                                this.applyOperations({ name: PARA_INSERT, start: _.copy(newPosition.slice(0, -1)), attrs });

                                                newParagraph = Position.getParagraphElement(activeRootNode, newPosition.slice(0, -1));
                                                this.implParagraphChangedSync($(newParagraph));

                                                selection.setTextSelection(newPosition, null, { simpleTextSelection: false, splitOperation: false });
                                                isSplitOperation = false;

                                                // updating lists, if required
                                                this.handleTriggeringListUpdate(paragraph);

                                            } else {

                                                this.setKeepPreselectedAttributes(true);  // preselected attributes must also be valid in following paragraph (26459)
                                                this.setUseParagraphCache(true);
                                                this.setParagraphCache(paragraph);  // Performance: Caching the currently used paragraph for temporary usage
                                                this.doCheckImplicitParagraph(startPosition);
                                                this.setGUITriggeredOperation(true); // Fix for 30597
                                                this.splitParagraph(startPosition, paragraph);
                                                this.setGUITriggeredOperation(false);
                                                this.setParagraphCache(null);  // Performance: Deleting the currently used paragraph cache
                                                this.setUseParagraphCache(false);

                                                // Special behaviour for splitting an empty paragraph
                                                if (_.last(localParaPos) === 0) { //  || (iPad)) {
                                                    // Fix for 28568: Splitted empty paragraph needs immediately content -> calling validateParagraphNode
                                                    this.validateParagraphNode(paragraph);
                                                }

                                                // Avoiding cursor jumping after 'Enter' at the end of a paragraph, because new paragraph is empty.
                                                followingParagraph = paragraph.nextSibling;
                                                if (paragraphLength > 0 && endOfParagraph && followingParagraph && isParagraphNode(followingParagraph) && Position.getParagraphNodeLength(followingParagraph) === 0) {
                                                    this.implParagraphChangedSync($(paragraph.nextSibling));
                                                }

                                                // modifying the attributes, if changeTracking is activated
                                                if (this.getChangeTrack().isActiveChangeTracking()) {
                                                    this.applyOperations({
                                                        name: SET_ATTRIBUTES,
                                                        start: _.copy(newPosition.slice(0, -1)),
                                                        attrs: { changes: { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null } }
                                                    });
                                                } else {
                                                    if (isChangeTrackNode(paragraph)) {
                                                        this.applyOperations({
                                                            name: SET_ATTRIBUTES,
                                                            start: _.copy(newPosition.slice(0, -1)),
                                                            attrs: { changes: { inserted: null, removed: null, modified: null } }
                                                        });
                                                    }
                                                }

                                                // checking 'nextStyleId' at paragraphs
                                                if (endOfParagraph) {

                                                    const styleId = getElementStyleId(paragraph);
                                                    const styleName = this.paragraphStyles.getName(styleId);

                                                    styleAttributes = this.paragraphStyles.getStyleSheetAttributeMap(styleId);
                                                    if (styleAttributes.paragraph && styleAttributes.paragraph.nextStyleId) {
                                                        const nextStyleId = styleAttributes.paragraph.nextStyleId;

                                                        if (nextStyleId === styleId || nextStyleId === styleName) {
                                                            //next style is same style
                                                        } else {
                                                            //listStyleId = null because header want normal text after them
                                                            this.applyOperations({
                                                                name: SET_ATTRIBUTES,
                                                                start: _.copy(newPosition.slice(0, -1)),
                                                                attrs: { styleId: nextStyleId, paragraph: { listStyleId: null, listLevel: -1 } }
                                                            });
                                                        }
                                                    }
                                                }
                                            }
                                        });

                                    }
                                }

                                // set hyperlink style and url attribute
                                if (hyperlinkSelection !== null) {
                                    this.getUndoManager().enterUndoGroup(() => {
                                        insertHyperlink(this, hyperlinkSelection.start, hyperlinkSelection.end, hyperlinkSelection.url, this.getActiveTarget());
                                    }, null, { preventSelectionChange: true });
                                }

                                // now apply 'AutoCorrection'
                                if (numAutoCorrect.listDetection && numAutoCorrect.listDetection.numberFormat !== undefined) {
                                    this.getUndoManager().enterUndoGroup(() => {
                                        this.deleteRange(numAutoCorrect.startPosition, numAutoCorrect.endPosition);
                                        this.createList((numAutoCorrect.listDetection.numberFormat === 'bullet') ? 'bullet' : 'numbering', {
                                            listStartValue: numAutoCorrect.listDetection.listStartValue,
                                            left: numAutoCorrect.listDetection.leftString,
                                            right: numAutoCorrect.listDetection.rightString,
                                            symbol: numAutoCorrect.listDetection.symbol,
                                            startPosition: numAutoCorrect.startPosition,
                                            numberFormat: numAutoCorrect.listDetection.numberFormat
                                        });
                                    }, null, { preventSelectionChange: true });
                                }

                                selection.setTextSelection(newPosition, null, { simpleTextSelection: isSimpleTextSelection, splitOperation: isSplitOperation, keepFocus: this.forceToKeepFocus() });

                                this.setKeepPreselectedAttributes(false);

                            }; // function doHandleEnterKey

                            // an optionally modified selection
                            let modifiedSelection = null;

                            if (selection.hasRange()) {
                                // a deferred object required for the undo group
                                const enterDef = this.createDeferred();

                                this.getUndoManager().enterUndoGroup(() => {
                                    // Special handling for selected drawing frames
                                    if (selection.isDrawingFrameSelection()) {
                                        // not deleting the drawing -> adding the content into or before the drawing instead of deleting it
                                        modifiedSelection = selection.getValidTextPositionAccordingToDrawingFrame();

                                        if (modifiedSelection) {
                                            // setting the new selection
                                            selection.setTextSelection(modifiedSelection);
                                            // inserting the return before the drawing character into the text frame
                                            doHandleEnterKey();
                                        }

                                    } else {

                                        this.deleteSelected()
                                            .done(() => {
                                                doHandleEnterKey();
                                            })
                                            .always(() => {
                                                enterDef.resolve();   // closing the undo group
                                            });
                                        return enterDef.promise();  // for the undo group
                                    }
                                });

                            } else {
                                doHandleEnterKey();
                            }

                            return $.when();

                        }, this.#inputTextTimeout);

                    } else if (lastKeyDownEvent.shiftKey) {
                        // insert a hard break
                        this.insertHardBreak();
                    }
                }
            } // end of else
        }

    }

    return KeyHandlingMixin;
}

// exports ====================================================================

export const XKeyHandling = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XTableOperation>}
     *  The extended class with keyboard handling functionality.
     */
    mixin: mixinKeyHandling
};
