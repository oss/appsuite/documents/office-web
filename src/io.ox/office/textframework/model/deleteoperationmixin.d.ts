/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { OperationGenerator } from "@/io.ox/office/editframework/model/operation/generator";
import { Position } from "@/io.ox/office/editframework/utils/operations";
import Snapshot from "@/io.ox/office/textframework/utils/snapshot";

// types ======================================================================

/**
 * Optional parameters for the function 'deleteRange()'.
 */
export interface DeleteRangeOptions {

    /**
     * If set to `false`, the text selection will not be set within this
     * function. This is useful, if the caller takes care of setting the cursor
     * after the operation. This is the case for 'Backspace' and 'Delete'
     * operations. It is a performance issue, that the text selection is not
     * set twice.
     */
    setTextSelection?: boolean;

    /**
     * Whether this function was triggered by the 'delete' or 'backspace' key.
     */
    deleteKey?: boolean;

    /**
     * If set to true, it will be checked if the range, that shall be deleted,
     * contains unrestorable content. In this case a dialog appears, in which
     * the user is asked, if he really wants to delete this content. This is
     * the case for 'Backspace' and 'Delete' operations.
     */
    handleUnrestorableContent?: boolean;
}

/**
 * Optional parameters for the function 'deleteSelected()'.
 */
interface DeleteSelectedOptions {

    /**
     * Whether deleteSelected was triggered from the pasting of clipboard. In
     * this case the check function 'checkSetClipboardPasteInProgress()' does
     * not need to be called, because blocking is already handled by the
     * clipboard function.
     */
    alreadyPasteInProgress?: boolean;

    /**
     * Whether this function was triggered by the 'delete' or 'backspace' key.
     * Or whether it was triggered via the controller function by tool bar or
     * context menu.
     */
    deleteKey?: boolean;

    /**
     * The snapshot that can be used, if the user cancels a long running
     * delete action. Typically this is created inside this function, but if
     * it was already created by the calling 'paste' function, it can be reused
     * here.
     */
    snapshot?: Snapshot;

    /**
     * Whether the character attributes at the start position shall be saved.
     * This attributes can be used later, if text is inserted after the content
     * was deleted and this new text shall have the same character attributes.
     */
    saveStartAttrs?: boolean;

    /**
     * Custom definied warning message during delete dialog.
     */
    warningLabel?: boolean;

    /**
     * Whether paragraphs shall not be deleted completely, so that the document
     * structure is not modified. In this scenario the "shortest path" must be
     * avoided, so that all paragraphs inside a table are visited and tables
     * are not deleted completely.
     */
    keepParagraphs?: boolean;

}

/**
 * Optional return value of the function `deleteSelected()`. This is used, when
 * "onlyCreateOperations" is set to true.
 */
export interface DeleteSelectedOperationResult {
    askUser: boolean;
    generator: OperationGenerator;
}

/**
 * Functions exported from the interface "DeleteOperationMixin".
 */
export interface DeleteOperationMixin {

    askUserHandler(askUser: boolean, asyncDelete: boolean): JPromise;
    deleteRange(start: Position, end: Opt<Position>, options: DeleteRangeOptions & { handleUnrestorableContent: true }): JPromise;
    deleteRange(start: Position, end?: Position, options?: DeleteRangeOptions): void;
    deleteSelected(options: DeleteSelectedOptions & { onlyCreateOperations: true }): JPromise<DeleteSelectedOperationResult>;
    deleteSelected(options?: DeleteSelectedOptions): JPromise;
}
