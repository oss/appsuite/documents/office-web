/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { Position }  from "@/io.ox/office/editframework/utils/operations";
import { AttrSetConstraint, ChangesAttributes, CharacterAttributes, ParagraphAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import { PoolMapConstraint } from "@/io.ox/office/editframework/model/attributepool";
import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import TableStyleCollection from "@/io.ox/office/editframework/model/tablestylecollection";
import { BaseAttributePoolMap, EditModelEventMap, EditModel } from "@/io.ox/office/editframework/model/editmodel";
import { DrawingAttributePoolMap, DrawingAttributeSet } from "@/io.ox/office/drawinglayer/model/drawingmodel";
import { NumberFormatter } from "@/io.ox/office/textframework/model/numberformatter";
import CharacterStyleCollection from "@/io.ox/office/textframework/format/characterstyles";
import ParagraphStyleCollection from "@/io.ox/office/textframework/format/paragraphstyles";
import TableRowStyleCollection, { TableRowAttributes } from "@/io.ox/office/textframework/format/tablerowstyles";
import Selection from "@/io.ox/office/textframework/selection/selection";
import { TextBaseApplication } from "@/io.ox/office/textframework/app/application";
import ChangeTrack from "@/io.ox/office/textframework/components/changetrack/changetrack";
import PageLayout from "@/io.ox/office/textframework/components/pagelayout/pagelayout";
import RangeMarker from "@/io.ox/office/textframework/components/rangemarker/rangemarker";
import FieldManager from "@/io.ox/office/textframework/components/field/fieldmanager";
import { DeleteOperationMixin } from "@/io.ox/office/textframework/model/deleteoperationmixin";

// types ======================================================================


export interface TextBaseAttributePoolMap extends BaseAttributePoolMap, DrawingAttributePoolMap {
    changes: [ChangesAttributes];
    character: [CharacterAttributes];
    paragraph: [ParagraphAttributes];
    row: [TableRowAttributes];
}

export type TextStyleCollection<AttrSetT extends AttrSetConstraint<AttrSetT>> = StyleCollection<TextBaseModel, TextBaseAttributePoolMap, AttrSetT>;

/**
 * Type mapping for the events emitted by `TextBaseModel` instances.
 */
export interface TextBaseModelEventMap<
    PoolMapT extends PoolMapConstraint<PoolMapT> & BaseAttributePoolMap = BaseAttributePoolMap
> extends EditModelEventMap<PoolMapT> {
    "paragraphUpdate:after": [paragraph: JQuery];
    "tableUpdate:after": [table: JQuery];
    "initialPageBreaks:done": [];
    "pageBreak:after": [];
    "document:reloaded": [snapshot: unknown];
    "document:reloaded:after": [snapshot: unknown];
}

// class TextBaseModel ========================================================

interface TextBaseModel<
    PoolMapT extends PoolMapConstraint<PoolMapT> & TextBaseAttributePoolMap = TextBaseAttributePoolMap,
    EvtMapT extends TextBaseModelEventMap<PoolMapT> = TextBaseModelEventMap<PoolMapT>
> extends EditModel<PoolMapT, EvtMapT>, DeleteOperationMixin {}

declare abstract class TextBaseModel<
    PoolMapT extends PoolMapConstraint<PoolMapT> & TextBaseAttributePoolMap = TextBaseAttributePoolMap,
    EvtMapT extends TextBaseModelEventMap<PoolMapT> = TextBaseModelEventMap<PoolMapT>
> extends EditModel<PoolMapT, EvtMapT> implements DeleteOperationMixin {

    declare readonly docApp: TextBaseApplication;

    readonly abstract characterStyles: CharacterStyleCollection<TextBaseModel>;
    readonly abstract paragraphStyles: ParagraphStyleCollection<TextBaseModel>;
    readonly abstract tableStyles: TableStyleCollection<TextBaseModel, any, any>;
    readonly abstract tableRowStyles: TableRowStyleCollection<TextBaseModel>;
    readonly abstract tableCellStyles: StyleCollection<TextBaseModel, TextBaseAttributePoolMap, any>;
    readonly abstract drawingStyles: StyleCollection<TextBaseModel, TextBaseAttributePoolMap, DrawingAttributeSet>;
    readonly abstract pageStyles: StyleCollection<TextBaseModel, TextBaseAttributePoolMap, any>;

    declare readonly numberFormatter: NumberFormatter;

    public constructor(docApp: TextBaseApplication, config?: unknown);

    checkChangesMode(attrs: Dict): Dict; // TS-TODO
    getNode(): JQuery;
    getCurrentRootNode(target?: string): JQuery;
    getRootNode(target?: string, index?: number): JQuery;
    getActiveTarget(): string;
    getDefaultUIHyperlinkStylesheet(): string | null;
    getCommentLayer(): any; // TODO
    getDrawingLayer(): any; // TODO
    getFieldManager(): FieldManager;
    getListenerList(): null | Dict; // TODO
    doCheckImplicitParagraph(postion: number[], options?: Dict): void;
    getSelection(): Selection;
    getChangeTrack(): ChangeTrack;
    getPageLayout(): PageLayout;
    getRangeMarker(): RangeMarker;
    exitCropMode(): void;
    extendPropertiesWithTarget(props: Dict, target: string): void;
    decreaseModifyingCounter(): void;
    increaseModifyingCounter(): void;
    isFastLoadImport(): boolean;
    isHeaderFooterEditState(): boolean;
    isCommentFunctionality(): boolean;
    isFullOdfTextframeFunctionality(): boolean;
    prepareTextSpanForInsertion(position: Position, options?: { isInsertText?: boolean; useCache?: boolean; allowDrawingGroup?: boolean }, target?: string | NodeOrJQuery): HTMLSpanElement | null;
    setFieldManager(manager: unknown): void;
    setListCollection(collection: unknown): void;
    setNumberFormatter(formatter: unknown): void;
    updateLists(allParagraphs: JQuery, updateOptions?: object): JPromise;
    insertText(text: string, pos: Position): void;
    setActiveMouseDownEvent(value: boolean): void;
    setActiveTarget(target?: string): void;
    setBlockKeyboardEvent(value: boolean): void;
    setLastOperationEnd(pos: Position): void;
    implParagraphChanged(node: NodeOrJQuery | NodeOrJQuery[]): void;
    collectCharacterAttributesForInsertOperation(node: NodeOrJQuery, pos: Position): Partial<CharacterAttributes> | null;
    getCharacterAttributesFromFinalSpan(node: NodeOrJQuery, pos: Position): Partial<CharacterAttributes> | null;

    inheritInlineCharacterAttributes(node: NodeOrJQuery, attrs?: Partial<CharacterAttributes>): void; // inherited from "attributeoperationmixin"
    checkTransferOfInlineCharacterAttributes(node: NodeOrJQuery, attrs?: Partial<CharacterAttributes>): void; // inherited from "attributeoperationmixin"
}

export default TextBaseModel;
