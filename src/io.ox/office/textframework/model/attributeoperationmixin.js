/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { NODE_SELECTOR, VERTICAL_ALIGNMENT_ATTRIBUTE, getAllGroupDrawingChildren, getClosestTextFrameDrawingNode,
    getDrawingNode, getDrawingType, getTextFrameNode, isAutoResizeHeightAttributes,
    isDrawingFrame, isGroupDrawingFrame, isTableDrawingFrame, isTextFrameShapeDrawingFrame,
    isTwoPointShape, updateSelectionDrawingSize } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { removeTableAttributes, setBorderFlagsToCells, setBorderWidthToVisibleCells } from '@/io.ox/office/textframework/components/table/table';
import TableStyles from '@/io.ox/office/textframework/format/tablestyles';
import { PARAGRAPH_NODE_SELECTOR, findFirstPortionSpan, getComplexFieldId, getComplexFieldInstruction, getDrawingPlaceHolderNode,
    getMarginalTargetNode, isBookmarkNode, isComplexFieldNode, isDrawingLayerNode, isDrawingWithOnlyImplicitParagraph, isEmptyListParagraph, isEmptyParagraph,
    isEmptySpan, isFieldNode, isFloatingDrawingNode, isHardBreakNode, isImplicitParagraphNode, isInlineComponentNode,
    isInlineDrawingNode, isListLabelNode, isListParagraphNode, isManualPageBreakNode, isMarginalNode, isParagraphNode, isParagraphWithoutNeighbour,
    isPartOfParagraph, isRangeMarkerNode, isSlideNode, isSpan, isSpecialField, isSpellerrorNode, isSubstitutionPlaceHolderNode, isTableNode,
    isTextFrameTemplateTextParagraph, isTextFrameTemplateTextSpan, isTextSpan, iterateTextSpans } from '@/io.ox/office/textframework/utils/dom';
import { appendNewIndex, decreaseLastIndex, getContentNodeElement, getDrawingElement, getDOMPosition, getLastNodeFromPositionByNodeName,
    getOxoPosition, getParagraphElement, getParagraphLength, getParagraphNodeLength,
    getPositionRangeForNode, getSelectedElement, getWordBoundaries, increaseLastIndex, isValidElementRange, iterateParagraphChildNodes, isWhitespaceCharInPosition,
    shouldSetAttsExtendToWord, splitLargeSelection } from '@/io.ox/office/textframework/utils/position';
import { MOVE, PARA_INSERT, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import { BREAK, convertLengthToHmm, findNextNode, findPreviousNode, getArrayOption, getBooleanOption, getDomNode, getObjectOption,
    getOption, getStringOption, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';

import { is } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

function mixinAttributeOperation(BaseClass) {

    // constants --------------------------------------------------------------

    const formatPainterAllowList = {
        character: {
            character: [
                'anchor',
                'baseline',
                'bold',
                'caps',
                'color',
                'fillColor',
                'fontName',
                'fontNameComplex',
                'fontNameEastAsia',
                'fontNameSymbol',
                'fontSize',
                'italic',
                'strike',
                'underline',
                'vertAlign'
            ]
        },
        paragraph: {
            paragraph: [
                'alignment',
                'bullet',
                'bulletColor',
                'bulletFont',
                'bulletSize',
                'defaultTabSize',
                'fillColor',
                'indentFirstLine',
                'indentLeft',
                'indentRight',
                'level',
                'lineHeight',
                'listLabelHidden',
                'listLevel',
                'listStyleId',
                'listStartValue',
                'outlineLevel',
                'spacingAfter',
                'spacingBefore',
                'tabStops',
                'styleId'
            ]
        },
        image: {
            line: [
                'bitmap',
                'color',
                'color2',
                'gradient',
                'pattern',
                'style',
                'type',
                'width'
            ],
            fill: [
                'bitmap',
                'color',
                'color2',
                'gradient',
                'pattern',
                'type'
            ]
        },
        connector: {
            line: [
                'bitmap',
                'color',
                'color2',
                'gradient',
                'headEndLength',
                'headEndType',
                'headEndWidth',
                'pattern',
                'style',
                'tailEndLength',
                'tailEndType',
                'tailEndWidth',
                'type',
                'width'
            ]
        },
        shape: {
            character: [
                'anchor',
                'baseline',
                'bold',
                'caps',
                'color',
                'fillColor',
                'fontName',
                'fontNameComplex',
                'fontNameEastAsia',
                'fontNameSymbol',
                'fontSize',
                'italic',
                'strike',
                'underline',
                'vertAlign'
            ],
            paragraph: [
                'alignment',
                'bullet',
                'bulletColor',
                'bulletFont',
                'bulletSize',
                'defaultTabSize',
                'fillColor',
                'indentFirstLine',
                'indentLeft',
                'indentRight',
                'level',
                'lineHeight',
                'listLabelHidden',
                'listStartValue',
                'outlineLevel',
                'spacingAfter',
                'spacingBefore',
                'tabStops'
            ],
            shape: [
                'anchor',
                'anchorCentered',
                'autoResizeText',
                'fontScale',
                'horzOverflow',
                'lineReduction',
                'noAutoResize',
                'vert',
                'vertOverflow',
                'wordWrap'
            ],
            line: [
                'bitmap',
                'color',
                'color2',
                'gradient',
                'pattern',
                'style',
                'type',
                'width'
            ],
            fill: [
                'bitmap',
                'color',
                'color2',
                'gradient',
                'pattern',
                'type'
            ]
        },
        table: {
            table: [
                'borderBottom',
                'borderInsideHor',
                'borderInsideVert',
                'borderLeft',
                'borderRight',
                'borderTop',
                'exclude',
                'fillColor',
                'paddingBottom',
                'paddingLeft',
                'paddingRight',
                'paddingTop',
                'tableGrid',
                'width'
            ]
        }
    };

    // temporary array with only one entry, for use in Position functions for performance
    const TMPPOSARRAY = [0];

    /**
     * An object specifying those drawing types, at that attributes of the 'fill'
     * family can be assigned.
     * Info: This constant might move to DrawingLayer, especially if further
     *       constants of this type are required.
     */
    const FILL_DRAWING_TYPES = {
        shape: 1,
        group: 1,
        image: 1,
        chart: 1
    };

    /**
     * An object specifying those drawing types, at that attributes of the 'line'
     * family can be assigned.
     * Info: This constant might move to DrawingLayer, especially if further
     *       constants of this type are required.
     */
    const LINE_DRAWING_TYPES = {
        shape: 1,
        group: 1,
        image: 1,
        chart: 1,
        connector: 1
    };

    // mix-in class AttributeOperationMixin =====================================

    /**
     * A mix-in class for the document model class providing the operation
     * handling for setting attributes used in a presentation and text document.
     *
     * @param {EditApplication} app
     *  The application instance.
     */
    class AttributeOperationMixin extends BaseClass {

        // the application object
        #docApp = null;
        // a list of drawing filters to filter multi drawing selection
        #drawingFilterList = [];
        // FormatPainter
        #formatPainterActive = false;
        #formatPainterAttrs = {};

        constructor(docApp, ...baseCtorArgs) {

            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;

            // registering the filter for setting attributes to drawings. This is required
            // for multi drawing selections, that might make it possible to assign attributes
            // to drawings, that do not support these attributes.
            this.#registerDrawingFilter();
        }

        // public methods -----------------------------------------------------

        /**
         * Changes multiple attributes of the specified attribute family in the
         * current selection.
         *
         * @param {String} family
         *  The name of the attribute family containing the passed attributes.
         *
         * @param {Object} attributes
         *  A map of attribute value maps (name/value pairs), keyed by
         *  attribute families.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.clear=false]
         *      If set to true, all existing explicit attributes will be
         *      removed from the selection while applying the new attributes.
         *  @param {Boolean} [options.drawingFilterRequired=false]
         *      If set to true, it might be necessary to filter an existing
         *      multi drawing selection.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved after all operations have been
         *  generated and applied successfully.
         */
        setAttributes(family, attributes, options) {
            // Create an undo group that collects all undo operations generated
            // in the local setAttributes() method (it calls itself recursively
            // with smaller parts of the current selection).
            return this.getUndoManager().enterUndoGroup(() => {

                // table or drawing element contained by the selection
                let element = null;
                // operations generator
                const generator = this.createOperationGenerator();
                // the style sheet container
                const styleSheets = this.getStyleCollection(family);
                // logical position
                let localPosition = null;
                // another logical position
                let localDestPosition = null;
                // the length of the paragraph
                let paragraphLength = null;
                // whether after assigning cell attributes, it is still necessary to assign table attributes
                let createTableOperation = true;
                // paragraph helper position and helper node
                let paraPos = null, paraNode = null;
                // the old attributes, required for change tracking
                let oldAttrs = null;
                // whether the modification can be change tracked
                let isTrackableChange = true;
                // currently active root node
                const activeRootNode = this.getCurrentRootNode();
                // if we apply new style, do not remove manual pagebreaks
                let pageBreakBeforeAttr = false;
                let pageBreakAfterAttr = false;
                // the old attributes, required for pageBreak paragraph style
                let oldParAttr = null;
                // the promise for the asynchronous execution of operations
                let operationsPromise = null;
                // the promise for generating the operations
                let operationGeneratorPromise = null;
                // the complete promise for generation and applying of operations
                let setAttributesPromise = null;
                // the ratio of operation generation to applying of operation
                const operationRatio = 0.3;
                // an optional array with selection ranges for large selections
                let splittedSelectionRange = null;
                // a snapshot object
                let snapshot = null;
                // the selected drawings
                let drawings = null;
                // the selection object
                const selection = this.getSelection();
                // whether this is a selection in a master/layout slide
                let isMasterLayoutSelection = false;
                // whether the implicit paragraphs are handled like in ODF slide mode
                let isODFSlideMode = false;
                // whether an implicit paragraph got attributes
                let changedImplicitParagraph = false;

                /**
                 * Helper function to generate operations. This is done synchronously. But for asynchronous usage,
                 * the selection range can be restricted. This is especially useful for large selections.
                 *
                 * @param {Number[][]} [selectionRange]
                 *  An array with two logical positions for the start and the end position of a selection range.
                 */
                const generateAllSetAttributeOperations = selectionRange => {

                    // the logical start position, if specified
                    const localStartPos = (selectionRange && selectionRange[0]) || null;
                    // the logical end position, if specified
                    const localEndPos = (selectionRange && selectionRange[1]) || null;
                    // the change track object
                    const changeTrack = this.getChangeTrack();
                    // array containing start end end position of word in which cursor is placed
                    let wordBoundaryPos = null;
                    // start postion of wrapped word
                    let wordBoundaryStart = null;
                    // end position of wrapped word
                    let wordBoundaryEnd = null;
                    // start position of changetracked span
                    let changeTrackStartPos = null;
                    // end position of changetracked span
                    let changeTrackEndPos = null;
                    // the selected drawing nodes
                    let selectedDrawings = null;
                    // the affected drawing nodes of a multi drawing selection
                    let affectedDrawings = null;

                    // generates a 'setAttributes' operation with the correct attributes
                    const generateSetAttributeOperation = (startPosition, endPosition, oldAttrs) => {

                        // the options for the operation
                        let operationOptions = null;
                        // the attributes of the selected drawing
                        let drawingAttrs = null;

                        // assigning character attributes to paragraph in template drawings of master and layout slides (but not footer placeholder)
                        if (this.#docApp.isPresentationApp() && isMasterLayoutSelection && family === 'character') {
                            drawingAttrs = getExplicitAttributeSet(selection.getAnyDrawingSelection());
                            // -> not modifying the selection for simple shapes or text frames (63118) and also not for the footer of the master/layout slide
                            if (drawingAttrs.presentation && drawingAttrs.presentation.phType && _.isFunction(this.isPresentationFooterType) && !this.isPresentationFooterType(drawingAttrs.presentation.phType)) {
                                startPosition = _.initial(startPosition); // assigning character attributes to paragraph
                                endPosition = null;
                            }
                        }

                        operationOptions = { start: _.copy(startPosition), attrs: _.clone(attributes) };

                        // adding the old attributes, author and date for change tracking
                        if (oldAttrs) {
                            oldAttrs = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
                            operationOptions.attrs.changes = { modified: oldAttrs };
                        }

                        // add end position if specified
                        if (_.isArray(endPosition)) {
                            operationOptions.end = _.copy(endPosition);
                        }
                        // paragraph's style manual page break before
                        if (pageBreakBeforeAttr) {
                            operationOptions.attrs.paragraph = operationOptions.attrs.paragraph || {};
                            operationOptions.attrs.paragraph.pageBreakBefore = true;
                            pageBreakBeforeAttr = false;
                        }
                        // paragraph's style manual page break after
                        if (pageBreakAfterAttr) {
                            operationOptions.attrs.paragraph = operationOptions.attrs.paragraph || {};
                            operationOptions.attrs.paragraph.pageBreakAfter = true;
                            pageBreakAfterAttr = false;
                        }

                        // generate the 'setAttributes' operation
                        generator.generateOperation(SET_ATTRIBUTES, operationOptions);
                    };

                    // setting a specified selection, if defined
                    // if (selectionRange) { selection.setTextSelection.apply(selection, selectionRange); }

                    // generate 'setAttribute' operations
                    switch (family) {

                        case 'character':
                            if (selection.hasRange()) {
                                selection.iterateContentNodes((paragraph, position, startOffset, endOffset) => {

                                    const isUndefinedEnd = endOffset === undefined;

                                    if (isODFSlideMode && isImplicitParagraphNode(paragraph)) { return; } // no operation

                                    // validate start offset (iterator passes 'undefined' for fully covered paragraphs)
                                    if (!_.isNumber(startOffset)) {
                                        startOffset = 0;
                                    }
                                    // validate end offset (iterator passes 'undefined' for fully covered paragraphs)
                                    if (!_.isNumber(endOffset)) {
                                        endOffset = getParagraphNodeLength(paragraph) - 1;
                                    }

                                    if (changeTrack.isActiveChangeTracking()) {

                                        // iterating over all children to send the old attributes in the operation
                                        // remove all empty text spans which have sibling text spans, and collect
                                        // sequences of sibling text spans (needed for white-space handling)
                                        iterateParagraphChildNodes(paragraph, (node, nodestart, _nodelength, offsetstart, offsetlength) => {

                                            // evaluate only text spans that are partly or completely covered by selection
                                            if (isTextSpan(node)) {
                                                // one operation for each span
                                                changeTrackStartPos = nodestart + offsetstart;
                                                changeTrackEndPos = nodestart + offsetstart + offsetlength - 1;
                                                // decrease end position as long as it is whitespace character at that position
                                                // but only if it is last span in paragraph
                                                if (endOffset === changeTrackEndPos) {
                                                    if (!isWhitespaceCharInPosition(activeRootNode, position.concat([changeTrackEndPos + 1]))) {
                                                        while (changeTrackEndPos >= changeTrackStartPos && isWhitespaceCharInPosition(activeRootNode, position.concat([changeTrackEndPos]))) {
                                                            changeTrackEndPos -= 1;
                                                        }
                                                    }
                                                }
                                                if (changeTrackEndPos >= changeTrackStartPos) {
                                                    generateSetAttributeOperation(position.concat([changeTrackStartPos]), position.concat([changeTrackEndPos]), changeTrack.getOldNodeAttributes(node));
                                                }
                                            }

                                        }, undefined, { start: startOffset, end: endOffset });

                                        // DOCS-4211: Assiging character attributes to the paragraph, if the complete paragraph is selected
                                        // -> change tracking is not (always) useful, because the user cannot see the character attributes at the paragraphs directly
                                        if (startOffset === 0 && this.hasOoxmlTextCharacterFilterSupport() && (isUndefinedEnd || endOffset === (getParagraphNodeLength(paragraph) - 1))) {
                                            generateSetAttributeOperation(_.copy(position), undefined, changeTrack.getOldNodeAttributes(paragraph));
                                        }

                                    } else {

                                        // set the attributes at the covered text range
                                        if (startOffset === 0 && endOffset <= 0 && isImplicitParagraphNode(paragraph)) {
                                            generator.generateOperation(PARA_INSERT, { start: _.copy(position) });  // generating paragraph
                                            generateSetAttributeOperation(position);  // assigning attribute to new paragraph
                                        } else if (startOffset === 0 && endOffset <= 0 && getParagraphNodeLength(paragraph) === 0) {
                                            generateSetAttributeOperation(position);  // assigning attribute to existing empty paragraph
                                        } else if (startOffset <= endOffset) {
                                            // decrease end position as long as it is whitespace character at that position
                                            // we dont want ending whitespace to be attributed (underlined for example).
                                            // Exception is when end selection range is at the end of the paragraph (see #48059)
                                            if (!isWhitespaceCharInPosition(activeRootNode, position.concat([endOffset + 1])) && (endOffset !== getParagraphNodeLength(paragraph) - 1)) {
                                                while (endOffset > startOffset && isWhitespaceCharInPosition(activeRootNode, position.concat([endOffset]))) {
                                                    endOffset -= 1;
                                                }
                                            }
                                            generateSetAttributeOperation(position.concat([startOffset]), position.concat([endOffset]));

                                            // DOCS-4211: Assiging character attributes to the paragraph, if the complete paragraph is selected
                                            if (startOffset === 0 && this.hasOoxmlTextCharacterFilterSupport() && (isUndefinedEnd || endOffset === (getParagraphNodeLength(paragraph) - 1))) {
                                                generateSetAttributeOperation(_.copy(position));
                                            }
                                        }

                                    }
                                    this.getSpellChecker().reset(attributes, paragraph);
                                }, null, { startPos: localStartPos, endPos: localEndPos });
                            } else {  // selection has no range
                                // expand set attributes operation to whole word if cursor is inside word
                                if (shouldSetAttsExtendToWord(activeRootNode, selection.getStartPosition())) {
                                    wordBoundaryPos = getWordBoundaries(activeRootNode, selection.getStartPosition());
                                    paraPos = _.clone(selection.getStartPosition());
                                    paraPos.pop();
                                    paraNode = getParagraphElement(activeRootNode, paraPos);

                                    if (changeTrack.isActiveChangeTracking()) {
                                        // Creating one setAttribute operation for each span inside the paragraph, because of old attributes!
                                        oldAttrs = {};
                                        if (isParagraphNode(paraNode)) { oldAttrs = changeTrack.getOldNodeAttributes(paraNode); }
                                    }
                                    // set the attributes at the extended text range
                                    if (_.isArray(wordBoundaryPos) && wordBoundaryPos.length === 2) {
                                        wordBoundaryStart = _.isArray(wordBoundaryPos[0]) ? wordBoundaryPos[0] : null;
                                        wordBoundaryEnd = _.isArray(wordBoundaryPos[1]) ? decreaseLastIndex(wordBoundaryPos[1]) : null;
                                        if (wordBoundaryStart && wordBoundaryEnd) {
                                            generateSetAttributeOperation(wordBoundaryStart, wordBoundaryEnd, oldAttrs);
                                            this.getSpellChecker().reset(attributes, paraNode);
                                        }
                                    }
                                } else if (getParagraphLength(activeRootNode, _.clone(selection.getStartPosition())) === 0) {
                                    // Task 28187: Setting character attributes at empty paragraphs
                                    // In this.#implSetAttributes it is additionally necessary to remove the character attributes
                                    // from text span so that character styles at paragraph become visible

                                    paraPos = _.clone(selection.getStartPosition());
                                    paraPos.pop();
                                    paraNode = getParagraphElement(activeRootNode, paraPos);

                                    // Defining the oldAttrs, if change tracking is active
                                    if (changeTrack.isActiveChangeTracking()) {
                                        // Creating one setAttribute operation for each span inside the paragraph, because of old attributes!
                                        oldAttrs = {};
                                        if (isParagraphNode(paraNode)) { oldAttrs = changeTrack.getOldNodeAttributes(paraNode); }
                                    }

                                    if (isImplicitParagraphNode(paraNode)) {
                                        changedImplicitParagraph = true;
                                        generator.generateOperation(PARA_INSERT, { start: _.copy(paraPos), noUndo: true });
                                    }
                                    // generating setAttributes operation
                                    generateSetAttributeOperation(_.initial(_.clone(selection.getStartPosition())), undefined, oldAttrs);
                                } else {
                                    // using preselected attributes for non-empty paragraphs
                                    this.addPreselectedAttributes(attributes);
                                }
                            }
                            break;

                        case 'paragraph':
                            // deleted all changes for bug #26454#, because it did the opposite of word behavior, and that is not what the user expects! @see Bug 37174

                            selection.iterateContentNodes((paragraph, position) => {

                                if (isODFSlideMode && isImplicitParagraphNode(paragraph)) { return; } // no operation

                                // Defining the oldAttrs, if change tracking is active
                                if (changeTrack.isActiveChangeTracking()) {
                                    // Expanding operation for change tracking with old explicit attributes
                                    oldAttrs = changeTrack.getOldNodeAttributes(paragraph);
                                }
                                // Preserve pageBreakBefore and/or pageBreakAfter attribute after setting new paragraph style
                                if (isManualPageBreakNode(paragraph)) {
                                    oldParAttr = getExplicitAttributeSet(paragraph);
                                    if (_.isObject(oldParAttr) && oldParAttr.paragraph) {
                                        if (oldParAttr.paragraph.pageBreakBefore === true) {
                                            pageBreakBeforeAttr = true;
                                        }
                                        if (oldParAttr.paragraph.pageBreakAfter === true) {
                                            pageBreakAfterAttr = true;
                                        }
                                    }

                                }

                                // generating a new paragraph, if it is implicit
                                if (isImplicitParagraphNode(paragraph)) { generator.generateOperation(PARA_INSERT, { start: _.copy(position), noUndo: true }); }
                                // generating setAttributes operation
                                generateSetAttributeOperation(position, undefined, oldAttrs);
                            }, null, { startPos: localStartPos, endPos: localEndPos });
                            break;

                        case 'cell':
                            selection.iterateTableCells((cell, position) => {
                                // Defining the oldAttrs, if change tracking is active
                                if (changeTrack.isActiveChangeTracking()) {
                                    // Expanding operation for change tracking with old explicit attributes
                                    oldAttrs = changeTrack.getOldNodeAttributes(cell);
                                }
                                generateSetAttributeOperation(position, undefined, oldAttrs);
                            });
                            break;

                        case 'table':

                            element = selection.getEnclosingTable();

                            if (!element && this.useSlideMode() && selection.isDrawingSelection() && isTableDrawingFrame(selection.getSelectedDrawing())) {
                                element = selection.getSelectedDrawing();
                            }

                            if (element) {

                                localPosition = getOxoPosition(activeRootNode, element, 0);  // the logical position of the table

                                if (getBooleanOption(options, 'clear', false)) {
                                    // removing hard attributes at tables and cells, so that the table style will be visible
                                    removeTableAttributes(this, activeRootNode, element, localPosition, generator);
                                }

                                if (getBooleanOption(options, 'onlyVisibleBorders', false)) {
                                    // setting border width directly at cells -> overwriting values of table style
                                    setBorderWidthToVisibleCells(element, attributes, this.tableCellStyles, activeRootNode, generator, this);
                                    createTableOperation = false;
                                }

                                if (getBooleanOption(options, 'cellSpecificTableAttribute', false)) {
                                    // setting border flags directly at cells -> overwriting values of table style
                                    setBorderFlagsToCells(element, attributes, TableStyles.getBorderStyleFromAttributes(this.getAttributes('cell').cell || {}), activeRootNode, generator, this);
                                }

                                // setting attributes to the table element
                                // -> also setting table attributes, if attributes are already assigned to table cells, to
                                // keep the getter functions simple and performant
                                if (createTableOperation) {
                                    // Defining the oldAttrs, if change tracking is active
                                    if (changeTrack.isActiveChangeTracking()) {
                                        // Expanding operation for change tracking with old explicit attributes
                                        oldAttrs = changeTrack.getOldNodeAttributes(element);
                                    }
                                    generateSetAttributeOperation(localPosition, undefined, oldAttrs);
                                }
                            }
                            break;

                        case 'drawing':

                            selectedDrawings = selection.getSelectedDrawings();

                            if (!selectedDrawings || selectedDrawings.length === 0) { break; }

                            // a filter might be needed to generate only valid operations in a multi drawing selection
                            // -> example: No background for drawings of type 'connector'
                            affectedDrawings = (selection.isMultiSelection() && getBooleanOption(options, 'drawingFilterRequired', false)) ? this.#filterDrawingNodes(selectedDrawings, attributes) : selectedDrawings;

                            _.each(affectedDrawings, element => {

                                // TODO: this fails if a drawing style sheet changes the inline/floating mode instead of explicit attributes
                                if (isDrawingFrame(element)) {

                                    localPosition = getOxoPosition(activeRootNode, element, 0);

                                    if (_.isObject(attributes.drawing)) {

                                        // when switching from inline to floated, saving current position in the drawing, so that it can
                                        // be set correctly when switching back to inline.
                                        // This is only necessary, if the drawing was moved in that way, that implMove needed to be called.
                                        if ((attributes.drawing.inline === false) && (isInlineDrawingNode(element))) {
                                            $(element).data('inlinePosition', localPosition[localPosition.length - 1]);
                                            // Fixing vertical offset, if drawing is set from inline to floated (31722)
                                            if ((attributes.drawing.anchorVertBase) && (attributes.drawing.anchorVertBase === 'paragraph') &&
                                                (_.isNumber(attributes.drawing.anchorVertOffset)) && (attributes.drawing.anchorVertOffset === 0) &&
                                                (($(element).offset().top - $(element).closest(PARAGRAPH_NODE_SELECTOR).offset().top > 0))) {
                                                attributes.drawing.anchorVertOffset = convertLengthToHmm($(element).offset().top - $(element).closest(PARAGRAPH_NODE_SELECTOR).offset().top, 'px');
                                            }
                                        }

                                        // when switching from floated to inline, a move of the drawing might be necessary
                                        if ((attributes.drawing.inline === true) && (isFloatingDrawingNode(element)) && ($(element).data('inlinePosition'))) {

                                            localDestPosition = _.clone(localPosition);
                                            paragraphLength = getParagraphLength(activeRootNode, localDestPosition);
                                            if ((paragraphLength - 1) < $(element).data('inlinePosition')) {  // -> is this position still valid?
                                                localDestPosition[localDestPosition.length - 1] = paragraphLength - 1;
                                            } else {
                                                localDestPosition[localDestPosition.length - 1] = $(element).data('inlinePosition');
                                            }
                                            if (!_.isEqual(localPosition, localDestPosition)) {

                                                // the logical position of the paragraph
                                                const paraPos = _.clone(localDestPosition);
                                                paraPos.pop();
                                                // the paragraph node
                                                const paraNode = getParagraphElement(activeRootNode, paraPos);

                                                if (isImplicitParagraphNode(paraNode)) { generator.generateOperation(PARA_INSERT, { start: _.copy(paraPos), noUndo: true }); }
                                                generator.generateOperation(MOVE, { start: _.copy(localPosition), end: _.copy(localPosition), to: _.copy(localDestPosition) });

                                                localPosition = _.clone(localDestPosition);
                                            }
                                        }

                                        // setting new position (inline, paragraph, ...) is not change tracked
                                        if (!changeTrack.ctSupportsDrawingPosition()) { isTrackableChange = false; }
                                    }

                                    // setting border and fillcolor of drawing is not change tracked
                                    if (_.isObject(attributes.fill) || _.isObject(attributes.line)) { isTrackableChange = false; }

                                    // Defining the oldAttrs, if change tracking is active
                                    // -> and if change track supports changing of drawing position
                                    if (changeTrack.isActiveChangeTracking() && isTrackableChange) {
                                        // Expanding operation for change tracking with old explicit attributes
                                        oldAttrs = changeTrack.getOldNodeAttributes(element);
                                    }

                                    // by setting fill- or line-attributes on a group
                                    if (isGroupDrawingFrame(element) && (_.isObject(attributes.fill) || _.isObject(attributes.line))) {
                                        // generate operations for all children -> but using filter to avoid setting attributes to drawings, that do not support them
                                        _.each(this.#filterDrawingNodes(getAllGroupDrawingChildren(element), attributes), ele => {
                                            generateSetAttributeOperation(getOxoPosition(activeRootNode, ele, 0));
                                        });
                                    // otherwise
                                    } else {
                                        // set attributes on the group
                                        generateSetAttributeOperation(localPosition, undefined, oldAttrs);
                                    }
                                    if (selection.isCropMode()) { this.exitCropMode(); }
                                    $(element).closest(PARAGRAPH_NODE_SELECTOR).removeData('lineBreaksData'); //we changed paragraph layout, cached line breaks data needs to be invalidated

                                }
                            });

                            break;

                        default:
                            globalLogger.error('Editor.setAttributes(): missing implementation for family "' + family + '"');
                    }
                };

                /**
                 * Helper function to apply the generated operations asynchronously.
                 *
                 * @returns {jQuery.Promise}
                 *  A promise that will be resolved when the operations have been applied.
                 */
                const doSetAttributesAsync = () => {

                    operationsPromise = this.applyTextOperationsAsync(generator, null, { showProgress: false, leaveOnSuccess: true, progressStart: operationRatio });

                    // restore the original selection after successful apply of operations
                    operationsPromise.done(() => { this.getSelection().restoreBrowserSelection(); });

                    return operationsPromise;
                };

                /**
                 * In ODP it is not allowed to convert an implicit paragraph into a real paragraph because of attributes.
                 * In this case an empty paragraph would be created, that make the surrounding drawing invisible after download.
                 * Instead all attributes must be collected in preselected attributes or at the drawings itself.
                 *
                 * @returns {Booelan}
                 *  Whether the required attribute handling was completely done inside this function. This is the case
                 *  for cursor setting inside implicit paragraphs (no operations required) or for selected drawing(s).
                 */
                const handleODFSlideMode = () => {

                    // whether the drawing was already handled
                    let allDrawingsHandled = false;
                    // the number of handled drawings
                    let drawingCounter = 0;

                    isODFSlideMode = true; // implicit paragraphs are handled without any operation (ODP)

                    if (family === 'paragraph' || family === 'character') {

                        drawings = selection.getSelectedDrawings();

                        if (drawings && drawings.length > 0) {

                            // iterating over all selected drawings -> assigning attributes to drawings, not to paragraphs or spans
                            // -> avoiding inserting additional paragraphs
                            _.each(drawings, oneDrawing => {
                                if (isDrawingWithOnlyImplicitParagraph(oneDrawing)) {
                                    // setting the attributes as explicit attributes to implicit paragraphs
                                    this.paragraphStyles.setElementAttributes($(oneDrawing).find('div.p'), attributes);
                                    drawingCounter++;
                                }
                            });

                            allDrawingsHandled = (drawings.length === drawingCounter);
                        }
                    }

                    return allDrawingsHandled;
                };

                // add all attributes to be cleared
                if (getBooleanOption(options, 'clear', false)) {
                    if (family !== 'table') {  // special behaviour for tables follows below
                        attributes = styleSheets.extendAttrSet(styleSheets.buildNullAttributeSet(), attributes);
                    }
                }

                // nothig to do if no attributes will be changed
                if (_.isEmpty(attributes)) { return; }

                // avoid to insert empty paragraphs in empty drawings in ODP
                if (this.#docApp.isODF() && this.useSlideMode() && handleODFSlideMode()) { return; }  // no further operations, if handled inside 'handleODFSlideMode'

                // register pending style sheet via 'insertStyleSheet' operation
                if (_.isString(attributes.styleId) && styleSheets.isDirty(attributes.styleId)) {
                    this.generateInsertStyleOp(generator, family, attributes.styleId);
                }

                // on Android devices the user might have modified an existing selection range before setting the new attribute
                if (_.browser.Android) { selection.updateSelectionAfterBrowserSelection(); }

                // Special handling for presentation in master/layout slides -> selecting only complete paragraphs
                if (this.#docApp.isPresentationApp() && this.getActiveTarget() && _.isFunction(this.isLayoutOrMasterId) && this.isLayoutOrMasterId(this.getActiveTarget())) {
                    isMasterLayoutSelection = true;
                    this.setMasterLayoutSelection();
                }

                // checking selection size -> make array with splitted selection [0,100], [101, 200], ... (39061)
                splittedSelectionRange = splitLargeSelection(this.getCurrentRootNode(), selection.getStartPosition(), selection.getEndPosition(), this.getMaxTopLevelNodes());

                if (splittedSelectionRange && splittedSelectionRange.length > 1) {

                    // make sure that only one asynchronous call is processed at the same time
                    if (this.checkSetClipboardPasteInProgress()) { return $.when(); }

                    // blocking keyboard input during generation and applying of operations
                    this.setBlockKeyboardEvent(true);

                    // creating a snapshot
                    snapshot = new Snapshot(this);

                    // show a message with cancel button
                    // -> immediately grabbing the focus, after calling enterBusy. This guarantees, that the
                    // keyboard blocker works. Otherwise the keyboard events will not be catched by the page.
                    this.#docApp.docView.enterBusy({
                        cancelHandler: () => {
                            if (operationsPromise && operationsPromise.abort) { // order is important, the latter has to win
                                // restoring the old document state
                                snapshot.apply();
                                // calling abort function for operation promise
                                this.#docApp.enterBlockOperationsMode(() => { operationsPromise.abort(); });
                            } else if (operationGeneratorPromise && operationGeneratorPromise.abort) {
                                operationGeneratorPromise.abort();  // no undo of changes required
                            }
                        },
                        immediate: true,
                        warningLabel: gt('Sorry, formatting content will take some time.')
                    });

                    this.#docApp.docView.grabFocus();

                    // generate operations asynchronously
                    operationGeneratorPromise = this.iterateArraySliced(splittedSelectionRange, oneRange => {
                        // calling function to generate operations synchronously with reduced selection range
                        generateAllSetAttributeOperations(oneRange);
                    });

                    // add progress handling
                    operationGeneratorPromise.progress(progress => {
                        // update the progress bar according to progress of the operations promise
                        this.#docApp.getView().updateBusyProgress(operationRatio * progress);
                    });

                    setAttributesPromise = operationGeneratorPromise.then(doSetAttributesAsync).always(() => {
                        if (snapshot) { snapshot.destroy(); }
                        this.leaveAsyncBusy();
                    });

                } else {
                    // synchronous handling for small selections

                    // calling function to generate operations synchronously
                    generateAllSetAttributeOperations();

                    // apply all collected operations
                    this.applyOperations(generator);

                    // applying operations synchronously
                    setAttributesPromise = $.when();

                    // a refresh of the Android selection is required (DOCS-4830)
                    if (changedImplicitParagraph && _.browser.Android) { selection.updateSelectionAfterBrowserSelection(); }
                }

                // in ODF format, the filter needs to be informed about changes of table heights
                if (this.#docApp.isODF() && this.useSlideMode() && selection.isAdditionalTextframeSelection() && isTableDrawingFrame(selection.getSelectedTextFrameDrawing())) {
                    this.trigger('drawingHeight:update', selection.getSelectedTextFrameDrawing());
                }

                return setAttributesPromise;

            }, this); // enterUndoGroup();
        }

        /**
         * Changes a single attribute of the specified attribute family in the
         * current selection.
         *
         * @param {String} family
         *  The name of the attribute family containing the specified
         *  attribute.
         *
         * @param {String} name
         *  the key of the attribute in the assigned family
         *
         * @param {Object} value
         *  the new attributes
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.clear=false]
         *      If set to true, all existing explicit attributes will be
         *      removed from the selection, before applying the new attributes.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved after all operations have been
         *  generated and applied successfully.
         */
        setAttribute(family, name, value, options) {
            return this.setAttributes(family, { [family]: { [name]: value } }, options);
        }

        /**
         * Returns whether the format-painter is currently active or not
         *
         * @returns {Boolean}
         *  True or false, depends on format-painter-state
         */
        isFormatPainterActive() {
            return this.#formatPainterActive;
        }

        /**
         * Sets the state of the format-painter and handles some implicit
         * reset-work, when the format-painter will be disabled
         *
         * @param {Boolean} active
         *  Activate or deactivate the format-painter.
         */
        setFormatPainterState(active) {
            // set formatpainter-state
            this.#formatPainterActive = active;

            // handle some implicit specifications
            if (!active) {
                // reset formatpainter-attributes
                this.#formatPainterAttrs = {};

                // update the toolbar-icon
                this.#docApp.getController().update();
            }

            // setting cursor with class at content root node (54851)
            this.#docApp.getView().getContentRootNode().toggleClass('active-formatpainter', active);
        }

        /**
         * Handles the format-painting.
         *
         * @param  {Boolean} activate
         *  Activate or deactivate the format-painter.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.reset=false]
         *      If set to true, the active format-painter should simply
         *      turn off, without paste any style.
         */
        handleFormatPainter(activate, options) {

            const getAllowedAttrs = options => {

                const getFullAttributes = () => {

                    const hasStyleId = () => {
                        return (this.#formatPainterAttrs.explicit.paragraph && !_.isEmpty(this.#formatPainterAttrs.explicit.paragraph.styleId));
                    };

                    const merged = this.#formatPainterAttrs.merged[source];
                    const explicit = this.#formatPainterAttrs.explicit[source];
                    const attrs = (hasStyleId()) ? explicit : merged;

                    return (getBooleanOption(options, 'forceMerged', false)) ? merged : attrs;
                };

                const getFilteredAttributes = (allowed, attrs) => {
                    if (_.isUndefined(attrs) || _.isNull(attrs)) { return; }
                    const returnArr = {};
                    if (allowed) { _.each(allowed, key => { if (!_.isUndefined(attrs[key])) { returnArr[key] = attrs[key]; } }); }
                    return returnArr;
                };

                const source = getStringOption(options, 'source');
                const target = getStringOption(options, 'target', source);
                let allowed = formatPainterAllowList[target];
                const attrs = {};

                // console.log('    source: ', source);
                // console.log('    target: ', target);

                if (_.isEmpty(allowed)) { return {}; }

                if (target === 'default') {
                    allowed = formatPainterAllowList[source];
                }

                _.each(getFullAttributes(), (object, group) => {
                    // step over, when character-/ or fill-attributes should copied for twoPointShapes
                    if (twoPointShape && (group === 'character' || group === 'fill')) { return; }

                    let listedAttrs = null;

                    if (group === 'styleId' && !_.isEmpty(object)) {
                        listedAttrs = { group: object };

                    } else {
                        listedAttrs = getFilteredAttributes(allowed[group], object);
                    }

                    if (!_.isEmpty(listedAttrs)) {
                        attrs[group] = listedAttrs;
                    }
                });

                return _.copy(attrs, true); // using deep clone to avoid problem with shortened operations
            };

            const filterAttributes = (allowed, attrs, options) => {
                if (_.isNull(attrs)) { return {}; }

                const returnArr = {};
                const type = getStringOption(options, 'type', 'drawing');

                _.each(allowed, (array, key) => {
                    if (
                        // twoPoint-drawings (or -shapes) should not copy fill-styles
                        !(twoPointShape && key === 'fill')
                        &&
                        (
                            (type === 'drawing' && key !== 'character' && key !== 'paragraph')
                            ||
                            key === type
                        )
                    ) {
                        if (!_.isUndefined(attrs[key])) {
                            returnArr[key] = {};
                            _.each(attrs[key], (v, k) => { if (_.contains(array, k)) { returnArr[key][k] = v; } });
                        }
                    }
                });

                return returnArr;
            };

            const getMergedDrawingAttributes = drawAttrs => {
                // remove character and paragraph-attributes from drawing-attributes
                delete drawAttrs.character;
                delete drawAttrs.paragraph;
                return drawAttrs;
            };

            const isNoTextElement = () => { return (drawingType === 'image' || twoPointShape || drawingType === 'table'); };

            const extendChangeTrackingAttributes = (attrs, node) => {
                if (changeTrack.isActiveChangeTracking()) {
                    // Expanding operation for change tracking with old explicit attributes
                    oldAttrs      = changeTrack.getOldNodeAttributes(node);
                    oldAttrs      = _.extend(oldAttrs, changeTrack.getChangeTrackInfo());
                    attrs.changes = { modified: oldAttrs };
                }
                return attrs;
            };

            const generateAttributeOperation = (generator, options) => {
                const settings = {
                    start: _.copy(getArrayOption(options, 'start')),
                    attrs: getObjectOption(options, 'attrs')
                };

                if (getArrayOption(options, 'end')) {
                    settings.end = _.copy(getArrayOption(options, 'end'));
                }

                generator.generateOperation(SET_ATTRIBUTES, settings);
            };

            const filterDefaultAttributes = () => {

                const defaultAttrs = this.defAttrPool.getDefaultValueSet(['character', 'paragraph', 'drawing']);

                _.each(defaultAttrs, (defaultSet, family) => {
                    let mergedSet = null;
                    if (family === 'character' && mergedChar) { mergedSet = mergedChar.character; }
                    if (family === 'paragraph' && mergedPara) { mergedSet = mergedPara.paragraph; }
                    if (family === 'drawing' && mergedDraw) { mergedSet = mergedDraw; }

                    if (mergedSet) {
                        _.each(defaultSet, (value, key) => {
                            if (_.isObject(value)) {
                                if (_.isEqual(value, mergedSet[key])) { delete mergedSet[key]; }
                            } else {
                                if (value === mergedSet[key]) { delete mergedSet[key]; }
                            }
                        });
                    }
                });
            };

            const rootNode = this.getCurrentRootNode();

            const selection = this.getSelection();
            const hasRange = selection.hasRange();

            const reset = getBooleanOption(options, 'reset', false);
            const changeTrack = this.getChangeTrack();
            let oldAttrs = null;

            let drawingPos = null;
            let startParaPos = null;
            let startTextPos = null;

            let twoPointShape = false;
            let drawingNode = null;
            let drawingType = null;

            let mergedChar = null;
            let mergedPara = null;
            let mergedDraw = null;
            let explicitChar = null;
            let explicitPara = null;
            let explicitDraw = null;

            // get all needed oxo-position
            if (selection.isDrawingSelection()) {
                drawingPos          = selection.getStartPosition();
                startParaPos        = _.clone(drawingPos);
                startParaPos.push(0);
                startTextPos        = _.clone(startParaPos);
                startTextPos.push(0);

                drawingNode         = getDrawingElement(rootNode, drawingPos);
                drawingType         = getDrawingType(drawingNode);
                twoPointShape       = isTwoPointShape(drawingNode);

            } else {
                startTextPos        = selection.getStartPosition();
                startParaPos        = startTextPos.slice(0, startTextPos.length - 1);
                drawingPos          = startParaPos.slice(0, startParaPos.length - 1);
            }

            // get corresponding nodes to oxo-position
            const textSpanNode = (!twoPointShape && drawingType !== 'image' && drawingType !== 'table' && drawingType !== 'group') ? getSelectedElement(rootNode, startTextPos, 'span') : null;
            const paragraphNode = (!twoPointShape && drawingType !== 'image' && drawingType !== 'table' && drawingType !== 'group') ? getParagraphElement(rootNode, startParaPos) : null;

            // formatpainter is active (ready to paste saved styles)
            if (this.#formatPainterActive && !activate && !reset) {

                const generator = this.createOperationGenerator();
                let start = null;
                let end = null;

                this.getUndoManager().enterUndoGroup(() => {

                    let charOpAttrs = null;
                    let paraOpAttrs = null;
                    let styleDrawing = true;

                    if (!twoPointShape && drawingType !== 'image') {
                        this.doCheckImplicitParagraph(startTextPos); // 54885
                    }

                    if (hasRange) {
                        selection.iterateContentNodes((paragraphNode, position, startOffset, endOffset) => {
                            charOpAttrs = getAllowedAttrs({ source: 'character', target: drawingType, forceMerged: true });
                            start       = _.clone(position);
                            end         = _.clone(position);

                            if (!isEmptyParagraph(paragraphNode) && !isEmptyListParagraph(paragraphNode) && !isTextFrameTemplateTextParagraph(paragraphNode)) {
                                start.push(startOffset || 0);
                                end.push(endOffset || (getParagraphNodeLength(paragraphNode) - 1)); // length of paragraph must be reduced by 1 to be usable in operations
                            }

                            if (!_.isEmpty(charOpAttrs)) {
                                extendChangeTrackingAttributes(charOpAttrs, paragraphNode);

                                generateAttributeOperation(generator, {
                                    start:  _.clone(start),
                                    end:    _.clone(end),
                                    attrs:  charOpAttrs
                                });
                            }

                            paraOpAttrs = getAllowedAttrs({ source: 'paragraph', target: drawingType });

                            if (!_.isEmpty(paraOpAttrs)) {
                                extendChangeTrackingAttributes(paraOpAttrs, paragraphNode);
                                let formatListLevel, formatListStyleId;
                                paraOpAttrs.paragraph = paraOpAttrs.paragraph || {};

                                if (this.#formatPainterAttrs.explicit.paragraph.paragraph) {
                                    formatListLevel = this.#formatPainterAttrs.explicit.paragraph.paragraph.listLevel;
                                    formatListStyleId = this.#formatPainterAttrs.explicit.paragraph.paragraph.listStyleId;
                                }
                                _.extend(paraOpAttrs, {
                                    styleId: this.#formatPainterAttrs.explicit.paragraph.styleId || null,
                                    paragraph: {
                                        listStyleId: formatListStyleId || null,
                                        listStyleLevel: formatListLevel || -1
                                    }
                                });

                                generateAttributeOperation(generator, {
                                    start: _.clone(position),
                                    attrs: paraOpAttrs
                                });
                            }

                        }, null, { startTextPos: selection.getStartPosition(), endPos: selection.getEndPosition() });

                        // do not apply drawing-styles
                        if (selection.isAdditionalTextframeSelection()) { styleDrawing = false; }

                    // single click into a word
                    } else {
                        const wordOxoPos = (hasRange) ? null : getWordBoundaries(rootNode, startTextPos);

                        // at this point wordOxoPos can be null or an array of numbers or an array with two logical positions (task 58492)

                        // equal, if there was no word clicked
                        if (_.isArray(wordOxoPos) && _.isArray(wordOxoPos[0]) && _.isArray(wordOxoPos[1]) && !_.isEqual(wordOxoPos[0], wordOxoPos[1])) {
                            const tmpStartPos = (hasRange) ? startTextPos : wordOxoPos[0];
                            const tmpEndPos = (hasRange) ? selection.getEndPosition() : wordOxoPos[1];

                            charOpAttrs = getAllowedAttrs({ source: 'character', target: drawingType });
                            start = tmpStartPos;
                            end = decreaseLastIndex(tmpEndPos);

                            if (!_.isEmpty(charOpAttrs)) {
                                extendChangeTrackingAttributes(charOpAttrs, textSpanNode);

                                generateAttributeOperation(generator, {
                                    start: _.clone(start),
                                    end: _.clone(end),
                                    attrs: charOpAttrs
                                });
                            }
                        }

                        paraOpAttrs = getAllowedAttrs({ source: 'paragraph', target: drawingType });

                        if (!_.isEmpty(paraOpAttrs)) {
                            extendChangeTrackingAttributes(paraOpAttrs, paragraphNode);

                            if (this.#formatPainterAttrs.explicit.paragraph.styleId) {
                                paraOpAttrs.styleId = this.#formatPainterAttrs.explicit.paragraph.styleId;
                            }

                            generateAttributeOperation(generator, {
                                start: _.clone(startParaPos),
                                attrs: paraOpAttrs
                            });
                        }
                    }

                    const drawOpAttrs = getAllowedAttrs({ source: 'drawing', target: drawingType });

                    if (!_.isEmpty(drawOpAttrs) && styleDrawing) {
                        extendChangeTrackingAttributes(drawOpAttrs, drawingNode);

                        generateAttributeOperation(generator, {
                            start: _.copy(drawingPos),
                            attrs: drawOpAttrs
                        });
                    }

                    // apply all collected operations (inside undo group)
                    this.applyOperations(generator);
                });

                this.setFormatPainterState(activate);

            // Activate formatpainter and save current styles!
            } else if (!this.#formatPainterActive && activate) {
                mergedChar   = (isNoTextElement())     ? null : this.characterStyles.getElementAttributes(textSpanNode);
                mergedPara   = (isNoTextElement())     ? null : this.paragraphStyles.getElementAttributes(paragraphNode);
                mergedDraw   = (_.isNull(drawingType)) ? null : getMergedDrawingAttributes(this.drawingStyles.getElementAttributes(drawingNode));

                explicitChar = (isNoTextElement())     ? null : getExplicitAttributeSet(textSpanNode);
                explicitPara = (isNoTextElement())     ? null : getExplicitAttributeSet(paragraphNode);
                explicitDraw = (_.isNull(drawingType)) ? null : getExplicitAttributeSet(drawingNode);

                // removing those attributes from the merged attributes that are anyhow the default attributes (64517)
                filterDefaultAttributes();

                // special case for ODF: inherit attributes
                if (this.#docApp.isODF()) {
                    // merge character-attributes of all parent elements
                    if (explicitPara && explicitPara.character) {
                        explicitChar = explicitChar || {};
                        explicitChar.character = _.extend(explicitPara.character, explicitChar.character);
                        delete explicitPara.character;
                    }
                    if (explicitDraw && explicitDraw.character) {
                        explicitChar = explicitChar || {};
                        explicitChar.character = _.extend(explicitDraw.character, explicitChar.character);
                        delete explicitDraw.character;
                    }

                    // merge paragraph-attributes of all parent elements
                    if (explicitDraw && explicitDraw.paragraph) {
                        explicitChar = explicitChar || {};
                        explicitChar.paragraph = _.extend(explicitDraw.paragraph, explicitChar.paragraph);
                        delete explicitDraw.paragraph;
                    }
                }

                this.#formatPainterAttrs = {
                    explicit: {
                        character:  explicitChar,
                        paragraph:  explicitPara,
                        drawing:    explicitDraw
                    },
                    merged: {
                        character:  filterAttributes((drawingType) ? formatPainterAllowList[drawingType] : formatPainterAllowList.character, mergedChar, { type: 'character' }),
                        paragraph:  filterAttributes((drawingType) ? formatPainterAllowList[drawingType] : formatPainterAllowList.paragraph, mergedPara, { type: 'paragraph' }),
                        drawing:    filterAttributes(formatPainterAllowList[drawingType], mergedDraw)
                    }
                };

                this.setFormatPainterState(activate);

                // close the change track popup, if format painter is active
                this.#docApp.docView.hideChangeTrackPopup();

            } else if (reset) {
                this.setFormatPainterState(false);
            }
        }

        getDefaultMarginBottom() {
            return this.defAttrPool.getDefaultValue('paragraph', 'marginBottom') || 352;
        }

        /**
         * Returns a multiplier value for the spacing represented by the passed
         * paragraph attributes, as used in GUI controls.
         *
         * @param {Object} paraAttrs
         *  A map with paragraph attributes.
         *
         * @returns {Number|Null}
         *  A multiplier value for the spacing represented by the passed
         *  paragraph attributes; or null, if no explicit spacing could be
         *  determined.
         */
        getParagraphSpacing(paraAttrs) {
            const marginBottom = paraAttrs.marginBottom;
            if (typeof marginBottom !== 'number') { return null; }
            return Math.round(marginBottom / this.getDefaultMarginBottom());
        }

        /**
         * Changes the spacing between the selected paragraphs.
         *
         * @param {Number} multiplier
         *  A multiplier used in GUI controls that specifies the count of
         *  default bottom margins defined for paragraphs.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved after all operations have been
         *  generated and applied successfully.
         */
        setParagraphSpacing(multiplier) {
            const marginBottom = multiplier * this.getDefaultMarginBottom();
            // bug 46249: margin top needs to be considered when 'Paragraph spacing' is set to none in our GUI
            if (this.useSlideMode()) {
                return this.setAttributes('paragraph', { paragraph: { spacingBefore: 0, spacingAfter: { value: marginBottom, type: 'fixed' } } });
            }
            return this.setAttributes('paragraph', { paragraph: { marginTop: 0, marginBottom } });
        }

        /**
         * Changes multiple attributes of the specified attribute family for
         * the passed table node. Different from setAttributes in a way that uses
         * passed node instead of selected node with selection.
         *
         * @param {Node|jQuery} tableNode
         *  Table node to which attributes are applied
         *
         * @param {String} family
         *  The name of the attribute family containing the passed attributes.
         *
         * @param {Object} attributes
         *  A map of attribute value maps (name/value pairs), keyed by
         *  attribute families.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.clear=false]
         *      If set to true, all existing explicit attributes will be
         *      removed from the selection while applying the new attributes.
         *  @param {Boolean} [options.onlyVisibleBorders=false]
         *      Setting border width directly at cells -> overwriting values of table style.
         *  @param {Boolean} [options.cellSpecificTableAttribute=false]
         *      Setting border flags directly at cells -> overwriting values of table style.
         *
         */
        setAttributesToPassedTableNode(tableNode, family, attributes, options) {

            // the undo manager
            const undoManager = this.getUndoManager();

            // Create an undo group that collects all undo operations generated
            // in the local setAttributes() method (it calls itself recursively
            // with smaller parts of the current selection).
            undoManager.enterUndoGroup(() => {

                // logical position
                let localPosition = null;
                // operations generator
                const generator = this.createOperationGenerator();
                // whether after assigning cell attributes, it is still necessary to assign table attributes
                let createTableOperation = true;
                // the page node
                const editdiv = this.getNode();
                // the old attributes, required for change tracking
                let oldAttrs = null;

                // generates a 'setAttributes' operation with the correct attributes
                const generateSetAttributeOperation = (startPosition, endPosition, oldAttrs) => {

                    // the options for the operation
                    const operationOptions = { start: _.copy(startPosition), attrs: _.clone(attributes) };

                    // adding the old attributes, author and date for change tracking
                    if (oldAttrs) {
                        oldAttrs = _.extend(oldAttrs, this.getChangeTrack().getChangeTrackInfo());
                        operationOptions.attrs.changes = { modified: oldAttrs };
                    }

                    // add end position if specified
                    if (_.isArray(endPosition)) {
                        operationOptions.end = _.copy(endPosition);
                    }

                    // generate the 'setAttributes' operation
                    generator.generateOperation(SET_ATTRIBUTES, operationOptions);
                };

                // nothig to do if no attributes will be changed
                if (_.isEmpty(attributes)) { return; }

                if (tableNode) {

                    localPosition = getOxoPosition(editdiv, tableNode, 0);  // the logical position of the table

                    if (getBooleanOption(options, 'clear', false)) {
                        // removing hard attributes at tables and cells, so that the table style will be visible
                        removeTableAttributes(this, editdiv, tableNode, localPosition, generator);
                    }

                    if (getBooleanOption(options, 'onlyVisibleBorders', false)) {
                        // setting border width directly at cells -> overwriting values of table style
                        setBorderWidthToVisibleCells(tableNode, attributes, this.tableCellStyles, editdiv, generator, this);
                        createTableOperation = false;
                    }

                    if (getBooleanOption(options, 'cellSpecificTableAttribute', false)) {
                        // setting border flags directly at cells -> overwriting values of table style
                        setBorderFlagsToCells(tableNode, attributes, TableStyles.getBorderStyleFromAttributes(this.getAttributes('cell').cell || {}), editdiv, generator, this);
                    }

                    // setting attributes to the table element
                    // -> also setting table attributes, if attributes are already assigned to table cells, to
                    // keep the getter functions simple and performant
                    if (createTableOperation) {
                        // Defining the oldAttrs, if change tracking is active
                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            // Expanding operation for change tracking with old explicit attributes
                            oldAttrs = this.getChangeTrack().getOldNodeAttributes(tableNode);
                        }
                        generateSetAttributeOperation(localPosition, undefined, oldAttrs);
                    }
                }

                // apply all collected operations
                this.applyOperations(generator);

            }, this); // end of enterUndoGroup
        }

        /**
         * Returns the values of all formatting attributes of the elements in
         * the current selection associated to the specified attribute family.
         *
         * @param {String} family
         *  The name of the attribute family used to select specific elements
         *  in the current selection:
         *  - 'character': all text spans (text portions, text components),
         *  - 'paragraph': all paragraph nodes,
         *  - 'table': all table nodes,
         *  - 'drawing': all drawing object nodes.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.groupOnly=false]
         *      Whether only the attributes of the drawing group shall be
         *      collected, not of the children.
         *  @param {Number} [options.maxIterations=-1]
         *      An optional number for limiting the maximum iteration count for
         *      nodes to be investigated. If the value is set to -1, this limit
         *      is ignored.
         *
         * @returns {Object}
         *  A map of attribute value maps (name/value pairs), keyed by
         *  attribute family.
         */
        getAttributes(family, options) {

            // the selection object
            const selection = this.getSelection();
            // whether the selection is a simple cursor
            const isCursor = selection.isTextCursor();
            // table or drawing element contained by the selection
            let element = null;
            // resulting merged attributes
            let mergedAttributes = null;
            // max iterations
            const maxIterations = getOption(options, 'maxIterations');
            // get only attributes of the group
            const groupOnly = getBooleanOption(options, 'groupOnly', false);

            // merges the passed element attributes into the resulting attributes
            const mergeElementAttributes = elementAttributes => {

                // whether any attribute is still unambiguous
                let hasNonNull = false;

                // merges the passed attribute value into the attributes map
                const mergeAttribute = (attributes, name, value) => {

                    if (!(name in attributes)) {
                        // initial iteration: store value
                        attributes[name] = value;
                    } else if (!_.isEqual(value, attributes[name])) {
                        // value differs from previous value: ambiguous state
                        attributes[name] = null;
                    }
                    hasNonNull = hasNonNull || !_.isNull(attributes[name]);
                };

                // initial iteration: store attributes and return
                if (!mergedAttributes) {
                    mergedAttributes = elementAttributes;
                    return;
                }

                // process all passed element attributes
                _(elementAttributes).each((attributeValues, subFamily) => {
                    if (subFamily === 'styleId') {
                        mergeAttribute(mergedAttributes, 'styleId', attributeValues);
                    } else {
                        const mergedAttributeValues = mergedAttributes[subFamily];
                        _(attributeValues).each((value, name) => {
                            mergeAttribute(mergedAttributeValues, name, value);
                        });
                    }
                });

                // stop iteration, if all attributes are ambiguous
                return hasNonNull ? undefined : BREAK;
            };

            switch (family) {

                case 'character':
                    selection.iterateNodes((node, pos, offset, length) => {
                        return iterateTextSpans(node, span => {

                            // ignoring leading whitespaces in ending text span of selection range (DOCS-1725)
                            if (offset === 0 & length > 0 && !isCursor && !_.isEqual(pos, selection.getStartPosition()) && _.isEqual(increaseLastIndex(pos, length), selection.getEndPosition()) && this.#onlyWhitespacesAtStart(span, length)) { return; }

                            // ignore empty text spans (they cannot be formatted via operations),
                            // but get formatting of an empty span selected by a text cursor
                            if (isCursor || (span.firstChild.nodeValue.length > 0) || this.#isEmptyTextFrameSelection(span)) {
                                return mergeElementAttributes(this.characterStyles.getElementAttributes(span));
                            }
                        });
                    }, null, { maxIterations });
                    if (isCursor && this.getPreselectedAttributes()) {
                        // add preselected attributes (text cursor selection cannot result in ambiguous attributes)
                        this.defAttrPool.extendAttrSet(mergedAttributes, this.getPreselectedAttributes());
                    }
                    break;

                case 'paragraph':
                    selection.iterateContentNodes(paragraph => {
                        return mergeElementAttributes(this.paragraphStyles.getElementAttributes(paragraph));
                    }, null, { maxIterations });
                    break;

                case 'cell':
                    selection.iterateTableCells(cell => {
                        return mergeElementAttributes(this.tableCellStyles.getElementAttributes(cell));
                    });
                    break;

                case 'table':
                    element = selection.getEnclosingTable();

                    if (!element && this.useSlideMode() && selection.isDrawingSelection() && isTableDrawingFrame(selection.getSelectedDrawing())) {
                        element = selection.getSelectedDrawing();
                    }

                    if (element) {
                        mergeElementAttributes(this.tableStyles.getElementAttributes(element));
                    }
                    break;

                case 'drawing':
                    _.each(selection.getSelectedDrawings(), oneDrawing => {
                        let drawingAttrs = null;
                        if (isGroupDrawingFrame(oneDrawing) && groupOnly !== true) {
                            _.each(getAllGroupDrawingChildren(oneDrawing), drawing => {
                                drawingAttrs = this.drawingStyles.getElementAttributes(drawing);
                                this.#filterDrawingAttrs(drawing, drawingAttrs); // filter for the drawing attributes
                                mergeElementAttributes(drawingAttrs);
                            });
                        } else {
                            drawingAttrs = this.drawingStyles.getElementAttributes(oneDrawing);
                            if (selection.isMultiSelection()) { this.#filterDrawingAttrs(oneDrawing, drawingAttrs); } // filter for the drawing attributes
                            mergeElementAttributes(drawingAttrs);
                        }
                    });

                    break;

                default:
                    globalLogger.error('Editor.getAttributes(): missing implementation for family "' + family + '"');
            }

            return mergedAttributes || {};
        }

        /**
         * Removes all hard-set attributes depending on the current selection.
         * A selection range clears only hard-set character attributes and without
         * a range the hard-set paragraph attributes are cleared.
         */
        resetAttributes() {

            // the undo manager
            const undoManager = this.getUndoManager();
            // the selection object
            const selection = this.getSelection();

            const clearParagraphAttributes = paragraph => {
                const paraAttributes = this.paragraphStyles.buildNullAttributeSet();
                delete paraAttributes.styleId;
                this.setAttributes('paragraph', paraAttributes);
                if ($(paragraph).data('origCharAttrs')) { $(paragraph).removeData('origCharAttrs'); } // no longer required
            };

            const clearCharacterAttributes = () => {
                const charAttributes = this.characterStyles.buildNullAttributeSet();
                // don't reset hyperlink attribute
                delete charAttributes.styleId;
                delete charAttributes.character.url;
                this.setAttributes('character', charAttributes);
            };

            // In OOXML the character attributes at paragraphs are implicitely transferred to the child nodes. For the roundtrip after a 'Clear formatting'
            // action (without selection range) it is necessary that an operation is generated, that removes all hard character attributes from the child
            // nodes of the paragraph, whose character attributes are removed (DOCS-1899).
            const clearAllCharacterAttributesInAllSpansInParagraph = paragraph => {

                const paragraphLength = getParagraphNodeLength(paragraph);

                if (paragraphLength === 0) { return; } // this step is not necessary for empty paragraphs

                const oldStartPosition = selection.getStartPosition();
                const oldEndPosition = selection.getEndPosition();
                const paraPos = getOxoPosition(this.getCurrentRootNode(), paragraph, 0);  // the logical position of the paragraph
                selection.setTextSelection(appendNewIndex(paraPos), appendNewIndex(paraPos, paragraphLength));

                clearCharacterAttributes();

                selection.setTextSelection(oldStartPosition, oldEndPosition); // restoring the selection
            };

            // In OOXML the character attributes at paragraphs are implicitely transferred to the child nodes. For the roundtrip after a 'Clear formatting'
            // action (with selection range) it is necessary to explicitely transfer the character attributes from the paragraph to the child nodes. Therefore
            // the character attributes must be removed from the paragraph with an operation and must be assigned to all children of the paragraph, if they
            // do not have a hard character attribute with a different value (DOCS-1899).
            const handleCharacterAttributesAtParagraphs = () => {

                // iterate the content nodes (paragraphs and tables) covered by the selection
                selection.iterateContentNodes((contentNode, position) => {

                    if ($(contentNode).data('origCharAttrs')) {

                        // removing character attributes explicitely from paragraph, so that the filter is informed about the transfer to the children
                        const paragraphOperation = { name: SET_ATTRIBUTES, start: _.clone(position), attrs: this.characterStyles.buildNullAttributeSet() };
                        this.extendPropertiesWithTarget(paragraphOperation, this.getActiveTarget());
                        this.applyOperations(paragraphOperation);

                        // setting character attributes explicitely at all pararaph children (but not overwriting existing hard attributes), so that the filter is informed about the transfer
                        const charAttrs = $(contentNode).data('origCharAttrs');
                        iterateParagraphChildNodes(contentNode, (node, nodeStart, nodeLength) => {
                            const newAttrs = _.copy(charAttrs, true);
                            const explicitAttributes = getExplicitAttributeSet(node);
                            // if the explicit attribute is set AND the value is different then the character attribute from the paragraph, it must not be overwritten
                            if (explicitAttributes && explicitAttributes.character) {
                                for (const key in explicitAttributes.character) {
                                    if (newAttrs[key] && !_.isEqual(newAttrs[key], explicitAttributes.character[key])) { delete newAttrs[key]; }
                                }
                            }
                            if (!_.isEmpty(newAttrs)) {
                                const characterOperation = { name: SET_ATTRIBUTES, start: appendNewIndex(position, nodeStart), end: appendNewIndex(position, nodeStart + nodeLength - 1), attrs: { character: newAttrs } };
                                this.extendPropertiesWithTarget(characterOperation, this.getActiveTarget());
                                this.applyOperations(characterOperation);
                            }
                        });

                        $(contentNode).removeData('origCharAttrs');
                    }

                }, this, { shortestPath: true });
            };

            undoManager.enterUndoGroup(() => {

                if (!selection.hasRange()) {

                    const start = selection.getStartPosition();
                    const end = selection.getEndPosition();
                    const activeRootNode = this.getCurrentRootNode();
                    const firstParagraph = getLastNodeFromPositionByNodeName(activeRootNode, start, PARAGRAPH_NODE_SELECTOR);
                    const lastParagraph = getLastNodeFromPositionByNodeName(activeRootNode, end, PARAGRAPH_NODE_SELECTOR);

                    clearParagraphAttributes(firstParagraph);

                    // OOXML: Also clearing the hard character attributes at all spans inside the paragraph (DOCS-1899)
                    if (!this.#docApp.isODF()) { clearAllCharacterAttributesInAllSpansInParagraph(firstParagraph); }

                    const prevPara = findPreviousNode(activeRootNode, firstParagraph, PARAGRAPH_NODE_SELECTOR);
                    if (prevPara) {
                        this.paragraphStyles.updateElementFormatting(prevPara);
                    }
                    const nextPara = findNextNode(activeRootNode, lastParagraph, PARAGRAPH_NODE_SELECTOR);
                    if (nextPara) {
                        this.paragraphStyles.updateElementFormatting(nextPara);
                    }
                    this.addPreselectedAttributes(this.characterStyles.buildNullAttributeSet());
                } else {
                    // OOXML: Clearing the character attributes at the paragraph and transfer them to all children,
                    //        if the children do not have different own hard character attributes (DOCS-1899)
                    if (!this.#docApp.isODF()) { handleCharacterAttributesAtParagraphs(); }

                    // clearing the character attributes in the selection
                    clearCharacterAttributes();
                }
            }, this);
        }

        /**
         * Getting the vertical text alignment of a currently selected text frame node.
         *
         * @returns {String}
         *  The vertical text alignment mode of the text inside a selected text frame node. If it cannot
         *  be determined, the default value 'top' is returned.
         */
        getVerticalAlignmentMode() {
            // the selection object
            const selection = this.getSelection();

            if (selection.isAnyTableDrawingSelection()) {
                const attr = this.getAttributes('cell').cell;
                if (!attr) { return 'top'; } // DOCS-1760
                return attr.alignVert === 'center' ? 'centered' : attr.alignVert;
            }
            // the selected text frame node (also finding text frames inside groups)
            const drawingFrame = selection.getAnyTextFrameDrawing({ forceTextFrame: true });
            // the text frame node inside the shape
            const textFrame = getTextFrameNode(drawingFrame);

            return (textFrame && textFrame.length > 0 && textFrame.attr(VERTICAL_ALIGNMENT_ATTRIBUTE)) || 'top';
        }

        /**
         * Setting the vertical text alignment mode inside a text frame.
         *
         * @param {String} state
         *  The vertical text alignment mode of the text inside a selected text frame node.
         */
        setVerticalAlignmentMode(state) {
            // the selection object
            const selection = this.getSelection();
            // the operations generator
            const generator = this.createOperationGenerator();
            // the options for the setAttributes operation
            const operationOptions = {};
            // a container for the logical positions of all affected drawings
            const allDrawings = selection.getAllDrawingsInSelection(isTextFrameShapeDrawingFrame);

            if (!allDrawings.length > 0) { return; }

            if (selection.isAnyTableDrawingSelection()) {
                this.setAttribute('cell', 'alignVert', state === 'centered' ? 'center' : state);
            } else {
                // collecting the attributes for the operation
                operationOptions.attrs =  {};
                operationOptions.attrs.shape = { anchor: state };

                // iterating over all drawings
                _.each(allDrawings, oneDrawing => {
                    operationOptions.start = getOxoPosition(this.getCurrentRootNode(), oneDrawing, 0);
                    // generate the 'setAttributes' operation
                    generator.generateOperation(SET_ATTRIBUTES, operationOptions);
                });

                // apply all collected operations
                this.applyOperations(generator);
            }
        }

        // operation handler --------------------------------------------------

        /**
         * The handler for the setAttributes operation.
         *
         * Info:undo/redo generation is done inside 'implSetAttributes'.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether the operation was triggered by an external client.
         *
         * @returns {Boolean}
         *  Whether the setting of attributes was successful.
         */
        setAttributesHandler(operation, external) {
            return this.#implSetAttributes(operation.start, operation.end, operation.attrs, operation.target, external, operation.noUndo);
        }

        /**
         * Collecting the hard character attributes for a new inserted node in a paragraph from the previous
         * inline node (that is not a placeholder node).
         *
         * If the previous node has a child text span (for example a tabulator), the hard character attributes are
         * received from the text span.
         *
         * @param {HTMLElement|jQuery} insertedNode
         *  The DOM element that was inserted into a paragraph and that might inherit additional character attributes
         *  from its previous inline node.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.fromNextSibling=false]
         *      If set to true, the next sibling node is searched for character attributes. This happens only for empty
         *      text spans at the beginning of a paragraph. In the default case ("false"), the previous sibling is
         *      searched for character attributes.
         *
         * @returns {Object|null}
         *  An object with the inherited character attributes. Or null, if there are no inherited character attributes.
         */
        getInheritInlineCharacterAttributes(insertedNode, options) {

            let node = getDomNode(insertedNode);
            let previousInlineAttrs = null;
            let foundInlineNode = false;
            let characterAttrsNode = null;
            const iteratorFunc = getBooleanOption(options, 'fromNextSibling', false) ? 'nextSibling' : 'previousSibling';

            while (node[iteratorFunc] && !foundInlineNode) {
                node = node[iteratorFunc];
                // taking care of all inline nodes, but not the placeholder nodes for comments and drawings
                // -> also no inheritance of character attributes from previous or following drawing nodes, but the text span next to the drawing
                if ((isTextSpan(node) || isInlineComponentNode(node)) && !isSubstitutionPlaceHolderNode(node) && !isDrawingFrame(node)) {

                    // not using an empty span, that has a previous (or following) node
                    if (isEmptySpan(node) && node[iteratorFunc]) { continue; }

                    // a node for inheriting character attributes is found
                    foundInlineNode = true;
                    characterAttrsNode = node.firstChild && isSpan(node.firstChild) ? node.firstChild : node;
                    previousInlineAttrs = getExplicitAttributes(characterAttrsNode, 'character');
                }
            }

            return previousInlineAttrs;
        }

        /**
         * Transfer hard character attributes to a new inserted node in a paragraph. Every node inherits the
         * character attributes from its previous inline node (that is not a placeholder node).
         *
         * If the node has a child text span (for example a tabulator), the hard character attributes are
         * saved at the text span.
         *
         * @param {HTMLElement|jQuery} insertedNode
         *  The DOM element that was inserted into a paragraph and that might inherit additional character attributes
         *  from its previous inline node.
         *
         * @param {Object} [operationCharAttrs]
         *  The character attributes sent within the operation.
         */
        inheritInlineCharacterAttributes(insertedNode, operationCharAttrs) {

            const inheritedCharacterAttrs = this.getInheritInlineCharacterAttributes(insertedNode);
            let characterAttrsNode = null;
            let node = null;

            if (inheritedCharacterAttrs) {
                node = getDomNode(insertedNode);
                characterAttrsNode = (node.firstChild && isSpan(node.firstChild)) ? node.firstChild : node;
                // never overwrite attributes that were sent with the operation (DOCS-4796)
                if (operationCharAttrs) { for (const key in operationCharAttrs) { delete inheritedCharacterAttrs[key]; } }
                if (!is.empty(inheritedCharacterAttrs)) { this.characterStyles.setElementAttributes(characterAttrsNode, { character: inheritedCharacterAttrs }); }
            }
        }

        /**
         * Transfer hard character attributes to a previous node that is an empty text span and that has no previous
         * inline node sibling. Only this way it is possible to transfer the hard character attributes to the leading
         * empty span of a paragraph, if the paragraph for example starts with a bookmark.
         *
         * @param {HTMLElement|jQuery} insertedNode
         *  The DOM element that was inserted into a paragraph with hard character attributes.
         *
         * @param {Object} charAttrs
         *  The hard character attributes of the inserted DOM node.
         */
        checkTransferOfInlineCharacterAttributes(insertedNode, charAttrs) {

            const node = getDomNode(insertedNode);
            let previousNode = null;

            if (node.previousSibling) {
                previousNode = node.previousSibling;
                if (isEmptySpan(previousNode)) {
                    if (!previousNode.previousSibling) { // TODO: Previous placeholder nodes might be allowed
                        this.characterStyles.setElementAttributes(previousNode, { character: charAttrs });
                    }
                }
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Registering filter functions to avoid that attributes are assigned to drawings
         * that they do not support. This can happen in the case of multi drawing selections.
         * Not in every case the button to set an attribute is disabled, if the current
         * selection contains more than one drawing. For example the 'fillColor' can be set,
         * if drawings of type 'connector' are part of the multi drawing selection. In this
         * case the drawing selection must be decreased, before the operations are generated.
         *
         * Addtionally these filter are used to reduce the specified attributes in the 'getter'
         * function. In a multi selection or a group selection it can happen, that a line and
         * a circle with yellow background are selected. In this case, the background color of
         * the merged attributes shall be yellow, because the line does not support this family.
         * In this case it is necessary to remove the fill-family from the attribute set.
         *
         * Info: This function is called during the initialization phase. Additional drawing
         *       filters can be specified within this function.
         *
         * Info: Every filter gets as parameter the jQuerified drawing node and the attributes
         *       object as it is used as parameter for the this.setAttributes function.
         *       If the filter function returns 'false', the drawing will NOT get the specified
         *       attributes. If it returns 'true', it is possible to generate an operation
         *       for the specified drawing and the specified attributes.
         *       Optionally the filter can get the 'deleteAttrs' flag. This is required for the
         *       getter function, that collects the attributes for all selected drawings. This can
         *       be used to remove the fill attributes from drawings, that do not support this.
         */
        #registerDrawingFilter() {

            // shapes, charts, images and groups support fill color
            const fillColorFilter = (drawing, attributes, deleteAttrs) => {
                if (attributes.fill && !FILL_DRAWING_TYPES[getDrawingType(drawing)]) {
                    if (deleteAttrs) { attributes.fill = {}; } // empty object required for merging of attributes
                    return false;
                }
                return true;
            };

            // shapes, charts, images, connectors and groups support line color
            const lineColorFilter = (drawing, attributes, deleteAttrs) => {
                if (attributes.line && !LINE_DRAWING_TYPES[getDrawingType(drawing)]) {
                    if (deleteAttrs) { attributes.line = {}; } // empty object required for merging of attributes
                    return false;
                }
                return true;
            };

            // further filters can be specified here ...

            // adding the filter to the global filter list
            this.#drawingFilterList.push(fillColorFilter);
            this.#drawingFilterList.push(lineColorFilter);
        }

        /**
         * Returns the family of the attributes supported by the passed DOM
         * element.
         *
         * @param {HTMLElement|jQuery} element
         *  The DOM element whose associated attribute family will be returned.
         *  If this object is a jQuery collection, returns its first node.
         */
        #resolveElementFamily(element) {

            // the element, as jQuery object
            const $element = $(element);
            // the resulting style family
            let family = null;

            if (isDrawingFrame($element)) {
                family = 'drawing'; // selecting drawing first (necessary after 39312)
            } else if (isPartOfParagraph($element)) {
                family = 'character';
            } else if (isSlideNode($element)) {
                family = 'slide';
            } else if (isParagraphNode($element)) {
                family = 'paragraph';
            } else if (isTableNode($element)) {
                family = 'table';
            } else if ($element.is('tr')) {
                family = 'row';
            } else if ($element.is('td')) {
                family = 'cell';
            } else {
                globalLogger.warn('Editor.#resolveElementFamily(): unsupported element');
            }

            return family;
        }

        /**
         * Returning the list style id from specified paragraph attributes. The list
         * style can be set directly or via a style id at the paragraph.
         *
         * @param {Object} [attributes]
         *  An optional map of attribute maps (name/value pairs), keyed by attribute.
         *  It this parameter is defined, the parameter 'paragraph' can be omitted.
         *
         * @returns {String|null}
         *  The list style id or null, if no list style defined in specified attributes.
         */
        #getListStyleIdFromParaAttrs(attributes) {

            // shortcut, not handling style attributes
            if (attributes && attributes.paragraph && attributes.paragraph.listStyleId) { return attributes.paragraph.listStyleId; }

            // checking paragraph style
            if (attributes && attributes.styleId && this.isParagraphStyleWithListStyle(attributes.styleId)) { return this.#getListStyleInfoFromStyleId(attributes.styleId, 'listStyleId'); }

            return null;
        }

        /**
         * Returns the value of a specified paragraph attribute of a given
         * paragraph style.
         *
         * @param {String} styleId
         *  The paragraph style id, that will be checked checked.
         *
         * @param {String} paraAttr
         *  The attribute of the paragraph attributes of the style, whose value
         *  shall be returned.
         *
         * @returns {String}
         *  The value of the specified paragraph attribute of the specified style.
         */
        #getListStyleInfoFromStyleId(styleId, paraAttr) {
            const styleAttrs = this.paragraphStyles.getStyleSheetAttributeMap(styleId);
            return styleAttrs?.paragraph?.[paraAttr];
        }

        /**
         * Updating the neighbor paragraphs, if the list level, border, color or margin of the specified paragraph
         * was modified.
         *
         * @param {Node|jQuery} paragraph
         *  The paragraph node that gets new attributes assigned.
         *
         * @param {Object} attributes
         *  The container with the modified attributes of the paragraph.
         */
        #handleNeighborUpdate(paragraph, attributes) {

            // the previous and the next paragraph nodes
            let prevParagraph = null, nextParagraph = null;

            // update neighbors when listLevel, a border, the margin or the fillColor is changed (note: null must also be checked, there are some cases where the attribute has changed and the value is null)
            if (attributes && attributes.paragraph && (_.isNumber(attributes.paragraph.listLevel) || attributes.paragraph.borderLeft || attributes.paragraph.borderRight || attributes.paragraph.borderTop || attributes.paragraph.borderBottom || attributes.paragraph.borderInside || _.isNumber(attributes.paragraph.marginTop) || _.isNumber(attributes.paragraph.marginBottom) || attributes.paragraph.fillColor || attributes.paragraph.fillColor === null || attributes.paragraph.borderLeft === null || attributes.paragraph.borderRight === null || attributes.paragraph.borderTop === null || attributes.paragraph.borderBottom === null || attributes.paragraph.borderInside === null)) {

                // get the neighbors
                prevParagraph = $(paragraph).prev();
                nextParagraph = $(paragraph).next();

                // also updating the neighboring paragraphs
                if (prevParagraph && prevParagraph.length > 0 && isParagraphNode(prevParagraph)) { this.implParagraphChanged(prevParagraph); }
                if (nextParagraph && nextParagraph.length > 0 && isParagraphNode(nextParagraph)) { this.implParagraphChanged(nextParagraph); }
            }

        }

        /**
         * Updating the table node inside a specified table drawing node.
         *
         * @param {Node|jQuery} drawing
         *  The drawing node whose table node gets new attributes assigned.
         *
         * @param {Object} attributes
         *  The container with the modified attributes of the table drawing node.
         */
        #setTableAttributesInTableDrawing(drawing, attributes) {

            const tableNode = $(drawing).find('table');
            let tableAttrs = null;

            if (attributes.table) { tableAttrs = { table:  attributes.table }; }

            if (attributes.styleId) {
                tableAttrs = tableAttrs || {};
                tableAttrs.styleId = attributes.styleId;
            }

            if (tableAttrs) { this.tableStyles.setElementAttributes(tableNode, tableAttrs); }
        }

        /**
         * Reducing a set of specified drawings corresponding the global drawing filter list.
         * This is necessary to avoid, that drawings get attributes assigned, that they do not
         * support.
         *
         * @param {jQuery[]} drawings
         *  A collector of drawing nodes.
         *
         * @param {Object} attributes
         *  A map of attribute value maps (name/value pairs), keyed by
         *  attribute families.
         *
         * @returns {undefined}
         *  A filtered collector of drawing nodes.
         */
        #filterDrawingNodes(drawings, attributes) {

            // the local reference to the collector of drawing nodes.
            let filteredDrawings = drawings;

            _.each(this.#drawingFilterList, oneFilter => {
                filteredDrawings = _.filter(filteredDrawings, oneDrawing => { return oneFilter(oneDrawing, attributes); });
            });

            return filteredDrawings;
        }

        /**
         * Filtering the drawing attributes, so that not all drawing attributes are merged into the
         * set of merged attributes. For example the fill family attributes of a connector shall not
         * be used for the merged attributes. For example, if a cicle with yellow background is
         * selected together with a line (without any background), the mixture of all attributes
         * shall still be yellow. To achieve this, the fill-family has to be removed from the
         * attribute set of the line.
         *
         * @param {jQuery} drawing
         *  A drawing node.
         *
         * @param {Object} attributes
         *  A map of attribute value maps (name/value pairs), keyed by attribute families. This object
         *  will be modified in the filter function.
         */
        #filterDrawingAttrs(drawing, attributes) {
            _.each(this.#drawingFilterList, oneFilter => {
                oneFilter(drawing, attributes, true); // third parameter is true -> deleting attributes in filter
            });
        }

        /**
         * Check, whether a specified text span is an empty span in an empty paragraph in a
         * selected empty drawing text frame.
         *
         * @param {Node|jQuery} span
         *  The text span.
         *
         * @returns {Boolean}
         *  Whether the specified span is empty and is located in an empty paragraph without
         *  neighbors inside a selected drawing.
         */
        #isEmptyTextFrameSelection(span) {

            let isEmptyTextFrameSelection = false;

            if (this.getSelection().isDrawingSelection() && span.firstChild.nodeValue.length === 0) {
                const paragraph = $(span).parent();
                if (isEmptyParagraph(paragraph) && isParagraphWithoutNeighbour(paragraph)) {
                    isEmptyTextFrameSelection = true;
                }
            }

            return isEmptyTextFrameSelection;
        }

        /**
         * Performance: Check, whether a simplified paragraph update is sufficient for lists. This is the case, if the
         *              old and the new state of the paragraph is NOT a numbering list. For bullet lists (or switch to
         *              or removal of a bullet list) only the current paragraph needs to be updated
         */
        #listUpdateSingleParagraph(listLevel, oldListLevel, listStyleId, oldListStyleId) {

            if (!this.#docApp.isTextApp()) { return false; }
            if (_.isNumber(listLevel) && this.getListCollection().isNumberingList(listStyleId, listLevel)) { return false; }
            if (_.isNumber(oldListLevel) && this.getListCollection().isNumberingList(oldListStyleId, oldListLevel)) { return false; }

            return true;
        }

        /**
         * Check if a text in a node contains only whitespaces at the specified leading number of positions.
         *
         * @param {Node|jQuery} node
         *  The node, whose text content will be investigated.
         *
         * @param {Number} length
         *  The number of leading whitespaces, that need to be investigated.
         *
         * @returns {Boolean}
         *  Whether the text inside the specified node starts with the specified number of whitespaces.
         */
        #onlyWhitespacesAtStart(node, length) {

            const text = $(node).text();
            let index = 0;
            let char = null;
            let onlyWhitespaces = true;

            while (onlyWhitespaces && index < length) {
                char = text[index];
                if (char !== ' ' && char !== '\xa0') { onlyWhitespaces = false; }
                index++;
            }

            return onlyWhitespaces;
        }

        /**
         * Changes a specific formatting attribute of the specified element or
         * text range. The type of the attributes will be determined from the
         * specified range.
         *
         * @param {Number[]} start
         *  The logical start position of the element or text range to be
         *  formatted.
         *
         * @param {Number[]} [end]
         *  The logical end position of the element or text range to be
         *  formatted.
         *
         * @param {Object} attributes
         *  A map with formatting attribute values, mapped by the attribute
         *  names, and by attribute family names.
         *
         * @param {string[]} target
         *
         * @param {Boolean} external
         *
         * @param {Boolean} noUndo
         *  If set to true, no undo operation will be generated
         *
         */
        #implSetAttributes(start, end, attributes, target, external, noUndo) {

            // node info for start/end position
            let startInfo = null, endInfo = null;
            // the main attribute family of the target components
            let styleFamily = null;
            // the style sheet container for the target components
            let styleSheets = null;
            // a helper object to save the style sheets temporarely
            let savedStyleSheets = null;
            // options for style collection method calls
            let options = null;
            // the last text span visited by the character formatter
            let lastTextSpan = null;
            // undo operations going into a single action
            const undoOperations = [];
            // redo operation
            let redoOperation = null;
            // Performance: Saving data for list updates
            let paraAttrs = null, listStyleId = null, listLevel = null, oldListStyleId = null, oldListLevel = null;
            // whether only the current single paragraph needs a list update
            let onlyCurrentParaListUpdate = false;
            // a helper attribute object with values set to null
            let nullAttributes = null;
            // a helper iterator key
            let localkey = null;
            //element which is passed to page breaks calculation
            let currentElement;
            // whether an update of formatting is required after assigning attributes (needed by spellcheck attribute)
            let forceUpdateFormatting = false;
            // a data object, that is used for updating slides with master or layout target (presentation only)
            let forceLayoutFormattingData = null;
            //flags for triggering page breaks
            let isCharOrDrawingOrRow;
            let isClearFormatting;
            let isAttributesInParagraph;
            let isPageBreakBeforeAttributeInParagraph;
            let isParSpacing;
            let isBorderTopBot;
            let isParaIndent;
            // if target is present, root node is header or footer, otherwise editdiv
            const rootNode = this.getRootNode(target);
            // the parent node
            let parentNode = null;
            // the selection object
            const selection = this.getSelection();
            // a helper object for attribute modifications
            let modifiedAttributes = null;
            // whether a special handling for character attributes at empty paragraphs was used
            let usedSpecialCharacterHandling = false;
            // the unde manager object
            const undoManager = this.getUndoManager();

            // sets or clears the attributes using the current style sheet container
            // 'modifiedAttrs' is an optional parameter, that is only used, if the original attribute set was modified
            const setElementAttributes = (element, modifiedAttrs) => {
                styleSheets.setElementAttributes(element, modifiedAttrs ? modifiedAttrs : attributes, options);
            };

            // change listener used to build the undo operations
            const changeListener = (attributedNode, oldAttributes, newAttributes) => {

                if (noUndo) { return; }

                // the element node might be modified, if it is an empty text span (41250)
                const element = (!this.#docApp.isODF() && isEmptySpan(attributedNode)) ? attributedNode.parentNode : attributedNode;
                // selection object representing the passed element
                const range = getPositionRangeForNode(rootNode, element);
                // the attributes of the current family for the undo operation
                const undoAttributes = {};
                // the operation used to undo the attribute changes
                const undoOperation = { name: SET_ATTRIBUTES, start: _.copy(range.start), end: _.copy(range.end), attrs: undoAttributes };
                // last undo operation (used to merge character attributes of sibling text spans)
                const lastUndoOperation = ((undoOperations.length > 0) && (styleFamily === 'character')) ? _.last(undoOperations) : null;

                const insertUndoAttribute = (family, name, value) => {
                    undoAttributes[family] = undoAttributes[family] || {};
                    undoAttributes[family][name] = value;
                };

                // using modified logical positions, if the attributed text span is a template text
                // -> character attributes must be assigned to the paragraph
                if (isTextFrameTemplateTextSpan(attributedNode)) {
                    undoOperation.start = _.initial(undoOperation.start);
                    undoOperation.end = _.clone(undoOperation.start);
                }

                // merging the place holder drawings into the old attributes object (presentation only)
                if (forceLayoutFormattingData && forceLayoutFormattingData.oldAttrs) { oldAttributes = this.mergePlaceHolderAttributes(element, startInfo.family, oldAttributes, forceLayoutFormattingData.oldAttrs); }

                // extend undoOperation if target is present
                this.extendPropertiesWithTarget(undoOperation, target);

                // exceptional case for setting line type to 'none', to save the old color
                if (_.has(attributes, 'line') && _.has(attributes.line, 'type') && attributes.line.type === 'none') {
                    if (_.has(oldAttributes, 'line') && _.has(oldAttributes.line, 'color') && !_.isUndefined(oldAttributes.line.color)) {
                        undoAttributes.line = {};
                        undoAttributes.line.color = oldAttributes.line.color;
                    }
                }

                // exceptional case for setting auto-fit property in presentation, to preserve drawing height in undo, #46349
                if (this.useSlideMode()) {
                    const newAttrsAutoHeight = newAttributes && isAutoResizeHeightAttributes(newAttributes.shape);
                    const oldAttrsAutoHeight = oldAttributes && isAutoResizeHeightAttributes(oldAttributes.shape);
                    if (newAttrsAutoHeight && !oldAttrsAutoHeight && oldAttributes && oldAttributes.drawing) {
                        undoAttributes.drawing = undoAttributes.drawing || {};
                        undoAttributes.drawing = _.extend(undoAttributes.drawing, oldAttributes.drawing);
                    }
                }

                // find all old attributes that have been changed or cleared
                _(oldAttributes).each((attributeValues, family) => {
                    if (family === 'styleId') {
                        // style sheet identifier is a string property
                        if (attributeValues !== newAttributes.styleId) {
                            undoAttributes.styleId = attributeValues;
                        }
                    } else {
                        // process attribute map of the current family
                        _(attributeValues).each((value, name) => {
                            if (!(family in newAttributes) || !_.isEqual(value, newAttributes[family][name])) {
                                insertUndoAttribute(family, name, value);
                            }
                        });
                    }
                });

                // find all newly added attributes
                _(newAttributes).each((attributeValues, family) => {
                    if (family === 'styleId') {
                        // style sheet identifier is a string property
                        if (!_.isString(oldAttributes.styleId)) {
                            undoAttributes.styleId = null;
                        }
                    } else {
                        // process attribute map of the current family
                        _(attributeValues).each((_value, name) => {
                            if (!(family in oldAttributes) || !(name in oldAttributes[family])) {
                                insertUndoAttribute(family, name, null);
                            }
                        });
                    }
                });

                // try to merge 'character' undo operation with last array entry, otherwise add operation to array
                if (lastUndoOperation && (_.last(lastUndoOperation.end) + 1 === _.last(undoOperation.start)) && _.isEqual(lastUndoOperation.attrs, undoOperation.attrs)) {
                    lastUndoOperation.end = undoOperation.end;
                } else {
                    undoOperations.push(undoOperation);
                }

                // invalidate spell result if language attribute changes
                this.getSpellChecker().resetClosest(element);
            };

            // fail if attributes is not of type object
            if (!_.isObject(attributes)) {
                return false;
            }

            // do nothing if an empty attributes object has been passed
            if (_.isEmpty(attributes)) {
                return;
            }

            // resolve start and end position
            if (!_.isArray(start)) {
                globalLogger.warn('Editor.#implSetAttributes(): missing start position');
                return false;
            }

            // validating the logical positions (forced by 57048)
            if (!isValidElementRange(rootNode, start, end)) {
                globalLogger.warn('Editor.#implSetAttributes(): invalid attribute range: ' + JSON.stringify(start) + ' ' + (end ? JSON.stringify(end) : ''));
                return false;
            }

            // whether the cache can be reused (performance)
            const lastOperation = this.getLastOperation();
            const useCache = (!target && lastOperation && this.getParagraphCacheOperation()[lastOperation.name] &&
                selection.getParagraphCache() && _.isEqual(_.initial(start), selection.getParagraphCache().pos) && _.isUndefined(lastOperation.target));

            if (useCache) {
                TMPPOSARRAY[0] = _.last(start);
                startInfo = getDOMPosition(selection.getParagraphCache().node, TMPPOSARRAY, true);
                // setting paragraph cache is not required, it can simply be reused in next operation
            } else {
                startInfo = getDOMPosition(rootNode, start, true);

                if (!startInfo) {
                    globalLogger.warn('Editor.#implSetAttributes(): invalid start node!');
                }

                if (startInfo.family === 'character') {
                    selection.setParagraphCache(startInfo.node.parentElement, _.clone(_.initial(start)), _.last(start));
                } else {
                    selection.setParagraphCache(null); // invalidating the cache (previous operation might not be in PARAGRAPH_CACHE_OPERATIONS)
                }
            }

            if (!startInfo || !startInfo.node) {
                globalLogger.warn('Editor.#implSetAttributes(): invalid start position: ' + JSON.stringify(start));
                return false;
            }
            // get attribute family of start and end node
            startInfo.family = this.#resolveElementFamily(startInfo.node);

            if (!startInfo.family) { return; }

            if (_.isArray(end)) {
                if (startInfo.family === 'character') {
                    //characters are children of paragraph, end is in the same paragraph, so we dont have to search the complete document for it
                    TMPPOSARRAY[0] = _.last(end);
                    endInfo = getDOMPosition(startInfo.node.parentElement, TMPPOSARRAY, true);
                } else {
                    endInfo = getDOMPosition(rootNode, end, true);
                }

                endInfo.family = this.#resolveElementFamily(endInfo.node);

                if (!endInfo.family) { return; }

            } else {
                end = start;
                endInfo = startInfo;
            }

            // options for the style collection method calls (build undo operations while formatting)
            options = (undoManager.isUndoEnabled() && !external) ? { changeListener } : null;

            // updating the slide model (the attributes are not only saved in the 'data' object, but also in the model itself)
            if (this.useSlideMode()) {
                // updating the attributes of family 'slide'
                if (startInfo.family === 'slide') {
                    this.updateSlideFamilyModel(start, attributes, target);
                    if (target && _.isFunction(this.isLayoutOrMasterId) && this.isLayoutOrMasterId(target) && attributes && attributes.fill) { forceLayoutFormattingData = { slideBackgroundUpdate: true, target, attrs: _.copy(attributes, true) }; } // only required for 48471
                } else {
                    // updating the list style attributes at drawing nodes
                    if (startInfo.family === 'drawing' && this.drawingStyles.setDrawingListStyleAttributes) { this.drawingStyles.setDrawingListStyleAttributes(startInfo.node, attributes); }
                    // Special handling for presentation in master/layout slides -> updating styles (before calling setElementAttributes)
                    if (target && _.isFunction(this.isLayoutOrMasterId) && this.isLayoutOrMasterId(target)) { forceLayoutFormattingData = this.updateMasterLayoutStyles(styleFamily === 'character' ? startInfo.node.parentNode : startInfo.node, target, startInfo.family, attributes, { immediateUpdate: false, saveOldAttrs: true }); }
                }
            }

            // characters (start or end may point to a drawing node, ignore that but format as
            // characters if the start object is different from the end object)
            if ((startInfo.family === 'character') || (endInfo.family === 'character') ||
                ((startInfo.node !== endInfo.node) && isDrawingFrame(startInfo.node) && isDrawingFrame(endInfo.node))) {

                // check that start and end are located in the same paragraph (and handling absolute positioned drawings correctly)
                if (startInfo.node.parentNode !== endInfo.node.parentNode) {

                    // replacing the absolute positioned drawings by its place holder nodes
                    if (isDrawingLayerNode(startInfo.node.parentNode)) { startInfo.node = getDrawingPlaceHolderNode(startInfo.node); }
                    if (isDrawingLayerNode(endInfo.node.parentNode)) { endInfo.node = getDrawingPlaceHolderNode(endInfo.node); }

                    if (startInfo.node.parentNode !== endInfo.node.parentNode) {
                        globalLogger.warn('Editor.#implSetAttributes(): end position in different paragraph');
                        return;
                    }
                }

                // visit all text span elements covered by the passed range
                // (not only the direct children of the paragraph, but also
                // text spans embedded in component nodes such as fields and tabs)
                styleFamily = 'character';
                styleSheets = this.characterStyles;
                iterateParagraphChildNodes(startInfo.node.parentNode, node => {

                    // visiting the span inside a hard break node
                    // -> this is necessary for change tracking attributes
                    if (isHardBreakNode(node)) {
                        setElementAttributes(node.firstChild);
                    }
                    // if element is field, set fixed attribute (for automatic updating), also bug #43655; bookmarks are handled with DOCS-3217
                    if (isComplexFieldNode(node) || isFieldNode(node) || isRangeMarkerNode(node) || isBookmarkNode(node)) {
                        setElementAttributes(node);
                        if (isFieldNode(node)) {
                            if (!this.useSlideMode()) {
                                this.getFieldManager().addSimpleFieldToCollection(node, target);
                            }
                            $(node).css('font-size', ''); // see bug #47515
                        }
                    }

                    // iterateTextSpans() visits the node itself if it is a
                    // text span, otherwise it visits all descendant text spans
                    // contained in the node except for drawings which will be
                    // skipped (they may contain their own paragraphs).
                    iterateTextSpans(node, span => {
                        let convertBack = false;
                        // check for a spellchecked span (this needs to be checked, before attributes are applied)
                        if (!forceUpdateFormatting && this.isImportFinished() && isSpellerrorNode(span)) { forceUpdateFormatting = true; }

                        if (isSpecialField(node)) { // to properly apply attributes to special field span, they need to be unwrapped from parent node
                            this.getFieldManager().restoreSpecialField(node);
                            convertBack = true;
                        }

                        setElementAttributes(span);

                        if (convertBack) { // and later returned to previous state
                            const instruction = getComplexFieldInstruction(node);
                            const fieldType = (/NUMPAGES/i).test(instruction) ? 'NUMPAGES' : 'PAGE';
                            const id = getComplexFieldId(node);
                            this.getFieldManager().convertToSpecialField(id, target, fieldType);
                        }

                        // try to merge with the preceding text span
                        mergeSiblingTextSpans(span, false);
                        // remember span (last visited span will be merged with its next sibling)
                        lastTextSpan = span;
                    });

                }, undefined, {
                    // options for Position.iterateParagraphChildNodes()
                    allNodes: true,
                    start: _(start).last(),
                    end: _(end).last(),
                    split: true
                });

                // updating the drawing node, if the change happened inside a text frame or shape (51964)
                if (!forceUpdateFormatting && getDrawingNode(startInfo.node.parentNode)) { forceUpdateFormatting = true; }

                // handling spell checking after modifying character attributes
                if (forceUpdateFormatting) { this.implParagraphChanged(startInfo.node.parentNode); }

                // try to merge last text span in the range with its next sibling
                if (lastTextSpan) {
                    mergeSiblingTextSpans(lastTextSpan, true);
                }

            // otherwise: only single components allowed at this time
            } else {

                // check that start and end point to the same element
                if (startInfo.node !== endInfo.node) {
                    globalLogger.warn('Editor.#implSetAttributes(): no ranges supported for attribute family "' + startInfo.family + '"');
                    return;
                }

                // format the (single) element
                styleFamily = startInfo.family;
                styleSheets = this.getStyleCollection(styleFamily);

                // Performance: Saving old list style id and old list level, before it is removed
                if (this.isImportFinished() && (styleFamily === 'paragraph')) {
                    paraAttrs = getExplicitAttributeSet(startInfo.node);
                    if (paraAttrs) {
                        oldListStyleId = this.#getListStyleIdFromParaAttrs(paraAttrs);
                        if (paraAttrs.paragraph) { oldListLevel = paraAttrs.paragraph.listLevel; }
                    }
                }

                // assigning character attributes to text spans, not to paragraphs (41250)
                if (!this.#docApp.isODF() && styleFamily === 'paragraph' && 'character' in attributes) {

                    savedStyleSheets = styleSheets; // saving stylesSheet objects
                    styleSheets = this.characterStyles; // overwriting current styleSheet object to use valid styleSheet (53118)

                    _.each($(startInfo.node).children('span'), node => {
                        // setting the character attributes (only those!) to the text span(s) inside the paragraph
                        if (isEmptySpan(node) || isTextFrameTemplateTextSpan(node)) {
                            setElementAttributes(node, { character: _.copy(attributes.character, true) });
                            usedSpecialCharacterHandling = true;
                        }
                    });

                    styleSheets = savedStyleSheets; // restoring styleSheet objects

                    if (usedSpecialCharacterHandling) {

                        this.implParagraphChanged(startInfo.node);

                        // saving the character attributes at the paragraph node, so that they can be used in clearAttributes operation
                        $(startInfo.node).data('origCharAttrs', _.copy(attributes.character, true));

                        // removing the character attributes, so that they are not assigned to the paragraph
                        modifiedAttributes = _.copy(attributes, true);  // not modifying the original object
                        if (!this.hasOoxmlTextCharacterFilterSupport()) { delete modifiedAttributes.character; } // DOCS-4211: Assigning character attributes also to the paragraph in docx

                        if (!_.isEmpty(modifiedAttributes)) { setElementAttributes(startInfo.node, modifiedAttributes); }
                    }
                }

                if (!usedSpecialCharacterHandling) {

                    setElementAttributes(startInfo.node);

                    // update also the neighbors, if the list level, border, color or margin was modified
                    this.#handleNeighborUpdate(startInfo.node, attributes);

                    // handling table attributes for drawing tables (in presentation app)
                    if (this.useSlideMode() && styleFamily === 'drawing' && isTableDrawingFrame(startInfo.node)) {
                        // TODO: Avoid duplicate table attributation (setElementAttributes for the drawing already formatted the table)
                        // -> in the following function the attributes shall only be set at the drawing node
                        this.#setTableAttributesInTableDrawing(startInfo.node, attributes);
                    }

                    // updating the selection of a selected drawing node (and drawing table node)
                    if (this.useSlideMode()) {
                        if (styleFamily === 'drawing') {
                            updateSelectionDrawingSize(startInfo.node);
                        } else if (styleFamily === 'row') {
                            updateSelectionDrawingSize($(startInfo.node).closest(NODE_SELECTOR));
                        }
                    }

                    if (styleFamily === 'drawing') {

                        this.trigger('change:drawing');
                        this.trigger('drawingHeight:update', startInfo.node, { external });
                        // -> this handler function for setAttributes operation must not generate operations inside
                        //    'drawingHeight:update'-handlers, if it was triggered by an external operation!
                    }
                }

                if (styleFamily === 'paragraph') {
                    // updating the paragraph node, if the change happened inside (54768)
                    if (!forceUpdateFormatting && getDrawingNode(startInfo.node)) { forceUpdateFormatting = true; }
                    if (forceUpdateFormatting) { this.implParagraphChanged(startInfo.node); }
                }
            }

            // create the undo action
            if (undoManager.isUndoEnabled() && !external && !noUndo) {
                redoOperation = { name: SET_ATTRIBUTES, start: _.copy(start), end: _.copy(end), attrs: attributes };
                // extend redoOperation if target is present
                this.extendPropertiesWithTarget(redoOperation, target);
                undoManager.addUndo(undoOperations, redoOperation);
            }

            // update numberings and bullets (but updateListsDebounced can only be called after successful document import)
            if (this.isImportFinished() && (styleFamily === 'paragraph')) {
                if (('styleId' in attributes) ||
                    (
                        _.isObject(attributes.paragraph) &&
                        (('listLevel' in attributes.paragraph) || ('listStyleId' in attributes.paragraph))
                    )
                ) {

                    // determining listStyleId und listLevel
                    if ((_.isObject(attributes.paragraph)) && ('listStyleId' in attributes.paragraph) && (attributes.paragraph.listStyleId !== null)) {
                        listStyleId = attributes.paragraph.listStyleId;  // list style assigned to paragraph
                        listLevel = ('listLevel' in attributes.paragraph) ? attributes.paragraph.listLevel : 0;
                    } else if (attributes.styleId && this.isParagraphStyleWithListStyle(attributes.styleId)) {
                        listStyleId = this.#getListStyleInfoFromStyleId(attributes.styleId, 'listStyleId');
                        listLevel = this.#getListStyleInfoFromStyleId(attributes.styleId, 'listLevel') || 0;
                    } else {
                        // list level modified -> checking paragraph attributes for list styles to receive list style id
                        paraAttrs = getExplicitAttributeSet(startInfo.node);
                        // updating lists, if required
                        if (this.isListStyleParagraph(null, paraAttrs)) {
                            if (paraAttrs && paraAttrs.paragraph) {
                                listStyleId = paraAttrs.paragraph.listStyleId;
                                listLevel = paraAttrs.paragraph.listLevel;
                            }
                        }
                    }

                    // handling the case, that the list style id is removed
                    if (!listStyleId && oldListStyleId) { listStyleId = oldListStyleId; }

                    // defining list style Id or listLevel, that need to be updated
                    if (listStyleId || listLevel) {
                        if (!listStyleId) {
                            listStyleId = this.paragraphStyles.getElementAttributes(startInfo.node).paragraph.listStyleId;
                            // fix for Bug 37594 styleId is not at the para-attrs and not in the style itself. but by merging we get the correct stlyeId
                        }
                        // mark this paragraph for later list update. This is necessary, because it does not yet contain
                        // the list label node (LIST_LABEL_NODE_SELECTOR) and is therefore ignored in updateLists.
                        $(startInfo.node).data('updateList', 'true');
                        // registering this list style for update
                        // special handling for removal of listLevel, for example after 'Backspace'
                        if (listStyleId && (listLevel === null || listLevel === -1)) {
                            $(startInfo.node).data('removeLabel', 'true');
                        }

                        // Performance: If this is an attribute change and the old and the new list styles are not numbering lists, only the current paragraph needs to be updated
                        onlyCurrentParaListUpdate = this.#listUpdateSingleParagraph(listLevel, oldListLevel, listStyleId, oldListStyleId);

                        // not registering listStyle for updating, if this is a bullet list -> updating only specific paragraphs
                        if (this.getListCollection() && this.getListCollection().isAllLevelsBulletsList(listStyleId)) { listStyleId = null; }

                        this.updateListsDebounced(onlyCurrentParaListUpdate ? $(startInfo.node) : null, { useSelectedListStyleIDs: true, paraInsert: true, listStyleId: (oldListStyleId ? [listStyleId, oldListStyleId] : listStyleId), listLevel });
                    }
                } else if (this.useSlideMode()) {

                    if (_.isObject(attributes.paragraph) && ('level' in attributes.paragraph || 'bullet' in attributes.paragraph)) {
                        this.updateListsDebounced($(startInfo.node));
                    }

                } else if ('character' in attributes) {  // modified character attributes may lead to modified list labels

                    // checking paragraph attributes for list styles to receive list style id
                    paraAttrs = getExplicitAttributeSet(startInfo.node);
                    // updating lists, after character attributes were modified
                    if (this.isListStyleParagraph(null, paraAttrs)) {
                        if (paraAttrs && paraAttrs.paragraph) {
                            listStyleId = paraAttrs.paragraph.listStyleId;
                            listLevel = paraAttrs.paragraph.listLevel;
                            this.updateListsDebounced(null, { useSelectedListStyleIDs: true, paraInsert: true, listStyleId, listLevel });
                        }
                    }
                }
            }

            if (this.#docApp.isODF() && (styleFamily === 'paragraph') && (attributes.character) && (getParagraphNodeLength(startInfo.node) === 0)) {
                // Task 28187: Setting character attributes at empty paragraphs
                // -> removing character attributes from text span so that character styles at paragraph become visible (also 30927)
                nullAttributes = _.copy(attributes, true);
                for (localkey in nullAttributes.character) { nullAttributes.character[localkey] = null; }
                this.characterStyles.setElementAttributes(findFirstPortionSpan(startInfo.node), nullAttributes);
            }

            if (this.isImportFinished()) {
                // adjust tabulators, if character or drawing attributes have been changed
                // (changing paragraph or table attributes updates tabulators automatically)
                if ((styleFamily === 'character') || (styleFamily === 'drawing')) {

                    parentNode = startInfo.node.parentNode;

                    if (!this.useSlideMode() || !isSlideNode(parentNode)) { // only update paragraphs, not slides
                        this.paragraphStyles.updateTabStops(parentNode);
                        if (this.useSlideMode()) {
                            this.updateListsDebounced($(parentNode));
                        } else if (_.last(start) === 0) { // in Text app only if first character is modified (58026)

                            // taking care of inheritance of character attributes to bullet list labels (DOCS-3422) -> numbering list labels already look good
                            if (styleFamily === 'character' && isListParagraphNode(parentNode) && startInfo.node.previousSibling && isListLabelNode(startInfo.node.previousSibling)) {
                                this.handleTriggeringListUpdate($(parentNode), { forceListLabelUpdate: true });
                            }

                            if (this.handleTriggeringListUpdate($(parentNode), { checkSplitInNumberedList: true })) { // taking care of 62422
                                $(parentNode).data('splitInNumberedList', 'true'); // using 'splitInNumberedList' so that only following, but not previous paragraphs are updated
                            }
                        }
                    }
                } else if (styleFamily === 'paragraph' && this.#docApp.isTextApp() && isListParagraphNode(startInfo.node) && attributes.paragraph && _.isNumber(attributes.paragraph.indentFirstLine)) {
                    this.updateListsDebounced(startInfo.node); // required for updating the distance between bullet and text (DOCS-2278) -> very specific update after using the ruler
                }

                // if target comes with operation, take care of targeted header/footer, otherwise, run normal page break rendering
                if (target) {
                    // trigger header/footer content update on other elements of same type, if change was made inside header/footer
                    if (isMarginalNode(startInfo.node)) {
                        this.updateEditingHeaderFooterDebounced(getMarginalTargetNode(this.getNode(), startInfo.node));
                    } else if (isMarginalNode(startInfo.node.parentNode)) {
                        this.updateEditingHeaderFooterDebounced(getMarginalTargetNode(this.getNode(), startInfo.node.parentNode));
                    }
                }
                this.setBlockOnInsertPageBreaks(!!target);

                // after document is loaded, trigger pagebreaks reposition on certain events like font size change, height of drawing or row changed
                isCharOrDrawingOrRow = 'character' in attributes || 'drawing' in attributes || 'row' in attributes || ('line' in attributes && 'width' in attributes.line);
                isClearFormatting = 'styleId' in attributes && attributes.styleId === null;

                if (!this.useSlideMode()) { // handling of page breaks, not required in presentation app

                    if (_.isObject(attributes.paragraph)) {
                        paraAttrs = attributes.paragraph;
                        // check level in Presentation & listLevel in Text
                        isAttributesInParagraph = ('lineHeight' in paraAttrs) || ('listLevel' in paraAttrs) || ('level' in paraAttrs) || ('listStyleId' in paraAttrs);
                        isPageBreakBeforeAttributeInParagraph = ('pageBreakBefore' in paraAttrs);
                        isParSpacing = 'marginBottom' in paraAttrs;
                        isBorderTopBot = 'borderTop' in paraAttrs || 'borderBottom' in paraAttrs;
                        isParaIndent = 'indentLeft' in paraAttrs || 'indentRight' in paraAttrs || 'indentFirstLine' in paraAttrs;
                    }

                    if (isCharOrDrawingOrRow || isClearFormatting || isAttributesInParagraph || isPageBreakBeforeAttributeInParagraph || isParSpacing || isBorderTopBot || isParaIndent) {
                        if (start.length > 1) {
                            // if its paragraph creation inside of table
                            currentElement = getContentNodeElement(rootNode, start.slice(0, 1));
                        } else {
                            currentElement = getContentNodeElement(rootNode, start);
                        }
                        if ($(currentElement).data('lineBreaksData')) {
                            $(currentElement).removeData('lineBreaksData'); //we changed paragraph layout, cached line breaks data needs to be invalidated
                        }
                        // attribute removed by user, needs to refresh page layout
                        if (isPageBreakBeforeAttributeInParagraph && !attributes.paragraph.pageBreakBefore) {
                            $(startInfo.node).removeClass('manual-page-break'); // remove class from div.p element
                            $(currentElement).removeClass('manual-page-break'); // remove class from parent table element also
                        }
                        this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(startInfo.node));
                        if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }
                    }

                    // adding handling for ruler update after resize drawing or modifying cell width or changing the paragraph styleId
                    if ((!isParaIndent) && ((_.isObject(attributes.drawing) && _.isNumber(attributes.drawing.width)) || (_.isObject(attributes.table) && _.isNumber(attributes.table.width)) || _.isString(attributes.styleId))) {
                        isParaIndent = true;
                    }
                }
            }

            if (isParaIndent) { this.trigger('ruler:initialize'); }

            // Special handling for presentation in master/layout slides -> updating styles
            if (target && _.isFunction(this.isLayoutOrMasterId) && this.isLayoutOrMasterId(target) && forceLayoutFormattingData) { this.forceMasterLayoutUpdate(forceLayoutFormattingData); }

            if (attributes.character && attributes.character.field && attributes.character.field.formFieldType === 'checkBox') {
                $(startInfo.node).text(attributes.character.field.checked ? '☒' : '☐');
            }

            // informing listeners, for example to clean and redraw remote selections (67116)
            this.trigger('docs:attribute:changed');

            return true;
        }

    }

    return AttributeOperationMixin;
}

// exports ====================================================================

export const XAttributeOperation = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XAttributeOperation>}
     *  The extended class with attribute operation functionality.
     */
    mixin: mixinAttributeOperation
};
