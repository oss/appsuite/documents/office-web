/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getBooleanOption, getIntegerOption } from '@/io.ox/office/tk/utils';
import { getClosestTextFrameDrawingNode, isTableDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { checkForLateralTableStyle, getAllInsertPositions, getCellPositionFromGridPosition, getColSpanSum,
    getColumnCount, getGridPositionFromCellPosition, getTableGridWithNewColumn, mergeableTables,
    shiftCellContent } from '@/io.ox/office/textframework/components/table/table';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { MAX_TABLE_CELLS, MAX_TABLE_COLUMNS, MAX_TABLE_ROWS } from '@/io.ox/office/textframework/utils/config';
import { CELL_MERGE, CELL_SPLIT, CELLS_INSERT, COLUMN_INSERT, DELETE, PARA_INSERT, ROWS_INSERT, SET_ATTRIBUTES,
    TABLE_INSERT, TABLE_MERGE, TABLE_SPLIT } from '@/io.ox/office/textframework/utils/operations';

function mixinTableOperation(BaseClass) {

    // mix-in class TableOperationMixin =======================================

    /**
     * A mix-in class for the document model class providing the table
     * operation handling used in a presentation and text document.
     *
     * @param {EditApplication} app
     *  The application instance.
     */

    class TableOperationMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Inserting a table into the document.
         * The undo manager returns the return value of the callback function.
         * For inserting a table it is important, that there is always a
         * paragraph between the new table and the neighboring table. This is
         * important for merging of tables, where the removal of this paragraph
         * leads to an automatic merge.
         *
         * @param {Object} size
         *  An object containing the properties 'width' and 'height' for the
         *  column and row count of the table.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        insertTable(size) {

            // undo manager returns the return value of the callback function
            return this.getUndoManager().enterUndoGroup(() => {

                // cursor position used to split the paragraph
                let startPosition = null;
                // paragraph to be split for the new table, and its position
                let paragraph = null, position = null;
                // text offset in paragraph, first and last text position in paragraph
                let offset = 0, startOffset = 0, endOffset = 0;
                // table attributes
                const attributes = { table: { tableGrid: [], width: 'auto' } };
                // table row and paragraph attributes
                let rowAttributes = null, rowOperationAttrs = null;
                const paraOperationAttrs = {};
                // default table style
                let tableStyleId = this.getDefaultUITableStylesheet();
                // a new implicit paragraph
                let newParagraph = null;
                // operations generator
                const generator = this.createOperationGenerator();
                // whether an additional paragraph is required behind or before the table
                let insertAdditionalParagraph = false;
                // the logical position of the additional paragraph
                let paraPosition = null;
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();
                // container root node of table
                const rootNode = this.getCurrentRootNode();
                // properties to be passed to generator for insert table operation
                let generatorProperties = {};
                // the selection object
                const selection = this.getSelection();

                // #49674 - odf's default table style has no borders, so we overwrite it with 'TableGridOx'
                //  'TableGrid' can be already sent with document, and it also has no border data
                if (this.#docApp.isODF() && tableStyleId === '_default') {
                    tableStyleId = this.getDefaultLateralTableODFDefinition().styleId;
                }

                const doInsertTable = () => {

                    startPosition = selection.getStartPosition();
                    position = startPosition.slice(0, -1);
                    paragraph = Position.getParagraphElement(rootNode, position);
                    if (!paragraph) { return; }

                    if (!DOM.isImplicitParagraphNode(paragraph)) {
                        // split paragraph, if the cursor is between two characters,
                        // or if the paragraph is the very last in the container node
                        offset = _.last(startPosition);
                        startOffset = Position.getFirstTextNodePositionInParagraph(paragraph);
                        endOffset = Position.getLastTextNodePositionInParagraph(paragraph);

                        if ((!paragraph.nextSibling) && (offset === endOffset)) {
                            if (DOM.isCellContentNode(paragraph.parentNode)) {
                                insertAdditionalParagraph = true; // inserting a valid paragraph behind the table in a table cell
                                position = Position.increaseLastIndex(position);
                                paraPosition = _.clone(position);
                                paraPosition = Position.increaseLastIndex(paraPosition);
                            } else {
                                // create a new empty implicit paragraph behind the table (if not inside another table)
                                newParagraph = DOM.createImplicitParagraphNode();
                                this.validateParagraphNode(newParagraph);
                                $(paragraph).after(newParagraph);
                                this.implParagraphChanged(newParagraph);
                                position = Position.increaseLastIndex(position);
                            }
                        } else if ((startOffset < offset) && (offset < endOffset)) {
                            this.splitParagraph(startPosition);
                            position = Position.increaseLastIndex(position);
                        } else if ((!paragraph.nextSibling) && (offset > 0)) {
                            this.splitParagraph(startPosition);
                            position = Position.increaseLastIndex(position);
                        } else if (offset === endOffset) {
                            // cursor at the end of the paragraph: insert before next content node
                            position = Position.increaseLastIndex(position);
                            // additionally adding an empty paragraph behind the table, if the following
                            // node is a table. Therefore there is always one paragraph between two tables.
                            // If this paragraph is removed, this leads to a merge of the tables (if this
                            // is possible).
                            if (paragraph.nextSibling && DOM.isTableNode(paragraph.nextSibling)) {
                                insertAdditionalParagraph = true;
                                paraPosition = _.clone(position);
                                paraPosition = Position.increaseLastIndex(paraPosition);
                            }
                        }
                    } else {
                        // if this is an explicit paragraph and the previous node is a table,
                        // there must be a new paragraph before the new table.
                        if (paragraph.previousSibling && DOM.isTableNode(paragraph.previousSibling)) {
                            insertAdditionalParagraph = true;
                            paraPosition = _.clone(position);
                        }
                    }

                    // prepare table column widths (values are relative to each other)
                    _(size.width).times(() => { attributes.table.tableGrid.push(1000); });

                    // set default table style
                    if (_.isString(tableStyleId)) {

                        // insert a pending table style if needed
                        checkForLateralTableStyle(generator, this, tableStyleId);

                        // add table style name to attributes
                        attributes.styleId = tableStyleId;

                        // default: tables do not have last row, last column and vertical bands
                        attributes.table.exclude = ['lastRow', 'lastCol', 'bandsVert'];
                    }

                    // modifying the attributes, if changeTracking is activated
                    if (this.getChangeTrack().isActiveChangeTracking()) {
                        attributes.changes = { inserted: this.getChangeTrack().getChangeTrackInfo() };
                        rowAttributes = { changes: { inserted: this.getChangeTrack().getChangeTrackInfo() } };
                    }

                    // insert the table, and add empty rows

                    generatorProperties = { start: _.clone(position), attrs: attributes };
                    this.extendPropertiesWithTarget(generatorProperties, target);
                    generator.generateOperation(TABLE_INSERT, generatorProperties);

                    rowOperationAttrs = { start: Position.appendNewIndex(position, 0), count: size.height, insertDefaultCells: true };
                    this.extendPropertiesWithTarget(rowOperationAttrs, target);
                    if (rowAttributes) { rowOperationAttrs.attrs = rowAttributes; }
                    generator.generateOperation(ROWS_INSERT, rowOperationAttrs);

                    // also adding a paragraph behind the table, if there is a following table
                    if (insertAdditionalParagraph) {
                        // modifying the attributes, if changeTracking is activated
                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            paraOperationAttrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo() };
                        }
                        generatorProperties =  _.isEmpty(paraOperationAttrs) ? { start: _.copy(paraPosition) } : { start: _.copy(paraPosition), attrs: paraOperationAttrs };
                        this.extendPropertiesWithTarget(generatorProperties, target);
                        generator.generateOperation(PARA_INSERT, generatorProperties);
                    }

                    // apply all collected operations
                    this.applyOperations(generator);

                    // set the cursor to first paragraph in first table cell
                    const newTablePosition = position.concat([0, 0, 0, 0]);
                    selection.setTextSelection(newTablePosition);
                    if (!_.isEqual(selection.getStartPosition(), newTablePosition)) { selection.setTextSelection(Position.increaseLastIndex(position).concat([0, 0, 0, 0])); } // handling for implicit paragraphs
                };

                if (selection.hasRange()) {
                    return this.deleteSelected()
                        .done(() => { doInsertTable(); });
                }

                doInsertTable();
                return $.when();

            }, this); // enterUndoGroup()

        }

        /**
         * Inserting a table row into a table in the document.
         */
        insertRow() {

            if (!this.isRowAddable()) {
                this.#docApp.getView().rejectEditTextAttempt('tablesizerow'); // checking table size (26809)
                return;
            }

            this.getUndoManager().enterUndoGroup(() => {

                // inserting only one row
                const count = 1;
                // the current logical position of the selection
                const position = this.getSelection().getEndPosition();
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();
                // current root container node for element
                const rootNode = this.getCurrentRootNode(target);
                // the row number from the logical position
                let referenceRow = null;
                // the html dom node of the row
                let rowNode = null;
                // the logical position of the row
                const rowPos = Position.getLastPositionFromPositionByNodeName(rootNode, position, 'tr');
                // the row attributes
                const rowAttrs = {};
                // operations generator
                const generator = this.createOperationGenerator();
                // created operation
                let newOperation = null;
                // new cursor position
                let newCursorPos = null;

                if (rowPos !== null) {
                    rowNode = Position.getLastNodeFromPositionByNodeName(rootNode, position, 'tr');
                    referenceRow = _.last(rowPos);
                    rowPos[rowPos.length - 1] += 1;

                    // modifying the attributes, if changeTracking is activated
                    if (this.getChangeTrack().isActiveChangeTracking()) {
                        rowAttrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null };
                    } else if (DOM.isChangeTrackNode(rowNode)) {
                        // change tracking is not active, but reference row has change track attribute -> this attribute needs to be removed
                        rowAttrs.changes = { inserted: null, removed: null, modified: null };
                    }

                    newOperation = { start: _.copy(rowPos), count, referenceRow };
                    if (!_.isEmpty(rowAttrs)) { newOperation.attrs = rowAttrs; }
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(ROWS_INSERT, newOperation);
                    this.setGUITriggeredOperation(true);
                    // apply all collected operations
                    this.applyOperations(generator);
                    this.setGUITriggeredOperation(false);

                    // setting the cursor position
                    newCursorPos = _.clone(rowPos);
                    newCursorPos.push(0, 0);
                    this.getSelection().setTextSelection(Position.getFirstPositionInParagraph(rootNode, newCursorPos));
                }

            }, this); // enterUndoGroup()
        }

        /**
         * Inserting a table column into a table in the document.
         */
        insertColumn() {

            if (!this.isColumnAddable()) {
                this.#docApp.getView().rejectEditTextAttempt('tablesizecolumn'); // checking table size (26809)
                return;
            }

            this.getUndoManager().enterUndoGroup(() => {

                // the selection object
                const selection = this.getSelection();
                // the selector for searching the table position
                const selector = this.useSlideMode() ? DOM.TABLE_NODE_AND_DRAWING_SELECTOR : DOM.TABLE_NODE_SELECTOR;
                // the logical end position of the selection
                const position = selection.getEndPosition();
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();
                const rootNode = this.getCurrentRootNode(target);
                const cellPosition = Position.getColumnIndexInRow(rootNode, position);
                const tablePos = Position.getLastPositionFromPositionByNodeName(rootNode, position, selector);
                const rowNode = Position.getLastNodeFromPositionByNodeName(rootNode, position, 'tr');
                const insertMode = 'behind';
                const gridPosition = getGridPositionFromCellPosition(rowNode, cellPosition).end; // inserting behind the end of the grid
                const tableGrid = getTableGridWithNewColumn(rootNode, tablePos, gridPosition, insertMode);
                // table node element
                const tablePoint = Position.getDOMPosition(rootNode, tablePos, true);
                // all rows in the table
                const allRows = DOM.getTableRows(tablePoint.node);
                // operations generator
                const generator = this.createOperationGenerator();
                // the attributes assigned to the cell(s)
                let cellAttrs = null;
                // created operation
                let newOperation = null;
                // position of the cursor after operations
                let newCursorPos = null;

                if (DOM.isTableWithMergedCells(tablePoint.node)) {
                    _.each(allRows, (row, index) => {

                        const cellPosition = getCellPositionFromGridPosition(row, gridPosition);
                        const newCellPosition = _.clone(tablePos); // the logical position of the row
                        newCellPosition.push(index);
                        newCellPosition.push(cellPosition + 1); // adding the new cell behind the existing cell
                        const cellNode = $(row).children(DOM.TABLE_CELLNODE_SELECTOR).eq(cellPosition);
                        // transfering cell attributes to the new inserted cell
                        const expCellAttributes = getExplicitAttributes(cellNode, 'cell');
                        expCellAttributes.gridSpan = 1;

                        newOperation = { start: _.copy(newCellPosition), count: 1, attrs: { cell: expCellAttributes } };

                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            newOperation.attrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null };
                        } else if (DOM.isChangeTrackNode(cellNode)) {
                            newOperation.attrs.changes = { inserted: null, removed: null, modified: null };
                        }

                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(CELLS_INSERT, newOperation);

                        // transfering paragraph and text attributes also to the new inserted cell
                        const container = DOM.getCellContentNode(cellNode); // the cell content node
                        const paragraph = container[0].firstChild; // the first paragraph in the cell determines the attributes for the new cell
                        let paraAttrs = null;
                        let charAttrs = null;

                        if (DOM.isParagraphNode(paragraph)) {
                            paraAttrs = getExplicitAttributeSet(paragraph, 'paragraph');
                            if (DOM.isListLabelNode(paragraph.firstChild)) {
                                charAttrs = getExplicitAttributeSet(paragraph.firstChild.nextSibling, 'character');
                            } else {
                                charAttrs = getExplicitAttributeSet(paragraph.firstChild, 'character');
                            }
                            if (charAttrs && charAttrs.character && charAttrs.character.url) {
                                delete charAttrs.character.url;
                                if (charAttrs.styleId && charAttrs.styleId.toLowerCase() === 'hyperlink') { delete charAttrs.styleId; }
                            }
                        }

                        const paraPosition = _.clone(newCellPosition);
                        paraPosition.push(0);
                        const newParagraphOperationOptions = { start: _.copy(paraPosition) };
                        if (paraAttrs?.paragraph || charAttrs?.character) { newParagraphOperationOptions.attrs = {}; }
                        if (paraAttrs?.paragraph) { newParagraphOperationOptions.attrs.paragraph = paraAttrs.paragraph; }
                        if (charAttrs?.character) { newParagraphOperationOptions.attrs.character = charAttrs.character; }
                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            if (!newParagraphOperationOptions.attrs) { newParagraphOperationOptions.attrs = {}; }
                            newParagraphOperationOptions.attrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null };
                        }
                        this.extendPropertiesWithTarget(newParagraphOperationOptions, target);
                        generator.generateOperation(PARA_INSERT, newParagraphOperationOptions);

                        if (index === 0) { // setting new cursor position
                            newCursorPos = _.clone(paraPosition);
                            newCursorPos.push(0); // this is the first position in the new column
                        }
                    });

                    newOperation = { attrs: { table: { tableGrid } }, start: _.clone(tablePos) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(SET_ATTRIBUTES, newOperation);
                } else {
                    newOperation = { start: _.copy(tablePos), tableGrid, gridPosition, insertMode };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(COLUMN_INSERT, newOperation);

                    // Iterating over all rows to handle change track attributesrrect at cells
                    _.each(allRows, (row, index) => {
                        // the current logical cell position
                        const cellPosition = getCellPositionFromGridPosition(row, gridPosition);
                        // the current cell node
                        const cellClone = $(row).children(DOM.TABLE_CELLNODE_SELECTOR).slice(cellPosition, cellPosition + 1);
                        // the container for the content nodes
                        const container = DOM.getCellContentNode(cellClone);
                        // the first paragraph in the cell determines the attributes for the new cell
                        const paragraph = container[0].firstChild;
                        // the position of the cell inside its row
                        let cellNumber = null;
                        // the logical position of the row
                        const rowPos = Position.getOxoPosition(rootNode, row, 0);
                        // the logical position of the new cell
                        let cellPos = _.clone(rowPos);
                        // the logical position of the new paragraph
                        const paraPos = _.clone(rowPos);

                        if (DOM.isParagraphNode(paragraph) && index === 0) {
                            cellNumber = $(cellClone).index();
                            cellNumber++;
                            paraPos.push(cellNumber);
                            paraPos.push(0);
                            newCursorPos = _.clone(paraPos);
                            newCursorPos.push(0); // as this is first position in new column
                        }

                        // modifying the cell attributes, if changeTracking is activated or if change track attributes need to be removed
                        cellAttrs = null;
                        if (this.getChangeTrack().isActiveChangeTracking()) {
                            cellAttrs = { changes: { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null } };
                        } else if (DOM.isChangeTrackNode(cellClone)) {
                            // change tracking is not active, but reference cell has change track attribute -> this attribute needs to be removed
                            cellAttrs = { changes: { inserted: null, removed: null, modified: null } };
                        }

                        if (cellAttrs !== null) {
                            cellPos = _.clone(rowPos);
                            cellNumber = $(cellClone).index() + 1;
                            cellPos.push(cellNumber);
                            newOperation = { attrs: cellAttrs, start: _.clone(cellPos) };
                            this.extendPropertiesWithTarget(newOperation, target);
                            generator.generateOperation(SET_ATTRIBUTES, newOperation);
                        }
                    });
                }

                // no call of implTableChanged in this.#implInsertColumn
                this.setRequiresElementFormattingUpdate(false);  // no call of implTableChanged -> attributes are already set in implSetAttributes
                this.setGUITriggeredOperation(true);

                // apply all collected operations
                this.applyOperations(generator);

                this.setRequiresElementFormattingUpdate(true);
                this.setGUITriggeredOperation(false);

                // setting the cursor position
                if (newCursorPos) { this.getSelection().setTextSelection(newCursorPos); }

            }, this); // enterUndoGroup()
        }

        /**
         * Merging table cells in a table in the document.
         */
        mergeCells() {

            // the selection object
            const selection = this.getSelection();
            const isCellSelection = selection.getSelectionType() === 'cell';
            const startPos = selection.getStartPosition();
            const endPos = selection.getEndPosition();
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current root container node for element
            const rootNode = this.getCurrentRootNode(target);
            // created operation
            let newOperation = null;

            startPos.pop();  // removing character position and paragraph
            startPos.pop();
            endPos.pop();
            endPos.pop();

            const startCol = startPos.pop();
            const endCol = endPos.pop();
            const startRow = startPos.pop();
            const endRow = endPos.pop();
            const tablePos = _.clone(startPos);
            let endPosition = null;
            const operations = [];

            if (endCol > startCol) {

                for (let i = endRow; i >= startRow; i--) {  // merging for each row

                    const rowPosition = Position.appendNewIndex(tablePos, i);

                    let localStartCol = startCol;
                    let localEndCol = endCol;

                    if (!isCellSelection && (i < endRow) && (i > startCol)) {
                        // merging complete rows
                        localStartCol = 0;
                        localEndCol = Position.getLastColumnIndexInRow(rootNode, rowPosition);
                    }

                    const count = localEndCol - localStartCol;
                    const cellPosition = Position.appendNewIndex(rowPosition, localStartCol);

                    newOperation = { name: CELL_MERGE, start: _.copy(cellPosition), count };
                    this.extendPropertiesWithTarget(newOperation, target);
                    operations.push(newOperation);

                    endPosition = _.clone(cellPosition);
                }

                // apply the operations (undo group is created automatically)
                this.applyOperations(operations);

                endPosition.push(0);
                endPosition.push(0);

                // setting the cursor position
                selection.setTextSelection(endPosition);
            }
        }

        /**
         * Inserting table cells into a table in the document.
         */
        insertCell() {

            // the selection object
            const selection = this.getSelection();
            const isCellSelection = selection.getSelectionType() === 'cell';
            const startPos = selection.getStartPosition();
            const endPos = selection.getEndPosition();
            const count = 1;  // default, adding one cell in each row
            let endPosition = null;
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current root container node for element
            const rootNode = this.getCurrentRootNode(target);
            // created operation
            let newOperation = null;

            startPos.pop();  // removing character position and paragraph
            startPos.pop();
            endPos.pop();
            endPos.pop();

            const startCol = startPos.pop();
            const endCol = endPos.pop();
            const startRow = startPos.pop();
            const endRow = endPos.pop();
            const tablePos = _.copy(startPos, true);
            const attrs = { cell: {} };
            const operations = [];

            for (let i = endRow; i >= startRow; i--) {

                const rowPosition = Position.appendNewIndex(tablePos, i);
                let localEndCol = endCol;

                if (!isCellSelection && (i < endRow) && (i > startCol)) {
                    // removing complete rows
                    localEndCol = Position.getLastColumnIndexInRow(rootNode, rowPosition);
                }

                localEndCol++;  // adding new cell behind existing cell
                const cellPosition = Position.appendNewIndex(rowPosition, localEndCol);
                attrs.cell.gridSpan = 1;  // only 1 grid for the new cell

                newOperation = { name: CELLS_INSERT, start: _.copy(cellPosition), count, attrs };
                this.extendPropertiesWithTarget(newOperation, target);
                operations.push(newOperation);

                // Applying new tableGrid, if the current tableGrid is not sufficient
                const tableDomPoint = Position.getDOMPosition(rootNode, tablePos);
                const rowDomPoint = Position.getDOMPosition(rootNode, rowPosition);

                if (tableDomPoint && DOM.isTableNode(tableDomPoint.node)) {

                    const tableGridCount = this.tableStyles.getElementAttributes(tableDomPoint.node).table.tableGrid.length;
                    const rowGridCount = getColSpanSum($(rowDomPoint.node).children());

                    if (rowGridCount > tableGridCount) {

                        localEndCol--;  // behind is evaluated in getTableGridWithNewColumn
                        const insertmode = 'behind';
                        const tableGrid = getTableGridWithNewColumn(rootNode, tablePos, localEndCol, insertmode);

                        // Setting new table grid attribute to table
                        newOperation = { name: SET_ATTRIBUTES, attrs: { table: { tableGrid } }, start: _.clone(tablePos) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        operations.push(newOperation);
                        this.setRequiresElementFormattingUpdate(false);  // no call of implTableChanged -> attributes are already set in implSetAttributes
                    }

                }

                endPosition = _.clone(cellPosition);
            }

            this.setGUITriggeredOperation(true);

            // apply the operations (undo group is created automatically)
            this.applyOperations(operations);

            this.setGUITriggeredOperation(false);
            this.setRequiresElementFormattingUpdate(true);

            endPosition.push(0);
            endPosition.push(0);

            // setting the cursor position
            selection.setTextSelection(endPosition);
        }

        /**
         * Creates operation to split given table in two tables. Split point is row where the cursor is positioned.
         * If cursor is placed in first row, only paragraph is inserted, and whole table shifted for one position down.
         * Row where the cursor is always goes with second newly created table, being the first row in that table.
         * In between two newly created tables, new paragraph is inserted.
         * If there is table in table(s), and cursor is in that table, only that table is split.
         * ODF doesnt support table split.
         */
        splitTable() {

            // the undo manager object
            const undoManager = this.getUndoManager();

            return undoManager.enterUndoGroup(() => {

                // operations generator
                const generator = this.createOperationGenerator();
                // the selection object
                const selection = this.getSelection();
                //position of the cursor
                const position = selection.getStartPosition();
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();
                // currently active root node
                const activeRootNode = this.getCurrentRootNode(target);
                // element
                const elementNode = Position.getContentNodeElement(activeRootNode, position.slice(0, -1));
                // table node from position
                const tableNode = $(elementNode).closest('table');
                // the position of the 'old' table
                const tablePosition = Position.getOxoPosition(activeRootNode, tableNode);
                // the position of table and row
                const tableAndRowPosition = position.slice(0, tablePosition.length + 1);
                // cursor position in new table after splitting
                const newPosition = _.clone(tableAndRowPosition);
                // attributes for the split table operation
                let attrs = null;
                let operationOptions = {};

                if (DOM.isTableNode(tableNode) && !DOM.isExceededSizeTableNode(tableNode)) { // proceed only if it is table node
                    if (_.last(tableAndRowPosition) !== 0) { // split if its not first row, otherwise just insert new paragraph before
                        operationOptions = { start: _.copy(tableAndRowPosition) };
                        this.extendPropertiesWithTarget(operationOptions, target);
                        generator.generateOperation(TABLE_SPLIT, operationOptions);

                        // update cursor position
                        newPosition[newPosition.length - 2] += 1;
                        newPosition[newPosition.length - 1] = 0;
                    }
                    // Adding change track information only to the inserted paragraph and only, if
                    // change tracking is active.
                    // -> the splitted table cannot be marked as inserted. Rejecting this change track
                    // requires automatic merge of the splitted table, if possible.
                    if (this.getChangeTrack().isActiveChangeTracking()) {
                        attrs = attrs || {};
                        attrs.changes = { inserted: this.getChangeTrack().getChangeTrackInfo(), removed: null };
                    }

                    // #36130 - avoid error in console: Selection.restoreBrowserSelection(): missing text selection range
                    selection.setTextSelection(Position.getFirstPositionInParagraph(activeRootNode, newPosition.slice(0, -1)));

                    operationOptions = { start: _.copy(newPosition.slice(0, -1)) };
                    if (attrs) { operationOptions.attrs = attrs; }
                    this.extendPropertiesWithTarget(operationOptions, target);
                    generator.generateOperation(PARA_INSERT, operationOptions);

                    // apply all collected operations
                    this.applyOperations(generator);
                    // set cursor in new paragraph between split tables
                    selection.setTextSelection(newPosition, null, { simpleTextSelection: false, splitOperation: false });
                }
            }, this);
        }

        /**
         * Creates one table from two neighbour tables. Conditions are that there is no other element between them,
         * both tables have same table style, and same number of columns (table grids). Condition that must be fulfilled,
         * is that union of this two tables doesn't exceed defined number of columns, row, or cells.
         * ODF doesn't support this feature.
         *
         * @param {Number[]} position
         *  OXO position of first table we try to merge
         * @param {Object} [options]
         * Optional parameters:
         *      @param {Boolean} [options.next]
         *      If set to false, we try to merge passed table position with previous element.
         *      Otherwise with next.
         */
        mergeTable(position, options) {

            // the undo manager object
            const undoManager = this.getUndoManager();

            return undoManager.enterUndoGroup(() => {

                // merging is designed to try to merge with next element, so if we are already at next element jump back to previous
                const next = getBooleanOption(options, 'next', true);
                // target for operation - if exists, it's for ex. header or footer
                const target = this.getActiveTarget();
                // currently active root node
                const activeRootNode = this.getCurrentRootNode(target);
                // element
                const elementNode = Position.getContentNodeElement(activeRootNode, position);
                // the 'old' table node
                const tableNode = $(elementNode).closest('table');
                // oxo position of table
                const tablePosition = Position.getOxoPosition(activeRootNode, tableNode);
                // second table node, which we try to merge with first, old one
                let secondTableNode;
                // operations generator
                const generator = this.createOperationGenerator();
                let operationOptions = {};

                // merging is designed to try to merge with next element, so if we are already at next element jump back to previous
                if (!next) {
                    secondTableNode = DOM.getAllowedNeighboringNode(tableNode, { next: false });
                    if (tablePosition[0] !== 0) {
                        tablePosition[0] -= 1;
                    }
                } else {
                    secondTableNode = DOM.getAllowedNeighboringNode(tableNode, { next: true });
                }

                if (DOM.isTableNode(secondTableNode) && mergeableTables(tableNode, secondTableNode)) {
                    // merging is posible only if two tables have the same number of cols and style id
                    operationOptions = { start: _.copy(tablePosition), rowcount: DOM.getTableRows(tableNode).length };
                    this.extendPropertiesWithTarget(operationOptions, target);
                    generator.generateOperation(TABLE_MERGE, operationOptions);
                    // apply all collected operations
                    this.applyOperations(generator);

                    return true;
                }

            }, this);
        }

        // operation handler --------------------------------------------------

        /**
         * The handler for the insertTable operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the table has been inserted successfully.
         */
        insertTableHandler(operation, external) {
            return this.#implInsertTable(operation, external);
        }

        /**
         * The handler for the insertRow operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the row has been inserted successfully.
         */
        insertRowHandler(operation, external) {

            // the undo manager object
            const undoManager = this.getUndoManager();

            if (undoManager.isUndoEnabled() && !external) {
                const count = getIntegerOption(operation, 'count', 1, 1);
                const undoOperations = [];
                let undoOperation = null;

                // TODO: create a single DELETE operation for the row range, once this is supported
                _(count).times(() => {
                    undoOperation = { name: DELETE, start: _.copy(operation.start) };
                    this.extendPropertiesWithTarget(undoOperation, operation.target);
                    undoOperations.push(undoOperation);
                });

                undoManager.addUndo(undoOperations, operation);
            }

            return this.#implInsertRows(operation.start, operation.count, operation.insertDefaultCells, operation.referenceRow, operation.attrs, operation.target);

        }

        /**
         * The handler for the insertColumn operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the column has been inserted successfully.
         */
        insertColumnHandler(operation, external) {

            // the undo manager object
            const undoManager = this.getUndoManager();

            if (undoManager.isUndoEnabled() && !external) {

                undoManager.enterUndoGroup(() => {

                    // COLUMNS_DELETE cannot be the answer to COLUMN_INSERT, because the cells of the new column may be inserted
                    // at very different grid positions. It is only possible to remove the new cells with deleteCells operation.
                    const localPos = _.clone(operation.start);
                    const rootNode = this.getRootNode(operation.target);
                    const table = this.useSlideMode() ? Position.getDOMPosition(rootNode, localPos, true).node : Position.getDOMPosition(rootNode, localPos).node;  // -> this is already the new grid with the new column!
                    const allRows = DOM.getTableRows(table);
                    const allCellInsertPositions = getAllInsertPositions(allRows, operation.gridPosition, operation.insertMode);
                    let cellPosition = null;
                    let undoOperation = {};
                    const expTableAttrs = getExplicitAttributeSet(table);
                    const tableGrid = (expTableAttrs && expTableAttrs.table && _.isArray(expTableAttrs.table.tableGrid)) ? _.copy(expTableAttrs.table.tableGrid) : null;

                    for (let i = (allCellInsertPositions.length - 1); i >= 0; i--) {
                        cellPosition = Position.appendNewIndex(localPos, i);
                        cellPosition.push(allCellInsertPositions[i]);
                        undoOperation = { name: DELETE, start: _.copy(cellPosition) };
                        this.extendPropertiesWithTarget(undoOperation, operation.target);
                        undoManager.addUndo(undoOperation);
                    }

                    // additionally a setAttribute operation for the old table grid is required
                    if (tableGrid) {
                        undoOperation = { name: SET_ATTRIBUTES, attrs: { table: { tableGrid } }, start: _.clone(localPos) };
                        this.extendPropertiesWithTarget(undoOperation, operation.target);
                        undoManager.addUndo(undoOperation);
                    }

                    undoManager.addUndo(null, operation);  // only one redo operation

                }, this); // enterUndoGroup()
            }

            return this.#implInsertColumn(operation.start, operation.gridPosition, operation.tableGrid, operation.insertMode, operation.target);
        }

        /**
         * The handler for the splitTable operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the table was splitted successfully.
         */
        tableSplitHandler(operation, external) {

            // the undo manager object
            const undoManager = this.getUndoManager();

            if (undoManager.isUndoEnabled() && !external) {
                const rowCount = _.last(operation.start);
                const tablePos = operation.start.slice(0, -1);
                const undoOperation = { name: TABLE_MERGE, start: _.copy(tablePos), rowcount: rowCount };

                this.extendPropertiesWithTarget(undoOperation, operation.target);
                undoManager.addUndo(undoOperation, operation);
            }

            return this.#implSplitTable(operation.start, operation.target);
        }

        /**
         * The handler for the mergeTable operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the table was merged successfully.
         */
        tableMergeHandler(operation, external) {
            return this.#implMergeTable(operation, external);
        }

        /**
         * The handler for the mergeCell operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the cells were merged successfully.
         */
        cellMergeHandler(operation, external) {

            // the undo manager object
            const undoManager = this.getUndoManager();

            if (undoManager.isUndoEnabled() && !external) {
                const content = null;
                const gridSpan = null;
                const undoOperation = { name: CELL_SPLIT, start: _.copy(operation.start), content, gridSpan };

                this.extendPropertiesWithTarget(undoOperation, operation.target);
                undoManager.addUndo(undoOperation, operation);
            }

            return this.#implMergeCell(_.copy(operation.start, true), operation.count, operation.target);
        }

        /**
         * The handler for the insertCell operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the table cell were inserted successfully.
         */
        cellInsertHandler(operation, external) {

            // the undo manager object
            const undoManager = this.getUndoManager();

            if (undoManager.isUndoEnabled() && !external) {
                const count = getIntegerOption(operation, 'count', 1, 1);
                const undoOperations = [];
                let undoOperation = null;

                // TODO: create a single DELETE operation for the cell range, once this is supported
                _(count).times(() => {
                    undoOperation = { name: DELETE, start: _.copy(operation.start) };
                    this.extendPropertiesWithTarget(undoOperation, operation.target);
                    undoOperations.push(undoOperation);
                });
                undoManager.addUndo(undoOperations, operation);
            }

            return this.#implInsertCells(operation.start, operation.count, operation.attrs, operation.target);
        }

        // private methods ----------------------------------------------------

        /**
         * Removing all cell contents.
         *
         * @param {HTMLElement|jQuery} cellNode
         *  The cell node, whose content will be replaced by a non-implicit paragraph.
         */
        #implClearCell(cellNode) {

            // the container for the content nodes
            const container = DOM.getCellContentNode(cellNode);
            // the last paragraph in the cell
            const paragraph = DOM.createParagraphNode();

            container.empty().append(paragraph);

            // validate the paragraph (add the dummy node)
            this.validateParagraphNode(paragraph);
            // and NOT formatting the implicit paragraph -> leads to 'jumping' of table after inserting row or column
            // paragraphStyles.updateElementFormatting(paragraph);
        }

        /**
         * Handler for inserting a new table.
         *
         * @param {Object} operation
         *  The insertTable operation.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the table has been inserted successfully.
         */
        #implInsertTable(operation, external) {

            // the new table
            const table = $('<table role="grid">').append($('<colgroup>'));
            // new implicit paragraph behind the table
            let newParagraph = null;
            // number of rows in the table (only for operation.sizeExceeded)
            let tableRows;
            // number of columns in the table (only for operation.sizeExceeded)
            let tableColumns;
            // number of cells in the table (only for operation.sizeExceeded)
            let tableCells;
            // element from which pagebreaks calculus is started downwards
            let currentElement;
            // specifies which part of the table exceeds the size
            let overflowPart;
            // the specific value which exceeds the size
            let overflowValue;
            // the maximal allowed value for the part
            let maxValue;
            // undo operation for handled operation
            let undoOperation = {};

            if (operation.start.length > 2) {
                // if its paragraph creation inside of table
                currentElement = Position.getContentNodeElement(this.getNode(), operation.start.slice(0, 1));
            } else {
                currentElement = Position.getContentNodeElement(this.getNode(), operation.start);
            }

            // insertContentNode() writes warning to console
            if (!this.insertContentNode(_.clone(operation.start), table, operation.target)) { return false; }

            // generate undo/redo operations
            if (this.getUndoManager().isUndoEnabled() && !external) {
                undoOperation = { name: DELETE, start: _.copy(operation.start) };
                this.extendPropertiesWithTarget(undoOperation, operation.target);
                this.getUndoManager().addUndo(undoOperation, operation);
            }

            // Special replacement setting for a table, that cannot be displayed because of its size.
            if (_.isObject(operation.sizeExceeded)) {

                tableRows = getIntegerOption(operation.sizeExceeded, 'rows', 0);
                tableColumns = getIntegerOption(operation.sizeExceeded, 'columns', 0);
                tableCells = tableRows * tableColumns;

                if ((tableRows > 0) && (tableColumns > 0)) {

                    if (tableRows > MAX_TABLE_ROWS) {
                        overflowPart = 'rows';
                        overflowValue = tableRows;
                        maxValue = MAX_TABLE_ROWS;
                    } else if (tableColumns > MAX_TABLE_COLUMNS) {
                        overflowPart = 'cols';
                        overflowValue = tableColumns;
                        maxValue = MAX_TABLE_COLUMNS;
                    } else if (tableCells > MAX_TABLE_CELLS) {
                        overflowPart = 'cols';
                        overflowValue = tableCells;
                        maxValue = MAX_TABLE_CELLS;
                    }
                }
                DOM.makeExceededSizeTable(table, overflowPart, overflowValue, maxValue);
                table.data('attributes', operation.attrs);  // setting attributes from operation directly to data without using 'setElementAttributes'

            // apply the passed table attributes
            } else if (_.isObject(operation.attrs) && _.isObject(operation.attrs.table)) {

                // We don't want operations to create operations themself, so we can't
                // call 'checkForLateralTableStyle' here.

                if (this.getBlockKeyboardEvent()  && _.browser.Firefox) { table.data('internalClipboard', true); }  // Fix for task 29401
                this.tableStyles.setElementAttributes(table, operation.attrs);
            }

            if (operation.target) {
                // trigger header/footer content update on other elements of same type, if change was made inside header/footer
                if (DOM.isMarginalNode(table) || DOM.getMarginalTargetNode(this.getNode(), table).length) {
                    table.addClass(DOM.MARGINAL_NODE_CLASSNAME);
                    this.updateEditingHeaderFooterDebounced(DOM.getMarginalTargetNode(this.getNode(), table));
                }
                this.setBlockOnInsertPageBreaks(true);
            }

            // call pagebreaks re-render after inserting table
            this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(table));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            // adding an implicit paragraph behind the table (table must not be the last node in the document)
            if (table.next().length === 0) {
                newParagraph = DOM.createImplicitParagraphNode();
                this.validateParagraphNode(newParagraph);
                // insert the new paragraph behind the existing table node
                table.after(newParagraph);
                this.implParagraphChanged(newParagraph);
            } else if (DOM.isImplicitParagraphNode(table.next())) {
                if (DOM.isTableInTableNode(table)) {
                    table.next().css('height', 0);  // hiding the paragraph behind the table inside another table
                }
            }

            if (this.isUndoRedoRunning()  && _.browser.Firefox) { table.data('undoRedoRunning', true); }  // Fix for task 30477

            return true;
        }

        /**
         * The hander function that inserts row(s) into a table in the DOM.
         *
         * @param {Number[]} start
         *  The logical position for inserting the column.
         *
         * @param {Number} [count=1]
         *  The number of rows to be inserted.
         *
         * @param {Boolean} insertDefaultCells
         *  Whether default cells shall be inserted in the new rows.
         *
         * @param {Number} [referenceRow]
         *  Whether a specified reference row can be used for the new row.
         *
         * @param {Object} [attrs]
         *  An object with the row attributes.
         *
         * @param {String} [target]
         *  The target corresponding to the logical position.
         *
         * @returns {Boolean}
         *  Whether the table column has been inserted successfully.
         */
        #implInsertRows(start, count, insertDefaultCells, referenceRow, attrs, target) {

            const localPosition = _.copy(start, true);
            const useReferenceRow = _.isNumber(referenceRow);
            let newRow = null;
            const rootNode = this.getRootNode(target);
            let odpDefaultParaAttrs = null; // some default paragraph attributes used for ODP
            let table = null; // the table node
            let tableBodyNode = null; // the table body node
            const currentElement = Position.getContentNodeElement(this.getNode(), start.slice(0, 1));

            if (!Position.isPositionInTable(rootNode, localPosition)) {
                return false;
            }

            if (!_.isNumber(count)) {
                count = 1; // setting default for number of rows
            }

            const tablePos = _.copy(localPosition, true);
            tablePos.pop();

            if (this.useSlideMode()) {
                table = Position.getDOMPosition(rootNode, tablePos, true).node; // returns the drawing that contains the table node
                if (isTableDrawingFrame(table)) {
                    table = $(table).find('table'); // getting the table node inside the table drawing
                }
            } else {
                table = Position.getDOMPosition(rootNode, tablePos).node;
            }

            const tableRowDomPos = Position.getDOMPosition(rootNode, localPosition);
            let tableRowNode = null;
            let row = null;
            let cellsInserted = false;

            if (!table) { return false; } // not a valid table position specified

            if (this.isGUITriggeredOperation()) {  // called from 'insertRow' -> temporary branding for table
                $(table).data('gui', 'insert');
            }

            tableBodyNode = $(table).children('tbody');

            if (tableBodyNode.length === 0) {
                tableBodyNode = $('<tbody>');
                $(table).append(tableBodyNode); // the <tbody> node is no longer inserted automatically with jQuery 3
            }

            if (tableRowDomPos) {
                tableRowNode = tableRowDomPos.node;
            }

            if (useReferenceRow) {

                if (this.isGUITriggeredOperation()) {
                    // Performance: Called from 'insertRow' -> simple table do not need new formatting.
                    // Tables with conditional styles only need to be updated below the referenceRow
                    // (and the row itself, too, for example southEast cell may have changed)

                    // Bugfix 44230: introduced a check to prevent wrong values for '.data('reducedTableFormatting')' in updateRowFormatting(), when inserting
                    // rows very quickly via button, so that more than one rows are inserted before the debounced updateRowFormatting() is executed
                    if (_.isNumber($(table).data('reducedTableFormatting'))) {
                        $(table).data('reducedTableFormatting', Math.min(referenceRow, $(table).data('reducedTableFormatting')));
                    } else {
                        $(table).data('reducedTableFormatting', referenceRow);
                    }
                }

                row = DOM.getTableRows(table).eq(referenceRow).clone(true);

                if (!row || row.length === 0) { return false; } // not hiding errors in operations

                // clear the cell contents in the cloned row
                row.children('td').get().forEach(oneCell => { this.#implClearCell(oneCell); });

                // cells with list in the first paragraph, should automatically receive this list information, too
                // checking the cell contents in the cloned row
                _.each(DOM.getTableRows(table).eq(referenceRow).children('td'), (td, index) => {
                    // the container for the content nodes
                    const container = DOM.getCellContentNode(td);
                    // the first paragraph in the cell determines the attributes for the new cell
                    const paragraph = container[0].firstChild;
                    // the paragraph attributes
                    let paraAttributes = null;
                    // character attrs of first paragraph
                    let charAttrs = null;
                    // td node of newly created row, with coresponding index as prev row
                    const newTd = row.children('td').eq(index);
                    // cell content node of newly created row, with coresponding index as prev row
                    const newContainer = DOM.getCellContentNode(newTd);
                    // paragraph node of newly created row, with coresponding index as prev row
                    const newPara = newContainer[0].firstChild;

                    if (DOM.isParagraphNode(paragraph)) {
                        paraAttributes = getExplicitAttributeSet(paragraph);
                        if (DOM.isListLabelNode(paragraph.firstChild)) {
                            charAttrs = getExplicitAttributeSet(paragraph.firstChild.nextSibling);
                        } else {
                            charAttrs = getExplicitAttributeSet(paragraph.firstChild);
                        }

                        if (!_.isEmpty(paraAttributes)) { this.paragraphStyles.setElementAttributes(newPara, paraAttributes); }
                        if (!_.isEmpty(charAttrs)) {
                            if (charAttrs.character && charAttrs.character.url) {
                                delete charAttrs.character.url;
                                if (charAttrs.styleId && charAttrs.styleId.toLowerCase() === 'hyperlink') { delete charAttrs.styleId; }
                            }
                            this.characterStyles.setElementAttributes(newPara.firstChild, charAttrs);
                        }

                        // mark this paragraph for later list update. This is necessary, because it does not yet contain
                        // the list label node (DOM.LIST_LABEL_NODE_SELECTOR) and is therefore ignored in updateLists.
                        if (this.isImportFinished() && this.handleTriggeringListUpdate(newPara, { paraInsert: true })) { $(newPara).data('updateList', 'true'); }
                    }
                });

                cellsInserted = true;

            } else if (insertDefaultCells) {

                const columnCount = getColumnCount(table);
                // prototype elements for row, cell, and paragraph
                const paragraph = DOM.createParagraphNode(); // no implicit paragraph
                let cell = null;

                if (this.#docApp.isPresentationApp() && this.#docApp.isODF()) { // 55193
                    odpDefaultParaAttrs = { paragraph: { indentFirstLine: 0, indentLeft: 0, bullet: { type: 'none' } } };
                    this.paragraphStyles.setElementAttributes(paragraph, odpDefaultParaAttrs);
                }

                cell = DOM.createTableCellNode(paragraph);

                // insert empty text node into the paragraph
                this.validateParagraphNode(paragraph);

                row = $('<tr>').attr('role', 'row');

                // clone the cells in the row element
                _.times(columnCount, () => { row.append(cell.clone(true).attr('role', 'gridcell')); });

                cellsInserted = true;

            } else {
                row = $('<tr>').attr('role', 'row');
            }

            _.times(count, () => {
                newRow = row.clone(true);

                // apply the passed attributes
                if (_.isObject(attrs)) { this.tableRowStyles.setElementAttributes(newRow, attrs); }

                if (tableRowNode) {
                    // insert the new row before the existing row at the specified position
                    $(tableRowNode).before(newRow);
                } else {
                    // append the new row to the table
                    $(tableBodyNode).append(newRow);
                }
            });

            // recalculating the attributes of the table cells
            if (cellsInserted && this.requiresElementFormattingUpdate()) { this.implTableChanged(table); }

            // Setting cursor
            if (insertDefaultCells || useReferenceRow) {
                localPosition.push(0);
                localPosition.push(0);
                localPosition.push(0);

                this.setLastOperationEnd(localPosition);
            }

            // if operation is not targeted, render page breaks
            if (target) {
                if (DOM.isMarginalNode(table) || DOM.getMarginalTargetNode(this.getNode(), table).length) {
                    $(table).find('tr, td, div, span').addClass(DOM.MARGINAL_NODE_CLASSNAME);
                }
                this.setBlockOnInsertPageBreaks(true);
            }

            this.setQuitFromPageBreaks(true); // quit from any currently running pagebreak calculus - performance optimization
            this.insertPageBreaks(currentElement, getClosestTextFrameDrawingNode(table));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            return true;
        }

        /**
         * The hander function that inserts the column into a table in the DOM.
         *
         * @param {Number[]} start
         *  The logical position for inserting the column.
         *
         * @param {Number} gridPosition
         *  The index of the new column inside the table grid.
         *
         * @param {Number[]} tableGrid
         *  The complete array of relative widths for the entire table, containing the
         *  new entry for the new column. Will be set to the tableGrid attribute of the
         *  table.
         *
         * @param {String} [insertMode]
         *  The insert mode determines whether the column is inserted behind or before
         *  an existing column. Currently only the value 'behind' is evaluated. In all
         *  other cases, the new column is inserted before an existing column.
         *
         * @param {String} [target]
         *  The target corresponding to the logical position.
         *
         * @returns {Boolean}
         *  Whether the table column has been inserted successfully.
         */
        #implInsertColumn(start, gridPosition, tableGrid, insertMode, target) {

            const localPosition = _.copy(start, true);
            const rootNode = this.getRootNode(target);
            let table = null;
            let tableDrawingNode = null;
            let allRows = null;
            // whether the presentation model needs to be used
            const isSlideMode = this.useSlideMode();

            if (!Position.isPositionInTable(rootNode, localPosition)) {
                return false;
            }

            table = isSlideMode ? Position.getDOMPosition(rootNode, localPosition, true).node : Position.getDOMPosition(rootNode, localPosition).node;
            allRows = DOM.getTableRows(table);

            if (isSlideMode && isTableDrawingFrame(table)) {
                tableDrawingNode = table;
                table = $(table).find('table');
            }

            if (this.isGUITriggeredOperation()) {  // called from 'insertColumn' -> temporary branding for table
                $(table).data('gui', 'insert');
            }

            _.each(allRows, row => {

                const cellPosition = getCellPositionFromGridPosition(row, gridPosition);
                const cellNode = $(row).children(DOM.TABLE_CELLNODE_SELECTOR).eq(cellPosition);
                const expCellAttributes = getExplicitAttributes(cellNode, 'cell');
                const isMergedCell = expCellAttributes && _.isNumber(expCellAttributes.gridSpan) && expCellAttributes.gridSpan > 1;
                // gridPos = null;
                let cellClone = null;
                // increaseCellLength = false;

                // -> this process simulates the PowerPoint behavior.
                // -> merged table cells are increased in length, no new table cell is inserted
                //                if (isSlideMode) {
                //
                //                    if (isMergedCell) {
                //
                //                        gridPos = getGridPositionFromCellPosition(row, cellPosition);
                //
                //                        if ((insertMode === 'behind' && gridPos.end > gridPosition) || (insertMode !== 'behind' && gridPos.start < gridPosition)) {
                //                            increaseCellLength = true;
                //                        }
                //                    }
                //
                //                    if (increaseCellLength) {
                //                        // apply the passed table cell attributes
                //                        this.tableCellStyles.setElementAttributes(cellNode, { cell: { gridSpan: (expCellAttributes.gridSpan + 1) } });
                //
                //                    } else {
                //                        // inserting a new cell
                //                        cellClone = cellNode.clone(true);
                //                        this.#implClearCell(cellClone);
                //
                //                        if (insertMode === 'behind') {
                //                            cellClone.insertAfter($(row).children().get(cellPosition));
                //                        } else {
                //                            cellClone.insertBefore($(row).children().get(cellPosition));
                //                        }
                //
                //                        if (isMergedCell) {
                //                            this.tableCellStyles.setElementAttributes(cellClone, { cell: { gridSpan: 1 } });
                //                        }
                //                    }
                //
                //                } else {

                // this behavior is the OX Text behavior, that client and filter also support in OX Presentation

                cellClone = cellNode.clone(true);

                this.#implClearCell(cellClone);

                if (insertMode === 'behind') {
                    cellClone.insertAfter($(row).children().get(cellPosition));
                } else {
                    cellClone.insertBefore($(row).children().get(cellPosition));
                }

                if (isMergedCell) {
                    this.tableCellStyles.setElementAttributes(cellClone, { cell: { gridSpan: 1 } });
                }

                // }

                if (this.isImportFinished()) {
                    // the container for the content nodes
                    const container = DOM.getCellContentNode(cellNode);
                    // the first paragraph in the cell determines the attributes for the new cell
                    const paragraph = container[0].firstChild;
                    // the paragraph attributes
                    let paraAttrs = null;
                    // character attrs of first paragraph
                    let charAttrs = null;
                    // cell container in newly created column
                    const newContainer = DOM.getCellContentNode(cellClone);
                    // paragraph in cell of newly created column
                    const newPara = (newContainer && newContainer.length) ? newContainer[0].firstChild : null;

                    if (DOM.isParagraphNode(paragraph)) {
                        paraAttrs = getExplicitAttributeSet(paragraph);
                        if (DOM.isListLabelNode(paragraph.firstChild)) {
                            charAttrs = getExplicitAttributeSet(paragraph.firstChild.nextSibling);
                        } else {
                            charAttrs = getExplicitAttributeSet(paragraph.firstChild);
                        }
                    }
                    // no operations needed, as filter does the same implicitely on insertColumn
                    if (!_.isEmpty(paraAttrs) && newPara) { this.paragraphStyles.setElementAttributes(newPara, paraAttrs); }
                    if (!_.isEmpty(charAttrs) && newPara?.firstChild) {
                        if (charAttrs?.character?.url) {
                            delete charAttrs.character.url;
                            if (charAttrs.styleId && charAttrs.styleId.toLowerCase() === 'hyperlink') { delete charAttrs.styleId; }
                        }
                        this.characterStyles.setElementAttributes(newPara.firstChild, charAttrs);
                    }

                    // mark this paragraph for later list update. This is necessary, because it does not yet contain
                    // the list label node (DOM.LIST_LABEL_NODE_SELECTOR) and is therefore ignored in updateLists.
                    if (this.isImportFinished() && this.handleTriggeringListUpdate(newPara, { paraInsert: true })) { $(newPara).data('updateList', 'true'); }
                }

            });

            // setting new tableGrid attribute
            this.tableStyles.setElementAttributes(table, { table: { tableGrid } });
            if (tableDrawingNode) { this.drawingStyles.setElementAttributes(tableDrawingNode, { table: { tableGrid } }); }

            // recalculating the attributes of the table cells
            // if (this.requiresElementFormattingUpdate()) {
            this.implTableChanged(table);
            // }

            // Setting cursor to first position in table
            localPosition.push(0);
            localPosition.push(gridPosition);
            localPosition.push(0);
            localPosition.push(0);

            this.setLastOperationEnd(localPosition);

            return true;
        }

        /**
         * After splitting a table with place holders in the new table (that
         * was cloned before), it is necessary to update the place holder nodes in the
         * models. Also range marker and complex field models need to be updated.
         *
         * @param {jQuery} table
         *  The table node.
         */
        #updateAllModelsInNewTable(table) {
            // iterating over all paragraphs
            _.each(table.find(DOM.PARAGRAPH_NODE_SELECTOR), para => { this.updateAllModels(para); });
        }

        /**
         * Splitting a table at a specified logical position.
         *
         * @param {Number[]} position
         *  The logical position at which the table shall be splitted.
         *
         * @param {String[]} target
         *  ID of target root node for operation
         *
         * @returns {Boolean}
         *  Whether the table has been splitted successfully.
         */
        #implSplitTable(position, target) {

            const positionRowToSplit = _.last(position);
            // the position of the 'old' table
            const tablePosition = position.slice(0, -1);
            // root node
            const rootNode = this.getRootNode(target);
            // the 'old' table node
            const tableNode = Position.getContentNodeElement(rootNode, tablePosition);
            // the position of the 'new' table
            //newTablePosition = Position.increaseLastIndex(tablePosition),
            const newTableNode = $(tableNode).clone(true);
            // collection of first level table rows, excluding rows containing page break
            const tableNodeRows = DOM.getTableRows(tableNode);
            // collection of first level new table rows, excluding rows containing page break
            const newTableNodeRows = DOM.getTableRows(newTableNode);

            // if we clone table with manual page break, it should not be inherited in new table
            if (newTableNode.hasClass('manual-page-break')) {
                newTableNode.find(DOM.MANUAL_PAGE_BREAK_SELECTOR).removeClass('manual-page-break');
                newTableNode.removeClass('manual-page-break');
            }
            // remove ending rows from old table
            $(tableNodeRows[positionRowToSplit]).nextAll('tr').addBack().remove();
            // remove begining rows from new table
            $(newTableNodeRows[positionRowToSplit]).prevAll('tr').remove();
            newTableNode.insertAfter($(tableNode));

            this.implTableChanged(tableNode);
            this.implTableChanged(newTableNode);

            // all drawings in the new table need to be reformatted (DOCS-2813)
            _.each(newTableNode.find('.drawing'), drawing => { this.drawingStyles.updateElementFormatting(drawing); });

            this.#updateAllModelsInNewTable(newTableNode);

            // new cursor position at merge position
            this.setLastOperationEnd(_.clone(Position.getFirstPositionInParagraph(rootNode, position)));

            // run page break render if operation is not targeted
            if (target) {
                this.setBlockOnInsertPageBreaks(true);
            }

            this.insertPageBreaks(tableNode, getClosestTextFrameDrawingNode(tableNode));
            if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

            return true;
        }

        /**
         * Merging two tables.
         *
         * @param {Object} operation - Contains name of the operation, start - the logical position
         *  of table from where merging should start, and optionally target ID of root node
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the tables are merged successfully.
         */
        #implMergeTable(operation, external) {

            // the position of the 'old' table
            const tablePosition = operation.start;
            // currently active root node
            const rootNode = this.getRootNode(operation.target);
            // the 'old' table node
            const tableNode = Position.getContentNodeElement(rootNode, tablePosition);
            // new table node, next to old table
            const nextTableNode = DOM.getAllowedNeighboringNode(tableNode, { next: true });
            // cached collection of rows for next table
            const nextTableNodeRows = DOM.getTableRows(nextTableNode);
            // position of next table
            const nextTablePosition = _.clone(tablePosition);
            // information for table split, if undo is called:
            // position of last row inside table
            let rowPosInTable;
            // helper for stored value of row position in iteration
            let rowPosition;
            // helper for stored value of cell position in iteration
            let cellPosition;
            // the undo manager object
            const undoManager = this.getUndoManager();
            //generator of operations
            const generator = undoManager.isUndoEnabled() && !external ? this.createOperationGenerator() : null;
            // created operation
            let newOperation = null, newOperation2 = null, newOperation3 = null, newOperation4 = null;

            nextTablePosition[nextTablePosition.length - 1] += 1;
            // calculate last row position from current table, used for undo table split operation
            rowPosInTable = Position.getLastRowIndexInTable(rootNode, tablePosition);
            if (rowPosInTable > -1) {
                // increased by 1 at the end, because split starts from above of passed row
                rowPosInTable += 1;
            } else { // invalid value
                rowPosInTable = 0;
            }

            // logical position of table and its last row
            const tableAndRowPosition = Position.appendNewIndex(tablePosition, rowPosInTable);

            if (DOM.isTableNode(nextTableNode)) {
                if (generator) {
                    newOperation = { start: _.copy(tableAndRowPosition) };
                    this.extendPropertiesWithTarget(newOperation, operation.target);
                    generator.generateOperation(TABLE_SPLIT, newOperation);
                    newOperation2 = { start: _.copy(nextTablePosition) };
                    this.extendPropertiesWithTarget(newOperation2, operation.target);
                    generator.generateSetAttributesOperation(nextTableNode, newOperation2); // generate undo operation to set attributes to table
                    //generate set attributes for rows and cells
                    _.each(nextTableNodeRows, (row, index) => {
                        rowPosition = _.clone(nextTablePosition);
                        rowPosition.push(index);
                        newOperation3 = { start: _.copy(rowPosition) };
                        this.extendPropertiesWithTarget(newOperation3, operation.target);
                        generator.generateSetAttributesOperation(row, newOperation3);
                        _.each(row.cells, (cell, index) => {
                            cellPosition = _.clone(rowPosition);
                            cellPosition.push(index);
                            newOperation4 = { start: _.copy(cellPosition) };
                            this.extendPropertiesWithTarget(newOperation4, operation.target);
                            generator.generateSetAttributesOperation(cell, newOperation4);
                        });
                    });
                    undoManager.addUndo(generator.getOperations(), operation);
                }

                $(tableNode).find('> tbody').last().append($(nextTableNodeRows));
                $(nextTableNode).remove();

                this.implTableChanged(tableNode);

                // new cursor position at merge position
                this.setLastOperationEnd(_.clone(Position.getFirstPositionInParagraph(rootNode, tablePosition)));

                // run page break render if operation is not targeted
                if (operation.target) {
                    this.setBlockOnInsertPageBreaks(true);
                }

                this.insertPageBreaks(tableNode, getClosestTextFrameDrawingNode(tableNode));
                if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

                return true;
            } else {
                globalLogger.error('this.#implMergeTable: Element following the table is not a table.');
                return false;
            }
        }

        /**
         * The hander function that merges table cells in the DOM.
         *
         * @param {Number[]} start
         *  The logical position of the row.
         *
         * @param {Number} count
         *  The number of cells to be merged.
         *
         * @param {String} [target]
         *  The target corresponding to the logical position.
         *
         * @returns {Boolean}
         *  Whether the cells were merged successfully.
         */
        #implMergeCell(start, count, target) {

            const rowPosition = _.copy(start, true);
            const localStartCol = rowPosition.pop();
            const localEndCol = localStartCol + count;
            const rootNode = this.getRootNode(target);
            // Counting the colSpan off all cells in the range
            const row = Position.getDOMPosition(rootNode, rowPosition).node;
            const allSelectedCells = $(row).children().slice(localStartCol, localEndCol + 1);
            const colSpanSum = getColSpanSum(allSelectedCells);
            // Shifting the content of all following cells to the first cell
            const targetCell = $(row).children().slice(localStartCol, localStartCol + 1);
            const sourceCells = $(row).children().slice(localStartCol + 1, localEndCol + 1);

            shiftCellContent(targetCell, sourceCells);

            sourceCells.remove();

            // apply the passed table cell attributes
            this.tableCellStyles.setElementAttributes(targetCell, { cell: { gridSpan: colSpanSum } });

            return true;
        }

        /**
         * The hander function that inserting table cells in the DOM.
         *
         * @param {Number[]} start
         *  The logical start position.
         *
         * @param {Number} count
         *  The number of cells to be inserted.
         *
         * @param {Object} [attrs]
         *  Some optional table cell attributes.
         *
         * @param {String} [target]
         *  The target corresponding to the logical position.
         *
         * @returns {Boolean}
         *  Whether the cells were inserted successfully.
         */
        #implInsertCells(start, count, attrs, target) {

            const localPosition = _.clone(start);
            let tableCellDomPos = null;
            let tableCellNode = null;
            let paragraph = null;
            let cell = null;
            let row = null;
            const rootNode = this.getRootNode(target);
            const selector = this.useSlideMode() ? DOM.TABLE_NODE_AND_DRAWING_SELECTOR : DOM.TABLE_NODE_SELECTOR;
            let tableNode = Position.getLastNodeFromPositionByNodeName(rootNode, start, selector);

            if (!tableNode) {
                return false;
            }

            if (isTableDrawingFrame(tableNode)) {
                // the table is a drawing, but the table node is required
                tableNode = $(tableNode).find('table');
            }

            if (!_.isNumber(count)) {
                count = 1; // setting default for number of rows
            }

            tableCellDomPos = Position.getDOMPosition(rootNode, localPosition);

            if (tableCellDomPos) {
                tableCellNode = tableCellDomPos.node;
            }

            // prototype elements for row, cell, and paragraph
            paragraph = DOM.createImplicitParagraphNode();  // must be implicit because of undo-operation

            // insert empty text node into the paragraph
            this.validateParagraphNode(paragraph);

            cell = DOM.createTableCellNode(paragraph);

            // apply the passed table cell attributes
            if (_.isObject(attrs)) {
                this.tableCellStyles.setElementAttributes(cell, attrs);
            }

            if (tableCellNode) {
                _.times(count, () => { $(tableCellNode).before(cell.clone(true)); });
                row = $(tableCellNode).parent();
            } else {
                const rowPos = localPosition.slice(0, -1);
                row = Position.getDOMPosition(rootNode, rowPos).node;
                if (!row) { return false; } // invalid row position
                // check, if this specified position is valid
                const rowGridCount = getColSpanSum($(row).children());
                if (_.last(start) > rowGridCount) { return false; } // invalid position for the table cells (0 based)
                // appending the cells to the end of the row
                _.times(count, () => { $(row).append(cell.clone(true)); });
            }

            // setting cursor to first paragraph in the cell
            localPosition.push(0);
            localPosition.push(0);
            this.setLastOperationEnd(localPosition);

            // recalculating the attributes of the table cells
            if (this.requiresElementFormattingUpdate()) {
                this.implTableChanged(tableNode);
            }

            return true;
        }

    }

    return TableOperationMixin;
}

// exports ====================================================================

export const XTableOperation = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XTableOperation>}
     *  The extended class with table operation functionality.
     */
    mixin: mixinTableOperation
};
