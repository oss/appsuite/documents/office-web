/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { NODE_SELECTOR, getClosestTextFrameDrawingNode, getDrawingType, isDrawingFrame, isNoWordWrapDrawingFrame,
    isTableDrawingFrame, isTextFrameNode, removeDrawingSelection  } from '@/io.ox/office/drawinglayer/view/drawingframe';
import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { DELETE_COLUMNS_QUERY, DELETE_COLUMNS_TITLE, DELETE_CONTENTS_QUERY, DELETE_CONTENTS_TITLE, DELETE_ROWS_QUERY,
    DELETE_ROWS_TITLE } from '@/io.ox/office/editframework/view/editlabels';
import { getAllRemovePositions, getCellPositionFromGridPosition, getColumnCount,
    getGridColumnRangeOfCell, getGridPositionFromCellPosition, getRowCount,
    getHorizontalCellLength, isHorizontallyMergedCell } from '@/io.ox/office/textframework/components/table/table';
import * as DOM from '@/io.ox/office/textframework/utils/dom';
import * as Position from '@/io.ox/office/textframework/utils/position';
import { COLUMNS_DELETE, DELETE, PARA_MERGE, SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import Snapshot from '@/io.ox/office/textframework/utils/snapshot';
import { BREAK, SMALL_DEVICE, getBooleanOption, getObjectOption, getOption, getStringOption,
    handleParagraphIndex, mergeSiblingTextSpans } from '@/io.ox/office/textframework/utils/textutils';
import { ary } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { IOS_SAFARI_DEVICE } from '@/io.ox/office/tk/dom';

function mixinDeleteOperation(BaseClass) {

    // mix-in class DeleteOperationMixin ======================================

    /**
     * A mix-in class for the document model class providing the operation
     * handling for deleting all kind of content used in a presentation
     * and text document.
     */

    class DeleteOperationMixin extends BaseClass {

        // the application object
        #docApp = null;
        // whether the delete operation was triggerd by backspace or delete
        #deleteOrBackSpaceKeyPressed = false;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Generates the operations that will delete the current selection, and
         * executes the operations.
         * Info: With the introduction of 'rangeMarker.handlePartlyDeletedRanges' it is
         * now possible, that not only the end position of the current selection is
         * modified within 'deleteSelected', but also the start position. This happens
         * for example, if the selection includes an end range marker. In this case the
         * corresponding start range marker is also removed. If this is located in the
         * same paragraph as the current selection start position, this position will
         * also be updated. Therefore after executing 'deleteSelected', it is necessary
         * that the start position is read from the selection again without relying on
         * the value before executing 'deleteSelected'.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.alreadyPasteInProgress=false]
         *      Whether deleteSelected was triggered from the pasting of clipboard. In
         *      this case the check function 'checkSetClipboardPasteInProgress()' does
         *      not need to be called, because blocking is already handled by the
         *      clipboard function.
         *  @param {Boolean} [options.deleteKey=false]
         *      Whether this function was triggered by the 'delete' or 'backspace' key.
         *      Or whether it was triggered via the controller function by tool bar or
         *      context menu.
         *  @param {Object} [options.snapshot]
         *      The snapshot that can be used, if the user cancels a long running
         *      delete action. Typically this is created inside this function, but if
         *      it was already created by the calling 'paste' function, it can be reused
         *      here.
         *  @param {Boolean} [options.saveStartAttrs=false]
         *      Whether the character attributes at the start position shall be saved.
         *      This attributes can be used later, if text is inserted after the content
         *      was deleted and this new text shall have the same character attributes.
         *  @param {Boolean} [options.onlyCreateOperations=false]
         *      Whether the operations shall only be created, but not applied.
         *  @param {Boolean} [options.keepParagraphs=false]
         *      Whether paragraphs shall not be deleted completely, so that the document
         *      structure is not modified. In this scenario the shortest path must be
         *      avoided, so that all paragraphs inside a table are visited and the table
         *      is not deleted completely.
         *  @param {String} [options.warningLabel=null]
         *      Custom definied warning message during delete dialog.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        deleteSelected(options) {

            // the operations generator
            const generator = this.createOperationGenerator();
            // generator for inserting empty placeholder drawings (Presentation app only)
            let insertDrawingGenerator = null;
            // generator for setAttributes operations for ancestors of place holder drawings (Presentation app only)
            let placeHolderAttrsGenerator = null;
            // the logical position of the first and current partially covered paragraph
            let firstParaPosition = null, currentParaPosition = null;
            // an optional new start position that can be caused by additionally removed start range nodes outside the selection range
            let newStartPosition = null;
            // a helper paragraph for setting attributes
            let paragraph = null;
            // the selection object
            const selection = this.getSelection();
            // whether the selection is a rectangular cell selection
            const isCellSelection = selection.getSelectionType() === 'cell';
            // whether the selection is a multi drawing selection
            const isMultiDrawingSelection = selection.isMultiSelectionSupported() && selection.isMultiSelection();
            // whether the selection is a single drawing selection
            const isSingleDrawingSelection = !isMultiDrawingSelection && selection.isDrawingSelection();
            // the logical position, at which an absolute positioned drawing is located (52803)
            let oneDrawingPosition = null;
            // whether it is necessary to ask the user before deleting content
            let askUser = false;
            // whether an additional merge operation is required
            let doMergeParagraph = false;
            // the paragraph at the end of the selection
            let selectionEndParagraph = null;
            // the promise for generating the operations
            let operationGeneratorPromise = null;
            // the promise for the asynchronous execution of operations
            let operationsPromise = null;
            // a node counter for the iterator function
            let counter = 0;
            // the first and last node of iteration
            let firstNode = null, lastNode = null, currentNode = null;
            // whehter a node is the first node of a selection
            let isFirstNode = false;
            // wheter the first or last paragraph of a selection are only partly removed
            let firstParaRemovedPartly = false, lastParaRemovedPartly = false;
            // the merge position for the first paragraph
            let firstParaMergePos = 0;
            // whether it is necessary to check, if the last paragraph of the selection need to be merged
            let checkMergeOfLastParagraph = false;
            // changeTrack: Whether the content was really deleted (inserted by same user) or only attributes were set
            let changeTrackDeletedContent = false;
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current element container node
            const rootNode = this.getCurrentRootNode(target);
            // whether it is required to check for range markers inside the selection
            const isRangeCheckRequired = !this.getRangeMarker().isEmpty();
            // all classes used to identify the range markers
            let allRangeMarkerSelector = isRangeCheckRequired ? DOM.RANGEMARKERNODE_SELECTOR : null;
            // the collector for all range marker nodes (including comments)
            let allRangeMarkerNodes = $();
            // a container for all IDs of removed comments (OT)
            const allRemovedCommentIDs = [];
            // created operation
            let newOperation = null;
            // whether a progress bar shall be made visible (this might already be done by an outer function)
            const alreadyPasteInProgress = getBooleanOption(options, 'alreadyPasteInProgress', false);
            // whether this function was triggered by pressing 'delete' or 'backspace'
            const deleteKeyPressed = getBooleanOption(options, 'deleteKey', false);
            // whether the character attributes at the start position need to be saved for later usage
            const saveStartAttrs = getBooleanOption(options, 'saveStartAttrs', false);
            // whether a valid snapshot was already created by the caller of this function (typically a 'paste' action over a selection range)
            let snapshot = getObjectOption(options, 'snapshot', null);
            // whether a custom defined message during delete is displayed or default one
            const warningLabel = getStringOption(options, 'warningLabel', gt('Sorry, deleting content will take some time.'));
            // whether paragraphs must not be deleted completely
            const keepParagraphs = getBooleanOption(options, 'keepParagraphs', false);
            // whether the snapshot was created inside this function
            let snapshotCreated = false;
            // whether the deleting will be done asynchronous (in large selections)
            let asyncDelete = false;
            // the ratio of operation generation to applying of operation
            const operationRatio = 0.3;
            // an optional array with selection ranges for large selections
            let splittedSelectionRange = null;
            // whether the user aborted the delete process
            let userAbort = false;
            // whether the operations shall only be created, but not applied
            const onlyCreateOperations = options?.onlyCreateOperations;

            // helper function, that is necessary because the user might be asked,
            // if he really wants to delete the content.
            const doDeleteSelected = () => {

                // clean up function after the operations were applied
                const postDeleteOperations = () => {

                    this.setGUITriggeredOperation(false);
                    this.#deleteOrBackSpaceKeyPressed = false;

                    if (firstParaPosition) {
                        // set paragraph attributes immediately, if paragraph is without content
                        paragraph = Position.getParagraphElement(rootNode, firstParaPosition);
                        if ((paragraph) && ($(paragraph).text().length === 0)) {
                            this.validateParagraphNode(paragraph);
                        }
                    }

                    if (!isCellSelection) {
                        if (currentParaPosition) {
                            paragraph = Position.getParagraphElement(rootNode, currentParaPosition);
                            if ((paragraph) && ($(paragraph).text().length === 0)) {
                                this.validateParagraphNode(paragraph);
                            }
                        } else {
                            globalLogger.warn('Editor.doDeleteSelected(): currentParaPosition is missing!');
                        }

                        // special handling for additionally removed range start handlers
                        // -> checking if this is a valid position (maybe a complete table was removed)
                        if (newStartPosition) {
                            if (Position.isValidElementPosition(rootNode, newStartPosition, { allowFinalPosition: true })) {
                                this.setLastOperationEnd(newStartPosition);
                            } else {
                                const toplevelNodePosition = [_.first(newStartPosition)];
                                if (Position.isValidElementPosition(rootNode, toplevelNodePosition)) {
                                    this.setLastOperationEnd(Position.getFirstPositionInParagraph(rootNode, toplevelNodePosition));
                                } else {
                                    this.setLastOperationEnd(Position.getFirstPositionInParagraph(rootNode, [0]));
                                }
                            }
                        }

                        if (this.getChangeTrack().isActiveChangeTracking() && !changeTrackDeletedContent) {
                            // if change tracking is active, and the content was not deleted (inserted by the same user before?),
                            // then the old selection can be reused.
                            this.setLastOperationEnd(selection.getStartPosition()); // setting cursor before(!) removed selection
                        }
                    } else {
                        // cell selection
                        if (firstParaPosition) {   // not complete table selected
                            this.setLastOperationEnd(Position.getFirstPositionInCurrentCell(rootNode, firstParaPosition));
                        }
                    }

                    // collapse selection (but not after canceling delete process), also handling absolute positioned drawings (52803)
                    if (!userAbort) {
                        if (!oneDrawingPosition) {
                            const checkedPosition = this.#validatePositionAfterRemoval(rootNode, this.getLastOperationEnd(), selection.getStartPosition(), selection.getEndPosition());
                            if (checkedPosition) { this.setLastOperationEnd(checkedPosition); }
                        }
                        selection.setTextSelection(oneDrawingPosition ? oneDrawingPosition : this.getLastOperationEnd());
                    }

                    if (SMALL_DEVICE && _.browser.Safari && this.#docApp.docView.openSoftKeyboard) {
                        this.#docApp.docView.openSoftKeyboard();
                    }
                };

                // apply the operations (undo group is created automatically)
                this.setGUITriggeredOperation(true); // Fix for 30597

                // setting the cursor near an removed absolutely positioned drawing (52803)
                if (isSingleDrawingSelection && !this.getActiveTarget() && !DOM.isInlineComponentNode(selection.getSelectedDrawing())) {
                    oneDrawingPosition = Position.getOxoPositionOfAbsolutePositionedNode(this.getNode(), selection.getSelectedDrawing(), this.#docApp.getView().getZoomFactor() * 100);
                }

                if (asyncDelete) {

                    // applying all operations asynchronously
                    operationsPromise = this.applyTextOperationsAsync(generator, null, { showProgress: false, leaveOnSuccess: true, progressStart: operationRatio });

                    // handler for 'always', that is not triggered, if 'this' is in destruction (document is closed, 42567)
                    this.onSettled(operationsPromise, postDeleteOperations);

                } else {

                    // applying all operations synchronously
                    this.applyOperations(generator);

                    postDeleteOperations();

                    operationsPromise = $.when();
                }

                return operationsPromise;
            };

            // helper function to check, if an existing place holder drawing node needs to be replaced
            // by a new empty drawing place holder.
            const checkNonEmptyPlaceHolderDrawings = (drawing, pos) => {
                insertDrawingGenerator = this.handleNonEmptyPlaceHolderDrawings ? this.handleNonEmptyPlaceHolderDrawings(drawing, pos, insertDrawingGenerator) : null; // call to Presentation model
            };

            // helper function to check, if a place holder drawing on a target slide is removed, so that
            // its attributes must be transferred to the children on the document slides.
            const checkTransferOfPlaceHolderAttributes = (drawing, target) => {
                placeHolderAttrsGenerator = this.handlePlaceHolderAttributeTransfer(drawing, target, placeHolderAttrsGenerator); // call to Presentation model
            };

            // helper function to generate all delete operations in multi drawing selections
            const deleteDrawingsInMultiDrawingSelection = () => {

                let drawingNode = null;
                let startPos = null;
                // in a multi selection it can happen, that some of the drawings are non-empty place holder drawings.
                // In this case, an insertDrawing for an empty place holder must be generated. But the logical position
                // cannot be the same as for the 'delete' operation of this drawing, because the multi selection can
                // contain additional drawings, that are not replaced with an empty place holder. Therefore it is necessary
                // to reduce the logical position of the insertDrawing operation for every non replaced drawing.
                let reduction = 0;
                // the old and the new number of generated insert-Operations
                let oldCount = 0;
                let newCount = 0;

                _.each(selection.getMultiSelection(), drawingSelection => {

                    drawingNode = selection.getDrawingNodeFromMultiSelection(drawingSelection);
                    startPos = selection.getStartPositionFromMultiSelection(drawingSelection);

                    newOperation = { start: _.copy(startPos) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);

                    // checking if an insertDrawing operation is needed for place holder drawings
                    if (this.useSlideMode) {
                        if (target) {
                            // if this is a place holder drawing, its attributes must be transferred
                            checkTransferOfPlaceHolderAttributes(drawingNode, target);
                        } else {
                            oldCount = insertDrawingGenerator ? insertDrawingGenerator.getOperationCount() : 0;
                            checkNonEmptyPlaceHolderDrawings(drawingNode, (reduction > 0 ? Position.decreaseLastIndex(startPos, reduction) : _.clone(startPos)));
                            newCount = insertDrawingGenerator ? insertDrawingGenerator.getOperationCount() : 0;
                            // increase the reduction by 1, if no insertDrawing operation was generated
                            if (newCount === oldCount) { reduction++; }
                        }
                    }
                });

                // checking, if unrestorable content will be removed
                askUser = this.#containsUnrestorableElements(selection.getFirstSelectedDrawingNode().closest(DOM.PARAGRAPH_NODE_SELECTOR));
            };

            // helper function to generate all delete operations
            const generateAllDeleteOperations = selectionRange => {

                // the logical start position, if specified
                const localStartPos = (selectionRange && selectionRange[0]) || null;
                // the logical end position, if specified
                const localEndPos = (selectionRange && selectionRange[1]) || null;
                // the change track object
                const changeTrack = this.getChangeTrack();
                // whether the shortest path can be used by the selection iterator
                let useShortestPath = !keepParagraphs;

                // special handling for completely seleted tables in cell selections (FF only)
                if (selection.isCompleteTableCellSelection()) { useShortestPath = false; } // -> not deleting table, but the content

                // in the case of empty place holder drawing in ODP presentation, the final paragraph must be removed, too
                if (this.#docApp.isODF() && this.useSlideMode() && this.isCompletePlaceHolderContentSelection && this.isCompletePlaceHolderContentSelection(selection.getStartPosition(), selection.getEndPosition(), selection.getSelectedTextFrameDrawing())) {
                    newOperation = { start: _.initial(selection.getStartPosition()) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);
                    // saving the existing attributes as preselected attributes for the case, that the user
                    // immediately starts typing again without setting selection out of the shape (52649)
                    this.addCharacterAttributesToPreselectedAttributes(Position.getParagraphElement(this.getCurrentRootNode(), _.initial(selection.getStartPosition())), 0, 'character');
                }

                // visit all content nodes (tables, paragraphs) in the selection
                selection.iterateContentNodes((node, position, startOffset, endOffset, parentCovered) => {

                    // whether the node is the last child of its parent
                    const isLastChild = node === node.parentNode.lastChild;
                    // whether a paragraph is selected completely
                    let paragraphSelected = false;
                    // a collector of all rows of a table
                    let allRows = null;
                    // a helper DOM point
                    let domPos = null;
                    // the paragraph length
                    let paraLength = 0;
                    // whether this is a first paragraph in a completely selected cell
                    let isFirstParagraphInSelectedCell = false;
                    // whether an operation needs to be generated
                    let generateOperation = true;

                    counter++;  // counting the nodes
                    if (counter === 1) {
                        isFirstNode = true;
                        firstNode = node;
                    } else {
                        isFirstNode = false;
                        lastNode = node;
                        lastParaRemovedPartly = false;
                    }

                    // saving the current node
                    currentNode = node;

                    if (DOM.isParagraphNode(node)) {

                        // remember first and last paragraph
                        if (!firstParaPosition) { firstParaPosition = position; }
                        currentParaPosition = position;

                        if (_.isNumber(startOffset) && !_.isNumber(endOffset)) {
                            if (startOffset === 0) {
                                paragraphSelected = true;  // the full paragraph can be delete
                            } else {
                                checkMergeOfLastParagraph = true;  // for the final paragraph, a merge-check is necessary after iteration
                            }
                        }

                        // checking if a paragraph is selected completely and requires merge
                        paragraphSelected = _.isNumber(startOffset) && (startOffset === 0) && !_.isNumber(endOffset);

                        // not deleting the first paragraph in a table cell, if selection expands to further cells (not required for cell selections (49219))
                        if (isFirstNode && startOffset === 0 && !isCellSelection && DOM.isFirstParagraphInTableInCell(node) && selection.isCellExceedingSelection()) {
                            isFirstParagraphInSelectedCell = true;
                        }

                        if (changeTrack.isActiveChangeTracking()) { // complete separation of code for change tracking

                            if (!DOM.isImplicitParagraphNode(node)) {

                                if (!isFirstParagraphInSelectedCell) { // never deleting the final paragraph in a table cell

                                    // do not delete the paragraph node, if it is only covered partially;
                                    // or if it is the last paragraph when the parent container is cleared completely
                                    if (parentCovered ? isLastChild : (_.isNumber(startOffset) || _.isNumber(endOffset))) {

                                        // 'deleteText' operation needs valid start and end position
                                        startOffset = _.isNumber(startOffset) ? startOffset : 0;
                                        endOffset = _.isNumber(endOffset) ? endOffset : (Position.getParagraphLength(rootNode, position) - 1);

                                        // delete the covered part of the paragraph
                                        if (isCellSelection) {
                                            // TODO
                                        } else if (paragraphSelected || ((startOffset === 0) && (endOffset === 0) && (Position.getParagraphLength(rootNode, position) === 0))) {
                                            if (changeTrack.isActiveChangeTracking()) {
                                                if (!DOM.isImplicitParagraphNode(node)) { // Checking if this is an implicit paragraph
                                                    newOperation = { start: _.copy(position), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                                    this.extendPropertiesWithTarget(newOperation, target);
                                                    generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                                }
                                            }
                                        } else if (startOffset <= endOffset) {
                                            // Will be handled during iteration over all paragraph children
                                        }
                                    } else {
                                        newOperation = { start: _.copy(position), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                        this.extendPropertiesWithTarget(newOperation, target);
                                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                    }
                                }

                                // if change tracking is active, three different cases need to be disinguished:
                                // - the node is already marked for removal -> no operation needs to be created
                                // - the node is marked for insertion of new text -> text can be removed with delete operation
                                // - if this is a text span without change track attribute -> a setAttributes operation is created
                                // - other content is simply removed, no change tracking implemented yet

                                // iterating over all nodes of the paragraph is required
                                Position.iterateParagraphChildNodes(node, (subnode, nodestart, nodelength, offsetstart, offsetlength) => {

                                    // whether a delete operation needs to be created
                                    let createDeleteOperation = true;
                                    // whether a delete operation needs to be created
                                    let createSetAttributesOperation = false;
                                    // whether the node is a change track insert node
                                    let isChangeTrackInsertNode = false;

                                    if (DOM.isTextSpan(subnode) || DOM.isTextComponentNode(subnode) || isDrawingFrame(subnode)) {
                                        if (changeTrack.isInsertNodeByCurrentAuthor(subnode)) {
                                            // the content was inserted during active change track
                                            createDeleteOperation = true;
                                            createSetAttributesOperation = false;
                                            isChangeTrackInsertNode = true;
                                        } else if (DOM.isChangeTrackRemoveNode(subnode)) {
                                            // the content was already removed during active change track
                                            createDeleteOperation = false;
                                            createSetAttributesOperation = false;
                                        } else {
                                            // a set attributes operation is required for text spans without change track attribute
                                            createDeleteOperation = false;
                                            createSetAttributesOperation = true;
                                        }
                                    }

                                    if (createSetAttributesOperation) {
                                        if (changeTrack.isChangeTrackableNode(subnode)) {
                                            newOperation = { start: position.concat([nodestart + offsetstart]), end: position.concat([nodestart + offsetstart + offsetlength - 1]), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                            this.extendPropertiesWithTarget(newOperation, target);
                                            generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                        }
                                    }

                                    if (createDeleteOperation) {
                                        newOperation = { start: position.concat([nodestart + offsetstart]), end: position.concat([nodestart + offsetstart + offsetlength - 1]) };
                                        this.extendPropertiesWithTarget(newOperation, target);
                                        generator.generateOperation(DELETE, newOperation);

                                        // the 'inserted' is no longer valid at a completely removed node
                                        // Info: The order is reversed -> 1. step: Setting attributes, 2. step: Removing all content
                                        if (isChangeTrackInsertNode && (offsetstart === 0) && (offsetlength === nodelength)) {
                                            newOperation = { start: position.concat([nodestart]), end: position.concat([nodestart + offsetlength - 1]), attrs: { changes: { mode: null } } };
                                            this.extendPropertiesWithTarget(newOperation, target);
                                            generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                        }

                                    }

                                }, undefined, { start: startOffset, end: endOffset });
                            }

                        } else {  // no change tracking active

                            if (!DOM.isImplicitParagraphNode(node)) {

                                // in cell selections, the last paragraph in a cell must not be deleted
                                if (isCellSelection) { parentCovered = true; }

                                // do not delete the paragraph node, if it is only covered partially;
                                // or if it is the last paragraph when the parent container is cleared completely
                                if (parentCovered ? isLastChild : (_.isNumber(startOffset) || _.isNumber(endOffset))) {

                                    // 'deleteText' operation needs valid start and end position
                                    paraLength = Position.getParagraphLength(rootNode, position);
                                    startOffset = _.isNumber(startOffset) ? startOffset : 0;
                                    endOffset = _.isNumber(endOffset) ? endOffset : (paraLength - 1);

                                    // delete the covered part of the paragraph
                                    if (isMultiDrawingSelection) {
                                        if (deleteKeyPressed) { deleteDrawingsInMultiDrawingSelection(); }
                                    } else if (isCellSelection || isFirstParagraphInSelectedCell) {
                                        if (this.#containsUnrestorableElements(node)) { askUser = true; }
                                        if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(node, allRangeMarkerSelector)); }
                                        if (isLastChild || isFirstParagraphInSelectedCell) { // not deleting the last paragraph (or the first in a multi cell selection)
                                            if (paraLength > 0) {
                                                newOperation = { start: Position.appendNewIndex(position, startOffset), end: Position.appendNewIndex(position, endOffset) };
                                            } else {
                                                generateOperation = false; // no operation for empty paragraphs
                                            }
                                        } else {
                                            newOperation = { start: _.copy(position) };
                                        }

                                        if (generateOperation) {
                                            this.extendPropertiesWithTarget(newOperation, target);
                                            generator.generateOperation(DELETE, newOperation);
                                        }
                                    } else if (paragraphSelected || ((startOffset === 0) && (endOffset === 0) && (Position.getParagraphLength(rootNode, position) === 0))) {  // -> do not delete from [1,0] to [1,0]
                                        if (isRangeCheckRequired && paragraphSelected) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(node, allRangeMarkerSelector)); }
                                        if (keepParagraphs) {
                                            const currentParaLength = Position.getParagraphLength(rootNode, position);
                                            newOperation = currentParaLength > 0 ? { start: [...position, 0], end: [...position, currentParaLength - 1] } : null;
                                        } else {
                                            newOperation = { start: _.copy(position) };
                                        }
                                        if (newOperation) {
                                            this.extendPropertiesWithTarget(newOperation, target);
                                            generator.generateOperation(DELETE, newOperation);
                                            this.handleTriggeringListUpdate(node);
                                        }
                                    } else if (startOffset <= endOffset) {
                                        if (isFirstNode) {
                                            firstParaRemovedPartly = true;
                                            firstParaMergePos = startOffset;
                                        } else {
                                            lastParaRemovedPartly = true;
                                        }

                                        // saving the character attributes
                                        if (isFirstNode && saveStartAttrs) { this.addCharacterAttributesToPreselectedAttributes(node, startOffset, 'character'); }

                                        if (this.#containsUnrestorableElements(node, startOffset, endOffset)) { askUser = true; }
                                        if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(node, allRangeMarkerSelector, startOffset, endOffset)); }
                                        newOperation = { start: position.concat([startOffset]), end: position.concat([endOffset]) };
                                        this.extendPropertiesWithTarget(newOperation, target);
                                        generator.generateOperation(DELETE, newOperation);

                                        // presentation specific stuff
                                        if (this.useSlideMode()) {
                                            domPos = Position.getDOMPosition(node, [startOffset], true);
                                            if (domPos && domPos.node && isDrawingFrame(domPos.node)) {
                                                if (target) {
                                                    // if this is a place holder drawing, its attributes must be transferred
                                                    checkTransferOfPlaceHolderAttributes(domPos.node, target);
                                                } else {
                                                    // checking if an insertDrawing operation is needed for place holder drawings
                                                    checkNonEmptyPlaceHolderDrawings(domPos.node, position.concat([startOffset]));
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if (this.#containsUnrestorableElements(node)) { askUser = true; }
                                    if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(node, allRangeMarkerSelector)); }
                                    if (keepParagraphs) { // TODO DOCS-5132: Check, if this is a paragraph
                                        const currentParaLength = Position.getParagraphLength(rootNode, position);
                                        newOperation = currentParaLength > 0 ? { start: [...position, 0], end: [...position, currentParaLength - 1] } : null;
                                    } else {
                                        newOperation = { start: _.copy(position) };
                                    }
                                    if (newOperation) {
                                        this.extendPropertiesWithTarget(newOperation, target);
                                        generator.generateOperation(DELETE, newOperation);
                                    }
                                }
                            }
                        }

                    } else if (DOM.isTableNode(node)) {

                        allRows = DOM.getTableRows(node);

                        if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(node) && !changeTrack.allAreInsertedNodesByCurrentAuthor(allRows)) {
                            if (DOM.isExceededSizeTableNode(node)) {
                                newOperation = { start: _.clone(position), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                this.extendPropertiesWithTarget(newOperation, target);
                                generator.generateOperation(SET_ATTRIBUTES, newOperation);
                            } else {
                                // Table is not (necessarily) marked with 'inserted', but all rows
                                // -> setting changes attribute at all rows, not at the table
                                _.each(allRows, (_row, index) => {
                                    const rowPosition = _.clone(position);
                                    rowPosition.push(index);
                                    newOperation = { start: _.copy(rowPosition), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                    this.extendPropertiesWithTarget(newOperation, target);
                                    generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                });
                            }
                        } else {
                            // delete entire table
                            newOperation = { start: _.copy(position) };
                            this.extendPropertiesWithTarget(newOperation, target);
                            generator.generateOperation(DELETE, newOperation);
                            // checking, if this is a table with exceeded size
                            if (DOM.isExceededSizeTableNode(node) || this.#containsUnrestorableElements(node)) { askUser = true; }
                            if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(node, allRangeMarkerSelector)); }
                            // important for the new position: Although change track might be activated, the content was really removed
                            changeTrackDeletedContent = true;
                        }
                    } else {
                        globalLogger.error('Editor.deleteSelected(): unsupported content node');
                        return BREAK;
                    }

                }, this, { shortestPath: useShortestPath, startPos: localStartPos, endPos: localEndPos });

            };  // end of generateAllDeleteOperations

            // helper function to resort all delete operations and optionally add further delete operations
            // for example for range markers
            const resortDeleteOperations = () => {

                // operations MUST be executed in reverse order to preserve the positions
                generator.reverseOperations();

                // Merging paragraphs additionally is required, if:
                // - firstNode and lastNode of selection are (different) paragraphs
                // - firstNode and lastNode are not removed completely
                // - firstNode and lastNode have the same parent
                //
                // In Firefox tables there is a cell selection. In this case all paragraphs
                // are deleted completely, so that no merge is required.
                // Merging is also not allowed, if change tracking is active.

                doMergeParagraph = firstParaPosition && firstParaRemovedPartly && lastParaRemovedPartly && !keepParagraphs && (firstNode.parentNode === lastNode.parentNode);

                // it is additionally necessary to merge paragraphs, if the selection ends at position 0 inside a paragraph.
                // In this case the 'end-paragraph' will not be iterated in 'selection.iterateContentNodes'. So this need
                // to be checked manually.
                if (!doMergeParagraph && checkMergeOfLastParagraph && (_.last(selection.getEndPosition()) === 0)) {
                    // there is another chance to merge paragraph, if mouse selection goes to start of following paragraph
                    // using currentParaPosition and end of selection and lastNode
                    // Additionally the first paragraph and the selectionEndParagraph need to have the same parent
                    selectionEndParagraph = Position.getDOMPosition(rootNode, _.initial(selection.getEndPosition())).node;
                    if (currentNode && currentNode.nextSibling && selectionEndParagraph && (currentNode.nextSibling === selectionEndParagraph) && (firstNode.parentNode === selectionEndParagraph.parentNode)) {
                        doMergeParagraph = true;
                    }
                }

                if (doMergeParagraph) {
                    if (this.getChangeTrack().isActiveChangeTracking()) {
                        newOperation = { start: _.copy(firstParaPosition), attrs: { changes: { removed: this.getChangeTrack().getChangeTrackInfo() } } };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                    } else {
                        newOperation = { start: _.copy(firstParaPosition), paralength: firstParaMergePos };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(PARA_MERGE, newOperation);
                    }
                }

                // handling for all collected ranges (if content is removed before the current start of the selection
                // (for example range start nodes), if is necessary to know the new position of the current start
                // position.
                if (allRangeMarkerNodes.length > 0) {
                    newStartPosition = this.getRangeMarker().handlePartlyDeletedRanges(allRangeMarkerNodes, generator, selection.getStartPosition());
                    // collecting all comment IDs for generating deleteComment operations, that are required for OT
                    _.each(allRangeMarkerNodes, node => { if (DOM.isCommentPlaceHolderNode(node)) { allRemovedCommentIDs.push(DOM.getTargetContainerId(node)); } });
                }

            };  // end of resortDeleteOperations

            if (!selection.hasRange()) { return $.when(); }

            // setting the global variable that is evaluated in the delete handler (DOCS-3074)
            this.#deleteOrBackSpaceKeyPressed = deleteKeyPressed;

            // Search not only for range markers, but also for comments, if required
            if (allRangeMarkerSelector && this.#docApp.isTextApp() && !this.getCommentLayer().isEmpty()) { allRangeMarkerSelector += ', ' + DOM.COMMENTPLACEHOLDER_NODE_SELECTOR; }

            // checking selection size -> make array with splitted selection [0,100], [101, 200], ... (39061)
            splittedSelectionRange = onlyCreateOperations ? [] :  Position.splitLargeSelection(this.getCurrentRootNode(), selection.getStartPosition(), selection.getEndPosition(), this.getMaxTopLevelNodes());

            // OT: Blocking external operations (because of the possible dialog also, if deleting is synchronous)
            if (!onlyCreateOperations) { this.setInternalOperationBlocker(true); }

            if (splittedSelectionRange && splittedSelectionRange.length > 1) {

                // delete must always be asynchronous
                asyncDelete = true;

                // make sure that only one asynchronous call is processed at the same time
                if (!alreadyPasteInProgress && this.checkSetClipboardPasteInProgress()) { return $.when(); }

                // blocking keyboard input during generation and applying of operations
                // -> always required after introduction of jQuery 3
                this.setBlockKeyboardEvent(true);

                if (splittedSelectionRange.length > 1) {

                    // creating a snapshot (but not, if it was already created by the caller of this function)
                    if (!snapshot) {
                        snapshot = new Snapshot(this);
                        snapshotCreated = true;
                    }

                    // show a message with cancel button
                    // -> immediately grabbing the focus, after calling enterBusy. This guarantees, that the
                    // keyboard blocker works. Otherwise the keyboard events will not be catched by the page.
                    this.#docApp.docView.enterBusy({
                        cancelHandler: () => {
                            userAbort = true;  // user aborted the process
                            if (operationsPromise && operationsPromise.abort) { // order is important, the latter has to win
                                // restoring the old document state
                                snapshot.apply();
                                // calling abort function for operation promise
                                this.#docApp.enterBlockOperationsMode(() => { operationsPromise.abort(); });
                            } else if (operationGeneratorPromise && operationGeneratorPromise.abort) {
                                // cancel during creation of operation -> no undo required
                                operationGeneratorPromise.abort();
                            }
                        },
                        immediate: true,
                        warningLabel
                    });

                    this.#docApp.docView.grabFocus();
                }

                // generate operations asynchronously
                operationGeneratorPromise = this.iterateArraySliced(splittedSelectionRange, oneRange => {
                    // calling function to generate operations synchronously with reduced selection range
                    generateAllDeleteOperations(oneRange);
                });

                operationGeneratorPromise.done(() => {

                    // optimizing the operations at the border of the range of splitted operations
                    this.#improveAsyncDeleteOperations(generator, this.getMaxTopLevelNodes());

                    // checking, if the delete operations can be merged
                    if (!keepParagraphs) { this.#checkMergeOfDeleteOperations(generator); }

                    // synchronous resorting of delete operations
                    resortDeleteOperations();

                    // adding required operations for inserting drawings (Presentation app only)
                    if (insertDrawingGenerator) { generator.appendOperations(insertDrawingGenerator.getOperations()); }

                    // adding required operations for setAttributes operations for drawings in document slides
                    if (placeHolderAttrsGenerator) { generator.appendOperations(placeHolderAttrsGenerator.getOperations()); }

                    // Asking the user, if he really wants to remove the content, if it contains
                    // a table with exceeded size or other unrestorable elements. This cannot be undone.
                    // return this.askUserHandler();
                });

                operationGeneratorPromise.fail(() => {
                    // user abort during creation of operation -> no undo required
                    // -> leaving busy mode immediately
                    this.leaveAsyncBusy();
                });

                // add progress handling
                operationGeneratorPromise.progress(progress => {
                    // update the progress bar according to progress of the operations promise
                    this.#docApp.getView().updateBusyProgress(operationRatio * progress);
                });

            } else {

                // synchronous handling for small selections
                asyncDelete = false;
                // generating operations synchronously
                generateAllDeleteOperations(splittedSelectionRange[0]);
                // checking, if the delete operations can be merged
                if (!keepParagraphs) { this.#checkMergeOfDeleteOperations(generator); }
                // synchronous resorting of delete operations
                resortDeleteOperations();
                // adding required operations for inserting drawings (Presentation app only)
                if (insertDrawingGenerator) { generator.appendOperations(insertDrawingGenerator.getOperations()); }
                // adding required operations for setAttributes operations for drawings in document slides
                if (placeHolderAttrsGenerator) { generator.appendOperations(placeHolderAttrsGenerator.getOperations()); }

                // only creation of operations, not applying operations
                if (onlyCreateOperations) { return Promise.resolve({ generator, askUser }); }

                // bugfix #53830: ContextMenu doesn't close automatically
                this.#docApp.getView().grabFocus();

                if (!askUser) {
                    // directly applying the operations
                    doDeleteSelected().always(() => {
                        this.setInternalOperationBlocker(false); // OT: No longer blocking external operations
                    });
                    return $.when(); // fast synchronous exit, required for jQuery 3
                } else {
                    operationGeneratorPromise = $.when();
                }
            }

            // delete contents on resolved promise
            return operationGeneratorPromise.then(() => {
                return this.askUserHandler(askUser, asyncDelete);
            }).then(doDeleteSelected).always(() => {
                this.setInternalOperationBlocker(false); // OT: No longer blocking external operations
                if (snapshotCreated) { snapshot.destroy(); }
            });
        }

        /**
         * Editor API function to generate 'delete' operations.
         * This is a generic function, that can be used to delete any component (text, paragraph,
         * cell, row, table, ...). Deleting columns is not supported, because columns cannot
         * be described with a logical position.
         * The parameter 'start' and 'end' are used to specify the position of the components that
         * shall be deleted. For all components except 'text' the 'end' position will be ignored.
         * For paragraphs, cells, ... only one specific component can be deleted within this
         * operation. Only on text level a complete range can be deleted.
         *
         * @param {Number[]} start
         *  The logical start position.
         *
         * @param {Number[]} [end]
         *  The logical end position (optional). This can be different from 'start' only for text ranges
         *  inside one paragraph. A text range can include characters, fields, and drawing objects,
         *  but must be contained in a single paragraph.
         *
         * @param {Object} [options]
         *  Additional optional options, that can be used for performance reasons.
         *  @param {Boolean} [options.setTextSelection=true]
         *      If set to false, the text selection will not be set within this
         *      function. This is useful, if the caller takes care of setting the
         *      cursor after the operation. This is the case for 'Backspace' and
         *      'Delete' operations. It is a performance issue, that the text
         *      selection is not set twice.
         *  @param {Boolean} [options.deleteKey=false]
         *      Whether this function was triggered by the 'delete' or 'backspace' key.
         *  @param {Boolean} [options.handleUnrestorableContent=false]
         *      If set to true, it will be checked if the range, that shall be
         *      deleted, contains unrestorable content. In this case a dialog appears,
         *      in which the user is asked, if he really wants to delete this content.
         *      This is the case for 'Backspace' and 'Delete' operations.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        deleteRange(start, end, options) {

            // a helper dom position element
            let domPos = null;
            // whether the cursor shall be set in this function call
            const setTextSelection = getBooleanOption(options, 'setTextSelection', true);
            // whether start and end position are different
            const handleUnrestorableContent = getBooleanOption(options, 'handleUnrestorableContent', false);
            // whether start and end logical position are different
            const isSimplePosition = !_.isArray(end) || _.isEqual(start, end);
            // whether this function was triggered by pressing 'delete' or 'backspace'
            const deleteKeyPressed = getBooleanOption(options, 'deleteKey', false);
            // a paragraph node
            let paragraph = null;
            // whether it is necessary to ask the user before deleting range
            let askUser = false;
            // the resulting promise
            let promise = null;
            // target ID of currently active root node, if existing
            const target = this.getActiveTarget();
            // currently active root node
            const activeRootNode = this.getCurrentRootNode(target);
            // created operation
            let newOperation = null;

            // helper function, that is necessary because the user might be asked,
            // if he really wants to delete the range.
            const doDeleteRange = () => {

                // whether a delete operation needs to be created
                let createDeleteOperation = true;
                // whether a delete operation needs to be created
                let createSetAttributesOperation = false;
                // attribute object for changes attribute
                let attrs;
                // the dom point at text level position
                let element;
                // a logical helper position
                let lastOperationEndLocal = null;
                // the change track object
                const changeTrack = this.getChangeTrack();
                // the operations generator
                let generator = null;

                // special handling for active change tracking
                if (changeTrack.isActiveChangeTracking()) {

                    // TODO: Also checking the end position?!
                    // -> not required, if this function is only called from deleting with 'backspace' or 'delete',
                    // that call first of all 'deleteSelected'.
                    // -> otherwise it might happen during active change tracking, that more than one operation
                    // needs to be generated.
                    element = Position.getDOMPosition(paragraph, [_.last(start)], true);

                    if (element && element.node && (DOM.isTextSpan(element.node) || DOM.isInlineComponentNode(element.node))) {
                        if (changeTrack.isInsertNodeByCurrentAuthor(element.node)) {
                            // the content was inserted during active change track (by the same user)
                            createDeleteOperation = true;
                            createSetAttributesOperation = false;
                        } else if (DOM.isChangeTrackRemoveNode(element.node)) {
                            // the content was already removed during active change track
                            createDeleteOperation = false;
                            createSetAttributesOperation = false;
                        } else {
                            // a set attributes operation is required for text spans without change track attribute
                            // or for spans inserted by another user
                            createDeleteOperation = false;
                            createSetAttributesOperation = true;
                        }
                    }

                    if (createSetAttributesOperation) {
                        createDeleteOperation = false;
                        // adding changes attributes
                        attrs = { changes: { removed: changeTrack.getChangeTrackInfo() } };
                        newOperation = { name: SET_ATTRIBUTES, start: _.clone(start), end: _.clone(end), attrs };
                        this.extendPropertiesWithTarget(newOperation, target);
                        this.applyOperations(newOperation);
                    }

                    // Setting the cursor to the correct position, if no character was deleted
                    if (!createDeleteOperation) {
                        lastOperationEndLocal = _.clone(end);
                        lastOperationEndLocal[lastOperationEndLocal.length - 1] += 1;
                        this.setLastOperationEnd(lastOperationEndLocal);
                    }

                }

                if (createDeleteOperation) {

                    generator = this.createOperationGenerator(); // using generator for grouping operations

                    // Using end as it is, not subtracting '1' like in 'deleteText'
                    this.setGUITriggeredOperation(true); // Fix for 30597
                    newOperation = { start: _.clone(start), end: _.clone(end) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);

                    // in ODP the paragraph has also to be removed, if the place holder drawing will be empty
                    if (this.#docApp.isODF() && this.useSlideMode() && this.isCompletePlaceHolderContentSelection && this.isCompletePlaceHolderContentSelection(start, Position.increaseLastIndex(end), this.getSelection().getSelectedTextFrameDrawing())) {
                        newOperation = { start: _.initial(start) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(DELETE, newOperation);
                        // saving the existing attributes as preselected attributes for the case, that the user
                        // immediately starts typing again without setting selection out of the shape (52649)
                        this.addCharacterAttributesToPreselectedAttributes(Position.getParagraphElement(this.getCurrentRootNode(), _.initial(start)), 0, 'character');
                    }

                    this.applyOperations(generator);
                    this.setGUITriggeredOperation(false);
                    this.#deleteOrBackSpaceKeyPressed = false;

                    // Setting paragraph attributes immediately, if paragraph is without content.
                    // This avoids jumping of following paragraphs.
                    if ((_.last(start) === 0) && (_.last(end) === 0)) {
                        domPos = Position.getDOMPosition(activeRootNode, _.chain(start).clone().initial().value(), true);
                        if ((domPos) && (domPos.node) && ($(domPos.node).is(DOM.PARAGRAPH_NODE_SELECTOR)) && ($(domPos.node).text().length === 0)) {
                            this.validateParagraphNode(domPos.node);
                        }
                    }

                    // setting the cursor position
                    if (setTextSelection) {
                        this.getSelection().setTextSelection(this.getLastOperationEnd());
                    }
                }
            };

            end = end || _.clone(start);

            // setting the global variable that is evaluated in the delete handler (DOCS-3074)
            this.#deleteOrBackSpaceKeyPressed = deleteKeyPressed;

            // checking, if an unrestorable
            if (handleUnrestorableContent) {
                paragraph = Position.getParagraphElement(activeRootNode, _.initial(start));
                if (paragraph) {
                    if (isSimplePosition && this.#containsUnrestorableElements(paragraph, _.last(start), _.last(start))) {
                        askUser = true;
                    } else {
                        // if node contains special page number fields in header and footer restore original state before delete
                        this.getFieldManager().checkRestoringSpecialFields(paragraph, _.last(start), _.last(end));
                    }
                }
            }

            // Asking the user, if he really wants to remove the content, if it contains
            // a table with exceeded size or other unrestorable elements. This cannot be undone.
            if (askUser) {
                promise = this.showDeleteWarningDialog(DELETE_CONTENTS_TITLE, DELETE_CONTENTS_QUERY);
                // return focus to editor after 'No' button
                promise.fail(() => { this.#docApp.getView().grabFocus(); });
            } else {
                promise = $.when();
            }

            // delete contents on resolved promise
            promise.done(doDeleteRange);

            return handleUnrestorableContent ? promise : undefined;
        }

        /**
         * Editor API function to generate 'delete' operation for the complete table.
         * This method use the 'deleteRows' method with a predefined start and end index.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        deleteTable() {

            // the start position of the selection
            const position = this.getSelection().getStartPosition();
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current element container node
            const rootNode = this.getCurrentRootNode(target);
            // the index of the row inside the table, in which the selection ends
            const endIndex = Position.getLastRowIndexInTable(rootNode, position);

            return this.deleteRows({ start: 0, end: endIndex });
        }

        /**
         * Editor API function to generate 'delete' operations for rows or a complete table. This
         * function is triggered by an user event. It evaluates the current selection. If the
         * selection includes all rows of the table, the table is removed completely within one
         * operation. If the row or table contains unrestorable content, the user is asked, whether
         * he really wants to delete the rows.
         *
         * @param {Object} options
         *  Optional parameters:
         *  @param {Number} [options.start]
         *   The index of the start row
         *  @param {Number} [options.end]
         *   The index of the end row
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        deleteRows(options) {

            // the operations generator
            const generator = this.createOperationGenerator();
            // the selection object
            const selection = this.getSelection();
            // the table sector
            const selector = this.useSlideMode() ? DOM.TABLE_NODE_AND_DRAWING_SELECTOR : DOM.TABLE_NODE_SELECTOR;
            // the start position of the selection
            const position = selection.getStartPosition();
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current element container node
            const rootNode = this.getCurrentRootNode(target);
            // the index of the row inside the table, in which the selection starts
            const start = getOption(options, 'start', Position.getRowIndexInTable(rootNode, position));
            // the index of the row inside the table, in which the selection ends
            const end = getOption(options, 'end', selection.hasRange() ? Position.getRowIndexInTable(rootNode, selection.getEndPosition()) : start);
            // the logical position of the table
            const tablePos = Position.getLastPositionFromPositionByNodeName(rootNode, position, selector);
            // the index of the last row in the table
            const lastRow = Position.getLastRowIndexInTable(rootNode, position);
            // whether the complete table shall be deleted
            const isCompleteTable = (start === 0) && (end === lastRow);
            // logical row position
            let rowPosition = null;
            // the table node containing the row
            let tableNode = null;
            // the row nodes to be deleted
            let rowNode = null;
            // the collector of all table rows
            let allRows = null;
            // loop counter
            let i = 0;
            // whether it is necessary to ask the user before removing row or table
            let askUser = false;
            // the resulting promise
            let promise = null;
            // whether it is required to check for range markers inside the selection
            const isRangeCheckRequired = !this.getRangeMarker().isEmpty();
            // all classes used to identify the range markers
            let allRangeMarkerSelector = isRangeCheckRequired ? DOM.RANGEMARKERNODE_SELECTOR : null;
            // the collector for all range marker nodes (including comments)
            let allRangeMarkerNodes = $();
            // a container for all IDs of removed comments (OT)
            const allRemovedCommentIDs = [];
            // the change track object
            const changeTrack = this.getChangeTrack();
            // created operation
            let newOperation = null;

            // helper function, that is necessary because the user might be asked,
            // if he really wants to delete the selected rows.
            const doDeleteRows = () => {

                this.setGUITriggeredOperation(true);
                this.applyOperations(generator);
                this.setGUITriggeredOperation(false);

                // setting the cursor position after deleting content
                selection.setTextSelection(this.getLastOperationEnd());
            };

            // helper function to determine a valid new start position for range markers. This is necessary, if
            // from an existing range, only the start marker is removed, but not the end marker.
            const getValidInsertStartRangePosition = () => {

                // the logical position for inserting range markers
                let insertStartRangePosition = null;
                // whether the last row was deleted
                const lastRowDeleted = (end === lastRow);

                if (!lastRowDeleted) {
                    // a following remaining row comes to the current cursor position, so that it is still valid
                    insertStartRangePosition = selection.getStartPosition();
                    insertStartRangePosition[insertStartRangePosition.length - 1] = 0;
                } else {
                    // Find the first valid position behind the table
                    insertStartRangePosition = Position.getFirstPositionInParagraph(this.getCurrentRootNode(), Position.increaseLastIndex(tablePos));
                    if (isCompleteTable) {
                        // because the table is removed completely, the position needs to be reduced by 1
                        insertStartRangePosition[insertStartRangePosition.length - 2] -= 1;
                    }
                }

                return insertStartRangePosition;
            };

            // Search not only for range markers, but also for comments, if required
            if (this.#docApp.isTextApp() && allRangeMarkerSelector && !this.getCommentLayer().isEmpty()) { allRangeMarkerSelector += ', ' + DOM.COMMENTPLACEHOLDER_NODE_SELECTOR; }

            // Generating only one operation, if the complete table is removed.
            // Otherwise sending one operation for removing each row.
            if (isCompleteTable) {
                tableNode = Position.getTableElement(rootNode, tablePos);
                allRows = DOM.getTableRows(tableNode);
                // not setting attributes, if this table was inserted with change tracking by the current user
                if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(tableNode) && !changeTrack.allAreInsertedNodesByCurrentAuthor(allRows)) {
                    // Table is not (necessarily) marked with 'inserted', but all rows
                    // -> setting changes attribute at all rows, not at the table
                    _.each(allRows, (_row, index) => {
                        rowPosition = _.clone(tablePos);
                        rowPosition.push(index);
                        newOperation = { start: _.copy(rowPosition), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                    });
                } else {
                    newOperation = { start: _.copy(tablePos, true) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);
                    if (this.#containsUnrestorableElements(tablePos)) { askUser = true; }
                    if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(tableNode, allRangeMarkerSelector)); }
                }
            } else {

                // let previousRowNode = null;
                // let followingRowNode = null;

                for (i = end; i >= start; i--) {
                    rowPosition = _.clone(tablePos);
                    rowPosition.push(i);
                    rowNode = Position.getTableRowElement(rootNode, rowPosition);

                    // if (i === start) { previousRowNode = rowNode.previousSibling; }
                    // if (i === end) { followingRowNode = rowNode.nextSibling; }

                    if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(rowNode)) {
                        newOperation = { start: _.copy(rowPosition), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                    } else {
                        newOperation = { start: _.copy(rowPosition) };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(DELETE, newOperation);
                        if (this.#containsUnrestorableElements(rowPosition)) { askUser = true; }
                        if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(rowNode, allRangeMarkerSelector)); }
                    }
                }

                // checking for vertically merged table cells, so that the start value has to be reset
                // 1. continue to restart (the row with the 'restart' cell was removed)
                // 2. restart to ''       (all rows of following cells with 'continue' are removed -> less important)

            }

            // handling for all collected ranges
            if (allRangeMarkerNodes.length > 0) {
                this.getRangeMarker().handlePartlyDeletedRanges(allRangeMarkerNodes, generator, getValidInsertStartRangePosition());
                _.each(allRangeMarkerNodes, node => { if (DOM.isCommentPlaceHolderNode(node)) { allRemovedCommentIDs.push(DOM.getTargetContainerId(node)); } });
            }

            // Asking the user, if he really wants to remove the content, if it contains
            // at least one row with exceeded size or other unrestorable elements. This cannot be undone.
            if (askUser) {
                promise = this.showDeleteWarningDialog(DELETE_ROWS_TITLE, DELETE_ROWS_QUERY);
            } else {
                promise = $.when();
            }

            // delete rows on resolved promise
            return promise.done(doDeleteRows);
        }

        /**
         * Editor API function to generate 'delete' operations for columns or a complete table. This
         * function is triggered by an user event. It evaluates the current selection. If the
         * selection includes all columns of the table, the table is removed completely within one
         * operation. If the column or table contains unrestorable content, the user is asked, whether
         * he really wants to delete the columns.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the dialog has been closed with
         *  the default action; or rejected, if the dialog has been canceled.
         *  If no dialog is shown, the promise is resolved immediately.
         */
        deleteColumns() {

            // the operations generator
            const generator = this.createOperationGenerator();
            // the selection object
            const selection = this.getSelection();
            // the start position of the selection
            const position = selection.getStartPosition();
            // the table sector
            const selector = this.useSlideMode() ? DOM.TABLE_NODE_AND_DRAWING_SELECTOR : DOM.TABLE_NODE_SELECTOR;
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current root container node for element
            const rootNode = this.getCurrentRootNode();
            // the logical position of the table
            const tablePos = Position.getLastPositionFromPositionByNodeName(rootNode, position, selector);
            // the table node
            const tableNode = this.useSlideMode() ? Position.getDOMPosition(rootNode, tablePos, true).node : Position.getDOMPosition(rootNode, tablePos).node;
            // the maximum grid number of the table
            const maxGrid = getColumnCount(tableNode) - 1;
            // the row node at the logical start position of the selection
            const rowNode = Position.getLastNodeFromPositionByNodeName(rootNode, position, 'tr');
            // the index of the column at the logical start position of the selection
            const startColIndex = Position.getColumnIndexInRow(rootNode, position);
            // the index of the column at the logical end position of the selection
            const endColIndex = selection.hasRange() ? Position.getColumnIndexInRow(rootNode, selection.getEndPosition()) : startColIndex;
            // a helper object for calculating grid positions in the table
            const returnObj = getGridPositionFromCellPosition(rowNode, startColIndex);
            // the start grid position in the table
            const startGrid = returnObj.start;
            // the end grid position in the table
            const endGrid = selection.hasRange() ? getGridPositionFromCellPosition(rowNode, endColIndex).end : returnObj.end;
            // whether the complete table shall be deleted
            const isCompleteTable = (startGrid === 0) && (endGrid === maxGrid);
            // logical row position
            let rowPos = null;
            // logical cell position
            let cellPos = null;
            // the index of the last row in the table
            let maxRow = null;
            // whether all rows will be deleted
            let deletedAllRows = false;
            // a jQuery collection containing all rows of the specified table
            let allRows = null;
            // An array, that contains for each row an array with two integer values
            // for start and end position of the cells.
            let allCellRemovePositions = null;
            // a row node
            let currentRowNode = null;
            // a cell node
            let cellNode = null;
            // Two integer values for start and end position of the cells in the row.
            let oneRowCellArray = null;
            // helper numbers for cell descriptions inside a row
            let start = 0, end = 0;
            // the table grid of a table
            let tableGrid = null;
            // loop counter
            let i = 0, j = 0;
            // whether it is necessary to ask the user before removing row or table
            let askUser = false;
            // the resulting promise
            let promise = null;
            // whether it is necessary to set the table grid again, after removing cells
            let refreshTableGrid = false;
            // whether the delete column operation is required with activated change tracking
            // This delete is required, if the column was inserted with change tracking
            let deleteForChangeTrackRequired = false;
            // whether it is required to check for range markers inside the selection
            const isRangeCheckRequired = !this.getRangeMarker().isEmpty();
            // all classes used to identify the range markers
            let allRangeMarkerSelector = isRangeCheckRequired ? DOM.RANGEMARKERNODE_SELECTOR : null;
            // the collector for all range marker nodes (including comments)
            let allRangeMarkerNodes = $();
            // a container for all IDs of removed comments (OT)
            const allRemovedCommentIDs = [];
            // the change track object
            const changeTrack = this.getChangeTrack();
            // created operation
            let newOperation = null;
            // whether this is a table that contains at least one (horizontally) merged cell
            let isTableWithMergedCells = false;
            // a collector for all delete operations for cells in one row
            let allDeleteOpsInOneRow = null;

            // helper function, that is necessary because the user might be asked,
            // if he really wants to delete the selected rows.
            const doDeleteColumns = () => {

                // apply the operations (undo group is created automatically)
                this.setGUITriggeredOperation(true);
                this.applyOperations(generator);
                this.setGUITriggeredOperation(false);
                this.setRequiresElementFormattingUpdate(true);

                // setting the cursor position after deleting the content
                selection.setTextSelection(this.getLastOperationEnd());
            };

            // helper function to determine a valid new start position for range markers. This is necessary, if
            // from an existing range, only the start marker is removed, but not the end marker.
            const getValidInsertStartRangePosition = () => {

                // the logical position for inserting range markers
                let insertStartRangePosition = null;

                if (isCompleteTable || deletedAllRows) {
                    // Find the first valid position behind the table
                    insertStartRangePosition = Position.getFirstPositionInParagraph(this.getCurrentRootNode(), Position.increaseLastIndex(tablePos));
                    // because the table is removed completely, the position needs to be reduced by 1
                    insertStartRangePosition[insertStartRangePosition.length - 2] -= 1;
                }

                return insertStartRangePosition;
            };

            // Search not only for range markers, but also for comments, if required
            if (allRangeMarkerSelector && this.#docApp.isTextApp() && !this.getCommentLayer().isEmpty()) { allRangeMarkerSelector += ', ' + DOM.COMMENTPLACEHOLDER_NODE_SELECTOR; }

            // Generating only one operation, if the complete table is removed.
            // Otherwise sending operations for removing each columns, maybe for rows and
            // for setting new table attributes.
            if (isCompleteTable) {
                allRows = DOM.getTableRows(tableNode);
                // not setting attributes, if this table was inserted with change tracking by the current user
                if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(tableNode) && !changeTrack.allAreInsertedNodesByCurrentAuthor(allRows)) {
                    // Table is not (necessarily) marked with 'inserted', but all rows
                    // -> setting changes attribute at all rows, not at the table
                    _.each(allRows, (_row, index) => {
                        const rowPosition = _.clone(tablePos);
                        rowPosition.push(index);
                        newOperation = { start: _.copy(rowPosition), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                    });
                } else {
                    newOperation = { start: _.copy(tablePos, true) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(DELETE, newOperation);
                    if (this.#containsUnrestorableElements(tablePos)) { askUser = true; }
                    if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(tableNode, allRangeMarkerSelector)); }
                }
            } else {
                isTableWithMergedCells = DOM.isTableWithMergedCells(tableNode); // for OT the deleteColumns operation cannot be used (OX Text)

                // generating delete columns operation, but further operations might be necessary
                // -> if this is necessary for change tracking, this need to be evaluated using deleteForChangeTrackRequired
                if (!changeTrack.isActiveChangeTracking() && !isTableWithMergedCells) {
                    newOperation = { start: _.copy(tablePos), startGrid, endGrid };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(COLUMNS_DELETE, newOperation);
                }

                // Checking, if there will be rows without cells after the columns are deleted
                // -> this is only possible in OX Text, where horizontally merged cells are removed completely.
                // -> in OX Presentation it is not possible to delete a complete row, if not the complete table is deleted.
                if (!this.useSlideMode()) {

                    maxRow = getRowCount(tableNode) - 1;
                    deletedAllRows = true;
                    allRows = DOM.getTableRows(tableNode);
                    allCellRemovePositions = getAllRemovePositions(allRows, startGrid, endGrid);

                    for (i = maxRow; i >= 0; i--) {
                        rowPos = _.clone(tablePos);
                        rowPos.push(i);
                        currentRowNode = Position.getDOMPosition(rootNode, rowPos).node;
                        oneRowCellArray =  allCellRemovePositions[i];
                        end = oneRowCellArray.pop();
                        start = oneRowCellArray.pop();

                        if ($(currentRowNode).children().length === (end - start + 1)) {
                            // checking row attributes
                            if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(currentRowNode)) { // not setting attributes, if this row was inserted with change tracking
                                newOperation = { start: _.copy(rowPos, true), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                this.extendPropertiesWithTarget(newOperation, target);
                                generator.generateOperation(SET_ATTRIBUTES, newOperation);
                            } else {
                                newOperation = { start: _.copy(rowPos) };
                                this.extendPropertiesWithTarget(newOperation, target);
                                generator.generateOperation(DELETE, newOperation);
                                if (this.#containsUnrestorableElements(rowPos)) { askUser = true; }
                                if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(currentRowNode, allRangeMarkerSelector)); }
                                refreshTableGrid = true;
                                deleteForChangeTrackRequired = true;
                            }
                        } else {
                            deletedAllRows = false;
                            // checking unrestorable content in all cells, that will be removed
                            if (isTableWithMergedCells) { allDeleteOpsInOneRow = []; }

                            for (j = start; j <= end; j++) {
                                cellPos = _.clone(rowPos);
                                cellPos.push(j);

                                // marking all cells with change track attribute (TODO: Also necessary to check the parent nodes?)
                                cellNode = Position.getSelectedTableCellElement(rootNode, cellPos);
                                if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(cellNode)) { // not setting attributes, if this cell was inserted with change tracking
                                    newOperation = { start: _.copy(cellPos, true), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                    this.extendPropertiesWithTarget(newOperation, target);
                                    generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                } else {
                                    if (this.#containsUnrestorableElements(cellPos)) { askUser = true; }
                                    if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(cellNode, allRangeMarkerSelector)); }
                                    refreshTableGrid = true;
                                    deleteForChangeTrackRequired = true;

                                    // generating delete operations for each cell, if the table contains (horizontally) merged cells (OT)
                                    if (isTableWithMergedCells) {
                                        newOperation = { start: _.copy(cellPos, true) };
                                        this.extendPropertiesWithTarget(newOperation, target);
                                        allDeleteOpsInOneRow.push(newOperation);
                                    }
                                }
                                if (j === end && allDeleteOpsInOneRow && allDeleteOpsInOneRow.length > 0) {
                                    allDeleteOpsInOneRow.reverse();
                                    _.each(allDeleteOpsInOneRow, oneOpOptions => {
                                        generator.generateOperation(DELETE, oneOpOptions);
                                    });
                                }
                            }
                        }
                    }

                    if (changeTrack.isActiveChangeTracking() && deleteForChangeTrackRequired && !isTableWithMergedCells) {
                        newOperation = { start: _.copy(tablePos), startGrid, endGrid };
                        this.extendPropertiesWithTarget(newOperation, target);
                        generator.generateOperation(COLUMNS_DELETE, newOperation);
                    }

                    // Deleting the table explicitely, if all its content was removed
                    if (deletedAllRows) {
                        // not setting attributes, if this table was inserted with change tracking by the current user
                        if (changeTrack.isActiveChangeTracking() && !changeTrack.isInsertNodeByCurrentAuthor(tableNode) && !changeTrack.allAreInsertedNodesByCurrentAuthor(allRows)) {
                            // Table is not (necessarily) marked with 'inserted', but all rows
                            // -> setting changes attribute at all rows, not at the table
                            _.each(allRows, (_row, index) => {
                                const rowPosition = _.clone(tablePos);
                                rowPosition.push(index);
                                newOperation = { start: _.copy(rowPosition), attrs: { changes: { removed: changeTrack.getChangeTrackInfo() } } };
                                this.extendPropertiesWithTarget(newOperation, target);
                                generator.generateOperation(SET_ATTRIBUTES, newOperation);
                            });
                        } else {
                            newOperation = { start: _.clone(tablePos) };
                            this.extendPropertiesWithTarget(newOperation, target);
                            generator.generateOperation(DELETE, newOperation);
                            if (this.#containsUnrestorableElements(tablePos)) { askUser = true; }
                            if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(tableNode, allRangeMarkerSelector)); }
                        }
                    } else {
                        if (refreshTableGrid) {
                            // Setting new table grid attribute to table (but not, if change tracking is activated
                            tableGrid = _.clone(this.tableStyles.getElementAttributes(tableNode).table.tableGrid);
                            tableGrid.splice(startGrid, endGrid - startGrid + 1);  // removing column(s) in tableGrid (automatically updated in table node)
                            newOperation = { attrs: { table: { tableGrid } }, start: _.clone(tablePos) };
                            this.extendPropertiesWithTarget(newOperation, target);
                            generator.generateOperation(SET_ATTRIBUTES, newOperation);
                            this.setRequiresElementFormattingUpdate(false);   // no call of implTableChanged -> attributes are already set in implSetAttributes
                        }
                    }
                } else { // presentation app

                    if (isTableWithMergedCells) { // special handling for tables with merged cells, because deleteColumns cannot be used for OT
                        // delete all cells that are completely in the deleted range -> broader cells are shrinked
                        maxRow = getRowCount(tableNode) - 1;
                        allRows = DOM.getTableRows(tableNode);
                        allCellRemovePositions = getAllRemovePositions(allRows, startGrid, endGrid);

                        for (i = maxRow; i >= 0; i--) {
                            rowPos = _.clone(tablePos);
                            rowPos.push(i);
                            currentRowNode = Position.getDOMPosition(rootNode, rowPos).node;
                            oneRowCellArray =  allCellRemovePositions[i];
                            end = oneRowCellArray.pop();
                            start = oneRowCellArray.pop();

                            allDeleteOpsInOneRow = [];

                            for (j = start; j <= end; j++) {
                                cellPos = _.clone(rowPos);
                                cellPos.push(j);

                                cellNode = Position.getSelectedTableCellElement(rootNode, cellPos);
                                if (this.#containsUnrestorableElements(cellPos)) { askUser = true; }
                                if (isRangeCheckRequired) { allRangeMarkerNodes = allRangeMarkerNodes.add(Position.collectElementsBySelector(cellNode, allRangeMarkerSelector)); }

                                // generating delete operations for each fully covered cell
                                // generating setAttributes operation with reduced cell width for each partly covered cell

                                let removeCell = true;
                                if (isHorizontallyMergedCell(cellNode)) {
                                    // is it completely covered by the specified grid range?
                                    const gridPos = getGridColumnRangeOfCell(cellNode); // the grid position of the cell
                                    if (gridPos.start < startGrid || gridPos.end > endGrid) {
                                        removeCell = false;  // not removing this cell, but reduce it in size
                                        let cellLength = gridPos.end - gridPos.start + 1; // the old length of the table cell
                                        if (gridPos.start <= startGrid && gridPos.end >= endGrid) {
                                            cellLength -= (endGrid - startGrid + 1);
                                        } else {
                                            if (endGrid > gridPos.end) {
                                                cellLength -= (gridPos.end - startGrid + 1);
                                            } else if (startGrid < gridPos.start) {
                                                cellLength -= (endGrid - gridPos.start + 1);
                                            }
                                        }

                                        newOperation = { start: _.copy(cellPos, true), attrs: { cell: { gridSpan: cellLength } } };
                                        this.extendPropertiesWithTarget(newOperation, target);
                                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                    }
                                }

                                if (removeCell) {
                                    newOperation = { start: _.copy(cellPos, true) };
                                    this.extendPropertiesWithTarget(newOperation, target);
                                    allDeleteOpsInOneRow.push(newOperation);
                                }

                                if (j === end && allDeleteOpsInOneRow && allDeleteOpsInOneRow.length > 0) {
                                    allDeleteOpsInOneRow.reverse();
                                    _.each(allDeleteOpsInOneRow, oneOpOptions => {
                                        generator.generateOperation(DELETE, oneOpOptions);
                                    });
                                }
                            }
                        }
                    }

                    // refreshing the table grid
                    tableGrid = _.clone(this.tableStyles.getElementAttributes(tableNode).table.tableGrid);
                    tableGrid.splice(startGrid, endGrid - startGrid + 1);  // removing column(s) in tableGrid (automatically updated in table node)
                    newOperation = { attrs: { table: { tableGrid } }, start: _.clone(tablePos) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    generator.generateOperation(SET_ATTRIBUTES, newOperation);
                    this.setRequiresElementFormattingUpdate(false);   // no call of implTableChanged -> attributes are already set in implSetAttributes
                }
            }

            // handling for all collected ranges
            if (allRangeMarkerNodes.length > 0) {
                this.getRangeMarker().handlePartlyDeletedRanges(allRangeMarkerNodes, generator, getValidInsertStartRangePosition());
                // collecting all comment IDs for generating deleteComment operations, that are required for OT
                _.each(allRangeMarkerNodes, node => { if (DOM.isCommentPlaceHolderNode(node)) { allRemovedCommentIDs.push(DOM.getTargetContainerId(node)); } });
            }

            // Asking the user, if he really wants to remove the columns, if it contains
            // at least one cell with exceeded size or other unrestorable elements. This cannot be undone.
            if (askUser) {
                promise = this.showDeleteWarningDialog(DELETE_COLUMNS_TITLE, DELETE_COLUMNS_QUERY);
            } else {
                promise = $.when();
            }

            // delete columns on resolved promise
            return promise.done(doDeleteColumns);
        }

        /**
         * Editor API function to generate 'delete' operations for table cells.
         */
        deleteCells() {

            // the selection object
            const selection = this.getSelection();
            const isCellSelection = selection.getSelectionType() === 'cell';
            const startPos = selection.getStartPosition();
            const endPos = selection.getEndPosition();
            let localPos = null;
            // target for operation - if exists, it's for ex. header or footer
            const target = this.getActiveTarget();
            // current element container node
            const rootNode = this.getCurrentRootNode(target);
            // created operation
            let newOperation = null;

            startPos.pop();  // removing character position and paragraph
            startPos.pop();
            endPos.pop();
            endPos.pop();

            const startCol = startPos.pop();
            const endCol = endPos.pop();
            const startRow = startPos.pop();
            const endRow = endPos.pop();
            const tablePos = _.clone(startPos);
            const operations = [];

            for (let i = endRow; i >= startRow; i--) {

                const rowPosition = Position.appendNewIndex(tablePos, i);
                let localStartCol = startCol;
                let localEndCol = endCol;

                if (!isCellSelection && (i < endRow) && (i > startCol)) {
                    // removing complete rows
                    localStartCol = 0;
                    localEndCol = Position.getLastColumnIndexInRow(rootNode, rowPosition);
                }

                for (let j = localEndCol; j >= localStartCol; j--) {
                    localPos = Position.appendNewIndex(rowPosition, j);
                    newOperation = { name: DELETE, start: _.copy(localPos) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    operations.push(newOperation);
                }

                // removing empty row
                const rowNode = Position.getDOMPosition(rootNode, rowPosition).node;
                if ($(rowNode).children().length === 0) {
                    localPos = Position.appendNewIndex(tablePos, i);
                    newOperation = { name: DELETE, start: _.copy(localPos) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    operations.push(newOperation);
                }

                // checking if the table is empty
                const tableNode = Position.getDOMPosition(rootNode, tablePos).node;
                if (getRowCount(tableNode) === 0) {
                    newOperation = { name: DELETE, start: _.clone(tablePos) };
                    this.extendPropertiesWithTarget(newOperation, target);
                    operations.push(newOperation);
                }
            }

            // apply the operations (undo group is created automatically)
            this.applyOperations(operations);

            // setting the cursor position
            selection.setTextSelection(this.getLastOperationEnd());
        }

        // public helper functions --------------------------------------------

        /**
         * Public helper function to make this.#implDeleteText publicly available.
         * For parameters look at definition of 'this.#implDeleteText'.
         */
        implDeleteText(startPosition, endPosition, options, target) {
            return this.#implDeleteText(startPosition, endPosition, options, target);
        }

        // operation handler --------------------------------------------------

        /**
         * The handler for the delete operation.
         *
         * @param {Object} oneOperation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether the specified operation is an external operation.
         *
         * @returns {Boolean}
         *  Whether the delete operation was successful.
         */
        deleteHandler(oneOperation, external) {

            // delete operations might be merged
            const unmergedOperations = this.#getUnmergedOperations(oneOperation);

            return unmergedOperations.every(operation => {

                // node info about the paragraph to be deleted
                let nodeInfo = null;
                // attribute type of the start node
                let type = null;
                // the undo manager
                const undoManager = this.getUndoManager();
                // generator for the undo/redo operations (not required for external operations)
                const generator = (!external && undoManager.isUndoEnabled()) ? this.createOperationGenerator() : null;
                // if its header/footer editing, root node is header/footer node, otherwise editdiv
                const rootNode = this.getRootNode(operation.target);
                // undo operation for passed operation
                let undoOperation = null;

                // undo/redo generation
                if (generator) {

                    let position;
                    let paragraph;
                    let start;
                    let end;

                    nodeInfo = Position.getDOMPosition(rootNode, operation.start, true);
                    type = this.#resolveElementType(nodeInfo.node);

                    switch (type) {

                        case 'text':
                            position = operation.start.slice(0, -1);
                            paragraph = Position.getCurrentParagraph(rootNode, position);
                            start = operation.start[operation.start.length - 1];
                            end = _.isArray(operation.end) ? operation.end[operation.end.length - 1] : start;

                            undoOperation = { start: _.copy(start), end: _.copy(end), target: operation.target, clear: true };
                            generator.generateParagraphChildOperations(paragraph, position, undoOperation);
                            undoManager.addUndo(generator.getOperations(), operation);
                            break;

                        case 'paragraph':
                            if (!DOM.isImplicitParagraphNode(nodeInfo.node)) {
                                generator.generateParagraphOperations(nodeInfo.node, operation.start, { target: operation.target });

                                this.trigger('operation:delete:paragraph', { operation, generator, external });

                                undoManager.addUndo(generator.getOperations(), operation);
                            }
                            break;

                        case 'cell':
                            generator.generateTableCellOperations(nodeInfo.node, operation.start, { target: operation.target });
                            undoManager.addUndo(generator.getOperations(), operation);
                            break;

                        case 'row':
                            generator.generateTableRowOperations(nodeInfo.node, operation.start, { target: operation.target });
                            undoManager.addUndo(generator.getOperations(), operation);
                            break;

                        case 'table':
                            if (!DOM.isExceededSizeTableNode(nodeInfo.node)) {
                                generator.generateTableOperations(nodeInfo.node, operation.start, { target: operation.target }); // generate undo operations for the entire table
                                undoManager.addUndo(generator.getOperations(), operation);
                            }
                            if (DOM.isTableInTableNode(nodeInfo.node)) {
                                if (($(nodeInfo.node).next().length > 0) && (DOM.isImplicitParagraphNode($(nodeInfo.node).next()))) {
                                    $(nodeInfo.node).next().css('height', '');  // making paragraph behind the table visible after removing the table
                                }
                            }
                            break;
                    }
                }

                // finally calling the implementation function to delete the content
                return this.#implDelete(operation.start, operation.end, operation.target);
            });
        }

        /**
         * The handler for the delete column operation.
         *
         * @param {Object} operation
         *  The operation object.
         *
         * @param {Boolean} external
         *  Whether this is an external operation.
         *
         * @returns {Boolean}
         *  Whether the delete operation was successful.
         */
        deleteColumnsHandler(operation, external) {

            // the current root node
            const rootNode = this.getRootNode(operation.target);
            // the table node
            const table = this.useSlideMode() ? Position.getDrawingElement(rootNode, operation.start) : Position.getTableElement(rootNode, operation.start);
            // the undo manager
            const undoManager = this.getUndoManager();

            if (table) {

                if (undoManager.isUndoEnabled() && !external) {

                    const allRows = DOM.getTableRows(table);
                    const allCellRemovePositions = getAllRemovePositions(allRows, operation.startGrid, operation.endGrid);
                    const generator = this.createOperationGenerator();

                    // In OX Text the cells are simply removed. Horizontally merged cells are not handled. Therefore the
                    // length of each row might be different. In OX Presentation, merged cells are reduced in length, so
                    // that every row has the same length.

                    allRows.get().forEach((oneRow, index) => {

                        const rowPos = operation.start.concat([index]);
                        const cells = $(oneRow).children();
                        const oneRowCellArray =  allCellRemovePositions[index];
                        let end = oneRowCellArray.pop();
                        const start = oneRowCellArray.pop(); // more than one cell might be deleted in a row
                        let counter = 0;
                        let cellLength = 0;
                        let allAffectedCells = null;
                        let newOperation = null;

                        // start < 0: no cell will be removed in this row
                        if (start >= 0) {

                            if (end < 0) {
                                // remove all cells until end of row
                                end = cells.length;
                            } else {
                                // closed range to half-open range
                                end = Math.min(end + 1, cells.length);
                            }

                            if (this.useSlideMode()) {

                                // try to find only partly covered cells
                                // -> for those cells a setAttributes operation must be created with the correct colSpan value
                                // -> the first or the last cell might be covered partly
                                allAffectedCells = cells.slice(start, end);

                                _.each(allAffectedCells, oneCell => {

                                    // the logical position of the cell
                                    const cellPos = Position.appendNewIndex(rowPos, (start + counter));
                                    // whether the cell is decreased in size
                                    let reduceCellLength = false;
                                    // the grid position of the cell
                                    let gridPos = null;

                                    // check, if the table removed completely
                                    if (isHorizontallyMergedCell(oneCell)) {

                                        // is it completely covered by the specified grid range?
                                        gridPos = getGridColumnRangeOfCell(oneCell);

                                        if (gridPos.start < operation.startGrid || gridPos.end > operation.endGrid) {
                                            reduceCellLength = true;
                                        }
                                    }

                                    if (reduceCellLength) {
                                        cellLength = getHorizontalCellLength(oneCell);
                                        // generate setAttribute Operation
                                        newOperation = { attrs: { cell: { gridSpan: cellLength } }, start: _.copy(cellPos) };
                                        this.extendPropertiesWithTarget(newOperation, operation.target);
                                        generator.generateOperation(SET_ATTRIBUTES, newOperation);
                                    } else {
                                        generator.generateTableCellOperations(oneCell, cellPos, { target: operation.target });
                                    }

                                    counter++;
                                });

                            } else {
                                // generate operations for all covered cells
                                cells.slice(start, end).get().forEach((oneCell, index) => {
                                    generator.generateTableCellOperations(oneCell, rowPos.concat([start + index]), { target: operation.target });
                                });
                            }
                        }
                    });

                    undoManager.addUndo(generator.getOperations(), operation);
                }

                this.#implDeleteColumns(operation.start, operation.startGrid, operation.endGrid, operation.target);
            }

            return true;
        }

        /**
         * Checking whether a node contains an element, that cannot be restored
         * in undo action. This are tables with exceeded size or drawings that
         * are not of type 'image'.
         *
         * @param {Node|jQuery|Number[]} node
         *  The node, whose descendants are investigated. For convenience reasons
         *  this can also be a logical position that is transformed into a node
         *  automatically.
         *
         * @param {Number} [startPos]
         *  An optional offset that can be used to reduce the search range inside
         *  the specified node.
         *
         * @param {Number} [endPos]
         *  An optional offset that can be used to reduce the search range inside
         *  the specified node.
         *
         * @returns {Boolean}
         *  Returns true, if the node contains an unrestorable descendant,
         *  otherwise false.
         */
        containsUnrestorableElements(node, startPos, endPos) {
            return this.#containsUnrestorableElements(node, startPos, endPos);
        }

        /**
         * Helper function for asking the user, if unrestorable content shall be deleted
         *
         * @param {Boolean} askUser
         *  Whether a dialog shall be shown to inform the user about unrestoreable content
         *  that will be deleted.
         *
         * @param {Boolean} asyncDelete
         *  Whether the delete process will be asynchronous.
         *
         * @returns {jQuery.Promise}
         *  The promise for the dialog. If no dialog is shown, the promise is immediately
         *  resolved.
         */
        askUserHandler(askUser, asyncDelete) {

            if (!askUser) { return $.when(); }

            // the promise for asking the user (if required)
            let askUserPromise = null;

            // Asking the user, if he really wants to remove the content, if it contains
            // a table with exceeded size or other unrestorable elements. This cannot be undone.
            askUserPromise = this.showDeleteWarningDialog(DELETE_CONTENTS_TITLE, DELETE_CONTENTS_QUERY);
            // return focus to editor after 'No' button
            askUserPromise.fail(() => {
                // user abort during visible dialog -> no undo required -> leaving busy mode immediately
                if (asyncDelete) { this.leaveAsyncBusy(); }
                this.#docApp.getView().grabFocus();
            });

            return askUserPromise;
        }

        // private methods ----------------------------------------------------

        /**
         * Check the validity of the new selection position after removal of the selected content.
         * The new position can be compared with the start position and the end position before the
         * removal.
         *
         * @param {Node|jQuery} rootNode
         *  The root node corresponding to the logical position.
         *
         * @param {Position} newPosition
         *  The calculated new logical position after removal.
         *
         * @param {Position} oldStartPosition
         *  The old start position of the selection before removal.
         *
         * @param {Position} _oldEndPosition
         *  The old end position of the selection before removal.
         *
         * @returns {Position|undefined}
         *  Returns a new logical position, if the "newPosition" is not valid. Otherwise "undefined"
         *  is returned.
         */
        #validatePositionAfterRemoval(rootNode, newPosition, oldStartPosition, _oldEndPosition) {

            if (ary.last(oldStartPosition) !== 0) { return; } // nothing to do -> only last text position inside paragraph is critical
            if (ary.equals(newPosition, oldStartPosition)) { return; } // nothing to do -> new position is fine

            // comparing the paragraph positions -> is the new position before the old start position? (DOCS-5148)
            if (newPosition.length === oldStartPosition.length && newPosition.length > 1 && ary.at(newPosition, -2) < ary.at(oldStartPosition, -2)) {
                const isValidOldStartPosition = Position.isValidElementPosition(rootNode, oldStartPosition, { allowFinalPosition: true });
                if (isValidOldStartPosition) { return oldStartPosition; } // reuse the old start position
            }
        }

        /**
         * Checking whether a node contains an element, that cannot be restored
         * in undo action. This are tables with exceeded size or drawings that
         * are not of type 'image'.
         *
         * @param {Node|jQuery|Number[]} node
         *  The node, whose descendants are investigated. For convenience reasons
         *  this can also be a logical position that is transformed into a node
         *  automatically.
         *
         * @param {Number} [startOffset]
         *  An optional offset that can be used to reduce the search range inside
         *  the specified node.
         *
         * @param {Number} [endOffset]
         *  An optional offset that can be used to reduce the search range inside
         *  the specified node.
         *
         * @returns {Boolean}
         *  Returns true, if the node contains an unrestorable descendant,
         *  otherwise false.
         */
        #containsUnrestorableElements(node, startOffset, endOffset) {

            // whether the node contains an unrestorable descendant
            let contains = false;
            // a dom point calculated from logical position
            let domPos = null;
            // a locally defined helper node
            let searchNode = null;
            // the logical position of a non-image drawing
            let drawingPos = null;

            // supporting for convenience also logical positions, that are automatically
            // converted into dom nodes
            if (_.isArray(node)) {
                domPos = Position.getDOMPosition(this.getCurrentRootNode(), node, true);
                if (domPos && domPos.node) { searchNode = domPos.node; }
            } else {
                searchNode = node;
            }

            if (searchNode) {
                if ($(searchNode).find(DOM.TABLE_SIZE_EXCEEDED_NODE_SELECTOR).length > 0) {
                    contains = true;
                } else {
                    // checking if the table or paragraph contains drawings, that are not of type 'image'
                    // or if the table contains another table with exceeded size
                    $(NODE_SELECTOR + ', ' + DOM.DRAWINGPLACEHOLDER_NODE_SELECTOR, searchNode).get().forEach(drawing => {

                        if (this.useSlideMode()) {
                            // in slide mode drawing with type 'undefined' cannot be restored
                            if (getDrawingType(drawing) === 'undefined') {
                                if (_.isNumber(startOffset) && (_.isNumber(endOffset))) {
                                    // the logical position of the drawing with type undefined
                                    drawingPos = Position.getOxoPosition(this.getCurrentRootNode(), drawing, 0);
                                    if (Position.isNodePositionInsideRange(drawingPos, startOffset, endOffset)) {
                                        contains = true;
                                    }
                                } else {
                                    contains = true;
                                }
                            }
                        } else {

                            if (DOM.isDrawingPlaceHolderNode(drawing)) { drawing = DOM.getDrawingPlaceHolderNode(drawing); }

                            if (DOM.isUnrestorableDrawingNode(drawing)) {
                                // if only a part of a paragraph is deleted, it has to be checked,
                                // if the non-image and non-textframe drawing is inside this selected part.
                                if (_.isNumber(startOffset) && (_.isNumber(endOffset))) {
                                    // the logical position of the non-image drawing
                                    drawingPos = Position.getOxoPosition(this.getCurrentRootNode(), drawing, 0);
                                    if (Position.isNodePositionInsideRange(drawingPos, startOffset, endOffset)) {
                                        contains = true;
                                    }
                                } else {
                                    contains = true;
                                }
                            }
                        }
                    });
                }

                // if node contains comment place holders, they also can contain unrestorable content
                if (!contains && this.#docApp.isTextApp()) {
                    contains = this.getCommentLayer().checkUnrestorableCommentNode(searchNode, startOffset, endOffset);
                }

                // if node contains special page number fields in header and footer restore original state before delete
                this.getFieldManager().checkRestoringSpecialFields(searchNode, startOffset, endOffset);
            }

            return contains;
        }

        /**
         * Checking whether there can be an undo for the delete operation.
         * Undo is not possible, if a table with exceeded size is removed,
         * or if a drawing, that is not of type image is removed.
         *
         * @param {String} type
         *  The type of the node to be removed. This can be 'text', 'paragraph',
         *  'cell', 'row' and table.
         *
         * @param {Node|jQuery} node
         *  The node to be removed.
         *
         * @param {Number[]} start
         *  The logical start position of the element or text range to be
         *  deleted.
         *
         * @param {Number[]} [end]
         *  The logical end position of the element or text range to be
         *  deleted. This is only relevant for type 'text'.
         *
         * @param {String[]} [target]
         *  If element is marginal, target is needed in group with
         *  start and end to determine root container.
         */
        #checkDisableUndoStack(type, node, start, end, target) {

            // info about the parent paragraph or table node
            let position = null, paragraph = null;
            // last index in start and end position
            let startOffset = 0, endOffset = 0;

            // deleting the different types:
            switch (type) {

                case 'text':
                    // get paragraph node from start position
                    if (!_.isArray(start) || (start.length === 0)) {
                        globalLogger.warn('Editor.disableUndoStack(): missing start position');
                        return false;
                    }
                    position = start.slice(0, -1);
                    paragraph = Position.getParagraphElement(this.getRootNode(target), position);
                    if (!paragraph) {
                        globalLogger.warn('Editor.disableUndoStack(): no paragraph found at position ' + JSON.stringify(position));
                        return false;
                    }

                    // validate end position
                    if (_.isArray(end) && !Position.hasSameParentComponent(start, end)) {
                        globalLogger.warn('Editor.disableUndoStack(): range not in same paragraph');
                        return false;
                    }

                    // start and end offset in paragraph
                    startOffset = _.last(start);
                    endOffset = _.isArray(end) ? _.last(end) : startOffset;
                    if (endOffset === -1) { endOffset = undefined; }

                    // visit all covered child nodes (iterator allows to remove the visited node)
                    Position.iterateParagraphChildNodes(paragraph, localnode => {

                        if (DOM.isDrawingPlaceHolderNode(localnode)) { localnode = DOM.getDrawingPlaceHolderNode(localnode); }

                        if (DOM.isUnrestorableDrawingNode(localnode)) {
                            this.setDeleteUndoStack(true);
                        }

                        if (this.#docApp.isTextApp() && DOM.isCommentPlaceHolderNode(localnode) && this.getCommentLayer().checkUnrestorableCommentNode(localnode)) {
                            this.setDeleteUndoStack(true);
                        }

                        if (DOM.isSpecialField(localnode)) {
                            this.getFieldManager().restoreSpecialField(localnode);
                        }
                    }, undefined, { start: startOffset, end: endOffset });
                    break;

                case 'paragraph':
                    if (this.#containsUnrestorableElements(node)) {
                        this.setDeleteUndoStack(true);
                    }
                    break;

                case 'cell':
                    if (this.#containsUnrestorableElements(node)) {
                        this.setDeleteUndoStack(true);
                    }
                    break;

                case 'row':
                    if (this.#containsUnrestorableElements(node)) {
                        this.setDeleteUndoStack(true);
                    }
                    break;

                case 'table':
                    if (DOM.isExceededSizeTableNode(node) || this.#containsUnrestorableElements(node)) {
                        this.setDeleteUndoStack(true);
                    }
                    break;
            }
        }

        /**
         * Collecting all list style IDs of the paragraphs inside the specified elements.
         *
         * @param {Array|HTMLElement|jQuery} nodes
         *  The collection of nodes, that will be checked for paragraphs with list styles.
         *  This can be an array with html nodes or jQuery nodes. It can also be a html node
         *  or a jQuery object not included into an array. Above all nodes is iterated.
         *
         * @returns {[Array]}
         *  An array containing the list style IDs of the paragraphs inside the specified elements.
         */
        #getListStyleIDsOfParagraphs(nodes) {

            let listStyleIDs = null;

            if ((!nodes) || (_.isArray(nodes) && _.isEmpty(nodes))) { return listStyleIDs; }

            _.chain(nodes).getArray().any(oneNode => {
                $(oneNode).get().forEach(node => {
                    $(node).find(DOM.PARAGRAPH_NODE_SELECTOR).get().forEach(paragraph => {
                        // checking paragraph attributes for list styles
                        const paraAttrs = getExplicitAttributeSet(paragraph);
                        // updating lists, if required
                        if (this.isListStyleParagraph(null, paraAttrs) && paraAttrs && paraAttrs.paragraph) {
                            listStyleIDs = listStyleIDs || [];
                            if (!_.contains(listStyleIDs, paraAttrs.paragraph.listStyleId)) { listStyleIDs.push(paraAttrs.paragraph.listStyleId); }
                        }
                    });
                });
            });

            return listStyleIDs;
        }

        /**
         * Check, whether a given collection of nodes contain paragraphs with list
         * styles.
         *
         * @param {Array|HTMLElement|jQuery} nodes
         *  The collection of nodes, that will be checked for paragraphs with list styles.
         *  This can be an array with html nodes or jQuery nodes. It can also be a html node
         *  or a jQuery object not included into an array. Above all nodes is iterated.
         *
         * @returns {Boolean}
         *  Whether at least one of the specified nodes contains at least one list paragraph node.
         */
        #elementsContainListStyleParagraph(nodes) {

            let isListParagraph = false;

            if ((!nodes) || (_.isArray(nodes) && _.isEmpty(nodes))) { return isListParagraph; }

            _.chain(nodes).getArray().any(oneNode => {
                $(oneNode).get().forEach(node => {
                    $(node).find(DOM.PARAGRAPH_NODE_SELECTOR).get().forEach(paragraph => {
                        if (this.isListStyleParagraph(paragraph)) {
                            isListParagraph = true;
                            return !isListParagraph; // stopping 'each' iteration, if list paragraph was found
                        }
                    });
                    return !isListParagraph; // stopping 'each' iteration, if list paragraph was found
                });
                return isListParagraph; // stopping 'any' iteration, if list paragraph was found
            });

            return isListParagraph;
        }

        /**
         * Returns the type of the attributes supported by the passed DOM
         * element.
         *
         * @param {HTMLElement|jQuery} element
         *  The DOM element whose associated attribute type will be returned.
         *  If this object is a jQuery collection, returns its first node.
         *
         * @returns {String}
         *  A type for the specified element.
         */
        #resolveElementType(element) {

            // the element, as jQuery object
            const $element = $(element);
            // the resulting style type
            let type = null;

            if (DOM.isPartOfParagraph($element)) {
                type = 'text';
            } else if (DOM.isParagraphNode($element)) {
                type = 'paragraph';
            } else if (DOM.isTableNode($element)) {
                type = 'table';
            } else if ($element.is('tr')) {
                type = 'row';
            } else if ($element.is('td')) {
                type = 'cell';
            } else {
                globalLogger.warn('Editor.resolveElementType(): unsupported element');
            }

            return type;
        }

        /**
         * Returns a valid text position at the passed component position
         * (paragraph or table). Used to calculate the new text cursor position
         * after deleting a component.
         *
         * @returns {Number[]}
         *  A valid logical text position.
         */
        #getValidTextPosition(position) {

            // container root node of table
            const rootNode = this.getCurrentRootNode();
            // a logical text position
            let textPosition = Position.getFirstPositionInParagraph(rootNode, position);

            // fall-back to last position in document (e.g.: last table deleted)
            if (!textPosition) {
                textPosition = Position.getLastPositionInParagraph(rootNode, [rootNode[0].childNodes.length - 1]);
            }

            return textPosition;
        }

        /**
         * Helper function to generate multiple delete operations from one delete operation that
         * deletes content from more than one paragraph or table. The handler for the delete
         * operation requires, that every single step is within one paragraph.
         * This is the contrary function to 'this.#checkMergeOfDeleteOperations'.
         *
         * @param {Object} operation
         *  The operation object with the operation that needs to be splitted in several paragraph
         *  specific operations.
         *
         * @returns {Object[]}
         *  An array of delete operation(s) generated from the one specified delete operation.
         */
        #getUnmergedOperations(operation) {

            if (!operation.end) { return [operation]; } // simple case, no end position -> no unmerge

            const operations = [];

            // generating operations, so that only one paragraph is affected by the operation
            // delete operations need to be generated in reverse order
            const start = _.copy(operation.start);
            const end = _.copy(operation.end);

            // the target node
            const rootNode = this.getRootNode(operation.target);

            // determining the index of the paragraph/table node inside the start position
            const paraIndex = Position.getLastIndexInPositionByNodeName(rootNode, start, DOM.CONTENT_NODE_SELECTOR);
            const startParaPos = start[paraIndex];
            const endParaPos = end[paraIndex];

            if (!operation.end) { return [operation]; } // simple case, this is not a merged operation
            if (_.isEqual(_.first(start, paraIndex + 1), _.first(end, paraIndex + 1))) { return [operation]; } // simple case, inside one paragraph

            // counter to create logical positions of the paragraphs
            let currentParaPos = startParaPos;
            // the logical position of the paragraph
            const fullParaPos = (start.length > paraIndex + 1) ? _.initial(operation.start) : _.copy(operation.start);

            let startPos = null;
            let endPos = null;

            let newOperation = _.copy(operation, true);

            // generating the first operation
            if (start.length > paraIndex + 1) {
                endPos = _.copy(fullParaPos);
                endPos.push(Position.getParagraphLength(rootNode, endPos) - 1);
                newOperation.end = endPos;
            } else if (start.length === paraIndex + 1) {
                delete newOperation.end; // only using start position, if complete paragraph/table will be deleted
            }

            operations.push(newOperation);
            currentParaPos++;

            // generating the middle operations, if required
            while (currentParaPos < endParaPos) {
                newOperation = _.copy(operation, true);
                newOperation.start = _.copy(fullParaPos);
                newOperation.start[newOperation.start.length - 1] = currentParaPos; // increasing only the final position
                delete newOperation.end;

                currentParaPos++;
                operations.unshift(newOperation); // inserting before the previous operation
            }

            newOperation = _.copy(operation, true);

            // generating the final operation
            if (end.length > paraIndex + 1) {
                startPos = _.copy(end);
                startPos[startPos.length - 1] = 0;
                newOperation.start = startPos;
                newOperation.end = _.copy(end);
            } else if (end.length === paraIndex + 1) {
                newOperation.start = _.copy(end);
                delete newOperation.end; // only using start position, if complete paragraph/table will be deleted
            }

            // adding the final operation
            operations.unshift(newOperation); // inserting before the previous operation

            return operations;
        }

        /**
         * Check, whether it is possible to generate less delete operations.
         * This is for example the case, if only full blocks of paragraphs/tables are deleted
         * and only the start and end position are text selections.
         * This is the contrary function to 'this.#getUnmergedOperations'.
         */
        #checkMergeOfDeleteOperations(generator) {

            const allOperations = generator.getOperations();
            let currentMergeOperation = null;

            // helper function that merges two following delete operations, if possible
            const mergeTwoOperations = (op1, op2) => {
                // both must be delete operations with the same target
                if (op1.name !== DELETE || op2.name !== DELETE || op1.target !== op2.target) { return false; }

                // both must work on the same paragaph -> determining the index of the paragraph (not table!) inside the start position
                // the target string
                const target = firstOperation.target;
                // the target node
                const rootNode = this.getRootNode(target);
                // the paragraph index in the position
                const paraIndex = Position.getLastIndexInPositionByNodeName(rootNode, op1.start, DOM.PARAGRAPH_NODE_SELECTOR); // zero based value

                if (!_.isNumber(paraIndex)) { return false; } // failed to determine paragraph (for example row selection)

                if (Math.abs(op1.start.length - op2.start.length) > 1) { return false; } // both operations can differ in length only by 1

                if (paraIndex > 0 && !_.isEqual(_.first(op1.start, paraIndex), _.first(op2.start, paraIndex))) { return false; } // both operations use the same paragraph parent

                // is the paragraph position increased by 1 ? (this might be an already merged operation, so that 'end' is different from start paragraph)
                if (op1.end) {
                    if (op1.end[paraIndex] !== op2.start[paraIndex] - 1) { return false; }
                } else {
                    if (op1.start[paraIndex] !== op2.start[paraIndex] - 1) { return false; }
                }

                // is the second paragraph start at position 0 inside the paragraph and is the first paragraph completely selected?
                if ((op2.start.length === paraIndex + 2) && (_.last(op2.start) > 0)) { return false; }
                if (op1.end && (op1.end.length === paraIndex + 2) && (_.last(op1.end) !== (Position.getParagraphLength(rootNode, _.first(op1.end, (paraIndex + 1))) - 1))) { return false; }

                // -> both operations can be merged
                if (op2.end) {
                    op1.end = op2.end;
                } else {
                    op1.end = op2.start;
                }

                currentMergeOperation = op1; // saving the currentMergeOperation for the next iteration

                return true; // merged two operations
            };

            // helper function, that merges all possible delete operations
            const mergeDeleteOperations = () => {
                let merged = false;
                let newOperations = null;
                _.each(allOperations, (op, index) => {
                    if (!op) { op = currentMergeOperation; }
                    const nextOp = allOperations[index + 1];
                    if (nextOp) {
                        const didMerge = mergeTwoOperations(op, allOperations[index + 1]);
                        if (didMerge) {
                            allOperations[index + 1] = null;
                            merged = true;
                        }
                        if (currentMergeOperation && !didMerge) { currentMergeOperation = null; }
                    }
                });

                if (merged) {
                    newOperations = _.filter(allOperations, op => {
                        return op !== null; // removing the merged operations
                    });
                }

                return newOperations;
            };

            // helper function, that checks if all operations are directly following paragraphs operations.
            // Only the first and last operation can additionally have text selection ranges.
            // Also checking target!
            const isSimpleOperationRange = () => {

                // the target string
                const target = firstOperation.target;
                // the target node
                const rootNode = this.getRootNode(target);
                // determining the index of the paragraph (not table!) inside the start position
                const paraIndex = Position.getLastIndexInPositionByNodeName(rootNode, firstOperation.start, DOM.PARAGRAPH_NODE_SELECTOR); // zero based value
                // the position must be the paragraph position or one more for the text
                const lastIndex = firstOperation.start.length - 1;

                if (firstOperation.name !== DELETE) { return false; } // no delete operation -> no merge
                if (!_.isNumber(paraIndex)) { return false; } // failed to determine paragraph (for example row selection)

                if (lastIndex - paraIndex <= 1) {
                    // if there is an additional position, the end position must be the last position of the paragraph
                    if (lastIndex - paraIndex === 1) {
                        if (!firstOperation.end) { return false; } // this can be the case for example in multi drawing selections
                        const fullPara = _.last(firstOperation.end) === (Position.getParagraphLength(rootNode, _.first(firstOperation.end, (paraIndex + 1))) - 1);
                        if (!fullPara) { return false; }
                    }
                    const paragraphParentPos = (paraIndex) > 0 ? _.first(firstOperation.start, paraIndex) : null;
                    let paraPosCounter = firstOperation.start[paraIndex];

                    let isSimple = true;
                    // all following operations must have the same target and an increasing count at the paragraph position
                    // -> iterating over all operations, special handling for the last operation
                    _.each(allOperations, (op, index) => {

                        if (isSimple) { // do nothing, if 'isSimple is already false

                            index++;

                            const isFirstOp = (index === 1);

                            if (op.name !== DELETE) { isSimple = false; } // no delete operation -> not mergeable
                            if (op.target !== target) { isSimple = false; } // different target -> not mergeable

                            if (!isFirstOp) {

                                const isLastOp = (index === allOperations.length);

                                paraPosCounter++; // increasing the paragraph position exactly by 1
                                const fullParaPos = paragraphParentPos ? _.copy(paragraphParentPos) : [];
                                fullParaPos.push(paraPosCounter); // the full logical position of the following paragraph

                                if (isLastOp) {
                                    // two possibilities: with out and same length as paragraph or an additional text selection with start and end
                                    const startPos = op.start;
                                    const opLastIndex = startPos.length - 1;

                                    if (opLastIndex - paraIndex === 0) {
                                        if (op.end) { isSimple = false; } // an own end position -> not mergeable
                                        if (!_.isEqual(op.start, fullParaPos)) { isSimple = false; } // invalid paragraph position -> not mergeable
                                    } else if (opLastIndex - paraIndex === 1) {
                                        // there is an additional text selection in the last delete operation
                                        if (_.last(op.start) !== 0) { isSimple = false; } // the final operation must start at 0, otherwise it is not mergeable
                                        if (!_.isEqual(_.initial(op.start), fullParaPos)) { isSimple = false; } // invalid start position -> not mergeable
                                        if (!_.isEqual(_.initial(op.end), fullParaPos)) { isSimple = false; } // invalid end position -> not mergeable
                                    } else {
                                        isSimple = false; // position of last operation does not fit -> not mergeable
                                    }

                                } else { // an operation between first and last operation
                                    // in async mode it is possible, that the operation contain the full paragraph content with start and end position
                                    if (op.end) { isSimple = false; } // an own end position -> not mergeable
                                    if (!_.isEqual(op.start, fullParaPos)) { isSimple = false; } // invalid paragraph position -> not mergeable
                                }
                            }
                        }
                    });

                    if (isSimple) { return true; }
                }

                return false;
            };

            if (allOperations.length <= 1) { return false; } // nothing to do

            // analyzing the operations -> a remote client must be able to restore the delete operations
            //                             for each single paragraph

            // the selection is inside the main document, inside one drawing or one table cell
            // but no multi drawing selection in Presentation app.
            // Change tracking is not active and only delete operations are generated
            const firstOperation = _.first(allOperations);
            const lastOperation = _.last(allOperations);

            // checking for a simple selection with only delete operations and removal of full paragraphs/tables between
            // a first and a last operation that can have additional text selection
            const isSimpleSelection = isSimpleOperationRange();

            if (isSimpleSelection) {
                firstOperation.end = lastOperation.end ? _.copy(lastOperation.end) : _.copy(lastOperation.start);
                generator.clearOperations();
                generator.insertOperations(firstOperation); // only one simple delete operation remains (that can be simply restored)
            } else {
                if (!isSimpleSelection) {
                    // trying to merge all mergeable operations inside the list of operations
                    const newOperations = mergeDeleteOperations();
                    if (newOperations) {
                        generator.clearOperations();
                        generator.insertOperations(newOperations); // saving the new generated operations in the generator
                    }
                }
            }

        }

        /**
         * Optimizing the operations at the border of the range of splitted operations splitsize.
         * There might be the following generated delete operations:
         * delete start: [116]
         * delete start: [117,0] end: [117,45]
         * delete start: [118]
         *
         * In that case the second operation can be changed to
         *
         * delete start: [117]
         *
         * if it is at the split size position.
         */
        #improveAsyncDeleteOperations(generator, splitsize) {

            const allOperations = generator.getOperations();
            let multi = 1;

            let opNumber = multi * splitsize;
            let currentOp = allOperations[opNumber];

            const isFullParaDeleteOperation = op => {
                return op.name === DELETE && !op.end && op.start && op.start.length === 1;
            };

            while (currentOp) {
                // this can be the selection of a full top level paragraph
                if (currentOp.start && currentOp.end && currentOp.start.length === 2 && currentOp.end.length === 2) {
                    const target = currentOp.target;
                    const rootNode = this.getRootNode(target);

                    // check, if the full paragraph is selected
                    if ((_.last(currentOp.start) === 0) && (_.last(currentOp.end) === Position.getParagraphLength(rootNode, _.initial(currentOp.end)) - 1)) {
                        // checking the neighbour operations
                        const prevOp = allOperations[opNumber - 1];
                        const nextOp = allOperations[opNumber + 1];
                        if (prevOp && nextOp && isFullParaDeleteOperation(prevOp) && isFullParaDeleteOperation(nextOp)) {
                            const paraPos = _.first(currentOp.start);
                            if ((prevOp.start[0] === (paraPos - 1)) && (nextOp.start[0] === (paraPos + 1))) {
                                currentOp.start = [paraPos]; // simplifying the delete operation
                                delete currentOp.end;
                            }
                        }
                    }
                }

                multi++;
                opNumber = multi * splitsize;
                currentOp = allOperations[opNumber];
            }
        }

        /**
         * In Word the explicit attributes at empty spans are removed, when the empty span is
         * - the last span in the paragraph behind a drawing
         * - the first span in the paragraph before a drawing
         * - positioned between two drawings
         *
         * Info: This function can be activated, if the filter supports this handling, too
         *       -> 8.0.1
         *
         * @param {Node} node
         *  The node, whose explicit attributes might be removed implicitely.
         */
        // #handleExplicitCharacterAttributes(node) {
        //     if (DOM.isEmptySpan(node)) {
        //         const explicitAttrs = getExplicitAttributeSet(node);
        //         if (explicitAttrs.character) {
        //             // this explicit attributes must be removed:
        //             // - if the empty span is the last span in the paragraph behind a drawing
        //             // - if the empty span is the first span in the paragraph before a drawing
        //             // - if the empty span is positioned between two drawings
        //             if ((!node.previousSibling && DOM.isDrawingFrame(node.nextSibling)) ||
        //                 (!node.nextSibling && DOM.isDrawingFrame(node.previousSibling)) ||
        //                 (DOM.isDrawingFrame(node.previousSibling) && DOM.isDrawingFrame(node.nextSibling))) {
        //                 // removing the character attributes at the empty span implicitely (DOCS-4178)
        //                 const newAttrs = { character: {} };
        //                 for (const key in explicitAttrs.character) { newAttrs.character[key] = null; }
        //                 this.characterStyles.setElementAttributes(node, newAttrs);
        //             }
        //         }
        //     }
        // }

        /**
         * Deletes a table at the specified position.
         *
         * @param {Number[]} position
         *  The logical position of the table to be deleted.
         *
         * @param {String} target
         *  Id of the container node, where operation is applied.
         *
         * @returns {Boolean}
         *  TRUE if the function has been processed successfully
         *  otherwise FALSE.
         */
        #implDeleteTable(position, target) {

            // the target node
            const rootNode = this.getRootNode(target);
            // the logical position of the table
            const tablePosition = Position.getLastPositionFromPositionByNodeName(rootNode, position, DOM.TABLE_NODE_SELECTOR);
            // the table node
            const tableNode = Position.getTableElement(rootNode, tablePosition);
            // whether there are list paragraphs inside the table
            let containListStyleParagraph = false;
            // the list style IDs in the table
            let allListStyleIDs = null;
            // the new cursor position
            let newPosition = null;

            if (tableNode) {
                containListStyleParagraph = this.#elementsContainListStyleParagraph(tableNode); // needs to be checked before removing the table
                if (containListStyleParagraph) { allListStyleIDs = this.#getListStyleIDsOfParagraphs(tableNode); }
                // checking for an implicit paragraph behind the table (that can be removed) or a shrinked paragraph behind the table
                if (tableNode.nextSibling) {
                    if (DOM.isImplicitParagraphNode(tableNode.nextSibling)) {
                        if (tableNode.previousSibling && DOM.isParagraphNode(tableNode.previousSibling)) {
                            $(tableNode.nextSibling).remove(); // removing an implicit paragraph behind the table, if this is not the last element
                            newPosition = Position.decreaseLastIndex(tablePosition);
                        }
                    } else if (DOM.isIncreasableParagraph(tableNode.nextSibling)) {
                        $(tableNode.nextSibling).css('height', ''); // next is empty, but shrinked paragraph node
                    }
                }

                $(tableNode).remove();
                if (!newPosition) { newPosition = tablePosition; }
                this.setLastOperationEnd(this.#getValidTextPosition(newPosition));
            } else {
                globalLogger.warn('Editor.implDeleteTable(): not tableNode found ' + JSON.stringify(position));
                return false;
            }

            // the deleted paragraphs can be part of a list, update all lists
            if (containListStyleParagraph) {
                this.updateListsDebounced(null, { useSelectedListStyleIDs: true, listStyleId: allListStyleIDs ? _.uniq(allListStyleIDs) : null, listLevel: null });
            }

            return true;
        }

        /**
         * Deletes table row(s) at the specified position.
         *
         * @param {Number[]} pos
         *  The logical position of the table.
         *
         * @param {Number} startRow
         *  The index of the first row to be deleted.
         *
         * @param {Number} endRow
         *  The index of the last row to be deleted.
         *
         * @param {String} target
         *  The id of the container node, where operation is applied.
         *
         * @returns {Boolean}
         *  Whether the rows could be deleted.
         */
        #implDeleteRows(pos, startRow, endRow, target) {

            // the logical position
            let localPosition = _.copy(pos, true);
            // the root node specified by the target string
            const rootNode = this.getRootNode(target);
            // whether there are list paragraphs inside the deleted row(s)
            let containListStyleParagraph = false;
            // the list style IDs in the deleted row(s)
            let allListStyleIDs = null;
            // the table node
            let table = null;
            // the row nodes inside the table
            let rowNodes = null;

            if (!Position.isPositionInTable(rootNode, localPosition)) {
                globalLogger.warn('Editor.implDeleteRows(): position not in table ' + JSON.stringify(pos));
                return false;
            }

            table = Position.getDOMPosition(rootNode, localPosition, true).node;

            if (isTableDrawingFrame(table)) {
                // the table is a drawing, but the table node is required
                table = $(table).find('table');
            }

            rowNodes = DOM.getTableRows(table).slice(startRow, endRow + 1);

            containListStyleParagraph = this.#elementsContainListStyleParagraph(rowNodes); // needs to be checked before removing the row(s)
            if (containListStyleParagraph) { allListStyleIDs = this.#getListStyleIDsOfParagraphs(rowNodes); }
            rowNodes.remove();

            if (this.isGUITriggeredOperation()) {  // called from 'deleteRows' -> temporary branding for table
                $(table).data('gui', 'remove');
                // Performance: Simple tables do not need new formatting.
                // Tables with styles need to be updated below the removed row. Also the
                // row above need to be updated, because it may have become the final row.

                // Bugfix 44230: introduced a check to prevent wrong values for '.data('reducedTableFormatting')' in updateRowFormatting(), when inserting/deleting
                // rows very quickly via button, so that more than one rows are inserted/deleted before the debounced updateRowFormatting() is executed
                if (_.isNumber($(table).data('reducedTableFormatting'))) {
                    $(table).data('reducedTableFormatting', Math.min((startRow > 0 ? startRow - 1 : 0), $(table).data('reducedTableFormatting')));
                } else {
                    $(table).data('reducedTableFormatting', startRow > 0 ? startRow - 1 : 0);
                }
            }

            // undo also needs table refresh for right border in Firefox (32374)
            if (this.isUndoRedoRunning()  && _.browser.Firefox) { $(table).data('undoRedoRunning', true); }

            // Setting cursor
            const lastRow = getRowCount(table) - 1;

            if (lastRow >= 0) {
                if (endRow > lastRow) {
                    endRow = lastRow;
                }
                localPosition.push(endRow);
                localPosition.push(0);  // using first cell in row
                localPosition.push(0);  // using first paragraph or table
                localPosition = Position.getFirstPositionInParagraph(rootNode, localPosition);

                // recalculating the attributes of the table cells
                if (this.requiresElementFormattingUpdate()) {
                    this.implTableChanged(table);
                }

                this.setLastOperationEnd(localPosition);
            }

            // the deleted paragraphs can be part of a list, update all lists
            if (containListStyleParagraph) {
                this.updateListsDebounced(null, { useSelectedListStyleIDs: true, listStyleId: allListStyleIDs ? _.uniq(allListStyleIDs) : null, listLevel: null });
            }

            return true;
        }

        /**
         * Delete table cells at the specified position.
         *
         * @param {Array<Number>} pos
         *  The logical position of the table to be deleted.
         *
         * @param {Array<Number>} start
         *  The logical start position of the cell range.
         *
         * @param {Array<Number>} end
         *  The logical end position of the cell range.
         *
         * @param {String} target
         *  Id of the container node, where operation is applied.
         *
         * @returns {Boolean}
         *  TRUE if the function has been processed successfully,
         *  otherwise FALSE.
         */
        #implDeleteCells(pos, start, end, target) {

            const localPosition = _.copy(pos, true);
            let tableRowDomPos = null;
            let row = null;
            let table = null;
            let maxCell = 0;
            let cellNodes = null;
            const allCellNodes = [];
            const rootNode = this.getRootNode(target);
            // whether there are list paragraphs inside the deleted cell(s)
            let containListStyleParagraph = false;
            // the list style IDs in the deleted row(s)
            let allListStyleIDs = null;

            if (!Position.isPositionInTable(rootNode, localPosition)) {
                globalLogger.warn('Editor.implDeleteCells(): position not in table ' + JSON.stringify(pos));
                return false;
            }

            tableRowDomPos = Position.getDOMPosition(rootNode, localPosition);

            if (tableRowDomPos) {
                row = tableRowDomPos.node;
            }

            if (row) {

                maxCell = $(row).children().length - 1;

                if (start <= maxCell) {

                    if (end > maxCell) {
                        cellNodes = $(row).children().slice(start);

                    } else {
                        cellNodes = $(row).children().slice(start, end + 1);
                    }

                    if (this.#elementsContainListStyleParagraph(cellNodes)) {
                        containListStyleParagraph = true;
                        allListStyleIDs = allListStyleIDs || [];
                        allListStyleIDs = allListStyleIDs.concat(this.#getListStyleIDsOfParagraphs(cellNodes));
                    }

                    cellNodes.remove(); // removing all following cells
                    allCellNodes.push(cellNodes);
                }
            }

            // setting cursor position
            localPosition.push(0);
            localPosition.push(0);
            localPosition.push(0);

            if (row && this.requiresElementFormattingUpdate()) {
                table = $(row).closest('table');
                // undo of insertColumn also needs table refresh for right border in Firefox
                if (this.isUndoRedoRunning() && _.browser.Firefox) { table.data('undoRedoRunning', true); }
                this.implTableChanged(table);
            }

            this.setLastOperationEnd(localPosition);

            // the deleted paragraphs can be part of a list, update all lists
            if (containListStyleParagraph) {
                this.updateListsDebounced(null, { useSelectedListStyleIDs: true, listStyleId: allListStyleIDs ? _.uniq(allListStyleIDs) : null, listLevel: null });
            }

            return true;
        }

        /**
         * Deletes table column(s) at the specified position.
         *
         * @param {Number[]} start
         *  The logical position of the table.
         *
         * @param {Number} startGrid
         *  The index of the first column to be deleted.
         *
         * @param {Number} endGrid
         *  The index of the last column to be deleted.
         *
         * @param {String} target
         *  The id of the container node, where operation is applied.
         */
        #implDeleteColumns(start, startGrid, endGrid, target) {

            const localPosition = _.copy(start, true);
            let cellNodes = null;
            const allCellNodes = [];
            const rootNode = this.getRootNode(target);
            // whether there are list paragraphs inside the deleted cell(s)
            let containListStyleParagraph = false;
            // the list style IDs in the deleted row(s)
            let allListStyleIDs = null;
            // whether the document is loaded completely
            const documentLoaded = this.isImportFinished();
            // the table node
            let table = null;
            // the rows inside the table node
            let allRows = null;
            let endColInFirstRow = -1;
            let lastColInFirstRow = 0;

            if (!Position.isPositionInTable(rootNode, localPosition)) {
                return;
            }

            table = Position.getDOMPosition(rootNode, localPosition, true).node;

            if (isTableDrawingFrame(table)) {
                // using the table node instead of the drawing of type 'table'
                table = $(table).find('table');
            }

            allRows = DOM.getTableRows(table);

            if (this.isGUITriggeredOperation()) {  // called from 'deleteColumns' -> temporary branding for table
                $(table).data('gui', 'remove');
            }

            allRows.get().forEach(

                // In OX Text the cells are simply removed. Horizontally merged cells are not handled. Therefore the
                // length of each row might be different. In OX Presentation, merged cells are reduced in length, so
                // that every row has the same length.

                (row, i) => {

                    const startCol = getCellPositionFromGridPosition(row, startGrid, false, documentLoaded);
                    const endCol = getCellPositionFromGridPosition(row, endGrid, false, documentLoaded);

                    if ((i === 0) && (endCol !== -1)) {
                        endColInFirstRow = endCol;
                    }

                    if (startCol !== -1) {  // do nothing if startCol is out of range for this row

                        if (endCol === -1) {
                            // checking whether undo of operations is possible and remove cells
                            cellNodes = $(row).children().slice(startCol);
                        } else {
                            // checking whether undo of operations is possible and remove all cells in the range
                            cellNodes = $(row).children().slice(startCol, endCol + 1);
                        }

                        if (this.#elementsContainListStyleParagraph(cellNodes)) {
                            containListStyleParagraph = true;
                            allListStyleIDs = allListStyleIDs || [];
                            allListStyleIDs = allListStyleIDs.concat(this.#getListStyleIDsOfParagraphs(cellNodes));
                        }

                        if (this.useSlideMode()) {

                            // OX Presentation style: Reducing width of horizontally merged cells, if not covered completely

                            // -> reverting the cellNodes order, so that deletion does not influence following calculations of grid position
                            cellNodes = $(cellNodes.get().reverse());

                            _.each(cellNodes, cell => {

                                let removeCell = true;

                                // is this a horizontally merged cell?
                                // -> remove cell or reduce in length

                                if (isHorizontallyMergedCell(cell)) {
                                    // is it completely covered by the specified grid range?
                                    const gridPos = getGridColumnRangeOfCell(cell);

                                    if (gridPos.start < startGrid || gridPos.end > endGrid) {

                                        removeCell = false;  // not removing this cell, but reduce it in size
                                        let cellLength = gridPos.end - gridPos.start + 1; // the old length of the table cell

                                        if (gridPos.start <= startGrid && gridPos.end >= endGrid) {
                                            cellLength -= (endGrid - startGrid + 1);
                                        } else {
                                            if (endGrid > gridPos.end) {
                                                cellLength -= (gridPos.end - startGrid + 1);
                                            } else if (startGrid < gridPos.start) {
                                                cellLength -= (endGrid - gridPos.start + 1);
                                            }
                                        }

                                        this.tableCellStyles.setElementAttributes(cell, { cell: { gridSpan: cellLength } });
                                    }
                                }

                                if (removeCell) {
                                    this.#checkDisableUndoStack('cell', cell);
                                    this.updateCollectionModels(cell); // updating all models, for drawings, comments, range markers, ...
                                    $(cell).remove(); // removing cell node
                                }
                            });

                        } else {

                            // OX Text style: Deleting merged cells completely

                            _.each(cellNodes, cell => {
                                this.#checkDisableUndoStack('cell', cell);
                                // updating all models, for drawings, comments, range markers, ...
                                this.updateCollectionModels(cell);
                            }).remove();  // removing cell nodes

                            allCellNodes.push(cellNodes); // collecting all cells?
                        }
                    }
                }
            );

            // Setting cursor
            lastColInFirstRow = DOM.getTableRows(table).first().children().length - 1;

            if ((endColInFirstRow > lastColInFirstRow) || (endColInFirstRow === -1)) {
                endColInFirstRow = lastColInFirstRow;
            }
            localPosition.push(0);
            localPosition.push(endColInFirstRow);
            localPosition.push(0);
            localPosition.push(0);

            // delete undo stack immediately if this is necessary and not a part of an undo group
            if (!this.isInUndoGroup() && this.deleteUndoStack()) {
                this.getUndoManager().clearUndoActions();
                this.setDeleteUndoStack(false);
            }

            // recalculating the attributes of the table cells
            if (this.requiresElementFormattingUpdate()) {
                this.implTableChanged(table);
            }

            // the deleted paragraphs can be part of a list, update all lists
            if (containListStyleParagraph) {
                this.updateListsDebounced(null, { useSelectedListStyleIDs: true, listStyleId: allListStyleIDs ? _.uniq(allListStyleIDs) : null, listLevel: null });
            }

            this.setLastOperationEnd(localPosition);
        }

        /**
         * Deletes text at the specified position.
         *
         * @param {Number[]} startPosition
         *  The logical start position.
         *
         * @param {Number[]} endPosition
         *  The logical end position.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.allowEmptyResult=false]
         *      In internal calls (from other operations except delete), it can be allowed,
         *      that there is nothing to delete.
         *  @param {Boolean} [options.keepDrawingLayer=false]
         *      Whether the drawings in the drawing layer or comments in the comment layer
         *      shall not be removed -> this is necessary after splitting a paragraph.
         *
         * @param {String} target
         *  The id of the container node, where operation is applied.
         *
         * @returns {Boolean}
         *  Whether the text was deleted.
         */
        #implDeleteText(startPosition, endPosition, options, target) {

            // info about the parent paragraph node
            let position = null, paragraph = null;
            // last index in start and end position
            let startOffset = 0, endOffset = 0;
            // next sibling text span of last visited child node
            let nextTextSpan = null;
            // a counter for the removed elements
            let removeCounter = 0;
            // the number of text positions to be removed
            let removePositions = 0;
            // in internal calls (from other operations except delete), it can be allowed,
            // that there is nothing to delete
            const allowEmptyResult = getBooleanOption(options, 'allowEmptyResult', false);
            // whether the drawings in the drawing layer or comments in the comment layer shall not be removed
            // -> this is necessary after splitting a paragraph
            const keepDrawingLayer = getBooleanOption(options, 'keepDrawingLayer', false);
            // root node used for calculating position - if target is present - root node is header or footer
            const rootNode = this.getRootNode(target);
            // whether a check of the paragraph index is required
            let checkParagraphIndex = false;
            // a helper position
            let lastOperationEndLocal = null;

            // get paragraph node from start position
            if (!_.isArray(startPosition) || (startPosition.length < 2)) {
                globalLogger.warn('Editor.#implDeleteText(): missing start position');
                return false;
            }
            position = startPosition.slice(0, -1);
            paragraph = (this.useParagraphCache() && this.getParagraphCache()) || Position.getParagraphElement(rootNode, position);
            if (!paragraph) {
                globalLogger.warn('Editor.#implDeleteText(): no paragraph found at position ' + JSON.stringify(position));
                return false;
            }

            // validate end position
            if (_.isArray(endPosition) && !Position.hasSameParentComponent(startPosition, endPosition)) {
                globalLogger.warn('Editor.#implDeleteText(): range not in same paragraph');
                return false;
            }

            // start and end offset in paragraph
            startOffset = _.last(startPosition);
            endOffset = _.isArray(endPosition) ? _.last(endPosition) : startOffset;
            if (endOffset === -1) { endOffset = undefined; }
            if (endOffset !== undefined) {
                removePositions = endOffset - startOffset + 1;
            }

            // visit all covered child nodes (iterator allows to remove the visited node)
            Position.iterateParagraphChildNodes(paragraph, (node, _nodeStart, nodeLength) => {

                // previous text span of current node
                let prevTextSpan = null;
                // the field manager object
                const fieldManager = this.getFieldManager();
                // the drawing layer object
                const drawingLayer = this.getDrawingLayer();
                // the character attributes at empty spans need to be saved sometimes
                let emptySpanAttrs = null;

                // remove preceding position offset node of floating drawing objects
                if (DOM.isFloatingDrawingNode(node)) {
                    $(node).prev(DOM.OFFSET_NODE_SELECTOR).remove();
                }

                // get sibling text spans
                prevTextSpan = DOM.isTextSpan(node.previousSibling) ? node.previousSibling : null;
                nextTextSpan = DOM.isTextSpan(node.nextSibling) ? node.nextSibling : null;

                if ((!prevTextSpan) && (DOM.isTextSpanWithoutTextNode(node.previousSibling))) { prevTextSpan = node.previousSibling; }
                if ((!nextTextSpan) && (DOM.isTextSpanWithoutTextNode(node.nextSibling))) { nextTextSpan = node.nextSibling; }

                // clear text in text spans
                if (DOM.isTextSpan(node)) {

                    // only remove the text span, if it has a sibling text span
                    // (otherwise, it separates other component nodes)
                    if (prevTextSpan || nextTextSpan) {
                        $(node).remove();
                    } else {
                        // remove text, but keep text span element
                        node.firstChild.nodeValue = '';
                        // but remove change track information immediately (38791)
                        if (DOM.isChangeTrackNode(node)) { this.getChangeTrack().updateChangeTrackAttributes($(node), {}); }
                        // removing hard character attributes from empty spans located next to drawings (DOCS-4178)
                        // -> this requires changes in the filter, too -> 8.0.1
                        // if (this.#docApp.isTextApp() && !this.#docApp.isODF()) { this.#handleExplicitCharacterAttributes(node); }
                    }
                    removeCounter += nodeLength;
                    return;
                }

                // other component nodes (drawings or text components)
                if (DOM.isTextComponentNode(node) || isDrawingFrame(node) || (this.#docApp.isTextApp() && DOM.isCommentPlaceHolderNode(node)) || DOM.isRangeMarkerNode(node)) {

                    // if we are removing manualy inserted page break by user, trigger the pagebreak recalculation on document
                    if (DOM.isTextComponentNode(node) && $(node.parentNode).hasClass('manual-page-break')) {
                        $(node.parentNode).removeClass('manual-page-break');
                        this.insertPageBreaks(node.parentNode, getClosestTextFrameDrawingNode(node.parentNode));
                    }

                    // node is the image place holder -> remove also the drawing from the drawing layer
                    if (DOM.isDrawingPlaceHolderNode(node)) {
                        removeDrawingSelection(DOM.getDrawingPlaceHolderNode(node));
                        drawingLayer.removeFromDrawingLayer(node, { keepDrawingLayer });
                    } else if (DOM.isAbsoluteParagraphDrawing(node)) {
                        if ($(node).data('textWrapMode') === 'none') { checkParagraphIndex = true; }
                        drawingLayer.removeSpaceMakerNode(node);
                        drawingLayer.removeDrawingFromModelContainer(node);
                        removeDrawingSelection(node);
                        this.updateCollectionModels(node); // checking, if the drawing contains other nodes that are collected in a model
                    } else if (this.#docApp.isTextApp() && DOM.isCommentPlaceHolderNode(node)) {
                        this.getCommentLayer().removeFromCommentLayer(node, { keepDrawingLayer });
                    } else if (DOM.isRangeMarkerNode(node) && !keepDrawingLayer) {
                        this.getRangeMarker().removeRangeMarker(node);
                    } else if (DOM.isComplexFieldNode(node) && !keepDrawingLayer) {
                        fieldManager.removeFromComplexFieldCollection(node);
                        // it is important to clean up class names when deleting cx field
                        fieldManager.cleanUpClassWhenDeleteCx(node);
                    } else if (DOM.isFieldNode(node)) {
                        fieldManager.removeSimpleFieldFromCollection(node);
                    }

                    // safely release image data (see comments in BaseApplication.destroyImageNodes())
                    this.#docApp.destroyImageNodes(node);

                    // remove the visited node
                    $(node).remove();

                    // remove previous empty sibling text span (not next sibling which would break iteration)
                    if (DOM.isEmptySpan(prevTextSpan) && nextTextSpan) {
                        if (DOM.isEmptySpan(nextTextSpan) && DOM.isBookmarkNode(node)) { // saving the attributes, if the following span is also empty (54497)
                            emptySpanAttrs = getExplicitAttributeSet(prevTextSpan);
                            if (!_.isEmpty(emptySpanAttrs)) { this.characterStyles.setElementAttributes(nextTextSpan, emptySpanAttrs); }
                        }
                        $(prevTextSpan).remove();
                    }
                    removeCounter += nodeLength;
                    return;
                }

                globalLogger.error('Editor.#implDeleteText(): unknown paragraph child node');
                return BREAK;

            }, undefined, { start: startOffset, end: endOffset, split: true });

            // removed must be > 0 and it must be the difference from endOffset - startOffset + 1
            // check, if at least one element was removed
            if (!allowEmptyResult && (removeCounter === 0)) {
                globalLogger.warn('Editor.#implDeleteText(): no component found from position: ' + JSON.stringify(startPosition) + ' to ' + JSON.stringify(endPosition));
                return false;
            }

            // check, if the removed part has the correct length
            if (!allowEmptyResult && (removePositions > 0) && (removePositions !== removeCounter)) {
                globalLogger.warn('Editor.#implDeleteText(): incorrect number of removals: Required: ' + removePositions + ' Done: ' + removeCounter);
                return false;
            }

            // remove next sibling text span after deleted range, if empty,
            // otherwise try to merge with equally formatted preceding text span
            if (nextTextSpan && DOM.isTextSpan(nextTextSpan.previousSibling)) {
                if (DOM.isEmptySpan(nextTextSpan)) {
                    $(nextTextSpan).remove();
                } else {
                    mergeSiblingTextSpans(nextTextSpan);
                }
            }

            if (!DOM.isSlideNode(paragraph)) {  // -> no update of full slide if one drawing on the slide is deleted
                // validate paragraph node, store operation position for cursor position
                if (this.isGUITriggeredOperation()) {
                    if (this.#deleteOrBackSpaceKeyPressed && !IOS_SAFARI_DEVICE) { // exception for Safari on touch device for DOCS-3957
                        this.implParagraphChangedSync($(paragraph)); // avoid cursor jumping when deleting content (DOCS-3074). Check:Task 30597: Local client can defer attribute setting,
                    } else {
                        this.implParagraphChanged(paragraph);
                    }
                } else if (this.isUndoRedoRunning() || !this.isImportFinished()) { // Task 30603: deferring paragraph formatting when loading document
                    this.implParagraphChanged(paragraph);
                } else { // external operations
                    if (this.#docApp.isOTEnabled()) {
                        this.implParagraphChanged($(paragraph)); // OT: Avoiding to lose the focus permanently
                    } else {
                        this.implParagraphChangedSync($(paragraph));  // Performance and task 30597: Remote client must set attributes synchronously
                    }
                }
            }

            lastOperationEndLocal = _.clone(startPosition);

            // Updating the paragraph index after removing at least one drawing
            if (checkParagraphIndex && paragraph && this.#docApp.isTextApp() && this.isIncreasedDocContent()) { handleParagraphIndex(paragraph); }

            this.setLastOperationEnd(lastOperationEndLocal);

            return true;
        }

        /**
         * Removes a specified element or a text range. The type of the element
         * will be determined from the parameters start and end.
         * specified range. Currently, only single components can be deleted,
         * except for text ranges in a paragraph. A text range can include
         * characters, fields, and drawing objects, but must be contained in a
         * single paragraph.
         *
         * @param {Number[]} start
         *  The logical start position of the element or text range to be
         *  deleted.
         *
         * @param {Number[]} [end]
         *  The logical end position of the element or text range to be
         *  deleted. Can be omitted, if the end position is equal to the start
         *  position (single component)
         *
         * @param {String} target - optional
         *  If target is existing, rootNode is not editdiv, but currently edited header/footer
         *
         *  @returns {Boolean}
         *   TRUE if the function has been processed successfully, otherwise
         *   FALSE.
         */
        #implDelete(start, end, target) {

            // node info for start/end position
            let startInfo = null, endInfo = null;
            // position description for cells
            let rowPosition, startCell, endCell;
            // position description for rows
            let tablePosition, startRow, endRow;
            // a new implicit paragraph node
            let newParagraph = null;
            // a temporary helper position
            let localPosition = null;
            // result of function (default true)
            let result = true;
            // starting point for inserting page breaks downwards
            const position = start.length > 1 ? start.slice(0, 1) : start.slice(0);
            // whehter the removed node is a slide node
            let isSlideNode = false;
            // the parent node of the removed node
            let parentNode = null;
            // used for pagebreaks
            let currentElement = null;
            // the height of the paragraph before deleting content
            let prevHeight = null;
            // the space maker node and the paragraph containing the space maker node
            let spaceMakerNode = null, spaceMakerParagraph = null;
            // a top level drawing inside the drawing layer
            let topLevelDrawing = null;
            // whether content in the drawing layer in a text frame was removed
            let isDrawingLayerText = false;
            // a text frame node, that might contain the start position
            let textFrameNode = null;
            // whether a removed char is the last char in its paragraph
            let isLastCharInPar = false;
            // container element with target id
            const rootNode = this.getRootNode(target);

            // resolve start and end position
            if (!_.isArray(start)) {
                globalLogger.warn('Editor.#implDelete(): missing start position');
                return false;
            }
            startInfo = Position.getDOMPosition(rootNode, start, true);
            if (!startInfo || !startInfo.node) {
                globalLogger.warn('Editor.#implDelete(): invalid start position: ' + JSON.stringify(start));
                return false;
            }
            endInfo = _.isArray(end) ? Position.getDOMPosition(rootNode, end, true) : startInfo;
            if (!endInfo || !endInfo.node) {
                globalLogger.warn('Editor.#implDelete(): invalid end position: ' + JSON.stringify(end));
                return false;
            }

            end = end || start;

            // get attribute type of start and end node
            startInfo.type = this.#resolveElementType(startInfo.node);
            endInfo.type = this.#resolveElementType(endInfo.node);

            // check that start and end point to the same element type
            if ((!startInfo.type || !endInfo.type) || (startInfo.type !== endInfo.type)) {
                globalLogger.warn('Editor.#implDelete(): problem with node types: ' + startInfo.type + ' and ' + endInfo.type);
                return false;
            }

            // check that start and end point to the same element for non text types (only one cell, row, paragraph, ...)
            if ((startInfo.type !== 'text') && (startInfo.node !== endInfo.node)) {
                globalLogger.warn('Editor.#implDelete(): no ranges supported for node type "' + startInfo.type + '"');
                return false;
            }

            // check that for text nodes start and end point have the same parent
            if ((startInfo.type === 'text') && (startInfo.node.parentNode !== endInfo.node.parentNode)) {
                if (DOM.isDrawingLayerNode(endInfo.node.parentNode)) {
                    if (startInfo.node.parentNode !== DOM.getDrawingPlaceHolderNode(endInfo.node).parentNode) {
                        globalLogger.warn('Editor.#implDelete(): deleting range only supported inside one paragraph.');
                        return false;
                    }
                } else if (DOM.isDrawingLayerNode(startInfo.node.parentNode)) {
                    if (endInfo.node.parentNode !== DOM.getDrawingPlaceHolderNode(startInfo.node).parentNode) {
                        globalLogger.warn('Editor.#implDelete(): deleting range only supported inside one paragraph.');
                        return false;
                    }
                } else {
                    globalLogger.warn('Editor.#implDelete(): deleting range only supported inside one paragraph.');
                    return false;
                }
            }

            // checking whether undo of operations is possible (but not in the loading phase)
            if (this.isImportFinished()) { this.#checkDisableUndoStack(startInfo.type, startInfo.node, _.clone(start), _.clone(end), target); }

            // check for a text frame node
            textFrameNode = getClosestTextFrameDrawingNode(startInfo.node.parentNode);

            let nextNodeNeighbour;
            let prevNodeNeighbour;

            // deleting the different types:
            switch (startInfo.type) {

                case 'text':

                    isLastCharInPar = !startInfo.node.nextSibling && (startInfo.offset === startInfo.node.textContent.length - 1);
                    // setting the paragraph node (required for page breaks)
                    currentElement = startInfo.node.parentNode;
                    if (DOM.isDrawingLayerNode(currentElement)) { currentElement = DOM.getDrawingPlaceHolderNode(startInfo.node).parentNode; }
                    if (!$(currentElement.parentNode).hasClass('pagecontent') && !DOM.isSlideNode(currentElement)) {
                        if (isTextFrameNode(currentElement.parentNode)) {
                            // is the text frame inside a drawing in the drawing layer?
                            if (DOM.isInsideDrawingLayerNode(currentElement)) {
                                isDrawingLayerText = true;
                                // measuring height changes at the current paragraph (that is already currentElement
                                // But for the top level measurement the paragraph containing the space maker node is required
                                topLevelDrawing = DOM.getTopLevelDrawingInDrawingLayerNode(currentElement);
                                spaceMakerNode = DOM.getDrawingSpaceMakerNode(topLevelDrawing);
                                if (DOM.getDrawingSpaceMakerNode(topLevelDrawing)) { spaceMakerParagraph = $(spaceMakerNode).parentsUntil(this.getNode(), DOM.PARAGRAPH_NODE_SELECTOR).last()[0]; }
                                // div.textframe height needs to be measured synchronously
                                currentElement = currentElement.parentNode;
                            } else if (this.#docApp.isTextApp()) {
                                currentElement = $(currentElement).parentsUntil(this.getNode(), DOM.PARAGRAPH_NODE_SELECTOR).last()[0];  // its a div.p inside paragraph
                            }
                        } else if (this.#docApp.isTextApp() && target) { // header/footer handling
                            if (!DOM.isMarginalContentNode(currentElement.parentNode)) {
                                currentElement = $(currentElement).parents(DOM.TABLE_NODE_SELECTOR).last()[0]; //its a div.p inside table(s)
                            }
                            // TODO parent DrawingFrame
                        } else if (this.#docApp.isTextApp()) {
                            currentElement = $(currentElement).parentsUntil(this.getNode(), DOM.TABLE_NODE_SELECTOR).last()[0]; //its a div.p inside table(s)
                        }
                    }
                    // caching paragraph height before removing text
                    prevHeight = currentElement ? currentElement.offsetHeight : 0;

                    // removing text
                    result = this.#implDeleteText(start, end, {}, target);

                    if (this.#docApp.isTextApp()) {
                        // block page breaks render if operation is targeted
                        if (target) { this.setBlockOnInsertPageBreaks(true); }

                        // using the same paragraph node again
                        if (currentElement && (!isDrawingLayerText) && ($(currentElement).data('lineBreaksData'))) {
                            $(currentElement).removeData('lineBreaksData'); // we changed paragraph layout, cached line breaks data needs to be invalidated
                        }

                        this.setQuitFromPageBreaks(true); // quit from any currently running pagebreak calculus - performance optimization
                    }

                    // trigger repainting only if height changed or element is on two pages,
                    // or paragraph is on more pages, and last char in paragraph is not deleted (performance improvement)
                    if (currentElement && ((prevHeight !== currentElement.offsetHeight) || ($(currentElement).hasClass('contains-pagebreak') && !isLastCharInPar))) {
                        this.insertPageBreaks(isDrawingLayerText ? spaceMakerParagraph : currentElement, textFrameNode);
                        if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }
                    }

                    break;

                case 'paragraph':
                    if (DOM.isImplicitParagraphNode(startInfo.node)) {
                        globalLogger.warn('Editor.#implDelete(): Error: Operation tries to delete an implicit paragraph!');
                        return false;
                    }
                    // used to check if next neighbour is table, and after deletion, selection has to be updated
                    nextNodeNeighbour = $(startInfo.node).next();

                    // only for text, there are no paragraph borders and color in presentation
                    // used for updating paragraphs that have a color or a border
                    // important: be careful that the resulting node remain the same when moving this line in the function!
                    prevNodeNeighbour = !this.useSlideMode() ? $(startInfo.node).prev() : null;

                    isSlideNode = DOM.isSlideNode(startInfo.node);

                    // Setting lastOperationEnd for the special case, that insertParagraph was used instead of splitParagraph
                    // -> if there is a previous paragraph, use the last position of this previous paragraph (task 30742)
                    if (($(startInfo.node).prev().length > 0) && (DOM.isParagraphNode($(startInfo.node).prev()))) {
                        localPosition = _.clone(start);
                        localPosition[localPosition.length - 1]--;
                        localPosition = Position.getLastPositionInParagraph(rootNode, localPosition);
                        this.setLastOperationEnd(localPosition);
                    } else if ($(startInfo.node).prev().length === 0) {
                        // special case: Removing the first paragraph (undo-redo handling)
                        localPosition = _.clone(start);
                        localPosition = Position.getFirstPositionInParagraph(rootNode, localPosition);
                        this.setLastOperationEnd(localPosition);
                    }

                    if (!isSlideNode && ((DOM.isParagraphWithoutNeighbour(startInfo.node)) || (DOM.isFinalParagraphBehindTable(startInfo.node)))) {
                        newParagraph = DOM.createImplicitParagraphNode();
                        $(startInfo.node).parent().append(newParagraph);
                        this.validateParagraphNode(newParagraph);
                        // if original paragraph was marked as marginal, mark newly created implicit as marginal, too
                        if (DOM.isMarginalNode(startInfo.node)) {
                            $(newParagraph).addClass(DOM.MARGINAL_NODE_CLASSNAME);
                        }

                        this.implParagraphChanged(newParagraph);

                        // handling empty list items in ODF presentations
                        if (this.#docApp.isODF() && this.useSlideMode()) {
                            // in ODP paragraphs, it might be necessary to transfer paragraph attributes to the implicit paragraph
                            const paraAttrs = this.getReplaceImplicitParagraphAttrs ? this.getReplaceImplicitParagraphAttrs(newParagraph) : null;
                            if (paraAttrs) { this.paragraphStyles.setElementAttributes(newParagraph, paraAttrs); }
                            this.implParagraphChangedSync(newParagraph, { restoreSelection: false }); // avoid flickering of paragraph
                            this.handleTriggeringListUpdate(newParagraph);
                        }
                    }
                    this.trigger('delete:paragraph:before', { startNode: (startInfo && startInfo.node) });

                    // checking, if the paragraph contains drawing place holder nodes, comment place holder nodes or range
                    // marker nodes. In this case all models need to be updated. Furthermore the comments and the drawings
                    // in their layers need to be removed, too.
                    this.updateCollectionModels(startInfo.node);

                    // the deleted paragraphs can be part of a list, update all lists (check this before removal (66528)
                    if (!isSlideNode) { this.handleTriggeringListUpdate(startInfo.node); }

                    // removing the existing drawing selections
                    if (isSlideNode) { _.each($(startInfo.node).children('.drawing.selected'), oneDrawing => { removeDrawingSelection(oneDrawing); }); }

                    // removing the slide container of layout and master slides, too
                    if (isSlideNode) { parentNode = startInfo.node.parentNode; }

                    $(startInfo.node).remove(); // remove the paragraph from the DOM
                    if (parentNode && DOM.isSlideContainerNode(parentNode)) { $(parentNode).remove(); }

                    // only for text, there are no paragraph borders and color in presentation
                    if (!this.useSlideMode()) {
                        // only needed for updating paragraphs that have a color or a border
                        this.paragraphStyles.updateParagraphBorderAndColorNodes([nextNodeNeighbour, prevNodeNeighbour]);
                    }

                    if (DOM.isTableNode(nextNodeNeighbour)) { // #34685 properly set cursor inside table after delete paragraph
                        this.getSelection().setTextSelection(Position.getFirstPositionInParagraph(rootNode, start), null, { simpleTextSelection: false, splitOperation: false });
                    }

                    // block page breaks render if operation is targeted
                    if (this.#docApp.isTextApp() && target) { this.setBlockOnInsertPageBreaks(true); }

                    // get currentElement again (maybe whole paragraph has deleted), and pass it to repaint pagebreaks from that page only
                    currentElement = Position.getContentNodeElement(rootNode, position);
                    this.insertPageBreaks(currentElement, textFrameNode);
                    if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

                    break;

                case 'cell':

                    // checking for absolute drawings, comments and range markers
                    this.updateCollectionModels(startInfo.node);

                    rowPosition = _.clone(start);
                    startCell = rowPosition.pop();
                    endCell = startCell;
                    result = this.#implDeleteCells(rowPosition, startCell, endCell, target);
                    break;

                case 'row':

                    // checking for absolute drawings, comments and range markers
                    this.updateCollectionModels(startInfo.node);

                    tablePosition = _.clone(start);
                    startRow = tablePosition.pop();
                    endRow = startRow;
                    result = this.#implDeleteRows(tablePosition, startRow, endRow, target);

                    // block page breaks render if operation is targeted
                    if (this.#docApp.isTextApp() && target) { this.setBlockOnInsertPageBreaks(true); }

                    // get currentElement again (maybe whole paragraph has deleted), and pass it to repaint pagebreaks from that page only
                    currentElement = Position.getContentNodeElement(rootNode, position);
                    this.insertPageBreaks(currentElement, textFrameNode);
                    if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

                    break;

                case 'table':

                    // checking for absolute drawings, comments and range markers
                    this.updateCollectionModels(startInfo.node);

                    result = this.#implDeleteTable(start, target);

                    // block page breaks render if operation is targeted
                    if (this.#docApp.isTextApp() && target) {
                        // trigger header/footer content update on other elements of same type, if change was made inside header/footer
                        this.updateEditingHeaderFooterDebounced(this.getRootNode(target));
                        this.setBlockOnInsertPageBreaks(true);
                    }

                    // get currentElement again (maybe whole paragraph has deleted), and pass it to repaint pagebreaks from that page only
                    currentElement = Position.getContentNodeElement(rootNode, position);
                    this.insertPageBreaks(currentElement, textFrameNode);
                    if (!this.isPageBreakMode()) { this.#docApp.getView().recalculateDocumentMargin(); }

                    break;

                default:
                    globalLogger.error('Editor.#implDelete(): unsupported node type: ' + startInfo.type);
            }

            // delete undo stack immediately if this is necessary and not a part of an undo group
            if (!this.isInUndoGroup() && this.deleteUndoStack()) {
                this.getUndoManager().clearUndoActions();
                this.setDeleteUndoStack(false);
            }

            if (isNoWordWrapDrawingFrame(textFrameNode)) {
                this.insertPageBreaks(currentElement, textFrameNode);
            }

            return result;
        }
    }

    return DeleteOperationMixin;
}

// exports ====================================================================

export const XDeleteOperation = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XDeleteOperation>}
     *  The extended class with delete operation functionality.
     */
    mixin: mixinDeleteOperation
};
