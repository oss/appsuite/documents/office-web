/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { fun } from '@/io.ox/office/tk/algorithms';
import { CHANGE_CONFIG, CHANGE_STYLESHEET, DELETE_LIST, DELETE_STYLESHEET, INSERT_FONT, INSERT_LIST,
    INSERT_STYLESHEET, INSERT_THEME } from '@/io.ox/office/textframework/utils/operations';
import { STORAGE_AVAILABLE } from '@/io.ox/office/tk/config';

function mixinStorageOperation(BaseClass) {

    // mix-in class StorageOperationMixin =====================================

    /**
     * A mix-in class for saving the operations that are needed to load a
     * document from local storage.
     */

    class StorageOperationMixin extends BaseClass {

        // a collector for all those operations that are needed to load a document from the local storage
        #storageOperationCollector = [];
        // Load performance: List if operation names for operations that need to be stored in the local storage, so that the
        // document can be loaded without operations and fast load string.
        // These operations do not modify the DOM, but register data in the global models:
        // document attributes, themes, font descriptions, style sheets and list styles.
        #STORAGE_OPERATIONS = {};
        // whether the collector already contains the unique "changeConfig" operation
        #ccIncluded = false;

        constructor(docApp, ...baseCtorArgs) {

            super(docApp, ...baseCtorArgs);

            // list of operations that need to be handled for local storage support
            fun.do(() => {
                this.#STORAGE_OPERATIONS[CHANGE_CONFIG] = { isCC: true };
                this.#STORAGE_OPERATIONS[INSERT_THEME] = { insert: true };
                this.#STORAGE_OPERATIONS[INSERT_FONT] = { insert: true };
                this.#STORAGE_OPERATIONS[INSERT_STYLESHEET] = { insert: true };
                this.#STORAGE_OPERATIONS[DELETE_STYLESHEET] = { delete: INSERT_STYLESHEET, properties: ['styleId', 'type'] };
                this.#STORAGE_OPERATIONS[CHANGE_STYLESHEET] = { isCss: true, change: INSERT_STYLESHEET, properties: ['styleId', 'type'] };
                this.#STORAGE_OPERATIONS[INSERT_LIST] = { insert: true };
                this.#STORAGE_OPERATIONS[DELETE_LIST] = { delete: INSERT_LIST, properties: ['listStyleId'] };
            });
        }

        // public methods -----------------------------------------------------

        /**
         * Load performance: Getting the selector for selected operations that need to be stored in the local storage.
         *
         * @returns {Object[]}
         *  A container with all operations that need to be stored in the local storage.
         */
        getStorageOperationCollector() {
            return this.#storageOperationCollector;
        }

        /**
         * Load performance: Collecting selected operations in collector for saving them in local storage. This
         * makes it possible to load documents without any operations or fast load string, because all required
         * data is saved in the local storage.
         * The operations need to be saved for every supported loading process. Also if 'Use local storage' is
         * disabled, because it might be enabled by the user later.
         * The operations should not be collected, if the user does not has the ability to use the local storage.
         *
         * @param {Object} operation
         *  An operation object.
         */
        saveStorageOperation(operation) {

            // Leave immediately, if local immediately storage is not supported.
            // Checking:
            // localStorageApp: whether the app supports local storage -> only those apps contain this function.
            // STORAGE_AVAILABLE: whether the system supports local storage (this is checked below)
            // saveFileInLocalStorage: whether the user can use local storage -> might be changed by the user
            //                         during editing the document.
            if (!STORAGE_AVAILABLE || !operation || !operation.name) { return; }

            const storageOp = this.#STORAGE_OPERATIONS[operation.name];
            if (!storageOp) { return; }

            if (storageOp.insert) {
                // insert operations can be added simply to the collector
                this.#storageOperationCollector.push(operation);

            } else if (storageOp.delete) {
                // delete operations must remove an already existing operation
                this.#deleteOperationFromCollector(operation);

            } else if (storageOp.isCC) {
                // handling for "changeConfig" operation, that must stay unique
                if (this.#ccIncluded) {
                    this.#mergeChangeConfigOperation(operation);
                } else {
                    this.#storageOperationCollector.push(operation);
                    this.#ccIncluded = true;
                }

            } else if (storageOp.isCss) {
                // handling for changeStyleSheet operation
                this.#mergeChangeStyleSheetOperation(operation);
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Helper function for the operations deleteList and deleteStyleSheet.
         * These functions need to remove the corresponding insertList or
         * insertStyleSheet operations from the operations collector.
         *
         * @param {Object} operation
         *  An operation object.
         */
        #deleteOperationFromCollector(operation) {

            // insertStyleSheet and deleteStyleSheet have properties 'styleId' and 'type'
            // insertListStyle and deleteListStyle have property 'listStyleId' (only via undo)

            const compareObject = { name: this.#STORAGE_OPERATIONS[operation.name].delete };
            _.each(this.#STORAGE_OPERATIONS[operation.name].properties, prop => {
                compareObject[prop] = operation[prop];
            });

            // searching the corresponding operation to remove it from collector
            const op = _.findWhere(this.#storageOperationCollector, compareObject);

            // removing the one found operation from the collector
            if (op) { this.#storageOperationCollector = _.without(this.#storageOperationCollector, op); }
        }

        /**
         * Helper function to merge "changeConfig" operations.
         *
         * @param {Object} operation
         *  An operation object.
         */
        #mergeChangeConfigOperation(operation) {

            // searching the corresponding operation in the collector
            const op = _.findWhere(this.#storageOperationCollector, operation.name);

            // merging content of new operation into existing operation
            if (op?.attrs && operation.attrs) {
                this.defAttrPool.extendAttrSet(op.attrs, operation.attrs);
            }
        }

        /**
         * Helper function to merge 'changeStyleSheet' operations.
         *
         * @param {Object} operation
         *  An operation object.
         */
        #mergeChangeStyleSheetOperation(operation) {

            const compareObject = { name: this.#STORAGE_OPERATIONS[operation.name].change };
            _.each(this.#STORAGE_OPERATIONS[operation.name].properties, prop => {
                compareObject[prop] = operation[prop];
            });

            // searching the corresponding operation in the collector
            const op = _.findWhere(this.#storageOperationCollector, compareObject);

            // merging content of new operation into existing operation
            if (op && op.attrs && operation.attrs) {
                this.defAttrPool.extendAttrSet(op.attrs, operation.attrs);
            }
        }

    }

    return StorageOperationMixin;
}

// exports ====================================================================

export const XStorageOperation = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XStorageOperation>}
     *  The extended class with storage operation functionality.
     */
    mixin: mixinStorageOperation
};
