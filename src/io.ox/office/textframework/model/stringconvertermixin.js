/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import { DEBUG } from '@/io.ox/office/textframework/utils/config';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { handleMinifiedObject } from '@/io.ox/office/editframework/utils/operationutils';
import { getFileUrl } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { TEXTFRAMECONTENT_NODE_CLASS, clearSelection, getContentNode, getTextFrameNode, isShapeDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { COMMENTLAYER_NODE_SELECTOR, DRAWINGLAYER_NODE_SELECTOR, DRAWINGPLACEHOLDER_LINK, DRAWING_SPACEMAKER_LINK, HEADER_FOOTER_PLACEHOLDER_SELECTOR,
    HIDDENAFTERATTACH_CLASSNAME, INCREASED_CONTENT_CLASS, LAYOUTSLIDELAYER_SELECTOR, MASTERSLIDELAYER_SELECTOR, PAGECONTENT_NODE_SELECTOR, SLIDE_SELECTOR,
    ensureExistingTextNode, ensureExistingTextNodeInField, handleReadOnly, isDrawingFrame, isImageDrawingNode, isEmptySpan, isParagraphNode, isSlideNode, isSpan,
    makeExceededSizeTable } from '@/io.ox/office/textframework/utils/dom';
import { getBooleanOption } from '@/io.ox/office/textframework/utils/textutils';

function mixinStringConverter(BaseClass) {

    // mix-in class StringConverterMixin ======================================

    /**
     * A mix-in class for the converters, that convert the document model in
     * the DOM into a string and vice versa. This is used for example for
     * saving the document in the localstorage or for evaluating the string
     * received from the server in the fast load process.
     */
    class StringConverterMixin extends BaseClass {

        // the application object
        #docApp = null;

        constructor(docApp, ...baseCtorArgs) {
            super(docApp, ...baseCtorArgs);
            this.#docApp = docApp;
        }

        // public methods -----------------------------------------------------

        /**
         * Generating a document string from the editdiv element. jQuery data
         * elements are saved at the sub elements with the attribute 'jquerydata'
         * assigned to these elements. Information about empty paragraphs
         * is stored in the attribute 'nonjquerydata'.
         *
         * @param {jQuery.Deferred} [maindef]
         *  The deferred used to update the progress.
         *
         * @param {Number} [startProgress]
         *  The start value of the notification progress.
         *
         * @param {Number} [endProgress]
         *  The end value of the notification progress.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved or rejected if the document strings
         *  could be generated successfully. All document strings are packed into
         *  an array. Several document strings are generated for each layer, that
         *  needs to be stored in the local storage. This are the page content,
         *  the drawing layer, ... .
         */
        getFullModelDescription(maindef, startProgress, endProgress) {

            // collecting all keys from data object for debug reasons
            let dataKeys = [];
            // collector for all different saved nodes (page content, drawing layer , ...)
            const allNodesCollector = [];
            // collector for all html strings and the extension in the registry for every layer (page content, drawing layer , ...)
            const allHtmlStrings = [];
            // the number of chunks, that will be evaluated
            const chunkNumber = 5;
            // the current notification progress
            let currentNotify = startProgress;
            // the notify difference for one chunk
            const notifydiff = math.roundp((endProgress - startProgress) / chunkNumber, 0.01);
            // a selected drawing
            let selectedDrawing = null;
            // the padding-bottom value saved at the pagecontent node
            let paddingBottomInfo = null;
            // the selection object
            const selection = this.getSelection();

            // converting all editdiv information into a string, saving especially the data elements
            // finding all elements, that have a 'data' jQuery object set.
            // Helper function to split the array of document nodes into smaller parts.
            const prepareNodeForStorage = node => {

                // the jQuery data object of the node
                let dataObject = null;
                // the return promise
                const def = this.createDeferred();

                // updating progress bar
                if (maindef) {
                    currentNotify += math.roundp(notifydiff, 0.01);
                    maindef.notify(currentNotify); // updating corresponding to the length inside each -> smaller snippets deferred.
                }

                try {

                    // the current node, as jQuery object
                    node = $(node);

                    // simplify restauration of empty text nodes
                    if ((node.is('span')) && (isEmptySpan(node))) {
                        dataKeys.push('isempty');
                        // using nonjquerydata instead of jquerydata (32286)
                        node.attr('nonjquerydata', JSON.stringify({ isempty: true }));
                    }

                    // removing additional tracking handler options, that are specific for OX Text (Chrome only)
                    if (_.browser.WebKit && isDrawingFrame(node)) {
                        node.removeData('trackingOptions');
                        node.removeData('tracking-options');
                    }

                    // handling jQuery data objects
                    dataObject = node.data();

                    // removing spell check information at nodes
                    this.getSpellChecker().clearSpellcheckHighlighting(node, dataObject);

                    if (this.#docApp.isPresentationApp()) {
                        if (isSlideNode(node)) {
                            // for slides some additional data must be saved
                            dataObject = this.prepareSlideNodesForStorage(node, dataObject);
                        } else if (isDrawingFrame(node)) {
                            // drawing handling: saving list styles from model at drawing and renaming source of image to avoid too many image requests, when attaching the string to the DOM
                            dataObject = this.prepareDrawingNodesForStorage(node, dataObject);
                        }
                    }

                    if (_.isObject(dataObject) && !_.isEmpty(dataObject)) {
                        if (dataObject[DRAWINGPLACEHOLDER_LINK]) { delete dataObject[DRAWINGPLACEHOLDER_LINK]; }  // no link to other drawing allowed
                        if (dataObject[DRAWING_SPACEMAKER_LINK]) { delete dataObject[DRAWING_SPACEMAKER_LINK]; }  // no link to other drawing allowed
                        dataKeys = dataKeys.concat(_.keys(dataObject));

                        // DOCS-892: Minifying the dataObject to reduce string length in local storage
                        // The dataObject is a direct reference to the jQuery data object. There might be
                        // cases, when it is better to clone this object, because 'handleMinifiedObject'
                        // modifies the object. During closing the document, generating a clone is
                        // superfluous (see also 55226).
                        handleMinifiedObject(dataObject);

                        node.attr('jquerydata', JSON.stringify(dataObject));  // different key -> not overwriting isempty value
                        // globalLogger.log('Saving data: ' + this.nodeName + ' ' + this.className + ' : ' + JSON.stringify($(this).data()));
                    }

                    def.resolve();

                } catch (ex) {
                    // dataError = true;
                    globalLogger.info('quitHandler, failed to save document in local storage (3): ' + ex.message);
                    def.reject();
                }

                return def.promise();
            };  // end of 'prepareNodeForStorage'

            // appending all slides to the DOM
            if (this.#docApp.isPresentationApp()) { this.appendAllSlidesToDom(); }

            // removing drawing selections
            if (selection.getSelectionType() === 'drawing') {
                selectedDrawing = selection.getSelectedDrawings();
                _.each(selectedDrawing, drawing => { // drawing must be jQueryfied
                    clearSelection(drawing);
                    // clear crop frame from currently active crop drawings
                    if (selection.isCropMode()) { this.exitCropMode(); }
                    // removing additional tracking handler options, that are specific for OX Text (Chrome only)
                    drawing.removeData('trackingOptions');
                    drawing.removeData('tracking-options');
                });
            }

            // removing selections around 'additional' text frame selections
            if (selection.isAdditionalTextframeSelection()) {
                selectedDrawing = selection.getSelectedTextFrameDrawing();
                if (this.#docApp.isTextApp()) { clearSelection(selectedDrawing); }
                // removing additional tracking handler options, that are specific for OX Text (Chrome only)
                selectedDrawing.removeData('trackingOptions');
                selectedDrawing.removeData('tracking-options');

                if (this.#docApp.isPresentationApp()) { selection.setSlideSelection({ triggerChange: false }); } // includes handling template texts (61454)
            }

            // removing artificial selection of empty paragraphs
            if (!_.isEmpty(this.getHighlightedParagraphs()) || !_.isEmpty(this.getHighlightedCells())) { this.removeArtificalHighlighting(); }

            // removing a change track selection, if it exists
            if (this.getChangeTrack().getChangeTrackSelection()) { this.getChangeTrack().clearChangeTrackSelection(); }

            if (this.#docApp.isTextApp()) {
                // removing classes at a highlighted comment thread (because comment thread is active or hovered), if necessary
                this.getCommentLayer().clearCommentThreadHighlighting();

                // removing an optionally set comment author filter
                this.getCommentLayer().removeAuthorFilter();
            }

            // removing active header/footer context menu, if exists
            if (this.isHeaderFooterEditState()) {
                // first clean up drawing in placeholder node, and then leave edit mode
                this.getPageLayout().clearSelectionFromPlaceholderNode(this.getActiveTarget());
                this.getPageLayout().leaveHeaderFooterEditMode();
            }

            // removing search highlighting
            this.getSearchHandler().clearHighlighting();

            // removing artifical increased size of an implicit paragraph
            if (this.getIncreasedParagraphNode()) {
                $(this.getIncreasedParagraphNode()).css('height', 0);
                this.setIncreasedParagraphNode(null);
            }

            // First layer: Collecting all nodes of the page content
            allNodesCollector.push({ extension: '', selector: PAGECONTENT_NODE_SELECTOR, allNodes: this.getNode().children(PAGECONTENT_NODE_SELECTOR).find('*') });

            // saving specific page content node styles at its first child (style 'padding-bottom')
            paddingBottomInfo = this.getNode().children(PAGECONTENT_NODE_SELECTOR).css('padding-bottom');
            this.getNode().children(PAGECONTENT_NODE_SELECTOR).children(':first').attr('pagecontentattr', JSON.stringify({ 'padding-bottom': paddingBottomInfo }));

            if (this.#docApp.isTextApp()) {
                // Second layer in OX Text: Handling the drawing layer (taking care of order in allNodesCollector)
                if (this.getDrawingLayer().containsDrawingsInPageContentNode()) { allNodesCollector.push({ extension: '_DL', selector: DRAWINGLAYER_NODE_SELECTOR, allNodes: this.getNode().children(DRAWINGLAYER_NODE_SELECTOR).find('*') }); }
                // Third layer in OX Text: Handling the header/footer placeholder layer (taking care of order in allNodesCollector)
                if (this.getPageLayout().hasContentHeaderFooterPlaceHolder()) { allNodesCollector.push({ extension: '_HFP', selector: HEADER_FOOTER_PLACEHOLDER_SELECTOR, allNodes: this.getNode().children(HEADER_FOOTER_PLACEHOLDER_SELECTOR).find('*') }); }
                // Fourth layer in OX Text: Handling the comment layer (taking care of order in allNodesCollector)
                if (!this.getCommentLayer().isEmpty()) { allNodesCollector.push({ extension: '_CL', selector: COMMENTLAYER_NODE_SELECTOR, allNodes: this.getNode().children(COMMENTLAYER_NODE_SELECTOR).find('*') }); }

                // saving specific page node class names at the first child of the page content node
                if (this.isIncreasedDocContent()) { this.getNode().children(PAGECONTENT_NODE_SELECTOR).children(':first').addClass(INCREASED_CONTENT_CLASS); }
            }

            // Fifth and sixth layer: Master slide layer and layout slide layer of presentation application
            if (this.#docApp.isPresentationApp()) {
                allNodesCollector.push({ extension: '_LSL', selector: LAYOUTSLIDELAYER_SELECTOR, allNodes: this.getNode().children(LAYOUTSLIDELAYER_SELECTOR).find('*') });
                if (!this.#docApp.isODF()) { allNodesCollector.push({ extension: '_MSL', selector: MASTERSLIDELAYER_SELECTOR, allNodes: this.getNode().children(MASTERSLIDELAYER_SELECTOR).find('*') }); }
            }

            // iterating over all nodes (first page content, then drawing layer, ...)
            const promise = this.iterateArraySliced(allNodesCollector, oneLayer => {

                return this.iterateArraySliced(oneLayer.allNodes, prepareNodeForStorage)
                    .done(() => {
                        // collecting the html strings for each layer
                        allHtmlStrings.push({ extension: oneLayer.extension, htmlString: this.getNode().children(oneLayer.selector).html() });
                    });
            });

            return promise.then(() => { return allHtmlStrings; });
        }

        /**
         * Generating the editdiv element from an html string. jQuery data
         * elements are restored at the sub elements, if the attribute 'jquerydata'
         * is assigned to these elements.
         *
         * @param {String} htmlString
         *  The html string for the editdiv element.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.usedLocalStorage=false]
         *      If set to true, the specified html string was loaded from the
         *      local storage. In this case, no browser specific modifications
         *      need to be done on the string.
         *  @param {Boolean} [options.drawingLayer=false]
         *      If set to true, the specified html string is the drawing layer.
         *  @param {Boolean} [options.commentLayer=false]
         *      If set to true, the specified html string is the comment layer.
         *  @param {Boolean} [options.headerFooterLayer=false]
         *      If set to true, the specified html string is the header/footer placeholder layer.
         */
        setFullModelNode(htmlString, options) {

            // collecting all keys assigned to the data objects
            const dataKeys = [];
            // the data objects saved in the string for the nodes
            let dataObject = null;
            // whether the html string was read from the local storage
            const usedLocalStorage = getBooleanOption(options, 'usedLocalStorage', false);
            // whether the html string describes the comment layer (loaded from local storage)
            const isCommentLayer = getBooleanOption(options, 'commentLayer', false);
            // whether the html string describes the drawing layer (loaded from local storage)
            const isDrawingLayer = getBooleanOption(options, 'drawingLayer', false);
            // whether the html string describes the header/footer layer (loaded from local storage)
            const isHeaderFooterLayer = getBooleanOption(options, 'headerFooterLayer', false);
            // whether the html string describes the master slide layer (loaded from local storage)
            const isMasterSlideLayer = getBooleanOption(options, 'masterSlideLayer', false);
            // whether the html string describes the layout slide layer (loaded from local storage)
            const isLayoutSlideLayer = getBooleanOption(options, 'layoutSlideLayer', false);
            // eather the page content node, parent of the editdiv, or headerFooterContainer node
            let contentNode = null;

            // helper function to get the node in which the html string needs to be inserted
            const getStringParentNode = () => {

                // the parent node for each layer
                let parentNode = null;

                if (this.useSlideMode()) {
                    if (usedLocalStorage) {
                        if (isMasterSlideLayer) {
                            parentNode = this.getOrCreateMasterSlideLayerNode();
                        } else if (isLayoutSlideLayer) {
                            parentNode = this.getOrCreateLayoutSlideLayerNode();
                        } else {
                            parentNode = this.getNode().children(PAGECONTENT_NODE_SELECTOR);
                        }
                    } else {
                        parentNode = this.getNode();
                    }
                } else if (isDrawingLayer) {
                    parentNode = this.getDrawingLayer().getDrawingLayerNode();
                } else if (isCommentLayer) {
                    parentNode = this.getCommentLayer().getOrCreateCommentLayerNode({ visible: false });
                } else if (isHeaderFooterLayer) {
                    parentNode = this.getPageLayout().getHeaderFooterPlaceHolder();
                } else {
                    parentNode = this.getNode().children(PAGECONTENT_NODE_SELECTOR);
                }

                return parentNode;
            };

            // registering import type
            if (!this.isImportFinished()) {
                if (usedLocalStorage) {
                    this.setLocalStorageImport(true);
                } else {
                    this.setFastLoadImport(true);
                }
            }

            if (DEBUG && isCommentLayer) { this.getCommentLayer().saveCommentHtmlString(htmlString); }

            if (htmlString) {

                // setting the content node for each layer
                contentNode = getStringParentNode();

                // Performance: Hiding the page node, IE seems to parse the DOM completely // TODO -> Checking for using local storage
                if (this.#docApp.isPresentationApp()) { contentNode.children(PAGECONTENT_NODE_SELECTOR).addClass(HIDDENAFTERATTACH_CLASSNAME); }

                // setting new string to editdiv node
                contentNode
                    .html(htmlString)
                    .find('[jquerydata], [nonjquerydata]')
                    .get().forEach(oneNode => {
                        const node = $(oneNode);
                        let characterAttributes = null;
                        let key = null;

                        if (node.attr('nonjquerydata')) {
                            try {
                                dataObject = JSON.parse(node.attr('nonjquerydata'));
                                for (key in dataObject) {
                                    if (key === 'isempty') {  // the only supported key for nonjquerydata
                                        ensureExistingTextNode(oneNode);
                                        dataKeys.push(key);
                                    }
                                }
                                node.removeAttr('nonjquerydata');
                            } catch (ex) {
                                globalLogger.error('FastLoad: Failed to parse attributes (nonjquerydata): ' + node.attr('nonjquerydata') + ' Exception message: ' + ex.message);
                            }
                        }

                        if (node.attr('jquerydata')) {

                            try {
                                dataObject = JSON.parse(node.attr('jquerydata'));

                                // DOCS-892: Expanding the dataObject to get usable attributes from local storage or fast load string
                                handleMinifiedObject(dataObject, '', true);

                                // for relative urls which points into the document the correct image src attribute needs to be calculated.
                                // (the correct api url .../api or .../appsuite/api is not known when creating the fastload string)
                                if (!usedLocalStorage && isImageDrawingNode(node) && dataObject.attributes && dataObject.attributes.image) {
                                    node.find('img').attr('src', getFileUrl(this.#docApp, dataObject.attributes.image.imageUrl));
                                }

                                // No list style in comments in odt: 38829 (simply removing the list style id)
                                if (this.#docApp.isODF() && !usedLocalStorage && isCommentLayer && isParagraphNode(node) && dataObject.attributes && dataObject.attributes.paragraph && dataObject.attributes.paragraph.listStyleId) { delete dataObject.attributes.paragraph.listStyleId; }

                                // collecting all paragraphs with character attributes (not doing this after DOCS-4211)
                                if (!this.#docApp.isODF() && !usedLocalStorage && isParagraphNode(node) && !this.hasOoxmlTextCharacterFilterSupport()) {
                                    // check for character attributes
                                    if ('attributes' in dataObject && 'character' in dataObject.attributes) {
                                        characterAttributes = dataObject.attributes.character;
                                        node.data('origCharAttrs', _.copy(characterAttributes, true)); // saving the character attributes at the paragraph node, so that they can be used in clearAttributes operation
                                        delete dataObject.attributes.character;
                                        if (_.isEmpty(dataObject.attributes)) { delete dataObject.attributes; }
                                    }
                                }

                                for (key in dataObject) {
                                    if (node.data(key) && _.isObject(node.data(key))) {
                                        node.data(key, this.defAttrPool.extendAttrSet(node.data(key), dataObject[key], { clone: true })); // merge required
                                    } else {
                                        node.data(key, dataObject[key]);
                                    }
                                    dataKeys.push(key);
                                }

                                // assigning character attributes to the first (empty) text span (not doing this after DOCS-4211)
                                if (characterAttributes) { this.#assignCharacterAttributesToSpan(node, { attributes: { character: characterAttributes } }, dataKeys); }

                                node.removeAttr('jquerydata');
                            } catch (ex) {
                                globalLogger.error('FastLoad: Failed to parse attributes (jquerydata): ' + node.attr('jquerydata') + ' Exception message: ' + ex.message);
                            }
                        }

                        if (!this.#docApp.isODF() && isShapeDrawingFrame(node)) {
                            this.#removeEmptyTextFrameNodesInDrawing(node); // removing empty text frames without paragraphs (no paragraphs in special shapes, 50349)
                        }

                    });

                if (!usedLocalStorage) { // updating execeeded size table is required, if document was loaded with fast load, but not from local storage (63132)
                    contentNode.find('table[size-exceed-values]').get().forEach(oneTable => {
                        // the values provided by the backend which specifies what part of the table exceeds the size
                        const values = $(oneTable).attr('size-exceed-values');
                        // the separate parts
                        let parts = null;

                        if (values) {
                            parts = values.split(',');
                            if (parts && parts.length === 3) {
                                makeExceededSizeTable($(oneTable), parts[0], parts[1], parts[2]);
                            }
                        }
                    });
                }

                // Performance: Hiding slides immediately, IE seems to parse the DOM completely
                // -> the slide format manager removes this marker class, before slide is formatted
                if (this.#docApp.isPresentationApp()) { contentNode.find(SLIDE_SELECTOR).addClass(HIDDENAFTERATTACH_CLASSNAME); }

                // setting table cell attribute 'contenteditable' to true for non-MSIE browsers (task 33642)
                // -> this is not necessary, if the document was loaded from the local storage
                if (!usedLocalStorage) {
                    handleReadOnly(contentNode.find('div.cell'));
                }

                // assigning page content attributes, that are saved at its first child
                if (contentNode.children(':first').attr('pagecontentattr')) {
                    dataObject = JSON.parse(contentNode.children(':first').attr('pagecontentattr'));
                    if (dataObject['padding-bottom']) {
                        contentNode.css('padding-bottom', dataObject['padding-bottom']);
                    }
                    contentNode.children(':first').removeAttr('pagecontentattr');
                }

                // saving specific page node class names at the first child of the page content node
                if (contentNode.children(':first').hasClass(INCREASED_CONTENT_CLASS)) {
                    this.getNode().addClass(INCREASED_CONTENT_CLASS);
                    contentNode.children(':first').removeClass(INCREASED_CONTENT_CLASS);
                }

                if (this.#docApp.isODF()) { // in odt it is possible to have empty fields, text node needs to be ensured, #49844
                    _.each(contentNode.find('.empty-field'), fieldNode => {
                        ensureExistingTextNodeInField(fieldNode.firstChild);
                    });
                }

                // TODO: if still of interest, create a dedicated logger with configuration key, instead of using global logger
                // if (!_.isEmpty(dataKeys)) { globalLogger.info('Setting data keys: ' + getSortedArrayString(dataKeys)); }
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Assigning the character attributes at (empty) paragraphs to the text span
         * inside the paragraphs (not doing this after DOCS-4211).
         *
         * @param {jQuery} paragraph
         *  The paragraph node.
         *
         * @param {Object} attributes
         *  The object with the character attributes.
         *
         * @param {Array} dataKeys
         *  The collector for all assigned keys.
         */
        #assignCharacterAttributesToSpan(paragraph, attributes, dataKeys) {

            _.each(paragraph.children('span'), node => {
                if (isSpan(node)) {
                    // setting the character attributes to the text span(s) inside the paragraph
                    for (const key in attributes) {
                        $(node).data(key, attributes[key]);
                        dataKeys.push(key);
                    }
                }
            });

        }

        /**
         * Helper function to remove empty text frame nodes without paragraph in shapes. In fast load process
         * every shape gets a text frame node, so that a paragraph can be inserted. This is not correct for
         * selected connector shapes (bug 50349). Because this information is not available on server side,
         * this needs to be fixed with this process on client side. An improved server side process might be
         * created later.
         *
         * @param {Node|jQuery} drawing
         *  The drawing node.
         */
        #removeEmptyTextFrameNodesInDrawing(drawing) {

            const textFrameNode = getTextFrameNode(drawing);

            if (textFrameNode.children().length === 0) {
                // removing the drawing node
                textFrameNode.remove();
                // removing class from 'textframecontent' from content node
                getContentNode(drawing).removeClass(TEXTFRAMECONTENT_NODE_CLASS);
            }

        }
    }

    return StringConverterMixin;
}

// exports ====================================================================

export const XStringConverter = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XStringConverter>}
     *  The extended class with string converter functionality.
     */
    mixin: mixinStringConverter
};
