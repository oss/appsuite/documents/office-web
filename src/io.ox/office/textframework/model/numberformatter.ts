/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { pick } from "@/io.ox/office/tk/algorithms";

import type { FormatCategory, BaseFormatterConfig } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { BaseFormatter } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";

// class NumberFormatter ======================================================

/**
 * A number formatter for a text document.
 *
 * @param docModel
 *  The document model containing this instance.
 */
export class NumberFormatter<DocModelT extends TextBaseModel = TextBaseModel> extends BaseFormatter {

    readonly docModel: DocModelT;

    // constructor ------------------------------------------------------------

    constructor(docModel: DocModelT, config?: BaseFormatterConfig) {
        super(docModel.docApp.getFileFormat(), config);
        this.docModel = docModel;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns all available number format codes for the specified format
     * category and current UI language.
     *
     * @param category
     *  The identifier of the number format category.
     *
     * @returns
     *  The format codes for the specified category.
     */
    getCategoryCodes(category: FormatCategory): string[] {
        const resource = this.docModel.docApp.resourceManager.getResource("numberformats");
        const categoryFormats = pick.array(resource.categories, category.toLowerCase(), true);
        return categoryFormats.map(entry => pick.string(entry, "value")).filter(Boolean);
    }
}
