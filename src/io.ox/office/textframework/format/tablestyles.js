/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is } from '@/io.ox/office/tk/algorithms';
import { IOS_SAFARI_DEVICE, SMALL_DEVICE, convertHmmToLength, convertLengthToHmm } from '@/io.ox/office/tk/dom';
import { iterateSelectedDescendantNodes } from '@/io.ox/office/tk/utils';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { isTableDrawingFrame } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { opSchemeColor, Color } from '@/io.ox/office/editframework/utils/color';
import Gradient from '@/io.ox/office/editframework/utils/gradient';
import { NO_BORDER, NONE, isVisibleBorder } from '@/io.ox/office/editframework/utils/border';
import { LineHeight } from '@/io.ox/office/editframework/utils/lineheight';
import TableStyleCollection from '@/io.ox/office/editframework/model/tablestylecollection';
import { getColumnCount, getGridColumnRangeOfCell, getGridRowRangeOfCell, getRowCount, updateColGroup } from '@/io.ox/office/textframework/components/table/table';
import { DRAWING_NODE_SELECTOR, PARAGRAPH_NODE_SELECTOR, TAB_NODE_SELECTOR, TABLE_NODE_SELECTOR, getCellContentNode, getTableRows,
    isExceededSizeTableNode } from '@/io.ox/office/textframework/utils/dom';

// constants ==============================================================

// definitions for table attributes
const TABLE_ATTRIBUTES = {
    width:              { def: 'auto', parse: parseTableWidth },
    fillColor:          { def: Color.AUTO },
    tableGrid:          { def: [], scope: 'element' },
    exclude:            { def: [], scope: 'element' },
    borderLeft:         { def: NONE },
    borderTop:          { def: NONE },
    borderRight:        { def: NONE },
    borderBottom:       { def: NONE },
    borderInsideHor:    { def: NONE },
    borderInsideVert:   { def: NONE },
    indent:             { def: 0 },
    paddingTop:         { def: 0 },
    paddingBottom:      { def: 0 },
    paddingLeft:        { def: 0 },
    paddingRight:       { def: 0 }
};

// the conditional table attributes (only 'wholeTable' has no geometric dependencies)
const TABLE_REGIONS = ['firstRow', 'lastRow', 'firstCol', 'lastCol', 'band1Vert', 'band2Vert', 'band1Hor', 'band2Hor', 'northEastCell', 'northWestCell', 'southEastCell', 'southWestCell'];

// the border properties
const BORDER_PROPS = ['borderTop', 'borderBottom', 'borderLeft', 'borderRight', 'borderInsideHor', 'borderInsideVert'];

// private functions ==========================================================

function parseTableWidth(value) {
    return (is.number(value) || is.string(value)) ? value : undefined;
}

// class TableStyles ======================================================

/**
 * Contains the style sheets for table formatting attributes. The CSS
 * formatting will be written to table elements and their rows and cells.
 *
 * @param {TextModel} docModel
 *  The text document model containing instance.
 */
class TableStyles extends TableStyleCollection {

    constructor(docModel) {

        // base constructor
        super(docModel, {
            families: ['cell', 'paragraph', 'character', 'changes'],
            styleAttributesResolver: (...args) => this.#resolveTableStyleAttributes(...args),
            elementAttributesResolver: (...args) => this.#resolveTableElementAttributes(...args)
        });

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('table', TABLE_ATTRIBUTES);

        // register the formatting handlers for DOM elements
        this.registerFormatHandler(this.#updateTableFormatting);
    }

    // public methods -----------------------------------------------------

    /**
     * Performance: Helper function to update an optional table background.
     * This is only supported in the presentation app.
     *
     * @param {Node|jQuery} drawing
     *  The table drawing node.
     */
    updateTableDrawingBackground(drawing) {

        // the explicit drawing attributes
        const expAttrs = getExplicitAttributeSet(drawing);
        // the table node inside the drawing
        const tableNode = $(drawing).find(TABLE_NODE_SELECTOR);

        if (tableNode.length > 0 && expAttrs && expAttrs.styleId) {
            const styleSheetAttributeMap = this.getStyleSheetAttributeMap(expAttrs.styleId);
            this.#handleTableBackground(tableNode, styleSheetAttributeMap);
        }
    }

    /**
     * Check the stored table styles of a document and adds a 'missing'
     * default table style. This ensures that we can insert tables that
     * are based on a reasonable default style.
     */
    insertMissingTableStyles() {

        const styleNames = this.getStyleSheetNames();
        const parentId = this.getDefaultStyleId();
        const hasDefaultStyle = _.isString(parentId) && (parentId.length > 0);
        const defTableDef = this.docModel.getDefaultLateralTableDefinition();
        const defTableAttr = this.docModel.getDefaultLateralTableAttributes();
        let defTableOdfDef = null;
        let defTableOdfAttr = null;
        const isODF = this.docApp.isODF();
        const themeModel = this.docModel.getThemeModel();  // the active theme model of the document

        if (!hasDefaultStyle) {
            // Add a missing default table style
            this.createDirtyStyleSheet(defTableDef.styleId, defTableDef.styleName, defTableAttr, { priority: 59, defStyle: defTableDef.default });
        } else {
            // Search for a style defined in the document that can be used for tables
            // If we cannot find it we have to add it.
            let lowestUIPriority = 99;
            let tableStyleId = null;

            _(styleNames).each((_name, id) => {
                const uiPriority = this.getUIPriority(id);

                if (_.isNumber(uiPriority) && (uiPriority < lowestUIPriority)) {
                    tableStyleId = id;
                    lowestUIPriority = uiPriority;
                }
            });

            if ((!tableStyleId) || ((tableStyleId === this.getDefaultStyleId()) && (lowestUIPriority === 99))) {
                // OOXML uses a default table style which contains no border
                // definitions. Therfore we add our own default table style
                // if we only find the default style with uiPriority 99
                this.createDirtyStyleSheet(defTableDef.styleId, defTableDef.styleName, defTableAttr, { parent: parentId, priority: 59 });
            }

            if (this.docApp.isODF() && tableStyleId === this.getDefaultStyleId() && tableStyleId === '_default') {
                // ODF uses a default table style which contains no border definitions.
                // Therefore we add our own default table style, see #49674
                defTableOdfDef = this.docModel.getDefaultLateralTableODFDefinition();
                defTableOdfAttr = this.docModel.getDefaultLateralTableODFAttributes();
                this.createDirtyStyleSheet(defTableOdfDef.styleId, defTableOdfDef.styleName, defTableOdfAttr, { parent: parentId, priority: 59 });
            }
        }

        // adding further table styles, if not already available
        if (this.docModel.useSlideMode()) {

            this.#insertMissingTableStyle('{2D5ABB26-0587-4C30-8999-92F81FD0307C}', 'No Style, No Grid', 80, '', TableStyles.getTableNoGridNoStylePresentation('text1', themeModel));
            this.#insertMissingTableStyle('{5940675A-B579-460E-94D1-54222C63F5DA}', 'No Style, Table Grid', 82, '', TableStyles.getTableGridPresentation('text1', themeModel));

            this.#insertMissingTableStyle('{616DA210-FB5B-4158-B5E0-FEB733F419BA}', 'Light Style 3', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('text1', 'text1', isODF));
            this.#insertMissingTableStyle('{BC89EF96-8CEA-46FF-86C4-4CE0E7609802}', 'Light Style 3 - Accent 1', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('accent1', 'text1', isODF, themeModel));
            this.#insertMissingTableStyle('{5DA37D80-6434-44D0-A028-1B22A696006F}', 'Light Style 3 - Accent 2', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('accent2', 'text1', isODF, themeModel));
            this.#insertMissingTableStyle('{8799B23B-EC83-4686-B30A-512413B5E67A}', 'Light Style 3 - Accent 3', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('accent3', 'text1', isODF, themeModel));
            this.#insertMissingTableStyle('{ED083AE6-46FA-4A59-8FB0-9F97EB10719F}', 'Light Style 3 - Accent 4', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('accent4', 'text1', isODF, themeModel));
            this.#insertMissingTableStyle('{BDBED569-4797-4DF1-A0F4-6AAB3CD982D8}', 'Light Style 3 - Accent 5', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('accent5', 'text1', isODF, themeModel));
            this.#insertMissingTableStyle('{E8B1032C-EA38-4F05-BA0D-38AFFFC7BED3}', 'Light Style 3 - Accent 6', 85, 'Light', TableStyles.getLightStyle3TableStyleAttributesPresentation('accent6', 'text1', isODF, themeModel));

            this.#insertMissingTableStyle('{073A0DAA-6AF3-43AB-8588-CEC1D06C72B9}', 'Medium Style 2', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('dark1', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}', 'Medium Style 2 - Accent 1', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent1', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{21E4AEA4-8DFA-4A89-87EB-49C32662AFE0}', 'Medium Style 2 - Accent 2', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent2', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{F5AB1C69-6EDB-4FF4-983F-18BD219EF322}', 'Medium Style 2 - Accent 3', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent3', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{00A15C55-8517-42AA-B614-E9B94910E393}', 'Medium Style 2 - Accent 4', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent4', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{7DF18680-E054-41AD-8BC1-D1AEF772440D}', 'Medium Style 2 - Accent 5', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent5', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{93296810-A885-4BE3-A3E7-6D5BEEA58F35}', 'Medium Style 2 - Accent 6', 59, 'Medium', TableStyles.getMediumStyle2TableStyleAttributesPresentation('accent6', 'light1', 'dark1', themeModel));

            this.#insertMissingTableStyle('{8EC20E35-A176-4012-BC5E-935CFFF8708E}', 'Medium Style 3', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('dark1', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{6E25E649-3F16-4E02-A733-19D2CDBF48F0}', 'Medium Style 3 - Accent 1', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('accent1', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{85BE263C-DBD7-4A20-BB59-AAB30ACAA65A}', 'Medium Style 3 - Accent 2', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('accent2', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{EB344D84-9AFB-497E-A393-DC336BA19D2E}', 'Medium Style 3 - Accent 3', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('accent3', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{EB9631B5-78F2-41C9-869B-9F39066F8104}', 'Medium Style 3 - Accent 4', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('accent4', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{74C1A8A3-306A-4EB7-A6B1-4F7E0EB9C5D6}', 'Medium Style 3 - Accent 5', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('accent5', 'light1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{2A488322-F2BA-4B5B-9748-0D474271808F}', 'Medium Style 3 - Accent 6', 59, 'Medium', TableStyles.getMediumStyle3TableStyleAttributesPresentation('accent6', 'light1', 'dark1', themeModel));

            this.#insertMissingTableStyle('{D7AC3CCA-C797-4891-BE02-D94E43425B78}', 'Medium Style 4', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('dark1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{69CF1AB2-1976-4502-BF36-3FF5EA218861}', 'Medium Style 4 - Accent 1', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('accent1', 'dark1', themeModel));
            this.#insertMissingTableStyle('{8A107856-5554-42FB-B03E-39F5DBC370BA}', 'Medium Style 4 - Accent 2', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('accent2', 'dark1', themeModel));
            this.#insertMissingTableStyle('{0505E3EF-67EA-436B-97B2-0124C06EBD24}', 'Medium Style 4 - Accent 3', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('accent3', 'dark1', themeModel));
            this.#insertMissingTableStyle('{C4B1156A-380E-4F78-BDF5-A606A8083BF9}', 'Medium Style 4 - Accent 4', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('accent4', 'dark1', themeModel));
            this.#insertMissingTableStyle('{22838BEF-8BB2-4498-84A7-C5851F593DF1}', 'Medium Style 4 - Accent 5', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('accent5', 'dark1', themeModel));
            this.#insertMissingTableStyle('{16D9F66E-5EB9-4882-86FB-DCBF35E3C3E4}', 'Medium Style 4 - Accent 6', 59, 'Medium', TableStyles.getMediumStyle4TableStyleAttributesPresentation('accent6', 'dark1', themeModel));

        } else {

            this.#insertMissingTableStyle('TableGrid', 'Table Grid', 1, '', TableStyles.getTableGrid(themeModel));
            this.#insertMissingTableStyle('TableNormal', 'Normal Table', 2, '', TableStyles.getNormalTable());

            this.#insertMissingTableStyleGroup('LightShading',   'Light Shading',    60, TableStyles.getLightShadingTableStyleAttributes, 'Light');
            this.#insertMissingTableStyleGroup('MediumShading1', 'Medium Shading 1', 63, TableStyles.getMediumShading1TableStyleAttributes, 'Medium');
            this.#insertMissingTableStyleGroup('MediumShading2', 'Medium Shading 2', 64, TableStyles.getMediumShading2TableStyleAttributes, 'Medium');
            this.#insertMissingTableStyleGroup('MediumGrid1',    'Medium Grid 1',    67, TableStyles.getMediumGrid1TableStyleAttributes, 'Medium');
        }
    }

    // private methods ----------------------------------------------------

    // in presentation app the border attributes are assigned to 'cell' family, not 'table' family. But for correct
    // resolving of table border properties it is necessary, that they are assigned to the 'table' family, too (48647).
    #transferWholeTableBordersToTable(tableStyleAttributes, attributes) {
        if (tableStyleAttributes.wholeTable.cell) {
            _.each(BORDER_PROPS, prop => {
                if (tableStyleAttributes.wholeTable.cell[prop] && (!tableStyleAttributes.wholeTable.table || !tableStyleAttributes.wholeTable.table[prop])) {
                    if (!attributes.table) { attributes.table = {}; }
                    attributes.table[prop] = _.copy(tableStyleAttributes.wholeTable.cell[prop], true);
                }
            });
        }
    }

    /**
     * Returns the attributes of the specified attribute family contained
     * in table style sheets. Resolves the conditional attributes that
     * match the position of the passed source element.
     *
     * @param {Object} styleAttributes
     *  The complete 'attributes' object of a table style sheet.
     *
     * @param {jQuery} tableNode
     *  The DOM table node referring to a table style sheet, as jQuery
     *  object.
     *
     * @param {jQuery} cellNode
     *  The DOM cell node that has initially requested the formatting
     *  attributes of a table style sheet, as jQuery object (may be empty).
     *
     * @returns {Object}
     *  The formatting attributes extracted from the passed style sheet
     *  attributes object, as map of attribute value maps (name/value
     *  pairs), keyed by attribute family.
     */
    #resolveTableStyleAttributes(styleAttributes, tableNode, cellNode) {

        // the column interval covered by the cell
        const colInterval = (cellNode.length > 0) ? getGridColumnRangeOfCell(cellNode) : null;
        // the row interval covered by the cell
        const rowInterval = (cellNode.length > 0) ? getGridRowRangeOfCell(cellNode) : null;
        // the width of the table (number of columns)
        const colCount = getColumnCount(tableNode);
        // the height of the table (number of rows)
        const rowCount = getRowCount(tableNode);
        // the resulting cell attribute set
        const attrSet = {};

        // the options needed to resolve the correct cell attributes
        const  options = { firstRow: true, lastRow: true, firstCol: true, lastCol: true, bandsHor: true, bandsVert: true };
        _.each(getExplicitAttributes(tableNode, 'table', true).exclude, key => {
            delete options[key];
        });

        // in presentation app the border attributes are set at the 'cell' family
        if (this.docModel.useSlideMode() && styleAttributes.wholeTable) {
            this.#transferWholeTableBordersToTable(styleAttributes, attrSet);
        }

        // attaching all explicit attributes to the options for OX Text (required for DOCS-3245)
        if (this.docApp.isTextApp()) { options.explicitAttributes = getExplicitAttributes(tableNode, 'table', true); }

        const attrSetResult = this.resolveCellAttributeSet(styleAttributes, colInterval, rowInterval, colCount, rowCount, options);
        return this.extendAttrSet(attrSet, attrSetResult.cellAttrSet);
    }

    /**
     * Returns the attributes of the specified attribute family contained
     * in table style sheets. Resolves the conditional attributes that
     * match the position of the passed source element.
     *
     * @param {Object} elementAttributes
     *  The explicit attributes of the table element, as map of attribute
     *  value maps (name/value pairs), keyed by attribute family.
     *
     * @param {jQuery} tableNode
     *  The DOM table node, as jQuery object.
     *
     * @param {jQuery} cellNode
     *  The DOM cell node that has initially requested the formatting
     *  attributes, as jQuery object (may be empty).
     *
     * @returns {Object}
     *  The resolved explicit formatting attributes, as map of attribute
     *  value maps (name/value pairs), keyed by attribute family.
     */
    #resolveTableElementAttributes(elementAttributes, tableNode, cellNode) {
        return this.#resolveTableStyleAttributes({ wholeTable: elementAttributes }, tableNode, cellNode);
    }

    /**
     * Setting the table background. This can currently only be done using a style.
     * Only supported in presentation app.
     *
     * @param {jQuery} table
     *  The DOM table node, as jQuery object.
     *
     * @param {Object} styleSheetAttributeMap
     *  The style sheet attributes of the table
     */
    #handleTableBackground(table, styleSheetAttributeMap) {

        if (styleSheetAttributeMap && styleSheetAttributeMap.backgroundStyle && styleSheetAttributeMap.backgroundStyle.fill) {

            // the fill attributes of the slide
            const fillAttrs = styleSheetAttributeMap.backgroundStyle.fill;
            // the CSS properties for the slide
            const cssProps = {};

            let elmSlide;
            let targets;
            let gradient;
            let dataUrl;

            // calculate the CSS fill attributes
            switch (fillAttrs.type) {
                case 'none':
                    // clear everything: color and bitmaps
                    cssProps.background = '';
                    break;

                case 'solid':
                    cssProps.background = this.docModel.getCssColor(fillAttrs.color, 'fill', this.docModel.getThemeTargets(table));
                    break;

                case 'gradient':
                    elmSlide = table[0];
                    targets = this.docModel.getThemeTargets(table);
                    gradient = Gradient.create(Gradient.getDescriptorFromAttributes(fillAttrs));
                    dataUrl = Gradient.parseImageDataUrl(gradient, this.docModel, { width: elmSlide.offsetWidth, height: elmSlide.offsetHeight }, targets);

                    cssProps.background = ['url("', dataUrl, '")'].join('');
                    cssProps.backgroundSize = '100%'; // #49272
                    break;

                default:
                    globalLogger.warn('TableStyles.#handleTableBackground(): unknown fill type "' + fillAttrs.type + '"');
            }

            // apply the fill attributes
            table.css(cssProps);

        } else {
            table.css({ background: '' }); // removing background
        }

    }

    /**
     * Will be called for every table element whose attributes have been
     * changed. Repositions and reformats the table according to the passed
     * attributes.
     *
     * @param {jQuery} table
     *  The `<table>` element whose table attributes have been changed, as
     *  jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute value maps (name/value pairs), keyed by
     *  attribute family, containing the effective attribute values merged
     *  from style sheets and explicit attributes.
     *
     * @param {Boolean} async
     *  If set to true, the table formatting will be updated asynchronously
     *  in a browser timeout loop.
     *
     * @returns {jQuery.Promise}
     *  The promise of a Deferred object that will be resolved when the
     *  the table is completely formatted.
     */
    #updateTableFormatting(table, mergedAttributes, async) {

        // the column widths
        const tableGrid = mergedAttributes.table.tableGrid;
        // the explicit table attributes
        const tableAttributes = getExplicitAttributes(table, 'table');
        // all row nodes in the table
        let rowNodes = getTableRows(table);
        // a collector for all cells in the first row
        const allCellsInFirstRow = rowNodes.first().children('td').children('div.cell');
        // the upper left cell of the table (the div.cell element)
        const firstCell = allCellsInFirstRow.first();
        // a counter for the cells (performance)
        const cellCounter = allCellsInFirstRow.length * rowNodes.length;
        // whether this update was triggered by a resize of table (column width)
        let isTableResize = false;
        // whether the table has a 'simple' style
        let isSimpleTableStyle = false;
        // whether cells were removed from the table
        let isGUIRemoveOperation = false;
        // the number of the first row, that requires a formatting update
        let minUpdateRow = null;
        // the style sheet attributes for the table
        let styleSheetAttributeMap = null;
        // a collector for cell border styles, so that they can be assigned to the table later (DOCS-2400)
        // -> this is relevant for ODT (OX Text and OX Presentation) and OOXML (only OX Presentation)
        const cellBorderCollector = ((this.docApp.isODF() || this.docApp.isPresentationApp()) && !table.data('odtBorderSet')) ? [] : null;

        // checking if the attributes have table geometric dependencies
        // -> then a complete reformatting of the table is required.
        // Otherwise only the new cells (insert operations) or no cells
        // (delete operations) need to be formatted.
        const checkSimpleTableStyle = () => {

            let isSimple = true;

            _.each(TABLE_REGIONS, attribute => {
                if ((attribute in styleSheetAttributeMap) && !_.contains(tableAttributes.exclude, attribute)) {
                    isSimple = false;
                }
            });
            return isSimple;
        };

        // helper function to update the top and the left resizers in the table cell
        const updateTopLeftResizers = () => {

            // the old upper left cell is no longer the current upper left cell
            table.find('.upperLeftCell').removeClass('upperLeftCell').removeData('cellcount');

            // removing old top and left resizer nodes
            table.find('.resize.top, .resize.left').remove();

            // adding new top and left resizers to first row and first column
            allCellsInFirstRow.prepend($('<div>').addClass('resize top'));

            rowNodes.children('td:first-child').children('div.cell').prepend($('<div>').addClass('resize left'));

            // marking the new upper left cell (saving cell count for performance reasons)
            firstCell.addClass('upperLeftCell').data('cellcount', cellCounter);
        };

        // updates the formatting of the passed table row
        const updateRowFormatting = rowNode => {

            // update the table row itself
            this.docModel.tableRowStyles.updateElementFormatting(rowNode);

            // update table cells (do NOT pass merged attributes passed
            // to the this.#updateTableFormatting() method as base attributes,
            // attributes must be recalculated for each single cell!)
            _.each($(rowNode).children('td'), cellNode => {

                let cellFormatting = true;

                // Performance: Updating only new cells, if this update was triggered from a GUI event
                // and if the table has a simple table style -> otherwise all cells need to be updated
                // Performance: Simplified cell formatting for deleteColumn and deleteRow GUI operations.
                // For the insert operations (insertRow or insertColumn) cell (and paragraph) formatting
                // is always required.
                if (isGUIRemoveOperation) {
                    if (isSimpleTableStyle) {
                        cellFormatting = false;  // using simplified cell formatting update (only taking care of tab stops)
                        // TODO: For not simple table styles the full cell formatting is only required for those cells
                        // that are behind the new/removed cell in the row for insertColumn and deleteColumn
                    }
                }

                // Performance: Resizing the table (column width) requires only tab stop recalculation
                if (isTableResize) { cellFormatting = false; }

                if (cellFormatting) {
                    // complete formatting update of the cell -> this is necessary for complex table styles, but also for simple
                    // table styles, because paragraph formatting is also necessary. For example determines the paragraph height the
                    // height of the row, if the table row height is set to 'auto'.
                    this.docModel.tableCellStyles.updateElementFormatting(cellNode, { collector: cellBorderCollector });
                } else {
                    // checking if a cell contains tab stops, at least these need to be updated, if cell width was changed
                    if ($(cellNode).find(TAB_NODE_SELECTOR).length > 0) {
                        // updating the tabs is always required, because the width of a cell might have changed -> updating all paragraphs in the table cell
                        iterateSelectedDescendantNodes(getCellContentNode(cellNode), PARAGRAPH_NODE_SELECTOR, paragraph => {
                            this.docModel.paragraphStyles.updateTabStops(paragraph);
                        }, undefined, { children: true });
                    }
                }
            });
        };

        // no formatting of exceeded size table (52505)
        if (isExceededSizeTableNode(table)) {
            this.docModel.getChangeTrack().updateChangeTrackAttributes(table, mergedAttributes); // change track attribute handling
            return $.when();
        }

        // checking if the attributes have table geometric dependencies or table background
        if (mergedAttributes.styleId) {
            styleSheetAttributeMap = this.docModel.tableStyles.getStyleSheetAttributeMap(mergedAttributes.styleId);
            isSimpleTableStyle = checkSimpleTableStyle();
        }

        // no formatting of the drawing node with table attributes in OX Presentation
        if (this.docApp.isPresentationApp() && isTableDrawingFrame(table)) { return $.when(); }

        let width = tableAttributes.width;
        // TODO: Adding support for width in percent
        // -> better for changes of page size or page orientation
        if (width === 'auto') {
            table.css('width', '100%');
        } else {
            // adapt size of table to its parent (36132,36163)
            width = convertHmmToLength(width, 'px', 1);
            const parentWidth = table.parent().width();
            table.css('width', (parentWidth < width) ? '100%' : (width + 'px'));
        }

        // indent of table in OX Text (DOCS-3544)
        if (this.docApp.isTextApp() && mergedAttributes.table.indent > 0) {
            table.css('margin-left', convertHmmToLength(mergedAttributes.table.indent, 'px', 1));
        }

        // setting table background
        // -> but not for tables in OX Text and OOXML (background-color is in Word the space between the cells) -> DOCS-3464
        const useTableBackground = !(this.docApp.isTextApp() && this.docApp.isOOXML());

        if (useTableBackground) { table.css('background-color', this.docModel.getCssColor(tableAttributes.fillColor, 'fill')); }

        // handling of table background
        // if (this.docModel.useSlideMode()) { this.#handleTableBackground(table, styleSheetAttributeMap); }

        if (table.data('gui')) {
            if (table.data('gui') === 'remove') {
                isGUIRemoveOperation = true;
            }
            table.removeData('gui');
        }

        if (table.data('tableResize')) {
            isTableResize = true;
            table.removeData('tableResize');
        }

        // reformatting table colgroup in Firefox during pasting internal clip board, task 29401, task 30477
        if ((table.data('internalClipboard') || table.data('undoRedoRunning')) && _.browser.Firefox) {
            table.removeData('internalClipboard');
            table.removeData('undoRedoRunning');
        }

        // update column widths (also required for bug fixes 29212 and 29675)
        updateColGroup(table, tableGrid);

        // Fix for 30821 (on webkit browsers), max-width of 100% is ignored, if width is defined
        if ((_.browser.WebKit) && (table.closest('div.cellcontent').length > 0) && (table.css('max-width') === '100%') && (table.width() > table.closest('div.cellcontent').width())) {
            table.css('width', '');
        }

        // Performance: Immediately return, if no update required for simple tables (for example after deleteRows).
        // Even update of tab stops is not required for insertRow and deleteRow. But insertRow requires update of
        // paragraph attributes.
        if (_.isNumber(table.data('reducedTableFormatting'))) {
            minUpdateRow = table.data('reducedTableFormatting');
            table.removeData('reducedTableFormatting');
            if ((isSimpleTableStyle) && (isGUIRemoveOperation)) {
                return $.when();
            }
        }

        // Performance check for very slow devices. In this case an asynchronous update is
        // required, so that the user gets faster feed back
        async = async || (IOS_SAFARI_DEVICE && SMALL_DEVICE);

        // updating the top and left resize markers in the table. This is necessary, if the
        // upper left cell is no longer the upper left cell of the table or if the number of
        // cells in the table has changed (insertRow, ...)
        if (this.docModel.useSlideMode() && firstCell.length > 0 && (!firstCell.hasClass('upperLeftCell') || firstCell.data('cellcount') !== cellCounter)) { updateTopLeftResizers(); }

        // Performance: Reducing the number of rows, because upper rows require no update
        if (_.isNumber(minUpdateRow)) {
            rowNodes = rowNodes.slice(minUpdateRow);
        }

        // change track attribute handling
        this.docModel.getChangeTrack().updateChangeTrackAttributes(table, mergedAttributes);

        // update rows, cells, ... , but only if required
        let promise = null;
        if (async) {
            // process the table asynchronously
            promise = this.iterateArraySliced(rowNodes, updateRowFormatting);
            promise.always(() => {
                if (this.docModel.useSlideMode()) { this.#handleTableBackground(table, styleSheetAttributeMap); } // (48370)
                if (cellBorderCollector && cellBorderCollector.length) { this.#calculateTableBorderFromCellBorders(table, cellBorderCollector); } // DOCS-2400
                this.docModel.trigger('table:formatting:done', table);
            });

        } else {
            // update all rows at once (but asynchronous, so that for example a new column is visible immediately)
            promise = this.executeDelayed(() => {
                _.each(rowNodes, rowNode => { updateRowFormatting(rowNode); });
                if (this.docModel.useSlideMode()) { this.#handleTableBackground(table, styleSheetAttributeMap); } // (48370)
                if (cellBorderCollector && cellBorderCollector.length) { this.#calculateTableBorderFromCellBorders(table, cellBorderCollector); } // DOCS-2400
                this.docModel.trigger('table:formatting:done', table);
            });
        }

        return promise;
    }

    // inserts the table style sheet described by the passed parameters, if it does not exist yet, and marks it as dirty
    #insertMissingTableStyle(styleId, styleName, uiPriority, category, attrs) {
        // check by style sheet name (they are always loaded in English from Word, while the style IDs are localized)
        // In Presentation app the style IDs are unique and never translated (55470)
        if (!this.containsStyleSheet(styleId) && !this.containsStyleSheetByName(styleName)) {
            this.createDirtyStyleSheet(styleId, styleName, attrs, { priority: uiPriority, category });
        } else {
            this.setStyleOptions(styleId, { category, priority: uiPriority });
        }
    }

    // inserts a group of table style definitions for the primary text color and all six accent colors
    #insertMissingTableStyleGroup(baseStyleId, baseStyleName, uiPriority, generateAttributesFunc, category) {
        // the active theme model of the document
        const themeModel = this.docModel.getThemeModel();

        this.#insertMissingTableStyle(baseStyleId, baseStyleName, uiPriority, category ? category : baseStyleName, generateAttributesFunc('text1', themeModel));
        for (let index = 1; index <= 6; index += 1) {
            this.#insertMissingTableStyle(baseStyleId + '-Accent' + index, baseStyleName + ' Accent ' + index, uiPriority, category ? category : baseStyleName, generateAttributesFunc('accent' + index, themeModel));
        }
    }

    /**
     * Calculating the borders for a table from the collection of all cell borders. This is required in ODT,
     * because borders are only assigned to cells. But the table border picker relies on border styles at
     * tables and therefore does not work correctly after loading and shows a wrong table border style (DOCS-2400).
     *
     * Within this function some additional border attributes are assigned to the table, that were not sent
     * by the filter during loading and that will also not be sent to the filter.
     *
     * @param {jQuery} table
     *  The table node.
     *
     * @param {Object[]} cellBorders
     *  The collected cell border attributes. Additionally the column interval and the row interval of the
     *  cell inside the table is collected next to the cell attributes.
     */
    #calculateTableBorderFromCellBorders(table, cellBorders) {

        // the table borders calculated from the cell borders
        const tableBorderSet = {};
        // the width of the table (number of columns)
        const colCount = getColumnCount(table);
        // the height of the table (number of rows)
        const rowCount = getRowCount(table);
        // the border style from one cell that is used at the table
        let defaultBorderStyle = null;
        // the table drawing node (in OX Presentation)
        let tableDrawingNode = null;

        // setting default values
        _.each(BORDER_PROPS, prop => { tableBorderSet[prop] = true; });

        _.each(cellBorders, cellBorder => {

            const rowRange = cellBorder.rowInterval;
            const colRange = cellBorder.colInterval;

            const isFirstRow = rowRange && (rowRange.start === 0);
            const isLastRow = rowRange && (rowRange.end === rowCount - 1);
            const isFirstCol = colRange && (colRange.start === 0);
            const isLastCol = colRange && (colRange.end === colCount - 1);

            if (isFirstRow && cellBorder.attrs.borderTop.style === 'none') { tableBorderSet.borderTop = false; }
            if (isLastRow && cellBorder.attrs.borderBottom.style === 'none') { tableBorderSet.borderBottom = false; }
            if (isFirstCol && cellBorder.attrs.borderLeft.style === 'none') { tableBorderSet.borderLeft = false; }
            if (isLastCol && cellBorder.attrs.borderRight.style === 'none') { tableBorderSet.borderRight = false; }

            if (!isFirstRow && cellBorder.attrs.borderTop.style === 'none') { tableBorderSet.borderInsideHor = false; }
            if (!isLastRow && cellBorder.attrs.borderBottom.style === 'none') { tableBorderSet.borderInsideHor = false; }

            if (!isFirstCol && cellBorder.attrs.borderLeft.style === 'none') { tableBorderSet.borderInsideVert = false; }
            if (!isLastCol && cellBorder.attrs.borderRight.style === 'none') { tableBorderSet.borderInsideVert = false; }

            // finding a valid border style
            if (!defaultBorderStyle) {
                defaultBorderStyle = this.#findValidBorderStyle(cellBorder.attrs);
            }
        });

        for (const border in tableBorderSet) {
            if (tableBorderSet[border]) {
                tableBorderSet[border] = defaultBorderStyle;
            } else {
                tableBorderSet[border] = NO_BORDER;
            }
        }

        // -> setting simulated border attributes to odt tables -> only evaluated by the table border style picker
        this.setElementAttributes(table, { table: tableBorderSet });

        // in OX Presentation the table attributes must be assigned to the table drawing
        if (this.docApp.isPresentationApp()) {
            tableDrawingNode = table.closest(DRAWING_NODE_SELECTOR);
            this.setElementAttributes(tableDrawingNode, { table: tableBorderSet });
        }

        // saving one selected border style for odt files to be able to restore borders at tables with 'simulated' styles
        if (this.docApp.isTextApp() && this.docApp.isODF() && defaultBorderStyle) {
            table.data('odtBorderStyle', JSON.stringify(defaultBorderStyle));
        }

        table.data('odtBorderSet', 1); // only do this once for every table in ODT (after loading and after inserting)
    }

    /**
     * Searching for one used border style assigned to a cell, that can be used as border style for the table border settings.
     *
     * @param {Object} cellAttrs
     *  The set of cell attributes assigned to a table cell.
     *
     * @returns {Object|Null}
     *  A valid border attribute. If this cannot be found, null is returned.
     */
    #findValidBorderStyle(cellAttrs) {

        let borderAttribute = null;

        _.each(BORDER_PROPS, prop => {
            if (!borderAttribute && cellAttrs[prop] && cellAttrs[prop].style && cellAttrs[prop].style !== 'none') { borderAttribute = cellAttrs[prop]; }
        });

        return borderAttribute;
    }

    // static methods ---------------------------------------------------------

    static getNormalTable() {
        return {
            wholeTable: {
                table: {
                    paddingLeft: 190,
                    paddingTop: 0,
                    paddingRight: 190,
                    paddingBottom: 0
                }
            }
        };
    }

    static getTableGrid(themeModel) {
        return {
            wholeTable: {
                table: {
                    borderLeft: { style: 'single', color: TableStyles.addFallbackValueToColor(Color.TEXT1, themeModel, 'line'), width: 18 },
                    borderTop: { style: 'single', color: TableStyles.addFallbackValueToColor(Color.TEXT1, themeModel, 'line'), width: 18 },
                    borderRight: { style: 'single', color: TableStyles.addFallbackValueToColor(Color.TEXT1, themeModel, 'line'), width: 18 },
                    borderBottom: { style: 'single', color: TableStyles.addFallbackValueToColor(Color.TEXT1, themeModel, 'line'), width: 18 },
                    borderInsideHor: { style: 'single', color: TableStyles.addFallbackValueToColor(Color.TEXT1, themeModel, 'line'), width: 18 },
                    borderInsideVert: { style: 'single', color: TableStyles.addFallbackValueToColor(Color.TEXT1, themeModel, 'line'), width: 18 },
                    paddingLeft: 190,
                    paddingTop: 0,
                    paddingRight: 190,
                    paddingBottom: 0
                },
                paragraph: {
                    lineHeight: { type: 'percent', value: 100 },
                    marginBottom: 0
                }
            }
        };
    }

    /**
     * Calculates unique border width from all border attributes and returns it.
     *
     * @param {Object} attributes
     * @param {Boolean|Undefined} [skipBorderInside]
     *  Presentation doesn't use borderInsideHor and borderInsideVert, skip them.
     *
     * @returns {Number}
     */
    static getBorderStyleFromAttributes(attributes, skipBorderInside) {

        let allWidths = [];
        let width = 'none';

        if (attributes) {
            if (isVisibleBorder(attributes.borderLeft)) { allWidths.push(attributes.borderLeft.width); }
            if (isVisibleBorder(attributes.borderRight)) { allWidths.push(attributes.borderRight.width); }
            if (isVisibleBorder(attributes.borderTop)) { allWidths.push(attributes.borderTop.width); }
            if (isVisibleBorder(attributes.borderBottom)) { allWidths.push(attributes.borderBottom.width); }
            if (!skipBorderInside && isVisibleBorder(attributes.borderInsideHor)) { allWidths.push(attributes.borderInsideHor.width); }
            if (!skipBorderInside && isVisibleBorder(attributes.borderInsideVert)) { allWidths.push(attributes.borderInsideVert.width); }
        }

        allWidths = _.uniq(allWidths);

        if (allWidths.length === 1) {
            width = allWidths[0];
            width = convertHmmToLength(width, 'pt', 0.1);  // converting from 1/100 mm to pt
        }

        return width;
    }

    static getAttributesFromBorderStyle(borderWidth, attributes) {

        const borderLeft = _.clone(attributes.borderLeft);
        const borderRight = _.clone(attributes.borderRight);
        const borderTop = _.clone(attributes.borderTop);
        const borderBottom = _.clone(attributes.borderBottom);
        const borderInsideHor = _.clone(attributes.borderInsideHor);
        const borderInsideVert = _.clone(attributes.borderInsideVert);

        // converting from pt to 1/100 mm
        borderWidth = convertLengthToHmm(borderWidth, 'pt');

        borderLeft.width = borderWidth;
        borderRight.width = borderWidth;
        borderTop.width = borderWidth;
        borderBottom.width = borderWidth;
        borderInsideHor.width = borderWidth;
        borderInsideVert.width = borderWidth;

        return {
            borderLeft,
            borderRight,
            borderTop,
            borderBottom,
            borderInsideHor,
            borderInsideVert
        };
    }

    // Light Shading table style attributes

    static getLightShadingTableStyleAttributes(colorId, themeModel) {

        const SINGLE_BORDER_LINE = { style: 'single', color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line'), width: 35 };
        const BAND_COLOR = TableStyles.addFallbackValueToColor(opSchemeColor(colorId, { lumMod: 20000 }, { lumOff: 80000 }), themeModel);
        const ROW_CELL_ATTRIBUTES = { borderLeft: NONE, borderTop: SINGLE_BORDER_LINE, borderRight: NONE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE };
        const BAND_CELL_ATTRIBUTES = { borderLeft: NONE, borderRight: NONE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: BAND_COLOR };

        return {
            wholeTable: {
                table: { borderTop: SINGLE_BORDER_LINE, borderBottom: SINGLE_BORDER_LINE },
                paragraph: { lineHeight: LineHeight.SINGLE, marginBottom: 0 },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId, { shade: 74902 }), themeModel, 'text') }
            },
            firstRow: { cell: ROW_CELL_ATTRIBUTES, paragraph: { lineHeight: LineHeight.SINGLE, marginTop: 0, marginBottom: 0 }, character: { bold: true } },
            lastRow: { cell: ROW_CELL_ATTRIBUTES, paragraph: { lineHeight: LineHeight.SINGLE, marginTop: 0, marginBottom: 0 }, character: { bold: true } },
            firstCol: { character: { bold: true } },
            lastCol: { character: { bold: true } },
            band1Vert: { cell: BAND_CELL_ATTRIBUTES },
            band1Hor: { cell: BAND_CELL_ATTRIBUTES }
        };
    }

    // Medium Shading 1 table style attributes

    static getMediumShading1TableStyleAttributes(colorId, themeModel) {

        const SCHEME_COLOR = TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line');
        const BAND_COLOR = TableStyles.addFallbackValueToColor(opSchemeColor(colorId, { lumMod: 20000 }, { lumOff: 80000 }), themeModel);
        const SINGLE_BORDER_LINE = { style: 'single', color: SCHEME_COLOR, width: 35 };
        const DOUBLE_BORDER_LINE = { style: 'double', color: SCHEME_COLOR, width: 26 };

        return {
            wholeTable: {
                table: { borderLeft: SINGLE_BORDER_LINE, borderTop: SINGLE_BORDER_LINE, borderRight: SINGLE_BORDER_LINE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: SINGLE_BORDER_LINE },
                paragraph: { lineHeight: LineHeight.SINGLE, marginBottom: 0 }
            },
            firstRow: {
                cell: { borderLeft: SINGLE_BORDER_LINE, borderTop: SINGLE_BORDER_LINE, borderRight: SINGLE_BORDER_LINE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: SCHEME_COLOR },
                paragraph: { lineHeight: LineHeight.SINGLE, marginTop: 0, marginBottom: 0 },
                character: { bold: true, color: TableStyles.addFallbackValueToColor(Color.BACK1, themeModel, 'text') }
            },
            lastRow: {
                cell: { borderLeft: SINGLE_BORDER_LINE, borderTop: DOUBLE_BORDER_LINE, borderRight: SINGLE_BORDER_LINE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE },
                paragraph: { lineHeight: LineHeight.SINGLE, marginTop: 0, marginBottom: 0 },
                character: { bold: true }
            },
            firstCol: { character: { bold: true } },
            lastCol: { character: { bold: true } },
            band1Vert: { cell: { fillColor: BAND_COLOR } },
            band1Hor: { cell: { borderInsideHor: NONE, borderInsideVert: NONE, fillColor: BAND_COLOR } },
            band2Hor: { cell: { borderInsideHor: NONE, borderInsideVert: NONE } }
        };
    }

    // Medium Shading 2 table style attributes

    static getMediumShading2TableStyleAttributes(colorId, themeModel) {

        const SCHEME_COLOR = TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel);
        const BACK_COLOR = TableStyles.addFallbackValueToColor(Color.BACK1, themeModel, 'text');
        const BAND_COLOR = TableStyles.addFallbackValueToColor(opSchemeColor('background1', { lumMod: 95000 }), themeModel);
        const SINGLE_BORDER_LINE = { style: 'single', color: Color.AUTO, width: 79 };
        const DOUBLE_BORDER_LINE = { style: 'double', color: Color.AUTO, width: 26 };
        const NORTH_CELL_ATTRIBUTES = { borderLeft: NONE, borderTop: SINGLE_BORDER_LINE, borderRight: NONE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE };

        return {
            wholeTable: {
                table: { borderTop: SINGLE_BORDER_LINE, borderBottom: SINGLE_BORDER_LINE },
                paragraph: { lineHeight: LineHeight.SINGLE, marginBottom: 0 }
            },
            firstRow: {
                cell: { borderLeft: NONE, borderTop: SINGLE_BORDER_LINE, borderRight: NONE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: SCHEME_COLOR },
                paragraph: { lineHeight: LineHeight.SINGLE, marginTop: 0, marginBottom: 0 },
                character: { bold: true, color: BACK_COLOR }
            },
            lastRow: {
                cell: { borderLeft: NONE, borderTop: DOUBLE_BORDER_LINE, borderRight: NONE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: BACK_COLOR },
                paragraph: { lineHeight: LineHeight.SINGLE, marginTop: 0, marginBottom: 0 },
                character: { color: Color.AUTO }
            },
            firstCol: {
                cell: { borderLeft: NONE, borderTop: NONE, borderRight: NONE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: SCHEME_COLOR },
                character: { bold: true, color: BACK_COLOR }
            },
            lastCol: {
                cell: { borderLeft: NONE, borderRight: NONE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: SCHEME_COLOR },
                character: { bold: true, color: BACK_COLOR }
            },
            band1Vert: { cell: { borderLeft: NONE, borderRight: NONE, borderInsideHor: NONE, borderInsideVert: NONE, fillColor: BAND_COLOR } },
            band1Hor: { cell: { fillColor: BAND_COLOR } },
            northEastCell: { cell: NORTH_CELL_ATTRIBUTES },
            northWestCell: { cell: NORTH_CELL_ATTRIBUTES, character: { color: BACK_COLOR } }
        };
    }

    // Medium Grid 1 table style attributes

    static getMediumGrid1TableStyleAttributes(colorId, themeModel) {

        const BAND_COLOR = TableStyles.addFallbackValueToColor(opSchemeColor(colorId, { lumMod: 40000 }, { lumOff: 60000 }), themeModel, 'line');
        const SINGLE_BORDER_LINE = { style: 'single', color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line'), width: 35 };
        const THICK_BORDER_LINE = { style: 'single', color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line'), width: 79 };

        return {
            wholeTable: {
                table: { borderLeft: SINGLE_BORDER_LINE, borderTop: SINGLE_BORDER_LINE, borderRight: SINGLE_BORDER_LINE, borderBottom: SINGLE_BORDER_LINE, borderInsideHor: SINGLE_BORDER_LINE, borderInsideVert: SINGLE_BORDER_LINE },
                cell: { fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId, { lumMod: 20000 }, { lumOff: 80000 }), themeModel) },
                paragraph: { lineHeight: LineHeight.SINGLE, marginBottom: 0 }
            },
            firstRow: { character: { bold: true } },
            lastRow: { cell: { borderTop: THICK_BORDER_LINE }, character: { bold: true } },
            firstCol: { character: { bold: true } },
            lastCol: { character: { bold: true } },
            band1Vert: { cell: { fillColor: BAND_COLOR } },
            band1Hor: { cell: { fillColor: BAND_COLOR } }
        };
    }

    // Light Style 3 table style attributes (Presentation)
    static getLightStyle3TableStyleAttributesPresentation(colorId1, colorId2, isODF, themeModel) {
        const tfm = isODF ? 'tint' : 'alpha'; // bug 52757: ODP: using 'tint' instead of 'alpha' for fallback value
        return {
            wholeTable: {
                cell: {
                    borderLeft: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderRight: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderInsideHor: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderInsideVert: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') }
                },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text') }
            },
            band1Hor: { cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { [tfm]: 20000 }), themeModel) } },
            band1Vert: { cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { [tfm]: 20000 }), themeModel) } },
            firstRow: {
                cell: { borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') } },
                character: { bold: true }
            },
            lastRow: {
                cell: { borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') } },
                character: { bold: true }
            }
        };
    }

    // Medium Style 2 table style attributes (Presentation)
    static getMediumStyle2TableStyleAttributesPresentation(colorId1, colorId2, colorId3, themeModel) { // 'accent1', 'light1', 'dark1'
        return {
            wholeTable: {
                cell: {
                    fillType: 'solid',
                    fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 20000 }), themeModel),
                    borderLeft: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') },
                    borderRight: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') },
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') },
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') },
                    borderInsideHor: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') },
                    borderInsideVert: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') }
                },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3), themeModel, 'text') }
            },
            band1Hor: { cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 40000 }), themeModel) } },
            band1Vert: { cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 40000 }), themeModel) } },
            firstCol: {
                cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel) },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'), bold: true }
            },
            firstRow: {
                cell: {
                    fillType: 'solid',
                    fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel),
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'line') }
                },
                character: {
                    color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'),
                    bold: true
                }
            },
            lastCol: {
                cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel) },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'), bold: true }
            },
            lastRow: {
                cell: {
                    fillType: 'solid',
                    fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel),
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'fill') }
                },
                character: {
                    color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'),
                    bold: true
                }
            }
        };
    }

    // Medium Style 3 table style attributes (Presentation)
    static getMediumStyle3TableStyleAttributesPresentation(colorId1, colorId2, colorId3, themeModel) { // 'accent1', 'light1', 'dark1'
        return {
            wholeTable: {
                cell: {
                    fillType: 'solid',
                    fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel),
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3), themeModel, 'line') },
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3), themeModel, 'line') }
                },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3), themeModel, 'text') }
            },
            band1Hor: { cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3, { tint: 20000 }), themeModel) } },
            band1Vert: { cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3, { tint: 20000 }), themeModel) } },
            firstCol: {
                cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel) },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'), bold: true }
            },
            firstRow: {
                cell: {
                    fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel),
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3), themeModel, 'line') }
                },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'), bold: true }
            },
            lastCol: {
                cell: { fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel) },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text'), bold: true }
            },
            lastRow: {
                cell: {
                    fillType: 'solid', fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel),
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId3), themeModel, 'line') }
                }
            }
            // southEastCell:{
            //     character: { color: opSchemeColor(colorId3) }
            // },
            // southWestCell:{
            //     character: { color: opSchemeColor(colorId3) }
            // }
        };
    }

    // Medium Style 4 table style attributes (Presentation)
    static getMediumStyle4TableStyleAttributesPresentation(colorId1, colorId2, themeModel) {
        return {
            wholeTable: {
                cell: {
                    fillType: 'solid',
                    fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 20000 }), themeModel),
                    borderLeft: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderRight: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderInsideHor: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') },
                    borderInsideVert: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') }
                },
                character: { color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId2), themeModel, 'text') }
            },
            band1Hor: { cell: { fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 40000 }), themeModel) } },
            band1Vert: { cell: { fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 40000 }), themeModel) } },
            firstRow: {
                cell: { fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 20000 }), themeModel) },
                character: { bold: true }
            },
            lastRow: {
                cell: {
                    fillColor: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1, { tint: 20000 }), themeModel),
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId1), themeModel, 'line') }
                },
                character: { bold: true }
            }
        };
    }

    // Table grid, no style (but not setting fillType to 'none' (55588))
    static getTableGridPresentation(colorId, themeModel) {
        return {
            wholeTable: {
                cell: {
                    borderLeft: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line') },
                    borderRight: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line') },
                    borderBottom: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line') },
                    borderTop: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line') },
                    borderInsideHor: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line') },
                    borderInsideVert: { style: 'single', width: 35, color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'line') }
                }
            }
        };
    }

    // No grid, no style
    static getTableNoGridNoStylePresentation(colorId, themeModel) {
        return {
            wholeTable: {
                cell: {
                    borderLeft: { style: 'none' },
                    borderRight: { style: 'none' },
                    borderBottom: { style: 'none' },
                    borderTop: { style: 'none' },
                    borderInsideHor: { style: 'none' },
                    borderInsideVert: { style: 'none' }
                },
                character: { fontName: 'Calibri Light', color: TableStyles.addFallbackValueToColor(opSchemeColor(colorId), themeModel, 'text'), bold: false }
            }
        };
    }

    /**
     * Takes a theme color and adds a corresponding fallback color value.
     * Note: directly manipulates the json color object.
     *
     * @param {Object} jsonColor
     *  The JSON representation of the color to be extended, as used in
     *  document operations.
     *
     * @param {ThemeModel} themeModel
     *  The model of the theme used to map scheme color names to color values.
     *
     * @param {String|Color} [auto='fill']
     *  Additional information needed to resolve the automatic color. See
     *  method Color.resolve() for details about this parameter.
     *
     * @returns {Object}
     *  The color extended with the fallback value.
     */
    static addFallbackValueToColor(jsonColor, themeModel, auto) {

        const color = Color.parseJSON(jsonColor);
        const autoType = auto || 'fill';
        const resolvedColor = color.resolve(autoType, themeModel);

        // do not modify passed color, may be preset constants (e.g. `Color.TEXT1`)
        return { ...jsonColor, fallbackValue: resolvedColor.hex };
    }

    /**
     * Getter for the collection of border properties.
     *
     * @returns {String[]}
     *  The collection of border properties.
     */
    static getBorderProps() {
        return BORDER_PROPS;
    }

}

// exports ================================================================

export default TableStyles;
