/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { is } from "@/io.ox/office/tk/algorithms";
import { convertHmmToCssLength, convertHmmToLength, parseCssLength } from "@/io.ox/office/tk/dom";

import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import type { ParagraphAttributeSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { AttributeConfigBag } from "@/io.ox/office/editframework/model/attributefamilypool";

import { TABLE_REPEAT_ROWNODE_CLASSNAME } from "@/io.ox/office/textframework/utils/dom";
import type { TextBaseAttributePoolMap } from "@/io.ox/office/textframework/model/editor";
import type TextBaseModel from "@/io.ox/office/textframework/model/editor";

// types ======================================================================

export interface TableRowAttributes {

    /**
     * The height of the table row. If 'auto' then the row is auto sized.
     */
    height: number | string;

    /**
     * Denotes row to be repeated across pages of split table (repeated rows).
     */
    headerRow: boolean;
}

export interface TableRowAttributeSet extends ParagraphAttributeSet {
    row: TableRowAttributes;
}

// constants ==================================================================

// definitions for table row attributes
const ROW_ATTRIBUTES: AttributeConfigBag<TableRowAttributes> = {
    height:    { def: "auto", parse: parseRowHeight },
    headerRow: { def: false }
};

// private functions ==========================================================

function parseRowHeight(value: unknown): string | number | undefined {
    return (is.number(value) || is.string(value)) ? value : undefined;
}

// class TableRowStyles =======================================================

/**
 * Contains the style sheets for table row formatting attributes. The CSS
 * formatting will be read from and written to `<tr>` elements.
 *
 * @param docModel
 *  The text document model containing instance.
 */
class TableRowStyleCollection<DocModelT extends TextBaseModel> extends StyleCollection<DocModelT, TextBaseAttributePoolMap, TableRowAttributeSet> {

    constructor(docModel: DocModelT) {

        // base constructor
        super(docModel, "row", {
            families: ["changes"],
            styleSheetSupport: false
        });

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily("row", ROW_ATTRIBUTES);

        // register the formatting handler for DOM elements
        this.registerFormatHandler(this.#updateTableRowFormatting);
    }

    // private methods ----------------------------------------------------

    /**
     * Will be called for every table row element whose attributes have been
     * changed. Repositions and reformats the table row according to the passed
     * attributes.
     *
     * @param row
     *  The `<tr>` element whose table row attributes have been changed,
     *  as jQuery object.
     *
     * @param mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute family,
     *  containing the effective attribute values merged from style sheets and
     *  explicit attributes.
     */
    #updateTableRowFormatting(row: JQuery, mergedAttributes: TableRowAttributeSet): void {

        // the row attributes of the passed attribute map
        const rowAttributes = mergedAttributes.row;
        let rowHeight: number;
        let cellHeight = null;
        let paddingHeight = 0;

        if (rowAttributes.height !== "auto") { // rowAttributes.height is a number

            if (_.browser.WebKit || _.browser.IE) {

                // Chrome requires row height at the cells, setting height at <tr> is ignored.
                rowHeight = convertHmmToLength(rowAttributes.height as number, "px", 1);
                row.children("td").get().forEach(rowNode => {

                    if (this.docModel.useSlideMode()) {
                        paddingHeight = 0; // this is necessary for precision of vertical resize of table drawings
                    } else {
                        paddingHeight = parseCssLength(rowNode, "paddingTop") + parseCssLength(rowNode, "paddingBottom") + parseCssLength(rowNode, "borderBottomWidth");
                    }

                    cellHeight = `${rowHeight - paddingHeight}px`;
                    $(rowNode).css("height", cellHeight);
                });
            } else {
                // FireFox requires row height at the rows. Setting height at cells, makes
                // height of cells unchanged, even if text leaves the cell.
                row.css("height", convertHmmToCssLength(rowAttributes.height as number, "px", 1));
            }
        } else if (_.browser.WebKit) {
            // Chrome requires row height at the cells.
            row.children("td").css("height", 0);
        } else if (_.browser.IE) {
            row.children("td").css("height", "");
        } else {
            // FireFox requires row height at the rows.
            row.css("height", "");
        }

        if (rowAttributes.headerRow) { row.addClass(TABLE_REPEAT_ROWNODE_CLASSNAME); }

        // change track attribute handling
        this.docModel.getChangeTrack().updateChangeTrackAttributes(row, mergedAttributes as unknown as Dict); // TODO
    }
}

// exports ================================================================

export default TableRowStyleCollection;
