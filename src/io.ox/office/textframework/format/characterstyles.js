/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { math } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { SMALL_DEVICE, convertHmmToLength, convertLength, convertToEM } from '@/io.ox/office/tk/utils';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';
import { MINOR_FONT_KEY, getCssTextDecoration, isCharacterFontThemed, isColorThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import { PARAGRAPH_NODE_SELECTOR, TABLE_CELLNODE_SELECTOR, TEXTFRAMECONTENT_NODE_SELECTOR, isHyperlinkNode, isSpecialField } from '@/io.ox/office/textframework/utils/dom';
import { getParagraphElement } from '@/io.ox/office/textframework/utils/position';

// constants ==============================================================

// definitions for character attributes
const DEFINITIONS = {

    fontName: {
        def: MINOR_FONT_KEY
    },

    fontNameSymbol: {
        def: ''
    },

    fontNameComplex: {
        def: ''
    },

    fontNameEastAsia: {
        def: ''
    },

    bold: {
        def: false
    },

    italic: {
        def: false
    },

    underline: {
        def: false
    },

    strike: {
        def: 'none'
    },

    caps: {
        def: 'none'
    },

    // baseline for Text
    vertAlign: {
        def: 'baseline'
    },

    // baseline for Presentation
    baseline: {
        def: 0
    },

    color: {
        def: Color.AUTO
    },

    fillColor: {
        def: Color.AUTO
    },

    language: {
        def: ''
    },

    url: {
        def: '',
        scope: 'element'
    },

    anchor: {
        def: '',
        scope: 'element'
    },

    autoDateField: {
        def: '',
        scope: 'element'
    },

    field: {
        def: '', // #45332, new odf filter sends field properties as character attribute
        scope: 'element'
    },

    // runtime-only attributes

    spellerror: {
        def: false,
        scope: 'runtime'
    },

    highlight: {
        def: false,
        scope: 'runtime'
    }
};

// definitions of character attributes specific for slide documents
const SLIDE_DEFINITIONS = {
    fontSize: {
        def: 18
    }
};

// definitions of character attributes specific for text documents
const TEXT_DEFINITIONS = {
    fontSize: {
        def: 11
    }
};

const PARENT_RESOLVERS = {
    paragraph: span => { return span.closest(PARAGRAPH_NODE_SELECTOR); }
};

// class CharacterStyles ==================================================

/**
 * Contains the style sheets for character formatting attributes. The CSS
 * formatting will be written to text span elements contained somewhere in
 * the paragraph elements.
 *
 * @param {TextBaseModel} docModel
 *  The document model containing this instance.
 *
 * @param {Object} [initoptions]
 *  Optional parameters passed to the base class constructor.
 */
class CharacterStyles extends StyleCollection {

    constructor(docModel, initOptions) {

        // Setting option to ignore character attributes at all ancestor nodes in OX Text for OOXML (DOCS-4211, -> this is evaluated only for filterVersion >= 2)
        const ignoreFamilyAtParents = (docModel.docApp.isTextApp() && docModel.docApp.isOOXML()) ? { ignoreFamilyAtParents: 'character' } : null;

        // base constructor
        super(docModel, 'character', _.extend({}, initOptions, { parentResolvers: PARENT_RESOLVERS }, ignoreFamilyAtParents));

        // initialization -----------------------------------------------------

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('character', DEFINITIONS);

        // app specific attributes (55728) (still avoiding subclass of CharacterStyles)
        if (this.docApp.isPresentationApp()) {
            this.attrPool.registerAttrFamily('character', SLIDE_DEFINITIONS);
        } else {
            this.attrPool.registerAttrFamily('character', TEXT_DEFINITIONS);
        }

        // register the formatting handlers for DOM elements
        this.registerFormatHandler(this.#updateCharacterFormatting);
        this.registerPreviewHandler(this.#updatePreviewFormatting);
    }

    // public methods -----------------------------------------------------

    /**
     * Returns the effective CSS font family for the passed character
     * attributes.
     *
     * @param {Object} charAttributes
     *  A complete map with character attributes supported by this
     *  instance.
     *
     * @param {String|Array<String>} [targets]
     *  A single target name or an array of target names to specify the
     *  theme's target.
     *
     * @returns {String}
     *  The effective CSS font family for the passed character attributes.
     */
    resolveCssFontFamily(charAttributes, targets) {
        let fontFamily = this.docModel.getCssFontFamily(charAttributes.fontName, targets);
        if (charAttributes.fontNameSymbol) {
            if (fontFamily) { fontFamily += ','; }
            fontFamily += this.docModel.getCssFontFamily(charAttributes.fontNameSymbol, targets);
        }
        return fontFamily;
    }

    /**
     * Sets the text line height of the specified DOM element.
     *
     * @param {HTMLElement|jQuery} element
     *  The element whose line height will be changed. If this object is a
     *  jQuery collection, uses the first DOM node it contains.
     *
     * @param {Object} lineHeight
     *  The new line height value. Must contain the properties 'type' and
     *  'value'.
     *
     * @param {Partial<CharacterAttributes>} charAttributes
     *  Character formatting attributes influencing the normal line height.
     *
     * @param {String|Array<String>} [targets]
     *  A single target name or an array of target names to specify the
     *  theme's target.
     *
     */
    updateElementLineHeight(element, lineHeight, charAttributes, targets) {

        // effective line height in pixels (start with passed value, converted from 1/100 mm)
        let height = convertHmmToLength(lineHeight.value, 'px');
        // CSS unit string to be appended to the effective line height
        let unit = 'px';
        // value for the CSS attribute 'vertical-align'
        let vertAlign = null;

        // type 'fixed': use line height as passed, otherwise convert to percentage
        if (lineHeight.type !== 'fixed') {

            // workaround for bug 47960: get normal line height of enlarged font
            const normalLineHeight = this.docModel.getRenderFont(charAttributes, 10, targets).getNormalLineHeight() / 10;
            // workaround for Bug 47960 & Bug 45951
            const fontHeight = this.docModel.useSlideMode() ? normalLineHeight : convertLength(charAttributes.fontSize, 'pt', 'px');

            // calculate effective line height
            switch (lineHeight.type) {
                case 'leading':
                    height += normalLineHeight;
                    break;
                case 'atLeast':
                    height = Math.max(height, normalLineHeight);
                    break;
                case 'percent':
                    // 'lineHeight.value' is a value in percent
                    height = normalLineHeight * lineHeight.value / 100;

                    // -> handling of Bug 47960 & Bug 45951, lineHeight in PP is based on "baseline" instead of "fontsize" -> factor 1.15
                    // -> not using 'LineHeight._115' as default value, because a switch from '1.0' to '1.1' in the paragraph settings
                    //    of PP leads to operation with 110 % line height. This must increase the existing lineheight, and not decrease it,
                    //    as it would be, if the lineheight is changed from 115% (the default) to 110%. (61284)
                    if (this.docApp.isPresentationApp()) { height *= 1.15; }
                    break;
                case 'normal':
                    height = normalLineHeight;
                    break;
                default:
                    globalLogger.error('CharacterStyles.updateElementLineHeight(): invalid line height type');
            }

            // bug 47960: offset to pretend rounding error
            height =  Math.ceil(height) + 0.5;

            // convert to relative size in percent
            height = math.roundp(height / fontHeight * 100, 0.01);
            unit = '%';

            if (height < 100) {
                //workaround for Bug 48695
                vertAlign = '0.5em';
            }
        }

        $(element).first().css({
            lineHeight: height + unit,
            verticalAlign: vertAlign
        });
    }

    // private methods ----------------------------------------------------

    /**
     * Will be called for every text span whose character attributes have
     * been changed.
     *
     * @param {jQuery} textSpan
     *  The text span whose character attributes have been changed, as
     *  jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    #updateCharacterFormatting(textSpan, mergedAttributes) {

        if (isSpecialField(textSpan.parent())) { return; }

        // the parent paragraph of the node (may be a grandparent)
        let paragraph = textSpan.closest(PARAGRAPH_NODE_SELECTOR);

        // DOCS-4838: getting the paragraph at the current position for preview
        if (paragraph.length === 0 && textSpan.hasClass('preview-span')) {
            paragraph = getParagraphElement(this.docModel.getSelection().getRootNode(), _.initial(this.docModel.getSelection().getStartPosition()));
        }

        // the closest cell node (if it exists)
        const cell = textSpan.closest(TABLE_CELLNODE_SELECTOR);
        // the closest text frame content node (if it exists)
        const textframeContent = textSpan.closest(TEXTFRAMECONTENT_NODE_SELECTOR);
        // the full set of attributes at the paragraph
        const fullParaAttrs = this.docModel.paragraphStyles.getElementAttributes(paragraph);
        // the merged attributes of the paragraph
        const paraAttrs = fullParaAttrs.paragraph;
        // the character attributes of the passed attribute map
        const charAttrs = mergedAttributes.character;
        // effective text color, font size, and vertical offset
        let textColor = charAttrs.color;
        let fontSize = charAttrs.fontSize;
        let textDecoration = getCssTextDecoration(charAttrs);
        let position = 'static';
        let bottom = null;
        // the background color of the cell and text frame
        let cellBackgroundColor = null, textFrameBackgroundColor = null;
        // an array containing all affected fill colors of text, paragraph and cell
        const allFillColors = [];
        // the effective line height, that might be reduced inside comment
        const effectiveLineHeight = paraAttrs.lineHeight;
        // the target chain, that is required to resolve the correct theme
        let targets = null;

        // restriction of max font size for small devices (only for OX Text)
        if (SMALL_DEVICE && this.docApp.isTextApp() && fontSize > 16) {
            fontSize = 16;
        }

        let url = charAttrs.url;
        if (!_.isString(url) || (url.length === 0)) { url = null; }
        let anchor = charAttrs.anchor;
        if (!_.isString(anchor) || (anchor.length === 0)) { anchor = null; }

        // update formatting of the passed text span
        textSpan.attr({
            title: url,
            'data-hyperlink-url': url,
            'data-anchor-url': anchor,
            lang: charAttrs.language,
            'data-auto-date': charAttrs.autoDateField || null
        });

        textSpan.css({
            fontWeight: charAttrs.bold ? 'bold' : 'normal',
            fontStyle: charAttrs.italic ? 'italic' : 'normal',
            backgroundColor: this.docModel.getCssColor(charAttrs.fillColor, 'fill')
        });

        textSpan.toggleClass('spellerror', !!charAttrs.spellerror);
        textSpan.toggleClass('highlight', !!charAttrs.highlight);
        textSpan.toggleClass('uppercase', charAttrs.caps === 'all');
        textSpan.toggleClass('lowercase', charAttrs.caps === 'small');
        textSpan.toggleClass('hidehyperlinkinanchor', !!(anchor && this.docApp.isOOXML() && this.#hideHyperlinkInAnchor(mergedAttributes, fullParaAttrs))); // DOCS-3251

        // Checking also the background-color of an affected cell, if there is one (28988).
        // For performance reasons, not using attributes (might be assigned to row or table, too),
        // but checking CSS style 'background-color' of the table directly.
        if (cell.length > 0) {
            cellBackgroundColor = Color.parseCSS(cell.css('background-color'), true);
            if (cellBackgroundColor) { allFillColors.push(cellBackgroundColor.toJSON()); }
        }

        // not only checking table cells, but also text frames, for their background-color (36385)
        if (textframeContent.length > 0) {
            textFrameBackgroundColor = Color.parseJSON(textframeContent.css('background-color'), true);
            if (textFrameBackgroundColor) { allFillColors.push(textFrameBackgroundColor.toJSON()); }
        }

        // adding fill color of paragraph and character (order is important)
        allFillColors.push(paraAttrs.fillColor);
        allFillColors.push(charAttrs.fillColor);

        // calculate effective text color, according to fill colors
        // or theme if it's hyperlink, #45263
        if (isHyperlinkNode(textSpan) && this.docModel.useSlideMode()) {
            textColor = '#' + this.docModel.getThemeModelForNode(textSpan).getSchemeColor('hyperlink', '00f');
            textDecoration = 'underline';
        } else {
            if (isColorThemed(textColor)) { targets = this.docModel.getThemeTargets(paragraph); }
            textColor = this.docModel.getCssTextColor(textColor, allFillColors, targets);
        }

        // calculate font height and vertical alignment (according to escapement)
        if ((charAttrs.vertAlign === 'sub') || (charAttrs.baseline < 0)) {
            position = 'relative';
            bottom = '-0.25em';
            fontSize = math.roundp(fontSize * 0.66, 0.1);
        } else if ((charAttrs.vertAlign === 'super') || charAttrs.baseline > 0) {
            position = 'relative';
            bottom = '0.5em';
            fontSize = math.roundp(fontSize * 0.66, 0.1);
        }

        if (!targets && isCharacterFontThemed(charAttrs)) {
            targets = this.docModel.getThemeTargets(paragraph);
        }

        // update CSS of the text span
        textSpan.css({
            fontFamily: this.resolveCssFontFamily(charAttrs, targets),
            fontSize: convertToEM(fontSize, 'pt'),
            color: textColor,
            textDecoration,
            position,
            bottom
        });

        // update line height due to changed font settings
        this.updateElementLineHeight(textSpan, effectiveLineHeight, charAttrs, targets);

        // change track attribute handling
        this.docModel.getChangeTrack().updateChangeTrackAttributes(textSpan, mergedAttributes);

        // TODO: set bullet character formatting according to paragraph attributes
        // Bug 30794: We want have a consistent list-label formatting, therefore only the
        // code in updateList in editor should be responsible to set character formatting.
        // This should be extended as the user is currently only able to change the list-label
        // formatting via character attributes in paragraph styles.
        //$(paragraph).find('> ' + DOM.LIST_LABEL_NODE_SELECTOR + ' > span').css('font-size', characterAttributes.fontSize + 'pt');
    }

    /**
     * Will be called for every text span used as preview in the UI.
     *
     * @param {jQuery} textSpan
     *  The text span preview node, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A complete attribute set, containing the effective attribute values
     *  merged from style sheets and explicit attributes.
     */
    #updatePreviewFormatting(textSpan, mergedAttributes) {

        // the merged character attributes
        const charAttrs = mergedAttributes.character;
        // the resolved text color
        const colorDesc = this.docModel.resolveColor(Color.parseJSON(charAttrs.color), 'text');

        // update formatting of the passed text span
        textSpan.css({
            fontFamily: this.resolveCssFontFamily(charAttrs),
            fontSize: math.clamp(math.roundp(10 + (charAttrs.fontSize - 10) / 1.5, 0.1), 6, 22) + 'pt',
            fontWeight: charAttrs.bold ? 'bold' : 'normal',
            fontStyle: charAttrs.italic ? 'italic' : 'normal',
            color: (colorDesc.y > 0.95) ? 'black' : colorDesc.css // bug 40872: use black if color is too light
        });
    }

    /**
     * The underline of an anchor in a TOC is typically not visible, even if the anchor has the style "Hyperlink". In
     * this case the underline must be hidden (DOCS-3251).
     *
     * But if the paragraph that contains the anchor has a paragraph style, that explicitely sets underline to true,
     * the underline is visible (DOCS-3376).
     */
    #hideHyperlinkInAnchor(mergedAttributes, paraAttrs) {

        // hide the underline, if it is caused by the Hyperlink style at the span.
        let isUnderlineOfHyperlinkStyle = mergedAttributes.styleId && mergedAttributes.styleId.toLowerCase().indexOf('hyperlink') >= 0;

        // -> but do not hide the underline, if it is specified at a style at the paragraph style (DOCS-3376)
        let paraStyleAttrs = null;

        if (paraAttrs.styleId) {
            paraStyleAttrs = this.docModel.paragraphStyles.getStyleAttributeSet(paraAttrs.styleId);
            if (paraStyleAttrs?.character?.underline) { isUnderlineOfHyperlinkStyle = false; }
        }

        return isUnderlineOfHyperlinkStyle;
    }

}

// exports ================================================================

export default CharacterStyles;
