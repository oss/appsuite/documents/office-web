/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { OpColor } from "@/io.ox/office/editframework/utils/color";
import { OpBorder } from "@/io.ox/office/editframework/utils/border";
import { CharacterAttributes, ParagraphAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import BaseTableStyleCollection from "@/io.ox/office/editframework/model/tablestylecollection";
import { BaseAttributePoolMap } from "@/io.ox/office/editframework/model/editmodel";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";

// types ======================================================================

export interface TableAttributes {

    /**
     * Width of the table, as number in 1/100 of millimeters.
     */
    width: number | string;

    /**
     * Fill color of the table.
     */
    fillColor: OpColor;

    /**
     * Grid width of columns in relative units.
     */
    tableGrid: number[];

    /**
     * Array containing information, if conditional attributes will be used. As
     * default value, all styles will be used, so that this array can be empty.
     */
    exclude: string[];

    /**
     * Left border of the table (will be set in the table cells).
     */
    borderLeft: OpBorder;

    /**
     * Top border of the table (will be set in the table cells).
     */
    borderTop: OpBorder;

    /**
     * Right border of the table (will be set in the table cells).
     */
    borderRight: OpBorder;

    /**
     * Bottom border of the table (will be set in the table cells).
     */
    borderBottom: OpBorder;

    /**
     * Inner horizontal borders inside the table (will be set in the table
     * cells).
     */
    borderInsideHor: OpBorder;

    /**
     * Inner vertical borders inside the table (will be set in the table
     * cells).
     */
    borderInsideVert: OpBorder;

    /**
     * Left indent of the table (only OX Text)
     */
    indent: number;

    /**
     * Top padding of the whole table
     */
    paddingTop: number;

    /**
     * Bottom padding of the whole table
     */
    paddingBottom: number;

    /**
     * Left padding of the whole table
     */
    paddingLeft: number;

    /**
     * Right padding of the whole table
     */
    paddingRight: number;
}

export interface TableAttributeSet {
    table: TableAttributes;
    paragraph: ParagraphAttributes;
    character: CharacterAttributes;
}

// class TableStyleCollection =================================================

export default class TableStyleCollection<DocModelT extends TextBaseModel> extends BaseTableStyleCollection<DocModelT, BaseAttributePoolMap, TableAttributeSet> {
    public constructor(docModel: DocModelT);
}
