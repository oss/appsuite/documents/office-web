/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { ary } from '@/io.ox/office/tk/algorithms';
import { convertCssLengthToHmm, convertLengthToHmm } from '@/io.ox/office/tk/dom';

import { Color } from '@/io.ox/office/editframework/utils/color';
import { LineHeight } from '@/io.ox/office/editframework/utils/lineheight';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';
import { TAB_NODE_SELECTOR, ensureExistingTextNodeInTab, isTabNode } from '@/io.ox/office/textframework/utils/dom';

// constants ==============================================================

// definitions for paragraph attributes
const DEFINITIONS = {

    alignment: {
        def: 'left'
    },

    fillColor: {
        def: Color.AUTO // auto for paragraph fill resolves to 'transparent'
    },

    /**
     * Line height relative to font settings. The CSS attribute 'line-height'
     * must be set separately at every descendant text span because a relative
     * line height (e.g. 200%) would not be derived from the paragraph relatively
     * to the spans, but absolutely according to the paragraph's font size.
     *
     * Example: The paragraph has a font size of 12pt and a line-height of 200%,
     * resulting in 24pt. This value will be derived absolutely to a span with a
     * font size of 6pt, resulting in a relative line height of 24pt/6pt = 400%
     * instead of the expected 200%.
     */
    lineHeight: { def: LineHeight.SINGLE },

    tabStops: {
        def: [],
        merge: (tabStops1, tabStops2) => {
            // Merge tabStops2 into array tabStops1

            // map by position for faster access
            const tabStopMap = new Map/*<number, TabStop>*/();
            tabStops1.forEach(tabStop1 => tabStopMap.set(tabStop1.pos, tabStop1));

            // insert the tab stops in the second array
            tabStops2.forEach(tabStop2 => {
                if (tabStop2.value === 'clear') {
                    tabStopMap.delete(tabStop2.pos);
                } else {
                    tabStopMap.set(tabStop2.pos, tabStop2);
                }
            });

            // create and return a sorted array
            return ary.sortFrom(tabStopMap.values(), tabStop => tabStop.pos);
        }
    },

    indentFirstLine: { def: 0 },

    indentLeft: { def: 0 },

    indentRight: { def: 0 }
};

// non-breaking space character
const NBSP = '\xa0';

// maps fill character attribute values to fill characters
const TAB_FILL_CHARS = { dot: '.', hyphen: '-', underscore: '_', middleDot: '·' };

// class ParagraphStyles ==================================================

/**
 * Contains the style sheets for paragraph formatting attributes. The CSS
 * formatting will be read from and written to DOM paragraph elements. This
 * class is intended to be used as a base class for paragraph style
 * collections of the different applications
 *
 * @param {EditModel} docModel
 *  The document model containing this instance.
 */
class ParagraphStyles extends StyleCollection {

    // whether default tab stop size is specified by paragraph (false), or globally at document (true)
    #globalDefTabStop = false;

    constructor(docModel, initOptions) {

        // base constructor
        super(docModel, 'paragraph', initOptions);

        // initialization -----------------------------------------------------

        // deferred initialization
        this.docApp.onInit(() => {
            this.#globalDefTabStop = !this.attrPool.hasAttrEntry('paragraph', 'defaultTabSize');
        });

        // register the attribute definitions for the style family
        this.attrPool.registerAttrFamily('paragraph', DEFINITIONS);

        // register the formatting handlers for DOM elements
        this.registerFormatHandler(this.#updateParagraphFormatting);
        this.registerPreviewHandler(this.#updatePreviewFormatting);
    }

    // public methods -----------------------------------------------------

    /**
     * Updates all tabulators in the passed paragraph node. This method
     * uses both the default tab size and existing paragraph tab stop
     * definitions. Fill characters are also supported.
     *
     * @param {HTMLElement|jQuery} paragraph
     *  The paragraph node whose tabulator nodes will be updated. If this
     *  object is a jQuery collection, uses the first DOM node it contains.
     *
     * @returns {ParagraphStyles}
     *  A reference to this instance.
     */
    updateTabStops(paragraph) {
        this.implUpdateTabStops($(paragraph), this.getElementAttributes(paragraph));
        return this;
    }

    // protected methods --------------------------------------------------

    /**
     * Updates all tabulators in the passed paragraph node. This method
     * uses both the default tab size and existing paragraph tab stop
     * definitions. Fill characters are also supported.
     *
     * _Attention:_ Currently, only left&right aligned tabs are supported!
     *
     * @param {jQuery} paragraph
     *  The paragraph node whose tabulator nodes will be updated, as jQuery
     *  object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    implUpdateTabStops(paragraph, mergedAttributes) {

        // performance: check existence of tab elements first
        if (paragraph.find(TAB_NODE_SELECTOR).length === 0) { return; }

        // default tab stop width from document settings or paragraph settings
        const defaultTabStop = this.#globalDefTabStop ? this.docModel.globalConfig.defaultTabStop : mergedAttributes.paragraph.defaultTabSize;
        // paragraph tab stop definitions
        const paraTabStops = mergedAttributes.paragraph.tabStops;
        // effective left margin of the paragraph
        const marginLeft = convertCssLengthToHmm(paragraph.css('margin-left'), 'px');
        // end position of previous tabulator
        let prevTabEndPos = null;
        // first node of paragraph
        let firstNode = null;
        // top position of first node
        let topFirst = 0;
        // height of first node
        let heightFirst = 0;
        // leftmost position used a base to calculate tab raster
        let zeroLeft = 0;
        // first line indent
        const indentFirstLine = mergedAttributes.paragraph.indentFirstLine;
        // intersect function
        const intersect = (xs1, xe1, xs2, xe2) => { return ((xs1 <= xs2) && (xe1 >= xs2)) || ((xs2 <= xs1) && (xe2 >= xs1)); };
        // active paragraph tab stops
        let activeParaTabStops = null;
        // if paraTabStops has left and right oriented tabs, mixedTabs is set by the right-tab. if it is set it overwrites behavior of text-aligment and para-margin
        const mixedTab = this.#getMixedTab(paraTabStops);

        // zoom factor in floating point notation
        const zoomFactor = this.docApp.getView().getZoomFactor();

        if (paragraph.length) {
            // Calculate top position and height of first paragraph child. Our paragraph
            // have always at least one child element.
            firstNode = $(paragraph.get(0).firstChild);
            topFirst = firstNode.length ? convertLengthToHmm(firstNode.position().top, 'px') : 0;
            heightFirst = firstNode.length ? convertLengthToHmm(firstNode.height(), 'px') : 0;

            _.each(paragraph.children(TAB_NODE_SELECTOR), oneTabNode => {
                //reset all tab sizes, for a much cleaner new calculating
                //(for example changing text-alignment from justify to left)
                $(oneTabNode).css('width', 0);

                // the current tab node of the paragraph
                const tabNode = $(oneTabNode);
                // the span within the tab node containing the fill characters
                const tabSpan = $(oneTabNode.firstChild);
                // the position of the tab node
                const pos = tabNode.position();
                // the left bound position of the tab node in 1/100th mm
                let leftHMM = 0;
                // the top bound position of the tab node in 1/100th mm
                const topHMM = convertLengthToHmm(pos.top, 'px');
                // the top bound position of the first child node of the paragraph in 1/100th mm
                let topSpanHMM = 0;
                // the height of the first child node of the paragraph in 1/100th mm
                let heightSpanHMM = 0;
                // the calculated width of the tab stop
                let width = 0;
                // the fill character of the tab stop
                let fillChar = null;
                // the tab stop values
                let tabStop;
                // ignore the tab position and continue with the next one
                let ignore = false;
                // tab stop type
                let tabStopType = 'left';
                // indent first line tab stop position
                let indentFirstLineTabStopValue = 0;
                // first line tab stop
                let indentFirstLineTabStop = null;
                // insert index for virtual first line indent tab stop
                let indentFirstLineTabStopIndex = 0;

                const calcLeftTab = () => { return Math.max(0, tabStop.pos - (leftHMM % tabStop.pos)); };

                const calcRightTab = () => {
                    const paraParent = paragraph.parent();
                    // calculating the paragraph width -> using 'outerWidth' instead of 'width' (see document of task DOCS-3269)
                    const paraWidth = paraParent.hasClass('cellcontent') ? convertLengthToHmm(paraParent.outerWidth()) : (convertLengthToHmm(paragraph.outerWidth()) + marginLeft);
                    // smaller value wins (for example in tables, para width is smaller than tabstop width)
                    const effTabStopPos = Math.min(tabStop.pos, paraWidth);
                    return this.#calcRightAlignTabstop(effTabStopPos, tabNode, leftHMM);
                };

                const calcMiddleTab = () => { return (calcLeftTab() + calcRightTab()) / 2; };

                if (mergedAttributes.paragraph.alignment === 'center') {
                    // for a centered paragraph tab stops are calculated where the first character is treated as leftmost (0) position
                    zeroLeft = firstNode.length ? -(marginLeft + convertLengthToHmm(firstNode.position().left, 'px')) : -marginLeft;
                } else {
                    zeroLeft = marginLeft;
                }

                // calculate left bound position of the tab node in 1/100th mm, including zoom factor for browsers using transf.scale
                leftHMM = zeroLeft + convertLengthToHmm(pos.left / zoomFactor, 'px');

                if (prevTabEndPos) {
                    // Check to see if we are in a chain of tabs. Force to use the previous tab end position
                    // as start position. Some browsers provide imprecise positions therefore we use the correct
                    // previous position from the last calculation.

                    // Check previous node and that text is empty - this is a precondition for the chaining
                    // code.
                    if (tabNode.prev().length && tabNode.prev().text().length === 0) {
                        const checkPrevPrevNode = tabNode.prev().length ? tabNode.prev().prev() : null;
                        if (checkPrevPrevNode.length && isTabNode(checkPrevPrevNode) && prevTabEndPos.top === topHMM) {
                            leftHMM = prevTabEndPos.center || prevTabEndPos.right;
                        }
                    }
                }

                // Special case for first line indent. For the first line the negative indent
                // must be used as a tab stop position using the absolute value. We need to set
                // at least one character for the tabSpan to have a valid width otherwise we
                // won't get a correct height which is needed for the check.
                if (indentFirstLine < 0) {
                    indentFirstLineTabStopValue = Math.abs(indentFirstLine);
                    if (marginLeft > 0) {
                        // In case of a margin left, we need to add the value to the indentFirstLineTabStopValue! Bug 45030
                        indentFirstLineTabStopValue += marginLeft;
                    }
                    // Fix for 29265 and 30847: Removing empty text node in span with '.contents().remove().end()'.
                    // Otherwise there are two text nodes in span after '.text('abc')' in IE.
                    tabSpan.contents().remove().end().text(NBSP);
                    heightSpanHMM = convertLengthToHmm(tabSpan.height(), 'px');
                    topSpanHMM = convertLengthToHmm(tabSpan.position().top, 'px');
                    // checkout if the first line indent is active for this special tab stop
                    if (intersect(topFirst, topFirst + heightFirst, topSpanHMM, topSpanHMM + heightSpanHMM) &&
                        (leftHMM + 10) < indentFirstLineTabStopValue) {
                        width = Math.max(0, indentFirstLineTabStopValue - leftHMM);
                    }

                    if (width > 1) {
                        // find index for the first line indent position within the paragraph tab stop array
                        indentFirstLineTabStopIndex = _.findLastIndex(paraTabStops, tab => {
                            return indentFirstLineTabStopValue > tab.pos;
                        }) + 1;
                        // create a copy of the paragraph tab stops and add the first indent tab stop at
                        // the correct position to the active paragraph tab stop array
                        indentFirstLineTabStop = { value: 'left', pos: indentFirstLineTabStopValue, fillChar: NBSP, processed: false };
                        activeParaTabStops = paraTabStops.slice();
                        activeParaTabStops.splice(indentFirstLineTabStopIndex, 0, indentFirstLineTabStop);
                        // reset width - the tab stop position must be calculated using all tab stop positions
                        width = 0;
                    } else {
                        // reset text within span
                        tabSpan.contents().remove();
                        tabSpan[0].appendChild(document.createTextNode(''));
                        // use only paragraph tab stops
                        activeParaTabStops = paraTabStops;
                    }
                } else {
                    // reset text within span
                    tabSpan.contents().remove();
                    tabSpan[0].appendChild(document.createTextNode(''));
                    // use only paragraph tab stops
                    activeParaTabStops = paraTabStops;
                }

                // Paragraph tab stops. Only paragraph tab stop can have a fill character and
                // define a new alignment
                if (width <= 1) {
                    tabStop = _(activeParaTabStops).find(tab => {
                        return ((leftHMM + 10) < tab.pos) && !tab.processed;
                    });
                    if (tabStop) {

                        tabStopType = tabStop.value || 'left';

                        // calculate tab stop size based on tab stop properties
                        if (tabStopType === 'left') {
                            // left bound tab stop
                            width = calcLeftTab();
                        } else if (tabStopType === 'right') {
                            // right bound tab stop
                            width = calcRightTab();
                            // Ignore this tab stop if width is zero. Don't use the default
                            // tab stop which is only active to left bound tab stops!
                            ignore = (width === 0);

                            if (!ignore && _.indexOf(activeParaTabStops, tabStop) === activeParaTabStops.length - 1) {
                                if (!_.browser.IE) {
                                    paragraph.css('width', 'calc(100% + 5mm)');
                                }
                            }

                        } else if (tabStopType === 'center') {
                            // right bound tab stop combinded with left bound tab stop
                            width = calcMiddleTab();
                            // Ignore this tab stop if width is zero. Don't use the default
                            // tab stop which is only active to left bound tab stops!
                            ignore = (width === 0);
                        }

                        // insert fill character
                        if (width > 1) {
                            fillChar = _.isString(tabStop.fillChar) ? TAB_FILL_CHARS[tabStop.fillChar] : NBSP;
                            if (fillChar) {
                                ParagraphStyles.insertTabFillChar(tabSpan, fillChar, width);
                            }
                            // Set processed flag to prevent using the same tab again due to rounding errors
                            // resulting by the browser calculation.
                            tabStop.processed = true;
                        }
                    }
                }

                if (!ignore) {
                    // only process default tab stop if tab stop is not set to ignore
                    if (width <= 1) {
                        // tab size calculation based on default tab stop
                        width = Math.max(0, defaultTabStop - (leftHMM % defaultTabStop));
                        width = (width <= 10) ? defaultTabStop : width; // no 0 tab size allowed, check for <= 10 to prevent rounding errors
                        // reset possible fill character
                        ParagraphStyles.insertTabFillChar(tabSpan, NBSP, width);
                    }
                    prevTabEndPos = { right: leftHMM + width, top: topHMM };
                }
                // always set a width to the tab div (even an ignored tab div needs to set the size to zero)
                tabNode.css('width', (width / 100) + 'mm');
            });

            this.#calculateMarginForMixedTab(paragraph, mergedAttributes, mixedTab);

            // reset processed flag for tab stops again
            paraTabStops.forEach(tab => { tab.processed = false; });
        }
    }

    // private methods ----------------------------------------------------

    /**
     * Will be called for every paragraph whose character attributes have
     * been changed.
     *
     * @param {jQuery} paragraph
     *  The paragraph node whose attributes have been changed, as jQuery
     *  object.
     *
     * @param {Object} mergedAttributes
     *  A map of attribute maps (name/value pairs), keyed by attribute
     *  family, containing the effective attribute values merged from style
     *  sheets and explicit attributes.
     */
    #updateParagraphFormatting(paragraph, mergedAttributes) {

        // the merged paragraph attributes
        const paraAttrs = mergedAttributes.paragraph;

        paragraph.css({
            textAlign: paraAttrs.alignment,
            backgroundColor: this.docModel.getCssColor(paraAttrs.fillColor, 'fill')
        });
    }

    /**
     * Will be called for paragraphs used as preview elements in the GUI.
     *
     * @param {jQuery} paragraph
     *  The preview paragraph node, as jQuery object.
     *
     * @param {Object} mergedAttributes
     *  A complete attribute set, containing the effective attribute values
     *  merged from style sheets and explicit attributes.
     */
    #updatePreviewFormatting(paragraph, mergedAttributes) {

        // the merged paragraph attributes
        const paraAttrs = mergedAttributes.paragraph;

        // update formatting of the passed paragraph node
        paragraph.css('text-align', paraAttrs.alignment);

        // format the text spans contained in the paragraph element
        const { characterStyles } = this.docModel;
        _.each(paragraph.children(), paraNode => {
            characterStyles.updateElementFormatting(paraNode, { baseAttributes: mergedAttributes, preview: true });
        });
    }

    /**
     * Calculates the width of a right aligned tab element
     * depending on the position and the remaining size of
     * the following nodes.
     *
     * @param {Number} tabStopPos
     *  The right aligned tab stop.
     *
     * @param {jQuery} tabNode
     *  The right aligned tab node as jQuery.
     *
     * @param {Number} tabNodePosHMM
     *  The current position of the tab node in 1/100th mm.
     *
     * @returns {Number}
     *  The width of the right aligned tab stop. May be zero if the
     *  tab stop is not active.
     */
    #calcRightAlignTabstop(tabStopPos, tabNode, tabNodePosHMM) {

        const nextNodes = tabNode.nextAll();
        let node;
        let rightPos = tabNodePosHMM;
        let width = 0;
        let i = 0;
        const len = nextNodes.length;

        // Loop through all following nodes and sum up their width. Exceeding
        // the tab stop position or a new tab stops the iteration.
        for (i = 0; i < len && rightPos < tabStopPos; i++) {
            node = $(nextNodes.get(i));
            if (isTabNode(node)) {
                break;
            }
            rightPos += convertLengthToHmm(node.width(), 'px');
        }

        if (rightPos < tabStopPos) {
            // Active right aligned tab stop. Subtract at least 1/10mm
            // rounding error regarding position values from the browser.
            width = tabStopPos - rightPos - 10;
            width = Math.max(0, width);
        }

        return width;
    }

    #getMixedTab(paraTabStops) {
        const leftTab = _.find(paraTabStops, tab => {
            return !tab.value || tab.value === 'left';
        });
        const rightTab = _.find(paraTabStops, tab => {
            return tab.value === 'center' || tab.value === 'right';
        });
        if (leftTab && rightTab) {
            return rightTab;
        }
    }

    /**
     * right tab can lay right above from the border, this cannot be triggered by MS-WORD interface,
     * but it is a typical behavior for MS-"table of content"
     * so we calculate if right tab is outside of the margin, and if true,
     * we reduce the margin-right of the paragraph
     */
    #calculateMarginForMixedTab(paragraph, paraAttributes, mixedTab) {
        paragraph.css('margin-right', null);

        if (!mixedTab) {
            return;
        }

        const pageAttrs = this.attrPool.getDefaultValues('page');
        const maxParaWidth = pageAttrs.width - pageAttrs.marginLeft - pageAttrs.marginRight;
        const indentRight = paraAttributes.paragraph.indentRight;
        const currentParaWidth = mixedTab.pos + indentRight;

        if (currentParaWidth > maxParaWidth) {
            const newIndentRight = indentRight - (currentParaWidth - maxParaWidth);
            paragraph.css('margin-right', (newIndentRight / 100) + 'mm');
        }
    }

}

// static methods ---------------------------------------------------------

/**
 * Fills the passed text span with a sufficient number of the specified
 * fill character.
 *
 * @param {jQuery} spanNode
 *  The span node to be filled. The current CSS formatting of this node is
 *  used to calculate the number of fill characters needed.
 *
 * @param {String} fillChar
 *  The fill character.
 *
 * @param {Number} width
 *  The target width of the tabulator, in 1/100 of millimeters.
 */
ParagraphStyles.insertTabFillChar = (spanNode, fillChar, width) => {

    // multiplier for a better average calculation
    const multiplier = 15;
    // multiplier fill characters, used to calculate average character width
    const checkString = fillChar.repeat(multiplier);
    // average character width, in 1/100 mm
    const widthPx = Math.max(1, spanNode.contents().remove().end().text(checkString).width());
    const charWidth = Math.max(convertLengthToHmm(widthPx, 'px') / multiplier);
    // number of characters needed to fill the specified width
    const charCount = Math.floor(width / charWidth);
    // the fill character, repeated by the calculated number
    const fillString = (charCount > 0) ? fillChar.repeat(charCount) : NBSP;
    // a shortened string, if element is too wide
    let shortFillString = null;

    // insert the fill string into the element
    spanNode.contents().remove().end().text(fillString);
    if (!fillString) { ensureExistingTextNodeInTab(spanNode); }

    // shorten fill string by one character, if element is too wide (e.g. due to rounding errors)
    if ((fillString.length > 1) && (convertLengthToHmm(spanNode.width(), 'px') >= width)) {
        shortFillString = fillString.slice(0, -1);
        spanNode.contents().remove().end().text(shortFillString);
        if (!shortFillString) { ensureExistingTextNodeInTab(spanNode); }
    }

    // workaround for iOS & Safari mobile - needs a text node directly within
    // the tab div to enable an external keyboard to jump through a tab chains
    // with empty spans
    if (_.device('ios') && _.device('safari')) {
        if (!spanNode[0].nextSibling) {
            spanNode.after(document.createTextNode(NBSP));
        }
    }
};

// exports ================================================================

export default ParagraphStyles;
