/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { TOUCH_DEVICE } from "@/io.ox/office/tk/dom";
import { extendOptions } from "@/io.ox/office/tk/utils";

import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";

import { FONT_HEADER_LABEL, MORE_FONT_STYLES_LABEL,
    BOLD_BUTTON_OPTIONS, ITALIC_BUTTON_OPTIONS, UNDERLINE_BUTTON_OPTIONS, STRIKEOUT_BUTTON_OPTIONS,
    CLEAR_FORMAT_BUTTON_OPTIONS, FORMAT_PAINTER_LABEL, LIST_SETTINGS_LABEL
} from "@/io.ox/office/textframework/view/labels";
import { Button, CompoundButton,
    FontFamilyPicker, FontSizePicker, TextColorPicker, FillColorPicker,
    TextPositionGroup, BulletListStylePicker, NumberedListStylePicker,
    TextBoxAlignmentPicker, ParagraphSpacingMenu
} from "@/io.ox/office/textframework/view/controls";

// class FontFamilyToolBar ====================================================

/**
 * A tool bar with controls to select the font family, and the font size.
 *
 * The tool bar will contain the following controls:
 * - A font family picker (controller key "character/fontname").
 * - A font size picker (controller key "character/fontsize").
 *
 * @param {TextBaseView} docView
 *  The document view instance containing this tool bar.
 */
export class FontFamilyToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            shrinkToMenu: { icon: "bi:fonts", tooltip: FONT_HEADER_LABEL }
        });

        // create the controls of this tool bar
        this.addControl("character/fontname", new FontFamilyPicker(docView));
        this.addContainer();
        this.addControl("character/fontsize", new FontSizePicker(docView));
    }
}

// class FormatPainterToolBar =================================================

/**
 * A tool bar with a controls to copy the formatting of the selection.
 *
 * @param {TextframeworkView} docView
 *  The document view instance containing this tool bar.
 */
export class FormatPainterToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        var docModel = docView.docModel;

        this.listenTo(docModel.getSelection(), "update", function (e) {
            if (TOUCH_DEVICE || docModel.getSelection().isSlideSelection()) { return; }

            if (e && e.event && e.event.type === "mouseup" && docModel.isFormatPainterActive()) {
                docModel.handleFormatPainter(false);

            } else if (docModel.isFormatPainterActive() && docModel.getSelection().isSlideSelection()) {
                docModel.setFormatPainterState(false);
            }
        });

        this.listenTo(docModel.getSelection(), "change", function () {
            if (docModel.getSelection().isSlideSelection()) { return; }

            if (docModel.getSelection().isDrawingSelection()) {
                docModel.handleFormatPainter(false);

            } else if (docModel.isFormatPainterActive() && docModel.getSelection().isSlideSelection()) {
                docModel.setFormatPainterState(false);

            } else if (TOUCH_DEVICE && docModel.isFormatPainterActive()) {
                docModel.handleFormatPainter(false);
            }
        });

        // create the controls of this tool bar
        this.addControl("format/painter", new Button({ icon: "png:format-painter", tooltip: FORMAT_PAINTER_LABEL, toggle: true, dropDownVersion: { label: FORMAT_PAINTER_LABEL } }));
    }
}

// class FontStyleToolBar =====================================================

/**
 * A tool bar with controls to change different font style attributes.
 *
 * The tool bar will contain the following controls:
 *  - A bold button (controller key "character/bold").
 *  - An italic button (controller key "character/italic").
 *  - An underline button (controller key "character/underline").
 *  - A strikeout button (controller key "character/strike").
 *  - An escapement picker (controller key "character/vertalign").
 *  - A reset button (controller key "character/reset").
 *
 * @param {TextBaseView} docView
 *  The document view instance containing this tool bar.
 *
 * @param {object} [config]
 *  Configuration options:
 *  - {boolean} [config.hideTextPosition=false]
 *      If set to true, the text position group (subscript/superscript)
 *      will not be inserted.
 */
export class FontStyleToolBar extends ToolBar {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            ...config,
            shrinkToMenu: {
                itemKey: "character/bold",
                splitButton: true,
                caretTooltip: MORE_FONT_STYLES_LABEL,
                ...BOLD_BUTTON_OPTIONS
            }
        });

        // create the generic controls of this toolbar
        this.addControl("character/bold",      new Button(BOLD_BUTTON_OPTIONS));
        this.addControl("character/italic",    new Button(ITALIC_BUTTON_OPTIONS));
        this.addControl("character/underline", new Button(UNDERLINE_BUTTON_OPTIONS));

        // create a dropdown menu with advanced font style options (hidden in shrunken toolbar mode)
        const moreButton = this.addControl("view/character/format/menu", new CompoundButton(docView, {
            icon: "png:font-format",
            tooltip: MORE_FONT_STYLES_LABEL,
            dropDownVersion: { visible: false }
        }));
        this._addMoreControls(moreButton, { dropDownVersion: { tooltip: null } });

        // create the advanced controls for "shrunken toolbar" mode
        this._addMoreControls(this, { dropDownVersion: { visible: true } });
    }

    // private methods --------------------------------------------------------

    /*private*/ _addMoreControls(target, options) {

        target.addSection("strike");
        target.addControl("character/strike", new Button(extendOptions(STRIKEOUT_BUTTON_OPTIONS, options)));

        // TODO: bug 64725: "hideTextPosition" option can be removed if Spreadsheet supports vertical text position (subscript/superscript)
        if (!this.config.hideTextPosition) {
            target.addSection("vertalign");
            target.addControl("character/vertalign", new TextPositionGroup(options));
        }

        target.addSection("reset");
        target.addControl("character/reset", new Button(extendOptions(CLEAR_FORMAT_BUTTON_OPTIONS, options)));
    }
}

// class FontStyleToolBar =====================================================

//#. fill color behind single characters (instead of entire paragraph/cell)
const TEXT_HIGHLIGHT_COLOR_LABEL = gt("Text highlight color");

/**
 * A tool bar with controls to change the font color attributes.
 *
 * The tool bar will contain the following controls:
 *  (1) A text color button (controller key "character/color").
 *  (2) A text fill color button (controller key "character/fillcolor").
 *
 * @param {TextBaseView} docView
 *  The document view instance containing this tool bar.
 */
export class FontColorToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            shrinkToMenu: { icon: "png:color", tooltip: gt("Colors") }
        });

        // create the controls of this tool bar
        this.addControl("character/color",     new TextColorPicker(docView));
        this.addControl("character/fillcolor", new FillColorPicker(docView, { icon: "png:font-fill-color", label: null, tooltip: TEXT_HIGHLIGHT_COLOR_LABEL }), { visibleKey: "character/fillcolor" });
    }
}

// class ListStyleToolBar =====================================================

//#. indentation of lists (one list level up)
const DEC_LEVEL_LABEL = gt("Indent less");
//#. indentation of lists (one list level down)
const INC_LEVEL_LABEL = gt("Indent more");

/**
 * A tool bar with controls to change the settings of numbered lists, or bullet
 * lists.
 *
 * The tool bar will contain the following controls:
 * - A bullet list picker (controller key "paragraph/list/bullet").
 * - A numbered list picker (controller key "paragraph/list/numbered").
 * - A decrement level button (controller key "paragraph/list/decindent").
 * - An increment level button (controller key "paragraph/list/incindent").
 *
 * @param {TextBaseView} docView
 *  The document view instance containing this tool bar.
 */
export class ListStyleToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView, {
            shrinkToMenu: { icon: "png:lists", tooltip: LIST_SETTINGS_LABEL }
        });

        // create the controls of this tool bar
        this.addControl("paragraph/list/bullet", new BulletListStylePicker(docView));
        this.addContainer();
        this.addControl("paragraph/list/numbered", new NumberedListStylePicker(docView));
        this.addContainer();
        this.addControl("paragraph/list/decindent", new Button({ icon: "png:list-dec-level", tooltip: DEC_LEVEL_LABEL, skipStateAttr: false, dropDownVersion: { label: DEC_LEVEL_LABEL } }));
        this.addControl("paragraph/list/incindent", new Button({ icon: "png:list-inc-level", tooltip: INC_LEVEL_LABEL, skipStateAttr: false, dropDownVersion: { label: INC_LEVEL_LABEL } }));
    }
}

// class BoxAlignmentToolBar ==================================================

/**
 * A tool bar with controls to change the settings of numbered lists, or bullet
 * lists.
 *
 * The tool bar will contain the following controls:
 * - A picker for horizontal and vertical paragraph alignment.
 * - A picker for line height and paragraph spacing.
 *
 * @param {TextBaseView} docView
 *  The document view instance containing this tool bar.
 */
export class BoxAlignmentToolBar extends ToolBar {

    constructor(docView) {

        // base constructor
        super(docView);

        // create the controls of this tool bar
        this.addControl("view/drawing/alignment/menu", new TextBoxAlignmentPicker(docView));
        this.addControl("view/drawing/spacing/menu",   new ParagraphSpacingMenu(docView));
    }
}
