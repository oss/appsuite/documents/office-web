/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { RadioList } from "@/io.ox/office/tk/control/radiolist";

// class DrawingPositionPicker ================================================

/**
 * Dropdown menu control for position and text floating of drawing objects in
 * text documents.
 */
export class DrawingPositionPicker extends RadioList {

    constructor(config) {

        // base constructor
        super({
            icon: "png:drawing-inline",
            //#. alignment and text floating of drawing objects in text documents
            label: gt.pgettext("drawing-pos", "Position"),
            //#. alignment and text floating of drawing objects in text documents
            tooltip: gt.pgettext("drawing-pos", "Drawing position"),
            updateCaptionMode: "icon",
            ...config
        });

        // initialization
        this.addSection("left");
        this.addOption("left:none",    { icon: "png:drawing-left-none",    label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Left aligned, no text wrapping") });
        this.addOption("left:right",   { icon: "png:drawing-left-right",   label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Left aligned, text wraps at right side") });
        this.addSection("right");
        this.addOption("right:none",   { icon: "png:drawing-right-none",   label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Right aligned, no text wrapping") });
        this.addOption("right:left",   { icon: "png:drawing-right-left",   label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Right aligned, text wraps at left side") });
        this.addSection("center");
        this.addOption("center:none",  { icon: "png:drawing-center-none",  label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Centered, no text wrapping") });
        this.addOption("center:left",  { icon: "png:drawing-center-left",  label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Centered, text wraps at left side") });
        this.addOption("center:right", { icon: "png:drawing-center-right", label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Centered, text wraps at right side") });
    }
}
