/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { getButtonValue, getCaptionNode } from "@/io.ox/office/tk/forms";
import { Button } from "@/io.ox/office/tk/control/button";

import { StyleSheetPicker } from "@/io.ox/office/editframework/view/control/stylesheetpicker";
import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";
import {
    APPLY_STYLE_LABEL, CHANGE_STYLE_LABEL, CREATE_STYLE_LABEL, DELETE_STYLE_LABEL,
    PARAGRAPH_STYLE_LABEL, RENAME_STYLE_LABEL
} from "@/io.ox/office/textframework/view/labels";

// class ParagraphStyleContextMenu ============================================

class ParagraphStyleContextMenu extends ContextMenu {

    constructor(stylePicker) {

        super(stylePicker.docView, stylePicker.menu.el, {
            // TODO: hacked code for functions like "create new paragraph stylesheet"
            selector: 'a.btn:not([data-value^="$cmd$"])'
        });

        this.$anchorButton = $();
        this.styleCollection = stylePicker.styleCollection;

        this.on("popup:show", () => this.$anchorButton.addClass("contextmenu-anchor"));
        this.on("popup:hide", () => this.$anchorButton.removeClass("contextmenu-anchor"));
    }

    // protected methods ------------------------------------------------------

    // add the buttons dynamically for selected stylesheet
    /*protected override*/ implPrepareContext(sourceEvent) {

        this.$anchorButton = $(sourceEvent.target).closest(".btn");
        const styleId = getButtonValue(this.$anchorButton);

        this.destroyAllControls();
        this.addControl("paragraph/stylesheet",       new Button({ label: APPLY_STYLE_LABEL,  value: styleId }));
        this.addControl("paragraph/changestylesheet", new Button({ label: CHANGE_STYLE_LABEL, value: styleId }));

        if (this.styleCollection.isCustom(styleId)) {
            this.addControl("paragraph/renamestylesheet", new Button({ label: RENAME_STYLE_LABEL, value: styleId }));
            this.addControl("paragraph/deletestylesheet", new Button({ label: DELETE_STYLE_LABEL, value: styleId }));
        }
    }
}

// class ParagraphStylePicker =================================================

/**
 * A dropdown menu control for paragraph stylesheets.
 */
export class ParagraphStylePicker extends StyleSheetPicker {

    constructor(docView) {

        // base constructor
        super(docView, "paragraph", {
            width: "12em",
            icon: "png:pilcrow",
            tooltip: PARAGRAPH_STYLE_LABEL,
            gridColumns: 4,
            resourceKey: "paragraphstylenames"
        });

        // context menu for changing styles
        this.contextMenu = this.member(new ParagraphStyleContextMenu(this));

        // do not close own popup menu when context menu opens
        this.menu.registerFocusableNodes(this.contextMenu.$el);

        // create the sections for the option buttons
        this.addSection("styles");
        this.addSection("special", { sorted: false, gridColumns: 1 });

        // apply paragraph formatting to option buttons
        this.menu.on("list:option:add", ($button, styleId) => {
            // TODO: hacked code for functions like "create new paragraph stylesheet"
            if (!styleId.startsWith("$cmd$")) {
                $button.prepend('<div class="page-effect abs">');
                this._setParagraphFormatting($button, styleId);
            }
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the dropdown menu button.
     */
    /*protected override*/ implUpdate(styleId) {
        super.implUpdate(styleId);
        this._setParagraphFormatting(this.$menuButton, styleId);
        getCaptionNode(this.$menuButton).children("span").css("color", "var(--text)");
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();
        // TODO: hacked code for functions like "create new paragraph stylesheet"
        this.addOption("$cmd$/paragraph/createstylesheet", { section: "special", label: CREATE_STYLE_LABEL });
    }

    // private methods --------------------------------------------------------

    /**
     * Formats the passed DOM element according to the specified paragraph
     * stylesheet.
     */
    /*private*/ _setParagraphFormatting($button, styleId) {
        const $caption = getCaptionNode($button);
        this.styleCollection.setElementAttributes($caption, { styleId }, { preview: true });
    }
}
