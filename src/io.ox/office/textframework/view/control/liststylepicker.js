/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { str, dict } from "@/io.ox/office/tk/algorithms";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

import { DEFAULT_LIST_STYLE_ID } from "@/io.ox/office/textframework/utils/listutils";

// class ListStylePicker ======================================================

/**
 * Base class for a dropdown list control containing different styles for
 * bullet lists, or numbered lists.
 */
class ListStylePicker extends RadioList {

    constructor(docView, listStyles, config) {

        // base constructor
        super({
            highlight: Boolean,
            splitValue: DEFAULT_LIST_STYLE_ID,
            updateCaptionMode: "none",
            listLayout: "grid",
            sorted: true,
            valueSerializer: listStyleId => listStyles[listStyleId]?.listKey,
            ...config
        });

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // private properties
        this._listStyles = listStyles;

        // CSS marker class for styling
        this.menu.$el.addClass("list-style-picker");
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // CSS font family of the "Cambria" font (with fall-back fonts)
        const fontFamily = this.docModel.getCssFontFamily("Cambria");
        // font attributes for the drop-down list entries
        const labelStyle = { fontFamily, fontSize: "18px", minWidth: "18px" };

        // create the sections for the regular styles, and the special "None" style
        this.addSection("default");
        this.addSection("special", { sorted: false, gridColumns: 1 });

        // create all list styles passed to the constructor
        dict.forEach(this._listStyles, (listStyle, listStyleId) => {
            if (str.trimAndCleanNPC(listStyle.listLabel)) {
                this.addOption(listStyleId, {
                    section: "default",
                    label: _.noI18n(listStyle.listLabel),
                    labelStyle,
                    textAlign: "center",
                    tooltip: _.noI18n(listStyle.tooltip),
                    sortIndex: listStyle.sortIndex
                });
            } else {
                this.addOption(listStyleId, {
                    section: "special",
                    label: _.noI18n(listStyle.tooltip),
                    textAlign: "center"
                });
            }
        });
    }
}

// class BulletListStylePicker ================================================

export class BulletListStylePicker extends ListStylePicker {
    constructor(docView) {
        super(docView, docView.docModel.getPredefinedBulletListStyles(), {
            icon: "png:list-bullet",
            tooltip: gt("Bullet list"),
            gridColumns: 4,
            dropDownVersion: { label: gt("Bullet list") }
        });
    }
}

// class NumberedListStylePicker ==============================================

export class NumberedListStylePicker extends ListStylePicker {
    constructor(docView) {

        // find number of list styles per group (different in Spreadsheet and Text)
        const predefinedStyles = docView.docModel.getPredefinedNumberedListStyles();
        const gridColumns = _.where(predefinedStyles, { format: "decimal" }).length || 4; // bug 53055: use default value 4

        super(docView, predefinedStyles, {
            icon: "png:list-numbered",
            tooltip: gt("Numbered list"),
            gridColumns,
            dropDownVersion: { label: gt("Numbered list") }
        });
    }
}
