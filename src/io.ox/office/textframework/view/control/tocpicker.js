/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";
import gt from "gettext";

import { getCaptionNode } from "@/io.ox/office/tk/forms";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

import "@/io.ox/office/textframework/view/control/tocpicker.less";

// constants ==================================================================

const TOC_ENTRIES = [
    { id: 1, dots: true,  page: true,  tooltip: /*#. insert table of contents template with dots and page numbers */ gt("With dots and page numbers") },
    { id: 2, dots: false, page: true,  tooltip: /*#. insert table of contents template with page numbers */ gt("With page numbers") },
    { id: 3, dots: false, page: false, tooltip: /*#. insert table of contents template with headings text only */ gt("Headings only") }
];

// class TocPicker ============================================================

/**
 * Control to pick Table of contents template from dropdown
 */
export class TocPicker extends RadioList {

    constructor(config) {

        super({
            icon: "png:table-of-contents",
            label: /*#. button to insert table of contents in document */ gt("Table of contents"),
            tooltip: gt("Insert table of contents"),
            listLayout: "grid",
            gridColumns: 1,
            ...config
        });

        this.menu.$el.addClass("toc-picker");

        for (const { id, dots, page, tooltip } of TOC_ENTRIES) {
            const $caption = getCaptionNode(this.addOption(id, { tooltip }));
            for (let line = 1; line <= 3; line += 1) {
                $caption.append($(`<div class="line flex-row">`).css({ "margin-left": `${line - 1}em`, "aria-hidden": true }).append(
                    $("<span>").text(gt("Heading %1$d", line)),
                    $('<span class="dots flex-grow">').css("visibility", dots ? "" : "hidden"),
                    $('<span class="noI18n">').text(String(line ** 2 + 2)).css("visibility", page ? "" : "hidden")
                ));
            }
        }
    }
}
