/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { is, str } from "@/io.ox/office/tk/algorithms";
import { COMMENTFILTERGROUP_CLASS } from "@/io.ox/office/textframework/utils/dom";
import { CheckGroup } from "@/io.ox/office/tk/control/checkgroup";

// private functions ==========================================================

/**
 * A case-insensitive list item matcher.
 */
function matcher(value, entry) {
    return is.string(value) && str.equalsICC(value, entry);
}

// class CommentFilterGroup ===================================================

/**
 * A check-list group control that shows all comment authors.
 */
export class CommentFilterGroup extends CheckGroup {

    constructor(docView) {

        // base constructor
        super({ matcher, sorted: true, checkAll: true });

        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // adding a marker class to the group, so that it can be identified later (DOCS-2657)
        this.$el.addClass(COMMENTFILTERGROUP_CLASS);

        // running once because of deferred creation of tool pane. commentlayer is already completely initialized (66216)
        this._updateAuthorList();

        // register listener for comment layer notification if list of authors is modified
        this.listenTo(docView, "update:commentauthorlist", this._updateAuthorList);
    }

    // private methods --------------------------------------------------------

    /**
     * Generating one button for each comment author.
     */
    /*private*/ _updateAuthorList() {

        this.clearOptions();

        const commentLayer = this.docApp.isTextApp() ? this.docModel.getCommentLayer() : undefined;
        commentLayer?.getCommentCollection().getCommentsAuthorList().forEach(author => {
            this.addOption(author, { label: _.noI18n(author) });
        });
    }
}
