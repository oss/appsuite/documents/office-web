/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { type EditFrameworkDialogConfig, HOR_ALIGNMENT_LABELS } from "@/io.ox/office/editframework/view/editlabels";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/view/editlabels";

// types ======================================================================

/**
 * Additional configuration options for `BaseDialog` used in applications.
 */
export interface TextFrameworkDialogConfig extends EditFrameworkDialogConfig {

    /**
     * If set to `true`, suppresses processing of external operations and
     * keyboard events while the dialog is open. Default value is `false`.
     *
     * For details, see:
     * - `EditModel.setInternalOperationBlocker()`
     * - `TextBaseModel.setBlockKeyboardEvent()`
     */
    blockModelProcessing?: boolean;
}

// constants ==================================================================

/**
 * Label for drop-down menus.
 */
export const MORE_FONT_STYLES_LABEL = gt("More font styles");

//#. horizontal text alignment in a paragraph
export const PARA_ALIGNMENT_LABEL = gt("Paragraph alignment");

//#. vertical text alignment in a shape
export const TEXT_VERT_ALIGNMENT_LABEL = gt("Text alignment");

//#. horizontal and vertical text alignment in a shape
export const TEXT_ALIGNMENT_IN_SHAPE_LABEL = gt("Text alignment");

//#. vertical text alignment in a shape
export const TEXT_VERT_ALIGNMENT_TOOLTIP = gt("Align text vertically inside textbox");

//#. horizontal and vertical text alignment in a shape
export const TEXT_ALIGNMENT_IN_SHAPE_TOOLTIP = gt("Align text horizontally and vertically inside textbox");

//#. vertical text position (subscript/superscript)
export const TEXT_POSITION_LABEL = gt("Text position");

//#. distance between lines of text within a paragraph
export const LINE_SPACING_LABEL = gt("Line spacing");

//#. distance in between two or more selected paragraphs
export const PARAGRAPH_SPACING_LABEL = gt("Paragraph spacing");

//#. combined label text of both label texts from above
export const PARAGRAPH_AND_LINE_SPACING_LABEL = gt("Paragraph & Line spacing");

/**
 * An array with style options for all supported horizontal paragraph
 * alignments.
 */
export const PARA_ALIGNMENT_STYLES = [
    { value: "left",    icon: "png:para-align-left",    label: HOR_ALIGNMENT_LABELS.left },
    { value: "center",  icon: "png:para-align-center",  label: HOR_ALIGNMENT_LABELS.center },
    { value: "right",   icon: "png:para-align-right",   label: HOR_ALIGNMENT_LABELS.right },
    { value: "justify", icon: "png:para-align-justify", label: HOR_ALIGNMENT_LABELS.justify }
];

/**
 * An array with style options for all supported vertical paragraph alignments.
 */
export const PARA_VERT_ALIGNMENT_STYLES = [
    { value: "top",      icon: "png:cell-v-align-top",    label: gt("Top") },
    { value: "centered", icon: "png:cell-v-align-middle", label: gt("Middle") },
    { value: "bottom",   icon: "png:cell-v-align-bottom", label: gt("Bottom") }
];

export const GO_TO_ANCHOR_OPTIONS = {
    //#. anchor link: follow anchor to selected paragraph
    label: gt("Follow anchor link"),
    tooltip: gt("Click to go to this paragraph in document")
};

export const SHOW_SNAPLINES_CHECKBOX_OPTIONS = {
    //#. view: use alignment lines to align shapes during resize or move
    label: gt("Show alignment guides"),
    tooltip: gt("Show or hide alignment guides on shape move or resize")
};

// changetracking -------------------------------------------------------------

export const ACCEPT_AND_NEXT_OPTIONS = {
    //#. change tracking: accept the current document change and jump to the next change
    label: gt("Accept and next"),
    tooltip: gt("Accept the current change and select the next change")
};

export const ACCEPT_CURRENT_OPTIONS = {
    //#. change tracking: accept the current document change
    label: gt("Accept this change"),
    tooltip: gt("Accept this change in the document")
};

export const ACCEPT_ALL_OPTIONS = {
    //#. change tracking: accept all changes in the document
    label: gt("Accept all changes"),
    tooltip: gt("Accept all changes in the document")
};

export const REJECT_AND_NEXT_OPTIONS = {
    //#. change tracking: reject the current change and jump to the next change
    label: gt("Reject and next"),
    tooltip: gt("Reject the current change and select the next change")
};

export const REJECT_CURRENT_OPTIONS = {
    //#. change tracking: reject the current document change
    label: gt("Reject this change"),
    tooltip: gt("Reject this change in the document")
};

export const REJECT_ALL_OPTIONS = {
    //#. change tracking: reject all changes in this document
    label: gt("Reject all changes"),
    tooltip: gt("Reject all changes in the document")
};

// stylesheets ----------------------------------------------------------------

export const PARAGRAPH_STYLE_LABEL = gt("Paragraph style");

export const APPLY_STYLE_LABEL = gt("Apply style");

export const CHANGE_STYLE_LABEL = gt("Update style");

export const CHANGE_STYLE_LABEL_TOOLTIP = gt("Update current style from selection");

export const RENAME_STYLE_LABEL = gt("Rename style");

export const DELETE_STYLE_LABEL = gt("Delete style");

export const CREATE_STYLE_LABEL = gt("Create new style");

export const CREATE_STYLE_LABEL_TOOLTIP = gt("Create new style from selection");

/**
 * A text label or tool tip for a "Format Painter" control.
 */
//#. copy formatting from one location and apply it to other objects
export const FORMAT_PAINTER_LABEL = gt("Format painter");

// textfields -----------------------------------------------------------------

export const EDIT_FIELD_OPTIONS = {
    //#. fields: edit format of selected field
    label: gt("Edit field format"),
    tooltip: gt("Edit format of selected field")
};

export const UPDATE_FIELD_OPTIONS = {
    //#. fields: update selected field
    label: gt("Update field"),
    tooltip: gt("Update selected field")
};

export const UPDATE_ALL_FIELDS_OPTIONS = {
    //#. fields: update all fields in document
    label: gt("Update all fields"),
    tooltip: gt("Update all fields in document")
};

export const INSERT_SLIDE_NUMBER_OPTIONS = {
    //#. fields: insert number of the current slide
    label: gt("Slide number"),
    tooltip: gt("Insert current slide number")
};

export const INSERT_DATE_TIME_OPTIONS = {
    //#. fields: insert current date and time
    label: gt("Date and time"),
    tooltip: gt("Insert current date and time")
};

export const FOOTER_BUTTON_OPTIONS = {
    //#. fields: insert footer fields
    label: gt("Footer"),
    icon: "png:header-footer",
    tooltip: gt("Insert footer with fields like date and slide number")
};
