/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { LineHeight } from "@/io.ox/office/editframework/utils/lineheight";
import { Button, RadioGroup, RadioList, CompoundButton } from "@/io.ox/office/editframework/view/editcontrols";
import { CHANGE_STYLE_LABEL_TOOLTIP, CREATE_STYLE_LABEL, CREATE_STYLE_LABEL_TOOLTIP, LINE_SPACING_LABEL,
    PARA_ALIGNMENT_LABEL, PARA_ALIGNMENT_STYLES, PARA_VERT_ALIGNMENT_STYLES, PARAGRAPH_AND_LINE_SPACING_LABEL,
    PARAGRAPH_SPACING_LABEL, PARAGRAPH_STYLE_LABEL, TEXT_POSITION_LABEL, TEXT_VERT_ALIGNMENT_LABEL,
    TEXT_VERT_ALIGNMENT_TOOLTIP } from "@/io.ox/office/textframework/view/labels";
import { CommentFilterGroup } from "@/io.ox/office/textframework/view/control/commentfiltergroup";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/view/editcontrols";
export * from "@/io.ox/office/textframework/view/control/paragraphstylepicker";
export * from "@/io.ox/office/textframework/view/control/liststylepicker";
export * from "@/io.ox/office/textframework/view/control/drawingpositionpicker";
export * from "@/io.ox/office/textframework/view/control/commentfiltergroup";
export * from "@/io.ox/office/textframework/view/control/tocpicker";

// class ParagraphAlignmentGroup ==============================================

/**
 * A picker control for horizontal text alignment in paragraphs.
 */
export class ParagraphAlignmentGroup extends RadioGroup {

    constructor(config) {

        super({
            tooltip: PARA_ALIGNMENT_LABEL,
            ...config
        });

        // set accessible name - WCAG 2.0 Success Criterion 4.1.2
        this.$el.attr({ "aria-label": PARA_ALIGNMENT_LABEL, "aria-labelledby": "dummy" });

        for (const style of PARA_ALIGNMENT_STYLES) {
            this.addOption(style.value, { icon: style.icon, tooltip: style.label });
        }
    }
}

// class ParaVertAlignmentGroup ===============================================

/**
 * A picker control for vertical text alignment in paragraphs.
 */
export class ParaVertAlignmentGroup extends RadioGroup {

    constructor(config) {

        super({
            tooltip: TEXT_VERT_ALIGNMENT_LABEL,
            ...config
        });

        // set accessible name - WCAG 2.0 Success Criterion 4.1.2
        this.$el.attr({ "aria-label": TEXT_VERT_ALIGNMENT_LABEL, "aria-labelledby": "dummy" });

        for (const style of PARA_VERT_ALIGNMENT_STYLES) {
            this.addOption(style.value, { icon: style.icon, tooltip: style.label });
        }
    }
}

// class ParagraphAlignmentPicker =============================================

/**
 * A picker control for horizontal text alignment in paragraphs.
 */
export class ParagraphAlignmentPicker extends RadioList {

    constructor(config) {

        super({
            icon: "png:para-align-left",
            tooltip: PARA_ALIGNMENT_LABEL,
            updateCaptionMode: "icon",
            ...config
        });

        for (const style of PARA_ALIGNMENT_STYLES) {
            this.addOption(style.value, { icon: style.icon, label: style.label });
        }
    }
}

// class ParaVertAlignmentPicker ==============================================

/**
 * A picker control for vertical text alignment in paragraphs.
 */
export class ParaVertAlignmentPicker extends RadioList {

    constructor(config) {

        super({
            icon: "png:cell-v-align-middle",
            label: TEXT_VERT_ALIGNMENT_LABEL,
            tooltip: TEXT_VERT_ALIGNMENT_TOOLTIP,
            updateCaptionMode: "icon",
            ...config
        });

        for (const style of PARA_VERT_ALIGNMENT_STYLES) {
            this.addOption(style.value, { icon: style.icon, label: style.label });
        }
    }
}

// class TextBoxAlignmentPicker ===============================================

/**
 * A picker control for horizontal and vertical alignment of paragraphs in text
 * boxes.
 */
export class TextBoxAlignmentPicker extends CompoundButton {

    constructor(docView, config) {

        super(docView, {
            icon: "png:para-align-left",
            tooltip: gt("Paragraph alignment"),
            updateCaptionMode: "none",
            ...config
        });

        this.addSection("hor", gt("Horizontal alignment"));
        const horizontalGroup = this.addControl("paragraph/alignment", new RadioGroup());
        for (const style of PARA_ALIGNMENT_STYLES) {
            horizontalGroup.addOption(style.value, { icon: style.icon, label: style.label });
        }

        this.addSection("ver", gt("Vertical alignment"));
        const verticalGroup = this.addControl("drawing/verticalalignment", new RadioGroup());
        for (const style of PARA_VERT_ALIGNMENT_STYLES) {
            verticalGroup.addOption(style.value, { icon: style.icon, label: style.label });
        }
    }
}

// class TableAlignmentPicker =================================================

/**
 * A picker control for horizontal and vertical alignment of paragraphs in
 * text boxes.
 */
export class TableAlignmentPicker extends CompoundButton {

    constructor(docView, config) {

        super(docView, {
            icon: "png:para-align-left",
            tooltip: gt("Paragraph alignment"),
            updateCaptionMode: "none",
            ...config
        });

        this.addSection("hor", gt("Horizontal alignment"));
        const horizontalGroup = this.addControl("paragraph/alignment", new RadioGroup());
        for (const style of PARA_ALIGNMENT_STYLES) {
            horizontalGroup.addOption(style.value, { icon: style.icon, label: style.label });
        }

        this.addSection("ver", gt("Vertical alignment"));
        const verticalGroup = this.addControl("table/verticalalignment", new RadioGroup());
        for (const style of PARA_VERT_ALIGNMENT_STYLES) {
            verticalGroup.addOption(style.value, { icon: style.icon, label: style.label });
        }
    }
}

// class LineHeightPicker =====================================================

export class LineHeightPicker extends RadioGroup {

    constructor() {

        super({
            valueSerializer: LineHeight.serialize,
            dropDownVersion: { label: LINE_SPACING_LABEL }
        });

        this.addOption(LineHeight.SINGLE,   { icon: "png:para-line-spacing-100", label: /*#. text line spacing in paragraphs */ gt("100%") });
        this.addOption(LineHeight._115,     { icon: "png:para-line-spacing-115", label: /*#. text line spacing in paragraphs */ gt("115%") });
        this.addOption(LineHeight.ONE_HALF, { icon: "png:para-line-spacing-150", label: /*#. text line spacing in paragraphs */ gt("150%") });
        this.addOption(LineHeight.DOUBLE,   { icon: "png:para-line-spacing-200", label: /*#. text line spacing in paragraphs */ gt("200%") });
    }
}

// class ParagraphSpacingPicker ===============================================

export class ParagraphSpacingPicker extends RadioGroup {

    constructor() {

        super({
            dropDownVersion: { label: PARAGRAPH_SPACING_LABEL }
        });

        this.addOption(0, { label: /*#. no distance between selected paragraphs */       gt.pgettext("paragraph-spacing", "None") });
        this.addOption(1, { label: /*#. a normal distance between selected paragraphs */ gt.pgettext("paragraph-spacing", "Normal") });
        this.addOption(2, { label: /*#. a large distance between selected paragraphs */  gt.pgettext("paragraph-spacing", "Wide") });
    }
}

// class ParagraphSpacingMenu =================================================

/**
 * A picker control for line height inside paragraphs, and spacing between
 * paragraphs.
 */
export class ParagraphSpacingMenu extends CompoundButton {

    constructor(docView) {

        super(docView, {
            icon: "png:para-line-spacing-100",
            tooltip: PARAGRAPH_AND_LINE_SPACING_LABEL,
            skipStateAttr: true,
            dropDownVersion: { label: PARAGRAPH_AND_LINE_SPACING_LABEL }
        });

        this.addSection("line", LINE_SPACING_LABEL);
        this.addControl("paragraph/lineheight", new LineHeightPicker());

        this.addSection("para", PARAGRAPH_SPACING_LABEL);
        this.addControl("paragraph/spacing", new ParagraphSpacingPicker());
    }
}

// class TextPositionGroup ====================================================

/**
 * A button group control for text position (subscript/superscript).
 */
export class TextPositionGroup extends RadioGroup {

    constructor(config) {

        super({
            toggleValue: "baseline",
            ...config
        });

        // set accessible name - WCAG 2.0 Success Criterion 4.1.2
        this.$el.attr({ "aria-label": TEXT_POSITION_LABEL, "aria-labelledby": "dummy" });

        this.addOption("sub",   { icon: "png:font-subscript",   tooltip: gt("Subscript"),   dropDownVersion: { label: gt("Subscript"),   tooltip: null } });
        this.addOption("super", { icon: "png:font-superscript", tooltip: gt("Superscript"), dropDownVersion: { label: gt("Superscript"), tooltip: null } });
    }
}

// class UserFilterPicker =====================================================

/**
 * Drop-down menu control for setting a user filter for the comments in the
 * comments layer.
 */
export class UserFilterPicker extends CompoundButton {

    constructor(docView) {

        // base constructor
        super(docView, {
            label: gt("Comment authors"),
            tooltip: gt("Show comment authors"),
            anchorBorder: ["right", "left", "bottom", "top"],
            updateCaptionMode: "icon"
        });

        // initialization
        this.addControl("comment/authorFilter", new CommentFilterGroup(docView), { sticky: true });
    }
}

// class CommentDisplayModePicker =============================================

/**
 * Dropdown menu control for modifying the highlight mode and visibility of the
 * comments in the comments layer. Additionally a list with all comment authors
 * can be used to filter specific authors.
 */
export class CommentDisplayModePicker extends CompoundButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            icon: "png:comment-show",
            label: gt("Markup"),
            tooltip: gt("Highlight comments mode"),
            updateCaptionMode: "icon"
        });

        // initialization
        const displayModeGroup = this.addControl("comment/displayMode", new RadioGroup());
        if (!config?.smallDevice) {
            displayModeGroup.addOption("all",      { label: gt("Highlight all comments") });
            displayModeGroup.addOption("selected", { label: gt("Highlight selected comments") });
        }
        displayModeGroup.addOption("bubbles", { label: gt("Show bubble comments") });
        // displayModeGroup.addOption("none",    { label: gt("Highlight no comments") });
        displayModeGroup.addOption("hidden",  { label: gt("Show no comments") });

        this.addSection("users");
        // button with a list of all users who created comments in the document
        this.addControl(null, new UserFilterPicker(docView));
    }
}

// class AnchoragePicker ======================================================

/**
 *
 * Dropdown menu control for position and text floating of drawing objects in
 * text documents.
 */
export class AnchoragePicker extends RadioList {

    constructor() {

        // base constructor
        super({
            icon: "png:position-anchor",
            label: /*#. alignment and text floating of drawing objects in text documents */ gt.pgettext("drawing-pos", "Alignment"),
            tooltip: /*#. alignment and text floating of drawing objects in text documents */ gt.pgettext("drawing-pos", "Drawing alignment"),
            updateCaptionMode: "icon"
        });

        // initialization
        this.addOption("inline",     { icon: "png:position-on-text",         label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Move with text") });
        this.addOption("paragraph",  { icon: "png:position-on-paragraph",    label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Move with paragraph") });
        this.addOption("margin",     { icon: "png:position-on-page-margin",  label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Move with page margin") });
        this.addOption("page",       { icon: "png:position-on-page",         label: /*#. drawing object position in paragraph */ gt.pgettext("drawing-pos", "Move with page") });
    }
}

// class InTextStyleContextSubMenu ============================================

export class InTextStyleContextSubMenu extends CompoundButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            autoCloseParent: false,
            hideDisabled: true,
            anchorBorder: ["right", "left"],
            label: PARAGRAPH_STYLE_LABEL,
            ...config
        });

        this._styleId = "";

        this.addControl("paragraph/createstylesheet", new Button({ label: CREATE_STYLE_LABEL, tooltip: CREATE_STYLE_LABEL_TOOLTIP }));
        this._changeStyleButton = this.addControl("paragraph/changestylesheet", new Button({ value: () => this._styleId, tooltip: CHANGE_STYLE_LABEL_TOOLTIP }));

        this.listenTo(this.menu, "popup:beforeshow", this._updateChangeStyleButton);
    }

    // private methods --------------------------------------------------------

    /*private*/ _updateChangeStyleButton() {
        this._styleId = this.docModel.getAttributes("paragraph").styleId;
        if (this._styleId) {
            this._changeStyleButton.setLabel(gt('Change style "%1$s"', this.docModel.getTranslatedNameByStyleId(this._styleId)));
            this._changeStyleButton.show();
        } else {
            this._changeStyleButton.hide();
        }
    }
}
