/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { getTrackingRectangle } from '@/io.ox/office/drawinglayer/utils/drawingutils';
import { coord } from '@/io.ox/office/tk/algorithms';
import { Rectangle } from '@/io.ox/office/tk/dom';
import { getDomNode, getFunctionOption, getOption, getStringOption, hasHorizontalScrollBar,
    hasVerticalScrollBar } from '@/io.ox/office/tk/utils';

// class SelectionBox =====================================================

/**
 * The selection represents a rectangle on the application content root node, that
 * can be used for setting a selection or for creating the size and position of a
 * shape or a text frame
 *
 * @param {PresentationApplication} app
 *  The application containing this document model.
 *
 * @param {HTMLElement|jQuery} node
 *  The node at which the event handling for the selection box shall be registered.
 */
function SelectionBox(app, node) {

    var // self reference
        self = this,
        // the currently active mode
        activeMode = null,
        // the previous mode, that was active before
        previousMode = null,
        // the handler function for the specified events
        selectionBoxHandler = null,
        // whether the selection box is active
        selectionBoxActive = false,
        // whether the selection box is visible
        selectionBoxAppended = false,
        // whether the click happened on the vertical scrollbar
        isScrollBarEvent = false,
        // a marker for those operations that are triggered from the selection box
        // -> this marker can be used recognize operation not from selection box, so
        //    that the box can be cancelled.
        isSelectionBoxOperation = false,
        // the selection box element
        selectionBoxNode = $('<div>').addClass('selectionbox'),
        // the container for all registered modes
        allModes = {};

    // private methods ----------------------------------------------------

    /**
     * Handling all tracking events while creating the selection box to select drawings.
     *
     * @returns {Function}
     *  The handler function for all events specified in 'SelectionBox.SELECTION_BOX_EVENTS'
     *  at the node, that is given to the SelectionBox constructor.
     */
    function generateSelectionBoxHandler() {

        var // the start position of mouse down event
            anchor = coord.point(0, 0),
            // the current position of mouse down event
            current = coord.point(0, 0),
            // the root node, containing the scrollbar
            scrollNode = node,
            // position of the last processed mouse/touch event
            eventPos = null,
            // the position of the content root node
            pos = null,
            // whether the mouse or touch point has really been moved (with threshold distance)
            mouseMoved = false;

        // initialing the selection box according to the passed event
        function startSelectionBox(event, eventType) {

            var // the options for the active mode
                modeOptions = allModes[activeMode],
                // the optional filter for the event target
                selectionFilter = getStringOption(modeOptions, 'selectionFilter', ''),
                // the optional exclude filter for the event target
                excludeFilter =  getStringOption(modeOptions, 'excludeFilter', ''),
                // a callback function that is triggered when the selection box is created
                // -> if this function returns 'false', the selection box will be cancelled.
                startHandler = getFunctionOption(modeOptions, 'startHandler'),
                // the context of the callback functions
                context = getOption(modeOptions, 'context'),
                // whether the selection box can continue after executing the start handler
                continueSelectionBox = true;

            // the horizontal and vertical event position
            eventPos = self.getEventPosition(event, eventType, eventPos);
            if (!eventPos || (excludeFilter && $(event.target).is(excludeFilter))) {
                selectionBoxActive = true;  // setting to true temporarily
                self.cancelSelectionBox();
                app.getController().update(); // update controller if buttons are highlighted when mode is active
                return;
            }

            mouseMoved = false;

            if (!selectionFilter || $(event.target).is(selectionFilter)) {

                // check, if this is an event on the vertical or horizontal scroll bar
                isScrollBarEvent = false; // reset
                if (hasVerticalScrollBar(scrollNode)) {
                    isScrollBarEvent = event.clientX > (scrollNode.offset().left + getDomNode(scrollNode).clientWidth);
                    if (isScrollBarEvent) { return; }
                }

                if (hasHorizontalScrollBar(scrollNode)) {
                    isScrollBarEvent = event.clientY > (scrollNode.offset().top + getDomNode(scrollNode).clientHeight);
                    if (isScrollBarEvent) { return; }
                }

                // -> selection box can be activated
                selectionBoxActive = true;

                if (previousMode) { selectionBoxNode.removeClass(previousMode); }
                selectionBoxNode.addClass(activeMode);  // customizing the selection box

                // setting the specified class at the app content root node
                if (modeOptions && modeOptions.contentRootClassActive) { $(node).addClass(modeOptions.contentRootClassActive); }

                pos = scrollNode.offset();
                anchor.x = eventPos.x + scrollNode.scrollLeft() - pos.left;
                anchor.y = eventPos.y + scrollNode.scrollTop() - pos.top;

                selectionBoxNode.css({ left: anchor.x, top: anchor.y });

                if (startHandler) { continueSelectionBox = startHandler.call(context, new Rectangle(anchor.x, anchor.y, 0, 0)); }

                if (continueSelectionBox === false) {
                    self.cancelSelectionBox();
                    return;
                }

                addCaptureEventListeners();
            }

        }

        // updating the selection box according to the passed event
        function updateMove(event, eventType) {

            var // the options for the active mode
                modeOptions = allModes[activeMode],
                // a callback function that is triggered when the selection box is moved
                moveHandler = getFunctionOption(modeOptions, 'moveHandler'),
                // the context of the callback functions
                context = getOption(modeOptions, 'context');

            if (!selectionBoxActive) { return; }

            // the horizontal and vertical event position
            eventPos = self.getEventPosition(event, eventType, eventPos);
            if (!eventPos) {
                self.cancelSelectionBox();
                return;
            }

            // appending the selection box to the scroll node
            if (!selectionBoxAppended) {
                scrollNode.append(selectionBoxNode);
                selectionBoxAppended = true;
            }

            // avoiding scroll problems on touch devices
            if ((eventType === 'touch') && modeOptions.preventScrolling) { event.preventDefault(); }

            // reading scrollPosition again. Maybe it was not updated or not updated completely.
            current.x = eventPos.x + scrollNode.scrollLeft() - pos.left;
            current.y = eventPos.y + scrollNode.scrollTop() - pos.top;

            // the current location of the selected frame rectangle
            var rectOptions = { ...modeOptions.trackingOptions };
            if (activeMode === 'shape') { rectOptions.modifiers = event; }
            var rectangle = getTrackingRectangle(anchor, current, rectOptions);

            // start tracking after a threshold of 3 pixels
            mouseMoved ||= (rectangle.origShift >= 3);
            if (!mouseMoved) { rectangle.set(anchor.x, anchor.y, 0, 0); }

            selectionBoxNode.css(rectangle.toCSS());
            if (moveHandler) { moveHandler.call(context, rectangle); }

            return rectangle;
        }

        // handling the selection box size, when moving is stopped according to the passed event
        function stopSelectionBox(event, eventType) {

            var // the options for the active mode
                modeOptions = allModes[activeMode],
                // a callback function that is triggered when the selection box is stopped
                stopHandler = getFunctionOption(modeOptions, 'stopHandler'),
                // the context of the callback functions
                context = getOption(modeOptions, 'context');

            if (!selectionBoxActive) { return; }
            if (isScrollBarEvent) { return; } // this happens after a click on the vertical or horizontal scroll bar

            var rectangle = updateMove(event, eventType);
            if (rectangle && stopHandler) { stopHandler.call(context, rectangle); }
        }

        // return the actual selection box handler function
        return function (event) {

            switch (event.type) {
                case 'mousedown':
                case 'touchstart':
                    if (activeMode && event.button !== 2) { // no selection box on right click
                        startSelectionBox(event, (event.type === 'touchstart') ? 'touch' : 'mouse');
                    } else if (app.getModel().useSlideMode() && $(event.target).is(app.getModel().getMultiSelectionBoxFilterSelector())) { // setting slide selection on right click when target is outside of slide
                        app.getModel().getSelection().setSlideSelection();
                    }
                    break;
                case 'mousemove':
                case 'touchmove':
                    updateMove(event, (event.type === 'touchmove') ? 'touch' : 'mouse');
                    break;
                case 'keydown':
                case 'keyup':
                    updateMove(event, 'key');
                    break;
                case 'mouseup':
                case 'touchend':
                    stopSelectionBox(event, (event.type === 'touchend') ? 'touch' : 'mouse');
                    finalizeSelectionBox();
                    break;
                case 'touchcancel':
                    finalizeSelectionBox();
                    break;
            }
        };

    } // end of selectionBoxHandler() local scope

    /**
     * Finalizing the selection box handling.
     */
    function finalizeSelectionBox() {

        if (!selectionBoxActive) { return; }

        var // the options for the active mode
            modeOptions = allModes[activeMode],
            // a callback function that is triggered when the selection box is finalize after stop
            finalizeHandler = getFunctionOption(modeOptions, 'finalizeHandler'),
            // the context of the callback functions
            context = getOption(modeOptions, 'context');

        removeCaptureEventListeners();

        selectionBoxNode.css({ left: '', top: '', width: '', height: '' });
        selectionBoxNode.detach();

        selectionBoxNode.removeClass(activeMode);

        // removing the optional specified class from the app content root node
        if (modeOptions && modeOptions.contentRootClassActive) { $(node).removeClass(modeOptions.contentRootClassActive); }
        if (finalizeHandler) { finalizeHandler.call(context); }

        selectionBoxActive = false;
        selectionBoxAppended = false;
        isScrollBarEvent = false;
    }

    /**
     * Handling for a class that can be set at the app content root node. The class
     * of the previous mode is removed and the class for the new mode is set. This
     * can be used for example to define a cursor (this would be too late, if it
     * happens in the mouse down handler.
     *
     * @param {String} oldMode
     *  The string specifying the old active mode.
     *
     * @param {String} newMode
     *  The string specifying the new active mode.
     */
    function handleAppContentRootClass(oldMode, newMode) {

        // removing the class from the app content root node immediately
        var oldOptions = oldMode ? allModes[oldMode] : null;
        if (oldOptions && oldOptions.contentRootClass) {
            $(node).removeClass(oldOptions.contentRootClass);
        }

        // setting the class to the app content root node class immediately
        var newOptions = allModes[newMode];
        if (newOptions && newOptions.contentRootClass) {
            $(node).addClass(newOptions.contentRootClass);
        }
    }

    /**
     * Add eventListener to window, to capture the events (mousemove, mouseup,...) which are outside of the slide or browser
     */
    function addCaptureEventListeners() {
        SelectionBox.CAPTURE_EVENTS.forEach(function (event) {
            window.addEventListener(event, selectionBoxHandler, true);
        });
    }

    /**
     * Remove the capture events.
     */
    function removeCaptureEventListeners() {
        SelectionBox.CAPTURE_EVENTS.forEach(function (event) {
            window.removeEventListener(event, selectionBoxHandler, true);
        });
    }

    // public methods -----------------------------------------------------

    /**
     * Registering a selection box mode at a specified node.
     *
     * @param {String} mode
     *  The identifier string to switch between several modes.
     *
     * @param {Object} options
     *  Additional optional options:
     *  @param {Boolean} [options.selectionFilter]
     *      A string, that can be used to filter the target nodes of the event.
     *  @param {String} [options.contentRootClass]
     *      A class name that will be set at the application content root node.
     *      This class is set immediately at the content root node, if the mode
     *      is activated. It is not set at the first mouse-down event.
     *      This parameter can be used to customize the application content root
     *      node. Typically this can modify the cursor after activating the
     *      specified mode.
     *  @param {String} [options.contentRootClassActive]
     *      A class name that will be set at the application content root node
     *      during an active selection box. Contrary to 'options.contentRootClass'
     *      this class is set in the start handler and not directly after
     *      activating the mode.
     *      Typically this can modify the cursor after starting the selection box.
     *  @param {Boolean} [options.leaveModeOnCancel=false]
     *      Whether the currently active mode has to be left, if the user cancels
     *      the action.
     *  @param {Boolean} [options.preventScrolling=false]
     *      prevent scrolling while moving
     *  @param {Function} [options.startHandler]
     *      A callback function that is called when the selection box is created.
     *      The callback function receives the rectangle of the selection box (as
     *      instance of the class Rectangle). The rectangle is calculated relative
     *      to the upper left corner of the application content root node.
     *      If the startHandler function returns 'false', the selection box will
     *      not be created.
     *  @param {Function} [options.moveHandler]
     *      A callback function that is called when the selection box is moved.
     *      The callback function receives the rectangle of the selection box (as
     *      instance of the class Rectangle). The rectangle is calculated relative
     *      to the upper left corner of the application content root node.
     *  @param {Function} [options.stopHandler]
     *      A callback function that is called when the selection box is stopped.
     *      The callback function receives the rectangle of the selection box (as
     *      instance of the class Rectangle). The rectangle is calculated relative
     *      to the upper left corner of the application content root node. After
     *      the stopHandler, the finalizeHandler will be called.
     *  @param {Function} [options.finalizeHandler]
     *      A callback function that is called when the selection box is stopped
     *      or canceled. can be used for cleanup code.
     *  @param {Function} [options.clearGuidelinesHandler]
     *      A callback function that is called when the selection box is canceled.
     *      It is used to clean up guidelines, which is neccessary when
     *      selectionBox is not active anymore.
     *  @param {Object} [options.context]
     *      The context the callback function will be bound to.
     */
    this.registerSelectionBoxMode = function (mode, options) {
        // saving the options for the specified mode
        allModes[mode] = options;
    };

    /**
     * Unregistering a selection box at a specified node.
     */
    this.deRegisterSelectionBox = function () {
        // de-registering the specified handler function at the specified node
        $(node).off(SelectionBox.SELECTION_BOX_EVENTS, selectionBoxHandler);
    };

    /**
     * Setting a specified mode.
     *
     * @param {String} mode
     *  The mode that shall be active.
     *
     * @returns {String}
     *  The active mode.
     */
    this.setActiveMode = function (mode) {

        if (mode !== activeMode) { // no multiple setting of same mode

            // stop a running mode, if this is cancellable
            if (activeMode && self.isCancelModeActive()) {
                // closing an open selection box
                if (self.isSelectionBoxActive()) { finalizeSelectionBox(); }
                self.setPreviousMode();
            }

            previousMode = activeMode;
            activeMode = mode;

            // switching the class at the app content root node immediately
            handleAppContentRootClass(previousMode, activeMode);
            // ... and removing the old class from the selection box
            if (previousMode) { selectionBoxNode.removeClass(previousMode); }
        }

        return activeMode;
    };

    /**
     * Getting the currently active selection box mode.
     *
     * @returns {String}
     *  The active mode.
     */
    this.getActiveMode = function () {
        return activeMode;
    };

    /**
     * Setting back to the previous mode.
     *
     * @returns {String}
     *  The active mode.
     */
    this.setPreviousMode = function () {

        var // the previous selection mode
            tempMode = activeMode;

        activeMode = previousMode;
        previousMode = tempMode;

        // switching the class at the app content root node immediately
        handleAppContentRootClass(previousMode, activeMode);
        // ... and removing the old class from the selection box
        if (previousMode) { selectionBoxNode.removeClass(previousMode); }

        return activeMode;
    };

    /**
     * Check, whether a specified mode is already registered at the
     * selection box.
     *
     * @returns {Boolean}
     *  Whether a specified mode is already registered at the
     *  selection box.
     */
    this.isModeRegistered = function (mode) {
        return _.isObject(allModes[mode]);
    };

    /**
     * Whether the selection box is currently active.
     *
     * @returns {Boolean}
     *  Whether the selection box is currently active.
     */
    this.isSelectionBoxActive = function () {
        return selectionBoxActive;
    };

    /**
     * Whether the selection box is currently appended to the application content
     * root node.
     *
     * @returns {Boolean}
     *  Whether the selection box is currently appended to the application content
     *  root node.
     */
    this.isSelectionBoxAppended = function () {
        return selectionBoxAppended;
    };

    /**
     * Whether the vertical scroll bar is moved. In this case no seletion box is shown.
     *
     * @returns {Boolean}
     *  Whether the vertical scroll bar is currently moved.
     */
    this.isVerticalScrollBarActive = function () {
        return isScrollBarEvent;
    };

    /**
     * Whether the currently active mode shall be stopped when the
     * user cancels it.
     *
     * @returns {Boolean}
     *  Whether the currently active mode shall be stopped when the
     *  user cancels it.
     *
     */
    this.isCancelModeActive = function () {
        return (allModes[activeMode] && allModes[activeMode].leaveModeOnCancel) || false;
    };

    /**
     * Cancel the currently active mode and close selection box.
     */
    this.cancelSelectionBox = function () {
        // remove shape guidelines
        if (allModes[activeMode] && allModes[activeMode].clearGuidelinesHandler) {
            allModes[activeMode].clearGuidelinesHandler.call();
        }
        // closing an open selection box
        if (self.isSelectionBoxActive()) { finalizeSelectionBox(); }

        // leaving the active mode, if specified, that cancel has to leave active mode
        if (self.isCancelModeActive()) {
            self.setPreviousMode();

            // update controller if buttons are highlighted when mode is active
            app.getController().update();
        }
    };

    /**
     * Requesting the marker that can be used to identify operations generated from
     * the selection box. The can be used to cancel an active selection box, if the
     * current operation was not triggered by the selection box.
     *
     * @returns {Boolean}
     *  Whether the marker for operations generated from the selection box is set.
     */
    this.isSelectionBoxOperation = function () {
        return isSelectionBoxOperation;
    };

    /**
     * Setting the marker that can be used to identify operations generated from
     * the selection box. The can be used to cancel an active selection box, if the
     * current operation was not triggered by the selection box.
     *
     * param {Boolean} value
     *  Setting the marker for operations generated from the selection box.
     */
    this.setSelectionBoxOperation = function (value) {
        isSelectionBoxOperation = value;
    };

    /**
     * Returns horizontal and vertical cursor position from event.
     *
     * @param {JEvent} event
     *  A jQuery event object.
     * @param {String} eventType
     *  Type of the event.
     * @param {Object} [eventPos]
     *  Optional parameter for already calculated position.
     *
     * @returns {Object|null}
     */
    this.getEventPosition = function (event, eventType, eventPos) {
        switch (eventType) {
            case 'mouse':
                return { x: event.pageX, y: event.pageY };
            case 'touch':
                if (!event.originalEvent) { return null; }
                var touches = event.originalEvent.touches;
                var changed = event.originalEvent.changedTouches;
                return ((touches.length === ((event.type === 'touchend') ? 0 : 1)) && (changed.length === 1)) ? { x: changed[0].pageX, y: changed[0].pageY } : null;
            case 'key':
                return eventPos;
        }
        return null;
    };

    // initialization -----------------------------------------------------

    // generating the handler function
    selectionBoxHandler = generateSelectionBoxHandler();

    // registering the handler function at the specified node
    $(node).on(SelectionBox.SELECTION_BOX_EVENTS, selectionBoxHandler);
}

// constants --------------------------------------------------------------

// the events for the selection box
SelectionBox.SELECTION_BOX_EVENTS = 'mousedown touchstart'; //'mousedown touchstart mouseup touchend mousemove touchmove touchcancel';

// events to capture if the selection box is active, it's need to catch the events wich are outside of the slide or browser
SelectionBox.CAPTURE_EVENTS = ['mouseup', 'touchend', 'mousemove', 'touchmove', 'touchcancel', 'keydown', 'keyup'];

// exports ================================================================

export default SelectionBox;
