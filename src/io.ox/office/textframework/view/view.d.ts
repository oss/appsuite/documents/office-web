/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { DisplayText } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { EditViewEventMap, EditView } from "@/io.ox/office/editframework/view/editview";
import TextBaseModel from "@/io.ox/office/textframework/model/editor";
import { TextBaseApplication } from "@/io.ox/office/textframework/app/application";
import { FieldConfigType } from "@/io.ox/office/textframework/components/field/fieldmanager";

// types ======================================================================

export interface FieldDatePopupEvent {
    value: DisplayText;
    format: string;
    standard: DisplayText;
}

/**
 * Type mapping for the events emitted by `TextBaseView` instances.
 */
export interface TextBaseViewEventMap extends EditViewEventMap {
    "fielddatepopup:change": [event: FieldDatePopupEvent];
    "fielddatepopup:autoupdate": [state: boolean];
}

// class TextBaseView =========================================================

export default abstract class TextBaseView<EvtMapT extends TextBaseViewEventMap = TextBaseViewEventMap> extends EditView<EvtMapT> {

    declare readonly docModel: TextBaseModel;

    protected constructor(docApp: TextBaseApplication, docModel: TextBaseModel, config?: unknown);

    hideFieldFormatPopup(): void;
    showFieldFormatPopup(fieldConfig?: FieldConfigType, node?: NodeOrJQuery): void;
}
