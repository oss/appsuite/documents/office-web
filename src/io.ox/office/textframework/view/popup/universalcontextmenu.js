/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { fun } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE } from "@/io.ox/office/tk/dom";

import { getExplicitAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import { EditContextMenu } from "@/io.ox/office/editframework/view/popup/editcontextmenu";
import { ImageSourceType } from "@/io.ox/office/drawinglayer/utils/imageutils";

import {
    getCellContentNode, getDomTree, getPresentationCommentBubbleRootNode, isCellNode,
    isChangeTrackNode, isDrawingFrame, isEmptyCell, isEmptyParagraph, isEmptySpan,
    isParagraphNode, isPresentationCommentBubbleTarget, isSpan, isTableNode, isTextSpan
} from "@/io.ox/office/textframework/utils/dom";

import { getOxoPositionFromPixelPosition, isNodePositionInsideRange, isPositionInTable } from "@/io.ox/office/textframework/utils/position";

import {
    ACCEPT_CURRENT_OPTIONS, DELETE_DRAWING_LABEL, DELETE_DRAWING_TOOLTIP, EDIT_FIELD_OPTIONS, EDIT_HYPERLINK_LABEL,
    FORMAT_DRAWING_LABEL, FORMAT_DRAWING_TOOLTIP, GO_TO_ANCHOR_OPTIONS, GROUP_DRAWING_LABEL, GROUP_DRAWING_TOOLTIP,
    INSERT_COMMENT_OPTIONS, INSERT_DATE_TIME_OPTIONS, INSERT_HEADER_LABEL, INSERT_HYPERLINK_OPTIONS, INSERT_IMAGE_OPTIONS,
    INSERT_SLIDE_NUMBER_OPTIONS, OPEN_HYPERLINK_LABEL, REJECT_CURRENT_OPTIONS, REMOVE_HYPERLINK_LABEL, OX_AI_OPTIONS_TRANSLATE,
    UNGROUP_DRAWING_LABEL, UNGROUP_DRAWING_TOOLTIP, UPDATE_ALL_FIELDS_OPTIONS, UPDATE_FIELD_OPTIONS
} from "@/io.ox/office/textframework/view/labels";

import { Button, CompoundButton, DrawingArrangementPicker, InsertTextFrameButton, InTextStyleContextSubMenu, LanguagePicker, TocPicker } from "@/io.ox/office/textframework/view/controls";

import { isTextFrameNode } from "@/io.ox/office/drawinglayer/view/drawingframe";

// constants ==================================================================

const ENABLE_KEYS = ["document/editable"];

// class UniversalContextMenu =================================================

/**
 * A context menu for normal text. Provides menu actions to manipulate entire
 * text.
 */
export class UniversalContextMenu extends EditContextMenu {

    constructor(docView, config) {

        // base constructor
        super(docView, docView.docModel.getNode(), {
            delay: 0,
            ...config,
            repaintAlways: true
        });

        // type flags for current selection
        this._isRangeSelected = false;
        this._isDrawingSelected = false;
        // the clicked node
        this._node = null;
        // the type of the clicked node (drawing, table, ...)
        this._nodeType = null;
        // click position when opening the context menu (mouseX, mouseY)
        this._clickPos = null;
        // OX position of the cursor
        this._resetSelectionTo = null;

        // hide this context menu automatically when scrolling the document page
        this.listenTo(docView.getContentRootNode(), "scroll", () => this.hide());

        // restore selection when releasing mouse button or context key
        this.listenTo(this.docModel.getNode(), "mouseup keyup", this._restoreSelectionHandler);
    }

    // public methods --------------------------------------------------------

    /**
     * Returns whether a range is selected or not
     *
     * @returns {boolean}
     *   Is a range selected or not
     */
    isRangeSelected() {
        return this._isRangeSelected;
    }

    isDrawingSelected() {
        return this._isDrawingSelected;
    }

    // protected methods ------------------------------------------------------

    /**
     * Prepares the context menu.
     * Checks if a table, drawing or something else was clicked
     * and initialize the correct menu
     */
    /*protected override*/ implPrepareContext(sourceEvent) {

        const sourceTarget = sourceEvent.target;
        const selection = this.docModel.getSelection();
        this._isDrawingSelected = this.docModel.isDrawingSelected();
        this._isRangeSelected = !this._isDrawingSelected && selection.hasRange();

        if (_.browser.MacOS && _.browser.Chrome) {
            this._prepareForChromeOnOSX(sourceEvent);
        }

        if (!sourceTarget && !this._isDrawingSelected) { return; }

        if (this._isDrawingSelected) {
            this._node = (selection.isMultiSelectionSupported() && selection.isMultiSelection()) ? selection.getFirstSelectedDrawingNode()[0] : selection.getSelectedDrawing()[0];
        } else {
            this._node = (_.browser.IE && $(sourceTarget).is(".page")) ? $(window.getSelection().focusNode.parentNode) : sourceTarget;
        }

        const arrDomTree = getDomTree(this._node, {
            endNodes: [
                isTextFrameNode,
                isDrawingFrame,
                isCellNode,
                isTableNode,
                isPresentationCommentBubbleTarget
            ]
        });

        this._nodeType = _.last(arrDomTree);

        this._clickPos = { mouseX: sourceEvent.pageX, mouseY: sourceEvent.pageY };

        if (!ENABLE_KEYS.every(key => this.docView.isControllerItemEnabled(key)) && !this._isRangeSelected) {
            const charAttrs = getExplicitAttributes(this._node, "character", true);
            if (!charAttrs.url) { return false;  }
        }
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        const isPresentationCommentBubble = isPresentationCommentBubbleTarget(this._nodeType);
        if (isPresentationCommentBubble) {
            this.docView.trigger("contextmenu:init:threadedcomment", { node: getPresentationCommentBubbleRootNode(this._nodeType) });
        }

        // remove all existing controls from context menu
        this.destroyAllControls();
        this.setToolTip("");

        // add clipboard commands
        if (!isPresentationCommentBubble) {
            this.addClipboardEntries();
        }

        if (ENABLE_KEYS.every(key => this.docView.isControllerItemEnabled(key))) {
            const selection = this.docModel.getSelection();
            // Is Drawing
            if (isDrawingFrame(this._nodeType)) {
                this._prepareDrawingMenu();

                // Is Table
            } else if (isTableNode(this._nodeType) || isCellNode(this._nodeType)) {
                if (isCellNode(this._nodeType) && isSpan(this._node) && !isEmptyCell(getCellContentNode($(this._node).parent()))) {
                    this._prepareTextMenu();
                } else {
                    this._prepareTableMenu();
                }

            } else if (isPresentationCommentBubble) {
                this.preparePresentationCommentBubbleMenu();

                // Is Text
            } else if (selection.isAdditionalTextframeSelection() && !isTextSpan(this._node)) {
                this._prepareDrawingMenu();

            } else if (selection.isSlideSelection()) {
                this.prepareSlideMenu({ position: this._clickPos });

            } else {
                this._prepareTextMenu();
            }
        } else {
            this._prepareHyperlinkMenu();
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Prepares the context menu for text
     */
    /*private*/ _prepareTextMenu() {

        // dd the Language Group to the context menu *once*
        const createLanguageSubMenu = fun.once(() => {
            this.addSection("language");
            this.addControl("character/language", new LanguagePicker(this.docView, {
                autoCloseParent: false,
                updateCaptionMode: "none",
                anchorBorder: ["right", "left"],
                hideUnusedLanguages: true  // only show the languages used in the document
            }));
            if (this.docApp.canUseAI()) {
                this.addControl("oxai/create/selectionrange", new Button({ value: "translate", ...OX_AI_OPTIONS_TRANSLATE, iconStyle: { width: "24px", height: "24px" } }));
            }
        });

        const characterAttrs = getExplicitAttributes(this._node, "character", true);

        // field manager instance
        const fieldManager = this.docModel.getFieldManager();
        // spell checker
        const spellChecker = this.docModel.getSpellChecker();
        const hasOnlineSpellingGroup = characterAttrs.spellerror && spellChecker.isOnlineSpelling();
        const effectiveUrl = this._isRangeSelected ? null : characterAttrs.url;
        const hasFieldGroup = fieldManager.isHighlightState() && !this.docModel.useSlideMode();
        const hasChangeTrackingGroup = !this._isRangeSelected && (isChangeTrackNode(this._node) ||
            (hasFieldGroup && fieldManager.isSupportedFieldType() && isChangeTrackNode(fieldManager.getSelectedFieldNode().prev())));

        // onlinespelling
        if (hasOnlineSpellingGroup) {
            const spellResult = spellChecker.getSpellErrorWord();
            const hasReplacements = spellResult.replacements && spellResult.replacements.length > 0;
            const hasSpellResultWord = spellResult.word && spellChecker.hasSpellerrorClass(spellResult.node);
            if (hasReplacements || hasSpellResultWord) {
                if (hasReplacements) {
                    this.addSection("replacements");
                    _.each(spellResult.replacements, replace => {
                        this.addControl("document/spelling/replace", new Button({ label: replace, value: replace }));
                    });
                }
                if (hasSpellResultWord) {
                    const value = { word: spellResult.word, start: spellResult.start, end: spellResult.end, nodes: spellResult.nodes, target: this.docModel.getActiveTarget() };
                    this.addSection("spelling");
                    this.addControl("document/spelling/ignoreword",     new Button({ label: /*#. Context menu entry to ignore a word from spellchecking. */ gt("Ignore all"), value }));
                    this.addControl("document/spelling/userdictionary", new Button({ label: /*#. Context menu entry to add a word into the user dictionary. */ gt("Add to dictionary"), value: { ...value, updateUserDict: true } }));
                }
                // language submenu directly below spelling entries
                createLanguageSubMenu();
            }
        }

        // Normal Text
        this.addSection("text");
        const insertSubMenu = this.addControl(null, new CompoundButton(this.docView, { autoCloseParent: false, hideDisabled: true, label: INSERT_HEADER_LABEL, anchorBorder: ["right", "left"] }));
        insertSubMenu.addControl("image/insert/dialog",        new Button({ value: ImageSourceType.DRIVE, ...INSERT_IMAGE_OPTIONS }));
        insertSubMenu.addControl("textframe/insert",           new InsertTextFrameButton({ icon: null }));
        insertSubMenu.addControl("comment/insert",             new Button({ ...INSERT_COMMENT_OPTIONS, icon: null }));
        insertSubMenu.addControl("character/hyperlink/dialog", new Button(INSERT_HYPERLINK_OPTIONS));
        if (this.docModel.useSlideMode()) {
            insertSubMenu.addControl("insertSlideNumber", new Button(INSERT_SLIDE_NUMBER_OPTIONS));
            insertSubMenu.addControl("insertDateTime",    new Button(INSERT_DATE_TIME_OPTIONS));
        }

        if (!SMALL_DEVICE && this.docApp.isTextApp()) {
            this.addControl("paragraph/attrs/dialog", new Button({ label: /*#. Context menu entry to open dialog with paragraph properties. */ gt("Paragraph") }));
            this.addControl(null, new InTextStyleContextSubMenu(this.docView));
        }

        if (this.docApp.isTextApp()) {
            this.addSection("numbering");
            this.addControl("paragraph/list/startListWithOne", new Button({ label: gt("Start list with first value") }));
            this.addControl("paragraph/list/continueList",     new Button({ label: gt("Continue list numbering") }));
        }

        createLanguageSubMenu();

        // Hyperlink
        if (effectiveUrl) {
            this.addSection("hyperlink");
            this.addControl("character/hyperlink/dialog", new Button({ label: EDIT_HYPERLINK_LABEL }));
            this.addControl("character/hyperlink/remove", new Button({ label: REMOVE_HYPERLINK_LABEL }));
            this.addControl("character/hyperlink/valid",  new Button({ label: OPEN_HYPERLINK_LABEL, href: effectiveUrl }));
            this.setToolTip(_.noI18n(effectiveUrl));
        }

        // fields
        if (hasFieldGroup) {
            if (fieldManager.isSupportedFieldType()) {

                this.addSection("fields");
                if (characterAttrs.anchor) {
                    this.addControl("goToAnchor", new Button(GO_TO_ANCHOR_OPTIONS));
                }
                if (fieldManager.supportsFieldFormatting() && !this._isRangeSelected) {
                    this.addControl("editField", new Button(EDIT_FIELD_OPTIONS));
                }
                if (fieldManager.isTableOfContentsHighlighted()) {
                    this.addControl("document/inserttoc", new TocPicker({ autoCloseParent: false, hideDisabled: true, updateCaptionMode: "none", label: gt("Table of contents layouts"), tooltip: gt("Change current layout"), anchorBorder: ["right", "left"] }));
                }
                this.addControl("updateField", new Button(UPDATE_FIELD_OPTIONS));

                this.addSection("allfields");
                this.addControl("updateAllFields", new Button(UPDATE_ALL_FIELDS_OPTIONS));
            }
        }

        // change tracking
        if (hasChangeTrackingGroup) {
            this.addSection("changetrack");
            this.addControl("acceptSelectedChangeTracking", new Button(ACCEPT_CURRENT_OPTIONS));
            this.addControl("rejectSelectedChangeTracking", new Button(REJECT_CURRENT_OPTIONS));
        }
    }

    /*private*/ _prepareHyperlinkMenu() {

        const characterAttrs = getExplicitAttributes(this._node, "character", true);
        const effectiveUrl = this._isRangeSelected ? null : characterAttrs.url;

        if (effectiveUrl) {
            this.addControl("character/hyperlink/valid", new Button({ label: OPEN_HYPERLINK_LABEL, href: effectiveUrl }));
            this.setToolTip(_.noI18n(effectiveUrl));
        }
    }

    /**
     * Prepares the context menu for drawings
     */
    /*private*/ _prepareDrawingMenu() {

        this.addSection("format");
        this.addControl("drawing/format/dialog", new Button({ label: FORMAT_DRAWING_LABEL, tooltip: FORMAT_DRAWING_TOOLTIP }));

        this.addSection("delete");
        this.addControl("drawing/delete", new Button({ label: DELETE_DRAWING_LABEL, tooltip: DELETE_DRAWING_TOOLTIP }));

        this.addSection("grouping");
        this.addControl("drawing/group",   new Button({ label: GROUP_DRAWING_LABEL, tooltip: GROUP_DRAWING_TOOLTIP }));
        this.addControl("drawing/ungroup", new Button({ label: UNGROUP_DRAWING_LABEL, tooltip: UNGROUP_DRAWING_TOOLTIP }));

        this.addSection("order");
        this.addControl("drawing/order", new DrawingArrangementPicker(this.docView, { anchorBorder: ["right", "left"] }));
    }

    /**
     * Prepares the context menu for tables
     */
    /*private*/ _prepareTableMenu() {

        this.addSection("table");
        this.addControl("table/insert/row",    new Button({ label: gt("Insert row") }));
        this.addControl("table/delete/row",    new Button({ label: gt("Delete row") }));
        this.addControl("table/insert/column", new Button({ label: gt("Insert column") }));
        this.addControl("table/delete/column", new Button({ label: gt("Delete column") }));
        this.addControl("table/delete",        new Button({ label: gt("Delete table") }));

        this.addSection("insert");
        const insertMenu = this.addControl(null, new CompoundButton(this.docView, { autoCloseParent: false, hideDisabled: true, label: INSERT_HEADER_LABEL, anchorBorder: ["right", "left"] }));
        insertMenu.addControl("image/insert/dialog",        new Button({ value: ImageSourceType.DRIVE, ...INSERT_IMAGE_OPTIONS }));
        insertMenu.addControl("textframe/insert",           new InsertTextFrameButton({ icon: null }));
        insertMenu.addControl("comment/insert",             new Button({ ...INSERT_COMMENT_OPTIONS, icon: null }));
        insertMenu.addControl("character/hyperlink/dialog", new Button(INSERT_HYPERLINK_OPTIONS));
    }

    /**
     * A handler that should be invoked on contextmenu event (see in global "contextmenu.js")
     */
    /*private*/ _prepareForChromeOnOSX(event) {

        const editDiv = this.docModel.getNode();
        const selection = this.docModel.getSelection();

        const oxoPosition = getOxoPositionFromPixelPosition(editDiv, event.pageX, event.pageY);
        if (!oxoPosition) { return; }

        // click into table
        if (isPositionInTable(editDiv, oxoPosition.start)) {

            let reset = false;

            if (isParagraphNode(event.target) && isEmptyParagraph(event.target)) { reset = true; }
            if (isSpan(event.target) && isEmptySpan(event.target)) { reset = true; }
            if (isCellNode(event.target)) { reset = true; oxoPosition.start.push(0); }
            if (!selection.hasRange() && isParagraphNode(event.target)) { reset = true; } // 64433

            if (reset) {
                this._resetSelectionTo = oxoPosition.start;
                selection.setTextSelection(this._resetSelectionTo);
                return;
            }
        }

        const startPos = selection.getStartPosition();
        const endPos = selection.getEndPosition();

        // click into another paragraph
        if (_.first(oxoPosition.start) !== _.first(startPos) && _.first(oxoPosition.start) !== _.first(endPos)) {
            this._resetSelectionTo = oxoPosition.start;
            selection.setTextSelection(this._resetSelectionTo);

            // click into the same paragraph
        } else {
            // don't click on the selection
            if (!isNodePositionInsideRange(oxoPosition.start, _.last(startPos), _.last(endPos))) {
                this._resetSelectionTo = oxoPosition.start;
                selection.setTextSelection(this._resetSelectionTo);
            }
        }
    }

    /*private*/ _restoreSelectionHandler(event) {
        if (this._resetSelectionTo && (event.type === "mouseup")) {
            this.docModel.getSelection().setTextSelection(this._resetSelectionTo);
        }
        this._resetSelectionTo = null;
    }
}
