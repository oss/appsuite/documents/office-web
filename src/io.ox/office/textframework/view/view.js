/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import "@/io.ox/office/textframework/view/style.less";

import { EditView } from "@/io.ox/office/editframework/view/editview";
import { showInsertImageDialog } from "@/io.ox/office/editframework/view/editdialogs";

// class TextBaseView =========================================================

/**
 * Generic base class of a view for a document using the text framework.
 *
 * @param {EditApplication} docApp
 *  The application instance containing this view.
 *
 * @param {Editor} docModel
 *  The document model created by the passed application.
 *
 * @param {object} [config]
 *  Configuration options that will be passed to the constructor of the base
 *  class `EditView`.
 */
class TextBaseView extends EditView {

    constructor(docApp, docModel, config) {

        // base constructor
        super(docApp, docModel, config);

        // block processing of keyboard events in the document while a modal dialog is shown
        this.on("dialog:show", (_dialog, config, level) => this.#handleModalDialog(true, config, level));
        this.on("dialog:close", (_dialog, config, level) => this.#handleModalDialog(false, config, level));
    }

    // public methods ---------------------------------------------------------

    /**
     * Shows an insert image dialog, and inserts the selected image into the
     * document.
     *
     * @param {ImageSourceType} sourceType
     *  The type of the image dialog to be shown.
     *
     * @param {Object} options
     *  Optional parameters passed to the method `EditModel#insertImageURL`.
     *
     * @returns {Promise<void>}
     *  A promise that will settle after the dialog has been closed and the
     *  image has been inserted into the document.
     */
    async insertImageWithDialog(sourceType, options) {
        await showInsertImageDialog(this, sourceType, imageDesc => {
            return this.docModel.insertImageURL(imageDesc, options);
        });
    }

    /**
     * Applications supporting document margins need to overwrite this method
     * to recalculate the page element margins depending on current zoom.
     */
    recalculateDocumentMargin() {
        // intentionally empty
    }

    /**
     * Applications supporting text fields need to overwrite this method to
     * show the floating menu for text fields on the current selection.
     */
    showFieldFormatPopup() {
        // intentionally empty
    }

    /**
     * Applications supporting text fields need to overwrite this method to
     * hide the floating menu for text fields.
     */
    hideFieldFormatPopup() {
        // intentionally empty
    }

    /**
     * Applications supporting change tracking need to overwrite this method to
     * show the floating menu for change tracking on the current selection.
     */
    showChangeTrackPopup() {
        // intentionally empty
    }

    /**
     * Applications supporting change tracking need to overwrite this method to
     * hide the floating menu for change tracking.
     */
    hideChangeTrackPopup() {
        // intentionally empty
    }

    /**
     * Subclasses implement this method to scroll the application pane to make
     * the passed node visible.
     *
     * @param {HTMLElement|jQuery} _node
     *  The DOM element that will be made visible by scrolling the
     *  application pane. Must be contained somewhere in the application
     *  pane. If this object is a jQuery collection, uses the first node it
     *  contains.
     *
     * @param {Object} _options
     *  @param {Boolean} [_options.forceToTop=false]
     *   Whether the specified node shall be at the top border of the
     *   visible area of the scrolling node.
     */
    scrollToChildNode(_node, _options) {
        window.console.error("TextBaseView: called abstract method `scrollToChildNode`");
    }

    // private methods --------------------------------------------------------

    #handleModalDialog(show, config, level) {
        if (config?.blockModelProcessing && (level === 1)) {
            this.docModel.setBlockKeyboardEvent(show);
        }
    }
}

// exports ================================================================

export default TextBaseView;
