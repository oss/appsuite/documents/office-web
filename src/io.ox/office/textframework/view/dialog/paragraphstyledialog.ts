/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { str } from "@/io.ox/office/tk/algorithms";
import { createDiv, createSpan, createIcon, replaceChildren, setElementLabel } from "@/io.ox/office/tk/dom";
import { TextField } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import type { TextFrameworkDialogConfig } from "@/io.ox/office/textframework/view/labels";

import "@/io.ox/office/textframework/view/dialog/paragraphstyledialog.less";

// types ======================================================================

export interface ParagraphStyleDialogConfig extends TextFrameworkDialogConfig {

    /**
     * The initial style name to be shown in the dialog.
     */
    styleName: string;

    /**
     * A callback function that tries to resolve the passed style name to the
     * name of an existing stylesheet with a conflicting name, using some fuzzy
     * logic for lookup.
     *
     * @param styleName
     *  The name of the paragraph stylesheet entered in the dialog.
     *
     * @returns
     *  The name of a conflicting paragraph stylesheet if existing.
     */
    styleResolver(styleName: string): Opt<string>;

    /**
     * A callback function invoked when the OK button of the dialog has been
     * clicked. If the function returns a promise that will be rejected, or
     * throws an exception, the dialog will be kept open.
     *
     * @param styleName
     *  The name of the paragraph stylesheet entered in the dialog.
     */
    actionHandler(styleName: string): MaybeAsync;
}

// class ParagraphStyleDialog =================================================

export class ParagraphStyleDialog extends BaseDialog<void, ParagraphStyleDialogConfig> {

    // constructor ------------------------------------------------------------

    public constructor(config: ParagraphStyleDialogConfig) {

        // base constructor
        super({ ...config, id: "io-ox-office-paragraph-style-dialog" });

        // create the text input field for the style name
        const nameField = new TextField(config.styleName, { classes: "form-group", maxLength: 100 });

        // render the text control, and take ownership
        this.renderControl(this.bodyNode, nameField);

        // add the warning label to the dialog body
        const warningNode = createDiv("warning-label text-warning");
        nameField.el.append(warningNode);

        // update the warning label after every change of the text input field
        this.addOkValidator(() => {

            // try to find a conflicting stylesheet, update warning text
            const warningText = this.#getStyleNameWarning(nameField.rawText);
            if (warningText) {
                replaceChildren(warningNode,
                    createIcon("bi:exclamation-triangle-fill"),
                    createSpan({ label: warningText })
                );
            } else {
                setElementLabel(warningNode, str.noI18n("\xa0"));
            }

            // silently disallow empty textfield
            return !!nameField.rawText && !warningText;
        });

        // call action handler when pressing OK button
        this.setOkHandler(() => config.actionHandler.call(this, nameField.value));
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a warning text to be shown in the dialog, if the passed style
     * name is invalid.
     */
    #getStyleNameWarning(styleName: string): Opt<string> {

        // bug 56349: do not allow style name consisting entirely of whitespace
        if (/^\s+$/.test(styleName)) {
            return gt("Invalid name for a paragraph style.");
        }

        // use passed style resolver to find a conflicting stylesheet
        const otherName = this.config.styleResolver(styleName);
        if (otherName) {
            //#. warning text shown in the text input dialog to create new paragraph style
            //#. "%1$s" is the name of the existing paragraph style
            return gt('Paragraph style "%1$s" exists already.', otherName);
        }

        // style name is valid
        return undefined;
    }
}
