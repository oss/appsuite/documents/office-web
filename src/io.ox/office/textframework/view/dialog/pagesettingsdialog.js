/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import '@/io.ox/office/textframework/view/dialog/pagesettingsdialog.less';

import { createElement, createDiv, createFieldSet, createLabel } from '@/io.ox/office/tk/dom';
import { DropdownList } from '@/io.ox/office/tk/form';
import { LengthField } from '@/io.ox/office/tk/controls';
import { BaseDialog } from '@/io.ox/office/tk/dialogs';

// constants ==============================================================

// generic labels
var PAGE_WIDTH_LABEL = /*#. Defining the page width */ gt('Width');
var PAGE_HEIGHT_LABEL = /*#. Defining the page height */ gt('Height');
var MARGIN_LEFT_LABEL = /*#. Defining the left page margin */ gt('Left');
var MARGIN_TOP_LABEL = /*#. Defining the top page margin */ gt('Top');
var MARGIN_RIGHT_LABEL = /*#. Defining the right page margin */ gt('Right');
var MARGIN_BOTTOM_LABEL = /*#. Defining the bottom page margin */ gt('Bottom');

// labels for OX Text page settings
var PAGE_SETTINGS_LABEL = /*#. Defining the page extents and page margins */ gt('Page settings');
var PAGE_FORMAT_LABEL = /*#. Predefined standard page formats, e.g. A4 */ gt('Paper format');
var PAGE_ORIENT_LABEL = /*#. Orientation of the page, e.g. Portrait or Landscape */ gt('Paper orientation');
var PAGE_MARGINS_LABEL = /*#. Predefined page margins for the edges of a paper */ gt('Page margins');
var PAGE_SIZE_LABEL = /*#. Defining the page extents */ gt('Paper size');

// labels for OX Presentation slide settings
var SLIDE_SETTINGS_LABEL = /*#. Defining the slide extents*/ gt('Slide settings');
var SLIDE_FORMAT_LABEL = /*#. Predefined standard slide formats, e.g. A4*/ gt('Slide format');
var SLIDE_ORIENT_LABEL = /*#. Orientation of the slide, e.g. Portrait or Landscape*/ gt('Orientation');
var SLIDE_SIZE_LABEL = /*#. Defining the slide extents*/ gt('Slide size');

// page format settings for OX Text
var PAGE_FORMAT_ENTRIES = [
    { value: 'letter',    attrs: { width: 21590, height: 27940 }, label: /*#. Predefined 'Letter' page format*/ gt('Letter') },
    { value: 'tabloid',   attrs: { width: 27900, height: 43200 }, label: /*#. Predefined 'Tabloid' page format*/ gt('Tabloid') },
    { value: 'legal',     attrs: { width: 21590, height: 35560 }, label: /*#. Predefined 'Legal' page format*/ gt('Legal') },
    { value: 'statement', attrs: { width: 14000, height: 21600 }, label: /*#. Predefined 'Statement' page format*/ gt('Statement') },
    { value: 'executive', attrs: { width: 18400, height: 26700 }, label: /*#. Predefined 'Executive' page format*/ gt('Executive') },
    { value: 'folio',     attrs: { width: 21600, height: 33000 }, label: /*#. Predefined 'Folio' page format*/ gt('Folio') },
    { value: 'a3',        attrs: { width: 29700, height: 42000 }, label: /*#. Predefined 'A3' page format*/ gt('A3') },
    { value: 'a4',        attrs: { width: 21000, height: 29700 }, label: /*#. Predefined 'A4' page format*/ gt('A4') },
    { value: 'a5',        attrs: { width: 14800, height: 21000 }, label: /*#. Predefined 'A5' page format*/ gt('A5') },
    { value: 'b4',        attrs: { width: 25000, height: 35300 }, label: /*#. Predefined 'B4' page format*/ gt('B4') },
    { value: 'b5',        attrs: { width: 17600, height: 25000 }, label: /*#. Predefined 'B5' page format*/ gt('B5') }
];

// slide format settings for OX Presentation (default list for OOXML)
var SLIDE_FORMAT_ENTRIES_OOX = [
    { value: 'screen_4_by_3',     attrs: { width: 19050, height: 25400 }, label: /*#. Predefined 'Standard (4:3)' slide format for presentations */ gt('Standard (4:3)') },
    { value: 'screen_16_by_9',    attrs: { width: 14290, height: 25400 }, label: /*#. Predefined 'Widescreen (16:9)' slide format for presentations */ gt('Widescreen (16:9)') },
    { value: 'screen_16_by_10',   attrs: { width: 15880, height: 25400 }, label: /*#. Predefined 'Widescreen (16:10)' slide format for presentations */ gt('Widescreen (16:10)') },
    { value: 'screen_16_by_9_hd', attrs: { width: 19050, height: 33870 }, label: /*#. Predefined 'Widescreen HD (16:9)' slide format for presentations */ gt('Widescreen HD (16:9)') }
];

// ODF slide size overrides for OX Presentation
var SLIDE_SIZE_OVERRIDES_ODF = {
    screen_4_by_3:     { width: 21000, height: 28000 },
    screen_16_by_9:    { width: 15750, height: 28000 },
    screen_16_by_10:   { width: 17500, height: 28000 },
    screen_16_by_9_hd: { width: 21000, height: 37340 }
};

var SLIDE_FORMAT_ENTRIES_ODF = SLIDE_FORMAT_ENTRIES_OOX.map(function (entry) {
    entry = _.copy(entry, true);
    _.assign(entry.attrs, SLIDE_SIZE_OVERRIDES_ODF[entry.value]);
    return entry;
});

// entries for page orientation dropdown
var PAGE_ORIENT_ENTRIES = [
    { value: 'portrait',  label: /*#. The 'Portrait' page orientation*/ gt('Portrait') },
    { value: 'landscape', label: /*#. The 'Landscape' page orientation*/ gt('Landscape') }
];

// entries for page margins dropdown
var PAGE_MARGINS_ENTRIES = [
    { value: 'narrow', attrs: { marginLeft: 1270, marginTop: 1270, marginRight: 1270, marginBottom: 1270 }, label: /*#. A set of predefined, 'Narrow' page margins*/ gt('Narrow') },
    { value: 'normal', attrs: { marginLeft: 2540, marginTop: 2540, marginRight: 2540, marginBottom: 2540 }, label: /*#. A set of predefined, 'Normal' page margins*/ gt('Normal') },
    { value: 'wide',   attrs: { marginLeft: 5080, marginTop: 5080, marginRight: 5080, marginBottom: 5080 }, label: /*#. A set of predefined, 'Wide' page margins*/ gt('Wide') }
];

// list entry for dropdown menus
var CUSTOM_VALUE = 'custom';
var CUSTOM_ENTRY = { value: CUSTOM_VALUE, label: /*#. A custom extent */ gt('Custom') };

// minimum/maximum margins (in 1/100 mm)
var MIN_MARGIN = 0;
var MAX_MARGIN = 5000;

// private functions ======================================================

/**
 * Returns a dropdown menu entry from the passed list whose attributes
 * match the passed page attributes with values in 1/100 of millimeters.
 * Uses fuzzy matching (the attribute values are allowed to differ by at
 * most 0.1 millimeters).
 *
 * @param {DropdownListEntry[]} listEntries
 *  The list of dropdown menu entries.
 *
 * @param {Dict<number>} pageAttrs
 *  The page attributes to be searched.
 *
 * @returns {Opt<DropdownListEntry>}
 *  A matching dropdown list entry if available.
 */
function findMatchingEntry(listEntries, pageAttrs) {
    return _.find(listEntries, function (entry) {
        return _.every(entry.attrs, function (value, name) {
            return Math.abs(pageAttrs[name] - value) <= 10;
        });
    });
}

function swapSizeAttrs(attrs) {
    return { width: attrs.height, height: attrs.width };
}

// class PageSettingsDialog ===============================================

/**
 * The page settings modal dialog.
 *
 * Shows the currently set page width and page height, the page margins and some
 * other page attributes directly after executing the dialog.
 * Depending on the given values, a predefined page format as well as a predefined
 * set of page margins is tried to be determined and selected/shown accordingly.
 * The page attributes can be changed by various dropdown menu and spinfield controls.
 * The changes will be applied to the current document after clicking the 'Ok' button.
 *
 * The dialog itself is shown by calling this.execute. The caller can react on successful
 * finalization of the dialog within an appropriate done-Handler, chained to the
 * this.execute function call.
 *
 * @param {TextBaseView} docView
 *  The document view containing this dialog.
 */
class PageSettingsDialog extends BaseDialog {

    constructor(docView) {

        // document model and application
        var docApp = docView.docApp;
        var docModel = docView.docModel;
        var slideMode = docApp.isPresentationApp();
        var odf = docApp.isODF();

        // whether to show the page margin controls
        var pageMargins = !slideMode;

        // base constructor
        super({
            id: 'io-ox-office-page-settings-dialog',
            title: slideMode ? SLIDE_SETTINGS_LABEL : PAGE_SETTINGS_LABEL,
            width: pageMargins ? 450 : 350,
            focus: 'input[data-name="width"]',
            blockModelProcessing: true
        });

        var self = this;

        // the page attributes
        var initPageAttrs = _.copy(docModel.pageStyles.getElementAttributes(docModel.getNode()).page, true);
        var pageAttrs = _.copy(initPageAttrs, true);

        // "Page format" dropdown menu
        var pageFormatEntries = slideMode ? (odf ? SLIDE_FORMAT_ENTRIES_ODF : SLIDE_FORMAT_ENTRIES_OOX) : PAGE_FORMAT_ENTRIES;
        var pageFormatLabel = slideMode ? SLIDE_FORMAT_LABEL : PAGE_FORMAT_LABEL;
        var pageFormatList = new DropdownList(pageFormatEntries, 'a4', { type: "link", classes: "page-format", label: _.noI18n(pageFormatLabel + ':'), undef: CUSTOM_ENTRY });

        // "Page orientation" dropdown menu
        var pageOrientLabel = slideMode ? SLIDE_ORIENT_LABEL : PAGE_ORIENT_LABEL;
        var pageOrientList = new DropdownList(PAGE_ORIENT_ENTRIES, 'portrait', { type: "link", classes: "page-orientation", label: _.noI18n(pageOrientLabel + ':') });

        // "Page margins" dropdown menu
        var pageMarginsList = pageMargins ? new DropdownList(PAGE_MARGINS_ENTRIES, 'normal', { type: "link", classes: "page-margins", label: _.noI18n(PAGE_MARGINS_LABEL + ':'), undef: CUSTOM_ENTRY }) : null;

        // the spinfield controls
        var widthSpinner = null;
        var heightSpinner = null;
        var marginTopSpinner = null;
        var marginBottomSpinner = null;
        var marginLeftSpinner = null;
        var marginRightSpinner = null;

        // private methods ----------------------------------------------------

        /**
         * Updates the dropdown lists according to the spinfield values.
         */
        function updateDropdownLists() {

            // update page orientation according to page size
            if (slideMode || _.isUndefined(pageAttrs.orientation)) { pageAttrs.orientation = (pageAttrs.width >= pageAttrs.height) ? 'landscape' : 'portrait'; }
            pageOrientList.value = pageAttrs.orientation;

            // update page format preset in dropdown menu
            var isPortrait = pageAttrs.orientation === 'portrait';
            var matchAttrs = isPortrait ? pageAttrs : swapSizeAttrs(pageAttrs);
            var pageFormatEntry = findMatchingEntry(pageFormatEntries, matchAttrs);
            pageFormatList.value = pageFormatEntry ? pageFormatEntry.value : CUSTOM_VALUE;

            // update page margins preset in dropdown menu
            if (pageMarginsList) {
                var pageMarginsEntry = findMatchingEntry(PAGE_MARGINS_ENTRIES, pageAttrs);
                pageMarginsList.value = pageMarginsEntry ? pageMarginsEntry.value : CUSTOM_VALUE;
            }
        }

        /**
         * Sets the current page width and page height
         * values for the given pageFormat object and
         * updates the corresponding controls.
         *
         * @param {NodeOrJQuery} parent
         *  The parent element to insert the spin field into.
         *
         * @param {string} name
         *  The internal name of the spin field.
         *
         * @param {string} label
         *  The text label in front of the spin field.
         *
         * @param {number} min
         *  The minimum value allowed in the spin field.
         *
         * @param {number} max
         *  The maximum value allowed in the spin field.
         *
         * @returns {LengthField}
         *  The new spinfield control, with the additional public property
         *  `name` containing the name of the page attribute.
         */
        function createSpinner(parent, name, label, min, max) {

            // the container for the label and spinfield
            var $box = $('<div class="spinner-box">').appendTo(parent);

            // create the spinfield control
            const unitField = self.member(new LengthField({ min, max }));
            const ctrlId = `io-ox-office-control-${unitField.uid}`;
            unitField.$input.attr({ id: ctrlId, 'data-name': name });
            unitField.name = name;
            unitField.setValue(initPageAttrs[name]);

            // insert label and spinfield into the container
            $box.append(createLabel({ label, for: ctrlId }), unitField.$el);

            // commit dialog on pressing ENTER inside the spinfield
            self.registerEnterGroup(unitField);

            // update other dialog settings when the spinfield value changes
            unitField.on('group:commit', function (value) {
                pageAttrs[name] = value;
                updateDropdownLists();
            });

            return unitField;
        }

        /**
         * Updates the values of all spinfields according to the current values
         * of the associated page attributes.
         */
        function updateSpinners() {

            function updateSpinner(spinner) {
                spinner.setValue(pageAttrs[spinner.name]);
            }

            updateSpinner(widthSpinner);
            updateSpinner(heightSpinner);

            if (pageMargins) {
                updateSpinner(marginLeftSpinner);
                updateSpinner(marginTopSpinner);
                updateSpinner(marginRightSpinner);
                updateSpinner(marginBottomSpinner);
            }
        }

        /**
         * Creates and layouts all dialog controls, contained within the body of the
         * dialog.
         * This function, in conjunction with the dialogs' less file,
         * determines the general visual layout and functionality of the dialog
         */
        function initControls() {

            const optionsFieldSet = self.bodyNode.appendChild(createFieldSet());

            // initialize the page/slide format dropdown menu
            self.renderControl(optionsFieldSet, pageFormatList);
            pageFormatList.onChange(function (value) {
                if (!pageAttrs.orientation) { pageAttrs.orientation = slideMode ? 'landscape' : 'portrait'; }
                var isPortrait = pageAttrs.orientation === 'portrait';
                var entry = pageFormatList.getEntryOf(value);
                if (entry) {
                    _.extend(pageAttrs, isPortrait ? entry.attrs : swapSizeAttrs(entry.attrs));
                    // update the dropdown menus (page orientation)
                    updateDropdownLists();
                    updateSpinners();
                }
            });

            // initialize the page orientation dropdown menu
            self.renderControl(optionsFieldSet, pageOrientList);
            pageOrientList.onChange(function (value) {
                if ((value !== pageAttrs.orientation) && (
                    ((value === 'portrait') && (pageAttrs.width > pageAttrs.height)) ||
                    ((value === 'landscape') && (pageAttrs.width < pageAttrs.height))
                )) {
                    var tmp = pageAttrs.width;
                    pageAttrs.width = pageAttrs.height;
                    pageAttrs.height = tmp;
                    pageAttrs.orientation = value;
                    updateSpinners();
                }
            });

            // initialize the page margins dropdown menu
            if (pageMarginsList) {
                self.renderControl(optionsFieldSet, pageMarginsList);
                pageMarginsList.onChange(function (value) {
                    var entry = pageMarginsList.getEntryOf(value);
                    if (entry) {
                        _.extend(pageAttrs, entry.attrs);
                        updateSpinners();
                    }
                });
            }

            // separator line between dropdowns and spinfields
            self.bodyNode.appendChild(createElement('hr'));

            // container for page size spinfields
            var pageSizeBox = self.bodyNode.appendChild(createFieldSet({
                classes: 'spinner-fieldset',
                legend: slideMode ? SLIDE_SIZE_LABEL : PAGE_SIZE_LABEL,
                indent: true
            }));
            var boxCol = pageSizeBox.appendChild(createDiv('spinner-column'));

            // create the page size spinfields
            var minPageSize = slideMode ? 2540 : 10000;
            var maxPageSize = slideMode ? 142240 : 55000;
            widthSpinner  = createSpinner(boxCol, 'width',  PAGE_WIDTH_LABEL,  minPageSize, maxPageSize);
            heightSpinner = createSpinner(boxCol, 'height', PAGE_HEIGHT_LABEL, minPageSize, maxPageSize);

            // page margin spinfields
            if (pageMargins) {

                // separator line between size and margin spinfields
                self.bodyNode.appendChild(createElement('hr'));

                // container for page margin spinfields
                var pageMarginsBox = self.bodyNode.appendChild(createFieldSet({
                    classes: 'spinner-fieldset',
                    legend: PAGE_MARGINS_LABEL,
                    indent: true
                }));
                var boxCol1 = pageMarginsBox.appendChild(createDiv('spinner-column'));
                var boxCol2 = pageMarginsBox.appendChild(createDiv('spinner-column'));

                // create page margin spinfields
                marginTopSpinner    = createSpinner(boxCol1, 'marginTop',    MARGIN_TOP_LABEL,    MIN_MARGIN, MAX_MARGIN);
                marginBottomSpinner = createSpinner(boxCol1, 'marginBottom', MARGIN_BOTTOM_LABEL, MIN_MARGIN, MAX_MARGIN);
                marginLeftSpinner   = createSpinner(boxCol2, 'marginLeft',   MARGIN_LEFT_LABEL,   MIN_MARGIN, MAX_MARGIN);
                marginRightSpinner  = createSpinner(boxCol2, 'marginRight',  MARGIN_RIGHT_LABEL,  MIN_MARGIN, MAX_MARGIN);
            }

            // initialize the dropdown menus according to the page attributes
            updateDropdownLists();
        }

        // initialization -----------------------------------------------------

        // create the layout of the dialog
        initControls();

        // handler for the OK button
        this.setOkHandler(() => docModel.setPaperFormat(pageAttrs, initPageAttrs));
    }
}

// exports ================================================================

export default PageSettingsDialog;
