/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { SMALL_DEVICE, convertCssLengthToHmm, convertHmmToLength, convertLengthToHmm } from '@/io.ox/office/tk/utils';
import { createLabel } from '@/io.ox/office/tk/dom';
import { BaseDialog } from '@/io.ox/office/tk/dialogs';

import { getExplicitAttributes, getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';

import { calculateBitmapSettings } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { CROPPING_RESIZER_SELECTOR } from '@/io.ox/office/drawinglayer/view/drawingframe';

import { SET_ATTRIBUTES } from '@/io.ox/office/textframework/utils/operations';
import { getPixelPositionOfSelectionInSelectionLayer, getPixelPositionToRootNodeOffset } from '@/io.ox/office/textframework/utils/position';
import { isImageNode } from '@/io.ox/office/textframework/utils/dom';
import { calculateCropOperation } from '@/io.ox/office/textframework/components/drawing/imagecropframe';
import { LengthField } from '@/io.ox/office/textframework/view/controls';

import '@/io.ox/office/textframework/view/dialog/imagecropdialog.less';

// constants ==============================================================

var imageWidthTooltip = /*#. Tooltip message for setting image width*/ gt('Set image width');
var imageHeightTooltip = /*#. Tooltip message for setting image height*/ gt('Set image height');
var imageHorzOffTooltip = /*#. Tooltip message for setting horizontal offset of the image*/ gt('Set horizontal offset');
var imageVertOffTooltip = /*#. Tooltip message for setting vertical offset of the image*/ gt('Set vertical offset');
var cropWidthTooltip = /*#. Tooltip message for setting width of cropping frame*/ gt('Set crop position width');
var cropHeightTooltip = /*#. Tooltip message for setting height of cropping frame*/ gt('Set crop position height');
var cropLeftTooltip = /*#. Tooltip message for setting left position of cropping frame*/ gt('Set left position');
var cropTopTooltip = /*#. Tooltip message for setting top position of cropping frame*/ gt('Set top position');

var MIN_SIZE = 0;
var MAX_SIZE = 30000;
var MIN_OFFSET = -30000;
var MAX_OFFSET = 30000;

// class ImageCropDialog ==================================================

/**
 * The image crop properties dialog.
 * Provides different cropping properties.
 *
 *
 * The changes will be applied to the current document after clicking the 'Ok' button.
 *
 * The dialog itself is shown by calling this.execute. The caller can react on successful
 * finalization of the dialog within an appropriate done-Handler, chained to the
 * this.execute function call.
 *
 * @param {EditView} docView
 *  The document view containing this dialog instance.
 */
class ImageCropDialog extends BaseDialog {

    constructor(docView) {

        // base constructor ---------------------------------------------------
        super({
            id: 'io-ox-office-image-crop-dialog',
            //#. Dialog title: Change cropping properties of image
            title: gt('Crop position'),
            width: SMALL_DEVICE ? 260 : 300,
            movable: true,
            blockModelProcessing: true
        });

        var self = this;
        var docModel = docView.docModel;
        var isTextApp = docView.docApp.isTextApp();

        // collection of values and spin fields in dialog, mapped by attribute name
        var map = {};

        // the drawing frame of the image object to be cropped
        var imageFrame = docModel.getSelection().getSelectedDrawing();

        // private methods ----------------------------------------------------

        function createSpinField(attrName, initValue, min, max, label, tooltip, controlRootNode, linkedAttr) {

            const spinField = new LengthField({ tooltip, min, max });
            const ctrlId = `io-ox-office-control-${spinField.uid}`;
            spinField.$input.attr("id", ctrlId);

            var entry = map[attrName] = {
                attrName,
                spinField,
                initValue,
                currValue: initValue
            };

            spinField.setValue(initValue);

            spinField.on('group:commit', function (value) {

                var diff = value - entry.currValue;
                if (diff === 0) { return; }

                entry.currValue = value;

                var linkedEntry = linkedAttr ? map[linkedAttr] : null;
                if (linkedEntry) {
                    if ((attrName === 'cropWidth') || (attrName === 'cropHeight')) { diff /= 2; }
                    var newLinkedValue = linkedEntry.currValue - diff;
                    linkedEntry.spinField.setValue(newLinkedValue);
                    linkedEntry.currValue = newLinkedValue;
                }

                updateImagePosition();
            });

            self.registerEnterGroup(spinField);

            var labelNode = createLabel({ classes: 'spin-field-label', label, for: ctrlId });
            var containerNode = $('<div class="ind-wrapper">').append(labelNode, spinField.$el);
            controlRootNode.append(containerNode);

            return spinField;
        }

        function getVal(attrName) {
            return map[attrName].currValue;
        }

        function isChanged(attrName) {
            var entry = map[attrName];
            return entry.initValue !== entry.currValue;
        }

        function hasAnyChanges() {
            return _.some(map, function (entry) { return entry.initValue !== entry.currValue; });
        }

        /**
         * Verifies that the current document selection still contains the
         * image object handled by this dialog.
         *
         * @returns {boolean}
         *  Whether the current document selection still contains the image
         *  object handled by this dialog.
         */
        function hasOriginalImageSelection() {
            var drawingFrame = docModel.getSelection().getSelectedDrawing()[0];
            return !!drawingFrame && imageFrame.is(drawingFrame);
        }

        function updateImagePosition(options) {

            var drawing = docModel.getSelection().getSelectedDrawing();
            var drawingAttrs = getExplicitAttributes(drawing, 'drawing');

            if (options && options.reset) {
                _.forEach(map, function (entry) { entry.currValue = entry.initValue; });
            }

            var cropWidth = getVal('cropWidth');
            var cropHeight = getVal('cropHeight');
            var cropProps = calculateCropOperation(cropWidth, cropHeight, getVal('imageWidth'),  getVal('imageHeight'),  getVal('offsetX'),  getVal('offsetY'));
            if (cropProps && !_.isEmpty(cropProps)) {
                var horzSettings = calculateBitmapSettings(docModel.getApp(), cropWidth, cropProps.cropLeft, cropProps.cropRight);
                var vertSettings = calculateBitmapSettings(docModel.getApp(), cropHeight, cropProps.cropTop, cropProps.cropBottom);
                var imgSelector = 'img:not("' + CROPPING_RESIZER_SELECTOR + '")'; // DOCS-3017

                imageFrame.find(imgSelector).css({
                    left: horzSettings.offset,
                    top: vertSettings.offset,
                    width: horzSettings.size,
                    height: vertSettings.size
                });
            }

            var drawingCssPros = {
                width: convertHmmToLength(cropWidth, 'px'),
                height: convertHmmToLength(cropHeight, 'px')
            };

            if (!drawingAttrs.inline) {
                var leftVal = getVal('left');
                var topVal = getVal('top');
                if (drawingAttrs.anchorVertBase === 'paragraph' && isTextApp) {
                    var pageAttrs  = _.copy(docModel.pageStyles.getElementAttributes(docModel.getNode()).page, true);
                    leftVal -= pageAttrs.marginLeft;
                    topVal -= pageAttrs.marginTop;
                }
                drawingCssPros.left =  convertHmmToLength(leftVal, 'px');
                drawingCssPros.top =  convertHmmToLength(topVal, 'px');
            }

            imageFrame.css(drawingCssPros);
            var drawingSelectionNode = imageFrame.data('selection');
            if (drawingSelectionNode) {
                if (isTextApp) {
                    docModel.getSelection().updateOverlaySelection(imageFrame);
                } else {
                    drawingSelectionNode.css(drawingCssPros);
                }
            }
        }

        /**
         * Initialize controls of the dialog.
         */
        function initControls() {
            var explAttrs = getExplicitAttributeSet(imageFrame);
            var imageAttrs = explAttrs.image;
            var drawingAttrs = explAttrs.drawing;
            var cropLeft = (imageAttrs && imageAttrs.cropLeft) || 0;
            var cropRight = (imageAttrs && imageAttrs.cropRight) || 0;
            var cropTop = (imageAttrs && imageAttrs.cropTop) || 0;
            var cropBottom = (imageAttrs && imageAttrs.cropBottom) || 0;
            var horzSettings = calculateBitmapSettings(docModel.getApp(), explAttrs.drawing.width, cropLeft, cropRight);
            var vertSettings = calculateBitmapSettings(docModel.getApp(), explAttrs.drawing.height, cropTop, cropBottom);
            var iniImageWidth = convertCssLengthToHmm(horzSettings.size);
            var iniImageHeight = convertCssLengthToHmm(vertSettings.size);
            var iniImageX = iniImageWidth * (cropRight - cropLeft) / 200;
            var iniImageY = iniImageHeight * (cropBottom - cropTop) / 200;

            // image spinner fields
            var imageContainer = $('<div class="image-pos-area">').append($('<h4 class="dialog-heading">').text(gt('Image position')));
            createSpinField('imageWidth', iniImageWidth, MIN_SIZE, MAX_SIZE, gt('Width'), imageWidthTooltip, imageContainer);
            createSpinField('imageHeight', iniImageHeight, MIN_SIZE, MAX_SIZE, gt('Height'), imageHeightTooltip, imageContainer);
            createSpinField('offsetX', iniImageX, MIN_OFFSET, MAX_OFFSET, gt('Offset X'), imageHorzOffTooltip, imageContainer);
            createSpinField('offsetY', iniImageY, MIN_OFFSET, MAX_OFFSET, gt('Offset Y'), imageVertOffTooltip, imageContainer);

            // crop spinner fields
            var cropContainer = $('<div class="crop-pos-area">').append($('<h4 class="dialog-heading">').text(gt('Crop frame position')));
            createSpinField('cropWidth', drawingAttrs.width, MIN_SIZE, MAX_SIZE, gt('Width'), cropWidthTooltip, cropContainer, 'offsetX');
            createSpinField('cropHeight', drawingAttrs.height, MIN_SIZE, MAX_SIZE, gt('Height'), cropHeightTooltip, cropContainer, 'offsetY');

            // inline - left and top are disabled
            if (!drawingAttrs.inline) {
                // paragraph aligned
                var leftValue, topValue;
                if (drawingAttrs.anchorVertBase !== 'page' && isTextApp) {
                    var pageAttrs = docModel.pageStyles.getElementAttributes(docModel.getNode()).page;
                    leftValue = drawingAttrs.anchorHorOffset + pageAttrs.marginLeft;
                    topValue = drawingAttrs.anchorVertOffset + pageAttrs.marginTop;
                } else if (drawingAttrs.anchorVertBase === 'page' && isTextApp) {
                    leftValue = drawingAttrs.anchorHorOffset;
                    topValue = drawingAttrs.anchorVertOffset;
                } else { // default absolute position
                    leftValue = drawingAttrs.left;
                    topValue = drawingAttrs.top;
                }
                createSpinField('left', leftValue, MIN_OFFSET, MAX_OFFSET, gt('Left'), cropLeftTooltip, cropContainer, 'offsetX');
                createSpinField('top', topValue, MIN_OFFSET, MAX_OFFSET, gt('Top'), cropTopTooltip, cropContainer, 'offsetY');
            }

            // append all containers to control area
            var mainControlArea = $('<div class="control-area">').append(imageContainer, cropContainer);
            self.$bodyNode.append(mainControlArea);
        }

        // initialization -----------------------------------------------------

        // sanity check
        if (!isImageNode(imageFrame)) {
            throw new Error('ImageCropDialog.constructor(): invalid document selection, image object expected');
        }

        // create the layout of the dialog
        initControls();

        // handler for the OK button
        this.setOkHandler(function () {

            // OT: selected image may have been deleted by concurrent editor
            if (!hasOriginalImageSelection()) { return; }

            if (!hasAnyChanges()) { return; }

            var cropWidth = getVal('cropWidth');
            var cropHeight = getVal('cropHeight');
            var imageAttrs = calculateCropOperation(cropWidth, cropHeight, getVal('imageWidth'),  getVal('imageHeight'),  getVal('offsetX'),  getVal('offsetY'));
            if (_.isEmpty(imageAttrs)) { return; }

            var drawingFrame = docModel.getSelection().getSelectedDrawing();
            var oldDrawingAttrs = getExplicitAttributes(drawingFrame, 'drawing');
            var pageAligned = isTextApp && (oldDrawingAttrs.anchorVertBase === 'page');
            var isInline = oldDrawingAttrs.inline;

            var drawingAttrs = {};
            if (isChanged('cropWidth')) { drawingAttrs.width = cropWidth; }
            if (isChanged('cropHeight')) { drawingAttrs.height = cropHeight; }

            if (isTextApp && !pageAligned && !isInline) { // OX Text: sometimes convert paragraph aligned drawing to page aligned
                if (isChanged('left') || isChanged('top')) { // -> only change the position (and alignment), if it is really required
                    var zoomPercentage = docView.getZoomFactor() * 100;
                    var keepParagraphAligned = false;
                    var paragraphOffset = null;
                    if (oldDrawingAttrs.anchorVertBase === 'paragraph') {
                        var paragraphNode = drawingFrame.closest('.p');
                        paragraphOffset = getPixelPositionToRootNodeOffset(paragraphNode, drawingFrame, zoomPercentage);
                        keepParagraphAligned = paragraphOffset.x >= 0 && paragraphOffset.y >= 0; // no negative shift -> keep the drawing paragraph aligned
                    }
                    if (keepParagraphAligned) {
                        drawingAttrs.anchorHorOffset = convertLengthToHmm(paragraphOffset.x, 'px');
                        drawingAttrs.anchorVertOffset = convertLengthToHmm(paragraphOffset.y, 'px');
                        drawingAttrs.anchorHorBase = 'column';
                        drawingAttrs.anchorVertBase = 'paragraph';
                    } else {
                        var pos = getPixelPositionOfSelectionInSelectionLayer(docModel.getRootNode(), docModel.getNode(), drawingFrame, zoomPercentage);
                        drawingAttrs.anchorHorOffset = convertLengthToHmm(pos.x, 'px');
                        drawingAttrs.anchorVertOffset = convertLengthToHmm(pos.y, 'px');
                        drawingAttrs.anchorHorBase = 'page';
                        drawingAttrs.anchorVertBase = 'page';
                    }
                    drawingAttrs.anchorHorAlign = 'offset';
                    drawingAttrs.anchorVertAlign = 'offset';
                    drawingAttrs.inline = false;
                }
            } else if (isTextApp && pageAligned && !isInline) {
                if (isChanged('left')) { drawingAttrs.anchorHorOffset = getVal('left'); }
                if (isChanged('top')) { drawingAttrs.anchorVertOffset = getVal('top'); }
            } else if (!isInline) {
                if (isChanged('left')) { drawingAttrs.left = getVal('left'); }
                if (isChanged('top')) { drawingAttrs.top = getVal('top'); }
            }

            var properties = {
                start: docModel.getSelection().getStartPosition(),
                attrs: { drawing: drawingAttrs, image: imageAttrs }
            };
            var target = docModel.getActiveTarget();
            if (target) { properties.target = target; }

            var generator = docModel.createOperationGenerator();
            generator.generateOperation(SET_ATTRIBUTES, properties);
            docModel.applyOperations(generator);
        });

        // on cancel restore original values
        this.setCancelHandler(function () {
            if (hasAnyChanges()) {
                updateImagePosition({ reset: true });
            }
        });

        // OT: close this dialog immediately if the document selection does not
        // contain the handled image object anymore
        this.listenTo(docModel.getSelection(), 'change', function () {
            if (!hasOriginalImageSelection()) { self.close(); }
        });
    }
}

// exports ================================================================

export default ImageCropDialog;
