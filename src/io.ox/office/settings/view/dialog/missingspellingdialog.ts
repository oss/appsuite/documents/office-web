/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { createFieldSet } from "@/io.ox/office/tk/dom";
import { type CheckListEntry, CheckList } from "@/io.ox/office/tk/form";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { type LanguageWithoutSpelling, spellingSettings } from "@/io.ox/office/settings/api";

import "@/io.ox/office/settings/view/dialog/missingspellingdialog.less";

// types ======================================================================

interface ListEntry extends CheckListEntry<string> {
    checked: boolean;
}

// class MissingSpellingDialog ================================================

/**
 * A modal dialog that lists all known languages without spelling support in a
 * checklist. Checked languages will not be mentioned to the user in
 * notifications about missing spelling support.
 */
export default class MissingSpellingDialog extends BaseDialog<void> {

    public constructor() {

        super({
            id: "io-ox-office-settings-missing-spelling-dialog",
            //#. Button label for the save button for the user dictionary dialog.
            okLabel: gt("Save"),
            //#. Title of the user's spell-check language blocklist dialog.
            title: gt("Unsupported spell checker notification")
        });

        // the fieldset container for the checkboxes
        const fieldSet = this.bodyNode.appendChild(createFieldSet({
            //#. a list of languages without spellcheckig support a user wishes to be informed about
            legend: gt("Suppress missing spell checker warnings for the following languages:"),
            indent: true
        }));

        // collect and prepare all languages to be shown in the checklist
        const listEntries = spellingSettings.getLanguagesWithoutSpelling().map((entry): Opt<ListEntry> => {
            const name = LOCALE_DATA.getLanguageAndRegionName(entry.lang, true);
            // checkbox will be checked for language *without* notification
            return name ? { value: entry.lang, label: name, checked: !entry.notify } : undefined;
        }).filter(Boolean);

        // create and render the checklist control
        const blockedLangs = listEntries.filter(entry => entry.checked).map(entry => entry.value);
        const checkList = new CheckList(listEntries, blockedLangs, { sort: true, checkAll: true });
        this.renderControl(fieldSet, checkList);

        // action handler for the OK button
        this.setOkHandler(() => {
            const langEntries: LanguageWithoutSpelling[] = listEntries.map(entry => ({ lang: entry.value, notify: !checkList.value.includes(entry.value) }));
            spellingSettings.setLanguagesWithoutSpelling(langEntries);
        });
    }
}
