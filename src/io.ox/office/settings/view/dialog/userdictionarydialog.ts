/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import gt from "gettext";

import { str, ary } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE, insertChildren, createDiv, createSpan, createButton, createDeleteButton, createInput, enableElement, setFocus, matchKeyCode } from "@/io.ox/office/tk/dom";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { userDictionary } from "@/io.ox/office/settings/api";

import "@/io.ox/office/settings/view/dialog/userdictionarydialog.less";

// class UserDictionaryDialog =================================================

/**
 * The User Dictionary Edit Dialog which is visible if the user presses the setting's edit button.
 */
export class UserDictionaryDialog extends BaseDialog<void> {

    // properties -------------------------------------------------------------

    // the current contents to be saved to the user dictionary
    readonly #words = userDictionary.getWords();

    // the <input> element for new words
    readonly #inputNode: HTMLInputElement;

    // the "Add" button to add a new word to the list
    readonly #addButton: HTMLButtonElement;

    // the list element with all words in the user dictionary
    readonly #wordList: HTMLElement;

    // constructor ------------------------------------------------------------

    constructor() {

        super({
            id: "io-ox-office-userdictionary-dialog",
            //#. Title of the user dictionary dialog.
            title: gt("User dictionary"),
            //#. Button label for the save button for the user dictionary dialog.
            okLabel: gt("Save")
        });

        // the container for the top-level input controls
        const inputPanel = this.bodyNode.appendChild(createDiv("input-group form-group"));
        inputPanel.addEventListener("keydown", event => {
            if (matchKeyCode(event, "ENTER")) {
                event.stopPropagation();
                event.preventDefault();
                this.#onClickAddButton();
            }
        });

        // create the text field control
        this.#inputNode = inputPanel.appendChild(createInput({
            classes: "form-control",
            //#. Placeholder text for the input field for add words to the user dictionary
            placeholder: gt("Add a new word")
        }));
        this.#inputNode.setAttribute("role", "textbox");
        this.#inputNode.addEventListener("input", () => this.#validateAddButton());

        // create the "Add" button to add a new word to the list
        this.#addButton = inputPanel.appendChild(createButton({
            action: "add",
            classes: "input-group-addon",
            //#. Button label for adding a word to the user dictionary
            label: gt("Add"),
            click: () => this.#onClickAddButton()
        }));

        // the container for the word list
        const wordListPanel = this.bodyNode.appendChild(createDiv("panel word-list-panel"));

        // create the word list control
        this.#wordList = wordListPanel.appendChild(createDiv({
            classes: "list-group word-list noI18n",
            style: { height: SMALL_DEVICE ? (window.innerHeight - 300) : 307 }
        }));

        // add list entries for all known words
        LOCALE_DATA.getCollator().sort(this.#words);
        for (const word of this.#words) {
            this.#createListItem(word);
        }

        this.setOkHandler(() => {
            const oldWords = userDictionary.getWords();
            const addWords = _.difference(this.#words, oldWords);
            const delWords = _.difference(oldWords, this.#words);
            userDictionary.updateWords(addWords, delWords);
        });

        this.#validateAddButton();
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the value of the input element (split by whitespace, trimmed,
     * and converted to lower-case characters).
     */
    #getInputWords(): string[] {
        const tokens = str.splitTokens(this.#inputNode.value);
        return tokens.map(word => word.trim()).filter(Boolean);
    }

    /**
     * Creates a new list element for the passed word.
     *
     * @param word
     *  The word to be added to the list.
     *
     * @param [prepend]
     *  If set to `true`, the new list entry will be prepended instead of being
     *  appended.
     */
    #createListItem(word: string, prepend?: boolean): void {

        // create the list item element
        const listItem = createDiv("list-group-item");
        insertChildren(this.#wordList, prepend ? 0 : -1, listItem);

        // create the label element containing the word
        listItem.appendChild(createSpan({ classes: "word-label", label: _.noI18n(word) }));

        // event callback to delete the list item
        const removeListItem = (focusTarget?: Nullable<HTMLElement>): void => {
            ary.deleteFirst(this.#words, word);
            listItem.remove();
            setFocus(focusTarget ?? this.#inputNode);
        };

        // create the "Delete" button
        //#. button label for deleting a word from the user dictionary
        const deleteButton = listItem.appendChild(createDeleteButton(removeListItem, { tooltip: gt("Delete word"), classes: _.browser.Safari ? "selection-margin" : "" }));

        // delete list item on DEL/BS keys
        deleteButton.addEventListener("keydown", event => {
            const prevButton = listItem.previousElementSibling?.querySelector<HTMLElement>(".btn");
            const nextButton = listItem.nextElementSibling?.querySelector<HTMLElement>(".btn");
            if (matchKeyCode(event, "ENTER") || matchKeyCode(event, "BACKSPACE") || matchKeyCode(event, "DELETE")) {
                removeListItem(nextButton ?? prevButton);
            } else if (matchKeyCode(event, "UP_ARROW")) {
                if (prevButton) { setFocus(prevButton); }
            } else if (matchKeyCode(event, "DOWN_ARROW")) {
                if (nextButton) { setFocus(nextButton); }
            } else {
                return;
            }
            event.stopPropagation();
            event.preventDefault();
        });
    }

    /**
     * Updates the enabled state of the "Add" button according to the contents
     * of the textfield control.
     */
    #validateAddButton(): void {
        enableElement(this.#addButton, this.#getInputWords().length > 0);
    }

    /**
     * Inserts the current value of the textfield control to the list.
     */
    #onClickAddButton(): void {
        for (const word of this.#getInputWords().reverse()) {
            if (!this.#words.includes(word)) {
                this.#words.push(word);
                this.#createListItem(word, true);
            }
        }
        this.#inputNode.value = "";
        setFocus(this.#inputNode);
        this.#validateAddButton();
    }
}
