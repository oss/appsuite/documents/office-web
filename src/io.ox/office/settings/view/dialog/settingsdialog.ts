/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { createDiv } from "@/io.ox/office/tk/dom";
import { BaseDialog } from "@/io.ox/office/tk/dialogs";

import { CLOSE_LABEL, DOCUMENTS_SETTINGS_TITLE } from "@/io.ox/office/settings/view/labels";
import { renderSettingsView } from "@/io.ox/office/settings/view/view";

import "@/io.ox/office/settings/view/dialog/settingsdialog.less";

// class SettingsDialog =======================================================

/**
 * A modal dialog that displays the documents settings. Used in multi-tab mode
 * as replacement for the settings overlay.
 */
export class SettingsDialog extends BaseDialog<void> {

    public constructor() {

        // base constructor
        super({
            id: "io-ox-office-settings-dialog",
            width: 600,
            title: DOCUMENTS_SETTINGS_TITLE,
            okLabel: CLOSE_LABEL,
            cancelLabel: null, // hide the Cancel button
            movable: true
        });

        // create the root element for the form sections
        // (CSS class "settings-detail-pane" activates core styling for settings in the dialog)
        const settingsPane = this.bodyNode.appendChild(createDiv("settings-detail-pane"));

        // asynchronously render the form sections into the pane element
        jpromise.floating(renderSettingsView(settingsPane, { dialog: true }));
    }
}
