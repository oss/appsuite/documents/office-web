/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import yell from '$/io.ox/core/yell';
import { closeSettings } from '$/io.ox/settings/util';

import { is, jpromise } from '@/io.ox/office/tk/algorithms';
import { setElementLabel, createElement, createDiv, createSpan, createAnchor, createButton, createDeleteButton } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getPath, deleteFromCache, getShortenedTitle, getStandardTemplateFolderId, getTrashState, showPermissionsDialog } from '@/io.ox/office/tk/utils/driveutils';

import { TemplateFolders } from '@/io.ox/office/settings/model/templatefolders';
import { ADD_NEW_TEMPLATE_FOLDER, GLOBAL_TEMPLATE_FOLDER, PERMISSIONS_LABEL, USER_TEMPLATE_FOLDER } from '@/io.ox/office/settings/view/labels';
import { AbstractSection } from "@/io.ox/office/settings/view/section/abstractsection";
import TemplatePickerDialog from '@/io.ox/office/settings/view/dialog/templatepickerdialog';

// side-effect import for CSS styling of the list view (needed for `SettingsDialog` in multi-tab mode)
// ES6-LATER: use a real `ListView` instead of just manually replicating the DOM structure here
import '$/io.ox/backbone/mini-views/settings-list-view';

import '@/io.ox/office/settings/view/section/templatefoldersection.less';

// constants ==================================================================

/**
 * A message that is triggered after the context admin choses a new template
 * folder.
 */
const PERMISSIONS_MESSAGE_LABEL = gt("To grant users access to all templates in this folder, please use the Permission dialog. All users need to be able to read all objects in this folder.");

// class TemplateFolderSection ================================================

export class TemplateFolderSection extends AbstractSection {

    constructor(model, config) {
        const global = config?.global;
        super(global ? "global-template-folders" : "user-template-folders", global ? GLOBAL_TEMPLATE_FOLDER : USER_TEMPLATE_FOLDER);

        // access to passed configuration in class methods
        this.config = { ...config };

        // private properties
        this._model = (model instanceof TemplateFolders) ? model : null;
        this._folders = is.array(model) ? model : null;
        this._listNode = null;
        this._dialog = false; // whether to render into a modal dialog
        this._busy = false; // true while folder picker dialog is open
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ async implRender(section, options) {

        // globally unique class to be used in global settings app, and in modal dialogs
        section.classList.add("io-ox-office-template-folder-section");

        // create a list view for all template folders to be displayed
        this._listNode = section.appendChild(createElement("ul", "list-group list-unstyled settings-list-view"));

        // initialize members
        this._dialog = options?.dialog;
        this._busy = false;

        // add standard template folder as permanent (non-removable) source folder
        if (!this.config.global) {
            const stdPath = await this._resolveFolderPath(getStandardTemplateFolderId());
            if (stdPath) { this._addListItem(stdPath, { locked: true }); }
        }

        // render static folders passed to constructor
        if (this._folders) {
            await this._renderListItems(this._folders, { locked: true });
        }

        // render folders from user settings, create a button to open the folder picker for adding new folders
        if (this._model) {
            await this._renderListItems(this._model.getFolders());
            section.append(createButton({
                action: "newtemplatefolder",
                label: ADD_NEW_TEMPLATE_FOLDER,
                click: () => jpromise.floating(this._pickTemplateFolder()) // ignore returned promise
            }));
        } else if (!this._listNode.hasChildNodes()) {
            // hide entire section, if the folder list is empty
            section.classList.add("hidden");
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the full path information for the passed folder identifier.
     */
    /*private*/ async _resolveFolderPath(folderId) {
        try {
            const isTrash = await getTrashState(folderId);
            return isTrash ? null : await getPath(folderId);
        } catch (err) {
            globalLogger.exception(err);
            return null;
        }
    }

    /**
     * Adds a new list element with folder title, folder link (opens Drive),
     * and Delete button.
     */
    /*private*/ _addListItem(folderPath, options) {

        var folderInfo = _.last(folderPath);
        var folderId = folderInfo.id;

        // create the <li> root node for the list item
        const itemNode = this._listNode.appendChild(createElement("li", "settings-list-item"));

        // the top-level content elements
        const contentsNode = itemNode.appendChild(createDiv("list-item-contents"));
        const controlsNode = itemNode.appendChild(createDiv("list-item-controls"));

        // bold folder name (not in modal dialogs to save vertical space)
        if (!this._dialog) {
            contentsNode.append(createDiv({ classes: "folder-name", label: _.noI18n(folderInfo.title) }));
        }

        // breadcrumb path with deep link to Drive (except in modal dialogs)
        let displayPath = "", tooltipPath = "";
        folderPath.forEach((entry, index) => {
            if (index > 0) { displayPath += " > "; tooltipPath += " > "; }
            const folderName = entry.title;
            displayPath += getShortenedTitle(folderName, 30);
            tooltipPath += getShortenedTitle(folderName, 50);
        });
        const foldersNode = contentsNode.appendChild(this._dialog ? createSpan() : createAnchor("#"));
        foldersNode.classList.add("folder-path");
        setElementLabel(foldersNode, { label: _.noI18n(displayPath), title: _.noI18n(tooltipPath) });
        if (foldersNode instanceof HTMLAnchorElement) {
            foldersNode.classList.add("deep-link-files");
            foldersNode.setAttribute("target", "_blank");
            foldersNode.dataset.folder = folderId;
            // issue #69: close "Settings" dialog when navigating to the template folder in Drive
            foldersNode.addEventListener("click", closeSettings);
        }

        if (this._model && this.config.global) {
            controlsNode.append(createButton({
                type: "toolbar",
                label: PERMISSIONS_LABEL,
                click() { jpromise.floating(showPermissionsDialog(folderId)); }
            }));
        }

        // create the "Delete" button, unless the list item is locked
        if (this._model && !options?.locked) {
            controlsNode.appendChild(createDeleteButton(() => this._removeListItem(folderPath, itemNode), { type: "toolbar", icon: "bi:x-lg", classes: "remove" }));
        } else {
            controlsNode.appendChild(createDiv("remove-placeholder"));
        }
    }

    /**
     * Removes the list item for a template folder from the user settings, and
     * from the GUI.
     */
    /*private*/ _removeListItem(path, itemNode) {
        const id = _.last(path).id;
        if (!this._model.removeFolder(id)) {
            globalLogger.warn("$badge{TemplateFolderSection} folder not in list", id);
        } else {
            itemNode.remove();
        }
    }

    /*private*/ async _renderListItems(folders, options) {
        for (const folder of folders) {
            deleteFromCache(folder);
            const path = await this._resolveFolderPath(folder);
            if (path) { this._addListItem(path, options); }
        }
    }

    /*private*/ async _pickTemplateFolder() {

        if (this._busy) { return; }
        this._busy = true;

        const folderId = await new TemplatePickerDialog().show();
        if (folderId && (folderId !== getStandardTemplateFolderId())) {
            const path = await this._resolveFolderPath(folderId);
            const folder = path ? _.last(path).id : null;
            if (folder && this._model.addFolder(folder)) {
                this._addListItem(path);
                if (this.config.global) {
                    yell({ type: 'info', message: PERMISSIONS_MESSAGE_LABEL });
                }
            }
        }

        this._busy = false;
    }
}
