/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { checkbox, fieldset } from "$/io.ox/core/settings/util";

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { settings as docsSettings } from "@/io.ox/office/settings";

// types ======================================================================

export interface RenderSectionOptions {

    /**
     * Whether the settings section will be inserted into a modal dialog. By
     * default, the settings section must be rendered for the global settings
     * view pane.
     */
    dialog?: boolean;
}

// class AbstractSection ======================================================

/**
 * Represents a section with form controls (a `<fieldset>` element) in the
 * Documents settings view.
 *
 * The method `render()` will create a new `<fieldset>` element, and will call
 * the abstract method `implRender()` to be implemented by the subclass.
 */
export abstract class AbstractSection {

    readonly #id: string;
    readonly #title: string;

    // constructor ------------------------------------------------------------

    protected constructor(id: string, title: string) {
        this.#id = id;
        this.#title = title;
    }

    // public methods ---------------------------------------------------------

    /**
     * Renders and returns the section element represented by this instance.
     *
     * @returns
     *  The `<fieldset>` element representing the settings section.
     */
    render(options?: RenderSectionOptions): HTMLFieldSetElement {
        const section = fieldset(this.#title)[0];
        section.dataset.sectionId = this.#id;
        jpromise.invokeFloating(async () => {
            section.classList.add("render-pending");
            await this.implRender(section, options);
            section.classList.remove("render-pending");
        });
        return section;
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses must fill the passed section element (a `<fieldset>`) with
     * form controls.
     *
     * @param section
     *  The `<fieldset>` element to be initialized.
     */
    protected abstract implRender(section: HTMLFieldSetElement, options?: RenderSectionOptions): MaybeAsync;

    /**
     * Creates a new checkbox control element.
     *
     * @param configKey
     *  The key of the boolean item in the Documents settings to be controlled
     *  by the checkbox control.
     *
     * @param label
     *  The UI label to be shown in the checkbox.
     *
     * @returns
     *  The new checkbox DOM element (still detached).
     */
    protected createCheckBox(configKey: string, label: string): HTMLElement {
        const $checkbox = checkbox(configKey, label, docsSettings);
        // DOCS-4258: changed settings need to be saved back to configuration
        $checkbox.on("change", () => jpromise.floating(docsSettings.save()));
        return $checkbox[0];
    }
}
