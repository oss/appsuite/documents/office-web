/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { createLabel } from "@/io.ox/office/tk/dom";
import { type SelectListEntry, SelectList } from "@/io.ox/office/tk/form";
import { type LengthUnit, LOCALE_DATA, localeDataRegistry } from "@/io.ox/office/tk/locale";

import * as Labels from "@/io.ox/office/settings/view/labels";
import { AbstractSection } from "@/io.ox/office/settings/view/section/abstractsection";

// constants ==================================================================

// readable names for the length units used in the GUI
const UNIT_NAMES: Record<LengthUnit, string> = {
    cm: Labels.UNIT_CM_LABEL,
    mm: Labels.UNIT_MM_LABEL,
    in: Labels.UNIT_IN_LABEL,
};

// private functions ==========================================================

/**
 * Returns the order of length units in the dropdown menu, according to the
 * _standard unit_ of the current UI locale.
 */
function getUnitOrder(): readonly LengthUnit[] {
    switch (localeDataRegistry.getLocaleData(LOCALE_DATA.lc).unit) {
        case "in":  return ["in", "cm", "mm"];
        case "cm":
        case "mm":  return ["cm", "mm", "in"];
    }
}

// class LengthUnitSection ====================================================

export class LengthUnitSection extends AbstractSection {

    public constructor() {
        super("length-unit", Labels.UNIT_SECTION_TITLE);
    }

    // protected methods ------------------------------------------------------

    protected override implRender(section: HTMLFieldSetElement): void {

        // prepare entries in dropdown control
        type LengthUnitEntry = SelectListEntry<LengthUnit>;
        const entries = getUnitOrder().map((unit): LengthUnitEntry => ({ value: unit, label: UNIT_NAMES[unit] }));

        // create and render the dropdown list control
        const selectList = new SelectList(entries, LOCALE_DATA.unit, {
            id: "io-ox-office-standard-unit",
            style: { maxWidth: "15em" },
            size: 1
        });
        section.append(createLabel({ label: Labels.UNIT_DROPDOWN_LABEL, for: selectList.id }));
        section.append(selectList.render());

        // update user settings when changing the selected value
        selectList.onChange(unit => jpromise.floating(LOCALE_DATA.setUnit(unit)));
    }
}
