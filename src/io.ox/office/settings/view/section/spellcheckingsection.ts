/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { createDiv, createButton } from "@/io.ox/office/tk/dom";

import { SPELLING_SETTINGS_KEY, spellingSettings } from "@/io.ox/office/settings/api";
import * as Labels from "@/io.ox/office/settings/view/labels";
import { AbstractSection } from "@/io.ox/office/settings/view/section/abstractsection";
import { UserDictionaryDialog } from "@/io.ox/office/settings/view/dialog/userdictionarydialog";
import MissingSpellingDialog from "@/io.ox/office/settings/view/dialog/missingspellingdialog";

// class SpellCheckingSection =================================================

// NOTE: there are no app-specific spellchecking settings anymore (DOCS-630)

export class SpellCheckingSection extends AbstractSection {

    public constructor() {
        super("spell-checking", Labels.SPELL_SECTION_TITLE);
    }

    // protected methods ------------------------------------------------------

    protected override implRender(section: HTMLFieldSetElement): void {

        // checkbox for "Spell permanently"
        section.append(this.createCheckBox(SPELLING_SETTINGS_KEY, Labels.SPELL_CHECKBOX_LABEL));

        // button to open "Edit user dictionary" dialog
        const dictRowNode = section.appendChild(createDiv("form-group row"));
        const dictColNode = dictRowNode.appendChild(createDiv("col-md-12"));
        dictColNode.appendChild(createButton({
            action: "edituserdic",
            label: Labels.USER_DICT_BUTTON_LABEL,
            click() { jpromise.floating(new UserDictionaryDialog().show()); }
        }));

        // button to open "Set language notification" dialog
        if (spellingSettings.hasLanguageWithoutSpelling()) {
            const langRowNode = section.appendChild(createDiv("form-group row"));
            const langColNode = langRowNode.appendChild(createDiv("col-md-12"));
            langColNode.appendChild(createButton({
                action: "editmissingspelling",
                label: Labels.NOTIFY_LANGUAGE_BUTTON_LABEL,
                click() { jpromise.floating(new MissingSpellingDialog().show()); }
            }));
        }
    }
}
