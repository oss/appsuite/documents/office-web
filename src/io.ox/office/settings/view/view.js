/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from '$/jquery';
import ox from '$/ox';

import { header } from '$/io.ox/core/settings/util';

import { fun } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { SINGLE_CONTEXT_DEPLOYMENT } from '@/io.ox/office/tk/utils/shareutils';
import { isGuest } from '@/io.ox/office/tk/utils/driveutils';
import { BROWSER_TAB_SUPPORT } from '@/io.ox/office/tk/utils/tabutils';

import { contextTemplateFolders, userTemplateFolders, fetchContextTemplateFolders } from '@/io.ox/office/settings/api';
import { DOCUMENTS_HEADER_LABEL } from '@/io.ox/office/settings/view/labels';
import { LengthUnitSection } from '@/io.ox/office/settings/view/section/lengthunitsection';
import { SpellCheckingSection } from '@/io.ox/office/settings/view/section/spellcheckingsection';
import { TemplateFolderSection } from '@/io.ox/office/settings/view/section/templatefoldersection';
import { ThemeSection } from '@/io.ox/office/settings/view/section/themesection';

import { DOCUMENTS_COLLABORA } from '@/io.ox/office/baseframework/utils/baseconfig';

import "@/io.ox/office/settings/view/view.less";

// private functions ==========================================================

/**
 * Returns whether the current user is the context administrator.
 *
 * @returns {Promise<boolean>}
 *  A promise that will fulfil with a boolean value specifying whether the
 *  current user is the context administrator. The promise will never reject
 *  but will fulfil with `false` on network errors.
 */
async function isContextAdmin() {
    try {
        const response = await $.ajax(`${ox.root}/api/user/me?session=${ox.session}`, { type: "GET", dataType: "json" });
        return !!response.data.user_id && (response.data.user_id === response.data.context_admin);
    } catch (err) {
        globalLogger.exception(err, 'request "/api/user/me" failed');
        return false;
    }
}

/**
 * Creates all needed section renderers according to the settings of the
 * current user account.
 */
const createSectionsOnce = fun.once(async () => {

    const sections = DOCUMENTS_COLLABORA ? [] : [
        new LengthUnitSection(),
        new SpellCheckingSection()
    ];

    // special editor theming in multi-tab enviornment
    if (BROWSER_TAB_SUPPORT && !DOCUMENTS_COLLABORA) {
        sections.push(new ThemeSection());
    }

    // user template folders (not for guest users)
    if (!isGuest()) {
        sections.push(new TemplateFolderSection(userTemplateFolders));
    }

    // global admin templates will be disabled in single context mode
    if (!isGuest() && !SINGLE_CONTEXT_DEPLOYMENT) {

        // grant write access for context templates to the context admin only
        if (await isContextAdmin()) {
            sections.push(new TemplateFolderSection(contextTemplateFolders, { global: true }));
        } else {
            // regular users can view but not modify the context templates
            try {
                const folders = await fetchContextTemplateFolders();
                sections.push(new TemplateFolderSection(folders, { global: true }));
            } catch (err) {
                globalLogger.exception(err, 'action request "getcontexttemplatefolders" failed');
            }
        }
    }

    return sections;
});

// static initialization ======================================================

// callback to re-render the last root node filled with Documents settings form controls
let repeatRenderSettingsView;

// Register a global listener for Documents settings changes from other browser
// tabs, and repaint the last registered root node containing the Documents
// settings form. In the main browser tab (and in single-tab mode), this is
// always the same DOM element provided by the Settings app, but in Documents
// child tabs, this will be an arbitrary element contained in a modal settings
// dialog.
globalEvents.on("change:config:docs", data => data.remote && repeatRenderSettingsView?.());

// public functions ===========================================================

/**
 * Renders the form controls for all Documents settings into the passed DOM
 * element.
 *
 * @param {NodeOrJQuery} rootNode
 *  The DOM element to be filled with the Documents settings form.
 *
 * @param {RenderSectionOptions} [options]
 *  Optional parameters.
 */
export async function renderSettingsView(rootNode, options) {

    // This function will be called from the Settings app (overlay app in
    // single-tab mode, and in the main tab in multi-tab mode), and from the
    // modal `DocumentsSettings` dialog (child tabs in multi-tab mode).

    // store current root node for global event processing
    repeatRenderSettingsView = () => renderSettingsView(rootNode, options);

    // await creation of the section renderers before rendering (no flickering)
    const sections = await createSectionsOnce();
    // the root node, as JQuery object
    const $rootNode = $(rootNode);

    // clear the root node and create the title element (except in modal dialogs)
    $rootNode.empty();
    if (!options?.dialog) {
        $rootNode.append(header(DOCUMENTS_HEADER_LABEL, 'ox.documents.user.sect.firststeps.settings.html', 'help-documents'));
    }

    const $settingsBody = $('<div data-point="io.ox/office/settings/detail/view" class="settings-body io-ox-office-settings">');
    $rootNode.append($settingsBody);

    const $settingsSection = $('<section class="io-ox-office-main settings-section flex-col flex-grow min-h-0 mb-0">');
    $settingsSection.css("padding-top", "15px");
    $settingsBody.append($settingsSection);

    // call all section renderers which will create their own section nodes
    for (const section of sections) {
        $settingsSection.append(section.render(options));
    }
}
