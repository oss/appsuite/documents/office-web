/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ox from "$/ox";
import gt from "gettext";

// re-exports =================================================================

export * from "@/io.ox/office/tk/dom/labels";

// constants ==================================================================

/**
 * Label for the Documents settings panel entry and its header.
 */
export const DOCUMENTS_HEADER_LABEL = gt("Documents");

/**
 * Label for the Documents settings dialog title and the Documents Settings
 * button tooltip.
 */
export const DOCUMENTS_SETTINGS_TITLE = gt("Documents settings");

// "Measurement units" section ------------------------------------------------

//#. header for changing the standard measurement unit (inch, centimeter, ...)
export const UNIT_SECTION_TITLE = gt("Measurements");

//#. followed by a dropdown: "Show measurements in units of [millimeter/centimeter/inch]".
export const UNIT_DROPDOWN_LABEL = gt("Show measurements in units of");

export const UNIT_DROPDOWN_TOOLTIP = gt("Show measurements in units of inch, centimeter, or millimeter");

//#. Label for "centimeters" unit used in measurements settings drop-down.
export const UNIT_CM_LABEL = gt("centimeter");

//#. Label for "millimeters" unit used in measurements settings drop-down.
export const UNIT_MM_LABEL = gt("millimeter");

//#. Label for "inches" unit used in measurements settings drop-down.
export const UNIT_IN_LABEL = gt("inch");

// "Spell checking" section ---------------------------------------------------

//#. section header spellchecking configuration settings
export const SPELL_SECTION_TITLE = gt("Spell checking");

//#. checkbox label for spelling errors configuration
export const SPELL_CHECKBOX_LABEL = gt("Check for errors permanently");

//#. Button label for opening the user dictionary dialog
export const USER_DICT_BUTTON_LABEL = gt("Edit user dictionary");

//#. Button label for opening the user spell-check languages dialog
export const NOTIFY_LANGUAGE_BUTTON_LABEL = gt("Set language notification");

// "Theme" section ------------------------------------------------------------

//#. header for changing the Theme settings
export const THEME_SECTION_TITLE = gt("Theme");

// #. %1$s is placeholder for the product name like "OX App Suite"
export const THEME_CHECKBOX_LABEL = gt("Use %1$s theme in editors", ox.serverConfig.productName || "OX App Suite");


// "My template folders" section ----------------------------------------------

/**
 * Label for the user's template folder.
 */
export const USER_TEMPLATE_FOLDER = gt("My template folders");

/**
 * Label for the global template folder created by the context admin.
 */
export const GLOBAL_TEMPLATE_FOLDER = gt("Global template folders");

/**
 * The dialog header to choose a new template folder.
 */
export const SELECT_TEMPLATE_FOLDER = gt("Select template folder");

/**
 * A button label to choose a new template folder.
 */
export const ADD_NEW_TEMPLATE_FOLDER = gt("Add new template folder");

/**
 * Button label to open Drive's permission dialog.
 */
export const PERMISSIONS_LABEL = gt("Permissions");
