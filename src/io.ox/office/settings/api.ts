/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { locationHash } from "$/url";

import { is, to } from "@/io.ox/office/tk/algorithms";
import { getFlag } from "@/io.ox/office/tk/config";
import { FILTER_MODULE_NAME, sendRequest } from "@/io.ox/office/tk/utils/io";

import { SpellingSettings } from "@/io.ox/office/settings/model/spellingsettings";
import { UserDictionary } from "@/io.ox/office/settings/model/userdictionary";
import { TemplateFolders } from "@/io.ox/office/settings/model/templatefolders";

// re-exports =================================================================

export { type LanguageWithoutSpelling, SPELLING_SETTINGS_KEY } from "@/io.ox/office/settings/model/spellingsettings";

// constants ==================================================================

/**
 * The configuration key for the Documents editor GUI theme mode.
 */
export const THEME_SETTINGS_KEY = "documents/theme/useAppSuiteThemeColors";

// globals ====================================================================

/**
 * The global settings for online spelling.
 */
export const spellingSettings = new SpellingSettings();

/**
 * The global user dictionary with additional words used for online spelling.
 */
export const userDictionary = new UserDictionary();

/**
 * Container with identifiers of all global template folders.
 */
export const contextTemplateFolders = new TemplateFolders("ContextTemplateFolders");

/**
 * Container with identifiers of all user-defined template folders.
 */
export const userTemplateFolders = new TemplateFolders("MyTemplateFolders");

// public functions ===========================================================

/**
 * Returns whether Documents editor GUI is using dedicated Documents theme
 * colors depending on the type of the edited document in the global topbar
 * (`true`), or the core theme colors (`false`).
 */
export function useEditorThemeColors(): boolean {
    const hashValue = locationHash("office:uicolors"); // URL hash overrides user settings
    return (hashValue === "app") || ((hashValue !== "theme") && !getFlag(THEME_SETTINGS_KEY, false));
}

/**
 * Fetches the list of context template folders from filter service (not from
 * the user configuration).
 *
 * @returns
 *  A promise that fulfils with the identifiers of context template folders.
 */
export async function fetchContextTemplateFolders(): Promise<string[]> {
    const data = await sendRequest(FILTER_MODULE_NAME, { action: "getcontexttemplatefolders" });
    return to.array(data, true).filter(v => is.string(v) && !!v, true) as string[];
}
