/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';

import ext from '$/io.ox/core/extensions';

import { isGuest } from '@/io.ox/office/tk/utils/driveutils';
import { BROWSER_TAB_SUPPORT } from '@/io.ox/office/tk/utils/tabutils';

import { spellingSettings } from "@/io.ox/office/settings/api";
import * as Labels from '@/io.ox/office/settings/view/labels';
import { renderSettingsView } from '@/io.ox/office/settings/view/view';

// static initialization ======================================================

// create an entry in the settings main menu (sidepane)
ext.point('io.ox/settings/pane/main').extend({
    id: 'io.ox/office',
    title: Labels.DOCUMENTS_HEADER_LABEL,
    ref: 'io.ox/office',
    loadSettingPane: false,
    index: 4000,
    searchTerms: [{
        title: Labels.UNIT_DROPDOWN_TOOLTIP,
        highlight: () => $('#io-ox-office-standard-unit').closest('fieldset').children('legend')
    }, {
        title: Labels.SPELL_CHECKBOX_LABEL,
        highlight: '#io-ox-office-ischeckspellingpermanently'
    }, {
        title: Labels.USER_DICT_BUTTON_LABEL,
        highlight: 'button[data-action="edituserdic"]'
    }, {
        title: Labels.NOTIFY_LANGUAGE_BUTTON_LABEL,
        highlight: 'button[data-action="editmissingspelling"]',
        requires: () => spellingSettings.hasLanguageWithoutSpelling()
    }, {
        title: Labels.THEME_CHECKBOX_LABEL,
        highlight: '#io-ox-office-documents-theme-useappsuitethemecolors',
        requires: () => BROWSER_TAB_SUPPORT
    }, {
        title: Labels.ADD_NEW_TEMPLATE_FOLDER,
        highlight: 'button[data-action="newtemplatefolder"]',
        requires: () => !isGuest()
    }]
});

// extension points with "/settings/detail" suffix will be invoked by the
// Settings app when activating the "Documents" entry in the main navigation
ext.point('io.ox/office/settings/detail').extend({
    index: 100,
    // DOM element to be initialized will be passed as "this" calling context
    draw() { return renderSettingsView(this); }
});
