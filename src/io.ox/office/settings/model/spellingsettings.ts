/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { settings as docsSettings } from "@/io.ox/office/settings";

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import { getFlag, setValue } from "@/io.ox/office/tk/config";
import { EObject } from "@/io.ox/office/tk/objects";

// types ======================================================================

/**
 * Settings for a language without spelling support.
 */
export interface LanguageWithoutSpelling {

    /**
     * The code of the language lacking spelling support.
     */
    lang: string;

    /**
     * Whether to notify the user about missing spelling support.
     */
    notify: boolean;
}

// constants ==================================================================

// the configuration key for "spellchecking enabled" setting
export const SPELLING_SETTINGS_KEY = "isCheckSpellingPermanently";

// configuration keys for lists of languages without spelling support
const SILENT_LANGS_SETTINGS_KEY = "module/unsupportedspellchecklanguageblacklist";
const NOTIFY_LANGS_SETTINGS_KEY = "module/unsupportedspellchecklanguagewhitelist";

// private functions ==========================================================

function isValidLanguage(lang: unknown): lang is string {
    return is.string(lang) && !!lang && (lang !== "none");
}

function loadLanguages(configKey: string): string[] {
    // bug 54502: old versions have stored string arrays instead of plain strings
    const value = docsSettings.get(configKey);
    const array = (is.string(value) ? value.split(",") : is.array(value) ? value : []);
    return array.filter(isValidLanguage);
}

// class SpellingSettings =====================================================

export class SpellingSettings extends EObject {

    // constructor ------------------------------------------------------------

    constructor() {
        super({ _kind: "singleton" });
        this.#load();
        this.listenTo(docsSettings, `change:${SILENT_LANGS_SETTINGS_KEY}`, this.#load);
        this.listenTo(docsSettings, `change:${NOTIFY_LANGS_SETTINGS_KEY}`, this.#load);
    }

    // properties -------------------------------------------------------------

    // maps languages to boolean flags whether notifications for missing spelling support are allowed
    readonly #langs = new Map<string, boolean>();

    // public methods ---------------------------------------------------------

    /**
     * Returns whether online spelling is globally enabled in all browser tabs.
     */
    isSpellingEnabled(): boolean {
        return getFlag(SPELLING_SETTINGS_KEY, true);
    }

    /**
     * Enables or disables online spelling globally for all documents in all
     * browser tabs.
     */
    enableSpelling(state: boolean): void {
        jpromise.floating(setValue(SPELLING_SETTINGS_KEY, state));
    }

    /**
     * Returns a list with all languages lacking spelling support, and whether
     * to notify the user about the respective languages.
     *
     * @returns
     *  A list with all known languages without spelling support.
     */
    getLanguagesWithoutSpelling(): LanguageWithoutSpelling[] {
        const list: LanguageWithoutSpelling[] = [];
        for (const [lang, notify] of this.#langs) {
            list.push({ lang, notify });
        }
        return list;
    }

    /**
     * Registers the passed languages lacking support for spelling. The current
     * language settings will be overwritten completely.
     */
    setLanguagesWithoutSpelling(entries: LanguageWithoutSpelling[]): void {
        this.#langs.clear();
        for (const { lang, notify } of entries) {
            if (isValidLanguage(lang)) {
                this.#langs.set(lang, notify);
            }
        }
        this.#save();
    }

    /**
     * Registers the passed additional languages lacking support for spelling.
     *
     * @param langs
     *  The languages to be registered.
     *
     * @param notify
     *  Whether to notify the user if the language is used in a document.
     */
    addLanguagesWithoutSpelling(langs: string[], notify: boolean): void {
        for (const lang of langs) {
            if (isValidLanguage(lang)) {
                this.#langs.set(lang, notify);
            }
        }
        this.#save();
    }

    /**
     * Returns whether any languages without spelling support have been
     * registered.
     *
     * @returns
     *  Whether any languages without spelling support have been registered.
     */
    hasLanguageWithoutSpelling(): boolean {
        return this.#langs.size > 0;
    }

    /**
     * Returns whether notifications about missing spelling support for the
     * specified language will be suppressed.
     *
     * @param lang
     *  The language to be checked.
     *
     * @returns
     *  Whether notifications about missing spelling support for the specified
     *  language will be suppressed.
     */
    isSpellingNotificationBlocked(lang: string): boolean {
        return this.#langs.get(lang) === false;
    }

    // private methods --------------------------------------------------------

    #load(): void {
        this.#langs.clear();
        for (const lang of loadLanguages(SILENT_LANGS_SETTINGS_KEY)) {
            this.#langs.set(lang, false);
        }
        for (const lang of loadLanguages(NOTIFY_LANGS_SETTINGS_KEY)) {
            this.#langs.set(lang, true);
        }
    }

    #save(): void {
        const silentLangs: string[] = [];
        const notifyLangs: string[] = [];
        for (const [lang, notify] of this.#langs) {
            (notify ? notifyLangs : silentLangs).push(lang);
        }
        docsSettings.set(SILENT_LANGS_SETTINGS_KEY, silentLangs.join(","));
        docsSettings.set(NOTIFY_LANGS_SETTINGS_KEY, notifyLangs.join(","));
        // DOCS-3801: Force sending settings to server immediately. This settings save request may occur
        // shortly after writing languages collected from document which causes to throttle the settings
        // server request by 5 seconds (see method `Settings#save` in $/io.ox/core/settings).
        jpromise.floating(docsSettings.saveAndYell(undefined, { force: true }));
    }
}
