/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary, jpromise } from "@/io.ox/office/tk/algorithms";
import { getArray } from "@/io.ox/office/tk/config";
import { EObject } from "@/io.ox/office/tk/objects";
import { settings as docsSettings } from "@/io.ox/office/settings";

// class TemplateFolders ======================================================

export class TemplateFolders extends EObject {

    // properties -------------------------------------------------------------

    readonly #key: string;
    #folders!: string[];

    // constructor ------------------------------------------------------------

    constructor(key: string) {
        super({ _kind: "singleton" });
        this.#key = key;
        this.#load();
        this.listenTo(docsSettings, `change:${key}`, this.#load);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the list of template folders from the user configuration.
     *
     * @returns
     * The identifiers of user template folders.
     */
    getFolders(): string[] {
        return this.#folders.slice();
    }

    /**
     * Adds the specified folder identifier to the user settings.
     *
     * @param folder
     *  The identifier of the template folder to be added.
     *
     * @returns
     *  Whether the entry has actually been added to the list (was *not* in the
     *  list before).
     */
    addFolder(folder: string): boolean {
        const includes = this.#folders.includes(folder);
        if (!includes) {
            this.#folders.push(folder);
            this.#save();
        }
        return !includes;
    }

    /**
     * Removes the specified folder identifier from the user settings.
     *
     * @param folder
     *  The identifier of the template folder to be removed.
     *
     * @returns
     *  Whether the entry has actually been removed from the list (it *was* in
     *  the list before).
     */
    removeFolder(folder: string): boolean {
        const removed = ary.deleteFirst(this.#folders, folder);
        if (removed) { this.#save(); }
        return removed;
    }

    // private methods --------------------------------------------------------

    #load(): void {
        this.#folders = getArray(this.#key).filter(v => is.string(v) && !!v) as string[];
    }

    #save(): void {
        docsSettings.set(this.#key, this.#folders);
        jpromise.floating(docsSettings.saveAndYell());
    }
}
