/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import { getArray } from "@/io.ox/office/tk/config";
import { EObject } from "@/io.ox/office/tk/objects";
import { settings as docsSettings } from "@/io.ox/office/settings";

// types ======================================================================

/**
 * Type mapping for the events emitted by `UserDictionary` instances.
 */
export interface UserDictionaryEventMap {

    /**
     * Will be emitted when the dictionary has been changed.
     *
     * @param addWords
     *  The words that have been added to the dictionary.
     *
     * @param delWords
     *  The words that have been deleted from the dictionary.
     */
    save: [addWords: string[], delWords: string[]];
}

// constants ==================================================================

// the configuration key for the user dictionary
const SETTINGS_KEY = "userdictionary";

// class UserDictionary =======================================================

export class UserDictionary extends EObject<UserDictionaryEventMap> {

    // properties -------------------------------------------------------------

    #words!: Set<string>;

    // constructor ------------------------------------------------------------

    constructor() {
        super({ _kind: "singleton" });
        this.#load();
        this.listenTo(docsSettings, `change:${SETTINGS_KEY}`, this.#load);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed word is contained in the user dictionary.
     *
     * @returns
     *  Whether the passed word is contained in the user dictionary.
     */
    hasWord(word: string): boolean {
        word = word.trim();
        return !!word && this.#words.has(word);
    }

    /**
     * Returns a copy of all words registered in the user dictionary.
     *
     * @returns
     *  A copy of all words registered in the user dictionary, in no specific
     *  order.
     */
    getWords(): string[] {
        return Array.from(this.#words);
    }

    /**
     * Adds a new word to the user dictionary.
     *
     * @param word
     *  The word to be added to the user dictionary.
     */
    addWord(word: string): void {
        this.updateWords([word], []);
    }

    /**
     * Remove a word from the user dictionary.
     *
     * @param word
     *  The word to be removed from the user dictionary.
     */
    removeWord(word: string): void {
        this.updateWords([], [word]);
    }

    /**
     * Updates the word list, writes the word list to the user configuration,
     * and triggers a "save" event.
     *
     * @param addWords
     *  The words to be added to the dictionary.
     *
     * @param removeWords
     *  The words to be removed from the dictionary.
     */
    updateWords(addWords: string[], removeWords: string[]): void {

        const newAddWords: string[] = [];
        const newDelWords: string[] = [];

        // add unknown words into the dictionary
        for (let word of addWords) {
            word = word.trim();
            if (word && !this.#words.has(word)) {
                this.#words.add(word);
                newAddWords.push(word);
            }
        }

        // remove known words from the dictionary
        for (let word of removeWords) {
            word = word.trim();
            if (word && this.#words.has(word)) {
                this.#words.delete(word);
                newDelWords.push(word);
            }
        }

        // save the dictionary and trigger the "save" event
        if (newAddWords.length || newDelWords.length) {
            this.#save();
            this.trigger("save", newAddWords, newDelWords);
        }
    }

    // private methods --------------------------------------------------------

    #load(): void {
        this.#words = new Set(getArray(SETTINGS_KEY).filter(v => is.string(v) && !!v) as string[]);
    }

    #save(): void {
        docsSettings.set(SETTINGS_KEY, this.getWords());
        jpromise.floating(docsSettings.saveAndYell());
    }
}
