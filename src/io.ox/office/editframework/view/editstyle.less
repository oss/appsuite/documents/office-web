/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";

/* restrict all definitions to editor applications */
.io-ox-office-main.io-ox-office-edit-main {

    /* ===================================================================== */
    /* application pane                                                      */
    /* ===================================================================== */

    .app-pane {

        // Clipboard ==========================================================

        .clipboard {
            position: absolute;
            width: 1px;
            height: 1px;
            overflow: hidden;
            line-height: normal;

            // empty clipboard node must have minimum size to be focusable on Firefox
            min-width: 1px;
            min-height: 1px;

            // never show focus outline styles
            outline: none !important;

            // fix problems when plugging Spreadsheet app into the Viewer, DOCS-2136
            &.viewer-mode {
                position: static;
                top: auto;
            }
        }
    }

    /* ===================================================================== */
    /* document contents                                                     */
    /* ===================================================================== */

    .formatted-content {

        /* placeholders ==================================================== */

        /* replacement layout for unsupported content */
        .placeholder {
            box-sizing: border-box;
            padding: 3px 6px;
            border: 1px solid #ccc;
            border-radius: 0;
            background: fade(white, 80%);
            box-shadow: 0 0 12px -2px #ccc inset;
            line-height: normal;
            text-align: left;
            font-size: 13px;

            > .background-icon {
                color: #e5e5e5;
                text-align: center;
                > [data-icon-id] { vertical-align: middle; }
            }

            > p {
                position: relative;
                margin: 0;

                &:first-child {
                    font-weight: bold;
                    color: var(--border);
                }

                &:not(:first-child) {
                    margin-top: 4px;
                    font-size: 85%;
                }
            }
        }
    }
}

/* ========================================================================= */
/* control groups                                                            */
/* ========================================================================= */

// no restriction to ".io-ox-office-edit-main", needed in view panes and pop-up menus
.io-ox-office-main {

    // a text field for the document file name
    .group.text-field.filename-field {
        opacity: 1 !important; // also when disabled
    }

    // special selected/hover styling for tab button groups (thick bar on bottom border of button)
    .group.tab-button-style .btn {
        font-weight: bold;
        color: var(--text-gray);

        // disable the standard hover overlay effect
        .hover-overlay-off();

        // add element for hover and selected effect
        &::before {
            content: "";
            .absolute-with-distance(auto, 3px, 3px, 3px);
            height: 3px;
        }

        &:hover {
            border-color: transparent !important;
            &::before { background: var(--text-gray-hover); }
        }

        &.selected {
            background: none !important;
            color: var(--text);
            z-index: 1;
            &::before { background: var(--accent) !important; }
        }
    }

    .negative-red {
        --negative-red-color: red;
        :root.dark & { --negative-red-color: #f66; }
        color: var(--negative-red-color) !important;
    }

    // class ApplicationStatusLabel ===========================================

    // application status label in toppane
    .group.app-status-label {
        min-width: 13em;
        opacity: 0.6;

        > .caption > span {
            font-size: 90%;
        }
    }

    // class ToolPaneButtonGroup ==============================================

    // toolpane selector button group
    .group.toolpane-button-group a.btn.selected {
        cursor: default;
        pointer-events: none;
    }

    // class ToolPanePicker ===================================================

    // toolpane selector dropdown menu for small devices
    .group.toolpane-picker {
        &.shrink-mode > a.btn > .caret-node { display: none; }
    }

    // class UserBadge ========================================================

    .group.user-badge {
        padding: 5px 10px;

        > .user-name {
            flex-grow: 1;
            width: 13em;
            .text-overflow-hidden();

            > a {
                padding: var(--distance-v) var(--std-control-padding);
                cursor: pointer;
            }
        }

        > .user-status {
            position: relative;
            padding: 3px;
            text-align: center;

            > div {
                width: 26px;
                height: 26px;
                border: 1px solid transparent;

                [data-icon-id] {
                    opacity: 0.7;
                }
            }

            .generate-scheme-color-rules({
                &[data-color-index="@{scheme-index}"] > div {
                    background-color: fade(@scheme-color, 10%);
                    border-color: fade(@scheme-color, 30%);
                }
            });
        }
    }
}

/* ========================================================================= */
/* pop-up nodes                                                              */
/* ========================================================================= */

.io-ox-office-main.popup-container.popup-menu {

    // class FontFamilyPicker =================================================

    &.list-menu.font-family-picker a.btn > .sub-label {
        align-self: center;
        margin-left: var(--distance-h);
        font-size: 90%;
        opacity: 0.7;
    }

    // class FontSizePicker ===================================================

    &.list-menu.font-size-picker a.btn {
        padding-right: 1.25rem !important;
    }

    // class TableSizePicker ==================================================

    &.table-size-picker {
        user-select: none;

        > .popup-content {
            padding: 0;
        }

        .button {
            display: inline-block;
            position: relative;
            float: none;
            width: auto;
            height: auto;
            margin: 0;
            padding: 3px;
            background: none;
            border: none;
            vertical-align: top;
            font-size: 1px;
            line-height: 1px;
            touch-action: none;
        }
    }

    // class StyleSheetPicker =================================================

    &.style-picker [data-section] a.btn {

        // underground area for displaying a page or fill effect
        > .page-effect {
            .absolute-with-distance(2px);
            background: white;
            border-radius: 2px;
        }

        // underground area for displaying border styles
        > .border-effect {
            .absolute-with-distance(4px);
        }
    }

    // class ParagraphStylePicker =============================================

    &.style-picker.family-paragraph [data-section]:not([data-section="special"]) a.btn {

        --this-entry-padding: var(--std-control-padding);

        max-width: 15em;

        &:not(.selected).contextmenu-anchor {
            .dropdown-open-colors();
        }
    }

    // class TableStylePicker =================================================

    &.style-picker.family-table [data-section] a.btn {
        width: 120px !important;
        height: 72px !important;
        padding: 0 4px;
        text-align: center;

        img {
            position: relative;
            left: 1px;
            top: 1px;
            align-self: center;
        }
    }
} /* end of .io-ox-office-main.popup-container.popup-menu */

/* ========================================================================= */
/* Comment Threads                                                           */
/* ========================================================================= */

.io-ox-office-main {

    /**
     * Standard color for comment bubbles.
     */
    --comment-bubble: var(--scheme-color-1);

    /**
    * Standard color for comment bubbles.
    */
    --comment-bubble-text: white;

    // a bubble icon with embedded count
    .comment-thread-count {
        position: relative;

        [data-icon-id] {
            color: var(--comment-bubble);
        }

        &::after {
            .absolute-with-distance(2px, 0, 0, 0);
            content: attr(data-thread-count);
            color: var(--comment-bubble-text);
            font-size: 0.8em;
            font-weight: bold;
        }

        &[data-thread-count="0"] {
            display: none;
        }
    }
}

/* ========================================================================= */
/* crash dialogs                                                             */
/* ========================================================================= */

.io-ox-office-main.generate-log-dialog {

    .result {
        padding: 10px 0;
        text-align: center;
        font-weight: normal;
        font-size: 14px;
    }

    .details {
        padding-top: 15px;
        text-align: center;
        font-size: 9px;
        opacity: 0.6;
    }
}
