/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { dict } from "@/io.ox/office/tk/algorithms";

import { FloatingMenu } from "@/io.ox/office/baseframework/view/popup/floatingmenu";
import { SHOW_REMOTE_SELECTIONS } from "@/io.ox/office/editframework/utils/editconfig";
import { UserBadge } from "@/io.ox/office/editframework/view/control/userbadge";

import "@/io.ox/office/editframework/view/popup/collaboratormenu.less";

// class CollaboratorMenu =====================================================

/**
 * A floating popup menu displaying collaborating users.
 *
 * @param {EditView} docView
 *  The document view containing this menu instance.
 */
export class CollaboratorMenu extends FloatingMenu {

    constructor(docView, config) {

        // base constructor
        super(docView, gt("Collaborators"), {
            classes: "collaborator-menu",
            tooltip: gt("List of collaborators of this document"),
            autoFocus: false,
            showOwnColor: config?.showOwnColor
        });

        // all existing user badges, mapped by unique user index
        this._bagdeGroups = {};
        // whether the menu has been closed by the user (do not show up automatically anymore)
        this._manuallyClosed = false;

        // toggle auto-open functionality if user has opened/closed this popup
        this.on("menu:userclose", () => { this._manuallyClosed = true; });
        this.on("popup:show", () => { this._manuallyClosed = false; });

        // lazy repaint of the menu when list of remote users changes
        this.requestMenuRepaintOnEvent(this.docApp, "docs:users");

        // auto show/hide collaborator list depending on user event
        // bug 46610: listen to "docs:users", not "docs:users:selection", so that
        // no update happens, if only the selection has changed
        this.listenTo(this.docApp, "docs:users", activeClients => {
            if (activeClients.length < 2) {
                this.hide();
            } else if (!this._manuallyClosed && this.docApp.isActive() && !this.docModel.blockPopupMenus()) {
                this._showSilently();
            }
        });

        // bug 35534: restore collaborators popup
        this.listenTo(this.docApp.getWindow(), "show", () => {
            if (!this._manuallyClosed && (this.docApp.getActiveClients().length >= 2)) {
                this._showSilently();
            }
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Reinitializes the user badges. Generates badges for all new clients, and
     * removes badges of inactive clients.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // a local copy of the map, remaining entries are inactive users that will be hidden
        const localGroups = { ...this._bagdeGroups };

        // process all active users (returned array is sorted by unique client index)
        this.docApp.getActiveClients().forEach(clientData => {

            // update existing badge group
            let badgeGroup = localGroups[clientData.clientUID];
            if (badgeGroup) {
                badgeGroup.setValue(clientData);
                badgeGroup.show();
                delete localGroups[clientData.clientUID];
                return;
            }

            // create a new badge group
            const showColorBox = SHOW_REMOTE_SELECTIONS && (clientData.remote || this.config.showOwnColor);
            badgeGroup = this.addControl(null, new UserBadge(this.docView, { showColorBox }));
            badgeGroup.setValue(clientData);
            this._bagdeGroups[clientData.clientUID] = badgeGroup;
        });

        // hide all remaining inactive users
        dict.forEach(localGroups, badgeGroup => badgeGroup.hide());
    }

    // private methods --------------------------------------------------------

    /**
     * Shows this menu and restores the current state of the flag
     * `manuallyClosed` that will be updated automatically in an event
     * handler.
     */
    /*private*/ _showSilently() {
        const manuallyClosed = this._manuallyClosed;
        this.show();
        this._manuallyClosed = manuallyClosed;
    }
}
