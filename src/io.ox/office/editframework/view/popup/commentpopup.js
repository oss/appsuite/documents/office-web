/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { detachChildren } from "@/io.ox/office/tk/dom";
import { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";

// class CommentPopup =========================================================

/**
 * A popup node for a comment thread.
 */
export class CommentPopup extends BaseMenu {

    constructor(config) {

        // base constructor
        super({
            displayType: "embedded", // popup needs to be hovered by modal dialogs
            anchorBorder: ["bottom", "top", "right", "left"],
            coverAnchor: true,
            closable: true,
            autoClose: false,
            autoFocus: false,
            ...config
        });

        this.$el.addClass("comments-popup");
        this.$body.addClass("comments-container");

        // DOCS-3153: Always close the editor with unsaved changes.
        // TODO: This is an easy solution that may kick away unsaved changes unexpectedly.
        // This may be unexpected by the user, but implementing a full "Go to comment popup
        // with unsaved changes" would be much more complicated with the current code base.
        this.on("popup:beforehide", this._discardUnsavedChanges);
    }

    destructor() {
        this._discardUnsavedChanges();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Renders the passed comment thread into this pop-up menu.
     *
     * @param {CommentFrame} commentFrame
     *  The frame instance of an entire comment thread to be shown in this
     *  popup menu.
     */
    setCommentFrame(commentFrame) {

        // remove old comment frame from the container
        detachChildren(this.$body[0]);

        // activate CSS styling for selected comment frames in sidepane
        const $commentFrame = commentFrame.getNode().addClass("selected");
        this.$body.append($commentFrame);
    }

    // private methods --------------------------------------------------------

    /*private*/ _discardUnsavedChanges() {
        // try to click the "Cancel" button of a comment in edit mode
        this.$body.find(".editoraction .btn[data-action=cancel]").click();
    }
}
