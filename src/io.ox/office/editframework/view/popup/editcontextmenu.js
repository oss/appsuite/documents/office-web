/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { ContextMenu } from "@/io.ox/office/baseframework/view/popup/contextmenu";

import { Button } from "@/io.ox/office/editframework/view/editcontrols";
import { CLIPBOARD_CUT_OPTIONS, CLIPBOARD_COPY_OPTIONS, CLIPBOARD_PASTE_OPTIONS } from "@/io.ox/office/editframework/view/editlabels";

// class EditContextMenu ======================================================

/**
 * Base class for context menus with helper methods that add generic edit
 * commands into the context menu.
 */
export class EditContextMenu extends ContextMenu {

    // public methods ---------------------------------------------------------

    /**
     * Adds entries for clipboard commands "Cut", "Copy", and "Paste" to
     * this context menu.
     */
    addClipboardEntries() {

        // iOS and Android devices provide a native context menu with cut, copy and paste.
        if (!_.device("iOS || Android")) {
            this.addSection("clipboard");
            this.addControl("document/cut",   new Button(CLIPBOARD_CUT_OPTIONS));
            this.addControl("document/copy",  new Button(CLIPBOARD_COPY_OPTIONS));
            this.addControl("document/paste", new Button(CLIPBOARD_PASTE_OPTIONS));
        }
    }
}
