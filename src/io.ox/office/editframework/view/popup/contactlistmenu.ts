/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";

import { type AnyKeyEvent, Rectangle, matchKeyCode, isEscapeKey } from "@/io.ox/office/tk/dom";
import { getButtonValue } from "@/io.ox/office/tk/forms";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { type ContactData, ContactFetcher, createContactPicture, setContactPicture } from "@/io.ox/office/tk/utils/contactutils";
import { type ListMenuConfig, type ListMenuEventMap, ListMenu } from "@/io.ox/office/tk/popup/listmenu";

import "@/io.ox/office/editframework/view/popup/contactlistmenu.less";

// types ======================================================================

export type ContactListMenuConfig = ListMenuConfig<ContactData>;

/**
 * Type mapping for the events emitted by `ContactListMenu` instances.
 */
export interface ContactListMenuEventMap extends ListMenuEventMap<ContactData> {

    /**
     * Will be emitted after a contact has been selected in the list menu.
     *
     * @param contact
     *  The descriptor object for the selected contact.
     */
    "contact:commit": [contact: ContactData];
}

// class ContactListMenu ======================================================

export class ContactListMenu extends ListMenu<ContactData, ContactListMenuConfig, ContactListMenuEventMap> {

    readonly #fetcher = new ContactFetcher({ limit: 20, sort: true });

    // constructor ------------------------------------------------------------

    constructor(config?: ContactListMenuConfig) {

        // base constructor
        super({
            anchorAlign: "center",
            autoFocus: false,
            ...config
        });

        // CSS marker class for styling
        this.$el.addClass("contact-list-menu");

        // handle click events in the list
        this.$el.on("click", ".btn[data-index]", $event => {
            this.#triggerCommit($event.currentTarget as Opt<HTMLElement>);
        });

        // lazily update contact photos of visible list entries
        this.on(["popup:show", "popup:layout"], this.#updateContactPictures);
        this.listenTo(this.el, "scroll", this.#updateContactPictures, { passive: true });
    }

    // public methods ---------------------------------------------------------

    /**
     * Initializes this menu instance with all contacts that fit the passed
     * text.
     *
     * @param name
     *  The user display name that must match all contacts to be inserted.
     *
     * @returns
     *  A promise that will fulfil with all contacts that have been inserted
     *  into this menu; or that will reject if another asynchronous menu
     *  initialization is running in the meantime.
     */
    async initialize(name: string): Promise<ContactData[]> {

        // fetch all matching contacts
        const contacts = await this.#fetcher.fetch(name || "");

        // clear old option buttons *after* query has returned
        this.clearOptions();

        // create the list entries if the query succeeds (no race condition)
        contacts.forEach((contact, index) => {
            this.addOption(contact).attr("data-index", index).append(
                createContactPicture(),
                $('<div class="user-info">').append(
                    $('<div class="user-name">').text(_.noI18n(contact.fullName)),
                    $('<div class="mail-addr">').text(_.noI18n(contact.mailAddr))
                )
            );
        });

        // return sorted contact list to caller
        return contacts;
    }

    /**
     * Processes a keyboard event.
     *
     * @param event
     *  The keyboard event to be processed (DOM event, or JQuery event). Cursor
     *  up/down keys will select another option button. The "Enter" key on a
     *  selected button results in firing a "contact:commit" event.
     */
    handleKeyDownEvent(event: AnyKeyEvent): boolean {

        // do nothing in hidden pop-up menu
        if (!this.isVisible()) { return false; }

        // the selected option button
        const button = this.getSelectedOptions()[0];

        // trigger commit event on Enter key
        if (matchKeyCode(event, "ENTER", { shift: null }) || matchKeyCode(event, "TAB", { shift: null })) {
            this.#triggerCommit(button);
            return true;
        }

        // trigger commit event on Enter key
        if (isEscapeKey(event)) {
            this.hide();
            return true;
        }

        // move selection for cursor (and other) keys
        if (this.selectOptionForKey(event, button)) {
            return true;
        }

        return false;
    }

    // private methods --------------------------------------------------------

    @debounceMethod({ delay: 50 })
    #updateContactPictures(): void {
        const rootRect = Rectangle.from(this.el.getBoundingClientRect());
        for (const button of this.getOptions().filter(":not([data-fetched])").get()) {
            const rect = Rectangle.from(button.getBoundingClientRect());
            if (rootRect.overlaps(rect)) {
                const contact = getButtonValue(button) as Opt<ContactData>;
                setContactPicture($(button).find(".contact-picture"), contact);
                button.dataset.fetched = "true";
            }
        }
    }

    #triggerCommit(button?: HTMLElement): void {
        const contact = getButtonValue(button) as Opt<ContactData>;
        if (contact) { this.trigger("contact:commit", contact); }
    }
}
