/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';
import { Button, TextField } from "@/io.ox/office/editframework/view/editcontrols";

// constants ==================================================================

//#. label for "Replace" button (search/replace toolbar)
const REPLACE_LABEL = gt("Replace");
//#. label for "Replace all" button (search/replace toolbar)
const REPLACE_ALL_LABEL = gt("Replace all");
//#. shorter label for "Replace all" button (search/replace toolbar) for small devices
const REPLACE_ALL_SHORT_LABEL = gt("All");

// class SearchTextField ======================================================

/**
 * A text field used to search for some text in the document. Shows a badge
 * with the number of search results currently highlighted in the document.
 */
class SearchTextField extends TextField {

    constructor(docView) {

        super({
            placeholder: gt("Search ..."),
            tooltip: gt("Find text"),
            width: "25em",
            shrinkWidth: "8em",
            clearButton: true
        });

        // listen to the controller item "document/search/result" and update the info badge
        this.listenTo(docView, "controller:execute", (key, value) => {
            if (key === "document/search/result") {
                const { count } = value;
                if (count === 0) {
                    this.setInfoBadge("");
                } else if (count < 100) {
                    this.setInfoBadge(_.noI18n(String(count)), gt.ngettext("%1$d result found", "%1$d results found", count, count));
                } else {
                    this.setInfoBadge(_.noI18n("99+"), gt("More than 99 results found"));
                }
            }
        });
    }
}

// class ReplaceTextField =====================================================

/**
 * A text field used to replace some text in the document.
 */
class ReplaceTextField extends TextField {

    constructor(/*docView*/) {

        super({
            placeholder: gt("Replace with ..."),
            tooltip: gt("Replacement text"),
            width: "25em",
            shrinkWidth: "8em",
            clearButton: true
        });
    }
}

// class SearchToolBar ========================================================

/**
 * A toolbar with form controls for searching for text in a document.
 */
export class SearchToolBar extends ToolBar {

    constructor(docView) {
        super(docView);

        this.addControl("document/search/text",  new SearchTextField(docView));
        this.addControl("document/search/start", new Button({ icon: "bi:search", tooltip: gt("Start search") }));
        this.addControl("document/search/prev",  new Button({ icon: "bi:chevron-left", tooltip: gt("Select previous search result") }));
        this.addControl("document/search/next",  new Button({ icon: "bi:chevron-right", tooltip: gt("Select next search result") }));
    }
}

// class ReplaceToolBar =======================================================

/**
 * A toolbar with form controls for replacing text in a document.
 */
export class ReplaceToolBar extends ToolBar {

    constructor(docView) {
        super(docView);

        this.addControl("document/replace/text", new ReplaceTextField(docView));
        this.addControl("document/replace/next", new Button({ label: REPLACE_LABEL, tooltip: gt("Replace selected search result and select the next result") }));
        this.addControl("document/replace/all",  new Button({ label: REPLACE_ALL_LABEL, shrinkLabel: REPLACE_ALL_SHORT_LABEL, tooltip: gt("Replace all search results") }));
    }
}
