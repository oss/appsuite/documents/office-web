/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { MessageDialog } from "@/io.ox/office/tk/dialogs";
import { createSpan, createIcon } from "@/io.ox/office/tk/dom";

import { CLIPBOARD_CUT_OPTIONS, CLIPBOARD_COPY_OPTIONS, CLIPBOARD_PASTE_OPTIONS } from "@/io.ox/office/editframework/view/editlabels";

import "@/io.ox/office/editframework/view/dialog/clipboardnoticedialog.less";

// constants ==================================================================

//#. The translated name of the "Control"/"Ctrl" key on the keyboard used on Windows/Linux systems.
const MODIFIER_KEY = _.device("MacOS || iOS") ? _.noI18n("\u2318") : gt("Ctrl");

// class ClipboardNoticeDialog ================================================

/**
 * A modal dialog that displays a message informing that pasting from clipboard
 * is possible via keyboard command only.
 *
 * @param {EditView} docView
 *  The document view instance that has created this dialog.
 */
export default class ClipboardNoticeDialog extends MessageDialog {

    constructor(docView) {

        // base constructor
        super({
            id: "io-ox-office-clipboard-notice-dialog",
            width: 420,
            //#. The title of the cut, copy and paste keyboard shortcut notice dialog
            title: gt("Cut, copy, and paste"),
            showReadOnly: true
        });

        this.docModel = docView.docModel;

        function createTableRow(settings, shortcut) {
            return $("<tr>").attr("title", settings.tooltip).append(
                $("<td>").append(createIcon(settings.icon)),
                $("<td>").append(createSpan({ label: settings.label })),
                $("<td>").append(createIcon("bi:caret-right-fill")),
                $("<td>").append(createSpan({ label: shortcut }))
            );
        }

        // add the input controls to the dialog body
        this.$bodyNode.append(
            $('<div class="form-group">').append(
                $("<div>").text(gt("Some actions are unavailable via the context menu. Please use these shortcuts:"))
            ),
            $('<div class="form-group">').append(
                $("<table>").append(
                    //#. Keyboard shortcut advice for "cut to clipboard"
                    //#. %1$s is the placeholder for the mofifier key "Ctrl" or "Meta"
                    createTableRow(CLIPBOARD_CUT_OPTIONS, gt("%1$s + X", MODIFIER_KEY)),
                    //#. Keyboard shortcut advice for "copy to clipboard"
                    //#. %1$s is the placeholder for the mofifier key "Ctrl" or "Meta"
                    createTableRow(CLIPBOARD_COPY_OPTIONS, gt("%1$s + C", MODIFIER_KEY)),
                    //#. Keyboard shortcut advice for "paste from clipboard"
                    //#. %1$s is the placeholder for the mofifier key "Ctrl" or "Meta"
                    createTableRow(CLIPBOARD_PASTE_OPTIONS, gt("%1$s + V", MODIFIER_KEY))
                )
            )
        );
    }

    destructor() {
        this.docModel.getSelection().restoreBrowserSelection({ forceWhenReadOnly: true });
        super.destructor();
    }
}
