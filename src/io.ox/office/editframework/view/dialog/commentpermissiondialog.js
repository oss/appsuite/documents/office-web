/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import yell from '$/io.ox/core/yell';
import ext from '$/io.ox/core/extensions';
import FilesAPI from '$/io.ox/files/api';

import { BaseDialog } from '@/io.ox/office/tk/dialogs';
import { FILTER_MODULE_NAME } from '@/io.ox/office/tk/utils/io';
import { authorizeGuard, getFileModelFromDescriptor, isGuest, isShareable } from '@/io.ox/office/tk/utils/driveutils';
import { createContactPicture } from '@/io.ox/office/tk/utils/contactutils';

import { COMMENT_USER_PIC_SIZE } from '@/io.ox/office/editframework/view/editlabels';

import '@/io.ox/office/editframework/view/dialog/commentpermissiondialog.less';

// private functions ==========================================================

/**
 * Determines the configuration options for a `CommentPermissionDialog`.
 *
 * @param {EditApplication} docApp
 *
 * @returns {CommentPermissionDialogConfig}
 *  The dialog configuration, with the following properties:
 *  - {string} title
 *    The title for the dialog.
 *  - {string} message
 *    The main message text to be shown in the dialog body.
 *  - {string} okLabel
 *    The label to be shown by the OK button of the dialog.
 *  - {boolean} canShare
 *    Whether to user has the permission to share the edited document with
 *    other users.
 */
function getDialogConfig(docApp) {

    // user is logged in as guest, thus cannot invite mentioned users to visit the document
    if (isGuest()) {
        return {
            title: gt('You cannot use contacts in comments'),
            message: gt('You used a contact in a comment to send this contact a notification. This feature is not supported for your account. No notifications will be sent.'),
            okLabel: gt('Comment anyway'),
            canShare: false
        };
    }

    // user is able to change the permissions of the file, thus can invite mentioned users to visit the document
    var fileDesc = docApp.getFileDescriptor();
    if (isShareable(fileDesc)) {
        return {
            title: gt('Share file "%1$s"', docApp.getFullFileName()),
            message: gt('These recipients do not have "Reviewer" permissions to this file. They will not be able to edit it unless you share the file with them. Every recipient gets an individual email.'),
            okLabel: gt('Share'),
            canShare: true
        };
    }

    // user is not able to change the permissions of the file, thus cannot invite mentioned users
    return {
        title: gt('Someone needs access to this file'),
        message: gt('These recipients do not have permissions to this file. Please contact the owner of the file to grant them permissions.'),
        okLabel: gt('Comment anyway'),
        canShare: false
    };
}

// class CommentPermissionDialog ==============================================

/**
 * To be used to initialize setPermission dialog
 *
 * @param {Array<{id: number, email: string, first_name: string, last_name: string, initial: boolean}>} newMentions
 */
export default class CommentPermissionDialog extends BaseDialog {

    constructor(docView, newMentions) {

        // base constructor
        super({
            id: 'io-ox-office-comment-permission-dialog',
            ...getDialogConfig(docView.docApp)
        });

        var self = this;

        // the application instance needed to send server requests
        var docApp = docView.docApp;

        // information for the edited file to be shared with mentioned users
        var fileDesc = docApp.getFileDescriptor();
        var fileInfo = { folder_id: fileDesc.folder_id, id: fileDesc.id };

        // This list will contain the list of
        // @param Array<{entity: number, group: boolean, bits: number}>
        var updatedObjectPermissionList = [];

        // private methods ----------------------------------------------------

        /**
         * Create usersNode to show the users that require permissions
         *
         * @param {Array} mentions
         * @returns {JQuery}
         */
        function getUserComponent(mentions) {

            return mentions.reduce(function (userListNode, mention) {
                var userNode = $('<div class="permission-user-node">'),
                    userDetails = $('<div class="permission-user-node-details">'),
                    userIdentityType = null,
                    userIdentityEmail = null;

                const pictureNode = createContactPicture({ userId: mention.id ?? -1 }, { size: COMMENT_USER_PIC_SIZE });

                if (!mention.id || mention.is_guest === true) {
                    userIdentityEmail = $('<div class="permission-guest-email">').text(mention.email);
                    userIdentityType = $('<div>').text(gt('Guest'));

                    userDetails.append(userIdentityEmail, userIdentityType);

                } else {
                    // `userInfo.userId` is a string containing userId, firstName and lastName merged using '::'
                    var userElems = mention.userId.split('::');
                    var fName = userElems[2], lName = userElems[3];
                    var userNameNode = $('<div>');
                    var userNameFirst = $('<span>').text(fName ? fName : '');
                    var userNameLast = $('<span class="permission-user-last-name">').text(lName ? lName + ', ' : '');
                    userNameNode.append(userNameLast, userNameFirst);

                    var userIdentityNode = $('<div>');
                    userIdentityType = $('<span>').text(gt('Internal user'));
                    userIdentityEmail = $('<span class="permission-user-email">').text('(' + mention.email + ')');
                    userIdentityNode.append(userIdentityType, userIdentityEmail);

                    userDetails.append(userNameNode, userIdentityNode);
                }

                userNode.append(pictureNode, userDetails);
                return userListNode.append(userNode);

            }, $('<div class="comment-permission-content-users">'));
        }

        /**
         * On generating the dialog, proper content based on user permission is generated
         *
         * @param {{entity: number, group: boolean, bits: number}[]} mentions
         */
        function getDialogContent(mentions) {
            var content = $('<div class="comment-permission-content">');
            if (mentions && (mentions.length > 0) && !isGuest()) {
                content.append(getUserComponent(mentions));
            }
            return content.append($('<div>').text(self.config.message));
        }

        /**
         * Updates the file file Permission on dialog confirm
         */
        function setFileWritePermissionToUsers() {
            var options = {
                cascadePermissions: false
                // notification: { transport: 'mail' }
            };
            return FilesAPI.update(fileInfo, { object_permissions: updatedObjectPermissionList }, options).catch(yell);
        }

        // public methods -----------------------------------------------------

        /**
         * It is responsible for the following
         * 1. Check if current user is guest
         *      1.1 Cannot use mentions to notify mentioned user or share the document
         *      1.2 Show the dialog with options -> "Comment anyway" or "Cancel"
         * 2. Check if there are any mentions
         * 3. Decide if user is allowed to share file mentioned users.
         * 4. Decide if the permission dialog needs to be shown
         *      4.1. Sets the content of the dialog
         *      4.2. Finalize the user list for permission update
         *      4.3. Displays the dialog
         * 5. if NOT then trigger comment save
         */
        this.handlePermission = function (triggerCommentSave) {

            // Check if current user is guest
            if (isGuest()) {

                // Show the dialog without any further edit
                this.append(getDialogContent());
                this.setOkHandler(triggerCommentSave);

                this.show();
                return;
            }

            // DOCS-1689: Filter out users who don't have write permission for a specified file. Put `emails` into the
            // request data, because an external user will not have a user ID but will surely have a mail address.
            var requestParams = {
                action: 'getdocumentuserwriteaccess',
                requestdata: JSON.stringify({
                    folderId: fileInfo.folder_id,
                    fileId: fileInfo.id,
                    emails: newMentions.map(function (user) { return user.email; })
                })
            };

            var request = docApp.sendRequest(FILTER_MODULE_NAME, requestParams, { method: 'POST' });

            // Response sample:
            // {"users":[333,355,6666,2257,2287],"emails":["email@em2.em2","email@email","someemail@email.ad"]}
            // If no user needs permission, the response will be an empty object.
            var promise = request.then(function (resp) {

                // Is there any user who needs permission
                if (!resp.emails || (resp.emails && resp.emails.length === 0)) {
                    // No user needs permission -> then trigger save
                    triggerCommentSave();
                    self.close();
                    return;
                }

                // collect all users who needs permissions from the provided mention list
                var mentionsToShow = [];
                var userIDsNeedPermission = [];
                updatedObjectPermissionList = newMentions.reduce(function (accumulator, mention) {
                    if (resp.emails.indexOf(mention.email) !== -1) {

                        /* Payload for guest user selected from dropdown
                        * {
                            bits: 1
                            type: "guest"
                            email_address: "someemail@email.ad"
                            display_name: "someemail@email.ad"
                            contact_id: 999 // [id] from autocomplete
                            contact_folder: 99 // [folder_id] from autocomplete
                        * }
                        *
                        * Payload randomly typed email as mentions
                        * {bits: 2, type: "guest", email_address: "email@test.test"}
                        **/
                        if (mention.is_guest) {
                            accumulator.push({
                                email_address: mention.email, group: false, bits: 2, type: 'guest'
                            });
                        } else {
                            accumulator.push({
                                entity: mention.id, group: false, bits: 2
                            });
                        }

                        userIDsNeedPermission.push(mention.id);
                        mentionsToShow.push(mention);
                    }

                    return accumulator;
                }, []);

                // Get the existing users with whom the current file is shared with
                FilesAPI.get(fileInfo, { cache: false }).then(function (data) {
                    var existingFilePermissionUsersArr = (data.object_permissions || []);

                    // merge existing permitted users with newly assigned users through mentions (in "updatedObjectPermissionList")
                    existingFilePermissionUsersArr.forEach(function (existingUser) {
                        if (userIDsNeedPermission.indexOf(existingUser.entity) === -1) {
                            updatedObjectPermissionList.push(existingUser);
                        }
                    });
                }).catch(function (error) {
                    yell(error);
                    self.close();
                });

                // Set the content of the dialog
                self.$bodyNode.append(getDialogContent(mentionsToShow));

                // Decide what Ok handler will do
                if (self.config.canShare) {
                    self.setOkHandler(() => setFileWritePermissionToUsers().then(triggerCommentSave));
                } else {
                    self.setOkHandler(triggerCommentSave);
                }

                // If the document is encrypted show the authorization dialog
                if (docApp.isDocumentEncrypted()) {

                    var promise = getFileModelFromDescriptor(fileDesc).then(function (model) {
                        return new ext.Baton({ data: model.toJSON(), models: [model], all: { models: [model] } });
                    });

                    promise.then(function (baton) {
                        // store authorization to the session until used
                        authorizeGuard(baton, { minSingleUse: true }).then(function () {
                            // Display the permission dialog
                            self.show();
                        }, function (err) {
                            self.close();
                            throw err;
                        });
                    });
                } else {
                    // Display the permission dialog
                    self.show();
                }
            });

            promise.catch(function (error) {
                yell(error);
                self.close();
            });
        };
    }
}
