/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';
import { locationHash } from '$/url';

import yell from '$/io.ox/core/yell';
import FilePicker from '$/io.ox/files/filepicker';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getFileBaseName, getPath, getStandardDocumentsFolderId, preparePath } from '@/io.ox/office/tk/utils/driveutils';
import { openEditorChildTab } from '@/io.ox/office/tk/utils/tabutils';

import { trackActionEvent } from '@/io.ox/office/baseframework/utils/apputils';
import { getMessageData } from '@/io.ox/office/baseframework/utils/infomessages';
import { launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';
import { getAppType, getEditableExtensions, isEditable, isNative } from '@/io.ox/office/baseframework/app/extensionregistry';
import { OPEN_DOCUMENT_LABEL } from '@/io.ox/office/baseframework/view/baselabels';

import { getStr, setValue } from '@/io.ox/office/editframework/utils/editconfig';

// globals ====================================================================

// the promise representing a running application launch to prevent multiple calls
var launchPromise = null;

// class OpenDocumentDialog ===================================================

/**
 * A dialog that allows to select files of a specific Documents application
 * in Drive. Will open a new browser tab for the loaded document, if the
 * current browser tab is not already a Documents child tab.
 *
 * @param {string} appType
 *  The application type identifier (e.g. `"text"`).
 */
class OpenDocumentDialog {

    constructor(appType) {
        this._appType = appType;
    }

    // public methods ---------------------------------------------------------

    /**
     * Shows the dialog, waits for user interaction, and opens the selected
     * document in a new browser tab.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when a document has been selected in the
     *  dialog, and the new browser tab for that document has been opened.
     */
    show() {

        var preselect = getStr(this._appType + '/recentOpenDocPath') || getStandardDocumentsFolderId();
        var extensions = getEditableExtensions();
        var filter = extensions.map(function (ext) { return '.' + ext; }).join(',');
        var isPortal = locationHash('app').indexOf('portal') >= 0;

        // Create an own promise to represent the dialog runtime. This is needed because the "Cancel" button of the
        // dialog does not reject the promise returned by the FilePicker constructor function. See bug 67731.
        return jpromise.new(function (fulfilFn, rejectFn) {

            var promise = new FilePicker({
                // filter files of disabled applications (capabilities)
                filter(file) {
                    // Bug 47431: check if there is a file name (String), otherwise return false
                    return _.isString(file.filename) && isEditable(file.filename);
                },
                sorter(file) {
                    return (file.filename || file.title).toLowerCase();
                },
                primaryButtonText: gt('Open'),
                header: OPEN_DOCUMENT_LABEL,
                folder: preselect,
                uploadButton: true,
                multiselect: false,
                //uploadFolder: getStandardDocumentsFolderId(),
                hideTrashfolder: true,
                acceptLocalFileType: filter,
                createFolderButton: false,
                keepDialogOpenOnSuccess: !isPortal,
                // reject the returned promise manually for "Cancel" button (see comment above)
                cancel: () => rejectFn('cancel'),
                tree: {
                    // Filter to disable the attachements folder
                    filter(folder, model) {
                        return !(/^maildrive:\/\/0/).test(model.get('id'));
                    }
                }
            });

            promise.done(function (selectedFiles) {

                var selectedFile = selectedFiles[0];
                if (!selectedFile) { return; }

                if (launchPromise) {
                    globalLogger.warn('OpenDocumentDialog: cannot load multiple documents at the same time');
                    return;
                }

                // show an alert banner with the target folder name
                if (selectedFile.creation_date) {
                    getPath(selectedFile.folder_id).done(function (paths) {
                        var path = preparePath(paths);
                        var messageData = getMessageData('INFO_DOC_SAVED_IN_FOLDER', { fullFileName: selectedFile.title, fullPathName: path, type: 'success', duration: 10000 });
                        yell(messageData.type, messageData.message);
                    });
                }

                const fileName = selectedFile.filename;
                const isNativeFile = isNative(fileName);
                const appType = getAppType(fileName);
                if (!appType) { return; } // TODO: error message?

                trackActionEvent(appType, "toolbar", "open");

                // open document in new browser tab (unless current app is a Documents portal)
                if (!isPortal) {
                    var params = {
                        folder: selectedFile.folder_id,
                        id: selectedFile.id
                    };
                    if (!isNativeFile) {
                        _.extend(params, {
                            convert: true,
                            destfolderid: selectedFile.folder_id,
                            destfilename: getFileBaseName(fileName)
                        });
                    }
                    fulfilFn();
                    openEditorChildTab(appType, params);
                    return;
                }

                // launch new application in active browser tab
                const launchOptions = isNativeFile ?
                    { action: 'load', file: selectedFile } :
                    { action: 'convert', target_folder_id: selectedFile.folder_id, templateFile: selectedFile, preserveFileName: true };

                launchPromise = launchDocsApp(appType, launchOptions).finally(() => { launchPromise = null; });

                setValue(appType + '/recentOpenDocPath', selectedFile.folder_id);

                fulfilFn();
            });
        });
    }
}

// exports ================================================================

export default OpenDocumentDialog;
