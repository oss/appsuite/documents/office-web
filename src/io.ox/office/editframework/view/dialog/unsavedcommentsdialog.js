/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { QueryDialog } from '@/io.ox/office/tk/dialogs';

// constants ==================================================================

const DIALOG_HEADERS = {
    text:         gt('There is an unposted comment in this document.'),
    spreadsheet:  gt('There is an unposted comment on this sheet.'),
    presentation: gt('There is an unposted comment on this slide.')
};

const NEW_COMMENT_MESSAGE = gt('You have added a comment that you have not yet posted. What do you want to do?');
const EDIT_COMMENT_MESSAGE = gt('You have edited a comment that you have not yet posted. What do you want to do?');

//#. button label: discard a new unsaved comment box
const NEW_COMMENT_OK_LABEL = gt('Continue and discard comment');

//#. button label: discard unsaved changes in a comment box
const EDIT_COMMENT_OK_LABEL = gt('Continue and discard edits');

//#. button label: jump back to unsaved contents of a comment box
const CANCEL_LABEL = gt('Back to comment');

// class UnsavedCommentsDialog ================================================

/**
 * A modal dialog asking what to do if the document contains a threaded
 * comment with unsaved changes (a new comment thread, a new reply, or text
 * edits in an existing comment).
 *
 * @param {EditView} docView
 *  The document view instance that has created this dialog.
 *
 * @param {string} commentType
 *  The type of the unsaved comment (one of "new", "reply", or "edit").
 *  This type identifier influences the text labels shown in the dialog.
 */
export default class UnsavedCommentsDialog extends QueryDialog {

    constructor(docView, commentType) {

        // the comments pane
        var commentsPane = docView.commentsPane;
        // special text for new comment frames (new thread, new reply)
        var isNewComment = (commentType === 'new') || (commentType === 'reply');

        // base constructor
        super({
            title: DIALOG_HEADERS[docView.docApp.appType],
            message: isNewComment ? NEW_COMMENT_MESSAGE : EDIT_COMMENT_MESSAGE,
            okLabel: isNewComment ? NEW_COMMENT_OK_LABEL : EDIT_COMMENT_OK_LABEL,
            noLabel: CANCEL_LABEL,
            width: 600,
            // Spreadsheet: close automatically, if active sheet will be deleted or hidden by another client
            autoCloseScope: docView.docApp.isSpreadsheetApp() ? 'activeSheet' : null
        });

        // drop unsaved comments when user clicks the "Discard" button
        this.setOkHandler(() => {
            commentsPane.deleteUnsavedComments();
        });

        // return to unsaved comment when user clicks "Back" button
        this.setCancelHandler(() => {
            // drop changes if dialog was closed automatically due to lost edit rights
            if (docView.docApp.isEditable()) {
                _.defer(() => commentsPane.selectUnsavedComment());
            } else {
                commentsPane.deleteUnsavedComments();
            }
        });
    }
}
