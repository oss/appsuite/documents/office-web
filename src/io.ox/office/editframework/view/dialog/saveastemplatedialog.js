/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { getStandardTemplateFolderId } from '@/io.ox/office/tk/utils/driveutils';
import SaveAsDialog from '@/io.ox/office/tk/dialog/saveasdialog';
import { userTemplateFolders, fetchContextTemplateFolders } from '@/io.ox/office/settings/api';
import { GLOBAL_TEMPLATE_FOLDER, USER_TEMPLATE_FOLDER } from '@/io.ox/office/editframework/view/editlabels';

// constants ==================================================================

// settings key for the last used folder
const RECENT_FOLDER_KEY = 'recentSaveAsTemplate';

// private functions ==========================================================

function getRecentFolderId(docView) {
    return docView.docApp.getUserSettingsValue(RECENT_FOLDER_KEY, getStandardTemplateFolderId());
}

function setRecentFolderId(docView, folderId) {
    if (folderId) { docView.docApp.setUserSettingsValue(RECENT_FOLDER_KEY, folderId); }
}

// class SaveAsTemplateDialog =================================================

/**
 * @param {EditView} docView
 *  The document view instance that has created this dialog.
 *
 * @param {String} title
 *  The title of the dialog.
 *
 * @param {String} fileName
 *  The initial file name to be shown in the text edit control.
 */
export default class SaveAsTemplateDialog extends SaveAsDialog {

    constructor(docView, title, fileName) {

        // base constructor
        super({
            title,
            value: fileName,
            folderId: getRecentFolderId(docView),
            flatPicker: true
        });

        const folderPicker = this.getFolderPicker();
        const userFolders = _.unique([getStandardTemplateFolderId(), ...userTemplateFolders.getFolders()]);
        folderPicker.addFolders(USER_TEMPLATE_FOLDER, userFolders);

        jpromise.floating(fetchContextTemplateFolders().then(folders => {
            folderPicker.addFolders(GLOBAL_TEMPLATE_FOLDER, folders);
        }));

        this.setOkHandler(() => {
            const folderId = this.getFolderId();
            setRecentFolderId(docView, folderId);
            return { folderId, fileName: this.getFileName() };
        });
    }
}
