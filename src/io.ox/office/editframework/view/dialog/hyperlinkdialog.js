/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { fun } from "@/io.ox/office/tk/algorithms";
import { IOS_SAFARI_DEVICE, enableElement } from '@/io.ox/office/tk/dom';
import { BaseDialog } from '@/io.ox/office/tk/dialogs';
import { DObject } from "@/io.ox/office/tk/objects";

import { hasSupportedProtocol, isValidURL } from '@/io.ox/office/editframework/utils/hyperlinkutils';
import { EDIT_HYPERLINK_LABEL, INSERT_HYPERLINK_LABEL, INVALID_HYPERLINK_LABEL } from '@/io.ox/office/editframework/view/editlabels';

// constants ==================================================================

// a unique DOM identifier for the label of the display text input field
const DISPLAY_LABEL_ID = DObject.makeUid();

// a unique DOM identifier for the display text input field
const DISPLAY_INPUT_ID = DObject.makeUid();

// a unique DOM identifier for the label of the URL input field
const URL_LABEL_ID = DObject.makeUid();

// a unique DOM identifier for the URL input field
const URL_INPUT_ID = DObject.makeUid();

// class HyperlinkDialog ======================================================

/**
 * A modal dialog with two text fields allowing to edit the URL and the
 * text representation of a hyperlink.
 *
 * @param {EditView} docView
 *  The document view instance that has created this dialog.
 *
 * @param {string|null} url
 *  The initial URL to be shown in the dialog. If set to `null`, the text
 *  field for the URL will appear empty.
 *
 * @param {string|null} display
 *  The initial display string for the URL to be shown in the dialog. If
 *  set to `null`, the text field for the display string will appear empty.
 *
 * @param {HyperlinkDialogConfig extends BaseDialogConfig} [config]
 *  Configuration options passed to the base class constructor. The
 *  following additional options are supported:
 *  - {boolean} [config.disableDisplay=false]
 *    If set to `true`, the text field for the display string will be
 *    disabled.
 */
export default class HyperlinkDialog extends BaseDialog {

    // whether to keep URL and display text in sync
    #syncDisplay = true;
    // the input field for the URL
    #urlInputNode;
    // the input field for the display text of the URL
    #displayInputNode;

    constructor(docView, url, display, config) {

        // base constructor
        super({
            width: 400,
            title: url ? EDIT_HYPERLINK_LABEL : INSERT_HYPERLINK_LABEL,
            okLabel: gt('Insert'),
            ...config
        });

        // whether to disable to display text field
        const disableDisplay = !!(config && config.disableDisplay);

        this.#syncDisplay = !disableDisplay && (!display || (url === display));

        this.#urlInputNode = $('<input>', {
            value: url || '',
            tabindex: 0,
            id: URL_INPUT_ID,
            role: 'textbox',
            'aria-labelledby': URL_LABEL_ID,
            'aria-required': true
        }).addClass('form-control');

        // the input field for the display text of the URL
        this.#displayInputNode = $('<input>', {
            value: disableDisplay ? '' : (display || url || ''),
            tabindex: 0,
            id: DISPLAY_INPUT_ID,
            role: 'textbox',
            'aria-labelledby': DISPLAY_LABEL_ID,
            'aria-required': true
        }).addClass('form-control');

        // initialization -----------------------------------------------------

        // disable text field for display string if specified
        if (config && config.disableDisplay) {
            enableElement(this.#displayInputNode[0], false);
        }

        // Bug 43868: ios auto correction should be of for input fields
        // TODO: use Forms.createInputNode() to create inputs here, it already has this fix
        if (IOS_SAFARI_DEVICE) {
            this.#urlInputNode.attr({ autocorrect: 'off', autocapitalize: 'none' });
            this.#displayInputNode.attr({ autocorrect: 'off', autocapitalize: 'none' });
        }

        // add the input controls to the dialog body
        this.$bodyNode.append(
            $('<div class="form-group">').append(
                $('<label id="' +  URL_LABEL_ID + '" for="' + URL_INPUT_ID + '">')
                    .text(/*#. "Insert URL" dialog: The URL itself */ gt('URL')),
                this.#urlInputNode
            ),
            $('<div class="form-group">').append(
                $('<label id="' + DISPLAY_LABEL_ID + '" for="' + DISPLAY_INPUT_ID + '">')
                    .text(/*#. "Insert URL" dialog: The display text of the URL */ gt('Text')),
                this.#displayInputNode
            )
        );

        // register a change handler to toggle the insert button
        this.addOkValidator(() => { return this.#getUrl().length > 0; });
        this.validateOnEvent(this.#urlInputNode, 'input');

        // update input fields when typing
        this.listenTo(this.#urlInputNode, 'input', this.#updateUrlField);
        this.listenTo(this.#displayInputNode, 'input', this.#updateDisplayField);

        // Bugfix 40844: when the user focus the displayInputNode via tab or mouse click,
        // the text in the displayInputNode should be selected
        this.listenTo(this.#displayInputNode, 'focus', () => {
            // a workaround is needed for the browsers below
            // otherwise on mouseup the selection is removed again
            if (_.browser.Chrome || _.browser.Safari || _.browser.Edge) {
                // the workaround: it's also mentioned here https://code.google.com/p/chromium/issues/detail?id=4505
                this.setTimeout(() => this.#displayInputNode.select());
            // this is how it's supposed to work normally
            } else {
                this.#displayInputNode.select();
            }
        });

        // handler for the OK button (keep dialog open, if the current URL is invalid)
        this.setOkHandler(() => {
            const currUrl = this.#getUrl();
            const display = disableDisplay ? null : this.#displayInputNode.val();

            // add http: as default if protocol is missing
            const newUrl = (currUrl.indexOf(':') < 0) ? ('http://' + currUrl) : currUrl;

            // show warning and keep the dialog open, if the URL is invalid
            if (!hasSupportedProtocol(newUrl) || !isValidURL(newUrl)) {
                docView.yell({ type: 'warning', message: INVALID_HYPERLINK_LABEL });
                return new $.Deferred().reject();
            }

            // return result object for a valid URL
            return { url: newUrl, text: display };
        }, { keepOpen: 'fail' });

        // add a Remove button, if a URL has been passed
        if (url) {
            this.createActionButton('remove', gt('Remove'), {
                alignLeft: true,
                buttonStyle: 'warning',
                handler: fun.const({ url: null, text: null })
            });
        }
    }

    // private methods ----------------------------------------------------

    /**
     * Returns the trimmed URL from the input field.
     *
     * @returns {String}
     *  The trimmed URL from the input field.
     */
    #getUrl() {
        // bug 46225: remove all embedded whitespace from the URL
        return this.#urlInputNode.val().replace(/[\x00-\x1f\x80-\x9f\s]+/g, '');
    }

    #updateUrlField() {
        const currUrl = this.#getUrl();
        this.#urlInputNode.attr('aria-invalid', currUrl.length === 0);
        if (this.#syncDisplay) { this.#displayInputNode.val(currUrl); }
    }

    #updateDisplayField() {
        this.#syncDisplay = false;
    }
}
