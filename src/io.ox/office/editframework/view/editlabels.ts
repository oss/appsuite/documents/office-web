/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import type { ControlCaptionOptions } from "@/io.ox/office/tk/dom";
import type { ButtonConfig } from "@/io.ox/office/tk/controls";

import type { BaseFrameworkDialogConfig } from "@/io.ox/office/baseframework/view/baselabels";

// re-exports =================================================================

export * from "@/io.ox/office/baseframework/view/baselabels";
export * from "@/io.ox/office/drawinglayer/view/drawinglabels";

// types ======================================================================

/**
 * Additional configuration options for `BaseDialog` used in applications.
 */
export interface EditFrameworkDialogConfig extends BaseFrameworkDialogConfig {

    /**
     * If set to `true`, suppresses closing the dialog automatically, if the
     * document switches to read-only mode. Default value is `false`.
     */
    showReadOnly?: boolean;
}

// constants ==================================================================

/**
 * Size of the user picture shown in comment frames, in browser pixels.
 */
export const COMMENT_USER_PIC_SIZE = 24;

// header labels --------------------------------------------------------------

//#. menu title: file settings and actions (download, rename, etc.)
export const FILE_HEADER_LABEL = gt.pgettext("menu-title", "File");

//#. menu title: font settings, colors, text alignment, etc.
export const FORMAT_HEADER_LABEL = gt.pgettext("menu-title", "Format");

//#. menu title: insert images, charts, hyperlinks, etc.
export const INSERT_HEADER_LABEL = gt.pgettext("menu-title", "Insert");

//#. menu title: font settings (font name, size, text color, bold/italic, etc.)
export const FONT_HEADER_LABEL = gt.pgettext("menu-title", "Font");

//#. menu title: alignment settings for text in paragraphs or table cells
export const ALIGNMENT_HEADER_LABEL = gt.pgettext("menu-title", "Alignment");

//#. menu title: number format settings for table cells
export const NUMBERFORMAT_HEADER_LABEL = gt.pgettext("menu-title", "Number format");

//#. menu title: settings for a single presentation slide
export const SLIDE_HEADER_LABEL = gt.pgettext("menu-title", "Slide");

//#. menu title: settings for the presentation mode
export const PRESENT_HEADER_LABEL = gt.pgettext("menu-title", "Slide Show");

//#. menu title: settings for a single text table
export const TABLE_HEADER_LABEL = gt.pgettext("menu-title", "Table");

//#. menu title: settings for document review (spelling, track/accept/reject changes)
export const REVIEW_HEADER_LABEL = gt.pgettext("menu-title", "Review");

//#. menu title: settings for document comments
export const COMMENT_HEADER_LABEL = gt.pgettext("menu-title", "Comments");

//#. menu title: settings for document review (spelling, track/accept/reject changes)
export const CHANGETRACKING_HEADER_LABEL = gt.pgettext("menu-title", "Change tracking");

//#. menu title: paragraph style sets (including sets of font-size, text color, bold/italic, etc.)
export const PARAGRAPH_STYLES_LABEL = gt.pgettext("menu-title", "Paragraph styles");

//#. menu title: paragraph (including sets of font-size, text color, bold/italic, etc.)
export const PARAGRAPH_LABEL = gt.pgettext("menu-title", "Paragraph");

//#. menu title: list settings (bullet list, numbered list, list-icon, etc.)
export const LIST_SETTINGS_LABEL = gt.pgettext("menu-title", "List settings");

//#. menu title: font styles (font name, size, text color, bold/italic, etc.)
export const FONT_STYLES_LABEL = gt.pgettext("menu-title", "Font styles");

//#. menu title: cell border settings for table cells
export const CELL_BORDER_LABEL = gt.pgettext("menu-title", "Cell border");

// other control label texts --------------------------------------------------

//#. cell border settings for table cells
export const CELL_BORDERS_LABEL = gt.pgettext("borders", "Cell borders");

//#. line color of borders in paragraphs and tables cells
export const BORDER_COLOR_LABEL = gt.pgettext("borders", "Border color");

//#. line style of borders in paragraphs and tables cells
export const BORDER_STYLE_LABEL = gt.pgettext("borders", "Border style");

//#. line width of borders in paragraphs and tables cells
export const BORDER_WIDTH_LABEL = gt.pgettext("borders", "Border width");

//#. clear all manual formatting of text or table cells (back to default style)
export const CLEAR_FORMAT_LABEL = gt("Clear formatting");

/**
 * Menu labels for horizontal text alignment.
 */
export const HOR_ALIGNMENT_LABELS = {
    left:    /*#. horizontal alignment of text in paragraphs or cells */ gt.pgettext("h-alignment", "Left"),
    center:  /*#. horizontal alignment of text in paragraphs or cells */ gt.pgettext("h-alignment", "Center"),
    right:   /*#. horizontal alignment of text in paragraphs or cells */ gt.pgettext("h-alignment", "Right"),
    justify: /*#. horizontal alignment of text in paragraphs or cells */ gt.pgettext("h-alignment", "Justify"),
    auto:    /*#. horizontal alignment of text in paragraphs or cells (automatically dependent on contents: text left, numbers right) */ gt.pgettext("h-alignment", "Automatic")
};

/**
 * Menu labels for vertical text alignment.
 */
export const VERT_ALIGNMENT_LABELS = {
    top:     /*#. vertical alignment of text in table cells */ gt.pgettext("v-alignment", "Top"),
    middle:  /*#. vertical alignment of text in table cells */ gt.pgettext("v-alignment", "Middle"),
    bottom:  /*#. vertical alignment of text in table cells */ gt.pgettext("v-alignment", "Bottom"),
    justify: /*#. vertical alignment of text in table cells */ gt.pgettext("v-alignment", "Justify")
};

// hyperlink labels -----------------------------------------------------------

export const HYPERLINK_LABEL = gt("Hyperlink");

export const EDIT_HYPERLINK_LABEL = gt("Edit hyperlink");

export const REMOVE_HYPERLINK_LABEL = gt("Remove hyperlink");

export const OPEN_HYPERLINK_LABEL = gt("Open hyperlink");

export const INSERT_HYPERLINK_LABEL = gt("Insert hyperlink");

export const INSERT_OR_EDIT_HYPERLINK_LABEL = gt("Insert or edit a hyperlink");

export const INVALID_HYPERLINK_LABEL = gt("This hyperlink is invalid.");

// control options ------------------------------------------------------------

/**
 * Standard options for an "Undo" button for comments.
 */
export const UNDO_BUTTON_OPTIONS = {
    icon: "png:undo",
    tooltip: gt("Revert last operation")
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Redo" button for comments.
 */
export const REDO_BUTTON_OPTIONS = {
    icon: "png:redo",
    tooltip: gt("Restore last operation")
} satisfies ControlCaptionOptions;

/**
 * Standard options for insert comment.
 */
export const INSERT_COMMENT_OPTIONS = {
    icon: "png:comment-add",
    label: gt("Comment"),
    //#. insert a comment into the text
    tooltip: gt("Insert comment")
} satisfies ControlCaptionOptions;

/**
 * Options for "create" buttons using OX AI.
 */
export const OX_AI_OPTIONS_CREATE = {
    icon: "bi:ox-ai",
    //#. opens dialog to create content with AI
    label: gt("Write about"),
    //#. %1$s is the name of the used AI model
    tooltip: gt("Create new content with AI")
} satisfies ControlCaptionOptions;

/**
 * Options for "rephrase" buttons using OX AI.
 */
export const OX_AI_OPTIONS_REPHRASE = {
    icon: "bi:ox-ai",
    //#. opens dialog to rephrase content with AI
    label: gt("Rephrase"),
    //#. %1$s is the name of the used AI model
    tooltip: gt("Rephrase content with AI")
} satisfies ControlCaptionOptions;

/**
 * Options for "summarize" buttons using OX AI.
 */
export const OX_AI_OPTIONS_SUMMARIZE = {
    icon: "bi:ox-ai",
    //#. opens dialog to summarize content with AI
    label: gt("Summarize"),
    //#. %1$s is the name of the used AI model
    tooltip: gt("Summarize content with AI")
} satisfies ControlCaptionOptions;

/**
 * Options for translate buttons using OX AI.
 */
export const OX_AI_OPTIONS_TRANSLATE = {
    icon: "bi:ox-ai",
    //#. opens dialog to translate content with AI
    label: gt("Translate"),
    //#. %1$s is the name of the used AI model
    tooltip: gt("Translate content with AI")
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Reply" button for comments.
 */
export const REPLY_COMMENT_OPTIONS = {
    icon: "bi:reply-fill",
    //#. start to reply in a comment thread
    label: gt.pgettext("comment-actions", "Reply"),
    //#. start to reply in a comment thread
    tooltip: gt.pgettext("comment-actions", "Reply to comment")
} satisfies ControlCaptionOptions;

// clipboard labels -----------------------------------------------------------

/**
 * Options for "Cut to clipboard" controls.
 */
export const CLIPBOARD_CUT_OPTIONS = {
    icon: "bi:scissors",
    //#. cut to system clipboard
    label: gt("Cut"),
    tooltip: gt("Cut to clipboard")
} satisfies ControlCaptionOptions;

/**
 * Options for "Copy to clipboard" controls.
 */
export const CLIPBOARD_COPY_OPTIONS = {
    icon: "bi:files",
    //#. copy to system clipboard
    label: gt("Copy"),
    tooltip: gt("Copy to clipboard")
} satisfies ControlCaptionOptions;

/**
 * Options for "Paste from clipboard" controls.
 */
export const CLIPBOARD_PASTE_OPTIONS = {
    icon: "bi:clipboard-plus",
    //#. paste from system clipboard
    label: gt("Paste"),
    tooltip: gt("Paste from clipboard")
} satisfies ControlCaptionOptions;

// dialog labels --------------------------------------------------------------

/**
 * Dialog title for a warning query dialog.
 */
//#. dialog title for "Do you want to delete" query
export const DELETE_CONTENTS_TITLE = gt("Delete contents");

/**
 * Message text for a warning query dialog.
 */
export const DELETE_CONTENTS_QUERY = gt("Deleting the selected elements cannot be undone. Do you want to continue?");

/**
 * Dialog title for a warning query dialog.
 */
//#. dialog title for "Do you want to delete" query
export const DELETE_ROWS_TITLE = gt("Delete rows");

/**
 * Message text for a warning query dialog.
 */
export const DELETE_ROWS_QUERY = gt("Deleting the selected rows cannot be undone. Do you want to continue?");

/**
 * Dialog title for a warning query dialog.
 */
//#. dialog title for "Do you want to delete" query
export const DELETE_COLUMNS_TITLE = gt("Delete columns");

/**
 * Message text for a warning query dialog.
 */
export const DELETE_COLUMNS_QUERY = gt("Deleting the selected columns cannot be undone. Do you want to continue?");

// options for controls -------------------------------------------------------

/**
 * Standard options for a "Bold" toggle button.
 */
export const BOLD_BUTTON_OPTIONS = {
    icon: "png:font-bold",
    tooltip: gt("Bold"),
    toggle: true,
    dropDownVersion: { label: gt("Bold") }
} satisfies ButtonConfig;

/**
 * Standard options for an "Italic" toggle button.
 */
export const ITALIC_BUTTON_OPTIONS = {
    icon: "png:font-italic",
    tooltip: gt("Italic"),
    toggle: true,
    dropDownVersion: { label: gt("Italic") }
} satisfies ButtonConfig;

/**
 * Standard options for an "Underline" toggle button.
 */
export const UNDERLINE_BUTTON_OPTIONS = {
    icon: "png:font-underline",
    tooltip: gt("Underline"),
    toggle: true,
    dropDownVersion: { label: gt("Underline") }
} satisfies ButtonConfig;

/**
 * Standard options for a "Strike through" toggle button.
 */
export const STRIKEOUT_BUTTON_OPTIONS = {
    icon: "png:font-strikeout",
    tooltip: gt("Strike through"),
    toggle: true,
    dropDownVersion: { label: gt("Strike through") }
} satisfies ButtonConfig;

/**
 * Standard options for a "Clear formatting" button.
 */
export const CLEAR_FORMAT_BUTTON_OPTIONS = {
    icon: "png:reset-formatting",
    tooltip: CLEAR_FORMAT_LABEL,
    skipStateAttr: true,
    dropDownVersion: { label: CLEAR_FORMAT_LABEL }
} satisfies ButtonConfig;

/**
 * Standard options for a "Insert hyperlink" button.
 */
export const INSERT_HYPERLINK_OPTIONS = {
    label: HYPERLINK_LABEL,
    tooltip: INSERT_HYPERLINK_LABEL
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Insert/edit hyperlink" button.
 */
export const HYPERLINK_BUTTON_OPTIONS = {
    icon: "png:hyperlink",
    label: HYPERLINK_LABEL,
    tooltip: INSERT_OR_EDIT_HYPERLINK_LABEL
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Show toolbar" check box.
 */
export const SHOW_TOOLBARS_CHECKBOX_OPTIONS = {
    //#. check box label: show/hide the upper toolbar panel
    label: gt("Show toolbars"),
    //#. check box tooltip: show/hide the upper toolbar panel
    tooltip: gt("Show or hide the toolbars")
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Show comments panel" check box.
 */
export const SHOW_COMMENTSPANE_CHECKBOX_OPTIONS = {
    //#. check box label: show/hide the comments
    label: gt("Show comments"),
    //#. check box tooltip: show/hide the comments
    tooltip: gt("Show or hide the comments")
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Show slide pane" check box.
 */
export const SHOW_SLIDEPANE_CHECKBOX_OPTIONS = {
    //#. check box label: show/hide the sidebar panel
    label: gt("Show sidebar"),
    //#. check box tooltip: show/hide the sidebar panel
    tooltip: gt("Show or hide the sidebar")
} satisfies ControlCaptionOptions;

/**
 * Standard options for a "Show collaborators" check box.
 */
export const SHOW_COLLABORATORS_CHECKBOX_OPTIONS = {
    //#. check box label: show/hide the collaborator list
    label: gt("Show collaborators"),
    //#. check box tooltip: show/hide the collaborator list
    tooltip: gt("Show or hide list of collaborators")
} satisfies ControlCaptionOptions;

// public functions ===========================================================

/**
 * Returns the localized name of a text color in a color scheme.
 *
 * @param index
 *  The one-based (!) index of the text color.
 *
 * @returns
 *  The translated name of the text color.
 */
export function getTextColorName(index: number): string {
    //#. The name of a text color in a color scheme (a color scheme consists
    //#. of two text colors, two background colors, and six accent colors).
    //#. Example result: "Text 1", "Text 2"
    //#. %1$d is the index of the text color
    return gt("Text %1$d", index);
}

/**
 * Returns the localized name of a fill color in a color scheme.
 *
 * @param index
 *  The one-based (!) index of the fill color.
 *
 * @returns
 *  The translated name of the fill color.
 */
export function getFillColorName(index: number): string {
    //#. The name of a background color in a color scheme (a color scheme consists
    //#. of two text colors, two background colors, and six accent colors).
    //#. Example result: "Background 1", "Background 2"
    //#. %1$d is the index of the background color
    return gt("Background %1$d", index);
}

/**
 * Returns the localized name of an accented color in a color scheme.
 *
 * @param index
 *  The one-based (!) index of the accented color.
 *
 * @returns
 *  The translated name of the accented color.
 */
export function getAccentColorName(index: number): string {
    //#. The name of an accent color in a color scheme (a color scheme consists
    //#. of two text colors, two background colors, and six accent colors).
    //#. Example result: "Accent 1", "Accent 2"
    //#. %1$d is the index of the accent color
    return gt("Accent %1$d", index);
}
