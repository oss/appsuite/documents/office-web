/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { BROWSER_TAB_SUPPORT } from "@/io.ox/office/tk/utils/tabutils";

import { importDebugMainModule } from "@/io.ox/office/baseframework/utils/apputils";
import { ATTACH_AS_FILE_TO_MAIL_LABEL, ATTACH_AS_PDF_TO_MAIL_LABEL, SEND_AS_MAIL_LABEL, SEND_CONTENT_AS_NEW_MAIL_LABEL } from "@/io.ox/office/baseframework/view/baselabels";
import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import { BaseView } from "@/io.ox/office/baseframework/view/baseview";

import { COMBINED_TOOL_PANES } from "@/io.ox/office/editframework/utils/editconfig";
import { ToolPaneManager } from "@/io.ox/office/editframework/view/toolpanemanager";
import { TopPane } from "@/io.ox/office/editframework/view/pane/toppane";
import { MainToolPane } from "@/io.ox/office/editframework/view/pane/maintoolpane";
import { SearchPane } from "@/io.ox/office/editframework/view/pane/searchpane";
import { DOWNLOAD_ICON, FILE_HEADER_LABEL, ZOOM_LABEL, ZOOMOUT_BUTTON_OPTIONS, ZOOMIN_BUTTON_OPTIONS } from "@/io.ox/office/editframework/view/editlabels";
import { CollaboratorMenu } from "@/io.ox/office/editframework/view/popup/collaboratormenu";
import { Button, PercentLabel, CompoundButton, NewDocumentButton, ReloadDocumentButton, AcquireEditButton, FileNameField, SaveAsCompoundButton } from "@/io.ox/office/editframework/view/editcontrols";
import { SearchToolBar } from "@/io.ox/office/editframework/view/edittoolbars";
import { ClipboardNoticeDialog, UnsavedCommentsDialog } from "@/io.ox/office/editframework/view/editdialogs";

import "@/io.ox/office/editframework/view/editstyle.less";

// class EditView =============================================================

/**
 * Base class for the application view of all editor applications.
 *
 * @property {ToolPaneManager} toolPaneManager
 *  The collection containing data for all registered tool bar tabs.
 *
 * @property {TopPane} topPane
 *  The top pane containing the tool bar tab buttons, and other global
 *  control elements.
 *
 * @property {MainToolPane} mainPane
 *  The main tool pane containing all existing tool bars.
 *
 * @property {SearchPane} searchPane
 *  The search/replace view pane.
 *
 * @property {CollaboratorMenu} collaboratorMenu
 *  The floating menu displaying the collaborating users.
 *
 * @param {EditApplication} docApp
 *  The application containing this view instance.
 *
 * @param {EditModel} docModel
 *  The document model created by the passed application.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options that are supported by the base
 *  class `BaseView`, with the following additions and differences:
 *  - {boolean} [config.enablePageSettings=false]
 *    If set to `true`, the file toolbar tab will contain a "Page settings"
 *    button to show a page settings dialog for the current document.
 *  - {boolean} [config.showOwnCollaboratorColor=false]
 *    If set to `true`, the current user will be shown like a remote user with
 *    a colored status box in the collaboration floating menu.
 */
export class EditView extends BaseView {

    constructor(docApp, docModel, config) {

        // base constructor
        super(docApp, docModel, {
            ...config,
            classes: "io-ox-office-edit-main"
        });

        // the toolbar tab collection
        this.toolPaneManager = this.member(new ToolPaneManager(this));

        // the toplevel toolpane containing global controls and the toolbar tabs
        this.topPane = null;

        // the main toolpane below the toplevel toolpane containing the toolbars
        this.mainPane = null;

        // the search/replace pane below the main toolpane
        this.searchPane = null;

        // collaborator floating menu
        this.collaboratorMenu = this.member(new CollaboratorMenu(this, { showOwnColor: config?.showOwnCollaboratorColor }));

        // UI labels for different application states
        this._appStateRegistry = new Map/*<string, AppStateConfig>*/();
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a new tab button in the top view pane used to control the
     * visibility of one or more toolbar components in the toolpane of the
     * application view.
     *
     * @param {string} tabId
     *  The unique identifier for the new tab button.
     *
     * @param {Function} renderFn
     *  The callback function invoked once to render the toolpane.
     *
     * @param {ToolPaneConfig} [config]
     *  Configuration options for the tab button and toolpane contents.
     */
    registerToolPane(tabId, renderFn, config) {
        this.toolPaneManager.registerToolPane(tabId, renderFn.bind(this), config);
    }

    /**
     * Inserts a new toolbar into the main toolpane.
     *
     * @param {ToolBarT extends ToolBar} toolBar
     *  The toolbar to be inserted into the main toolpane. The toolpane takes
     *  ownership of the passed toolbar!
     *
     * @param {DynamicToolBarOptions} [options]
     *  Optional parameters.
     *
     * @returns {ToolBarT}
     *  The toolbar instance passed to this method, for convenience.
     */
    insertToolBar(toolBar, options) {
        return this.mainPane.addDynamicToolBar(toolBar, options);
    }

    /**
     * Creates a new toolbar in the tool pane of the application view.
     *
     * @param {ToolBarConfig} [options]
     *  Optional parameters for constructing and inserting the toolbar.
     *
     * @returns {ToolBar}
     *  The new toolbar instance.
     */
    createToolBar(options) {
        return this.insertToolBar(new ToolBar(this, options));
    }

    /**
     * Returns whether the search pane is currently visible.
     *
     * @returns {boolean}
     *  Whether the search pane is currently visible.
     */
    isSearchActive() {
        return this.searchPane.isVisible();
    }

    /**
     * Registers a label text and optional icon for a specific user-defined
     * application state.
     *
     * @param {string} state
     *  The identifier of an application state.
     *
     * @param {AppStateConfig} config
     *  Configuration options for the application state:
     *  - {string} config.label
     *    The label text to be shown while the application state is active.
     *  - {string} [config.icon]
     *    The CSS class of an icon to be shown in front of the label.
     *  - {number} [config.delay]
     *    The delay time before the status label will be shown.
     *  - {boolean} [config.busy=false]
     *    If set to `true`, a rotating busy icon will be shown in front of the
     *    label. This option overrides the option "icon".
     *  - {boolean} [config.center=false]
     *    If set to `true`, the application state will be shown centered in the
     *    container.
     */
    registerAppStateConfig(state, config) {
        this._appStateRegistry.set(state, config);
    }

    /**
     * Returns the configuration for the specified application state.
     *
     * @param {string} state
     *  The identifier of an application state.
     *
     * @returns {Opt<AppStateConfig>}
     *  The configuration for the specified application state, if available.
     */
    getAppStateConfig(state) {
        return this._appStateRegistry.get(state);
    }

    /**
     * Creates a new editable container node for clipboard functionality, and
     * inserts it into the hidden container node of the application.
     *
     * @returns {JQuery}
     *  The new empty clipboard container node.
     */
    createClipboardNode() {
        // clipboard node must be content-editable to allow system copy-to-clipboard
        // clipboard node must contain "user-select-text" to be focusable in Chrome
        const clipboardNode = $('<div class="clipboard user-select-text" contenteditable="true" data-gramm="false" data-focus-role="clipboard">');
        this.insertHiddenNodes(clipboardNode);
        return clipboardNode;
    }

    /**
     * Returns whether the document can be modified currently.
     *
     * @returns {boolean}
     *  Whether the document can be modified.
     */
    isEditable() {
        return this.docApp.isEditable();
    }

    /**
     * Overwrites the base class method `BaseView.enterBusy()` and adds an
     * event handler for the application event "docs:editmode:leave" indicating
     * lost edit rights for the document. The event handler will call the
     * cancel handler passed to this method automatically.
     *
     * @param {object} [options]
     *  Optional parameters. Supports all options that are also supported by
     *  the base class method `BaseView.enterBusy()`, and the following
     *  additional options:
     *  - {boolean} [options.skipCancelOnReadOnly=false]
     *    If set to `true`, the cancel handler passed to this method will NOT
     *    be called automatically, when the document is losing the edit rights.
     */
    enterBusy(options) {

        // call base class before registering handler
        super.enterBusy(options);

        // invoke cancel handler when leaving edit mode
        if (options?.cancelHandler && !options?.skipCancelOnReadOnly) {
            const scope = Symbol("scope");
            this.listenTo(this.docApp, "docs:editmode:leave", options.cancelHandler, { once: true, scope });
            this.listenTo(this.docApp.getWindow(), "idle", () => this.stopListeningToScope(scope), { once: true });
        }
    }

    /**
     * Shows a modal dialog that displays a message informing that clipboard
     * paste is possible via keyboard command only.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil with the action identifier, if the OK button
     *  has been pressed.
     */
    async showClipboardNoticeDialog() {
        await new ClipboardNoticeDialog(this).show();
    }

    /**
     * Shows the open document dialog, waits for user interaction, and opens
     * the selected document in a new browser tab.
     *
     * @returns {Promise<void>}
     *  A promise that will fulfil when a document has been selected in the
     *  dialog, and the new browser tab for that document has been opened.
     */
    async showOpenDocumentDialog() {
        const { default: OpenDocumentDialog } = await import("@/io.ox/office/editframework/view/dialog/opendocumentdialog");
        await new OpenDocumentDialog(this.docApp.appType).show();
    }

    /**
     * Returns the collection with threaded comments that is currently active
     * in this document view. May return `null` to indicate msising support or
     * some other uninitialzed state.
     *
     * This dummy method is intended to be overwritten in subclasses.
     *
     * **ATTENTION**: DO NOT CACHE the return value of this method. The active
     * comment collection may change at any time due to some user interaction.
     *
     * @returns {CommentCollection|null}
     *  The collection with threaded comments that is currently active in this
     *  document view if available, otherwise `null`.
     */
    getCommentCollection() {
        return null;
    }

    /**
     * Activates the collection with threaded comments that contains a comment
     * with unsaved changes. This method will only be called when the document
     * contains an unsaved comment somewhere (as reported by the document model
     * method `Editmodel#hasUnsavedComments`).
     *
     * This dummy method is intended to be overwritten in subclasses.
     *
     * @returns {MaybeAsync}
     *  An optional promise that will fulfil when this method has activated the
     *  collection containing the unsaved comment asynchronously.
     */
    activateUnsavedCommentCollection() {
        return undefined;
    }

    /**
     * Selects the comment frame containing unsaved changes (text edits in an
     * existing comment, a new comment thread, or a reply comment), if
     * available, and shows an alert banner to the user informing about the
     * unsaved changes.
     *
     * @param {boolean} [showDiscardDialog=false]
     *  If set to `true`, and if a comment with unsaved changes exists, a query
     *  dialog will be shown instead of the alert banner. The user can select
     *  to return to the unsaved comment, or to discard the changes and proceed
     *  with the current action.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when there is no comment with unsaved
     *  changes (or if the user has clicked on the "Continue and discard
     *  comment" button in the query dialog); or that will reject if there
     *  exists a comment with unsaved changes (and the user has clicked the
     *  "Return to comment" button in case of a query dialog).
     */
    processUnsavedComment(showDiscardDialog) {

        // early exit if threaded comments are not supported, or there is no unsaved comment
        if (!this.commentsPane || !this.docModel.hasUnsavedComments()) { return jpromise.resolve(); }

        // activate the correct component with the unsaved comment (e.g.,
        // switch to sheet in spreadsheets; scroll to slide in presentations)
        const promise = jpromise.invoke(() => this.activateUnsavedCommentCollection());

        // handle the unsaved comment in the activated collection
        return promise.then(() => {

            // get data of the unsaved comment from the collection (early exit with
            // fulfilled promise to indicate that no unsaved comment is available)
            const collection = this.getCommentCollection();
            const unsavedData = collection?.getUnsavedComment();
            if (!unsavedData) { return; }

            // select the comment with unsaved changes in the comments pane
            this.commentsPane.selectUnsavedComment();

            // dialog mode: show a query dialog and wait for the answer
            if (showDiscardDialog) {
                const commentType = unsavedData.isNewThread ? "new" : unsavedData.isNewReply ? "reply" : "edit";
                return new UnsavedCommentsDialog(this, commentType).show();
            }

            // alert mode: show an alert banner and return a rejected promise
            this.yell({ message: gt("Your document contains unposted comments.") });
            return jpromise.reject();
        },

        // catch errors from activation (rejected promise would indicate to keep unsaved comment)
        err => {
            globalLogger.exception(err, "while activating unsaved comments collection");
        });
    }

    /**
     * Shows and selects the comment thread that is currently in edit mode, if
     * available.
     */
    selectEditComment() {
        // select the comment with unsaved changes in the comments pane
        this.commentsPane?.selectEditComment();
    }

    // protected methods ------------------------------------------------------

    /**
     * Initialization after construction.
     *
     * @returns {Promise<void>}
     *  A promise that fulfils after initialization.
     */
    /*protected override*/ async implInitialize() {

        this.listenTo(this.docApp, "docs:editmode", editMode => {
            // indicates the document edit mode
            this.docApp.getWindowNode().toggleClass("doc-editable", editMode);
        });

        // register labels for common application states
        //#. label shown in the toolbar while the document is loading
        this.registerAppStateConfig("preview", { label: gt("Loading document"), busy: true, center: true });
        //#. label shown in the toolbar while user changes are sent to the server
        this.registerAppStateConfig("sending", { label: gt("Saving changes"), busy: true, delay: 200 });
        //#. label shown in the toolbar when all changes have been sent to the server
        this.registerAppStateConfig("ready",   { label: gt("All changes saved"), icon: "bi:check" });

        // create the tool bars (initialize themselves internally)
        this.topPane = this.addPane(new TopPane(this));
        this.mainPane = this.addPane(new MainToolPane(this));
        this.searchPane = this.addPane(new SearchPane(this));

        // hide search pane when application is in internal error state
        this.listenTo(this.docApp, "docs:state:error", () => this.searchPane.hide());

        // DOCS-1541: set the focus into the document
        if (BROWSER_TAB_SUPPORT && _.browser.Safari) {
            this.listenToGlobal("tab:show", () => this.grabFocus({ restoreBrowserSelection: true }));
        }

        // call initialization handler passed from sub class constructor
        const { initHandler } = this.config;
        if (initHandler) { await initHandler.call(this); }
    }

    /**
     * Initialization of debug mode after construction.
     *
     * @returns {Promise<void>}
     *  A promise that fulfils after initialization.
     */
    /*protected override*/ async implInitializeDebug() {
        // extend the view with debug functionality from the debug package
        await importDebugMainModule(this.docApp, module => module.initializeView(this));
    }

    /**
     * Initialization after importing the document. Needed to be executed after
     * import, to be able to hide specific GUI elements depending on the file
     * type.
     */
    /*protected override*/ async implInitializeGui(/*options*/) {

        // initialize all tool panes of the document view
        this.implInitToolPanes();

        // initialize the "View" dropdown menu lazily
        const { viewButton } = this.topPane;
        this.listenTo(viewButton.menu, "popup:beforeshow", () => this.implInitViewMenu(viewButton), { once: true });

        // create a "debug" toolpane if the debug package exists
        await importDebugMainModule(this.docApp, module => {
            this.registerToolPane("debug", () => module.initializeToolPane(this), { label: _.noI18n("Debug"), trailing: true, priority: 9999 });
        });

        // hide "Replace" pane in "combined panes" mode when selecting other toolpanes than "Search"
        this.toolPaneManager.on("tab:activate", (_newEntry, oldEntry) => {
            if (COMBINED_TOOL_PANES && (oldEntry?.tabId === "search")) {
                if (this.searchPane.isVisible()) {
                    this.searchPane.hide();
                } else {
                    this.executeControllerItem("document/search/end");
                }
            }
        });
    }

    /**
     * Subclasses need to overwrite this method to register additional
     * toolpanes.
     */
    /*protected*/ implInitToolPanes() {
        this.registerToolPane("file",   this._initFileToolPane,   { label: FILE_HEADER_LABEL, visibleKey: "view/toolpane/file/visible", priority: 10 });
        this.registerToolPane("search", this._initSearchToolPane, { priority: 10 });
    }

    /**
     * Subclasses need to overwrite this method to initialize the contents of
     * the "View options" menu lazily.
     */
    /*protected*/ implInitViewMenu(viewButton) {
        viewButton.addSection("zoom", { label: ZOOM_LABEL, inline: true });
        viewButton.addControl("view/zoom/dec", new Button(ZOOMOUT_BUTTON_OPTIONS), { sticky: true });
        viewButton.addControl("view/zoom",     new PercentLabel({ textAlign: "center" }));
        viewButton.addControl("view/zoom/inc", new Button(ZOOMIN_BUTTON_OPTIONS), { sticky: true });
    }

    // private methods --------------------------------------------------------

    /*private*/ _initFileToolPane() {

        if (BROWSER_TAB_SUPPORT) {
            const newToolBar = this.createToolBar({ visibleKey: "app/bundled" });
            newToolBar.addControl("view/new/menu", new NewDocumentButton(this));

            const openToolBar = this.createToolBar({ visibleKey: "app/bundled" });
            openToolBar.addControl("document/open", new Button({ label: gt("Open document") }));
        }

        if (!COMBINED_TOOL_PANES) {
            const renameToolBar = this.createToolBar();
            renameToolBar.addControl("document/rename", new FileNameField(this));

            const saveAsToolBar = this.createToolBar({ visibleKey: "app/bundled" });
            saveAsToolBar.addControl("view/saveas/menu", new SaveAsCompoundButton(this));
        }

        if (this.config.enablePageSettings) {
            const pageToolBar = this.createToolBar();
            const tooltip = this.docModel.useSlideMode() ? gt("Slide settings") : gt("Page settings");
            pageToolBar.addControl("document/pagesettings", new Button({ icon: "png:page-layout", tooltip, dropDownVersion: { label: tooltip } }));
        }

        const downloadToolBar = this.createToolBar({ shrinkToMenu: { label: gt("Actions") } });
        downloadToolBar.addControl("document/download", new Button({ icon: DOWNLOAD_ICON, tooltip: gt("Download"), dropDownVersion: { label: gt("Download") } }));
        downloadToolBar.addControl("document/print",    new Button({ icon: "bi:printer", tooltip: gt("Print as PDF"), dropDownVersion: { label: gt("Print as PDF") } }), { visibleKey: "document/print" });
        // "Send as mail" button vs. dropdown menu
        const canConvertToPDFAttachment = this.docApp.canConvertTo("pdfAttachment");
        const canConvertToInlineHTML = this.docApp.canConvertTo("inlineHtml");
        if (canConvertToPDFAttachment || canConvertToInlineHTML) {
            const btnSendMail = new CompoundButton(this, { icon: "bi:envelope", tooltip: SEND_AS_MAIL_LABEL, dropDownVersion: { label: SEND_AS_MAIL_LABEL } });
            btnSendMail.addControl("document/sendmail", new Button({ label: ATTACH_AS_FILE_TO_MAIL_LABEL }));
            if (canConvertToPDFAttachment) {
                btnSendMail.addControl("document/sendmail/pdf-attachment", new Button({ value: { attachMode: "pdfAttachment" }, label: ATTACH_AS_PDF_TO_MAIL_LABEL }));
            }
            if (canConvertToInlineHTML) {
                btnSendMail.addControl("document/sendmail/inline-html", new Button({ value: { attachMode: "inlineHtml" }, label: SEND_CONTENT_AS_NEW_MAIL_LABEL }));
            }
            downloadToolBar.addControl("document/sendmail/menu", btnSendMail);
        } else {
            downloadToolBar.addControl("document/sendmail", new Button({ icon: "bi:envelope", tooltip: SEND_AS_MAIL_LABEL, dropDownVersion: { label: SEND_AS_MAIL_LABEL } }));
        }

        const shareToolBar = this.createToolBar({ visibleKey: "document/share" });
        shareToolBar.addControl("document/share/inviteuser", new Button({ icon: "bi:share", tooltip: gt("Share / Permissions"), dropDownVersion: { label: gt("Share / Permissions") } }), { visibleKey: "document/share/inviteuser" });

        const actionToolBar = this.createToolBar();
        actionToolBar.addControl("document/acquireeditOrPending", new AcquireEditButton(this), { visibleKey: "document/acquireedit" });
        actionToolBar.addControl("document/reload",               new ReloadDocumentButton(),  { visibleKey: "document/reload" });
    }

    /*private*/ _initSearchToolPane() {
        this.insertToolBar(new SearchToolBar(this));
    }
}
