/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, itr, ary } from "@/io.ox/office/tk/algorithms";
import type { IconId } from "@/io.ox/office/tk/dom";
import { getDebugFlag } from "@/io.ox/office/tk/config";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { layoutLogger } from "@/io.ox/office/tk/utils/logger";

import { ViewObject } from "@/io.ox/office/baseframework/view/viewobject";

import type { EditView } from "@/io.ox/office/editframework/view/editview";

// types ======================================================================

/**
 * Configuration of a toolpane to be shown in the application view, together
 * with a tab button in the toppane.
 */
export interface ToolPaneConfig {

    /**
     * The identifier of an icon for the tab button in the toppane.
     */
    icon?: IconId;

    /**
     * The label text for the tab button. Can be omitted for toolpanes with
     * dynamic labels (see option "labelKey").
     */
    label?: string;

    /**
     * The keys of controller items that control the visibility of the tab
     * button (and thus the availability of the toolpane). The visibility will
     * be bound to the "enabled" state of the respective controller items. If
     * set to multiple keys, all controller items must be enabled. If a key
     * starts with an exclamation mark, the enabled state will be reversed. If
     * omitted, the tab button will always be visible.
     */
    visibleKey?: string | string[];

    /**
     * If specified, the key of a controller item whose value will be used to
     * update the label text of the tab button dynamically.
     */
    labelKey?: string;

    /**
     * The priority of the toolpane and tab button. Used to decide which
     * toolpane gets activated when the active toolpane has been hidden by a
     * controller item. The higher the number, the lower the priority of the
     * toolpane. Default value is `0`.
     */
    priority?: number;

    /**
     * Whether the toolpane should stay active regardless of other toolpanes
     * becoming visible. Default value is `false`.
     */
    sticky?: boolean;
}

/**
 * Readonly version of `ToolPaneEntry` instances.
 */
export type ToolPaneEntryRO = Readonly<ToolPaneEntry>;

/**
 * A renderer callback function invoked while a toolpane will be activated for
 * the first time. Implementations should put all code for creating toolbars
 * and form controls into this function for improved startup performance of the
 * entire application.
 */
export type ToolPaneRenderFn = (this: ToolPaneEntryRO) => void;

/**
 * Type mapping for the events emitted by `ToolPaneManager` instances.
 */
export interface ToolPaneManagerEventMap {

    /**
     * Will be emitted after a new toolpane and tab button have been created.
     *
     * @param entry
     *  Configuration of the new toolpane.
     */
    "tab:create": [entry: ToolPaneEntryRO];

    /**
     * Will be emitted after a toolpane has been shown or hidden.
     *
     * @param entry
     *  Configuration of the toggled toolpane.
     */
    "tab:toggle": [entry: ToolPaneEntryRO];

    /**
     * Will be emitted after the label text of a tab button has been changed
     * dynamically due to the current value of a controller item.
     *
     * @param entry
     *  Configuration of the toolpane.
     */
    "tab:label": [entry: ToolPaneEntryRO];

    /**
     * Will be emitted before a new toolpane will be activated.
     *
     * @param newEntry
     *  Configuration of the activated toolpane; or `undefined`, if the current
     *  toolpane has been deactivated.
     *
     * @param oldEntry
     *  Configuration of the deactivated toolpane; or `undefined`, if no
     *  toolpane was activated.
     */
    "tab:activate": [newEntry: Opt<ToolPaneEntryRO>, oldEntry: Opt<ToolPaneEntryRO>];

    /**
     * Will be emitted after a new toolpane has been activated.
     *
     * @param newEntry
     *  Configuration of the activated toolpane; or `undefined`, if the current
     *  toolpane has been deactivated.
     *
     * @param oldEntry
     *  Configuration of the deactivated toolpane; or `undefined`, if no
     *  toolpane was activated.
     */
    "tab:activated": [newEntry: Opt<ToolPaneEntryRO>, oldEntry: Opt<ToolPaneEntryRO>];
}

// class ToolPaneEntry ========================================================

/**
 * Stores all settings for a single toolpane and its associated tab button.
 */
export class ToolPaneEntry {

    readonly tabId: string;
    readonly renderFn: ToolPaneRenderFn;
    readonly visibleKeys: string[];
    readonly labelKey: string;
    readonly priority: number;
    readonly sticky: boolean;
    icon: Opt<string>;
    label: string;
    visible: boolean;

    // constructor ------------------------------------------------------------

    constructor(tabId: string, renderFn: ToolPaneRenderFn, config?: ToolPaneConfig) {
        this.tabId = tabId;
        this.renderFn = renderFn;
        this.visibleKeys = ary.wrap(config?.visibleKey);
        this.labelKey = config?.labelKey ?? "";
        this.priority = config?.priority ?? 0;
        this.sticky = !!config?.sticky;
        this.icon = config?.icon;
        this.label = config?.label || "";
        // initially hide toolpanes with existing visibility key
        this.visible = this.visibleKeys.length === 0;
    }
}

// class ToolPaneManager ======================================================

/**
 * An instance of this class contains all registered toolpanes.
 */
export class ToolPaneManager extends ViewObject<EditView, ToolPaneManagerEventMap> {

    // properties -------------------------------------------------------------

    // storage for all registered toolpanes
    readonly #registry = new Map<string, ToolPaneEntry>();
    // identifiers of uninitialized toolpanes
    readonly #pendingIds = new Set<string>();

    // identifier of the active toolpane
    #activeTabId: string | null = null;
    // identifier of the last active toolpane, while no toolpane is active (bug 39071)
    #lastTabId: string | null = null;
    // identifier of the toolpane that is currently rendered
    #renderTabId: string | null = null;
    // whether the first toolpane must be made visible
    #forceFirstTab = false;

    // constructor ------------------------------------------------------------

    constructor(docView: EditView) {

        // base constructor
        super(docView);

        // show first toolpane after import
        docView.waitForImport(() => {
            if (this.docApp.showMainToolPaneAtStart()) {
                this.#forceFirstTab = true;
            }
            // render toolpanes in a background idle loop
            this.listenTo(document, ["keydown", "keyup", "mousedown", "mouseup", "touchstart", "touchend"], this.#renderNextPendingToolPane, { capture: true });
            this.#renderNextPendingToolPane();
        });

        // update visibility of the toolpanes according to controller
        this.listenTo(docView, "controller:update", this.#updateControllerHandler);

        // DOCS-1602: do not handle controller updates during reload phase
        this.listenTo(this.docApp, "docs:reload:prepare", () => {
            this.stopListeningTo(docView, "controller:update");
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a new toolpane.
     *
     * @param tabId
     *  The unique identifier for the new toolpane.
     *
     * @param renderFn
     *  The renderer callback function invoked while the toolpane will be
     *  activated for the first time.
     *
     * @param [config]
     *  Configuration options for the toolpane.
     */
    registerToolPane(tabId: string, renderFn: ToolPaneRenderFn, config?: ToolPaneConfig): void {

        if (this.#registry.has(tabId)) {
            layoutLogger.error(`$badge{ToolPaneManager} registerToolPane: toolpane "${tabId}" already registered`);
        } else {
            layoutLogger.log(`$badge{ToolPaneManager} registerToolPane: registering toolpane "${tabId}"`);
        }

        // the entry for the new toolpane
        const entry = new ToolPaneEntry(tabId, renderFn, config);

        // insert new entry and notify listeners
        this.#registry.set(tabId, entry);
        this.#pendingIds.add(tabId);
        this.trigger("tab:create", entry);
    }

    /**
     * Returns the identifier of the active toolpane.
     *
     * @returns
     *  The identifier of the active toolpane.
     */
    getActiveTabId(): string | null {
        return this.#activeTabId;
    }

    /**
     * Checks, whether the specified toolpane is currently active.
     *
     * @param tabId
     *  The unique identifier of a toolpane.
     *
     * @returns
     *  Whether the specified toolpane is currently active.
     */
    isToolPaneActive(tabId: string): boolean {
        return tabId === this.#activeTabId;
    }

    /**
     * Activates the specified toolpane. If another toolpane was active, a
     * "tab:deactivate" event will be triggered for that toolpane. Afterwards,
     * if the passed toolpane identifier is valid, a "tab:activate" event will
     * be triggered.
     *
     * @param tabId
     *  The identifier of the toolpane to be activated. The value `null` will
     *  just deactivate the current toolpane.
     */
    activateToolPane(tabId: string | null): void {

        // nothing to do if the active tab does not change
        if (this.#activeTabId === tabId) { return; }

        // initialize new toolpanes
        if (tabId) {
            this.#renderToolPane(tabId);
        }

        // update the active identifier
        const oldTabId = this.#activeTabId;
        this.#activeTabId = tabId;

        // trigger event to activate the new toolpane
        const oldEntry = this.#getEntry(oldTabId);
        const newEntry = this.#getEntry(tabId);
        this.trigger("tab:activate", newEntry, oldEntry);

        // bug 39071: rescue last tab identifier, to be able to restore later
        if (!tabId) { this.#lastTabId = oldTabId; }

        // do not expose the async update code to caller
        this.onSettled(this.docApp.docController.update(), () => {
            this.trigger("tab:activated", newEntry, oldEntry);
        });
    }

    /**
     * Restores the last visible toolpane, if no toolpane is currently visible.
     */
    restoreActiveToolPane(): void {
        if (!this.#activeTabId) {
            if (this.#lastTabId) {
                this.activateToolPane(this.#lastTabId);
                this.#lastTabId = null;
            } else {
                this.#activateBestToolPane();
            }
        }
    }

    /**
     * Returns the identifier of the toolpane that is currently being rendered.
     *
     *
     * @returns
     *  The identifier of the toolpane currently rendered. Will only return a
     *  valid tab identifier while a renderer callback function of a registered
     *  toolpane is being executed, otherwise `null`.
     */
    getRenderingTabId(): string | null {
        return this.#renderTabId;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the registry entry of the specified toolpane.
     *
     * @param tabId
     *  The identifier of the toolpane.
     */
    #getEntry(tabId: string | null): Opt<ToolPaneEntry> {
        return tabId ? this.#registry.get(tabId) : undefined;
    }

    /**
     * Activates the first visible toolpane from the passed array of toolpanes
     * (the first visible toolpane that has a higher priority than the active
     * toolpane, if available in the passed array).
     *
     * @param entries
     *  A custom collection of toolpanes to be used to activate a toolpane.
     *
     * @returns
     *  Whether a visible toolpane has been found and activated successfully.
     */
    #activateBestToolPane(entries?: ToolPaneEntry[]): boolean {

        // fallback to all toolpanes if nothing was passed
        const sourceEntries = Array.from(entries ?? this.#registry.values());

        // filter by visible toolpane
        let visibleEntries = sourceEntries.filter(entry => entry.visible);

        // filter by priority of the active tab
        const activeEntry = this.#getEntry(this.#activeTabId);
        if (activeEntry?.visible) {
            visibleEntries = visibleEntries.filter(entry => entry.priority < activeEntry.priority);
        }

        // no toolpane available for activation
        if (!visibleEntries.length) { return false; }

        // activate toolpane with highest priority
        ary.sortBy(visibleEntries, entry => entry.priority);
        this.activateToolPane(visibleEntries[0].tabId);

        // flag for first toolpane visibility no longer needed
        this.#forceFirstTab = false;

        return true;
    }

    /**
     * Updates the visibility of the tab buttons.
     */
    #updateControllerHandler(): void {

        // collect all visible toolpanes (*before* updating visibility!)
        const visibleEntries = ary.filterFrom(this.#registry.values(), entry => entry.visible);
        // collect all hidden toolpanes (*before* updating visibility!)
        const hiddenEntries = ary.filterFrom(this.#registry.values(), entry => !entry.visible);
        // the active toolpane
        let activeEntry = this.#getEntry(this.#activeTabId);

        // update visibility and label text of all tab buttons
        this.#registry.forEach(entry => {

            // tab buttons without visibility key are always visible
            const visible = entry.visibleKeys.every(key => this.docView.isControllerItemEnabled(key));
            // get dynamic label text
            const label = entry.labelKey ? this.docView.getControllerItemValue(entry.labelKey) : null;

            // toggle the tab button, if the visibility state has changed
            if (entry.visible !== visible) {
                entry.visible = visible;
                this.trigger("tab:toggle", entry);
            }

            // change the label text of the tab button
            if (is.string(label) && (entry.label !== label)) {
                entry.label = label;
                this.trigger("tab:label", entry);
            }
        });

        // nothing more to do, if active toolpane is visible *and* sticky
        if (activeEntry?.visible && activeEntry.sticky) { return; }

        // do not activate a toolpane, if some are visible, but none is active
        // -> but do activate the toolpane, if the flag 'forceFirstTab' is true.
        if (visibleEntries.length && !activeEntry && !this.#forceFirstTab) { return; }

        // activate a specific toolpane at first load (e.g. restore the last used toolpane after a reload)
        const tabAfterLoad = this.#forceFirstTab ? this.docApp.getLaunchOption("activeTabId") : null;
        if (is.string(tabAfterLoad) && tabAfterLoad) {

            this.activateToolPane(tabAfterLoad);
            // flag for first toolpane visibility no longer needed
            this.#forceFirstTab = false;

            // fallback when specified toolpane is not available (e.g. readonly mode after reload)
            activeEntry = this.#getEntry(this.#activeTabId);
            if (!activeEntry?.visible) {
                this.#activateBestToolPane();
            }

        } else {
            // if the active toolpane is now hidden, activate the first visible toolpane; otherwise
            // activate the first toolpane with higher priority that changed from hidden to visible
            this.#activateBestToolPane(activeEntry?.visible ? hiddenEntries : undefined);
        }
    }

    /**
     * Renders the specified toolpane by calling its renderer callback.
     */
    #renderToolPane(tabId: string): void {
        if (this.#pendingIds.delete(tabId)) {
            layoutLogger.takeTime(`$badge{ToolPaneManager} renderToolPane: tabId="${tabId}"`, () => {
                this.#renderTabId = tabId;
                try {
                    this.#getEntry(tabId)?.renderFn();
                } catch (err) {
                    layoutLogger.exception(err);
                }
                this.#renderTabId = null;
            });
        }
    }

    /**
     * Renders an uninitialized toolpane after a long debounce timeout.
     */
    @debounceMethod({ delay: 2500 })
    #renderNextPendingToolPane(): void {

        // background initialization can be disabled in debug mode
        if (getDebugFlag("office:block-toolpane-init")) { return; }

        // find a pending toolpane to be rendered (render very large toolpanes first)
        const nextTabId = ["drawing", "insert"].find(tabId => this.#pendingIds.has(tabId)) || itr.first(this.#pendingIds);
        if (!nextTabId) {
            this.stopListeningTo(document);
            return;
        }

        // render the toolpane (invoke its rendering callback)
        this.#renderToolPane(nextTabId);

        // start new timer for the next pending toolpane
        this.#renderNextPendingToolPane();
    }
}
