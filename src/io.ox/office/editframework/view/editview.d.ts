/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ToolBarConfig, ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import { DynamicToolBarOptions } from "@/io.ox/office/baseframework/view/pane/dynamicpane";
import { BaseViewEventMap, BaseView } from "@/io.ox/office/baseframework/view/baseview";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import { ToolPaneManager } from "@/io.ox/office/editframework/view/toolpanemanager";
import { CommentsPane } from "@/io.ox/office/editframework/view/pane/commentspane";
import { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// types ======================================================================

export interface AbstractCommentCollection {
    hasUnsavedComment(): boolean;
    deleteUnsavedComment(): void;
}

/**
 * Type mapping for the events emitted by `EditView` instances.
 */
export interface EditViewEventMap extends BaseViewEventMap { }

// class EditView =============================================================

export abstract class EditView<EvtMapT extends EditViewEventMap = EditViewEventMap> extends BaseView<EvtMapT> {

    declare readonly docApp: EditApplication;
    declare readonly docModel: EditModel;
    declare readonly commentsPane: Opt<CommentsPane<EditView>>;

    readonly toolPaneManager: ToolPaneManager;

    protected constructor(docApp: EditApplication, docModel: EditModel, config?: unknown);

    isEditable(): boolean;

    insertToolBar<ToolBarT extends ToolBar>(toolBar: ToolBarT, options?: DynamicToolBarOptions): ToolBarT;
    createToolBar(options?: ToolBarConfig): ToolBar;

    abstract getCommentCollection(): AbstractCommentCollection | null;
}
