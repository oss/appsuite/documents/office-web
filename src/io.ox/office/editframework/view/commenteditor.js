/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { is, fun } from '@/io.ox/office/tk/algorithms';
import { escapeHTML, matchKeyCode, isEscapeKey } from '@/io.ox/office/tk/dom';
import { parseAndSanitizeHTML } from '@/io.ox/office/tk/utils';
import { EObject } from '@/io.ox/office/tk/objects';

import { MENTIONS_ENABLED } from '@/io.ox/office/editframework/utils/editconfig';
import { OX_PROVIDER_ID, resolveUserId } from '@/io.ox/office/editframework/utils/operationutils';
import { ContactListMenu } from '@/io.ox/office/editframework/view/popup/contactlistmenu';

// private functions ==========================================================

/**
 * Returns the position of the text caret in the comment editor, used as anchor
 * for the contacts list menu for mentions.
 */
function getCaretRect() {
    const currentNode = window.getSelection().anchorNode;
    // DOCS-4288: Safari loses text selection of textarea when clicking in the
    // contacts dropdown menu, resulting in an auto-close of the menu element
    // before the click event can be processed. Fix is to remember and return
    // the last valid position of the caret.
    // TODO: Better fix would be to create a distinct <span> element for the
    // mention currently being edited, and use that element as menu anchor.
    // This would make the code more robust against browser-specific behavior
    // of text selection vs. browser focus.
    const caretRect = fun.do(() => {
        if (currentNode?.nodeType === 3) {
            var range = document.createRange();
            range.selectNodeContents(currentNode);
            var rects = range.getClientRects();
            return rects[rects.length - 1];
        }
        return currentNode?.getBoundingClientRect();
    });
    if (caretRect) { getCaretRect.lastCaretRect = caretRect; }
    return getCaretRect.lastCaretRect;
}


// Detect if `text` contains the full name of the passed `contact` API data
function textContainsContactName(text, contact) {

    const firstName = contact.firstName.trim().toLowerCase();
    const lastName = contact.lastName.trim().toLowerCase();
    text = text.toLowerCase();

    return !!(firstName && lastName) && (
        text.includes(`${firstName} ${lastName}`) ||
        text.includes(`${firstName}, ${lastName}`) ||
        text.includes(`${lastName} ${firstName}`) ||
        text.includes(`${lastName}, ${firstName}`)
    );
}

// class CommentEditor ========================================================

export default class CommentEditor extends EObject {

    constructor(commentModel, isReply, isNewComment) {

        super();

        var self = this;

        var placeholder = !MENTIONS_ENABLED ? "" : isReply ? gt('@mention or reply...') : gt('@mention or comment...');

        var commentTextFrame = $('<div class="comment-text-frame ignorepageevent" contenteditable="true" data-gramm="false">').attr("data-placeholder", placeholder);

        const contactListMenu = this.member(new ContactListMenu({ anchor: getCaretRect, focusableNodes: commentTextFrame }));

        var trackSelection = null,
            shouldPreventDefault = false,
            mentionDelimiter = ' @',
            windowSelectionTextContentHistory = null,
            listKeyWordInfo = null;

        // private functions ======================================================

        /**
         *  Get current word by caret position
         *
         *  @param {number} caretPos
         *  @param {string} text
         *  @returns {Object<{keyword: string, startIndex?: number, endIndex?: number, caretPos?: number}> | null}
         */
        function getKeywordInfo(caretPos, text) {
            if (!caretPos || !text || !MENTIONS_ENABLED) {
                return null;
            }

            // is mention at start of line
            var isMentionAtSOL = mentionDelimiter.length === 1;

            var preText = text.substring(0, caretPos),
                postText = text.substring(caretPos, text.length);

            var lstIndex = isMentionAtSOL ? preText.indexOf(mentionDelimiter) : preText.lastIndexOf(mentionDelimiter),
                leftHalf = text.substring(lstIndex >= 0 ? lstIndex : 0, caretPos);

            var fstIndex = postText.indexOf(' '),
                rightHalf = postText.substring(0, fstIndex >= 0 ? fstIndex + 1 : text.length);

            var keyword = leftHalf + rightHalf;

            return {
                startIndex: isMentionAtSOL ? lstIndex : lstIndex + 1,
                endIndex: (fstIndex >= 0 ? fstIndex + caretPos : caretPos),
                keyword,
                caretPos
            };

        }

        /**
         * @param {ContactData|string} contact
         *  API data of a contact committed from the dropdown list, or a pure
         *  email address, to be inserted as a mention.
         *
         * @param {Object<{keyword: string, startIndex?: number, endIndex?: number, caretPos?: number}>} [newKeyWordInfo]
         */
        function insertContactAsMention(contact, newKeyWordInfo) {

            var currentNode = trackSelection.selectedNode;

            contactListMenu.hide();

            var keyWordInfo = newKeyWordInfo || getKeywordInfo(trackSelection.caretAt, trackSelection.selectedNode.textContent);
            if (!keyWordInfo) { return; }

            // convert pure mail address to pseudo contact data
            if (is.string(contact)) {
                contact = { firstName: "", lastName: "", fullName: contact, mailAddr: contact, userId: 0, folderId: null, contactId: null };
            }

            const { firstName, lastName,  mailAddr, userId, folderId, contactId } = contact;

            // internal_userid === 0 means guest user
            const isGuest = userId === 0;

            // TODO: full name always in order first+last name?
            const fullName = (!isGuest && firstName && lastName) ? `${firstName} ${lastName}` : contact.fullName;

            if (currentNode.nodeName === '#text' && fullName && mailAddr) { // TextNode
                var parentNode = currentNode.parentNode,
                    coloredNode = document.createElement('SPAN');

                coloredNode.setAttribute('data-comment', 'true');
                coloredNode.setAttribute('data-new', 'true');
                coloredNode.setAttribute('data-fname', firstName);
                coloredNode.setAttribute('data-lname', lastName);
                coloredNode.setAttribute('data-id', userId);
                coloredNode.setAttribute('data-email', mailAddr);
                coloredNode.setAttribute('data-isguest', isGuest);
                coloredNode.setAttribute('data-contactid', contactId);
                coloredNode.setAttribute('data-contactfolder', folderId);
                coloredNode.setAttribute('data-initial', 'true');
                coloredNode.setAttribute('data-content', fullName);
                // coloredNode.setAttribute('contenteditable', 'false');
                coloredNode.setAttribute('title', mailAddr);
                coloredNode.textContent = `@${fullName}`; // escape the text content part

                var nodelist = splitTextNodeMixMention(currentNode.textContent, keyWordInfo.startIndex, keyWordInfo.endIndex, coloredNode);

                while (nodelist.length !== 0) {
                    parentNode.insertBefore(nodelist[0], currentNode);
                }

                parentNode.removeChild(currentNode);
                parentNode.normalize();

                if (windowSelectionTextContentHistory.cursorNode) {
                    self.placeCaretAtEnd(windowSelectionTextContentHistory.cursorNode);
                } else {
                    if (coloredNode.nextSibling && coloredNode.nextSibling.nodeName === '#text' && (coloredNode.nextSibling.textContent !== ' ' || _.browser.Firefox)) {
                        self.makeSelection(coloredNode.nextSibling, 1);
                    } else {
                        self.placeCaretAtEnd(parentNode);
                    }
                }
            }

            triggerMentionsEvent();
        }

        /**
         *  To convert a part of the text into mention, this function breaks the single TextNode into 3 parts.
         *  1. preText [#text]
         *  2. mention [SPAN]
         *  3. postText [#text]
         *
         *  Returns nodeList[]
         */
        function splitTextNodeMixMention(s, start, end, mentionNode) {
            var tempDiv = document.createElement('DIV'),
                preText = s.substring(0, start),
                postText = s.substring(end);

            if (preText) {
                tempDiv.appendChild(document.createTextNode(preText));
            }

            tempDiv.appendChild(mentionNode);
            tempDiv.appendChild(document.createTextNode(postText || ' '));

            return tempDiv.childNodes;
        }

        /**
         * Replace the content of selection with the content inside @param:replacement
         *
         * @param {DocumentFragment} replacement
         */
        function replaceSelectedContent(replacement) {
            var sel, range;
            if (window.getSelection) {
                sel = window.getSelection();
                if (sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    range.deleteContents();

                    var lastChild = replacement.lastChild;
                    range.insertNode(replacement);
                    self.placeCaretAtEnd(lastChild);
                }
            }
        }

        /**
         *
         * We dont want any html in commentEditor except the ones generated by the editor itself
         * This function gets rid of all html tags except mention tags
         *
         * @param {String} htm
         * @param {*} flagHandleText
         *
         * @returns {DocumentFragment}
         */
        function checkInsideAndFix(htm, flagHandleText) {
            var destinationFragment = new DocumentFragment();  // This document fragment element will contain the filtered and sanitixed version of the paste content
            var temp = document.createElement('DIV'); // As passed html can have multiple nodes without a parent. Here, we put them inside a container DIV

            if (typeof htm !== 'string') {
                destinationFragment.appendChild(document.createElement('SPAN'));
                return destinationFragment;
            }
            if (flagHandleText) {
                temp.innerText = htm;
            } else {
                $(temp).html(parseAndSanitizeHTML(htm));
            }
            serializeDOMStructure(temp, destinationFragment);  // Filter and sanitize

            function serializeDOMStructure(htm, destinationFragment) {
                if (htm.childNodes && htm.childNodes.length > 0) { // if element have childNodes
                    while (htm.childNodes.length > 0) {  // iterate through each childNodes
                        serializeDOMStructure(htm.firstChild, destinationFragment); // drill in until you reach a leafNode (deepest element of this child)
                    }
                } else { // Element is leafNode (has no child)
                    var parent = htm.parentNode,
                        span = document.createElement('SPAN');

                    if (htm.nodeName === 'BR') {
                        destinationFragment.appendChild(span);
                        span.appendChild(htm);
                    } else if (htm.nodeType === 3) { // leafNode is textNode
                        if (parent.nodeName === 'SPAN' && parent.getAttribute('data-comment') === 'true') { // if parent is span with exception of it being an OX mention
                            destinationFragment.appendChild(span);
                            var parentContentEditable = parent.getAttribute('contenteditable');
                            if (parentContentEditable) {
                                span.setAttribute('contenteditable', parentContentEditable);
                            }
                            span.setAttribute('style', parent.getAttribute('style'));

                            var attrs = parent.dataset;
                            Object.keys(attrs).forEach(function (attribute) {
                                span.setAttribute('data-' + attribute, attrs[attribute]);
                            });
                            span.appendChild(htm);
                        } else { // if parent is NOT span or label
                            var trimmed = htm.nodeValue.trim();
                            if (trimmed !== '') {
                                destinationFragment.appendChild(span);
                                span.appendChild(htm); // append the textNode to the "siblingTrap"
                            } else {
                                parent.removeChild(htm);
                            }
                        }
                    } else {
                        parent.removeChild(htm);
                    }
                }
            }
            return destinationFragment;
        }

        function triggerMentionsEvent() {
            const hasMentions = commentTextFrame.find('[data-comment="true"][data-new="true"]').length > 0;
            self.trigger("has:mentions", hasMentions);
        }

        /**
         *  This function detects and handles changes made in the text of a mention
         */
        function handleEditorMentionModification(selection) {
            if (selection && selection.focusNode && isMentionNode(selection.focusNode.parentNode)) {
                var mentionNode = selection.focusNode.parentNode,
                    dataContent = mentionNode.getAttribute('data-content'),
                    originalContent = dataContent.startsWith("@") ? dataContent : `@${dataContent}`,
                    updatedContent = mentionNode.innerText;

                if (originalContent !== updatedContent) {
                    var mentionParentNode = mentionNode.parentNode;

                    // IE do not support node.previousSibling for "#text" nodes hence need to do it manually
                    // Also node.normalize() seems inconsistent in IE, need to create node.normalize() of our own
                    mentionParentNode.normalize();

                    var mentionSiblings = mentionParentNode.childNodes;
                    var previousTextSibling = null;
                    var nextTextSibling = null;

                    mentionSiblings.forEach(function (nodeItem, index) {
                        if (nodeItem === mentionNode) {
                            previousTextSibling = mentionSiblings[(index - 1)] ? mentionSiblings[(index - 1)].nodeName === '#text' ? mentionSiblings[(index - 1)] : null : null;
                            nextTextSibling = mentionSiblings[(index + 1)] ? mentionSiblings[(index + 1)].nodeName === '#text' ? mentionSiblings[(index + 1)] : null : null;
                        }
                    });

                    var finalText = '';
                    var selectionIndex = {
                        start: 0,
                        end: 0
                    };

                    if (previousTextSibling) {
                        finalText += previousTextSibling.textContent;
                    }

                    selectionIndex.start = finalText.length;
                    finalText += updatedContent;
                    selectionIndex.end = finalText.length;

                    if (nextTextSibling) {
                        finalText += nextTextSibling.textContent;
                    }

                    var newTextNode = document.createTextNode(finalText);

                    if (previousTextSibling) {
                        mentionParentNode.insertBefore(newTextNode, previousTextSibling);

                        mentionParentNode.removeChild(previousTextSibling);
                        mentionParentNode.removeChild(mentionNode);
                        if (nextTextSibling) { mentionParentNode.removeChild(nextTextSibling); }
                    } else if (nextTextSibling) {
                        mentionParentNode.insertBefore(newTextNode, nextTextSibling);

                        mentionParentNode.removeChild(nextTextSibling);
                        mentionParentNode.removeChild(mentionNode);
                    } else {
                        mentionParentNode.replaceChild(newTextNode, mentionNode);
                    }

                    mentionParentNode.normalize();

                    self.makeSelection(newTextNode, selectionIndex.start, selectionIndex.end);
                }
            }
        }

        /**
         *  Returns if the "node" is mention node
         *
         *  @returns {boolean}
         */
        function isMentionNode(node) {
            return !!(node && node.nodeName === 'SPAN' && node.getAttribute('data-comment') === 'true');
        }

        // public functions ======================================================

        /**
         * This function checks if passed string is in valid email address format.
         *
         * @param {string} sample
         * @returns {boolean}
         * Expect "TRUE" if passed `sample` is a valid email address format else "FALSE"
         */
        this.isEmailAddress = function (sample) {
            /*  Pursuit of a valid email address  */
            return !!sample && (sample.length >= 6) && /^[A-Za-z0-9._-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/.test(sample);
        };

        /**
         *  This function returns existing mentions in the comment editor
         *
         *  @returns {Array<any>} mentions
         */
        this.getMentions = function () {
            var mentionElementArray = Array.prototype.slice.call(commentTextFrame.find('[data-comment="true"]'));
            var mentions = [];
            if (mentionElementArray.length) {
                mentionElementArray.forEach(function (el) {
                    var name = el.textContent,
                        id = el.getAttribute('data-id') !== 'null' ? parseInt(el.getAttribute('data-id'), 10) : null;

                    mentions.push({
                        display_name: name.replace(/^@/, ""),
                        email: el.getAttribute('data-email'),
                        user_id: el.getAttribute('title'),
                        internal_userid: id,
                        provider_id: OX_PROVIDER_ID,
                        length: name.length,
                        first_name: el.getAttribute('data-fname') !== 'null' ? el.getAttribute('data-fname') : null,
                        last_name: el.getAttribute('data-lname') !== 'null' ? el.getAttribute('data-lname') : null,
                        initial: el.getAttribute('data-initial') === 'true',
                        is_guest: el.getAttribute('data-isguest') === 'true',
                        contact_id: el.getAttribute('data-contactid') !== 'null' ? parseInt(el.getAttribute('data-contactid'), 10) : null,
                        contact_folder: el.getAttribute('data-contactfolder') !== 'null' ? parseInt(el.getAttribute('data-contactfolder'), 10) : null
                    });
                });
            }

            return mentions;
        };

        /**
         *  Places the caret at the end of the element passed as parameter to this function
         *
         *  @param {HTMLElement | undefined} el
         *  in case of "undefined" default element will be set to comment editor
         */
        this.placeCaretAtEnd = function (el) {
            _.defer(function () {
                el = el || commentTextFrame[0];

                if (el.focus) {
                    // Internet explorer does not support synchronous call focus() function
                    el.focus();
                }

                if (_.browser.Firefox && $(el).children('span').length > 0) { // DOCS-2672
                    var lastSpan = $(el).children('span').last();
                    var textNode = lastSpan.contents()[0];
                    if (textNode) { el = textNode; }
                }
                var range = document.createRange();
                range.selectNodeContents(el);
                range.collapse(false);
                var sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            });
        };

        /**
         *  Highlights a DOM node or simply put sets focus on the passed DOM node
         *
         *  @param {HTMLElement} el
         *  The DOM element on which the focus is required (required)
         *
         *  @param {number} offsetStartIndex
         *  Selection start index (required)
         *
         *  @param {number | undefined} offsetEndIndex
         *  Selection end index (optional). If undefined - the cursor will blink at position "offsetStartIndex"
         */
        this.makeSelection = function (el, offsetStartIndex, offsetEndIndex) {
            var range = document.createRange();
            var sel = window.getSelection();
            range.setStart(el, offsetStartIndex);
            if (offsetEndIndex) {
                range.setEnd(el, offsetEndIndex);
            }
            // range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
            // el.focus();
        };

        /**
         *  Returns Comment editor DOM node
         *
         *  @returns {JQuery}
         */
        this.getCommentTextFrame = function () {
            return commentTextFrame;
        };

        /**
         *  Sets plain text into the editable area of the comment editor
         *  This is used when comments are loaded with mentions, to highlight the mentions in the editor, if any.
         *
         *  @param {Object} recoverOptions
         */
        this.setContent = function (recoverOptions) {

            var editorContentText = is.string(recoverOptions?.text) ? recoverOptions.text : (!isNewComment && commentModel.getText()) ? commentModel.getText() : '';
            var newEditorContentText = null;
            var mentions = isNewComment ? recoverOptions?.mentions : commentModel.getMentions();

            if (mentions) {

                const useAuthorList = commentModel.docModel.docApp.isPresentationApp() || commentModel.docModel.docApp.isTextApp(); // only in OX Spreadsheet the "userId" is part of the mention after reload
                const authorList = (!isNewComment && useAuthorList) ? commentModel.docModel.getAuthorsList() : null;

                newEditorContentText = mentions.reduce(function (acc, mention, index, src) {

                    const mentionStart = mention.pos;
                    const mentionEnd = mentionStart + mention.length;
                    const mentionText = editorContentText.slice(mentionStart, mentionEnd);

                    // text before a mention
                    let textSlice = ''; // a slice of a comment text
                    if (index === 0) {
                        textSlice = editorContentText.slice(0, mentionStart);
                        acc += textSlice && escapeHTML(textSlice);
                    }

                    const mentionUser = authorList ? authorList.find(author => author.authorName === mentionText.replace(/^@/, "")) : mention;
                    const [email, id, fname, lname] = mentionUser?.userId?.split('::') || [];
                    // DOCS-4483, the attribute "data-id" must always be the "real" user id. But in the splitted string is the increased "operationId"
                    const localId = !_.isUndefined(id) ? resolveUserId(parseInt(id, 10)) : null;
                    acc += '<span data-comment="true"' +
                        ` data-fname="${escapeHTML(fname || "")}"` +
                        ` data-lname="${escapeHTML(lname || "")}"` +
                        ` data-id="${localId}"` +
                        ` data-email="${escapeHTML(email || "")}"` +
                        ` title="${escapeHTML(email || "")}"` +
                        ` data-content="${escapeHTML(mentionText)}"` +
                        `>${escapeHTML(mentionText)}</span>`;

                    // string part till the next mention/end of the text, if there are no more mentions
                    return acc + escapeHTML(editorContentText.slice(mentionEnd, src[index + 1]?.pos));
                }, '');
            } else {
                newEditorContentText = escapeHTML(editorContentText);
            }

            // bug 67560: split multi-line text into paragraph elements
            if (newEditorContentText) {
                // DOCS-4256: create DIV elements with embedded BR to prevent rendering errors with leading/trailing empty lines
                commentTextFrame[0].innerHTML = newEditorContentText.split('\n').map(markup => `<div>${markup || '<br>'}</div>`).join('');
            } else {
                commentTextFrame.empty(); // must be empty to show placeholder
            }
        };

        // initializations ----------------------------------------------------

        commentTextFrame.on('keydown', event => {

            if (_.browser.Safari && _.device('macos') && matchKeyCode(event, "TAB")) { return; } // special tab handling in Safari browser (DOCS-4493)

            event.stopPropagation();

            // apply selected list item, or move list selection
            if (contactListMenu.handleKeyDownEvent(event)) {
                shouldPreventDefault = true;
                return false;
            }

            // if there is no mention dropdown list present
            if (matchKeyCode(event, "ENTER", { ctrlOrMeta: true })) {
                self.trigger("action:save");
                return false;
            }

            // if there is no mention dropdown list present
            if (isEscapeKey(event)) {
                self.trigger("action:cancel");
                return false;
            }
        });

        /**
         * handle keyUp on comment editor, steps as follows:
         * STEP 1: Check "shouldPreventDefault", special case for "ArrowUp, ArrowDown and Enter". Depending on whether those keys were meant for suggestion list of text content of the editor
         * STEP 2: Get selection of current node and check if "@/+" attempt of mention
         * STEP 3: If @mention attempt detected -> Trigger 'autoCompleteAPI' -> Show the suggested user list
         */
        commentTextFrame.on('keyup', function (e) {

            if (shouldPreventDefault) {
                e.preventDefault();
                shouldPreventDefault = false; // default false
                return;
            }

            var isEnter = e.key === 'Enter';
            var selection = window.getSelection();

            handleEditorMentionModification(selection);

            if (!isEnter) {
                windowSelectionTextContentHistory = {
                    textContent: selection.focusNode ? selection.focusNode.nodeValue || selection.focusNode.textContent || '' : '',
                    caretAt: selection.focusOffset,
                    focusNode: selection.focusNode,
                    cursorNode: null
                };
            } else if (windowSelectionTextContentHistory?.focusNode && !windowSelectionTextContentHistory.focusNode.parentNode) {
                windowSelectionTextContentHistory = null; // DOCS-3166 -> avoiding that the suggestion list is opened; not using an invalid saved focus node, that is no longer in the DOM
            }

            var textContentFull = '';
            listKeyWordInfo = null;
            shouldPreventDefault = false; // default false

            // check if selection exists
            if (selection?.focusNode && (selection.focusOffset || (!selection.focusOffset && isEnter))) {
                var caretAt = selection.focusOffset;
                textContentFull = selection.focusNode.nodeValue || selection.focusNode.textContent || '';
                mentionDelimiter = textContentFull.startsWith('@') ? '@' : ' @';
                trackSelection = {
                    caretAt,
                    selectedNode: selection.focusNode
                };

                if (!selection.focusOffset && isEnter && windowSelectionTextContentHistory) {
                    caretAt = windowSelectionTextContentHistory.caretAt + 1;
                    // Forcing it to get detected by check -> email then space
                    textContentFull = windowSelectionTextContentHistory.textContent + ' ';
                    trackSelection = {
                        caretAt,
                        selectedNode: windowSelectionTextContentHistory.focusNode
                    };
                    windowSelectionTextContentHistory.cursorNode = selection.focusNode;
                }

                listKeyWordInfo = getKeywordInfo(caretAt, textContentFull); // set keyword from focused content
                var unfilteredKeyword = listKeyWordInfo?.keyword?.trim() || '';
                var isCursorInMentionNode = isMentionNode(selection.focusNode.parentNode); // if cursor travelling through a mention node -> No need to get suggestionList

                if (!isCursorInMentionNode && listKeyWordInfo?.keyword.includes(mentionDelimiter) && unfilteredKeyword) {
                    var name = unfilteredKeyword.slice(1);
                    contactListMenu.initialize(name).then(contacts => {

                        const isCharacterSpaceAtEnd = listKeyWordInfo.keyword.endsWith(" ");

                        if (contacts.length) {
                            contactListMenu.show();

                            // if client types all the letters of a user's email/name from list -> turn it into a mention automatically
                            // The suggestion must contain only one item/user to avoid conflicts
                            if (isCharacterSpaceAtEnd && (contacts.length === 1) && !textContainsContactName(textContentFull, contacts[0])) {
                                insertContactAsMention(contacts[0]);
                            }
                            return;
                        }

                        // Check if entered keyword is email address (isEmail) mention
                        if (isCharacterSpaceAtEnd && self.isEmailAddress(name)) {
                            insertContactAsMention(name, listKeyWordInfo);
                        }
                    }, _.noop);

                    return;
                }
            }

            contactListMenu.hide();

            triggerMentionsEvent();
        });

        contactListMenu.on("contact:commit", contact => {
            insertContactAsMention(contact, listKeyWordInfo);
        });

        commentTextFrame.on("click", () => {
            // keeping contact list menu open, when focus is set into comment editor (DOCS-3797), but closing
            // it explicitely, when user clicks into the comment editor.
            contactListMenu.hide();
        });

        commentTextFrame.on('paste', function (e) {
            e.preventDefault();
            var paste = e.originalEvent.clipboardData;
            var pastedContent = null;
            var flagHandleText = false;

            if (!paste) {
                // this is for IE 11
                pastedContent = window.clipboardData.getData('Text');
                flagHandleText = true;
            } else {

                pastedContent = paste.getData('text/html');

                if (pastedContent === '') {
                    pastedContent = paste.getData('text/plain');
                    flagHandleText = true;
                }

                if (pastedContent === '') {
                    return;
                }
            }

            var serializedPaste = checkInsideAndFix(pastedContent, flagHandleText);
            // Replace the selected area on commentBox with above sanitized content
            replaceSelectedContent(serializedPaste);
        });

        // DOCS-4591, not propagating composition events to the parent editor
        commentTextFrame.on('compositionstart compositionupdate compositionend', function (e) {
            e.stopPropagation();
        });

        commentTextFrame.on('focus', function () {
            if (commentModel.docApp.isTextApp()) {
                var pageNode = commentModel.docModel.getNode();
                pageNode.attr('contenteditable', false);
                pageNode.children('.pagecontent').attr('contenteditable', true);
            }
            commentModel.docApp.docController.enableUndoRedo(false);
        });

        commentTextFrame.on('blur', function () {
            if (commentModel.docApp.isTextApp()) {
                var pageNode = commentModel.docModel.getNode();
                pageNode.attr('contenteditable', true);
                pageNode.children('.pagecontent').removeAttr('contenteditable');
            }
            commentModel.docApp.docController.enableUndoRedo(true);
        });
    }
}
