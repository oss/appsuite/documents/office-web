/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import moment from '$/moment';

import Dropdown from '$/io.ox/backbone/mini-views/dropdown';

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import { IOS_SAFARI_DEVICE, TOUCH_DEVICE, escapeHTML, createIcon, createButton, enableElement, matchKeyCode } from '@/io.ox/office/tk/dom';
import { setToolTip } from '@/io.ox/office/tk/forms';
import { isGuest } from '@/io.ox/office/tk/utils/driveutils';
import { createContactPicture, setContactPicture } from '@/io.ox/office/tk/utils/contactutils';
import { BaseObject } from '@/io.ox/office/tk/objects';
import { getBooleanOption, getFunctionOption, getNumberOption } from '@/io.ox/office/tk/utils';

import { format, getUTCDateAsLocal } from '@/io.ox/office/editframework/utils/dateutils';
import { CONTACTS_AVAILABLE, UNITTEST } from '@/io.ox/office/editframework/utils/editconfig';
import CommentEditor from '@/io.ox/office/editframework/view/commenteditor';
import CommentPermissionDialog from '@/io.ox/office/editframework/view/dialog/commentpermissiondialog';
import { OX_PROVIDER_ID, calculateUserId, resolveUserId } from '@/io.ox/office/editframework/utils/operationutils';
import { MORE_LABEL, REPLY_COMMENT_OPTIONS } from '@/io.ox/office/editframework/view/editlabels';

// constants ==============================================================

/**
 * A special CSS marker class for elements in a comment frame that want to
 * let "click" events bubble up to the document root. Click events from
 * these elements and all their descendants will be propagated. All other
 * click events will be consumed exclusively (the comment frame will be
 * selected only).
 */
var PROPAGATE_CLICKS_CLASS = 'propagate-clicks';

// private functions ======================================================

function getAuthorName(commentModel) {
    var authorName = '';

    if (commentModel.getAuthor().trim().length) {
        authorName = commentModel.getAuthor();
    }

    return authorName;
}

function getFormatedDate(commentModel, useLocalNowAsUTC) {
    var date = useLocalNowAsUTC ? getUTCDateAsLocal(new Date(commentModel.getDate())) : new Date(Date.parse(commentModel.getDate()));
    return format(date, moment.localeData()._longDateFormat.LLL);
}

// class ThreadedCommentEditor ============================================

class ThreadedCommentEditor extends BaseObject {

    constructor(docView, commentModel, editorOptions, recoverOptions) {

        super();

        var commentEditor = null;

        var updateUnsavedTimer = null,
            unsavedComment = null;

        var closeHandler = getFunctionOption(editorOptions, 'closeHandler', _.noop),
            newComment = getBooleanOption(editorOptions, 'newComment', false),
            newThread = getBooleanOption(editorOptions, 'newThread', false);

        var isRecoverData = !!recoverOptions,
            isRecoverThread = (isRecoverData && newThread);

        // initialText = (isRecoverData ? getStringOption(recoverOptions, 'text', '') : ((!newComment && commentModel.getText()) || '')),
        var initialPosX = (isRecoverThread && getNumberOption(recoverOptions, 'posX', null)) || null,
            initialPosY = (isRecoverThread && getNumberOption(recoverOptions, 'posY', null)) || null;

        var initialPositionList = (initialPosX && initialPosY && [initialPosX, initialPosY]); // {Array|null}

        var editor = $('<div class="commentEditor">');

        function getEnteredText() {
            return parseEnteredText(commentTextFrame[0]); // passing the html element into the function
        }

        function isEmptyText() {
            var text = commentTextFrame.text();
            return $.trim(text) === '';
        }

        /**
         * This function parses the text in the comment editor and generates a resulting string without html
         * tags. It is called when the comment is saved and also much more often, when the comment content is
         * changed and the content must be stored, to be restored later (DOCS-2515). Because this generation
         * of the restore data within this function, while the comment is still editable, the DOM must not
         * be manipulated within this function.
         *
         * @param {HTMLElement} commentContentEditableHTMLElement
         *  The comment contentEditable DOM element, not the jQuerified global 'commentContentEditableElement'
         *
         * @returns {String}
         *  The string representation of the comment content.
         */
        function parseEnteredText(commentContentEditableHTMLElement) {

            if (!(commentContentEditableHTMLElement?.isContentEditable  || UNITTEST)) {
                throw new Error('Not a contentEditable element.');
            }

            var commentText = (function textContent(node) {
                var child = null;
                var type = null;
                var resultString = '';  // resultString holds the text of all children

                for (child = node.firstChild; child !== null; child = child.nextSibling) {
                    type = child.nodeType;

                    if (type === 3 || type === 4) { // Text and CDATASection nodes
                        resultString += child.nodeValue;
                    } else if (type === 1) { // Recurse for Element nodes
                        if (child !== commentContentEditableHTMLElement.firstChild) { // fix for docs-2695, prevent newline if text starts with a div
                            if ((child.tagName === 'DIV')  || child.tagName === 'P') {
                                resultString += '\n';
                            } else if (child.tagName === 'BR') {
                                var parent = child.parentNode;
                                var parentTagName = parent.tagName;

                                if (((parentTagName === 'DIV' || parentTagName === 'P') && parent.children.length === 1) || (parentTagName === 'SPAN' && parent.firstChild === child)) {
                                    continue;
                                } else {
                                    resultString += '\n';
                                }
                            }
                        }
                        resultString += textContent(child);
                    }
                }

                return resultString;
            }(commentContentEditableHTMLElement));

            return commentText;
        }

        function close(reason) {
            docView.commentsPane.setEditCommentModel(null); // TODO: This dependency from the page will not work in OX Text
            docView.trigger('commenteditor:closed', { reason, isNewThread: newThread });
            commentTextFrame.empty();
            if (newThread) {
                docView.commentsPane.removeNewThread(); // TODO: This dependency from the page will not work in OX Text
            } else {
                closeHandler();
            }
        }

        function saveHandler() {

            var commentInfo = getCommentInfo(commentTextFrame);
            var mentions = commentInfo.mentions;

            // find all new mentions to be notified (array of mentions must not contain an entry with the mail
            // address but without the `initial` flag)
            var newMentions = _.filter((mentions || []).reduce(function (map, mention) {
                var email = mention.email;
                map[email] = (mention.initial && (map[email] !== false)) ? mention : false;
                return map;
            }, {}), _.isObject);

            if (newMentions.length > 0 && !isGuest()) {
                var permissionDialog = new CommentPermissionDialog(docView, newMentions);
                permissionDialog.handlePermission(commitCommentSave);
            } else {
                commitCommentSave();
            }

            function commitCommentSave() {

                var commentCollection = docView.getCommentCollection();
                var commentSaveData = { text: commentInfo.text, mentions };

                if (newComment || newThread) {
                    if (!isEmptyText()) {
                        updateAuthorListOnSave();
                        var positionList = (initialPositionList/* {Array|null} */ || (newThread && commentModel.getPositionList && commentModel.getPositionList()/* {Array|any} */));
                        positionList = ((_.isArray(positionList) && positionList) || null); // {Array|null}
                        commentCollection.insertThread(commentModel.getAnchor(), commentSaveData, positionList);
                        commentCollection.deleteUnsavedComment();
                        close('save');
                    }
                } else {
                    if (commentModel.getText() !== commentSaveData.text) {
                        updateAuthorListOnSave();
                        commentCollection.deleteUnsavedComment();
                        commentCollection.changeThread(commentModel, commentSaveData);
                    }

                    close('save');
                }

                function updateAuthorListOnSave() {
                    var authorList = null;

                    if (mentions) {
                        authorList = commentModel.docModel.getAuthorsList() || [];

                        if (authorList && authorList.length) {
                            mentions.forEach(function (mention) {
                                var mentionUser = _.find(authorList, function (author) {
                                    return author.authorName === mention.displayName;
                                });

                                if (!mentionUser) {
                                    addToAuthorList(mention);
                                }
                            });

                        } else {
                            mentions.forEach(function (mention) {
                                addToAuthorList(mention);
                            });
                            commentModel.docModel.setAuthorsList(authorList);
                        }
                    }

                    function addToAuthorList(mention) {
                        authorList.push({
                            providerId: OX_PROVIDER_ID,
                            authorName: mention.displayName,
                            authorId: authorList.length,
                            userId: mention.userId
                        });
                    }
                }
            }
        }

        function cancelHandler() {
            docView.getCommentCollection().deleteUnsavedComment();
            close('cancel');
        }

        function getCommentInfo(editorElement) {
            var result = {
                text: getEnteredText(commentTextFrame)
            };
            var mentionElementArray = Array.prototype.slice.call(editorElement.find('[data-comment="true"]'));

            if (mentionElementArray.length) {
                result.mentions = [];

                mentionElementArray.reduce(function (position, el) {
                    var name = el.textContent;
                    var pos = result.text.indexOf(name, position);
                    var id = el.getAttribute('data-id') !== 'null' ? parseInt(el.getAttribute('data-id'), 10) : null;

                    // DOCS-4483, the attribute "data-id" must always be the "real" user id. But in the splitted string is the increased "operationId"
                    var operationId = id ? calculateUserId(id) : null;

                    var firstName = el.getAttribute('data-fname') !== 'null' ? el.getAttribute('data-fname') : null;
                    var lastName = el.getAttribute('data-lname') !== 'null' ? el.getAttribute('data-lname') : null;
                    var mentionOptions = {
                        displayName: name.charAt(0) === '@' ? name.substring(1) : name,
                        email: el.getAttribute('data-email'),
                        userId: el.getAttribute('data-email') + '::' + operationId + '::' + firstName + '::' + lastName,
                        // -> the filter needs the transformed operation ID, but it must be reverted for the 'sendCommentNotification' request
                        // userId: el.getAttribute('title'),
                        id, // this untransformed ID is used for the following local permission handling
                        providerId: OX_PROVIDER_ID,
                        pos,
                        length: name.length,
                        initial: el.getAttribute('data-initial') === 'true',
                        is_guest: el.getAttribute('data-isguest') === 'true'
                    };
                    var isContactId = el.getAttribute('data-contactid') !== 'null' ? parseInt(el.getAttribute('data-contactid'), 10) : null;
                    var isContactFolder = el.getAttribute('data-contactfolder') !== 'null' ? parseInt(el.getAttribute('data-contactfolder'), 10) : null;

                    if (isContactId) {
                        mentionOptions.contact_id = isContactId;
                    }

                    if (isContactFolder) {
                        mentionOptions.contact_folder = isContactFolder;
                    }

                    result.mentions.push(mentionOptions);

                    return pos + name.length;
                }, 0);
            }

            return result;
        }

        function createUnsavedComment() {
            unsavedComment = {};
            var commentInfo = getCommentInfo(commentTextFrame);

            if (newThread) {
                var positionList = (commentModel.getPositionList && commentModel.getPositionList());
                positionList = ((_.isArray(positionList) && positionList) || null);

                if (positionList) {
                    unsavedComment.posX = positionList[0];
                    unsavedComment.posY = positionList[1];
                }
                unsavedComment.isNewThread = true;
            } else if (newComment) {
                unsavedComment.isNewReply = true;
            } else {
                unsavedComment.isEditComment = true;
            }
            unsavedComment.model = commentModel;
            unsavedComment.text = commentInfo.text;
            unsavedComment.mentions = commentInfo.mentions;

            return unsavedComment;
        }

        function updateUnsavedComment() {

            if (updateUnsavedTimer) {
                updateUnsavedTimer.abort();
                updateUnsavedTimer = null;
            }

            var commentCollection = docView.getCommentCollection();
            var commentInfo = null; // comment information object

            if (unsavedComment === null) {

                commentCollection.setUnsavedComment(createUnsavedComment());

            } else {
                commentInfo = getCommentInfo(commentTextFrame);

                if (unsavedComment.isEditComment && commentModel.getText() === commentInfo.text) {
                    commentCollection.deleteUnsavedComment();
                } else if (unsavedComment.isNewReply || unsavedComment.isNewThread || unsavedComment.isEditComment) {
                    unsavedComment.text = commentInfo.text;
                    unsavedComment.mentions = commentInfo.mentions;
                }
            }
        }

        function setHideButtonState() {
            // hide the send button if the comment text is empty
            enableElement(sendButton, !isEmptyText());
        }

        function onChange() {
            setHideButtonState();
            updateUnsavedComment();
            $unsavedMsg.hide();
        }

        // special tab handling in Safari browser (DOCS-4493)
        function textFrameKeyDownHandler(event) {
            if (matchKeyCode(event, "TAB")) {
                const focusButton = sendButton.disabled ? cancelButton : sendButton;
                focusButton.focus();
                return false;
            }
        }

        // special tab handling in Safari browser (DOCS-4493)
        function sendButtonKeyDownHandler(event) {
            if (matchKeyCode(event, "TAB")) {
                cancelButton.focus();
                return false;
            }
        }

        // initialization -----------------------------------------------------

        // commentModel.isReply() always seems to return `false`
        // var isReply = (newComment && !newThread) ? true : (!newComment && !newThread) ? undefined : false;
        var isReply = !!(newComment && !newThread);

        commentEditor = new CommentEditor(commentModel, isReply, newComment || newThread);

        var commentTextFrame = commentEditor.getCommentTextFrame();

        const mailNotifyText = isGuest() ? gt('You added people to this comment. Attention: Due to missing permissions no email will be sent!') : gt('You added people to this comment who will receive an email notification.');

        const $mailNotifyMsg = $('<div class="mail-notify-message">').text(mailNotifyText).hide();

        const $unsavedMsg = $('<div class="unsaved-comment-message">').text(gt('Please post your comment.')).hide();

        const sendButton = createButton({
            classes: "ignorepageevent",
            type: "primary",
            action: "send",
            icon: "svg:paperplane",
            //#. send changes in comment text to server
            tooltip: gt.pgettext('comment-actions', 'Send'),
            disabled: true,
            click: saveHandler
        });

        const cancelButton = createButton({
            classes: "ignorepageevent",
            type: "default",
            action: "cancel",
            icon: "bi:x-lg",
            //#. discard local changes in comment text
            tooltip: gt.pgettext('comment-actions', 'Discard'),
            click: cancelHandler
        });

        const mainPanel = $('<div class="editormain noI18n">').append(commentTextFrame);
        const actionPanel = $('<div class="editoraction">').append(
            $('<div class="editor-messages">').append($mailNotifyMsg, $unsavedMsg),
            sendButton, cancelButton
        );
        editor.append(mainPanel, actionPanel);

        // save comment (e.g. on Ctrl+Enter)
        this.listenTo(commentEditor, "action:save", saveHandler);
        this.listenTo(commentEditor, "action:cancel", cancelHandler);

        // toggle visibility of email notification message
        this.listenTo(commentEditor, "has:mentions", state => {
            $mailNotifyMsg.toggle(state);
        });

        // special tab handling in Safari browser (DOCS-4493)
        if (_.browser.Safari && _.device('macos')) {
            commentTextFrame.on('keydown', textFrameKeyDownHandler);
            $(sendButton).on("keydown", sendButtonKeyDownHandler);
        }

        if (IOS_SAFARI_DEVICE) {
            commentTextFrame.on('input propertychange keyup paste', onChange);
        } else {
            commentTextFrame.on('input propertychange paste', onChange);
        }

        commentEditor.setContent(isRecoverData ? { text: recoverOptions.text, mentions: recoverOptions.mentions } : undefined);

        setHideButtonState();

        docView.trigger('commenteditor:created', commentEditor, commentModel, commentTextFrame);

        // @param selectAll ofType boolean
        this.focus = function (selectAll) {
            _.defer(function () {
                if (selectAll) {
                    var range = document.createRange();
                    range.selectNodeContents(commentTextFrame[0]);

                    var sel = window.getSelection();
                    sel.removeAllRanges();
                    sel.addRange(range);
                }

                commentTextFrame.focus();
            });
        };

        this.edit = function () {
            var commentCollection = docView.getCommentCollection();
            if (commentModel) {
                commentCollection.setUnsavedComment(createUnsavedComment());
                docView.commentsPane.setEditCommentModel(commentModel); // TODO: This dependency from the page will not work in OX Text

                // self.placeCaretAtEnd();
                commentEditor.placeCaretAtEnd();
            }
        };

        commentTextFrame.blur(function () {
            if (unsavedComment) {
                updateUnsavedComment();
            }
            if (newComment && isEmptyText() && !newThread) {
                docView.getCommentCollection().deleteUnsavedComment();
                close();
            }
        });

        this.listenTo(docView, 'comment:delete:all', function () { close('deleteAll'); }); // the user selected 'Delete all comments'

        if (TOUCH_DEVICE) {
            this.listenTo(docView, 'refresh:layout:after', function () {
                this.executeDelayed(() => commentEditor.placeCaretAtEnd(), 100); // setting the cursor into the comment editor again (DOCS-3963)
            });
        }

        this.appendTo = function (element) {
            element.append(editor);
        };

        this.detach = function () {
            editor.detach();
        };

        this.getNode = function () {
            return editor;
        };

        this.highlightUnsavedComment = function () {
            $unsavedMsg.show()[0].scrollIntoView();
            this.focus(true);
        };

        this.registerDestructor(function () {
            commentEditor?.destroy();
            editor.remove();
            commentTextFrame.off();
        });
    }
}

// class ThreadedCommentElement ===============================================

class ThreadedCommentElement extends BaseObject {

    constructor(docView, commentModel, replyHandler, newThread, threadRecoverOptions) {

        super();

        var self = this;

        var app = docView.docApp;
        var docModel = commentModel.docModel;
        var editor;

        var editorOptions = {
            closeHandler: editorCloseHandler,
            newThread
        };

        function editorCloseHandler() {
            text.show();
            editor.destroy();
            editor = null;
            comment.removeClass('edit-mode');
            docModel.trigger('comments:update:vertical');
        }

        function edit(recoverOptions) {
            text.hide();
            comment.addClass('edit-mode');
            editor = new ThreadedCommentEditor(docView, commentModel, editorOptions, (recoverOptions || threadRecoverOptions));
            editor.appendTo(main);
            editor.edit();
        }

        function setText(recoverOptions) {
            var contentText = commentModel.getText(); // comment's text from the operation
            var mentions = commentModel.getMentions(); // comment's mentions from the operation
            var authorList = null; // the list of all known authors
            var resultMentionsText = null;

            if (mentions) {

                if (!mentions[0].userId) {
                    authorList = docModel.getAuthorsList();
                }

                resultMentionsText = mentions.reduce(function (acc, mention, index, src) {
                    var startIndex = null;
                    var mentionStart = mention.pos;
                    var mentionEnd = mentionStart + mention.length;
                    var mentionText = contentText.slice(mentionStart, mentionEnd);
                    // getting userId for the mention, either from mention object itself (on create) or from the mapping of the author list with the displayName (on loading)
                    var mentionUser = null;
                    var userId = null;
                    var textSlice = ''; // a slice of a comment text
                    var setInternalUserId = false;

                    if (authorList) {
                        mentionUser = _.find(authorList, function (author) {
                            var mentionTextWithoutAtSign = mentionText.charAt(0) === '@' ? mentionText.substring(1) : mentionText;

                            return author.authorName === mentionTextWithoutAtSign;
                        });

                        userId = mentionUser && mentionUser.userId;
                    } else {
                        userId = mention && mention.userId;
                    }

                    userId = userId ? userId : '';
                    var userInfoArr = userId.split('::');

                    // text before a mention
                    if (index === 0) {
                        startIndex = 0;
                        textSlice = contentText.slice(startIndex, mentionStart);
                        acc += textSlice && escapeHTML(textSlice);
                    }

                    if (userInfoArr[1] && is.number(parseInt(userInfoArr[1], 10))) { setInternalUserId = true; }

                    // mention's text
                    acc += `<span data-comment="true" contenteditable="false" title="${userInfoArr[0] && escapeHTML(userInfoArr[0])}"`;
                    if (setInternalUserId) { acc += `data-detail-popup="halo" data-userid="${userInfoArr[1]}" class="needs-action person propagate-clicks halo-available"`; }
                    acc += `>${mentionText && escapeHTML(mentionText)}</span>`;

                    // string part till the next mention/end of the text, if there are no more mentions
                    if (src[index + 1]) {
                        textSlice = contentText.slice(mentionEnd, src[index + 1].pos);
                    } else {
                        textSlice = contentText.slice(mentionEnd);
                    }

                    acc += textSlice && escapeHTML(textSlice);

                    return acc;
                }, '');
            } else {
                resultMentionsText = escapeHTML(contentText);
            }

            // bug 67560: split multi-line text into paragraph elements
            // DOCS-4256: create DIV elements with embedded BR to prevent rendering errors with leading/trailing empty lines
            text[0].innerHTML = (resultMentionsText || contentText).split('\n').map(markup => `<div>${markup || '<br>'}</div>`).join('');

            // DOCS-3860, activating the halo for the mentions
            _.each(text.find('span[data-userid]'), function (oneMention) {
                const userId = $(oneMention).attr('data-userid');
                $(oneMention).data('internal_userid', resolveUserId(parseInt(userId, 10)));
                $(oneMention).removeAttr('data-userid');
            });

            if (recoverOptions && !threadRecoverOptions && !editor) {
                /*
                    *  Dealing with passed `recoverOptions` and not having present `threadRecoverOptions`
                    *  within in the outer `ThreadedCommentElement` scope also implies not yet having
                    *  access to this scope's `editor`.
                    *
                    *  This exclusively happens if `setText` is the end point of a forwarding chain of
                    *  method calls that always starts with
                    *
                    *  => `updateCommentsPane` -->
                    *     --> if `recoverOptions` is available forward from within `updateCommentsPane` to `changeComment({}, commentModel, recoverOptions)` =>
                    *
                    *  => `changeComment` -->
                    *     --> if there is access to a responding instance of `ThreadedCommentFrame` forward from within `changeComment` to `threadedCommentFrame.changeComment(commentModel, changeRecoverOptions)` =>
                    *
                    *  => `threadedCommentFrame.changeComment` just forwards to =>
                    *  => `threadedCommentElement.change(changeRecoverOptions)` -->
                    *     --> this public method finally calls this locally{ThreadedCommentElement} scoped endpoint ... `setText(recoverOptions)
                    *
                    *  If something like this, an unsaved text change of an already existing comment, occurs one not only has to
                    *  display the still unsaved comment via ... `text.html(getHtmlFormattedText(commentModel.getText()));` ...
                    *  ... but one also needs to switch this view state into it's edit mode with providing the unsaved changes to it.
                    */
                edit(recoverOptions);
            }

        }

        function setPosition() {
            if (!commentModel.isReply() && commentModel.getAnchorString && commentModel.getAnchorString().length) {
                position.text(_.noI18n(commentModel.getAnchorString()));
            }
        }

        /**
         * In OX Text is happens, that comments were generated with an older version of OX Text or Word. These comments can contain
         * images, tables, ... . If a comment contains such content, the filter sends 'restorable' set to false for such a comment. If
         * the user modifies such a comment, this can lead to data loss in such a 'complex' comment. Therefore the user needs to be
         * warned about this.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when this is not a text app or not a complex comment. If this is a complex comment in OX Text
         *  the promise fulfils, if the user accepts the risc. If not, the promise will be rejected and the editor will not be opened.
         */
        function processComplexComment() {

            if (!app.isTextApp()) { return $.when(); }

            if (commentModel.isRestorable()) { return $.when(); }

            // the deferred object to be fulfilled (user accepts the risc), or rejected (user does not accept the risc)
            var deferred = $.Deferred();
            // create an abortable promise
            var promise = self.createAbortablePromise(deferred);

            const scope = Symbol("event scope");

            // the reply button inside the comment button node
            var continueButton = $('<a class="continuebutton" tabindex="0">' + gt('Proceed') + '</a>');
            var cancelButton = $('<a class="cancelbutton" tabindex="0">' + gt('Cancel') + '</a>');
            var infoBox = $('<div class="infobox">');

            var contentRootNode = docView.getContentRootNode();

            // appending the info box into the comment
            function showInfoBox() {

                var infoBoxLeft = $('<div class="infoboxleft">').append(createIcon('bi:info-circle'));
                var infoBoxRight = $('<div class="infoboxright">');
                var infoBoxText = $('<div class="infoboxtext">').append($('<span>').text(gt('This comment contains content this app cannot display. Editing may result in changing or losing that content. Click cancel if you do not want to risk it.')));
                var infoBoxButtons = $('<div class="infoboxbuttons">');

                infoBoxButtons.append(continueButton);
                infoBoxButtons.append(cancelButton);

                infoBoxRight.append(infoBoxText);
                infoBoxRight.append(infoBoxButtons);

                infoBox.append(infoBoxLeft);
                infoBox.append(infoBoxRight);

                comment.append(infoBox);

                // informing listeners about infoBox (so that for example comment lines can be repainted)
                docView.trigger('comment:infobox:changed', commentModel, { created: true });
            }

            // helper function to restore the previous comment state and hiding the info box again
            function hideInfoBoxAndRemoveHandlers(event) {
                self.stopListeningToScope(scope);
                infoBox.remove(); // removing all nodes again
                comment.children().removeClass('hideComment'); // making comment node visible again
                event?.preventDefault();
                event?.stopPropagation();
                // informing listeners about infoBox (so that for example comment lines can be repainted)
                docView.trigger('comment:infobox:changed', commentModel, { created: false });
            }

            // handler at infobox to avoid closing when user clicks into infobox
            function doNothingHandler(event) {
                event.preventDefault();
                event.stopPropagation();
            }

            // handling when the user pressed the continue button
            function continueHandler(event) {
                hideInfoBoxAndRemoveHandlers(event);
                deferred.resolve(); // resolving the promise
            }

            // handling when the user pressed the cancel button
            function cancelHandler(event) {
                hideInfoBoxAndRemoveHandlers(event);
                deferred.reject(); // resolving the promise
            }

            // handling when the view triggers, that the infobox must be closed after a selection change
            function closeInfoBoxAfterSelectionChange(id) {
                if (commentModel.getId() === id) { cancelHandler(); }
            }

            // helper function to register all required handlers
            function registerHandlers() {
                self.listenTo(continueButton, 'click', continueHandler, { scope });
                self.listenTo(cancelButton, 'click', cancelHandler, { scope });
                self.listenTo(contentRootNode, 'click', cancelHandler, { scope });
                self.listenTo(infoBox, 'click', doNothingHandler, { scope });
                self.listenTo(docView, 'comment:selection:lost', closeInfoBoxAfterSelectionChange, { scope });
            }

            // registering the handlers
            registerHandlers();

            // hiding the existing comment
            comment.children().addClass('hideComment');

            // showing the new info content
            showInfoBox();

            return promise;
        }

        /**
         * Adding the actions "Edit", "Delete" and "Reply" to the comment. These are visible, if
         * the user has edit privileges.
         */
        function addActionNodeToComment() {

            // container for the action buttons on the right side
            var actionNode = $('<div class="action">');
            comment.append(actionNode);

            var menuNode = $(createIcon('bi:three-dots-vertical'));
            setToolTip(menuNode, MORE_LABEL);
            var dropdown = new Dropdown({ label: menuNode });

            dropdown.link('comment-edit', gt('Edit'), function () {
                if (editor) {
                    editor.focus();
                } else {
                    docView.processUnsavedComment().then(processComplexComment).then(function () {
                        edit();
                        docModel.trigger('comments:update:vertical');
                    });
                }
            });

            dropdown.link('comment-delete', commentModel.isReply() ? gt('Delete') : gt('Delete thread'), function () {
                // Bug 68247: The "delete/reply" controller task deletes the comment and this frame instance. This
                // must be done in a timeout, otherwise synchronous destruction kills JQuery's event handler code.
                self.setTimeout(function () {
                    docView.executeControllerItem('threadedcomment/delete/reply', commentModel);
                }, 0);
            });

            // render the dropdown HTML
            actionNode.append(dropdown.render().$el);
            dropdown.$el.addClass(PROPAGATE_CLICKS_CLASS);

            // dropdown event handling
            dropdown.$el.on('show.bs.dropdown', function () { comment.addClass('actions-active'); });
            dropdown.$el.on('hide.bs.dropdown', function () { comment.removeClass('actions-active'); });

            actionNode.append(createButton({
                type: "bare",
                action: "reply",
                icon: REPLY_COMMENT_OPTIONS.icon,
                tooltip: REPLY_COMMENT_OPTIONS.tooltip,
                click() {
                    jpromise.floating(docView.processUnsavedComment().then(function () {
                        replyHandler();
                        docModel.trigger('comments:update:vertical');
                    }));
                }
            }));

        }

        var authorColorIndex = app.getAuthorColorIndex(commentModel.getAuthor());
        var authorClassName = is.number(authorColorIndex) ? ('comment-author-' + authorColorIndex) : '';

        var comment = $('<div class="comment">');

        if (newThread) {
            comment.addClass('new');
        }

        // Main container for image, author, address and content
        var main = $('<div class="main">');
        comment.append(main);

        var commentinfo = $('<div class="commentinfo">');
        main.append(commentinfo);

        // author contact picture
        var pictureNode = createContactPicture();
        commentinfo.append(pictureNode);

        // right comment container
        var authorDate = $('<div class="authordate">');
        commentinfo.append(authorDate);

        // name of the author of the comment
        var authorName = getAuthorName(commentModel);
        var authorNameElement = $(CONTACTS_AVAILABLE ? '<a>' : '<div>');
        authorNameElement.addClass('author ignorepageevent').addClass(authorClassName).text(_.noI18n(authorName));
        authorDate.append(authorNameElement);

        var internalUserId = commentModel.getInternalUserId();
        this.onFulfilled(docView.docApp.getUserInfo(internalUserId), info => {
            if (info && (authorName === info.operationName || authorName === escapeHTML(info.operationName))) { // escapeHTML because of OX Presentation (DOCS-2869)
                if (!isGuest()) {
                    authorNameElement.attr({
                        href: '#',
                        'data-detail-popup': 'halo'
                    });
                    authorNameElement.addClass('needs-action person ' + PROPAGATE_CLICKS_CLASS);
                    authorNameElement.data({ internal_userid: internalUserId });
                    // use operation name whenever possible, but not for guests
                    authorNameElement.text(_.noI18n((info.guest && info.displayName) || info.operationName));
                }

                // calling Contacts API asynchronously increases the chance that the picture is shown correctly, especially for remote clients
                this.setTimeout(() => setContactPicture(pictureNode, { userId: internalUserId }), 0);
            }
        });

        // the date of the comment
        if (commentModel.getDate()) {
            var useLocalNowAsUTC = app.isTextApp() || app.isPresentationApp();
            var dateText = getFormatedDate(commentModel, useLocalNowAsUTC);
            authorDate.append($('<div class="date">').text(_.noI18n(dateText)));
        }

        var position = $('<div class="position">');
        position.addClass(authorClassName);

        // the position of the comment e.g. the cell name of the comment
        if (!newThread && !commentModel.isReply() && commentModel.getAnchorString() && commentModel.getAnchorString().length) {
            setPosition();
            commentinfo.append(position);
        }

        // the comment text
        var text = $('<div class="text noI18n">');
        setText();
        main.append(text);

        // the actions "sidepane" area (dropdown menu with additional actions, and "Reply" button)
        if (!newThread) { addActionNodeToComment(); }

        comment.data('id', commentModel.getId());

        this.change = function (changeRecoverOptions) {
            setText(changeRecoverOptions);
        };

        this.move = function () {
            setPosition();
        };

        this.edit = function () {
            edit();
        };

        this.getNode = function () {
            return comment;
        };

        this.getModel = function () {
            return commentModel;
        };

        this.detach = function () {
            comment.detach();
        };

        this.highlightUnsavedComment = function () {
            editor?.highlightUnsavedComment();
        };

        this.registerDestructor(function () {
            editor?.destroy();
            comment.remove();
        });
    }
}

// class CommentFrame =========================================================

export default class CommentFrame extends BaseObject {

    constructor(docView, threadComments, newThread, threadRecoverOptions) {

        super();

        var docModel = docView.docModel;

        var editorOptions = {
            closeHandler: editorCloseHandler,
            newThread,
            newComment: true
        };

        // all comments of the thread mapped by ID
        var commentEntryMap = this.member(new Map/*<string, ThreadedCommentElement>*/());
        var threadComment = threadComments[0];

        var replyEditor;

        function editorCloseHandler() {
            replyEditor.destroy();
            replyEditor = null;
            docModel.trigger('comments:update:vertical');
        }

        function replyToComment(replyRecoverOptions) {
            replyEditor = new ThreadedCommentEditor(docView, threadComment, editorOptions, replyRecoverOptions);
            replyEditor.appendTo(rootNode);
            replyEditor.edit();
        }

        function createHideButton(commentModel) {
            var button = $('<a class="button hide-button">');
            button.on('click', function () {
                rootNode.addClass('show-all');
                docView.getCommentCollection().selectComment(commentModel);
            });
            return button;
        }

        /**
         * Counting the visible, not filtered comment elements in a thread. To show the correct number
         * of comments to be shown on the hide button, it is also important to know, whether the
         * parent comment is also hidden.
         */
        function countVisibleCommentElements() {
            var counter = 0;
            var parentIsHidden = false;

            commentEntryMap.forEach(function (comment) {
                var model = comment.getModel();
                if (model.isHidden && model.isHidden()) {
                    if (!model.isReply()) { parentIsHidden = true; }
                } else {
                    counter++;
                }
            });
            return { counter, parentIsHidden };
        }

        function setHideButtonText() {
            var countInfo = countVisibleCommentElements();
            var count = countInfo.counter - (countInfo.parentIsHidden ? 1 : 2); // only counting the not filtered comments

            if (count > 0) {
                //#. button label to show more replies of a comment thread
                //#. %1$d is the number of replies that can be shown
                hideButton.text(gt.ngettext('View %1$d more reply', 'View %1$d more replies', count, count));
                threadCommentElement.getNode().after(hideButton);
            } else {
                hideButton.detach();
            }
        }

        function createCommentElement(commentModel) {
            var commentElement = new ThreadedCommentElement(docView, commentModel, replyToComment, newThread, threadRecoverOptions);
            commentEntryMap.set(commentModel.getId(), commentElement);
            return commentElement;
        }

        function appendComments() {
            commentEntryMap.forEach(function (comment) {
                if (comment.getModel().isReply()) {
                    comment.detach();
                }
            });

            hideButton.detach();

            var commentCount = 1;
            var lastComment = null;

            var documentCollection = docView.getCommentCollection();
            var commentModels = documentCollection.getByAddress(threadComment.getAnchor());

            // If the complete thread was deleted the result can be empty
            if (commentModels) {
                commentModels.forEach(function (commentModel) {
                    if (commentModel.isReply()) {
                        commentCount++;

                        var commentElement = commentEntryMap.get(commentModel.getId());

                        if (commentElement) {
                            var comment = commentElement.getNode();

                            comment.removeClass('all-show');
                            if (lastComment) {
                                lastComment.after(comment);
                            } else {
                                threadCommentElement.getNode().after(comment);
                            }

                            if (commentCount >= 2) {
                                if (lastComment !== null) {
                                    lastComment.addClass('all-show');
                                }
                            }
                            lastComment = comment;
                        }
                    }
                });
            }

            setHideButtonText();
        }

        var threadColorIndex = docView.docApp.getAuthorColorIndex(threadComment.getAuthor());
        var threadColorClassName = '';
        if (_.isNumber(threadColorIndex)) {
            threadColorClassName = 'thread-author-' + threadColorIndex;
        }

        var rootNode = $('<div class="thread">');
        rootNode.addClass(threadColorClassName);
        rootNode.attr('data-comment-id', threadComment.getId());
        rootNode.toggleClass('selected', !!newThread);

        var threadCommentElement = createCommentElement(threadComment);
        rootNode.append(threadCommentElement.getNode());

        var hideButton = createHideButton(threadComment);
        var isTouchDevice = _.device('smartphone || tablet');

        if (!isTouchDevice) {
            // select the clicked thread using the corresponding threadModel
            rootNode.on('click', function (event) {
                docView.getCommentCollection().selectComment(threadComment);
                // let click event bubble up if it needs to be handled (dropdown menu, contact halo, ...)
                return $(event.target).closest('.' + PROPAGATE_CLICKS_CLASS, rootNode[0]).length ? undefined : false;
            });
            // handle comment thread's mouseenter event with the corresponding commentModel
            rootNode.on('mouseenter', function (evt) {
                docView.commentsPane.trigger('thread:mouseenter', { originalEvent: evt, commentModel: threadComment }); // TODO: This dependency from the page will not work in OX Text
            });

            // handle comment thread's mouseleave event with the corresponding commentModel
            rootNode.on('mouseleave', function (evt) {
                docView.commentsPane.trigger('thread:mouseleave', { originalEvent: evt, commentModel: threadComment }); // TODO: This dependency from the page will not work in OX Text
            });
        }

        threadComments.forEach(function (commentModel) {
            if (commentModel.isReply()) {
                createCommentElement(commentModel);
            }
        });

        appendComments();

        if (newThread) {
            rootNode[0].scrollIntoView();
        }

        this.editComment = function (commentModel) {
            commentEntryMap.get(commentModel.getId()).edit();
        };

        this.addComment = function (commentModel) {
            createCommentElement(commentModel);
            appendComments();
        };

        this.changeComment = function (commentModel, changeRecoverOptions) {
            var commentElement = commentEntryMap.get(commentModel.getId());
            if (commentElement) { commentElement.change(changeRecoverOptions); }
        };

        this.moveComment = function (commentModel) {
            commentEntryMap.get(commentModel.getId()).move();
        };

        this.deleteComment = function (commentModel) {
            var commentElement = commentEntryMap.get(commentModel.getId());
            if (commentElement) { commentElement.destroy(); }
            commentEntryMap.delete(commentModel.getId());
            appendComments();
            //Delete hide Button
        };

        this.select = function (select) {
            rootNode.toggleClass('show-all selected', select);
        };

        this.reply = function (replyRecoverOptions) {
            replyToComment(replyRecoverOptions);
        };

        this.getNode = function () {
            return rootNode;
        };

        this.getThreadComment = function () {
            return threadComment;
        };

        this.equals = function (commentFrame) {
            // DOCS-3839: "threadComment" may have been destroyed (TODO: reason is still unclear)
            return !!commentFrame && !!threadComment && !threadComment.destroyed && threadComment.equals(commentFrame.getThreadComment());
        };

        this.highlightUnsavedComment = function (commentModel) {
            if (replyEditor) {
                replyEditor.highlightUnsavedComment();
            } else if (newThread) {
                threadCommentElement.highlightUnsavedComment();
            } else {
                var comment = commentEntryMap.get(commentModel.getId());
                if (comment) {
                    comment.highlightUnsavedComment();
                }
            }
        };

        this.onReply = function () {
            return !!replyEditor;
        };

        /**
         * Updating the visibility of the comment nodes and the thread nodes in the comments pane.
         * This might be modified by filtering of comment authors.
         */
        this.updateCommentVisibility = function () {

            var hideThread = true;

            commentEntryMap.forEach(function (comment) {
                comment.getNode().toggleClass('isFilteredComment', comment.getModel().isHidden());
                if (!comment.getModel().isHidden()) { hideThread = false; }
            });

            this.getNode().toggleClass('isFilteredThread', hideThread);

            // additionally it might be necessary to update the text on the hide button
            setHideButtonText();
        };

        this.delete = function () {
            if (replyEditor) {
                docView.getCommentCollection().deleteUnsavedComment();
            }
            this.destroy();
        };

        this.registerDestructor(function () {
            replyEditor?.destroy();
            docView.trigger('commentThread:delete', rootNode); // allowing apps to deregister handler functions
            rootNode.remove();
        });
    }
}
