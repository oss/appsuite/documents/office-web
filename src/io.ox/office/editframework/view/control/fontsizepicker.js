/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { ComboField, NumberValidator } from "@/io.ox/office/tk/controls";

// class FontSizePicker =======================================================

/**
 * A combo-box control used to select a font height.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the base class `ComboField`.
 */
export class FontSizePicker extends ComboField {

    constructor(_docView, config) {

        // base constructor
        super({
            width: "5em",
            tooltip: gt("Font size"),
            keyboard: "number",
            validator: new NumberValidator({ min: 1, max: 999.9, precision: 0.1 }),
            ...config
        });

        // additional marker class
        this.menu.$el.addClass("font-size-picker");
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();
        [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 20, 22, 24, 26, 28, 32, 36, 40, 44, 48, 54, 60, 66, 72, 80, 88, 96].forEach(size => {
            this.addOption(size, { label: _.noI18n(String(size)), textAlign: "right" });
        });
    }
}
