/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { math, dict } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import { TranslationDictionary } from "@/io.ox/office/baseframework/resource/translationdictionary";

// private functions ==========================================================

/**
 * Sorting comparator for the stylesheets in the dropdown list.
 */
function compareOptionButtons(sortIndex1, sortIndex2) {
    // first check for priority, then compare style names with regard to embedded numbers
    return math.compare(sortIndex1.priority, sortIndex2.priority) ||
        LOCALE_DATA.getCollator({ numeric: true }).compare(sortIndex1.styleName, sortIndex2.styleName);
}

// class StyleSheetPicker =====================================================

/**
 * A dropdown list control used to select a stylesheet. The dropdown list
 * entries will visualize the formatting attributes of the stylesheet if
 * possible.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 *
 * @param {string} family
 *  The attribute family of the stylesheets visualized by this control.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the base class `RadioList`,
 *  and the following additional options:
 *  - {string|string[]} [config.previewFamilies]
 *    The attribute families used to get the formatting options of the option
 *    buttons representing the stylesheets. If omitted, only attributes of the
 *    family specified by the "family" parameter will be used.
 *  - {string} [config.resourceKey]
 *    The key of the localized JSON resource containing the configuration of
 *    translated stylesheet names. Each property of the map must be a string
 *    with the English stylesheet name as key. These keys may contain regular
 *    expressions which must be placed in capturing groups (in parentheses).
 *    The values of the map are the translated stylesheet names. The
 *    placeholders "$1", "$2", etc. will match the capturing groups defined in
 *    the key patterns.
 *  - {boolean} [config.hideStyleLabel=false]
 *    Whether to skip creating a label element with the stylesheet name.
 *  - {object[]} [config.additionalStyles]
 *    Additional stylesheets to create option buttons for. The array contains
 *    objects with the style attributes.
 */
export class StyleSheetPicker extends AppRadioList {

    constructor(docView, family, config) {

        // base constructor
        super(docView, {
            listLayout: "grid",
            sorted: compareOptionButtons,
            updateCaptionMode: "none",
            valueSerializer: value => this._serializeValue(value),
            ...config
        });

        // the theme model that has been used to create the scheme color table
        this.themeModel = this.docModel.getThemeModel();
        // the style collection
        this.styleCollection = this.docModel.getStyleCollection(family);

        // the configuration for translated stylesheet names
        this._dictionary = undefined;

        // add CSS class to menu button and dropdown list for special formatting
        this.$el.addClass(`style-picker family-${family}`);
        this.menu.$el.addClass(`app-${this.docApp.appType} style-picker family-${family}`);

        // refresh the dropdown menu after theme has changed
        this.menu.requestMenuRepaintOnAllEvents(this.docModel.themeCollection);

        // update color buttons after a theme change
        this.listenTo(this.docModel, "update:targetchain", () => {
            const newThemeModel = this.docModel.getThemeModel();
            if (this.themeModel !== newThemeModel) {
                this.themeModel = newThemeModel;
                this.menu.requestMenuRepaint();
                this.refresh();
            }
        });

        // handle all changes in the stylesheet collection
        this.waitForImportSuccess(() => {
            this.listenTo(this.styleCollection, "insert:stylesheet", this._insertStyleHandler);
            this.listenTo(this.styleCollection, "delete:stylesheet", this._deleteStyleHandler);
            this.listenTo(this.styleCollection, "change:stylesheet", this._insertStyleHandler);
        });

        // load the translated style names if specified
        if (config?.resourceKey) {
            this._dictionary = this.docApp.resourceManager.getTranslations(config.resourceKey);
            this.menu.requestMenuRepaint();
            this.refresh();
        } else {
            this._dictionary = new TranslationDictionary({});
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Writes the translated name of the specified stylesheet into the dropdown
     * button.
     */
    /*protected override*/ implUpdate(styleId) {
        super.implUpdate(styleId);
        let styleName = styleId ? this.styleCollection.getName(styleId) : undefined;
        if (!styleName) {
            styleName = this.config.additionalStyles?.find(entry => entry.id === styleId)?.name;
        }
        this.setLabel(styleName ? this._translateStyleName(styleName) : "");
    }

    /**
     * Fills the dropdown list with all known style names, and adds preview CSS
     * formatting to the option buttons.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.clearOptions();

        // insert the stylesheets
        const styleNames = this.styleCollection.getStyleSheetNames({ skipHidden: true });
        dict.forEach(styleNames, (styleName, styleId) => this._createStylesheetButton(styleName, styleId));

        // create additional option buttons
        this.config.additionalStyles?.forEach(entry => {
            this._createOption(entry.name, entry.id, entry.category, entry.priority);
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the translation of the passed stylesheet name for the current
     * locale. Additionally, the passed style name will be adjusted further for
     * GUI display. Automatic line breaks before numbers will be prevented by
     * converting the preceding space characters to NBSP characters.
     *
     * @param {string} styleName
     *  The original (English) stylesheet name.
     *
     * @returns {string}
     *  The translated and adjusted stylesheet name. If no translation is
     *  available, the passed stylesheet name will be adjusted and returned.
     */
    /*private*/ _translateStyleName(styleName) {
        // replace space characters before numbers with NBSP characters
        return _.noI18n(this._dictionary.translate(styleName).replace(/ (\d)/g, "\xa0$1"));
    }

    /**
     * Calculates the value for the element attributes "data-value" and
     * "data-state" used in automated tests.
     */
    /*private*/ _serializeValue(styleId) {
        return this.styleCollection.isCustom(styleId) ? this.styleCollection.getName(styleId) : styleId;
    }

    /*private*/ _createOption(styleName, styleId, category, priority) {

        // TODO: hacked code for functions like "create new paragraph stylesheet" we could use the category for
        if (!styleId.startsWith("$cmd$")) {
            // translate and adjust style name
            styleName = this._translateStyleName(styleName);
        }

        // create the option button
        const label = this.config.hideStyleLabel ? null : styleName;
        const sortIndex = { priority, styleName };
        this.addOption(styleId, { section: category, label, tooltip: styleName, sortIndex, priority });
    }

    /**
     * Creates a new button in the dropdown menu for the specified stylesheet.
     */
    /*private*/ _createStylesheetButton(styleName, styleId) {

        // nothing to do for hidden styles
        if (this.styleCollection.isHidden(styleId)) { return; }

        // the section identifier for the stylesheet
        const category = this.styleCollection.getUICategory(styleId) || "styles";
        // sorting priority
        const priority = Math.floor(this.styleCollection.getUIPriority(styleId));

        this._createOption(styleName, styleId, category, priority);
    }

    /**
     * Handles inserted or modified stylesheets in the style collection.
     */
    /*private*/ _insertStyleHandler(styleId) {

        // bug 33737, 33747: restore browser focus after DOM manipulation
        this.menu.guardFocusedOption(() => {
            this.deleteOption(styleId);
            this._createStylesheetButton(this.styleCollection.getName(styleId), styleId);
        });
    }

    /**
     * Handles deleted stylesheets in the style collection.
     */
    /*private*/ _deleteStyleHandler(styleId) {
        this.deleteOption(styleId);
    }
}
