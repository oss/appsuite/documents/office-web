/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is } from "@/io.ox/office/tk/algorithms";
import { createIcon } from "@/io.ox/office/tk/dom";
import { setToolTip } from "@/io.ox/office/tk/forms";
import { isGuest } from "@/io.ox/office/tk/utils/driveutils";
import { UserPicture } from "@/io.ox/office/tk/control/userpicture";

import { CONTACTS_AVAILABLE, DEBUG } from "@/io.ox/office/editframework/utils/editconfig";
import { EDIT_ICON } from "@/io.ox/office/editframework/view/editlabels";

// class UserBadge ============================================================

/**
 * A badge displaying properties of an arbitrary user, consisting the user's
 * profile picture, display name, and active status.
 *
 * @param {EditView} docView
 *  The document view containing this control instance.
 *
 * @param {object|null} clientData
 *  The initial user information. Must contain the string properties "userId",
 *  "userName", "colorIndex", "remote", and "editor". See method
 *  `EditApplication#getActiveClients` for more details.
 *
 * @param {object} [config]
 *  Configuration options that will be passed to base class `UserPicture`. The
 *  following additional options are supported:
 *  - {boolean} [config.showColorBox=false]
 *    If set to `true`, the current user will have a colored status box.
 */
export class UserBadge extends UserPicture {

    constructor(docView, config) {

        // base constructor
        super({ ...config, size: 32 });

        // document access
        this.docModel = docView.docModel;
        this.docView = docView;
        this.docApp = docView.docApp;

        // initialization of the root node of this badge
        this.$el.addClass("user-badge").attr("role", "dialog");

        // insert the additional nodes into the group
        this.$el.append(
            $('<div class="user-name">'),
            $('<div class="user-status">').append('<div class="user-color">')
        );

        // bug 36482: repaint editor icon when application state changes, e.g. offline mode
        this.listenTo(this.docApp, "docs:state", this._updateStatusBox);
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(clientData) {
        super.implUpdate(clientData);

        const $userName = this.$el.find(".user-name").empty();
        // whether the user info pop-up can be shown
        const userHaloAvailable = CONTACTS_AVAILABLE && clientData && !clientData.guest && !isGuest();

        // the link (or inactive text label) containing the user name
        const $userLink = userHaloAvailable ? $('<a href="#" class="needs-action person" data-detail-popup="halo" >') : $('<div class="disabled">');
        $userName.append($userLink);

        // add display name of the user to link element
        $userLink.text(clientData?.userName ? _.noI18n(clientData.userName) : gt("Guest"));

        // add current user name and user identifier to link
        if (clientData?.userId) {
            $userLink.data("internal_userid", clientData.userId);
            // add the internal user identifier in debug mode
            if (DEBUG) {
                const $debugId = $('<span style="font-size:90%;font-style:italic;color:gray;">');
                $debugId.text(_.noI18n(`(${clientData.userId}) `));
                setToolTip($debugId, _.noI18n("OX user identifier"));
                $userLink.prepend($debugId);
            }
        }

        // update the color and icon in the status box
        this._updateStatusBox();
    }

    // private methods --------------------------------------------------------

    /*private*/ _updateStatusBox() {

        const clientData = this.getValue();
        const $userStatus = this.$el.find(".user-status");
        const $userColor = $userStatus.children(".user-color").empty();

        // show a colored status box for the user
        const showColor = this.config.showColorBox && is.number(clientData?.colorIndex);
        $userStatus.attr("data-color-index", showColor ? clientData.colorIndex : null);

        // mark user with editing rights with an appropriate icon (bug 36482: not during offline mode)
        if (clientData?.editor && this.docApp.isOnline()) {

            // create the editor icon
            $userColor.append(createIcon(EDIT_ICON));

            // tooltip for edit rights
            setToolTip($userColor, clientData.remote ?
                //#. shown as tooltip in a list of all users working on the same document
                gt("This user has edit rights for the document.") :
                //#. shown as tooltip in a list of all users working on the same document
                gt("You have edit rights for the document.")
            );
        } else {
            // delete icon and tooltip
            setToolTip($userColor, "");
        }
    }
}
