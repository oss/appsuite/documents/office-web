/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { setToolTip } from "@/io.ox/office/tk/forms";
import { TextField } from "@/io.ox/office/tk/control/textfield";

// class FileNameField ========================================================

/**
 * A text field used to edit the file name.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 */
export class FileNameField extends TextField {

    constructor(docView) {

        // base constructor
        super({
            classes: "filename-field",
            tooltip: gt("Rename document"),
            placeholder: gt("Document name"),
            width: "24em",
            shrinkWidth: "8em",
            select: true
        });

        // properties
        this.docView = docView;
        this.docApp = docView.docApp;

        // set document name as tool tip in read-only mode
        this.listenTo(this.docApp, "docs:editmode", this._updateToolTip);
        this._updateToolTip();

        // bug 54734: restore old file name when losing edit rights without committing
        this.listenTo(this.docApp, "docs:editmode:leave", () => {
            if (this.isEditMode()) { this.triggerCancel(); }
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Sets a predefined tool tip in edit mode, or the document name in
     * read-only mode.
     */
    /*private*/ _updateToolTip() {
        const fileName = this.docApp.getFullFileName();
        const tooltip = this.docView.isEditable() ? gt("Rename document") : _.noI18n(fileName);
        setToolTip(this.$el, tooltip);
    }
}
