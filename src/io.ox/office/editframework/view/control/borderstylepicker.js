/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { map } from "@/io.ox/office/tk/algorithms";
import { resolveCssVar } from "@/io.ox/office/tk/dom";
import { Canvas } from "@/io.ox/office/tk/canvas";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

import { getBorderPattern } from "@/io.ox/office/editframework/utils/border";
import { BORDER_STYLE_LABEL } from "@/io.ox/office/editframework/view/editlabels";

// constants ==================================================================

// the canvas element used to generate the bitmaps
const canvas = new Canvas({ location: { width: 32, height: 18 }, _kind: "singleton" });

// maps unique bitmap keys to the data URLs
const bitmapUrlMap = new Map/*<string, string>*/();

// private functions ==========================================================

/**
 * Creates a bitmap for the specified border line style, and returns its data
 * URL.
 *
 * @param {string} lineStyle
 *  The effective line style. MUST be one of "none", "solid", "dashed",
 *  "dotted", "dashDot", or "dashDotDot".
 *
 * @param {string} lineColor
 *  The effective CSS line color.
 *
 * @param {number} [lineWidth=1]
 *  Width of a (single) line in the border style, in pixels. The value 0 will
 *  be interpreted as hair line.
 *
 * @param {number} [lineCount=1]
 *  The number of parallel lines shown in the border.
 *
 * @returns {string}
 *  The data URL of the generated bitmap.
 */
function getBorderStyleBitmapUrl(lineStyle, lineColor, lineWidth = 1, lineCount = 1) {

    // return data URL of a bitmap already created, or create a new bitmap
    return map.upsert(bitmapUrlMap, `${lineStyle},${lineColor},${lineWidth},${lineCount}`, () => {

        return canvas.clear().renderToDataURL((context, width, height) => {

            // nothing to do for style "none"
            if (lineStyle === "none") { return; }

            // effective line width
            const lineWidthPx = Math.max(1, lineWidth);
            // dash pattern
            const pattern = getBorderPattern(lineStyle, lineWidthPx);

            // initialize line settings
            context.setLineStyle({ style: lineColor, width: lineWidthPx, pattern });

            // render hair lines as semi-transparent pixel lines
            if (lineWidth < 1) { context.setGlobalAlpha(0.4); }

            // draw the lines
            const y1 = Math.floor(height / 2) - lineWidthPx * (lineCount - 1) - (lineWidthPx % 2) / 2;
            for (let y = y1; lineCount > 0; lineCount -= 1, y += 2 * lineWidthPx) {
                context.drawLine(0, y, width, y);
            }
        });
    });
}

// class BorderStylePicker ====================================================

/**
 * A generic drop-down list control for border styles.
 *
 * @param {object[]} entries
 *  An array of descriptors for the entries of the dropdown list. Each array
 * element MUST be an object with the following properties:
 *  - {unknown} value
 *    The value associated to the list item.
 *  - {string} label
 *    The text label for the list item.
 *  - {string} style
 *    The line style: one of "solid", "dashed", "dotted", "dashDot", or
 *    "dashDotDot".
 *  - {number} [width=1]
 *    Width of a (single) line, in pixels. The value 0 will be interpreted as
 *    hair line.
 *  - {number} [count=1]
 *    Number of parallel lines.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `RadioList`.
 */
export class BorderStylePicker extends RadioList {

    constructor(entries, config) {

        // base constructor
        super({
            icon: "png:line-style",
            label: BORDER_STYLE_LABEL,
            updateCaptionMode: "none",
            ...config
        });

        // properties
        this._entries = entries;

        // repaint canvas-based bitmap icons
        this.menu.requestMenuRepaintOnGlobalEvent("change:theme");
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.clearOptions();

        const textColor = resolveCssVar("--text");
        const selectColor = resolveCssVar("--listmenu-selected-text-color");

        this._entries.forEach(({ value, label, style, width, count }) => {
            const bmpIcon1 = getBorderStyleBitmapUrl(style, textColor, width, count);
            const bmpIcon2 = (selectColor === textColor) ? "" : getBorderStyleBitmapUrl(style, selectColor, width, count);
            this.addOption(value, { label, bmpIcon: [bmpIcon1, bmpIcon2] });
        });
    }
}
