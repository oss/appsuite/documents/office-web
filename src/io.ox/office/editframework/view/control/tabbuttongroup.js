/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { setCaptionText, showNodes } from '@/io.ox/office/tk/forms';
import { RadioGroup } from '@/io.ox/office/tk/control/radiogroup';

// class TabButtonGroup =======================================================

/**
 * A button group used to select the active toolpane.
 */
export class TabButtonGroup extends RadioGroup {

    constructor(docView, config) {

        // base constructor
        super({ ...config, role: "tablist" });

        // own marker class for styling
        this.$el.addClass("tab-button-group");

        // update control according to the tab collection
        this.listenTo(docView.toolPaneManager, "tab:create",   this._createTabButton);
        this.listenTo(docView.toolPaneManager, "tab:label",    this._setTabButtonLabel);
        this.listenTo(docView.toolPaneManager, "tab:toggle",   this._toggleTabButton);
        this.listenTo(docView.toolPaneManager, "tab:activate", entry => this.setValue(entry?.tabId));
    }

    // private methods --------------------------------------------------------

    /*private*/ _toggleTabButton(entry) {
        showNodes(this.findOptions(entry.tabId), entry.visible);
        this.refresh({ immediate: true });
    }

    /*private*/ _setTabButtonLabel(entry) {
        setCaptionText(this.findOptions(entry.tabId), entry.label);
        this.refresh({ immediate: true });
    }

    /*private*/ _createTabButton(entry) {
        if (entry.label || entry.labelKey) {
            this.addOption(entry.tabId, { icon: entry.icon, label: entry.label, role: "tab" });
            this._toggleTabButton(entry);
        }
    }
}
