/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";
import ox from "$/ox";

import { is } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA, localeLogger } from "@/io.ox/office/tk/locale";
import { getButtonValue, setCaptionIcon, setButtonOptions } from "@/io.ox/office/tk/forms";
import { RadioList } from "@/io.ox/office/tk/control/radiolist";

import { getLocalesWithDictionary } from "@/io.ox/office/editframework/utils/editconfig";

// constants ==================================================================

// locale codes of all languages to be shown, with the translated language names
const LOCALES = [
    "ca_ES",
    "cs_CZ",
    "da_DK",
    "de_DE",
    "el_GR",
    "en_GB",
    "en_US",
    "es_ES",
    "fi_FI",
    "fr_FR",
    "hu_HU",
    "it_IT",
    "ja_JP",
    "lv_LV",
    "nl_NL",
    "pl_PL",
    "pt_PT",
    "pt_BR",
    "ro_RO",
    "ru_RU",
    "sk_SK",
    "sv_SE",
    "tr_TR",
    "zh_CN",
    "zh_TW"
];

// special list item value for the entry "None" (no language selected)
const NONE_VALUE = "none";

// class LanguagePicker =======================================================

/**
 * A drop-down list with all supported languages.
 *
 * @param {object} [config]
 *  Optional parameters. Supports all options of the base class `RadioList`.
 *  Additionally, the following options are supported:
 *  - {boolean} [config.hideUnusedLanguages=false]
 *    If set to `true`, shows the language of the currently selected word
 *    only. By default, all supported languages will be shown.
 */
export class LanguagePicker extends RadioList {

    constructor(docView, config) {

        // base constructor
        super({
            label: gt("Language"),
            tooltip: gt("Text language"),
            width: "14em",
            shrinkWidth: "10em",
            ...config,
            sorted: true,
            // show most recently used languages in an own section
            mruSize: 4,
            mruSettingsKey: "languagepicker"
        });

        // the language identifiers of all visible list entries
        this._usedLangIds = new Set/*<string>*/();

        this.menu.$el.addClass("language-picker");

        // create the "None" button
        this.addSection("none");
        this.addOption(NONE_VALUE, {
            //#. A special entry used in a language selector control that means "No language selected" (e.g. no spell checking).
            label: gt.pgettext("language", "None"),
            labelStyle: { fontStyle: "italic" }
        });

        // create the buttons for all supported languages
        this.addSection("langs", config?.hideUnusedLanguages ? { filter: langId => this._usedLangIds.has(langId) } : undefined);
        LOCALES.forEach(locale => {
            const languageName = LOCALE_DATA.getLanguageAndRegionName(locale, true);
            if (languageName) {
                const langId = locale.replace("_", "-");
                this.addOption(langId, { icon: "png:none", label: languageName });
            } else {
                localeLogger.error(`$badge{LanguagePicker} no language name available for locale "${locale}"`);
            }
        });

        // always show the UI language (not locale!)
        this._usedLangIds.add(ox.language.replace("_", "-"));

        // always show languages marked as "used" in document settings
        const docLangIds = docView.docModel.globalConfig.usedLanguages;
        if (is.array(docLangIds)) { docLangIds.forEach(langId => this._usedLangIds.add(langId)); }

        // show icons for available dictionaries
        this.onFulfilled(getLocalesWithDictionary(), supportedLocales => {
            for (const buttonNode of this.getOptions().get()) {
                const langId = getButtonValue(buttonNode);
                if ((langId !== NONE_VALUE) && supportedLocales.includes(langId.replace("-", "_"))) {
                    setCaptionIcon(buttonNode, "png:dict-available");
                    setButtonOptions(buttonNode, { icon: "png:dict-available" }); // for auto-update of dropdown button
                }
            }
            this.refresh();
        });

        // add an activated language to the MRU list
        this.on("group:commit", langId => {
            if (langId !== NONE_VALUE) {
                this.menu.setMRUValue(langId);
            }
        });
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(langId) {
        super.implUpdate(langId);
        this._usedLangIds.add(langId);
    }
}
