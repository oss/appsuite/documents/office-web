/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { ComboField, NumberValidator } from "@/io.ox/office/tk/controls";
import { extendOptions } from "@/io.ox/office/tk/utils";
import { BORDER_WIDTH_LABEL } from "@/io.ox/office/editframework/view/editlabels";

// class BorderWidthPicker ====================================================

/**
 * A combo field with predefined entries for border line widths, in points.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options supported by the base class
 *  `ComboField`.
 */
export class BorderWidthPicker extends ComboField {

    constructor(_docView, config) {

        // base constructor
        super(extendOptions({
            width: "5em",
            tooltip: BORDER_WIDTH_LABEL,
            style: { textAlign: "right" },
            keyboard: "number",
            validator: new NumberValidator({ min: 0.5, max: 10, precision: 0.1 }),
            dropDownVersion: { label: config?.tooltip || BORDER_WIDTH_LABEL }
        }, config));

        // update dropdown list on changed UI locale
        this.menu.requestMenuRepaintOnGlobalEvent("change:locale");
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.clearOptions();

        [0.5, 1, 1.5, 2, 2.5, 3, 4, 6].forEach(size => {
            this.addOption(size, {
                //#. A single list entry of a GUI border width picker.
                //#. Example result: "5 points"
                //#. %1$d is the width of the border line.
                label: gt.npgettext("borders", "%1$d point", "%1$d points", size, LOCALE_DATA.formatDecimal(size)),
                textAlign: "right"
            });
        });
    }
}
