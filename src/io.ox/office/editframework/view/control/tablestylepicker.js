/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { ary, dict } from "@/io.ox/office/tk/algorithms";
import { containsNode } from "@/io.ox/office/tk/dom";
import { Canvas } from "@/io.ox/office/tk/canvas";
import { DataPipeline } from "@/io.ox/office/tk/workers";
import { getButtonValue } from "@/io.ox/office/tk/forms";

import { isVisibleBorder } from "@/io.ox/office/editframework/utils/border";
import { StyleSheetPicker } from "@/io.ox/office/editframework/view/control/stylesheetpicker";

// constants ==================================================================

const CELL_WIDTH = 16;

const CELL_HEIGHT = 8;

const COL_COUNT = 5;

const ROW_COUNT = 6;

const ICON_WIDTH = CELL_WIDTH * COL_COUNT + 5.5;

const ICON_HEIGHT = CELL_HEIGHT * ROW_COUNT + 5.5;

const ICON_CONTAINER_WIDTH = ICON_WIDTH + 5.5;

const ICON_CONTAINER_HEIGHT = ICON_HEIGHT + 5.5;

// private functions ==========================================================

/**
 * Returns the effective rendering line width for the passed border.
 */
function getBorderWidth(border) {
    return (border.style === "double") ? 2 : 1;
}

// class TableStylePicker =====================================================

/**
 * A dropdown menu control for table stylesheets.
 *
 * @param {EditView} docView
 *  The document view instance owning this control.
 *
 * @param {Function} styleFlagsResolver
 *  A callback function that will be invoked everytime before the dropdown menu
 *  will be painted. Must return an object with the current style flags of the
 *  selected table element; or `null`, if no table element is selected. All
 *  style flags can be omitted, their default values are `false`.
 *
 * @param {object} [config]
 *  Configuration options.
 */
export class TableStylePicker extends StyleSheetPicker {

    constructor(docView, styleFlagsResolver, config) {

        // base constructor
        super(docView, "table", {
            width: "16em",
            icon: "png:table-style",
            //#. tool tip: predefined styles for text tables
            tooltip: gt("Table style"),
            gridColumns: 7,
            resourceKey: "tablestylenames",
            hideStyleLabel: true,
            additionalStyles: config?.additionalStyles
        });

        // table style flags (visibility of header/footer, first/last column, etc.)
        this._styleFlags = dict.fill("firstRow lastRow firstCol lastCol bandsHor bandsVert", false);
        // the canvas element used to render the preview tables
        this._canvas = this.member(new Canvas({ location: { left: -1, top: -1, width: ICON_WIDTH + 2, height: ICON_HEIGHT + 2 } }));
        // background renderer for pending tables
        this._pipeline = this.member(new DataPipeline(buttonNode => this._updateTableFormatting(buttonNode), { paused: true }));

        // create the sections for the stylesheet buttons
        this.addSection("styles");
        ["Light", "Medium", "Dark"].forEach(id => this.addSection(id, this._translateStyleName(id)));

        // register a handler that inserts a table element into each option button
        this.listenTo(this.menu, "list:option:add", $button => {

            $button.css({
                minWidth: ICON_CONTAINER_WIDTH,
                maxWidth: ICON_CONTAINER_WIDTH,
                maxHeight: ICON_CONTAINER_HEIGHT,
                padding: 0,
                lineHeight: "normal"
            }).append('<img style="margin:0px;">');

            // an additional background fill node as overlay
            $button.prepend('<div class="page-effect abs">');

            // register the button for rendering, but do not actually render it yet
            this._pipeline.pushValue($button[0]);
        });

        // repaint the preview tables, if the style flags of the selected table are different
        this.listenTo(this.menu, "popup:beforeshow", () => {

            // the current style flags for comparison
            const oldStyleFlags = { ...this._styleFlags };
            // get the current table style flags
            const newStyleFlags = styleFlagsResolver.call(this);

            // update the complete style flags object (newStyleFlags may be incomplete!)
            dict.forEachKey(this._styleFlags, key => {
                this._styleFlags[key] = !!newStyleFlags[key];
            });

            // put all buttons into the pending map, if the style flags have changed
            if (!_.isEqual(oldStyleFlags, this._styleFlags)) {
                this._pipeline.abort();
                this._pipeline.pushValues(this.getOptions().get());
            }

            // render the pending table elements
            this._pipeline.resume();
        });

        // stop rendering pending tables when hiding the popup menu
        this.listenTo(this.menu, "popup:hide", () => {
            this._pipeline.pause();
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the border with stronger visual appearance.
     *
     * @param {Border} border1
     * The first border to compare.
     *
     * @param {Border} border2
     * The second border to compare.
     *
     * @param {string|string[]|null} targets
     *  A name or an array of names to specify the active theme targets.
     *
     * @returns {Border}
     *  One of the passed border with stronger visual appearance. If one of the
     *  passed borders is invisible, the other border will be returned.
     */
    /*private*/ _getStrongerBorder(border1, border2, targets) {

        // handle missing borders (return the other border)
        if (!isVisibleBorder(border1)) { return border2; }
        if (!isVisibleBorder(border2)) { return border1; }

        // compare pixel border width
        const widthDiff = getBorderWidth(border1) - getBorderWidth(border2);
        if (widthDiff > 0) { return border1; }
        if (widthDiff < 0) { return border2; }

        // compare luma value (darker color wins!)
        const y1 = this.docModel.parseAndResolveColor(border1.color, "line", targets).y;
        const y2 = this.docModel.parseAndResolveColor(border2.color, "line", targets).y;
        return (y1 < y2) ? border1 : border2;
    }

    /**
     * Set the line style for the specified border.
     *
     * @param {Canvas2DContext} context
     *  The wrapped rendering context of a canvas element.
     *
     * @param {Border} border
     * to get the line style
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted or
     *  set to `null`, the active theme of the document will be used.
     *
     * @returns {number} the with of the border
     */
    /*private*/ _setBorderLineStyle(context, border, targets) {
        const cssColor = this.docModel.getCssColor(border.color, "line", targets);
        const lineWidth = getBorderWidth(border);
        context.setLineStyle({ style: cssColor, width: lineWidth });
        return lineWidth;
    }

    /**
     * Renders a line as a text placeholder to show text color.
     *
     * @param {Canvas2DContext} context
     *  The wrapped rendering context of a canvas element.
     *
     * @param {CellAttributeSet} attrSet
     *  The style attributes of the cell.
     *
     * @param {OpColor[]} fillColors
     *  The fill color of the cell.
     *
     * @param {number} left
     *  The left position of the cell.
     *
     * @param {number} top
     *  The top top positon of the cell.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted or
     *  set to `null`, the active theme of the document will be used.
     */
    /*private*/ _renderTextPlaceholder(context, attrSet, fillColors, left, top, targets) {
        // Paint line for the text color
        const textColor = attrSet.character?.color;
        const cssColor = this.docModel.parseAndResolveTextColor(textColor, fillColors, targets).css;
        context.setLineStyle({ style: cssColor, width: 1 });
        context.drawLine(left + 4, top + CELL_HEIGHT - 4.5, left + CELL_WIDTH - 4, top + CELL_HEIGHT - 4.5);
    }

    /**
     * Render Border and the text placeholder see@ renderTextPlaceholder().
     *
     * @param {Canvas2DContext} context
     *  The wrapped rendering context of a canvas element.
     *
     * @param {CellAttributeSet} attrSet
     *  The style attributes of the cell.
     *
     * @param {number} col the column index of the cell.
     * @param {number} row the row index of the cell.
     * @param {type} topAttr the style attributes of the cell above
     * @param {type} bottomAttr the style attributes of the cell bellow
     * @param {type} leftAttr the style attributes of the cell on the left side
     * @param {type} rightAttr the style attributes of the cell on the right side
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted or
     *  set to `null`, the active theme of the document will be used.
     */
    /*private*/ _renderBordersAndTextPlaceholder(context, attrSet, row, col, topAttr, bottomAttr, leftAttr, rightAttr, targets) {

        const left = row * CELL_WIDTH + 2;
        const top = col * CELL_HEIGHT + 2;

        const cellStyle = attrSet.cell;
        const fillColors = [];
        if (cellStyle) {

            if (cellStyle.fillColor) {
                fillColors.push(cellStyle.fillColor);
            }

            let borderWidth, borderTop, borderBottom, borderLeft, border;

            if (isVisibleBorder(cellStyle.borderTop)) {
                border = cellStyle.borderTop;
                if (row > 0 && topAttr?.cell?.borderBottom) {
                    border = this._getStrongerBorder(border, topAttr.cell.borderBottom, targets);
                }
                borderWidth = this._setBorderLineStyle(context, border, targets);
                borderTop = top - borderWidth / 2;
                context.drawLine(left, borderTop, left + CELL_WIDTH, borderTop);
            }

            if (isVisibleBorder(cellStyle.borderBottom)) {
                border = cellStyle.borderBottom;
                if (row + 1 < ROW_COUNT && bottomAttr?.cell?.borderTop) {
                    border = this._getStrongerBorder(border, bottomAttr.cell.borderTop, targets);
                }
                borderWidth = this._setBorderLineStyle(context, border, targets);
                borderBottom = top + CELL_HEIGHT - borderWidth / 2;
                context.drawLine(left, borderBottom, left + CELL_WIDTH, borderBottom);
            }

            if (isVisibleBorder(cellStyle.borderLeft)) {
                border = cellStyle.borderLeft;
                if (col > 0 && leftAttr?.cell?.borderRight) {
                    border = this._getStrongerBorder(border, leftAttr.cell.borderRight, targets);
                }
                borderWidth = this._setBorderLineStyle(context, border, targets);
                borderTop = borderTop || (top - 0.5);
                borderBottom = borderBottom || (top + CELL_HEIGHT + 0.5);
                borderLeft = left - borderWidth / 2;
                context.drawLine(borderLeft, borderTop, borderLeft, borderBottom);
            }

            if (isVisibleBorder(cellStyle.borderRight)) {
                border = cellStyle.borderRight;
                if (col + 1 < COL_COUNT && rightAttr?.cell?.borderLeft) {
                    border = this._getStrongerBorder(border, rightAttr.cell.borderLeft, targets);
                }
                borderWidth = this._setBorderLineStyle(context, border, targets);
                borderTop = borderTop || (top - 0.5);
                borderBottom = borderBottom || (top + CELL_HEIGHT + 0.5);
                borderLeft = left + CELL_WIDTH - borderWidth / 2;
                context.drawLine(borderLeft, borderTop, borderLeft, borderBottom);
            }
        }

        this._renderTextPlaceholder(context, attrSet, fillColors, left, top, targets);
    }

    /**
     * Render the fill color for the specified cell.
     *
     * @param {Canvas2DContext} context
     *  The wrapped rendering context of a canvas element.
     *
     * @param {Number} col
     *  the column index of the cell.
     *
     * @param {Number} row
     *  the row index of the cell.
     *
     * @param {CellAttributeSet} attrSet
     *  The style attributes of the cell.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted or
     *  set to `null`, the active theme of the document will be used.
     */
    /*private*/ _renderCellFillColor(context, col, row, attrSet, targets) {
        if (attrSet.cell?.fillColor) {
            const left = col * CELL_WIDTH + 2;
            const top = row * CELL_HEIGHT + 2;
            context.setFillStyle(this.docModel.getCssColor(attrSet.cell.fillColor, "fill", targets));
            context.drawRect(left, top, CELL_WIDTH, CELL_HEIGHT, "fill");
        }
    }

    /**
     * Registers a new table that will be formatted delayed with a specific
     * table stylesheet.
     */
    /*private*/ _updateTableFormatting(buttonNode) {

        // check that the button is still in the dropdown menu (menu may have been cleared in the meantime)
        if (!containsNode(this.menu.$el, buttonNode)) { return; }

        // the identifier of the table style sheet
        const styleId = getButtonValue(buttonNode);
        // the current theme targets
        const targets = this.docModel.getThemeTargets();
        // the attribute set of the table style
        const tableAttrs = this.styleCollection.getStyleSheetAttributeMap(styleId, true);

        // render into the canvas, and obtain the data URL
        const dataURL = this._canvas.clear().renderToDataURL(context => {

            // paint background color and cache the cell style attributes
            const attrMatrix = ary.generate(ROW_COUNT, row => ary.generate(COL_COUNT, col => {
                const { cellAttrSet } = this.styleCollection.resolveCellAttributeSet(tableAttrs, col, row, COL_COUNT, ROW_COUNT, this._styleFlags);
                this._renderCellFillColor(context, col, row, cellAttrSet, targets);
                return cellAttrSet;
            }));

            // paint the border by using the cached cell styles
            attrMatrix.forEach((attrVector, row) => {
                const prevVector = (row > 0) ? attrMatrix[row - 1] : null;
                const nextVector = (row + 1 < ROW_COUNT) ? attrMatrix[row + 1] : null;
                attrVector.forEach((cellAttrs, col) => {
                    const tAttrs = prevVector ? prevVector[col] : null;
                    const bAttrs = nextVector ? nextVector[col] : null;
                    const lAttrs = (col > 0) ? attrVector[col - 1] : null;
                    const rAttrs = (col + 1 < COL_COUNT) ? attrVector[col + 1] : null;
                    this._renderBordersAndTextPlaceholder(context, cellAttrs, col, row, tAttrs, bAttrs, lAttrs, rAttrs, targets);
                });
            });
        });

        // set the rendered table image
        $(buttonNode).children("img").attr("src", dataURL);
    }
}
