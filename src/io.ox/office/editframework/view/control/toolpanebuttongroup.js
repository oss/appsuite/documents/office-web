/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { setFocus, findFocusable, matchKeyCode } from "@/io.ox/office/tk/dom";
import { getButtonValue } from "@/io.ox/office/tk/forms";

import { TabButtonGroup } from "@/io.ox/office/editframework/view/control/tabbuttongroup";

// class ToolPaneButtonGroup ==================================================

/**
 * A button group used to select the active toolpane.
 */
export class ToolPaneButtonGroup extends TabButtonGroup {

    constructor(docView) {

        // base constructor
        super(docView, {
            classes: "toolpane-button-group tab-button-style"
        });

        // add own keyboard handling
        this.$el.on("keydown", event => {

            const startNode = event.target;
            const focusableNodes = findFocusable(this.$el).get();
            const index = focusableNodes.indexOf(startNode);

            const focusNode = (index < 0) ? null :
                matchKeyCode(event, "LEFT_ARROW") ? focusableNodes[index - 1] :
                matchKeyCode(event, "RIGHT_ARROW") ? focusableNodes[index + 1] : null;

            if (focusNode) {
                setFocus(focusNode);
                this.triggerCommit(getButtonValue(focusNode), { preserveFocus: true });
                event.preventDefault();
            }
        });

        // hide toolbars after double click on tab buttons
        this.$el.on("dblclick", () => docView.mainPane.hide());
    }
}
