/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { str } from "@/io.ox/office/tk/algorithms";
import { createSpan } from "@/io.ox/office/tk/dom";

import { TextValidator, AppComboField } from "@/io.ox/office/baseframework/view/basecontrols";
import { MAJOR_FONT_KEY, MINOR_FONT_KEY } from "@/io.ox/office/editframework/utils/attributeutils";

// class FontNameValidator ====================================================

class FontNameValidator extends TextValidator {

    constructor(docModel) {
        super();
        this.docModel = docModel;
    }

    valueToText(value) {
        return this.docModel.resolveFontName(value);
    }
}

// class FontFamilyPicker =====================================================

/**
 * A combo-box control used to select a font family.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the base class `ComboField`.
 */
export class FontFamilyPicker extends AppComboField {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            width: "12em",
            tooltip: gt("Font name"),
            ...config,
            sorted: true,
            typeAhead: true,
            validator: new FontNameValidator(docView.docModel),
            shrinkWidth: "8.5em"
        });

        // CSS marker class for styling
        this.menu.$el.addClass("font-family-picker");

        // repaint all known fonts after font collection or theme collection have changed
        this.menu.requestMenuRepaintOnAllEvents(this.docModel.themeCollection);
        this.menu.requestMenuRepaintOnAllEvents(this.docModel.fontCollection);
        this.menu.requestMenuRepaintOnEvent(this.docModel, "update:targetchain");
    }

    // protected methods ------------------------------------------------------

    /**
     * Fills the drop-down list with all known font names.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        this.clearOptions();

        // create the menu entries for scheme fonts
        if (this.docApp.isOOXML()) {
            this.addSection("scheme");
            //#. a label suffix for the default font of the document, used for "important text" such as headings (shown in font drop-down font name)
            this._createFontEntry(MAJOR_FONT_KEY, gt.pgettext("font", "(Headings)"), 1);
            //#. a label suffix for the default font of the document, used for "regular text" (shown in font drop-down after font name)
            this._createFontEntry(MINOR_FONT_KEY, gt.pgettext("font", "(Body)"), 2);
        }

        // create the menu entries for the regular fonts contained in the document font collection
        this.addSection("fonts");
        for (const fontName of this.docModel.fontCollection.getFontNames()) {
            this._createFontEntry(fontName);
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Creates a list item for the specified font.
     */
    /*private*/ _createFontEntry(fontKey, subLabel, sortIndex) {
        const fontName = this.valueToText(fontKey);
        const $button = this.addOption(fontKey, {
            label: _.noI18n(fontName),
            labelStyle: { fontFamily: this.docModel.getCssFontFamily(fontName), fontSize: "115%" },
            tooltip: _.noI18n(str.concatTokens(fontName, subLabel)),
            sortIndex
        });
        if (subLabel) {
            $button.append(createSpan({ classes: "sub-label", label: subLabel }));
        }
    }
}
