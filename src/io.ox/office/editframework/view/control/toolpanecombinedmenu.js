/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button } from "@/io.ox/office/baseframework/view/basecontrols";
import { ToolPanePicker } from "@/io.ox/office/editframework/view/control/toolpanepicker";
import { UNDO_BUTTON_OPTIONS, REDO_BUTTON_OPTIONS } from '@/io.ox/office/editframework/view/editlabels';

// class ToolPaneCombinedMenu =================================================

/**
 * A button with a dropdown menu used to select the active toolpane, and
 * additional controls specifically for "combined paned" mode, e.g. a button
 * for toggling the search/replace toolpanes, and buttons for Undo/Redo.
 */
export class ToolPaneCombinedMenu extends ToolPanePicker {

    constructor(docView) {

        // base constructor
        super(docView);

        // always show the icon only, never the label of the active toolpane
        this.toggleShrinkMode(true);

        // the list with additional tab buttons for "combined panes" mode (separated with a line)
        this.addSection("combinedpanes");
        const searchButton = this.addControl("view/toolpane/combinedpanes/search", new Button({ icon: "bi:search", label: gt("Search") }));

        // add Undo/Redo buttons in an inline section
        this.addSection("undo", { inline: true });
        this.addControl("document/undo", new Button(UNDO_BUTTON_OPTIONS), { visibleKey: "document/editable" });
        this.addControl("document/redo", new Button(REDO_BUTTON_OPTIONS), { visibleKey: "document/editable" });

        // update the label of the "Search" tab button according to visibility of the additional "Replace" pane
        this.listenTo(docView, "controller:update", () => {
            const isSearchTab = docView.toolPaneManager.getActiveTabId() === "search";
            const isReplaceVisible = docView.searchPane.isVisible();
            searchButton.setLabel((isSearchTab && !isReplaceVisible) ? gt("Replace") : gt("Search"));
        });
    }
}
