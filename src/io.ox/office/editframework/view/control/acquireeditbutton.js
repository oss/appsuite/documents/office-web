/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button } from "@/io.ox/office/tk/control/button";
import { EDIT_ICON, EDIT_SHORT_LABEL } from "@/io.ox/office/editframework/view/editlabels";

// class AcquireEditButton ====================================================

/**
 * A button used to acquire edit rights for the document.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 */
export class AcquireEditButton extends Button {

    constructor(docView) {

        // base constructor
        super({
            icon: EDIT_ICON,
            label: EDIT_SHORT_LABEL,
            tooltip: gt("Acquire edit rights")
        });

        // start and stop the spinning animation
        this.listenTo(docView.docApp, "docs:acquireeditrights:start", () => this._toggleAnimation(true));
        this.listenTo(docView.docApp, "docs:acquireeditrights:end docs:editmode:enter", () => this._toggleAnimation(false));
    }

    // private methods --------------------------------------------------------

    /**
     * Starts or stops the spinning animation while edit rights are being
     * acquired.
     *
     * @param {boolean} state
     *  Whether to start or stop the animation.
     */
    _toggleAnimation(state) {
        if (state) {
            this.setIcon("bi:arrow-repeat", { classes: "do-spin" });
        } else {
            this.setIcon(EDIT_ICON);
        }
    }
}
