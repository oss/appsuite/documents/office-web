/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { Label } from "@/io.ox/office/tk/control/label";

// class ApplicationStatusLabel ===============================================

/**
 * Shows the current status of the application when it has changed.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 */
export class ApplicationStatusLabel extends Label {

    constructor(docView) {

        // base constructor
        super({
            classes: "app-status-label",
            wrapText: true
        });

        this.docView = docView;
        this._timer = null;

        // make label node an ARIA live region
        this.$el.attr({
            role: "status",
            "aria-live": "polite",
            "aria-relevant": "additions",
            "aria-atomic": true,
            "aria-readonly": true
        });

        // update caption according to application status
        this.listenTo(docView.docApp, "docs:state", this._updateStatusLabel);
    }

    // private methods --------------------------------------------------------

    /*private*/ _updateStatusLabel(state) {

        // the registered caption settings for the new application state
        const stateConfig = this.docView.getAppStateConfig(state);

        // DOCS-3497: update the data attribute for automated testing
        this.$el.attr("data-state", state);

        // cancel pending timer for a busy caption
        this._timer?.abort();

        // if the application state is a temporary "busy" state, defer showing the caption
        this._timer = this.setTimeout(() => {
            this.setLabel(stateConfig?.label ?? "");
            if (stateConfig?.busy) {
                this.setIcon("bi:arrow-repeat", { classes: "do-spin" });
            } else {
                this.setIcon(stateConfig?.icon ?? null);
            }
        }, stateConfig?.delay ?? 0);
    }
}
