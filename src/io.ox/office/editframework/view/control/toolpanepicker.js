/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";
import { TabButtonGroup } from "@/io.ox/office/editframework/view/control/tabbuttongroup";

// class ToolPanePicker =======================================================

/**
 * A button with a dropdown menu used to select the active toolpane.
 */
export class ToolPanePicker extends CompoundButton {

    constructor(docView, config) {

        // base constructor
        super(docView, {
            //#. tool tip for a drop-down selector for tool bars (on small devices)
            tooltip: gt("Select a toolbar"),
            width: "11em",
            shrinkIcon: "bi:list",
            ...config
        });

        // own marker class for styling
        this.$el.addClass("toolpane-picker tab-button-style");

        // dropdown menu for toolpanes always in selected state
        this.$menuButton.addClass("selected");

        // the list with all regular tab buttons
        this.addSection("tabs");
        this.addControl("view/toolbars/tab", new TabButtonGroup(docView));

        // update control according to the tab collection
        this.listenTo(docView.toolPaneManager, "tab:activate", entry => {
            this.setLabel(entry?.label || "");
        });
    }
}
