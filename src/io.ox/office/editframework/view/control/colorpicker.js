/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { is, math, fun, ary } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE, TOUCH_DEVICE, toNode, createDiv, createSpan, getExactNodeSize } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { getDebugFlag } from "@/io.ox/office/tk/config";
import { extendOptions } from "@/io.ox/office/tk/utils";

import { AppRadioList } from "@/io.ox/office/baseframework/view/basecontrols";
import { Color } from "@/io.ox/office/editframework/utils/color";
import { getTextColorName, getFillColorName, getAccentColorName } from "@/io.ox/office/editframework/view/editlabels";

import "@/io.ox/office/editframework/view/control/colorpicker.less";

// constants ==================================================================

// whether to show a slider for the scheme colors
const SMALL_COLOR_PICKER = (SMALL_DEVICE && TOUCH_DEVICE) || getDebugFlag("office:small-color-picker");

// private functions ==========================================================

// maps "light" and "dark" scheme color identifiers to "background" and "text" as used in color picker
function normalizeSchemeColorId(colorId) {
    // TODO: map light/dark to lighter/darker color according to color scheme
    return { light1: "background1", light2: "background2", dark1: "text1", dark2: "text2" }[colorId] || colorId;
}

// returns whether type and value are considered being equal
function equalTypeAndValue(opColor1, opColor2) {

    // exactly the same type
    if (!opColor1 || !opColor2 || (opColor1.type !== opColor2.type)) { return false; }

    // exactly the same value (not for auto colors)
    const value1 = is.string(opColor1.value) ? opColor1.value.toLowerCase() : null;
    const value2 = is.string(opColor2.value) ? opColor2.value.toLowerCase() : null;
    if ((opColor1.type === "auto") || (value1 === value2)) { return true; }

    // scheme colors: normalize color IDs (replace light/dark with background/text)
    return (opColor1.type === "scheme") && !!value1 && !!value2 && (normalizeSchemeColorId(value1) === normalizeSchemeColorId(value2));
}

function equalTransformation(transform1, transform2) {
    return is.string(transform1.type) && (transform1.type === transform2.type) && (
        (!("value" in transform1) && !("value" in transform2)) ||
        // bug 30085: may not be completely equal
        (is.number(transform1.value) && is.number(transform2.value) && (Math.abs(transform1.value - transform2.value) <= 256))
    );
}

// returns whether transformations are considered being equal
function equalTransformations(opColor1, opColor2) {
    // transformations may be missing or empty in both colors
    if (is.empty(opColor1.transformations) && is.empty(opColor2.transformations)) { return true; }
    // arrays must be equally sized, and transformations must be similar enough
    return is.array(opColor1.transformations) && is.array(opColor2.transformations) && ary.equals(opColor1.transformations, opColor2.transformations, equalTransformation);
}

// class PresetColorRegistry ==================================================

/**
 * Storage for all predefined colors shown in a `ColorPicker` control. Will be
 * created once (via static method `get()`, and shared by all color picker
 * instances.
 */
class PresetColorRegistry {

    /*private*/ constructor() {

        // option button configurations for standard colors
        this.standardColors = [
            { name: "dark-red",    color: Color.fromRgb("C00000"), tooltip: gt("Dark red") },
            { name: "red",         color: Color.fromRgb("FF0000"), tooltip: gt("Red") },
            { name: "orange",      color: Color.fromRgb("FFC000"), tooltip: gt("Orange") },
            { name: "yellow",      color: Color.fromRgb("FFFF00"), tooltip: gt("Yellow") },
            { name: "light-green", color: Color.fromRgb("92D050"), tooltip: gt("Light green") },
            { name: "green",       color: Color.fromRgb("00B050"), tooltip: gt("Green") },
            { name: "light-blue",  color: Color.fromRgb("00B0F0"), tooltip: gt("Light blue") },
            { name: "blue",        color: Color.fromRgb("0070C0"), tooltip: gt("Blue") },
            { name: "dark-blue",   color: Color.fromRgb("002060"), tooltip: gt("Dark blue") },
            { name: "purple",      color: Color.fromRgb("7030A0"), tooltip: gt("Purple") }
        ];

        // definitions for scheme color table
        // "shades" arrays: 0 = pure color (MUST exist once per array!); negative values = darken in %; positive values = lighten in %
        const SCHEME_COLORS = [
            { name: "background1", tooltip: getFillColorName(1),   shades: [0,  -5, -15, -25, -35, -50] },
            { name: "text1",       tooltip: getTextColorName(1),   shades: [50, 35,  25,  15,   5,   0] },
            { name: "background2", tooltip: getFillColorName(2),   shades: [0, -10, -25, -50, -75, -90] },
            { name: "text2",       tooltip: getTextColorName(2),   shades: [80, 60,  40,   0, -25, -50] },
            { name: "accent1",     tooltip: getAccentColorName(1), shades: [80, 60,  40,   0, -25, -50] },
            { name: "accent2",     tooltip: getAccentColorName(2), shades: [80, 60,  40,   0, -25, -50] },
            { name: "accent3",     tooltip: getAccentColorName(3), shades: [80, 60,  40,   0, -25, -50] },
            { name: "accent4",     tooltip: getAccentColorName(4), shades: [80, 60,  40,   0, -25, -50] },
            { name: "accent5",     tooltip: getAccentColorName(5), shades: [80, 60,  40,   0, -25, -50] },
            { name: "accent6",     tooltip: getAccentColorName(6), shades: [80, 60,  40,   0, -25, -50] }
        ];

        // public constants
        this.SCHEME_SIZE = SCHEME_COLORS.length;
        this.SHADE_COUNT = SCHEME_COLORS[0].shades.length;

        // option button configurations for scheme colors
        this.schemeColors = [];
        // option button configurations for base scheme colors (unshaded)
        this.baseSchemeColors = [];

        // generate all shaded scheme colors
        SCHEME_COLORS.forEach((scheme, schemeIdx) => {
            scheme.shades.forEach((shade, shadeIdx) => {

                // the color (will be transformed according to shade)
                const color = Color.fromScheme(scheme.name);
                // the serialized name of the color for automated testing, and tooltip
                let { name, tooltip } = scheme;

                if (shade > 0) {
                    color.transform("lumMod", (100 - shade) * 1000).transform("lumOff", shade * 1000);
                    name += `-lighter${shade}`;
                    //#. The full name of a light theme color (a base color lightened by a specific percentage value)
                    //#. Example result: "Green, lighter 20%"
                    //#. %1$s is the name of the base color
                    //#. %2$d is the percentage value, followed by a literal percent sign
                    tooltip = gt("%1$s, lighter %2$d%", tooltip, shade);
                } else if (shade < 0) {
                    shade = Math.abs(shade);
                    color.transform("lumMod", (100 - shade) * 1000);
                    name += `-darker${shade}`;
                    //#. The full name of a dark theme color (a base color darkened by a specific percentage value)
                    //#. Example result: "Green, darker 20%"
                    //#. %1$s is the name of the base color
                    //#. %2$d is the percentage value, followed by a literal percent sign
                    tooltip = gt("%1$s, darker %2$d%", tooltip, shade);
                }

                // sorting index:
                // tabular mode: first by shade, *then* by scheme index, to keep scheme colors in columns
                // linear mode: first by scheme index, *then* by shade, to keep all shades of a scheme color together
                const sortIndex = {
                    tabular: shadeIdx * this.SCHEME_SIZE + schemeIdx,
                    linear: schemeIdx * this.SHADE_COUNT + shadeIdx
                };

                // create the option button configuration
                const entry = { name, color, tooltip, sortIndex };
                this.schemeColors.push(entry);

                // register base scheme colors
                if (shade === 0) {
                    this.baseSchemeColors.push(entry);
                }
            });
        });

        // sorter functions for option buttons
        this.tabularSorter = (idx1, idx2) => math.compare(idx1.tabular, idx2.tabular);
        this.linearSorter = (idx1, idx2) => math.compare(idx1.linear, idx2.linear);

        // map to convert color operation strings to color names
        this._colorToNameMap = new Map/*<string, string>*/();

        // register color names for button value serialization
        const registerColorName = entry => this._colorToNameMap.set(entry.color.toOpStr(), entry.name);
        this.standardColors.forEach(registerColorName);
        this.schemeColors.forEach(registerColorName);
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed color to a string to be used as serialized control
     * value for automated tests.
     */
    serializeColor(opColor) {
        const colorStr = Color.parseJSON(opColor).toOpStr();
        return this._colorToNameMap.get(colorStr) || colorStr;
    }
}

// static functions -----------------------------------------------------------

PresetColorRegistry.get = fun.once(() => new PresetColorRegistry());

// class ColorPicker ==========================================================

/**
 * Creates a control with a dropdown menu used to choose a color from a set of
 * color buttons. Shows a selection of standard colors, and a table of shaded
 * scheme colors from the current document theme. It has two display modes. One
 * used on small devices with a colorslider to browse through the scheme colors
 * and a mode for all other devices, which has a color table to display all
 * scheme colors.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 *
 * @param {object} [config]
 *  Optional parameters. Supports all options of the RadioList base class. The
 *  option "splitValue" may be set to a JSON color definition to set this color
 *  picker to sticky mode: The split button will always contain the last color
 *  that has been selected in the dropdown menu.
 *
 *  The following additional options are supported:
 *  - {AutoColorType|null} [config.autoColorType=null]
 *    The color type used to resolve the automatic color entry. If set to
 *    `null` or omitted, the button for the automatic color will not be created
 *    at all.
 */
export class ColorPicker extends AppRadioList {

    constructor(docView, config) {

        // registry singleton for all predefined colors
        const registry = PresetColorRegistry.get();

        // base constructor
        super(docView, extendOptions({
            title: config?.label || config?.tooltip,
            coverAnchor: true,
            listLayout: "grid",
            gridColumns: SMALL_COLOR_PICKER ? registry.SHADE_COUNT : registry.SCHEME_SIZE,
            updateCaptionMode: "none",
            matcher: (opColor1, opColor2) => this._sameColors(opColor1, opColor2),
            updateSplitValue: true, // DOCS-1103: sticky color mode (split button remembers last selected color)
            valueSerializer: opColor => registry.serializeColor(opColor),
            dropDownVersion: { label: config?.tooltip }
        }, config));

        // registry for all predefined colors
        this._registry = registry;
        // the theme model that has been used to create the scheme color table
        this._themeModel = this.docModel.getThemeModel();
        // the color box in the dropdown button
        this._colorBox = createDiv("color-preview-box");
        // the replacement color for the automatic color entry
        this._autoColor = Color.auto();

        // add marker class for additional CSS formatting
        this.menu.$el.addClass("color-picker").toggleClass("large-buttons", SMALL_COLOR_PICKER);

        // add the color preview box to the split button or menu button
        this.$caption.after(this._colorBox);

        // refresh color buttons in the dropdown menu after theme has changed
        this.menu.requestMenuRepaintOnAllEvents(this.docModel.themeCollection);

        // update color buttons after the current theme has changed
        this.listenTo(this.docModel, "update:targetchain", () => {
            const newThemeModel = this.docModel.getThemeModel();
            if (this._themeModel !== newThemeModel) {
                this._themeModel = newThemeModel;
                this.menu.requestMenuRepaint();
                this.refresh();
            }
        });

        // initialize the scheme color slider
        if (SMALL_COLOR_PICKER) {

            // the root container of the color slider
            this._sliderRoot = createDiv("color-slider");

            // initialize the entries for the color slider
            const sliderBoxes = this._sliderRoot.appendChild(createDiv("color-slider-boxes"));
            this._sliderEntries = registry.baseSchemeColors.map(entry => {
                const el = sliderBoxes.appendChild(createDiv({ classes: "color-slider-box", tooltip: entry.tooltip }));
                return { ...entry, el };
            });

            // the slider element over the buttons to be tracked
            const sliderTracker = this._sliderRoot.appendChild(createDiv("color-slider-tracker"));

            // update slider tracker when scrolling the color buttons
            this.listenTo(this.menu, "list:section:add", section => {
                if (section.id === "scheme") {
                    this.listenTo(section.$container[0], "scroll", () => {
                        const index = Math.round(section.getGridSettings().first / registry.SHADE_COUNT);
                        const offset = this._sliderEntries[index].el.offsetLeft;
                        sliderTracker.style.left = `${offset}px`;
                    }, { passive: true });
                }
            });

            // initialize tracking observer for the slider
            const observer = this.member(new TrackingObserver({
                start: "move",
                move: record => {
                    const offset = record.point.x - this._sliderRoot.getBoundingClientRect().left;
                    const index = Math.floor(offset / getExactNodeSize(this._sliderEntries[0].el).width);
                    const entry = this._sliderEntries[math.clamp(index, 0, this._sliderEntries.length - 1)];
                    this._scrollSchemeColors(entry.name);
                }
            }));

            // start observing the slider element
            observer.observe(this._sliderRoot);

            // scroll to the current scheme color when opening the menu
            this.listenTo(this.menu, "popup:show", () => {
                const opColor = this.getValue();
                const colorId = (opColor?.type === "scheme") ? normalizeSchemeColorId(opColor.value) : "accent1";
                this._scrollSchemeColors(colorId);
            });
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets the current color value (removes all "alpha" transformations from
     * the passed color).
     *
     * @param {OpColor} opColor
     *  The new JSON color value.
     */
    /*override*/ setValue(opColor) {
        super.setValue(Color.parseJSON(opColor).opaque().toJSON());
    }

    /**
     * Sets the current color for the split button (removes all "alpha"
     * transformations from the passed color).
     *
     * @param {OpColor} opColor
     *  The new JSON color for the split button.
     */
    /*override*/ setSplitValue(opColor) {
        super.setSplitValue(Color.parseJSON(opColor).opaque().toJSON());
    }

    /**
     * Sets the specified color to render the color box in the option button
     * for the automatic color, and repaints that option button.
     *
     * @param {Color} color
     *  The new replacement color for the automatic color entry.
     */
    setAutoColor(color) {
        this._autoColor = color;
        this._setBackgroundColor(this.menu.findOptions(Color.AUTO).find(".color-box"), Color.AUTO);
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the color box in the dropdown menu button.
     */
    /*protected override*/ implUpdate(opColor) {
        super.implUpdate(opColor);
        // split mode: do not use the current control value, but the cached (sticky) split value
        this._setBackgroundColor(this._colorBox, this.getSplitValue() || opColor);
    }

    /**
     * Inserts all available colors into the dropdown menu.
     */
    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // remove all color buttons
        this.clearOptions();

        // add automatic color
        if (this.config.autoColorType) {

            const label = (this._resolveColor(Color.AUTO).a === 0) ?
                /*#. no fill color, transparent */ gt("No color") :
                /*#. automatic text color (white on dark backgrounds, otherwise black) */ gt("Automatic color");

            this.addSection("auto", { gridColumns: 1 });
            this._createColorButton({ color: Color.auto(), label });
        }

        // section order according to the device type
        if (SMALL_COLOR_PICKER) {
            this._createStandardColors();
            this._createSchemeColors();
            this._refreshSchemeSlider();
        } else {
            this._createSchemeColors();
            this._createStandardColors();
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Resolves the passed JSON color according to the current theme target
     * chain.
     *
     * @param {OpColor} opColor
     *  The JSON color to be resolved.
     *
     * @returns {ColorDescriptor}
     *  The descriptor for the resolved color.
     */
    /*private*/ _resolveColor(opColor) {
        const { autoColorType } = this.config;
        const autoColor = (autoColorType && this._autoColor.isAuto()) ? autoColorType : this._autoColor;
        return Color.parseJSON(opColor).resolve(autoColor, this._themeModel);
    }

    /**
     * Sets the passed JSON color as background color at the specified DOM
     * color box node.
     *
     * @param {NodeOrJQuery} colorBox
     *  The DOM node of a color box.
     *
     * @param {OpColor} opColor
     *  The JSON representation of the color to be set as background color,
     *  as used in document operations.
     */
    /*private*/ _setBackgroundColor(colorBox, opColor) {
        const node = toNode(colorBox);
        if (node) { node.style.background = this._resolveColor(opColor).css; }
    }

    // same result color but create with different transformations
    /*private*/ _sameHSL(opColor1, opColor2) {
        const hsl1 = this._resolveColor(opColor1).hsl;
        const hsl2 = this._resolveColor(opColor2).hsl;
        const dist = Math.abs(hsl1.h - hsl2.h) + Math.abs(hsl1.s - hsl2.s) + Math.abs(hsl1.l - hsl2.l); // ignoring alpha channel, #48333
        return dist < 0.002;
    }

    /**
     * Returns whether the passed colors are considered being equal.
     */
    /*private*/ _sameColors(opColor1, opColor2) {
        return (opColor1 === opColor2) || (equalTypeAndValue(opColor1, opColor2) && (equalTransformations(opColor1, opColor2) || this._sameHSL(opColor1, opColor2)));
    }

    /**
     * Creates an option buton with an additional color box element showing the
     * specified color.
     */
    /*private*/ _createColorButton(entry) {
        const opColor = entry.color.toJSON();
        if (opColor.type === "scheme") { opColor.fallbackValue = this._resolveColor(opColor).hex; }
        const colorBox = createSpan("color-box");
        this._setBackgroundColor(colorBox, opColor);
        this.addOption(opColor, entry).prepend(colorBox);
    }

    /**
     * Creates all option buttons for the standard colors.
     */
    /*private*/ _createStandardColors() {
        this.addSection("standard", gt("Standard Colors"));
        for (const entry of this._registry.standardColors) {
            this._createColorButton(entry);
        }
    }

    /**
     * Creates all option buttons for the scheme colors (base colors and shaded
     * colors).
     */
    /*private*/ _createSchemeColors() {
        const sorted = SMALL_COLOR_PICKER ? this._registry.linearSorter : this._registry.tabularSorter;
        this.addSection("scheme", { label: gt("Theme Colors"), sorted, scrollRow: SMALL_COLOR_PICKER });
        for (const entry of this._registry.schemeColors) {
            this._createColorButton(entry);
        }
    }

    /**
     * Scrolls to the specified block of scheme color buttons.
     */
    /*private*/ _scrollSchemeColors(colorId) {

        // find the scheme index of the specified scheme color
        colorId = normalizeSchemeColorId(colorId);
        const index = this._sliderEntries.findIndex(entry => entry.name === colorId);
        if (index < 0) { return; }

        // scroll the button container to the scheme color block
        // (slider will be updated by scroll event handler)
        const section = this.menu.getSection("scheme");
        section.scrollToOption(this._registry.SHADE_COUNT * index);
    }

    /**
     * Refreshes all color slider buttons.
     */
    /*private*/ _refreshSchemeSlider() {

        // insert the slider into the "scheme" section of the dropdown menu
        const section = this.menu.getSection("scheme");
        section.$el.append(this._sliderRoot);

        // calculate the RGB colors for the current document theme
        for (const entry of this._sliderEntries) {
            this._setBackgroundColor(entry.el, entry.color.toJSON());
        }
    }
}
