/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { is, math } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE, KeyCode } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { Canvas } from "@/io.ox/office/tk/canvas";
import { getNodePositionInPage } from "@/io.ox/office/tk/utils";
import { createButtonNode, setButtonKeyHandler, setToolTip } from "@/io.ox/office/tk/forms";
import { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";
import { ToolTip } from "@/io.ox/office/tk/popup/tooltip";
import { CaptionControl } from "@/io.ox/office/tk/control/captioncontrol";
import { XMenuControl } from "@/io.ox/office/tk/control/xmenucontrol";

// constants ==================================================================

// width of a single grid cell, in pixels
const CELL_WIDTH = TOUCH_DEVICE ? 27 : 18;

// height of a single grid cell, in pixels
const CELL_HEIGHT = TOUCH_DEVICE ? 24 : 16;

// class TableSizeToolTip =====================================================

/**
 * A tooltip showing the current table size for a `TableSizePicker` control.
 */
class TableSizeToolTip extends ToolTip {

    constructor(anchorMenu) {

        super({ anchor: anchorMenu.$el });

        this.anchorMenu = anchorMenu;

        // ARIA additions
        this.$el.attr({ id: this.uid, "aria-live": "assertive", "aria-relevant": "additions", "aria-atomic": true, role: "tooltip" });
    }

    /**
     * Dynamically calculates the position and alignment of the grid size
     * tooltip according to the growing direction of the parent popup node.
     */
    /*protected override*/ implPrepareLayout() {
        const { growTop, growLeft } = this.anchorMenu.getGrowDirections();
        const anchorBorder = growTop ? "bottom" : "top";
        const anchorAlign = growLeft ? "trailing" : "leading";
        return { anchorBorder, anchorAlign };
    }
}

// class TableSizeMenu ========================================================

/**
 * The popup menu element for a `TableSizePicker` control.
 *
 * @param {number} maxCols
 *  The maximal number of columns to be displayed in the popup menu.
 *
 * @param {number} maxRows
 *  The maximal number of rows to be displayed in the popup menu.
 */
class TableSizeMenu extends BaseMenu {

    constructor(maxCols, maxRows) {

        // base constructor
        super({ displayType: "blocking" });

        // additional CSS marker class
        this.$el.addClass("table-size-picker");

        // minimum/maximum size allowed to choose
        this._MIN_SIZE = { width: 1, height: 1 };
        this._MAX_SIZE = { width: maxCols, height: maxRows };
        // minimum size visible even if selected size is smaller
        this._MIN_VIEW_SIZE = { width: Math.min(this._MAX_SIZE.width, 6), height: Math.min(this._MAX_SIZE.height, 4) };

        // maximum grid size, according to options, and screen size
        this._maxGridSize = { ...this._MAX_SIZE };
        // current grid size
        this._gridSize = { ...this._MIN_SIZE };
        // whether to grow to the left or right or to the top or bottom
        this._growTop = false;
        this._growLeft = false;

        // the button control to be inserted into the dropdown menu
        // since "aria-label" would override "aria-describedby" and title is used as native tooltip, so nothing is set
        this._$gridButton = createButtonNode({ role: "menuitem" });
        // canvas element for rendering the grid lines
        this._canvas = this.member(new Canvas());
        // the tooltip node showing the current grid size
        this._sizeToolTip = this.member(new TableSizeToolTip(this));

        // current position of mouse pointer (page coordinates)
        this._mousePos = null;
        // handling an "Enter", if the table size picker was prepared with mouse movements
        this._isSimulatedClick = false;

        // embed the button control in the dropdown menu
        this.$body.append(this._$gridButton.append(this._canvas.el));

        // ARIA additions
        this._$gridButton.attr("aria-describedby", this._sizeToolTip.uid);

        // button click handler (convert ENTER, SPACE, and TAB keys to click events)
        setButtonKeyHandler(this._$gridButton, { tab: true });
        this._$gridButton.on("click", event => {
            // bug 31170: restrict to "click" events that originate from keyboard shortcuts
            // (prevent double trigger from "end" tracking notification and "click" events)
            if (is.number(event.keyCode) || this._isSimulatedClick) {
                this.trigger("commit", this._gridSize, { sourceEvent: event });
            }
        });

        // register event handlers
        this.on("popup:beforeshow", this._popupBeforeShowHandler);
        this.on("popup:show", this._popupShowHandler);
        this.on("popup:hide", this._popupHideHandler);

        // inclusive "start" for the first touch on mobile
        this.listenTo(this.$body, "keydown", this._gridKeyDownHandler);

        // initialize tracking observer for the grid area
        const observer = this.member(new TrackingObserver({
            start: "move",
            move: record => {
                this._mousePos = record.point;
                this.refresh({ immediate: true });
            },
            end: record => {
                this.trigger("commit", this._gridSize, { sourceEvent: record.inputEvent });
            }
        }));

        // start observing the grid area
        observer.observe(this._$gridButton[0]);
    }

    destructor() {
        this._unbindGlobalEventHandlers();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the directions where to grow the popup menu to, according to the
     * mouse pointer position.
     *
     * @returns {object}
     *  An object with the boolean flags "growTop" and  "growLeft".
     */
    getGrowDirections() {
        return { growTop: this._growTop, growLeft: this._growLeft };
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the size of the table grid according to the available space
     * around this form control.
     */
    /*protected override*/ implPrepareLayout(anchorPos, availableSizes) {

        // decide where to grow the grid to
        const growTop = this._growTop = (availableSizes.bottom.height < 200) && (availableSizes.top.height > availableSizes.bottom.height);
        const growLeft = this._growLeft = availableSizes.left.width > availableSizes.right.width;

        // calculate maximum allowed size of the grid according to available space in the window
        const maxGridSize = this._maxGridSize;
        maxGridSize.width = Math.min(this._MAX_SIZE.width, Math.floor((anchorPos.width + (growLeft ? availableSizes.left.width : availableSizes.right.width) - 10) / CELL_WIDTH));
        maxGridSize.height = Math.min(this._MAX_SIZE.height, Math.floor(((growTop ? availableSizes.top.height : availableSizes.bottom.height) - 10) / CELL_HEIGHT));

        // update grid size, if a mouse position is available
        if (this._mousePos) {

            // position/size of the grid
            const gridPosition = getNodePositionInPage(this._canvas.el);
            // mouse position relative to grid origin
            const mouseRelX = growLeft ? (gridPosition.left + gridPosition.width - this._mousePos.x) : (this._mousePos.x - gridPosition.left);
            const mouseRelY = growTop ? (gridPosition.top + gridPosition.height - this._mousePos.y) : (this._mousePos.y - gridPosition.top);

            // enlarge grid if the last column/row is covered more than 80% of its width/height
            this._gridSize = { width: Math.floor(mouseRelX / CELL_WIDTH + 1.2), height: Math.floor(mouseRelY / CELL_HEIGHT + 1.2) };
        }

        // validate grid size according to current limits
        const gridSize = this._gridSize;
        gridSize.width = math.clamp(gridSize.width, this._MIN_SIZE.width, maxGridSize.width);
        gridSize.height = math.clamp(gridSize.height, this._MIN_SIZE.height, maxGridSize.height);

        // initialize size of the canvas element (clears the canvas implicitly)
        this._canvas.initialize({
            width: Math.max(gridSize.width, this._MIN_VIEW_SIZE.width) * CELL_WIDTH + 1,
            height: Math.max(gridSize.height, this._MIN_VIEW_SIZE.height) * CELL_HEIGHT + 1
        });

        // render the grid
        this._canvas.render((context, width, height) => {

            // the pixel size of the highlighted area
            const highlightWidth = gridSize.width * CELL_WIDTH + 1;
            const highlightHeight = gridSize.height * CELL_HEIGHT + 1;
            // a path object containing all line segments of the grid
            const path = context.createPath();

            // render the filled area
            context.setFillStyle("#c8c8c8");
            context.drawRect(growLeft ? (width - highlightWidth) : 0, growTop ? (height - highlightHeight) : 0, highlightWidth, highlightHeight, "fill");

            // render the lines
            for (let x = 0; x <= width; x += CELL_WIDTH) { path.pushLine(x, 0, x, height); }
            for (let y = 0; y <= height; y += CELL_HEIGHT) { path.pushLine(0, y, width, y); }
            context.setLineStyle({ style: "black", width: 1 });
            context.setGlobalAlpha(0.1);
            context.translate(0.5, 0.5);
            context.drawPath(path, "stroke");
        });

        // update the tooltip
        this._sizeToolTip.setText(
            //#. Tooltip for an "Insert table" toolbar control that shows the size of a table that
            //#. will be inserted into a document, while moving with the mouse over a preview grid.
            //#. %1$s is the placeholder for the translated text "M column(s)"
            //#. %2$s is the placeholder for the translated text "N row(s)"
            gt("Table size %1$s \xd7 %2$s",
                //#. Tooltip for an "Insert table" toolbar control
                //#. %1$d is the number of columns currently selected with the mouse
                gt.ngettext("%1$d column", "%1$d columns", gridSize.width, gridSize.width),
                //#. Tooltip for an "Insert table" toolbar control
                //#. %1$d is the number of rows currently selected with the mouse
                gt.ngettext("%1$d row", "%1$d rows", gridSize.height, gridSize.height)
            )
        );

        // return precalculated position and alignment to the anchor
        return {
            anchorBorder: growTop ? "top" : "bottom",
            anchorAlign: growLeft ? "trailing" : "leading"
        };
    }

    // private methods --------------------------------------------------------

    /**
     * Handles "mousemove" events in the open menu element.
     */
    /*private*/ _gridMouseMoveHandler(event) {
        this._mousePos = { x: event.pageX, y: event.pageY };
        this.refresh({ immediate: true });
        return false;
    }

    /**
     * Handles keyboard events in the open menu element.
     */
    /*private*/ _gridKeyDownHandler(event) {

        // update the grid size according to the pressed cursor key
        switch (event.keyCode) {
            case KeyCode.LEFT_ARROW:
                this._gridSize.width += (this._growLeft ? 1 : -1);
                break;
            case KeyCode.UP_ARROW:
                this._gridSize.height += (this._growTop ? 1 : -1);
                break;
            case KeyCode.RIGHT_ARROW:
                this._gridSize.width += (this._growLeft ? -1 : 1);
                break;
            case KeyCode.DOWN_ARROW:
                this._gridSize.height += (this._growTop ? -1 : 1);
                break;
            case KeyCode.ENTER:
                this._isSimulatedClick = true;
                this._$gridButton.click(); // pressing "Enter" after opening table size picker with mouse
                break;
            default:
                return;
        }

        // clear last cached mouse position when using cursor keys, repaint the grid
        this._mousePos = null;
        this.refresh({ immediate: true });
        return false;
    }

    /**
     * Unbind the global event listener registered at the document.
     */
    /*private*/ _unbindGlobalEventHandlers() {
        this.stopListeningTo(document);
    }

    /**
     * Handles "popup:beforeshow" events and initializes the dropdown grid.
     * Registers a "mouseenter" handler at the menu element that starts a
     * "mousemove" listener when the mouse will hover the grid element the
     * first time.
     */
    /*private*/ _popupBeforeShowHandler() {

        // unbind all running global event handlers
        this._unbindGlobalEventHandlers();

        // initialize and render the grid node
        this._gridSize = { ...this._MIN_SIZE };
        this._mousePos = null;

        // wait for mouse to enter the grid before listening globally to mousemove events
        this._$gridButton.off("mouseenter").one("mouseenter", () => {
            this.listenTo(document, "mousemove", this._gridMouseMoveHandler);
        });
    }

    /**
     * Handles "popup:show" events, shows the tooltip node.
     */
    /*private*/ _popupShowHandler() {
        this._sizeToolTip.show();
    }

    /**
     * Handles "popup:hide" events.
     */
    /*private*/ _popupHideHandler() {
        this._sizeToolTip.hide();
        this._unbindGlobalEventHandlers();
    }
}

// class TableSizePicker ======================================================

/**
 * A dropdown button and a dropdown menu containing a resizable grid allowing
 * to select a specific size.
 *
 * @param {number} maxCols
 *  The maximal number of columns to be displayed in the control.
 *
 * @param {number} maxRows
 *  The maximal number of rows to be displayed in the control.
 */
export class TableSizePicker extends XMenuControl.mixin(CaptionControl) {

    constructor(_docView, maxCols, maxRows) {

        // the dropdown anchor button
        const $button = createButtonNode({
            icon: "png:table-insert",
            label: /*#. a table in a text document */ gt.pgettext("text-doc", "Table")
        });

        // base constructor (`XMenuControl` mixin class expects button and menu instance)
        super($button, new TableSizeMenu(maxCols, maxRows));

        // set own root element as anchor for the dropdown menu
        this.menu.setAnchor(this.$el);

        // ARIA additions
        $button.attr("aria-haspopup", true);
        setToolTip($button, /*#. insert a table in a text document */ gt.pgettext("text-doc", "Insert a table"));

        // add the dropdown button to the group
        this.$el.append($button);

        // commit grid size triggered from popup menu
        this.menu.on("commit", (gridSize, options) => {
            this.triggerCommit(gridSize, options);
        });
    }
}
