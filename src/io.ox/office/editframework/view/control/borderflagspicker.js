/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { is, dict } from "@/io.ox/office/tk/algorithms";
import { CheckBox } from "@/io.ox/office/tk/control/checkbox";

import { CompoundButton } from "@/io.ox/office/baseframework/view/control/compoundbutton";

import * as Labels from "@/io.ox/office/editframework/view/editlabels";

// constants ==================================================================

// settings for all border buttons in toggle mode (property `name` is used in automated tests!)
const TOGGLE_BORDER_ENTRIES = [
    { name: "left",    keys: "l", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Left border") },
    { name: "right",   keys: "r", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Right border") },
    { name: "top",     keys: "t", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Top border") },
    { name: "bottom",  keys: "b", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Bottom border") },
    { name: "insidev", keys: "v", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Inner vertical borders") },
    { name: "insideh", keys: "h", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Inner horizontal borders") }
];

// settings for all border buttons in preset mode (property `name` is used in automated tests!)
const PRESET_BORDER_ENTRIES = [
    { name: "none",               keys: "",       label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "No borders") },
    { name: "all",                keys: "lrtbvh", label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "All borders") },
    { name: "outer",              keys: "lrtb",   label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Outer borders") },
    { name: "inner",              keys: "vh",     label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Inner borders") },
    { name: "left-right",         keys: "lr",     label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Left and right borders") },
    { name: "top-bottom",         keys: "tb",     label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Top and bottom borders") },
    { name: "left-right-insidev", keys: "lrv",    label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "All vertical borders") },
    { name: "top-bottom-insideh", keys: "tbh",    label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "All horizontal borders") },
    { name: "outer-insidev",      keys: "lrtbv",  label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Inner vertical and outer borders") },
    { name: "outer-insideh",      keys: "lrtbh",  label: /*#. in paragraphs and table cells */ gt.pgettext("borders", "Inner horizontal and outer borders") }
];

// private functions ==========================================================

/**
 * Returns the CSS class name of the icon representing the passed border keys.
 */
function getBorderIconClass(keys) {
    const iconClass = "tbhlrv".split("").filter(key => keys.includes(key)).join("");
    return `png:border${iconClass ? `-${iconClass}` : ""}`;
}

// class BorderFlagsPicker ====================================================

/**
 * A drop-down list control with different settings for single or multiple
 * components supporting outer and inner border lines. The list entries are
 * divided into two sections. The first section contains entries for single
 * border lines that can be toggled. The second section contains list entries
 * for complex border settings for multiple component borders.
 *
 * The method `setValue()` expects flag set values of type
 * `PtRecord<BorderKey,boolean|null>` specifying the visibility state of the
 * respective border line. The value `null` represents an ambiguous state for
 * the border line.
 *
 * @param {object} [config]
 *  Optional parameters. Supports all options of the base class
 *  `CompoundButton`. Additionally, the following options are supported:
 *  - {boolean|()=>boolean} [config.showInnerH=false]
 *    If set to `true`, the drop-down list will provide list entries for
 *    horizontal inner borders between multiple components that are stacked
 *    vertically. If set to a function, the visibility of the affected list
 *    items will be evaluated dynamically every time this control will be
 *    updated, according to the boolean return value.
 *  - {boolean|()=>boolean} [config.showInnerV=false]
 *    If set to `true`, the drop-down list will provide list entries for
 *    vertical inner borders between multiple components that are located
 *    side-by-side. If set to a function, the visibility of the affected
 *    list items will be evaluated dynamically every time this control will
 *    be updated, according to the boolean return value.
 */
export class BorderFlagsPicker extends CompoundButton {

    constructor(docView, config) {

        // the tooltip to be shown as drop-down version label too
        const tooltip = config?.tooltip || /*#. in paragraphs and tables cells */ gt.pgettext("borders", "Borders");

        // base constructor
        super(docView, {
            icon: "png:border-tblr",
            cancelButton: Labels.CLOSE_LABEL,
            focusableNodes: ".io-ox-office-main.window-blocker",
            skipStateAttr: true,
            dropDownVersion: { label: tooltip },
            ...config,
            tooltip,
            repaintAlways: true // always repaint dropdown menu when opening
        });

        // all checkboxes and their border keys
        this._allMenuEntries = [];
        // cached border flags from update handler for partial updates
        this._cachedBorderFlags = {};
        // cached border flags for deferred execution of multiple fast clicks
        this._waitingBorderFlags = {};
        // cached border flags while executing a click
        this._runningBorderFlags = {};
        // cached promise for deferred execution of multiple fast clicks
        this._pendingPromise = null;
        // whether the dropdown menu has been painted
        this._menuPainted = false;
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the border icon in the menu button, and the value and visibility
     * of the checkboxes.
     */
    /*protected override*/ implUpdate(borderFlags) {
        super.implUpdate(borderFlags);
        // update the cached border flags after partial updates triggered by the checkboxes
        Object.assign(this._cachedBorderFlags, borderFlags);
        // update the border icon in the upper menu button
        const keys = this._getActiveBorderKeys().filter(key => this._cachedBorderFlags[key]);
        this.setIcon(getBorderIconClass(keys));
        // update the "data-state" attribute manually
        this.$el.attr("data-state", keys.join(""));
        // update visibility and value of all checkboxes
        this._updateAllCheckBoxes();
    }

    /*protected override*/ implRepaintMenu() {
        super.implRepaintMenu();

        // create the menu entries once
        if (!this._menuPainted) {
            this._menuPainted = true;

            // create the single-border toggle buttons
            this.addSection("toggle");
            this._createCheckBoxes(TOGGLE_BORDER_ENTRIES, true);

            // create the preset border buttons
            this.addSection("preset");
            this._createCheckBoxes(PRESET_BORDER_ENTRIES, false);
        }

        // always update visibility of the checkboxes when opening the menu
        this._updateAllCheckBoxes();
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether to currently use inner horizontal border lines.
     */
    /*private*/ _useInnerHBorder() {
        const { showInnerH } = this.config;
        return (showInnerH === true) || (is.function(showInnerH) && showInnerH());
    }

    /**
     * Returns whether to currently use inner vertical border lines.
     */
    /*private*/ _useInnerVBorder() {
        const { showInnerV } = this.config;
        return (showInnerV === true) || (is.function(showInnerV) && showInnerV());
    }

    /**
     * Returns the keys of all active borders. Inner border lines may be
     * deactivated dynamically or permanently.
     */
    /*private*/ _getActiveBorderKeys() {
        const keys = ["t", "b", "l", "r"];
        if (this._useInnerHBorder()) { keys.push("h"); }
        if (this._useInnerVBorder()) { keys.push("v"); }
        return keys;
    }

    /**
     * Returns a flag set with all active borders.
     *
     * @param {BorderKey[]} keys
     *  The keys of all borders to be set to `true` in the returned flag
     *  set. All other borders will be set to `false`.
     */
    /*private*/ _createBorderFlags(keys) {
        return dict.generate(this._getActiveBorderKeys(), key => keys.includes(key));
    }

    /*private*/ _getControllerPromise() {
        return this.docApp.docController.getRunningItemPromise();
    }

    /**
     * Triggers the commit event for the passed border flags. If the last
     * commit is still running, the flags will be collected in a map, and
     * will be committed later when current processing is finished.
     */
    /*private*/ _triggerBorders(sourceEvent, borderFlags) {

        // collect the passed forder flags
        Object.assign(this._waitingBorderFlags, borderFlags);
        if (this._pendingPromise) {
            this._updateAllCheckBoxes();
            return;
        }

        // wait for ruuning controller item (prevent recursive call errors)
        this._pendingPromise = this._getControllerPromise();
        this.onFulfilled(this._pendingPromise, () => {
            this._runningBorderFlags = this._waitingBorderFlags;
            this._waitingBorderFlags = {};
            this._pendingPromise = null;
            this.triggerCommit(this._runningBorderFlags, {
                sourceEvent,
                // keep focus in the drop-down menu
                focusTarget: this.menu.$body
            });
            this.onFulfilled(this._getControllerPromise(), () => {
                this._runningBorderFlags = {};
                this._updateAllCheckBoxes();
            });
        });
    }

    /**
     * Creates a checkbox for each entry in the passed configuration list.
     */
    /*private*/ _createCheckBoxes(settingsList, toggle) {
        settingsList.forEach(settings => {

            // the list of all border names covered by the checkbox
            const keys = settings.keys.split("");

            // do not create a checkbox, if inner border lines will never be shown
            const isInnerH = keys.includes("h");
            if (isInnerH && !this.config.showInnerH) { return; }
            const isInnerV = keys.includes("v");
            if (isInnerV && !this.config.showInnerV) { return; }

            // create and insert the checkbox
            const checkBox = this.addControl(null, new CheckBox({
                icon: getBorderIconClass(keys),
                label: settings.label,
                value: settings.name // creates "data-value" attribute for automated testing
            }));

            // ignore double clicks on the checkbox
            checkBox.$el.on("dblclick", false);

            // forward all commit events to the compound control, but consolidate fast clicks while preceding click is processing
            checkBox.on("group:commit", (state, options) => {
                this._triggerBorders(options.sourceEvent, toggle ? dict.fill(keys, state) : this._createBorderFlags(keys));
            });

            // store all data for the checkbox in the list (for update handler)
            this._allMenuEntries.push({ checkBox, keys, toggle, isInnerH, isInnerV });
        });
    }

    /**
     * Updates the value and visibility state of all checkboxes in the
     * drop-down menu.
     */
    /*private*/ _updateAllCheckBoxes() {

        // whether the inner borders are currently available (according to predicate functions)
        const useInnerH = this._useInnerHBorder();
        const useInnerV = this._useInnerVBorder();
        // current borders to be visualized (use local pending borders to prevent flickering)
        const borderFlags = { ...this._cachedBorderFlags, ...this._runningBorderFlags, ...this._waitingBorderFlags };

        // update all checkboxes in the drop-down menu
        this._allMenuEntries.forEach(menuEntry => {

            // update visibility according to dynamic existence of inner borders
            menuEntry.checkBox.toggle((useInnerH || !menuEntry.isInnerH) && (useInnerV || !menuEntry.isInnerV));

            // update checkbox value (visibility of all borders must match)
            const keys = menuEntry.toggle ? menuEntry.keys : this._getActiveBorderKeys();
            menuEntry.checkBox.setValue(keys.every(key => {
                // DOCS-2547: compound entries must not match `false` with `null` (mixed borders)
                const isVisible = menuEntry.toggle ? !!borderFlags[key] : borderFlags[key];
                const visibleExpected = menuEntry.keys.includes(key);
                return isVisible === visibleExpected;
            }));
        });
    }
}
