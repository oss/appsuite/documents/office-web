/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { isGuest } from "@/io.ox/office/tk/utils/driveutils";

import { Button, CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";

import {
    TEXT_DOCUMENT_NAME, TEXT_DOCUMENT_NAME_ENCRYPTED,
    SPREADSHEET_DOCUMENT_NAME, SPREADSHEET_DOCUMENT_NAME_ENCRYPTED,
    PRESENTATION_DOCUMENT_NAME, PRESENTATION_DOCUMENT_NAME_ENCRYPTED
} from "@/io.ox/office/baseframework/view/baselabels";

import { GUARD_AVAILABLE, PRESENTATION_AVAILABLE, SPREADSHEET_AVAILABLE, TEXT_AVAILABLE } from "@/io.ox/office/editframework/utils/editconfig";

// class NewDocumentButton ====================================================

/**
 * A dropdown button used to create new documents of various types.
 *
 * @param {EditView} docView
 *  The document view instance containing this control.
 */
export class NewDocumentButton extends CompoundButton {

    constructor(docView) {

        // base constructor
        super(docView, { label: gt("New") });

        if (!isGuest()) {
            if (TEXT_AVAILABLE) {
                this.addControl("document/new", new Button({ label: TEXT_DOCUMENT_NAME, value: "text" }));
                if (GUARD_AVAILABLE) {
                    this.addControl("document/new/encrypted", new Button({ label: TEXT_DOCUMENT_NAME_ENCRYPTED, value: "text" }));
                }
            }

            if (SPREADSHEET_AVAILABLE) {
                this.addControl("document/new", new Button({ label: SPREADSHEET_DOCUMENT_NAME, value: "spreadsheet" }));
                if (GUARD_AVAILABLE) {
                    this.addControl("document/new/encrypted", new Button({ label: SPREADSHEET_DOCUMENT_NAME_ENCRYPTED, value: "spreadsheet" }));
                }
            }

            if (PRESENTATION_AVAILABLE) {
                this.addControl("document/new", new Button({ label: PRESENTATION_DOCUMENT_NAME, value: "presentation" }));
                if (GUARD_AVAILABLE) {
                    this.addControl("document/new/encrypted", new Button({ label: PRESENTATION_DOCUMENT_NAME_ENCRYPTED, value: "presentation" }));
                }
            }
        }

        this.addSection("quit");
        this.addControl("app/quit", new Button({ label: gt("Recent documents and templates") }));
    }
}
