/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { ToolPane } from "@/io.ox/office/baseframework/view/pane/toolpane";
import { ToolBar } from '@/io.ox/office/baseframework/view/pane/toolbar';

import { COMBINED_TOOL_PANES } from "@/io.ox/office/editframework/utils/editconfig";
import { QUIT_BUTTON_OPTIONS, UNDO_BUTTON_OPTIONS, REDO_BUTTON_OPTIONS, VIEW_LABEL } from "@/io.ox/office/editframework/view/editlabels";
import { Button, Label, CompoundButton, ApplicationStatusLabel, ToolPaneButtonGroup, ToolPanePicker } from "@/io.ox/office/editframework/view/editcontrols";

import "@/io.ox/office/editframework/view/pane/toppane.less";

// class TopPane ==============================================================

/**
 * Represents the top-level view pane in OX Document applications, containing
 * the file name, the file drop-down menu, and additional global controls such
 * as the Close button and the Undo/Redo buttons.
 *
 * @param {EditView} docView
 *  The document view instance containing this view pane.
 */
export class TopPane extends ToolPane {

    constructor(docView) {

        // base constructor
        super(docView, {
            position: "top",
            classes: "top-pane"
        });

        // public properties --------------------------------------------------

        /**
         * A toolbar in the trailing area of this pane. Custom controls can be
         * inserted into the section "custom" later.
         *
         * @type ToolBar
         */
        this.rightToolBar = new ToolBar(docView);

        /**
         * The tab buttons to activate the toolpanes.
         *
         * @type ToolPaneButtonGroup
         */
        this.toolPaneGroup = new ToolPaneButtonGroup(docView);

        /**
         * The dropdown control to activate the toolpanes.
         *
         * @type ToolPanePicker
         */
        this.toolPanePicker = new ToolPanePicker(docView);

        /**
         * The button with a drop-down menu containing all controls for the
         * view settings of the application.
         *
         * @type CompoundButton
         */
        this.viewButton = new CompoundButton(docView, { label: VIEW_LABEL });

        /**
         * The application status label.
         *
         * @type ApplicationStatusLabel
         */
        this.statusLabel = new ApplicationStatusLabel(docView);

        // initialization -----------------------------------------------------

        // create the toolbar containing the tab buttons for the main toolpane
        const leftToolBar = this.addToolBar(new ToolBar(docView), { targetSection: "leading" });
        leftToolBar.addControl("view/toolbars/tab", this.toolPaneGroup);
        leftToolBar.addControl("view/toolbars/tab", this.toolPanePicker);
        this.toolPanePicker.hide();

        // create the toolbar containing the application status labels
        const centerToolBar = this.addToolBar(new ToolBar(docView), { targetSection: "center" });
        centerToolBar.addControl("app/encrypted", new Label({ icon: "bi:lock-fill", tooltip: gt("Document is encrypted") }), { visibleKey: "app/encrypted" });
        centerToolBar.addControl("view/appstatuslabel", this.statusLabel);

        // center status label while loading a document
        this.listenTo(docView.docApp, "docs:state", state => {
            const center = docView.getAppStateConfig(state)?.center;
            this.$centerSection.css("justify-content", center ? "center" : "");
        });

        // create the left part of the trailing toolbar with the standard controls
        this.addToolBar(this.rightToolBar, { targetSection: "trailing" });
        this.rightToolBar.addSection("common");
        this.rightToolBar.addControl("document/undo", new Button(UNDO_BUTTON_OPTIONS), { visibleKey: "document/editable" });
        this.rightToolBar.addControl("document/redo", new Button(REDO_BUTTON_OPTIONS), { visibleKey: "document/editable" });
        this.rightToolBar.addContainer();
        this.rightToolBar.addControl("view/pane/search", new Button({ icon: "bi:search", tooltip: gt("Toggle search"), toggle: true }), { visibleKey: "app/valid" });
        this.rightToolBar.addSection("custom");
        // section "custom" is intended to be extended by client code
        this.rightToolBar.addSection("view");
        this.rightToolBar.addControl("view/settings/menu", this.viewButton, { visibleKey: "app/valid" });
        this.rightToolBar.addSection("quit");
        this.rightToolBar.addControl("app/quit", new Button(QUIT_BUTTON_OPTIONS), { visibleKey: "view/quit/visible", alwaysVisible: true });

        // hide leading toolbar until document import is finished
        this.$leadingSection.hide();
        this.waitForImport(() => this.$leadingSection.show());

        // initialize all shrink/expand handlers for changed window size
        this._initShrinkHandlers();

        // hide this pane in "combined panes" mode, and in plugged mode (viewer overlay app)
        this.toggle(!COMBINED_TOOL_PANES && !this.docApp.isViewerMode());
    }

    // private methods --------------------------------------------------------

    /**
     * Initializes all shrink/expand handlers for changed window size.
     */
    /*private*/ _initShrinkHandlers() {

        // step 1: distance mode for reduced padding/distances
        this.addAutoFitDistanceSteps();

        // step 2: hide text of application status label (icon only)
        this.addAutoFitStep(shrink => {
            this.statusLabel.toggleShrinkMode(shrink);
        });

        // step 3: switch to dropdown for toolpanes (application status label back to visible)
        this.addAutoFitStep(shrink => {
            this.toolPaneGroup.toggle(!shrink);
            this.toolPanePicker.toggle(shrink);
            this.statusLabel.toggleShrinkMode(!shrink);
        });

        // step 4: hide text of application status label again
        this.addAutoFitStep(shrink => {
            this.statusLabel.toggleShrinkMode(shrink);
        });

        // step 5: hide label of toolpane dropdown list
        this.addAutoFitStep(shrink => {
            this.toolPanePicker.toggleShrinkMode(shrink);
        });
    }
}
