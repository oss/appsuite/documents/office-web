/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ToolBar } from "@/io.ox/office/baseframework/view/pane/toolbar";
import type { DynamicPaneConfig, DynamicToolBarOptions, DynamicPaneIteratorOptions, DynamicToolBarWrapper, DynamicPaneEventMap } from "@/io.ox/office/baseframework/view/pane/dynamicpane";
import { DynamicPane } from "@/io.ox/office/baseframework/view/pane/dynamicpane";

import { COMBINED_TOOL_PANES } from "@/io.ox/office/editframework/utils/editconfig";
import { QUIT_BUTTON_OPTIONS } from "@/io.ox/office/editframework/view/editlabels";
import { Button, ToolPaneCombinedMenu } from "@/io.ox/office/editframework/view/editcontrols";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

import "@/io.ox/office/editframework/view/pane/maintoolpane.less";

// types ======================================================================

/**
 * Type mapping for the events emitted by `MainToolPane` instances.
 */
export interface MainToolPaneEventMap extends DynamicPaneEventMap {

    /**
     * Will be emitted after the toolpane has been refreshed when switching the
     * toolbars.
     */
    "toolpane:refreshed": [];
}

// class MainToolPane =========================================================

/**
 * Represents the main toolpane in OX Document applications shown below the
 * toplevel view pane.
 */
export class MainToolPane extends DynamicPane<EditView, DynamicPaneConfig, MainToolPaneEventMap> {

    /** Maps toolbars to their associated tab identifiers. */
    readonly #tabIdMap = new Map<ToolBar, string>();

    // constructor ------------------------------------------------------------

    /**
     * @param docView
     *  The document view instance containing this toolpane.
     */
    constructor(docView: EditView) {

        // base constructor
        super(docView, {
            position: "top",
            classes: "main-pane",
            cursorNavigate: true
        });

        // special formatting for "combined panes" mode
        this.el.classList.toggle("combined-panes", COMBINED_TOOL_PANES);

        // show/hide tool pane depending on user settings
        this.toggle(this.docApp.showMainToolPaneAtStart());

        // dropdown menu for toolpanes in "combined panes" mode, with other controls such as Undo/redo
        const menuToolBar = this.addToolBar(new ToolBar(docView), { targetSection: "leading" });
        menuToolBar.addControl("view/toolbars/tab", new ToolPaneCombinedMenu(docView), { visibleKey: "view/toolbars/tab/combinedpanes/visible" });

        // create another close button for "combined panes" mode
        const quitToolBar = this.addToolBar(new ToolBar(docView), { targetSection: "trailing" });
        quitToolBar.addControl("app/quit", new Button(QUIT_BUTTON_OPTIONS), { visibleKey: "view/quit/combinedpanes/visible" });

        // listen to tab events of the toolpane manager
        const toolPaneManager = docView.toolPaneManager;
        this.listenTo(toolPaneManager, "tab:activate", tabEntry => this.#activateTab(tabEntry?.tabId));

        // deactivate toolbar tabs while toolpane is hidden
        this.on("pane:hide", () => toolPaneManager.activateToolPane(null));
        // bug 39071: reactivate last toolbar tab, if toolpane becomes visible
        this.on("pane:show", () => toolPaneManager.restoreActiveToolPane());

        // set stable marker after tab switch and refresh (used in automated tests)
        this.on("pane:refreshed", () => {
            this.el.dataset.stable = "true";
            if (toolPaneManager.getActiveTabId()) { this.trigger("toolpane:refreshed"); }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts a new toolbar into this toolpane. Adds support for dynamic
     * toolbars to the same method of base class `ToolPane`.
     *
     * The visibility of dynamic toolbars can be bound to controller items, and
     * their contents will be fit into the available space automatically.
     *
     * @param toolBar
     *  The toolbar to be added to this toolpane. The toolpane takes ownership
     *  of the toolbar!
     *
     * @param [options]
     *  Optional parameters. In dynamic mode, all of the supported options of
     *  `DynamicToolBarOptions` may have been passed to the constructor of the
     *  toolbar before instead, and will be used from there in this case.
     *  Options passed with this parameter will override the toolbar options
     *  though.
     *
     * @returns
     *  The toolbar instance passed to this method, for convenience.
     */
    override addDynamicToolBar<ToolBarT extends ToolBar>(toolBar: ToolBarT, options?: DynamicToolBarOptions): ToolBarT {

        // check that this method is only called during toolpane rendering
        const tabId = this.docView.toolPaneManager.getRenderingTabId();
        if (!tabId) { throw new Error("MainToolPane.addToolBar: invalid call"); }

        // remember tab identifier for the toolbar
        this.#tabIdMap.set(toolBar, tabId);

        // insert the dynamic toolbar into this toolpane
        return super.addDynamicToolBar(toolBar, options);
    }

    // protected methods ------------------------------------------------------

    /**
     * Creates an iterator that visits all toolbar entries of the active tab.
     *
     * This method overrides the method of the base class, and adds a filter
     * that skips all toolbars not associated to the active toolbar tab.
     *
     * @yields
     *  The wrapper objects for dynamic toolbars.
     */
    protected override *yieldDynamicWrappers(options?: DynamicPaneIteratorOptions): IterableIterator<DynamicToolBarWrapper> {

        // do not process any toolbar while this toolpane is hidden
        const tabId = this.docView.toolPaneManager.getActiveTabId();
        if (!tabId) { return; }

        // filter for toolbars associated to the active tab
        for (const wrapper of super.yieldDynamicWrappers(options)) {
            if (this.#tabIdMap.get(wrapper.toolBar) === tabId) {
                yield wrapper;
            }
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Shows the toolbars associated to the specified tab identifier.
     *
     * @param tabId
     *  The identifier of the toolbar tab. If set to `undefined`, all toolbars
     *  will be hidden.
     */
    #activateTab(tabId: Opt<string>): void {

        // remove stable flag (will be restored after rerendering the toolpane)
        delete this.el.dataset.stable;

        // hide old toolbars, show new toolbars (use base class iterator without tab filter)
        for (const wrapper of super.yieldDynamicWrappers()) {
            const wrapperTabId = this.#tabIdMap.get(wrapper.toolBar);
            wrapper.updateVisibility(tabId === wrapperTabId);
        }

        // show the toolpane, if a tab has been activated; then run all expand handlers,
        // and restart autofit algorithm for the new toolbars
        if (tabId) {
            this.show();
            this.resetAutoFitState();
            this.refresh({ immediate: true });
        }
    }
}
