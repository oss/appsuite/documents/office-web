/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { isEscapeKey } from "@/io.ox/office/tk/dom";

import { DynamicPane } from "@/io.ox/office/baseframework/view/pane/dynamicpane";

import { SearchToolBar, ReplaceToolBar } from "@/io.ox/office/editframework/view/edittoolbars";
import type { EditView } from "@/io.ox/office/editframework/view/editview";

// class SearchPane ===========================================================

/**
 * Represents the search pane in OX Document edit applications.
 *
 * @param docView
 *  The document view instance containing this search pane.
 */
export class SearchPane extends DynamicPane<EditView> {

    // constructor ------------------------------------------------------------

    constructor(docView: EditView) {

        // base constructor
        super(docView, {
            position: "top",
            classes: "search-pane"
        });

        // hide this toolpane initially
        this.hide();

        // lazy initialization before first usage
        this.one("pane:beforeshow", this.#initialize);

        // execute the "End of search" command when closing the search pane
        this.on("pane:hide", () => jpromise.floating(docView.executeControllerItem("document/search/end")));
    }

    // private methods --------------------------------------------------------

    /**
     * Initializes this view pane (used for lazy initialization before first
     * usage).
     */
    #initialize(): void {

        // create the "Search" toolbar (hide in "combined panes" mode, it is shown in main toolpane then)
        this.addDynamicToolBar(new SearchToolBar(this.docView), { visibleKey: "!view/combinedpanes" });

        // create the "Replace" toolbar
        this.addDynamicToolBar(new ReplaceToolBar(this.docView));

        // when ESCAPE bubbles up to the window body from everywhere, hide the search pane too
        this.listenTo(this.docApp.getWindowNode(), "keydown", this.#keyDownHandler);

        // hide the search pane explicitly on ESCAPE key while focused
        // (base class `BasePane` stops propagation of the ESCAPE key event)
        this.listenTo(this.$el, "keydown", this.#keyDownHandler);
    }

    /**
     * Keyboard event handler that hides this search pane on ESCAPE key.
     */
    #keyDownHandler(event: JTriggeredEvent): void | false {
        if (this.isVisible() && isEscapeKey(event)) {
            this.docView.grabFocus();
            this.hide();
            return false;
        }
    }
}
