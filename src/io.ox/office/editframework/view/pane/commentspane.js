/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { BasePane } from '@/io.ox/office/baseframework/view/pane/basepane';
import { XCommentsPane } from '@/io.ox/office/editframework/view/xcommentspane';

// class CommentsPane =========================================================

/**
 * This view pane contains all comment threads of the active page.
 *
 * @param {EditView} docView
 *  The document view containing this instance.
 *
 * @param {string} noCommentsLabel
 *  The UI label for the placeholder text shown, if the comments pane does
 *  not contain any comments.
 */
export class CommentsPane extends XCommentsPane.mixin(BasePane) {

    constructor(docView, noCommentsLabel) {

        // base constructor
        super(
            // parameters for the mixin class XCommentsPane
            docView,
            noCommentsLabel,

            // parameters for the base class BasePane
            docView, {
                position: 'right',
                classes: 'comments-pane',
                size: 300,
                minSize: 220,
                maxSize: 400,
                resizable: true
            }
        );

        // appending content to the pane root node after it is set as this.$el
        // in the constructor of the class "BasePane"
        this.appendContentsToNode();

        // initialize visibility
        this.toggle(!!docView.docApp.getLaunchOption('commentsPaneVisible'));
    }
}
