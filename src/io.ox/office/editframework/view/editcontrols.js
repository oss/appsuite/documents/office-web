/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from "gettext";

import { Button, CheckBox, CompoundButton } from "@/io.ox/office/baseframework/view/basecontrols";

import { Color } from "@/io.ox/office/editframework/utils/color";
import { ColorPicker } from "@/io.ox/office/editframework/view/control/colorpicker";

// re-exports =================================================================

export * from "@/io.ox/office/baseframework/view/basecontrols";
export * from "@/io.ox/office/drawinglayer/view/drawingcontrols";
export * from "@/io.ox/office/editframework/view/control/toolpanebuttongroup";
export * from "@/io.ox/office/editframework/view/control/toolpanepicker";
export * from "@/io.ox/office/editframework/view/control/toolpanecombinedmenu";
export * from "@/io.ox/office/editframework/view/control/applicationstatuslabel";
export * from "@/io.ox/office/editframework/view/control/newdocumentbutton";
export * from "@/io.ox/office/editframework/view/control/acquireeditbutton";
export * from "@/io.ox/office/editframework/view/control/filenamefield";
export * from "@/io.ox/office/editframework/view/control/colorpicker";
export * from "@/io.ox/office/editframework/view/control/borderflagspicker";
export * from "@/io.ox/office/editframework/view/control/borderstylepicker";
export * from "@/io.ox/office/editframework/view/control/borderwidthpicker";
export * from "@/io.ox/office/editframework/view/control/fontfamilypicker";
export * from "@/io.ox/office/editframework/view/control/fontsizepicker";
export * from "@/io.ox/office/editframework/view/control/stylesheetpicker";
export * from "@/io.ox/office/editframework/view/control/languagepicker";
export * from "@/io.ox/office/editframework/view/control/tablesizepicker";
export * from "@/io.ox/office/editframework/view/control/tablestylepicker";

// class ReloadDocumentButton =================================================

export class ReloadDocumentButton extends Button {
    constructor() {
        super({ icon: "bi:arrow-repeat", label: gt("Reload"), tooltip: gt("Reload document") });
    }
}

// class TextColorPicker ======================================================

/**
 * A color picker control for selecting a text color. The list entry for the
 * automatic color will be shown as "Black" by default, but can be changed with
 * the method `ColorPicker#setAutoColor`.
 *
 * @param {EditView} docView
 *  The document view that contains this color picker.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the `ColorPicker` base
 *  class.
 */
export class TextColorPicker extends ColorPicker {
    constructor(docView, config) {
        super(docView, {
            icon: "png:font-color",
            tooltip: gt("Text color"),
            splitValue: Color.RED,
            autoColorType: "text",
            ...config
        });
    }
}

// class FillColorPicker ======================================================

/**
 * A color picker control for selecting a fill color. The list entry for the
 * automatic color will be shown as "No fill" by default, but can be changed
 * with the method `ColorPicker#setAutoColor`.
 *
 * @param {EditView} docView
 *  The document view that contains this color picker.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the `ColorPicker` base
 *  class.
 */
export class FillColorPicker extends ColorPicker {
    constructor(docView, config) {
        super(docView, {
            icon: "png:cell-fill-color",
            label: gt("Fill color"),
            splitValue: Color.YELLOW,
            autoColorType: "fill",
            ...config
        });
    }
}

// class SaveAsCompoundButton =================================================

export class SaveAsCompoundButton extends CompoundButton {

    constructor(docView) {

        // base constructor
        super(docView, {
            label: gt("Save in Drive"),
            shrinkIcon: "bi:save"
        });

        // save to native file formats
        this.addSection("saveasfile");
        this.addControl("document/saveas/native/dialog",    new Button({ label: gt("Save as") }));
        this.addControl("document/saveas/encrypted/dialog", new Button({ label: gt("Save as (encrypted)") }), { visibleKey: "document/saveas/encrypted/dialog" });
        this.addControl("document/saveas/template/dialog",  new Button({ label: gt("Save as template") }));

        // save to PDF
        this.addSection("saveaspdf");
        this.addControl("document/saveas/pdf/dialog", new Button({ label: gt("Export as PDF") }));

        // "autosave" pseudo checkbox
        this.addSection("autosave");
        this.addControl("document/autosave", new CheckBox({ label: gt("AutoSave") }));
    }
}
