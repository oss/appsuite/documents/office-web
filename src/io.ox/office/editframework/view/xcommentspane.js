/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { scrollToChildNode } from '@/io.ox/office/tk/utils';
import { jpromise } from '@/io.ox/office/tk/algorithms';
import { SMALL_DEVICE, detachChildren, insertChildren, createButton, setFocus } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { DataPipeline } from '@/io.ox/office/tk/workers';

import { COMMENT_HEADER_LABEL } from '@/io.ox/office/editframework/view/editlabels';
import CommentFrame from '@/io.ox/office/editframework/view/commentframe';
import { CommentPopup } from '@/io.ox/office/editframework/view/popup/commentpopup';

import '@/io.ox/office/editframework/view/pane/commentspane.less';
import '@/io.ox/office/editframework/view/commentframe.less';

// singletons =============================================================

/**
 * Background task for destroying comment frames.
 */
const destroyWorker = new DataPipeline(commentFrame => {
    commentFrame.destroy();
}, { delay: 1000, slice: 50, interval: 100, _kind: "singleton" });

function mixinCommentsPane(BaseClass) {

    // mix-in class CommentsPaneMixin =========================================

    /**
     * A mix-in class with shared functionality for comments used by the
     * classes `CommentsPane` (for Spreadsheet and Presentation), and
     * `TextCommentsPane` (for Text).
     */
    class CommentsPaneMixin extends BaseClass {

        // the application, and document model
        #docApp = null;
        #docModel = null;
        #docView = null;

        // the comment model which is currently in editmode
        #editCommentModel = null;

        // the frame which is currently selected
        #selectedCommentFrame = null;

        // the map with all comment frames, mapped by comment IDs
        #commentFrameMap = null;

        // the threadedframe of the new thread which is not commited
        #newCommentFrame = null;

        // the popup for the bubble mode (smartphone) which shows the new threadedcomment frame
        #commentPopup;

        // the header node in the comments pane
        #header = $('<div class="header">');

        // label if the page/sheet contains no comments
        #noCommentsInfoNode;

        // the container node for the comment frames
        #commentsContainer = $('<div class="comments-container">');

        // special handling for Text application
        #isTextApp = false;

        // whether this comments pane needs to repaint all comment frames when it becomes visible
        #needsFullRepaint = true;

        // will be in pending state while comments are being repainted (e.g. defer selection of unsaved comment)
        #repaintDef;

        /**
         * @param {EditView} docView
         *  The document view owning this instance.
         *
         * @param {string} noCommentsLabel
         *  The UI label for the placeholder text shown, if the comments pane does
         *  not contain any comments.
         *
         * @param {unknown[]} baseCtorArgs
         *  All following arguments will be passed to the constructor of the
         *  base class `ModelBaseClass`.
         */
        constructor(docView, noCommentsLabel, ...baseCtorArgs) {

            super(...baseCtorArgs);

            this.#docView = docView;
            this.#docApp = docView.docApp;
            this.#docModel = docView.docModel;

            this.#commentFrameMap = this.member(new Map/*<string, CommentFrame>*/());
            this.#isTextApp = this.#docApp.isTextApp();

            this.#repaintDef = jpromise.resolve();

            this.#noCommentsInfoNode = $('<div class="no-comments-info">').append($('<span>').text(noCommentsLabel));

            // the title label inside the header node
            const titleNode = $('<div class="title-label">').text(COMMENT_HEADER_LABEL);

            // create the close button element
            const closeButton = createButton({
                classes: "ignorepageevent closebutton",
                type: "bare",
                action: "close",
                icon: "bi:x-lg",
                tooltip: gt.pgettext('comment-actions', 'Hide comments pane'),
                click: () => this.hide()
            });

            this.#header.append(titleNode, closeButton);

            if (!this.#isTextApp) {
                this.#docModel.waitForImportSuccess(() => {
                    // DOCS-1508: repaint all comment frames after import (only if this pane is visible)
                    if (this.isVisible()) { this.#tryRepaintAllCommentFrames(); }
                    // DOCS-1508: repaint all dirty comment frames when this pane becomes visible
                    this.on('pane:show', this.#tryRepaintAllCommentFrames);
                }, this);
            }

            this.on('pane:show', () => {
                if (SMALL_DEVICE) {
                    this.hide();
                } else {
                    const selectedThreadElement = this.#commentsContainer.find('.thread.selected');
                    if (selectedThreadElement.length) {
                        selectedThreadElement.get(0).scrollIntoView();
                    } else {
                        this.#commentsContainer.scrollTop(0);
                    }
                }
            });

            this.listenTo(this.#docApp, 'docs:editmode:leave', this.deleteUnsavedComments);

            if (this.#isTextApp) {
                this.listenTo(this.#docApp, 'docs:editmode', () => {
                    if (this.isVisible()) {
                        this.#repaintAllCommentFramesSync();
                        // Performance: Update comments pane only, if it is visible (especially in loading phase, DOCS-2391)
                        if (this.#docModel.isImportFinished()) {
                            // triggering 'updateVerticalPositionInCommentsPane' for vertically updating the comments pane
                            this.#docView.trigger('update:comments');
                        }
                    }
                });
            }

            this.listenTo(this.#docModel, 'select:threadedcomment', (model, handler) => { this.#setThreadSelection(model, handler); });

            this.listenTo(this.#docModel, 'insert:threadedcomment', this.#insertComment);
            this.listenTo(this.#docModel, 'delete:threadedcomment:end', this.#deleteComments);
            this.listenTo(this.#docModel, 'new:threadedcomment:thread', this.#insertNewThread);
            this.listenTo(this.#docModel, 'new:threadedcomment:thread:delete', this.removeNewThread);
            this.listenTo(this.#docModel, 'new:threadedcomment:comment', this.#insertNewComment);
            this.listenTo(this.#docModel, 'change:threadedcomment', this.#changeComment);
            this.listenTo(this.#docModel, 'after:move:threadedcomments', this.#initRepaintAllCommentFrames);

            // Create a new comment thread frame and open the editor for new thread
            // or open the reply editor for a reply.
            this.listenTo(this.#docModel, 'start:threadedcomment', this.#startNewThread);
            this.listenTo(this.#docModel, 'start:threadedcomment:reply', this.#replyToComment);

            this.listenTo(this.#docModel, 'commentbubble:mouseenter', this.#handleCommentBubbleMouseenter);
            this.listenTo(this.#docModel, 'commentbubble:mouseleave', this.#handleCommentBubbleMouseleave);

            this.listenTo(this.#docModel, 'threadedcomment:registry:empty', this.#clearAndHideCommentsPane);

            // always hide the comments pane when an internal application error occurs
            this.listenTo(this.#docApp, 'docs:state:error', () => { this.hide(); });

            // destroy class members on destruction
            this.registerDestructor(() => { this.#newCommentFrame?.destroy(); });
        }

        // public methods -----------------------------------------------------

        /**
         * Appending several required content nodes to the specified root node element "this.$el".
         * This function must be called in the constructor of the extending class directly after
         * the "super" call or after "this.$el" is set. "this.$el" is the jQuerified root node
         * that is used as comments pane.
         */
        appendContentsToNode() {
            this.$el.append(this.#header, this.#noCommentsInfoNode, this.#commentsContainer);
        }

        removeNewThread() {
            if (this.#newCommentFrame) {
                // this.#docView.trigger('delete:threadedcomment:start', newThreadedCommentFrame.getThreadComment());
                this.#setThreadSelection();

                this.#newCommentFrame.destroy();
                this.#newCommentFrame = null;
                if (this.#commentPopup) {
                    // comment popup may be in its own `hide` method currently (e.g. by clicking the close button),
                    // do not destroy it synchronously to prevent type errors from following code inside `hide`
                    this.setTimeout(this.#commentPopup.destroy.bind(this.#commentPopup), 0);
                    this.#commentPopup = null;
                }
                this.setEditCommentModel(null);
                this.#docView.trigger('refresh:layout');

                this.#refreshNoCommentsInfo();
            }
        }

        isEditMode() {
            return !!this.#editCommentModel || !!this.#newCommentFrame;
        }

        setEditCommentModel(commentModel) {
            this.#editCommentModel = commentModel;
            this.#commentsContainer.toggleClass('edit', this.isEditMode());
            this.#docApp.docController.update();
        }

        /**
         * Returns the comment frame for the passed comment model.
         *
         * @param {CommentModel} commentModel
         *  The model of a comment.
         */
        detachOrCreateCommentFrame(commentModel) {

            let commentFrame;
            if (this.#newCommentFrame && this.#newCommentFrame.getThreadComment().getAnchor().equals(commentModel.getAnchor())) {
                commentFrame = this.#newCommentFrame;
            } else {
                commentFrame = this.#getCommentFrame(commentModel);
                // DOCS-1508: create missing comment frames on demand (frames have not rendered yet)
                if (!commentFrame) {
                    // collect all comment models of the thread (TODO: should become a method of the collection)
                    const collection = this.getCollection();
                    let commentModels = collection ? collection.getCommentModels() : null;
                    if (commentModels) {
                        const parentModel = commentModel.getParent() || commentModel;
                        commentModels = commentModels.filter(model => {
                            return (model.getParent() || model) === parentModel;
                        });
                        if (commentModels.length > 0) {
                            commentFrame = this.#createCommentFrame(commentModels);
                        }
                    }
                }
            }

            if (commentFrame) {
                commentFrame.getNode().detach();
            } else {
                globalLogger.error('$badge{CommentsPaneMixin} detachOrCreateCommentFrame: unable to obtain frame for comment ' + commentModel.getId());
            }

            return commentFrame;
        }

        deleteUnsavedComments() {
            this.removeNewThread();
            this.setEditCommentModel(null);
            this.#docModel.deleteUnsavedComments();
        }

        /**
         * Shows and selects the specified comment thread.
         */
        selectThread(threadModel) {
            this.show();
            // TODO: cache requested selection for lazy rendering
            this.#setThreadSelection(threadModel);
        }

        selectUnsavedComment() {

            // make the comments pane visible
            this.show();

            // DOCS-3848: wait for debounced repaint of all comment frames
            this.onFulfilled(this.#repaintDef, () => {

                // select the comment frame with the unsaved changes
                if (this.#newCommentFrame) {
                    if (this.#isTextApp && this.getCollection().selectTemporaryComment) { this.getCollection().selectTemporaryComment(); } // only in Text application
                    this.#newCommentFrame.highlightUnsavedComment();
                    if (this.#isTextApp) { this.#scrollCommentFrameIntoView(this.#newCommentFrame); }
                } else if (this.#editCommentModel) {
                    if (SMALL_DEVICE || this.#isTextApp) {
                        this.getCollection().selectComment(this.#editCommentModel);
                    }
                    const editCommentFrame = this.#getCommentFrame(this.#editCommentModel);
                    if (editCommentFrame) {
                        editCommentFrame.highlightUnsavedComment(this.#editCommentModel);
                        if (this.#isTextApp) { this.#scrollCommentFrameIntoView(editCommentFrame); }
                    }
                }
            });
        }

        /**
         * Shows and selects the comment thread that is currently in edit mode.
         */
        selectEditComment() {

            if (!this.#editCommentModel) { return; }

            // make the comments pane visible
            this.show();

            this.onFulfilled(this.#repaintDef, () => {

                if (SMALL_DEVICE || this.#isTextApp) {
                    this.getCollection().selectComment(this.#editCommentModel);
                }
                const editCommentFrame = this.#getCommentFrame(this.#editCommentModel);
                if (editCommentFrame) {
                    editCommentFrame.highlightUnsavedComment(this.#editCommentModel, false);
                    if (this.#isTextApp) { this.#scrollCommentFrameIntoView(editCommentFrame); }
                }
            });
        }

        /**
         * Returning the header node.
         *
         * @returns {jQuery}
         *  The header inside the comments container.
         */
        getHeader() {
            return this.#header;
        }

        /**
         * Returning the container for the comment nodes.
         *
         * @returns {jQuery}
         *  The container for the threaded frames.
         */
        getCommentsContainer() {
            return this.#commentsContainer;
        }

        /**
         * Returns an iterator visiting all comment frame elements.
         *
         * @returns {IterableIterator<CommentFrame>}
         *  An iterator visiting all comment frame elements.
         */
        commentFrames() {
            return this.#commentFrameMap.values();
        }

        /**
         * Returns the active comment collection from the document view.
         *
         * @returns {CommentCollection|null}
         *  The collection with threaded comments that is currently active in
         *  this document view if available, otherwise `null`.
         */
        getCollection() {
            return this.#docView.getCommentCollection();
        }

        /**
         * Getting a ThreadedCommentFrame comment object by its ID.
         *
         * In OX Text this can also be the new created threaded comment frame,
         * that is not already part of the comment collection.
         *
         * @param {string} id
         *  The id of the first comment in the thread, that is used as key for
         *  the threadedCommentFrame in its collector.
         *
         * @returns {Opt<CommentFrame>}
         *  The comment frame, if existing; otherwise `undefined`.
         */
        getThreadedCommentFrameById(id) {
            return (this.#isTextApp && this.getCollection().isTemporaryCommentId(id)) ? this.#newCommentFrame : this.#commentFrameMap.get(id);
        }

        /**
         * Repaints all comment frames in this pane.
         */
        repaintAllComments() {
            // remove the additional frame for the unsaved comment
            this.removeNewThread();
            // repaint all comment frames (will setup a background task)
            this.#initRepaintAllCommentFrames();
        }

        /**
         * Returns the text element of the edited comment if available.
         *
         * @returns {JQuery}
         *  The text element of the edited comment if available; otherwise an empty
         *  JQuery collection.
         */
        getEditFocusTarget() {
            return this.$el.find(".comment-text-frame");
        }

        /**
         * Helper function to set the focus into the commens pane, especially into the
         * closer button. This should not be necessary explicitely, but in OX Text and
         * OX Presentation the 'f6-target' is not reliable, when the comments pane is
         * open. In this scenario, when the user presses F6 (or Strg-F6) the focus must
         * be explicitely set into the comments pane using this function.
         */
        setFocusIntoCommentsPane() {
            return setFocus(this.$el.find(".closebutton"));
        }

        moveComments(commentModels) {
            if (this.#needsFullRepaint) { return; }
            for (const commentModel of commentModels) {
                const commentFrame = this.#getCommentFrame(commentModel);
                if (commentFrame) { commentFrame.moveComment(commentModel); }
            }
        }

        // private methods ----------------------------------------------------

        /**
         * Show the info label if no comments exists or show the commentsContainer if comments exists.
         */
        #refreshNoCommentsInfo() {
            const hasComments = (this.#commentFrameMap.size > 0) || !!this.#newCommentFrame;
            this.#commentsContainer.toggle(hasComments);
            this.#noCommentsInfoNode.toggle(!hasComments && this.isVisible());
        }

        /**
         * Returns the DOM comment frame for the passed comment model.
         *
         * @param {CommentModel} commentModel
         *  The comment model to return the comment frame for.
         *
         * @returns {Opt<CommentFrame>}
         *  The DOM comment frame for the passed model, if available; otherwise
         *  `undefined`.
         */
        #getCommentFrame(commentModel) {

            if (!commentModel) { return undefined; }

            let commentFrame;
            if (commentModel.isReply()) {
                commentFrame = this.getThreadedCommentFrameById(this.#isTextApp ? commentModel.getParentId() : commentModel.getParent().getId());
                if (!commentFrame && commentModel.hasInvalidParent && commentModel.hasInvalidParent()) {
                    commentFrame = this.getThreadedCommentFrameById(commentModel.getReplacedParentId());
                }
            } else {
                commentFrame = this.getThreadedCommentFrameById(commentModel.getId());
            }
            return commentFrame;
        }

        /**
         * Creates a new comment frame element for the specified comment model,
         * and inserts the comment frame into the DOM container element.
         *
         * @param {CommentModel[]} commentThread
         *  The array of comment models forming a complete comment thread.
         *
         * @param {number} [index]
         *  the insertion index of the new comment frame in the comments pane.
         *  If omitted, the new frame will be appended.
         *
         * @returns {CommentFrame}
         *  The new comment frame for the passed comment thread.
         */
        #createCommentFrame(commentThread, index) {

            // DOCS-2871: try to resolve existing comment frame (concurrent rendering
            // from comments pane initialization triggered by inserting a new comment)
            let commentFrame = this.#getCommentFrame(commentThread[0]);
            if (commentFrame) { return commentFrame; }

            // whether there is always an empty comments container
            // -> this happens on small devices in OX Spreadsheet and OX Presentation (DOCS-4399)
            const emptyCommentsContainer = SMALL_DEVICE && !this.#isTextApp;

            // create and insert a new comment frame
            commentFrame = new CommentFrame(this.#docView, commentThread);
            if (!_.isNumber(index) || emptyCommentsContainer) {
                this.#commentsContainer.append(commentFrame.getNode());
            } else {
                insertChildren(this.#commentsContainer[0], index, commentFrame.getNode()[0]);
            }

            // add the comment frame into the cache map
            this.#commentFrameMap.set(commentThread[0].getId(), commentFrame);

            // trigger event for additional subclass handling
            this.trigger('insert:frame', commentFrame);
            return commentFrame;
        }

        /**
         * Scrolling a specified comment thread into the visible view.
         *
         * This might be specific to OX Text.
         *
         * @param {CommentFrame} commentFrame
         *  The threaded comment frame instance that will be scrolled
         *  into the view.
         */
        #scrollCommentFrameIntoView(commentFrame) {

            const frameNode = commentFrame.getNode();
            const frameTop = frameNode.offset().top;
            const contentRootNode = this.#isTextApp ? this.#docView.getContentRootNode() : this.#commentsContainer;
            const rootNodeTop = contentRootNode.offset().top;

            if ((frameTop < rootNodeTop) || (frameTop + frameNode.height() > rootNodeTop + contentRootNode.height())) {
                scrollToChildNode(contentRootNode, frameNode);
            }
        }

        /**
         * Checking whether the specified first comment model is a valid child of the specified second comment model (DOCS-2397).
         *
         * @param {Object} commentModel
         *  The comment model object of the child.
         *
         * @param {Object} parentCommentModel
         *  The comment model object of the parent.
         *
         * @returns {boolean}
         *  Whether the specified first comment model is a valid child of the specified second comment model.
         */
        #isValidParent(commentModel, parentCommentModel) {
            if (commentModel.getParent() && commentModel.getParent().getId() !== parentCommentModel.getId()) { return false; }
            return true;
        }

        /**
         * Removes and destroys all existing comment frames.
         */
        #removeAllCommentFrames() {

            if (this.#newCommentFrame) {
                this.#newCommentFrame.getNode().detach();
            }

            // destroy all existing frame instances (in a background worker), and clear the map
            detachChildren(this.#commentsContainer[0]);
            destroyWorker.pushValues(this.#commentFrameMap.values());
            this.#commentFrameMap.clear();

            this.#noCommentsInfoNode.hide();
        }

        /**
         * Repaints all missing comment frames.
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {number} [options.maxCount]
         *    The maximum number of comment frames to be created by this call.
         *    If there are more comment models without an associated comment
         *    frame, they will be skipped. Default is infinity (create all
         *    missing comment frames).
         */
        #repaintMissingCommentFrames(options) {

            // de-init comment edit mode
            if (!options?.debounced) {
                this.#editCommentModel = null;
                this.#commentsContainer.removeClass('edit');
            }

            const collection = this.getCollection();
            const commentModels = collection ? collection.getCommentModels() : null;
            const unsavedData = collection ? collection.getUnsavedComment() : null;

            if (commentModels && commentModels.length > 0) {

                const selectedThread = collection.getSelectedThread();
                let selectionComment = null;
                let commentThread = [];
                const allCommentThreads = [];

                commentModels.forEach(commentModel => {

                    if (commentModel.isReply()) {
                        // DOCS-2397: attach comments with invalid parent to the first thread (like PP)
                        if (allCommentThreads[0] && !this.#isValidParent(commentModel, commentThread[0])) {
                            allCommentThreads[0].push(commentModel);
                        } else {
                            commentThread.push(commentModel);
                        }
                    } else {

                        if (commentThread.length) { allCommentThreads.push(commentThread); }
                        commentThread = [];

                        if (commentModel.equals(selectedThread)) {
                            selectionComment = commentModel;
                        }

                        commentThread.push(commentModel);
                    }
                });

                if (commentThread.length) { allCommentThreads.push(commentThread); }

                // maximum number of comment frames to be created
                let maxCount = (options && options.maxCount) || Infinity;
                // use `every()` to be able to leave the loop early
                allCommentThreads.every((commentThread, index) => {
                    if (!this.#getCommentFrame(commentThread[0])) {
                        this.#createCommentFrame(commentThread, index);
                        // restore unsaved reply or edit
                        if (unsavedData && commentThread.indexOf(unsavedData.model) >= 0) {
                            const recoverOptions = { text: unsavedData.text, mentions: unsavedData.mentions };
                            if (unsavedData.isNewReply) {
                                this.#replyToComment(unsavedData.model, recoverOptions);
                            } else if (unsavedData.isEditComment) {
                                this.#changeComment(unsavedData.model, recoverOptions);
                            }
                        }
                        maxCount -= 1;
                    }
                    return maxCount > 0;
                });

                if (selectionComment) {
                    this.#setThreadSelection(selectionComment);
                }
            }

            // restore unsaved new comment
            if (!options?.debounced && unsavedData && unsavedData.isNewThread) {
                this.#startNewThread({ model: unsavedData.model }, unsavedData);
            }

            if (this.#newCommentFrame && !this.#commentsContainer.has(this.#newCommentFrame.getNode()).length) {
                this.#commentsContainer.append(this.#newCommentFrame.getNode());
            }

            this.#refreshNoCommentsInfo();
        }

        /**
         * Removes all existing comment frames, and repaints all comment frames
         * synchronously.
         */
        #repaintAllCommentFramesSync() {
            this.#needsFullRepaint = false;
            this.#removeAllCommentFrames();
            this.#repaintMissingCommentFrames();
        }

        /**
         * Removes all existing comment frames, and repaints all comment frames
         * asynchronously. This is the direct callback function that calls the
         * debounced function "this._repaintAllCommentFramesDebounced()".
         */
        #repaintAllCommentFramesAsync() {

            this.#needsFullRepaint = false;
            this.#repaintDef = jpromise.deferred();
            this.#removeAllCommentFrames();
            this.#repaintMissingCommentFrames({ maxCount: 10 });

            this._repaintAllCommentFramesDebounced(); // direct callback calls debounced callback
        }

        /**
         * Removes all existing comment frames, and repaints all comment
         * frames. In difference to the method `repaintAllCommentFramesSync()`,
         * this method will paint just a small number of comment panes
         * synchronously, and will continue to repaint the remaining comment
         * panes at a later time.
         */
        @debounceMethod({ delay: 100 })
        _repaintAllCommentFramesDebounced() {
            this.#repaintMissingCommentFrames({ debounced: true });
            this.#repaintDef.resolve();
        }

        /**
         * Repaints all comment frames, if the dirty flag has been set (see
         * method `this.#initRepaintAllCommentFrames()`), and this comments pane is
         * currently visible.
         */
        #tryRepaintAllCommentFrames() {
            if (this.#isTextApp) {
                this.#repaintAllCommentFramesSync();
            } else if (this.#needsFullRepaint && this.isVisible()) {
                this.#repaintAllCommentFramesAsync();
            }
        }

        /**
         * Sets the dirty marker that signals to repaint all comment frames in
         * this comments pane, and prepares to repaint the comment frames at a
         * later time.
         */
        #initRepaintAllCommentFrames() {
            this.#needsFullRepaint = true;
            this.#tryRepaintAllCommentFrames();
        }

        #setThreadSelection(comment, deselectHandler) {

            const isTextBubbleMode = this.#isTextApp && this.#docModel.getCommentLayer().isBubbleMode();

            if (this.#selectedCommentFrame && this.#selectedCommentFrame.destroyed) {
                this.#selectedCommentFrame = null;
            }

            if (comment) {

                const commentFrame = this.#getCommentFrame(comment);
                const threadElement = commentFrame ? commentFrame.getNode() :  null;

                if (threadElement && !(this.#selectedCommentFrame && this.#selectedCommentFrame.equals(commentFrame))) {

                    // if (!this.#editCommentModel || this.#editCommentModel.equals(comment)) { // DOCS-1935

                    if (this.#selectedCommentFrame) {
                        this.#selectedCommentFrame.select(false);
                        if (deselectHandler) { this.#docView.getContentRootNode().off('click', deselectHandler); } // handler at content root node no longer required
                    }

                    commentFrame.select(true);
                    // setting a handler to get rid of the selection later
                    // -> delayed, so that selection is not removed immediately; especially in combination with requrired scrolling and bubble mode
                    if (deselectHandler) {
                        this.setTimeout(() => { this.#docView.getContentRootNode().on('click', deselectHandler); }, 100);
                    }

                    this.#selectedCommentFrame = commentFrame;
                    // } // DOCS-1935

                    this.#scrollCommentFrameIntoView(commentFrame);

                }
            } else if (this.#selectedCommentFrame && (!this.#editCommentModel || this.#editCommentModel.equals(comment) || isTextBubbleMode)) {
                this.#selectedCommentFrame.select(false);
                this.#selectedCommentFrame = null;
                if (deselectHandler) { this.#docView.getContentRootNode().off('click', deselectHandler); } // handler at content root node no longer required
                this.trigger('thread:deselect:all', comment);
            }
        }

        #insertNewThread(commentModel) {
            this.show();
            this.#insertComment(commentModel);
            this.#setThreadSelection(commentModel);
            this.#refreshNoCommentsInfo();
        }

        #insertNewComment(commentModel) {
            this.show();
            const commentFrame = this.#getCommentFrame(commentModel);
            if (commentFrame) { commentFrame.addComment(commentModel); }
            this.#refreshNoCommentsInfo();
        }

        #replyToComment(commentModel, replyRecoverOptions) {
            if (this.isEditMode()) {
                return;
            }
            this.show();
            const commentFrame = this.#getCommentFrame(commentModel);
            if (commentFrame) { commentFrame.reply(replyRecoverOptions); }
        }

        #changeComment(commentModel, changeRecoverOptions) {
            if (this.#needsFullRepaint) { return; }
            const commentFrame = this.#getCommentFrame(commentModel);
            if (commentFrame) { commentFrame.changeComment(commentModel, changeRecoverOptions); }
        }

        #deleteComments(commentModels) {

            if (this.#needsFullRepaint || !commentModels) { return; }

            commentModels.forEach(commentModel => {

                if (this.#editCommentModel && (commentModel.equals(this.#editCommentModel) || (!commentModel.isReply() && this.#editCommentModel.isReply() && commentModel.getId() === this.#editCommentModel.getParentId()))) {
                    this.setEditCommentModel(null);
                }

                const commentFrame = this.#getCommentFrame(commentModel);
                if (commentFrame) {
                    if (commentModel.isReply()) {
                        commentFrame.deleteComment(commentModel);
                    } else {
                        this.#commentFrameMap.delete(commentModel.getId());
                        commentFrame.delete();
                    }
                }
            });
            this.#refreshNoCommentsInfo();
        }

        #insertComment(commentModel) {

            if (this.#needsFullRepaint || !commentModel) { return; }

            if (commentModel.isReply()) {
                const commentFrame = this.#getCommentFrame(commentModel);
                if (commentFrame) { commentFrame.addComment(commentModel); }
            } else {
                const collection = this.getCollection();
                const commentModels = collection ? collection.getCommentModels() : null;

                if (commentModels && commentModels.length > 0) {
                    let index = 0;
                    commentModels.forEach(listCommentModel => {

                        if (!listCommentModel.isReply()) {
                            if (commentModel.equals(listCommentModel)) {
                                this.#createCommentFrame([commentModel], index);
                            }
                            index++;
                        }
                    });
                } else {
                    this.#createCommentFrame([commentModel]);
                }
                this.#refreshNoCommentsInfo();
            }
        }

        #startNewThread(options, threadRecoverOptions) {

            this.show();

            // DOCS-2903: always deselect existing comment frames
            this.#setThreadSelection();

            if (this.isEditMode()) {
                return;
            }

            this.#newCommentFrame = new CommentFrame(this.#docView, [options.model], true, threadRecoverOptions);
            this.#selectedCommentFrame = this.#newCommentFrame;

            if (SMALL_DEVICE) {
                if (!this.#isTextApp) {
                    this.#commentPopup = new CommentPopup(options.popupOptions);
                    this.#commentPopup.setCommentFrame(this.#newCommentFrame);
                    this.#commentPopup.show();
                }
            } else {
                this.#refreshNoCommentsInfo();
                this.#commentsContainer.append(this.#newCommentFrame.getNode());
                this.#commentsContainer.scrollTop(this.#commentsContainer.prop('scrollHeight'));
            }

            _.defer(() => { if (this.#newCommentFrame) { this.#newCommentFrame.editComment(options.model); } });
        }

        #getThreadContainerByCommentId(commentId) {
            return this.#commentsContainer.find('*[data-comment-id="' + commentId + '"]');
        }

        #handleCommentBubbleMouseenter(payload) {
            this.#getThreadContainerByCommentId(payload.commentModel.getId()).addClass('hover');
        }

        #handleCommentBubbleMouseleave(payload) {
            this.#getThreadContainerByCommentId(payload.commentModel.getId()).removeClass('hover');
        }

        #clearAndHideCommentsPane() {
            this.repaintAllComments();
            this.toggle(false);
        }
    }

    return CommentsPaneMixin;
}

// exports ====================================================================

export const XCommentsPane = {

    /**
     * Creates and returns a subclass of the passed class with additional
     * methods for interacting with an application and its states.
     *
     * Provides methods to send server requests, and to defer code execution
     * according to the state of the document import process. All deferred code
     * (server request handlers, import callbacks) will be aborted
     * automatically when destroying the instance.
     *
     * @param {CtorType<ClassT extends DObject>} BaseClass
     *  The base class to be extended.
     *
     * @returns {CtorType<ClassT & XCommentsPane>}
     *  The extended class with comments pane functionality.
     */
    mixin: mixinCommentsPane
};
