/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import ext from '$/io.ox/core/extensions';
import yell from '$/io.ox/core/yell';
import { Action as createAction } from '$/io.ox/backbone/views/actions/util';
import MailApi from '$/io.ox/mail/api';

import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { getFileBaseName, getStandardDocumentsFolderId } from '@/io.ox/office/tk/utils/driveutils';
import { canOpenDocsTab, openEditorChildTab, openEmptyChildTab } from '@/io.ox/office/tk/utils/tabutils';
import { getEditorModulePath } from '@/io.ox/office/baseframework/utils/apputils';
import { getMessageData } from '@/io.ox/office/baseframework/utils/infomessages';
import { getMessageData as getErrorMessageData } from '@/io.ox/office/baseframework/utils/errormessages';
import { isEditable, isNative } from '@/io.ox/office/baseframework/app/extensionregistry';
import { prefetchDocsApp, launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';

// private functions ==========================================================

/**
 * Handler for MailApi.saveAttachments errors.
 */
function saveAttachmentsErrorHandler(error) {
    globalLogger.error('MailApi.saveAttachments returns an error', error);
    yell('error', getErrorMessageData('GENERAL_SERVER_COMPONENT_NOT_WORKING_ERROR').message);
}

// public functions ===========================================================

/**
 * Creates all required actions and extends the OX Mail application with
 * document editing of e-mail attachments for a specific OX Documents
 * application.
 *
 * @param {AppType} appType
 *  The type identifier of the application.
 */
export function createMailActions(appType) {
    globalLogger.info(`$badge{launch} creating mail actions for ${appType} application`);

    // the mail extension point
    var ACTION_POINT = 'io.ox/mail/office/' + appType;

    const moduleName = getEditorModulePath(appType);

    // Copy mail attachment to Files and edit
    createAction(ACTION_POINT + '-edit-asnew', {
        id: 'save',
        capabilities: 'infostore',
        collection: 'one',

        matches(baton) {
            // filename of the attachment
            var filename = baton.first().filename;
            // whether "edit as new" should be shown for the attachment
            var editable = isEditable(filename, moduleName);

            // prefetch application source code, if application will not open in new browser tab
            if (editable && !canOpenDocsTab()) {
                prefetchDocsApp(appType);
            }
            return editable;
        },

        action(baton) {
            // the id of the target folder in Drive
            var targetFolderId = getStandardDocumentsFolderId();
            // try to open a new browser tab
            var newWindow = canOpenDocsTab() ? openEmptyChildTab() : undefined;

            MailApi.saveAttachments(baton.array(), targetFolderId).done(function (result) {
                // the data of the created Drive file returned by the mail API
                var fistResult = _.first(result);

                if (fistResult && fistResult.data && !fistResult.error) {
                    // the file descriptor passed to the OX Documents application launcher
                    var file = _.clone(fistResult.data);

                    // Bug 40376 add the origin property to the file descriptor
                    file.source = 'drive';
                    // attention: be aware that this object is parsed via url later when you add large data to it
                    file.origin = { source: 'mail', folder_id: file.mailFolder, id: file.mailUID };
                    delete file.mailFolder;
                    delete file.mailUID;

                    if (newWindow) {
                        var params = {
                            folder: file.folder_id,
                            id: file.id,
                            origin: JSON.stringify(file.origin),
                            showDocStoredNotification: 'true'
                        };

                        if (isNative(file.filename)) {
                            params.get = true;
                        } else {
                            params.convert = true;
                            params.destfolderid = file.folder_id;
                            params.destfilename = getFileBaseName(file.filename);
                        }

                        openEditorChildTab(appType, params, newWindow);
                        return;
                    }

                    // show a notification to the user that a new file has been created in Drive
                    yell('success', getMessageData('INFO_DOC_STORED_IN_DEFAULTFOLDER_AS', { fullFileName: file.filename }).message);

                    // launch the OX Documents editor application
                    const launchOptions = isNative(file.filename) ?
                        { action: 'load', file } :
                        { action: 'convert', target_folder_id: file.folder_id, templateFile: file, preserveFileName: true };
                    launchDocsApp(appType, launchOptions);

                } else {
                    saveAttachmentsErrorHandler(fistResult);
                }
            })
            .fail(yell);
        }
    });

    var link = {
        id: appType + '-edit',
        index: 1000,
        prio: 'lo',
        mobile: 'lo',
        title: gt('Edit as new'),
        ref: ACTION_POINT + '-edit-asnew'
    };

    // extend mail attachment view with documents "edit as new"
    ext.point('io.ox/mail/attachment/links').extend(_.copy(link, true));

    // extend Viewer toolbar for mail attachments with documents "edit as new"
    ext.point('io.ox/core/viewer/toolbar/links/mail').extend(_.copy(link, true));
}
