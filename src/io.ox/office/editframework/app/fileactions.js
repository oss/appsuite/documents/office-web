/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import ext from '$/io.ox/core/extensions';
import FolderAPI from '$/io.ox/core/folder/api';
import { Action as createAction } from '$/io.ox/backbone/views/actions/util';
import yell from '$/io.ox/core/yell';
import FilesApi from '$/io.ox/files/api';
import { canEditDocFederated, isFileVersionUploading } from '$/io.ox/files/util';

import { createIconMarkup } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { authorizeGuardWithTab, canModify, getCreateState, getCurrentFileVersionState, getFileBaseName, getStandardDocumentsFolderId,
    getTrashState, getWriteState, hasGuardExt, isGuest, isTrash } from '@/io.ox/office/tk/utils/driveutils';
import { canOpenDocsTab, openEditorChildTab } from '@/io.ox/office/tk/utils/tabutils';

import { getEditorModulePath } from '@/io.ox/office/baseframework/utils/apputils';
import { DOCUMENTS_COLLABORA } from '@/io.ox/office/baseframework/utils/baseconfig';
import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import { getMessageData } from '@/io.ox/office/baseframework/utils/errormessages';
import { isConvertible, isEditable, isError, isNative, isTemplate, supportsEditMode } from '@/io.ox/office/baseframework/app/extensionregistry';
import { getRunningEditorAppForFile, prefetchDocsApp, launchDocsApp } from '@/io.ox/office/baseframework/app/appfactory';

import { GUARD_AVAILABLE } from '@/io.ox/office/editframework/utils/editconfig';
import { DEFAULT_FILE_BASE_NAME } from '@/io.ox/office/editframework/view/editlabels';

// constants ==================================================================

const FILE_VERSION_IS_UPLOADING_MSG = gt('This document cannot be edited at the moment because a new version is being uploaded. Please wait until the upload is complete.');

// private functions ==========================================================

/**
 * Returns whether the file in the folder described by the passed baton
 * object are considered to be editable.
 * The trash folder for example will be treated as read-only folder.
 *
 * @param {Object} baton
 *  The baton object providing access to a folder.
 *
 * @returns {jQuery.Promise}
 *  A Promise that will resolve with a Boolean value,
 *  specifying whether the folder is considered editable.
 */
function isEditableFolder(baton) {
    var fileDesc = baton.first();
    // no fileDesc = no trash folder (e.g. main portal)
    if (!fileDesc.id) { return $.when(true); }

    return getTrashState(fileDesc.folder_id).then(function (state) {
        return $.when(!state);
    });
}

/**
 * Returns whether the files in the folder described by the passed baton
 * object are considered to be editable and the current version.
 *
 * @param {Object} baton
 *  The baton object providing access to a folder.
 *
 * @returns {jQuery.Promise}
 *  A Promise that will resolve with a Boolean value,
 *  specifying whether the folder is considered editable and
 *  the file represents the current version.
 */
function isEditableFolderAndCurrentVersion(baton) {

    // check if file is editable
    var promise = isEditableFolder(baton);

    // check if file is the current version
    return promise.then(function (canEdit) {
        var fileDesc = baton.first();
        return canEdit && getCurrentFileVersionState(fileDesc, { cache: true });
    });
}

/**
 *  e.collection.has('one', 'read', 'modify') is the normal appsuite code
 *  e.baton.models[0].hasWritePermissions() is the new code for sharing
 *  Bug 40846
 */
function hasWritePermissions(e) {
    if (e.collection.has('one', 'read')) {
        if (e.collection.has('modify')) {
            return $.when(true);
        }
        if (e.models && e.models.length) {
            return e.models[0].hasWritePermissions();
        }
    }
    return $.when(false);
}

// public functions ===========================================================

/**
 * Creates all required actions and links in the OX Drive application for a
 * specific OX Documents application.
 *
 * @param {AppType} appType
 *  The type identifier of the application.
 *
 * @param {object} appConfig
 *  Additional configuration:
 *  - {string} appConfig.newDocumentLabel -- (required!) The label shown for
 *    the "Create new document" action link inserted into the toolbar of the
 *    Drive application.
 *  - {number} appConfig.newDocumentIndex -- (required!) The index of the
 *    "Create new document" action link that will be inserted into the tool bar
 *    of OX Drive. The action links of the various Documents editor
 *    applications will be ordered increasing by this index.
 *  - {boolean} [appConfig.viewerModeSupport=false] -- If set to `true`, the
 *    application can be plugged as a Viewer overlay application.
 */
export function createFileActions(appType, appConfig) {
    globalLogger.info(`$badge{launch} creating file actions for ${appType} application`);

    // root for extension points in OX Drive application
    var ACTION_POINT = appType;

    const moduleName = getEditorModulePath(appType);

    // private methods ----------------------------------------------------

    function getRunningAppForBaton(baton) {

        var fileDesc = baton.first();
        var fileName = fileDesc ? fileDesc.filename : null;
        if (!fileName || !isEditable(fileName, moduleName)) { return null; }

        const docApp = getRunningEditorAppForFile(appType, fileDesc);
        return docApp?.isImportSucceeded() ? docApp : null;
    }

    /**
     * Launches a new OX Documents application with the passed options.
     */
    function launchApplication(launchOptions) {
        if ((!launchOptions.file && !launchOptions.templateFile) || (launchOptions.file && !isFileVersionUploading(launchOptions.file.id, FILE_VERSION_IS_UPLOADING_MSG)) || (launchOptions.templateFile && !isFileVersionUploading(launchOptions.templateFile.id, FILE_VERSION_IS_UPLOADING_MSG))) {
            return launchDocsApp(appType, launchOptions);
        }
    }

    /**
     * Launches a new OX Documents application with the passed params in a new browser tab.
     */
    function launchApplicationInTab(params, targetTab) {
        if (!isFileVersionUploading(params.id, FILE_VERSION_IS_UPLOADING_MSG)) {
            openEditorChildTab(appType, params, targetTab);
        }
    }

    /**
     * Shows an error message for the given error object.
     */
    function showError(error) {
        yell('error', getMessageData(new ErrorCode(error)).message);
    }

    /**
     * Fetches application source code, if the editor application will not open
     * in a new browser tab.
     */
    function prefetchAppSources() {
        if (!canOpenDocsTab()) {
            prefetchDocsApp(appType);
        }
    }

    // initialization -----------------------------------------------------

    // Creates a new empty document in the current folder of the OX Drive application.
    createAction(ACTION_POINT + '-newblank', {
        toggle: supportsEditMode(appType),
        folder: 'create',
        matches(baton) {
            //Bug 40479
            //it is important to call getData before of "can" or "is" calls
            //DriveUtils does this already, but in this situation we dont have the id of the folder yet
            return baton.app.folder.getData().then(function (data) {
                if (_.isObject(data) && _.isString(data.id)) {
                    return getCreateState(data.id).then(function (createState) {
                        if (createState) {
                            return getWriteState(data.id).then(function (writeState) {
                                if (writeState) {
                                    return getTrashState(data.id).then(function (trashState) {
                                        return !trashState;
                                    });
                                } else {
                                    return false;
                                }
                            });
                        } else {
                            return false;
                        }
                    });
                } else {
                    // Bug 46390
                    // In case we cannot retrieve any data from the folder
                    // we cannot support the edit mode.
                    return false;
                }
            });
        },
        quick(baton) {
            return !isTrash(baton.folder_id);
        },
        action(baton) {
            if (canOpenDocsTab()) {
                var params = {
                    target_folder_id: baton.app.folder.get(),
                    target_filename: DEFAULT_FILE_BASE_NAME,
                    new: true
                };
                launchApplicationInTab(params);
                return;
            }
            launchApplication({ action: 'new', target_folder_id: baton.app.folder.get() });
        }
    });

    var editAction = function (baton, options) {
        var fileDescriptor = baton.first();
        var presentationMode = (options && options.presentationMode) || false;

        function launch(fileDescriptor) {
            var fileName = fileDescriptor.filename;
            var launchOptions = (fileName && isNative(fileName)) ? { action: 'load', file: fileDescriptor } : { action: 'convert', templateFile: fileDescriptor, preserveFileName: true };
            if (presentationMode) { launchOptions.presentationMode = true; }
            launchApplication(launchOptions);
        }

        function launchInNewTab(fileDescriptor) {
            var hasGuardExtension = hasGuardExt(fileDescriptor.filename),
                targetTab;

            if (isFileVersionUploading(fileDescriptor.id, FILE_VERSION_IS_UPLOADING_MSG)) {
                return $.when();
            }

            // Guard authorization
            var promise = hasGuardExtension ? authorizeGuardWithTab(baton) : Promise.resolve(null);

            // set authorization data to the model
            return promise.then(function (result) {
                var params = {
                    folder: encodeURIComponent(fileDescriptor.id)
                };
                if (fileDescriptor.folder_id !== undefined) {
                    _.extend(params, {
                        folder: fileDescriptor.folder_id,
                        id: fileDescriptor.id
                    });
                }

                if (result) {
                    if (hasGuardExtension) {
                        _.extend(params, { auth_code: result.authCode });
                    }
                    targetTab = result.targetTab;
                }

                if (presentationMode) { params.presentationMode = true; }

                launchApplicationInTab(params, targetTab);
            });
        }

        if (canOpenDocsTab()) {
            launchInNewTab(fileDescriptor);
        } else {
            launch(fileDescriptor);
        }
    };

    var templAction = function (baton) {
        var fileDesc = baton.first();
        if (canOpenDocsTab()) {
            var params = {
                folder: fileDesc.folder_id,
                id: fileDesc.id,
                destfolderid: getStandardDocumentsFolderId(),
                destfilename: DEFAULT_FILE_BASE_NAME,
                convert: true
            };
            launchApplicationInTab(params);
            return;
        }
        launchApplication({ action: 'convert', template: true, target_folder_id: getStandardDocumentsFolderId(), templateFile: fileDesc, target_filename: DEFAULT_FILE_BASE_NAME });
    };

    var templEditAction = function (baton) {
        var fileDesc = baton.first();
        var params = {
            folder: fileDesc.folder_id,
            id: fileDesc.id
        };
        if (fileDesc.filename && isNative(fileDesc.filename)) {
            if (canOpenDocsTab()) {
                launchApplicationInTab(params);
                return;
            }
            launchApplication({ action: 'load', file: fileDesc });
        } else {
            if (canOpenDocsTab()) {
                _.extend(params, {
                    preserveFileName: true,
                    convert: true
                });
                launchApplicationInTab(params);
                return;
            }
            launchApplication({ action: 'convert', target_folder_id: fileDesc.folder_id, templateFile: fileDesc, preserveFileName: true });
        }
    };

    // Loads an existing document from the current file in the OX Drive application.
    // If the document requires conversion to a supported file format, creates the
    // converted file and loads it.
    createAction(ACTION_POINT + '-edit', {
        collection: 'one && read',
        async matches(baton) {
            if (!this.quick(baton)) { return false; }
            prefetchAppSources();
            const canEdit = await hasWritePermissions(baton);
            return canEdit && isEditableFolderAndCurrentVersion(baton);
        },
        // TODO try to make matches sync
        quick(baton) {
            const filename = baton.first().filename;
            return !!filename &&
                isNative(filename, moduleName) &&
                !isTemplate(filename, moduleName) &&
                isEditable(filename, moduleName) &&
                !(DOCUMENTS_COLLABORA && hasGuardExt(filename)) &&
                !canEditDocFederated(baton.models[0]);
        },
        action: editAction
    });

    // --------------------------------------------------------------------
    // Extension point for using OX Presentation Editor as presenter for
    // pptx and odp files.
    // --------------------------------------------------------------------

    if (appType === 'presentation') {
        // Loads an existing document from the current file in the OX Drive application.
        // If the document requires conversion to a supported file format, creates the
        // converted file and loads it.
        createAction(ACTION_POINT + '-presentationmode', {
            collection: 'one && read',
            matches(baton) {
                if (!this.quick(baton)) { return false; }
                prefetchAppSources();
                return getCurrentFileVersionState(baton.first(), { cache: true });
            },
            // TODO try to make matches sync
            quick(baton) {
                const filename = baton.first().filename;
                return !!filename &&
                    isNative(filename, moduleName) &&
                    !isTemplate(filename, moduleName) &&
                    isEditable(filename, moduleName) &&
                    !DOCUMENTS_COLLABORA;
            },
            action(baton) {
                editAction(baton, { presentationMode: true });
            }
        });
    }

    createAction(ACTION_POINT + '-new-fromtemplate', {
        collection: 'one && read',
        matches(baton) {
            if (!this.quick(baton)) { return false; }
            return isEditableFolderAndCurrentVersion(baton);
        },
        quick(baton) {
            const filename = baton.first().filename;
            // workaround for Guest/anonymous
            // guest -> no new-from-template button!
            // TODO: make param newfolderid, this could happen in current folder, if there are create rights for the guest
            return !isGuest() &&
                !!filename &&
                !isError(filename) &&
                isTemplate(filename, moduleName);
        },
        action: templAction
    });

    //double click or enter in Drive, this must run before the default action in drive, because only the first one is called
    createAction('io.ox/files/actions/default', {
        id: moduleName,
        before: 'default',
        matches(obj) {
            var fileDesc = obj.first();
            var filename = fileDesc.filename;
            var folder_id = fileDesc.folder_id;

            if (!filename) { return false; }
            if (isTrash(folder_id)) { return false; }
            var fileModel = new FilesApi.Model(fileDesc);
            if (canEditDocFederated(fileModel)) { return false; }

            // new from template -> only read permissions of template file required
            if (isTemplate(filename, moduleName)) { return true; }

            if (!isNative(filename, moduleName)) { return false; }

            return canModify(fileDesc);
        },
        action(baton) {
            var filename = baton.first().filename;
            if (filename && isTemplate(filename, moduleName)) {
                if (hasGuardExt(filename)) {
                    if (!isNative(filename, moduleName)) { return false; }
                    templEditAction(baton);
                } else {
                    templAction(baton);
                }
            } else {
                editAction(baton);
            }
        }
    });

    // --------------------------------------------------------------------
    // Extension point for plugging the application into OX Viewer.
    // --------------------------------------------------------------------
    if (appConfig.viewerModeSupport) {
        ext.point(`${moduleName}/viewer/load/drive`).extend({
            id: 'default',
            index: 10000,
            launch(baton) {
                var fileDesc = {};
                var origData = null;

                var fileModel = baton.first();

                if (!fileModel) { return; }

                var cryptoAuth = null;

                if (fileModel.isFile()) {
                    fileDesc = fileModel.toJSON();
                    fileDesc.source = 'drive';
                    cryptoAuth = fileDesc.file_options?.params?.cryptoAuth || null;

                } else if (fileModel.isMailAttachment()) {
                    origData = fileModel.get('origData');
                    fileDesc.source = 'mail';
                    fileDesc.id = origData.mail.id;
                    fileDesc.folder_id = origData.mail.folder_id;
                    fileDesc.version = origData.id;
                    fileDesc.file_mimetype = fileModel.get('file_mimetype');
                    fileDesc.filename = fileModel.getDisplayName();
                    cryptoAuth = origData.auth || origData.security?.authentication || null;

                } else if (fileModel.isPIMAttachment()) {
                    origData = fileModel.get('origData');
                    if (origData.module === 1) {
                        fileDesc.source = 'calendar';
                    } else if (origData.module === 4) {
                        fileDesc.source = 'tasks';
                    } else if (origData.module === 7) {
                        fileDesc.source = 'contacts';
                    }

                    fileDesc.id = origData.attached;
                    fileDesc.folder_id = origData.folder;
                    fileDesc.version = origData.managedId || origData.id;
                    fileDesc.file_mimetype = fileModel.get('file_mimetype');
                    fileDesc.filename = fileModel.getDisplayName();

                } else if (fileModel.isComposeAttachment()) {
                    origData = fileModel.get('origData');
                    fileDesc.source = 'compose';
                    fileDesc.id = origData.space;
                    fileDesc.folder_id = origData.space;
                    fileDesc.version = origData.id;
                    fileDesc.file_mimetype = fileModel.get('file_mimetype');
                    fileDesc.filename = fileModel.getDisplayName();
                }

                return launchApplication({ action: 'load', file: fileDesc, plugged: true, page: baton.page, auth_code: cryptoAuth }).catch(showError);
            }
        });
    }

    // --------------------------------------------------------------------

    // Creates a new document as copy of the current file in the OX Drive application.
    if (supportsEditMode(appType)) {

        ////////////////////////////////////////////////////////////////////////

        var editAsNew = function (ref, requires, encrypted, startInPresentationMode) {
            createAction(ACTION_POINT + ref, {
                collection: 'one && read',
                matches(baton) {
                    if (!this.quick(baton)) { return false; }
                    return requires.call(this, baton);
                },
                quick(baton) {
                    const filename = baton.first().filename;
                    // workaround for Guest/anonymous
                    // guest -> no my files -> no edit-as-new button!
                    return !isGuest() &&
                        !!filename &&
                        !baton.originFavorites &&
                        isEditable(filename, moduleName) &&
                        !(DOCUMENTS_COLLABORA && hasGuardExt(filename));
                },
                action(baton) {

                    var fileDesc = baton.first();
                    var hasGuardExtension = hasGuardExt(fileDesc.filename);

                    if (isFileVersionUploading(fileDesc.id, FILE_VERSION_IS_UPLOADING_MSG)) {
                        return $.when();
                    }

                    // whether Guard authorization is needed
                    var authorize = encrypted || hasGuardExtension;

                    // Guard authorization
                    var promise = authorize ? authorizeGuardWithTab(baton) : Promise.resolve(null);

                    // set authorization data to the model
                    return promise.then(function (result) {

                        var convert = fileDesc.filename && isConvertible(fileDesc.filename);
                        var action = convert ? 'convert' : 'new';

                        if (canOpenDocsTab()) {

                            if (result) {

                                var fileModel = _.first(baton.models);
                                var fileName = fileModel.get('filename');
                                var fileId = fileModel.get('id');
                                var folderId = fileModel.get('folder_id');

                                getCreateState(folderId).always(function (state) {
                                    if (!state) { folderId = getStandardDocumentsFolderId(); }

                                    var params = {
                                        app: moduleName,
                                        folder: folderId,
                                        id: fileId,
                                        preserveFileName: convert
                                    };

                                    if (isNative(fileName)) {
                                        _.extend(params, {
                                            target_folder_id: folderId,
                                            target_filename: DEFAULT_FILE_BASE_NAME,
                                            new: true
                                        });
                                    } else {
                                        _.extend(params, {
                                            convert: true,
                                            destfolderid: folderId,
                                            destfilename: getFileBaseName(fileName)
                                        });
                                    }

                                    if (encrypted) {
                                        params.crypto_action = 'Encrypt';
                                        params.auth_code = result.authCode;
                                    }

                                    if (hasGuardExtension) {
                                        params.decrypt_auth_code = result.authCode;
                                    }

                                    if (startInPresentationMode) { params.presentationMode = true; }

                                    launchApplicationInTab(params, result.targetTab);
                                });

                            } else {

                                var params = {
                                    folder: fileDesc.folder_id,
                                    id: fileDesc.id,
                                    preserveFileName: convert
                                };

                                if (isNative(fileDesc.filename)) {
                                    _.extend(params, {
                                        target_folder_id: fileDesc.folder_id,
                                        target_filename: DEFAULT_FILE_BASE_NAME,
                                        new: true
                                    });
                                } else {
                                    _.extend(params, {
                                        convert: true,
                                        destfolderid: fileDesc.folder_id,
                                        destfilename: getFileBaseName(fileDesc.filename)
                                    });
                                }

                                if (startInPresentationMode) { params.presentationMode = true; }

                                launchApplicationInTab(params);
                            }
                            return;
                        }

                        var launchOptions = {
                            action,
                            target_folder_id: fileDesc.folder_id,
                            templateFile: fileDesc,
                            preserveFileName: convert
                        };

                        if (encrypted && result) {
                            launchOptions.cryptoAction = 'Encrypt';
                            launchOptions.auth_code = result.authCode;
                        }

                        if (hasGuardExtension && result) {
                            launchOptions.decrypt_auth_code = result.authCode;
                        }

                        if (startInPresentationMode) { launchOptions.presentationMode = true; }

                        launchApplication(launchOptions);
                    });
                }
            });
        };

        var isExtraEditAsNew = function (baton) {
            var filename = baton.first().filename;
            return !isError(filename) && !isTemplate(filename, moduleName) && isEditable(filename, moduleName);
        };

        var isNormalEditAsNew = function (baton) {
            var filename = baton.first().filename;

            if (!isNative(filename, moduleName)) {
                return $.when(false);
            }
            return hasWritePermissions(baton);
        };

        var isNormalEditAsNewEncrypted = function (baton) {
            var folder_id = baton.first().folder_id;
            var canEncrypt = false;

            if (GUARD_AVAILABLE && folder_id) {
                var folder = FolderAPI.pool.getModel(folder_id);
                canEncrypt = folder && !FolderAPI.isExternalFileStorage(folder.attributes);
            }
            return canEncrypt;
        };

        var isEncrypted = function (baton) {
            return hasGuardExt(baton.first().filename);
        };

        editAsNew('-edit-asnew', function (baton) {
            return isNormalEditAsNew(baton).then(function (canNormalEditAsNew) {
                return (isExtraEditAsNew(baton) && (canNormalEditAsNew || (!canNormalEditAsNew && isEncrypted(baton)))) &&  isEditableFolderAndCurrentVersion(baton);
            });
        }, false, false);

        editAsNew('-edit-asnew-hi', function (baton) {
            return isNormalEditAsNew(baton).then(function (canNormalEditAsNew) {
                return (isExtraEditAsNew(baton) && !canNormalEditAsNew && !isEncrypted(baton)) && isEditableFolderAndCurrentVersion(baton);
            });
        }, false, false);

        editAsNew('-edit-asnew-hi-encrypted', function (baton) {
            return isNormalEditAsNew(baton).then(function (canNormalEditAsNew) {
                return (isExtraEditAsNew(baton) && isNormalEditAsNewEncrypted(baton) && !canNormalEditAsNew && isEncrypted(baton)) && isEditableFolderAndCurrentVersion(baton);
            });
        }, true, false);

        editAsNew('-edit-asnew-encrypted', function (baton) {
            return isNormalEditAsNew(baton).then(function (canNormalEditAsNew) {
                return (isExtraEditAsNew(baton) && isNormalEditAsNewEncrypted(baton) && !(!canNormalEditAsNew && isEncrypted(baton))) && isEditableFolderAndCurrentVersion(baton);
            });
        }, true, false);

        // No more presenting of ppt, pps in OX Presentation (DOC-2787)
        // if (appType === 'presentation') {

        //     editAsNew('-present-asnew', function (baton) {
        //         if (isNative(baton.first().filename)) { return false; } // fast synchronous exit for 'pptx and 'odp' -> no 'present as new' for native file types
        //         return isNormalEditAsNew(baton).then(function (canNormalEditAsNew) {
        //             return (isExtraEditAsNew(baton) && (canNormalEditAsNew || (!canNormalEditAsNew && isEncrypted(baton)))) &&  isEditableFolderAndCurrentVersion(baton);
        //         });
        //     }, false, true);

        //     editAsNew('-present-asnew-hi', function (baton) {
        //         if (isNative(baton.first().filename)) { return false; } // fast synchronous exit for 'pptx and 'odp' -> no 'present as new' for native file types
        //         return isNormalEditAsNew(baton).then(function (canNormalEditAsNew) {
        //             return (isExtraEditAsNew(baton) && !canNormalEditAsNew && !isEncrypted(baton)) && isEditableFolderAndCurrentVersion(baton);
        //         });
        //     }, false, true);
        // }

        ////////////////////////////////////////////////////////////////////////

        createAction(ACTION_POINT + '-edit-template', {
            collection: 'one && read',
            async matches(baton) {
                if (!this.quick(baton)) { return false; }
                const canEdit = await hasWritePermissions(baton);
                return canEdit && isEditableFolderAndCurrentVersion(baton);
            },
            quick(baton) {
                const filename = baton.first().filename;
                return !!filename &&
                    // template files cannot be edited directly, if they would need to be converted to another file format
                    isNative(filename, moduleName) &&
                    isEditable(filename, moduleName) &&
                    // FIXME: Collabora opens template files in read-only mode
                    !DOCUMENTS_COLLABORA;
            },
            action(baton) {
                var filename = baton.first().filename;
                if (!isTemplate(filename, moduleName)) {
                    editAction(baton);
                } else {
                    templEditAction(baton);
                }
            }
        });
    }

    // when a file is open in documents, use documents download to flush doc before download
    createAction('io.ox/files/actions/download', {
        id: `${moduleName}/download-while-file-open`,
        index: 50,
        device: '!ios || ios >= 12',
        collection: 'one',
        matches(baton) {
            return !!getRunningAppForBaton(baton);
        },
        action(baton) {
            var app = getRunningAppForBaton(baton);
            if (app) { app.download(); }
        }
    });

    // add new empty document action to primary button in Drive
    ext.point('io.ox/secondary').extend(
        {
            id: appType + '-newblank',
            index: 350 + appConfig.newDocumentIndex,
            render(baton) {
                if (baton.appId !== 'io.ox/files') { return; }
                this.action(ACTION_POINT + '-newblank', appConfig.newDocumentLabel, baton);
            }
        });

    // add new empty document action to mobile toolbar in Drive
    ext.point('io.ox/files/toolbar/new').extend({
        index: 200 + appConfig.newDocumentIndex,
        id: appType + '-newblank',
        title: appConfig.newDocumentLabel,
        ref: ACTION_POINT + '-newblank'
    });

    createAction('io.ox/office/portal/open' + appType, {
        action: async () => {
            const { default: OpenDocumentDialog } = await import('@/io.ox/office/editframework/view/dialog/opendocumentdialog');
            try {
                await new OpenDocumentDialog(appType).show();
            } catch (err) {
                if (err !== 'cancel') { globalLogger.exception(err); }
            }
        }
    });

    function createLink(id, title, prio, index, indexViewer, ref, section, icon) {
        var link = {
            id: appType + '-' + id,
            index,
            prio,
            mobile: 'lo',
            title,
            tooltip: title,
            icon,
            ref: ACTION_POINT + '-' + ref
        };
        if (section) {
            link.section = section;
        }

        // @Deprecated old halo view
        //
        // TODO: looks like this is needed for mobile view, needs to be checked
        //
        var mobileLink = _.clone(link);
        delete mobileLink.icon; // -> not using the icon
        ext.point('io.ox/files/links/inline').extend(mobileLink);

        // define links for document actions in the OX Viewer toolbar.
        var viewerLink = _.clone(link);
        viewerLink.index = indexViewer;
        ext.point('io.ox/core/viewer/toolbar/links/drive').extend(viewerLink);
        ext.point('io.ox/core/viewer/toolbar/links/guardDrive').extend(_.clone(link));

        // define links for document actions in the OX Drive toolbar.
        ext.point('io.ox/files/toolbar/links').extend(_.clone(link));

        // define links for document actions in the OX Viewer version drop-down,
        // but only extend the point that represents the current version,
        // and not the one representing older versions.
        var dropdownLink = _.clone(link);
        delete dropdownLink.icon; // -> not using the icon
        ext.point('io.ox/files/versions/links/inline/current').extend(dropdownLink);
    }

    //         id                      title                          prio  index  index-viewer ref                        section    icon
    createLink('edit',                 gt('Edit'),                    'hi', 350,    150,        'edit',                    'edit', 'bi/pencil.svg');
    createLink('newfromtemplate',      gt('New from template'),       'hi', 350,    150,        'new-fromtemplate',        'edit', createIconMarkup('svg:new-from-template'));

    createLink('edittemplate',         gt('Edit template'),           'lo', 360,    160,        'edit-template',           'edit');

    createLink('editasnew',            gt('Edit as new'),             'lo', 360,    160,        'edit-asnew',              'edit');
    createLink('editasnewhi',          gt('Edit as new'),             'hi', 350,    150,        'edit-asnew-hi',           'edit');

    createLink('editasnewencrypted',   gt('Edit as new (encrypted)'), 'lo', 370,    170,        'edit-asnew-encrypted',    'edit');
    createLink('editasnewhiencrypted', gt('Edit as new (encrypted)'), 'hi', 360,    160,        'edit-asnew-hi-encrypted', 'edit');

    if (appType === 'presentation') {
        createLink('presentationmode', gt('Present'),                 'hi', 450,    650,        'presentationmode',        'edit', 'bi/play-circle.svg');

        // No more presenting of ppt, pps in OX Presentation (DOCS-2787)
        // createLink('presentasnew',     gt('Present'),                 'lo', 460,  'present-asnew',           'edit', 'bi:play-circle');
        // createLink('presentasnewhi',   gt('Present'),                 'hi', 450,  'present-asnew-hi',        'edit', 'bi:play-circle');
    }

    // add contextmenu entry in drive
    ext.point('io.ox/files/listview/contextmenu').extend({
        id: appType + '-edit',
        index: 150,
        ref: ACTION_POINT + '-edit',
        section: '10',
        title: gt('Edit')
    });
}
