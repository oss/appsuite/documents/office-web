/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { AsyncWorker } from "@/io.ox/office/tk/workers";

import { TRANSFER_EDIT_RIGHTS_TIMEOUT } from "@/io.ox/office/editframework/utils/editconfig";
import type { AlertBannerConfig } from "@/io.ox/office/baseframework/view/popup/alertbanner";
import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// constants ==================================================================

// initial delay before showing the first alert banner, in milliseconds
const ACQUIRE_DELAY = 3_000;

// the delay for showing the second alert banner, in milliseconds
const TRY_AGAIN_TIMEOUT = TRANSFER_EDIT_RIGHTS_TIMEOUT + 10_000;

// singletons =================================================================

/**
 * Logs everything related for debugging edit right transfers on the client.
 * Bound to the debug option "office:log-edittransfer".
 */
export const acquireEditLogger = new Logger("office:log-edittransfer", {
    recordAll: true,
    tag: "RT-EDIT",
    tagColor: 0xFF0000
});

// class AcquireEditRightsWorker ==============================================

/**
 * A worker that executes all steps to acquire the edit rights of a document
 * from another user, and shows the progress of this process in the GUI by
 * painting some alert banners.
 *
 * Heuristic: We are relying on heuristics here because we don't get an
 * information in the moment when the editor actually accept/decline the
 * request. We get this information not until the whole transfer process is
 * finished. This contains also saving the document, which can't be estimated
 * (or only very roughly).
 *
 * To hide this from the user, the time period shown to user A is 3 seconds
 * longer than it is estimated. The idea is that it feels better when the
 * request is accepted before the time is up usually, than getting a message
 * that it takes longer as estimated. A side effect is that when the request is
 * accepted at once but the saving takes very long, that user A get displayed
 * that the decision is still pending. This is a trade-off which has to be made
 * at the moment.
 *
 * Furthermore this delay seems high enough so that the yell isn't now shown at
 * all at a fast edit rights transfer (e.g. when the user is inactive).
 */
export class AcquireEditRightsWorker extends AsyncWorker {

    readonly docApp: EditApplication;

    #runningPromise: Opt<Promise<void>>;
    #alertConfig: Opt<AlertBannerConfig>;

    // constructor ------------------------------------------------------------

    constructor(docApp: EditApplication) {
        super();

        this.docApp = docApp;

        // immediately reset cached promise (to be able to restart immediately after abort)
        this.on("worker:finish", () => { this.#runningPromise = undefined; });

        // log start/end of edit rights transfer
        this.on("worker:start", () => acquireEditLogger.info("AcquireEditRightsWorker: start acquiring edit rights"));
        this.on("worker:finish", result => acquireEditLogger.info("AcquireEditRightsWorker: acquiring edit rights finished:", result));

        // Step 1: Send the RT server request for acquiring edit rights.
        this.steps.addStep(() => this.#sendAcquireRequest());

        // Step 2: Start a timer in the background that sends a forced edit request.
        // The timer will be started from the `context` object that will be destroyed
        // automatically when this worker finishes its execution cycles. Therefore,
        // the timer will be aborted automatically, if transferring edit rights has
        // finished. The step does not return a promise so the timer will really run
        // in the background, while this worker sends requests and shows alert banners.
        this.steps.addStep(runner => {
            acquireEditLogger.log("$badge{AcquireEditRightsWorker} force timer started");
            // start background timer (worker will not await it)
            runner.setTimeout(() => {
                acquireEditLogger.log("$badge{AcquireEditRightsWorker} force timer fired");
                const { rtConnection } = this.docApp;
                if (rtConnection) {
                    acquireEditLogger.log("$badge{AcquireEditRightsWorker} forceEditRights now");
                    jpromise.floating(rtConnection.forceEditRights());
                }
            }, 2 * TRANSFER_EDIT_RIGHTS_TIMEOUT);
        });

        // Step 3: Wait for a few seconds before showing the first alert banner.
        this.steps.addDelay(ACQUIRE_DELAY);

        // Step 4: show an alert banner with a progress bar.
        this.steps.addStep(() => {
            const editUserName = this.docApp.getEditClientUserName();
            const message = editUserName ?
                //#. %1$s is the user name that has currently edit rights.
                gt("%1$s is being asked to grant you edit rights. This might take some time.", editUserName) :
                gt("The server is being asked to grant you edit rights. This might take some time.");
            this.#showAlert(message, TRANSFER_EDIT_RIGHTS_TIMEOUT);
        });

        // Step 5: Wait for the alert banner duration timer.
        this.steps.addDelay(TRANSFER_EDIT_RIGHTS_TIMEOUT);

        // Step 6: Show the second alert banner with a progress bar.
        this.steps.addStep(() => {
            const message = gt("The request takes longer than expected, trying again.");
            this.#showAlert(message, TRY_AGAIN_TIMEOUT);
        });

        // Step 7: Wait for the alert banner duration timer. This is important so that this worker
        // does not finish too early which would abort the background force timer from step 2.
        this.steps.addDelay(TRY_AGAIN_TIMEOUT);
    }

    // public methods ---------------------------------------------------------

    /**
     * Sends a new server request to acquire edit rights from the current edit
     * user. If such a request is currently running, the appropriate alert
     * banner will be restored, and the server request will be sent again.
     *
     * @returns
     *  A promise that will fulfil when the worker has finished successfully,
     *  or that will reject when the worker has failed or was aborted.
     */
    override start(): Promise<void> {

        // allow to re-enter the worker while it is running
        if (this.#runningPromise) {
            acquireEditLogger.info("AcquireEditRightsWorker: reentered running process, trying to resend server request");

            // restore alert banner (closes automatically on losing focus)
            if (this.#alertConfig) {
                this.docApp.docView.yell(this.#alertConfig);
            }

        } else {

            // start the worker process
            this.#runningPromise = super.start();
        }

        return this.#runningPromise;
    }

    // private methods --------------------------------------------------------

    async #sendAcquireRequest(): Promise<void> {

        // RT connection may have gone already while closing the application
        const rtConnection = this.docApp.rtConnection;
        if (!rtConnection) { return; }

        // send the server request
        try {
            acquireEditLogger.log("AcquireEditRightsWorker: sending server request for edit rights");
            await rtConnection.acquireEditRights();
        } finally {
            acquireEditLogger.log("AcquireEditRightsWorker: server request for edit rights finished");
        }
    }

    /**
     * Shows an alert banner with progress bar, and stores the configuration
     * for the alert banner internally for restoration.
     */
    #showAlert(message: string, duration: number): void {
        // restore alert when switching applications
        this.#alertConfig = { message, duration, progress: Date.now(), restoreHidden: true };
        this.docApp.docView.yell(this.#alertConfig);
    }
}
