/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import gt from 'gettext';

import { str, jpromise } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { authorizeGuardWithTab, getStandardDocumentsFolderId } from '@/io.ox/office/tk/utils/driveutils';
import { BROWSER_TAB_SUPPORT, openEditorChildTab, openPortalChildTab } from '@/io.ox/office/tk/utils/tabutils';

import { spellingSettings } from '@/io.ox/office/settings/api';
import MissingSpellingDialog from '@/io.ox/office/settings/view/dialog/missingspellingdialog';

import { BaseController } from '@/io.ox/office/baseframework/controller/basecontroller';
import { getTemplateExtension } from '@/io.ox/office/baseframework/app/extensionregistry';

import { SaveAsType, DEBUG, GUARD_AVAILABLE, CONVERTER_AVAILABLE, COMBINED_TOOL_PANES, searchLogger, getDebugFlag } from '@/io.ox/office/editframework/utils/editconfig';
import { DEFAULT_FILE_BASE_NAME } from '@/io.ox/office/editframework/view/editlabels';
import { SaveAsFileDialog, SaveAsTemplateDialog } from '@/io.ox/office/editframework/view/editdialogs';

// types ==================================================================

/**
 * Specifiers for a "Search" command in the document.
 */
export const SearchCommand = {

    /**
     * The configuration options (e.g. case-sensitivity, usage of regular
     * expressions) have been changed.
     */
    CONFIG: 'search:config',

    /**
     * Show a preview of matching search results while typing in the search
     * text field.
     */
    PREVIEW: 'search:preview',

    /**
     * Start a new search (e.g. after changing the search query string),
     * possibly from the current position in the document.
     */
    START: 'search:start',

    /**
     * Find the next occurrence of the search query string.
     */
    NEXT: 'search:next',

    /**
     * Find the previous occurrence of the search query string.
     */
    PREV: 'search:prev',

    /**
     * Finalize the current search command, e.g. after hiding the "Search"
     * toolbar.
     */
    END: 'search:end'
};

/**
 * Specifiers for a "Replace" command in the document.
 */
export const ReplaceCommand = {

    /**
     * Replace the next occurrence of the search query string.
     */
    NEXT: 'replace:next',

    /**
     * Replace all occurrences of the search query string at once.
     */
    ALL: 'replace:all'
};

// class EditController =======================================================

/**
 * The base class for controller classes of all OX Documents allowing to
 * edit a document.
 *
 * @param {EditApplication} docApp
 *  The application that has created this controller instance.
 *
 * @param {EditModel} docModel
 *  The document model created by the passed application.
 *
 * @param {EditView} docView
 *  The document view created by the passed application.
 */
export class EditController extends BaseController {

    constructor(docApp, docModel, docView) {

        // base constructor
        super(docApp, docModel, docView);

        // self reference
        var self = this;

        // the active undo manager
        var undoManager = null;

        // the current search query string
        var searchQuery = '';
        // the current replacement string
        var replaceString = '';

        // the current search configuration
        var searchConfig = {
            withcase: false,
            regexp: false
        };

        // the last cached search result
        var searchResult = {
            count: 0,
            complete: true
        };

        // whether the undo/controler items are enabled.
        var undoRedoEnabled = true;

        // temporary unsaved filename currently edited in a textfield
        var unsavedFileName = null;

        // private methods ----------------------------------------------------

        /**
         * Saves the current document to the specified folder using the
         * current file name without extension and prefixing it with
         * the phrase 'Copy of'.
         *
         * @param {SaveAsType} fileType
         *  The target file type to save the document to.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  - {boolean} [options.encrypted=false]
         *    If set to `true`, the document will be encrypted (secondary file
         *    extension "pgp" will be added). Currently only supported for file
         *    type `NATIVE`.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when the document has been saved
         *  successfully.
         */
        function showSaveAsDialog(fileType, options) {

            // select and notify unsaved comment (promise rejects if unsaved comment is available)
            var promise = docView.processUnsavedComment();

            // create the dialog instance depending on the file type
            promise = promise.then(function () {

                switch (fileType) {

                    case SaveAsType.NATIVE:
                        return new SaveAsFileDialog(docView,
                            // dialog title depends on encration
                            (options && options.encrypted) ? gt('Save as (encrypted)') : gt('Save as'),
                            //#. %1$s is the name of the original file used to create a new copy
                            _.noI18n.fix(gt('Copy of %1$s', docApp.getShortFileName()))
                        );

                    case SaveAsType.TEMPLATE:
                        return new SaveAsTemplateDialog(docView,
                            //#. %1$s is the file extension used for the template file
                            gt('Save as template (%1$s)', getTemplateExtension(docApp.getFullFileName())),
                            docApp.getShortFileName()
                        );

                    case SaveAsType.PDF:
                        return new SaveAsFileDialog(docView, gt('Export as PDF'), docApp.getShortFileName());

                    default:
                        globalLogger.error('$badge{EditController} showSaveAsDialog: unknown file type "' + fileType + '"');
                        return jpromise.reject();
                }
            });

            // show the dialog
            promise = promise.then(function (dialog) { return dialog.show(); });

            // perform the "Save As" action
            promise = promise.then(function (result) {

                // the new file name (trim NPCs and spaces at beginning and end, replace embedded NPCs)
                var shortName = _.isString(result.fileName) ? str.trimAndCleanNPC(result.fileName) : null;
                if (!shortName) {
                    globalLogger.error('$badge{EditController} showSaveAsDialog: missing file name');
                    return jpromise.reject();
                }

                // save the document to the specified file format
                return docApp.saveDocumentAs(result.folderId, shortName, fileType, options);
            });

            return promise;
        }

        function implExecuteSearch(command) {
            searchLogger.trace(function () { return '$badge{SearchHandler} implExecuteSearch: command="' + command + '" query="' + searchQuery + '"'; });
            return self.executeSearch(command, searchQuery, searchConfig);
        }

        function implExecuteReplace(command) {
            searchLogger.trace(function () { return '$badge{SearchHandler} implExecuteReplace: command="' + command + '" query="' + searchQuery + '" replace="' + replaceString + '"'; });
            return self.executeReplace(command, searchQuery, replaceString, searchConfig);
        }

        // public methods -----------------------------------------------------

        /**
         * Registers an undo manager that will be used for undo/redo controller
         * items, instead of the undo manager provided by the document model.
         *
         * @param {IUndoManager} newUndoManager
         *  The undo manager to be used by this controller.
         */
        this.registerUndoManager = function (newUndoManager) {
            if (undoManager !== newUndoManager) {
                this.stopListeningTo(undoManager);
                undoManager = newUndoManager;
                this.updateOnAllEvents(undoManager);
                this.update();
            }
        };

        /**
         * Reactivates the undo manager of the document model to be used for
         * undo/redo controller itens, instead of the undo manager that has
         * been registered with the method `registerUndoManager()`.
         */
        this.restoreUndoManager = function () {
            this.registerUndoManager(docModel.undoManager);
        };

        /**
         * Enable or disable the undo/redo controler items. By default it is enabled.
         *
         * @param {boolean} enable
         *  True to enable the undo/redo controler items otherwise false.
         */
        this.enableUndoRedo = function (enable) {
            if (undoRedoEnabled !== !!enable) {
                undoRedoEnabled = !!enable;
                this.update();
            }
        };

        /**
         * Returns the unsaved filename currently being edited in a textfield.
         *
         * @returns {Opt<string>}
         *  The current unsaved filename, if available and different from the
         * filename in the own file descriptor; otherwise `undefined`.
         */
        this.getUnsavedFileName = function () {
            return (unsavedFileName && (unsavedFileName !== docApp.getShortFileName())) ? unsavedFileName : undefined;
        };

        // initialization -----------------------------------------------------

        // register all controller items
        this.registerItems({

            // application and view

            // enabled unless the application is in 'internal error' state
            'app/valid': {
                parent: 'app/imported',
                enable() { return !docApp.isInternalError(); }
            },

            // dummy item for the application status label (updates itself internally)
            'view/appstatuslabel': {},

            // dummy item for the "View" drop-down menu
            'view/settings/menu': {},

            // enabled whan multiple browser tabs are supported
            'view/multitab': {
                enable() { return BROWSER_TAB_SUPPORT; }
            },

            // controller item used for visibility of GUI elements in "combined panes" mode
            // (toppane hidden, main toolpane contains additional controls such as "Quit" button)
            'view/combinedpanes': {
                enable() { return COMBINED_TOOL_PANES; }
            },

            // special visibility key for "Quit" buttons
            'view/quit/visible': {
                parent: ['app/quit', '!view/multitab']
            },

            // special visibility key for "Quit" button for "combined panes" mode
            'view/quit/combinedpanes/visible': {
                parent: ['view/quit/visible', 'view/combinedpanes']
            },

            // toggle visibility of the main toolpane
            'view/toolpane/show': {
                parent: '!view/combinedpanes',
                // always enabled (also in read-only mode, and in application error state)
                get() { return docView.mainPane.isVisible(); },
                set(state) {
                    jpromise.floating(docApp.setUserSettingsValue('showToolbars', state)); // save in background
                    docView.mainPane.toggle(COMBINED_TOOL_PANES || state);
                }
            },

            // the identifier of the active toolpane
            'view/toolbars/tab': {
                // always enabled (also in read-only mode, and in application error state)
                get() { return docView.toolPaneManager.getActiveTabId() || ''; },
                set(tabId) { docView.toolPaneManager.activateToolPane(tabId); },
                focusTarget(sourceType) {
                    // when activating via ENTER key, move focus into tool pane
                    return (sourceType === 'keyboard') ? docView.mainPane : null;
                }
            },

            'view/toolpane/file/visible': {
                // "File" toolpane is always visible
            },

            'view/toolbars/tab/combinedpanes/visible': {
                parent: 'view/combinedpanes',
                // bug 57992: hide dropdown menu in application error state (but not in debug mode to have access to debug toolbar)
                enable() { return DEBUG || !docApp.isInternalError(); }
            },

            'view/new/menu': {
                parent: ['app/valid', 'view/multitab']
            },

            // dummy item for the 'Save in Drive' drop-down menu
            'view/saveas/menu': {
                parent: 'app/valid'
            },

            // dummy item for the "Character Format" dropdown menu for small devices
            'view/character/format/menu': {
                parent: 'app/valid'
            },

            // dummy item for the "Paragraph Alignment" drop-down menu for drawing objects
            'view/drawing/alignment/menu': {
                parent: 'app/valid'
            },

            // dummy item for the "Paragraph Spacing" drop-down menu for drawing objects
            'view/drawing/spacing/menu': {
                parent: 'app/valid'
            },

            // This item represents the visibility of the search controls in the GUI.
            // - In "expanded panes" mode, this is the self-contained `SearchPane` below the main toolpane.
            // - In "combined panes" mode, this is the "search" section of the combined main toolpane (while the
            //   `SearchPane` below the main toolpane will be used to show the "replace" controls only).
            // This is important to make the keyboard shortcut work correctly in both viewpane modes.
            'view/pane/search': {
                parent: 'app/valid',
                get() {
                    return COMBINED_TOOL_PANES ? (docView.toolPaneManager.getActiveTabId === "search") : docView.searchPane.isVisible();
                },
                set(state) {
                    if (COMBINED_TOOL_PANES) {
                        if (state === true) {
                            docView.toolPaneManager.activateToolPane("search");
                        }
                    } else {
                        docView.searchPane.toggle(state);
                    }
                },
                focusTarget() {
                    // set focus into available search textfield regardless of viewpane mode
                    return docApp.getWindowNode().find('.tool-pane [data-key="document/search/text"] input');
                },
                // shortcut always enables the search pane (no toggling)
                shortcut: { keyCode: 'F', ctrlOrMeta: true, value: true }
            },

            // This item represents the "Search" tab button in the toolpane dropdown menu in "combined paned" mode. It
            // is used to activate the "search" section in the combined main toolpane, as well as for toggling the
            // "replace" controls shown in the `SearchPane` below the main toolpane.
            'view/toolpane/combinedpanes/search': {
                parent: 'view/combinedpanes',
                set() {
                    if (docView.toolPaneManager.getActiveTabId() === "search") {
                        docView.searchPane.toggle();
                    } else {
                        docView.toolPaneManager.activateToolPane("search");
                    }
                }
            },

            // document

            // parent iten that is enabled if the edited document is based on OOXML file format
            'document/ooxml': {
                enable() { return docApp.isOOXML(); }
            },

            // parent iten that is enabled if the edited document is based on ODF file format
            'document/odf': {
                enable() { return docApp.isODF(); }
            },

            'document/acquireedit': {
                parent: 'app/valid',
                enable() { return docApp.isAcquireEditRightsEnabled(); }
            },

            'document/acquireeditOrPending': {
                parent: 'app/valid',
                enable() { return docApp.isAcquireEditRightsEnabledPendingUpdates(); },
                set() { docApp.acquireEditRights(); }
            },

            'document/ot/enabled': {
                parent: 'app/imported',
                enable() { return docApp.isOTEnabled(); }
            },

            'document/reload': {
                parent: 'app/imported',
                enable() { return docApp.isReloadEnabled(); },
                set() { docApp.reloadDocument(); }
            },

            // to be used as parent item for all items that require edit mode
            'document/editable': {
                parent: 'app/valid',
                enable() { return docApp.isEditable(); }
            },

            // to be used as parent item for all items that require read-only mode
            'document/readonly': {
                parent: 'app/valid',
                enable() { return !docApp.isEditable(); }
            },

            'document/fullname': {
                get() { return docApp.getFullFileName(); }
            },

            'document/rename': (function () {
                var pendingFileName;
                return {
                    parent: ['document/editable', 'app/bundled'],
                    enable() { return docApp.isRenameEnabled(); },
                    get() {
                        // Bug 65339: Return the new filename from this item while the rename action is still running
                        // (and the new filename is not stored in the application yet), to prevent flickering old/new
                        // filenames in textfields.
                        return pendingFileName || docApp.getShortFileName();
                    },
                    preview(fileName) {
                        unsavedFileName = fileName;
                    },
                    set(fileName) {
                        pendingFileName = fileName;
                        return docApp.rename(fileName).always(() => { pendingFileName = undefined; });
                    },
                    async: 'background'
                };
            }()),

            // getter returns the number of available undo actions, setter applies the next undo action
            'document/undo': {
                parent: 'document/editable',
                enable() { return undoRedoEnabled && (this.value > 0); },
                get() { return undoManager.getUndoCount(); },
                set() { return undoManager.undo(); },
                shortcut: [
                    // bug 33077: restrict to application pane, to not interfere with text field's
                    // native undo/redo and automatic item execution after losing focus
                    { keyCode: 'Z', ctrlOrMeta: true, scope: '.app-pane' },
                    { keyCode: 'BACKSPACE', alt: true, scope: '.app-pane' }
                ]
            },

            // getter returns the number of available redo actions, setter applies the next redo action
            'document/redo': {
                parent: 'document/editable',
                enable() { return undoRedoEnabled && (this.value > 0); },
                get() { return undoManager.getRedoCount(); },
                set() { return undoManager.redo(); },
                shortcut: [
                    // bug 33077: restrict to application pane, to not interfere with text field's
                    // native undo/redo and automatic item execution after losing focus
                    { keyCode: 'Y', ctrlOrMeta: true, scope: '.app-pane' },
                    { keyCode: 'BACKSPACE', shift: true, alt: true, scope: '.app-pane' }
                ]
            },

            // base item for "Save as" actions requiring an unencrypted document
            'document/saveas/unencrypted': {
                parent: ['app/valid', '!app/encrypted']
            },

            'document/saveas/native/dialog': {
                parent: 'document/saveas/unencrypted',
                set() { return showSaveAsDialog(SaveAsType.NATIVE); }
            },

            'document/saveas/template/dialog': {
                parent: 'document/saveas/unencrypted',
                enable() { return !docApp.isRescueDocument(); },
                set() { return showSaveAsDialog(SaveAsType.TEMPLATE); }
            },

            'document/saveas/encrypted/dialog': {
                parent: 'app/valid',
                enable() { return GUARD_AVAILABLE && !docApp.isExternalStorage() && !docApp.isRescueDocument(); },
                set() { return showSaveAsDialog(SaveAsType.NATIVE, { encrypted: true }); }
            },

            'document/saveas/pdf/dialog': {
                parent: 'document/saveas/unencrypted',
                enable() { return CONVERTER_AVAILABLE && !docApp.isRescueDocument(); },
                set() { return showSaveAsDialog(SaveAsType.PDF); }
            },

            'document/autosave': {
                parent: 'document/editable',
                // auto-save is always enabled, trying to toggle it results in an alert box (see below)
                get() { return true; },
                set() {
                    docView.yell({
                        headline: gt('What is AutoSave?'),
                        message: gt('In OX Documents all your changes are saved automatically through AutoSave. It is not necessary to save your work manually.')
                    });
                },
                shortcut: { keyCode: 'S', ctrlOrMeta: true }
            },

            'document/users': {
                parent: 'app/valid',
                enable() { return (docApp.getActiveClients().length > 1) || getDebugFlag('office:collaborator-menu'); },
                get() { return docView.collaboratorMenu.isVisible(); },
                set(state) { docView.collaboratorMenu.toggle(state); }
            },

            // base item for search/replace
            'document/search': {
                // enabled in read-only mode
                parent: 'app/valid'
            },

            // text item for setting/changing the search query string
            'document/search/text': {
                parent: 'document/search',
                get() { return searchQuery; },
                set(query, settings) {
                    searchQuery = query;
                    // bug 46401: do not trigger search action when losing textfield focus (sourceType "blur" is triggered)
                    return (settings.sourceType === 'blur') ? undefined : implExecuteSearch(SearchCommand.START);
                },
                preview(query) {
                    // keep quicksearch query results when stopping editing in textfield ("query" is `undefined`)
                    if (query !== undefined) {
                        searchQuery = query;
                        return implExecuteSearch(SearchCommand.PREVIEW);
                    }
                }
            },

            // toggle item for usage of case sensitive matching in search/replace
            'document/search/withcase': {
                parent: 'document/search',
                get() { return searchConfig.withcase; },
                set(state) {
                    searchConfig.withcase = state;
                    return implExecuteSearch(SearchCommand.CONFIG);
                }
            },

            // toggle item for usage of regular expressions in search/replace
            'document/search/regexp': {
                parent: 'document/search',
                get() { return searchConfig.regexp; },
                set(state) {
                    searchConfig.regexp = state;
                    return implExecuteSearch(SearchCommand.CONFIG);
                }
            },

            // helper item for transporting the number of matches (needed for info badge in "Search" text field)
            'document/search/result': {
                parent: 'document/search',
                get() { return searchResult; },
                set(result) { _.assign(searchResult, result); },
                preserveFocus: true
            },

            'document/search/start': {
                parent: 'document/search',
                set() { return implExecuteSearch(SearchCommand.START); }
            },

            'document/search/prev': {
                parent: 'document/search',
                set() { return implExecuteSearch(SearchCommand.PREV); },
                shortcut: [{ keyCode: 'G', ctrlOrMeta: true, shift: true }, { keyCode: 'F3', shift: true }]
            },

            'document/search/next': {
                parent: 'document/search',
                set() { return implExecuteSearch(SearchCommand.NEXT); },
                shortcut: [{ keyCode: 'G', ctrlOrMeta: true }, { keyCode: 'F3' }]
            },

            'document/search/end': {
                parent: 'document/search',
                set() {
                    searchQuery = replaceString = '';
                    return implExecuteSearch(SearchCommand.END);
                }
            },

            'document/search/close': {
                parent: 'document/search',
                set() { return docView.searchPane.hide(); }
            },

            // base item for replace, disabled in read-only documents
            'document/replace': {
                parent: ['document/search', 'document/editable']
            },

            'document/replace/text': {
                parent: 'document/replace',
                get() { return replaceString; },
                set(text) { replaceString = text; }
            },

            'document/replace/next': {
                parent: 'document/replace',
                set() { return implExecuteReplace(ReplaceCommand.NEXT); }
            },

            'document/replace/all': {
                parent: 'document/replace',
                set() { return implExecuteReplace(ReplaceCommand.ALL); }
            },

            // open new document in a child browser tab
            'document/new': {
                parent: 'app/valid',
                set(appType) {
                    openEditorChildTab(appType, {
                        target_folder_id: getStandardDocumentsFolderId(),
                        target_filename: DEFAULT_FILE_BASE_NAME,
                        new: true
                    });
                }
            },

            // open new encrypted document in a child browser tab
            'document/new/encrypted': {
                parent: 'app/valid',
                enable() { return GUARD_AVAILABLE; },
                set(appType) {
                    return authorizeGuardWithTab().then(function (result) {
                        openEditorChildTab(appType, {
                            target_folder_id: getStandardDocumentsFolderId(),
                            target_filename: DEFAULT_FILE_BASE_NAME,
                            crypto_action: 'Encrypt',
                            auth_code: result.authCode,
                            new: true
                        }, result.targetTab);
                    });
                }
            },

            // Availability should only be dependent on conditions defined in 'allowStoreLogFile'
            'debug/downloadreport': {
                enable() { return docApp.allowStoreLogFile(); },
                set() { return docApp.createCrashDataAsDownload(); },
                shortcut: [{ keyCode: 'D', shift: true, ctrlOrMeta: true, alt: true }]
            },

            'document/new/fromTemplate': {
                parent: 'app/valid',
                set() {
                    openPortalChildTab(docApp.appType);
                }
            },

            'document/open': {
                parent: ['app/valid', 'view/multitab'],
                set() { return docView.showOpenDocumentDialog(); }
            },

            'document/settings/missingspelling/dialog': {
                enable() { return spellingSettings.hasLanguageWithoutSpelling(); },
                set() {
                    if (spellingSettings.hasLanguageWithoutSpelling()) {
                        return new MissingSpellingDialog().show();
                    }
                }
            },

            'format/painter': {
                parent: 'document/editable',
                enable() {
                    var selection = docModel.getSelection();
                    return (!selection.isMultiSelection() && !selection.isSlideSelection());
                },
                get() { return docModel.isFormatPainterActive(); },
                set(state) { docModel.handleFormatPainter(state, { reset: true }); }
            }

        });

        // initially register the undo manager of the document model
        docApp.onInit(() => {
            undoManager = docModel.undoManager;
            this.updateOnAllEvents(undoManager);
        });

        // update controller on more events after import
        this.waitForImport(() => {
            // ... after operations, or changed state of edit mode
            this.updateOnEvent(docApp, ["docs:state:error", "docs:users", "docs:editmode"]);
            // ... when changing active toolpanes
            this.updateOnAllEvents(docView.toolPaneManager);
            // ... when toggling the collaborator pop-up menu
            this.updateOnEvent(docView.collaboratorMenu, ["popup:show", "popup:hide"]);
        });

        // automatically close modal dialogs, if the document switches to read-only mode
        this.listenTo(docView, 'dialog:show', (dialog, config) => {
            if (!config?.showReadOnly) {
                dialog.listenTo(docApp, 'docs:editmode:leave', () => dialog.close());
            }
        });
    }

    // search/replace API -----------------------------------------------------

    /**
     * Will be invoked from controller items implementing a "Search" command.
     *
     * @param {SearchCommand} _command
     *  The search command to be executed.
     *
     * @param {string} _query
     *  The search query string.
     *
     * @param {SearchConfig} _config
     *  Additional configuration settings.
     *
     * @returns {MaybeAsync}
     *  May return a promise for asynchronous work. In this case, the view will
     *  be locked until the promise will settle.
     */
    executeSearch(_command, _query, _config) {
        searchLogger.error('$badge{EditController} executeSearch: missing implementation of abstract method');
        return undefined;
    }

    /**
     * Will be invoked from controller items implementing a "Replace" command.
     *
     * @param {ReplaceCommand} _command
     *  The replace command to be executed.
     *
     * @param {string} _query
     *  The search query string.
     *
     * @param {string} _replace
     *  The replacement text for the matches.
     *
     * @param {SearchConfig} _config
     *  Additional configuration settings.
     *
     * @returns {MaybeAsync void}
     *  May return a promise for asynchronous work. In this case, the view will
     *  be locked until the promise will settle.
     */
    executeReplace(_command, _query, _replace, _config) {
        searchLogger.error('$badge{EditController} executeReplace: missing implementation of abstract method');
        return undefined;
    }
}
