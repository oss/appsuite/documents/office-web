/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { BaseController } from "@/io.ox/office/baseframework/controller/basecontroller";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import { EditView } from "@/io.ox/office/editframework/view/editview";
import { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// types ======================================================================

export enum SearchCommand {
    CONFIG = "search:config",
    PREVIEW = "search:preview",
    START = "search:start",
    NEXT = "search:next",
    PREV = "search:prev",
    END = "search:end"
}

export enum ReplaceCommand {
    NEXT = "replace:next",
    ALL = "replace:all"
}

export interface SearchConfig {
    withcase: boolean;
    regexp: boolean;
}

export interface SearchResult {
    count: number;
    complete: boolean;
}

// class EditController =======================================================

export abstract class EditController extends BaseController {

    declare readonly docApp: EditApplication;
    declare readonly docModel: EditModel;
    declare readonly docView: EditView;

    protected constructor(docApp: EditApplication, docModel: EditModel, docView: EditView, config?: unknown);

    protected abstract executeSearch(command: SearchCommand, query: string, config: SearchConfig): MaybeAsync;
    protected abstract executeReplace(command: ReplaceCommand, query: string, replace: string, config: SearchConfig): MaybeAsync;
}
