/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';
import ox from '$/ox';
import { locationHash } from '$/url';

import ConverterUtils from '$/io.ox/core/tk/doc-converter-utils';
import UserAPI from '$/io.ox/core/api/user';
import TabApi from '$/io.ox/core/api/tab';

import { str, ary, pick, jpromise } from '@/io.ox/office/tk/algorithms';
import { createDiv, createSpan, createIcon, createButton, getSchemeColorIndex } from '@/io.ox/office/tk/dom';
import { MessageDialog, QueryDialogButtonMode } from '@/io.ox/office/tk/dialogs';
import { escapeHTML, getArrayOption, getBooleanOption, getFunctionOption, getIntegerOption, getNumberOption,
    getObjectOption, getStringOption, wasKeyPressedWithinTime } from '@/io.ox/office/tk/utils';
import { FILTER_MODULE_NAME, sendRequest } from '@/io.ox/office/tk/utils/io';
import { BROWSER_TAB_SUPPORT, tabLogger, isTabVisible } from '@/io.ox/office/tk/utils/tabutils';
import { Logger, globalLogger } from '@/io.ox/office/tk/utils/logger';
import { IntervalRunner } from '@/io.ox/office/tk/workers';
import { GUARD_EXT, authorizeGuard, copyFile, deleteFromCache, getFile, getFileBaseName, getFileModelFromDescriptor, getPath,
    isDriveFile, isGuest, preparePath, propagateChangeFile, purgeFile, getSanitizedFileName } from '@/io.ox/office/tk/utils/driveutils';

import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import * as ErrorContext from '@/io.ox/office/baseframework/utils/errorcontext';
import * as ErrorContextData from '@/io.ox/office/baseframework/utils/errorcontextdata';
import * as ClientError from '@/io.ox/office/baseframework/utils/clienterror';
import { INFO_DOC_CONVERT_STORED_IN_DEFFOLDER, INFO_DOC_CONVERTED_AND_STORED, INFO_DOC_CREATED_IN_DEFAULTFOLDER,
    INFO_DOC_SAVED_AS_TEMPLATE, INFO_DOC_SAVED_IN_FOLDER, INFO_EDITRIGHTS_RECEIVED, INFO_LOADING_IN_PROGRESS,
    INFO_PREPARE_LOSING_EDIT_RIGHTS, INFO_SYNCHRONIZATION_RO_SUCCESSFUL, INFO_SYNCHRONIZATION_SUCCESSFUL,
    INFO_USER_IS_CURRENTLY_EDIT_DOC } from '@/io.ox/office/baseframework/utils/infostate';
import { createCrashDataForError, getMessageData as getErrorMessageData, showOnceInDocLifetime, suppressDirectlyRepeated } from '@/io.ox/office/baseframework/utils/errormessages';
import { getMessageData } from '@/io.ox/office/baseframework/utils/infomessages';
import { FlushReason, QuitReason, downloadTextFile, importDebugMainModule } from '@/io.ox/office/baseframework/utils/apputils';

import { DOM_TEXT_EDIT_ALERT, getFileFormat, isNative, isScriptable, isTemplate, supportsEditMode } from '@/io.ox/office/baseframework/app/extensionregistry';
import { appsuiteLoggedOut, BaseApplication } from '@/io.ox/office/baseframework/app/baseapplication';

import RT2Const from '@/io.ox/office/rt2/rt2const';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';

import { format, getDateFormat, getTimeFormat } from '@/io.ox/office/editframework/utils/dateutils';
import * as Config from '@/io.ox/office/editframework/utils/editconfig';
import { SaveAsType, COMBINED_TOOL_PANES, STORAGE_AVAILABLE, RENAME_DISABLED, TRANSFER_EDIT_RIGHTS_TIMEOUT, getStorageValue, rtLogger } from '@/io.ox/office/editframework/utils/editconfig';
import { handleMinifiedActions, resolveUserId, getActionsArray } from '@/io.ox/office/editframework/utils/operationutils';
import { OTError, otLogger, dbgDumpActions } from '@/io.ox/office/editframework/utils/otutils';
import { importWebFonts } from '@/io.ox/office/editframework/utils/webfonts';
import { acquireEditLogger, AcquireEditRightsWorker } from '@/io.ox/office/editframework/app/acquireeditrightsworker';
import { CLOSE_LABEL, DEFAULT_FILE_BASE_NAME, EDIT_ICON, EDIT_LABEL, LOAD_RESTORED_ICON, LOAD_RESTORED_LABEL, RELOAD_ICON, RELOAD_LABEL } from '@/io.ox/office/editframework/view/editlabels';
import RTConnection from '@/io.ox/office/editframework/app/rtconnection';
import RTConnectionViewer from '@/io.ox/office/editframework/app/rtconnectionviewer';

// types ==================================================================

/**
 * Answer of this user for a request for edit rights sent by another user.
 */
var EditRightsRequestAnswer = {

    /** This user accepts transferring edit rights to the other user. */
    ACCEPT: 'accept',

    /** This user declines transferring edit rights to the other user. */
    DECLINE: 'decline',

    /** This user is inactive. */
    INACTIVE: 'inactive'
};

// constants ==============================================================

// maximal time for synchronization
var MAX_SECONDS_FOR_SYNC = 30;

// not locked id
var NOT_LOCKED_ID = -1;

// maximal time in ms for waiting for server response at close
var MAX_DELAY_FOR_CLOSE = 8000;

// maximal time in ms for waiting for server response at leave
var MAX_DELAY_FOR_DISCONNECT = 2000;

// maximal time in ms for waiting for EditApplication.sendActions() in quit
var MAX_DELAY_FOR_SEND_ACTIONS_QUIT = 5000;

// the update attributes used by server responses
var UPDATE_DATA_ATTRIBUTES = [
    'editUserId',
    'editUser',
    'hasErrors',
    'writeProtected',
    'activeUsers',
    'locked',
    'lockedByUser',
    'lockedByUserId',
    'failSafeSaveDone',
    'serverOSN'
];

// private global functions ===============================================

/**
 * Extracts and validates the operations array from the passed data object
 * of a server response.
 *
 * @param {Object} data
 *  The response data object of the server request.
 *
 * @returns {Operation[]}
 *  The operations array.
 */
function getOperationsFromData(data) {

    // operations are stored in the array property 'operations'
    var operations = pick.array(data, 'operations', true);

    // check that all array elements are objects
    if (!operations.every(_.isObject)) {
        globalLogger.error('$badge{EditApp} getOperationsFromData: invalid elements in operations array', operations);
        return [];
    }

    return operations;
}

/**
 * Returns a new promise that times out after the specified delay, if the
 * passed promise is still in pending state then.
 *
 * @param {JPromise<T>} promise
 *  The promise to be restricted with a timeout.
 *
 * @param {number} timeout
 *  The duration in milleseconds to wait for the promise.
 *
 * @param {string} location
 *  A text particle to be inserted into the timeout log message.
 *
 * @returns {JPromise<T>}
 *  A new promsie that forwards the result of the passed promise if
 *  available before the timeout.
 */
function addTimeout(promise, timeout, location) {

    // create a new promise
    return jpromise.new(function (fulfilFn, rejectFn) {

        // forward the result of the passed promise
        promise.done(fulfilFn).fail(rejectFn);

        // create a timer that rejects the deferred object after the specified timeout
        var timer = window.setTimeout(function () {
            rejectFn('timeout');
            globalLogger.warn(location + ' - operation timed out');
        }, timeout);
        promise.always(function () { window.clearTimeout(timer); });
    });
}

/**
 * Provides a text file download for the current global log buffer.
 */
function downloadFrontendLog(logData) {
    const fileName = `Documents_UI_log_${ox.user}${new Date().toISOString()}.txt`;
    downloadTextFile(fileName, logData);
}

// class EditApplication ==================================================

/**
 * The base class for all OX Documents applications allowing to edit a
 * document.
 */
export class EditApplication extends BaseApplication {

    /**
     * @param {CoreApp} coreApp
     *  The core application instance.
     *
     * @param {EditMVCFactory} mvcFactory
     *  A global factory singleton that implements creation of a new document
     *  model, a document view, and a document controller (in this order).
     *
     * @param {EditApplicationConfig} [initOptions]
     *  Optional parameters. Supports all options supported by the base class
     *  BaseApplication. Additionally, the following options are supported:
     *  @param {Boolean} [initOptions.applyActionsDetached=false]
     *      If set to true, the application pane will be detached from the DOM
     *      while the import actions will be applied. This may improve the
     *      import performance of large documents.
     *  @param {Function} [initOptions.mergeCachedOperationsHandler]
     *      A function that can be used to merge the operations and cached before they
     *      are sent to the server. Receives an operations array as first
     *      parameter, cached buffer of operations as second parameter,
     *      and must return an operations array. Will be called in
     *      the context of this application instance.
     *  @param {Number} [initOptions.sendActionsDelay=0]
     *      The duration in milliseconds used to debounce passing new pending
     *      actions to the realtime connection.
     *  @param {Number} [initOptions.realTimeDelay=700]
     *      The duration in milliseconds the realtime framework will collect
     *      messages before sending them to the server.
     *  @param {Boolean} [initOptions.useStorage=false]
     *      Load performance: Whether the local storage shall be used for
     *      storing the document.
     *  @param {Boolean} [initOptions.supportsOnlineSync=false]
     *      Whether the application supports synchronization to enable seamless
     *      editing of a document after a offline/online transition.
     *  @param {string} [initOptions.otConfigKey]
     *      The configuration key specifying whether OT (concurrent editing
     *      mode) is enabled for this application.
     */
    constructor(coreApp, mvcFactory, initOptions) {

        // base constructor
        super(coreApp, mvcFactory);

        // self reference
        var self = this;

        var launchOptions = this.launchConfig;

        // the document model instance
        var docModel = null;

        // application view: contains panes, tool bars, etc.
        var docView = null;

        // the operation transformation manager
        var otManager = null;

        // the unique identifier of this application
        var clientId = null;

        // the format of the document
        var fileFormat = null;

        // the name of the current user loading the document for operations
        var clientOperationName = null;

        // connection to the realtime framework
        var rtConnection = null;

        // buffer for actions not yet sent to the server
        var actionsBuffer = [];

        // new action collecting multiple operations in an undo group
        var pendingAction = null;

        // whether actions are currently sent to the server
        var sendingActions = false;

        // whether the actions buffer was modified after the last document flush
        var hasActionsAfterFlush = false;

        // last time operations where sent, to use for a forced delay when calling flushhandler
        var sentActionsTime = 0;

        // whether an optionally existing handler for optimizing the operations shall be used.
        var useOperationOptimization = true;

        // callback function that implements merging of current and cached operations, that represent update of complex date fields
        var mergeCachedOperationsHandler = getFunctionOption(initOptions, 'mergeCachedOperationsHandler');

        // whether OT is enabled for this document
        var isOTEnabled = (initOptions && initOptions.otConfigKey) ? Config.getFlag(initOptions.otConfigKey, true) : true;

        // application specific delay in milliseconds, until operations are sent to the server
        var sendActionsDelay = isOTEnabled ? 100 : getNumberOption(initOptions, 'sendActionsDelay', 0);

        // whether the application supports multiple selection
        var isMultiSelectionApp = getBooleanOption(initOptions, 'isMultiSelectionApp', true);

        // current application state (see method EditApplication.getState())
        var appState = null;

        // user-defined application state that temporarily overrides the application state
        var userState = null;

        // whether the application is currently connected to the server
        var connected = false;

        // cached name of the user currently editing the document
        var oldEditUser = null;

        // current state of the edit mode (false: read-only mode)
        var editMode = null;

        // if any operations where generated by the own document model and sent to the server
        var locallyModified = false;

        // if any operations where generated by a remote application
        var remotelyModified = false;

        // current server request to rename the edited document
        var renamePromise = null;

        // whether the file has been renamed locally
        var locallyRenamed = false;

        // whether the file has been move to other folder or to trash
        var movedAway = false;

        // whether application is locked globally (no file permissions)
        var locked = false;

        // document file is locked by the user
        var documentFileLockedByUser = null;

        // whether application is locked globally (internal unrecoverable error)
        var internalError = false;

        // status of the error log request during an internalError (false = no request / promise = request)
        var internalErrorSendLogPromise = false;

        // whether the user had edit rights before the offline handler was called
        var onlineEditRights = false;

        // whether the connection was at least once offline
        var offlineHandlerTriggered = false;

        // whether we received a shutdown broadcast or not
        var shutdownReceived = false;

        // old edit user id
        var oldEditClientId = null;

        // whether we are in a prepare losing edit rights state
        var inPrepareLosingEditRights = false;

        // whether the documents of the application can be saved in the local storage (in OT case localStorage is not supported yet -> shall be activated later)
        var localStorageApp = getBooleanOption(initOptions, 'localStorageApp', false) && !isOTEnabled;

        // the required version of the local storage
        var requiredStorageVersion = getIntegerOption(initOptions, 'requiredStorageVersion', 0);

        // the supported version of the local storage
        var supportedStorageVersion = getIntegerOption(initOptions, 'supportedStorageVersion', 0);

        // when renaming the document a reload needs to be triggered
        // information about this will be sent from the backend in finalizeImport()
        var renameNeedsReload = false;

        // reading configuration whether using local browser storage is configured
        // Additionally checking, if the 'Stay signed in' check box is checked or not
        var saveFileInLocalStorage = Config.USE_LOCAL_STORAGE && (ox.secretCookie === true) && !launchOptions.auth_code;

        // whether the document was loaded from the local storage
        var loadedFromStorage = false;

        // maps client identifiers to unique and constant integral indexes
        var clientIndexes = {};

        // next free unique client index (0 is used for this client)
        var nextClientIndex = 1;

        // all clients working on this document (including the local client, including inactive clients)
        var editClients = [];

        // all active clients working on this document (less than 60 seconds inactivity)
        var activeClients = [];

        // pending remote updates queue
        var pendingRemoteUpdates = [];

        // start time of preview phase (import still running but with enabled view)
        var previewStartTime = null;

        // time stamp when import finishes
        var importFinishedTime = 0;

        // server sent us a fast empty doc response with document data
        var fastEmpty = false;

        // Synchronization support online sync or not
        var supportsOnlineSync = getBooleanOption(initOptions, 'supportsOnlineSync', true);

        // must sync after online again, with mode ('editSync', 'viewSync')
        var mustSyncNowMode = null;

        // determines if we have to stop sync mode or not (in case of a servere problem)
        var stopSync = false;

        // whether the generic fail handler was executed
        var failureAlreadyEvaluated = false;

        // already shown warnings that should only be seen once
        var alreadyShownOnceWarnings = {};

        // the last shown warning
        var lastShownWarning = '';

        // unique key-value based index/map/registry of document authors, ... each author with a backend-generated key.
        var documentAuthorIndex = {};

        // unique list of document authors (e.g. change tracking, comments, collaboration...)
        var documentAuthorsList = [];

        // allow or disallow registering of operations
        var distributeOperations = true;

        // block registration of operations in 'operations:success'
        var operationsBlocked = false;

        // cache the operations during prohibition of registering operations (date fields on document load)
        var cacheBuffer = [];

        // deferred for rename and reload
        var renameAndReloadDeferred = null;

        // document restore id
        var docRestoreId = null;

        // document restore started
        var docRestore = false;

        // whether reloading the document (controller item "document/reload") is currently enabled
        var reloadEnabled = false;

        // whether the first toolbar tab is activated after loading
        var firstTabActivated = false;

        // data for an own request to acquire edit rights from another user
        var acquireEditRightsWorker = null;

        // data for an external request to transfer edit rights to another user (null: no request running)
        var editRightsTransferData = null;

        // time stamp for the last edit transfer related activity
        var lastEditTransferActivity = 0;

        // specifies if the backend can restore a document automatically
        // if needed or not (in case the client did it)
        var restoreDocOnClose = true;

        // stores the message data of the lastest error/warning for
        // reusage in case the message should be displayed again
        var currentMessageData = null;

        // indicating whether silentShutdownConnection is running or not
        var silentShutdownConnectionPromise = null;

        // flag to protect ourself to be called more than once for this notify
        var invalidSessionDetectedForApp = false;

        // stores the load mode used to load the current document
        var loadMode = 'slow load';

        // OT DEMO: For testing reasons no operations are sent to the server, if this mode is acitve
        var isOTBlockingOpsMode = false;

        // OT: Collector for all locally applied actions without acknowledge from the server
        var localActionsWithoutAck = [];

        // OT: The state of the document, sent by the server
        var serverOSN = 0;

        // DOCS-1430: a background worker that constantly sends the current local OSN
        var osnBeacon = null;

        // OT: A temporary document state sent by the server with an acknowledge as answer to sendActions. This variable is only
        //     set with a value > -1, if the serverOSN cannot be updated immediately. This can happen, because an ack for a
        //     sendActions can be faster then OSN sent from the server with external operations.
        var ackOSN = -1;

        // OT: last received osn from ackknowlagement from locally send operations
        var lastReceivedFromAck = -1;

        // whether sendAction has failed one time, resulting in a terminal error state
        var sendActionsFailedError = false;

        // the total number of external operations transformed with pending local actions
        var extOpCount = 0;

        // OT: A collector for all external actions, that cannot be applied because of an internal long running process
        var externalDataCache = [];

        // OT: A collector for all OSNs of the already applied external actions
        var externalAppliedOSNs = {};

        // information about the state from the loaded document
        var loadDocInfo = {};

        var currentDocUid = null;

        var currentClientUid = null;

        var isRescueDoc = false;

        // indicates that a browser closing has been started
        var browserClosing = false;

        // whether a sessionChange process is running
        var handleSessionChange = false;

        // private methods ----------------------------------------------------

        /**
         * Extracts the update data from the passed data object of a
         * server response.
         *
         * @param {Object} data
         *  The response data object of a server request.
         *
         * @returns {Object}
         *  An object which contains only the update attributes, if
         *  existing, otherwise undefined.
         */
        function getUpdateAttributesFromData(data) {

            // the return object
            var updateData = {};

            // copy only the known update attributes
            UPDATE_DATA_ATTRIBUTES.forEach(function (key) {
                if (key in data) { updateData[key] = data[key]; }
            });

            return updateData;
        }

        /**
         * Triggers the 'docs:state' event, and updates the 'data-app-state'
         * attribute at the root DOM node of this application.
         *
         * @param {String} [oldState]
         *  Optionally the old state can be specified, so that listeners will
         *  be informed about the old state, too.
         */
        function triggerChangeState(oldState, newError) {
            var state = self.getState();
            self.setRootAttribute('data-app-state', state);
            self.trigger('docs:state', state, oldState, newError);
            self.trigger('docs:state:' + state, newError);
            acquireEditLogger.log('statechange', state, oldState);
        }

        /**
         * Recalculates the application state. Triggers a 'docs:state' event,
         * if the application state has been changed.
         */
        function updateState(newError) {

            // calculate the new application state
            var newAppState = (function () {
                if (internalError) { return 'error'; }
                if (previewStartTime) { return 'preview'; }
                if (!connected) { return 'offline'; }
                if (locked || !editMode) { return 'readonly'; }
                // "sending" state: unsaved operations, but NOT other stuff (e.g. pending comment, bug 67496)
                if (hasUnsavedOperations()) { return 'sending'; }
                if (locallyModified) { return 'ready'; }
                return 'initial';
            }());

            // trigger event if state has been changed
            if (appState !== newAppState) {
                var oldState = appState;
                appState = newAppState;
                triggerChangeState(oldState, newError);
            }
        }

        /**
         * Changes the state of the edit mode of this application, and notifies
         * all event listeners.
         */
        function changeEditMode(newEditMode) {
            if (editMode !== newEditMode) {
                editMode = newEditMode;
                self.setRootAttribute('data-edit-mode', editMode);
                self.trigger('docs:editmode', editMode);
                acquireEditLogger.log('$back{LightPink|changeEditMode}', editMode);
                self.trigger(editMode ? 'docs:editmode:enter' : 'docs:editmode:leave');
                updateState();
            }
        }

        /**
         * Before loading a file, the client can check, if this file is already saved in the local storage.
         * The saved OSN and file version can be sent to the server, so that the server can check, if this
         * version is still valid. Then it is not necessary to parse the document on server side and send
         * operations and fastload string to the client. This saves time and bandwidth. (docs-974).
         *
         * @param {Object} launchOptions
         *  An object containing the launch options of the file.
         *
         * @param {Object} connectOptions
         *  An object containing the data that are sent to the server with the first 'connect' call. This
         *  object might be expanded within this function, so that it contains the OSN and the version of
         *  the file that shall be loaded.
         */
        function checkFileInLocalStorage(launchOptions, connectOptions) {

            if ((launchOptions.action !== 'load') || !_.isObject(launchOptions.file)) { return; }

            // defining the keys in the local storage
            var saveKey = launchOptions.file.id + '_' + launchOptions.file.folder_id;
            var osnKey = saveKey + '_OSN';
            var fileVersionKey = saveKey + '_FILEVERSION';
            var storageVersionKey = saveKey + '_STORAGEVERSION';

            // reading data from local storage
            var localStorageOsn = localStorage.getItem(osnKey);
            var localStorageFileVersion = localStorage.getItem(fileVersionKey);
            var localStorageVersion = localStorage.getItem(storageVersionKey);

            if (localStorageVersion >= requiredStorageVersion) { // checking the version of the storage

                if (localStorageOsn && localStorageFileVersion) {

                    localStorageOsn = parseInt(localStorageOsn, 10);
                    localStorageFileVersion = parseInt(localStorageFileVersion, 10);

                    if (_.isNumber(localStorageOsn) && _.isNumber(localStorageFileVersion)) {
                        connectOptions.storageOSN = localStorageOsn;
                        connectOptions.storageVersion = localStorageFileVersion;
                    }
                }
            }
        }

        /**
         * Provides am object containing optional data for info
         * messages.
         *
         * @returns {Object}
         *  An object filled with optional data for info messages.
         */
        function getInfoStateOptions() {
            var // the options object filled with context data
                options = {
                    documentFileLockedByUser,
                    fullFileName: self.getFullFileName(),
                    oldEditUser,
                    isEditor: isEditor()
                };

            return options;
        }

        /**
         * Shows a read-only alert banner according to the current state of the
         * application. If the document is locked due to missing editing
         * permissions, shows an appropriate alert, otherwise shows the name of
         * the current editing user.
         *
         * @param {String} [message]
         *  A custom message to be shown in the alert banner. If omitted, a
         *  default message will be generated according to the error/info state.
         *
         * @returns {boolean}
         *  Whether an alert banner has been shown. The return value `false`
         *  means there is already a high-priority alert banner visible, and
         *  the new alert banner does not contain the `priority` flag.
         */
        function showReadOnlyAlert(message, duration) {

            var // alert type
                type = 'info',
                // the alert title
                headline = gt('Read-Only Mode'),
                // state to check
                checkState = null,
                // message data
                messageData = null,
                // options
                options = null;

            if (_.isString(message)) {
                messageData = { type, message, headline };
                if (!Number.isNaN(duration)) { messageData.duration = duration; }
                messageData = _.extend({}, messageData, { action: 'acquireedit' });
            } else {
                options = getInfoStateOptions();
                checkState = self.getErrorState();
                messageData = currentMessageData;

                if (!_.isObject(messageData)) {
                    if (_.isObject(checkState) && (checkState.isError() || checkState.isWarning())) {
                        messageData = getErrorMessageData(checkState, options);
                    } else {
                        checkState = self.getInfoState();
                        if (_.isString(checkState)) {
                            messageData = getMessageData(checkState, options);
                        }
                    }
                }
            }

            self.implExtendMessageData(messageData);

            // show the alert banner
            return docView.yell(messageData);
        }

        /**
         * In debug mode, the global configuration value for 'useLocalStorage' can be overwritten
         * with the local configuration value of 'debugUseLocalStorage'.
         */
        var checkDebugUseLocalStorage = Config.DEBUG ? function () {
            saveFileInLocalStorage = self.getUserSettingsValue('debugUseLocalStorage', saveFileInLocalStorage) && !self.isDocumentEncrypted();
        } : $.noop;

        /**
         * Retrieves the user name by using the clientUID.
         *
         * @param {String} clientUID
         *  The unique client id from we want to get the user name.
         *
         * @returns {String}
         *  The user name or a empty string if we have no mapping.
         * A empty String means no user has edit rights at the moment.
         */
        function getUserNameByClientUID(clientUID) {
            var client = _.findWhere(editClients, { clientUID });
            return (client && client.userName) || '';
        }

        /**
         * Registering a listener to the 'tab:activated' event, so that the performance
         * measurements can detect the time until the toolbars are activated. This
         * handler is deregistered, after a maximum number of toolbar changes
         * happened. Typically this value is '2', because the 'format' toolbar is
         * activated after the 'file' toolbar.
         */
        var registerTabActivationHandler = Config.LOG_PERFORMANCE_DATA ? function () {

            // the maximum number of tab activations that shall be logged
            var maxTabChangeCounter = 2;
            // the number of logged tab activations
            var tabActivatedCounter = 0;

            docView.toolPaneManager.on('tab:activated', function activationHandler(tabEntry) {
                tabActivatedCounter++;
                // tabEntry can be null (64608)
                if (tabEntry) {
                    var perfLogKey = 'tabActivated_' + str.capitalizeFirst(tabEntry.tabId);
                    self.launchTracker.step(perfLogKey, `$badge{EditApp} Toolbar tab "${tabEntry.tabId}" activated`);
                }
                if (tabActivatedCounter === maxTabChangeCounter) {
                    docView.toolPaneManager.off('tab:activated', activationHandler);
                }
            });

        } : $.noop;

        /**
         * Registering a listener function for the 'tab:activated' event, that registers
         * the first finished activation of a toolbar tab.
         */
        function registerFirstTabActivatedHandler() {

            async function considerToolbarUIStable() {
                // await a controler update, otherwise some UI states (e.g. white selection for tabs) might not be visible yet
                await self.docController.update();
                self.launchTracker.step('considerToolbarUIStable', '$badge{EditApp} Toolbar is visually stable');
                // reveal the reloading app at this point of the loading process in case of a document reload
                self.setAppReloadDone();
            }

            if (self.showMainToolPaneAtStart()) {
                docView.toolPaneManager.one('tab:activated', () => {
                    firstTabActivated = true;
                    // The first finished pane layout rendering after the first
                    // tab was activated means that we have a visual stable toolbar.
                    docView.mainPane.one('toolpane:refreshed', considerToolbarUIStable);
                });
            } else {
                // alternative event, triggered earlier
                self.waitForImportSuccess(() => {
                    firstTabActivated = true;
                    // Without the main toolpane visible: We use the first toolpane unhide event as a indicator for a visual stable
                    // toolbar as there is no specific point when we know that all toolbars (top-bar, edit-toolbar,...) are done.
                    docView.one('toolpane:showcontents', function () {
                        // use a small delay to make sure all toolbars are unhidden to prevent flicker
                        self.setTimeout(considerToolbarUIStable, 150);
                    });
                });
            }

        }

        /**
         * Tries to restore the connection which was broken due
         * to real-time connection problems. Dependent on the root
         * cause and document state this can result in a document
         * reload, restore document or error message.
         *
         * @param {String|ErrorCode} cause
         *  The cause of the broken connection dependent on the
         *  RT notification sent: e.g. 'reset', 'timeout', etc. or
         *  this can be an error code which has to be used.
         */
        function tryToRestoreConnection(cause) {
            var errorCode = ErrorCode.isErrorCode(cause) ? cause : getErrorCodeFromRTCause(cause);

            errorCode.setErrorContext(ErrorContext.CONNECTION);

            if (rtConnectionExist()) {
                var hasPendingOperations = rtConnection.hasPendingActions() || self.hasUnsavedChangesOffice();

                // Make sure the user won't be able to make any changes
                // and we don't want to show any message as we could be
                // able to "restore" the connection using reloadDocument
                locked = true;
                changeEditMode(false);

                if (hasPendingOperations) {

                    if (isOTEnabled) {
                        self.setInternalError(ClientError.ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES, ErrorContext.CONNECTION);
                    } else {
                        // Try to restore the document data or set error
                        self.restoreDocument(errorCode);
                    }

                } else {
                    // No pending operations therefore it's safe to reload
                    // the document automatically.

                    // Adapt launch options to prevent removing the file as it
                    // could be empty and new or a template file.
                    // See #32719 "Save as with an empty document: => Load Error"
                    launchOptions.action = 'load';
                    delete launchOptions.template;

                    // close the current document and restart with new one
                    self.reloadDocument();
                }
            } else {
                self.setInternalError(errorCode, ErrorContext.CONNECTION);
            }
        }

        /**
         * Retrieves the default error code from a cause string which
         * specifies what caused a connection problem.
         *
         * @param {String} cause
         *  The cause string which represents the problem notification
         *  from RT which is mapped to a certain error code. The error
         *  code ERROR_UNKNOWN_REALTIME_FAILURE is used, if the cause
         *  could not be interpreted.
         *
         * @returns {ErrorCode}
         */
        function getErrorCodeFromRTCause(cause) {
            var errorCode;

            switch (cause) {
                case 'reset': errorCode = new ErrorCode(ClientError.ERROR_CONNECTION_RESET_RECEIVED); break;
                case 'timeout': errorCode = new ErrorCode(ClientError.ERROR_CONNECTION_TIMEOUT); break;
                case 'notMember': errorCode = new ErrorCode(ClientError.ERROR_CONNECTION_NOT_MEMBER); break;
                default: errorCode = new ErrorCode(ClientError.ERROR_UNKNOWN_REALTIME_FAILURE); break;
            }

            return errorCode;
        }

        /**
         * Handles notifications sent by the RT communication layer
         * that reports that the connection is now offline.
         */
        function connectionOfflineHandler(cause, importDeferred) {

            // make sure that application is able to show the error message and
            // reject the pending import promise
            if (importDeferred && (importDeferred.state() === 'pending')) {
                importDeferred.reject(cause);
                self.setInternalError(ClientError.ERROR_OFFLINE_WHILE_LOADING, ErrorContext.OFFLINE);
            }

            offlineHandlerTriggered = true;
            if (connected && !internalError && !self.isInQuit() && rtConnectionExist()) {
                onlineEditRights = editMode;
                connected = false;

                // DOCS-1430: stop the background task that repeatedly sends the local OSN
                if (isOTEnabled) { osnBeacon.abort(); }

                // Sets the client lock mode to ensure that the user is not able
                // to do anything via the UI while we try to synchronize.
                if (!mustSyncNowMode) {
                    locked = true;
                    changeEditMode(false);
                    updateState();
                    self.setInternalError(ClientError.WARNING_READONLY_DUE_TO_OFFLINE, ErrorContext.OFFLINE);
                } else if (rtConnection.hasPendingActions()) {
                    // A off-line notification while synchronizing means
                    // that we give up. In case of local changes provide
                    // more information on how the user can prevent data loss.
                    self.setInternalError(ClientError.ERROR_OFFLINE_WHILE_SYNC_LOCAL_CHANGES, ErrorContext.OFFLINE);
                } else {
                    // a off-line notification while synchronizing means
                    // that we give up and set internal error to true
                    self.setInternalError(ClientError.ERROR_OFFLINE_WHILE_SYNCHRONIZING, ErrorContext.OFFLINE);
                }
                // more logging for RT to better see who has the edit rights
                rtLogger.log('Connection offline handler called by Realtime!');

                // abort the background process to acquire edit rights
                abortPendingEditRightsRequest();
            }
        }

        /**
         * Handles notifications sent by the RT communication layer
         * that reports that the connection is now online.
         */
        function connectionOnlineHandler() {
            if (offlineHandlerTriggered && !connected && !internalError && !self.isInQuit()) {
                // more logging for RT to better see who has the edit rights
                rtLogger.log('Connection online handler called by Realtime!');

                // DOCS-1430: start the background task that repeatedly sends the local OSN
                if (isOTEnabled) { enableOsnBeacon(); }

                connected = true;
                // check for synchronization
                if (supportsOnlineSync) {

                    if (isOTEnabled) {
                        startSynchronizationProcessOT();
                    } else {
                        startSynchronizationProcess();
                    }
                }

                updateState();
                showReadOnlyAlert();
            }
        }

        /**
         * Starts a synchronization process between client and middleware. This
         * can be necessary in case the client was offline (no network
         * connection) or the session was invalid.
         */
        function startSynchronizationProcessOT() {
            rtLogger.log('Client supports synchronization - starting to sync with server');

            // protected against a possible async call while destroying
            if (!_.isObject(self)) {
                return;
            }

            // if application supports synchronization we have
            // to check what to do next - the editor needs to
            // synchronize while the other clients just reset
            // locked.
            if (onlineEditRights) {
                // edit synchronization must be done
                self.setInternalError(ClientError.WARNING_SYNC_AFTER_OFFLINE, ErrorContext.SYNCHRONIZATION);
                rtLogger.log('Client is editor - synchronizing with server using current client state');
                mustSyncNowMode = 'editSync';

                // check pending actions waiting for a resend in this application instance
                if (actionsBuffer && (actionsBuffer.length > 0)) {
                    // try to send actions again - should work as we are online again
                    sendActions();
                }
            } else {
                // view synchronization must be done
                self.setInternalError(ClientError.WARNING_SYNC_FOR_VIEWER, ErrorContext.SYNCHRONIZATION);
                rtLogger.log('Client is viewer - waiting for server messages to unlock view');
                mustSyncNowMode = 'viewSync';
            }

            // setup timeout to prevent waiting forever
            self.executeDelayed(function () {
                if (!stopSync && mustSyncNowMode) {
                    rtLogger.log('Synchronization time out detected - no synchronization possible');
                    checkSynchronizationStateOT({ cause: 'timeout' });
                }
            }, MAX_SECONDS_FOR_SYNC * 1000);

            if (mustSyncNowMode && rtConnectionExist()) {

                // (do not remove) rt2 layer must be notified that we're starting the sync process
                var promise = rtConnection.sync();

                // send sync stable request to ensure that the server requests missing
                // msgs with a NACK.
                promise = promise.then(
                    function () {
                        if (stopSync) { return; }
                        rtConnection.syncStable().then(function (data) {
                            rtLogger.log('SyncStable response received ', data);

                            // check that we really received a 'SYNCSTABLE' status update
                            if (data && data.docStatus) {
                                checkSynchronizationStateOT(null);
                            } else {
                                checkSynchronizationStateOT({ cause: 'reset' });
                            }
                        }, function () {
                            checkSynchronizationStateOT({ cause: 'reset' });
                        });
                    });

                promise.done(function () {
                    var timer = self.setInterval(function () {
                        // we have a stable connection
                        if (!mustSyncNowMode || stopSync) {
                            timer.abort();
                        // no connection, probably error
                        } else if (!rtConnectionExist()) {
                            // end repeating check early if we detected a problem
                            checkSynchronizationStateOT({ cause: 'reset' });
                            timer.abort();
                        // ... still synchronizing
                        } else {
                            checkSynchronizationStateOT(null);
                        }
                    }, {
                        delay: 0,
                        interval: (MAX_SECONDS_FOR_SYNC * 1000) / 10,
                        count: 9
                    });
                });
            }
        }

        /**
         * Starts a synchronization process between client and middleware. This
         * can be necessary in case the client was offline (no network
         * connection) or the session was invalid.
         */
        function startSynchronizationProcess() {
            rtLogger.log('Client supports synchronization - starting to sync with server');

            // protected against a possible async call while destroying
            if (!_.isObject(self)) {
                return;
            }

            // if application supports synchronization we have
            // to check what to do next - the editor needs to
            // synchronize while the other clients just reset
            // locked.
            if (onlineEditRights) {
                // edit synchronization must be done
                self.setInternalError(ClientError.WARNING_SYNC_AFTER_OFFLINE, ErrorContext.SYNCHRONIZATION);
                rtLogger.log('Client is editor - synchronizing with server using current client state');
                mustSyncNowMode = 'editSync';

                // check pending actions waiting for a resend in this application instance
                if (actionsBuffer && (actionsBuffer.length > 0)) {
                    // try to send actions again - should work as we are online again
                    sendActions();
                }
            } else {
                // view synchronization must be done
                self.setInternalError(ClientError.WARNING_SYNC_FOR_VIEWER, ErrorContext.SYNCHRONIZATION);
                rtLogger.log('Client is viewer - waiting for server messages to unlock view');
                mustSyncNowMode = 'viewSync';
            }

            // setup timeout to prevent waiting forever
            self.executeDelayed(function () {
                if (!stopSync && mustSyncNowMode) {
                    rtLogger.log('Synchronization time out detected - no synchronization possible');
                    checkSynchronizationState(null, null, null, { cause: 'timeout' });
                }
            }, MAX_SECONDS_FOR_SYNC * 1000);

            if (mustSyncNowMode && rtConnectionExist()) {
                var localServerOSN = -1;
                var editClientId = null;

                var promise = rtConnection.sync().then(function (data) {
                    rtLogger.log('Sync response received ', data);

                    // check that we really received a 'SYNC' status update
                    if (data && data.docStatus) {
                        var clientOSN = docModel.getOperationStateNumber();

                        // check sync state early to see if we are able to sync or not
                        // (in case we were editor and made changes in the not detected
                        // offline phase and lost edit rights)
                        editClientId = getStringOption(data.docStatus, 'editUserId', '');
                        localServerOSN = getIntegerOption(data.docStatus, 'serverOSN', -1);

                        // The sync data provides us with the latest doc status including the
                        // "current editor". Therefore set the editor to provide checkSync with
                        // the latest editor data.
                        oldEditClientId = editClientId;

                        checkSynchronizationState(editClientId, clientOSN, localServerOSN, null);
                        return $.when();
                    } else {
                        throw new Error('sync response contains no mandatory data - synchronization impossible!');
                    }
                }, function () {
                    checkSynchronizationState(null, null, null, { cause: 'reset' });
                });

                promise = promise.then(function () {
                    if (mustSyncNowMode && rtConnectionExist()) {
                        // send sync stable request to ensure that the server requests missing
                        // msgs with a NACK.
                        var stablePromise = rtConnection.syncStable();

                        stablePromise.done(function (data) {
                            rtLogger.log('SyncStable response received ', data);

                            // check that we really received a 'SYNCSTABLE' status update
                            if (data && data.docStatus) {
                                var clientOSN = docModel.getOperationStateNumber();

                                // ATTENTION: Don't retrieve the server osn here again - we want to see if we
                                // are able to catch up to the state that we received on our first sync request.
                                // A new/old editor can keep typing and push the osn further away, so we are not
                                // able to catch up in the small sync timing window. The check that we have local
                                // changes must be done by the server-side which knows the current editor and sends
                                // us a hang-up if we try to send local changes.
                                editClientId = getStringOption(data.docStatus, 'editUserId', '');

                                // The sync data provides us with the latest doc status including the
                                // "current editor". Therefore set the editor to provide checkSync with
                                // the latest editor data.
                                oldEditClientId = editClientId;

                                checkSynchronizationState(editClientId, clientOSN, localServerOSN, null);
                            } else {
                                checkSynchronizationState(null, null, null, { cause: 'reset' });
                            }
                        });

                        stablePromise.fail(function () {
                            checkSynchronizationState(null, null, null, { cause: 'reset' });
                        });

                        return stablePromise;
                    } else {
                        checkSynchronizationState(null, null, null, { cause: 'reset' });
                        throw new Error('synchronization not possible');
                    }
                }, function () {
                    checkSynchronizationState(null, null, null, { cause: 'reset' });
                });

                promise.done(function () {
                    var timer = self.setInterval(function () {

                        if (!mustSyncNowMode || !rtConnectionExist()) {
                            // end repeating check early if we detected a problem
                            checkSynchronizationState(null, null, null, { cause: 'reset' });
                            timer.abort();
                        } else {
                            var clientOSN = docModel.getOperationStateNumber();
                            var currentEditClientId = _.isString(editClientId) ? editClientId : oldEditClientId;

                            checkSynchronizationState(currentEditClientId, clientOSN, localServerOSN, null);
                        }
                    }, {
                        delay: 0,
                        interval: (MAX_SECONDS_FOR_SYNC * 1000) / 10,
                        count: 9
                    });
                });
            }
        }

        /**
         * Handles notifications sent by the RT communication layer
         * that reports that an error occurred.
         *
         * @param {object} cause
         *  The cause object which represents the problem notification
         *  from RT which is mapped to a certain error code. The error
         *  code ERROR_UNKNOWN_REALTIME_FAILURE is used, if the cause
         *  could not be interpreted.
         */
        function connectionErrorHandler(cause, importDeferred) {

            // more logging for RT to better see who has the edit rights
            rtLogger.log('Error handler called by RT2 communication layer, cause = ' + JSON.stringify(cause));

            // make sure that application is able to show the error message and
            // reject the pending import promise
            if (importDeferred && (importDeferred.state() === 'pending')) {
                importDeferred.reject(cause);
            }

            // Extract error information from cause to check for certain errors
            const validCause = _.isObject(cause) && _.isString(cause.error);
            if (!validCause) { rtLogger.error("$badge{EditApp} connectionErrorHandler: missing error cause"); }
            let errorCode = validCause ? new ErrorCode(cause) : ClientError.ERROR_UNKNOWN_REALTIME_FAILURE;

            if (self) {
                if (errorCode.getCodeAsConstant() === 'GENERAL_SESSION_INVALID_ERROR') {
                    // We need to handle a session invalid error with top-priority
                    // as it's a complete blocker for all possible further functions
                    // (they all need a valid session).
                    stopSync = true;
                    handleSessionInvalidError('RT');
                } else if (errorCode.getCodeAsConstant() === 'GENERAL_DOCUMENT_ALREADY_DISPOSED_ERROR') {
                    if (_.isString(mustSyncNowMode)) {
                        // Give up early, even in sync mode when we are notified that
                        // the document is already gone.
                        stopSync = true;
                        if (isOTEnabled) {
                            checkSynchronizationStateOT({ cause: 'error:disposed' });
                        } else {
                            checkSynchronizationState(null, null, null, { cause: 'error:disposed' });
                        }
                    } else {
                        self.setInternalError(errorCode, ErrorContext.CONNECTION);
                    }
                } else if (errorCode.getCodeAsConstant() === 'TOO_MANY_CONNECTIONS_ERROR') {
                    // force to stop a possible synchronization that results in
                    // a loop of reloading the document
                    stopSync = true;
                    mustSyncNowMode = null;

                    self.setInternalError(errorCode, ErrorContext.CONNECTION);

                } else if (!self.isLocked() && !self.isInQuit() && !_.isString(mustSyncNowMode)) {
                    // try to check error code and what we can do for the user
                    if ((cause.error === 'HANGUP_INVALID_OPERATIONS_SENT_ERROR') ||
                        (cause.error === 'HANGUP_INVALID_OSN_DETECTED_ERROR') ||
                        (cause.error === 'GENERAL_OFFLINE_CLIENT_IN_NON_SYNC_STATE')) {
                        // Invalid osn/operation detected or client is too far away
                        // from the current document to have successful sync -
                        // nothing can be done by the client - user must copy
                        // & paste to new doc.
                        // The error message provided will explain what to do.
                        self.setInternalError(cause, ErrorContext.CONNECTION);
                    } else if (cause.error === 'HANGUP_NO_EDIT_RIGHTS_ERROR') {
                        // Synchronization problem. Try to restore the document content
                        // to help the user to see his/her last changes easily.
                        self.restoreDocument(errorCode); // TODO not needed for OT, but check
                    } else {
                        errorCode = ClientError.ERROR_UNKNOWN_REALTIME_FAILURE;
                        self.setInternalError(cause, ErrorContext.CONNECTION);
                    }
                } else if (_.isString(mustSyncNowMode)) {
                    // While in sync mode we don't want to immediately show an error, but
                    // let the sync implementation decide what to do - normally it tries to resolve
                    // the situation by using a doc reload or to create an own copy of the
                    // document for the user.
                    // ATTENTION: Don't completely ignore the error as the rt2 low-level
                    // framework will reject all pending promises, which lead to strage
                    // warning in case the synchronization is successful (even we have received
                    // an error here).
                    stopSync = true;

                    if (isOTEnabled) {
                        checkSynchronizationStateOT({ cause: 'reset' });
                    } else {
                        checkSynchronizationState(null, null, null, { cause: 'reset' });
                    }

                }
            }
        }

        /**
         *  Checks whether the content of the document is saved persistently on the backend.
         *
         * @param {ErrorCode} showError
         *  Provide a show error that is used in case of the error code provided by the response.
         *
         * @returns {Boolean}
         *  TRUE if a document has been saved persistently on the backend.
         */
        function checkDocumentSaved(showError) {

            return self.sendFileRequest(FILTER_MODULE_NAME, { action: 'getdocumentstate' }).then(
                function (data) {
                    var responseErrorCode = new ErrorCode(data);
                    // 1.) no info about save status
                    if (responseErrorCode.isError()) {
                        showError = showError || responseErrorCode;
                        globalLogger.warn('EditApp: checkDocumentSaved received no save status, reason: ', responseErrorCode && responseErrorCode.getCodeAsConstant());
                        self.setInternalError(showError, ErrorContext.CONNECTION);
                        return false;
                    }
                    // 2) when true, the document is really saved on server side
                    return data.syncInfo['document-osn'] >= lastReceivedFromAck;
                },
                function (error) {
                    // 3) rejected request
                    globalLogger.error('EditApp: checkDocumentSaved', error);
                    return false;
                }
            );
        }

        /**
         * Handle a changed session for Document Apps, resulting in a automatic reload
         * when the document was persistently saved on the backend, and with a fork of
         * the document when not, because without a valid session nothing can be saved.
         *
         * Overview about the cases were this function is called, with a brief overview what to do:
         *  - A1) session lost detected by rt -> setInvalidSessionAppState() -> trigger core relogin
         *  - A2) session lost detected by core http -> setInvalidSessionAppState()
         *  - B1) relogin in same tab -> reload document or fork-error
         *  - B2) relogin by other tab -> if(!invalidSessionDetectedForApp) { setInvalidSessionAppState() }
         *        -> reload document or fork-error
         */
        function handleSessionChanged() {
            rtLogger.log('EditApp: handleSessionChanged called, process is already running? ', handleSessionChange);

            // only one session change per app at a time
            if (handleSessionChange) { return; }

            // Reminder: setting 'handleSessionChange' flag to false again is not needed at the moment, because either
            // a reload or terminal error happens, but it would be needed in case there will be a sync process
            // at some point in the future.
            handleSessionChange = true;

            // Think about these two cases:
            // - session loss detected in own tab -> relogin requested -> relogin:success -> handleSessionChanged
            // - other tab does a relogin -> propagateLogin -> login:success -> handleSessionChanged
            if (!invalidSessionDetectedForApp) { setInvalidSessionAppState(); }

            if (!locallyModified) {
                rtLogger.log('EditApp: handleSessionChanged, not locally modified');
                self.reloadDocument();
                return;
            }

            if (!self.hasUnsavedChangesOffice()) {
                rtLogger.log('EditApp: handleSessionChanged, no unsaved local changes');

                checkDocumentSaved(ClientError.ERROR_SESSION_LOST_WITH_UNKNOWN_DOCUMENT_STATE).always(
                    function (saved) {
                        rtLogger.log('EditApp: handleSessionChanged, document is saved on server: ', saved);

                        if (saved) {
                            self.reloadDocument();
                        } else {
                            self.setInternalError(ClientError.ERROR_SESSION_LOST_WITH_LOCAL_CHANGES, ErrorContext.CONNECTION);
                        }
                    }
                );

            } else {
                rtLogger.log('EditApp: handleSessionChanged, has unsaved local changes');
                self.setInternalError(ClientError.ERROR_SESSION_LOST_WITH_LOCAL_CHANGES, ErrorContext.CONNECTION);
            }
        }

        /**
         * Set the App to a session invalid state, read-only mode to prevent
         * that the user to make more changes to the document that cant be saved.
         */
        function setInvalidSessionAppState() {
            // Reminder: This state is terminal until a session can be changed at App runtime.
            invalidSessionDetectedForApp = true;
            locked = true;
            // without a session we can't do anything, so close the connection
            silentShutdownConnection();
        }

        /**
         * Handles a session invalid error, detected by the real-time framework or
         * by the HTTP layer (http.js). As a result, a new session is requested when
         * detected by RT, or it was already done before when detected in the HTTP layer.
         *
         * @param {String} detectedBy
         *  The source of the detection, whether it was detected by RT layer or the HTTP layer.
         */
        function handleSessionInvalidError(detectedBy) {
            rtLogger.log('EditApp: handleSessionInvalidError called, process is already running: ', invalidSessionDetectedForApp, '; detected by: ', detectedBy);

            // only one process at a time
            if (invalidSessionDetectedForApp) { return; }
            setInvalidSessionAppState();

            // Show warning message and switch to read-only mode
            self.setInternalError(ClientError.WARNING_SESSION_INVALID_DETECTED, ErrorContext.CONNECTION);

            // when detected by http, a relogin:required was already triggered by this, but for rt we need to do it to get a relogin
            if (detectedBy === 'RT') {
                self.requestRelogin();
            }
        }

        /**
         * Handles notification that the real-time framework discovered
         * that apply_ops messages have not be answered by the server.
         * This can relate to a busy server or other more serious problems
         * on the server-side.
         */
        function connectionNoResponseInTimeHandler() {
            rtLogger.log('NoResponseInTime notification received - Server has not responded in a certain time');

            // No response in time - could be that server is very busy or
            // that server-side has internal problems. Just give user a hint
            // to be careful making more changes.
            if (!self.isLocked() && !self.isInQuit()) {
                self.setInternalError(ClientError.WARNING_CONNECTION_INSTABLE);
            }
        }

        /**
         * Handles the notification sent by the server when the instance is going
         * to be shutdown. Therefore editing is not possible anymore and the user
         * can try to reload the document after some time.
         */
        function connectionShutdownHandler() {
            rtLogger.log('Shutdown notification received - Document middleware node is going down!');

            if (!self.isLocked() && !self.isInQuit() && rtConnectionExist()) {
                var hasPendingOperations = (rtConnection.hasPendingActions() || self.hasUnsavedChanges());
                var options = { silentShutdownConfig: { skipCloseDoc: true, skipDisconnect: true } };

                if (editMode && hasPendingOperations) {
                    // No restore possible, user must copy & paste his/her changes to a new document
                    self.setInternalError(ClientError.ERROR_SERVER_SHUTDOWN_NO_RESCUE_POSSIBLE, ErrorContext.CONNECTION, null, options);
                } else {
                    // Receiving edit rights is not possible anymore.
                    // User must use reload to have a working document again.
                    self.setInternalError(ClientError.ERROR_SERVER_GOING_TO_SHUTDOWN, ErrorContext.CONNECTION, null, options);
                }
            }
        }

        /**
         * Handle the notification sent by the server when the backend node
         * crashed which controls this document instance. Therefore editing
         * is not possible anymore and the user can try to reload the document.
         */
        function connectionNodeCrashedHandler() {
            rtLogger.log('Crashed node notification received - Document middleware node is not available anymore!');

            if (!self.isLocked() && !self.isInQuit() && rtConnectionExist()) {
                var hasPendingOperations = (rtConnection.hasPendingActions() || self.hasUnsavedChanges());
                var options = { silentShutdownConfig: { skipCloseDoc: true, skipDisconnect: true } };

                if (editMode && hasPendingOperations) {
                    // No restore possible, user must copy & paste his/her changes to a new document
                    self.setInternalError(ClientError.ERROR_SERVER_NODE_UNAVAILABLE_NO_RESCUE_POSSIBLE, ErrorContext.CONNECTION, null, options);
                } else {
                    // Receiving edit rights is not possible anymore.
                    // User must use reload to have a working document again.
                    self.setInternalError(ClientError.ERROR_SERVER_NODE_UNAVAILABLE, ErrorContext.CONNECTION, null, options);
                }
            }
        }

        /**
         * Handles the notification sent by the server when a severe problem
         * occured regarding the document. Therefore editing or switching
         * edit rights is not possible anymore. The user viewing can try to
         * reload the document and the editor can try to restore it using
         * his/her latest pending actions.
         *
         * @param {Object} data
         *  Information from the server regarding the cause of the problem
         *  including the error code.
         */
        function connectionHangupHandler(data) {
            rtLogger.log('Hangup notification received - document cannot be edited anymore!');

            if (!self.isLocked() && !self.isInQuit() && rtConnectionExist()) {
                var hasPendingOperations = (rtConnection.hasPendingActions() || self.hasUnsavedChanges());
                var errorCode = new ErrorCode(data);

                if (!errorCode.isError()) {
                    errorCode = new ErrorCode(ClientError.ERROR_SERVER_NOTIFIED_A_CRITICAL_DOC_PROBLEM);
                }

                if (isEditor() && (hasPendingOperations || shouldTryToRestoreDocument(errorCode))) {

                    if (isOTEnabled) {
                        self.setInternalError(ClientError.ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES, ErrorContext.CONNECTION);
                    } else {
                        // Try to restore the document data or set error
                        self.restoreDocument(errorCode);
                    }
                } else if (errorCode.getCodeAsConstant() === 'GENERAL_MALICIOUS_ACK_HANDLING_ERROR') {
                    // DOCS-3104: Try to handle this hang-up error more gracefully
                    // as we saw this issue in a "normal" scenario where the brower
                    // instance throttled the timers tremendously which lead to the
                    // GENERAL_MALICIOUS_ACK_HANDLING_ERROR error. In almost all cases
                    // the web-client was in a background tab and no changes were
                    // done by the user. Therefore we try to reload the document silently.
                    if (!hasPendingOperations) {
                        self.reloadDocument();
                    } else {
                        self.setInternalError(errorCode, ErrorContext.CONNECTION);
                    }
                // DOCS-3909: detect moved file in Drive resulting in lost permissions
                } else if (errorCode.getCodeAsConstant() === 'GENERAL_PERMISSION_READ_MISSING_ERROR') {
                    self.setInternalError(ClientError.ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE, ErrorContext.CONNECTION);
                // DOCS-3909: detect deleted file in Drive
                } else if (errorCode.getCodeAsConstant() === 'GENERAL_FILE_NOT_FOUND_ERROR') {
                    self.setInternalError(ClientError.ERROR_DOC_FILE_DELETED, ErrorContext.CONNECTION);
                } else {
                    // Editing or receiving edit rights is not possible anymore.
                    // User must use reload to have a working document again.
                    self.setInternalError(errorCode, ErrorContext.CONNECTION);
                }
            }
        }

        /**
         * Handles the notification sent by the server when a severe problem
         * occured regarding the OT (operation transformation). This client
         * must show an error message and reload the document to be active
         * in the realtime collaboration.
         *
         * This notification is only sent if OT is enabled on the server side.
         *
         * @param {Object} data
         *  Information from the server regarding the cause of the problem
         *  including the error code.
         */
        function connectionOTReloadHandler(data) {
            rtLogger.log('OT reload notification received - client must reload the document');

            if (!self.isInQuit()) {
                var errorCode = new ErrorCode(data);

                if (!errorCode.isError()) {
                    errorCode = new ErrorCode(ClientError.ERROR_SERVER_NOTIFIED_A_CRITICAL_DOC_PROBLEM);
                }

                handleReload(errorCode, ErrorContext.CONNECTION);
            }
        }

        /**
         * Handles the notification sent by the server when a document file has been
         * moved to different folder. Due to the fact that file movement is not
         * transparent, we need to reload the document for further usage.
         *
         *
         * @param {Object} data
         *  Information from the server regarding the document file movement.
         */
        function connectionFileMovedReloadHandler(data) {
            rtLogger.log('File moved and reload notification received - client must reload the document using a new file-id');

            if (!self.isInQuit()) {
                var errorCode = new ErrorCode(data);

                if (!errorCode.isError()) {
                    errorCode = new ErrorCode(ClientError.WARNING_DOC_MOVED_RELOAD_REQUIRED);
                }

                var fileId = data.fileId;
                var folderId = data.folderId;
                handleReload(errorCode, ErrorContext.CONNECTION, { id: fileId, folder_id: folderId }, 2500);
            }
        }

        /**
         * General handler for reloading the document after an error. By default
         * the same document is reloaded, but it can also be a different one.
         *
         * @param {ErrorCode} errorCode
         *  An error code that will be passed to setInternalError.
         *
         * @param {String} errorContext
         *  An error context string constant defined by ErrorContext.
         *
         * @param {Object} [newFileDescData]
         *  An optional object containing the folder & file-id of the
         *  new file to be loaded via reload (e.g. file has been moved).
         *
         * @param {Number} [delay]
         * An option parameter to delay the reload. e.g. to enable the
         * user to read a message. The default is 1500
         */
        function handleReload(errorCode, errorContext, newFileDescData, delay) {
            // freeze the toolbar UI via event, so that switching to read-only mode
            // due to 'setInternalError' will be hidden during the reload
            self.trigger('docs:reload:prepare');
            self.setInternalError(errorCode, errorContext);
            const promise = newFileDescData ? getFile(newFileDescData) : $.when();
            promise.then(function (newFile) {
                // delay UI reload, so that it's not too fast for the user
                self.executeDelayed(function () {
                    if (newFile) {
                        self.reloadDocument({ fileDesc: newFile });
                    } else {
                        self.reloadDocument();
                    }
                }, _.isNumber(delay) ? delay : 1500);
            });

        }

        /**
         * Determines, dependent on the provided error code, if the client
         * should try to restore the current document nor not.
         *
         * @param {ErrorCode} errorCode
         *  The error code provided by the backend to decide if a client
         *  triggered restore document makes sense or not.
         *
         * @returns {Boolean}
         *  TRUE if a restoreDocument should be tried by the client or
         *  FALSE if not.
         */

        function shouldTryToRestoreDocument() {
            return false;
        }

        /* ATTENTION: Keep the code below. Restore is currently not implemented with OT,
        thus always return false, but keep the code in case a restore is availabe at some
        point in the future.

        function shouldTryToRestoreDocument(errorCode) {
            var result = false;

            if (ErrorCode.isErrorCode(errorCode) && errorCode.isError()) {
                var codeAsString = errorCode.getCodeAsConstant();
                switch (codeAsString) {
                    case 'SAVEDOCUMENT_FILE_HAS_CHANGED_ERROR':
                    case 'GENERAL_FILE_NOT_FOUND_ERROR':
                    case 'GENERAL_PERMISSION_READ_MISSING_ERROR':
                    case 'GENERAL_PERMISSION_WRITE_MISSING_ERROR':
                    case 'LOADDOCUMENT_CANNOT_RETRIEVE_OPERATIONS_ERROR': result = true; break;
                }
            }

            return result;
        }  */

        /**
         * Handles the notification that an asynchronous rename has been
         * processed. It must be checked that the process was successful or
         * not. In case of a success the code has to trigger a reload of the
         * renamed document. An asynchronous rename is only triggered, if the
         * storage system uses intransparent file-ids. It that case the file-id
         * changes and the client needs to open the document using the new
         * file-id.
         */
        function connectionRenamedReloadHandler(data) {

            if (self.isInQuit()) { return; }

            // error from the erver
            var error = new ErrorCode(data);
            if (error.isError()) {
                if (renameAndReloadDeferred) {
                    renameAndReloadDeferred.reject(data);
                }
                return;
            }

            // fetch new file id and name of the renamed file
            var newFileId = getStringOption(data, 'fileId', null);
            var newFileName = getStringOption(data, 'fileName', null);

            // fail if some data is missing
            if (!newFileId || !newFileName) {
                if (renameAndReloadDeferred) {
                    renameAndReloadDeferred.reject(data);
                }
                return;
            }

            if (renameAndReloadDeferred) {
                renameAndReloadDeferred.resolve(data);
            }

            // create a new file descriptor
            self.updateFileDescriptor({
                id: newFileId,
                filename: newFileName,
                title: getFileBaseName(newFileName)
            });

            // Adapt launch options to prevent removing the file as it
            // could be empty and new or a template file.
            // See #32719 "Save as with an empty document: => Load Error"
            launchOptions.action = 'load';
            delete launchOptions.template;

            // marking the document as not modified (56023)
            locallyModified = false;
            // close the current document and restart with new one
            self.reloadDocument();
        }

        function checkForPendingOperations(requestingClientUID) {

            // unsaved changes: wait some time and try again
            if (self.hasUnsavedChangesOffice()) {
                self.executeDelayed(function () {
                    checkForPendingOperations(requestingClientUID);
                }, 2000);
                return;
            }

            // just to be sure that nothing is left
            sendActions().then(function () {
                // After sending the actions we can acknowledge that
                // we are ready to lose our edit rights.
                if (rtConnectionExist()) { rtConnection.canLoseEditRights(requestingClientUID); }
                inPrepareLosingEditRights = false;
                self.getController().update();
                // more logging for RT to better see who has the edit rights
                rtLogger.log('sent canLoseEditRights');
            });
        }

        /**
         * TODO: remove this function as soon as we know how to handle edit rights
         *
         * @param {Object} data
         *  Update state from the backend.
         */
        function handleEditRequestViaUpdate(data) {
            acquireEditLogger.log('$back{lightgreen|handleEditRequestViaUpdate}', data);

            if (self.isInQuit() || (mustSyncNowMode !== null) || inPrepareLosingEditRights) { return; }
            if (oldEditClientId !== clientId) { return; }

            // try to find the UID of the user requiesting edit rights
            var requestingUser = (data && _.isArray(data.activeUsers)) ? _.find(data.activeUsers, function (user) {
                return user && (user.clientUID !== clientId) && user.requestsEditRights;
            }) : null;

            var requestingClientUID = requestingUser ? requestingUser.clientUID : null;
            if (!requestingClientUID) { return; }

            acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - clientUIDRequesting ', editRightsTransferData, getUserNameByClientUID(requestingClientUID));
            //TODO it must be possible to abort this dialog -> we need some kind of global cancel function were this kind of pending functions are gathered

            // block all requests from other users while a transfer is running
            if (editRightsTransferData) {
                acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - BLOCK OTHER REQUESTS?', requestingClientUID);

                // just ignore more requests from the same requesting client
                if ((editRightsTransferData.clientUID !== requestingClientUID) && rtConnectionExist()) {
                    acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - REQUEST BLOCKED!');
                    rtConnection.declineEditRights(requestingClientUID);
                }
                return;
            }

            // when not blocked, handle the request
            acquireEditLogger.log('$back{lightgreen__|handleEditRequestViaUpdate} - Request edit rights', sentActionsTime);

            // accept/decline transfer
            var promise = handleEditRightsTransferRequest(requestingClientUID);

            // promise fulfils with an enum whether user accepts the transfer
            promise.done(function (answer) {
                acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - answer: ' + answer);

                // user has declined transfer
                if (answer === EditRightsRequestAnswer.DECLINE) {
                    acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - EditRights transfer declined');
                    if (rtConnectionExist()) {
                        rtConnection.declineEditRights(requestingClientUID);
                    }
                    return;
                }

                // user has accepted transfer, or is inactive (both result in transferring edit rights to the other user)
                acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - EditRights transfer accepted');

                inPrepareLosingEditRights = true;

                // Switch edit mode to false, this must happen before any asynchronous
                // calculation is done!
                changeEditMode(false);

                // Set the information message code correctly and use it if the
                // users tries to edit during the switching process (46273)
                // When the edit rights request was accepted automatically due to a inactive user,
                // show the transfer message. When the user was active, don't do it because the user
                // sees a dialog which already communicates this circumstance. -> no yell flooding
                self.setCurrentInfoState(INFO_PREPARE_LOSING_EDIT_RIGHTS, {
                    showMessage: answer === EditRightsRequestAnswer.INACTIVE
                });

                updateState();
                self.getController().update();

                // invoke callback for document specific preparations
                var preparePromise = jpromise.invoke(() => self.implPrepareLoseEditRights());
                preparePromise.always(function () {
                    rtLogger.log('handle PrepareLosingEditRights');
                    self.executeDelayed(function () {
                        checkForPendingOperations(requestingClientUID);
                    }, 1000);
                });
            });

            // e.g. cancel/abort
            promise.fail(function (reason) {
                acquireEditLogger.log('$back{lightgreen|__handleEditRequestViaUpdate} - EditRights transfer failure, reason:', reason);
            });
        }

        /**
         * Aborts a running request to acquire edit rights from another user.
         */
        function abortPendingEditRightsRequest() {
            acquireEditRightsWorker.abort();
            acquireEditLogger.log('$back{SandyBrown|abortPendingEditRightsRequest}');
        }

        /**
         * Aborts a running request to transfer edit rights to another user.
         * Rejects the promise and hides the alert banner asking the user for a
         * decision.
         */
        function abortPendingEditRightsTransfer() {
            if (editRightsTransferData) {
                editRightsTransferData.promise.abort();
                acquireEditLogger.log('abortPendingEditRightsTransfer');
            }
        }

        /**
         *  Returns whether the user is considered as inactive. The user activity
         *  is updated at the following cases:
         *
         * - When a keydown event is detected an the document node at the client.
         *
         * - When a operation is send by the client (there is a special treatment if it's
         *   unknown when the last operation was send after doc loading, see below...).
         *
         * - When the client receiving edit rights (it's better to to update the timestamp
         *   at receiving, because when it would be updated at the moment the user is clicking
         *   the acquire edit rights button, it takes time until he really gets edit rights.
         *   Therefore the user would have a much shorter time period until he is
         *   detected as inactive after finally getting edit rights.)
         *
         * - When the client declined the edit rights request by clicking on the decline button.
         */
        function isUserInactive() {
            var inactiveThreshold = 120000;
            var knownWhenLastOpWasSend = sentActionsTime > 0;
            var timeDeltaToLastOp = null;
            var timeDeltaToLastTransferActivity = (_.now() - lastEditTransferActivity);

            acquireEditLogger.log('inactivCheck knownWhenLastOpWasSend', knownWhenLastOpWasSend);
            if (knownWhenLastOpWasSend) {
                // when a op was created by the user, use the difference to the last send op as delta
                timeDeltaToLastOp = Date.now() - sentActionsTime;
                acquireEditLogger.log('inactivCheck sendActionsTime', sentActionsTime, timeDeltaToLastOp);
            } else {
                // when the document finished loading an no op is created after this by the user:
                // use the time at which the doc import was finished instead of the last op,
                // otherwise the inactive check would fail forever
                timeDeltaToLastOp = Date.now() - importFinishedTime;
                acquireEditLogger.log('inactivCheck importFinishedTime', importFinishedTime, timeDeltaToLastOp);
            }

            acquireEditLogger.log('inactivCheck User is inactive?', timeDeltaToLastOp > inactiveThreshold, !wasKeyPressedWithinTime(inactiveThreshold), 'timeDeltaToLastTransfer', timeDeltaToLastTransferActivity, timeDeltaToLastTransferActivity > inactiveThreshold);
            return (timeDeltaToLastOp > inactiveThreshold) && !wasKeyPressedWithinTime(inactiveThreshold) && (timeDeltaToLastTransferActivity > inactiveThreshold);
        }

        /**
         * TODO:
         *
         * @returns {JPromise<EditRightsRequestAnswer>}
         *  The answer of this user for the request of the specified user.
         */
        function handleEditRightsTransferRequest(requestingClientUID) {
            acquireEditLogger.log('$back{MediumAquaMarine|handleEditRightsTransferRequest}');

            // immediately accept the request if user is already inactive
            if (isUserInactive()) {
                return jpromise.resolve(EditRightsRequestAnswer.INACTIVE);
            }

            // the deferred object to be fulfilled (user answers), or rejected (canceled/aborted)
            var deferred = $.Deferred();
            // create an abortable promise (to be used from outside code)
            var promise = self.createAbortablePromise(deferred);

            // create a new data object containing all information about the running request
            editRightsTransferData = { deferred, promise, clientUID: requestingClientUID };

            // show the dialog and start the auto accept timer when the user is not inactive
            acquireEditLogger.log('$back{MediumAquaMarine|__handleEditRightsTransferRequest} - callForUserInput');

            // create and show the alert banner
            docView.yell({
                type: 'question',
                //#. %1$s is the user name that is requesting edit rights
                //#. %2$d the time value after which the requests is accepted automatically
                message: gt('%1$s is asking for edit rights. This request will be accepted automatically in %2$d seconds.', getUserNameByClientUID(requestingClientUID), Math.floor(TRANSFER_EDIT_RIGHTS_TIMEOUT / 1000)),
                duration: TRANSFER_EDIT_RIGHTS_TIMEOUT,
                progress: true,
                disableCloser: true, // do not close the alert banner when clicking somewhere else
                restoreHidden: true, // restore the alert when switching applications
                priority: true, // do not hide this banner when trying to show other low-prio banners
                // add the decision buttons when the alert will be shown
                renderFn(messageNode) {

                    var button1 = $.button({ label: gt('Decline') }).addClass('btn-default').css({ marginRight: 5 });
                    button1.on('click tab', function () {
                        acquireEditLogger.log('$back{Orchid|__handleEditRightsTransferRequest} - DECLINE BUTTON PRESSED BY USER');
                        // update timestamp for the transfer activity when this client has declined the transfer by clicking the button
                        lastEditTransferActivity = Date.now();
                        acquireEditLogger.log('$back{MediumAquaMarine|__handleEditRightsTransferRequest} -> DECISION: declined');
                        deferred.resolve(EditRightsRequestAnswer.DECLINE);
                    });

                    var button2 = $.button({ label: gt('Accept') }).addClass('btn-primary');
                    button2.on('click tab', function () {
                        acquireEditLogger.log('$back{Orchid|__handleEditRightsTransferRequest} - ACCEPT BUTTON PRESSED BY USER');
                        acquireEditLogger.log('$back{MediumAquaMarine|__handleEditRightsTransferRequest} -> DECISION: accepted');
                        deferred.resolve(EditRightsRequestAnswer.ACCEPT);
                    });

                    var buttonNode = $('<div class="button-area" style="text-align:center;margin-top:10px;">');
                    messageNode.append(buttonNode.append(button1, button2));
                }
            });

            // start a timer to auto-accept the request
            var autoAcceptTimer = self.executeDelayed(function () {
                acquireEditLogger.log('$back{MediumAquaMarine|__handleEditRightsTransferRequest} - DECISION: accepted automatically');
                deferred.resolve(EditRightsRequestAnswer.ACCEPT);
            }, TRANSFER_EDIT_RIGHTS_TIMEOUT);

            // clean up when the request finishes
            promise.always(function () {
                // `null` used as indicator that no request is running
                editRightsTransferData = null;
                // abort the auto-accept timer when this user answers
                autoAcceptTimer.abort();
                // explicitly drop the alert banner
                if (docView) { docView.hideYell({ priority: true }); }
            });

            return promise;
        }

        /**
         * Updates the edit state of the application according to the passed data of
         * a server notification message.
         *
         * @param {Object} data
         *  The data of a server notification message.
         */
        function applyEditState(data) {
            acquireEditLogger.log('$back{lightblue|applyEditState} - DOC UPDATE', data);

            var // unique identifier of current user with edit rights (not used for OT)
                editClientId = isOTEnabled ? null : getStringOption(data, 'editUserId', ''),
                // display name of current user with edit rights resolved with RT id (not used for OT)
                userNameByClientUID = isOTEnabled ? null : getUserNameByClientUID(editClientId),
                // display name of current user with edit rights (using data.editUser as fallback) (not used for OT)
                editUser = isOTEnabled ? null : (userNameByClientUID ? userNameByClientUID : data.editUser),
                // current state of document edit mode (not used for OT)
                oldEditMode = isOTEnabled ? null : editMode,
                // new state of document edit mode (not used for OT)
                newEditMode = isOTEnabled ? null : oldEditMode,
                // the file descriptor of this application
                file = self.getFileDescriptor(),
                // client OSN
                clientOSN = docModel.getOperationStateNumber(),
                // local server OSN (not used for OT)
                localServerOSN = isOTEnabled ? null : getIntegerOption(data, 'serverOSN', -1),
                // possible error/warning code sent via update
                errorCode  = new ErrorCode(data),
                // locked by user id
                lockedByUserId = getIntegerOption(data, 'lockedByUserId', NOT_LOCKED_ID),
                // write protected state (will only be sent once - must be ignored if property is not set)
                writeProtected = getBooleanOption(data, 'writeProtected', null);

            if (isOTEnabled) {
                rtLogger.log('$badge{EditApp} applyEditState: clientOSN = ' + clientOSN);
            } else {
                rtLogger.log('$badge{EditApp} applyEditState: clientOSN = ' + clientOSN + ', localServerOSN = ' + localServerOSN);
            }

            // no state information or internal error may have been set while request was running
            if (internalError || !UPDATE_DATA_ATTRIBUTES.some(function (key) { return key in data; })) {
                return;
            }

            if (!isOTEnabled && editClientId !== oldEditClientId) {
                rtLogger.log('Edit rights changed, new editing user id: ' + editClientId);
                // Bug 32043: Set edit mode only if the edit user has changed, otherwise it's
                // possible that we switch back to read/write mode although we switched
                // programmatically to read-only mode due to 'preparelosingeditrights'
                newEditMode = (clientId === editClientId);
                self.setCurrentInfoState(INFO_USER_IS_CURRENTLY_EDIT_DOC, { showMessage: false });

                acquireEditLogger.log('$back{lightblue|__applyEditState} - EDITOR HAS BEEN CHANGED', '|editClientId:', editClientId, '|oldEditClientId:', oldEditClientId);
                // Since the editor has been changed, all timers for forceEdit must be aborted.
                // Special case: a empty 'editClientId' means that there is no editor (e.g. editor left), therefore don't abort force edit
                if (acquireEditRightsWorker.running && editClientId !== '') {
                    acquireEditLogger.log('$back{lightblue|__applyEditState} - FORCE TIMER ABORTED the editor has been changed');
                    abortPendingEditRightsRequest();
                }
            }

            // check if we have to update the files view (fail safe save done on server)
            if (getBooleanOption(data, 'failSafeSaveDone', false)) {
                propagateChangeFile(file);
            }

            // internal server error: stop editing completely, show permanent error banner
            if (getBooleanOption(data, 'hasErrors', false)) {
                self.setInternalError(ClientError.ERROR_GENERAL_SYNCHRONIZATION_ERROR, ErrorContext.SYNCHRONIZATION);
                rtLogger.error('synchronization to the server has been lost.');
                return;
            }

            if (!locked) {
                if (Config.RESCUE_DOCUMENT_READONLY && self.isRescueDocument()) {
                    // special document format detected - rescue documents should always be read-only!
                    locked = true;
                    changeEditMode(false);
                    self.setInternalError(ClientError.WARNING_RESCUE_DOC_CANNOT_BE_CHANGED, ErrorContext.READONLY);
                } else if ((_.isBoolean(writeProtected) && writeProtected) || getBooleanOption(launchOptions, 'isAttachmentFile', false)) {
                    // ATTENTION: The write protected property is only sent once as it's
                    // client specific. Don't evaluate it, if the property is missing -
                    // especially don't use a boolean default value!!
                    locked = true;
                    changeEditMode(false);
                    // for presentation mode of read-only files 'setInternalError' must only be called to set 'currentMessageData'
                    self.setInternalError(ClientError.WARNING_NO_PERMISSION_FOR_DOC, ErrorContext.READONLY, null, { showMessage: !docModel.isValidReadOnlyMode(), hideError: docModel.isValidReadOnlyMode() });
                } else if ((lockedByUserId !== NOT_LOCKED_ID) && (ox.user_id !== lockedByUserId)) {
                    locked = true;
                    changeEditMode(false);
                    self.setInternalError(ClientError.WARNING_DOC_IS_LOCKED, ErrorContext.READONLY);
                    documentFileLockedByUser = getStringOption(data, 'lockedByUser', '');
                }
            }

            // Update edit mode (not if document is locked due to missing write permissions or
            // we are just in transition to give up edit rights).
            if (!locked && !inPrepareLosingEditRights) {
                // set edit mode at document model
                if (isOTEnabled) {
                    changeEditMode(true); // OT_DEMO
                } else {
                    rtLogger.log('Edit mode: ' + newEditMode);
                    changeEditMode(newEditMode);

                    // Show warning if edit rights have been lost, or are still missing
                    // after connection has been re-established, or if the edit user has changed.
                    if (!newEditMode && (!connected || oldEditMode || ((editUser !== oldEditUser) && editUser.length))) {
                        oldEditUser = editUser;
                        self.setCurrentInfoState(INFO_USER_IS_CURRENTLY_EDIT_DOC);
                    }

                    // Remember old edit user id to check for edit rights instead of using editMode
                    // which can be misleading if we in the process of switching the edit rights.
                    // There is a time where the client still has edit rights but is in read-only
                    // mode to prevent the user from changing the document further. All changes which
                    // have been done should be sent to the server in time!
                    if (connected && editClientId && (editClientId !== oldEditClientId)) {
                        // set the new editor
                        setEditClientId(editClientId);

                        oldEditUser = editUser;
                    }

                    // Show success banner, if edit rights gained after read-only
                    // mode, or after connection re-established, but not directly
                    // after loading the document (oldEditMode === null).
                    if (newEditMode && (oldEditMode === false)) {
                        self.setCurrentInfoState(INFO_EDITRIGHTS_RECEIVED);
                    }

                    // Make consistency check if we just have lost the edit rights
                    // and our OSN is higher than the server OSN. The old editor
                    // must never be in front of operation, as that would mean that
                    // operations have not been sent to the server side. E.g. a
                    // timeout scenario occurred.
                    if (connected && !newEditMode && oldEditMode && (localServerOSN !== -1) && (clientOSN > localServerOSN)) {
                        self.setInternalError(ClientError.ERROR_GENERAL_SYNCHRONIZATION_ERROR, ErrorContext.SYNCHRONIZATION);
                        rtLogger.error('synchronization between client and server is broken. Switching editor into read-only mode. New edit mode: ' + newEditMode + 'Old edit mode: ' + oldEditMode + ', local Server OSN: ' + localServerOSN + ', Client OSN: ' + clientOSN);
                    }
                }

                // Show a possible error/warning sent via update
                if (errorCode.isError() || errorCode.isWarning()) {
                    self.setInternalError(errorCode, ErrorContext.GENERAL);
                }
            }

            // application is now in connected state
            connected = true;
            updateState();
        }

        /**
         * Set a new EditClientId in this editapplication instance to reflect a changed editor.
         *
         * Note: After changing to new editor, the userData for the old
         * and new client must be updated to reflect that change.
         */
        function setEditClientId(editClientId) {

            var clientLostRights = oldEditClientId;
            var clientGainedRights = editClientId;

            var changedClients = [clientGainedRights];

            if (_.isString(clientLostRights) && (clientLostRights.length > 0)) {
                changedClients.push(clientLostRights);
            }

            // IMPORTANT: oldEditClientId must be updated before 'applyUserData'
            oldEditClientId = clientGainedRights;

            // after the editor is changed the userData must be updated
            applyUserData(changedClients);
        }

        /**
         * Manages the current synchronization state after a off-/online
         * transition.
         *
         * @param {Object} failedData
         *  An optional object describing what went wrong, if other handlers
         *  detected a bad synchronization state. Can be null.
         *  @param {String} [failedData.cause]
         *      A string describing the root cause why the synchronization
         *      has failed. Must be set, if failedData is provided
         */
        function checkSynchronizationStateOT(failedData) {
            var // error code to be set
                errorCode = null,
                // do we have local changes
                localChanges = hasLocalChanges();

            function hasLocalChanges() {
                if (actionsBuffer.length > 0) { return true; }

                var rtPendingActions = rtConnectionExist() ? rtConnection.getPendingActions() : null;
                return rtPendingActions && _.isArray(rtPendingActions.actions) && (rtPendingActions.actions.length > 0);
            }

            function logSynchronizationState() {
                rtLogger.log('logSynchronizationState... ',
                    '\n     localChanges = ', localChanges,
                    '\n     rtConnection.getPendingActions() = ', rtConnectionExist() && JSON.stringify(rtConnection.getPendingActions()),
                    '\n     actionsBuffer = ', JSON.stringify(actionsBuffer),
                    '\n     mustSyncNowMode = ', mustSyncNowMode,
                    '\n     failedData = ', JSON.stringify(failedData)
                );
            }

            // Start synchronization after we received an update message which
            // is an indication that we have a clean connection. We also must be
            // sure that all pending operations stored in the RT queue have
            // been sent to the backend, before we can enable the editor again.
            if (mustSyncNowMode) {

                rtLogger.log('Checking synchronization state with server');

                // Handle a detected failed synchronization state here - other
                // bad dynamic states are detected within this function.
                if (failedData) {
                    switch (failedData.cause) {
                        case 'timeout':
                            // in case of time out show error message and give up!
                            rtLogger.log('Synchronization not possible - maybe connection instance is gone');
                            errorCode = new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_TIMEOUT);
                            errorCode.setErrorContext(ErrorContext.SYNCHRONIZATION);
                            logSynchronizationState();
                            mustSyncNowMode = null;
                            break;

                        case 'notMember':
                        case 'reset':
                        case 'error:disposed':
                            // real-time is not able to recover easily - for now give up!
                            rtLogger.log('Synchronization not possible - connection instance is gone or rt-state broken');
                            errorCode = new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_NOT_POSSIBLE);
                            errorCode.setErrorContext(ErrorContext.SYNCHRONIZATION);
                            logSynchronizationState();
                            mustSyncNowMode = null;
                            break;
                    }

                    // make more checks if we need to restore the document or
                    // if we are able to restore the connection with a automatically
                    // reload!
                    // # 51033: Improve handling of Synchronization Errors
                    tryToRestoreConnection(errorCode);
                }

                switch (mustSyncNowMode) {
                    case 'editSync':
                        if (!localChanges) {
                            rtLogger.log('Synchronization successfully completed');
                            locked = false;
                            changeEditMode(true);
                            self.setCurrentInfoState(INFO_SYNCHRONIZATION_SUCCESSFUL, { resetWarning: true });
                        } else {
                            // wait until we were able to send out local changes
                            return;
                        }
                        break;

                    case 'viewSync':
                        rtLogger.log('Synchronization for viewer completed');
                        self.setCurrentInfoState(INFO_SYNCHRONIZATION_RO_SUCCESSFUL, { resetWarning: true });
                        break;
                }

                logSynchronizationState();
                mustSyncNowMode = null;
            }
        }

        /**
         * Manages the current synchronization state after a off-/online
         * transition.
         *
         * @param {String} editClientId
         *  The current editClientId received from the latest update message.
         *
         * @param {Number} clientOSN
         *  The client osn, can be null.
         *
         * @param {Number} serverOSNSync
         *  The server osn, can be null.
         *
         * @param {Object} failedData
         *  An optional object describing what went wrong, if other handlers
         *  detected a bad synchronization state. Can be null.
         *  @param {String} [failedData.cause]
         *      A string describing the root cause why the synchronization
         *      has failed. Must be set, if failedData is provided.
         */
        function checkSynchronizationState(editClientId, clientOSN, serverOSNSync, failedData) {
            var // error code to be set
                errorCode = null,
                // do we have local changes
                localChanges = hasLocalChanges();

            function hasLocalChanges() {
                if (actionsBuffer.length > 0) { return true; }

                var rtPendingActions = rtConnectionExist() ? rtConnection.getPendingActions() : null;
                return _.isObject(rtPendingActions) && _.isArray(rtPendingActions.actions) && (rtPendingActions.actions.length > 0);
            }

            function logSynchronizationState(clientOSN, serverOSNSync) {
                rtLogger.log('logSynchronizationState... ',
                    '\n     localChanges = ', localChanges,
                    '\n     rtConnection.getPendingActions() = ', rtConnectionExist() && JSON.stringify(rtConnection.getPendingActions()),
                    '\n     actionsBuffer = ', JSON.stringify(actionsBuffer),
                    '\n     mustSyncNowMode = ', mustSyncNowMode,
                    '\n     clientOSN = ', clientOSN,
                    '\n     serverOSN = ', serverOSNSync,
                    '\n     oldEditUser = ', oldEditUser,
                    '\n     failedData = ', JSON.stringify(failedData)
                );
            }

            // Start synchronization after we received an update message which
            // is an indication that we have a clean connection. We also must be
            // sure that all pending operations stored in the RT queue have
            // been sent to the backend, before we can enable the editor again.
            if (mustSyncNowMode) {

                rtLogger.log('Checking synchronization state with server');

                // Handle a detected failed synchronization state here - other
                // bad dynamic states are detected within this function.
                if (failedData) {
                    switch (failedData.cause) {
                        case 'timeout':
                            // in case of time out show error message and give up!
                            rtLogger.log('Synchronization not possible - maybe connection instance is gone');
                            errorCode = new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_TIMEOUT);
                            errorCode.setErrorContext(ErrorContext.SYNCHRONIZATION);
                            logSynchronizationState(clientOSN, serverOSNSync);
                            mustSyncNowMode = null;
                            break;

                        case 'notMember':
                        case 'reset':
                        case 'error:disposed':
                            // real-time is not able to recover easily - for now give up!
                            rtLogger.log('Synchronization not possible - connection instance is gone or rt-state broken');
                            errorCode = new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_NOT_POSSIBLE);
                            errorCode.setErrorContext(ErrorContext.SYNCHRONIZATION);
                            logSynchronizationState(clientOSN, serverOSNSync);
                            mustSyncNowMode = null;
                            break;
                    }

                    // make more checks if we need to restore the document or
                    // if we are able to restore the connection with a automatically
                    // reload!
                    // # 51033: Improve handling of Synchronization Errors
                    tryToRestoreConnection(errorCode);
                }

                switch (mustSyncNowMode) {
                    case 'editSync':
                        if (isEditor()) {
                            if (!localChanges) {
                                rtLogger.log('Synchronization successfully completed');
                                locked = false;
                                changeEditMode(true);
                                self.setCurrentInfoState(INFO_SYNCHRONIZATION_SUCCESSFUL, { resetWarning: true });
                            } else {
                                // wait until we receive the sync state from the
                                // backend or reached the timeout
                                return;
                            }
                        } else {
                            // We are not editor anymore - check if we can collaborate
                            // in viewer mode (only possible if we have no local changes)
                            if (localChanges) {
                                // We have local changes -> no synchronization possible. Just give up!
                                rtLogger.log('Synchronization not possible - editor changed');
                                errorCode = new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_LOST_EDIT_RIGHTS);
                                errorCode.setErrorContext(ErrorContext.SYNCHRONIZATION);
                                self.restoreDocument(errorCode);
                            } else if (clientOSN >= serverOSNSync) {
                                // We received all or more operations - the editor can still make changes while we are
                                // synchronizing. Unlock application to enable the user to use the full feature set.
                                rtLogger.log('Synchronization possible - but we lost edit rights');
                                locked = false;
                                self.setCurrentInfoState(INFO_SYNCHRONIZATION_RO_SUCCESSFUL, { resetWarning: true });
                            } else {
                                // We haven't received all operations in our sync time span. We give up and try to
                                // reload the document - this is the better/faster solution if there are a lot of changes.
                                rtLogger.log('Synchronization not possible - did not receive missing operations');
                                self.reloadDocument();
                            }
                        }
                        break;

                    case 'viewSync':
                        if (clientOSN >= serverOSNSync) {
                            rtLogger.log('Synchronization for viewer completed');
                            locked = false;
                            self.setCurrentInfoState(INFO_SYNCHRONIZATION_RO_SUCCESSFUL, { resetWarning: true });
                        } else {
                            // We need to reload the document - there are no operation updates
                            // and we are not able to generate them!
                            rtLogger.log('Synchronization not possible - did not receive missing operations');
                            self.reloadDocument();
                        }
                        break;
                }

                logSynchronizationState(clientOSN, serverOSNSync);
                mustSyncNowMode = null;
            }
        }

        /**
         * Updates the file state of the application according to the passed
         * data of a server notification message.
         *
         * @param {Object} data
         *  The data of a server notification message.
         */
        function applyFileState(data) {

            var // current file descriptor
                file = self.getFileDescriptor(),
                // the new file name
                newFileName = getStringOption(data, 'fileName', ''),
                // the new sanitizedFilename
                newSanitizedFileName = getStringOption(data, 'com.openexchange.file.sanitizedFilename', ''),
                // the new file version
                newFileVersion = getStringOption(data, 'fileVersion', ''),
                // the changed components of the file descriptor
                changedFile = {};

            // file name changed after a 'rename' operation
            if ((newFileName.length > 0) && (newFileName !== getStringOption(file, 'filename'))) {
                changedFile.filename = newFileName;
            }
            // sanitized file name changed after a 'rename' operation
            if ((newSanitizedFileName.length > 0) && (newSanitizedFileName !== getStringOption(file, 'com.openexchange.file.sanitizedFilename'))) {
                changedFile['com.openexchange.file.sanitizedFilename'] = newSanitizedFileName;
            }
            // file version changed after remote client closes the modified file
            if ((newFileVersion.length > 0) && (newFileVersion !== getStringOption(file, 'version'))) {
                changedFile.version = newFileVersion;
            }

            if (!internalError && !_.isEmpty(changedFile)) {
                self.updateFileDescriptor(changedFile);
                self.getController().update();
                updateState();
            }
        }

        /**
         * Check, whether the selection is the only different object
         * in the two specified arrays containing the information about
         * the clients.
         *
         * @param {Object[]} clients1
         *  An array with user data for clients.
         *
         * @param {Object[]} clients2
         *  An array with user data for clients.
         *
         * @returns {Boolean}
         *  Whether the two specified arrays with client data are identical,
         *  if the selection of the clients is ignored.
         */
        function onlySelectionChanged(clients1, clients2) {

            var reducedClients1 = null,
                reducedClients2 = null;

            if (clients1.length !== clients2.length) { return false; } // simple shortcut

            reducedClients1 = _.copy(clients1, true);
            reducedClients2 = _.copy(clients2, true);

            // deleting the selection objects
            _.each(reducedClients1, function (client) {
                if (client.userData && client.userData.selections) { delete client.userData.selections; }
                if (client.userData && client.userData.ranges) { delete client.userData.ranges; } // 46976
            });

            _.each(reducedClients2, function (client) {
                if (client.userData && client.userData.selections) { delete client.userData.selections; }
                if (client.userData && client.userData.ranges) { delete client.userData.ranges; }
            });

            return _.isEqual(reducedClients1, reducedClients2);
        }

        /**
         * Creates and returns the URL of a server request.
         *
         * @param {String} module
         *  The name of the server module.
         *
         * @param {Object} [params]
         *  Additional parameters inserted into the URL.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.currentVersion=false]
         *      If set to true, the version stored in the file descriptor will
         *      NOT be inserted into the generated URL (thus, the server will
         *      always access the current version of the document).
         *
         * @returns {String|Null}
         *  The URL of the server request; or null, if the application is not
         *  connected to a document file, or the current session is invalid.
         */
        this.getServerModuleDocUrl = function (module, params, options) {

            // return nothing if no file is present or without rtConnection
            if (!rtConnectionExist() || !ox.session || !this.hasFileDescriptor()) { return null; }

            // the parameters for the file currently loaded
            var fileParams = this.getFileParameters(_.extend({}, options, { encodeUrl: true }));

            // add default parameters (session and UID), and file parameters
            params = _.extend({ session: ox.session, uid: this.get('uniqueID'), docuid: rtConnection.getDocUID() }, fileParams, params);

            // build and return the resulting URL
            return ox.apiRoot + '/' + module + '?' + _.map(params, function (value, name) { return name + '=' + value; }).join('&');
        };

        /**
         * Check that this client instance is editor or not.
         *
         * ATTENTION: This does not mean that the editMode is enabled as
         * we could switch into read-only mode due to an error or offline!
         *
         * @returns {Boolean}
         *  TRUE if this client is the current editor or FALSE if not.
         */
        function isEditor() {
            return isOTEnabled || (clientId === oldEditClientId);
        }

        /**
         * Updates the data of all users editing the document. There are two
         * use cases:
         *
         * 1. A user data update from the server.
         * 2. A update due to a changed editor, which must be reflected in the
         *    user data too. Therefore the related clients must be updated, by
         *    providing an array of client UIDs.
         *
         * @param {object|string[]} userData
         *  The data object of a server notification message, or an array of
         *  client UIDs that will be updated according to the current global
         *  states.
         *
         * @returns {JPromise}
         *  A promise that fulfils when the user data have been processed.
         */
        var applyUserData = this.synchronize(function (userData) {

            acquireEditLogger.log('$back{PaleGoldenRod|applyUserData}: CLIENT UPDATE', userData);

            var data = null;

            // 1) update clients
            if (_.isArray(userData)) {

                // mock needed object structure
                // -> { clients: { changed: [{ clientUID: clientUID }] } };
                data = { clients: { changed: [] } };

                // create 'data' object from userData
                _.each(userData, function (clientUID) {
                    // build changed client list
                    data.clients.changed.push({ clientUID });
                });

            // 2) server data
            } else {
                data = userData;
            }

            if (!_.isObject(data)) {
                globalLogger.error('$badge{EditApp} applyUserData: called without valid data: ' + JSON.stringify(data));
                return $.when();
            }

            // reference to the old array of active clients, for comparison
            var oldActiveClients     = activeClients;
            var clientsStatus        = getObjectOption(data, 'clients', {});
            // editClients could be generated deferred because of UserAPI use
            var editClientsPromises  = [];
            var updatedActiveClients = [];

            // TODO: remove this fast edit rights handling code - it's just a HACK to get it running
            if ((oldEditClientId === clientId) && _.isArray(clientsStatus.changed) && (clientsStatus.changed.length > 0)) {
                handleEditRequestViaUpdate({ activeUsers: clientsStatus.changed });
            }

            // update clients user data (user's current selection, etc.)
            if (_.isArray(clientsStatus.list)) {
                // complete list of clients
                updatedActiveClients = clientsStatus.list;
            } else if (_.isArray(clientsStatus.left) || _.isArray(clientsStatus.joined) || _.isArray(clientsStatus.changed)) {
                var clientsMap = {};

                // create map to speed up processing
                activeClients.forEach(function (clientData) {
                    clientsMap[clientData.clientUID] = clientData;
                });

                if (_.isArray(clientsStatus.left)) {
                    // remove clients left
                    clientsStatus.left.forEach(function (clientUID) {
                        delete clientsMap[clientUID];
                    });
                }

                if (_.isArray(clientsStatus.joined)) {
                    // add clients joined
                    clientsStatus.joined.forEach(function (clientData) {
                        clientsMap[clientData.clientUID] = clientData;
                    });
                }

                if (_.isArray(clientsStatus.changed)) {
                    // modify existing clients data
                    clientsStatus.changed.forEach(function (clientData) {
                        clientsMap[clientData.clientUID] = _.extend({}, clientsMap[clientData.clientUID], clientData);
                    });
                }

                // write back map to array
                Object.keys(clientsMap).forEach(function (clientUID) {
                    updatedActiveClients.push(clientsMap[clientUID]);
                });
            }

            // trigger a 'docs:users' event only if server data contains a user list
            if (updatedActiveClients.length === 0) { return $.when(); }

            // update the list of edit clients (all active and inactive)
            _.each(updatedActiveClients, function (client) {
                var valid        = _.isObject(client) && _.isString(client.clientUID);
                var needUserInfo = valid && (!_.isNumber(client.userId) && _.isNumber(client.id));
                if (!valid) {
                    globalLogger.warn('$badge{EditApp} applyUserData: missing identifier');
                } else {
                    if (needUserInfo) {
                        //we always fetch the name, because the realtime name can be shown in a different way (than Word)
                        editClientsPromises.push(self.getUserInfo(client.id).then(function (info) {
                            client.userDisplayName   = info.displayName;
                            client.userOperationName = info.operationName;
                            return client;
                        }));
                    } else {
                        // no need to fetch data again
                        editClientsPromises.push($.when(client));
                    }
                }
            });

            // create a promise that is bound to the lifetime of this application instance
            var promise = self.createResolvedPromise().then(function () {
                return $.when(...editClientsPromises);
            });

            // next code will not run if the document has been closed in the meantime
            return promise.done(function (...args) {

                var // whether only the selection changed
                    // -> in this case only the remote selection needs to be repainted.
                    // -> all other listeners to 'docs:users' can ignore the event (46610).
                    onlySelectionChange = false;

                // translate raw server data to internal representation of the client
                editClients = args.map(function (client) {

                    var clientDesc = client;

                    if (!_.isNumber(client.userId)) {
                        // translate a new/updated client data to our own internal representation
                        clientDesc = {};
                        clientDesc.clientUID         = client.clientUID; // server sends a unique client ID'
                        clientDesc.userId            = client.id;        // server sends user identifier as 'id'
                        clientDesc.userName          = client.userDisplayName;
                        clientDesc.userOperationName = client.userOperationName;
                        clientDesc.guest             = client.guest;
                    }

                    var wasClientPreviouslyActive = _.find(oldActiveClients, function (oldClient) {
                        return oldClient.active && oldClient === client.clientUID;
                    });

                    // at runtime, a client should only be detected as inative after a certain time,
                    // but at start, an inactive client should stay inactive
                    var activeUserHeuristc = _.isNumber(client.durationOfInactivity)
                        && ((client.durationOfInactivity < 60) && wasClientPreviouslyActive);

                    // add other volatile client properties
                    clientDesc.active = (client.active === true) || activeUserHeuristc;
                    clientDesc.remote = clientId !== clientDesc.clientUID;
                    clientDesc.editor = oldEditClientId === clientDesc.clientUID;

                    // add user data if existing
                    if (_.isObject(client.userData)) {
                        clientDesc.userData = client.userData;
                    }

                    // store unknown clients into the index map
                    if (!(clientDesc.clientUID in clientIndexes)) {
                        clientIndexes[clientDesc.clientUID] = nextClientIndex;
                        nextClientIndex += 1;
                    }

                    // add client index and color scheme index into the descriptor
                    clientDesc.clientIndex = clientIndexes[clientDesc.clientUID];
                    clientDesc.colorIndex  = self.getAuthorColorIndex(clientDesc.userOperationName);

                    return clientDesc;
                });

                // create the list of active edit clients (less than 60 seconds inactivity)
                activeClients = _.where(editClients, { active: true });

                // sort by unique client index for comparison
                activeClients = _.sortBy(activeClients, 'clientIndex');

                // notify listeners with all active clients
                if (!_.isEqual(oldActiveClients, activeClients)) {
                    globalLogger.info('$badge{EditApp} applyUserData: activeClients=', activeClients);

                    // checking whether only the selection has changed
                    onlySelectionChange = onlySelectionChanged(oldActiveClients, activeClients);

                    if (!onlySelectionChange) { self.trigger('docs:users', activeClients); }

                    // when the client that requested edit rights is not in the activeClients list anymore (e.g. left), cancel the edit transfer request (Bug 55080)
                    if (editRightsTransferData && !_.findWhere(activeClients, { clientUID: editRightsTransferData.clientUID })) {
                        abortPendingEditRightsTransfer();
                    }

                    // get old and new state for 'requestsEditRights' from the client update for the current user
                    var oldRequestsEditRightsState = _.first(_.pluck(_.where(oldActiveClients, { clientUID: clientId }), 'requestsEditRights'));
                    var newRequestsEditRightsState = _.first(_.pluck(_.where(activeClients, { clientUID: clientId }), 'requestsEditRights'));

                    // when 'requestsEditRights' for the current user changed from true to false
                    if (oldRequestsEditRightsState === true && newRequestsEditRightsState === false) {
                        acquireEditLogger.log('$back{PaleGoldenRod|applyUserData}: REQUEST_EDITRIGHTS FOR USER -> is false - > event spinner off', oldRequestsEditRightsState, newRequestsEditRightsState, oldEditClientId !== clientId);

                        // abort the background process to acquire edit rights
                        abortPendingEditRightsRequest();

                        // 1) transfer request was declined: when the current user is not the editor, the transfer request was declined
                        if (oldEditClientId !== clientId) {
                            acquireEditLogger.log('$back{PaleGoldenRod|applyUserData}: YOUR REQUEST WAS DECLINED');
                            docView.yell({ message: gt('Your request for edit rights has been declined.') });

                        // 2) client got edit rights: when the current user has edits rights in this moment and the 'requestsEditRights' changed to false, the client just got edit rights after asking for edit rights
                        } else {
                            // update timestamp for the transfer activity when this client got edit rights
                            lastEditTransferActivity = _.now();
                        }
                    }

                    self.trigger('docs:users:selection', activeClients);
                }
            });
        });

        /**
         * Collecting all OSN of the cache for the external operations.
         *
         * @returns {String[]}
         *  A sorted array with all OSNs of the cached external operations. This is an empty array, if there are
         *  no cached external operations.
         */
        function getOSNsOfExternalDataCache() {
            const allOSNs = [];
            externalDataCache.forEach(data => allOSNs.push(pick.number(data.docStatus, 'serverOSN', -1)));
            return allOSNs;
        }

        /**
         * Getting a string of all OSNs of the cached external operations.
         *
         * @returns {String}
         *  A string of all OSNs of the cached external operations..
         */
        function getOSNsOfExternalDataCacheAsString() {
            return JSON.stringify(getOSNsOfExternalDataCache());
        }

        /**
         * OT: Applying one external set of actions that is saved inside the collector for external data that could
         * not be applied directly because of a long running asynchronous local process (undo/redo, copy/paste, ...).
         *
         * @param {Object} data
         *  The data of a server notification message. This is saved in the cache 'externalDataCache'.
         */
        function handleOneExternalDataObject(data) {
            applyRemoteOperations(data, true).done(function () {
                otLogger.info(`$badge{EditApp} handleOneExternalDataObject (done-handler): State of external data cache before shift: ${getOSNsOfExternalDataCacheAsString()}.`);
                externalDataCache.shift(); // removing the first element of the cache
                otLogger.info(`$badge{EditApp} handleOneExternalDataObject (done-handler): State of external data cache after shift: ${getOSNsOfExternalDataCacheAsString()}.`);
                if (externalDataCache.length > 0) {
                    handleOneExternalDataObject(externalDataCache[0]); // recursive call
                }
            });
        }

        /**
         * OT: Handler for the 'docs:operations:completed' event.
         * This handler function is registered for the event 'docs:operations:completed', when external operations
         * cannot be applied because of an internal block of these operations. These are mainly the long running
         * asynchronous local operations caused by undo/redo, copy/paste, setAttributes and delete.
         * In this case all local operations are registered in function 'registerAction' after the
         * 'docs:operations:completed' event.
         * After the local operations are successfully registered, the remote actions can be applied
         * successfully after using OT.
         *
         * Info: This handler is triggered from event 'docs:operations:completed'. It is important that this event is
         *       triggered after all actions from the long running internal process are registered in the function
         *       'registerAction'. But this event is also triggered, when the long running process is cancelled.
         *       Therefore the external actions will also be handled correctly, if the user cancels the long
         *       running internal process.
         */
        function checkExternalOperations() {
            otLogger.info(`$badge{EditApp} checkExternalOperations. Size of external data cache: ${externalDataCache.length}, OSNs: ${getOSNsOfExternalDataCacheAsString()}, Processing actions: ${docModel.isProcessingActions()}`);
            if (isOTEnabled && (externalDataCache.length > 0) && !docModel.isProcessingActions()) {
                handleOneExternalDataObject(externalDataCache[0]);
            }
        }

        /**
         * Changes the server OSN for the document state.
         *
         * This OSN is set:
         *  - after loading the document.
         *  - after receiving an Ack from the server for the operations sent to the server.
         *  - after receicing external operations from the server.
         *
         * This OSN must not be specified by the client. Only the server 'knows' the state
         * of the document.
         *
         * INFO: Increasing only by one, because serverOSN sent from Ack might be faster
         *       than serverOSN sent with external updates (operations sent from other
         *       clients). Because the Ack is resolved inside a promise it can happen, that
         *       this is a higher value than allowed. It can happen, that the external
         *       operations are not fully applied yet. In this case the operations sent
         *       to the server might contain a serverOSN that is too high.
         *
         * @param {Number} osn
         *   The current server OSN of this document.
         *
         * @param {boolean} [fromAck=false]
         *   Whether this OSN was sent with an Ack from the server.
         *
         * @param {boolean} [fromOPsAfter=false]
         *   Whether this function was called from event 'operations:after' (DOCS-2612).
         *   This flag is only required for OT logging reasons.
         */

        function setServerOSN(osn, fromAck, fromOPsAfter) {

            var previousServerOSN;

            if (fromOPsAfter) { otLogger.info(`$badge{EditApp} setServerOSN: Coming back to set OSN from ACK triggered by operations:after event. Current server OSN: ${serverOSN}. Planned new OSN from ACK: ${osn}`); }

            if (osn > serverOSN) {

                if (fromAck) {
                    lastReceivedFromAck = osn;
                    if (isOTEnabled && docModel.isProcessingExternalOperations()) {
                        otLogger.info(`$badge{EditApp} setServerOSN: External operations are currently applied -> coming back later. Keeping OSN: ${serverOSN}. NOT setting OSN from ACK: ${osn}`);
                        docModel.one('operations:after', function () { setServerOSN(osn, fromAck, true); });
                        return; // come back later after registering for the next call of 'operations:after' (DOCS-2612)
                    }

                    // only allowed is an increase by 1. Otherwise there are still outstanding
                    // updates with external operations.
                    if (osn === serverOSN + 1) {
                        serverOSN = osn;
                        ackOSN = -1;
                    } else {
                        ackOSN = osn; // temporarily saving the OSN, until it can be set -> not updating the server OSN
                        otLogger.info(`$badge{EditApp} setServerOSN: Saving OSN from ack temporarily: ${ackOSN}`);
                    }
                } else {
                    // update of OSN is triggered by an update with external operations
                    if (osn > serverOSN + 1) {
                        // this should not happen. A new serverOSN sent with update should always only increase by 1
                        otLogger.info(`$badge{EditApp} setServerOSN: Setting server OSN. Info: Increasing OSN by more than 1. Increasing from: ${serverOSN} to ${osn}`);
                    }
                    previousServerOSN = serverOSN;
                    serverOSN = osn;

                    if (ackOSN > serverOSN + 1) {
                        otLogger.info(`$badge{EditApp} setServerOSN: Far distance ACK: ACK OSN is: ${ackOSN}. But received external operations with OSN: ${serverOSN}`); // DOCS-2612
                    }

                    // checking, if the valid ack OSN was already sent
                    if (ackOSN === serverOSN + 1) {
                        otLogger.info(`$badge{EditApp} setServerOSN: Updating server OSN with saved ack osn: ${ackOSN}. Triggering event: docs:ackosn:updated after next operations:after.`);
                        serverOSN = ackOSN;
                        ackOSN = -1;
                        // triggering 'docs:ackosn:update'after the external operations are applied (DOCS-2612)
                        docModel.one('operations:after', function () {
                            otLogger.info(`$badge{EditApp} setServerOSN: Updating server OSN with saved ack osn: ${serverOSN}. Triggering event: docs:ackosn:updated.`);
                            self.trigger('docs:ackosn:updated');
                        });
                    } else if (ackOSN >= 0 && serverOSN > ackOSN) {
                        // this must never happen
                        otLogger.error(`$badge{EditApp} setServerOSN: ERROR: Setting server OSN. Receiving OSN from external operation: ${osn}. But still have temporary OSN from acknowledge: ${ackOSN}`);
                        otLogger.error(`$badge{EditApp} setServerOSN: ERROR: Current locally used server OSN is: ${previousServerOSN}`);
                        ackOSN = -1;
                        self.trigger('docs:ackosn:updated');
                    }
                }

            } else {
                // avoiding that external operations decrease OSN after getting ACK from server
                otLogger.info(`$badge{EditApp} setServerOSN: Setting server OSN. Avoiding to set server OSN to: ${osn}. Current OSN is already: ${serverOSN}`);
            }
            otLogger.info(`$badge{EditApp} setServerOSN: Setting server OSN to: ${serverOSN}`);
            docModel.trigger('change:osn', serverOSN); // showing the server OSN in the operations pane
        }

        /**
         * Applies actions and their operations passed with an 'update' message
         * received from a remote editor of the current document in a
         * background task.
         *
         * @param {Object} data
         *  The data of a server notification message.
         *
         * @param {boolean} [fromCache=false]
         *  Whether the specified data were sent from the server (`false`), or
         *  applied by the local cache for the external data (`true`). This
         *  cache is filled, if a long running local process avoids that the
         *  external actions can be applied directly.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved after all document actions have
         *  been applied.
         */
        function applyRemoteOperations(data, fromCache) {

            if (internalError) {
                rtLogger.error('$badge{EditApp} applyRemoteOperations: failed due to internal error');
                return $.Deferred().reject();
            }

            // get the server OSN for OT mode
            var newServerOSN = isOTEnabled ? pick.number(data.docStatus, 'serverOSN', -1) : 0;
            if (newServerOSN < 0) {
                globalLogger.error('$badge{EditApp} applyRemoteActions: missing server OSN in external operations');
                return $.Deferred().reject();
            }

            // the operations in the update message
            var operations = getOperationsFromData(data);

            // update the server OSN, if there are no further external operations after OT on server side
            if (operations.length === 0) {
                if (isOTEnabled) { setServerOSN(newServerOSN); }
                return $.when();
            }

            otLogger.info(`$badge{EditApp} applyRemoteOperations: Server OSN: ${newServerOSN}, cached: ${!!fromCache}.`);

            // DOCS-892: decompress property keys and values in all operations
            handleMinifiedActions([{ operations }], true);

            // clear local undo stack if external editor changes the document (not in OT mode)
            if (!isOTEnabled) { docModel.undoManager.clearUndoActions(); }

            // more RT debug logging to better find problems
            rtLogger.log('$badge{EditApp} applyRemoteOperations: last remote operation: ' + JSON.stringify(_.last(operations)));

            if (isOTEnabled) {

                // Logging the original state of the external operations, before modified by OT and logging the current OSN of the local document
                if (Config.DEBUG && !fromCache) { self.trigger('docs:operationtrace:new:external', operations, self.getServerOSN(), newServerOSN); }

                // blocking this operation, if internal asynchronous operations are applied (copy/paste, undo, ...) or if there are already previous
                // external operations waiting in the collector and this is an external operation not from the cache.
                if (docModel.isProcessingInternalOperations() || (externalDataCache.length > 0 && !fromCache)) {
                    // nothing to do yet, coming back later, after 'docs:operations:completed'
                    otLogger.info(`$badge{EditApp} applyRemoteOperations: Not processing external operations with server OSN ${newServerOSN}. Cached: ${!!fromCache}. Processing internal OPs:  ${docModel.isProcessingInternalOperations()}.`);

                    // collecting the external operations in collector, if this data object was not already applied from external cache handler
                    if (!fromCache) { externalDataCache.push(data); }

                    otLogger.info(`$badge{EditApp} applyRemoteOperations: State of external data cache: ${getOSNsOfExternalDataCacheAsString()}.`);

                    // leaving this function without applying the external actions
                    return self.createResolvedPromise({ cached: true });
                }

                // update the server OSN caused by an update with external operations
                setServerOSN(newServerOSN);
                otLogger.info(`$badge{EditApp} applyRemoteOperations: processing external operations with server OSN ${newServerOSN}. Cached: ${!!fromCache}`);

                // checking and collecting all OSNs of external applied operations (DOCS-4284)
                if (externalAppliedOSNs[newServerOSN]) {
                    otLogger.error(`$badge{EditApp} applyRemoteOperations: processing already applied external operations with server OSN ${newServerOSN}. Cached: ${!!fromCache}`);
                    return $.when(); // leaving this function without applying the external actions -> triggering reload (?)
                }

                externalAppliedOSNs[newServerOSN] = 1;

                // transform the external operations with the pending local operations
                if (localActionsWithoutAck.length > 0) {

                    try {
                        extOpCount += operations.length;
                        operations = self.otManager.transformOperations(operations, localActionsWithoutAck);
                    } catch (err) {
                        if (err instanceof OTError) {
                            actionsBuffer = []; // avoiding that local actions are sent to other clients
                            handleReload(new ErrorCode(ClientError.ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR));
                        } else {
                            otLogger.exception(err, '$badge{EditApp} applyRemoteOperations: operation transformation failed');
                        }
                        return $.Deferred().reject({ cause: 'operation' });
                    }

                } else {
                    otLogger.trace('$badge{EditApp} applyRemoteOperations: external operations not transformed:', operations);
                }
            }

            // apply all operations asynchronously
            var promise = docModel.applyExternalOperations(operations);

            // remember that actions have been received from the server
            promise.done(function () { remotelyModified = true; });

            promise.fail(function () {

                // workaround for Bug 46778
                if (!self || self.isInQuit()) { return; }

                rtLogger.error('$badge{EditApp} applyRemoteOperations: failed');
            });

            return promise;
        }

        /**
         * Use the remote actions and the current application state to detect that
         * we have to trigger restore document. This can happen in several
         * situations. E.g. a short-offline phase were the edit rights switched and
         * the document has been modified. Handlers are called in arbitrary order by
         * real-time, etc.
         *
         * @param {Array} operations
         *  Operations received from the remote-side.
         *
         * @returns {Boolean}
         *  TRUE if the restore document function must be triggered or FALSE otherwise.
         */
        function checkStateForMustTriggerRestoreDocument(operations) {

            if (isOTEnabled) { return false; } // TODO: OT DEMO This state no longer exists

            var // result of this function
                restore = false,
                // client osn retrieved from our model
                clientOSN = docModel.getOperationStateNumber(),
                // first remote operation osn
                firstRemoteOSN = -1;

            if (Config.RESTORE_DOCUMENT_ENABLED && (operations.length > 0)) {
                if ((mustSyncNowMode === 'editSync') && (rtConnectionExist() && rtConnection.hasPendingActions())) {
                    // we were the editor, have pending actions and are currently synchronizing due to off-/online transition
                    // receive remote actions
                    restore = true;
                } else {
                    // check the client/remote op osn to detect erronous state
                    firstRemoteOSN = operations[0].osn;
                    restore = (clientOSN !== firstRemoteOSN);
                }
            }

            return restore;
        }

        /**
         * Applies the passed application state and actions passed with an
         * 'update' push message received from the real-time framework. Actions
         * will be applied in a background loop to prevent unresponsive browser
         * and warning messages from the browser. If actions from older
         * 'update' messages are still being applied, the new message data
         * passed to this method will be cached, and will be processed after
         * the background task for the old actions is finished.
         *
         * @param {unknown} data
         *  The message data received from "update" messages sent by the
         *  real-time framework.
         *
         * @returns {JPromise}
         *  A promise that will be resolved or rejected after the message data
         *  has been processed.
         */
        var applyUpdateMessageData = this.synchronize(function (data) {

            // the operations in the update message
            var operations = getOperationsFromData(data);

            // check our current state and decide to continue or stop now and request to
            // restore the document
            if (checkStateForMustTriggerRestoreDocument(operations)) {
                // Don't continue to process this update message, because it will fail.
                // Just start to provide the user the possibility to restore his/her
                // document with restoreDocument().
                if (isOTEnabled) {
                    self.setInternalError(ClientError.ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES, ErrorContext.CONNECTION);
                } else {
                    if (mustSyncNowMode) {
                        self.restoreDocument(new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_LOST_EDIT_RIGHTS));
                    } else {
                        if (isEditor()) {
                            self.restoreDocument(new ErrorCode(ClientError.ERROR_SYNCHRONIZATION_LOST_EDIT_RIGHTS));
                        } else {
                            self.setInternalError(ClientError.ERROR_GENERAL_SYNCHRONIZATION_ERROR, ErrorContext.CONNECTION);
                        }
                    }
                }

                return $.when();
            }

            // apply all external operations asynchronously
            var promise = applyRemoteOperations(data);

            // additional processing: edit mode, file descriptor, user data, etc.
            promise.done(function (result) {

                if (_.isObject(data) && !_.isObject(data.docStatus)) {
                    globalLogger.error('$badge{EditApp} broadcast_update sent bad data ' + JSON.stringify(data));
                }

                if (isOTEnabled && result && result.cached) { return; } // the external operation was cached, nothing to do yet

                // update application state (read-only mode, etc.)
                // TODO: should be done in applyEditState (data.docStatus)
                applyEditState(data.docStatus);

                // update application state (fileName, fileVersion)
                applyFileState(data);
            });

            return promise;
        });

        /**
         * Registers the passed update message data in an internal queue, and
         * starts (or continues) to process all queued messages sequentially.
         *
         * @returns {JPromise}
         *  A promise that will be resolved or rejected after the message data
         *  has been processed.
         */
        function registerUpdateMessageData(data) {

            if (!_.isObject(data)) {
                globalLogger.error('$badge{EditApp} registerUpdateMessageData: invalid response (object expected):', data);
                this.setInternalError(ClientError.ERROR_WHILE_MODIFYING_DOCUMENT, ErrorContext.GENERAL);
                return $.Deferred().reject();
            }

            rtLogger.log('$badge{EditApp} registerUpdateMessageData: received update message:', data);

            // bug 30998: set user ID in all pending messages to the new user ID
            if ((pendingRemoteUpdates.length > 0) && (clientId !== data.editUserId)) {
                rtLogger.log(' ' + pendingRemoteUpdates.length + ' existing pending updates must be changed to user "' + data.editUserId + '"');
                var extendData = { editUser: data.editUser, editUserId: data.editUserId };
                pendingRemoteUpdates.forEach(function (entryData) { _.extend(entryData, extendData); });
            }

            // add the latest message data to the pending update queue
            pendingRemoteUpdates.push(data);

            // apply the update messages sequentially
            var promise = applyUpdateMessageData(data);

            // remove message data from pending queue after the message has been processed completely
            promise.always(function () {
                pendingRemoteUpdates.shift();
                rtLogger.log('$badge{EditApp} registerUpdateMessageData: update processed, ' + pendingRemoteUpdates.length + ' pending messages left');
            });

            return promise;
        }

        /**
         * Invokes the passed callback function, and logs the time needed to
         * settle the callback promise.
         */
        function logCallback(callback, baseKey, message) {

            // allowed to pass null as callback handler
            if (!callback) { return jpromise.resolve(); }

            // the promise used for logging the time for the callback
            var promise = jpromise.invoke(callback.bind(self)).fail(function () {
                globalLogger.warn('$badge{EditApp} failed initialization step: ' + message);
            });

            // bug 32520: reject the promise chain when closing document during import
            promise = promise.then(function (result) {
                return self.isInQuit() ? jpromise.reject({ cause: 'closing' }) : result;
            });

            // track time needed for the step
            return self.launchTracker.asyncStep(promise, baseKey, '$badge{EditApp} ' + message);
        }

        /**
         * Callback executed by the base class `BaseApplication` directly after
         * construction of the MVC instances.
         */
        /*protected override*/ this.implInitializeApp = async function () {

            // check if replacement fonts are installed and load them when needed
            await importWebFonts();

            // extend the application with debug functionality from the debug package
            await importDebugMainModule(self, module => module.initializeApp(self));
        };

        /**
         * Callback executed by the base class BaseApplication to initialize
         * the file descriptor. Creates a new document or converts an existing
         * document, if specified in the launch options.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the new document has been
         *  created successfully. Otherwise, the promise will be rejected.
         */
        /*protected override*/ this.implInitializeFile = function () {

            // check if called after destructor already has destroyed instance.
            if (!self || !launchOptions) {
                globalLogger.warn('$badge{EditApp} initFileHandler: called on an already destroyed instance.');
                return $.Deferred().reject();
            }

            // parameters passed to the server request
            var requestParams = {};
            // whether to convert the file to a native format
            var convert = false;
            // the performance tracker
            var launchTracker = self.launchTracker;

            // DOCS-1600: pass UI language to server
            requestParams.initial_language = ox.language.replace(/_/g, '-');

            // add Guard settings
            if (launchOptions.cryptoAction) {
                requestParams.cryptoAction = launchOptions.cryptoAction;
            }
            if (launchOptions.decrypt_auth_code) {
                requestParams.cryptoAuth = launchOptions.decrypt_auth_code;
                requestParams.encrypted_file = true;
            }

            // create new document if required
            switch (getStringOption(launchOptions, 'action', 'load')) {
                case 'load':
                    if (isDriveFile(launchOptions.file)) {
                        // load existing document, current file must exist
                        // re-fetch the file data via getFile to ensure that launchOptions.file has
                        // a rt-id, there were cases in the past were the rt-id was missing
                        return getFile(launchOptions.file).done(function (file) {
                            launchOptions.file = file;
                        });
                    } else {
                        return $.when();
                    }
                case 'convert':
                    convert = true;
                    break;
                case 'new':
                    Object.assign(requestParams, self.implPrepareNewDoc());
                    break;
                default:
                    globalLogger.error('$badge{EditApp} initFileHandler: unsupported launch action');
                    return $.Deferred().reject();
            }

            // file options
            var templateFile = getObjectOption(launchOptions, 'templateFile');
            var preserveFileName = getBooleanOption(launchOptions, 'preserveFileName', false);
            var targetFilename = getStringOption(launchOptions, 'target_filename', DEFAULT_FILE_BASE_NAME);
            var targetFolderId = getStringOption(launchOptions, 'target_folder_id', '');

            // origin of a file attachment stored as template file in Drive (e.g. a mail)
            var origin = getObjectOption(templateFile, 'origin');

            // sets document into internal error state, returns a rejected promise with an appropriate error descriptor
            function createErrorResponse(errorCode) {
                self.setInternalError(errorCode, ErrorContext.CREATEDOC);
                return $.Deferred().reject({ cause: convert ? 'convert' : 'create' });
            }

            // create the URL options
            _.extend(requestParams, {
                action: 'createdefaultdocument',
                document_type: self.appType,
                target_filename: targetFilename,
                target_folder_id: targetFolderId,
                preserve_filename: preserveFileName,
                convert
            });

            if (launchOptions.template) {
                _.extend(requestParams, {
                    action: 'createfromtemplate',
                    file_id: templateFile.id
                });
            }

            // additional parameters according to creation mode
            if (_.isObject(templateFile)) {
                _.extend(requestParams, {
                    file_id: templateFile.id,
                    version: templateFile.version
                });
            }

            // show alert banners after document has been imported successfully
            self.waitForImportSuccess(function () {

                // the file descriptor
                var fileDesc = self.getFileDescriptor();
                // the target folder, may differ from the folder in the launch options
                // Whether the file has been created in another folder. We
                // have to check the template option which always use the
                // user folder as target. In that case no message should be shown.
                var copied = !launchOptions.template && (targetFolderId !== fileDesc.folder_id);

                // For a file created from a template we have to check the
                // converted attribute in the file descriptor to be sure
                // that it has been converted to another file format or not.
                if (launchOptions.template ? fileDesc.converted : convert) {
                    self.setCurrentInfoState(copied ?
                        INFO_DOC_CONVERT_STORED_IN_DEFFOLDER :
                        INFO_DOC_CONVERTED_AND_STORED
                    );
                } else if (copied) {
                    self.setCurrentInfoState(INFO_DOC_CREATED_IN_DEFAULTFOLDER);
                }
            });

            //optional values for changing "insertField" operation by url-hashs assigned as "fields" or "sourcefields" if it is a csv file
            var fields = locationHash('fields');
            var sourcefields = locationHash('fields-source');
            if (fields || sourcefields) {
                locationHash('fields', null);
                locationHash('fields-source', null);
                requestParams.fields = fields;
                requestParams.sourcefields = sourcefields;
            }

            // send the server request to create the new document
            var promise = self.sendRequest(FILTER_MODULE_NAME, requestParams);

            // check response whether it contains an error code
            promise = promise.then(function (response) {
                // forward response data if no error occured, otherwise create a rejected promise with a GUI message
                var error = new ErrorCode(response);
                return error.isError() ? createErrorResponse(error) : response;
            }, function () {
                // server request failed completely, create generic response data
                return createErrorResponse();
            });

            // track time needed to create the new document
            launchTracker.asyncStep(promise, 'createNewDocAfterRequest', '$badge{EditApp} New file in remote storage created');

            // re-fetch the file data via getFile to ensure that data.file has
            // a rt-id, there were cases in the past were the rt-id was missing
            promise = promise.then(function (data) {
                return getFile(data.file).then(function (file) {
                    data.file = file;
                    return data;
                });
            });

            // process data descriptor returned by the server
            promise = promise.then(function (data) {

                // callback for promise piping: leaves busy mode, forwards original state and result
                function leaveBusyEarly(resolved, response) {

                    // in the presentation app, leaveBusyDuringImport was already called (and no longer exists)
                    var leaveBusyPromise = self.isPresentationApp() ? self.createResolvedPromise() : self.leaveBusyDuringImport({ globalFailure: !resolved });
                    launchTracker.asyncStep(leaveBusyPromise, 'afterLeaveBusyEarly', '$badge{EditApp} Busy state after creation of document left');

                    return leaveBusyPromise.then(
                        // leaving busy state succeeded: return original state and response
                        function () { if (resolved) { return response; } throw response; },
                        // leaving busy state failed: always reject the promise chain
                        function (err) { throw resolved ? err : response; }
                    );
                }

                if (!_.isObject(data.file)) {
                    globalLogger.error('$badge{EditApp} initFileHandler: missing file descriptor');
                    return $.Deferred().reject();
                }

                // add the origin to the file descriptor (e.g. mail descriptor whose attachment has been copied to Drive)
                if (origin) { data.file.origin = origin; }
                data.file.source = 'drive';

                if (data.converted) {
                    // add the converted property to set/show the current info state
                    data.file.converted = data.converted;
                }
                // determine if server sent an extended response with initial document contents
                fastEmpty = _.isString(data.htmlDoc) && _.isArray(data.operations) && (data.operations.length > 0);

                if (fastEmpty) {

                    // invoke fast-empty handler provided by the application
                    var fastEmptyLoadPromise = jpromise.invoke(() => self.implFastEmptyLoad(data.htmlDoc, data.operations));

                    return fastEmptyLoadPromise
                        // leave busy mode early (also if import handler fails)
                        .then(leaveBusyEarly.bind(null, true), leaveBusyEarly.bind(null, false))
                        // forward response data to resulting piped promise
                        .then(function () { return data; });
                }

                // forward returned data to piped promise
                return data;
            });

            // return the final piped promise with the file descriptor as response
            return promise.then(function (data) { return data.file; });
        };

        /**
         * Loads the document described in the current file descriptor.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the document has been loaded,
         *  or rejected when an error has occurred.
         */
        /*protected override*/ this.implImportDocument = function () {

            var // collected operations messages
                collectedOperationMessages = [],
                // update with documen(t operations
                asynchronousLoadFinished = self.createDeferred(),
                // the promise that will be resolved with importing the document
                importPromise = null,
                // the promise that will be resolved when connection has been established
                connectPromise = null,
                // preview data processed
                previewDeferred = $.Deferred(),
                // preview data received
                previewDataReceived = false,
                // preview data completed
                previewDataCompleted = false,
                // update messages during import
                connectUpdateMessages = [],
                // clients update messages during import
                connectUpdateClientsMessages = [],
                // whether the operations might miss in server response because local storage is up-to-date
                allowMissingOperations = false,
                // storage osn
                storageOSN = -1;

            // the performance tracker
            var launchTracker = self.launchTracker;

            // set busy mode during asynchronous file loading
            function enterBusyMode() {

                docView.enterBusy({
                    skipCancelOnReadOnly: true, // 54865
                    cancelHandler() {
                        if (!self.isInQuit()) {
                            // TODO refactor? silentshutdown before quit works but it's not the nicest solution
                            executeShutdownStep('.abortOpen');
                            // shutdown connection as fast as possible
                            silentShutdownConnection();
                            // the connection will be closed, but the ui needs to be closed too
                            self.quit();
                        }
                    },
                    immediate: true,
                    topLabel: _.noI18n(self.getFullFileName()),
                    warningLabel: gt('Sorry, your document is very large. It will take some time to be loaded.'),
                    warningDelay: 10000
                });
            }

            // Function to collect update data while connecting to/importing document.
            function collectUpdateEvent(data) {
                connectUpdateMessages.push(data);
                rtLogger.log('Document status update received during document import');
            }

            // function to collect clients update data while connecting to/importing document
            function collectUpdateClientsEvent(data) {
                connectUpdateClientsMessages.push(data);
                rtLogger.log('Clients update received during document import');
            }

            // Creates a load data object which can be processed
            // by the client (some adoptions to the server answer)
            // TODO: 1. remove format adoption
            // TODO: 2. application client code should handle many chunks
            function createLoadData(operationChunks) {
                var // final load data
                    loadData = {},
                    // complete set of operations
                    operations = [],
                    // optional rescue operations
                    rescueOperations = null;

                _.each(operationChunks, function (data) {
                    var // temporary copy
                        ops = data.operations;

                    $.extend(loadData, data);

                    if (ops) {
                        delete data.operations;
                        operations = operations.concat(ops);
                    }
                    // rescue operations are only sent with the final chunk
                    if (data.rescueOperations) {
                        rescueOperations = data.rescueOperations;
                    }
                });
                loadDocInfo = loadData && loadData.syncInfo;
                rtLogger.log('loadDocInfo', loadDocInfo);
                loadData.operations = operations;
                if (rescueOperations) { loadData.rescueOperations = rescueOperations; }
                if (_.isEmpty(loadData.operations) && loadData.syncInfo && (storageOSN === parseInt(loadData.syncInfo['document-osn'], 10))) {
                    allowMissingOperations = true; // no operations sent -> file in local storage is up-to-date
                }
                return loadData;
            }

            // Creates a load data object which can be processed
            // by the client (some adoptions to the server answer)
            // TODO: 1. remove format adoption
            // TODO: 2. application client code should handle many preview chunks
            function createPreviewData(previewChunks) {
                var // active sheet
                    activeSheet = 0,
                    // operations
                    operations = [];

                _.each(previewChunks, function (data) {
                    activeSheet = _.isNumber(data.activeSheet) ? data.activeSheet : activeSheet;
                    operations = operations.concat(data.operations);
                });

                return { preview: { activeSheet, operations } };
            }

            // Handler for asynchronous loading of the requested
            // document. Waits for the load finish event to complete
            // the load process
            function waitForLoadFinishEvent(data) {
                rtLogger.log('Final operation message received for asynchronous load');

                // OT: Setting initial value of serverOSN after loading document
                if (isOTEnabled && !self.isViewerMode() && data.docStatus) { setServerOSN(data.docStatus.serverOSN); }

                if (data.hasErrors || (self.getState() === 'error')) {
                    asynchronousLoadFinished.reject(data);
                } else {
                    previewDeferred.always(function () {
                        // add final response to collected messages
                        // as it contains the first document state
                        collectedOperationMessages.push(data);
                        asynchronousLoadFinished.resolve(createLoadData(collectedOperationMessages));
                    });
                }
            }

            // Collect all operation messages for processing
            // Dependent on the type of data preview processing or
            // just collecting is necessary.
            function collectOperations(data) {

                // Check if we have to store or process this data (preview)
                // or possibly we have a fast empty scenario where we can
                // continue unconditionally
                if (fastEmpty) {
                    // Fast empty doesn't need the operations - remove them
                    // and collect all other data while following data overrides
                    // existing properties.
                    delete data.actions;
                    if (collectedOperationMessages.length > 0) {
                        _.extend(collectedOperationMessages[0], data);
                    } else {
                        collectedOperationMessages.push(data);
                    }
                    previewDeferred.resolve();
                    return;
                }

                if (data.preview === true) {
                    previewDataReceived = true;
                    // preview - just store the data until we get the
                    // remaining datas
                    collectedOperationMessages.push(data);
                    return;
                }

                if (previewDataReceived && !previewDataCompleted) {
                    // don't process preview data anymore
                    previewDataCompleted = true;
                    // create load data to be processed by preview function
                    const previewData = createPreviewData(collectedOperationMessages);
                    // reset collected operations to start collecting remaining operations
                    collectedOperationMessages = [];
                    collectedOperationMessages.push(data);

                    // preview processed promise
                    var previewPromise = processPreviewData(previewData);
                    launchTracker.asyncStep(previewPromise, 'previewProcessing', '$badge{EditApp} Preview data processed');
                    previewPromise.done(function () { previewDeferred.resolve(); });
                    previewPromise.fail(function () {
                        previewDeferred.reject();
                        genericImportFailedHandler(data);
                    });
                    return;
                }

                // resolve preview deferred only we if didn't receive preview data
                if (!previewDataReceived && (previewDeferred.state() === 'pending')) {
                    previewDeferred.resolve();
                }

                // no preview - just store the data
                collectedOperationMessages.push(data);
            }

            // After being successfully connected to real-time framework and after all actions are
            // downloaded, in 'processDocumentActions' function the following steps happen:
            //
            // 1. check for errors, local storage, fast load and fast empty documents
            // 2. calling application specific pre process handler
            // 3. applying all actions
            // 4. calling application specific post process handler
            // 5. finalizing the import process
            //
            function processDocumentActions(data) {

                var // the initial operations from the server response
                    operations = getOperationsFromData(data),
                    // update data retrieved from connect message
                    updateData = getUpdateAttributesFromData(data.docStatus),
                    // the error data sent with the server response (no indication of an error)
                    error = new ErrorCode(data),
                    // the result passed with a rejected Deferred object
                    errorResult = null;

                // called for normal doc loading, and for fast load of empty doc
                // after postprocessing is done. Applies edit state and update message data

                function finalizeImport(data, connectUpdateMessages) {

                    // determine and set possible rescue document state
                    isRescueDoc = !!data.rescueOperations || getBooleanOption(data.docStatus, 'rescueDoc', null);

                    // update user data first as we receive the user ids which we
                    // need to display the correct user names
                    applyUserData(data).always(function () {
                        // update application state (read-only mode, etc.)
                        applyEditState(data.docStatus);
                    });

                    // extract the document restore ID which must be provided, if the client
                    // wants to request a document restore
                    docRestoreId = getStringOption(data.syncInfo, 'restore_id', null);
                    renameNeedsReload = getBooleanOption(data.syncInfo, 'renameNeedsReload', false);

                    // apply all 'update' messages collected during import (wait for the import promise,
                    // otherwise methods that work with the method isImportFinished() will not run correctly)
                    self.waitForImportSuccess(function () {
                        // apply clients updates
                        rtLogger.log('Applying clients update messages, count = ' + connectUpdateClientsMessages.length);
                        _.each(connectUpdateClientsMessages, function (clientsUpdateData) { applyUserData(clientsUpdateData); });
                        // apply actions received on connect() answer
                        rtLogger.log('Applying document update messages, count = ' + connectUpdateMessages.length);
                        _.each(connectUpdateMessages, function (updateData) { registerUpdateMessageData(updateData); });

                        if (rtConnectionExist()) {
                            // remove handler specific for loading
                            rtConnection.off(RT2Protocol.RESPONSE_OPEN_DOC_CHUNK, collectOperations);
                            rtConnection.off(RT2Protocol.RESPONSE_OPEN_DOC, waitForLoadFinishEvent);
                            // remove special update client handler for loading and setup the normal client update handler
                            rtConnection.off(RT2Protocol.BROADCAST_UPDATE_CLIENTS, collectUpdateClientsEvent).on(
                                RT2Protocol.BROADCAST_UPDATE_CLIENTS, function (data) { applyUserData(data); });
                            // remove special update handler for loading and setup the normal update handler
                            rtConnection.off(RT2Protocol.BROADCAST_UPDATE, collectUpdateEvent).on(
                                RT2Protocol.BROADCAST_UPDATE, function (data) { registerUpdateMessageData(data); });
                        }

                        updateState();

                        if (Config.DEBUG && data.rescueOperations) {
                            self.trigger('docs:rescueoperations', data.rescueOperations);
                        }

                        //if we dont call it here, it will be never called
                        asynchronousLoadFinished.resolve({ action: [] });
                    });

                    return $.when();
                }

                // Loading document from local storage, if possible.
                function handleLocalStorage() {

                    // the key under which the data might be stored in browser cache
                    var loadStorageKey = null;
                    // the key containing the version of the saved string in the storage
                    var storageVersionKey = null;
                    // the saved storage version
                    var storageVersion = 0;
                    // the key under which the OSN of the file is stored in browser cache
                    var loadStorageKeyOSN = null;
                    // the key under which the operations of the file are stored in browser cache
                    var loadStorageKeyOperations = null;
                    // the key under which the file version of the file is stored in browser cache
                    var storageFileVersionKey = null;
                    // the supported extensions for the local storage
                    var supportedExtensions = null;

                    // helper function to clear content in local storage
                    function clearLocalStorage() {
                        _.each(supportedExtensions, function (oneExtensionDefintion) { localStorage.removeItem(loadStorageKey + oneExtensionDefintion.extension); });
                        localStorage.removeItem(loadStorageKeyOSN);
                        localStorage.removeItem(storageVersionKey);
                        localStorage.removeItem(storageFileVersionKey);
                        localStorage.removeItem(loadStorageKeyOperations);
                    }

                    // helper function for loading operations from local storage
                    function loadOperationsFromStorage() {

                        // operations from local storage
                        var storageOperations = getStorageValue(loadStorageKeyOperations);
                        if (!_.isArray(storageOperations)) { return false; }

                        // there might be additional operations sent from the server, if the document was modified without generating new file version.
                        // documen-osn is still valid (no new file generated), but server-osn is increased (some new operations, that need to be appended)
                        // -> this need to be marked, because less formatting caused by using local storage is no longer possible.
                        if (operations.length > 0) {
                            docModel.setExternalLocalStorageOperationsExist(true); // saving info about external operations at the model
                        }

                        // set new operations array (and deleting operations sent from server)
                        operations = storageOperations.concat(operations);

                        // the last operation need valid opl and osn (if it was not already specified by actions sent from the server)
                        var lastOperation = _.last(operations);
                        if (!_.isNumber(lastOperation.osn)) {
                            lastOperation.opl = 1;
                            lastOperation.osn = parseInt(localStorage.getItem(loadStorageKeyOSN), 10) - 1;
                        }

                        return true;
                    }

                    if ((launchOptions.action !== 'load') || !_.isObject(launchOptions.file)) { return; }

                    if (STORAGE_AVAILABLE && localStorageApp && saveFileInLocalStorage && data.syncInfo &&
                        data.syncInfo.fileVersion && Number.isFinite(data.syncInfo['document-osn']) &&
                        data.syncInfo.fileId && data.syncInfo.folderId &&
                        _.isFunction(docModel.setFullModelNode) && _.isFunction(docModel.getSupportedStorageExtensions)
                    ) {

                        loadStorageKey = data.syncInfo.fileId + '_' + data.syncInfo.folderId;
                        storageFileVersionKey = loadStorageKey + '_FILEVERSION';
                        storageVersionKey = loadStorageKey + '_STORAGEVERSION';
                        loadStorageKeyOSN = loadStorageKey + '_OSN';
                        loadStorageKeyOperations = loadStorageKey + '_OPERATIONS';

                        supportedExtensions = docModel.getSupportedStorageExtensions();

                        // checking the supported storage version
                        storageVersion = (localStorage.getItem(storageVersionKey) && parseInt(localStorage.getItem(storageVersionKey), 10)) || 1;

                        if (storageVersion >= requiredStorageVersion) {

                            globalLogger.info('$badge{EditApp} importHandler: document-osn=' + data.syncInfo['document-osn'] + ', document-rev=' + data.syncInfo.fileVersion);

                            // Support for localStorage -> comparing OSN and file version
                            if ((localStorage.getItem(storageFileVersionKey) && localStorage.getItem(storageFileVersionKey) === data.syncInfo.fileVersion) &&
                                ((localStorage.getItem(loadStorageKeyOSN)) && parseInt(localStorage.getItem(loadStorageKeyOSN), 10) === parseInt(data.syncInfo['document-osn'], 10))) {

                                // checking operations before loading document
                                if (loadOperationsFromStorage()) {

                                    // Loading the document from the local storage
                                    supportedExtensions.some(function (oneExtensionDefintion) {

                                        var // one specific local storage key
                                            oneLocalStorageKey = loadStorageKey + oneExtensionDefintion.extension,
                                            // the options for setFullModelNode
                                            options = { usedLocalStorage: true };

                                        if (_.isObject(oneExtensionDefintion.additionalOptions)) {
                                            _.extend(options, oneExtensionDefintion.additionalOptions);
                                        }

                                        var storageItem = localStorage.getItem(oneLocalStorageKey);
                                        if (storageItem) {
                                            globalLogger.info('$badge{EditApp} importHandler: loading document from local storage. Key: ' + oneLocalStorageKey + ' Length: ' + localStorage.getItem(oneLocalStorageKey).length);
                                            loadedFromStorage = true;
                                            loadMode = 'local storage';
                                            docModel.setFullModelNode(storageItem, options);
                                        } else if (oneExtensionDefintion.optional) {
                                            globalLogger.info('$badge{EditApp} importHandler: optional local storage value not found. Key: ' + oneLocalStorageKey);
                                        } else {
                                            globalLogger.info('$badge{EditApp} importHandler: key in local storage not found. Key: ' + oneLocalStorageKey + '. But found OSN key: ' + loadStorageKeyOSN);
                                            clearLocalStorage();
                                            loadedFromStorage = false;
                                            // exit the some() loop early
                                            return true;
                                        }
                                    });
                                } else {
                                    globalLogger.info('$badge{EditApp} importHandler: required operations in local storage not found. Key: ' + loadStorageKeyOperations);
                                    clearLocalStorage();
                                }
                            } else {
                                globalLogger.info('$badge{EditApp} importHandler: file in local storage not found. Key: ' + loadStorageKey);
                                clearLocalStorage();
                            }
                        } else {
                            globalLogger.info('$badge{EditApp} importHandler: version of file in local storage not supported. Key: ' + loadStorageKey);
                            clearLocalStorage();
                        }
                    }

                    // setting all model data (before (external) operations are applied)
                    if (loadedFromStorage) { docModel.trigger('update:model', { usedLocalStorage: true }); }
                }

                // loading document with fast load, if possible
                function handleFastLoad() {

                    var // the html string sent from the server
                        htmlDoc = null,
                        // a helper string for header or footer and comments
                        unionString = '',
                        // determine fast load state from server answer
                        fastLoadActive = getBooleanOption(data, 'fastLoad', false);

                    if (loadedFromStorage) { return; } // do nothing, if document was already loaded from local storage

                    //if fastLoad has an error & debug is activated we want to yell it loud
                    if (!data.htmlDoc && Config.DEBUG && !fastEmpty && docModel.setFullModelNode && fastLoadActive && (self.getFileDescriptor() && !self.getFileDescriptor().filename.endsWith('_ox'))) {
                        docView.yell({ type: 'error', headline: _.noI18n('Error in fastload'), message: _.noI18n('you should have a look in the backend logfiles ("Exception while creating the generic html document")') });
                    }

                    if (data.htmlDoc && _.isFunction(docModel.setFullModelNode) && !fastEmpty) { // if doc is empty, this was already called
                        globalLogger.info('$badge{EditApp} importHandler: load complete HTML document, osn: ', data.syncInfo['document-osn']);
                        //docModel.setFullModelNode(data.htmlDoc);

                        // markup is in form of object, parse and get data from mainDocument
                        htmlDoc = JSON.parse(data.htmlDoc);

                        if (htmlDoc.fv) {
                            docModel.setFastLoadFilterVersion(htmlDoc.fv);
                        }

                        if (htmlDoc.mainDocument) {
                            docModel.setFullModelNode(htmlDoc.mainDocument);
                        }
                        // if there is header/footer data, restore that, too
                        if (htmlDoc.headerFooter) {
                            _.each(htmlDoc.headerFooter, function (element) {
                                unionString += element;
                            });
                            docModel.setFullModelNode(unionString, { headerFooterLayer: true });
                            docModel.getPageLayout().headerFooterCollectionUpdate();
                        }
                        // if there is comments data, restore that, too
                        if (htmlDoc.comments) {
                            unionString = '';
                            _.each(htmlDoc.comments, function (element) {
                                unionString += element;
                            });
                            docModel.setFullModelNode(unionString, { commentLayer: true });
                        }

                        loadMode = 'fast load';

                        // inform listener, that fastload is done (38562)
                        docModel.trigger('fastload:done');
                    }

                }

                // detecting errors from server (and client)
                function handleErrorResults() {

                    // the error cause
                    var cause = null;

                    if (getBooleanOption(data, 'hasErrors', false)) {
                        globalLogger.error('$badge{EditApp} importHandler: server side error');
                        cause = 'server';
                    } else if (getBooleanOption(data, 'operationError', false)) {
                        globalLogger.error('$badge{EditApp} importHandler: operation failure');
                        cause = data.message;
                    } else if (error.isError()) {
                        globalLogger.error('$badge{EditApp} importHandler: server side error, errorCode=' + error.getCodeAsConstant());
                        cause = 'server';
                    } else if (clientId.length === 0) {
                        globalLogger.error('$badge{EditApp} importHandler: missing client identifier');
                        cause = 'noclientid';
                    } else if (!fastEmpty && !allowMissingOperations && (!_.isArray(operations) || (operations.length === 0))) {
                        globalLogger.error('$badge{EditApp} importHandler: missing document operations');
                        cause = 'noactions';
                    }

                    return cause ? { cause } : null;
                }

                // returns the name of the current user to be used in document operations
                function getOpUserName() {
                    return self.getUserInfo(ox.user_id).done(function (info) {
                        clientOperationName = escapeHTML(info.operationName);
                    });
                }

                // set app to read-only in case of viewer mode
                if (self.isViewerMode()) {
                    locked = true;
                    loadDocInfo = {};
                    changeEditMode(false);
                }

                // get the own unique client identifier
                clientId = getStringOption(data.docStatus, 'clientId', '');
                clientIndexes[clientId] = 0;

                // check error conditions, leaving further processing immediately in error case
                errorResult = handleErrorResults();

                if (_.isObject(errorResult)) {
                    // return rejected Deferred object with error information
                    errorResult = _.extend(errorResult, genericImportFailedHandler(data));
                    return $.Deferred().reject(errorResult);
                }

                // load the file from storage (or another source)
                handleLocalStorage();

                // checking, if the file can be loaded with fast load
                handleFastLoad();

                // connect sends us the first update event together with the action data
                // make sure that it's the first update message to be applied
                connectUpdateMessages.unshift({ docStatus: updateData });

                // shortcut for fast empty documents
                if (fastEmpty) {
                    // we already have preprocessing, actions and formatting applied, so we finalize doc importing
                    loadMode = 'fast empty';
                    return finalizeImport(data, connectUpdateMessages).then(getOpUserName);
                }

                // invoke pre-process callback function passed to the constructor of this application
                let preProcessPromise = logCallback(() => self.implPreProcessImport(), 'preProcessing', 'Import preprocess handler finished');

                // setting the full name of the client in 'getOperationName'
                preProcessPromise = preProcessPromise.then(getOpUserName).then(function () {

                    // apply actions at document model asynchronously, update progress bar
                    var applyOpsPromise = logCallback(function () {
                        var detachAppPane = initOptions && initOptions.applyActionsDetached;
                        if (detachAppPane) { docView.detachAppPane(); }
                        var promise = docModel.applyExternalOperations(operations, { expand: true });
                        if (detachAppPane) { docView.onSettled(promise, docView.attachAppPane); }
                        return promise;
                    }, 'applyOperations', operations.length + ' document operations applied');

                    applyOpsPromise = applyOpsPromise.then(function () {

                        var // the promise for post processing the document loading
                            postProcessPromise = null;

                        // After successfully applied all actions we must have a valid operation state number.
                        // Otherwise there is a synchronization error and we have to stop processing the document
                        // further showing an error message.
                        if (docModel.getOperationStateNumber() < 0) {
                            return $.Deferred().reject({ cause: 'applyactions', headline: gt('Synchronization Error') });
                        }

                        // successfully applied all actions, at least one
                        // -> invoke post-process callback function passed to the constructor of this application

                        var fromStorage = loadedFromStorage && !docModel.externalLocalStorageOperationsExist();
                        postProcessPromise = logCallback(() => self.implPostProcessImport(fromStorage), 'postProcessing', 'Import postprocess handler finished');

                        postProcessPromise = postProcessPromise.then(function () {
                            return finalizeImport(data, connectUpdateMessages);
                        }, function (response) {
                            return _.extend({ cause: 'postprocess' }, response); // failure: post-processing failed
                        });

                        return postProcessPromise;

                    }, function (response) {
                        // failure: applying actions failed
                        // RT connection already destroyed when quitting while loading
                        if (rtConnectionExist()) {
                            rtConnection.off(RT2Protocol.RESPONSE_OPEN_DOC, waitForLoadFinishEvent);
                        }
                        if (response !== 'destroy') { // 63849
                            throw _.extend({ cause: 'applyactions' }, response);
                        }
                    });

                    return applyOpsPromise;

                }, function (response) {
                    // failure (pre-processing failed): adjust error response
                    throw _.extend({ cause: 'preprocess' }, response);
                });

                return preProcessPromise;
            }

            // Processing the data received from the server connect
            // -> check for errors from server
            // -> check for special case in which preview data were sent from server
            // -> synchronize call of 'processDocumentActions' in the synchronous and asynchronous loading process
            function processPreviewData(data) {

                var // extract error object from the server response
                    error = new ErrorCode(data),
                    // data for an early preview of the imported document (must contain some operations)
                    previewData = getObjectOption(data, 'preview'),
                    // the promise for the preprocess callback
                    localPromise = error.isError() ? $.Deferred().reject(data) : $.when();

                // helper function with preview information. In this scenario it is necessary to call the preprocess
                // handler (the same that is called in 'processDocumentActions'), before operations are applied.
                function handlePreviewData() {

                    // apply the operations
                    var previewOps = pick.array(data.preview, 'operations');
                    var promise = previewOps ? docModel.applyExternalOperations(previewOps, { expand: true }) : self.createResolvedPromise();
                    launchTracker.asyncStep(promise, 'previewApplyOperations', '$badge{EditApp} ' + (previewOps ? previewOps.length : 0) + ' preview operations applied');

                    // handle result of applying the preview operations
                    promise = promise.then(function () {
                        // invoke the preview handler after the operations have been applied
                        return jpromise.invoke(() => self.implPreviewDocument(previewData)).then(success => {
                            // TODO: treat false as error, or continue silently in busy mode?
                            return success ? self.leaveBusyDuringImport() : undefined;
                        });
                    }, function (cause) {
                        // error during applying operations (bug 36639)
                        if (cause !== 'abort') { // no error handling, if cause is 'abort' (47015)
                            data.operationError = true;
                            data.message = gt('An unrecoverable error occurred while modifying the document.');
                        }
                    });

                    return launchTracker.asyncStep(promise, 'previewEnd', '$badge{EditApp} Preview postprocess handler finished');
                }

                // checking the server answer for preview data
                if (_.isArray(previewData?.operations)) {
                    localPromise = handlePreviewData(); // special synchronous handling to show document preview
                }

                return localPromise;
            }

            // during opened undo group, collect all operations in the same action
            docModel.undoManager.on('undogroup:open', function () {
                pendingAction = createAction();
            });

            // undo group closed: send the pending action to the server
            docModel.undoManager.on('undogroup:close', function () {
                if (hasPendingAction()) {
                    registerAction(pendingAction);
                }
                pendingAction = null;

                // removing the blocker for external operations. This needs to be done inside 'undogroup:close', because in this
                // function the pending actions are registered in the OT container 'localActionsWithoutAck'.
                // Improvement: This should be more restrictive
                // -> not stop blocking caused by external operations
                // -> not stop blocking, if there are long running local processes, that trigger 'docModel.setInternalOperationBlocker(false)'
                //    by their own.
                // But in any case it must be sure, that the generated operations are registered inside 'localActionsWithoutAck', before any
                // external operation can be applied.
                docModel.setInternalOperationBlocker(false);
            });

            // collect all operations generated by the document model, send them to the server
            docModel.on('operations:success', function (operations, external, userData) {

                // ignore external operations
                if (external || operationsBlocked) { return; }

                if (_.isObject(pendingAction)) {
                    // if inside an undo group, push all operations into the pending action
                    rtLogger.log('operations:success - push operations into pending action');
                    pendingAction.operations = pendingAction.operations.concat(operations);
                    if (userData) { pendingAction.userData = userData; }
                } else {
                    // otherwise, generate and send a self-contained action
                    registerAction(createAction(operations, userData));
                }
            });

            // on prepare to lose edit rights, send cached operations
            docModel.on('cacheBuffer:flush', function () {
                rtLogger.log('cacheBuffer:flush received - send cached operations');
                if (_.isFunction(mergeCachedOperationsHandler) && cacheBuffer.length) {
                    actionsBuffer = mergeCachedOperationsHandler.call(self, actionsBuffer, cacheBuffer);
                    sendActions();
                }
            });

            // error occurred while applying operations
            docModel.on('operations:error', function () {
                if (self.isImportFinished()) {
                    self.setInternalError(ClientError.ERROR_WHILE_MODIFYING_DOCUMENT, ErrorContext.GENERAL);
                    rtLogger.error('An unrecoverable error occurred while modifying the document');
                } else {
                    self.setInternalError(ClientError.ERROR_WHILE_LOADING_DOCUMENT, ErrorContext.GENERAL);
                    rtLogger.error('An unrecoverable error occurred while loading the document');
                    asynchronousLoadFinished.reject();
                }
            });

            // disable dropping of images onto the application background area
            self.getWindowNode().on('drop', false);

            // create the real-time connection to the server, register event listeners
            if (self.isViewerMode() || getBooleanOption(launchOptions, 'isAttachmentFile', false)) {
                rtConnection = new RTConnectionViewer(self, initOptions);
            } else {
                rtConnection = new RTConnection(self);
            }
            this.rtConnection = rtConnection;

            // immediately show the busy blocker screen
            if (!fastEmpty) { enterBusyMode(); }

            if (rtConnectionExist()) {
                // register the on/off & error handler for our application
                rtConnection.on(RT2Const.EVENT_OFFLINE, function (cause) {
                    connectionOfflineHandler(cause, asynchronousLoadFinished);
                });
                rtConnection.on(RT2Const.EVENT_ONLINE, connectionOnlineHandler);
                rtConnection.on(RT2Const.EVENT_ERROR, function (cause) {
                    connectionErrorHandler(cause, asynchronousLoadFinished);
                });
                rtConnection.on(RT2Const.EVENT_NORESPONSE_IN_TIME, connectionNoResponseInTimeHandler);

                // add the collectUpdateEvent handler which stores update while we
                // are loading the document
                rtConnection.on(RT2Protocol.BROADCAST_UPDATE, collectUpdateEvent);
                rtConnection.on(RT2Protocol.BROADCAST_UPDATE_CLIENTS, collectUpdateClientsEvent);
                rtConnection.on(RT2Protocol.BROADCAST_SHUTDOWN, connectionShutdownHandler);
                rtConnection.on(RT2Protocol.BROADCAST_CRASHED, connectionNodeCrashedHandler);
                rtConnection.on(RT2Protocol.BROADCAST_RENAMED_RELOAD, connectionRenamedReloadHandler);
                rtConnection.on(RT2Protocol.BROADCAST_HANGUP, connectionHangupHandler);
                rtConnection.on(RT2Protocol.BROADCAST_OT_RELOAD, connectionOTReloadHandler);
                rtConnection.on(RT2Protocol.BROADCAST_FILE_MOVED_RELOAD, connectionFileMovedReloadHandler);
            }

            // log the initalization time needed before import actually starts
            launchTracker.step('initImport', '$badge{EditApp} Import process initialized');

            // create the connection to the backend using the real-time
            // framework and the file descriptor of this application instance
            connectPromise = establishConnection();

            // saveFileInLocalStorage can be overwritten with user setting in debug mode
            checkDebugUseLocalStorage();

            // analyzing data received from the server after connection is established.
            // The generic fail handler needs to react to all failures in processConnectData (37484))
            importPromise = connectPromise.then(function (data) {
                var errorCode = new ErrorCode(data);
                var openData  = { fastEmpty };

                if (errorCode.isError()) {
                    genericImportFailedHandler(data);
                } else {

                    // set-up handler to receive asynchronous loading messages
                    if (rtConnectionExist()) {
                        rtConnection.on(RT2Protocol.RESPONSE_OPEN_DOC_CHUNK, collectOperations);
                        rtConnection.on(RT2Protocol.RESPONSE_OPEN_DOC, waitForLoadFinishEvent);
                    }

                    // set possible authorization data for encrypted documents
                    if (launchOptions.auth_code) {
                        openData = _.extend({ auth_code: launchOptions.auth_code }, openData);
                    }

                    // in case of an advisory lock we provide the action to open data
                    if (data && data.advisoryLock) {
                        openData = _.extend({ advisoryLock: data.advisoryLock }, openData);
                    }

                    // the fact that we open a newly created document must be provided
                    // to the backend which controls the recent file list.
                    if (launchOptions.action === 'new') {
                        openData = _.extend({ newDoc: true }, openData);
                    }

                    // trigger to load the document asynchronously
                    // we are notified with DocOperationsMsg and
                    // DocLoadFinishMsg (see handler above)
                    if (fastEmpty) {
                        // In case of fastEmpty true only the client knows current osn - it must
                        // be sent to the backend to enable it to process it without additional
                        // processing.
                        openData = _.extend({ fastEmptyOSN: docModel.getOperationStateNumber() }, openData);
                    }

                    // inform server about file in local storage, if it exists. Maybe it can be used for loading. (docs-974)
                    if (STORAGE_AVAILABLE && !fastEmpty && saveFileInLocalStorage) {
                        checkFileInLocalStorage(launchOptions, openData);
                        storageOSN = openData.storageOSN;
                    }

                    // send request to open the document, this causes RT connection to trigger
                    // RESPONSE_OPEN_DOC_CHUNK and RESPONSE_OPEN_DOC events
                    if (rtConnectionExist()) { rtConnection.open(openData); }

                    // process data until loading has finished or an error occurred
                    return asynchronousLoadFinished.then(processDocumentActions, function (data) {
                        // never ever call genericImportFailedHandler twice
                        // let catch-function process the error response of the async
                        // load process.
                        return jpromise.reject(data);
                    });
                }
            }).catch(function (data) {
                return jpromise.reject(genericImportFailedHandler(data));
            });

            this.onSettled(importPromise, function () {
                // TODO: show a success message if the preview was active too long?
                previewStartTime = null;
                updateState();
            });

            return importPromise;
        };

        /**
         * Extends the message data with specific actions for the display
         * function.
         *
         * @param {Object} messageData
         *  The message data to be used for the yell method provided by the
         *  view. This object will be extended dependent on the action property
         *  and application.
         */
        /*protected override*/ this.implExtendMessageData = function (messageData) {
            if (_.isObject(messageData) && _.isString(messageData.action)) {
                // special handling for actions to be shown in the error/warning message
                switch (messageData.action) {
                    case 'reload': messageData.action = { itemKey: 'document/reload', icon: RELOAD_ICON, label: RELOAD_LABEL }; break;
                    case 'acquireedit': messageData.action = { itemKey: 'document/acquireeditOrPending', icon: EDIT_ICON, label: EDIT_LABEL }; break;
                    case 'loadrestored': messageData.action = { itemKey: 'document/reload', icon: LOAD_RESTORED_ICON, label: LOAD_RESTORED_LABEL }; break;
                    default: delete messageData.action; break;
                }
            }
        };

        /**
         * Establishes a connection via the real-time framework to the backend
         * using the file descriptor data used by this application instance.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the connection results.
         */
        function establishConnection() {

            // own deferred object for logging
            var deferred = self.createDeferred();

            // track time needed to establish the connection
            return logCallback(function () {

                // connect to server, receive initial operations of the document connect deferred
                var connectPromise = rtConnectionExist() ? rtConnection.connect() : self.createRejectedPromise(); // date = undefined in reject is ok
                rtLogger.log('Channel UID:', rtConnectionExist() ? rtConnection.getChannelUID() : null);

                currentDocUid = rtConnectionExist() ? rtConnection.getDocUID() : 'null';
                currentClientUid = rtConnectionExist() ? rtConnection.getClientUID() : 'null';

                // check response from backend for possible errors
                connectPromise = connectPromise.then(function (data) {
                    var errorCode = new ErrorCode(data);
                    if (data.hasErrors || errorCode.isError()) { throw data; }
                    return data;
                });

                // check for advisory lock
                connectPromise = connectPromise.then(function (data) {
                    var errorCode = new ErrorCode(data);
                    if (errorCode.isWarning() && errorCode.getCodeAsConstant() === 'ADVISORY_LOCK_SET_WARNING') {
                        var lockInfo = JSON.parse(errorCode.getErrorValue());
                        var date = new Date(lockInfo.since);
                        var since = format(date, getDateFormat()) + ' ' + format(date, getTimeFormat());
                        var title = gt('Document in use');

                        var userInfoPromise = self.getUserInfo(lockInfo.uid).then(function (userInfo) {
                            //#. %1$s is the firstname, lastname of the user editing the document
                            //#. %2$s is the date/time the user is working on the document
                            //#. %3$s is the hostname of the system where the document currently resides
                            var message = gt('%1$s edits this document since %2$s on %3$s. To avoid data loss, please open the document on %3$s. Do you want to cancel opening it here?',
                                userInfo.displayName, since, lockInfo.hostName);
                            return docView.showQueryDialog(title, message, { buttonMode: QueryDialogButtonMode.NO_BUTTON_FULFIL });
                        }, function () {
                            //#. %1$s is the date/time user is working on the document
                            //#. %2$s is the hostnam the system where the document currently resides
                            var message = gt('Another user edits this document since %1$s on %2$s. To avoid data loss, please open the document on %3$s. Do you want to cancel opening it here?',
                                since, lockInfo.hostName);
                            return docView.showQueryDialog(title, message, { buttonMode: QueryDialogButtonMode.NO_BUTTON_FULFIL });
                        });

                        userInfoPromise = userInfoPromise.then(
                            function (buttonState) {
                                if (buttonState === true) { self.quit(); }
                                if (buttonState === false) { data = _.extend({ advisoryLock: 'reset' }, data); }
                                return data;
                            });
                        return userInfoPromise;
                    }
                    return data;
                });

                // forward promise result to deferred object
                connectPromise.done(function (result) { deferred.resolve(result); });
                connectPromise.fail(function (err) { deferred.reject(err); });

                return deferred.promise();
            }, 'connect', 'Realtime connection established');
        }

        /**
         * Generic import failed handler which handles general errors that are
         * not application dependent.
         *
         * @param {Object} response
         *  The server response containing internal error information that can
         *  be used to create UI specific strings.
         *
         * @returns {Object}
         *  The result object extended with user information to be presented to
         *  the user describing what is the root cause of the error.
         */
        function genericImportFailedHandler(response) {

            function isAjaxResponseError(response) {
                return response && response.error_desc && response.error_id && _.isString(response.error_desc) && _.isString(response.error_id);
            }

            function createGenericError(response) {
                error = new ErrorCode(ErrorCode.GENERAL_ERROR);
                // add a description when possible
                if (response && response.error_desc) {
                    var errorText = response.error_desc;
                    error.setDescription(errorText);
                }
                return error;
            }

            if (!_.isObject(self) || self.isInQuit()) {
                response = { cause: 'quit' };
            }

            // this function must only run once
            // TODO: this is necessary because of call inside 'function processDocumentActions(data)'
            // -> this needs to be improved
            if (failureAlreadyEvaluated) { return { cause: '' }; }

            var // specific error code sent by the server
                error = new ErrorCode(response),
                // the error code constant as string
                constant = error.getCodeAsConstant();

            internalError = true;
            self.implImportFailed(error);

            response = _.clone(response);

            // handle special cases and set states correctly
            switch (constant) {
                case 'GENERAL_FILE_NOT_FOUND_ERROR': movedAway = true; break;
            }

            // always have a fallback error
            if (!error.isError()) {
                if (isAjaxResponseError(response)) {
                    error = createGenericError(response);
                } else {
                    error = new ErrorCode(ClientError.ERROR_UNKNOWN_REALTIME_FAILURE);
                }

            }

            // Bug 37898: No need to set an internal error during application shut-down.
            if (!self.isInQuit()) {
                // Use showMessage false to prevent setInternalError to show the error message.
                // The base view uses a importFailed handler to show the message in a more
                // general way.
                self.setInternalError(error, ErrorContext.LOAD, null, { showMessage: false });
            }

            response = _.extend({}, response, { errorForErrorCode: self.isViewerMode() });

            // not running this function twice
            failureAlreadyEvaluated = true;

            return response;
        }

        /**
         * Creates and returns a new action containing operations and
         * additional data.
         *
         * @param {Operation[]} [operations]
         *  The operations contained by the new action. If omitted, an empty
         *  array will be inserted into the action.
         *
         * @param {object} [userData]
         *  Arbitrary user data for OT to be stored in the action.
         *
         * @returns {OperationAction}
         *  The new action.
         */
        function createAction(operations, userData) {
            var action = { operations: operations || [] };
            if (userData) { action.userData = userData; }
            return action;
        }

        /**
         * Returns whether pending operations are waiting to be sent to the
         * server.
         *
         * @returns {boolean}
         */
        function hasPendingAction() {
            return pendingAction && pendingAction.operations && (pendingAction.operations.length > 0);
        }

        /**
         * Returns whether pending operations are currently being sent to the
         * server, OR are still waiting to be sent to the server.
         */
        function hasUnsavedOperations() {
            return sendingActions || (actionsBuffer.length > 0) || hasPendingAction();
        }

        /**
         * Removes all action from the internal array `localActionsWithoutAck`
         * that contain the specified property with a truthy value.
         *
         * @param {string} propName
         *  The name of a property to be checked in each action contained in
         *  the array `localActionsWithoutAck`.
         */
        function deleteActionsWithoutAck(propName) {
            ary.deleteAllMatching(localActionsWithoutAck, action => !!action[propName]);
        }

        /**
         * Sends all actions that are currently stored in the actions buffer.
         * If called repeatedly while still sending actions, the new actions
         * buffered in the meantime will be stored in another internal buffer,
         * and will be sent afterwards, until no new actions have been inserted
         * in the actions buffer anymore.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the last real-time message
         *  containing new actions has been acknowledged, or rejected if any
         *  real-time message has failed.
         */
        var sendActions = (function () {

            // the Deferred object that will be returned to the caller
            var resultDef = null;

            // sends the current actions buffer (calls itself recursively)
            function sendActionsBuffer() {

                // we need to debug operations while we are in the process of switching edit rights
                if (Config.REALTIME_DEBUGGING && inPrepareLosingEditRights && actionsBuffer && (actionsBuffer.length > 0)) {
                    var operations = actionsBuffer[actionsBuffer.length - 1].operations;
                    if (operations && (operations.length > 0)) {
                        globalLogger.log('$badge{EditApp} sendActionsBuffer: last action:', _.last(operations));
                    }
                }

                // OT DEMO: simulating process for OT: Local operations are not sent to the server but kept in local collector
                //          This is an important testing environment for client side OT.
                if (isOTEnabled && isOTBlockingOpsMode) {
                    self.executeDelayed(sendActionsBuffer, 1000);

                    resultDef.resolve();
                    return;
                }

                // try to reduce the number of actions and operations that need to be sent to the server
                if (useOperationOptimization) {
                    var oldLength = isOTEnabled ? actionsBuffer.length : 0;
                    actionsBuffer = self.implOptimizeOperations(actionsBuffer) ?? actionsBuffer;
                    if (isOTEnabled && actionsBuffer.length !== oldLength) { deleteActionsWithoutAck('_OPTIMIZED_OPERATION_'); }
                }

                // in debug case, all operations need an update of their osn, so that the operations pane shows the server OSN of the local document,
                // with that the operations were sent to the server. This osn in the operations is not evaluated on server side.
                // This must be done, before the actionsBuffer is cloned and minified, so that the changes are also visible in the 'localActionsWithoutAck'
                // container.
                // if (Config.DEBUG && isOTEnabled) { updateOSNinOperations(actionsBuffer, serverOSN); }

                dbgDumpActions('$badge{EditApp} sendActions: sending local actions, serverOSN=' + serverOSN, actionsBuffer);

                // Tracing internal operations, when they are sent to the server, together with the current OSN
                if (Config.DEBUG) { self.trigger('docs:operationtrace:send:internal', actionsBuffer, self.getServerOSN()); }

                // DOCS-892: Cloning and minifying operations before sending them to the server
                // -> the original operations are not minified, because there are too many side effects with objects that are directly included into the operations.
                // -> OT: the local operations without acknowledgement must not be minified, because they are required for further OT transformations and the local
                //        OT handler functions do not support minified operations.
                actionsBuffer = handleMinifiedActions(actionsBuffer);

                // OT: Setting marker at all actions, that they are sent to the server and can be removed from collection after next acknowledge from server
                //     -> this marker is only set to the unminified local actions without acknowledgement, not to the minified actions in the actions buffer.
                if (isOTEnabled) {
                    localActionsWithoutAck.forEach(function (action) { action.sentToServer = true; });
                }

                // convert action objects with arbitrary data to JSON objects to be sent to server
                var jsonActions = actionsBuffer.map(action => ({
                    operations: action.operations.map(operation => {
                        operation = { ...operation };
                        delete operation.dbg_op_id;
                        return operation;
                    })
                }));

                // send the current server OSN (the collected actions might have been modified by remote operations in the meantime)
                var sendServerOSN = isOTEnabled ? serverOSN : undefined;
                // send all actions via RT connection to the server
                var sendActionsPromise = rtConnectionExist() ? rtConnection.sendActions(jsonActions, sendServerOSN) : self.createRejectedPromise();

                sendActionsPromise.done(function (data) {

                    if (isOTEnabled) {
                        var fromAck = true;
                        otLogger.info('$badge{EditApp} sendActions: Receiving acknowledge with new OSN sent from server: ' + data.body.serverOSN);
                        setServerOSN(data.body.serverOSN, fromAck); // OT: Setting the new server OSN that the server sent with the acknowledgement

                        otLogger.info('$badge{EditApp} sendActions: Trying to clear local operation cache after receiving acknowledge');
                        clearLocalActionsWithoutAck(); // OT: Removing all those operations from the action container that are now registered on server side

                        if (Config.DEBUG) { self.trigger('docs:operationtrace:ackosn', data.body.serverOSN, sendServerOSN); }
                    }

                    if (internalError) {
                        // application switched to internal error state while sending the actions buffer
                        rtLogger.error('$badge{EditApp} sendActionsBuffer: sent successfully, but internal error detected!');
                        resultDef.reject();
                    } else if (actionsBuffer.length === 0) {
                        resultDef.resolve();
                    } else if (isOTEnabled && docModel.isProcessingExternalOperations()) { // OT: Never send local operations, if external operations are still applied
                        otLogger.info('$badge{EditApp} sendActions: External operations are currently applied. Calling sendActions later in operations:after handler!');
                        docModel.one('operations:after', sendActionsAfterApplyingExternalOperations); // registering for the next call of 'operations:after'
                        resultDef.resolve(); // OT: Do not call 'sendActionsBuffer' while external operations are applied (problem with long running external operations)
                    } else if (isOTEnabled && ackOSN > 0) { // OT: Never send local operations, if external operations are still missing
                        otLogger.info('$badge{EditApp} sendActions: External operations are still missing. Calling sendActions later in operations:after handler!');
                        docModel.one('operations:after', sendActionsAfterApplyingExternalOperations); // registering for the next call of 'operations:after'
                        resultDef.resolve(); // OT: Do not call 'sendActionsBuffer' while external operations are still missing
                    } else {
                        // more actions have been registered in the meantime while sending the buffer:
                        // send these actions too while keeping 'resultDef' in pending state
                        sendActionsBuffer();
                    }
                });

                sendActionsPromise.fail(function (response) {
                    rtLogger.error('$badge{EditApp} sendActionsBuffer: rtConnection.sendActions promise failed!');
                    sendActionsFailedError = true;
                    resultDef.reject(response);
                });

                actionsBuffer = [];
            }

            return function () {

                rtLogger.log('$badge{EditApp} sendActions: entered');

                // a real-time message is currently running: return the current result Deferred object that will be resolved after all messages
                // OT: This also takes care of sending only actions to the server after receiving the acknowledge for the previous actions.
                if (sendingActions) {
                    rtLogger.log('$badge{EditApp} sendActions: sending in progress - using current promise');
                    return resultDef.promise();
                }

                // internal error: immediately return with rejected Deferred
                if (internalError) {
                    rtLogger.error('$badge{EditApp} sendActions: internal error detected - promise rejected!');
                    return $.Deferred().reject();
                }

                // Remove all operations that are superfluous because of OT. These operations must not be sent to the server
                if (isOTEnabled) {
                    ary.deleteAllMatching(actionsBuffer, action => !action.operations.length);
                    ary.deleteAllMatching(localActionsWithoutAck, action => !action.operations.length);
                }

                // no new actions: return immediately
                if (actionsBuffer.length === 0) {
                    rtLogger.log('$badge{EditApp} sendActions: no need to send action buffer empty');
                    return $.when();
                }

                if (isOTEnabled) {
                    // it is possible that the client is not up to date with all external operations. It can happen, that this client
                    // already got an acknowledge for its last operations, but there are still some missing external operations. In this
                    // case this client must not sent its operations to the server, because the OSN is not clear.
                    // Example: The client got an acknowledge with OSN 48 for his own operations, but did not receive the external
                    //          operations with OSN 46 and 47. So the clients OSN is still 45. Sending the operations to the server
                    //          with OSN 45 causes the problem, that the own operations with OSN will be applied on server side. This
                    //          breaks the document immediately. And sending the OPs with OSN 48 is also not valid, because the external
                    //          operations with OSN 46 and 47 are not included.
                    //
                    // -> this process becomes superfluous, if the Acks are sent on the same channel as the external operations
                    //
                    // This problem is not resolved completely for long running operations:
                    // Example:
                    // Client has local operation not sent to the server and receives many external operations (for example from copy/paste).
                    // Server sends: extOp 494, extOp 494, extOp 494, ACK 495, extOp 494, extOp 494
                    // After receiving the ACK with OSN 495 the client sends its partly transformed operation with OSN 495 to the server.
                    // The server will not transform the OP with the paste-operations because it has a higher OSN, so the other clients
                    // get a broken partly transformed operation.
                    //
                    // -> the server must not send ACKs before the external operations are sent completely to the clients.
                    if (ackOSN >= 0) {
                        otLogger.info('$badge{EditApp} sendActions: Not all external operations are currently applied. Coming back later, when all external ops are applied!');
                        self.once('docs:ackosn:updated', sendActionsAfterReceivingRequiredExternalOperations); // registering for the next call of 'docs:ackosn:updated'
                        return resultDef.promise();
                    }

                    // it is possible that currently external operations are applied asynchronously. This process might modify the local
                    // operations. Therefore they cannot be sent to the server yet. Instead the 'operations:after' event has to occur,
                    // so that no further external operations are applied.
                    if (docModel.isProcessingExternalOperations()) {
                        otLogger.info('$badge{EditApp} sendActions: External operations are currently applied. Coming back later in operations:after handler!');
                        docModel.one('operations:after', sendActionsAfterApplyingExternalOperations); // registering for the next call of 'operations:after'
                        return resultDef.promise();
                    }
                }

                // prepare the result Deferred object (will be kept unresolved until
                // all actions have been sent, also including subsequent iterations
                resultDef = self.createDeferred().always(function () {
                    rtLogger.log('$badge{EditApp} sendActions: current send promise resolved - sendingActions = false');
                    sendingActions = false;
                    sentActionsTime = _.now();
                    updateState();
                });
                sendingActions = true;
                updateState();

                // start sending the actions (will call itself recursively)
                sendActionsBuffer();
                return resultDef.promise();
            };

        }()); // end of local scope for sendActions() method

        /**
         * Registers the passed action to be sent to the server. All registered
         * actions will be collected and sent automatically in a debounced
         * call to the sendActions() method.
         *
         * @param {Object} action
         *  A new action to be sent to the server.
         */
        var registerAction = (function () {

            // direct callback: called every time registerAction() is called, push action to array
            function storeAction(action) {

                // whether an exisiting cache buffer was emptied
                var usedCacheBuffer = false;

                rtLogger.log(function () {
                    var lastOp = (action.operations.length > 0) ? JSON.stringify(_.last(action.operations)) : null;
                    return '$badge{EditApp} storeAction: ' + (lastOp ? ('last operation: ' + lastOp) : 'no operations provided');
                });

                if (mergeCachedOperationsHandler && cacheBuffer.length > 0) {
                    actionsBuffer = mergeCachedOperationsHandler.call(self, actionsBuffer, cacheBuffer);
                    mergeCachedOperationsHandler = null;
                    cacheBuffer = [];
                    usedCacheBuffer = true;
                }

                if (!internalError && (action.operations.length > 0 || usedCacheBuffer)) {
                    if (action.operations.length > 0) {
                        actionsBuffer.push(action);
                        if (isOTEnabled) {
                            // OT: Both caches save the identical action -> later changes caused by OT are automatically reflected
                            //     inside the actionsBuffer that will be sent to the server.
                            localActionsWithoutAck.push(action); // this is the cache used by OT

                            if (Config.DEBUG) { self.trigger('docs:operationtrace:new:internal', action.operations, serverOSN); }

                            dbgDumpActions('$badge{EditApp} registerAction: added local action without ACK to cache', localActionsWithoutAck);
                        }
                    }
                    locallyModified = true;
                    hasActionsAfterFlush = true;
                    updateState();
                }
            }

            // create the debounced method to store and send the actions
            var storeAndSendDebounced = self.debounce(storeAction, sendActions, {
                delay: sendActionsDelay,
                maxDelay: sendActionsDelay
            });

            // the resulting registerAction() method returned from local scope
            function registerAction(action) {
                if (distributeOperations) {
                    storeAndSendDebounced(action);
                } else {
                    rtLogger.log('$badge{EditApp} registerAction: cached action during non-distribution mode');
                    cacheBuffer.push(action);
                }
            }

            return registerAction;
        }());

        /**
         * Common preparations for flushing the document. Leaves the edit mode
         * for threaded comments if active, invokes the callback handler passed
         * to the constructor, and sends all pending document actions to the
         * server.
         *
         * @param {FlushReason} reason
         *  The origin of the flush request.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when the document contents have been
         *  flushed successfully, or rejected otherwise.
         */
        function prepareFlushDocument(reason) {

            // quit: ask user to drop changes in comment (rejected promise: stay in edit mode)
            var promise = docView.processUnsavedComment(reason === FlushReason.QUIT);

            // await pending rename action
            promise = promise.then(function () { return renamePromise; });

            // invoke the callback handler passed to the constructor
            promise = promise.then(() => self.implPrepareFlushDocument(reason));

            // send pending actions to the server (use a timeout in application quit)
            promise = promise.then(function () {
                var sendPromise = sendActions();
                // add a forced timeout for quitting application
                if (reason === FlushReason.QUIT) {
                    sendPromise = addTimeout(sendPromise, MAX_DELAY_FOR_SEND_ACTIONS_QUIT, 'EditApplication.prepareFlushDocument');
                }
                return sendPromise;
            });

            return promise;
        }

        /**
         * Sends all pending actions and flushes the document.
         *
         * @param {FlushReason} reason
         *  The origin of the flush request.
         *
         * @returns {JPromise<unknown>}
         *  A promise that will fulfil when the real-time message has been
         *  acknowledged. The payload from the promise is the answer from the
         *  RT2 "flushDocument" response from the server.
         */
        /*protected override*/ this.implFlushDocument = function (reason) {

            // do nothing if application is already in internal error state
            if (internalError) { return jpromise.reject(); }

            // call the flush preparation handler passed to the constructor
            var promise = prepareFlushDocument(reason);

            // when quitting, immediately switch to readonly mode after preparation
            if (reason === FlushReason.QUIT) {
                promise.always(function () { changeEditMode(false); });
            }

            // finally, do the RT flush
            promise = promise.then(function () {
                return rtConnectionExist() ? rtConnection.flushDocument() : jpromise.reject();
            });

            return promise;
        };

        /**
         * Returns an object with the realtime id. Needed e.g. as additional
         * parameter for printing as we do a context switch on the server-side
         * which needs to retrieve the connected user on this document.
         *
         * @returns {Object|null}
         *  An object with the additional, necessary print options.
         */
        /*protected override*/ this.implGetPrintParams = function () {
            return rtConnectionExist() ? {
                doc_uid: rtConnection.getDocUID(),
                client_uid: rtConnection.getClientUID()
            } : null;
        };

        /**
         * Close and saves the document. Certain leave data may be transferred in
         * this request and added to the document. Only use this in emergency
         * cases, because it's a fire and forget request. Therefore, errors while
         * saving on the server can't be acknowledged and handled by the client.
         *
         * IMPORTANT: Don't use any async code or function calls inside this handler,
         * because most browsers can't process async code inside the 'beforeunload'
         * event handler.
         *
         */
        var emergencyLeave = _.once(function () {

            browserClosing = true;

            // send an "emergencyLeave" notification to the server
            if (rtConnectionExist()) {
                const leaveData = self.implPrepareLeaveData();
                rtConnection.emergencyLeave(leaveData);
            }

            // cancel background tasks
            changeEditMode(false);
        });

        /**
         * The handler function that will be called when the application is
         * asked to be closed. If the edited document has unsaved changes, a
         * dialog will be shown asking whether to save or drop the changes, or
         * to continue editing.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved if the application can be closed
         *  (either if it is unchanged, or the user has chosen to save or lose
         *  the changes), or will be rejected if the application will continue
         *  to run (user has cancelled the dialog, save operation failed).
         */
        function beforeQuitHandler() {

            // notify listeners (used in debug module)
            self.trigger("docs:edit:beforequithandler");

            // quit immediately if the application is in error state,
            // but wait a short moment for the error log transfer
            if (internalError) {
                return waitForInternalErrorLog().catch(() => {});
            }

            // bug 64509: finish renaming the document if a textfield still has focus
            var unsavedFileName = self.docController.getUnsavedFileName();
            var promise = (unsavedFileName && !renamePromise && self.isOnline()) ? self.docController.executeItem('document/rename', unsavedFileName) : jpromise.resolve();

            // must return a resolved promise in 'timeout' scenario, so that 'hasUnsavedChangesOffice' check is reached (DOCS-2310),
            // but must stay rejected to handle cancelled dialogs correctly (DOCS-2606).
            promise = promise.then(function () {
                // don't send document actions if the application is offline
                return self.isOnline() ? prepareFlushDocument(FlushReason.QUIT).catch(function (data) {
                    if (data === 'timeout') {
                        return data;
                    } else {
                        throw data;
                    }
                }) : null;
            });

            // still pending actions: ask user whether to close the application
            promise = promise.then(function () {
                var hasChanges = isEditor() && self.hasUnsavedChangesOffice();
                var dialogText = gt('This document contains unsaved changes. Do you really want to close?');

                return hasChanges ? docView.showQueryDialog(null, dialogText) : undefined;
            });

            return promise;
        }

        /**
         * Returns a promise representing the status of a possible request
         * for sending an error log to the backend (see 'internalError').
         * After a certain time it returns a rejected promise to handle
         * stuck requests.
         *
         * @returns {jQuery.Promise}
         *  A resolved promise when there is nothing to wait for, or when the
         *  request is done. After a certain time it returns a rejected promise
         *  to handle stuck requests.
         */
        function waitForInternalErrorLog() {
            return internalErrorSendLogPromise ? addTimeout(internalErrorSendLogPromise, 4000, 'EditApplication.waitForInternalErrorLog') : jpromise.resolve();
        }

        /**
         * Returns if the rtConnection exists at the moment.
         *
         * @returns {Boolean}
         *  True when the rtConnection is existing.
         */
        function rtConnectionExist() {
            return !!(rtConnection && !rtConnection.destroyed);
        }

        /**
         * Destroys the rtConnection.
         */
        function destroyConnection() {
            if (rtConnection) {

                // immediately abort all activities to transfer edit rights
                abortPendingEditRightsRequest();
                abortPendingEditRightsTransfer();

                // destroy the RT connection object
                rtConnection.destroy();
                self.rtConnection = rtConnection = null;
            }
        }

        /**
         * Sends the current document OSN, ignores all failures.
         *
         * @returns {JPromise}
         *  A promise that fulfils when the current OSN has been sent.
         */
        function silentSyncStable() {
            return rtConnectionExist() ? rtConnection.syncStable().catch(_.noop) : jpromise.resolve();
        }

        /**
         * Executes functions to shutdown the connection safely. The following three
         * functions are needed to shutdown the rtConnection:
         * - 1. closeDocument
         * - 2. disconnect
         * - 3. destroy
         *
         * When silentShutdownConnection() was entered, none of the supported function
         * can be called outside of silentShutdownConnection() to guarantee a valid
         * execution order like written above.
         *
         * The action 'abortOpen is a special case to abort opening a document.
         *
         *   @param {string} action
         *   The action that should be executed. For supported functions see implementation.
         *
         *   @param {unknown} data
         *   Data that should be forwarded to the actual rtConnection function call.
         *
         *  @returns {JPromise}
         *  Return a rejected promise when the rtConnection is not allowed to be
         *  called. Otherwise the a promise returned by the rtConnection function call.
         */
        function executeShutdownStep(action, data, timeout) {

            // prevent typeErrors and sending something after an emergencyLeave at browser close
            if (browserClosing || !rtConnectionExist()) { return jpromise.reject(); }

            var promise = null;
            var connectionOpenState = rtConnection.getConnectOpenState();

            rtLogger.log('$badge{EditApp} executeShutdownStep: - silentShutdownEntered:', silentShutdownConnectionEntered(), ' connectionOpenState:', connectionOpenState, ' action:', action);

            // once a 'silentShutdownConnection' was invoked all other close connection actions outside this function are not permitted anymore
            if (!silentShutdownConnectionEntered()) {

                var permitAbortOpenDoc = false;
                var permitCloseDoc = false;
                var permitDisconnect = false;

                // check what actions are allowed for the current connection open state, sending invalid actions results in thrown backend warnings
                switch (connectionOpenState) {
                    case 'unknown':     permitAbortOpenDoc = false; permitCloseDoc = false;  permitDisconnect = false; break;
                    case 'joining':     permitAbortOpenDoc = false; permitCloseDoc = false;  permitDisconnect = false; break;
                    case 'joined':      permitAbortOpenDoc = false; permitCloseDoc = false;  permitDisconnect = true;  break;
                    case 'opening':     permitAbortOpenDoc = true;  permitCloseDoc = false;  permitDisconnect = true;  break;
                    case 'opened':      permitAbortOpenDoc = false; permitCloseDoc = true;   permitDisconnect = true;  break;
                }

                promise = (function () {
                    switch (action) {
                        case '.abortOpen':     return permitAbortOpenDoc ? rtConnection.abortOpen() : null;
                        case '.closeDocument': return permitCloseDoc ? rtConnection.closeDocument(data) : null;
                        case '.disconnect':    return permitDisconnect ? rtConnection.disconnect(data) : null;
                    }
                }());
            }

            // allow special force handling with when 'silentShutdownConnection' was invoked
            if (silentShutdownConnectionEntered()) {
                promise = (function () {
                    switch (action) {
                        case '.closeDocument:force': return rtConnection.closeDocument(data);
                        case '.disconnect:force':    return rtConnection.disconnect(data);
                    }
                }());
            }

            // return rejected promise if no condition above matches
            if (!promise) { return jpromise.reject(); }

            // add timeout if specified
            return timeout ? addTimeout(promise, timeout, 'EditApplication.executeShutdownStep("' + action + '")') : promise;
        }

        /**
         * Returns whether the silentShutdownConnection function was entered.
         *
         * @returns {Boolean}
         *  True when silentShutdownConnection is running or was running.
         */
        function silentShutdownConnectionEntered() {
            // silentShutdownConnectionPromise can be null, resolved or rejected, it's pending as soon as silentShutdownConnection is entered
            return !(silentShutdownConnectionPromise === null);

        }

        /**
         *  Shutdown (close, disconnect, destroy) the rtConnection to release resources
         *  on the server. The document will also be frozen because without a connection
         *  changes can't be saved anymore.
         *
         *  A global promise is set inside this function to indicate that silentShutdownConnection
         *  was entered and when the shutdown has been finished.
         *
         * @param {Object} [silentShutdownConfig]
         * Control whether close document or disconnect should be skipped during the silentShutdownConnection process.
         *
         */
        function silentShutdownConnection(silentShutdownConfig) {
            var skipCloseDocument = getBooleanOption(silentShutdownConfig, 'skipCloseDoc', false);
            var skipDisconnect = getBooleanOption(silentShutdownConfig, 'skipDisconnect', false);

            rtLogger.log('$badge{EditApp} silentShutdownConnection entered! - silentShutdownConfig:', silentShutdownConfig, 'silentShutdownConnectionEntered', silentShutdownConnectionEntered());
            // only execute silentShutdownConnection one time
            if (silentShutdownConnectionEntered()) { return; }

            // -> freeze the document
            locked = true;
            changeEditMode(false);

            // DOCS-1430: stop the background task that repeatedly sends the local OSN
            if (isOTEnabled) { osnBeacon.abort(); }

            var def = $.Deferred();
            // global flag: mark that silentShutdownConnection was entered
            silentShutdownConnectionPromise = def.promise();

            //-> remove handler
            // remove all handler (e.g. error handler & collectUpdateEvent) to freeze the current state
            // notice: this off() is wrapped from the core event hub, it's NOT directly from jQuery
            if (rtConnectionExist()) { rtConnection.off(); }

            // -> closeDocument
            var closeDef;
            if (skipCloseDocument) {
                closeDef = $.when();
            } else {
                // we want no large delay here - without a response in 500ms from the server there is probably something wrong
                closeDef = executeShutdownStep('.closeDocument:force', { no_restore: restoreDocOnClose }, 500);
            }

            // -> disconnect
            closeDef.always(function (data) {
                var leaveDef;
                if (skipDisconnect) {
                    leaveDef = $.when();
                } else {
                    // a 'timeout' is also a rejected closeDef promise after all, but we want to know the reason for the reject
                    var closeDefTimeout = data === 'timeout';
                    var closeDefRejected = closeDef.state() === 'rejected';

                    // without a successful closeDocument, 'disconnectGracefully = true' must be used to ignore the state machine on the server
                    var disconnectGracefully =  closeDefTimeout || Boolean(skipCloseDocument) || closeDefRejected;

                    rtLogger.log('$badge{EditApp} silentShutdownConnection: closeDocument rejected by timeout?:', closeDefTimeout);

                    // we want no large delay here - without a response in 500ms from the server there is probably something wrong
                    leaveDef = executeShutdownStep('.disconnect:force', disconnectGracefully, 500);
                }

                // -> destroy
                leaveDef.always(function () {
                    destroyConnection();
                    rtLogger.log('$badge{EditApp} silentShutdownConnection is finished');
                    def.resolve();
                });
            });
        }

        /**
         * Called before the application will be really closed. Make sure that this function does
         * not break on typeErrors. The GateKeeper and finalizeRTConnection must be reached.
         *
         * @param {QuitReason} reason
         *  The reason for quitting the application, as passed to the method
         *  `BaseApplication.quit()`.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved or rejected if the application can
         *  be safely closed.
         */
        function quitHandler(reason) {
            // ATTENTION: Please add code only in a way that it's called in try/catch or know what you're doing

            function finalizeRTConnection() {
                // wait until all running silentShutdownConnection has been finished
                return $.when(silentShutdownConnectionPromise).always(function () {
                    rtLogger.log('$badge{EditApp} quitHandler.finalizeRTConnection called');

                    // silentShutdownConnection destroys the rtConnection, therefore it's a valid
                    // case that the rtConnection is already destroyed at this point
                    destroyConnection();
                });
            }

            // checks that the rtConnection is in a valid shutdown state while quitting - and when not force this state
            function connectionGateKeeper() {
                try {
                    rtLogger.log('$badge{EditApp} quitHandler.connectionGateKeeper entered');
                    if (!rtConnectionExist()) { return; } // already destroyed & important to prevent typeErrors
                    if (silentShutdownConnectionEntered()) { return; } // is already handled by silentShutdownConnection

                    var connectionOpenState = rtConnection.getCloseLeaveState();
                    rtLogger.log('$badge{EditApp} quitHandler.connectionGateKeeper - connectionOpenState:', connectionOpenState);

                    switch (connectionOpenState) {
                        case 'unknown':         silentShutdownConnection();                           return;
                        case 'closing':         silentShutdownConnection({ skipCloseDoc: true });     return;
                        case 'closed':          silentShutdownConnection({ skipCloseDoc: true });     return;
                        case 'disconnecting':   /*ok*/                                                return; // means no response from .disconnect, but that's no problem
                        case 'disconnected':    /*ok*/                                                return; // default case, no missing response or silentShutdownConnection(error/abort)
                        default:                silentShutdownConnection();
                    }
                } catch (e) {
                    silentShutdownConnection();
                    globalLogger.warn('$badge{EditApp} quitHandler.connectionGateKeeper: Error caught:', e);
                }
            }

            // ATTENTION: Please add code only in a way that it's called in try/catch or know what you're doing
            var quitDef = null;

            try {
                rtLogger.log('$badge{EditApp} quitHandler: reason', reason);

                // notify listeners (used in debug module)
                self.trigger("docs:edit:quithandler");

                // when a document is reloaded, possible follow up errors shouldn't be shown, only the connection must be shutdown.
                if (reason === QuitReason.RELOAD) {
                    silentShutdownConnection();
                    quitDef = $.when();
                } else {
                    quitDef = closeAndDisconnectHandler(reason);
                }
            } catch (e) {
                quitDef = $.when();
                globalLogger.warn('$badge{EditApp} quitHander: Error caught:', e);
            }

            // ATTENTION: Please be very careful in the following code, we don't have a try/catch here
            // and errors here may prevent closing/destroying the rtconnection to free resources on the server

            quitDef = quitDef.then(connectionGateKeeper, connectionGateKeeper);

            // silentShutdownConnection it must be finished before quitting the application,
            // or depending on the timing the destructor may have destroyed needed objects
            quitDef = quitDef.then(finalizeRTConnection, finalizeRTConnection);

            return quitDef;
        }

        function closeAndDisconnectHandler(reason) {

            var // the resulting deferred object
                def = null,
                // the application window
                win = self.getWindow(),
                // the file descriptor of this application (bug 34464: may be
                // null, e.g. when quitting during creation of a new document)
                file = self.getFileDescriptor(),
                // whether to show a progress bar (enabled after a short delay)
                showProgressBar = false,
                // recent file data
                recentFileData = null,
                // a collector for the application independent extensions used in the local storage
                allExtensions = ['_OSN', '_FILEVERSION', '_STORAGEVERSION', '_OPERATIONS'],
                // whether a logout is the reason for quitting the application
                isLogout = reason === QuitReason.LOGOUT;

            // the performance tracker
            var launchTracker = self.launchTracker;

            // updating the progress bar during saving file in storage
            function progressHandler(progress) {
                if (showProgressBar) { docView.updateBusyProgress(progress); }
            }

            // deleting all keys in the local storage that are older than a specified expiry date
            // and set the expiry date for the current file
            function cleanupLocalStorage(saveKey) {

                // the expiry date key used in the local storage
                var expiryDateString = 'appsuiteDocsExpiryDates';
                // the expiry dates saved in the local storage
                var expiryDates = localStorage.getItem(expiryDateString);
                // the current date
                var currentDate = _.now();
                // the maximum age of documents in the local storage
                var maxAge = 604800000; // 1 week

                // helper function to collect the storage extensions for all apps
                function collectAllAppExtensions() {
                    var allStorageExtensions = _.pluck(docModel.getSupportedStorageExtensions({ allApps: true }), 'extension');
                    return allStorageExtensions.concat(allExtensions);
                }

                if (expiryDates) {

                    expiryDates = JSON.parse(expiryDates);

                    if (_.isObject(expiryDates)) {
                        _.each(expiryDates, function (date, key) {
                            if (key !== saveKey && currentDate > date) {
                                // delete all keys starting with 'key'
                                var allAppExtensions = allAppExtensions || collectAllAppExtensions();

                                _.each(allAppExtensions, function (oneExt) {
                                    var oneKey = key + oneExt;
                                    localStorage.removeItem(oneKey);
                                });
                                delete expiryDates[key];
                            }
                        });
                    } else {
                        expiryDates = null; // something went wrong
                    }
                }

                // setting the new expiry date
                expiryDates = expiryDates || {};

                expiryDates[saveKey] = currentDate + maxAge;
                localStorage.setItem(expiryDateString, JSON.stringify(expiryDates));
            }

            // trying to save the file in the local storage
            function saveFileInStorage(data) {
                var // a timer for measuring conversion time
                    startTime = null,
                    // the key used in the local storage
                    saveKey = null,
                    // the key used in the local storage to save the OSN, file version and storage version
                    saveKeyOSN = null, versionKey = null, storageVersionKey = null, operationsKey = null,
                    // the resulting deferred object
                    def = null,
                    // whether the document was modified
                    newVersion = (locallyModified || locallyRenamed || remotelyModified),
                    // the operation state number of this local client
                    clientOSN = docModel.getOperationStateNumber(),
                    // a counter for the different strings in the local storage
                    counter = 0,
                    // the maximum number of previous versions, that are checked to be deleted
                    maxOldVersions = 20;

                // helper function to clear content in local storage
                function clearLocalStorage(options) {

                    var // whether old registry entries shall be removed
                        migration = getBooleanOption(options, 'migration', false);

                    // deleting all currently used registry entries
                    _.each(allExtensions, function (oneExtension) { localStorage.removeItem(saveKey + oneExtension); });

                    // Migration code: Removing old registry values, where the file version was part of the base key (these are no longer used)
                    if (migration) {
                        if (Number.isFinite(data.syncInfo.fileVersion)) {
                            _.each(_.range((data.syncInfo.fileVersion > maxOldVersions) ? (data.syncInfo.fileVersion - maxOldVersions) : 0, data.syncInfo.fileVersion), function (number) {
                                _.each(allExtensions, function (oneExtension) { localStorage.removeItem(saveKey + '_' + number + oneExtension); });
                            });
                        } else {
                            localStorage.removeItem(saveKey + '_' + data.syncInfo.fileVersion);
                            localStorage.removeItem(saveKey + '_CURRENTVERSION');
                        }
                    }
                }

                // saveFileInLocalStorage can be overwritten with user setting in debug mode
                checkDebugUseLocalStorage();

                // saving file in local storage, using the file version returned from the server (if storage can be used)
                // Bug 48086: The localStorage is cleared at logout. Since this function is called after that logout 'clear' call,
                // no private data should be stored to the localStorage when 'isLogout' is true.
                if ((newVersion || !loadedFromStorage) && data && data.syncInfo && !isLogout && !self.isDocumentEncrypted() &&
                    STORAGE_AVAILABLE && localStorageApp && saveFileInLocalStorage && _.isFunction(docModel.getFullModelDescription) &&
                    data.syncInfo.fileVersion && data.syncInfo.fileId && data.syncInfo.folderId &&
                    Number.isFinite(data.syncInfo['document-osn']) && (clientOSN === data.syncInfo['document-osn'])
                ) {

                    // defining the base key in the local storage
                    saveKey = data.syncInfo.fileId + '_' + data.syncInfo.folderId;

                    // clean up local storage, delete old entries, set expiry date for current file (docs-978)
                    cleanupLocalStorage(saveKey);

                    def = $.Deferred();

                    // show the progress bar, if saving takes more than 1.5 seconds
                    self.executeDelayed(function () { showProgressBar = true; }, 1500);

                    // create the string describing the document to save it in local storage
                    self.executeDelayed(function () {

                        def.notify(0.1);

                        // creating the converted document to be stored in local storage
                        startTime = _.now();

                        docModel.getFullModelDescription(def, 0.1, 0.9)
                        .always(function () {

                            // expanding this list, some layers might no longer be used
                            if (_.isFunction(docModel.getSupportedStorageExtensions)) {
                                _.each(docModel.getSupportedStorageExtensions(), function (oneExtension) {
                                    if (oneExtension && _.isString(oneExtension.extension)) { allExtensions.push(oneExtension.extension); }
                                });
                            }

                            // removing old content in the local storage (all possible layers need to be removed, even if they are no longer used)
                            clearLocalStorage({ migration: true });
                        })
                        .done(function (docStringList) {

                            function clearStorageAndBreak(errMsg) {
                                clearLocalStorage();
                                globalLogger.info('$badge{EditApp} quitHandler: ' + errMsg);
                                return false; // to exit the Array.every() loop early
                            }

                            // and finally writing all strings from each layer into the registry
                            docStringList.every(function (oneStringObject) {

                                var // the html string for one layer
                                    docString = oneStringObject.htmlString,
                                    // the save key for this one layer
                                    localSaveKey = saveKey + oneStringObject.extension,
                                    // the operations that need to be saved in local storage
                                    storageOperations = null,
                                    // the string containing the collected operations
                                    operationString = null;

                                globalLogger.info('$badge{EditApp} quitHandler: created document string with size: ' + docString.length + '. Time for conversion: ' + (_.now() - startTime) + ' ms.');

                                try {
                                    if (counter === 0) {
                                        // defining keys used in the local storage
                                        saveKeyOSN = saveKey + '_OSN';
                                        versionKey = saveKey + '_FILEVERSION';
                                        storageVersionKey = saveKey + '_STORAGEVERSION';
                                        operationsKey = saveKey + '_OPERATIONS';
                                        // saving the keys in the local storage
                                        localStorage.setItem(saveKeyOSN, data.syncInfo['document-osn']);
                                        localStorage.setItem(storageVersionKey, supportedStorageVersion);
                                        localStorage.setItem(versionKey, data.syncInfo.fileVersion);
                                        // ... and also saving the operations
                                        storageOperations = docModel.getStorageOperationCollector();
                                        _.each(storageOperations, function (op) {
                                            delete op.osn;
                                            delete op.opl;
                                        });
                                        operationString = JSON.stringify(storageOperations);
                                        localStorage.setItem(operationsKey, operationString);

                                        // checking, if saving operations was successful
                                        if (localStorage.getItem(operationsKey) && (localStorage.getItem(operationsKey).length === operationString.length)) {
                                            globalLogger.info('$badge{EditApp} quitHandler: successfully saved document operations in local storage with key: ' + operationsKey);
                                        } else {
                                            return clearStorageAndBreak('failed to save document operations in local storage');
                                        }
                                    }
                                    // saving string in the cache, if there is sufficient space
                                    localStorage.setItem(localSaveKey, docString);

                                    // checking, if saving was successful
                                    var localSaveItem = localStorage.getItem(localSaveKey);
                                    if (localSaveItem && (localSaveItem.length === docString.length)) {
                                        globalLogger.info('$badge{EditApp} quitHandler: successfully saved document in local storage with key: ' + localSaveKey);
                                    } else {
                                        return clearStorageAndBreak('failed to save document in local storage');
                                    }

                                    if (counter === 0) {
                                        var saveOSNItem = localStorage.getItem(saveKeyOSN);
                                        var saveOSN = saveOSNItem ? parseInt(saveOSNItem, 10) : -1;
                                        if (saveOSN === data.syncInfo['document-osn']) {
                                            globalLogger.info('$badge{EditApp} quitHandler: successfully saved OSN for document in local storage with key: ' + saveKeyOSN + ' : ' + saveOSN);
                                        } else {
                                            return clearStorageAndBreak('failed to save OSN for document in local storage');
                                        }
                                    }

                                    counter++;
                                    return true;

                                } catch {
                                    // do nothing, simply not using local storage
                                    return clearStorageAndBreak('failed to save document and/or document OSN in local storage');
                                }
                            });
                        })
                        .always(function () {
                            def.notify(1.0);
                            def.resolve(data);
                        });
                    });
                }

                return (def === null) ? data : def.promise();
            }

            function propagateChangedFile() {
                return propagateChangeFile(file);
            }

            // window may be gone already, e.g. during browser unload
            if (win) { win.busy(); }

            // whether the document is modified and needs a new version
            var modified = locallyModified || locallyRenamed || remotelyModified || movedAway;
            // whether the document has been created from default or explicit template
            var fromTemplate = (launchOptions.action === 'new') || launchOptions.template;

            if (file && (modified || Config.LOG_PERFORMANCE_DATA || (!fromTemplate && (editClients.length <= 1)))) {
                recentFileData = { file: _.extend({ last_opened: Date.now() }, file), app: self.appType };

                // docs-950: performance data is now logged via ajax request (same process as viewer uses in core module).
                launchTracker.setItem('LocalStorage', loadedFromStorage);
                launchTracker.setItem('FastEmptyLoad', fastEmpty);
                jpromise.floating(launchTracker.sendData());
            }

            var isRTReady = rtConnectionExist() && rtConnection.isConnected() && !self.isShutdownReceived();

            // Don't call closeDocument when we are offline or detected a bad connection state
            if (file && isRTReady) {
                // in case the client don't want a automatic restore document we
                // need to provide this in the close data.
                recentFileData = restoreDocOnClose ? recentFileData : _.extend({ no_restore: restoreDocOnClose }, recentFileData);
                // notify server to save/close the document file - after a certain time it returns a rejected promise to prevent a permanent pending promise (e.g. no server response)
                if (BROWSER_TAB_SUPPORT) {
                    def = closeDocument(reason, recentFileData).progress(progressHandler);
                } else {
                    def = closeDocument(reason, recentFileData).then(saveFileInStorage).progress(progressHandler);
                }
            } else {
                // error/offline/reset received: it's not safe to make remote calls therefore simply close rtConnection
                def = $.when();
            }

            // Disconnect real-time connection after document is closed (by closing
            // the document or "cancelling" the load process - this ensures that the
            // real-time instance is notified in every case).
            if (isRTReady) {
                def = def.then(function () {

                    // notify listeners (used in debug module)
                    self.trigger("docs:edit:quithandler:after");

                    // returns a rejected promise after a certain time to prevent a permanent pending promise (e.g. no server response)
                    return executeShutdownStep('.disconnect', null, MAX_DELAY_FOR_DISCONNECT);
                });
            }

            // bug 34464: do nothing else, if the new document has not been created yet (no file descriptor)
            if (file) {
                if (modified) {
                    // propagate changed document file to Files application (also in case of an error)
                    def = def.then(propagateChangedFile, propagateChangedFile);
                // bug 35797: do not delete an empty file, if user has requested to reload the document
                } else if ((reason !== 'reload') && ((launchOptions.action === 'new') || launchOptions.template) && (editClients.length <= 1) && !launchOptions.keepFile) {
                    // Delete new document in Files application if no changes have been made at all or
                    // the file has been created from a template and not via edit as new.
                    // Shared documents should never be deleted and also a quit due to reload
                    // should preserve a new file.
                    def = def.then(function () { return purgeFile(file); });
                }
            }
            return def;
        }

        /**
         * Close the document and handle the response.
         *
         * @param {QuitReason} reason
         *  The reason for quitting the application
         *
         * @param {Object} [recentFileData]
         *  The 'recent file data'.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the answer of the server
         *  request from 'rtConnection.closeDocument', or rejected on timeout.
         */
        function closeDocument(reason, recentFileData) {
            // the application window
            var win = self.getWindow();
            // set to null when no 'recentFileData' provided
            recentFileData = recentFileData ? recentFileData : null;

            // evaluates the result of the 'closedocument' call
            function checkCloseDocumentResult(data) {

                var // the resulting deferred object
                    def = self.createDeferred(),
                    // the error code in the response
                    errorCode = new ErrorCode(data),
                    // additional data for the error data
                    additionalData = {},
                    // restored file id, if provided
                    restoredFileId = null,
                    // restored file name if provided
                    restoredFileName = null,
                    // restored fole id, if provided
                    restoredFolderId = null,
                    // restored error code
                    restoredErrorCode = null;

                // no error/warning, forward response data to piped callbacks
                if (!errorCode.isError() && !errorCode.isWarning()) {
                    return data;
                }

                // retrieve the restored file name / folder id in case there was a restore operation
                // during the flushDocument
                restoredFileId = getStringOption(data, ErrorContextData.RESTORED_FILEID, null);
                restoredFileName = getStringOption(data, ErrorContextData.RESTORED_FILENAME, null);
                restoredFolderId = getStringOption(data, ErrorContextData.RESTORED_FOLDERID, null);
                restoredErrorCode = getStringOption(data, ErrorContextData.RESTORED_ERRORCODE, 'BACKUPDOCUMENT_RESTORE_DOCUMENT_WRITTEN');
                if (_.isString(restoredFileId) && _.isString(restoredFileName) && _.isString(restoredFolderId)) {
                    if (restoredErrorCode === ErrorCode.CONSTANT_NO_ERROR) {
                        // no error on restore means that we have to show the restore success error
                        restoredErrorCode = new ErrorCode({ errorClass: ErrorCode.ERRORCLASS_ERROR, error: 'BACKUPDOCUMENT_RESTORE_DOCUMENT_WRITTEN' });
                    } else {
                        // create error code object with error class and code
                        restoredErrorCode = new ErrorCode({ errorClass: ErrorCode.ERRORCLASS_ERROR, error: restoredErrorCode });
                    }

                    additionalData[ErrorContextData.RESTORED_FILENAME] = restoredFileName;
                    getPath(restoredFolderId).always(function (paths) {
                        additionalData[ErrorContextData.RESTORED_PATHNAME] = preparePath(paths);
                        additionalData[ErrorContextData.ORIGINAL_FILEID] = self.getFileDescriptor().id;

                        // update file descriptor to enable 'reload' to load the restored document file
                        self.updateFileDescriptor({ id: restoredFileId, folder_id: restoredFolderId });
                        self.setInternalError(restoredErrorCode, ErrorContext.CLOSE, errorCode, { additionalData });
                    });
                } else {
                    // all other cases show an error immediately
                    self.setInternalError(errorCode, ErrorContext.CLOSE, null, { additionalData });
                }

                if (docModel && docView) {
                    // Bug 32989: Register a new quit handler that will reject the pending
                    // deferred object (the base application has deleted all old quit handlers
                    // from its internal list, including this handler currently running). This
                    // covers execution of app.immediateQuit(), e.g. when logging out while
                    // this document is still waiting for the user to click the Quit button.
                    // Simply reject the deferred but do not return it, otherwise the logout
                    // process will be aborted.
                    self.registerQuitHandler(function () { def.reject(); });

                    // The original app.quit() method has been modified to ensure that the
                    // quit code does not run multiple times, e.g. by double-clicking the Quit
                    // button. Here, the application will return to idle state, and the user
                    // has to click the Quit button again to really close the application. Thus,
                    // calling app.quit() has to reject the deferred object in order to ensure
                    // that the application really shuts down.
                    self.quit = (function (parentQuit) {
                        return function (options) {
                            def.reject();
                            return parentQuit.call(self, options);
                        };
                    }(self.quit));

                    // back to idle to allow final copy&paste
                    win.idle();
                    def.always(function () { win.busy(); });

                    // avoiding pending promise, if file was deleted before logout (55273)
                    if (reason === QuitReason.LOGOUT) { def.reject(); }

                } else {
                    def.reject();
                }

                if (errorCode.getDescription()) {
                    globalLogger.log(errorCode.getDescription());
                }

                return def.promise();
            }

            // called in case the 'closedocument' call has been rejected
            function closeDocumentResultFailed(data) {
                // no response from closeDocument within the set timeout
                var noResponse = data === 'timeout';

                // timeout at closeDocument
                if (noResponse) {
                    // We only wait for MAX_SECONDS_FOR_CLOSE, because the user should not
                    // have to wait longer at closing the document. But, we don't know if it's
                    // an error or if the connection is just slow therefore don't set an error,
                    // just reject. The finalizeRTconnection handle this case later.
                    return self.createRejectedPromise(data);
                } else {
                    return checkCloseDocumentResult(data);
                }
            }

            return executeShutdownStep('.closeDocument', recentFileData, MAX_DELAY_FOR_CLOSE).then(checkCloseDocumentResult, closeDocumentResultFailed);
        }

        function getRegisteredAuthorItemById(authorId) {
            return documentAuthorIndex[authorId];
        }

        /**
         * Returns the index of an author from the document authors list. If
         * the author name is not known yet, it will be inserted into the list.
         *
         * @param {String} author
         *  The name of an author of this document.
         *
         * @returns {Number}
         *  The array index of the author in the document authors list.
         */
        function getAuthorIndex(author) {
            var index = _.indexOf(documentAuthorsList, author);
            if (index < 0) {
                index = documentAuthorsList.length;
                documentAuthorsList.push(author);
            }
            return index;
        }

        /**
         * OT: Helper function to clear the local operation cache.
         * This function is the listener for the event 'operations:after'. It is triggered, if the client gets
         * an Ack from the server for his own operations, but is currently applying a block of external operations.
         * In thise case the local operation cache must not be deleted immediately, but only, after applying all
         *  external operations.
         */
        function clearLocalActionsWithoutAckAfterEvent() {
            otLogger.info('$badge{EditApp} clearLocalActionsWithoutAckAfterEvent: Clearing the local actions!');
            clearLocalActionsWithoutAck();
        }

        /**
         * OT
         * Clearing all actions in the buffer for local operations that did not already receive an
         * acknowledge from the server after receiving the acknowledge. Only those operations can
         * be deleted, that have the marker 'sentToServer'. In the meantime there might be additional
         * local operations added to the container 'localActionsWithoutAck', that are not already
         * sent to the server. These new actions must not be removed from the container.
         */
        function clearLocalActionsWithoutAck() {

            if (docModel.isProcessingExternalOperations()) { // do not clear the cache while applying external operations
                // self.listenTo(docModel, 'operations:after', clearLocalActionsWithoutAckAfterEvent, { once: true });
                docModel.off('operations:after', clearLocalActionsWithoutAckAfterEvent); // registering for the next call of 'operations:after'
                otLogger.info('$badge{EditApp} clearLocalActionsWithoutAck: External operations are currently applied. Coming back later after event operations:after!');
                docModel.on('operations:after', clearLocalActionsWithoutAckAfterEvent); // registering for the next call of 'operations:after'
            } else if (ackOSN > 0) { // waiting for further still missing external operations
                self.off('docs:ackosn:updated', clearLocalActionsWithoutAckAfterEvent); // deregistering this handler again
                docModel.off('operations:after', clearLocalActionsWithoutAckAfterEvent); // registering for the next call of 'operations:after'
                otLogger.info('$badge{EditApp} clearLocalActionsWithoutAck: External operations are still missing. Coming back later after event docs:ackosn:updated or operations:after!');
                self.on('docs:ackosn:updated', clearLocalActionsWithoutAckAfterEvent); // registering for the next call of 'docs:ackosn:updated'
                docModel.on('operations:after', clearLocalActionsWithoutAckAfterEvent); // registering for the next call of 'operations:after'
            } else {
                otLogger.info('$badge{EditApp} clearLocalActionsWithoutAck: Clearing local cache. Before: ' + JSON.stringify(localActionsWithoutAck));
                docModel.off('operations:after', clearLocalActionsWithoutAckAfterEvent); // deregistering this handler again
                self.off('docs:ackosn:updated', clearLocalActionsWithoutAckAfterEvent); // deregistering this handler again
                deleteActionsWithoutAck('sentToServer');
                otLogger.info('$badge{EditApp} clearLocalActionsWithoutAck: Clearing local cache. After: ' + JSON.stringify(localActionsWithoutAck));
            }
        }

        /**
         * OT: Helper function to avoid sending internal actions to the server when currently external operations
         * are applied. Because external operations are applied asynchronously it might happen, that internal
         * operations shall be sent to the server in between the external operations. But this is not allowed,
         * because the external operations might modify the unsent local operations. The client has to sent the
         * fully transformed operations with the server OSN sent by the server with the external operations.
         * Therefore all external operations must be applied, before the client is allowed to sent its operations
         * to the server.
         * This function waits for the 'operations:after' event, that is sent after all external operations are
         * applied.
         */
        function sendActionsAfterApplyingExternalOperations() {
            otLogger.info('$badge{EditApp} sendActionsAfterApplyingExternalOperations: Sending actions after receiving operations:after event!');
            sendActions();
        }

        /**
         * OT: Helper function to avoid sending internal actions to the server when not all external operations
         * are received. This is possible, because the OSN sent with the Ack for the previous operations comes faster
         * than the OSN sent with the external operations.
         * -> this process becomes superfluous, if the Acks are sent on the same channel as the external operations
         */
        function sendActionsAfterReceivingRequiredExternalOperations() {
            otLogger.info('$badge{EditApp} sendActionsAfterReceivingRequiredExternalOperations: Sending actions after receiving docs:ackosn:updated event!');
            sendActions();
        }

        /**
         * Start sending the OsnBeacon.
         */
        function enableOsnBeacon() {
            if (self.isOnline() && isTabVisible()) {
                osnBeacon.start();
            }
        }

        // protected methods --------------------------------------------------

        /**
         * Leaves the busy mode while importing the document has not been
         * finished.
         *
         * _Attention:_ Must not be called after importing the document is finished.
         *
         * @param {Object} [options]
         *  Optional parameters supported by `BaseView#showAfterImport`.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the GUI initialization handler
         *  has finished.
         */
        this.leaveBusyDuringImport = _.once(function (options) {

            // remember start time of preview mode
            previewStartTime = self.launchTracker.step('previewStart', '$badge{EditApp} Busy state during import process left');
            updateState();

            // leave busy mode early (convert to a JPromise as expected by callers)
            return jpromise.from(docView.showAfterImport(options));
        });

        // public methods -----------------------------------------------------

        /**
         * Creates a method that debounces multiple invocations, and that waits
         * until the "action processing mode" of the document model is
         * currently active, i.e. the model currently applies operation actions
         * (needed e.g. in complex undo or redo operations). If the document
         * does not process any operations, the created method behaves like a
         * regular debounced method (see `EObject.debounce()`).
         *
         * Also available as class method decorator `@debounceAfterActionsMethod`,
         * see `./decorators.ts`.
         *
         * @param {EObject} targetObj
         *  The instance used as calling context for the passed callback
         *  function. Additionally, the lifetime of the target object will be
         *  taken into account. If the object will be destroyed before the
         *  operation actions have been applied, the deferred callback function
         *  will not be called anymore.
         *
         * @param {Function} directFn
         *  The callback function to be called directly each time the created
         *  debounced method will be invoked. Can be set to null, if nothing
         *  has to be done when calling the debounced method directly.
         *
         * @param {Function} deferFn
         *  The callback function to be called debounced, after the document
         *  model has applied the current operation actions.
         *
         * @param {DebounceOptions} [options]
         *  Optional parameters.
         *
         * @returns {Function}
         *  The debounced method, bound to the lifetime of the passed target
         *  object. Intended to be used as (private or public) instance method
         *  of the passed target object!
         */
        this.createDebouncedFor = function (targetObj, directFn, deferFn, options) {

            // whether the created method already waits for document operations
            var waiting = false;

            // defers the invocation of the passed deferred callback if actions are being applied
            function invokeDeferFn() {

                // nothing to do, if the method is already waiting for the operation promise
                if (waiting) { return; }

                // invoke deferred callback directly, if no operations are applied currently;
                // or if the application is still importing the document
                if (!self.isImportFinished() || !docModel.isProcessingActions()) {
                    return deferFn.call(targetObj);
                }

                // wait for the 'action processing mode' to finish
                var promise = docModel.waitForActionsProcessed();
                waiting = true;

                // invoke the deferred callback after operation processing is finished
                targetObj.onFulfilled(promise, () => {
                    waiting = false;
                    deferFn.call(targetObj);
                });
            }

            // create and return the special debounced method waiting for action processing mode
            return targetObj.debounce(directFn, invokeDeferFn, options);
        };

        /**
         * External cached operations are started with the event 'docs:operations:completed'. This is typically
         * triggered in the 'undogroup:close' handler. But under some circumstances it is necessary, that this
         * event is also triggered, when no undogroup is closed. This is typically the case, if a long running
         * block is removed. And this happens, if the blocker 'processingInternalOperations' is removed. This
         * is in most cases connected to the blocker 'blockKeyboardEvent' that can be set with the function
         * 'setBlockKeyboardEvent' of the model.
         *
         * This function allows an additional check (next to the undogroup:close handler) for external operations
         * after the internal block is removed. This can be used for dialogs or long running processes, in which
         * the internal keyboard is also typically blocked.
         *
         * It is important to check for pendingActions. If this is set, there is an open undogroup. In this case
         * the 'docs:operations:completed' will be triggered in the undogroup:close handler.
         */
        this.checkStartOfExternalOperations = function () {
            otLogger.info(`$badge{EditApp} checkStartOfExternalOperations: Check, if external operations can be applied: ${!!pendingAction} ${docModel.isProcessingInternalOperations()}`);
            if (isOTEnabled && !pendingAction && !docModel.isProcessingInternalOperations()) {
                // if there are still pending actions, the trigger for external operations will occur in 'undogroup:close' handler
                self.trigger('docs:operations:completed');
            }
        };

        /**
         * Whether it is possible to remove the internal blocker for the external operations. This is only
         * possible, after all internal operations are registered and the pendingAction is removed. This
         * happens typically in the 'undogroup:close' handler. Removing the blocker is also possible, if
         * the user cancels a long running process or closes a dialog without generating an operation.
         * A removal of the internal blocker is not possible, if a drawing is currently resized, moved, ...
         * and the finalization process for the drawing is not complete yet (OX Text and OX Presentation).
         */
        this.isRemovalOfInternalBlockerPossible = function () {
            return isOTEnabled && !pendingAction && !docModel.isDrawingChangeActive();
        };

        /**
         * Returns the file format of the edited document.
         */
        this.getFileFormat = function () {
            // TODO: remove the fallback to OOXML if the file format is known at construction time (no hasFileDescriptor() check anymore)
            var fileName;
            if (fileFormat === null) {
                fileName = this.hasFileDescriptor() ? this.getFullFileName() :
                    (launchOptions && launchOptions.templateFile) ? launchOptions.templateFile.filename : '';
                fileFormat = fileName ? getFileFormat(fileName) : null;
            }
            return fileFormat || 'ooxml';
        };

        /**
         * Returns whether the edited document is an Office-Open-XML file.
         */
        this.isOOXML = function () {
            return this.getFileFormat() === 'ooxml';
        };

        /**
         * Returns whether the edited document is an OpenDocument file.
         */
        this.isODF = function () {
            return this.getFileFormat() === 'odf';
        };

        /**
         * Returns whether the edited document is a template document file.
         */
        this.isTemplateFile = function () {
            return this.hasFileDescriptor() && isTemplate(this.getFullFileName());
        };

        /**
         * Returns whether the edited document may contain macro scripts.
         */
        this.isScriptable = function () {
            return this.hasFileDescriptor() && isScriptable(this.getFullFileName());
        };

        /**
         * Returns whether we received a shutdown broadcast from the server or
         * not.
         *
         * @returns {Boolean}
         *  TRUE if we have received the shutdown broadcast, otherwise FALSE.
         */
        this.isShutdownReceived = function () {
            return shutdownReceived;
        };

        /**
         * Returns the own unique client identifier used in real-time
         * communication.
         *
         * @returns {string}
         *  The own unique real-time client identifier.
         */
        this.getClientId = function () {
            return clientId;
        };

        /**
         * Returns a shortened version of the own unique client identifier.
         *
         * @returns {string}
         *  A shortened version of the own unique real-time client identifier.
         */
        this.getShortClientId = function () {
            return clientId.slice(0, 4);
        };

        /**
         * Returns the unique identifier for this document used in real-time
         * communication.
         *
         * @returns {string|null}
         *  The unique document identifier, or `null` if it is unknown.
         */
        this.getDocUID = function () {
            return rtConnectionExist() ? rtConnection.getDocUID() : null;
        };

        /**
         * Returns the unique document identifier or null if the uid is unknown.
         */
        this.getLoadMode = function () {
            return loadMode;
        };

        /**
         * Provides am object containing data that is set at after the load process.
         *
         * @returns {Object}
         *  An object filled with loading information.
         */
        this.getLoadDocInfo = function () {
            return loadDocInfo;
        };

        /**
         * Returns the unique real-time identifier of the client with edit
         * rights.
         *
         * @returns {String}
         *  The unique real-time identifier of the client with edit rights.
         */
        this.getEditClientId = function () {
            return oldEditClientId;
        };

        /**
         * Returns the user name of the client with edit rights.
         *
         * @returns {string}
         *  The user name of the client with edit rights.
         */
        this.getEditClientUserName = function () {
            return getUserNameByClientUID(oldEditClientId);
        };

        /**
         * Returns the own client name, can be used by operations
         *
         * @returns {String}
         *  The own operation name.
         */
        this.getClientOperationName = function () {
            return clientOperationName;
        };

        /**
         * Returns the array of all active clients currently editing this
         * document. Inactive clients (clients without activity for more than
         * 60 seconds) will not be included in this list.
         *
         * @returns {RemoteClientData[]}
         *  The active clients viewing or editing this document.
         */
        this.getActiveClients = function () {
            return activeClients;
        };

        /**
         * Returns whether this document has been modified locally (true after
         * any operations where generated and applied by the own document model
         * and sent to the server).
         *
         * @returns {Boolean}
         *  Whether this document has been modified locally.
         */
        this.isLocallyModified = function () {
            return locallyModified;
        };

        /**
         * Returns whether we are in the process of switching edit rights.
         */
        this.isPrepareLosingEditRights = function () {
            return inPrepareLosingEditRights;
        };

        /**
         * Sends an action POST request in a specific format to the server.
         * ATTENTION:
         * This method is bound to a working RT connection as the
         * processing on the server-side must be done in a RT environment.
         * Therefore this method uses the client-side RT framework to
         * provide necessary data via the used http-request.
         *
         * @param {String} action
         *  The action identifier, inserted as 'action' property into the
         *  request POST data.
         *
         * @param {Object} data
         *  The data object, inserted as 'requestdata' property into the
         *  request POST data.
         *
         * @param {Object} [options]
         *  Optional parameters. See method IO.sendRequest() for details.
         *
         * @returns {JPromise<object>}
         *  The abortable promise representing the action request.
         */
        this.sendActionRequest = function (action, data, options) {

            if (!rtConnectionExist()) { return self.createRejectedPromise(); }

            var params = { action, docuid: rtConnection.getDocUID(), requestdata: JSON.stringify(data) };
            options = _.extend({ method: 'POST' }, options); // default to POST

            var promise = this.sendFileRequest(FILTER_MODULE_NAME, params, options);
            promise.fail(function (err) {
                globalLogger.warn('$badge{EditApp} sendActionRequest: failed: ', action, data, options, err);
            });

            return promise;
        };

        /**
         * Sends an "addImageId" request which makes an uploaded image known in
         * the backend.
         *
         * @param {object} data
         *  The data containing the image identifier and file name, as returned
         *  from the "addimage" server request.
         *
         * @returns {JPromise<object>}
         *  A promise that will fulfil with the passed data after sending the
         *  realtime server request.
         */
        this.sendAddImageIdRequest = function (data) {
            return rtConnectionExist() ? rtConnection.addImageId(data).then(_.constant(data)) : jpromise.resolve(data);
        };

        /**
         * Sends a log message if logging is enabled and additionally allowed.
         *
         * IMPORTANT: Now what you're doing as it can expose user data to the server log!
         *
         * @param {String} message
         *  The message to be logged on the server-side.
         *
         * @returns {EditApplication}
         *  A reference to this instance.
         */
        this.sendLogMessage = function (message) {
            if (Config.LOG_ERROR_DATA && self && self.allowStoreLogFile()) {
                self.sendLogErrorData({ errorCode: message });
            }
            return this;
        };

        /**
         * Sending a comment notification to all users that are mentioned in the mentions.
         * Guest users do not send this notification mails.
         *
         * @param {Object} mentions
         *  The object describing the mentions in the comment.
         *
         * @param {String} text
         *  The text in the comment.
         *
         * @param {String} commentId
         *  The ID of the comment.
         *
         * @returns {jQuery.Promise}
         *  The abortable promise representing the server request. See method
         *  IO.sendRequest() for details. Or an already resolved promise, if the
         *  current user is a guest user.
         */
        this.sendCommentNotification = function (mentions, text, commentId) {

            if (isGuest()) { return $.when(); } // Guest users cannot send notification mails (DOCS-3843)

            // the file descriptor
            var fileInfo = self.getFileDescriptor();
            // the list of recipients that shall get an email
            var recipients = mentions && mentions.map(function (value) {
                return {
                    userId: resolveUserId(Number(value.userId.split('::')[1])),
                    emailAddress: value.email,
                    posInComment: value.pos,
                    lengthInComment: value.length
                };
            });
            // the data that will be posted
            var data = {
                recipients,
                comment: text,
                commentId,
                filename: fileInfo.filename,
                fileId: fileInfo.id,
                folderId: fileInfo.folder_id
            };
            // the request data object
            var requestData = {
                action: 'sendcommentnotification',
                requestdata: JSON.stringify(data)
            };

            return self.sendRequest(FILTER_MODULE_NAME, requestData, { method: 'POST' });
        };

        /**
         * Returns whether renaming the document is supported.
         *
         * @returns {boolean}
         *  Whether renaming the document is supported.
         */
        this.isRenameEnabled = function () {

            // renaming may be disabled by server-side configuration
            if (RENAME_DISABLED) { return false; }

            // file stores that use a fixed unique file ID can always rename the file
            // because it will be identified by its constant unique file identifier
            if (!renameNeedsReload && !Config.getDebugFlag('office:simple-filestore')) { return true; }

            // the file name must be used to identify the file (there is no unique ID):
            // - encrypted documents cannot be renamed then
            // - concurrently edited documents cannot be renamed then
            return !this.isDocumentEncrypted() && !isOTEnabled;
        };

        /**
         * Renames the file currently edited by this application.
         *
         * @param {String} shortName
         *  The new short file name (without extension).
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the file has been renamed
         *  successfully; or that will be rejected, if renaming the file has
         *  failed.
         */
        this.rename = function (shortName) {

            var
                file = this.getFileDescriptor(),
                // the original file name
                oldFileName = getStringOption(file, 'filename', this.getFullFileName()),
                oldSanitizedFilename = getStringOption(file, 'com.openexchange.file.sanitizedFilename', oldFileName),
                // the new file name (trim NPCs and spaces at beginning and end, replace embedded NPCs)
                newFileName = str.trimAndCleanNPC(shortName);

            // return running request (should not happen anyway!)
            if (renamePromise) {
                globalLogger.warn('$badge{EditApp} rename: multiple calls, ignoring subsequent call');
                return renamePromise;
            }

            // empty name (after trimming)
            if (newFileName.length === 0) {
                return $.Deferred().reject();
            }

            // name does not change
            if (newFileName === this.getShortFileName()) {
                return $.when();
            }

            newFileName += '.' + this.getFileExtension();
            if (this.isDocumentEncrypted()) { newFileName += GUARD_EXT; }

            // Bug 27724: immediately update file descriptor
            this.updateFileDescriptor({ filename: newFileName });

            // set needed promise in case of asynchronous rename necessary
            // TODO: priority is background-proc, because this deferred cant be handled by the frontend
            renameAndReloadDeferred = this.createDeferred();

            // prepare the rename (makes sure that view settings are sent before we start to rename the document)
            renamePromise = jpromise.invoke(() => this.implPrepareRenameDocument());

            // send pending operations
            renamePromise = renamePromise.then(sendActions);

            // send the server request via RT connection
            renamePromise = renamePromise.then(function () {
                return rtConnectionExist() ? rtConnection.renameDocument({ target_filename: newFileName }) : jpromise.reject();
            });

            // filter response by the 'error' property containing an error code
            renamePromise = renamePromise.then(function (data) {

                // the result of the rename can be extract from the error propety
                var error = new ErrorCode(data);

                // check result as it can be possible that the final result is sent via a RT notification
                if (error.isWarning() && error.getCodeAsConstant() === 'RENAMEDOCUMENT_SAVE_IN_PROGRESS_WARNING') {
                    // we have an asynchronous rename due to storage limitations, where we
                    // also need to reload the renamed document
                    return renameAndReloadDeferred.promise();
                }

                // forward result data on success, otherwise reject with error code
                return error.isError() ? jpromise.reject(error) : data;
            });

            // renaming succeeded: prevent deletion of new empty file, update file descriptor
            renamePromise.done(function (data) {
                locallyRenamed = true;
                self.updateFileDescriptor({
                    filename: getStringOption(data, 'fileName', oldFileName),
                    'com.openexchange.file.sanitizedFilename': getStringOption(data, 'com.openexchange.file.sanitizedFilename', oldSanitizedFilename)
                });
            });

            // renaming failed: show an appropriate warning message
            renamePromise.fail(function (response) {

                if (response !== 'abort') {
                    var errorCode = ErrorCode.isErrorCode(response) ? response : new ErrorCode(response);
                    errorCode.setErrorContext(ErrorContext.RENAME);
                    var messageData = getErrorMessageData(errorCode);
                    // do not restore the error alert banner after switching applications
                    messageData.restoreHidden = false;
                    globalLogger.warn('$badge{EditApp} rename: error: ' + errorCode.getErrorText());
                    docView.yell(messageData);
                }

                // back to old file name
                self.updateFileDescriptor({ filename: oldFileName });
            });

            // clean up after renaming has finished
            return renamePromise.always(function () {
                renamePromise = null;
            });
        };

        /**
         * Returns whether this application caches any contents that have not
         * been sent successfully to the server.
         *
         * @returns {boolean}
         *  Whether the application currently caches unsaved contents.
         */
        this.hasUnsavedChangesOffice = function () {
            return hasUnsavedOperations() ||
                !!renamePromise ||
                !!self.docController.getUnsavedFileName() ||
                // `docModel` may already be destroyed in browser unload situations
                (!!docModel && docModel.hasUnsavedComments()) ||
                sendActionsFailedError;
        };

        /**
         * Returns whether the page unload process should be stopped. Stopping
         * it (by returning `true`) will open a native browser dialog to let
         * the user decide about leaving the page.
         *
         * _Attention:_ DO NOT RENAME THIS METHOD! The OX core uses it to decide
         * whether to show a warning before the browser refreshes or closes the
         * page.
         *
         * @returns {boolean}
         *  Whether the native browser dialog to prevent leaving the page shall
         *  be shown.
         */
        this.hasUnsavedChanges = function () {

            // the inactive browser tab must suppress the browser query dialog before redirect at sign out
            if (ox.tabHandlingEnabled && (TabApi.getLoggingOutState() === TabApi.LOGGING_OUT_STATE.FOLLOWER)) {
                return false;
            }

            // when there is no RT connection something is really wrong (the document cannoot
            // be saved anymore, so it does not make sense to show the browser query dialog)
            if (!rtConnectionExist() || !rtConnection.isConnected()) {
                return false;
            }

            // return whether there are real unsaved changes in the application,
            // or cached in the RT connection instance
            return this.hasUnsavedChangesOffice() || rtConnection.hasPendingActions();
        };

        /**
         * Sets a temporary user-defined application state.
         *
         * @param {String|Null} newState
         *  The new user-defined application state; or null to return to the
         *  regular internal application state.
         *
         * @returns {EditApplication}
         *  A reference to this instance.
         */
        this.setUserState = function (newState) {
            if (!this.isInternalError() && (userState !== newState)) {
                userState = newState;
                triggerChangeState();
            }
            return this;
        };

        /**
         * Returns the current application state.
         *
         * @returns {String}
         *  A string representing the application state. Will be one of the
         *  following values:
         *  - 'error': internal error, application locked permanently.
         *  - 'offline': network connection missing.
         *  - 'readonly': the document cannot be changed.
         *  - 'initial': the document has not been changed yet.
         *  - 'ready': all local changes have been sent to the server.
         *  - 'sending': local changes are currently being sent to the server.
         *  If a specific user defined state has been set using the method
         *  EditApplication.setUserState(), it will be returned instead, unless
         *  the application is in internal error state.
         */
        this.getState = function () {
            return (userState && !this.isInternalError()) ? userState : appState;
        };

        /**
         * Returns whether the document can be modified currently.
         *
         * @returns {Boolean}
         *  Whether the document can be modified.
         */
        this.isEditable = function () {
            return editMode;
        };

        /**
         * Returns whether the application is currently in online state, i.e.
         * whether the method `getState()` does not return the state "offline".
         *
         * @returns {boolean}
         *  Whether the application is currently in online state.
         */
        this.isOnline = function () {
            return appState !== 'offline';
        };

        /**
         * Returns whether the application is in the internal error state, i.e.
         * whether the method `getState()` returns the state "error".
         *
         * @returns {boolean}
         *  Whether the application is in internal error state.
         */
        this.isInternalError = function () {
            return appState === 'error';
        };

        /**
         * Sets this application into the internal error/warning mode,
         * dependent on the error/warning code and shows the error message to
         * the user.
         *
         * @param {Object} errorCode
         *  An error code object, either from the client-side (see
         *  ClientError) or from the server-side.
         *
         * @param {String} context
         *  An error context string constant defined by ErrorContext.
         *
         * @param {Object} [cause]
         *  An error code object, either from the client-side (see
         *  ClientError) or from the server-side, which describe the root
         *  cause of an error. While errorCode describes the error to be
         *  shown, cause is the root cause of the error state. If this is null
         *  errorCode is used as root cause.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  @param {Boolean} [options.showMessage=true]
         *      If set to false, the application won't show any message to the
         *      user. This is applicable, if a handler takes over the
         *      responsibility to show the error/warning message.
         *  @param {Object} [options.additionalData=null]
         *      If set, this object can be used to provide context dependent
         *      data to the error message (e.g. file name etc.).
         *  @param {Object} [options.silentShutdownConfig=null]
         *      If set, this object can be used to setup silentShutdownConnection.
         *  @param {Boolean} [options.hideError=false]
         *      If set to true, the behavior is not an error and should therefore
         *      not be logged as an error.
         */
        this.setInternalError = function (errorCode, context, cause, options) {
            var // the error code object
                newError = ErrorCode.isErrorCode(errorCode) ? errorCode : new ErrorCode(errorCode),
                // the old error code object
                oldError = null,
                // message data
                messageData = null,
                // text for logging
                logError = null,
                // show warning
                showWarning = true,
                // additional data
                additionalData = getObjectOption(options, 'additionalData', {}),
                // the cause of the error, use errorCode as cause if not provided
                causeError = ErrorCode.isErrorCode(cause) ? cause : newError,
                // a new warning/error has been set
                newErrorCodeSet = false,
                // config object to setup silentShutdownConnection
                silentShutdownConfig = getObjectOption(options, 'silentShutdownConfig', {}),
                // not logging an error in the console
                hideError = getBooleanOption(options, 'hideError', false),
                // whether it is an error
                isError = newError.isError(),
                // whether it's the first error that happened in the app
                isFirstErrorInApp;

            // switch to the reloading app when an error happens, errors while reloading before
            // the 'importFinishPromise' is resolved are handled by 'failureBeforeImportFinishedHandler'
            this.setAppReloadDone();

            if (_.isString(context)) {
                newError.setErrorContext(context);
            }

            // check, if we have already a set error
            oldError = self.getErrorState();
            isFirstErrorInApp = !_.isObject(oldError) || !oldError.isError();

            if (isFirstErrorInApp) {
                // old errors should not be overwritten
                self.setErrorState(newError);
                newErrorCodeSet = true;
            } else if (_.isObject(oldError)) {
                // repeat to show the previously set error/warning
                newError = oldError;
                messageData = currentMessageData;
            }

            logError = self.getErrorState();
            if (ErrorCode.isErrorCode(logError) && !hideError) {
                globalLogger.error('$badge{EditApp} setInternalError: ' + logError.getErrorText());
            }

            // Set states only for true errors
            if (isError) {
                internalError = true;
                connected = false;
                locked = true;
                changeEditMode(false);
                silentShutdownConnection(silentShutdownConfig);

            // Set states for warnings
            } else {
                var onceWarningAlreadyShown = _.has(alreadyShownOnceWarnings, newError.getCodeAsConstant());

                var isLastShownWarning = lastShownWarning === newError.getCodeAsConstant();
                var suppressDirectRepeated = isLastShownWarning && suppressDirectlyRepeated(newError.getCodeAsConstant());

                // don't show warnings that should only be seen one time
                // don't show the last warning multiple times, when not explicitly allowed
                showWarning = !onceWarningAlreadyShown && !suppressDirectRepeated;

                if (showOnceInDocLifetime(newError.getCodeAsConstant())) {
                    alreadyShownOnceWarnings[newError.getCodeAsConstant()] = true;
                }

                lastShownWarning = newError.getCodeAsConstant();
            }

            updateState(newError);

            // update message data only if a new error/warning has been set
            if (!_.isObject(messageData) || newErrorCodeSet) {

                // note: there are errors that are displayed as warning, see 'getMessageData' for details
                messageData = getErrorMessageData(newError, _.extend({ causeError, viewerMode: self.isViewerMode() }, getInfoStateOptions(), additionalData));
                self.implExtendMessageData(messageData);
                reloadEnabled = getStringOption(messageData.action, 'itemKey') === 'document/reload';

                // store latest message data to be reusable
                currentMessageData = messageData;
            }

            if (getBooleanOption(options, 'showMessage', true) && showWarning) {
                self.waitForImport(function () { if (docView && docView.yell) { docView.yell(messageData); } }); // using waitForImport to avoid race conditions (DOCS-2885)
            }

            // log error at the end when the important parts are done
            // do it only for the first Error to prevent multiple invokations
            // and don't send errors to the backend when the browser is closing, because
            // the app can be in a strage state, creating invalid erors
            if (isError && isFirstErrorInApp && !browserClosing) {
                // 1. create server log entry, make sure it's sent too when quitting the app fast after the error
                try {
                    internalErrorSendLogPromise = self.sendLogErrorData({ errorCode: newError.getCodeAsConstant() });
                } catch (error) {
                    globalLogger.error('$badge{EditApp} Error when sendLogErrorData is callend in setInternalError error:', error, ' fileDec:', self && _.isFunction(self.getFileDescriptor) && self.getFileDescriptor());
                }

                if (self.allowStoreLogFile() && createCrashDataForError(newError.getCodeAsConstant())) {
                    if (isGuest()) {
                        // 2a. offer crashData as download for guests that can't create drive files
                        self.createCrashDataAsDownload(newError.getCodeAsConstant());
                    } else {
                        // 2b. create crashData in Drive and provide user feedback, this should not block a reload or quitting the app
                        try {
                            self.createCrashDataInDrive(newError.getCodeAsConstant());
                        } catch (error) {
                            globalLogger.error('$badge{EditApp} Error when createCrashDataInDrive is callend in setInternalError error:', error, ' fileDec:', self && _.isFunction(self.getFileDescriptor) && self.getFileDescriptor());
                        }
                    }
                }
            }
        };

        /**
         * Creates a feedback dialog and offers a download for the
         * current frontend log file.
         *
         * @param {String} [errorCode]
         * An optional parameter to forward the cause why crashData is
         * created to the dialog.
         */
        this.createCrashDataAsDownload = function (errorCode) {

            // don't send the log when no data exists
            const logData = this.generateLogData();
            if (!logData) { return; }

            const dialog = new MessageDialog({
                title: 'Generating debug report',
                message: 'A debug report can be downloaded. The report may contain content of previously opened documents.',
                classes: 'generate-log-dialog',
                okLabel: CLOSE_LABEL,
                showReadOnly: true
            });

            dialog.$bodyNode.append(
                // button node
                $(createDiv("result")).append(createButton({
                    icon: "bi:download",
                    label: "Download",
                    click: () => downloadFrontendLog(logData)
                })),
                // error details
                $(createDiv("details")).append(
                    createDiv({ label: `Error code: ${errorCode || "<none>"}` }),
                    createDiv({ label: `Timestamp (UTC): ${new Date().toISOString()}` })
                )
            );

            dialog.show();
        };

        /**
         * Uploads log data to the server to create a log file in Drive and gives
         * feedback to the user about the upload state.
         *
         * @param {String} [errorCode]
         * An optional parameter to forward the cause why crashData is
         * created to the dialog.
         */
        this.createCrashDataInDrive = function (errorCode) {

            // don't send the log when no data exists
            const logData = this.generateLogData();
            if (!logData) { return; }

            const dialog = new MessageDialog({
                title: 'Generating debug report',
                message: 'A debug report is uploaded to Drive/myFiles/crashdata. This can take a while. Closing the browser tab cancels the upload. The report may contain content of previously opened documents.',
                classes: 'generate-log-dialog',
                okLabel: CLOSE_LABEL,
                showReadOnly: true
            });

            const $container = $(createDiv("result")).append($('<div>').busy({ immediate: true }));
            const $details = $(createDiv("details")).append(
                createDiv({ label: `Error code: ${errorCode || "<none>"}` }),
                createDiv({ label: `Timestamp (UTC): ${new Date().toISOString()}` })
            );

            dialog.$bodyNode.append($container, $details);
            dialog.show();

            const showResult = (icon, label) => {
                $container.empty().append(createIcon(icon), createSpan({ label, style: { padding: "0 5px" } }));
            };

            const request = this.sendLogErrorData({ logData });
            dialog.onFulfilled(request, () => showResult("bi:check", "Upload finished."));
            dialog.onRejected(request, () => {
                showResult("bi:exclamation-circle", "Could not upload file.");
                // offer download as fallback
                const $fallbackMsg = $(createDiv({ label: "You may download the debug report manually.", style: { paddingTop: "5px", textAlign: "center" } }));
                dialog.$bodyNode.append($fallbackMsg.append($(createDiv({ style: { paddingTop: "10px" } })).append(createButton({
                    icon: "bi:download",
                    label: "Download",
                    click: () => downloadFrontendLog(logData)
                }))));
            });
        };

        /**
         * Check whether it's allowed to store a log file in Drive for the current user.
         *
         * IMPORTANT: Please be careful with changing conditions, as it can affect
         * privacy related matters.
         *
         * @returns {Boolean}
         *  Whether storing a log file is allowed.
         */
        this.allowStoreLogFile = function () {
            // only send logData when enabled via Config and when the file is not encrypted
            // in doubt do not allow (e.g. self === undefined)
            return Config.LOG_ERROR_DATA && !!self && !self.isDocumentEncrypted();
        };

        /**
         * Sends logging data to the server and returns a promise waiting for the
         * response. Depending on the parameters, it can be just a log entry in
         * the server log and/or a log file created in Drive.
         * The unique identifier of the application will be added to
         * the request parameters automatically. See method IO.sendRequest()
         * for further details.
         *
         * @param {object} errorData
         *  Optional parameters:
         *  - {string} [errorData.errorCode]
         *    The error code that will be written on the server log.
         *  - {string[]} [errorData.logData]
         *    The data that is used as content for the log file created in Drive.
         *
         * @returns {jQuery.Promise}
         *  The abortable promise representing the server request. See method
         *  sendRequest for details.
         */
        this.sendLogErrorData = function (errorData) {

            // what task should be done...
            var createLogEntryOnServer = _.has(errorData, 'errorCode');
            var logData = errorData.logData || '';

            // early out, no task to do
            if (!createLogEntryOnServer && !logData) {
                return self.createRejectedPromise();
            }

            // controls whether a given string will be anonymized
            function anonymizeDataSanitizer(stringData) {
                return anonymize ? 'hidden' : stringData;
            }

            function getErrorCodeString() {
                var errorCode = errorData && errorData.errorCode;
                // map an empty string, null and undefined etc. to unknownError
                var sanitizedErrorCode = errorCode ? errorCode : 'unknownError';
                return String(sanitizedErrorCode);
            }

            // whether privacy related data should be logged at the backend
            var anonymize = !Config.LOG_ERROR_DATA;
            var fileDec = self.getFileDescriptor();

            // build request - the backend expects to get type strings for every parameter
            var requestData = {
                action: 'logclientdata',

                // controls which data set is logged at the backend
                // for privacy reasons to not expose private data to the admin
                logAnonymous: String(anonymize),

                // to be fail safe, hide fileId/folderId/fileName too when in
                // 'anonymize' mode as these as they are the critical information
                fileId:     String(anonymizeDataSanitizer(fileDec && fileDec.id)),
                folderId:   String(anonymizeDataSanitizer(fileDec && fileDec.folder_id)),
                fileName:   String(anonymizeDataSanitizer(self.getFullFileName())),

                userId:     String(ox && ox.user_id),
                docUid:     String(currentDocUid),

                // To create log entry on server:
                // - provide 'errorCode' as parameter to create a log entry
                // - an empty string is the same an not providing the parameter
                // - must be string, we don't get errors from the server for non string type
                errorCode:  createLogEntryOnServer ? getErrorCodeString() : '',

                // To create log file in Drive:
                // - provide 'logData' as parameter to create a log entry
                // - an empty string is the same an not providing the parameter
                // - must be string, we don't get errors from the server for non string type
                // - check 'allowStoreLogFile' again, because it must be ensured that no log file is send to the server when not allowed
                logData:    (logData && self.allowStoreLogFile()) ? str.joinTail(logData, '\n') : ''
            };
            // don't use sendRequest app method, the request should not be aborted on quit/reload
            return sendRequest(FILTER_MODULE_NAME, requestData, { method: 'POST', dataType: 'xml' });
        };

        /**
         * Sets the current application information state, which is used to
         * provide information to the user.
         *
         * @param {String} infoState
         *  a info state defined by the InfoState class.
         *
         * @param {Object} [options]
         *  A optional object, which contains additional properties for the
         *  information message. If null or undefined, the implementation
         *  provides a default options object, which will be used by the
         *  internal info state => information message mapping function.
         *  Optional parameters:
         *  @param {Boolean} [options.showMessage=true]
         *      If set to false, the application won't show any
         *      message to the user. This is applicable, if a
         *      handler takes over the responsibility to show
         *      the error/warning message.
         *  @param {Boolean} [options.resetWarning=false]
         *      If set to true, the application would reset a previously
         *      set warning state. This is applicable if we switched
         *      from off- to online successfully.
         *
         * @returns {boolean}
         *  Whether an alert banner has been shown. The return value `false`
         *  means there is already a high-priority alert banner visible, and
         *  the new alert banner does not contain the `priority` flag.
         */
        this.setCurrentInfoState = function (infoState, options) {

            var // error message
                messageData = null,
                // show message option
                showMessage = getBooleanOption(options, 'showMessage', true),
                // reset a previous warning
                resetWarning = getBooleanOption(options, 'resetWarning', false),
                // old error state
                oldErrorState = null;

            if (resetWarning) {
                // Check, if we have already a set error as old errors should
                // never be overwritten
                oldErrorState = self.getErrorState();
                if (!_.isObject(oldErrorState) || !oldErrorState.isError()) {
                    self.setErrorState(new ErrorCode());
                }
            }

            self.setInfoState(infoState);

            if (showMessage) {
                messageData = getMessageData(infoState, _.extend({}, getInfoStateOptions(), options));
                self.implExtendMessageData(messageData);
                if (_.isObject(messageData)) {
                    return docView.yell(messageData);
                }
            }

            // no alert banner shown
            return false;
        };

        /**
         * Returns whether the application is locked for editing. Reasons for
         * the lock may be missing edit permissions for the document file, or
         * an unrecoverable internal application error.
         *
         * @returns {Boolean}
         *  Whether editing the document is locked.
         */
        this.isLocked = function () {
            return internalError || locked;
        };

        /**
         * Returns whether acquiring edit rights is currently possible (the
         * document must be in read-only mode, and no internal application
         * error has occurred).
         * Acquiring edit rights is not possible, if OT is enabled.
         *
         * @returns {Boolean}
         *  Whether acquiring edit rights is currently possible.
         */
        this.isAcquireEditRightsEnabled = function () {
            return !isOTEnabled && rtConnectionExist() && !this.isLocked() && this.isOnline() && !editMode;
        };

        /**
         * Returns whether acquiring edit rights is currently possible (the
         * document must be in read-only mode, no internal application
         * error has occurred, and no pending updates are available).
         *
         * @returns {Boolean}
         *  Whether acquiring edit rights is currently possible.
         */
        this.isAcquireEditRightsEnabledPendingUpdates  = function () {
            return this.isAcquireEditRightsEnabled() && (pendingRemoteUpdates.length <= 1);
        };

        /**
         * Tries to acquire edit rights for the document. Edit rights will be
         * updated automatically while synchronizing operations with the
         * server. Multiple calls of this function will be throttled with a
         * delay of 3 seconds (first call will be executed immediately).
         */
        this.acquireEditRights = function () {
            acquireEditLogger.log('$back{Orchid|acquireEditRights} - User wants to acquireEditRights -> Button pressed  ');

            // check that acquiring edit rights is still enabled
            if (!this.isAcquireEditRightsEnabled()) {
                abortPendingEditRightsRequest();
                return;
            }

            // start the worker (will continue and resend server request, if already running)
            return acquireEditRightsWorker.start();
        };

        /**
         * Saves the current document to a specified folder using the passed
         * file name (without extension). If saving the current document is
         * successful, the copied document, which restarts the application.
         *
         * @param {number} folderId
         *  The identifier of the folder the document will be saved to.
         *
         * @param {string} shortName
         *  The short file name (without extension) to be used.
         *
         * @param {SaveAsType} fileType
         *  The target file type to save the document to.
         *
         * @param {Object} [options]
         *  Optional parameters:
         *  - {boolean} [options.encrypted=false]
         *    If set to `true`, the document will be encrypted (secondary file
         *    extension "pgp" will be added). Currently only supported for file
         *    type `NATIVE`.
         *
         * @returns {JPromise}
         *  A promise that will fulfil when the document has been saved
         *  successfully.
         */
        this.saveDocumentAs = function (folderId, shortName, fileType, options) {

            // the new full file name (to be extended with the appropriate extension)
            var fullFileName = shortName;
            // folder path for yell
            var fullPathName = null;
            // the file descriptor of the edited document
            var fileDesc = self.getFileDescriptor();
            // the resulting promise
            var promise = null;
            // indicate that a guard error occurred
            var guardError = false;

            switch (fileType) {

                // encrypt the file (not supported for template files)
                case SaveAsType.NATIVE:
                    if (options && options.encrypted) {

                        // add file extension AND encryption extension to target file name
                        fullFileName += '.' + self.getFileExtension() + GUARD_EXT;

                        // set authorization data to the model
                        promise = authorizeGuard().then(function (auth_code) {
                            launchOptions.auth_code = auth_code;
                        }, function () {
                            guardError = true;
                            return jpromise.reject();
                        });

                        // DOCS-3237: flush document before copying the file
                        promise = promise.then(function () {
                            return self.flushDocument(FlushReason.DOWNLOAD);
                        });

                        promise = promise.then(function () {
                            var fileOptions = fileDesc.file_options || {};
                            var fileOptionsParams = fileOptions.params || {};
                            _.extend(fileOptionsParams, {
                                cryptoAction: 'Encrypt',
                                filedisplay: fullFileName,
                                ignoreWarnings: false
                            });
                            fileDesc.file_options = _.extend(fileOptions, { params: fileOptionsParams });

                            return copyFile(fileDesc, folderId);
                        });

                        break;
                    }

                // fallthrough (also for file type `NATIVE` without encryption)
                case SaveAsType.TEMPLATE:

                    // add file extension to target file name
                    fullFileName += '.' + self.getFileExtension();

                    // DOCS-3237: flush document before copying the file
                    promise = self.flushDocument(FlushReason.DOWNLOAD);

                    // use RT connection to create a copy of the file
                    promise = promise.then(function () {
                        return rtConnectionExist() ? rtConnection.copyDocument({
                            file_id: self.getFileParameters().id,
                            target_filename: shortName, // expects short file name
                            asTemplate: fileType === SaveAsType.TEMPLATE,
                            target_folder_id: folderId
                        }) : jpromise.reject();
                    });
                    break;

                // convert to PDF
                case SaveAsType.PDF:

                    // add PDF extension to target file name
                    fullFileName += '.pdf';

                    // flush the document contents
                    promise = self.flushDocument(FlushReason.DOWNLOAD);

                    // propagate changed file to the files API
                    promise = promise.then(function () {
                        return propagateChangeFile(fileDesc);
                    });

                    // send server request
                    promise = promise.then(function () {
                        return getFileModelFromDescriptor(fileDesc).then(function (fileModel) {
                            return ConverterUtils.sendConverterRequest(fileModel, {
                                documentformat: 'pdf',
                                saveas_filename: fullFileName,
                                saveas_folder_id: folderId
                            });
                        });
                    });

                    break;

                default:
                    globalLogger.error('$badge{EditApp} saveDocumentAs: unknown file type "' + fileType + '"');
                    return jpromise.reject();

            }

            // filter response by the 'error' property containing an error code
            promise = promise.then(function (data) {
                var error = new ErrorCode(data);
                return error.isError() ? jpromise.reject(error) : data;
            });

            // resolve effective file path and file name for yelling
            promise = promise.then(function (data) {

                // effective file name may have been changed to prevent duplicate file names
                fullFileName = getStringOption(data, 'filename') || fullFileName;

                // force Drive to reload data of target folder, resolve folder data
                deleteFromCache(folderId);
                return getPath(folderId).then(function (paths) {
                    fullPathName = preparePath(paths);
                    return data;
                });
            });

            promise = promise.then(function (data) {

                if (fileType === SaveAsType.PDF) {
                    self.setCurrentInfoState(INFO_DOC_SAVED_IN_FOLDER, {
                        fullFileName,
                        fullPathName,
                        type: 'success',
                        duration: 10000
                    });
                    return;
                }

                // Templates are always saved in the user directory. Notify users about this
                if (fileType === SaveAsType.TEMPLATE) {
                    self.setCurrentInfoState(INFO_DOC_SAVED_AS_TEMPLATE, {
                        fullFileName,
                        fullPathName,
                        type: 'success',
                        duration: 10000
                    });
                }

                var fileId = getStringOption(data, 'id', null);
                if (!fileId && options && options.encrypted) {
                    fileId = _.isArray(data) ? getStringOption(_.first(data), 'data', null) : data;
                }

                folderId = folderId || getStringOption(data, 'folder_id', null);

                return getFile({ id: fileId, folder_id: folderId }).then(function (newFile) {

                    // add short file name as title, update the file descriptor
                    newFile.title = getFileBaseName(newFile.filename);
                    self.updateFileDescriptor(newFile);

                    // bug 32719: adapt launch options to prevent removing the file as it
                    // could be empty and new or a template file
                    launchOptions.action = 'load';
                    delete launchOptions.template;

                    // close the current document and restart with new one
                    self.reloadDocument();
                    // Return a 'never' resolved deferred to keep the busy screen open until the reload is done.
                    // When the reload is done, the reloaded app will be destroyed and replaced by a new app
                    // without a busy screen. This current, reloaded app with be destroyed and therefore the
                    // deferred will be rejected (by `createAbortablePromise()`) at this moment too.
                    return self.createAbortablePromise($.Deferred());
                });
            });

            // copy the document failed: show an appropriate warning message
            promise.catch(function (error) {

                // ignore errors due to destruction on app quit, which is expected at reloading
                if (error === 'destroy') { return; }
                // ignore cancelling dialog & other guard errors as guard
                // already shows error messages
                if ((error === 'cancel') || guardError) { return; }

                // handle docConverterUtilss
                if (ConverterUtils.isDocConverterError(error)) {
                    docView.yell({
                        type: 'error',
                        message: ConverterUtils.getErrorTextFromResponse(error)
                    });
                    return;
                }

                // make sure we always have a valid error object
                error = ErrorCode.getErrorCode(error);
                error.setErrorContext(ErrorContext.SAVEAS);
                globalLogger.warn('$badge{EditApp} saveDocumentAs: Error code "' + error.getCodeAsConstant() + '"');

                // show an alert banner
                docView.yell(getErrorMessageData(error, getInfoStateOptions()));
            });

            return promise;
        };

        /**
         * Returns whether reloading the document is currently enabled.
         *
         * @returns {boolean}
         *  Whether reloading the document is currently enabled.
         */
        this.isReloadEnabled = function () {
            return reloadEnabled;
        };

        /**
         * Closes this application instance, and launches a new application
         * instance with the document that is defined by the file descriptor.
         * This could be the same file as before, but also a new file in case
         * of "Save As".
         *
         * NOTE: This is a instant hard reload, use 'handlereload' to handle it more user friendly.
         *
         * @param {object} [reloadLaunchOptions]
         *  Additional settings to be merged into the launch options passed to
         *  the new application instance.
         *
         * @param {boolean} [noBusyOverlay=false]
         *  If set to `true`, prevents that the busy overlay is triggered by a
         *  reload.
         *
         * @returns {JPromise}
         *  A promise that represents the quit process of this application
         *  instance.
         */
        this.reloadDocument = function (reloadLaunchOptions, noBusyOverlay) {

            // freeze the toolbar so that switching to read-only mode etc. is hidden to the user
            this.trigger('docs:reload:prepare');

            var file;
            var fullFileName;
            if (reloadLaunchOptions?.fileDesc) {
                // the file descriptor to reload a different document
                file = reloadLaunchOptions.fileDesc;
                fullFileName = getSanitizedFileName(file);
            } else {
                // the file descriptor of the current document
                file = this.getFileDescriptor();
                fullFileName = this.getFullFileName();
            }

            if (!isNative(fullFileName)) {
                globalLogger.error('$badge{EditApp} reloadDocument: unsupported document type');
                return jpromise.reject();
            }

            // create the effective launch options to be passed to the new application
            reloadLaunchOptions = _.extend({}, reloadLaunchOptions, { action: 'load', file });
            if (launchOptions.auth_code) {
                reloadLaunchOptions.auth_code = launchOptions.auth_code;
            }

            // add reload options that are not dependent on the specific app (text, spreadsheet, presentation)
            reloadLaunchOptions.activeTabId = docView.toolPaneManager.getActiveTabId();
            reloadLaunchOptions.commentsPaneVisible = docView.commentsPane && docView.commentsPane.isVisible();

            // application-specific callback handler to extend the reload options
            Object.assign(reloadLaunchOptions, this.implPrepareReloadDocument());

            // show busy screen to prevent further user interaction
            if (!docView.isBusy() && !noBusyOverlay) { docView.enterBusy(); }

            // quit this application, and let it reload itself
            globalLogger.info('$badge{EditApp} reloadDocument: start reloading the document', reloadLaunchOptions);
            return this.quit({ reason: QuitReason.RELOAD, userData: reloadLaunchOptions });
        };

        /**
         * Tries to restore the document content using the backup-data stored on
         * the backend. The client is able to provide own actions, to be merged into
         * the backup-data/operation queue. If restore document is disabled, this
         * function just sets the error state and returns.
         *
         * @param {ErrorCode} errorCode
         *  The error code which occurred previous and is the reason to restore
         *  the document.
         *
         * @param {Object} [options]
         *  Additional options to control the behaviour of the function.
         *  @param {Array} [options.actions]
         *      The client operations to be stored with the restore document
         *      function. The restore document functions try to merge the operations
         *      to restore the document content to prevent data loss.
         */
        this.restoreDocument = function (errorCode, options) {

            // possible operations stored by the client or provided by the backend.
            var sentActionsPending = getArrayOption(options, 'actions', null);
            // file descriptor
            var file = self.getFileDescriptor();
            // actions that must be provided to the restore function
            var actionsPending = null;

            /**
             * Tries to merge the actions retrieved from the real-time connection and
             * the actions retrieved from the action buffer of the application.
             *
             * @param {Object|Array} sentActions
             *  An actions object or array with operations objects from the
             *  real-time connection which are pending.
             *
             * @param {Object|Array} appActions
             *  An actions object or array with operations objects from the
             *  application instance stored in the actionsBuffer.
             *
             * @returns {Array}
             *  An array with operations or null if no operations are available
             *  to be applied for restore document.
             */
            function mergeActions(sentActions, appActions) {
                var // result actions object
                    actions = {},
                    // sent but not ack operations array
                    sentActionsArray = getActionsArray(sentActions),
                    // pending application operations array
                    appActionsArray = getActionsArray(appActions);

                if (_.isArray(sentActionsArray) && _.isArray(appActionsArray)) {
                    actions = sentActionsArray.concat(appActionsArray);
                } else {
                    // fall-back use the sent actions
                    actions = sentActionsArray;
                }

                return actions;
            }

            // in case we don't support restore document - just set the error
            // code and return
            if (!Config.RESTORE_DOCUMENT_ENABLED) {
                self.setInternalError(errorCode);
                return;
            }

            // No need to call restoreDocument more than once, but there are
            // several notifications which should result in a restoreDocument.
            // Therefore we protecr us from being called more than once.
            if (docRestore) { return; }

            // Set doc restore to true - we don't need to reset it as the view
            // is read-only and won't change anymore.
            docRestore = true;
            restoreDocOnClose = false;

            if (_.isNull(sentActionsPending)) {
                // actions are minified when sent via RT, so not sent actions from the App must be minified too
                var minifiedApplicationActionBuffer = handleMinifiedActions(actionsBuffer);

                // In case we don't get specific pending operations we try to
                // recover them using the real-time connection and application's
                // action buffer.
                if (rtConnectionExist()) {
                    actionsPending = mergeActions(rtConnection.getPendingActions(), minifiedApplicationActionBuffer);

                } else {
                    actionsPending =  minifiedApplicationActionBuffer;
                }

            } else {
                actionsPending = sentActionsPending;
            }

            // set editor to internal error, read-only mode and lock it
            locked = true;
            changeEditMode(false);
            updateState();
            internalError = true;

            var saveRestoreArgs = {
                action: 'saveaswithbackup',
                file_id: file.id,
                target_filename: file.filename,
                target_folder_id: file.folder_id,
                restore_id: docRestoreId,
                operations: _.isNull(actionsPending) ? actionsPending : JSON.stringify(actionsPending)
            };

            var saveRestorePromise = self.sendFileRequest(FILTER_MODULE_NAME, saveRestoreArgs, { method: 'POST' });

            saveRestorePromise.then(function (result) {
                var // error code from the saveaswithbackup
                    restoreErrorCode = new ErrorCode(result),
                    // deferred to determine path
                    deferred = $.Deferred(),
                    // file id of the restored file
                    fileId = getStringOption(result, ErrorContextData.RESTORED_FILEID, null),
                    // file name of the restored file
                    fileName = getStringOption(result, ErrorContextData.RESTORED_FILENAME, null),
                    // folder id containing the restored file
                    folderId = getStringOption(result, ErrorContextData.RESTORED_FOLDERID, null),
                    // the path name of the restored file
                    pathName = null,
                    // extended context data for error processing
                    additionalData = {};

                if (_.isObject(result) && _.isString(fileName) && _.isString(folderId)) {
                    getPath(folderId).always(function (paths) {
                        pathName = preparePath(paths);
                        deferred.resolve(pathName);
                    });
                } else {
                    deferred.resolve(null);
                }

                deferred.always(function (data) {
                    var // support reload
                        updateFileDescriptor = _.isString(data);

                    if (!restoreErrorCode.isError()) {
                        // no error -> provide the success restore error code
                        restoreErrorCode = new ErrorCode({ errorClass: ErrorCode.ERRORCLASS_ERROR, error: 'BACKUPDOCUMENT_RESTORE_DOCUMENT_WRITTEN' });
                    }

                    // set necessary context data for error processing/output
                    additionalData[ErrorContextData.RESTORED_FILENAME] = fileName;
                    additionalData[ErrorContextData.RESTORED_PATHNAME] = pathName;
                    additionalData[ErrorContextData.ORIGINAL_FILEID] = self.getFileDescriptor().id;
                    if (updateFileDescriptor) {
                        // to support to 'reload' a new / restored document file we have to
                        // update the stored file descriptor
                        self.updateFileDescriptor({ id: fileId, folder_id: folderId });
                    }
                    self.setInternalError(restoreErrorCode, errorCode.getErrorContext(), errorCode, { additionalData });
                });
            }, function () {
                self.setInternalError(errorCode);
            });
        };

        /**
         * Rejects an attempt to edit the document (e.g. due to received
         * keyboard events in read-only mode). The application will show an
         * appropriate warning alert banner to the user, and will update the
         * internal state according to the cause of the rejection.
         *
         * @param {String} [cause='edit']
         *  The cause of the rejection. If omitted, a simple edit attempt has
         *  been rejected, without any further influence on the application
         *  state.
         *  - 'image':
         *    Inserting an image into the document has failed.
         *  - 'siri':
         *    A Siri event has been detected. The application will be set to
         *    internal error state. The user has to reload the document to make
         *    further changes.
         *
         * @returns {boolean}
         *  Whether an alert banner has been shown. The return value `false`
         *  means there is already a high-priority alert banner visible, and
         *  the new alert banner does not contain the `priority` flag.
         */
        this.rejectEditAttempt = function (cause) {

            if (this.isViewerMode()) {
                return false;
            }

            switch (cause) {
                case 'image':
                    return docView.yell({ message: gt('The image could not be inserted.') });

                case 'imageHttp':
                    return docView.yell({ message: gt('You cannot insert this large image from an insecure page.') });

                case 'siri':
                    this.setInternalError(ClientError.ERROR_SIRI_NOT_SUPPORTED, ErrorContext.GENERAL);
                    return true;

                case 'loadingInProgress':
                    return this.setCurrentInfoState(INFO_LOADING_IN_PROGRESS);

                default:
                    return this.isImportFinished() ? showReadOnlyAlert() :
                        this.setCurrentInfoState(INFO_LOADING_IN_PROGRESS);
            }
        };

        /**
         * Returns information about a specific user account.
         *
         * @param {number} userId
         *  The database identifier of the user.
         *
         * @returns {JPromise<UserData>}
         *  A promise that fulfils with an object containing the following user
         *  information:
         *  - {number} id
         *      The user identifier passed to this method.
         *  - {string} displayName
         *      The display name of the user (to be used in the GUI).
         *  - {string} operationName
         *      The internal name of the user (to be used in JOSN operations).
         *  - {boolean} guest
         *      Will be `true`, if the user is not a normal AppSuite user (e.g.
         *      guests invited by email).
         */
        this.getUserInfo = function (userId) {

            // ensure to chain the API call to an abortable promise bound to own lifetime
            var promise = this.createResolvedPromise().then(function () {
                return UserAPI.get({ id: userId });
            });

            // build the user data response object from the raw API data
            return promise.then(function (apiData) {

                // missing `guest_created_by`: regular AppSuite user
                var isGuestLocal = !!apiData.guest_created_by;
                // same code as UserAPI.getName()
                var displayName = _.noI18n(apiData.display_name || apiData.email1 || '');
                // create an appropriate user name for operations
                var operationName = isGuestLocal ? ('User ' + apiData.user_id) :
                    (apiData.first_name && apiData.last_name) ? _.noI18n(apiData.first_name.trim() + ' ' + apiData.last_name.trim()) :
                    (apiData.last_name) ? _.noI18n(apiData.last_name.trim()) :
                    (apiData.first_name) ? _.noI18n(apiData.first_name.trim()) :
                    displayName;

                return { id: apiData.user_id, displayName, operationName, guest: isGuestLocal };
            });
        };

        /**
         * Calls the realtime update user data function debounced.
         *
         * @param {Object} userData
         *  the user data, containing e.g. user selection etc.
         *
         * @returns {EditApplication}
         *  A reference to this instance.
         */
        this.updateUserData = (function () {

            // the cached user data from the last call of this method
            var cachedData = null;

            // direct callback: store user data of last invocation
            function storeUserData(userData) {
                cachedData = userData;
                return this;
            }

            // deferred callback: send last cached user data to server
            function sendUserData() {
                // bug 32520: do not send anything when closing document during import
                // DOCS-1311: do not send anything until the import isn't finished (i.e. RT has established a working connection)
                if (rtConnectionExist() && self.isImportSucceeded() && !internalError && !self.isInQuit()) {
                    rtConnection.updateUserData('updateuserdata', cachedData);
                }
            }

            return self.debounce(storeUserData, sendUserData, { delay: 500, maxDelay: 1000 });
        }());

        /**
         * Adds an author to a local index/map/registry.
         *
         * @param {Object} authorOptions
         *  - {number|string} [authorOptions.authorId]
         *  - {string} [authorOptions.authorName]
         *  - {string} [authorOptions.initials]
         *  - {number} [authorOptions.colorIndex]
         */
        this.registerAuthorItem = function (authorOptions) {

            // Example
            // authors: [{
            //     providerId: 'ox'
            //     userId: '7114'
            //     authorId: 2,
            //     authorName: "John Doe",
            //     initials: "JD",
            //     colorIndex: 1
            // }, ... ]

            if (('authorId' in authorOptions) && ('authorName' in authorOptions)/* && ('initials' in authorOptions)*/) {

                var authorId    = authorOptions.authorId;
                var authorName  = (((_.isFinite(authorId) || _.isString(authorId)) && getStringOption(authorOptions, 'authorName', '').trim()) || '');

                if (authorName) {
                    documentAuthorIndex[authorId] = {
                        id:         authorId, // {String|Number}
                        name:       authorName, // non empty {String}
                        initials:   (getStringOption(authorOptions, 'initials', '').trim() || null), // non empty {String} || {Null}
                        providerId: (getStringOption(authorOptions, 'providerId', '').trim() || null), // non empty {String} || {Null}
                        userId:     (getStringOption(authorOptions, 'userId', '').trim() || null) // non empty {String} || {Null}
                    };
                }
            }
        };

        this.getRegisteredAuthorItemById = function (authorId) {
            return getRegisteredAuthorItemById(authorId);
        };
        // this.getRegisteredAuthorNameById = function (authorId) {
        //     var authorItem = getRegisteredAuthorItemById(authorId);
        //     return (authorItem && authorItem.name);
        // };
        // this.getRegisteredAuthorInitialsById = function (authorId) {
        //     var authorItem = getRegisteredAuthorItemById(authorId);
        //     return (authorItem && authorItem.initials);
        // };

        /**
         * Returns the scheme color index to be used to render information for
         * an author from the document authors list. If the author name is not
         * known yet, it will be inserted into the list.
         *
         * @param {String} author
         *  The name of an author of this document.
         *
         * @returns {Number}
         *  The one-based scheme color index of the author in the document
         *  authors list.
         */
        this.getAuthorColorIndex = function (author) {
            return getSchemeColorIndex(getAuthorIndex(author));
        };

        /**
         * Adds the passed names of authors to the document authors list, and
         * keeps the list entries unique.
         *
         * @param {Array<String>} authors
         *  Author names to be added to the document authors list.
         *
         * @returns {EditApplication}
         *  A reference to this instance.
         */
        this.addAuthors = function (authors) {
            // _.union() retains element order of documentAuthorsList
            documentAuthorsList = _.union(documentAuthorsList, authors);
            return this;
        };

        /**
         * Stops registering of operations during callback function.
         *
         * @param {Function} callback
         *  The callback function to be executed during blocking of operations distribution.
         */
        this.stopOperationDistribution = function (callback) {
            distributeOperations = false;
            return self.convertToPromise(callback.call(self)).always(function () {
                distributeOperations = true;
            });
        };

        /**
         * Returns whether the registration of operations inside 'operations:success'
         * is blocked.
         *
         * @returns {Boolean}
         *  Whether the registration of operations inside 'operations:success' is blocked.
         */
        this.isOperationsBlockActive = function () {
            return operationsBlocked;
        };

        /**
         * Blocks registration of operations in 'operations:success' listener. This is especially
         * useful, if the user cancelled a long running process of asynchronous executed
         * operations.
         *
         * @param {Function} callback
         *  The callback function to be executed during blocking of operations.
         */
        this.enterBlockOperationsMode = function (callback) {
            operationsBlocked = true;
            // emptying an already filled pending action object
            // -> this could be filled by a delete action before a paste action
            if (hasPendingAction()) {
                // marking the last operations in the list of locally executed operations
                self.trigger('docs:operations:remove', pendingAction.operations.length);
                // empty the list of already locally executed operations
                pendingAction = createAction(null, pendingAction.userData);
            }
            // calling the callback function
            callback.call(self);
            operationsBlocked = false;
        };

        /**
         * Whether the application supports multiple selections.
         *
         * @returns {Boolean}
         *  Returns whether this application supported multiple selection.
         */
        this.isMultiSelectionApplication = function () {
            return isMultiSelectionApp;
        };

        /**
         * Whether the first toolbar tab is created after loading a document.
         *
         * @returns {Boolean}
         *  Returns whether the first toolbar tab is created after loading a document.
         */
        this.isFirstTabActivated = function () {
            return firstTabActivated;
        };

        // operation transformations ------------------------------------------

        /**
         * Returns whether OT is enabled for the current document.
         *
         * @returns {Boolean}
         *  Whether OT is enabled for the current document.
         */
        this.isOTEnabled = function () {
            return isOTEnabled;
        };

        /**
         * The operation transformation manager for this document.
         */
        Object.defineProperty(this, 'otManager', {
            // create OT manager lazily on demand
            get() { return otManager || (otManager = mvcFactory.createOTManager(this.docModel)); },
            configurable: true,
            enumerable: true
        });

        /**
         * Transforms an external JSON document position with all local JSON
         * document operations waiting for acknoledgement.
         *
         * @param {Position} position
         *  The JSON document position to be transformed.
         *
         * @param {String} [target]
         *  An optional target string for the specified position.
         *
         * @returns {Opt<Position>}
         *  The transformed JSON position; or `undefined`, if the position
         *  cannot be transformed (e.g. has been "deleted").
         */
        this.transformPositionWithLocalActions = function (position, target) {
            return this.otManager.transformPosition(position, localActionsWithoutAck, target);
        };

        /**
         * Returns statistics for operation transformations.
         *
         * @returns {OTStatistics}
         *  The OT statistics.
         */
        this.getOTStatistics = function () {
            const otCount = otManager?.getStatistics().otCount ?? 0;
            return { extOpCount, otCount };
        };

        /**
         * OT DEMO
         * Setting the blocking operation mode for testing reasons. In this case, no operations
         * are sent to the server.
         *
         * @param {Boolean} value
         *  Activating or deactivating the blocking operation mode for testing reasons.
         */
        this.setOTBlockingOpsMode = function (value) {
            isOTBlockingOpsMode = value;
        };

        /**
         * OT DEMO
         * Check, whether the blocking mode for operations is active (only for testing reasons).
         *
         * @returns {Boolean}
         *  Whether the blocking mode for operations is active (only for testing reasons).
         */
        this.isOTBlockingOpsMode = function () {
            return isOTBlockingOpsMode;
        };

        /**
         * OT: Whether there are locally cached actions that wait for an acknowledge of the server.
         * Some of these actions might have been sent to the server in the meantime. These sent
         * actions have the marker 'sentToServer'.
         * After receicing an Ack from the server, these marked actions can be removed from this
         * local cache. All other actions have to stay inside this local cache.
         *
         * @returns {Boolean}
         *   Whether there are locally cached actions that wait for an acknowledge of the server.
         */
        this.hasLocalActionsWithoutAck = function () {
            return isOTEnabled && (localActionsWithoutAck.length > 0);
        };

        /**
         * OT: Getting the current server OSN of the document. This is a number, that is only
         * sent from the server. The client has to keep track of it. It must be sent with all
         * actions to the server, so that the server knows the state of the document, when this
         * actions were generated.
         * The server informs the client about a new OSN:
         *  - when loading the document.
         *  - when sending an Ack to the client for the operations sent from this client.
         *  - when sending actions from all other clients.
         *
         * @returns {Number}
         *   The current server OSN of this document.
         */
        this.getServerOSN = function () {
            return serverOSN;
        };

        /**
         * Checks, whether the document loaded has a rescue format or not.
         *
         * @returns {Boolean}
         *  Whether the document is a rescue document or not.
         */
        this.isRescueDocument = function () {
            return isRescueDoc;
        };

        /**
         * Checks whether the rescue document mode is active or not.
         *
         * @returns {Boolean}
         *  Whether the application is in rescue document mode or not.
         */
        this.isRescueDocMode = function () {
            return Config.DEBUG && isRescueDoc;
        };

        /**
         * Generates and returns the formatted global message list recorded
         * from all active loggers with an additional information header.
         *
         * @returns {Opt<string[]>}
         *  The global message list with an additional information header lines; or
         *  `undefined`, if no log data is available.
         */
        this.generateLogData = function () {

            // early out, don't return only the header without log entries
            const messages = Logger.getRecordedMessages();
            if (messages.length === 0) { return undefined; }

            // create header lines
            return [
                "__[OX_Documents_UI_log]__", // leading identifier, important for file type auto detection
                navigator.userAgent,
                `doc_uid: ${currentDocUid}`,
                `client_uid: ${currentClientUid}`,
                `app: ${this.appType}`,
                `guestUser: ${isGuest()}`,
                `fileName: ${this.getFullFileName()}`,
                " ", // empty separator line between header and log messages
                ...messages
            ];
        };

        /**
         * Provides a text file download for the current global log buffer.
         */
        this.downloadFrontendLog = function () {
            downloadFrontendLog(this.generateLogData());
        };

        /**
         * Setting whether the operation optimization before sending operations shall be used.
         * This is for example not useful, if operations are applied from the operation player.
         * In this case the operations visible in the operations panel should not be merged.
         *
         * @param {Boolean} value
         *  Whether an optionally existing handler for optimizing the operations shall be used.
         */
        this.useOperationOptimization = function (value) {
            useOperationOptimization = value;
        };

        /**
         * Whether the main toolpane should be shown when loading the document.
         *
         * @returns {boolean}
         *  Whether the main toolpane should be shown.
         */
        this.showMainToolPaneAtStart = function () {
            return !this.isViewerMode() && (COMBINED_TOOL_PANES || this.getUserSettingsValue('showToolbars', true) || _.device('!desktop'));
        };

        // initialization -----------------------------------------------------

        // an asynchronous worker with all steps to perform acquiring edit rights from another user
        acquireEditRightsWorker = this.member(new AcquireEditRightsWorker(this));
        acquireEditRightsWorker.on("worker:start", () => this.trigger('docs:acquireeditrights:start'));
        acquireEditRightsWorker.on("worker:finish", () => this.trigger('docs:acquireeditrights:end'));

        // DOCS-1430: a background worker that constantly sends the current local OSN
        osnBeacon = this.member(new IntervalRunner(silentSyncStable, { delay: 30000, interval: 30000 }));

        // setup event handlers
        this.on("docs:session:changed", handleSessionChanged);
        this.on("docs:session:invalid", () => handleSessionInvalidError("http"));

        // initialization after construction
        this.onInit(() => {

            // get references to MVC instances after construction
            docModel = this.docModel;
            docView = this.docView;

            // marker class for the root element (application window)
            this.getWindow().addClass('io-ox-office-edit-window');

            // store exact time stamp when import finishes (regardless of the result)
            this.waitForImport(function () {
                importFinishedTime = self.launchTracker.step('triggerImportSuccess', '$badge{EditApp} End of import process notified');
                lastEditTransferActivity = importFinishedTime;
                rtLogger.log('Load-mode:', loadMode);
            });

            // calculate initial application state
            updateState();

            // registering a handler for tab activation for performance measurements
            registerTabActivationHandler();

            // registering a handler for the first tab activation after loading a document
            registerFirstTabActivatedHandler();

            // switch to read-only mode when any document operations have failed
            this.listenTo(docModel, 'operations:error', function () { changeEditMode(false); });

            // write document attributes into DOM for automated testing
            this.listenTo(docModel, "change:config", changed => {
                if ("fv" in changed) {
                    this.setRootAttribute("data-filter-version", changed.fv);
                }
            });

            // switch to read-only mode when any document operations have failed
            this.on('docs:reload', handleReload);

            // triggered after the operations are registered for sending them to the server (OT)
            this.on('docs:operations:completed', checkExternalOperations);

            // check whether editing is globally disabled for this application type
            if (!supportsEditMode(self.appType)) {
                locked = true;
                changeEditMode(false);
                updateState();
                showReadOnlyAlert(DOM_TEXT_EDIT_ALERT, -1);
            }

            if (localStorageApp && saveFileInLocalStorage) {
                globalLogger.info('$badge{EditApp} onInit(): saving document to local storage is active');
                if (STORAGE_AVAILABLE) {
                    globalLogger.info('$badge{EditApp} onInit: current browser supports local storage');
                } else {
                    globalLogger.warn('$badge{EditApp} onInit: current browser does not support local storage');
                }
            }

            // track flush state for automatic flush on browser tab changes
            this.on('docs:flush:success', function () {
                hasActionsAfterFlush = false;
            });

            docModel.listenToGlobal("tab:hide", () => {
                // flush the document when the browser tab loses the focus and something in the document is changed
                if (hasActionsAfterFlush) {
                    tabLogger.info('$badge{EditApp} saving pending actions on hiding browser tab');
                    self.flushDocument(FlushReason.USER);
                }
                if (isOTEnabled) {
                    osnBeacon.abort();
                }
            });

            docModel.listenToGlobal("tab:show", () => {
                if (isOTEnabled) {
                    enableOsnBeacon();
                }
            });

            // flush the document on app hide when something is changed, this is similar to flush on tab hide, but for single
            // tab behavior, to have a unified behavior with tabs and to make sure the document securily saved at rest as fast as possible
            docModel.listenTo(self.getView(), 'view:hide', function () {
                if (hasActionsAfterFlush) {
                    tabLogger.info('$badge{EditApp} saving pending actions on hiding view');
                    self.flushDocument(FlushReason.USER);
                }
            });

        });

        // set quit handlers
        this.registerBeforeQuitHandler(beforeQuitHandler);
        this.registerQuitHandler(quitHandler);

        // send "emergencyLeave" before bowser tab unloads
        this.listenTo(ox, "beforeunload", unsavedChanges => {

            // after appsuite logout the page is redirected, nothing to do anymore here
            if (!unsavedChanges && !appsuiteLoggedOut()) {

                // refresh drive in another open tab
                const fileDesc = this.getFileDescriptor();
                if (BROWSER_TAB_SUPPORT && fileDesc) {
                    jpromise.floating(propagateChangeFile(fileDesc));
                }

                emergencyLeave();
            }
        });

        // send "emergencyLeave" when bowser tab unloads
        this.listenTo(window, "unload", () => {

            // after appsuite logout the page is redirected, nothing to do anymore here
            if (!appsuiteLoggedOut()) {
                emergencyLeave();
            }
        });

        // DOCS-1430: start sending the current local OSN to server in an endless loop
        this.waitForImportSuccess(function () {
            if (isOTEnabled) { enableOsnBeacon(); }
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Will be invoked when creating a new empty document.
     *
     * @returns {Opt<Dict>}
     *  A JSON object with additional parameters that will be inserted into the
     *  server request for creating the new document.
     */
    /*protected*/ implPrepareNewDoc() {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked before the document operations will be downloaded and
     * applied.
     *
     * @returns {MaybeAsync}
     *  May return a promise that will fulfil or reject after the document has
     *  been prepared.
     */
    /*protected*/ implPreProcessImport() {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked after the document operations have been downloaded and
     * applied successfully, intended to postprocess the document contents.
     *
     * @param {boolean} _fromStorage
     *  Whether the document has been loaded from the local storage.
     *
     * @returns {MaybeAsync}
     *  May return a promise that will fulfil or reject after the document has
     *  been postprocessed.
     */
    /*protected*/ implPostProcessImport(_fromStorage) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked when importing an empty document in a special fast way
     * is supported.
     *
     * @param {string} _markup
     *  The initial HTML markup.
     *
     * @param {Operation[]} _operations
     *  Additional document operations.
     *
     * @returns {MaybeAsync}
     *  May return a promise that will fulfil or reject after the document has
     *  been loaded quickly.
     */
    /*protected*/ implFastEmptyLoad(_markup, _operations) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked to show an early preview of the imported document, while
     * the import is still running.
     *
     * @param {Dict} _previewData
     *  The preview data object sent from the server.
     *
     * @returns {MaybeAsync<boolean>}
     *  A boolean value (optionally asynchronously via promise) stating whether
     *  displaying the preview succeeded. On success, the view will leave the
     *  busy state.
     */
    /*protected*/ implPreviewDocument(_previewData) {
        // intended to be overridden by subclasses
        return false;
    }

    /**
     * Will be invoked after importing the document has failed for any reason
     * (either connection problems, or while applying the operations).
     *
     * @param {ErrorCode} _error
     *  The parsed error response data.
     */
    /*protected*/ implImportFailed(_error) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked to optimize the generated document operations before
     * they will be sent to the server.
     *
     * @param {OperationAction[]} _actions
     *  The actions with document operations to be optimized.
     *
     * @returns {Opt<OperationAction[]>}
     *  The optimized document operations if available. Returning `undefined`
     *  causes to use the original operation actions passed to this method.
     */
    /*protected*/ implOptimizeOperations(_actions) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked before sending all pending actions, and flushing the
     * document to the source file.
     *
     * @param {FlushReason} _reason
     *  The origin of the flush request.
     *
     * @returns {MaybeAsync}
     *  May return a promise that will fulfil or reject as soon as the
     *  application has finished all preparations for flushing the document.
     */
    /*protected*/ implPrepareFlushDocument(_reason) {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked if the server sends a request to leave the edit mode.
     *
     * @returns {MaybeAsync}
     *  May return a promise that will fulfil or reject as soon as the
     *  application can safely switch to read-only mode.
     */
    /*protected*/ implPrepareLoseEditRights() {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked if the client wants to rename the document.
     *
     * @returns {MaybeAsync}
     *  May return a promise that will fulfil or reject as soon as the
     *  application can safely send the rename request.
     */
    /*protected*/ implPrepareRenameDocument() {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked before the document will actually be reloaded.
     *
     * @returns {Opt<Partial<BaseLaunchConfig>>}
     *  May return launch configuration options that will be merged into the
     *  existing launch configuration used to reload this document.
     */
    /*protected*/ implPrepareReloadDocument() {
        // intended to be overridden by subclasses
    }

    /**
     * Will be invoked before the browser window unloads.
     *
     * @returns {Opt<EmergencyLeaveData>}
     *  May return an `EmergencyLeaveData` object that will be passed to
     *  `RTConnection::emergencyLeave`, or `undefined` to indicate there is no
     *  extra data to be sent.
     */
    /*protected*/ implPrepareLeaveData() {
        // intended to be overridden by subclasses
    }
}
