/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';
import { locationHash } from '$/url';

import { uuid } from '@/io.ox/office/tk/algorithms';
import { FILTER_MODULE_NAME } from '@/io.ox/office/tk/utils/io';
import { BaseObject } from '@/io.ox/office/tk/objects';

import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import { rtLogger } from '@/io.ox/office/editframework/utils/editconfig';

import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import { calcDocUIDWithoutPrefix } from '@/io.ox/office/rt2/rt2utils';

// class RTConnectionViewer ===================================================

/**
 * Represents the connection to the real-time framework, used to send
 * message to and receive message from the server.
 *
 * Triggers the following events:
 * - 'update': Update notifications containing actions created by remote
 *  clients, and state information for the application (read-only, name and
 *  identifier of the current editor, etc.).
 *
 * @param {EditApplication} app
 *  The application containing this operations queue.
 *
 * @param {Object} [initOptions]
 *  Optional parameters:
 */
export default class RTConnectionViewer extends BaseObject {

    constructor(app /*, initOptions*/) {

        // base constructor
        super();

        // file descriptor of the edited document
        var file = app.getFileDescriptor();

        var traceActions = {};

        // is encrypted document files
        var isEncrypted = false;

        var clUID = uuid.random();

        var docUID = calcDocUIDWithoutPrefix(file['com.openexchange.realtime.resourceID'] || file.id);

        var self = this;

        // connect/open states
        var connectOpenState = 'unknown';

        // close/leave states
        var closeLeaveState = 'unknown';

        // public methods -----------------------------------------------------

        /* public string */ this.getClientUID = function () {
            return clUID;
        };

        /* public string */ this.getDocUID = function () {
            return docUID;
        };

        /* public string */ this.getChannelUID = function () {
            return 'not provided in viewer';
        };

        /* public boolean */ this.isConnected = function () {
            return ox.online;
        };

        /* public string */ this.getCloseLeaveState = function () {
            return closeLeaveState;
        };

        /**
         * Returns if the rtConnection (self) exists at the moment.
         *
         * @returns {Boolean}
         *  True when the rtConnection (self) is existing.
         */
        function rtConnectionExist() {
            return !!(self && !self.destroyed);
        }

        /**
         * Triggers an event on the rtConnection safely.
         *
         * @param {String} key
         * @param {Object} data
         */
        function rtConnectionTrigger(key, data) {
            if (rtConnectionExist()) {
                self.trigger(key, data);
            }
        }

        /**
         * Connects to the real-time group and sends the initial request for the
         * document import operations.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the getActions results.
         */
        this.connect = function () {
            // we use 'joined' because the viewer has no real connection
            connectOpenState = 'joined';
            var errorCode = new ErrorCode();
            return $.when(errorCode);
        };

        /**
         *
         */
        this.disconnect = function () {
            // we use 'disconnected' because the viewer has no real connection
            closeLeaveState = 'disconnected';
            var errorCode = new ErrorCode();
            return $.when(errorCode);
        };

        /**
         * Opens the real-time group and sends the initial request for the
         * document import operations.
         *
         * @param {Object} openData
         *  Contains properties to be sent while joining to the
         *  document real-time connection.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the getActions results.
         */
        this.open = function (openData) {

            var openDeferred = $.Deferred();
            var getOpsData = { action: 'getoperations', id: file.id, folder_id: file.folder_id, version: file.version, source: file.source };

            if (_.isString(openData.auth_code) && !_.isEmpty(openData.auth_code)) {
                isEncrypted = true;
                _.extend(getOpsData, { cryptoAuth: openData.auth_code });
            }

            app.sendRequest(FILTER_MODULE_NAME, getOpsData).done(function (data) {
                var error = new ErrorCode(data);
                var firstChunk = {};
                var lastChunk = null;

                if (!error.isError()) {
                    firstChunk.operations = data.operations;
                    if (data.htmlDoc) { firstChunk.htmlDoc = data.htmlDoc; }
                    if (data.syncInfo) { firstChunk.syncInfo = data.syncInfo; }

                    if (!app.getFullFileName() && data.syncInfo && data.syncInfo.fileName) { app.updateFileDescriptor({ filename: data.syncInfo.fileName }); } // DOCS-2920

                    if (_.isObject(data.preview)) {
                        // remaining ops chunk
                        lastChunk = {};
                        lastChunk.docStatus = { clientId: 'dummy' };
                        lastChunk.operations = data.operations;
                        lastChunk.error = data.error;
                        // preview ops chunk
                        firstChunk = data.preview;
                    }
                    firstChunk.docStatus = { clientId: 'dummy' };

                    // we probably only need 'opened' state for the mocked rtConnection
                    connectOpenState = 'opened';
                    openDeferred.resolve(data);

                    rtConnectionTrigger(RT2Protocol.RESPONSE_OPEN_DOC_CHUNK, firstChunk);

                    if (lastChunk) {
                        rtConnectionTrigger(RT2Protocol.RESPONSE_OPEN_DOC_CHUNK, lastChunk);
                        app.executeDelayed(function () {
                            rtConnectionTrigger(RT2Protocol.RESPONSE_OPEN_DOC, { docStatus: { serverOSN: 1, clientId: 'dummy' } });
                        }, 100);
                    } else {
                        rtConnectionTrigger(RT2Protocol.RESPONSE_OPEN_DOC, { docStatus: { serverOSN: 1, clientId: 'dummy' } });
                    }
                } else {
                    rtConnectionTrigger(RT2Protocol.RESPONSE_OPEN_DOC, _.extend({ hasErrors: true }, data));
                    openDeferred.reject(data);
                }
            });

            // Remember *open* deferred to be able to reject them if user
            // closes document while we are still connecting to the realtime group!
            return this.createAbortablePromise(openDeferred);
        };

        /**
         *
         */
        this.abortOpen = function () {
            return $.when();
        };

        /**
         * Send the given operations to the connected RT object
         * for further distribution to other connected clients
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the ACK of the internal send
         *  arrives.
         */
        this.sendActions = function () {
            return $.when();
        };

        /**
         * Sends user data update to the server. This user update data will
         * broadcasted to all collaborating clients.
         *
         * @returns {jQuery.Promise}
         */
        this.updateUserData =  function () {
            return $.when();
        };

        /**
         * Sends a real-time request to the server and waits for a response.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the answer of the server
         *  request, or rejected on error or timeout.
         */
        this.sendQuery =  function () {
            return $.when();
        };

        /**
         * Initiates flushing of the document using a synchronous request.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the answer for flushDocument
         *  from the server arrives.
         */
        this.flushDocument =  function () {
            return $.when();
        };

        /**
         * Send our wish to acquire the edit rights to the connected RT object
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the ACK of the internal send
         *  arrives.
         */
        this.acquireEditRights = function () {
            return $.when();
        };

        /**
         * Acknowledge to the server that our preparation to lose the
         * edit rights are completed.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the ACK of the internal send
         *  arrives.
         */
        this.canLoseEditRights = function () {
            return $.when();
        };

        /**
         * Sends client log message to the server so it can be written to the
         * server side log files for later debugging.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the ACK of the internal send
         *  arrives.
         */
        this.sendLogMessage =  function () {
            return $.when();
        };

        /**
         * Sends a sync request to the server, which will answer with a update
         * notification. This can be used to synchronize client and server
         * after an off-/online scenario.
         */
        this.sync =  function () {
            return $.when();
        };

        /**
         * Sends a sync stable request to the server. Which should answer with
         * the current document state and ensures that the client sends
         * messages with seqnr that are missing on the server-side. The client
         * can check if the osn and other properties to determine the synchronization
         * state.
         */
        this.syncStable = function () {
            return $.when();
        };

        /**
         * Sends a "alive" answer to the server, which extends the time
         * for client to make necessary changes until a "edit rights" switch will
         * be forced due to time-out.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the ACK of the internal send
         *  arrives.
         */
        this.alive =  function () {
            return $.when();
        };

        /**
         * Send an emergency leave request to the server with leaveData
         * that can be used by the server to make last-time changes if the
         * client has edit-rights.
         *
         * @param {Object|Null} leaveData
         *  An object which contains properties to be used by the server to
         *  save view settings within the document if the client has edit-
         *  rights.
         *
         *  @returns {Boolean}
         *   TRUE if the request could be sent otherwise FALSE.
         */
        /* public boolean */ this.emergencyLeave = function () {
            return true;
        };

        /**
         * Provides the current state open/connect of a document connection.
         * Can be one of the following values:
         * 'unknown', 'opening', 'opened', 'joining', 'joined'
         *
         * @returns {String}
         *  The current state of close/leave a document connection.
         */
        /* public string */ this.getConnectOpenState = function () {
            return connectOpenState;
        };

        /**
         * Close the document at the connected RT object
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the closing document status of
         *  the request.
         */
        this.closeDocument = function () {
            closeLeaveState = 'closing';
            var promise = null;
            if (isEncrypted) {
                var getOpsData = { action: 'getoperations', subaction: 'close', id: file.id, folder_id: file.folder_id, version: file.version, source: file.source };

                promise = app.sendRequest(FILTER_MODULE_NAME, getOpsData);
            } else {
                promise = $.when();
            }
            promise.done(function () { closeLeaveState = 'closed'; });
            return promise;
        };

        /**
         * Retrieves the pending actions, not processed by the backend. These
         * actions can be provided to a restoreDocument() function to try to
         * restore the document.
         *
         * @returns {Object}
         *  The result object contains the property "actions" which is an Array
         *  with all pending operations (without server-side ack).
         */
        this.getPendingActions = function () {
            return { actions: [] };
        };

        /**
         * Determines, if we have pending actions which have not been acknowledge by
         * the OX Documents backend.
         *
         * @returns {Boolean} TRUE, if there are pending actions, otherwise FALSE.
         */
        this.hasPendingActions = function () {
            return false;
        };

        // initialization -----------------------------------------------------

        rtLogger.log('RTConnectionViewer initialization');

        // read traceable actions from page URL
        if (locationHash('office:trace')) {
            for (const action of locationHash('office:trace').split(/\s*,\s*/)) {
                traceActions[action.toLowerCase()] = true;
            }
        }
    }
}
