/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { FileFormatType, FlushReason } from "@/io.ox/office/baseframework/utils/apputils";
import ErrorCode from "@/io.ox/office/baseframework/utils/errorcode";
import { BaseApplicationEventMap, BaseApplication } from "@/io.ox/office/baseframework/app/baseapplication";
import { BaseLaunchConfig, CoreApp } from "@/io.ox/office/baseframework/app/appfactory";
import { OTStatistics } from "@/io.ox/office/editframework/utils/otutils";
import { EditResourceManager } from "@/io.ox/office/editframework/resource/resourcemanager";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";
import { BaseOTManager } from "@/io.ox/office/editframework/model/operation/otmanager";
import { Operation } from "@/io.ox/office/editframework/utils/operations";
import { OperationAction } from "@/io.ox/office/editframework/utils/operationutils";
import { EditView } from "@/io.ox/office/editframework/view/editview";
import { EditController } from "@/io.ox/office/editframework/app/editcontroller";
import RTConnection from "@/io.ox/office/editframework/app/rtconnection";
import { EditMVCFactory } from "@/io.ox/office/editframework/app/mvcfactory";

// types ======================================================================

/**
 * Configuration options for `EditApplication` instances.
 */
export interface EditApplicationConfig {

    /**
     * The key of the boolean configuration entry specifying whether OT
     * (concurrent editing mode) is enabled for this application.
     */
    otConfigKey?: string;

    /**
     * If set to `true`, the application pane will be detached from the DOM
     * while the import actions will be applied. This may improve the import
     * performance of large documents. Default value is `false`.
     */
    applyActionsDetached?: boolean;

    /**
     * The duration in milliseconds used to debounce passing new pending
     * actions to the realtime connection. Default value is `0`.
     */
    sendActionsDelay?: number;

    /**
     * The duration in milliseconds the realtime framework will collect
     * messages before sending them to the server. Default value is `700`.
     */
    realTimeDelay?: number;

    /**
     * A function that can be used to merge the operations and cached before
     * they are sent to the server. Receives an operations array as first
     * parameter, cached buffer of operations as second parameter, and must
     * return an operations array. Will be called in the context of this
     * application instance.
     */
    mergeCachedOperationsHandler?: (actionsBuffer: OperationAction[], cacheBuffer: OperationAction[]) => OperationAction[];

    /**
     * Specifies whether the local storage shall be used for storing the
     * document to improve load performance. Default value is `false`.
     */
    useStorage?: boolean;

    /**
     * Specifies whether the application supports synchronization to enable
     * seamless editing of a document after a offline/online transition.
     * Default value is `false`.
     */
    supportsOnlineSync?: boolean;
}

/**
 * Descriptor of an active client viewing or editing the same document.
 */
export interface RemoteClientData {

    /**
     * The unique real-time session identifier of the client.
     */
    clientUID: string;

    /**
     * A unique integral zero-based index for the client that will never change
     * during the lifetime of this application.
     */
    clientIndex: number;

    /**
     * The identifier of the user (a user will have the same identifier on
     * every remote client).
     */
    userId: number;

    /**
     * The name of the user intended to be displayed in the UI.
     */
    userName: string;

    /**
     * Whether the entry represents a remote client (`true`), or the local
     * client (`false`).
     */
    remote: boolean;

    /**
     * Whether the respective client has edit rights.
     */
    editor: boolean;

    /**
     * A one-based index into the color scheme used to visualize the client
     * (e.g. user list, or remote selections).
     */
    colorIndex: number;

    /**
     * Optional application-dependent data for the user that has been set by
     * the respective client, e.g. the current selection.
     */
    userData?: object;
}

/**
 * Data for an emergency leave request that can be used by the server to make
 * last-time changes if the client has edit rights.
 */
export interface EmergencyLeaveData {

    /**
     * Additional JSON document operations to be saved. In concurrent editing
     * (OT) mode, these operations MUST contain the current server OSN
     * contained in the property "serverOSN" of this object.
     */
    operations?: readonly Operation[];

    /**
     * The local OSN used to synchronize server operations in concurrent
     * editing (OT) mode.
     */
    serverOSN?: number;
}

/**
 * Type mapping for the events emitted by `EditApplication` instances.
 */
export interface EditApplicationEventMap extends
    BaseApplicationEventMap,

    /**
     * Will be emitted after the state of the application has changed to the
     * specified identifier. Will be emitted after the generic "docs:state"
     * event. Example: when the state "offline" is reached, the application
     * will trigger a "docs:state" event, and a "docs:state:offline" event.
     */
    Record<`docs:state:${string}`, [error: Opt<ErrorCode>]> {

    /**
     * Will be emitted after the state of the application has changed.
     *
     * @param state
     *  The current application state, as also returned by the method
     *  `EditApplication::getState`.
     */
    "docs:state": [state: string, oldState: string, error: Opt<ErrorCode>];

    /**
     * Will be emitted when the editable state of the document changes.
     *
     * @param editable
     *  The current state whether the document is editable, as also returned by
     *  the method `EditApplication::isEditable`.
     */
    "docs:editmode": [editable: boolean];

    /**
     * Will be emitted when the document contents become editable. Will be
     * emitted after the generic "docs:editmode" event.
     */
    "docs:editmode:enter": [];

    /**
     * Will be emitted when the document contents are not editable anymore
     * (read-only). Will be emitted after the generic "docs:editmode" event.
     */
    "docs:editmode:leave": [];

    /**
     * Will be emitted after starting to acquire edit rights for the document.
     */
    "docs:acquireeditrights:start": [];

    /**
     * Will be emitted when acquiring edit rights for the document has
     * finished, regardless of the result.
     */
    "docs:acquireeditrights:end": [];

    /**
     * Will be emitted after the list of editor clients for this document has
     * changed.
     *
     * @param clients
     *  An array with all active editor clients, as also returned by the method
     *  `EditApplication::getActiveClients`.
     */
    "docs:users": [clients: RemoteClientData[]];

    /**
     * Will be emitted after the selection of one of the editor clients for
     * this document has changed.
     *
     * @param clients
     *  An array with all active editor clients, as also returned by the method
     *  `EditApplication::getActiveClients`.
     */
    "docs:users:selection": [clients: RemoteClientData[]];

    /**
     * Will be emitted when the operations are registered from sending them to
     * the server. Even if the user presses the 'cancel' button in a long
     * running process this event will be emitted. After this event it is sure
     * that an operation is fully completed (including regisration in OT
     * container), so that following (external) operations can be applied.
     */
    "docs:operations:completed": [];

    // TODO: event documentation

    "docs:operations:remove": [count: number];
    "docs:rescueoperations": [Operation[]];

    "docs:edit:beforequithandler": [];
    "docs:edit:quithandler": [];
    "docs:edit:quithandler:after": [];

    "docs:reload:prepare": [];
    "docs:reload": [error: ErrorCode];

    "docs:operationtrace:new:external": [operations: Operation[], startOSN: number, serverOSN: number];
    "docs:operationtrace:send:internal": [actions: OperationAction[], finalOSN: number];
    "docs:operationtrace:new:internal": [operations: Operation[], startOSN: number];
    "docs:operationtrace:ackosn": [ackOSN: number, sendOSN: number];
}

// class EditApplication ======================================================

export class EditApplication<
    EvtMapT extends EditApplicationEventMap = EditApplicationEventMap
> extends BaseApplication<EvtMapT> {

    declare readonly resourceManager: EditResourceManager<EditApplication, any>;
    declare readonly docModel: EditModel;
    declare readonly docView: EditView;
    declare readonly docController: EditController;

    readonly rtConnection: RTConnection | null;
    readonly otManager: BaseOTManager;

    constructor(
        coreApp: CoreApp,
        mvcFactory: EditMVCFactory<EditApplication>,
        appConfig?: EditApplicationConfig,
    );

    getFileFormat(): FileFormatType;
    isOOXML(): boolean;
    isODF(): boolean;

    isEditable(): boolean;
    isOnline(): boolean;
    isLocallyModified(): boolean;
    isInternalError(): boolean;
    rejectEditAttempt(cause: string): boolean;
    enterBlockOperationsMode(callback: () => void): void;

    getActiveClients(): RemoteClientData[];
    getClientId(): string;
    getClientOperationName(): string;
    getShortClientId(): string;
    getDocUID(): string;
    getLoadMode(): string;
    getLoadDocInfo(): Dict;
    getEditClientId(): string;
    getEditClientUserName(): string;

    updateUserData(data: object): void;

    getAuthorColorIndex(author: string): number;

    setInternalError(errorCode: ErrorCode | Dict, context: string, cause?: ErrorCode | Dict, options?: object): void;

    isOperationsBlockActive(): boolean;
    useOperationOptimization(value: boolean): void;

    isOTEnabled(): boolean;
    getOTStatistics(): OTStatistics;
    isOTBlockingOpsMode(): boolean;
    setOTBlockingOpsMode(state: boolean): void;
    getServerOSN(): number;

    showMainToolPaneAtStart(): boolean;

    reloadDocument(reloadLaunchOptions?: unknown, noBusyOverlay?: boolean): JPromise;

    downloadFrontendLog(): void;

    protected implPrepareNewDoc(): Opt<Dict>;
    protected implPreProcessImport(): MaybeAsync;
    protected implPostProcessImport(fromStorage: boolean): MaybeAsync;
    protected implFastEmptyLoad(markup: string, operations: Operation[]): MaybeAsync;
    protected implPreviewDocument(previewData: Dict): MaybeAsync<boolean>;
    protected implImportFailed(error: ErrorCode): void;
    protected implOptimizeOperations(actions: OperationAction[]): Opt<OperationAction[]>;
    protected implPrepareFlushDocument(reason: FlushReason): MaybeAsync;
    protected implPrepareLoseEditRights(): MaybeAsync;
    protected implPrepareRenameDocument(): MaybeAsync;
    protected implPrepareReloadDocument(): Opt<Partial<BaseLaunchConfig>>;
    protected implPrepareLeaveData(): Opt<EmergencyLeaveData>;
}
