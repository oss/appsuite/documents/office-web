/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import ox from '$/ox';

import SessionRestore from '$/io.ox/core/tk/sessionrestore';

import { uuid } from '@/io.ox/office/tk/algorithms';
import { BaseObject } from '@/io.ox/office/tk/objects';

import { ERROR_UNKNOWN_REALTIME_FAILURE } from '@/io.ox/office/baseframework/utils/clienterror';
import { REALTIME_DEBUGGING, SHOW_REMOTE_SELECTIONS, rtLogger } from '@/io.ox/office/editframework/utils/editconfig';

import DocRTBase from '@/io.ox/office/rt2/docrtbase';
import RT2Const from '@/io.ox/office/rt2/rt2const';
import RT2Protocol from '@/io.ox/office/rt2/shared/rt2protocol';
import RT2ErrorCode from '@/io.ox/office/rt2/shared/errorcode';

// class RTConnection =========================================================

/**
 * Represents the connection to the real-time framework, used to send
 * message to and receive message from the server.
 *
 * Triggers the following events:
 * - 'update': Update notifications containing actions created by remote
 *  clients, and state information for the application (read-only, name and
 *  identifier of the current editor, etc.).
 *
 * @param {EditApplication} app
 *  The application containing this operations queue.
 *
 * @param {Object} initOptions
 *  optional (!) set of options for initializing instance.
 */
export default class RTConnection extends BaseObject {

    constructor(app, initOptions) {

        // base constructor
        super();

        var // self reference
            self = this,

            // the real-time-2 connection instance
            rt2 = null,

            // the pending operations queue
            sentActionWaitForResponse = [],

            // old real-time client id
            oldClientUID = SessionRestore.state('documents.clientuid') || '',

            // repeated time to check for pending apply_ops responses
            CHECK_PENDING_INTERVAL = 30000,

            // connect/open states
            connectOpenState = 'unknown',

            // close/leave states
            closeLeaveState = 'unknown';

        // private methods ----------------------------------------------------

        function mem_RT2() {
            if (!rt2) {
                rt2 = self.member(DocRTBase.create(app, initOptions));
                SessionRestore.state('documents.clientuid', rt2.getClientUID());
            }
            return rt2;
        }

        /**
         * Provides a RT2 server target url to be used for http requests.
         *
         * @returns {String} a target url to be used for rt2 specific functions.
         */
        function getRT2Url(action, params) {
            var url = ox.apiRoot + '/rt2?action=' + action;
            var params_url = _.map(params, function (value, name) { return name + '=' + value; }).join('&');
            if (_.isString(params_url) && params_url.length > 0) {
                url += '&' + params_url;
            }
            url +=  '&session=' + ox.session;
            return url;
        }

        /**
         * Checks the pending actions waiting for a response
         */
        function checkPendingAcks() {
            var  now = Date.now();
            // state if noUpdateFromServer must be triggered
            var triggerNoUpdate = false;

            if (mem_RT2().isConnected()) {
                _.each(sentActionWaitForResponse, function (entry) {
                    var // the time elapsed since the action has been sent
                        timeSpan = now - entry.timeStamp;

                    if (timeSpan > CHECK_PENDING_INTERVAL) {
                        triggerNoUpdate = true;
                    }
                });

                if (triggerNoUpdate) {
                    self.trigger(RT2Const.EVENT_NORESPONSE_IN_TIME);
                }
            }
        }

        /**
         * Removes the actions data which received a response from the backend
         * side.
         *
         * {String} id
         *  The unique id of the action object which contains the operations which
         *  have been processed by the backend..
         */
        function removeProcessedActions(id) {
            if (_.isString(id) && (id.length > 0)) {

                // filter out all actions which have a map entry
                sentActionWaitForResponse = _.filter(sentActionWaitForResponse, function (waitActionData) {
                    return (id !== waitActionData.id);
                });
            }
        }

        /**
         * Provides the unqiue client id which references
         * this rt connection.
         *
         * @returns {String}
         *  The unqiue client id.
         */
        /* public string */ this.getClientUID = function () {
            return mem_RT2().getClientUID();
        };

        /**
         * Provides the unqiue document id which references
         * the connected document. This unique ID is only valid
         * after the connection has been successfully been established.
         *
         * @returns {String|undefined}
         *  The unqiue document id.
         */
        /* public string */ this.getDocUID = function () {
            return mem_RT2().getDocUID();
        };

        /**
         * Provides the unique channel id which references a single physical
         * connection between client and server which can be used by several
         * document instances.
         *
         * @returns {String}
         *  The channel UID this instance uses or null if there is not active
         *  channel.
         */
        /* public String */ this.getChannelUID = function () {
            return mem_RT2().getChannelUID();
        };

        /**
         * Determines if this instance has a connection to the server or not.
         *
         * @returns {Boolean}
         *  TRUE if there is a connection otherwise FALSE.
         */
        /* public boolean */ this.isConnected = function () {
            return mem_RT2().isConnected();
        };

        /**
         * Provides the current state close/leave of a document connection.
         * Can be one of the following values:
         * 'unknown', 'closing', 'closed', 'leaving', 'left'
         *
         * @returns {String}
         *  The current state of close/leave a document connection.
         */
        /* public string */ this.getCloseLeaveState = function () {
            return closeLeaveState;
        };

        /**
         * Provides the current state open/connect of a document connection.
         * Can be one of the following values:
         * 'unknown', 'opening', 'opened', 'joining', 'joined'
         *
         * @returns {String}
         *  The current state of close/leave a document connection.
         */
        /* public string */ this.getConnectOpenState = function () {
            return connectOpenState;
        };

        /**
         * Connects to the real-time endpoint using the provided unqiue
         * resource id.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the connect results.
         */
        /* public Promise */ this.connect = function () {
            connectOpenState = 'joining';
            rtLogger.log('RTConnection.connect called.');
            rtLogger.log('Client UID: ' + this.getClientUID() + ', document uid: ' + this.getDocUID());
            var promise = mem_RT2().join();
            promise.done(function () { connectOpenState = 'joined'; });
            return promise;
        };

        /**
         * Disconnects this real-time connection with the backend-side.
         * The real-time connection instance won't be able to serve any
         * requests which is bound to a working connection to the
         * backend.
         *
         * @param {Boolean} autoClose
         *  Specifies that the backend should handle this disconnect
         *  gracefully as the client wants to release resources in
         *  an urgent case, e.g. client error.
         *
         * @returns {jQuery.Promise}
         */
        /* public Promise */ this.disconnect = function (autoClose) {
            closeLeaveState = 'disconnecting';
            rtLogger.log('RTConnection.disconnect called, autoClose was', autoClose);
            var promise =  mem_RT2().leave(autoClose);
            promise.done(function () { closeLeaveState = 'disconnected'; });
            return promise;
        };

        /**
         * Initiates the loading of the document, which can be
         * provided by multiple messages.
         *
         * @param {Object} openData
         *  Contains properties to be sent while initiating the
         *  loading of the document associated with the real-time
         *  connection.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the closing document status of
         *  the request.
         */
        /* public Promise */ this.open = function (openData) {
            connectOpenState = 'opening';
            rtLogger.log('RTConnection.open called');
            openData = _.extend({ oldClientUID }, openData);
            var promise = mem_RT2().openDocument(openData);
            promise.done(function () { connectOpenState = 'opened'; });
            return promise;
        };

        /**
         * Requests the server to abort the loading of the document.
         * The request will be ignored if the loading process is not in
         * progress anymore. The client must initiate a closeDocument
         * before a open can be requested again.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the abort request has
         *  been processed.
         */
        /* public Promise */ this.abortOpen = function () {
            rtLogger.log('RTConnection.abortOpen called');
            return mem_RT2().abortOpen();
        };

        /**
         * Close the document at the connected RT object
         *
         * @param {Object|undefined} closeData
         *  The object can have the following properties:
         *  - {Boolean} no_restore
         *  - {Object} recentFileData
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved with the closing document status of
         *  the request.
         */
        /* public Promise */ this.closeDocument = function (closeData) {
            closeLeaveState = 'closing';
            rtLogger.log('RTConnection.closeDocument called');
            var promise =  mem_RT2().closeDocument(closeData);
            promise.done(function () { closeLeaveState = 'closed'; });
            return promise;
        };

        /**
         * Send an emergency leave request to the server with leaveData
         * that can be used by the server to make last-time changes if the
         * client has edit-rights.
         *
         * @param {EmergencyLeaveData} [leaveData]
         *  An optional object with properties to be used by the server to save
         *  additional contents. The following properties are supported:
         *  - {Operation[]} [operations]
         *    Additional JSON document operations to be saved. In concurrent
         *    editing (OT) mode, these operations MUST contain the current
         *    server OSN contained in the property `serverOSN` of this object.
         *  - {number} [serverOSN]
         *    The local OSN used to synchronize server operations in concurrent
         *    editing (OT) mode.
         *
         *  @returns {Boolean}
         *   TRUE if the request could be sent otherwise FALSE.
         */
        /* public boolean */ this.emergencyLeave = function (leaveData) {
            closeLeaveState = 'leaving';

            rtLogger.log('RTConnection.emergencyLeave called');

            var sent = false;
            var params = { client_uid: mem_RT2().getClientUID(), doc_uid: mem_RT2().getDocUID(), channel_uid: mem_RT2().getChannelUID() };
            var url = getRT2Url('emergencyleave', params);

            var payload = JSON.stringify(leaveData || {});

            try {
                var header = { type: 'text/plain; charset=utf-8' };
                payload = new Blob([payload], header);

                sent = navigator.sendBeacon(url, payload);

            } catch {
                rtLogger.log('RTConnection.emergencyLeave - sendBeacon triggered exception');
            }

            closeLeaveState = 'left';
            return sent;
        };

        /**
         * Reset the connection which reset the sequence numbers on both
         * sides (client & backend).
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when reset has been processed
         *  by the backend.
         */
        /* public Promise */ this.reset = function () {
            rtLogger.log('RTconnection.restart called');

            return mem_RT2().reset();
        };

        /**
         * Send the given operations to the connected RT object
         * for further distribution to other connected clients
         *
         * @param {Object} actions
         * The operations to be applied to the backend side. Wrapped into
         * a json object.resendAction
         *
         * @param {Number} [serverOSN]
         * OT: The state of the document, sent by the server
         * Non-OT: undefined
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the ACK of the internal send
         *  arrives.
         */
        /* public Promise */ this.sendActions = function (actions, serverOSN) {
            var id         = uuid.random();
            var actionData = { id, timeStamp: Date.now(), action: actions };

            // TODO: remove the following conversion - client application should provided
            // operations in a single array.
            var operations = [];
            _.each(actions, function (actionObj) {
                if (actionObj && actionObj.operations && actionObj.operations.length > 0) {
                    operations = operations.concat(actionObj.operations);
                }
            });

            var opsPromise = mem_RT2().send(RT2Protocol.REQUEST_APPLY_OPS, {}, { operations, serverOSN });

            // store promise to better analyse possible problems in off-/online scenarios
            actionData.promise = opsPromise;

            sentActionWaitForResponse.push(actionData);
            opsPromise.done(function () {
                removeProcessedActions(id);
            });

            // note: opsPromise.fail is already handled by rtConnection.on(RT2Const.EVENT_ERROR, ....)

            return opsPromise;
        };

        /**
         * Sends user data update to the server. This user update data will
         * broadcasted to all collaborating clients.
         *
         * @param {String} action
         *  Action name string to be handled by the server.
         *
         * @param {Object} userData
         *  An object containing relevant additional user data on the document.
         */
        /* public Promise */ this.updateUserData = SHOW_REMOTE_SELECTIONS ? function (action, userData) {
            rtLogger.log('RTConnection.updateUserData called', userData);
            return mem_RT2().sendAppAction('updateUserData', userData);
        } : function () { };

        /**
         * Sends a previously uploaded image id to the server.
         *
         * @param {String} imageInfo
         *  The unique image ID added to the document.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the answer for addImageId
         *  from the server arrives.
         */
        /* public Promise */ this.addImageId = function (imageInfo) {
            return mem_RT2().sendAppAction('add_image_id_request', { imageInfo });
        };

        /**
         * Sends a rename document request to the server.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the answer for renamehDocument
         *  from the server arrives.
         */
        /* public Promise */ this.renameDocument = function (newFileInfo) {
            // deferred to resolve with filtered backend data
            var deferred = $.Deferred();

            rtLogger.log('RTConnection.renameDocument called');
            mem_RT2().sendAppAction('rename_document', newFileInfo).then(function (data) {
                deferred.resolve(data.body);
            }, function (data) {
                deferred.reject(data.body);
            });

            return deferred.promise();
        };

        /**
         * Sends a sync request to the server. Which should answer with
         * the current document state. The client can check if the osn and
         * other properties to determine the synchronization state.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the answer for renamehDocument
         *  from the server arrives.
         */
        /* public Promise */ this.sync = function () {
            var deferred = $.Deferred();

            rtLogger.log('RTConnection.sync called');

            // TODO: remove conversion functions to provide just the body of the received
            // message.
            mem_RT2().sync({ osn: app.getModel().getOperationStateNumber() }).then(function (data) {
                deferred.resolve((data.body) ? data.body : data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise();
        };

        /**
         * Sends a sync stable request to the server. Which should answer with
         * the current document state and ensures that the client sends
         * messages with seqnr that are missing on the server-side. The client
         * can check if the osn and other properties to determine the synchronization
         * state.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the answer for renamehDocument
         *  from the server arrives.
         */
        /* public Promise */ this.syncStable = function () {
            var deferred = $.Deferred();

            rtLogger.log('RTConnection.syncStable called');

            // TODO: remove conversion functions to provide just the body of the received
            // message.
            mem_RT2().syncStable({ osn: app.getModel().getOperationStateNumber() }).then(function (data) {
                deferred.resolve((data.body) ? data.body : data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise();
        };

        /**
         * Initiates flushing of the document using a synchronous request.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the answer for flushDocument
         *  from the server arrives.
         */
        /* public Promise */ this.flushDocument = function () {
            rtLogger.log('RTConnection.flushDocument called');
            return mem_RT2().send(RT2Protocol.REQUEST_SAVE_DOC, {}, {});
        };

        /**
         * Send our wish to acquire the edit rights to the connected RT object
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the request has been
         *  processed by the backend.
         */
        /* public Promise */ this.acquireEditRights = function () {
            rtLogger.log('RTConnection.acquireedit called');
            return mem_RT2().send(RT2Protocol.REQUEST_EDITRIGHTS, {}, { action: 'request', osn: app.getModel().getOperationStateNumber() });
        };

        /**
         * Acknowledge to the server that our preparation to lose the
         * edit rights are completed.
         *
         * @param {String}
         *  The client UID provided by the server which identifies the
         *  client that requested edit rights and should receive it.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the request has been
         *  processed by the backend.
         */
        /* public Promise */ this.canLoseEditRights = function (clientUID) {
            rtLogger.log('RTConnection.canloseeditrights called');
            return mem_RT2().send(RT2Protocol.REQUEST_EDITRIGHTS, {}, { action: 'approved', value: clientUID, osn: app.getModel().getOperationStateNumber() });
        };

        /**
         * Notifies the server that we decline to provide the edit rights
         * to a certain/all client(s). ATTENTION: This message can only be sent
         * by the editor client.
         *
         * @param {String}
         *  The client UID provided by the server which identifies the
         *  client that requested edit rights and should NOT receive it. Can be
         *  null or undefined which means that all requests will be denied.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the request has been
         *  processed by the backend.
         */
        /* public Promise */ this.declineEditRights = function (clientUID) {
            rtLogger.log('RTConnection.declineeditrights called');
            var value = clientUID || '@ALL';
            return mem_RT2().send(RT2Protocol.REQUEST_EDITRIGHTS, {}, { action: 'declined', value, osn: app.getModel().getOperationStateNumber() });
        };

        /**
         * Notifies the server that we decline to provide the edit rights
         * to a certain/all client(s). ATTENTION: This message can only be sent
         * by the editor client.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the force request has been
         *  processed by the backend.
         */
        /* public Promise */ this.forceEditRights = function () {
            rtLogger.log('RTConnection.forceEditRights called');
            return mem_RT2().send(RT2Protocol.REQUEST_EDITRIGHTS, {}, { action: 'force', osn: app.getModel().getOperationStateNumber() });
        };

        /**
         * Requests the server to create a copy of the current document
         * including the latest changes and store it to a specific folder.
         *
         * @param {Object}
         *  An object containing the properties to create a copy of the
         *  current document in a specific folder and possible conversion
         *  to a template format.
         *
         * @returns {jQuery.Promise}
         *  A promise that will be resolved when the copy request has been
         *  processed.
         */
        /* public Promise */ this.copyDocument = function (data) {
            rtLogger.log('RTConnection.copyDocument called');
            return mem_RT2().sendAppAction('copydocument', data)

                // convert RT2 to RT1 format to be compatible to the following code
                .then(
                    function (data) {
                        return data.body;
                    },
                    function (data) {
                        return $.Deferred().reject(data.body);
                    });
        };

        /**
         * Retrieves the pending actions, not processed by the backend. These
         * actions can be provided to a restoreDocument() request to
         * restore the document.
         *
         * @returns {Object}
         *  The result object contains the property "actions" which is an Array
         *  with all pending operations (without server-side ack).
         */
        this.getPendingActions = function () {
            var // result of the merge of the pending operations
                pendingActions = [];

            if ((_.isArray(sentActionWaitForResponse)) && (sentActionWaitForResponse.length > 0)) {
                // the sentActionWaitForACK contains the actions with additional data:
                // {
                //    id: uuid,
                //    timeStamp: time of putting it into the queue,
                //    action: the ops array
                // }
                _.each(sentActionWaitForResponse, function (element) {
                    // extract the operations from our wrapper object and
                    // put it into the result array
                    pendingActions = pendingActions.concat(element.action);
                });
            }

            return { actions: pendingActions };
        };

        /**
         * Determines, if we have pending actions which have not been acknowledge by
         * the OX Documents backend.
         *
         * @returns {Boolean} TRUE, if there are pending actions, otherwise FALSE.
         */
        this.hasPendingActions = function () {
            return ((_.isArray(sentActionWaitForResponse)) && (sentActionWaitForResponse.length > 0));
        };

        /**
         * Triggers an internal problem via the rt2 framework asynchronously.
         * This is a DEBUG/TESTING function only and must not be enabled in
         * production environment.
         *
         * @param {String} problem
         *  Specifies which problem should be triggered in the rt2 framework.
         */
        this.debugTriggerProblem = REALTIME_DEBUGGING ? function (problem) {
            this.setTimeout(() => {
                switch (problem) {
                    case 'error': this.trigger(RT2Const.EVENT_ERROR, ERROR_UNKNOWN_REALTIME_FAILURE); break;
                    default: mem_RT2().debugTriggerProblem(problem); break;
                }
            }, 0);
        } : $.noop;

        // initialization -----------------------------------------------------

        rtLogger.log('RTConnection initialization');
        this.setInterval(checkPendingAcks, CHECK_PENDING_INTERVAL);

        // forward realtime events to own listeners
        mem_RT2().on(RT2Const.EVENT_RECEIVE, function (response) {
            if (self === null || self === undefined) {
                return;
            }
            //rtLogger.log('forward ' + response.type + ' [' + response.header[RT2Protocol.HEADER_MSG_ID] + ' : ' + response.header[RT2Protocol.HEADER_SEQ_NR] + '] to application ...');
            self.trigger(response.type, response.body);
        });

        // forward RT2 events regarding communication to possible handlers
        for (const eventType of RT2Const.EVENT_CONNECTION_STATE) {
            mem_RT2().on(eventType, function (data) {
                self?.trigger(eventType, data);
            });
        }

        mem_RT2().on(RT2Const.EVENT_ERROR, function (cause) {
            var errorCode = ERROR_UNKNOWN_REALTIME_FAILURE;

            if (self === null || self === undefined) {
                return;
            }

            // make mapping between rt2 errors and application error code
            if (RT2ErrorCode.isErrorCode(cause)) {
                errorCode = { error: cause.getCodeAsStringConstant(), errorClass: cause.getErrorClass() };
            } else if (_.isError(cause)) {
                errorCode = ERROR_UNKNOWN_REALTIME_FAILURE;
            }

            self.trigger(RT2Const.EVENT_ERROR, errorCode);
        });
    }
}
