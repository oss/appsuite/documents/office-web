/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { MethodDecorator, DebounceOptions, EObject } from "@/io.ox/office/tk/objects";
import { debounceMethod } from "@/io.ox/office/tk/objects";

import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// types ======================================================================

interface InstanceType extends EObject {
    readonly docApp: EditApplication;
}

// public functions ===========================================================

/**
 * A decorator factory for instance methods of classes with access to an edit
 * application instance, that debounces the invocations of the method
 * implementation, and defers it until document actions have been processed.
 *
 * The decorated method will wait while the "action processing mode" of the
 * document model is currently active, i.e. while the document model currently
 * applies operation actions (needed e.g. in complex undo or redo operations).
 * If the document does not process any operations, the debounced method
 * behaves like a regular debounced method (see decorator `debounceMethod`).
 *
 * @param [options]
 *  Optional parameters defining the behavior of the debounced method.
 *
 * @returns
 *  The method decorator function.
 */
export function debounceAfterActionsMethod<ThisT extends InstanceType>(options?: DebounceOptions): MethodDecorator<ThisT, void, []> {

    // create a decorator that debounces a method according to the passed options
    const debounceDecorator = debounceMethod<ThisT>(options);

    // return the decorator function that will patch a method descriptor
    return (methodFn, context) => {

        // first, debounce the original method (this replaces the `descriptor.value` property)
        methodFn = debounceDecorator(methodFn, context);

        // a weak cache for every class instance with its own "waiting" flag
        // (`WeakSet` will not prevent objects from being garbage collected)
        const waitingCache = new WeakSet<ThisT>();

        // redefine the instance method
        return function (this: ThisT): void {

            // nothing to do, if the method is already waiting for the operation promise
            if (waitingCache.has(this)) { return; }

            // invoke the method directly, if no operations are applied currently;
            // or if the application is still importing the document
            const { docModel } = this.docApp;
            if (!this.docApp.isImportFinished() || !docModel.isProcessingActions()) {
                methodFn.call(this);
                return;
            }

            // wait for the "action processing mode" to finish
            const promise = docModel.waitForActionsProcessed();
            waitingCache.add(this);

            // invoke the method after operation processing has been finished
            this.onSettled(promise, result => {
                waitingCache.delete(this);
                if (result.fulfilled) { methodFn.call(this); }
            });
        };
    };
}
