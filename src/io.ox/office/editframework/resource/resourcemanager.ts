/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type ResourceConfig, BaseResourceManager } from "@/io.ox/office/baseframework/resource/resourcemanager";
import type { EditApplication } from "@/io.ox/office/editframework/app/editapplication";

// re-exports =================================================================

export type { ResourceConfig };

// constants ==================================================================

/**
 * Shared resources for all application types.
 */
const RESOURCE_MAP = {
    paragraphstylenames: { module: "editframework", name: "paragraphstylenames", merge: true },
    tablestylenames:     { module: "editframework", name: "tablestylenames",     merge: true }
} as const satisfies Dict<ResourceConfig>;

// class EditResourceManager ==================================================

export class EditResourceManager<
    DocAppT extends EditApplication,
    ConfigMapT extends Dict<ResourceConfig>
> extends BaseResourceManager<DocAppT, typeof RESOURCE_MAP & ConfigMapT> {

    protected constructor(docApp: DocAppT, configs: ConfigMapT) {
        super(docApp, { ...RESOURCE_MAP, ...configs });
    }
}
