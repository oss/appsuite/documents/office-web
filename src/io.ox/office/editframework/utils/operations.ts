/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type Relation, is, math } from "@/io.ox/office/tk/algorithms";
import type { DrawingOperation } from "@/io.ox/office/drawinglayer/utils/operations";

const { floor, min } = Math;

// re-exports =================================================================

// names of all drawing operations
export * from "@/io.ox/office/drawinglayer/utils/operations";

// types ======================================================================

/**
 * A logical document position in a JSON operation object (a non-empty array
 * with non-negative integer elements).
 */
export type Position = [number, ...number[]];

/**
 * A generic JSON document operation.
 */
export interface Operation {

    /**
     * The identifier of the document operation.
     */
    name: string;

    /**
     * A target specifier for operations that can address components in
     * different areas of the document.
     */
    target?: string;

    /**
     * Operation sequence number (OSN), a sequence index for all operations of
     * the document.
     */
    osn?: number;

    /**
     * Operation length (OPL), the number of source operations merged into this
     * operation. Default value is `1`.
     */
    opl?: number;

    /**
     * The operation scope. If set to "filter", it will not be applied in the
     * editor clients, but will be processed by the server filter components
     * only.
     */
    scope?: "filter";

    /**
     * Debugging only: A unique identifier for the operation.
     */
    dbg_op_id?: number;

    /**
     * Debugging only: The identifier of the user who generated the operation.
     */
    dbg_user_id?: string;

    /**
     * Debugging only: Whether the operation has been merged with another.
     */
    dbg_merged?: true;
}

/**
 * An operation with mandatory `attrs` property.
 */
export interface AttrsOperation extends Operation {
    attrs: Dict;
}

/**
 * An operation with optional `attrs` property.
 */
export interface OptAttrsOperation extends Operation {
    attrs?: Dict;
}

/**
 * The operation "changeConfig" that initializes or changes global formatting
 * attributes of the document.
 */
export interface ChangeConfigOperation extends AttrsOperation {
    name: typeof CHANGE_CONFIG;
}

/**
 * The operation "insertFont" that sets properties for a specific font.
 */
export interface InsertFontOperation extends AttrsOperation {
    name: typeof INSERT_FONT;
    fontName: string;
}

/**
 * The operation "insertTheme" that defines a design theme with a scheme color
 * palette and other settings.
 */
export interface InsertThemeOperation extends Operation {
    name: typeof INSERT_THEME;
    themeName: string;
    colorScheme: Dict<string>;
    fontScheme: Dict<string>;
    target?: string;
}

/**
 * Base interface for style sheet operations.
 */
export interface StyleSheetOperation extends Operation {
    type: string;
    styleId: string;
}

/**
 * The operation "insertStyleSheet" that creates a new style sheet.
 */
export interface InsertStyleSheetOperation extends StyleSheetOperation, OptAttrsOperation {
    name: typeof INSERT_STYLESHEET;
    styleName?: string;
    parent?: string;
    uiPriority?: number;
    default?: boolean;
    hidden?: boolean;
    custom?: boolean;
}

/**
 * The operation "deleteStyleSheet" that deletes an existing style sheet.
 */
export interface DeleteStyleSheetOperation extends StyleSheetOperation {
    name: typeof DELETE_STYLESHEET;
}

/**
 * The operation "changeStyleSheet" that changes an existing style sheet.
 */
export interface ChangeStyleSheetOperation extends StyleSheetOperation, OptAttrsOperation {
    name: typeof CHANGE_STYLESHEET;
    styleName?: string;
    parent?: string;
}

/**
 * Base interface for auto-style operations.
 */
export interface AutoStyleOperation extends Operation {
    type?: string;
    styleId: string;
    otIndexShift?: boolean;
}

/**
 * The operation "insertAutoStyle" that creates a new auto-style.
 */
export interface InsertAutoStyleOperation extends AutoStyleOperation, OptAttrsOperation {
    name: typeof INSERT_AUTOSTYLE;
    default?: boolean;
}

/**
 * The operation "deleteAutoStyle" that deletes an existing auto-style.
 */
export interface DeleteAutoStyleOperation extends AutoStyleOperation {
    name: typeof DELETE_AUTOSTYLE;
}

/**
 * The operation "changeAutoStyle" that changes an existing auto-style.
 */
export interface ChangeAutoStyleOperation extends AutoStyleOperation, OptAttrsOperation {
    name: typeof CHANGE_AUTOSTYLE;
}

/**
 * A document operation with a `start` property containing a document postiton.
 */
export interface PositionOperation extends DrawingOperation {
    name: typeof POSITION;
}

/**
 * The operation "noop" that is used for client communication.
 */
export interface NoOpOperation extends Operation {

    /**
     * The identifier of the noop-operation.
     */
    name: typeof NOOP;

    /**
     * Data information for noOp-operations.
     */
    data?: unknown;
}

/**
 * Helper interface for operations with free access to all properties.
 */
export interface AnyOperation extends Dict, Operation { }

// constants ==================================================================

/**
 * An operation that does nothing (internal usage).
 */
export const NOOP = "noOp";

/**
 * The initial operation while importing a document, sets global attributes of
 * the model, including default formatting attributes.
 */
export const CHANGE_CONFIG = "setDocumentAttributes";

/**
 * An operation that inserts a new theme into the theme collection.
 */
export const INSERT_THEME = "insertTheme";

/**
 * An operation that inserts a new font description into the font collection.
 */
export const INSERT_FONT = "insertFontDescription";

/**
 * An operation that inserts a new style sheet into the document.
 */
export const INSERT_STYLESHEET = "insertStyleSheet";

/**
 * An operation that removes an existing style sheet from the document.
 */
export const DELETE_STYLESHEET = "deleteStyleSheet";

/**
 * An operation that changes the properties of an existing style sheet in the
 * document.
 */
export const CHANGE_STYLESHEET = "changeStyleSheet";

/**
 * An operation that inserts a new auto-style into the document.
 */
export const INSERT_AUTOSTYLE = "insertAutoStyle";

/**
 * An operation that changes the formatting attributes of an existing
 * auto-style in the document.
 */
export const CHANGE_AUTOSTYLE = "changeAutoStyle";

/**
 * An operation that deletes an existing auto-style from the document.
 */
export const DELETE_AUTOSTYLE = "deleteAutoStyle";

/**
 * An operation that carries a single document position (internal usage).
 */
export const POSITION = "position";

// public functions ===========================================================

/**
 * Returns whether the passed array is a valid document position.
 *
 * @param value
 *  The array value to be tested.
 *
 * @returns
 *  Whether the passed array is a valid document position.
 */
export function isPosition(value: unknown): value is Position {
    return is.array(value) && (value.length > 0) && value.every(elem => Number.isFinite(elem) && (elem as number >= 0) && (elem === floor(elem as number)));
}

/**
 * Returns a clone of the passed JSON document position.
 *
 * @param position
 *  The document position to be cloned.
 *
 * @returns
 *  The clone of the passed document position.
 */
export function clonePosition(position: Position): Position {
    return position.slice() as Position;
}

/**
 * Returns a deep clone of the passed array of JSON document positions.
 *
 * @param positions
 *  The document positions to be cloned.
 *
 * @returns
 *  The deep clone of the passed document positions.
 */
export function clonePositions(positions: Position[]): Position[] {
    return positions.map(clonePosition);
}

/**
 * Compares the elements of two JSON document positions.
 *
 * @param pos1
 *  The first document positions that will be compared to the second.
 *
 * @param pos2
 *  The second document positions that will be compared to the first.
 *
 * @param [maxLen]
 *  If specified, compares the specified number of leading elements in the
 *  passed positions. Otherwise, the entire positions will be compared.
 *
 * @returns
 *  Whether all array elements (or the specified number of array elements) are
 *  equal.
 */
export function equalPositions(pos1: Position, pos2: Position, maxLen?: number): boolean {

    // references to the same array
    if (pos1 === pos2) { return true; }

    // number of elements in the arrays to be compared
    const limited = is.number(maxLen);
    const len1 = limited ? min(pos1.length, maxLen) : pos1.length;
    const len2 = limited ? min(pos2.length, maxLen) : pos2.length;

    // minimum length of both arrays
    if (len1 !== len2) { return false; }
    const len = min(len1, len2);

    // compare all array elements
    for (let ai = 0; ai < len; ai += 1) {
        if (pos1[ai] !== pos2[ai]) { return false; }
    }

    // all compared elements are equal
    return true;
}

/**
 * Compares two JSON document positions lexicographically (starting with the
 * first element of both arrays, and visiting the following elements until a
 * pair of elements is different).
 *
 * @param pos1
 *  The first document positions that will be compared to the second.
 *
 * @param pos2
 *  The second document positions that will be compared to the first.
 *
 * @param [maxLen]
 *  If specified, compares the specified number of leading elements in the
 *  document positions. Otherwise, the entire positions will be compared.
 *
 * @returns
 *  The relation between the two document positions.
 */
export function comparePositions(pos1: Position, pos2: Position, maxLen?: number): Relation {

    // references to the same array
    if (pos1 === pos2) { return 0; }

    // number of elements in the arrays to be compared
    const limited = is.number(maxLen);
    const len1 = limited ? min(pos1.length, maxLen) : pos1.length;
    const len2 = limited ? min(pos2.length, maxLen) : pos2.length;
    // minimum length of both arrays
    const len = min(len1, len2);

    // compare all array elements
    for (let ai = 0; ai < len; ai += 1) {
        const rel = math.compare(pos1[ai], pos2[ai]);
        if (rel) { return rel; }
    }

    // all compared elements are equal: compare array lengths (restricted to the passed maximum length)
    return math.compare(len1, len2);
}

/**
 * Creates a JSON operation object.
 *
 * @param name
 *  The name of the operation.
 *
 * @param props
 *  Additional properties to be inserted into the operation.
 *
 * @returns
 *  The JSON operation object.
 */
export function makeOp(name: string, props?: Dict): Operation {
    return { name, ...props };
}
