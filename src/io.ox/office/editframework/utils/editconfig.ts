/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun, pick } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE } from "@/io.ox/office/tk/dom";
import { sendRequest } from "@/io.ox/office/tk/utils/io";
import { Logger, LogLevel } from "@/io.ox/office/tk/utils/logger";
import { DEBUG_ON_DEMAND, getFlag, getInt, getDebugFlag } from "@/io.ox/office/baseframework/utils/baseconfig";

const { min } = Math;

// re-exports =================================================================

export * from "@/io.ox/office/baseframework/utils/baseconfig";

// types ======================================================================

/**
 * Enumeration for different file types a document can be saved to.
 */
export enum SaveAsType {

    /**
     * Save document in its native file format.
     */
    NATIVE = "native",

    /**
     * Save a template document in the native file format.
     */
    TEMPLATE = "template",

    /**
     * Create a PDF file for the document.
     */
    PDF = "pdf"
}

// constants ==================================================================

/**
 * Specifies whether the local browser storage will be used for saving
 * documents.
 */
export const USE_LOCAL_STORAGE = getFlag("module/useLocalStorage", true);

/**
 * Specifies whether the current selection of remote users will be displayed in
 * all OX Documents edit applications.
 */
export const SHOW_REMOTE_SELECTIONS = getFlag("module/showRemoteSelections", true);

/**
 * Specifies whether the "document name" edit field should be disabled. This
 * ensures that environments which don't support transparent changes of the
 * file name don't break an active RT connection.
 */
export const RENAME_DISABLED = getFlag("module/renamedisabled", false);

/**
 * Specifies whether the application should support to restore a document.
 */
export const RESTORE_DOCUMENT_ENABLED = getFlag("module/documentRestoreEnabled", true);

/**
 * Specifies whether the application should load rescue documents in read-only
 * mode.
 */
export const RESCUE_DOCUMENT_READONLY = getFlag("module/rescueDocumentReadOnly", true);

/**
 * Specifies whether the contents of the toppane (tab buttons, undo/redo
 * controls, etc.) and the toolbars of the main toolpane are combined in a
 * single view pane element (used on small devices).
 */
export const COMBINED_TOOL_PANES = getDebugFlag("office:panes-combined") || (TOUCH_DEVICE && (min(screen.width, screen.height) <= 610)); // Nexus 7 = min resolution 601

/**
 * Specifies how long to wait for an answer when transferring edit rights,
 * before automatically accepting it, in milliseconds.
 */
export const TRANSFER_EDIT_RIGHTS_TIMEOUT = getInt("module/switchingEditRightsTimeout", 20) * 1_000; // configuration contains seconds

/**
 * Specifies whether realtime debugging is enabled in the server configuration.
 */
export const REALTIME_DEBUGGING = getFlag("module/realtimedebugging");

/**
 * Specifies whether realtime debugging is enabled in the server configuration.
 */
export const MENTIONS_ENABLED = getFlag("module/mentionsEnabled", true);

// debug loggers ==============================================================

/**
 * A logger intended to be used for realtime communication. Bound to the debug
 * configuration option "office:log-realtime".
 */
export const rtLogger = new Logger("office:log-realtime", {
    enabled: DEBUG_ON_DEMAND || REALTIME_DEBUGGING,
    defLogLevel: LogLevel.TRACE,
    recordAll: true,
    tag: "RT",
    tagColor: 0xFF0000
});

/**
 * A logger for "Search and replace" operations.
 */
export const searchLogger = new Logger("office:log-search", {
    tag: "SEARCH",
    tagColor: 0x00FF00
});

// public functions ===========================================================

/**
 * Retrieves the locale identifiers with a dictionary available for spell
 * checking.
 *
 * @returns
 *  A promise that will fulfil with an array of strings containing the locale
 * identifiers with a dictionary available for spell checking.
 */
export const getLocalesWithDictionary = fun.once((): JPromise<unknown[]> => {
    const req = sendRequest("spellchecker", { action: "supportedlocales" }, { method: "POST" });
    return req.then(data => pick.array(data, "SupportedLocales", true), () => []);
});
