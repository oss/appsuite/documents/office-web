/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { pick } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

export enum LineHeightType {
    NORMAL = "normal",
    PERCENT = "percent",
    FIXED = "fixed",
    LEADING = "leading",
    AT_LEAST = "atLeast"
}

/**
 * JSON value type for the formatting attribute `lineHeight` for paragraphs.
 */
export interface OpLineHeight {
    type: LineHeightType;
    value?: number;
}

// class LineHeight ===========================================================

export const LineHeight = {

    NORMAL: { type: LineHeightType.NORMAL } as const as OpLineHeight,

    SINGLE: { type: LineHeightType.PERCENT, value: 100 } as const as OpLineHeight,

    _115: { type: LineHeightType.PERCENT, value: 115 } as const as OpLineHeight,

    ONE_HALF: { type: LineHeightType.PERCENT, value: 150 } as const as OpLineHeight,

    DOUBLE: { type: LineHeightType.PERCENT, value: 200 } as const as OpLineHeight,

    serialize(lineHeight: OpLineHeight): string {
        const value = lineHeight.value ?? 0;
        switch (lineHeight.type) {
            case LineHeightType.NORMAL:   return "normal";
            case LineHeightType.PERCENT:  return `${value}%`;
            case LineHeightType.FIXED:    return `${value}hmm`;
            default:                      return `${lineHeight.type}:${value}hmm`;
        }
    },

    parse(data: unknown): OpLineHeight {
        const type = pick.enum(data, "type", LineHeightType, LineHeightType.NORMAL);
        return (type === LineHeightType.NORMAL) ? { type } : { type, value: pick.number(data, "value", 0) };
    }
};
