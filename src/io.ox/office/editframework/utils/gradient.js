/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is, math } from '@/io.ox/office/tk/algorithms';
import { getArrayOption, getBooleanOption, getNumberOption, getObjectOption, getStringOption } from '@/io.ox/office/tk/utils';

import { getExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { Color } from '@/io.ox/office/editframework/utils/color';

// locally scoped helper methods and object shortcuts =====================

var
    NOOP            = Function.prototype,

    expose_internal_class_name  = Object.prototype.toString,
    regXObjectObject            = (/^\[object\s+Object]$/),

    isObjectObject  = function (type) {
        return (!!type && regXObjectObject.test(expose_internal_class_name.call(type)));
    },

    PI_180 = Math.PI / 180,

    math_radix      = function getRadix(angleDegree) {
        return angleDegree * PI_180;
    },
    math_degree     = function getRadix(angleRadix) {
        return angleRadix / PI_180;
    },

    IMAGE_BLUEPRINT = document.createElement('img'),

    CANVAS_ELEMENT  = document.createElement('canvas'),
    CANVAS_CONTEXT  = CANVAS_ELEMENT.getContext('2d'),

    GRADIENT_FILLTYPE_OPERATIONS  = {
        linear_and_bound_false:       getGradientFillLinearUnbound,
        linear_and_bound_true:        getGradientFillLinearBound,

        path_circle_and_bound_false:  getGradientFillRadialUnbound,
        path_circle_and_bound_true:   getGradientFillRadialBound,

        path_rect_and_bound_false:    getGradientFillRectangularUnbound,
        path_rect_and_bound_true:     getGradientFillRectangularBound,

        path_shape_and_bound_false:   getGradientFillShapeUnbound,
        path_shape_and_bound_true:    getGradientFillShapeBound
    },
    LITERAL_PREFIX__PATH_TYPE     =   'path_',
    LITERAL_BRIDGE__AND_BOUND     =   '_and_bound_';

// private static (locally scoped) list of gradient references ============

// var gradientList = []; // @NOTIFICATION: for debugging only.

// private static (locally scoped) helper class and class methods =========

var GradientColor = (function (/*Object, Math*/) {

    function alphaChannelSubtraction(minuendValue, subtrahendValue) {
        return (minuendValue.a - subtrahendValue.a);
    }

    function colorValueSubtraction(minuendValue, subtrahendValue) {
        return {
            r: (minuendValue.r - subtrahendValue.r),
            g: (minuendValue.g - subtrahendValue.g),
            b: (minuendValue.b - subtrahendValue.b)
        };
    }
    function colorValueSummation(c1Value, c2Value) {
        return {
            r: (c1Value.r + c2Value.r),
            g: (c1Value.g + c2Value.g),
            b: (c1Value.b + c2Value.b)
        };
    }

    function colorValueChannelShifting(colorValue, percentageValue) {
        percentageValue = (Math.max(0, Math.min(100, percentageValue)) / 100);
        return {
            r: Math.round(colorValue.r * percentageValue),
            g: Math.round(colorValue.g * percentageValue),
            b: Math.round(colorValue.b * percentageValue)
        };
    }

    function parseBrightnessFromGradientColorValue(colorValue) {
        return Math.round(Math.sqrt(

            (colorValue.r * colorValue.r * 0.241) +
            (colorValue.g * colorValue.g * 0.691) +
            (colorValue.b * colorValue.b * 0.068)

        ) * colorValue.a);
    }

    function createGradientColorValueFromDescriptor(descriptorOrValue) {
        var value = {},
            alpha = descriptorOrValue.a,

            bytes = descriptorOrValue.bytes;

        alpha = (is.number(alpha) ? Math.max(0, Math.min(1, alpha)) : 1);
        // console.log('createGradientColorValueFromDescriptor :: alpha : ', alpha);

        if (bytes) {
            value.r = bytes[0];
            value.g = bytes[1];
            value.b = bytes[2];
        } else {
            value.r = descriptorOrValue.r;
            value.g = descriptorOrValue.g;
            value.b = descriptorOrValue.b;
        }
        value.a = alpha;

        return value;
    }

    function GradientColor(descriptor) {
        var colorValue = createGradientColorValueFromDescriptor(descriptor);

        this.compile = function (compileValue) {
            compileValue = createGradientColorValueFromDescriptor(compileValue);

            colorValue.r = compileValue.r;
            colorValue.g = compileValue.g;
            colorValue.b = compileValue.b;
            colorValue.a = compileValue.a;

            // object_extend(colorValue, createGradientColorValueFromDescriptor(compileValue));

            return this;
        };

        this.valueOf = function () {
            return { r: colorValue.r, g: colorValue.g, b: colorValue.b, a: colorValue.a };
        };
        this.toString = function () {
            return ['{ r: ', colorValue.r, ', g: ', colorValue.g, ', b: ', colorValue.b, ', a: ', colorValue.a, ' }'].join('');
        };

        this.parseBrightness = function () {
            return parseBrightnessFromGradientColorValue(colorValue);
        };

        this.deltaAlpha = function (subtrahendValue) {
            return alphaChannelSubtraction(colorValue, subtrahendValue.valueOf());
        };

        this.subtract = function (subtrahendValue) {
            return this.compile(colorValueSubtraction(colorValue, subtrahendValue.valueOf()));
        };
        // this.getDifference = function (subtrahendValue) {
        //     return (new GradientColor(colorValueSubtraction(colorValue, subtrahendValue.valueOf())));
        // };

        this.add = function (addValue) {
            return this.compile(colorValueSummation(colorValue, addValue.valueOf()));
        };
        // this.getSum = function (addValue) {
        //     return (new GradientColor(colorValueSummation(colorValue, addValue.valueOf())));
        // };

        this.shiftChannels = function (percentageValue) {
            return this.compile(colorValueChannelShifting(colorValue, percentageValue));
        };

        return this;
    }
    // GradientColor.prototype[ ... ]

    return GradientColor;

}(/*Object, Math*/));

// Microsoft specific color-stop list computation =========================

function isNeedsMicrosoftSpecificGradientCalculation(colorStopList) {
    return (
        (colorStopList.length === 2)
        && ((colorStopList[0].position === 0) && (colorStopList[1].position === 1))
        && (colorStopList[0].color !== colorStopList[1].color)
    );
}

function applyMSSpecificColorStopListPatch(colorStopList) {
    var
        patchedList = [],

        colorStopStartValue = colorStopList[0],
        colorStopEndValue   = colorStopList[1],

        gradientStartColor  = (new GradientColor(colorStopStartValue.colorDescriptor)),
        gradientEndColor    = ((new GradientColor(colorStopEndValue.colorDescriptor))),

        alphaReference      = gradientStartColor.valueOf().a,
        alphaDelta          = gradientEndColor.deltaAlpha(gradientStartColor);

    // please do not turn this function into a closure ... a plain function declaration donates most to an optimal JIT compiler result.
    function getShiftedAlpha(reference, delta, shift) {
        return Number((reference + (delta * shift)).toPrecision(2));
    }

    /**
     *  The Microsoft specific gradient for a full-range (0% - 100%),
     *  2-values colorstop-list seems to always stretch a gradient's
     *  scale much more in favor of it's colors of higher brightness.
     *
     *  The next steps take this into account and temporarily patch
     *  such a gradients appearance/behavior on the fly.
     */
    if (gradientStartColor.parseBrightness() < gradientEndColor.parseBrightness()) {

        // stepwise calculate from 100% to 0% and unshift results into list.
        patchedList = [{

            position: 0.9,
            color: getGradientColorString(gradientEndColor.valueOf(), getShiftedAlpha(alphaReference, alphaDelta, 0.9))

        }, colorStopEndValue];

        // 70% color value of original full color range positioned to 50%.
        gradientEndColor = (gradientEndColor).subtract(gradientStartColor).shiftChannels(70).add(gradientStartColor);
        patchedList.unshift({
            position: 0.5,
            color: getGradientColorString(gradientEndColor.valueOf(), getShiftedAlpha(alphaReference, alphaDelta, 0.5))
        });

        // 30% color value of new color stopped range (50% positioning * 60% channel shifting: .5 * .6 = .3) positioned to 20%.
        gradientEndColor = (gradientEndColor).subtract(gradientStartColor).shiftChannels(60).add(gradientStartColor);
        patchedList.unshift({
            position: 0.25,
            color: getGradientColorString(gradientEndColor.valueOf(), getShiftedAlpha(alphaReference, alphaDelta, 0.25))
        });

        patchedList.unshift(colorStopStartValue);

    } else {
        // inverted stepwise calculation (from 0% to 100% with switched operands) and push results into list.

        patchedList = [
            colorStopStartValue, {

                position: 0.1,
                color: getGradientColorString(gradientStartColor.valueOf(), getShiftedAlpha(alphaReference, alphaDelta, 0.1))
            }
        ];

        // 70% color value of original full color range positioned to 50%.
        gradientStartColor = (gradientStartColor).subtract(gradientEndColor).shiftChannels(70).add(gradientEndColor);
        patchedList.push({
            position: 0.5,
            color: getGradientColorString(gradientStartColor.valueOf(), getShiftedAlpha(alphaReference, alphaDelta, 0.5))
        });

        // 30% color value of new color stopped range (50% positioning * 60% channel shifting: .5 * .6 = .3) positioned to 20%.
        gradientStartColor = (gradientStartColor).subtract(gradientEndColor).shiftChannels(60).add(gradientEndColor);
        patchedList.push({
            position: 0.75,
            color: getGradientColorString(gradientStartColor.valueOf(), getShiftedAlpha(alphaReference, alphaDelta, 0.75))
        });

        patchedList.push(colorStopEndValue);
    }
    return patchedList;
}

function getPatchedColorStopListIfNeeded(colorStopList) {
    return ((isNeedsMicrosoftSpecificGradientCalculation(colorStopList) && applyMSSpecificColorStopListPatch(colorStopList)) || colorStopList);
}

// canvas helpers =========================================================

function returnCleanSketchSheet(width, height) {
    CANVAS_ELEMENT.width  = width;
    CANVAS_ELEMENT.height = height;
    //CANVAS_ELEMENT.style.width  = (width  + 'px');
    //CANVAS_ELEMENT.style.height = (height + 'px');

    //CANVAS_CONTEXT = CANVAS_ELEMENT.getContext('2d');
    CANVAS_CONTEXT.clearRect(0, 0, width, height);

    return {
        canvas:   CANVAS_ELEMENT,
        context:  CANVAS_CONTEXT
    };
}

function createCleanSketchSheetClone(width, height) {
    var
        context,
        canvas = CANVAS_ELEMENT.cloneNode();

    canvas.width  = width   || (width   = CANVAS_ELEMENT.width);
    canvas.height = height  || (height  = CANVAS_ELEMENT.height);

    context = canvas.getContext('2d');
    context.clearRect(0, 0, width, height);

    return {
        canvas,
        context
    };
}

// function createImageElementFromCanvas/*Context*/(canvas, width, height) {
//     var
//       //imageData   = context.getImageData(0, 0, width, height),
//       //elmImage    = document.createElement('img');
//
//         elmImage    = IMAGE_BLUEPRINT.cloneNode();
//
//     elmImage.width  = /*imageData.*/width || canvas.width;
//     elmImage.height = /*imageData.*/height || canvas.height;
//
//     elmImage.src    = canvas.toDataURL();
//   //elmImage.src    = ['data:', imageData.data].join('');
//
//     return elmImage;
// }

/**
 * @returns {CanvasPattern}
 */
function createPatternFillFromCanvas(sketchCanvas, drawingContext) {

    return drawingContext.createPattern(sketchCanvas, 'no-repeat');
}

// gradient computation functionality =====================================

/**
 * @returns {CanvasPattern}
 */
function renderRotatedPatternFromGradientFill(context, gradientFill, rotation, boxMeasures) {
    var
        canvas          = context.canvas,

        fillRextWidth   = canvas.width,
        fillRextHeight  = canvas.height,
        halfRectWidth   = Math.round(fillRextWidth / 2),
        halfRectHeight  = Math.round(fillRextHeight / 2),

        snapBoxWidth    = boxMeasures.width,
        snapBoxHeight   = boxMeasures.height,
        halfBoxWidth    = Math.round(snapBoxWidth / 2),
        halfBoxHeight   = Math.round(snapBoxHeight / 2),

        // translateX      = fillRextWidth - boxMeasures.mx,
        // translateY      = fillRextHeight - boxMeasures.my,

        bitmapSheet     = createCleanSketchSheetClone(snapBoxWidth, snapBoxHeight),
        bitmapCanvas    = bitmapSheet.canvas,
        bitmapContext   = bitmapSheet.context,

        sketchSheet     = returnCleanSketchSheet(fillRextWidth, fillRextHeight),
        sketchCanvas    = sketchSheet.canvas,
        sketchContext   = sketchSheet.context;

    sketchContext.setTransform(1, 0, 0, 1, 0, 0);

    bitmapContext.fillStyle = gradientFill;
    bitmapContext.fillRect(0, 0, snapBoxWidth, snapBoxHeight);

    sketchContext.translate(halfRectWidth, halfRectHeight);

    //sketchContext.translate((halfRectWidth + boxMeasures.x1), (halfRectHeight - boxMeasures.y1));
    //sketchContext.translate(translateX, translateY);

    sketchContext.rotate(math_radix(rotation));
    //sketchContext.translate(Math.round(boxMeasures.x1 / 2), -Math.round(boxMeasures.y1 / 2));

    sketchContext.drawImage(bitmapCanvas, -halfBoxWidth, -halfBoxHeight, snapBoxWidth, snapBoxHeight);

    sketchContext.setTransform(1, 0, 0, 1, 0, 0);
    //sketchContext.rotate(-math_radix(rotation));
    //sketchContext.translate(-halfRectWidth, -halfRectHeight);

    //sketchContext.translate(-translateX, -translateY);
    //sketchContext.translate((0 - boxMeasures.mx), (0 - boxMeasures.my));

    // // debug
    // //
    // var
    //     dumpCanvas  = document.createElement('canvas'),
    //     dumpContext;
    //
    // dumpCanvas.width  = boxMeasures.width;
    // dumpCanvas.height = boxMeasures.height;
    //
    // dumpContext = dumpCanvas.getContext('2d');
    // dumpContext.fillStyle = gradientFill;
    // dumpContext.fillRect(0, 0, boxMeasures.width, boxMeasures.height);
    //
    // document.body.appendChild(dumpCanvas);
    // dumpCanvas.style.position = 'absolute';
    // //
    // // debug

    return createPatternFillFromCanvas(sketchCanvas, context);
}

function getFillPathMeasures(rectWidth, rectHeight, gradientValue) {
    var
        x1 = Math.round(gradientValue.pathFillLeft  * rectWidth),
        y1 = Math.round(gradientValue.pathFillTop   * rectHeight),

        x2 = Math.round(rectWidth   - (gradientValue.pathFillRight  * rectWidth)),
        y2 = Math.round(rectHeight  - (gradientValue.pathFillBottom * rectHeight));

    return {
        x1,
        y1,
        x2,
        y2,
        width:  Math.abs(x2 - x1),
        height: Math.abs(y2 - y1)
    };
}

function getTileRectMeasures(rectWidth, rectHeight, gradientValue) {
    var
        x1 = Math.round(gradientValue.left  * rectWidth),
        y1 = Math.round(gradientValue.top   * rectHeight),

        x2 = Math.round(rectWidth   - (gradientValue.right  * rectWidth)),
        y2 = Math.round(rectHeight  - (gradientValue.bottom * rectHeight));

    return {
        x1,
        y1,
        x2,
        y2,
        width:  Math.abs(x2 - x1),
        height: Math.abs(y2 - y1)
    };
}

function getSnapBoxMeasuresForRotatedShape(pathCoords, rotation) {
    var
        mx  = pathCoords.mx,
        my  = pathCoords.my,

        deltaY, // a - cathetus
        deltaX, // b - cathetus
        radius, // c - hypotenuse

        radixA, // alpha angle (in radix)
        q,      // ordinal number of the Cartesian quadrant.

        xMin, yMin, xMax, yMax;

    // calculate rotated values for each {x, y} coordinate tuple.
    pathCoords.xValues.forEach(function (x, idx/*, pathCoords.xValues*/) {
        var
            y = pathCoords.yValues[idx];

        deltaY = my - y;
        deltaX = mx - x;
        radius = math.radius(deltaX, deltaY);

        q = (deltaX >= 0) ? ((deltaY >= 0) ? 2 : 3) : ((deltaY >= 0) ? 1 : 4);

        deltaY = Math.abs(deltaY);
        deltaX = Math.abs(deltaX);

        radixA = (deltaY === 0) ? 0 : Math.atan(deltaY / deltaX);

        if (q === 2) {
            radixA = math_radix(180) - radixA;
        } else if (q === 3) {
            radixA = math_radix(180) + radixA;
        } else if (q === 4) {
            radixA = math_radix(360) - radixA;
        }
        radixA = math_radix((math_degree(radixA) - rotation + 360) % 360);

        pathCoords.xValues[idx] = mx + Math.round(Math.cos(radixA) * radius);
        pathCoords.yValues[idx] = my + Math.round(Math.sin(radixA) * radius);
    });

    xMin = Math.min(...pathCoords.xValues);
    yMin = Math.min(...pathCoords.yValues);

    xMax = Math.max(...pathCoords.xValues);
    yMax = Math.max(...pathCoords.yValues);

    return {
        x1: xMin,
        y1: yMin,

        x2: xMax,
        y2: yMax,

        mx: (xMax - Math.round((xMax - xMin) / 2)),
        my: (yMax - Math.round((yMax - yMin) / 2)),

        width:  (xMax - xMin),
        height: (yMax - yMin)
    };
}

/**
 * @returns {Size}
 */
function getBoundingBoxMeasuresForRotatedDrawingRect(rectWidth, rectHeight, rotation) {
    var
        radius = math.radius(rectWidth, rectHeight) / 2,

        // adjusting the values to a system different from the Cartesian one.
        radixD = math_radix(((90  + 45 - rotation) + 360) % 360),
        radixC = math_radix(((0   + 45 - rotation) + 360) % 360),
        radixB = math_radix(((270 + 45 - rotation) + 360) % 360),
        radixA = math_radix(((180 + 45 - rotation) + 360) % 360),

        cornerPointList = [{
            // corner D
            x: Math.round(Math.cos(radixD) * radius),
            y: Math.round(Math.sin(radixD) * radius)
        }, {
            // corner C
            x: Math.round(Math.cos(radixC) * radius),
            y: Math.round(Math.sin(radixC) * radius)
        }, {
            // corner B
            x: Math.round(Math.cos(radixB) * radius),
            y: Math.round(Math.sin(radixB) * radius)
        }, {
            // corner A
            x: Math.round(Math.cos(radixA) * radius),
            y: Math.round(Math.sin(radixA) * radius)
        }],
        cornerPointExtrema = cornerPointList.reduce(function (collector, point) {

            collector.xMin = (collector.xMin === null) ? point.x : Math.min(collector.xMin, point.x);
            collector.xMax = (collector.xMax === null) ? point.x : Math.max(collector.xMax, point.x);
            collector.yMin = (collector.yMin === null) ? point.y : Math.min(collector.yMin, point.y);
            collector.yMax = (collector.yMax === null) ? point.y : Math.max(collector.yMax, point.y);

            return collector;

        }, { xMin: null, xMax: null, yMin: null, yMax: null });

    return {
        width: (Math.abs(cornerPointExtrema.xMax) + Math.abs(cornerPointExtrema.xMin)),
        height: (Math.abs(cornerPointExtrema.yMax) + Math.abs(cornerPointExtrema.yMin))
    };
}

/**
 * @returns {{x1: *, y1: *, x2: *, y2: *}}
 */
function getLinearGradientCoordinates(rectWidth, rectHeight, rotation) {
    var
        // gradient path computation seems to be done strictly within the boundaries
        // of a rectangular box that encloses its shape/drawing.
        // the rectangle's geometric centre most probably is the zero point of a
        // strangely flipped Cartesian coordinate system.
        //
        // - the assumption did prove right.
        // - all test documents render according to theirs powerpoint pendants and
        //   do pass the document roundtrip as well.
        //
        x1, y1, x2, y2,

        angle,
        hypotenuse,
        oppositeLeg,

        partialSquareDegree = (rotation % 90),
        boundaryAngleDegree = Math.atan(rectHeight / rectWidth);

    angle = math_radix(partialSquareDegree);

    if ((rotation >= 0) && (rotation <  90)) {
        if (rotation < boundaryAngleDegree) {

            x1 = 0;
            y1 = 0;
            x2 = Math.round(rectWidth / Math.cos(angle));
            y2 = Math.round((rectWidth * Math.tan(angle)) + ((x2 - rectWidth) * Math.tan(angle)));

        } else {
            angle       = math_radix(90 - partialSquareDegree);
            hypotenuse  = (rectWidth - (rectHeight * Math.tan(angle)));
            oppositeLeg = (hypotenuse * Math.sin(angle));

            x2 = rectWidth;
            y2 = rectHeight;
            x1 = Math.round(hypotenuse - (oppositeLeg * Math.sin(angle)));
            y1 = (0 - Math.round(oppositeLeg * Math.cos(angle)));
        }
    } else if ((rotation >= 90) && (rotation <  180)) {
        if (rotation < boundaryAngleDegree) {

            x1 = rectWidth;
            y1 = 0;
            y2 = Math.round(rectHeight / Math.cos(angle));
            x2 = Math.round((rectHeight * Math.tan(angle)) + ((y2 - rectHeight) * Math.tan(angle)));

        } else {
            angle       = math_radix(90 - partialSquareDegree);
            hypotenuse  = (rectHeight - (rectWidth * Math.tan(angle)));
            oppositeLeg = (hypotenuse * Math.sin(angle));

            x2 = 0;
            y2 = rectHeight;
            y1 = Math.round(hypotenuse - (oppositeLeg * Math.sin(angle)));
            x1 = (rectWidth + Math.round(oppositeLeg * Math.cos(angle)));
        }
    } else if ((rotation >= 180) && (rotation < 270)) {
        if (rotation < boundaryAngleDegree) {

            x1 = rectWidth;
            y1 = rectHeight;
            x2 = (rectWidth - Math.round(rectWidth / Math.cos(angle)));
            y2 = (rectHeight - Math.round((rectWidth * Math.tan(angle)) + ((x2 - rectWidth) * Math.tan(angle))));

        } else {
            angle       = math_radix(90 - partialSquareDegree);
            hypotenuse  = (rectWidth - (rectHeight * Math.tan(angle)));
            oppositeLeg = (hypotenuse * Math.sin(angle));

            x2 = 0;
            y2 = 0;
            x1 = (rectWidth - Math.round(hypotenuse - (oppositeLeg * Math.sin(angle))));
            y1 = (rectHeight + Math.round(oppositeLeg * Math.cos(angle)));
        }
    } else {
        if (rotation < boundaryAngleDegree) {

            x1 = 0;
            y1 = rectHeight;
            y2 = Math.round(rectHeight / Math.cos(angle));
            x2 = Math.round((rectHeight * Math.tan(angle)) + ((y2 - rectHeight) * Math.tan(angle)));

        } else {
            angle       = math_radix(90 - partialSquareDegree);
            hypotenuse  = (rectHeight - (rectWidth * Math.tan(angle)));
            oppositeLeg = (hypotenuse * Math.sin(angle));

            x2 = rectWidth;
            y2 = 0;
            y1 = (rectHeight - Math.round(hypotenuse - (oppositeLeg * Math.sin(angle))));
            x1 = (0 - Math.round(oppositeLeg * Math.cos(angle)));
        }
    }

    return { x1, y1, x2, y2 };
}

function parseColorStopsForFillPathRectangular(colorStopList) {
    // halfen of all positions and inverse duplicating the before halfened range.
    colorStopList = getPatchedColorStopListIfNeeded(colorStopList);

    colorStopList = Array.from(colorStopList).map(function (colorStopValue) {
        return {
            position: (Math.round(((1 - colorStopValue.position) / 2) * 100000) / 100000),
            color:    colorStopValue.color
        };
    });

    return colorStopList.concat(
        Array.from(colorStopList).reverse().map(function (colorStopValue) {
            return {
                position: (1 - colorStopValue.position),
                color:    colorStopValue.color
            };
        })
    );
}

function renderGradientFillRectangular(context, fillRect, gradientFillVertical, gradientFillHorizontal) {

    // // draw gradient fill-path "bow tie"-like :: fill path vertically with vertical gradient
    // context.beginPath();
    // context.moveTo(fillRect.x2, fillRect.y1); // "bow tie" start
    // context.lineTo(fillRect.x1, fillRect.y2); // ...
    // context.lineTo(fillRect.x2, fillRect.y2); // ...
    // context.lineTo(fillRect.x1, fillRect.y1); // "bow tie" end.

    // draw gradient fill-path "bow tie"-like :: fill path vertically with vertical gradient
    //
    context.beginPath();                            // "bow tie" start for the first (hvertical) "bow tie" ...
    context.moveTo((fillRect.x2 + 1), fillRect.y1); // ... with 1px overlapping of its edges
    context.lineTo((fillRect.x1 - 1), fillRect.y2); // ... in order to prevent sparkling pixels
    context.lineTo((fillRect.x2 + 1), fillRect.y2); // ... along the 2 just colliding fill paths.
    context.lineTo((fillRect.x1 - 1), fillRect.y1); // ... "bow tie" end.

    context.fillStyle = gradientFillVertical;
    context.fill();

    // draw gradient fill-path "bow tie"-like :: fill path horizontally with horizontal gradient
    //
    context.beginPath();                            // "bow tie" start for the second (horizontal) "bow tie" ...
    context.moveTo((fillRect.x1 + 1), fillRect.y1); // ...
    context.lineTo((fillRect.x2 - 1), fillRect.y2); // ... with 1px overlapping of its edges
    context.lineTo(fillRect.x2, fillRect.y2);       // ... in order to prevent sparkling pixels
    context.lineTo(fillRect.x2, fillRect.y1);       // ... along the 2 just colliding fill paths.
    context.lineTo((fillRect.x2 - 1), fillRect.y1); // ...
    context.lineTo((fillRect.x1 + 1), fillRect.y2); // ...
    context.lineTo(fillRect.x1, fillRect.y2);       // ... "bow tie" end.

    context.fillStyle = gradientFillHorizontal;
    context.fill();
}

/**
 * @returns {CanvasGradient}
 */
function createGradientFillLinear(context, colorStopList, x1, y1, x2, y2) {

    colorStopList = getPatchedColorStopListIfNeeded(colorStopList);
    var
        gradientFill = context.createLinearGradient(x1, y1, x2, y2);

    colorStopList.forEach(function (colorStopValue) {
        // window.console.log('+++ createGradientFillLinear :: addColorStop :: [position, color] : ', colorStopValue.position, colorStopValue.color);

        gradientFill.addColorStop(colorStopValue.position, colorStopValue.color);
    });
    return gradientFill;
}

function getGradientFillShapeUnbound(/* context, gradientValue, parentRotation, pathCoords */) {
    // globalLogger.info('+++ getGradientFillShapeUnbound +++ [gradientValue, parentRotation, pathCoords] : ', gradientValue, parentRotation, pathCoords);
}

function getGradientFillShapeBound(/* context, gradientValue */) {
    // globalLogger.info('+++ getGradientFillShapeBound +++ [gradientValue] : ', gradientValue);
}

function getGradientFillRectangularUnbound(context, gradientValue, parentRotation, pathCoords) {
    // globalLogger.info('+++ getGradientFillRectangularUnbound +++ [gradientValue, parentRotation, pathCoords] : ', gradientValue, parentRotation, pathCoords);
    var
        //canvas            = context.canvas,
        //canvasWidth       = canvas.width,
        //canvasHeight      = canvas.height,

        //boxMeasures       = getBoundingBoxMeasuresForRotatedDrawingRect(canvasWidth, canvasHeight, parentRotation),
        boxMeasures       = getSnapBoxMeasuresForRotatedShape(pathCoords, parentRotation),
        boundingWidth     = boxMeasures.width,
        boundingHeight    = boxMeasures.height,

        sketchSheet       = returnCleanSketchSheet(boundingWidth, boundingWidth),
        //sketchSheet       = returnCleanSketchSheet(canvasWidth, canvasHeight),
        sketchCanvas      = sketchSheet.canvas,
        sketchContext     = sketchSheet.context,

        tileRectMeasures  = getTileRectMeasures(boundingWidth, boundingHeight, gradientValue),
        //fillPathMeasures  = getFillPathMeasures(boundingWidth, boundingHeight, gradientValue),

        //tileRectMeasures  = getTileRectMeasures(canvasWidth, canvasHeight, gradientValue),
        //fillPathMeasures  = getFillPathMeasures(canvasWidth, canvasHeight, gradientValue),

        halfWidth         = Math.round(tileRectMeasures.width / 2),
        halfHeight        = Math.round(tileRectMeasures.height / 2),

        vertical_x        = (tileRectMeasures.x1 + halfWidth),
        vertical_y1       = tileRectMeasures.y1,
        vertical_y2       = tileRectMeasures.y2,

        horizontal_x1     = tileRectMeasures.x1,
        horizontal_x2     = tileRectMeasures.x2,
        horizontal_y      = (tileRectMeasures.y1 + halfHeight),

        colorStopList     = parseColorStopsForFillPathRectangular(gradientValue.colorStops);

    // // debugging
    // //
    // document.body.appendChild(sketchCanvas);
    // sketchCanvas.style.position = 'absolute';

    renderGradientFillRectangular(
        sketchContext,                                                                                                    // context,
        tileRectMeasures,                                                                                                 // fillRect,
        createGradientFillLinear(sketchContext, colorStopList, vertical_x, vertical_y1, vertical_x, vertical_y2),         // gradientFillVertical,
        createGradientFillLinear(sketchContext, colorStopList, horizontal_x1, horizontal_y, horizontal_x2, horizontal_y)  // gradientFillHorizontal
    );

    //// globalLogger.info('+++ getGradientFillRectangularUnbound +++ [canvas.width, canvas.height] : ', canvasWidth, canvasHeight);
    //// globalLogger.info('+++ getGradientFillRectangularUnbound +++ [tileRectMeasures] : ', tileRectMeasures);
    //// globalLogger.info('+++ getGradientFillRectangularUnbound +++ [fillPathMeasures] : ', fillPathMeasures);
    //// globalLogger.info('+++ getGradientFillRectangularUnbound +++ [gradientFill] : ', gradientFill);

    return createPatternFillFromCanvas(sketchCanvas, context);
}

function getGradientFillRectangularBound(context, gradientValue) {
    // globalLogger.info('+++ getGradientFillRectangularBound +++ [gradientValue] : ', gradientValue);
    var
        canvas            = context.canvas,
        canvasWidth       = canvas.width,
        canvasHeight      = canvas.height,

        sketchSheet       = returnCleanSketchSheet(canvasWidth, canvasHeight),
        sketchCanvas      = sketchSheet.canvas,
        sketchContext     = sketchSheet.context,

        tileRectMeasures  = getTileRectMeasures(canvasWidth, canvasHeight, gradientValue),

        halfWidth         = Math.round(tileRectMeasures.width / 2),
        halfHeight        = Math.round(tileRectMeasures.height / 2),

        vertical_x        = (tileRectMeasures.x1 + halfWidth),
        vertical_y1       = tileRectMeasures.y1,
        vertical_y2       = tileRectMeasures.y2,

        horizontal_x1     = tileRectMeasures.x1,
        horizontal_x2     = tileRectMeasures.x2,
        horizontal_y      = (tileRectMeasures.y1 + halfHeight),

        colorStopList     = parseColorStopsForFillPathRectangular(gradientValue.colorStops);

    // // debugging
    // //
    // document.body.appendChild(sketchCanvas);
    // sketchCanvas.style.position = 'absolute';

    renderGradientFillRectangular(
        sketchContext,                                                                                                    // context,
        tileRectMeasures,                                                                                                 // fillRect,
        createGradientFillLinear(sketchContext, colorStopList, vertical_x, vertical_y1, vertical_x, vertical_y2),         // gradientFillVertical,
        createGradientFillLinear(sketchContext, colorStopList, horizontal_x1, horizontal_y, horizontal_x2, horizontal_y)  // gradientFillHorizontal
    );

    //// globalLogger.info('+++ getGradientFillRectangularBound +++ [canvas.width, canvas.height] : ', canvasWidth, canvasHeight);
    //// globalLogger.info('+++ getGradientFillRectangularBound +++ [tileRectMeasures] : ', tileRectMeasures);
    //// globalLogger.info('+++ getGradientFillRectangularBound +++ [fillPathMeasures] : ', fillPathMeasures);
    //// globalLogger.info('+++ getGradientFillRectangularBound +++ [gradientFill] : ', gradientFill);

    return createPatternFillFromCanvas(sketchCanvas, context);
}

function getGradientFillRadialUnbound(context, gradientValue, parentRotation) {
    // globalLogger.info('+++ getGradientFillRadialUnbound +++ [gradientValue, parentRotation] : ', gradientValue, parentRotation);
    var
        canvas            = context.canvas,

        //tileRectMeasures  = getTileRectMeasures(canvas.width, canvas.height, gradientValue),
        //fillPathMeasures  = getFillPathMeasures(canvas.width, canvas.height, gradientValue),

        boxMeasures       = getBoundingBoxMeasuresForRotatedDrawingRect(canvas.width, canvas.height, parentRotation),
        boundingWidth     = boxMeasures.width,
        boundingHeight    = boxMeasures.height,

        tileRectMeasures  = getTileRectMeasures(boundingWidth, boundingHeight, gradientValue),
        fillPathMeasures  = getFillPathMeasures(boundingWidth, boundingHeight, gradientValue),

        path_x1           = fillPathMeasures.x1,
        path_y1           = fillPathMeasures.y1,
        //path_x2           = fillPathMeasures.x2,
        //path_y2           = fillPathMeasures.y2,

        cathetus_1a       = Math.abs(path_x1 - tileRectMeasures.x1),
        cathetus_1b       = Math.abs(path_y1 - tileRectMeasures.y1),
        radius_1          = Math.round(math.radius(cathetus_1a, cathetus_1b)),

        colorStopList     = getPatchedColorStopListIfNeeded(gradientValue.colorStops),
        //rotationValue     = (gradientValue.rotation % 360),

        gradientFill      = context.createRadialGradient(path_x1, path_y1, 0, path_x1, path_y1, radius_1);/*

    if ((path_x1 === path_x2) && (path_y1 === path_y2)) {

        gradientFill      = context.createRadialGradient(path_x1, path_y1, radius_1, path_x1, path_y1, 0);
    } else {
        var
            cathetus_2a   = Math.abs(path_x2 - tileRectMeasures.x2),
            cathetus_2b   = Math.abs(path_y2 - tileRectMeasures.y2),
            radius_2      = Math.round(math.radius(cathetus_2a, cathetus_2b) / 2);

        gradientFill      = context.createRadialGradient(path_x1, path_y1, radius_1, path_x2, path_y2, radius_2);
    }*/

    colorStopList.forEach(function (colorStopValue) {
        // window.console.log('+++ getGradientFillRadialUnbound :: addColorStop :: [position, color] : ', colorStopValue.position, colorStopValue.color);

        gradientFill.addColorStop(colorStopValue.position, colorStopValue.color);
    });

    //// globalLogger.info('+++ getGradientFillRadialUnbound +++ [canvas.width, canvas.height] : ', canvas.width, canvas.height);
    //// globalLogger.info('+++ getGradientFillRadialUnbound +++ [tileRectMeasures] : ', tileRectMeasures);
    //// globalLogger.info('+++ getGradientFillRadialUnbound +++ [fillPathMeasures] : ', fillPathMeasures);
    //// globalLogger.info('+++ getGradientFillRadialUnbound +++ [gradientFill] : ', gradientFill);

    return gradientFill;
}

function getGradientFillRadialBound(context, gradientValue) {
    // globalLogger.info('+++ getGradientFillRadialBound +++ [gradientValue] : ', gradientValue);
    var
        canvas            = context.canvas,

        tileRectMeasures  = getTileRectMeasures(canvas.width, canvas.height, gradientValue),
        fillPathMeasures  = getFillPathMeasures(canvas.width, canvas.height, gradientValue),

        path_x1           = fillPathMeasures.x1,
        path_y1           = fillPathMeasures.y1,
        //path_x2           = fillPathMeasures.x2,
        //path_y2           = fillPathMeasures.y2,

        cathetus_1a       = Math.abs(path_x1 - tileRectMeasures.x1),
        cathetus_1b       = Math.abs(path_y1 - tileRectMeasures.y1),
        radius_1          = Math.round(math.radius(cathetus_1a, cathetus_1b)),

        colorStopList     = getPatchedColorStopListIfNeeded(gradientValue.colorStops),
        //rotationValue     = (gradientValue.rotation % 360),

        gradientFill      = context.createRadialGradient(path_x1, path_y1, 0, path_x1, path_y1, radius_1);/*

        if ((path_x1 === path_x2) && (path_y1 === path_y2)) {

        gradientFill      = context.createRadialGradient(path_x1, path_y1, radius_1, path_x1, path_y1, 0);
        } else {
        var
            cathetus_2a   = Math.abs(path_x2 - tileRectMeasures.x2),
            cathetus_2b   = Math.abs(path_y2 - tileRectMeasures.y2),
            radius_2      = Math.round(math.radius(cathetus_2a, cathetus_2b) / 2);

        gradientFill      = context.createRadialGradient(path_x1, path_y1, radius_1, path_x2, path_y2, radius_2);
        }*/

    colorStopList.forEach(function (colorStopValue) {
        // window.console.log('+++ getGradientFillRadialBound :: addColorStop :: [position, color] : ', colorStopValue.position, colorStopValue.color);

        gradientFill.addColorStop(colorStopValue.position, colorStopValue.color);
    });

    //// globalLogger.info('+++ getGradientFillRadialBound +++ [canvas.width, canvas.height] : ', canvas.width, canvas.height);
    //// globalLogger.info('+++ getGradientFillRadialBound +++ [tileRectMeasures] : ', tileRectMeasures);
    //// globalLogger.info('+++ getGradientFillRadialBound +++ [fillPathMeasures] : ', fillPathMeasures);
    //// globalLogger.info('+++ getGradientFillRadialBound +++ [gradientFill] : ', gradientFill);

    return gradientFill;
}

/**
 * @returns {CanvasPattern}
 */
function getGradientFillLinearUnbound(context, gradientValue, parentRotation, pathCoords) {
    // globalLogger.info('+++ getGradientFillLinearUnbound +++ [gradientValue, parentRotation] : ', gradientValue, parentRotation, pathCoords);
    var
        gradientFill,

        boxMeasures     = pathCoords.xValues ? getSnapBoxMeasuresForRotatedShape(pathCoords, parentRotation) : { width: pathCoords.width, height: pathCoords.height },
        boxWidth        = boxMeasures.width,
        boxHeight       = boxMeasures.height,

        colorStopList   = gradientValue.colorStops,
        //isScaled        = gradientValue.isScaled,
        rotation        = (gradientValue.rotation % 360);

    if (rotation > 0) {
        var
            lineCoords  = getLinearGradientCoordinates(boxWidth, boxHeight, rotation);

        gradientFill    = createGradientFillLinear(context, colorStopList, lineCoords.x1, lineCoords.y1, lineCoords.x2, lineCoords.y2);
    } else {
        gradientFill    = createGradientFillLinear(context, colorStopList, 0, 0, boxWidth, 0);
    }
    return renderRotatedPatternFromGradientFill(context, gradientFill, ((360 - parentRotation) % 360), boxMeasures);
}

/**
 * @returns {CanvasGradient}
 */
function getGradientFillLinearBound(context, gradientValue) {
    //// globalLogger.info('+++ getGradientFillLinearBound +++ [gradientValue] : ', gradientValue);
    var
        gradientFill,

        canvas          = context.canvas,
        canvasWidth     = canvas.width,
        canvasHeight    = canvas.height,

        colorStopList   = gradientValue.colorStops,
        //isScaled        = gradientValue.isScaled,
        rotation        = (gradientValue.rotation % 360);

    if (rotation > 0) {
        var
            lineCoords  = getLinearGradientCoordinates(canvasWidth, canvasHeight, rotation);

        gradientFill    = createGradientFillLinear(context, colorStopList, lineCoords.x1, lineCoords.y1, lineCoords.x2, lineCoords.y2);
    } else {
        gradientFill    = createGradientFillLinear(context, colorStopList, 0, 0, canvasWidth, 0);
    }
    return gradientFill;
}

/**
 * @returns {CanvasGradient|CanvasPattern}
 */
function getGradientFill(context, gradientValue, parentRotation, pathCoords) {
    var
        operation,
        identifier    = '',

        gradientType  = gradientValue.type,
        pathType      = gradientValue.pathType,
        isBound       = (!pathCoords || !!gradientValue.isRotateWithShape || (parentRotation === 0));

    if (gradientType === 'linear') {
        identifier    = [gradientType, LITERAL_BRIDGE__AND_BOUND, isBound].join('');
    } else if (pathType) {
        identifier    = [LITERAL_PREFIX__PATH_TYPE, pathType, LITERAL_BRIDGE__AND_BOUND, isBound].join('');
    }
    operation         = GRADIENT_FILLTYPE_OPERATIONS[identifier] || NOOP;

    return operation(context, gradientValue, parentRotation, pathCoords);
}

/**
 * @returns {string}
 */
function getGradientColorString(descriptorOrValue, alpha) {
    alpha = (is.number(alpha) ? Math.max(0, Math.min(1, alpha)) : descriptorOrValue.a);

    var bytes = descriptorOrValue.bytes;
    var hex   = descriptorOrValue.hex;

    return (
        ((alpha === 1) && hex && ['#', hex.toLowerCase()].join(''))

        || (bytes && ['rgba(', [bytes[0], bytes[1], bytes[2], alpha].join(','), ')'].join(''))

        || ['rgba(', [descriptorOrValue.r, descriptorOrValue.g, descriptorOrValue.b, alpha].join(','), ')'].join('')
    );
}

/**
 *
 * @param {Object} colorStopValue
 * @returns  {Object} colorStopValue
 *  Returns a mapped variant of a `ColorStop` value with it's `color` attribute being
 *  the string representation of a canvas gradient color.
 *  In addition the newly assigned `colorDescriptor` attribute holds all information
 *  of how to technically describe a color by various color models.
 */
function resolveColorStopValue(colorStopValue, docModel, target) {
    var
        colorType   = Color.parseJSON(colorStopValue.color),
        descriptor  = { ...docModel.resolveColor(colorType, 'fill', target) }; // map [ColorDescriptor] instance to pure [Object] instance.
    //
    // [{
    //     a: 0.82,
    //     bytes: (4) [0, 0, 0, 209],
    //     css: "rgba(0,0,0,0.82)",
    //     dark: true,
    //     hex: "000000",
    //     hsl: {a: 0.82, h: 0, s: 0, l: 0},
    //     rgb: {a: 0.82, r: 0, g: 0, b: 0},
    //     type: "scheme",
    //     y: 0,
    // }, {
    //     a: 0,
    //     bytes: (4) [255, 255, 255, 0],
    //     css: "transparent",
    //     dark: false,
    //     hex: "FFFFFF",
    //     hsl: {a: 0, h: 0, s: 0, l: 1},
    //     rgb: {a: 0, r: 1, g: 1, b: 1},
    //     type: "scheme",
    //     y: 1,
    // }]

    // overwrite `color` attribute.
    //
    // https://developer.mozilla.org/en-US/docs/Web/API/CanvasGradient/addColorStop
    // https://developer.mozilla.org/en-US/docs/Web/CSS/color_value
    //
    colorStopValue.color = getGradientColorString(descriptor);
    colorStopValue.colorDescriptor = descriptor;

    return colorStopValue;
}

function getMappedColorStopValues(colorStopList) {
    return colorStopList.map(function (colorStop) {

        return colorStop.valueOf();
    });
}

/**
 *@returns {boolean}
 */
function isEqualGradients(gradient, type) {
    var
        gradientType  = gradient.getType(),
        isEqual       = (
            isBaseGradient(type) && ((type === gradient) || (

                (gradientType === type.getType()) &&

                (gradient.getRotation()       === type.getRotation()) &&
                (gradient.isRotateWithShape() === type.isRotateWithShape()) &&

                _.isEqual(getMappedColorStopValues(gradient.getColorStops()), getMappedColorStopValues(type.getColorStops()))
            )));

    if (isEqual) {
        isEqual = false;

        if ((gradientType === 'linear') && hasLinearTypeTrait(type)) {

            isEqual = (gradient.isScaled() === type.isScaled());

        } else if ((gradientType === 'path') && hasPathTypeTrait(type)) {
            var
                pathType = gradient.getPathType();
            if (pathType === type.getPathType()) {

                if (pathType === 'shape') {

                    isEqual = true;

                } else if (((pathType === 'circle') || (pathType === 'rect')) && hasWithFillRectTrait(type)) {
                    isEqual = (
                        _.isEqual(gradient.getEdgeValues(),      type.getEdgeValues()) &&
                        _.isEqual(gradient.getFillTileValues(),  type.getFillTileValues())
                    );
                }
            }
        }
    }
    return isEqual;
}

// BEGIN OF :: locally scoped helper module(s) / class(es) ----------------
//

// module ColorStop =======================================================

/**
 * @param position
 * @param color
 */
class ColorStop {

    constructor(position, color) {

        this.getPosition = function () {
            return position;
        };

        this.getColor = function () {
            return color.clone();
        };

        /**
         * Returns the type-descriptor representation of this color-stop.
         *
         * @returns {Object}
         *  The type-descriptor representation of this color-stop.
         */
        this.valueOf = function () {
            return {
                position,
                color:      color.toJSON()
            };
        };

        /**
         * Returns the JSON-style stringified type-descriptor representation
         * of this color-stop.
         *
         * @returns {Object}
         *  The JSON-style stringified type-descriptor representation of this color-stop.
         */
        this.toString = function () {
            return JSON.stringify(this.valueOf());
        };
    }
}

/**
 * @returns {ColorStop}
 */
function createColorStop(typeDescriptor) {
    if (!isObjectObject(typeDescriptor)) {

        typeDescriptor = {};
    }
    var
        position  = getNumberOption(typeDescriptor, 'position', 0,    0, 1, 0.00001),
        color     = Color.parseJSON(
            getObjectOption(typeDescriptor,         'color',    {})
        );
    //global.console.log('+++ createColorStop :: [position, color] : ', position, color);

    return (new ColorStop(position, color));

} // factory ColorStop

var ColorStopModule = {
    create: createColorStop
};
// module ColorStop =======================================================

//
// END OF :: locally scoped helper module(s) / class(es) ------------------

// constructor Gradient ===================================================
/**
 * Runtime representation of a gradient of any type supported in document
 * operations.
 *
 * @param state
 * @param traitList
 */
class Gradient {

    constructor(state, colorStopList, traitList) {

        // public methods -----------------------------------------------------

        /**
         * Returns the type of this gradient.
         *
         * @returns {String}
         *  The type of this gradient. See constructor parameter 'type' for
         *  details.
         */
        this.getType = function () {
            return state.type;
        };

        /**
         * Returns a gradient's color stop list.
         *
         * @returns {Array}
         *  A copy of this gradient's internal list of color stop types.
         */
        this.getColorStops = function () {
            return Array.from(colorStopList);
        };

        /**
         * Returns the type-descriptor representation of this gradient,
         * as it gets used in document operations.
         *
         * @returns {Object}
         *  The type-descriptor representation of this gradient.
         */
        this.valueOf = function () {
            var gadientValue = { ...state };
            gadientValue.colorStops = getMappedColorStopValues(colorStopList);
            return gadientValue;
        };

        /**
         * Returns the JSON-style stringified type-descriptor representation
         * of this gradient, as it gets used in document operations.
         *
         * @returns {Object}
         *  The JSON-style stringified type-descriptor representation of this gradient.
         */
        this.toString = function () {
            return JSON.stringify(this.valueOf());
        };

        /**
         * Returns whether this gradient equals the passed gradient type.
         *
         * @param {Gradient} type
         *  The gradient type to be compared with this gradient.
         *
         * @returns {Boolean}
         *  Whether this gradient is equal to the passed gradient type.
         */
        this.equals = function (type) {
            return isEqualGradients(this, type);
        };

        /**
         * Returns a deep clone of this gradient.
         *
         * @returns {Gradient}
         *  A deep clone of this gradient.
         */
        this.clone = function () {
            return (new Gradient({ ...state }, Array.from(colorStopList), Array.from(traitList)));
        };

        applyGradientSpecificTraits(traitList, this, state);
    }
}

// trait based gradient composition =======================================
function applyGradientSpecificTraits(traitList, gradient, state) {
    traitList.forEach(function (trait/*, idx, list*/) {

        trait.call(gradient, state);
    });
} // trait based gradient composition

// trait WithScalingTrait =================================================
function WithScalingTrait(state) {
    var
        type = this;

    type.isScaled = function () {
        return state.isScaled;
    };
} // trait WithScalingTrait

// trait WithRotationTrait ================================================
function WithRotationTrait(state) {
    var
        type = this;

    type.getRotation = function () {
        return state.rotation;
    };
    //type.setRotation = function (value) {
    //    state.rotation = value;
    //};

    type.isRotateWithShape = function () {
        return state.isRotateWithShape;
    };
} // trait WithRotationTrait

// trait WithFillRectTrait ================================================
function WithFillRectTrait(state) {
    var
        type = this;

    type.getEdgeValues = function () {
        return {
            top:    state.top,
            right:  state.right,
            bottom: state.bottom,
            left:   state.left
        };
    };
    type.getFillTileValues = function () {
        return {
            top:    state.pathFillTop,
            right:  state.pathFillRight,
            bottom: state.pathFillBottom,
            left:   state.pathFillLeft
        };
    };
} // trait WithFillRectTrait

// trait PathTypeTrait ====================================================
function PathTypeTrait(state) {
    var
        type = this;

    type.getPathType = function () {
        return state.pathType;
    };
    //WithRotationTrait.call(type, state);

} // trait PathTypeTrait

// trait PathTypeShapeTrait ===============================================
function PathTypeShapeTrait(state) {
    var
        type = this;

    PathTypeTrait.call(type, state);

} // trait PathTypeShapeTrait

// trait PathTypeWithFillRectTrait =========================================
function PathTypeWithFillRectTrait(state) {
    var
        type = this;

    PathTypeTrait.call(type, state);
    WithFillRectTrait.call(type, state);

} // trait PathTypeWithFillRectTrait

// trait LinearTypeTrait ==================================================
function LinearTypeTrait(state) {
    var
        type = this;

    WithScalingTrait.call(type, state);
    //WithRotationTrait.call(type, state);

} // trait LinearTypeTrait

// public static factory method ===========================================

/**
 * Evaluates the passed type-descriptor representation of a gradient and
 * returns a new instance of the class `Gradient` or returns the `NULL` value
 * in case of `typeDescriptor` not being an `Object` type.
 *
 * @param {Object} typeDescriptor
 *  The type-descriptor to be evaluated. Should be an object with the
 *  properties supported in document operations.
 *
 * @returns {Gradient|null}
 *  A new gradient instance or the `NULL` value.
 */
function createGradient(typeDescriptor) {
    var
        gradient        = null,

        state           = {},
        traitList       = [],

        colorStopList,
        createColorStop = ColorStopModule.create;

    if (isObjectObject(typeDescriptor)) {

        colorStopList = getArrayOption(typeDescriptor, 'colorStops', null, true);

        if (colorStopList) {
            colorStopList = colorStopList.map(createColorStop);
        } else {
            /**
             *  please intercept here in case of omitted "color"/"color2" properties
             *  in order to retrieve feasible (e.g. theme depended) default/fallback values
             *  before pushing them into this "colorStopList".
             */
            colorStopList   = [
                createColorStop({ position: 0, color: typeDescriptor.color }),
                createColorStop({ position: 1, color: typeDescriptor.color2 })
            ];
        }
        //global.console.log('+++ createGradient :: colorStopList : ', colorStopList);

        var
            // either 'linear' or 'path'.   // Module.LINEAR.type -  MINIMAL_GRADIENT_DESCRIPTOR.type
            gradientType        = getStringOption(typeDescriptor,   'type',               'linear',   true),
            pathType;

        state.type              = gradientType;

        state.rotation          = getNumberOption(typeDescriptor,   'rotation',           0,          0,      360,    0.00001);
        //  Bug: DOCS-1847
        //  OOXML filter will now always send a isRotateWithShape attribute and ODF doesn't support it.
        //  In ODF fillings are always rotated with it's shapes, hence we can initialize it
        //  by default with true.
        state.isRotateWithShape = getBooleanOption(typeDescriptor,  'isRotateWithShape',  true);

        //  state.flipH             = getBooleanOption(typeDescriptor,  'flipH',              false);
        //  state.flipV             = getBooleanOption(typeDescriptor,  'flipV',              false);
        //  // 'none' or 'x' or 'y' or 'xy'.
        //  //state.flip            = getStringOption(typeDescriptor,   'flip',               'none',     true);

        if (gradientType === 'linear') {

            // "linear" type exclusively:
            state.isScaled      = getBooleanOption(typeDescriptor,  'isScaled',           false);

            traitList.push(LinearTypeTrait);

        } else if (gradientType === 'path') {

            // "path" type exclusively:
            // 'circle' or 'rect' or 'shape'.
            pathType            = getStringOption(typeDescriptor,   'pathType',           'rect',     true);

            state.pathType      = pathType;

            if (pathType === 'shape') {
                traitList.push(PathTypeShapeTrait);

            } else {
                // (pathType === 'circle') || (pathType === 'rect')

                state.left           = getNumberOption(typeDescriptor, 'left',            0,          -1,     1);
                state.top            = getNumberOption(typeDescriptor, 'top',             0,          -1,     1);
                state.right          = getNumberOption(typeDescriptor, 'right',           0,          -1,     1);
                state.bottom         = getNumberOption(typeDescriptor, 'bottom',          0,          -1,     1);

                state.pathFillLeft   = getNumberOption(typeDescriptor, 'pathFillLeft',    0.5,        -1,     1); // - new "pathFill" default values ...
                state.pathFillTop    = getNumberOption(typeDescriptor, 'pathFillTop',     0.5,        -1,     1); //
                state.pathFillRight  = getNumberOption(typeDescriptor, 'pathFillRight',   0.5,        -1,     1); //   see: Bug 51597 - ODP: gradient background are not displayed
                state.pathFillBottom = getNumberOption(typeDescriptor, 'pathFillBottom',  0.5,        -1,     1); //   see: Gradient Data TYpe - https://intranet.open-xchange.com/wiki/documents-team:operations:general#gradient

                // // NOTE:
                // // can be omitted as soon as Sven changes the total of top/left/right/bottom values to the total of pathFill(Top/Left/Right/Bottom)
                // //
                // if (('left' in typeDescriptor) && !('pathFillRight' in typeDescriptor)) {
                //
                //     state.pathFillRight = (-1 * state.left) || 0;
                //   //state.pathFillLeft = 0.5;
                //
                // } else if (('right' in typeDescriptor) && !('pathFillLeft' in typeDescriptor)) {
                //
                //     state.pathFillLeft = (-1 * state.right) || 0;
                //   //state.pathFillRight = 0.5;
                // }
                // if (('top' in typeDescriptor) && !('pathFillBottom' in typeDescriptor)) {
                //
                //     state.pathFillBottom = (-1 * state.top) || 0;
                //   //state.pathFillTop = 0.5;
                //
                // } else if (('bottom' in typeDescriptor) && !('pathFillTop' in typeDescriptor)) {
                //
                //     state.pathFillTop = (-1 * state.bottom) || 0;
                //   //state.pathFillBottom = 0.5;
                // }

                traitList.push(PathTypeWithFillRectTrait);
            }
        }
        traitList.unshift(WithRotationTrait); // make rotation trait a generic behavior of any gradient type

        gradient = (new Gradient(state, colorStopList, traitList));
    }
    // if (gradient) { gradientList.push(gradient); } // @NOTIFICATION: for debugging only.

    return gradient;
}

// private static (locally scoped) trait and type detection helpers =======

function hasWithRotationTrait(type) {
    return (
        is.function(type.getRotation) &&
        is.function(type.isRotateWithShape)
    );
}
function hasWithScalingTrait(type) {
    return is.function(type.isScaled);
}
function hasWithFillRectTrait(type) {
    return (
        is.function(type.getEdgeValues) &&
        is.function(type.getFillTileValues)
    );
}

function hasGenericTraits(type) {
    return (
        is.function(type.equals) &&
        is.function(type.clone) &&

        is.function(type.getType) &&
        is.function(type.getColorStops) &&

        hasWithRotationTrait(type)
    );
}

function hasLinearTypeTrait(type) {
    return hasWithScalingTrait(type);
}
function hasPathTypeTrait(type) {
    return is.function(type.getPathType);
}

function isBaseGradient(type) {
    return (isObjectObject(type) && ((type instanceof Gradient) || hasGenericTraits(type)));  // - shortcut variant.
    //return (isObjectObject(type) && hasGenericTraits(type));                                  // - spoof pr variant.
}

// public static utility methods ==========================================

/**
 * Returns whether the passed type does match all criteria
 * for being a valid `Gradient` type or not.
 *
 * @param {any} type
 * The type that is going to be checked for being a valid `Gradient` type.
 *
 * @returns {Boolean}
 *  Whether the passed type does match all criteria for being a valid `Gradient` type.
 */
function isGradient(type) {
    var
        isValidType   = isBaseGradient(type),
        gradientType  = (isValidType && type.getType()),
        pathType;

    if (isValidType) {
        isValidType   = false;

        if (gradientType === 'linear') {

            isValidType = hasLinearTypeTrait(type);

        } else if (gradientType === 'path') {

            isValidType = hasPathTypeTrait(type);
            pathType    = (isValidType && type.getPathType());

            isValidType = (isValidType && (

                ((pathType === 'shape') && true) ||                     // - generic approach is feasible enough yet.
                //((pathType === 'shape') && hasPathTypeShapeTrait()) ||  // - use that, ones it has been specified more precisely.

                (((pathType === 'circle') || (pathType === 'rect')) && hasWithFillRectTrait(type))
            ));
        }
    }
    return isValidType;
}

/**
 * Returns whether the type-descriptor representations of two gradients are equal or not.
 *
 * @param {Object} typeDescriptorA
 * The type-descriptor representation of the first gradient to be compared
 * to the other gradient.
 *
 * @param {Object} typeDescriptorB
 * The type-descriptor representation of the second gradient to be compared
 * to the other gradient.
 *
 * @returns {Boolean}
 *  Whether the type-descriptor representations of two gradients are equal.
 */
function isEqualDescriptors(typeDescriptorA, typeDescriptorB) {
    return createGradient(typeDescriptorA).equals(createGradient(typeDescriptorB));
}

function getDescriptorFromFillAttributes(fillAttrs) {
    var
        gradientDescriptor = { ...fillAttrs.gradient };  // ensures always an object.

    // enable the descriptor to cover a minimal gradient (color transition) render fallback.
    gradientDescriptor.color  = fillAttrs.color;
    gradientDescriptor.color2 = fillAttrs.color2;

    return gradientDescriptor;
}

function resolveColors(gradient, docModel, target) {
    var
        gradientValue = gradient.valueOf();

    gradientValue.colorStops.map(function (colorStopValue) {
        return resolveColorStopValue(colorStopValue, docModel, target);
    });

    return gradientValue;
}

function parseFillStyle(docModel, gradient, context, target, pathCoords) {
    //// globalLogger.info('+++ Gradient.parseFillStyle +++ pathCoords : ', pathCoords);
    var                                                                           //  - refactor `CanvasWrapper` making it aware of a parent `Shape` abstraction
        $drawingNode    = $(context.canvas).closest('.drawing.rotated-drawing'),  //  - ones a canvas wrapper type can access it's parent shape type, retrieving
        drawingAttrs    = getExplicitAttributeSet($drawingNode),   //    values like e.g. `parentRotation` will be much easier and far cleaner.

        gradientValue   = resolveColors(gradient, docModel, target),
        gradientFill,

        parentRotation  = (drawingAttrs && drawingAttrs.drawing && drawingAttrs.drawing.rotation) || 0;
        //parentRotation  = DrawingFrame.getDrawingRotationAngle(docModel, $drawingNode) || 0;

    if ((context.canvas.width <= 0) || (context.canvas.height <= 0)) {
        gradientFill = 'transparent';
    } else {
        gradientFill = getGradientFill(context, gradientValue, parentRotation, pathCoords);
    }
    return gradientFill;
}

function parseImageDataUrl(gradient, docModel, canvasOrDescriptor, target) {
    var
        dataUrl,

        canvas,
        context;

    if (canvasOrDescriptor instanceof HTMLCanvasElement) {
        canvas = canvasOrDescriptor;
        context = canvas.getContext('2d');
    } else if (isObjectObject(canvasOrDescriptor)) {
        var sketchSheet = createCleanSketchSheetClone(canvasOrDescriptor.width, canvasOrDescriptor.height);
        canvas = sketchSheet.canvas;
        context = sketchSheet.context;
    }

    if (context) {
        context.fillStyle = parseFillStyle(docModel, gradient, context, target/*, null*/);  // @NOTE - Do never ever alter
        context.fillRect(0, 0, canvas.width, canvas.height);                                // precedence of this two lines.
        dataUrl = canvas.toDataURL();
    }

    return dataUrl;
    //return (dataUrl || '');
}

function parseImageElement(gradient, docModel, canvasOrDescriptor, target) {
    var
        elmImage  = IMAGE_BLUEPRINT.cloneNode(),

        dataUrl   = parseImageDataUrl(gradient, docModel, canvasOrDescriptor, target);
    if (dataUrl) {

        elmImage.width  = canvasOrDescriptor.width;
        elmImage.height = canvasOrDescriptor.height;

        elmImage.src    = dataUrl;
    }
    return elmImage;
}

// module Gradient --------------------------------------------------------

var Module = {      //  assign locally scoped static method implementations.

    // all:                function () { return array_from(gradientList); },       // @NOTIFICATION: for debugging only.
    create:             createGradient,

    resolveColors,
    parseFillStyle,

    parseImageDataUrl,
    parseImageElement,

    isGradient,
    isEqualDescriptors,

    getDescriptorFromAttributes: getDescriptorFromFillAttributes
};

// constants --------------------------------------------------------------

/**
 * The config object according to the default gradient type.
 */
Module.LINEAR = { type: 'linear', isRotateWithShape:  true }; // MINIMAL_GRADIENT_DESCRIPTOR

// exports ================================================================

export default Module;
