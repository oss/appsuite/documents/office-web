/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { dict, is, json } from '@/io.ox/office/tk/algorithms';

// constants ==================================================================

/**
 * Minifying the operations (DOCS-892).
 * Collector for all conversions to minify the operations (and data attribute objects).
 *
 * Syntax:
 *
 * 'keyChanges': The 'key' describes the string to be substituted. The value is an object,
 *               that must contain the key 'n' for 'new'.
 *               Example: "start: { n: 's' }"
 *               The key 'start' is replaced by the string 's'.
 *               Optionally the value object can contain a key 'p' for 'parent'. If this is
 *               specified, the key is only replaced, if the object has the specified parent.
 *               Example: "bold: { n: 'bo', p: { character: 1 } }"
 *               The key 'bold' is replaced by the string 'bo', only if the parent of the
 *               object is 'character'. In this case this is the family of the attribute.
 *               Info: As parent the original (long) key name can always be used!
 *               Info: More than one parent can be specified inside the parent object.
 *
 * 'valueChanges': The 'key' describes the string to be substituted. The value is an object,
 *               that must contain the key 'n' for 'new' and the key 'k' for 'key'.
 *               Example: "insertText: { n: 'it', k: 'n' }"
 *               The value 'insertText' is replaced by the string 'it', if the corresponding
 *               key is 'n' ('n' describes 'name' that is already replaced, because keys are
 *               exchanged before values during minification. During expansion values are first
 *               expanded, then keys).
 *               Optionally the value object can contain a key 'p' for 'parent'. If this is
 *               specified, the value is only replaced, if the object has the specified parent.
 *               Example: "right: { n: 'ri', k: 'al', p: { paragraph: 1 } }"
 *               The value 'right' is replaced by the string 'ri', only if the key is 'alignment'
 *               AND the parent of the object is 'paragraph'. In this case this is the family of
 *               the attribute.
 *               Info: As parent the original (long) key name can always be used!
 *               Info: More than one parent can be specified inside the parent object.
 */
const CONVERSIONS = {

    keyChanges: {

        // top level keys
        name: { n: 'n' },
        start: { n: 'o' },
        end: { n: 'q' },
        text: { n: 'w' },
        attrs: { n: 'a' },

        styleId: { n: 'st' },
        styleName: { n: 'sn' },
        target: { n: 'ta' },
        type: { n: 'ty' },

        // families

        changes: { n: 'cg' },
        character: { n: 'ch' },
        drawing: { n: 'dr' },
        paragraph: { n: 'pa' },
        presentation: { n: 'pr' },
        shape: { n: 'sh' },
        table: { n: 'tb' },

        // character attributes

        // bold: { n: 'bo', p: { character: 1 } }, // example for parent handling -> only replacing in family 'character'
        // bold: { n: 'bo' },
        // italic: { n: 'it' },
        // underline: { n: 'un' },

        // fontSize: { n: 'fs' },

        // fontName: { n: 'fn' },
        fontNameAsian: { n: 'fna' },
        fontNameComplex: { n: 'fnc' },
        fontNameEastAsia: { n: 'fnea' },
        fontNameSymbol: { n: 'fns' },

        // anchor: { n: 'an' },
        // baseline: { n: 'bl' },
        // vertAlign: { n: 'va' },
        // caps: { n: 'ca' },

        // color: { n: 'co' },
        // fillColor: { n: 'fco' },

        // type: { n: 'ty' },

        // // paragraph attributes

        // alignment: { n: 'al' },
        // lineHeight: { n: 'lh' },

        // indentLeft: { n: 'il' },
        // indentRight: { n: 'iri' },
        // indentFirstLine: { n: 'ifl' },

        // listStyleId: { n: 'lsi' },
        // listStartValue: { n: 'lsv' },
        // listLevel: { n: 'll' },
        // listLabelHidden: { n: 'llh' },

        // outlineLevel: { n: 'oll' },
        // tabStops: { n: 'ts' }

        // drawing attributes

        minFrameHeight: { n: 'mfh' }
    },

    valueChanges: {

        // operation names
        changeAutoStyle: { n: 'cas', k: 'n' },
        changeCells: { n: 'cc', k: 'n' },
        changeCFRule: { n: 'ccf', k: 'n' },
        changeChartAxis: { n: 'ccha', k: 'n' },
        changeChartGrid: { n: 'cchg', k: 'n' },
        changeChartLegend: { n: 'cchl', k: 'n' },
        changeChartSeries: { n: 'cchs', k: 'n' },
        changeChartTitle: { n: 'ccht', k: 'n' },
        changeColumns: { n: 'ccol', k: 'n' },
        changeComment: { n: 'cco', k: 'n' },
        changeDrawing: { n: 'cdr', k: 'n' },
        changeDVRule: { n: 'cva', k: 'n' },
        changeLayout: { n: 'cla', k: 'n' },
        changeMaster: { n: 'cma', k: 'n' },
        changeName: { n: 'cn', k: 'n' },
        changeNote: { n: 'cno', k: 'n' },
        changeRows: { n: 'cro', k: 'n' },
        changeStyleSheet: { n: 'css', k: 'n' },
        changeTable: { n: 'ct', k: 'n' },
        changeTableColumn: { n: 'ctc', k: 'n' },
        copySheet: { n: 'cs', k: 'n' },
        createError: { n: 'ce', k: 'n' },
        delete: { n: 'd', k: 'n' },
        deleteAutoStyle: { n: 'das', k: 'n' },
        deleteCFRule: { n: 'dcf', k: 'n' },
        deleteChartSeries: { n: 'dchs', k: 'n' },
        deleteColumns: { n: 'dcol', k: 'n' },
        deleteComment: { n: 'dco', k: 'n' },
        deleteDrawing: { n: 'dd', k: 'n' },
        deleteDVRule: { n: 'dva', k: 'n' },
        deleteHeaderFooter: { n: 'dhf', k: 'n' },
        deleteHyperlink: { n: 'dhy', k: 'n' },
        deleteListStyle: { n: 'dls', k: 'n' },
        deleteName: { n: 'dn', k: 'n' },
        deleteNote: { n: 'dno', k: 'n' },
        deleteNumberFormat: { n: 'dnf', k: 'n' },
        deleteRows: { n: 'dro', k: 'n' },
        deleteSheet: { n: 'dsh', k: 'n' },
        deleteStyleSheet: { n: 'dss', k: 'n' },
        deleteTable: { n: 'dta', k: 'n' },
        group: { n: 'g', k: 'n' },
        insertAutoStyle: { n: 'ias', k: 'n' },
        insertBookmark: { n: 'ibm', k: 'n' },
        insertCells: { n: 'ic', k: 'n' },
        insertCFRule: { n: 'icr', k: 'n' },
        insertChartSeries: { n: 'ichs', k: 'n' },
        insertColumn: { n: 'ico', k: 'n' },
        insertColumns: { n: 'icol', k: 'n' },
        insertComment: { n: 'icom', k: 'n' },
        insertComplexField: { n: 'icf', k: 'n' },
        insertDrawing: { n: 'id', k: 'n' },
        insertDVRule: { n: 'iva', k: 'n' },
        insertField: { n: 'if', k: 'n' },
        insertFontDescription: { n: 'ifd', k: 'n' },
        insertHardBreak: { n: 'ihb', k: 'n' },
        insertHeaderFooter: { n: 'ihf', k: 'n' },
        insertHyperlink: { n: 'ihl', k: 'n' },
        insertLayoutSlide: { n: 'ils', k: 'n' },
        insertListStyle: { n: 'ili', k: 'n' },
        insertMasterSlide: { n: 'ims', k: 'n' },
        insertName: { n: 'in', k: 'n' },
        insertNote: { n: 'ino', k: 'n' },
        insertNumberFormat: { n: 'inf', k: 'n' },
        insertParagraph: { n: 'ip', k: 'n' },
        insertRange: { n: 'ira', k: 'n' },
        insertRows: { n: 'iro', k: 'n' },
        insertSheet: { n: 'ish', k: 'n' },
        insertSlide: { n: 'isl', k: 'n' },
        insertStyleSheet: { n: 'iss', k: 'n' },
        insertTab: { n: 'itb', k: 'n' },
        insertTable: { n: 'ita', k: 'n' },
        insertText: { n: 'it', k: 'n' },
        insertTheme: { n: 'ith', k: 'n' },
        mergeCells: { n: 'mc', k: 'n' },
        mergeParagraph: { n: 'mp', k: 'n' },
        mergeTable: { n: 'mt', k: 'n' },
        move: { n: 'mo', k: 'n' },
        moveDrawing: { n: 'md', k: 'n' },
        moveLayoutSlide: { n: 'mls', k: 'n' },
        moveNotes: { n: 'mno', k: 'n' },
        moveSheet: { n: 'msh', k: 'n' },
        moveSlide: { n: 'msl', k: 'n' },
        noOp: { n: 'no', k: 'n' },
        setAttributes: { n: 'sa', k: 'n' },
        setDocumentAttributes: { n: 'sda', k: 'n' },
        splitParagraph: { n: 'sp', k: 'n' },
        splitTable: { n: 'st', k: 'n' },
        ungroup: { n: 'ug', k: 'n' },
        unknownValue: { n: 'uk', k: 'n' },
        updateComplexField: { n: 'ucf', k: 'n' },
        updateField: { n: 'uf', k: 'n' }

        // property values

        // baseline: { n: 'bl', k: 'va' },
        // auto: { n: 'au', k: 'ty' },
        // percent: { n: 'pc', k: 'ty' },

        // left: { n: 'le', k: 'al' },
        // // right: { n: 'ri', k: 'al', p: { paragraph: 1 } } // example for parent handling -> only replacing in family 'paragraph'
        // right: { n: 'ri', k: 'al' }

    }
};

/**
 * A set with all keys, whose values must never be minified. This is important
 * for user defined keys, like the 'tableNames' used in OX Spreadsheet (DOCS-4825).
 */
const FORBIDDEN_CONVERSION_KEYS = new Set(['tableNames']);

/**
 * The provider ID for OX AppSuite that is used in comments to find a valid
 * author for a specified author ID. This can only be resolved on client
 * side correctly, if the provider ID is 'ox'.
 */
export const OX_PROVIDER_ID = 'ox';

// vars =======================================================================

/**
 * Expanding the operations (DOCS-892)
 * Collector for all conversions to expand the minified operations (and other objects).
 * This object is generated automatically within the function
 * `generateExpandConversions()`.
 * The base for the expansion is the object `CONVERSIONS`.
 */
let EXPAND_CONVERSIONS;

// private functions ==========================================================

/**
 * Generating the conversion object for expanding the operations (DOCS-892).
 * This function is only called once and creates automatically the object that can
 * be used to expand minified operations.
 * Using the specified object `CONVERSIONS` for minifying operations
 * and objects, within this function the object `EXPAND_CONVERSIONS`
 * is generated, so that operations and other objects can be expanded again.
 */
function generateExpandConversions() {

    const keyChanges = CONVERSIONS.keyChanges;
    const valueChanges = CONVERSIONS.valueChanges;

    EXPAND_CONVERSIONS = { keyChanges: {}, valueChanges: {} };

    dict.forEach(keyChanges, (val, key) => {

        const newVal = key;
        const newKey = val.n;

        EXPAND_CONVERSIONS.keyChanges[newKey] = json.deepClone(val);
        EXPAND_CONVERSIONS.keyChanges[newKey].n = newVal;
    });

    dict.forEach(valueChanges, (val, key) => {

        const newVal = key;
        const newKey = val.n;

        EXPAND_CONVERSIONS.valueChanges[newKey] = json.deepClone(val);
        EXPAND_CONVERSIONS.valueChanges[newKey].n = newVal;
    });
}

// public functions ===========================================================

/**
 * Retrieves the actions array from an actions object or the actions array
 * itself.
 *
 * @param {Array<Object>|Object} actions
 *
 * @returns {Array<Object>}
 */
export function getActionsArray(actions) {
    return (is.dict(actions) && is.array(actions.actions)) ? actions.actions : actions;
}

/**
 * Minifying or expanding one specified object (DOCS-892).
 *
 * @param {Object} obj
 *  An object (for example an operation or attribute object) that shall be minified or
 *  expanded.
 *
 * @param {String} parent
 *  A string that specifies a parent for the given object. This can be used to specify
 *  the minification of the object to selected parents.
 *  Example: Using parent 'character' reduces the minification to those keys that are
 *  inside the character attribute object. If same keys or values are specified also
 *  inside another family, they are not replaced there.
 *  For more information look at the description of the object `CONVERSIONS`.
 *
 * @param {Boolean} [expand]
 *  Whether the specified object shall be expanded or minified. If this parameter is
 *  set to a truthy value, the object will be expanded. Otherwise (or if not specified)
 *  the object will be minified.
 */
export function handleMinifiedObject(obj, parent, expand) {

    if (expand && !EXPAND_CONVERSIONS) { generateExpandConversions(); }

    const keyChanges = expand ? EXPAND_CONVERSIONS.keyChanges : CONVERSIONS.keyChanges;
    const valueChanges = expand ? EXPAND_CONVERSIONS.valueChanges : CONVERSIONS.valueChanges;

    const changeKeys = key => {

        let newKey = null;
        let doReplace = false;

        if (is.string(key) && keyChanges[key]) {

            doReplace = !parent || !keyChanges[key].p || keyChanges[key].p[parent] === 1;

            if (doReplace) {
                newKey = keyChanges[key].n;

                obj[newKey] = obj[key];
                delete obj[key];
                key = newKey;
            }
        }

        return key;
    };

    const changeValues = (key, val) => {

        let oneKey = null;
        let doReplace = false;

        if (is.string(val) && valueChanges[val]) {

            doReplace = !parent || !valueChanges[val].p || valueChanges[val].p[parent] === 1;

            if (doReplace) {

                oneKey = valueChanges[val].k;
                if (obj[oneKey] && oneKey === key) {
                    obj[oneKey] = valueChanges[val].n;
                }
            }
        }

    };

    dict.forEach(obj, (val, key) => {

        let currentKey = null;

        if (FORBIDDEN_CONVERSION_KEYS.has(key)) { return; }

        if (expand) {
            changeValues(key, val);
            changeKeys(key);
        } else {
            currentKey = changeKeys(key);
            changeValues(currentKey, val);
        }

        if (is.dict(val) || (is.array(val) && (is.dict(val[0]) || is.array(val[0])))) { // TODO: check for more optimal solution
            handleMinifiedObject(val, key, expand); // recursive call (using original, not shortened key)
        }

    });

}

/**
 * Minifying or expanding the specified action(s) (DOCS-892).
 *
 * In the case of expanding the operations (doExpand is true) the original operations
 * array must be used and modified. The version with the shortened operations cannot
 * be used on client side.
 *
 * This is different in the case of shrinking operations (doExpand is a falsy value).
 * This call is used, before operations are sent to the server. In this case are
 * problems, if the original array with the original operations is modified (55226).
 *
 * @param {Object[]} actions
 *  An array of actions as it is used to send actions to the server or when
 *  receiving actions from the server.
 *
 * @param {Boolean} [doExpand=false]
 *  Whether the specified actions shall be expanded or minified. If this parameter is
 *  set to a truthy value, the actions will be expanded. Otherwise (or if not specified)
 *  the actions will be minified.
 *
 * @returns {Opt<Object>}
 *  An array of actions.
 *  If 'doExpand' is truthy this is the reference to the original parameter 'actions'.
 *  If 'doExpand' is falsy, this must be a reference to a new generated array.
 */
export function handleMinifiedActions(actions, doExpand) {

    if (!actions) { return undefined; }

    // working on the original array for expansion, but using a clone for shrinking
    // -> a clone is required to avoid side effects with objects that directly
    //    included into operations during operation generation.
    if (!doExpand) { actions = json.deepClone(actions); }

    actions.forEach(action => {
        if (is.array(action.operations)) {
            action.operations.forEach(op => {
                handleMinifiedObject(op, '', doExpand);
            });
        }
    });

    return actions;
}

/**
 * Calculating an internal user id, that is saved at the node as attribute
 * and saved in the xml file. The base for this id is the app server id.
 *
 * @param {Number} id
 *  The user id used by the app suite server.
 *
 * @returns {Number|Null}
 *  The user id, that is saved in xml file and saved as node attribute.
 */
export function calculateUserId(id) {
    return is.number(id) ? (id + 123) * 10 - 123 : null;
}

/**
 * Calculating the app suite user id from the saved and transported user id.
 *
 * @param {Number} id
 *  The user id, that is saved in xml file and saved as node attribute.
 *
 * @returns {Number|Null}
 *  The user id used by the app suite server.
 */
export function resolveUserId(id) {
    return is.number(id) ? ((id + 123) / 10 - 123) : null;
}
