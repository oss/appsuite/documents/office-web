/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import moment from "$/moment";

import { getLocalNowAsUTC } from "@/io.ox/office/tk/utils/dateutils";

import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

export * from "@/io.ox/office/tk/utils/dateutils";

// types ==================================================================

declare module "moment" {

    interface Locale {
        _longDateFormat: Record<LongDateFormatKey, string>;
    }
}

// constants ==============================================================

// most short dates we know from spreadsheet
const NO_YEARS = {
    cs: "D.MMM",
    de: "DD.MMM",
    en: "MMM DD",
    es: "DD/MMM",
    fr: "DD/MMM",
    hu: "DD.MMM",
    it: "DD/MMM",
    ja: "M\u6708D\u65E5",
    lv: "MMM DD",
    nl: "DD-MMM",
    pl: "DD MMM",
    sk: "MMM DD",
    zh: "D-MMM"
};

// static methods ---------------------------------------------------------

/**
 * Converts a date value to a string.
 *
 * @param date
 *  The date value to be formatted.
 *
 * @param dateFormat
 *  The Moment.js format code to be used.
 *
 * @returns
 *  The formatted date.
 */
export function format(date: Date, dateFormat: string): string {
    const result = moment(date.getTime()).format(dateFormat);
    return result;
}

/**
 * @returns
 *  dateformat, day month and year
 */
export function getDateFormat(): string {
    return moment.localeData()._longDateFormat.L;
}

/**
 * @returns
 *  timeformat, normally hour and minutes
 */
export function getTimeFormat(): string {
    return moment.localeData()._longDateFormat.LT;
}

/**
 * @returns
 *  dateformat for complete weekdays
 */
export function getWeekDayFormat(): string {
    return "dddd";
}

/**
 * @returns
 *  dateformat, day month without year, but not implemented at the moment!
 */
export function getNoYearFormat(): string {
    const lang = LOCALE_DATA.language as keyof typeof NO_YEARS;
    return NO_YEARS[lang] || NO_YEARS.en;
}

/**
 * Returns the current local time faked as UTC time in ISO date format, and
 * resets seconds and milliseconds in the resulting date to zero.
 * This is necessary, because the round trip with MS Office requires this.
 * So local time "2012-04-17" and "13:38:00" results in a string in
 * ISO date format: "2012-04-17T13:38:00Z"
 * "Z" at the end for UTC (+00:00)
 * So string contains always a shifted time, except in UTC time zone.
 *
 * @returns
 *  The current local time as UTC date in an ISO string representation.
 */
export function getMSFormatIsoDateString(): string {

    // a date whose UTC timestamp looks like current local time
    const date = getLocalNowAsUTC();
    date.setUTCSeconds(0);
    date.setUTCMilliseconds(0);

    // the date string in ISO format, without milliseconds
    return date.toISOString().replace(".000Z", "Z");
}
