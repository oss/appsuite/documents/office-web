/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str, fun } from "@/io.ox/office/tk/algorithms";
import { createDiv, createStyle, insertHiddenNodes, Font } from "@/io.ox/office/tk/dom";
import { getDebugFlag } from "@/io.ox/office/tk/config";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

// constants ==================================================================

// default font families used as fallback for character width detection
const SYSTEM_FAMILIES = ["serif", "sans-serif", "monospace"];

// the characters used to measure the font size
const TEST_CHARS = "IOWlam01.".split("");

// skip check for locally installed fonts
const DEBUG_INSTALL_FONTS = getDebugFlag("office:install-webfonts");

// globals ====================================================================

// the keys of known installed fonts
const fontInstalledCache = new Set<string>();

// private functions ==========================================================

/**
 * Returns a best guess whether the specified font is actually installed on the
 * current operating system.
 */
function isFontInstalled(family: string): boolean {

    // immediately return true for installed fonts (fonts never become uninstalled)
    if (fontInstalledCache.has(family)) { return true; }

    // calculate the widths of all test characters
    const font = new Font(family, 100);
    const charWidths = TEST_CHARS.map(char => font.getTextWidth(char));

    // Compare character sizes of the font with all default font families as fallback.
    // If the character widths are all the same (i.e. the browser did never fall back
    // to the default fonts), the font exists on the system.
    const installed = SYSTEM_FAMILIES.every(sysFamily => {
        const sysFont = font.clone();
        sysFont.family += `,${sysFamily}`;
        return TEST_CHARS.every((char, index) => charWidths[index] === sysFont.getTextWidth(char));
    });

    // add the name of an installed font to the cache
    if (installed) { fontInstalledCache.add(family); }
    return installed;
}

/**
 * Imports the specified webfont file, and generates the markup for the at-rule
 * "@font-face".
 */
async function importFont(family: string, bold: boolean, italic: boolean): Promise<Pair<string>> {

    // build the base name of the WOFF file (without extension)
    let fileName = family.toLowerCase();
    if (bold) { fileName += "-bold"; }
    if (italic) { fileName += "-italic"; }

    // build font object for access of font metrics
    const font = new Font(family, 20, bold, italic);

    // try to download the binary webfont data
    try {

        // typecast the dynamic module import (Vite provides type definition for woff files)
        const { default: fontUrl } = await import(`./woff/${fileName}.woff?url`) as typeof import("*.woff");
        globalLogger.log(() => `$badge{webfonts} downloading "${fileName}.woff"`);

        // generate the markup of the @font-face rule
        const ruleMarkup = str.joinTail([
            "@font-face {",
            ` font-family: "${font.family}";`,
            ` font-weight: ${bold ? "bold" : "normal"};`,
            ` font-style: ${italic ? "italic" : "normal"};`,
            ` src: url("${fontUrl}") format("woff");`,
            "}"
        ], "\n");

        // generate the markup of the <span> element referencing the font
        const spanMarkup = `<span style="font:${font.getCanvasFont()};">A</span>`;

        // return the markup strings
        return [ruleMarkup, spanMarkup];

    } catch (error) {
        globalLogger.exception(error, `$badge{webfonts} "${fileName}.woff" cannot be downloaded`);
        throw error;
    }
}

/**
 * Imports missing fonts for the specified font family.
 *
 * @param origFamily
 *  The name of the original font family to check for availability.
 *
 * @param replaceFamily
 *  The name of the replacement font family to check for availability, and to
 *  load as webfonts if not available.
 *
 * @returns
 *  A promise that will fulfil with a boolean value whether the specified font
 *  family is available (either installed on the device, or loaded as webfont).
 */
async function importFontFamily(origFamily: string, replaceFamily: string): Promise<boolean> {

    // check for installed fonts on the device
    const isInstalled = [origFamily, replaceFamily].some(family => {
        const installed = !DEBUG_INSTALL_FONTS && isFontInstalled(family);
        if (installed) {
            globalLogger.log(() => `$badge{webfonts} font "${family}" is installed locally`);
        }
        return installed;
    });
    if (isInstalled) { return true; }

    try {

        // load all font styles in parallel
        const results = await Promise.all([
            importFont(replaceFamily, false, false),
            importFont(replaceFamily, true,  false),
            importFont(replaceFamily, false, true),
            importFont(replaceFamily, true,  true)
        ]);

        // create the <style> element with the @font-face rule
        const markup = results.map(result => result[0]).join("");
        document.head.append(createStyle(markup));

        // insert a <div> referencing the webfonts in the DOM (this starts using the @font-face rule)
        const helperDiv = insertHiddenNodes(createDiv());
        helperDiv.innerHTML = results.map(result => result[1]).join("");

        // trigger a reflow, at least Chrome needs this to make the font really available for rendering in the browser
        (() => helperDiv.clientWidth)();

    } catch {
        // loading a webfont file has failed
        globalLogger.warn(() => `$badge{webfonts} could not load all styles of font "${replaceFamily}"`);
        return false;
    }

    // Check and resolve the promise if the font is available in the browser. This must be checked async, because when
    // the code runs synchronous e.g. Chrome is not able to make the font available for rendering in the browser. This
    // check should normally resolve with true in the first round, but browser can behave differently here. So better
    // be save and check until the font is finally available or the interval is timed out.
    for (let idx = 0; idx < 20; idx += 1) {
        // wait for 100ms
        await new Promise(resolve => { window.setTimeout(resolve, 100); });
        // check if the font is available now
        if (isFontInstalled(replaceFamily)) {
            globalLogger.log(() => `$badge{webfonts} font "${replaceFamily}" available for rendering`);
            return true;
        }
    }

    // break when the font is still not available in the browser after 2 seconds
    globalLogger.warn(() => `$badge{webfonts} timeout while inserting font "${replaceFamily}" into DOM`);
    return false;
}

// public functions ===========================================================

/**
 * Make replacement fonts for "Calibri" and "Cambria" available in the browser,
 * when neither these two nor the replacement fonts exists on the client.
 *
 * @returns
 *  A promise that will fulfil when the font is available in the browser, or
 *  when font loading did not work, because everything works like before the
 *  replacement fonts existed. There is no error handling possible in a fail
 *  handler.
 */
export const importWebFonts = fun.once(async (): Promise<boolean> => {
    // no short-circut: all fonts must be loaded
    const hasSansSerif = await importFontFamily("Calibri", "Carlito");
    const hasSerif = await importFontFamily("Cambria", "Caladea");
    return hasSansSerif && hasSerif;
});
