/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, str, dict, pick, json } from "@/io.ox/office/tk/algorithms";
import { type NodeOrJQuery, toJQuery } from "@/io.ox/office/tk/dom";
import type { OpColor } from "@/io.ox/office/editframework/utils/color";
import type { OpLineHeight } from "@/io.ox/office/editframework/utils/lineheight";

// types ======================================================================

/**
 * Extracts the types of the attribute family keys from an attribute set type,
 * excluding the special key "styleId" carrying the stylesheet identifier.
 *
 * @example
 *  AttrFamilyT<{ cell: CellT, row: RowT, col: ColT, styleId?: string }>
 *  // result type: "cell" | "row" | "col"
 */
export type AttrFamiliesOf<AttrSetT> = Exclude<KeysOf<AttrSetT>, "styleId">;

/**
 * Extracts the types of the attribute bags from an attribute set type,
 * excluding the special property "styleId" carrying the stylesheet identifier.
 *
 * @example
 *  AttrBagT<{ cell: CellT, row: RowT, col: ColT, styleId?: string }>
 *  // result type: CellT | RowT | ColT
 */
export type AttrBagsOf<AttrSetT> = AttrSetT[AttrFamiliesOf<AttrSetT>];

/**
 * Extracts the types of the attribute bags from an attribute set type,
 * excluding the special property "styleId" carrying the stylesheet identifier,
 * and converts the bag types to partial bags.
 *
 * @example
 *  PtAttrBagT<{ cell: CellT, row: RowT, col: ColT, styleId?: string }>
 *  // result type: Partial<CellT> | Partial<RowT> | Partial<ColT>
 */
export type PtAttrBagsOf<AttrSetT> = Partial<AttrSetT[AttrFamiliesOf<AttrSetT>]>;

/**
 * Helper type that restricts the type of the properties of attribute sets to
 * objects without interfering with the type of the keys. Additionally, an
 * optional string property "styleId" will be declared. Intended to be used in
 * generic type constraints ("extends" clauses).
 *
 * @example
 *  class MyClass<AttrSetT extends AttrSetConstraint<AttrSetT>> {
 *    ...
 *  }
 */
export type AttrSetConstraint<AttrSetT> = { [K in AttrFamiliesOf<AttrSetT>]: object } & StyledAttributeSet;

/**
 * Converts the passed attribute bag type to a partial attribute bag type (all
 * properties optional) including `null` values (used e.g. to notify the "mixed
 * state" when getting attributes from a selection, or to reset explicit
 * attributes when creating document operations).
 */
export type MixedAttrBag<AttrBagT extends object> = Partial<MapUnion<AttrBagT, null>>;

/**
 * Converts the passed complete attribute set type to a partial attribute set
 * type. The attribute bags of all families, as well as all attributes in the
 * bags, will be optional.
 */
export type PtAttrSet<AttrSetT extends AttrSetConstraint<AttrSetT>> = {
    [KT in KeysOf<AttrSetT>]?: Partial<AttrSetT[KT]>;
};

/**
 * The identifier of a stylesheet carried by an attribute set. The stylesheet
 * identifier property is always optional (also in complete attribute sets),
 * e.g. because of missing support depending on file format.
 */
export interface StyledAttributeSet {
    styleId?: string;
}

/**
 * Type definition of attribute maps for the "character" attribute family.
 */
export interface CharacterAttributes {

    /**
     * Name of the font family used to render text contents.
     */
    fontName: string;

    /**
     * Size of the font used to render text contents, in *points*.
     */
    fontSize: number;

    /**
     * Specifies whether to render text contents with bold characters.
     */
    bold: boolean;

    /**
     * Specifies whether to render text contents with italic characters.
     */
    italic: boolean;

    /**
     * Specifies whether to underline text contents.
     */
    underline: boolean;

    /**
     * Specifies whether to strike text contents.
     */
    strike: "none" | "single" | "double";

    /**
     * Color used to render text contents, underlines, and strike lines.
     */
    color: OpColor;

    /**
     * Field character attributes
     */
    field: {
        "text:fixed"?: string;
        pageNumFormat?: string;
        dateFormat?: string;
        dateValue?: string | null;
        fixed?: string;
        name?: string;
    };

    /**
     * TODO
     */
    language: string;

    /**
     * TODO
     */
    autoDateField: string | boolean;

    /**
     * TODO
     */
    url: string;
}

/**
 * Type definition of a character attribute set.
 */
export interface CharacterAttributeSet extends StyledAttributeSet, ChangesAttributeSet {
    character: CharacterAttributes;
}

/**
 * Partial character attribute set (all families and attributes are optional).
 */
export type PtCharacterAttributeSet = PtAttrSet<CharacterAttributeSet>;

/**
 * Type definition of attribute maps for the "paragraph" attribute family.
 */
export interface ParagraphAttributes {
    alignment: string;
    fillColor: OpColor;
    lineHeight: OpLineHeight;
    indentFirstLine: number;
    indentLeft: number;
    indentRight: number;
}

/**
 * Type definition of a paragraph attribute set.
 */
export interface ParagraphAttributeSet extends CharacterAttributeSet {
    paragraph: ParagraphAttributes;
}

/**
 * Partial paragraph attribute set (all families and attributes are optional).
 */
export type PtParagraphAttributeSet = PtAttrSet<ParagraphAttributeSet>;

/**
 * Defining a change track info object
 */
export interface ChangeTrackInfo {

    /**
     * The change track author.
     */
    author: string;

    /**
     * The date of the change track.
     */
    date: string;

    /**
     * The user id of the change track.
     */
    userId: string;

    /**
     * The change track author.
     */
    authorId?: string;

    /**
     * The old element attributes, that are required, if the change track is rejected. This is only necessary for change track type “modified”.
     */
    attrs?: object;
}

/**
 * Type definition of attribute maps for the "changetrack" attribute family.
 */
export interface ChangesAttributes {

    /**
     * The change track information about the insertion.
     */
    inserted: ChangeTrackInfo | null;

    /**
     * The change track information about the removal.
     */
    removed: ChangeTrackInfo | null;

    /**
     * The change track information about the modification.
     */
    modified: ChangeTrackInfo | null;
}

/**
 * Type definition of a paragraph attribute set.
 */
export interface ChangesAttributeSet {
    changes: ChangesAttributes;
}

/**
 * Partial paragraph attribute set (all families and attributes are optional).
 */
export type PtChangesAttributeSet = PtAttrSet<ChangesAttributeSet>;

// constants ==================================================================

/**
 * The internal identifier of the major scheme font in OOXML documents.
 */
export const MAJOR_FONT_KEY = "+mj-lt";

/**
 * The internal identifier of the minor scheme font in OOXML documents.
 */
export const MINOR_FONT_KEY = "+mn-lt";

// public functions ===========================================================

/**
 * Creates a clone of the passed attribute set. By default, a two-level deep
 * copy will be created (the attribute maps inside the attribute set will be
 * cloned shallowly, but the attribute value will be copied by value).
 *
 * @param attrSet
 *  The attribute set to be cloned.
 *
 * @param [cloneValues=false]
 *  If set to `true`, the attribute values will be cloned deeply too.
 *
 * @returns
 *  The clone of the passed attribute set.
 */
export function cloneAttributeSet<AttrSetT extends object>(attrSet: AttrSetT, cloneValues?: boolean): AttrSetT {
    return cloneValues ? json.deepClone(attrSet) : dict.mapDict(attrSet, json.flatClone) as unknown as AttrSetT;
}

/**
 * Returns whether the passed partial attribute set contains the specified
 * attribute, regardless of its value.
 *
 * @param attrSet
 *  A partial attribute set to be checked for an attribute.
 *
 * @param family
 *  The family of the attribute to be checked.
 *
 * @param name
 *  The name of the attribute to be checked.
 *
 * @returns
 *  Whether the passed attribute set contains the specified attribute.
 */
export function hasAttribute(attrSet: Dict, family: string, name: string): boolean {
    const attrs = pick.dict(attrSet, family);
    return !!attrs && (name in attrs);
}

/**
 * Returns whether the values of all attributes in the second attribute set are
 * equal to the values of the first attribute set.
 *
 * @param attrSet1
 *  The first attribute set. It may contain more attributes than the second
 *  attribute set passed to this method.
 *
 * @param attrSet2
 *  A partial attribute set. Each formatting attribute contained in this set
 *  must exist in the first attribute set, and their values must be equal.
 *
 * @returns
 *  Whether the values of all attributes in the second attribute set are equal
 *  to the values of the first attribute set.
 */
export function matchesAttributesSet(attrSet1: Dict, attrSet2: Dict): boolean {
    return dict.every(attrSet2, (attrs2, family) => {
        const attrs1 = pick.dict(attrSet1, family);
        return !!attrs1 && is.dict(attrs2) && dict.every(attrs2, (value, name) => _.isEqual(value, attrs1[name]));
    });
}

/**
 * Inserts a new attribute, or updates an existing attribute in the passed
 * partial attribute set.
 *
 * @param attrSet
 *  A partial attribute set to be changed.
 *
 * @param family
 *  The family of the attribute to be changed.
 *
 * @param name
 *  The name of the attribute to be changed.
 *
 * @param value
 *  The new value of the attribute. The value null will be inserted too, see
 *  function `deleteAttribute()` to delete an attribute from the attribute set.
 *
 * @param [missing=false]
 *  If set to `true`, only missing attributes will be inserted; existing
 *  attributes will not be changed.
 */
export function insertAttribute(attrSet: Dict, family: string, name: string, value: unknown, missing?: boolean): void {
    const attrs = pick.dict(attrSet, family) ?? (attrSet[family] = dict.create());
    if (!missing || !(name in attrs)) { attrs[name] = value; }
}

/**
 * Deletes an attribute from the passed partial attribute set.
 *
 * @param attrSet
 *  An partial attribute set to be changed.
 *
 * @param family
 *  The family of the attribute to be deleted.
 *
 * @param name
 *  The name of the attribute to be deleted.
 */
export function deleteAttribute(attrSet: Dict, family: string, name: string): void {
    const attrs = pick.dict(attrSet, family);
    if (attrs) {
        delete attrs[name];
        if (is.empty(attrs)) {
            delete attrSet[family];
        }
    }
}

/**
 * Adds or removes an attribute value in the specified attribute map.
 *
 * @param attrs
 *  A simple map with formatting attributes to be updated (this MUST NOT be a
 *  structured attribute set with sub objects mapped by family!).
 *
 * @param name
 *  The name of the formatting attribute to be changed.
 *
 * @param value
 *  The new value of the formatting attribute. If set to the value `null`, the
 *  specified attribute will be removed from the map.
 *
 * @returns
 *  Whether the attribute value has been changed (inserted, deleted, or updated
 *  to another value).
 */
export function updateAttribute<
    AttrsT extends object,
    NameT extends KeysOf<AttrsT>
>(
    attrs: Partial<AttrsT>,
    name: NameT,
    value: AttrsT[NameT]
): boolean {

    // whether this attribute has changed
    let changed = false;

    // remove the attribute from the map, if the passed value is null
    if (value === null) {
        if (name in attrs) {
            delete attrs[name];
            changed = true;
        }
    } else if (!_.isEqual(attrs[name], value)) {
        attrs[name] = json.deepClone(value);
        changed = true;
    }

    return changed;
}

/**
 * Returns the attribute set containing all explicit attributes stored in the
 * passed DOM element.
 *
 * @param node
 *  The DOM element whose explicit attribute set will be returned. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param [direct]
 *  If set to `true`, the returned attribute set will be a reference to the
 *  original object stored in the passed DOM node, which MUST NOT be modified!
 *  By default, a deep clone of the attribute set will be returned that can be
 *  freely modified.
 *
 * @returns
 *  The partial attribute set if existing, otherwise an empty object.
 */
export function getExplicitAttributeSet(node: NodeOrJQuery, direct?: false): Dict;
export function getExplicitAttributeSet(node: NodeOrJQuery, direct: true): Readonly<Dict>;
// implementation
export function getExplicitAttributeSet(node: NodeOrJQuery, direct?: boolean): Dict {
    // the original and complete attribute map
    const attrs = toJQuery(node, true).data("attributes") as unknown;
    // return attributes directly or as a deep clone
    return !is.dict(attrs) ? dict.create() : direct ? attrs : json.deepClone(attrs);
}

/**
 * Returns the attribute set containing all explicit attributes stored in the
 * passed DOM element.
 *
 * @param node
 *  The DOM element whose explicit attribute set will be returned. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param family
 *  The style family.
 *
 * @param [direct=false]
 *  If set to `true`, the returned attribute map will be a reference to the
 *  original object stored in the passed DOM node, which MUST NOT be modified!
 *  By default, a deep clone of the attribute map will be returned that can be
 *  freely modified.
 *
 * @returns
 *  The partial attribute set if existing, otherwise an empty object.
 */
export function getExplicitAttributes(node: NodeOrJQuery, family: string, direct?: false): Dict;
export function getExplicitAttributes(node: NodeOrJQuery, family: string, direct: true): Readonly<Dict>;
// implementation
export function getExplicitAttributes(node: NodeOrJQuery, family: string, direct?: boolean): Dict {
    // do not clone the entire attribute set
    const attrSet = getExplicitAttributeSet(node, true);
    const attrs = pick.dict(attrSet, family);
    // return attributes directly or as a deep clone
    return !attrs ? dict.create() : direct ? attrs : json.deepClone(attrs);
}

/**
 * Stores the passed explicit attribute set into the DOM node.
 *
 * @param node
 *  The DOM element whose explicit attribute set will be changed. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param attrSet
 *  The explicit attribute set to be stored into the passed DOM node.
 */
export function setExplicitAttributeSet(node: NodeOrJQuery, attrSet: Dict): void {
    toJQuery(node, true).data("attributes", attrSet);
}

/**
 * Returns the identifier of the style sheet referred by the passed
 * element.
 *
 * @param node
 *  The DOM element whose style sheet identifier will be returned. If this
 *  object is a jQuery collection, uses the first DOM node it contains.
 *
 * @returns
 *  The style sheet identifier at the passed element.
 */
export function getElementStyleId(node: JQuery): string | null {
    const styleId = getExplicitAttributeSet(node, true).styleId;
    return is.string(styleId) ? styleId : null;
}

/**
 * Returns whether the passed elements contain equal formatting attributes.
 *
 * @param node1
 *  The first DOM element whose formatting attributes will be compared with the
 *  attributes of the other passed DOM element. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @param node2
 *  The second DOM element whose formatting attributes will be compared with
 *  the attributes of the other passed DOM element. If this object is a jQuery
 *  collection, uses the first DOM node it contains.
 *
 * @returns
 *  Whether both DOM elements contain equal explicit formatting attributes.
 */
export function hasEqualElementAttributes(node1: NodeOrJQuery, node2: NodeOrJQuery): boolean {
    return (node1 === node2) || _.isEqual(getExplicitAttributeSet(node1, true), getExplicitAttributeSet(node2, true));
}

/**
 * Returns the value of the CSS property `text-decoration` for the passed
 * character attributes.
 *
 * @param charAttrs
 *  A map of character attributes. Uses the attributes `underline` and `strike`
 *  to generate the CSS property value.
 *
 * @returns
 *  The resulting value of the CSS property `text-decoration`.
 */
export function getCssTextDecoration(charAttrs: CharacterAttributes): string {
    return str.concatTokens(
        charAttrs.underline ? "underline" : null,
        (is.string(charAttrs.strike) && (charAttrs.strike !== "none")) ? "line-through" : null
    ) || "none";
}

/**
 * Returns whether the passed value is a JSON color value of type "scheme".
 */
export function isColorThemed(color: unknown): color is OpColor {
    return is.dict(color) && (color.type === "scheme") && is.string(color.value);
}

/**
 * Returns whether the passed attribute map contains a themed fill style.
 */
export function isFillThemed(fillAttrs: unknown): boolean {
    return is.dict(fillAttrs) && (isColorThemed(fillAttrs.color) || (fillAttrs.type === "gradient") || (fillAttrs.type === "pattern"));
}

/**
 * Returns whether the passed attribute map contains a themed line style.
 */
export function isLineThemed(lineAttrs: unknown): boolean {
    return is.dict(lineAttrs) && isColorThemed(lineAttrs.color);
}

/**
 * Returns whether the passed character attributes contain a reference to a
 * theme font.
 */
export function isCharacterFontThemed(charAttrs: unknown): boolean {
    const fontName = pick.string(charAttrs, "fontName");
    return (fontName === MAJOR_FONT_KEY) || (fontName === MINOR_FONT_KEY);
}
