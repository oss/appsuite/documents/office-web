/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { jpromise } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import type { Operation } from "@/io.ox/office/editframework/utils/operations";

// types ======================================================================

/**
 * Optional settings for an `OperationError`.
 */
export interface OperationErrorConfig {

    /**
     * An error code describing the cause of throwing an `OperationError`.
     * Default value is "operation".
     */
    cause?: string;

    /**
     * The JSON operation caused to throw an `OperationError`. May be omitted
     * in situations without a specific operation available.
     */
    operation?: Operation;
}

// class OperationError =======================================================

/**
 * A special exception for document operations thrown by methods of the class
 * `OperationContext`.
 *
 * @param operation
 *  The operation caused the exception.
 */
export class OperationError extends Error {

    // static functions -------------------------------------------------------

    /**
     * Throws a specific message code as object with `cause` property.
     *
     * @param msgCode
     *  The message code to be thrown.
     *
     * @param [errorMsg]
     *  A log message for internal errors to be printed to the browser console.
     */
    static throwCause(msgCode: string, errorMsg?: string): never {
        if (errorMsg) { globalLogger.error(errorMsg); }
        throw new OperationError(errorMsg || "generator failed", { cause: msgCode });
    }

    /**
     * Returns an immediately rejected promise containing a message code as
     * object with `cause` property.
     *
     * @param msgCode
     *  The message code to be thrown.
     *
     * @param [errorMsg]
     *  A log message for internal errors to be printed to the browser console.
     */
    static rejectCause<FT = void>(msgCode: string, errorMsg?: string): JPromise<FT> {
        return jpromise.new(() => OperationError.throwCause(msgCode, errorMsg));
    }

    /**
     * Throws a specific message code as object with `cause` property.
     *
     * @param msgCode
     *  The message code to be thrown. If this value is an empty string or
     *  nullish, no error will be thrown.
     *
     * @param [errorMsg]
     *  A log message for internal errors to be printed to the browser console.
     */
    static throwCauseIf(msgCode: Nullable<string>, errorMsg?: string): void {
        if (msgCode) { OperationError.throwCause(msgCode, errorMsg); }
    }

    /**
     * Throws a specific message code as object with `cause` property, if some
     * condition is not met.
     *
     * @param condition
     *  The condition expected to be truthy.
     *
     * @param msgCode
     *  The message code to be thrown if the condition is falsy.
     *
     * @param [errorMsg]
     *  A log message for internal errors to be printed to the browser console.
     */
    static assertCause(condition: unknown, msgCode: string, errorMsg?: string): asserts condition {
        if (!condition) { OperationError.throwCause(msgCode, errorMsg); }
    }

    // properties -------------------------------------------------------------

    override readonly name = "OperationError";

    override readonly cause: string;
    readonly operation: Opt<Operation>;

    // constructor ------------------------------------------------------------

    constructor(message: string, config?: OperationErrorConfig) {
        super(message);
        this.cause = config?.cause ?? "operation";
        this.operation = config?.operation;
    }
}
