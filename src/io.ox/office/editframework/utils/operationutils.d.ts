/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Operation } from "@/io.ox/office/editframework/utils/operations";

// types ======================================================================

/**
 * An action object that collects multiple JSON document operations in an
 * array property, amongst other settings.
 */
export interface OperationAction {

    /**
     * The JSON document operations making up the action.
     */
    operations: Operation[];

    /**
     * Whether this action with its operations has been sent to the server
     * already.
     */
    sentToServer?: boolean;

    /**
     * Additional user data needed for operation transformation.
     */
    userData?: object;
}

// functions ==================================================================

export function handleMinifiedObject<T extends object>(obj: T, parent?: string, expand?: boolean): void;
export function handleMinifiedActions(actions: OperationAction[], expand?: boolean): OperationAction[];
