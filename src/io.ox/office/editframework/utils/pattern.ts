/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import { Canvas } from "@/io.ox/office/tk/canvas";

import { type OpColor, AutoColorType, Color } from "@/io.ox/office/editframework/utils/color";
import type { ThemeModel } from "@/io.ox/office/editframework/model/themecollection";

// types ======================================================================

interface PatternMapEntry {
    url: string;
    pattern: CanvasPattern;
}

// constants ==================================================================

// the bitmaps of all predefined patterns
const PRESET_PATTERN_MAP: Dict<readonly number[]> = {
    cross:      [0xFF, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80],
    dashDnDiag: [0x88, 0x44, 0x22, 0x11, 0x00, 0x00, 0x00, 0x00],
    dashHorz:   [0xF0, 0x00, 0x00, 0x00, 0x0F, 0x00, 0x00, 0x00],
    dashUpDiag: [0x11, 0x22, 0x44, 0x88, 0x00, 0x00, 0x00, 0x00],
    dashVert:   [0x80, 0x80, 0x80, 0x80, 0x08, 0x08, 0x08, 0x08],
    diagBrick:  [0x01, 0x02, 0x04, 0x08, 0x18, 0x24, 0x42, 0x81],
    diagCross:  [0xC1, 0x63, 0x36, 0x1C, 0x1C, 0x36, 0x63, 0xC1],
    divot:      [0x40, 0x80, 0x40, 0x00, 0x08, 0x04, 0x08, 0x00],
    dkDnDiag:   [0xCC, 0x66, 0x33, 0x99, 0xCC, 0x66, 0x33, 0x99],
    dkHorz:     [0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00],
    dkUpDiag:   [0x33, 0x66, 0xCC, 0x99, 0x33, 0x66, 0xCC, 0x99],
    dkVert:     [0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC],
    dnDiag:     [0xC0, 0x60, 0x30, 0x18, 0x0C, 0x06, 0x03, 0x81],
    dotDmnd:    [0x80, 0x00, 0x22, 0x00, 0x08, 0x00, 0x22, 0x00],
    dotGrid:    [0xAA, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00],
    horz:       [0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
    horzBrick:  [0xFF, 0x80, 0x80, 0x80, 0xFF, 0x08, 0x08, 0x08],
    lgCheck:    [0xF0, 0xF0, 0xF0, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F],
    lgConfetti: [0xC0, 0xD8, 0x1B, 0x03, 0x30, 0xB1, 0x8D, 0x0C],
    lgGrid:     [0xFF, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80],
    ltDnDiag:   [0x88, 0x44, 0x22, 0x11, 0x88, 0x44, 0x22, 0x11],
    ltHorz:     [0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00],
    ltUpDiag:   [0x11, 0x22, 0x44, 0x88, 0x11, 0x22, 0x44, 0x88],
    ltVert:     [0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88, 0x88],
    narHorz:    [0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00],
    narVert:    [0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA],
    none:       [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
    openDmnd:   [0x80, 0x41, 0x22, 0x14, 0x08, 0x14, 0x22, 0x41],
    pct10:      [0x80, 0x00, 0x08, 0x00, 0x80, 0x00, 0x08, 0x00],
    pct20:      [0x88, 0x00, 0x22, 0x00, 0x88, 0x00, 0x22, 0x00],
    pct25:      [0x88, 0x22, 0x88, 0x22, 0x88, 0x22, 0x88, 0x22],
    pct30:      [0x88, 0x55, 0x22, 0x55, 0x88, 0x55, 0x22, 0x55],
    pct40:      [0x8A, 0x55, 0xAA, 0x55, 0xA8, 0x55, 0xAA, 0x55],
    pct5:       [0x80, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00],
    pct50:      [0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55],
    pct60:      [0xEE, 0x55, 0xBB, 0x55, 0xEE, 0x55, 0xBB, 0x55],
    pct70:      [0xEE, 0xBB, 0xEE, 0xBB, 0xEE, 0xBB, 0xEE, 0xBB],
    pct75:      [0xEE, 0xFF, 0xBB, 0xFF, 0xEE, 0xFF, 0xBB, 0xFF],
    pct80:      [0xFE, 0xFF, 0xEF, 0xFF, 0xFE, 0xFF, 0xEF, 0xFF],
    pct90:      [0xFE, 0xFF, 0xFF, 0xFF, 0xEF, 0xFF, 0xFF, 0xFF],
    plaid:      [0xF0, 0xF0, 0xF0, 0xF0, 0xAA, 0x55, 0xAA, 0x55],
    shingle:    [0x81, 0x42, 0x24, 0x18, 0x06, 0x01, 0x80, 0x80],
    smCheck:    [0xCC, 0xCC, 0x33, 0x33, 0xCC, 0xCC, 0x33, 0x33],
    smConfetti: [0x80, 0x10, 0x02, 0x40, 0x04, 0x20, 0x01, 0x08],
    smGrid:     [0xFF, 0x88, 0x88, 0x88, 0xFF, 0x88, 0x88, 0x88],
    solid:      [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
    solidDmnd:  [0x10, 0x38, 0x7C, 0xFE, 0x7C, 0x38, 0x10, 0x00],
    sphere:     [0x77, 0x98, 0xF8, 0xF8, 0x77, 0x89, 0x8F, 0x8F],
    trellis:    [0xFF, 0xCC, 0xFF, 0x33, 0xFF, 0xCC, 0xFF, 0x33],
    upDiag:     [0x81, 0x03, 0x06, 0x0C, 0x18, 0x30, 0x60, 0xC0],
    vert:       [0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80],
    wave:       [0x0C, 0x92, 0x60, 0x00, 0x0C, 0x92, 0x60, 0x00],
    wdDnDiag:   [0xC1, 0xE0, 0x70, 0x38, 0x1C, 0x0E, 0x07, 0x83],
    wdUpDiag:   [0x83, 0x07, 0x0E, 0x1C, 0x38, 0x70, 0xE0, 0xC1],
    weave:      [0x88, 0x41, 0x22, 0x15, 0x88, 0x45, 0x22, 0x54],
    zigZag:     [0x81, 0x42, 0x24, 0x18, 0x81, 0x42, 0x24, 0x18]
};

// globals ====================================================================

// a helper canvas element used to generate the fill patterns
const canvas = new Canvas({ location: { width: 8, height: 8 }, settings: { willReadFrequently: true }, _kind: "singleton" });

// a cache with all generated fill patterns
const patternCache = new Map<string, PatternMapEntry>();

// private functions ==========================================================

/**
 * Creates all data needed to render the specified predefined fill pattern.
 * Internally, all generated patterns will be cached, and will be returned
 * immediately on subsequent calls.
 *
 * @param patternId
 *  The identifier of a preset pattern to generate the rendering data for. If
 *  this value is not a supported pattern identifier, this function generates a
 *  pattern consisting of the background color only.
 *
 * @param backColor
 *  The background color (or fill color), as JSON color value used in document
 *  operations.
 *
 * @param foreColor
 *  The foreground color (or pattern color), as JSON color value used in
 *  document operations.
 *
 * @param themeModel
 *  The model of a document theme used to map scheme color names to color
 *  values.
 *
 * @returns
 *  The pattern descriptor.
 */
function generatePattern(patternId: string, backColor: OpColor, foreColor: OpColor, themeModel: ThemeModel): PatternMapEntry {

    // validate the pattern identifier
    if (!(patternId in PRESET_PATTERN_MAP)) { patternId = "none"; }

    // parse the passed JSON colors, and resolve to color descriptors
    const backColorDesc = Color.parseJSON(backColor).resolve(AutoColorType.FILL, themeModel);
    const foreColorDesc = Color.parseJSON(foreColor).resolve(AutoColorType.LINE, themeModel);

    // the cache key consists of the pattern identifier, and the resulting CSS colors (with alpha)
    const cacheKey = `${patternId}:${backColorDesc.css}:${foreColorDesc.css}`;

    // return an existing pattern, or create a new pattern
    return map.upsert(patternCache, cacheKey, () => {

        // fill with background color, draw the foreground pixels of the pattern
        canvas.renderPixels(context => {
            context.setColor(backColorDesc.css);
            context.fillRect(0, 0, 8, 8);
            context.setColor(foreColorDesc.css);
            for (const [y, bits] of PRESET_PATTERN_MAP[patternId].entries()) {
                for (let x = 0, mask = 128; x < 8; x += 1, mask /= 2) {
                    if (bits & mask) { context.drawPixel(x, y); }
                }
            }
        });

        // create a fill pattern from the canvas contents
        return canvas.render(context => ({
            url: canvas.getDataURL(),
            pattern: context.createPattern(canvas)
        }));
    });
}

// public functions ===========================================================

/**
 * Creates the data URL of a fill bitmap for the specified predefined fill
 * pattern.
 *
 * @param patternId
 *  The identifier of a preset pattern to generate the rendering data for. If
 *  this value is not a supported pattern identifier, this function generates a
 *  pattern consisting of the background color only.
 *
 * @param backColor
 *  The background color (or fill color), as JSON color value used in document
 *  operations.
 *
 * @param foreColor
 *  The foreground color (or pattern color), as JSON color value used in
 *  document operations.
 *
 * @param themeModel
 *  The model of a document theme used to map scheme color names to color
 *  values.
 *
 * @returns
 *  The data URL of a fill bitmap.
 */
export function createPatternDataURL(patternId: string, backColor: OpColor, foreColor: OpColor, themeModel: ThemeModel): string {
    return generatePattern(patternId, backColor, foreColor, themeModel).url;
}

/**
 * Creates a fill pattern structure for canvas elements for the specified
 * predefined fill pattern.
 *
 * @param patternId
 *  The identifier of a preset pattern to generate the rendering data for. If
 *  this value is not a supported pattern identifier, this function generates a
 *  pattern consisting of the background color only.
 *
 * @param backColor
 *  The background color (or fill color), as JSON color value used in document
 *  operations.
 *
 * @param foreColor
 *  The foreground color (or pattern color), as JSON color value used in
 *  document operations.
 *
 * @param themeModel
 *  The model of a document theme used to map scheme color names to color
 *  values.
 *
 * @returns
 *  The canvas pattern object. Can be used directly as value for the properties
 *  of a CanvasRenderingContext2D, or as value for the methods of a
 *  `Canvas2DContext` from a `Canvas` instance.
 */
export function createCanvasPattern(patternId: string, backColor: OpColor, foreColor: OpColor, themeModel: ThemeModel): CanvasPattern {
    return generatePattern(patternId, backColor, foreColor, themeModel).pattern;
}
