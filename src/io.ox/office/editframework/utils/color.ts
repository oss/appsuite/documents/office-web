/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, to, math, fun, ary, dict, pick, json } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import type { AlphaModel, ColorDescriptor } from "@/io.ox/office/tk/dom";
import { ColorType, RGBModel, HSLModel, getSystemColor, parseCssColor } from "@/io.ox/office/tk/dom";
import type { ThemeModel } from "@/io.ox/office/editframework/model/themecollection";

// types ======================================================================

/**
 * Enumeration of all types of a color supported in document operations.
 *
 * - "auto": The automatic color.
 * - "rgb": RGB color model (color channels as 6-digit hexadecimal value).
 * - "crgb": CRGB color model (color channels as percentage, no gamma).
 * - "hsl": HSL color model (hue, saturation, luminance).
 * - "preset": Predefined colors from a fixed color palette.
 * - "system": Specific colors depending on the operating system.
 * - "scheme": Scheme color from current document theme.
 */
export type OpColorType = "auto" | "rgb" | "crgb" | "hsl" | "preset" | "system" | "scheme";

/**
 * The JSON representation of a color transformation, as used in document
 * operations.
 */
export interface OpColorTransform {
    type: string;
    value?: number;
}

/**
 * The JSON representation of a color, as used in document operations.
 */
export interface OpColor {
    type: string;
    value?: string | Dict;
    transformations?: OpColorTransform[];
    fallbackValue?: string;
}

/**
 * Specifies the context for resolving the automatic color.
 */
export enum AutoColorType {

    /**
     * Resolve a text color (automatic color mapped to black).
     */
    TEXT = "text",

    /**
     * Resolve a line color (automatic color mapped to black).
     */
    LINE = "line",

    /**
     * Resolve a fill color (automatic color mapped to full transparency).
     */
    FILL = "fill"
}

/**
 * All available identifiers for preset colors.
 */
export type PresetColorId = keyof typeof PRESET_COLOR_MAP;

/**
 * All available identifiers for system colors.
 */
export type SystemColorId = keyof typeof SYSTEM_COLOR_MAP;

/**
 * All available identifiers for scheme colors.
 */
export type SchemeColorId =
    "text1" | "text2" | "background1" | "background2" |
    "dark1" | "dark2" | "light1" | "light2" |
    "accent1" | "accent2" | "accent3" | "accent4" | "accent5" | "accent6" |
    "hyperlink" | "followedHyperlink";

/**
 * The identifiers of all available variable color transformations (with value
 * property), as used in document operations.
 */
export type OpVarTransformType = keyof typeof VAR_TRANSFORMATIONS;

/**
 * The identifiers of all available constant color transformations (without
 * value property), as used in document operations.
 */
export type OpConstTransformType = keyof typeof CONST_TRANSFORMATIONS;

/**
 * The identifiers of all available color transformations, as used in document
 * operations.
 */
export type OpTransformType = OpVarTransformType | OpConstTransformType;

/**
 * Short syntax for a variable color transformation (with value property), used
 * in helper functions generating colors for document operations.
 */
export type OpVarTransformSpec = Record<OpVarTransformType, number>;

/**
 * Short syntax for a constant color transformation (without value property),
 * used in helper functions generating colors for document operations.
 */
export type OpConstTransformSpec = Record<OpConstTransformType, null>;

/**
 * Short syntax for a color transformation, used in helper functions generating
 * colors for document operations.
 */
export type OpTransformSpec = OpVarTransformSpec & OpConstTransformSpec;

// constants ==================================================================

// hue channel conversion factor (units per degree)
const HUE_PER_DEG = 60000;

// the gamma correction factors (intended to be similar to MSO)
const INV_GAMMA = 2.27;
const GAMMA = 1 / INV_GAMMA;

// maps all OOXML preset color names (camel-case!) to RGB values
const PRESET_COLOR_MAP = {
    aliceBlue: "f0f8ff",        antiqueWhite: "faebd7",         aqua: "00ffff",             aquamarine: "7fffd4",
    azure: "f0ffff",            beige: "f5f5dc",                bisque: "ffe4c4",           black: "000000",
    blanchedAlmond: "ffebcd",   blue: "0000ff",                 blueViolet: "8a2be2",       brown: "a52a2a",
    burlyWood: "deb887",        cadetBlue: "5f9ea0",            chartreuse: "7fff00",       chocolate: "d2691e",
    coral: "ff7f50",            cornflowerBlue: "6495ed",       cornsilk: "fff8dc",         crimson: "dc143c",
    cyan: "00ffff",             darkBlue: "00008b",             darkCyan: "008b8b",         darkGoldenrod: "b8860b",
    darkGray: "a9a9a9",         darkGreen: "006400",            darkGrey: "a9a9a9",         darkKhaki: "bdb76b",
    darkMagenta: "8b008b",      darkOliveGreen: "556b2f",       darkOrange: "ff8c00",       darkOrchid: "9932cc",
    darkRed: "8b0000",          darkSalmon: "e9967a",           darkSeaGreen: "8fbc8f",     darkSlateBlue: "483d8b",
    darkSlateGray: "2f4f4f",    darkSlateGrey: "2f4f4f",        darkTurquoise: "00ced1",    darkViolet: "9400d3",
    deepPink: "ff1493",         deepSkyBlue: "00bfff",          dimGray: "696969",          dimGrey: "696969",
    dkBlue: "00008b",           dkCyan: "008b8b",               dkGoldenrod: "b8860b",      dkGray: "a9a9a9",
    dkGreen: "006400",          dkGrey: "a9a9a9",               dkKhaki: "bdb76b",          dkMagenta: "8b008b",
    dkOliveGreen: "556b2f",     dkOrange: "ff8c00",             dkOrchid: "9932cc",         dkRed: "8b0000",
    dkSalmon: "e9967a",         dkSeaGreen: "8fbc8b",           dkSlateBlue: "483d8b",      dkSlateGray: "2f4f4f",
    dkSlateGrey: "2f4f4f",      dkTurquoise: "00ced1",          dkViolet: "9400d3",         dodgerBlue: "1e90ff",
    firebrick: "b22222",        floralWhite: "fffaf0",          forestGreen: "228b22",      fuchsia: "ff00ff",
    gainsboro: "dcdcdc",        ghostWhite: "f8f8ff",           gold: "ffd700",             goldenrod: "daa520",
    gray: "808080",             green: "008000",                greenYellow: "adff2f",      grey: "808080",
    honeydew: "f0fff0",         hotPink: "ff69b4",              indianRed: "cd5c5c",        indigo: "4b0082",
    ivory: "fffff0",            khaki: "f0e68c",                lavender: "e6e6fa",         lavenderBlush: "fff0f5",
    lawnGreen: "7cfc00",        lemonChiffon: "fffacd",         lightBlue: "add8e6",        lightCoral: "f08080",
    lightCyan: "e0ffff",        lightGoldenrodYellow: "fafad2", lightGray: "d3d3d3",        lightGreen: "90ee90",
    lightGrey: "d3d3d3",        lightPink: "ffb6c1",            lightSalmon: "ffa07a",      lightSeaGreen: "20b2aa",
    lightSkyBlue: "87cefa",     lightSlateGray: "778899",       lightSlateGrey: "778899",   lightSteelBlue: "b0c4de",
    lightYellow: "ffffe0",      lime: "00ff00",                 limeGreen: "32cd32",        linen: "faf0e6",
    ltBlue: "add8e6",           ltCoral: "f08080",              ltCyan: "e0ffff",           ltGoldenrodYellow: "fafad2",
    ltGray: "d3d3d3",           ltGreen: "90ee90",              ltGrey: "d3d3d3",           ltPink: "ffb6c1",
    ltSalmon: "ffa07a",         ltSeaGreen: "20b2aa",           ltSkyBlue: "87cefa",        ltSlateGray: "778899",
    ltSlateGrey: "778899",      ltSteelBlue: "b0c4de",          ltYellow: "ffffe0",         magenta: "ff00ff",
    maroon: "800000",           medAquamarine: "66cdaa",        medBlue: "0000cd",          mediumAquamarine: "66cdaa",
    mediumBlue: "0000cd",       mediumOrchid: "ba55d3",         mediumPurple: "9370db",     mediumSeaGreen: "3cb371",
    mediumSlateBlue: "7b68ee",  mediumSpringGreen: "00fa9a",    mediumTurquoise: "48d1cc",  mediumVioletRed: "c71585",
    medOrchid: "ba55d3",        medPurple: "9370db",            medSeaGreen: "3cb371",      medSlateBlue: "7b68ee",
    medSpringGreen: "00fa9a",   medTurquoise: "48d1cc",         medVioletRed: "c71585",     midnightBlue: "191970",
    mintCream: "f5fffa",        mistyRose: "ffe4e1",            moccasin: "ffe4b5",         navajoWhite: "ffdead",
    navy: "000080",             oldLace: "fdf5e6",              olive: "808000",            oliveDrab: "6b8e23",
    orange: "ffa500",           orangeRed: "ff4500",            orchid: "da70d6",           paleGoldenrod: "eee8aa",
    paleGreen: "98fb98",        paleTurquoise: "afeeee",        paleVioletRed: "db7093",    papayaWhip: "ffefd5",
    peachPuff: "ffdab9",        peru: "cd853f",                 pink: "ffc0cb",             plum: "dda0dd",
    powderBlue: "b0e0e6",       purple: "800080",               red: "ff0000",              rosyBrown: "bc8f8f",
    royalBlue: "4169e1",        saddleBrown: "8b4513",          salmon: "fa8072",           sandyBrown: "f4a460",
    seaGreen: "2e8b57",         seaShell: "fff5ee",             sienna: "a0522d",           silver: "c0c0c0",
    skyBlue: "87ceeb",          slateBlue: "6a5acd",            slateGray: "708090",        slateGrey: "708090",
    snow: "fffafa",             springGreen: "00ff7f",          steelBlue: "4682b4",        tan: "d2b48c",
    teal: "008080",             thistle: "d8bfd8",              tomato: "ff6347",           turquoise: "40e0d0",
    violet: "ee82ee",           wheat: "f5deb3",                white: "ffffff",            whiteSmoke: "f5f5f5",
    yellow: "ffff00",           yellowGreen: "9acd32"
};

// maps all OOXML system color names (camel-case!) to CSS system color names
const SYSTEM_COLOR_MAP = {
    "3dDkShadow": "threeddarkshadow",           "3dLight": "threedhighlight",               activeBorder: "activeborder",
    activeCaption: "activecaption",             appWorkspace: "appworkspace",               background: "background",
    btnFace: "buttonface",                      btnHighlight: "buttonhighlight",            btnShadow: "buttonshadow",
    btnText: "buttontext",                      captionText: "captiontext",                 gradientActiveCaption: "activecaption",
    gradientInactiveCaption: "inactivecaption", grayText: "graytext",                       highlight: "highlight",
    highlightText: "highlighttext",             hotLight: "highlight",                      inactiveBorder: "inactiveborder",
    inactiveCaption: "inactivecaption",         inactiveCaptionText: "inactivecaptiontext", infoBk: "infobackground",
    infoText: "infotext",                       menu: "menu",                               menuBar: "menu",
    menuHighlight: "highlight",                 menuText: "menutext",                       scrollBar: "scrollbar",
    window: "window",                           windowFrame: "windowframe",                 windowText: "windowtext"
};

// private functions ==========================================================

/**
 * Returns whether the passed JSON value is a valid color transformation.
 */
function isTransformation(data: unknown): data is OpColorTransform {
    return is.dict(data) && is.string(data.type) && (!("value" in data) || is.number(data.value));
}

/**
 * Returns the specified integer property from a JSON object, normalizes
 * the number by the specified scaling factor, but does not restrict it any
 * further.
 *
 * @param data
 *  An arbitrary JSON value to be parsed.
 *
 * @param prop
 *  The name of an integer property to be extracted from the JSON value. If the
 *  JSON value is not an object, or if the property does not exist, this
 *  function returns zero.
 *
 * @param scale
 *  The scaling factor the number will be divided by. MUST be positive.
 *
 * @returns
 *  The normalized value of the specified property.
 */
function getScaledNum(data: unknown, prop: string, scale: number): number {
    return Math.floor(pick.number(data, prop, 0)) / scale;
}

/**
 * Returns the specified integer property from a JSON value, normalizes the
 * number, but does not restrict it any further.
 *
 * @param data
 *  An arbitrary JSON value to be parsed.
 *
 * @param prop
 *  The name of an integer property to be extracted from the JSON value. If the
 *  JSON dvalue is not an object, or if the property does not exist, this
 *  function returns zero.
 *
 * @returns
 *  The normalized value of the specified property.
 */
function getNum(data: unknown, prop: string): number {
    return getScaledNum(data, prop, 100_000);
}

/**
 * Returns the specified integer property from a JSON value, normalizes the
 * number, and restricts it to non-negative numbers.
 *
 * @param data
 *  An arbitrary JSON value to be parsed.
 *
 * @param prop
 *  The name of an integer property to be extracted from the JSON value. If the
 *  JSON value is not an object, or if the property does not exist, this
 *  function returns zero.
 *
 * @returns
 *  The normalized value of the specified property, restricted to non-negative
 *  numbers.
 */
function getPosNum(data: unknown, prop: string): number {
    return Math.max(getScaledNum(data, prop, 100_000), 0);
}

/**
 * Returns the specified integer property from a JSON value, normalizes the
 * number, and restricts it to the interval `[0;1]`.
 *
 * @param data
 *  An arbitrary JSON value to be parsed.
 *
 * @param prop
 *  The name of an integer property to be extracted from the JSON value. If the
 *  JSON value is not an object, or if the property does not exist, this
 *  function returns zero.
 *
 * @returns
 *  The normalized value of the specified property, restricted to the interval
 *  `[0;1]`.
 */
function getLimNum(data: unknown, prop: string): number {
    return math.clamp(getScaledNum(data, prop, 100_000), 0, 1);
}

/**
 * Applies all color transformations to the passed color model, and returns the
 * resulting transformed color model.
 *
 * @param colorModel
 *  The color model to be transformed.
 *
 * @param transforms
 *  The color transformations to be applied.
 *
 * @returns
 *  The transformed color mdoel.
 */
function applyTransformations(colorModel: AlphaModel, transforms: OpColorTransform[]): AlphaModel {
    transforms.forEach(transform => {
        const type = pick.string(transform, "type", "");
        const handler = type ? (TRANSFORMATIONS as Dict<ColorTransformFn>)[type] : null;
        if (handler) {
            colorModel = handler(colorModel, transform);
        } else {
            globalLogger.warn(`$badge{Color} applyTransformations: unknown color transformation "${type}"`);
        }
    });
    return colorModel;
}


/**
 * Appends transformations to a JSON color object in-place.
 *
 * @param opColor
 *  The color to be extended in-place.
 *
 * @param transforms
 *  One or more color transformations. Each parameter MUST be an object with
 *  exactly one property whose key is the transformation type identifier.
 */
function appendTransformations(opColor: OpColor, transforms: OpTransformSpec[]): OpColor {

    const opTransforms: OpColorTransform[] = [];
    for (const transform of transforms) {
        const type = dict.anyKey(transform);
        if (type) {
            const value = transform[type];
            opTransforms.push(is.number(value) ? { type, value } : { type });
        }
    }

    // add transformations (clone existing transformations array)
    if (opTransforms.length) {
        (opColor.transformations ??= []).push(...opTransforms);
    }

    return opColor;
}

// color transformations ======================================================

// implementation function of a color transformation
type ColorTransformFn = (model: AlphaModel, transform: unknown) => AlphaModel;

// all variable color transformation handlers, mapped by transformation name
const VAR_TRANSFORMATIONS = fun.do(() => {

    // workaround to force record property type to `ColorTransformFn`
    const tfn = (fn: ColorTransformFn): ColorTransformFn => fn;

    return {
        alpha:    tfn((model, transform) => model.set("a", getNum(transform, "value"))),
        alphaMod: tfn((model, transform) => model.modulate("a", getPosNum(transform, "value"))),
        alphaOff: tfn((model, transform) => model.offset("a", getNum(transform, "value"))),

        red:      tfn((model, transform) => model.toRGB().setGamma("r", getNum(transform, "value"), GAMMA)),
        redMod:   tfn((model, transform) => model.toRGB().modulateGamma("r", getPosNum(transform, "value"), GAMMA)),
        redOff:   tfn((model, transform) => model.toRGB().offsetGamma("r", getNum(transform, "value"), GAMMA)),

        green:    tfn((model, transform) => model.toRGB().setGamma("g", getNum(transform, "value"), GAMMA)),
        greenMod: tfn((model, transform) => model.toRGB().modulateGamma("g", getPosNum(transform, "value"), GAMMA)),
        greenOff: tfn((model, transform) => model.toRGB().offsetGamma("g", getNum(transform, "value"), GAMMA)),

        blue:     tfn((model, transform) => model.toRGB().setGamma("b", getNum(transform, "value"), GAMMA)),
        blueMod:  tfn((model, transform) => model.toRGB().modulateGamma("b", getPosNum(transform, "value"), GAMMA)),
        blueOff:  tfn((model, transform) => model.toRGB().offsetGamma("b", getNum(transform, "value"), GAMMA)),

        hue:      tfn((model, transform) => model.toHSL().setH(getScaledNum(transform, "value", HUE_PER_DEG))),
        hueMod:   tfn((model, transform) => model.toHSL().modulateH(getPosNum(transform, "value"))),
        hueOff:   tfn((model, transform) => model.toHSL().offsetH(getScaledNum(transform, "value", HUE_PER_DEG))),

        sat:      tfn((model, transform) => model.toHSL().set("s", getNum(transform, "value"))),
        satMod:   tfn((model, transform) => model.toHSL().modulate("s", getPosNum(transform, "value"))),
        satOff:   tfn((model, transform) => model.toHSL().offset("s", getNum(transform, "value"))),

        lum:      tfn((model, transform) => model.toHSL().set("l", getNum(transform, "value"))),
        lumMod:   tfn((model, transform) => model.toHSL().modulate("l", getPosNum(transform, "value"))),
        lumOff:   tfn((model, transform) => model.toHSL().offset("l", getNum(transform, "value"))),

        shade:    tfn((model, transform) => model.toRGB().darkenGamma(1 - getPosNum(transform, "value"), GAMMA)),
        tint:     tfn((model, transform) => model.toRGB().lightenGamma(1 - getPosNum(transform, "value"), GAMMA))
    };
});

// all constant color transformation handlers, mapped by transformation name
const CONST_TRANSFORMATIONS = fun.do(() => {

    // workaround to force record property type to `ColorTransformFn`
    const tfn = (fn: ColorTransformFn): ColorTransformFn => fn;

    return {
        gamma:    tfn(model => model.toRGB().gamma(GAMMA)),
        invGamma: tfn(model => model.toRGB().gamma(INV_GAMMA)),

        comp:     tfn(model => model.toHSL().comp()),
        inv:      tfn(model => model.toRGB().invGamma(GAMMA)),
        gray:     tfn(model => model.toRGB().gray())
    };
});

// all color transformation handlers, mapped by transformation name
const TRANSFORMATIONS = { ...VAR_TRANSFORMATIONS, ...CONST_TRANSFORMATIONS };

// public functions ===========================================================

/**
 * Adds transformations to a JSON color object.
 *
 * @param color
 *  The JSON color to be extended.
 *
 * @param transforms
 *  One or more color transformations. Each parameter MUST be an object with
 *  exactly one property whose key is the transformation type identifier.
 *
 * @returns
 *  A clone of the passed color, with all transformations added.
 */
export function transformOpColor(color: OpColor, ...transforms: OpTransformSpec[]): OpColor {

    const opTransforms: OpColorTransform[] = [];
    for (const transform of transforms) {
        const type = dict.anyKey(transform);
        if (type) {
            const value = transform[type];
            opTransforms.push(is.number(value) ? { type, value } : { type });
        }
    }

    // always clone the passed color object
    color = { ...color };
    if (color.transformations) {
        color.transformations = color.transformations.slice();
    }

    return appendTransformations(color, transforms);
}

/**
 * Returns a JSON color object of type "rgb".
 *
 * @param rgb
 *  A 6-digit hexadecimal character (`RRGGBB`), or an integer with the desired
 *  hexadecimal representation of an RGB value.
 *
 * @returns
 *  The JSON color of type "rgb", with hexadecimal "value" property converted
 *  to uppercase letters.
 */
export function opRgbColor(rgb: string | number, ...transforms: OpTransformSpec[]): OpColor {
    return appendTransformations({ type: "rgb", value: (is.number(rgb) ? rgb.toString(16) : rgb).toUpperCase() }, transforms);
}

/**
 * Returns a JSON color object of type "crgb".
 *
 * @returns
 *  The JSON color of type "crgb".
 */
export function opCrgbColor(r: number, g: number, b: number, ...transforms: OpTransformSpec[]): OpColor {
    return appendTransformations({ type: "crgb", value: { r, g, b } }, transforms);
}

/**
 * Returns a JSON color object of type "hsl".
 *
 * @returns
 *  The JSON color of type "hsl".
 */
export function opHslColor(h: number, s: number, l: number, ...transforms: OpTransformSpec[]): OpColor {
    return appendTransformations({ type: "hsl", value: { h, s, l } }, transforms);
}

/**
 * Returns a JSON color object of type "preset".
 *
 * @param id
 *  The preset color identifier.
 *
 * @returns
 *  The JSON color of type "preset".
 */
export function opPresetColor(id: PresetColorId, ...transforms: OpTransformSpec[]): OpColor {
    return appendTransformations({ type: "preset", value: id }, transforms);
}

/**
 * Returns a JSON color object of type "system".
 *
 * @param id
 *  The system color identifier.
 *
 * @returns
 *  The JSON color of type "system".
 */
export function opSystemColor(id: SystemColorId, ...transforms: OpTransformSpec[]): OpColor {
    return appendTransformations({ type: "system", value: id }, transforms);
}

/**
 * Returns a JSON color object of type "scheme".
 *
 * @param id
 *  The identifier of the scheme color.
 *
 * @returns
 *  The JSON color of type "scheme", with "value" property set to the passed
 *  scheme color identifier.
 */
export function opSchemeColor(id: SchemeColorId | "phClr", ...transforms: OpTransformSpec[]): OpColor {
    return appendTransformations({ type: "scheme", value: id }, transforms);
}

// class Color ================================================================

/**
 * Runtime representation of a color of any type supported in document
 * operations.
 *
 * @param type
 *  The type of the color.
 *
 * @param [value]
 *  The JSON value of the color. The expected data type depends on the passed
 *  color type:
 *  - `AUTO`: Color value will be ignored and can be omitted.
 *  - `SYSTEM`, `PRESET`, `SCHEME`: A string with the name of the color.
 *  - `CRGB`: An object with the integer properties `r`, `g`, and `b`, each of
 *    them in the interval `[0;100000]` (representing 0% to 100%).
 *  - `RGB`: A string with a 6-digit hexadecimal character (`RRGGBB`).
 *  - `HSL`: An object with the integer properties `h`, `s`, and `l`. The
 *    property `h` is expected to be in the interval `[0;21599999]`
 *    (representing 1/60000 of degrees), the other properties have to be in the
 *    interval `[0;100000]` (representing 0% to 100%).
 *
 * @param [transforms]
 *  A JSON array with color transformations to be applied to the base color
 *  described by the parameters `type` and `value`. Each array element should
 *  be an object with the string property `type`, and the optional integer
 *  property `value`. Supports all color transformations that are supported in
 *  document operations.
 *
 * @param [defRGB]
 *  A fall-back RGB color value (6-digit hexadecimal number as string). May be
 *  provided by document operations, and can be used in case the color cannot
 *  be resolved correctly.
 */
export class Color implements Equality<Color>, Cloneable<Color> {

    // static constants -------------------------------------------------------

    /**
     * The JSON representation of the automatic color.
     */
    static readonly AUTO = Object.freeze<OpColor>({ type: "auto" });

    /**
     * The JSON representation of the color black (#000000).
     */
    static readonly BLACK = Object.freeze(opRgbColor("000000"));

    /**
     * The JSON representation of the color white (#ffffff).
     */
    static readonly WHITE = Object.freeze(opRgbColor("ffffff"));

    /**
     * The JSON representation of the color pure red (#ff0000).
     */
    static readonly RED = Object.freeze(opRgbColor("ff0000"));

    /**
     * The JSON representation of the color pure yellow (#ffff00).
     */
    static readonly YELLOW = Object.freeze(opRgbColor("ffff00"));

    /**
     * The JSON representation of the color pure green (#00ff00).
     */
    static readonly GREEN = Object.freeze(opRgbColor("00ff00"));

    /**
     * The JSON representation of the color pure cyan (#00ffff).
     */
    static readonly CYAN = Object.freeze(opRgbColor("00ffff"));

    /**
     * The JSON representation of the color pure blue (#0000ff).
     */
    static readonly BLUE = Object.freeze(opRgbColor("0000ff"));

    /**
     * The JSON representation of the color pure violet (#ff00ff).
     */
    static readonly VIOLET = Object.freeze(opRgbColor("ff00ff"));

    /**
     * The JSON representation of the first dark color.
     */
    static readonly DARK1 = Object.freeze(opSchemeColor("dark1"));

    /**
     * The JSON representation of the second dark color.
     */
    static readonly DARK2 = Object.freeze(opSchemeColor("dark2"));

    /**
     * The JSON representation of the first light color.
     */
    static readonly LIGHT1 = Object.freeze(opSchemeColor("light1"));

    /**
     * The JSON representation of the second light color.
     */
    static readonly LIGHT2 = Object.freeze(opSchemeColor("light2"));

    /**
     * The JSON representation of the first text color.
     */
    static readonly TEXT1 = Object.freeze(opSchemeColor("text1"));

    /**
     * The JSON representation of the second text color.
     */
    static readonly TEXT2 = Object.freeze(opSchemeColor("text2"));

    /**
     * The JSON representation of the first background color.
     */
    static readonly BACK1 = Object.freeze(opSchemeColor("background1"));

    /**
     * The JSON representation of the second background color.
     */
    static readonly BACK2 = Object.freeze(opSchemeColor("background2"));

    /**
     * The JSON representation of the first accent color.
     */
    static readonly ACCENT1 = Object.freeze(opSchemeColor("accent1"));

    /**
     * The JSON representation of the second accent color.
     */
    static readonly ACCENT2 = Object.freeze(opSchemeColor("accent2"));

    /**
     * The JSON representation of the third accent color.
     */
    static readonly ACCENT3 = Object.freeze(opSchemeColor("accent3"));

    /**
     * The JSON representation of the fourth accent color.
     */
    static readonly ACCENT4 = Object.freeze(opSchemeColor("accent4"));

    /**
     * The JSON representation of the fifth accent color.
     */
    static readonly ACCENT5 = Object.freeze(opSchemeColor("accent5"));

    /**
     * The JSON representation of the sixth accent color.
     */
    static readonly ACCENT6 = Object.freeze(opSchemeColor("accent6"));

    /**
     * The JSON representation of the hyperlink scheme color.
     */
    static readonly HYPERLINK = Object.freeze(opSchemeColor("hyperlink"));

    /**
     * The JSON representation of the placeholder color.
     */
    static readonly PLACEHOLDER = Object.freeze(opSchemeColor("phClr"));

    // static functions -------------------------------------------------------

    /**
     * Creates a new instance of the class `Color` of type "auto".
     *
     * @returns
     *  A new color instance.
     */
    static auto(): Color {
        return new Color("auto");
    }

    /**
     * Creates a new instance of the class `Color` of type "rgb".
     *
     * @param rgb
     *  A 6-digit hexadecimal character (`RRGGBB`), or an integer with the
     *  desired hexadecimal representation of an RGB value.
     *
     * @returns
     *  A new color instance.
     */
    static fromRgb(rgb: string | number, ...transforms: OpTransformSpec[]): Color {
        return Color.parseJSON(opRgbColor(rgb, ...transforms));
    }

    /**
     * Creates a new instance of the class `Color` of type "crgb".
     *
     * @returns
     *  A new color instance.
     */
    static fromCrgb(r: number, g: number, b: number, ...transforms: OpTransformSpec[]): Color {
        return Color.parseJSON(opCrgbColor(r, g, b, ...transforms));
    }

    /**
     * Creates a new instance of the class `Color` of type "hsl".
     *
     * @returns
     *  A new color instance.
     */
    static fromHsl(h: number, s: number, l: number, ...transforms: OpTransformSpec[]): Color {
        return Color.parseJSON(opHslColor(h, s, l, ...transforms));
    }

    /**
     * Creates a new instance of the class `Color` of type "preset".
     *
     * @param id
     *  The preset color identifier.
     *
     * @returns
     *  A new color instance.
     */
    static fromPreset(id: PresetColorId, ...transforms: OpTransformSpec[]): Color {
        return Color.parseJSON(opPresetColor(id, ...transforms));
    }

    /**
     * Creates a new instance of the class `Color` of type "system".
     *
     * @param id
     *  The system color identifier.
     *
     * @returns
     *  A new color instance.
     */
    static fromSystem(id: SystemColorId, ...transforms: OpTransformSpec[]): Color {
        return Color.parseJSON(opSystemColor(id, ...transforms));
    }

    /**
     * Creates a new instance of the class `Color` of type "scheme".
     *
     * @param id
     *  The scheme color identifier.
     *
     * @returns
     *  A new color instance.
     */
    static fromScheme(id: SchemeColorId | "phClr", ...transforms: OpTransformSpec[]): Color {
        return Color.parseJSON(opSchemeColor(id, ...transforms));
    }

    /**
     * Parses the passed JSON representation of a color, and returns a new
     * instance of the class `Color`.
     *
     * @param data
     *  The JSON value to be parsed. Should be an object with the properties
     *  supported in document operations.
     *
     * @returns
     *  A new color instance.
     */
    static parseJSON(data: unknown): Color {
        return new Color(
            pick.string(data, "type", "auto"),
            pick.prop(data, "value", null),
            pick.array(data, "transformations"),
            pick.string(data, "fallbackValue", "")
        );
    }

    /**
     * Converts the passed CSS color value to an instance of the class `Color`.
     *
     * @param [value]
     *  The CSS color value. The following CSS colors will be accepted:
     *  - Predefined color names, e.g. `red`.
     *  - Hexadecimal colors (6-digit, and 3-digit), with leading hash sign.
     *  - RGB colors in function style, using the `rgb()` or `rgba()` function.
     *  - HSL colors in function style, using the `hsl()` or `hsla()` function.
     *  If missing or not a string, this method will return the value null.
     *
     * @param [fill=false]
     *  If set to `true`, the color is intended to be used as fill color. If
     *  the color is fully transparent, the automatic color will be returned,
     *  instead of an RGB color with the alpha channel set to `0`.
     *
     * @returns
     *  A new color instance, if the passed string could be converted to a
     *  color, otherwise `null`.
     */
    static parseCSS(value?: unknown, fill?: boolean): Color | null {

        // parse the passed CSS color
        const colorDesc = is.string(value) ? parseCssColor(value) : null;
        if (!colorDesc) { return null; }

        // value of the alpha channel, in the interval [0;100000]
        const alpha = Math.round(colorDesc.a * 100_000);

        // return auto color for fill mode will full transparency
        if (fill && (alpha === 0)) {
            return Color.auto();
        }

        // create a new color object according to the type of the passed CSS color
        const color = fun.do(() => {
            switch (colorDesc.type) {
                case ColorType.PRESET:
                    return Color.fromPreset(value as PresetColorId);
                case ColorType.SYSTEM:
                    return Color.fromSystem(value as SystemColorId);
                case ColorType.HSL: {
                    const hsl = colorDesc.hsl;
                    return Color.fromHsl(Math.round(hsl.h * HUE_PER_DEG), Math.round(hsl.s * 100_000), Math.round(hsl.l * 100_000));
                }
                default:
                    return Color.fromRgb(colorDesc.hex);
            }
        });

        // add the alpha channel
        return color.alpha(alpha);
    }

    /**
     * Returns whether the passed JSON representations of two colors are equal.
     *
     * @param jsonColor1
     * The JSON representation of the first color to be compared to the other
     * color.
     *
     * @param jsonColor2
     * The JSON representation of the second color to be compared to the other
     * color.
     *
     * @returns
     *  Whether the JSON representations of two colors are equal.
     */
    static isEqual(jsonColor1: unknown, jsonColor2: unknown): boolean {
        // Convert to `Color` instance, and use its euqals() method. This normalizes
        // missing properties (e.g. missing `type` property defaults to type `auto`),
        // and ignores unused properties (e.g. the `fallbackValue` property).
        return Color.parseJSON(jsonColor1).equals(Color.parseJSON(jsonColor2));
    }

    // properties -------------------------------------------------------------

    readonly #type: string;
    readonly #value?: string | Dict;
    #transforms: Opt<OpColorTransform[]>;
    readonly #defRGB?: string;

    // constructor ------------------------------------------------------------

    private constructor(type: string, value?: unknown, transforms?: unknown[], defRGB?: unknown) {

        this.#type = type;

        // create copies of input values to prevent modification from outside
        if (is.string(value)) {
            this.#value = (type === "rgb") ? value.toUpperCase() : value;
        } else if (is.dict(value)) {
            this.#value = { ...value };
        }

        if (transforms) {
            this.#transforms = json.deepClone(transforms.filter(isTransformation));
        }

        if (is.string(defRGB) && defRGB) {
            this.#defRGB = defRGB.toUpperCase();
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this color is the automatic color.
     *
     * @returns
     *  Whether this color is the automatic color.
     */
    isAuto(): boolean {
        return this.#type === "auto";
    }

    /**
     * Returns whether this color is equal to the passed color.
     *
     * @param color
     *  The color to be compared with this color.
     *
     * @returns
     *  Whether this color is equal to the passed color.
     */
    equals(color: Color): boolean {
        const opColor1 = this.toJSON(), opColor2 = color.toJSON();
        // ignore unused properties, e.g. `fallbackValue`
        return (opColor1.type === opColor2.type) &&
            _.isEqual(opColor1.value, opColor2.value) &&
            _.isEqual(opColor1.transformations, opColor2.transformations);
    }

    /**
     * Returns a deep clone of this color.
     *
     * @returns
     *  A deep clone of this color.
     */
    clone(): Color {
        return new Color(this.#type, this.#value, this.#transforms, this.#defRGB);
    }

    /**
     * Appends a color transformation to this color.
     *
     * @param type
     *  The identifier of a color transformation.
     *
     * @param [value]
     *  The value for a variable color transformation.
     *
     * @returns
     *  A reference to this instance.
     */
    transform(type: OpConstTransformType): this;
    transform(type: OpVarTransformType, value: number): this;
    // implementation
    transform(type: OpTransformType, value?: number): this {
        const transforms = this.#transforms ??= [];
        transforms.push(is.number(value) ? { type, value } : { type });
        return this;
    }

    /**
     * Removes all alpha transformations from this color, and appends a single
     * "alpha" transformation (unless the value is 100,000 or greater, i.e.
     * opaque).
     *
     * @param value
     *  The level of transparency. The value 0 creates a fully transparent
     *  color, the value 100,000 creates a fully opaque color.
     *
     * @returns
     *  A reference to this instance.
     */
    alpha(value: number): this {
        // remove all "alpha" transformations
        if (this.#transforms) {
            ary.deleteAllMatching(this.#transforms, transform => transform.type.startsWith("alpha"));
        }
        // add new "alpha" transformation
        if (value < 100_000) {
            this.transform("alpha", value);
        }
        // remove empty transformations array
        if (this.#transforms?.length === 0) {
            this.#transforms = undefined;
        }
        return this;
    }

    /**
     * Removes all alpha transformations from this color, leaving an opaque
     * color.
     *
     * @returns
     *  A reference to this instance.
     */
    opaque(): this {
        return this.alpha(100_000);
    }

    /**
     * Returns a color descriptor with details about the color represented by
     * this instance.
     *
     * @param auto
     *  Additional information needed to resolve the automatic color. Can be a
     *  value from the color mode enumeration for predefined automatic color
     *  types, or an instance of the class `Color` that is NOT the automatic
     *  color.
     *
     * @param [themeModel]
     *  The model of a document theme used to map scheme color names to color
     *  values. If missing, theme colors will not be resolved correctly.
     *
     * @returns
     *  A descriptor for the resulting color.
     */
    resolve(auto: AutoColorType | Color, themeModel?: ThemeModel): ColorDescriptor {

        // shortcut to the current value
        const value = this.#value;
        // shortcut to the default RGB value
        const defRGB = this.#defRGB;
        // the color model containing the resolved color
        let colorModel: Opt<AlphaModel>;
        // the original color type to be passed to the color descriptor
        let colorType = ColorType.RGB;
        // whether to apply the color transformations
        let applyTransforms = true;

        function createHexModel(hex: unknown): RGBModel {
            const rgbModel = is.string(hex) ? RGBModel.parseHex(hex) : null;
            return rgbModel ?? new RGBModel(0, 0, 0, 1);
        }

        function createDefaultModel(): RGBModel {
            applyTransforms = false;
            return createHexModel(defRGB);
        }

        // resolve automatic color with explicit color passed in 'auto' parameter
        if (this.isAuto() && (auto instanceof Color)) {
            if (this.#transforms) {
                auto = auto.clone();
                auto.#transforms = json.deepClone(this.#transforms);
            }
            return auto.resolve(AutoColorType.FILL, themeModel);
        }

        // create a color model for all supported color types
        switch (this.#type) {

            // predefined colors with fixed RGB values
            case "preset": {
                const presetColor = is.string(value) ? (PRESET_COLOR_MAP as Dict<string>)[value] : null;
                colorModel = createHexModel(presetColor);
                colorType = presetColor ? ColorType.PRESET : ColorType.RGB;
                break;
            }

            // predefined system UI colors without fixed RGB values
            case "system": {
                const systemColor = is.string(value) ? (SYSTEM_COLOR_MAP as Dict<string>)[value] : null;
                colorModel = createHexModel(systemColor ? getSystemColor(systemColor) : null);
                colorType = systemColor ? ColorType.SYSTEM : ColorType.RGB;
                break;
            }

            // CRGB color model: each channel in the interval [0;100000]
            case "crgb":
                colorModel = new RGBModel(getLimNum(value, "r"), getLimNum(value, "g"), getLimNum(value, "b"), 1).gamma(GAMMA);
                break;

            // HSL color model (hue, saturation, luminance)
            case "hsl":
                colorModel = new HSLModel(getScaledNum(value, "h", HUE_PER_DEG), getLimNum(value, "s"), getLimNum(value, "l"), 1);
                colorType = ColorType.HSL;
                break;

            // simple hexadecimal RGB
            case "rgb":
                colorModel = createHexModel(value);
                break;

            // scheme color
            case "scheme":
                if (themeModel && is.string(value)) {
                    if (value === "style") {
                        globalLogger.warn('$badge{Color} resolve: scheme color type "style" not implemented');
                        colorModel = createDefaultModel();
                    } else {
                        colorModel = createHexModel(themeModel.getSchemeColor(value, "000000"));
                        colorType = ColorType.SCHEME;
                    }
                } else {
                    colorModel = createDefaultModel();
                }
                break;

            // automatic color, effective color depends on passed 'auto' parameter
            case "auto":
                // resolve automatic color by color type name
                switch (auto) {
                    case AutoColorType.TEXT:
                    case AutoColorType.LINE:
                        colorModel = new RGBModel(0, 0, 0, 1); // black
                        break;
                    case AutoColorType.FILL:
                        colorModel = new RGBModel(0, 0, 0, 0); // full transparency
                        colorType = ColorType.TRANSPARENT;
                        applyTransforms = false;
                        break;
                    default:
                        globalLogger.error(`$badge{Color} resolve: unknown color type "${auto}" for auto color`);
                        colorModel = createDefaultModel();
                }
                break;

            default:
                globalLogger.error(`$badge{Color} resolve: unknown color type "${this.#type}"`);
                colorModel = createDefaultModel();
        }

        // apply all transformations
        if (applyTransforms && this.#transforms) {
            colorModel = applyTransformations(colorModel, this.#transforms);
        }

        // create the color descriptor for the resulting color model
        return colorModel.getDescriptor(colorType);
    }

    /**
     * Returns the value of the alpha channel represented by this instance.
     *
     * @returns
     *  The value of the alpha channel.
     */
    resolveAlpha(): number {

        // automatic color is always opaque (also if there are transformations)
        if (this.isAuto() || !this.#transforms) { return 1; }

        // apply all transformations
        return applyTransformations(new RGBModel(0, 0, 0, 1), this.#transforms).a;
    }

    /**
     * Returns a color descriptor with details about the color represented by
     * this instance, assuming it to be a text color on a specific background
     * style. The automatic color will be resolved to the color black or white,
     * depending on the lightness of the passed background colors. All other
     * colors will be resolved according to the method `resolve()`.
     *
     * @param fillColors
     *  The source fill colors, from outermost to innermost level. The last
     *  explicit (non-automatic) fill color in the array will be used as
     *  effective fill color.
     *
     * @param [themeModel]
     *  The model of a document theme used to map scheme color names to color
     *  values. If missing, theme colors will not be resolved correctly.
     *
     * @returns
     *  A descriptor for the resulting color.
     */
    resolveText(fillColors: Color[], themeModel?: ThemeModel): ColorDescriptor {

        // the resulting text color
        let textColor = this as Color;
        // the effective fill color
        let fillColor: Opt<Color>;

        // resolve explicit colors directly
        if (this.isAuto()) {

            // find last non-transparent fill color in the passed array
            // TODO: merge semi-transparent colors?
            fillColors.forEach(currFillColor => {
                if (!currFillColor.isAuto()) { fillColor = currFillColor; }
            });

            // resolve to black or white according to lightness of fill color (assume white for missing fill color)
            const dark = fillColor?.resolve(AutoColorType.FILL, themeModel).dark;
            textColor = Color.fromPreset(dark ? "white" : "black");
        }

        return textColor.resolve(AutoColorType.TEXT, themeModel);
    }

    /**
     * Returns a color descriptor with details for a color that is a mix of
     * this color instance, and the passed color instance.
     *
     * @param color
     *  The other color to be mixed with this color.
     *
     * @param weighting
     *  Specifies how much of the other color will be mixed into this color.
     *  - Values equal to or less than zero will result in an exact copy of
     *    THIS color.
     *  - Values equal to or greater than one will result in an exact copy of
     *    the OTHER color.
     *  - Values between zero and one result in a mixed color between this and
     *    the other color, where values close to zero result in colors similar
     *    to THIS color, and values close to one result in colors similar to
     *    the OTHER color.
     *
     * @param auto
     *  Additional information needed to resolve the automatic color. See
     *  method `resolve()` for details.
     *
     * @param [themeModel]
     *  The model of a document theme used to map scheme color names to color
     *  values. If missing, theme colors will not be resolved correctly.
     *
     * @returns
     *  A descriptor for the resulting color.
     */
    resolveMixed(color: Color, weighting: number, auto: AutoColorType | Color, themeModel?: ThemeModel): ColorDescriptor {

        // resolve this color and the other color
        const desc1 = this.resolve(auto, themeModel);
        const desc2 = color.resolve(auto, themeModel);

        // early exit if passed weithing is out of the open interval (0,1)
        if (weighting <= 0) { return desc1; }
        if (weighting >= 1) { return desc2; }

        // use the RGBA color model to calculate a mixed color
        const r = desc1.rgb.r + (desc2.rgb.r - desc1.rgb.r) * weighting;
        const g = desc1.rgb.g + (desc2.rgb.g - desc1.rgb.g) * weighting;
        const b = desc1.rgb.b + (desc2.rgb.b - desc1.rgb.b) * weighting;
        const a = desc1.rgb.a + (desc2.rgb.a - desc1.rgb.a) * weighting;

        return new RGBModel(r, g, b, a).getDescriptor(ColorType.RGB);
    }

    /**
     * Returns the JSON representation of this color, as used in document
     * operations.
     *
     * @returns
     *  The JSON representation of this color.
     */
    toJSON(): OpColor {
        const opColor: OpColor = { type: this.#type };
        if ((this.#type !== "auto") && this.#value) { opColor.value = this.#value; }
        if (this.#transforms && (this.#transforms.length > 0)) { opColor.transformations = this.#transforms.slice(); }
        if (this.#defRGB) { opColor.fallbackValue = this.#defRGB; }
        return opColor;
    }

    /**
     * Serializes this color to a token list used in document operations.
     *
     * @returns
     *  A token list used in document operations.
     */
    toOpStr(): string {

        // type and value of the base color
        let colorStr = fun.do(() => {
            switch (this.#type) {
                case "preset": return `pre ${to.string(this.#value, "black")}`;
                case "system": return `sys ${to.string(this.#value, "text")}`;
                case "crgb":   return `crgb ${pick.number(this.#value, "r", 0)} ${pick.number(this.#value, "g", 0)} ${pick.number(this.#value, "b", 0)}`;
                case "rgb":    return `rgb ${to.string(this.#value, "000000").toUpperCase()}`;
                case "hsl":    return `hsl ${pick.number(this.#value, "h", 0)} ${pick.number(this.#value, "s", 0)} ${pick.number(this.#value, "l", 0)}`;
                case "scheme": return (this.#value === "phClr") ? "ph" : `sch ${to.string(this.#value, "text1")}`;
                default:       return "auto";
            }
        });

        // color transformations
        this.#transforms?.forEach(transform => {
            colorStr += ` ${transform.type}`;
            if (is.number(transform.value)) { colorStr += ` ${transform.value}`; }
        });

        // fallback RGB value for ODF
        if (this.#defRGB) {
            colorStr += ` res ${this.#defRGB.toUpperCase()}`;
        }

        return colorStr;
    }

    /**
     * Returns the string representation of this color for debug logging.
     */
    toString(): string {
        return this.toOpStr();
    }
}
