/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, dict, is, json } from "@/io.ox/office/tk/algorithms";

import type { OpBorder } from "@/io.ox/office/editframework/utils/border";
import { Color, type OpColor } from "@/io.ox/office/editframework/utils/color";
import { NO_BORDER, isVisibleBorder as _isVisibleBorder } from "@/io.ox/office/editframework/utils/border";

// types ======================================================================

/**
 * A mixed border object representing the states of the properties of
 * multiple JSON border objects (`OpBorder`).
 */
export interface MixedBorder {

    /**
     * Whether the visibility state of the borders is ambiguous. If `true`,
     * a few but not all borders are visible, and the other borders are
     * invisible.
     */
    mixed: boolean;

    /**
     * The line style of all visible border lines. Will be `null`, if the
     * line style is ambiguous. Will be "none", if ALL borders are
     * invisible.
     */
    style: string | null;

    /**
     * The line width of all visible border lines, in 1/100 mm. Will be
     * `null`, if the line width is ambiguous. Will be omitted, if ALL
     * borders are invisible.
     */
    width?: number | null;

    /**
     * The line color of all visible border lines. Will be `null`, if the
     * line color is ambiguous. Will be omitted, if ALL borders are
     * invisible.
     */
    color?: OpColor | null;
}

/**
 * The options for the getBorderAttributes function.
 */
export interface GetBorderAttributesOptions {

    /**
     * If set to `true`, a single inner border attribute `borderInside` will
     * be used (for paragraphs which are stacked vertically and do not have
     * vertical inner borders), instead of the two attributes
     * `borderInsideHor` and `borderInsideVert`.
     */
    paragraph?: boolean;

    /**
     * If set to `true`, and the border is invisible (e.g. the paragraph
     * border is removed in text), it will set the related border attribute
     * in the attributes map to `null` instead of `NO_BORDER`.
     */
    clearNone?: boolean;

    /**
     * If set to `true`, a full set of border attributes shall be returned.
     */
    fullBorderSet?: boolean;
}

/**
 * The options for the getBorderFlags function.
 */
export interface GetBorderFlagsOptions {

    /**
     * If set to `true`, a single inner border attribute `borderInside` will
     * be used instead of the two attributes `borderInsideHor` and
     * `borderInsideVert` (for paragraphs which are stacked vertically and
     * do not have vertical inner borders).
     */
    paragraph?: boolean;
}

/**
 * The single-character keys of all supported borders.
 *
 * - "l" = Left border line.
 * - "r" = Right border line.
 * - "t" = Top border line.
 * - "b" = Bottom border line.
 * - "v": Vertical inner border line.
 * - "h": Horizontal inner border line.
 */
export type AttrBorderKey = keyof typeof CELL_BORDER_NAMES;

// constants ==============================================================

// the names of all cell border attributes, mapped by border keys
const  CELL_BORDER_NAMES = {
    l: "borderLeft",
    r: "borderRight",
    t: "borderTop",
    b: "borderBottom",
    h: "borderInsideHor",
    v: "borderInsideVert"
};

// the names of all paragraph border attributes, mapped by border keys
const  PARAGRAPH_BORDER_NAMES = {
    l: "borderLeft",
    r: "borderRight",
    t: "borderTop",
    b: "borderBottom",
    h: "borderInside"
};

// private functions ======================================================

/**
 * Returns a map with names of border attributes used in operations, mapped
 * by single-letter border keys.
 */
function getBorderAttrNames(options?: { paragraph?: boolean }): typeof PARAGRAPH_BORDER_NAMES | typeof CELL_BORDER_NAMES {
    return (options?.paragraph) ? PARAGRAPH_BORDER_NAMES : CELL_BORDER_NAMES;
}

// public functions =======================================================

/**
 * Returns whether the passed mixed border object is valid and visible
 * (line style ambiguous or different to "none", and line width ambiguous
 * or greater than `0`).
 *
 * @param [mixedBorder]
 *  The mixed border object, as returned for example by the function
 *  `mixBorders()`. If the parameter is missing or not an object, it is
 *  considered invisible.
 *
 * @returns
 *  Whether the mixed border object represents a visible border style.
 */
export function isVisibleBorder(mixedBorder?: MixedBorder): boolean {
    return is.dict(mixedBorder) &&
        // in mixed state, there are always some borders visible
        (!!mixedBorder.mixed || (mixedBorder.style !== "none")) &&
        // border width may be ambiguous, but must not be 0)
        ((mixedBorder.width === null) || (is.number(mixedBorder.width) && (mixedBorder.width > 0)));
}

/**
 * Returns whether the passed mixed border object contains any ambiguous
 * properties, i.e. whether either the property `style`, `width`, or
 * `color` is set to `null`. The property `mixed` will be ignored.
 *
 * @param [mixedBorder]
 *  The mixed border object, as returned for example by the function
 *  `mixBorders()`. If the parameter is missing or not an object, `false`
 *  will be returned (a missing border object is considered to be
 *  invisible, and therefore not ambiguous).
 *
 * @returns
 *  Whether the passed mixed border object contains ambiguous properties.
 */
export function hasAmbiguousProperties(mixedBorder?: MixedBorder): boolean {
    return is.dict(mixedBorder) && ((mixedBorder.style === null) || (mixedBorder.width === null) || (mixedBorder.color === null));
}

/**
 * Returns whether the passed mixed border object is completely ambiguous
 * (whether the `mixed` flag is set, and all border properties are set to
 * the value `null`).
 *
 * @param [mixedBorder]
 *  The mixed border object, as returned for example by the function
 *  `mixBorders()`. If the parameter is missing or not an object, `false`
 *  will be returned (a missing border object is considered to be
 *  invisible, and therefore not ambiguous).
 *
 * @returns
 *  Whether the passed mixed border object is completely ambiguous.
 */
export function isFullyAmbiguousBorder(mixedBorder?: MixedBorder): boolean {
    return is.dict(mixedBorder) && !!mixedBorder.mixed && (mixedBorder.style === null) && (mixedBorder.width === null) && (mixedBorder.color === null);
}

/**
 * Mixes the passed border objects, and returns a mixed border object
 * representing the states of the single border properties.
 *
 * @param args
 *  An unlimited number of parameters, each parameter can be a single mixed
 *  border object and/or a regular border attribute value, or an array of
 *  these objects.
 *
 * @returns
 *  A mixed border object representing the states of the properties of all
 *  passed borders.
 */
export function mixBorders(...args: Array<MixedBorder | MixedBorder[]>): MixedBorder {

    // the resulting mixed border object
    let mixedBorder = null;

    // process all parameters
    for (const arg of args) {
        for (const border of ary.wrap(arg)) {

            // start with a copy of the first border object
            if (!mixedBorder) {
                mixedBorder = json.flatClone(border);
                continue;
            }

            // whether the passed border is visible
            const visible = ("mixed" in border) ? isVisibleBorder(border) : _isVisibleBorder(border);

            // update the mixed visibility state
            if (!mixedBorder.mixed) {
                mixedBorder.mixed = border.mixed || (isVisibleBorder(mixedBorder) !== visible);
            }

            // skip the other border line properties, if the border is invisible
            if (!visible) { continue; }

            // mix line style (null represents mixed state of visible styles)
            if (mixedBorder.style === "none") {
                mixedBorder.style = border.style;
            } else if (mixedBorder.style !== border.style) {
                mixedBorder.style = null;
            }

            // update line width (null represents mixed state)
            if (!("width" in mixedBorder)) {
                mixedBorder.width = border.width;
            } else if (is.number(mixedBorder.width) && (mixedBorder.width !== border.width)) {
                mixedBorder.width = null;
            }

            // update line color (null represents mixed state)
            if (!("color" in mixedBorder)) {
                mixedBorder.color = border.color;
            } else if (is.dict(mixedBorder.color) && !Color.isEqual(mixedBorder.color, border.color)) {
                mixedBorder.color = null;
            }
        }
    }

    // create a complete mixed border object
    const defaultBorder = { mixed: false, style: "none" };
    mixedBorder = { ...defaultBorder, ...mixedBorder };

    // do not return unused attributes for invisible borders retrieved from input data
    return (mixedBorder.style === "none") ? { mixed: mixedBorder.mixed, style: "none" } : mixedBorder;
}

/**
 * Converts the passed mixed border attributes to a flag set describing the
 * visibility states of all borders.
 *
 * @param borderAttrs
 *  The attribute map containing mixed border objects, as returned for
 *  example by the method `mixBorders()`, mapped by border attribute names
 *  (e.g. `borderLeft`, `borderTop`, etc.).
 *
 * @param [options]
 *  Optional parameters:
 *  - {boolean} [options.paragraph=false]
 *    If set to `true`, a single inner border attribute `borderInside` will
 *    be used instead of the two attributes `borderInsideHor` and
 *    `borderInsideVert` (for paragraphs which are stacked vertically and
 *    do not have vertical inner borders).
 *
 * @returns
 *  A dictionary with single-letter border keys ("l", "t", etc.), and
 *  boolean values or `null` specifying whether the respective borders are
 *  visible. If the visibility state of a mixed border object in the passed
 *  attribute map is missing or ambiguous (property `mixed` is `true`), the
 *  map value will be `null`. Otherwise, the boolean value of the property
 *  states whether the border is visible.
 */
export function getBorderFlags(borderAttrs: Dict<MixedBorder>, options?: GetBorderFlagsOptions): Record<AttrBorderKey, boolean | null> {

    // the border attribute names
    const attrNames = getBorderAttrNames(options);
    // result mapping border attribute names to boolean values
    const borderFlags = dict.create() as Record<AttrBorderKey, boolean | null>;

    dict.forEach(attrNames, (attrName, borderKey) => {
        const border = borderAttrs[attrName];
        if (is.dict(border)) {
            borderFlags[borderKey] = border.mixed ? null : isVisibleBorder(border);
        }
    });

    return borderFlags;
}

/**
 * Converts the passed object describing the visibility states of cell
 * border attributes to a table attribute map containing all border
 * attributes.
 *
 * @param borderFlags
 *  A dictionary with single-letter border keys ("l", "t", etc.), mapping
 *  boolean values specifying whether the respective border is visible.
 *  Properties not contained in this map will not occur in the returned
 *  attribute map.
 *
 * @param origAttrs
 *  The original attribute map of the target object (table, cell, ...). May
 *  also be a map of mixed borders with ambiguous border properties. If any
 *  of the border properties is ambiguous (value `null`), the default
 *  border will be used instead.
 *
 * @param defaultBorder
 *  Default border style style used when making a border attribute visible
 *  without having a corresponding visible border value in the passed
 *  original border attributes.
 *
 * @param [options]
 *  Optional parameters:
 *  - [options.paragraph=false]
 *    If set to `true`, a single inner border attribute `borderInside` will
 *    be used (for paragraphs which are stacked vertically and do not have
 *    vertical inner borders), instead of the two attributes
 *    `borderInsideHor` and `borderInsideVert`.
 *  - [options.clearNone=false]
 *    If set to `true`, and the border is invisible (e.g. the paragraph
 *    border is removed in text), it will set the related border attribute
 *    in the attributes map to `null` instead of `NO_BORDER`.
 *  - [options.fullBorderSet=false]
 *    If set to `true`, a full set of border attributes shall be returned.
 *
 * @returns
 *  The effective border attributes mentioned in the passed border flags.
 */
export function getBorderAttributes(borderFlags: PtRecord<AttrBorderKey, boolean>, origAttrs: Dict<MixedBorder>, defaultBorder: OpBorder, options?: GetBorderAttributesOptions): Dict<OpBorder | null> {

    // the border attribute names
    const attrNamesMap = getBorderAttrNames(options);
    // the resulting border attributes
    const borderAttrs = dict.create<OpBorder | null>();

    const copyBorderAttr = (attrName: string, visible?: boolean): void => {

        // set to `null` for invisible borders with option `clearNone`
        // (this causes to remove the border attribute from the attribute set)
        if (!visible && options?.clearNone) {
            borderAttrs[attrName] = null;
            return;
        }

        // the original border attribute value or mixed border object
        const origBorder = origAttrs[attrName];
        // whether the original border is visible
        const origVisible = isVisibleBorder(origBorder);
        // use default border, if any of the original border properties is ambiguous
        const newBorder = !visible ? NO_BORDER : (origVisible && !hasAmbiguousProperties(origBorder)) ? origBorder : defaultBorder;

        // build the border attribute value according to visibility and original borders
        const { mixed, ...tmpBorder } = json.deepClone(newBorder) as Dict;

        borderAttrs[attrName] = tmpBorder as unknown as OpBorder; // TODO
    };

    // fill the resulting attribute map
    dict.forEach(borderFlags, (visible, borderKey) => {
        const attrName = (attrNamesMap as Dict<string | undefined>)[borderKey];
        if (attrName) { copyBorderAttr(attrName, visible); }
    });

    // fill up missing border attributes if specified
    if (options?.fullBorderSet) {
        dict.forEach(attrNamesMap, attrName => {
            if (!borderAttrs[attrName]) {
                copyBorderAttr(attrName, isVisibleBorder(origAttrs[attrName]));
            }
        });
    }

    return borderAttrs;
}
