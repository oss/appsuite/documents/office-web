/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { is, ary, dict, pick, debug } from "@/io.ox/office/tk/algorithms";
import { LogLevel, Logger } from "@/io.ox/office/tk/utils/logger";

import type { Position, Operation, AnyOperation, OptAttrsOperation } from "@/io.ox/office/editframework/utils/operations";
import { equalPositions } from "@/io.ox/office/editframework/utils/operations";
import type { OperationAction } from "@/io.ox/office/editframework/utils/operationutils";

// types ======================================================================

/**
 * A collection with statistics for operation transformations in a document.
 */
export interface OTStatistics {

    /**
     * The total number of external operations trnasformed with pending local
     * actions without acknowlegdement.
     */
    extOpCount: number;

    /**
     * The total number of single operation transformations performed by the OT
     * manager of this application.
     */
    otCount: number;
}

/**
 * Optional parameters for helper functions that reduce properties in arbitrary
 * dictionaries in operation transformations.
 */
export interface ReducePropertyOptions {

    /**
     * If set to `true`, properties that have a (deeply) equal value in the
     * local and external dictionary will be deleted from both dictionaries.
     *
     * If set to `false`, properties with equal values will be kept in both
     * dictionaries (this implies that the local dictionary will never change,
     * because properties with different values will only be deleted from the
     * external dictionary).
     *
     * Default value is `false`.
     */
    deleteEqual?: boolean;
}

/**
 * Optional parameters for helper functions that remove empty attribute sets
 * from document operations.
 */
export interface RemoveEmptyAttrsOptions {

    /**
     * If set to `true`, an operation without any attributes left (the property
     * `attrs` is missing or is an empty dictionary) will be set to "removed"
     * state (transformation reduces attributes only).
     *
     * If set to `false`, an empty `attrs` property will be deleted from the
     * operation only (reducing attributes is part of a more complex
     * transformation).
     *
     * Default value is `false`.
     */
    removeEmptyOp?: boolean;
}

/**
 * Optional parameters for helper functions that reduce entire attribute sets
 * (operation property `attrs`) in operation transformations.
 */
export interface ReduceOperationAttrsOptions extends ReducePropertyOptions, RemoveEmptyAttrsOptions {
}

/**
 * The resulting array indexes of a transformed "move" or "copy" operation in
 * an array.
 */
export interface OTMoveResult {

    /**
     * The array index of the moved or copied element.
     */
    fromIdx: number;

    /**
     * The index in the array where the element will be moved or copied to.
     */
    toIdx: number;
}

/**
 * A permutation vector for sorting array elements. The values in this vector
 * represent the old positions of the elements in the array, that will be moved
 * to the respective location in the vector.
 */
export type OTSortVector = number[];

/**
 * The resulting array indexes of concurring "insert" or "delete" operations in
 * the same array.
 */
export interface OTShiftShiftResult {

    /**
     * The array index of the locally inserted or deleted element.
     */
    lclIdx: number;

    /**
     * The array index of the externally inserted or deleted element.
     */
    extIdx: number;
}

/**
 * The resulting array indexes of an "insert" or "delete" operation concurring
 * with a "move" or "copy" operation in the same array.
 */
export interface OTShiftMoveResult {

    /**
     * The array index of the inserted or deleted element.
     */
    shiftIdx: number;

    /**
     * The array indexes of the moved element; or `undefined`, if the moved
     * array element has been deleted by the "delete" operation.
     */
    moveRes: Opt<OTMoveResult>;
}

/**
 * The resulting array indexes of an "insert" or "delete" operation concurring
 * with a "sort" operation in the same array.
 */
export interface OTShiftSortResult {

    /**
     * The array index of the inserted or deleted element.
     */
    shiftIdx: number;

    /**
     * The transformed sort vector; or `undefined`, if the "sort" operation
     * becomes a no-op.
     */
    sortVec: Opt<OTSortVector>;
}

/**
 * The resulting array indexes of a "delete" operation concurring with a "copy"
 * operation in the same array.
 */
export interface OTDeleteCopyResult extends OTShiftMoveResult {

    /**
     * The array index of the copied element that needs to be deleted because
     * the delete operation has deleted the original copied element.
     */
    delToIdx: Opt<number>;
}

/**
 * The resulting array indexes of two concurring "move" operations in the same
 * array.
 */
export interface OTMoveMoveResult {

    /**
     * The array indexes of the transformed local "move" operation; or
     * `undefined`, if the local "move" operation becomes a no-op.
     */
    lclRes: Opt<OTMoveResult>;

    /**
     * The array indexes of the transformed external "move" operation; or
     * `undefined`, if the local "move" operation becomes a no-op.
     */
    extRes: Opt<OTMoveResult>;
}

/**
 * The resulting array indexes of a "move" operation concurring with a "copy"
 * operation in the same array.
 */
export interface OTMoveCopyResult {

    /**
     * The array indexes of the transformed "move" operation; or `undefined`,
     * if the "move" operation becomes a no-op.
     */
    moveRes: Opt<OTMoveResult>;

    /**
     * The array indexes of the transformed "copy" operation.
     */
    copyRes: OTMoveResult;
}

/**
 * The resulting array indexes of a "move" operation concurring with a "sort"
 * operation in the same array.
 */
export interface OTMoveSortResult {

    /**
     * The array indexes of the transformed "move" operation; or `undefined`,
     * if the "move" operation becomes a no-op.
     */
    moveRes: Opt<OTMoveResult>;

    /**
     * The transformed sort vector for the "sort" operation; or `undefined`, if
     * the "sort" operation becomes a no-op.
     */
    sortVec: Opt<OTSortVector>;
}

/**
 * The resulting array indexes of two concurring "copy" operations in the same
 * array.
 */
export interface OTCopyCopyResult {

    /**
     * The array indexes of the transformed local "copy" operation.
     */
    lclRes: OTMoveResult;

    /**
     * The array indexes of the transformed external "copy" operation.
     */
    extRes: OTMoveResult;
}

/**
 * The resulting sort vectors of two concurring "sort" operations in the same
 * array.
 */
export interface OTSortSortResult {

    /**
     * The transformed sort vector for the local "sort" operation; or
     * `undefined`, if the local "sort" operation becomes a no-op.
     */
    lclSortVec: Opt<OTSortVector>;

    /**
     * The transformed sort vector for the external "sort" operation; or
     * `undefined`, if the external "sort" operation becomes a no-op.
     */
    extSortVec: Opt<OTSortVector>;
}

// constants ==================================================================

/**
 * A marker for operations that need to be removed.
 */
const REMOVED_OPERATION_KEY = "_REMOVED_OPERATION_";

// logger =====================================================================

/**
 * Logger singleton for messages related to operation transformations, bound to
 * the URL debug flag `office:log-ot`.
 */
export const otLogger = new Logger("office:log-ot", { tag: "OT", tagColor: 0xFF00FF, recordAll: true }); // DOCS-3113, DOCS-3114, avoiding too much traffic in console

// class OTError ==============================================================

/**
 * A special error class for failures in implementations of operation
 * transformations.
 */
export class OTError extends Error {

    override readonly name = "OTError";
    readonly forceReload: boolean;
    readonly lclOp: Readonly<Operation>;
    readonly extOp: Readonly<Operation>;

    constructor(
        message: string,
        lclOp: Readonly<Operation>,
        extOp: Readonly<Operation>,
        forceReloadOption?: boolean
    ) {
        super(`${message} (lclOp="${lclOp.name}" extOp="${extOp.name}")`);
        this.lclOp = lclOp;
        this.extOp = extOp;
        this.forceReload = !!forceReloadOption;
    }
}

// private functions ==========================================================

function moveResult(fromIdx: number, toIdx: number): Opt<OTMoveResult> {
    return (fromIdx === toIdx) ? undefined : { fromIdx, toIdx };
}

function checkSortVec(sortVec: OTSortVector): Opt<OTSortVector> {
    return sortVec.every((from, to) => from === to) ? undefined : sortVec;
}

// public functions ===========================================================

/**
 * Returns a unique key for the transformation between two document operations
 * with the specified names.
 *
 * @param lclName
 *  The name of the local document operation.
 *
 * @param extName
 *  The name of the external document operation.
 *
 * @returns
 *  The unique key for the transformation between two document operations with
 *  the specified names.
 */
export function getOTKey(lclName: string, extName: string): string {
    return `${lclName}|${extName}`;
}

/**
 * Splits the passed unique key for a transformation between two document
 * operations (as returned by `getOTKey()`) into the operation names.
 *
 * @param otKey
 *  A unique key for the transformation between two document operations.
 *
 * @returns
 *  A result object with the properties `lclName` and `extName`.
 */
export function parseOTKey(otKey: string): { lclName: string; extName: string } {
    const tokens = otKey.split("|", 2);
    return { lclName: tokens[0] || "", extName: tokens[1] || "" };
}

/**
 * Returns whether the passed operation has the marker for being ignored in OT
 * handling.
 *
 * @param operation
 *  An operation handled by OT.
 *
 * @returns
 *  Whether the operation has the marker for being ignored in OT handling.
 */
export function isOperationRemoved(operation: Operation): boolean {
    return !!(operation as AnyOperation)[REMOVED_OPERATION_KEY];
}

/**
 * Adds a marker property to the passed operations causing to ignore them in OT
 * handling.
 *
 * @param operations
 *  One or more JSON document operations handled by OT.
 */
export function setOperationRemoved(...operations: Operation[]): void {
    operations.forEach(operation => ((operation as AnyOperation)[REMOVED_OPERATION_KEY] = 1));
}

/**
 * Copies specific existing properties from the source dictionary to the target
 * dictionary. If the property does not exist in the source dictionary, it will
 * be deleted in the target dictionary.
 *
 * @param targetMap
 *  The dictionary to write properties to.
 *
 * @param sourceMap
 *  The dictionary to read properties from.
 *
 * @param keys
 *  The keys of all properties to be copied.
 */
export function copyProperties<T extends object>(targetMap: T, sourceMap: Readonly<T>, ...keys: Array<keyof T>): void {
    keys.forEach(key => {
        if (key in sourceMap) {
            targetMap[key] = sourceMap[key];
        } else {
            delete targetMap[key];
        }
    });
}

/**
 * Reduces a specific property in local and external ditionaries for operation
 * transformation. This generic function can be used for operation properties,
 * for formatting attribute sets, etc.
 *
 * Removes the property from the external dictionary if it is contained in the
 * local dictionary, and the property values are different (local operations
 * always win by definition).
 *
 * Optionally, properties with (deeply) equal values will be removed from both
 * dictionaries.
 *
 * @param lclMap
 *  The dictionary from the local operation (already applied) that overrules
 *  the external operation. This dictionary will be reduced in-place!
 *
 * @param extMap
 *  The dictionary from the external operation (to be applied locally). This
 *  dictionary will be reduced in-place!
 *
 * @param keys
 *  The names of all properties (or a single property name) to be reduced in
 *  both dictionaries.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether the property has been removed from the external dictionary.
 */
export function reduceProperties<T extends object>(lclMap: T, extMap: T, keys: Array<KeysOf<T>> | KeysOf<T>, options?: ReducePropertyOptions): boolean {

    // whether the property has been deleted
    let reduced = false;

    // process single string or string array
    for (const key of ary.wrap(keys)) {

        // nothing to do, if either property is missing
        if (!(key in lclMap) || !(key in extMap)) { continue; }

        // compare property values deeply
        const isEqual = _.isEqual(lclMap[key], extMap[key]);

        // delete from local, if equal to external (only in optional delete-equal mode)
        if (options?.deleteEqual && isEqual) { delete lclMap[key]; reduced = true; }

        // delete from external, if different to local (or always in delete-equal mode)
        if (options?.deleteEqual || !isEqual) { delete extMap[key]; reduced = true; }
    }

    return reduced;
}

/**
 * Invokes a callback function for the value of an existing attribute in the
 * passed attributed document operation, and inserts the return value of the
 * callback function into the attribute set.
 *
 * @param attrOp
 *  The JSON document operation with an (optional) attribute set.
 *
 * @param family
 *  The style family of the attribute to be transformed.
 *
 * @param key
 *  The key of the attribute to be transformed.
 *
 * @param callback
 *  The callback function that will be invoked, if the specified attribute
 *  exists in the attribute set of the operation. Receives the attribute value,
 *  and the attribute key. Returns the new value of the attribute. Can return
 *  `undefined` to keep the attribute unchanged.
 */
export function transformAttribute(attrOp: OptAttrsOperation, family: string, key: string, callback: (value: unknown, key: string) => unknown): void {
    const attrMap = pick.dict(attrOp.attrs, family);
    if (attrMap && (key in attrMap)) {
        const value = callback(attrMap[key], key);
        if (value !== undefined) { attrMap[key] = value; }
    }
}

/**
 * Reduces the local and external attribute set for operation transformation.
 *
 * Removes the attributes from the external attribute set that are contained in
 * the local attribute set, and have different values, so that the external
 * attribute set can be applied to an object afterwards, but behaves as if this
 * happened before applying the local attribute set.
 *
 * Optionally, removes attributes with equal values from the local and external
 * attribute set. These attributes do not need to be applied again locally and
 * externally (optionally, because sometimes it may be better to keep these
 * attributes to prevent duplication of operations for partially overlapping
 * format ranges).
 *
 * @param lclAttrSet
 *  The local attribute set already applied at an object that overrules the
 *  external attributes. This object will be reduced in-place!
 *
 * @param extAttrSet
 *  The external attribute set to be applied at an object without changing the
 *  local attributes. This object will be reduced in-place!
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether at least one attribute has been removed from the attribute sets.
 */
export function reduceAttributeSets(lclAttrSet: Dict, extAttrSet: Dict, options?: ReducePropertyOptions): boolean {

    // whether an attribute has been deleted in the attribute sets
    let reduced = false;

    // process all attribute maps in the external attribute set
    dict.forEach(extAttrSet, (extAttrs, family) => {

        // special handling for style sheet identifier
        if (family === "style") {
            // "OR reduced" must be located at end to prevent short-circuit!
            reduced = reduceProperties(lclAttrSet, extAttrSet, "style", options) || reduced;
            return;
        }

        // fail-safety: attribute sets must contain object properties
        const lclAttrs = lclAttrSet[family];
        if (!is.dict(lclAttrs) || !is.dict(extAttrs)) { return; }

        // delete the external attributes that will be overwritten by the local attributes
        // TODO: use merger callback in attribute definition
        for (const key in extAttrs) {
            // "OR reduced" must be located at end to prevent short-circuit!
            reduced = reduceProperties(lclAttrs, extAttrs, key, options) || reduced;
        }

        // delete entire attribute map if it drained
        if (is.empty(lclAttrs)) { delete lclAttrSet[family]; }
        if (is.empty(extAttrs)) { delete extAttrSet[family]; }
    });

    return reduced;
}

/**
 * Removes the property `attrs` from the passed operation, if it is empty; or
 * optionally sets the entire operation to "removed" state in this case.
 *
 * @param attrOp
 *  The operation with an optional `attrs` property.
 *
 * @param options
 *  Optional parameters.
 */
export function removeEmptyAttributes(attrOp: OptAttrsOperation, options?: RemoveEmptyAttrsOptions): void {
    if (is.empty(attrOp.attrs)) {
        if (options?.removeEmptyOp) {
            setOperationRemoved(attrOp);
        } else {
            delete attrOp.attrs;
        }
    }
}

/**
 * Reduces the attribute sets in the local and external operation for operation
 * transformation, using the function `reduceAttributeSets()`.
 *
 * @param lclOp
 *  The local operation with an attribute set already applied at an object that
 *  overrules the external attributes. The attribute set will be reduced
 *  in-place, and will even be deleted from the operation if drained.
 *
 * @param extOp
 *  The external operation with an attribute set to be applied at an object
 *  without changing the local attributes. The attribute set will be reduced
 *  in-place, and will even be deleted from the operation if drained.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether at least one attribute has been removed from the attribute sets.
 */
export function reduceOperationAttributes(lclOp: OptAttrsOperation, extOp: OptAttrsOperation, options?: ReduceOperationAttrsOptions): boolean {
    const reduced = lclOp.attrs && extOp.attrs && reduceAttributeSets(lclOp.attrs, extOp.attrs, options);
    removeEmptyAttributes(lclOp, options);
    removeEmptyAttributes(extOp, options);
    return !!reduced;
}

// array index transformations ------------------------------------------------

/**
 * Transforms an array index against an "insert" operation for multiple
 * elements in the same array.
 *
 * @param xfIdx
 *  The array index to be transformed according to the "insert" oeration.
 *
 * @param insIdx
 *  The index in the array where the first new element will be inserted.
 *
 * @param insSize
 *  The number of inserted array elements.
 *
 * @returns
 *  The transformed array index. If the new array elements will be inserted
 *  before the passed index, it will be increased by the number of inserted
 *  elements.
 */
export function transformIndexInsert(xfIdx: number, insIdx: number, insSize: number): number {
    return (xfIdx < insIdx) ? xfIdx : (xfIdx + insSize);
}

/**
 * Transforms an array index against a "delete" operation for multiple elements
 * in the same array.
 *
 * @param xfIdx
 *  The array index to be transformed according to the "delete" oeration.
 *
 * @param delIdx
 *  The index of the first array element to be deleted.
 *
 * @param delSize
 *  The number of deleted array elements.
 *
 * @param [keepDel=false]
 *  If set to `true`, the array index will not be "deleted" (as indicated by
 *  returning `undefined`) but will be moved to the index of the first deleted
 *  array element.
 *
 * @returns
 *  The transformed array index. If the array elements will be deleted before
 *  the passed index, it will be decreased by the number of deleted elements.
 *  If the element itself will be deleted, `undefined` will be returned.
 */
export function transformIndexDelete(xfIdx: number, delIdx: number, delSize: number, keepDel: true): number;
export function transformIndexDelete(xfIdx: number, delIdx: number, delSize: number, keepDel?: boolean): Opt<number>;
// implementation
export function transformIndexDelete(xfIdx: number, delIdx: number, delSize: number, keepDel?: boolean): Opt<number> {
    return (xfIdx < delIdx) ? xfIdx : (xfIdx < delIdx + delSize) ? (keepDel ? delIdx : undefined) : (xfIdx - delSize);
}

/**
 * Transforms an array index against a "move" operation for multiple elements
 * in the same array.
 *
 * @param xfIdx
 *  The array index to be transformed according to the "move" operation.
 *
 * @param fromIdx
 *  The index of the first array element that will be moved to another index.
 *
 * @param fromSize
 *  The number of moved array elements.
 *
 * @param toIdx
 *  The index in the array where the elements will be moved to.
 *
 * @returns
 *  The transformed array index.
 */
export function transformIndexMove(xfIdx: number, fromIdx: number, fromSize: number, toIdx: number): number {
    // element moved by itself
    if ((fromIdx <= xfIdx) && (xfIdx < fromIdx + fromSize)) { return toIdx + xfIdx - fromIdx; }
    // element shifted backwards
    if ((fromIdx <= xfIdx) && (xfIdx < toIdx + fromSize)) { return xfIdx - fromSize; }
    // element shifted forwards
    if ((toIdx <= xfIdx) && (xfIdx < fromIdx)) { return xfIdx + fromSize; }
    // element not moved at all
    return xfIdx;
}

/**
 * Transforms an array index against a "sort" operation in the same array.
 *
 * @param xfIdx
 *  The array index to be transformed according to the "sort" operation.
 *
 * @param sortVec
 *  The sort vector for the array elements. See description of `OTSortVector`
 *  for details.
 *
 * @returns
 *  The transformed array index.
 */
export function transformIndexSort(xfIdx: number, sortVec: OTSortVector): number {
    const newIdx = sortVec.indexOf(xfIdx);
    return (newIdx >= 0) ? newIdx : xfIdx;
}

// array operations transformations -------------------------------------------

/**
 * Transforms array indexes for concurring "insert" operations in the same
 * array.
 *
 * @param lclIdx
 *  The array index of the locally inserted array element.
 *
 * @param extIdx
 *  The array index of the externally inserted array element.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexInsertInsert(lclIdx: number, extIdx: number): OTShiftShiftResult {

    // first, shift local index away (external index wins)
    lclIdx = transformIndexInsert(lclIdx, extIdx, 1);
    // transform external index with the new local insertion index
    extIdx = transformIndexInsert(extIdx, lclIdx, 1);

    return { lclIdx, extIdx };
}

/**
 * Transforms array indexes for an "insert" operation concurring with a
 * "delete" operation in the same array.
 *
 * @param insIdx
 *  The array index of the new array element.
 *
 * @param delIdx
 *  The array index of the deleted array element.
 *
 * @returns
 *  The transformed array indexes for both operations. The property `lclIdx`
 *  will contain the transformed insertion index (parameter `insIdx`); the
 *  property `extIdx` will contain the transformed deletion index (parameter
 *  `delIdx`).
 */
export function transformIndexInsertDelete(insIdx: number, delIdx: number): OTShiftShiftResult {

    // first, shift away delete index according to insert index (new element cannot collide with existing element)
    delIdx = transformIndexInsert(delIdx, insIdx, 1);
    // transform insert index with the new deletion index (will not collide, see above)
    insIdx = transformIndexDelete(insIdx, delIdx, 1, true);

    return { lclIdx: insIdx, extIdx: delIdx };
}

/**
 * Transforms array indexes for an "insert" operation concurring with a "move"
 * operation in the same array.
 *
 * @param insIdx
 *  The array index of the new array element.
 *
 * @param fromIdx
 *  The array index of the moved array element.
 *
 * @param toIdx
 *  The index in the array where the element will be moved to.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexInsertMove(insIdx: number, fromIdx: number, toIdx: number): OTShiftMoveResult {

    // transform the move indexes according to the insert operation
    fromIdx = transformIndexInsert(fromIdx, insIdx, 1);
    toIdx = transformIndexInsert(toIdx, insIdx, 1);
    // transform the insert index according to the *new* move indexes
    insIdx = transformIndexMove(insIdx, fromIdx, 1, toIdx);

    return { shiftIdx: insIdx, moveRes: moveResult(fromIdx, toIdx) };
}

/**
 * Transforms array indexes for an "insert" operation concurring with a "copy"
 * operation in the same array.
 *
 * @param insIdx
 *  The array index of the new array element.
 *
 * @param fromIdx
 *  The array index of the copied array element.
 *
 * @param toIdx
 *  The index in the array where the copied element will be inserted.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexInsertCopy(insIdx: number, fromIdx: number, toIdx: number): OTShiftMoveResult {

    // transform the index of the copied element according to the insert index
    fromIdx = transformIndexInsert(fromIdx, insIdx, 1);
    // "insert" wins over "copy" per definition (shift cloned element away)
    if (insIdx <= toIdx) { toIdx += 1; } else { insIdx += 1; }

    return { shiftIdx: insIdx, moveRes: { fromIdx, toIdx } };
}

/**
 * Transforms an "insert" operation and a "sort" operation in the same array.
 *
 * @param insIdx
 *  The array index of the new element inserted into the array.
 *
 * @param sortVec
 *  The sort vector for the array elements.
 *
 * @returns
 *  The transformed index and sort vector for both operations.
 */
export function transformIndexInsertSort(insIdx: number, sortVec: OTSortVector): OTShiftSortResult {
    sortVec = sortVec.map(xfIdx => transformIndexInsert(xfIdx, insIdx, 1));
    sortVec.splice(insIdx, 0, insIdx);
    // the position of the inserted element will not change
    return { shiftIdx: insIdx, sortVec: checkSortVec(sortVec) };
}

/**
 * Transforms array indexes for concurring "delete" operations in the same
 * array.
 *
 * @param lclIdx
 *  The array index of the locally deleted array element.
 *
 * @param extIdx
 *  The array index of the externally deleted array element.
 *
 * @returns
 *  The transformed array indexes for both operations; or `undefined`, if both
 *  operations delete the same array element (the passed indexes are equal).
 */
export function transformIndexDeleteDelete(lclIdx: number, extIdx: number): Opt<OTShiftShiftResult> {

    // both operations delete the same array element (operations become no-ops)
    if (lclIdx === extIdx) { return undefined; }

    // transform both indexes with the original (untransformed) indexes
    const newExtIdx = transformIndexDelete(extIdx, lclIdx, 1, true);
    const newLclIdx = transformIndexDelete(lclIdx, extIdx, 1, true);

    return { lclIdx: newLclIdx, extIdx: newExtIdx };
}

/**
 * Transforms array indexes for a "delete" operation concurring with a "move"
 * operation in the same array.
 *
 * @param delIdx
 *  The array index of the deleted array element.
 *
 * @param fromIdx
 *  The array index of the moved array element.
 *
 * @param toIdx
 *  The index in the array where the element will be moved to.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexDeleteMove(delIdx: number, fromIdx: number, toIdx: number): OTShiftMoveResult {

    // moved element will be deleted too: delete operation wins
    if (delIdx === fromIdx) { return { shiftIdx: toIdx, moveRes: undefined }; }

    // transform the delete index according to the move operation
    const shiftIdx = transformIndexMove(delIdx, fromIdx, 1, toIdx);
    // compare source index with *old* deletion index (equality already checked above)
    fromIdx = transformIndexDelete(fromIdx, delIdx, 1, true);
    // compare target index with *new* deletion index (equality already checked above)
    toIdx = transformIndexDelete(toIdx, shiftIdx, 1, true);

    return { shiftIdx, moveRes: moveResult(fromIdx, toIdx) };
}

/**
 * Transforms array indexes for a "delete" operation concurring with a "copy"
 * operation in the same array.
 *
 * @param delIdx
 *  The array index of the deleted array element.
 *
 * @param fromIdx
 *  The array index of the copied array element.
 *
 * @param toIdx
 *  The index in the array where the copied element will be inserted.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexDeleteCopy(delIdx: number, fromIdx: number, toIdx: number): OTDeleteCopyResult {

    // move source index of copied element according to deleted element
    const newFromIdx = transformIndexDelete(fromIdx, delIdx, 1);

    // transform delete index according to index of cloned element
    const shiftIdx = transformIndexInsert(delIdx, toIdx, 1);

    // keep target index if it would be deleted
    toIdx = transformIndexDelete(toIdx, delIdx, 1, true);

    // create the result object (omit copy indexes if element was deleted)
    const moveRes = is.number(newFromIdx) ? { fromIdx: newFromIdx, toIdx } : undefined;
    return { shiftIdx, moveRes, delToIdx: moveRes ? undefined : toIdx };
}

/**
 * Transforms a "delete" operation and a "sort" operation in the same array.
 *
 * @param delIdx
 *  The array index of the deleted array element.
 *
 * @param sortVec
 *  The sort vector for the array elements.
 *
 * @returns
 *  The transformed index and sort vector for both operations.
 */
export function transformIndexDeleteSort(delIdx: number, sortVec: OTSortVector): OTShiftSortResult {
    const newDelIdx = transformIndexSort(delIdx, sortVec);
    sortVec = sortVec.slice();
    sortVec.splice(newDelIdx, 1);
    sortVec = sortVec.map(xfIdx => transformIndexDelete(xfIdx, delIdx, 1, true));
    return { shiftIdx: newDelIdx, sortVec: checkSortVec(sortVec) };
}

/**
 * Transforms array indexes for concurring "move" operations in the same array.
 *
 * @param lclFrom
 *  The array index of the locally moved element.
 *
 * @param lclTo
 *  The index in the array where the local element will be moved to.
 *
 * @param extFrom
 *  The array index of the externally moved element.
 *
 * @param extTo
 *  The index in the array where the external element will be moved to.
 *
 * @returns
 *  The transformed array indexes for both "move" operations.
 */
export function transformIndexMoveMove(lclFrom: number, lclTo: number, extFrom: number, extTo: number): OTMoveMoveResult {

    // transform both "from" positions according to the opposite move operation
    const newLclFrom = transformIndexMove(lclFrom, extFrom, 1, extTo);
    const newExtFrom = transformIndexMove(extFrom, lclFrom, 1, lclTo);

    // special handling needed if both clients move the same element, or if the external move is a no-op:
    // local operation wins, external operation must be ignored
    if ((lclFrom === extFrom) || (extFrom === extTo)) {
        extTo = newExtFrom;
    } else {
        // transform local target index for server according to external operation (already applied there)
        lclTo = transformIndexMove(lclTo, newExtFrom, 1, extTo);
        // transform external target index according to local operation already applied
        extTo = transformIndexMove(extTo, newLclFrom, 1, lclTo);
    }

    return { lclRes: moveResult(newLclFrom, lclTo), extRes: moveResult(newExtFrom, extTo) };
}

/**
 * Transforms array indexes for a "move" operation concurring with a "copy"
 * operation in the same array.
 *
 * @param moveFrom
 *  The array index of the moved element.
 *
 * @param moveTo
 *  The index in the array where the element will be moved to.
 *
 * @param copyFrom
 *  The array index of the copied element.
 *
 * @param copyTo
 *  The index in the array where the copied element will be inserted.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexMoveCopy(moveFrom: number, moveTo: number, copyFrom: number, copyTo: number): OTMoveCopyResult {

    const oldMoveFrom = moveFrom, oldMoveTo = moveTo;

    // transform move indexes
    moveFrom = transformIndexInsert(moveFrom, copyTo, 1);
    moveTo = transformIndexInsert(moveTo, copyTo, 1);
    // transform copy indexes
    copyFrom = transformIndexMove(copyFrom, oldMoveFrom, 1, oldMoveTo);
    copyTo = transformIndexMove(copyTo, moveFrom, 1, moveTo);

    return { moveRes: moveResult(moveFrom, moveTo), copyRes: { fromIdx: copyFrom, toIdx: copyTo } };
}

/**
 * Transforms array indexes for a "move" operation concurring with a "sort"
 * operation in the same array.
 *
 * @param fromIdx
 *  The array index of the moved element.
 *
 * @param toIdx
 *  The index in the array where the element will be moved to.
 *
 * @param sortVec
 *  The sort vector for the array elements.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexMoveSort(fromIdx: number, toIdx: number, sortVec: OTSortVector): OTMoveSortResult {
    // adjust the sort vector to neutralize the move operation
    sortVec = sortVec.map(xfIdx => transformIndexMove(xfIdx, fromIdx, 1, toIdx));
    // always ignore the move operation (per definition)
    return { moveRes: undefined, sortVec: checkSortVec(sortVec) };
}

/**
 * Transforms array indexes for concurring "copy" operations in the same array.
 *
 * @param lclFrom
 *  The array index of the locally copied element.
 *
 * @param lclTo
 *  The index in the array where the new local element will be inserted.
 *
 * @param extFrom
 *  The array index of the externally copied element.
 *
 * @param extTo
 *  The index in the array where the new external element will be inserted.
 *
 * @returns
 *  The transformed array indexes for both "copy" operations.
 */
export function transformIndexCopyCopy(lclFrom: number, lclTo: number, extFrom: number, extTo: number): OTCopyCopyResult {
    const oldLclTo = lclTo;
    lclFrom = transformIndexInsert(lclFrom, extTo, 1);
    lclTo = transformIndexInsert(oldLclTo, extTo, 1);
    extFrom = transformIndexInsert(extFrom, oldLclTo, 1);
    extTo = transformIndexInsert(extTo, lclTo, 1);
    return { lclRes: { fromIdx: lclFrom, toIdx: lclTo }, extRes: { fromIdx: extFrom, toIdx: extTo } };
}

/**
 * Transforms array indexes for a "copy" operation concurring with a "sort"
 * operation in the same array.
 *
 * @param fromIdx
 *  The array index of the copied element.
 *
 * @param toIdx
 *  The index in the array where the new element will be inserted.
 *
 * @param sortVec
 *  The sort vector for the array elements.
 *
 * @returns
 *  The transformed array indexes for both operations.
 */
export function transformIndexCopySort(fromIdx: number, toIdx: number, sortVec: OTSortVector): OTMoveSortResult {
    fromIdx = transformIndexSort(fromIdx, sortVec);
    sortVec = sortVec.map(xfIdx => transformIndexInsert(xfIdx, toIdx, 1));
    // the insertion index of the copied element will not be changed
    sortVec.splice(toIdx, 0, toIdx);
    return { moveRes: { fromIdx, toIdx }, sortVec: checkSortVec(sortVec) };
}

/**
 * Transforms array indexes for concurring "sort" operations in the same array.
 *
 * @param lclSortVec
 *  The sort vector of the local "sort" operation.
 *
 * @param extSortVec
 *  The sort vector of the external "sort" operation.
 *
 * @returns
 *  The transformed sort vectors for both "sort" operations.
 */
export function transformIndexSortSort(lclSortVec: OTSortVector, extSortVec: OTSortVector): OTSortSortResult {
    // transform local sort vector according to external sorting
    lclSortVec = lclSortVec.map(xfIdx => transformIndexSort(xfIdx, extSortVec));
    // always ignore the external sort operation (local array order wins)
    return { lclSortVec: checkSortVec(lclSortVec), extSortVec: undefined };
}

// position transformations ---------------------------------------------------

/**
 * Transforms a document position in-place against an "insert" operation.
 *
 * @param xfPos
 *  The document position to be transformed in-place against the "insert"
 *  operation.
 *
 * @param insPos
 *  The start position of the inserted component.
 *
 * @param insSize
 *  The size of the inserted component.
 *
 * @returns
 *  The transformed document position (a reference to the same array passed in
 *  `xfPos` which has been transformed in-place).
 */
export function transformPositionInsert(xfPos: Position, insPos: Position, insSize: number): Position {
    const ai = insPos.length - 1;
    if ((ai < xfPos.length) && equalPositions(xfPos, insPos, ai)) {
        xfPos[ai] = transformIndexInsert(xfPos[ai], insPos[ai], insSize);
    }
    return xfPos;
}

/**
 * Transforms a document position in-place against a "delete" operation.
 *
 * @param xfPos
 *  The document position to be transformed in-place against the "delete"
 *  operation.
 *
 * @param delPos
 *  The start position of the deleted component.
 *
 * @param delSize
 *  The size of the deleted component.
 *
 * @returns
 *  The transformed document position (a reference to the same array passed in
 *  `xfPos` which has been transformed in-place); or `undefined`, if the
 *  transformed document position was deleted implicitly by the "delete"
 *  operation.
 */
export function transformPositionDelete(xfPos: Position, delPos: Position, delSize: number): Opt<Position> {
    const ai = delPos.length - 1;
    if ((ai < xfPos.length) && equalPositions(xfPos, delPos, ai)) {
        const xfIdx = transformIndexDelete(xfPos[ai], delPos[ai], delSize);
        if (!is.number(xfIdx)) { return undefined; }
        xfPos[ai] = xfIdx;
    }
    return xfPos;
}

/**
 * Transforms a document position in-place against a "move" operation in the
 * same parent component.
 *
 * @param xfPos
 *  The document position to be transformed in-place against the "move"
 *  operation.
 *
 * @param fromPos
 *  The start position of the moved component.
 *
 * @param fromSize
 *  The size of the moved component.
 *
 * @param toIdx
 *  The target index where the component will be moved to (inside its parent
 *  component, i.e. by changing the last element of `fromPos`).
 *
 * @returns
 *  The transformed document position (a reference to the same array passed in
 *  `xfPos` which has been transformed in-place).
 */
export function transformPositionMove(xfPos: Position, fromPos: Position, fromSize: number, toIdx: number): Position {
    const ai = fromPos.length - 1;
    if ((ai < xfPos.length) && equalPositions(xfPos, fromPos, ai)) {
        xfPos[ai] = transformIndexMove(xfPos[ai], fromPos[ai], fromSize, toIdx);
    }
    return xfPos;
}

// debugging ------------------------------------------------------------------

/**
 * Stringifies the passed JSON operation for debug logging.
 */
export function dbgFormatOp(op?: Operation): string {
    if (!op) { return ""; }
    const { name, osn, opl, dbg_op_id, dbg_user_id, dbg_merged, ...props } = op;
    return `${name}(${debug.stringify(props).slice(1, -1)})`;
}

/**
 * Stringifies the passed JSON operations array for debug logging.
 */
export function dbgFormatOps(ops: Operation[], indent: number): string {
    const indStr = "\xa0".repeat(indent);
    return ops.map((op, idx) => `${indStr}[${idx}] ${dbgFormatOp(op)}`).join("\n");
}

/**
 * Stringifies the passed action user data for debug logging.
 */
export function dbgFormatUserData(userData: object, indent: number): string {
    const indStr1 = "\xa0".repeat(indent);
    const indStr2 = `\n\xa0${indStr1}`;
    // eslint-disable-next-line @typescript-eslint/no-base-to-string
    return `${indStr1}userData:${indStr2}${String(userData).replace(/\n/g, indStr2)}`;
}

/**
 * Stringifies the passed JSON operation action for debug logging.
 */
export function dbgFormatAction(action: OperationAction, index: number, indent: number): string {
    const indStr = "\xa0".repeat(indent);
    const osn = action.operations[0]?.osn;
    let msg = `${indStr}action[${index}]${is.number(osn) ? ` osn=${osn}` : ""} sentToServer=${!!action.sentToServer}`;
    if (action.userData) { msg += `\n${dbgFormatUserData(action.userData, indent + 1)}`; }
    return `${msg}\n${indStr} operations:\n${dbgFormatOps(action.operations, indent + 2)}`;
}

/**
 * Stringifies the passed JSON operation actions for debug logging.
 */
export function dbgFormatActions(actions: OperationAction[], indent: number): string {
    return actions.map((a, i) => dbgFormatAction(a, i, indent)).join("\n");
}

export function dbgDumpActions(msg: string, actions: OperationAction[]): void {
    otLogger.log(() => {
        const lines = [msg];
        if (otLogger.isLogLevelActive(LogLevel.TRACE)) { lines.push(dbgFormatActions(actions, 1)); }
        return lines.join("\n");
    });
}
