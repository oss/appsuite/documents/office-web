/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, jpromise } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import { convertHmmToLength } from '@/io.ox/office/tk/dom';
import { Canvas } from '@/io.ox/office/tk/canvas';

import { getFileUrl } from '@/io.ox/office/drawinglayer/utils/imageutils';
import { getStrechedCoords } from '@/io.ox/office/drawinglayer/utils/drawingutils';

const round = Math.round;

// constants ==============================================================

// all unresolved rendering context wrappers waiting for texture to be
// loaded in, keyed by object UIDs. This ensures that only one <img> loading
// for one canvas is done.
// Must be property maintained and cleaned up, as it is global -> memleaks
const pendingContextSet = new Set/*<Canvas2DContext>*/();

// private functions ======================================================

function calculateOffset(rectAlignment, offsetX, offsetY, widthPx, heightPx, patternWidth, patternHeight, options) {

    let tempX = 0;
    let tempY = 0;
    // if tile is flipped/ mirrored (none, horizontal - x, vertical - y, or both - xy)
    const flipMode = options && options.flipMode;
    // used for ODF stretching when image (pattern) size is greater than canvas size, image is anchored to coresponding rectAlignment edge
    const anchorToEdge = options && options.anchorToEdge;
    // used for ODF streching, if width of image is greater than canvas width
    const widthDiff = patternWidth > widthPx ? widthPx - patternWidth : 0;
    // used for ODF streching, if height of image is greater than canvas height
    const heightDiff = patternHeight > heightPx ? heightPx - patternHeight : 0;

    switch (rectAlignment) {
        case 'topLeft':
            tempX = 0;
            tempY = 0;
            break;
        case 'top':
            tempX = ((widthPx - patternWidth) / 2) % patternWidth;
            if (flipMode) { tempX += patternWidth / 4; }
            if (anchorToEdge && widthDiff) { tempX = widthDiff / 2; }
            break;
        case 'topRight':
            tempX = widthPx % patternWidth;
            if (flipMode) { tempX += patternWidth / 2; }
            if (anchorToEdge && widthDiff) { tempX = widthDiff; }
            break;
        case 'left':
            tempY = ((heightPx - patternHeight) / 2) % patternHeight;
            if (flipMode) { tempY += patternHeight / 4; }
            if (anchorToEdge && heightDiff) { tempY = heightDiff / 2; }
            break;
        case 'center':
            tempX = ((widthPx - patternWidth) / 2) % patternWidth;
            tempY = ((heightPx - patternHeight) / 2) % patternHeight;
            if (flipMode) {
                tempX += patternWidth / 4;
                tempY += patternHeight / 4;
            }
            if (anchorToEdge && widthDiff) { tempX = widthDiff / 2; }
            if (anchorToEdge && heightDiff) { tempY = heightDiff / 2; }
            break;
        case 'right':
            tempX = widthPx % patternWidth;
            tempY = ((heightPx - patternHeight) / 2) % patternHeight;
            if (flipMode) {
                tempX += patternWidth / 2;
                tempY += patternHeight / 4;
            }
            if (anchorToEdge && widthDiff) { tempX = widthDiff; }
            if (anchorToEdge && heightDiff) { tempY = heightDiff / 2; }
            break;
        case 'bottomLeft':
            tempY = heightPx % patternHeight;
            if (flipMode) { tempY += patternHeight / 2; }
            if (anchorToEdge && heightDiff) { tempY = heightDiff; }
            break;
        case 'bottom':
            tempX = ((widthPx - patternWidth) / 2) % patternWidth;
            tempY = heightPx % patternHeight;
            if (flipMode) {
                tempX += patternWidth / 4;
                tempY += patternHeight / 2;
            }
            if (anchorToEdge && widthDiff) { tempX = widthDiff / 2; }
            if (anchorToEdge && heightDiff) { tempY = heightDiff; }
            break;
        case 'bottomRight':
            tempX = widthPx % patternWidth;
            tempY = heightPx % patternHeight;
            if (flipMode) {
                tempX += patternWidth / 2;
                tempY += patternHeight / 2;
            }
            if (anchorToEdge && widthDiff) { tempX = widthDiff; }
            if (anchorToEdge && heightDiff) { tempY = heightDiff; }
            break;
        default:
            globalLogger.error('$badge{Texture} calculateOffset: rectAlignment is unknown!');
    }

    // whether offset should be explicitely set
    if (options && options.useOffset) {
        offsetX %= patternWidth;
        offsetY %= patternHeight;
        // if the values are positive, we need to shift them top left for the 1 pattern dimensions
        // this way pattern will always cover whole canvas object, and there will be no gaps if offset is positive
        if (offsetX > 0) { offsetX -= patternWidth; }
        if (offsetY > 0) { offsetY -= patternHeight; }
        if (tempX > 0) { tempX -= patternWidth; }
        if (tempY > 0) { tempY -= patternHeight; }
        tempX += offsetX;
        tempY += offsetY;
        tempX %= patternWidth;
        tempY %= patternHeight;
    }

    return { offsetX: tempX, offsetY: tempY };
}

/**
 * ODF specific bitmap streching.
 *
 * @param {Object} odfStretch
 *  Contains properties of the odf specific bitmap stretch.
 * @param {Number} canvasW
 *  Width of the containing canvas, in pixels.
 * @param {Number} canvasH
 *  Height of the containing canvas, in pixels.
 * @param {Number} imgWidth
 *  Image width in pixels.
 * @param {Number} imgHeight
 *  Image height in pixels.
 *
 * @returns {Object}
 */
function getStretchOdfCoords(odfStretch, canvasW, canvasH, imgWidth, imgHeight) {

    const getScaledVal = (value, scale) => { return value * (scale / 100); };

    const sWidth = odfStretch.width ? convertHmmToLength(odfStretch.width, 'px', 1) : (is.number(odfStretch.scaleX) ? getScaledVal(canvasW, odfStretch.scaleX) : imgWidth);
    const sHeight = odfStretch.height ? convertHmmToLength(odfStretch.height, 'px', 1) : (is.number(odfStretch.scaleY) ? getScaledVal(canvasH, odfStretch.scaleY) : imgHeight);
    const rectAlignment = odfStretch.rectAlignment;

    const calcOffset = calculateOffset(rectAlignment, 0, 0, canvasW, canvasH, sWidth, sHeight, { anchorToEdge: true });

    const tLeft = calcOffset.offsetX;
    const tWidth = sWidth;
    const tTop = calcOffset.offsetY;
    const tHeight = sHeight;

    return { left: round(tLeft), top: round(tTop), width: round(tWidth), height: round(tHeight) };
}

// public functions =======================================================

/**
 * Creates and returns texture fill for given object, based on bitmap and its attributes.
 *
 * @param  {PresentationModel} docModel
 *         Presentation doc model
 * @param  {Canvas2DContext} context
 *         Context wrapper context as provided by the class Canvas.
 * @param  {Object} fillAttrs
 *         Shape fill attributes
 * @param  {Number} widthPx
 *         Canvas width in pixels
 * @param  {Number} heightPx
 *          Canvas height in pixels
 * @param  {Number|null} rotationAngle
 *         If exist, angle of rotation in radians
 * @param  {Object} [options]
 *         Optional set of properties.
 *  @param {Boolean} [options.isSlide=false]
 *         If texture is applied to slide background.
 *  @param {Boolean} [options.noAlpha=false]
 *         If alpha should be ignored.
 *  @param {Array<String>|Null} [options.themeTargets]
 *         The theme target chain according to the target resolver callback.
 *
 * @returns {jQuery.Promise}
 *  A promise that will be resolved with the generated canvas pattern.
 */
export function getTextureFill(docModel, context, fillAttrs, widthPx, heightPx, rotationAngle, options) {

    // bug 52106, imageUrl must be specified
    if (!fillAttrs || !fillAttrs.bitmap || !fillAttrs.bitmap.imageUrl) { return jpromise.reject(); }

    // prevent multiple entrance with same canvas
    if (pendingContextSet.has(context)) { return jpromise.reject(); }
    pendingContextSet.add(context);

    // create an <img> element from the passed image URL
    const imageUrl = getFileUrl(docModel.docApp, fillAttrs.bitmap.imageUrl);
    let promise = docModel.docApp.createImageNode(imageUrl);

    promise = promise.then(imgNode => {

        // tiling properties as an object, sent with operation
        const tilingObj = fillAttrs.bitmap.tiling;
        // whether tiling properties are existing
        const isTiling = !_.isEmpty(tilingObj);
        // effects
        const effectsArray = fillAttrs.bitmap.effects;
        // stretching properties as an objects, sent with operation
        const stretchingObj = fillAttrs.bitmap.stretching;
        // checking existence of streching properties
        const isStretching = !_.isEmpty(stretchingObj);
        // ODF specific stretching properties
        const odfStretchingObj = fillAttrs.bitmap.stretchingAligned;
        // check for existence of ODF streching properties
        const isOdfStretching = !_.isEmpty(odfStretchingObj);
        // canvas repeat property, depending on set tiling
        const isRepeat = isTiling ? 'repeat' : 'no-repeat';
        // wheter to unrotate texture for complementary negative angle or not (slide backgrounds have this always)
        const isRotateWithShape = (options && options.isSlide) || (fillAttrs.bitmap.rotateWithShape !== false);
        // streching percentage values for x and y direction
        const stretchX = (isTiling && is.number(tilingObj.stretchX) && tilingObj.stretchX > 0) ? tilingObj.stretchX / 100 : 1;
        const stretchY = (isTiling && is.number(tilingObj.stretchY) && tilingObj.stretchY > 0) ? tilingObj.stretchY / 100 : 1;
        // offset number values from where to start tiling or image. Can be also negative
        const offsetX = (isTiling && is.number(tilingObj.offX)) ? convertHmmToLength(tilingObj.offX, 'px', 1) : 0;
        const offsetY = (isTiling && is.number(tilingObj.offY)) ? convertHmmToLength(tilingObj.offY, 'px', 1) : 0;
        // normalized transparency value (canvas uses inverted value of filter stored one)
        const nTransparency = is.number(fillAttrs.bitmap.transparency) ? (1 - fillAttrs.bitmap.transparency) : 1;
        // rectangle alignment properties
        const rectAlignment = (isTiling && tilingObj.rectAlignment) || null;
        // if tile is flipped/ mirrored (none, horizontal - x, vertical - y, or both - xy)
        const flipMode = (isTiling && tilingObj.flipMode) || null;
        // if flip mode is used
        const isFlipMode = flipMode && flipMode !== 'none';
        const dpiDiff = 96 / 96; // TODO: some images have different dpi, filter needs to send them
        // canvas width in pixels
        let cWidth = isTiling ? round(imgNode[0].width * stretchX * dpiDiff) : widthPx;
        // canvas height in pixels
        let cHeight = isTiling ? round(imgNode[0].height * stretchY * dpiDiff) : heightPx;
        // temporary helper canvas to store intermediate state
        const tempCanvas = new Canvas();
        // another helper canvas to store offset state
        let offsetCanvas = null;
        const themeTargets = options?.themeTargets;

        if (!isTiling && !isRotateWithShape) {
            cWidth = widthPx * Math.abs(Math.cos(rotationAngle)) + heightPx * Math.abs(Math.sin(rotationAngle));
            cHeight = heightPx * Math.abs(Math.cos(rotationAngle)) + widthPx * Math.abs(Math.sin(rotationAngle));
        }

        tempCanvas.initialize({ width: cWidth, height: cHeight });

        // $('body').append(tempCanvas.$el.css({ zIndex: 99, position: 'fixed' })); // DEBUG

        // scaling of the image
        tempCanvas.render(tempCtx => {

            const getDuotoneColorComponent = (base, color1, color2) => {
                color2 = color2 * base / 0xFF;
                color1 = color1 * (0xFF - base) / 0xFF;
                return Math.trunc(color1 + color2);
            };

            const getLuminance = (red, green, blue) => {
                return Math.trunc((blue * 29 + green * 151 + red * 76) / 256);
            };

            let coordObj; // left, top, width and height coordinate values where to place image on canvas

            if (isStretching) {
                coordObj = getStrechedCoords(stretchingObj, cWidth, cHeight);
            } else if (isOdfStretching) {
                coordObj = getStretchOdfCoords(odfStretchingObj, cWidth, cHeight, imgNode[0].width, imgNode[0].height);
            } else {
                coordObj = { left: 0, top: 0, width: cWidth, height: cHeight };
            }

            // optional parameter wheter or not to ignore alpha transparency value
            if (!options?.noAlpha) {
                tempCtx.setGlobalAlpha(nTransparency);
            }

            tempCtx.drawImage(imgNode[0], coordObj);

            if (Array.isArray(effectsArray)) {
                for (const effect of effectsArray) {

                    if (effect.type === 'duotone') {

                        const [r1, g1, b1] = docModel.parseAndResolveColor(effect.color, 'fill', themeTargets).bytes;
                        const [r2, g2, b2] = docModel.parseAndResolveColor(effect.color2, 'fill', themeTargets).bytes;

                        const imgData = tempCtx.ctx.getImageData(0, 0, cWidth, cHeight);
                        const { data } = imgData;

                        for (let i = 0; i < data.length; i += 4) {
                            const base = getLuminance(data[i], data[i + 1], data[i + 2]);
                            data[i]     = getDuotoneColorComponent(base, r1, r2);
                            data[i + 1] = getDuotoneColorComponent(base, g1, g2);
                            data[i + 2] = getDuotoneColorComponent(base, b1, b2);
                        }
                        tempCtx.ctx.putImageData(imgData, 0, 0);
                    }
                }
            }
        });

        // offset
        if (offsetX || offsetY || rectAlignment || flipMode) {
            // another helper canvas to store offset state
            offsetCanvas = new Canvas({ location: { width: 2 * cWidth, height: 2 * cHeight } });
            // $('body').append(offsetCanvas.$el.css({ zIndex: 99, position: 'fixed', top: (cHeight + 20) })); // DEBUG

            if (isFlipMode) {
                switch (flipMode) {
                    case 'x':
                        offsetCanvas.render(offsetCtx => {
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: cHeight, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.scale(-1, 1);
                            offsetCtx.translate(-cWidth * 2, 0);
                            //offsetCtx.drawRect(0, 0, 2 * cWidth, 2 * cHeight, 'stroke'); // DEBUG
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: cHeight, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                        });
                        break;
                    case 'y':
                        offsetCanvas.render(offsetCtx => {
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.drawImage(tempCanvas, { left: cWidth, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.scale(1, -1);
                            offsetCtx.translate(0, -cHeight * 2);
                            //offsetCtx.drawRect(0, 0, 2 * cWidth, 2 * cHeight, 'stroke'); // DEBUG
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.drawImage(tempCanvas, { left: cWidth, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                        });
                        break;
                    case 'xy':
                        offsetCanvas.render(offsetCtx => {
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.scale(-1, 1);
                            offsetCtx.translate(-cWidth * 2, 0);
                            //offsetCtx.drawRect(0, 0, 2 * cWidth, 2 * cHeight, 'stroke'); // DEBUG
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.scale(1, -1);
                            offsetCtx.translate(0, -cHeight * 2);
                            //offsetCtx.drawRect(0, 0, 2 * cWidth, 2 * cHeight, 'stroke'); // DEBUG
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                            offsetCtx.scale(-1, 1);
                            offsetCtx.translate(-cWidth * 2, 0);
                            //offsetCtx.drawRect(0, 0, 2 * cWidth, 2 * cHeight, 'stroke'); // DEBUG
                            offsetCtx.drawImage(tempCanvas, { left: 0, top: 0, width: cWidth, height: cHeight }, { left: 0, top: 0, width: cWidth, height: cHeight });
                        });
                        break;
                    default:
                        globalLogger.error('$badge{Texture} getTextureFill: flipMode is unknown!');
                }

                offsetCanvas.render(offsetCtx => {
                    const calcOffset = calculateOffset(rectAlignment, offsetX, offsetY, widthPx, heightPx, cWidth * 2, cHeight * 2, { flipMode: true, useOffset: true });
                    const pattern = offsetCtx.createPattern(offsetCanvas, 'repeat');
                    offsetCtx.clearRect(0, 0, 2 * cWidth, 2 * cHeight);
                    offsetCtx.translate(calcOffset.offsetX, calcOffset.offsetY);
                    offsetCtx.setFillStyle(pattern).drawRect(0, 0, 2 * cWidth - calcOffset.offsetX, 2 * cHeight - calcOffset.offsetY, 'fill');
                });
            } else {
                tempCanvas.render(tempCtx => {
                    const calcOffset = calculateOffset(rectAlignment, offsetX, offsetY, widthPx, heightPx, cWidth, cHeight, { useOffset: true });
                    const pattern = tempCtx.createPattern(tempCanvas, 'repeat');
                    tempCtx.clearRect(0, 0, cWidth, cHeight);
                    tempCtx.translate(calcOffset.offsetX, calcOffset.offsetY);
                    tempCtx.setFillStyle(pattern).drawRect(0, 0, cWidth - calcOffset.offsetX, cHeight - calcOffset.offsetY, 'fill');
                });
            }
        }

        // DEBUG
        // tempCanvas.$el.remove();
        // if (offsetCanvas) { offsetCanvas.$el.remove(); }

        const result = context.createPattern(isFlipMode ? offsetCanvas : tempCanvas, isRepeat);
        tempCanvas.destroy();
        if (offsetCanvas) { offsetCanvas.destroy(); }
        return result;
    });

    promise.fail(() => {
        globalLogger.warn('$badge{Texture} getTextureFill: failed to load shape background image!');
    });

    promise.always(() => {
        pendingContextSet.delete(context);
    });

    return promise;
}
