/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict, pick } from "@/io.ox/office/tk/algorithms";
import { convertLengthToHmm, convertHmmToLength } from "@/io.ox/office/tk/dom";
import type { CanvasLineDashPattern } from "@/io.ox/office/tk/canvas";
import { type OpColor, AutoColorType, Color } from "@/io.ox/office/editframework/utils/color";
import type { ThemeModel } from "@/io.ox/office/editframework/model/themecollection";

const { min, max, floor, round } = Math;

// types ======================================================================

/**
 * The JSON representation of a border, as used in document operations.
 */
export interface OpBorder {
    style: string;
    width?: number;
    color?: OpColor;
}

/**
 * The values of CSS border styles.
 */
export type CSSBorderStyle = "none" | "solid" | "double" | "dashed" | "dotted";

/**
 * The values of CSS attributes describing a border style.
 */
export interface CSSBorderAttrs {

    /**
     * The effective CSS border style.
     */
    style: CSSBorderStyle;

    /**
     * The effective border width in pixels (rounded to integer).
     */
    width: number;

    /**
     * The effective CSS color string.
     */
    color: string;
}

/**
 * Optional parameters for the function `getCssBorderAttributes()`.
 */
export interface CSSBorderAttrOptions {

    /**
     * If specified, a replacement color to be used in case the border object
     * contains the automatic color.
     */
    autoColor?: OpColor;

    /**
     * If set to `true`, border lines with a resulting width less than one
     * pixel (hair lines) will be drawn semi-transparent. Default value is
     * `false`.
     */
    transparentHair?: boolean;

    /**
     * If set to `true`, the border will be rendered for a preview element in
     * the GUI. The border width will be restricted to two pixels in that case.
     * default value is `false`.
     */
    preview?: boolean;
}

/**
 * Optional parameters for the function `getCssBorder()`.
 */
export interface CSSBorderOptions extends CSSBorderAttrOptions {

    /**
     * If set to `true`, the return value for invisible borders will be the
     * empty string instead of the keyword `none`. Default value is `false`.
     */
    clearNone?: boolean;
}

/**
 * Predefined generic line widths.
 */
export enum LineWidthPreset {
    HAIR = "hair",
    THIN = "thin",
    MEDIUM = "medium",
    THICK = "thick"
}

// constants ==================================================================

// maximum line width for borders in GUI preview elements, in pixels
const MAX_PREVIEW_LINEWIDTH = 2;

// minimum line width for borders with style "double", in pixels
const MIN_DOUBLE_LINEWIDTH = 3;

// maps operation line styles to CSS line styles (anything else maps to "solid")
const CSS_LINE_STYLES: Dict<CSSBorderStyle> = {
    none: "none",
    double: "double",
    triple: "double",
    dashed: "dashed",
    dashSmallGap: "dashed",
    dotted: "dotted",
    dotDash: "dotted",
    dotDotDash: "dotted",
    dashDotStroked: "dotted"
};

// maps predefined border width names to line width in 1/100 mm
const BORDER_WIDTH_HMM_MAP = {
    hair: convertLengthToHmm(0.5, "px"),
    thin: convertLengthToHmm(1, "px"),
    medium: convertLengthToHmm(2, "px"),
    thick: convertLengthToHmm(3, "px")
};

// maps border attribute styles to dash patterns
const BORDER_PATTERNS: Dict<number[]> = {
    dashed: [6, 2],
    dotted: [2, 2],
    dashDot: [6, 2, 2, 2],
    dashDotDot: [6, 2, 2, 2, 2, 2]
};

/**
 * Predefined border style for `no border` state.
 */
export const NO_BORDER = Object.freeze<OpBorder>({ style: "none" });

/**
 * Predefined border style for `no border` state.
 *
 * @deprecated
 *  Use constant `NO_BORDER` instead.
 */
export const NONE = NO_BORDER;

/**
 * Line width of hair lines in 1/100 mm.
 */
export const HAIR_WIDTH_HMM = BORDER_WIDTH_HMM_MAP.hair;

/**
 * Line width of thin lines in 1/100 mm.
 */
export const THIN_WIDTH_HMM = BORDER_WIDTH_HMM_MAP.thin;

/**
 * Line width of medium lines in 1/100 mm.
 */
export const MEDIUM_WIDTH_HMM = BORDER_WIDTH_HMM_MAP.medium;

/**
 * Line width of thick lines in 1/100 mm.
 */
export const THICK_WIDTH_HMM = BORDER_WIDTH_HMM_MAP.thick;

// globals ====================================================================

// cache for scaled border patterns, mapped by line width
const borderPatternCache: Dict<Dict<number[]>> = { 1: BORDER_PATTERNS };

// public functions ===========================================================

/**
 * Returns the best CSS border style that can represent the passed line style
 * used in border attributes.
 *
 * @param style
 *  The border line style, as used in formatting attributes of document
 *  operations.
 *
 * @returns
 *  The best matching CSS border style.
 */
export function getCssBorderStyle(style: string): CSSBorderStyle {
    return CSS_LINE_STYLES[style] || "solid";
}

/**
 * Returns the border dash pattern for the passed line style and width,
 * intended to be used in a canvas renderer (see class `Canvas`).
 *
 * @param style
 *  The border line style, as used in formatting attributes of document
 *  operations.
 *
 * @param width
 *  The line width, in pixels.
 *
 * @returns
 *  The dash pattern for the passed line style, or `null` for solid lines.
 */
export function getBorderPattern(style: string, width: number): CanvasLineDashPattern {

    // get pattern map for integral line width
    width = max(1, round(width));
    let patternMap = borderPatternCache[width];

    // build missing pattern map for new line width
    if (!patternMap) {
        borderPatternCache[width] = patternMap = dict.create();
        dict.forEach(BORDER_PATTERNS, (pattern, key) => {
            patternMap[key] = pattern.map(length => floor(length * (width / 2 + 0.5)));
        });
    }

    // return pattern for passed line style
    return patternMap[style] || null;
}

/**
 * Returns the effective CSS attributes for the passed border value.
 *
 * @param border
 *  The JSON border object as used in operations.
 *
 * @param themeModel
 *  The model of the theme used to map scheme color names to color values.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A map with CSS border attributes. Contains the property 'style' with
 *  the effective CSS border style as string; the property 'width' with the
 *  effective border width in pixels (as number, rounded to entire pixels);
 *  and the property 'color' with the effective CSS color (as string with
 *  leading hash sign).
 */
export function getCssBorderAttributes(border: unknown, themeModel: ThemeModel, options?: CSSBorderAttrOptions): CSSBorderAttrs {

    // convert operation line styles to CSS styles
    const style = getCssBorderStyle(pick.string(border, "style", "none"));

    // convert 1/100mm to pixels (round to one digit)
    let width = (style === "none") ? 0 : convertHmmToLength(pick.number(border, "width", 0), "px");

    // minimum width for borders with style 'double', maximum width for preview borders
    const preview = options?.preview;
    if ((style === "double") && (width > 0)) {
        width = preview ? MIN_DOUBLE_LINEWIDTH : max(width, MIN_DOUBLE_LINEWIDTH);
    } else if (preview) {
        width = min(width, MAX_PREVIEW_LINEWIDTH);
    }

    // parse JSON border color
    const color = Color.parseJSON(pick.dict(border, "color"));

    // add color transparency for hair lines
    if ((width > 0) && (width < 1) && options?.transparentHair) {
        color.transform("alphaMod", 50000);
    }

    // round line width to entire pixels
    width = (width > 0) ? max(round(width), 1) : 0;

    // resolve color object to CSS color value
    const jsonAutoColor = options?.autoColor;
    const autoColor = jsonAutoColor ? Color.parseJSON(jsonAutoColor) : AutoColorType.LINE;
    return { style, width, color: color.resolve(autoColor, themeModel).css };
}

/**
 * Converts the passed border attribute object to a CSS border value.
 *
 * @param border
 *  The JSON border object as used in operations.
 *
 * @param themeModel
 *  The model of the theme used to map scheme color names to color values.
 *
 * @param [options]
 *  Optional parameters. Supports all options supported by the method
 *  Border.getCssBorderAttributes(). Additionally, the following options
 *  are supported:
 *
 * @returns
 *  The CSS border value converted from the passed border object.
 */
export function getCssBorder(border: unknown, themeModel: ThemeModel, options?: CSSBorderOptions): string {
    const visible = isVisibleBorder(border);
    const cssAttrs = visible ? getCssBorderAttributes(border, themeModel, options) : null;
    return cssAttrs ? `${cssAttrs.style} ${cssAttrs.width}px ${cssAttrs.color}` : options?.clearNone ? "" : "none";
}

/**
 * Returns whether the passed value is a valid and visible border style (line
 * style different to `none`, and line width greater than `0`).
 *
 * @param border
 *  The border attribute value as used in operations. Any value but a JSON
 *  object with some specific properties will be interpreted as invisible
 *  border.
 *
 * @returns
 *  Whether the passed border attribute value is visible.
 */
export function isVisibleBorder(border: unknown): border is OpBorder {
    return is.dict(border) && is.string(border.style) && (border.style !== "none") && is.number(border.width) && (border.width > 0);
}

/**
 * Returns whether the passed borders are equal.
 *
 * @param border1
 * The first border to compare. Any value but a JSON object will be considered
 * to be equal to an invisible border.
 *
 * @param border2
 * The second border to compare. Any value but a JSON object will be considered
 * to be equal to an invisible border.
 *
 * @returns
 *  Whether the two borders are equal.
 */
export function equalBorders(border1: unknown, border2: unknown): boolean {
    const visible1 = isVisibleBorder(border1);
    const visible2 = isVisibleBorder(border2);
    if (!visible1 || !visible2) { return !visible1 && !visible2; }
    return dict.equals(border1, border2, ["style", "width"])
        && dict.equals(border1, border2, "color", Color.isEqual);
}

/**
 * Returns the best matching line width preset for the passed line width.
 *
 * @param width
 *  The line width, in 1/100 of millimeters.
 *
 * @returns
 *  The best matching line width preset for the passed line width.
 */
export function getPresetForWidth(width: number): LineWidthPreset {
    const pixels = convertHmmToLength(width, "px");
    return (pixels >= 2.5) ? LineWidthPreset.THICK :
        (pixels >= 1.5) ? LineWidthPreset.MEDIUM :
        (pixels >= 0.75) ? LineWidthPreset.THIN :
        LineWidthPreset.HAIR;
}

/**
 * Returns the line width for a predefined border width identifier.
 *
 * @param preset
 *  A line width preset.
 *
 * @returns
 *  The resulting line width, in 1/100 of millimeters.
 */
export function getWidthForPreset(preset: LineWidthPreset): number {
    return BORDER_WIDTH_HMM_MAP[preset] || 0;
}
