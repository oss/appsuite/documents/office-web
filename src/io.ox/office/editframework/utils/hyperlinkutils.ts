/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

/**
 * Returns an RE pattern for a character supported in URLs (including
 * encoded characters with leading percent sign).
 */
function createCharPattern(extraChars: string): string {
    return "([a-z0-9\\-._~!$&'()*+,;=\xa0-\ud7ff\uf900-\ufdcf\ufdf0-\uffef" + extraChars + "]|%[0-9a-f]{2})";
}

// pattern for a decimal integer in the range from 0 to 255
const DEC_BYTE_PATTERN = "(\\d|[1-9]\\d|1\\d{2}|2[0-4]\\d|25[0-5])";

// pattern for a complete IPv4 address
const IP4_ADDRESS_PATTERN = "((" + DEC_BYTE_PATTERN + "\\.){3}" + DEC_BYTE_PATTERN + ")";

// pattern for a hexadecimal 2-byte integer
const HEX_WORD_PATTERN = "[0-9a-f]{0,4}";

// pattern for a complete IPv6 address
const IP6_ADDRESS_PATTERN = "(\\[" + HEX_WORD_PATTERN + "(:" + HEX_WORD_PATTERN + "){0,7}(:" + IP4_ADDRESS_PATTERN + ")?\\])";

// pattern for the leading scheme in a URL, with trailing colon
const SCHEME_PATTERN = "([a-z][a-z0-9+\\-.]*:)";

// pattern for regular characters in a URL
const CHAR_PATTERN = createCharPattern("");

// pattern for the user/password part in a URL, with trailing at-sign
const USER_PASS_PATTERN = "(" + CHAR_PATTERN + "*:" + CHAR_PATTERN + "*@)";

// pattern for the host part in a URL, including optional trailing port number
const HOST_PORT_PATTERN = "(" + IP4_ADDRESS_PATTERN + "|" + IP6_ADDRESS_PATTERN + "|" + CHAR_PATTERN + "*)(:\\d+)?";

// pattern for characters in a directory name of a URL
const DIRNAME_CHAR_PATTERN = createCharPattern(":@");

// pattern for a directory name in a URL (optionally empty), with leading slash character
const SUBDIR_PATTERN = "(/" + DIRNAME_CHAR_PATTERN + "*)";

// pattern for a non-empty directory name, followed by a trailing subpath
const PATH_PATTERN = "(" + DIRNAME_CHAR_PATTERN + "+" + SUBDIR_PATTERN + "*)";

// pattern for the location part in a URL
const LOCATION_PATTERN = "(//" + USER_PASS_PATTERN + "?" + HOST_PORT_PATTERN + SUBDIR_PATTERN + "*|/" + PATH_PATTERN + "?|" + PATH_PATTERN + ")";

// pattern for the query part in a URL
const QUERY_PATTERN = "(\\?" + createCharPattern(":@/?\ue000-\uf8ff") + "*)";

// pattern for the fragment part in a URL
const FRAGMENT_PATTERN = "(#" + createCharPattern(":@/?") + "*)";

// regular expression matching valid URLs
const VALID_URL_RE = new RegExp("^" + SCHEME_PATTERN + LOCATION_PATTERN + "?" + QUERY_PATTERN + "?" + FRAGMENT_PATTERN + "?$", "i");

// regular expression matching all supported protocols
// group 1: the protocol with trailing colon and double-slash
// group 2: the remaining part of the URL
const SUPPORTED_PROTOCOLS_RE = /^(https?:\/\/|ftp:\/\/|mailto:)(.*)$/i;

// regular expression matching valid e-mail addresses (without protocol)
const  VALID_EMAIL_RE = /^[a-z0-9._%+\\-]+@[a-z0-9.-]+\.[a-z]{2,}$/i;

// public methods ---------------------------------------------------------

/**
 * Returns whether the passed string starts with a URL protocol supported
 * by OX editor applications.
 *
 * @param text
 *  The string to be checked.
 *
 * @returns
 *  Whether the passed string starts with a URL protocol supported by OX
 *  editor applications.
 */
export function hasSupportedProtocol(text: string): boolean {
    return SUPPORTED_PROTOCOLS_RE.test(text);
}

/**
 * Returns whether the passed string is basically a valid URL. Note that
 * this is not a fully compliant URL check.
 *
 * @param text
 *  The string to be checked.
 *
 * @returns
 *  Whether the passed string is a valid URL.
 */
export function isValidURL(text: string): boolean {
    return VALID_URL_RE.test(text);
}

/**
 * Detects a valid URL in the passed text.
 *
 * @param text
 *  The text to be checked.
 *
 * @returns
 *  The passed text without trailing white-space, if it contains a valid
 *  hyperlink; otherwise null.
 */
export function checkForHyperlink(text: string): string | null {

    // trim trailing whitespace
    text = text.replace(/\s+$/, "");

    // do magic auto detection if protocol is missing
    if (/^www\../i.test(text)) {
        text = "http://" + text;
    } else if (/^ftp\../i.test(text)) {
        text = "ftp://" + text;
    } else if (VALID_EMAIL_RE.test(text)) {
        text = "mailto:" + text;
    }

    // extract protocol and remainder of the passed text
    const matches = SUPPORTED_PROTOCOLS_RE.exec(text);

    // do not accept unsupported protocols
    if (!is.array(matches)) { return null; }

    // explicit check for e-mail addresses
    if (matches[1] === "mailto:") {
        return VALID_EMAIL_RE.test(matches[2]) ? text : null;
    }

    // check validity of the entire URL
    return isValidURL(text) ? text : null;
}
