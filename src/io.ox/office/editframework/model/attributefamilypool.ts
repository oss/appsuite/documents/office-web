/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { type ValOf, is, to, ary, dict, json } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { DObject } from "@/io.ox/office/tk/objects";

// types ======================================================================

/**
 * The scope of a formatting attribute.
 */
export enum AttributeScope {

    /**
     * The formatting attribute can be applied to stylesheets only.
     */
    STYLE = "style",

    /**
     * The formatting attribute can be applied to objects only (e.g. instances
     * of `AttributedModel`, or DOM elements).
     */
    ELEMENT = "element",

    /**
     * The formatting attribute can be applied at runtime to objects only (e.g.
     * instances of `AttributedModel`, or DOM elements), and it will not be
     * parsed from or serialized to document operations.
     */
    RUNTIME = "runtime"
}

/**
 * A callback function invoked for parsing an attribute value from a document
 * operation.
 */
export type AttributeParseFn<VT> = (value: unknown) => Opt<VT>;

/**
 * A callback function invoked when overwriting an existing formatting
 * attribute with a new value.
 */
export type AttributeMergeFn<VT> = (oldValue: VT, newValue: VT) => VT;

/**
 * Configuratopn options for a single formatting attribute.
 */
export interface AttributeConfig<VT> {

    /**
     * The default value of the formatting attribute.
     *
     * Will be used if neither a stylesheet nor an explicit value exists for
     * the attribute. MUST be a JSON value (including arrays and dictionaries)
     * except the special value `null`.
     */
    def: VT;

    /**
     * The scope of the formatting attribute. If omitted, the attribute can be
     * used everywhere.
     */
    scope?: AttributeScope;

    /**
     * A callback function invoked when parsing attribute values from document
     * operations. If omitted, the value will be parsed according to the type
     * of the default value.
     */
    parse?: AttributeParseFn<VT>;

    /**
     * A callback function invoked when merging different attribute bags where
     * both bags contain the formatting attribute, for example while collecting
     * attributes from stylesheets and explicit attribute sets. If omitted, the
     * new attribute will replace the old attribute.
     */
    merge?: AttributeMergeFn<VT>;

    /**
     * The names of all formatting attributes of the same family that need to
     * be inserted into the undo operation that will be generated when changing
     * the value of this attribute. The value `"*"` indicates "all registered
     * attributes of the family".
     */
    undo?: string | string[];
}

/**
 * A dictionary with configurations for all formatting attributes of a specific
 * attribute bag.
 *
 * @template AttrsT
 *  The type shape of the attribute bag (attribute names to value types).
 *
 * @template ExtT
 *  Extended options expected/supported in the configuration entries.
 */
export type AttributeConfigBag<AttrsT, ExtT = Empty> = {
    [KT in keyof AttrsT]: AttributeConfig<AttrsT[KT]> & ExtT;
};

/**
 * A dictionary with configurations for *additional* formatting attributes of
 * an *existing* attribute bag.
 *
 * @template ExtAttrsT
 *  The type shape of the extended attribute bag (attribute names to value
 *  types).
 *
 * @template BaseAttrsT
 *  The type shape of the base attribute bag `ExtAttrsT` has been extended
 *  from.
 *
 * @template ExtT
 *  Extended options expected/supported in the configuration entries.
 */
export type ExtAttributeConfigBag<ExtAttrsT, BaseAttrsT, ExtT = Empty> = {
    [KT in keyof Omit<ExtAttrsT, KeysOf<BaseAttrsT>>]: AttributeConfig<ExtAttrsT[KT]> & ExtT;
};

/**
 * Options for extending attribute bags with new attribute values.
 */
export interface ExtendAttrsOptions {

    /**
     * If set to `true`, the target attribute bag will be cloned deeply instead
     * of being extended in-place. The method will return the new cloned
     * attribute bag, the original attribute bag passed to the method will not
     * be modified. Default value is `false`.
     */
    clone?: boolean;

    /**
     * If set to `true`, attributes with the value `null` in the second
     * attribute bag will be removed from the target attribute bag. By default,
     * attributes with the value `null` will be inserted into the target
     * attribute bag.
     */
    autoClear?: boolean;
}

/**
 * Generic type of a pool entry for a single attribute. Name and value are
 * typed generically for the entire attribute bag.
 */
export type AttributePoolEntryT<AttrsT extends object, ExtT = Empty> = AttributePoolEntry<KeysOf<AttrsT>, ValOf<AttrsT>, ExtT>;

/**
 * Value type of an iterator that visits the attributes in an attribute bag.
 * Contains the attribute name, its value from the attribute bag, its current
 * default value from the pool, and the attribute pool entry with configuration
 * options of the attribute.
 */
export interface AttrIteratorEntry<AttrsT extends object, ExtT> {
    name: KeysOf<AttrsT>;
    value: ValOf<AttrsT>;
    def: ValOf<AttrsT>;
    entry: AttributePoolEntryT<AttrsT, ExtT>;
}

// private functions ==========================================================

// default parser for number attributes
function parseNumber(value: unknown): Opt<number> {
    return Number.isFinite(value) ? value as number : undefined;
}

// default parser for string attributes
function parseString(value: unknown): Opt<string> {
    return is.string(value) ? value : undefined;
}

// default parser for boolean attributes
function parseBoolean(value: unknown): Opt<boolean> {
    return is.boolean(value) ? value : undefined;
}

// class AttributePoolEntry ===================================================

/**
 * The configuration of a single formatting attribute.
 */
export class AttributePoolEntry<KT, VT, ExtT = Empty> {

    /**
     * The name of the formatting attribute.
     */
    readonly name: KT;

    /**
     * Access to the complete configuration (including custom extensions).
     */
    readonly config: AttributeConfig<VT> & ExtT;

    /**
     * The parser callback function.
     */
    readonly parse: AttributeParseFn<VT>;

    /**
     * The merge callback function.
     */
    readonly merge: Opt<AttributeMergeFn<VT>>;

    /**
     * The names of all attributes to be grouped in undo operations.
     */
    readonly undo: string[];

    /**
     * Specifies whether the attribute can be serialized to operations.
     */
    readonly serializable: boolean;

    // constructor ------------------------------------------------------------

    constructor(name: KT, config: AttributeConfig<VT> & ExtT) {

        // standard settings
        this.name = name;
        this.config = config;
        this.merge = config.merge;
        this.undo = ary.wrap(config.undo);
        this.serializable = this.config.scope !== AttributeScope.RUNTIME;

        // initialize value parser (fallback to default parsers depeding on value type)
        type ParseFn = AttributeParseFn<VT>;
        if (config.parse) {
            this.parse = config.parse;
        } else if (is.number(config.def)) {
            this.parse = parseNumber as unknown as ParseFn;
        } else if (is.string(config.def)) {
            this.parse = parseString as unknown as ParseFn;
        } else if (is.boolean(config.def)) {
            this.parse = parseBoolean as unknown as ParseFn;
        } else if (is.array(config.def)) {
            this.parse = to.array as unknown as ParseFn;
        } else {
            this.parse = to.dict as unknown as ParseFn;
        }
    }
}

// class AttributeFamilyPool ==================================================

/**
 * Each instance contains the configurations of all formatting attributes of a
 * single attribute family.
 *
 * @template AttrsT
 *  The type shape of the attribute bag (attribute names to value types).
 *
 * @template ExtT
 *  Extended configuration options for all attributes.
 */
export class AttributeFamilyPool<AttrsT extends object, ExtT = Empty> extends DObject {

    /**
     * The name of the attribute family, used as top-level key in attribute
     * sets.
     */
    readonly name: string;

    // configuration of all registered formatting attributes
    readonly #map = new Map<string, AttributePoolEntryT<AttrsT, ExtT>>();
    // all attribute default values
    readonly #def = dict.create() as AttrsT;

    // constructor ------------------------------------------------------------

    constructor(name: string) {
        super();
        this.name = name;
    }

    // public methods ---------------------------------------------------------

    registerAttrs(configs: Partial<AttributeConfigBag<AttrsT, ExtT>>): void {
        dict.forEach(configs, (config, name) => {
            if (this.#map.has(name)) {
                globalLogger.warn(`$badge{AttributeFamily} register: multiple registrations of attribute "${this.name}.${name}"`);
            }
            const entry = new AttributePoolEntry(name, config!);
            this.#map.set(name, entry);
            this.#def[name] = config!.def;
        });
    }

    /**
     * Returns whether this pool contains a specific attribute entry.
     *
     * @param name
     *  The name of the attribute to be checked. Can be any string.
     *
     * @returns
     *  Whether this pool contains an entry for the specified attribute.
     */
    hasEntry(name: string): name is KeysOf<AttrsT> {
        return this.#map.has(name);
    }

    /**
     * Returns a specific attribute entry from this pool.
     *
     * @param name
     *  The name of the attribute to be returned. Can be any string.
     *
     * @returns
     *  The configuration entry for the specified attribute; or `undefined`, if
     *  this pool does not contain such an entry.
     */
    getEntry<KT extends KeysOf<AttrsT>>(name: KT): AttributePoolEntry<KT, AttrsT[KT], ExtT>;
    getEntry<KT extends string>(name: KT): KT extends KeysOf<AttrsT> ? Opt<AttributePoolEntry<KT, AttrsT[KT], ExtT>> : undefined;
    // implementation
    getEntry(name: string): Opt<AttributePoolEntryT<AttrsT, ExtT>> {
        return this.#map.get(name);
    }

    /**
     * Creates an iterator for the attribute entries in this pool.
     *
     * @returns
     *  An iterator for the attribute entries in this pool.
     */
    yieldEntries(): IterableIterator<AttributePoolEntryT<AttrsT, ExtT>> {
        return this.#map.values();
    }

    /**
     * Returns the default value of an attribute.
     *
     * @param name
     *  The name of the attribute to be returned.
     *
     * @returns
     *  A deep copy of the attribute's default value.
     */
    getDefault<KT extends KeysOf<AttrsT>>(name: KT): AttrsT[KT] {
        return json.deepClone(this.#def[name]);
    }

    /**
     * Returns the default values of all attributes.
     *
     * @returns
     *  A deep copy of the default values of all registered attributes.
     */
    getDefaults(): AttrsT {
        return json.deepClone(this.#def);
    }

    /**
     * Builds an attribute bag containing `null` for all registered formatting
     * attributes.
     *
     * @returns
     *  A deep copy of the default values of all registered attributes.
     */
    buildNullValues(): PtMapType<AttrsT, null> {
        const values = dict.create() as PtMapType<AttrsT, null>;
        for (const entry of this.yieldEntries()) {
            if (entry.serializable) {
                values[entry.name] = null;
            }
        }
        return values;
    }

    /**
     * Changes the default values for specific formatting attributes.
     *
     * @param values
     *  The attribute default values to be changed.
     *
     * @returns
     *  The new default values that have actually been changed; or `undefined`,
     *  if nothing has changed.
     */
    changeDefaults(values: Partial<AttrsT>): Opt<Partial<AttrsT>> {

        // all changed attributes
        const changed = dict.createPartial<AttrsT>();

        // process all changed default values
        for (const { name, value, def } of this.yieldAttrs(values)) {
            if (!is.nullish(value) && !_.isEqual(value, def)) {
                this.#def[name] = json.deepClone(value);
                changed[name] = json.deepClone(value);
            }
        }

        // return changed default values
        return is.empty(changed) ? undefined : changed;
    }

    /**
     * Creates an iterator for the attributes in an attribute bag. The iterator
     * will provide key, value, and configuration entry for the attribute.
     *
     * @param attrs
     *  The attribute bag to be visited.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @yields
     *  The attributes in the passed attribute bag.
     */
    *yieldAttrs(attrs: Partial<AttrsT>): IterableIterator<AttrIteratorEntry<AttrsT, ExtT>> {
        for (const [name, value] of dict.entries(attrs)) {
            const entry = this.getEntry(name);
            if (entry && (value !== undefined)) {
                yield { name, value: value as ValOf<AttrsT>, def: this.#def[name], entry };
            }
        }
    }

    /**
     * Extends an attribute bag with another attribute bag.
     *
     * If the configuration of a specific attribute contains a merger callback
     * function, and both attribute bags contain a value, that merger function
     * will be called to merge the values from both bags, otherwise the value
     * of the second bag will be copied to the first bag.
     *
     * @param attrs1
     *  (in/out) The target attribute bag to be extended in-place.
     *
     * @param attrs2
     *  An attribute bag to be merged over the first attribute bag. If nullish,
     *  the first attribute bag will not be changed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The extended attribute bag.
     */
    extendAttrs(attrs1: Partial<AttrsT>, attrs2: Nullable<Partial<AttrsT>>, options?: ExtendAttrsOptions): Partial<AttrsT> {

        // clone the first attribute set if specified
        if (options?.clone) {
            attrs1 = json.deepClone(attrs1);
        }

        // nothing to do without attributes to be merged
        if (!attrs2) { return attrs1; }

        // copy attribute values of second attribute set
        for (const { name, value: value2, entry } of this.yieldAttrs(attrs2)) {

            // either set return value from merger, or copy the attribute directly
            if (options?.autoClear && (value2 === null)) {
                delete attrs1[name];
            } else if (entry.merge && !is.nullish(attrs1[name]) && (value2 !== null)) {
                const value1 = attrs1[name] as ValOf<AttrsT>;
                attrs1[name] = entry.merge(value1, value2);
            } else {
                attrs1[name] = value2;
            }
        }

        return attrs1;
    }

    /**
     * Parses the passed generic dictionary and returns a partial attribute bag
     * with the values of all attributes supported by this family.
     *
     * @param source
     *  The source dictionary to be parsed.
     *
     * @returns
     *  The attribute bag parsed from the dictionary.
     */
    parseDict(source: Dict): Partial<AttrsT> {

        const attrs = dict.createPartial<AttrsT>();

        for (const [name, value] of dict.entries(source)) {

            // silantly skip unknown properties in the dictionary, and runtime-scoped attributes
            const entry = this.getEntry(name);
            if (!entry?.serializable) { continue; }

            // try to parse the attribute value
            try {
                const parsed = entry.parse(value);
                if (parsed !== undefined) {
                    attrs[name as KeysOf<AttrsT>] = parsed;
                } else {
                    globalLogger.warn(`$badge{AttributeFamilyPool} parseDict: failed to parse attribute "${this.name}.${name}"`);
                }
            } catch (err) {
                globalLogger.exception(err, `$badge{AttributeFamilyPool} parseDict: exception caught while parsing attribute "${this.name}.${name}"`);
            }
        }

        return attrs;
    }
}
