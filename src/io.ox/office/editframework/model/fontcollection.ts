/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str, ary, json } from "@/io.ox/office/tk/algorithms";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";

import type { OperationContext } from "@/io.ox/office/editframework/model/operation/context";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

type CSSDefaultFont = "sans-serif" | "serif" | "monospace";

/**
 * Descriptor for a preset font to be inserted into the font collection.
 */
interface PresetFont {
    name: string;
    altNames: string[];
    display?: boolean | FileFormatType;
}

/**
 * Descriptor for preset fonts of a specific font family.
 */
interface PresetFontFamily {
    cssDefault: CSSDefaultFont;
    pitch: "variable" | "fixed";
    fonts: PresetFont[];
}

/**
 * An entry for a font in the font collection.
 */
interface FontDescriptor {
    altNames: string[];
    display: boolean | FileFormatType;
    cssDefault?: CSSDefaultFont;
    cssFontFamily?: string;
}

/**
 * Type mapping for the events emitted by `FontCollection` instances.
 */
export interface FontCollectionEventMap {

    /**
     * Will be emitted after a font has been inserted into a font collection.
     *
     * @param fontName
     *  The name of the inserted font.
     */
    "insert:font": [fontName: string];
}

// constants ==================================================================

// all known fonts, mapped by font name
const PRESET_FONTS: readonly PresetFontFamily[] = [{
    cssDefault: "sans-serif",
    pitch: "variable",
    fonts: [
        { name: "Helvetica",            altNames: ["Arial"],                    display: true },
        { name: "Arial",                altNames: ["Helvetica"],                display: true },
        { name: "Arial Black",          altNames: ["sans serif", "Arial"] },
        { name: "Arial Narrow",         altNames: ["Calibri Light"] },
        { name: "Verdana",              altNames: ["Arial"],                    display: true },
        { name: "Tahoma",               altNames: ["Arial"],                    display: true },
        { name: "Calibri",              altNames: ["Carlito", "Arial"],         display: true },
        { name: "Calibri Light",        altNames: ["Carlito", "Arial"],         display: true },
        { name: "Carlito",              altNames: ["Calibri", "Arial"] },
        { name: "Albany",               altNames: ["Liberation Sans", "Arial"] }, // old OOo default for sans-serif
        { name: "Liberation Sans",      altNames: ["Albany", "Arial"],          display: FileFormatType.ODF }, // LO default for sans-serif
        { name: "Impact",               altNames: ["Sans Serif", "Arial"],      display: true }, // bug 49056: in Linux, "sans serif" is not the same as "sans-serif"
        { name: "Helvetica Neue",       altNames: ["Helvetica"] },
        { name: "Helvetica Light",      altNames: ["Helvetica"] },
        { name: "Open Sans",            altNames: ["Helvetica Neue"] },
        { name: "Open Sans Light",      altNames: ["Open Sans"] },
        { name: "Open Sans Semibold",   altNames: ["Open Sans"] },
        { name: "Open Sans Extrabold",  altNames: ["Open Sans Semibold"] },
        { name: "Euphemia",             altNames: ["Calibri", "Arial"] },
        { name: "Roboto",               altNames: ["Calibri", "Arial"] }
    ]
}, {
    cssDefault: "serif",
    pitch: "variable",
    fonts: [
        { name: "Times",                altNames: ["Times New Roman"] },
        { name: "Times New Roman",      altNames: ["Times"],                        display: true },
        { name: "Georgia",              altNames: ["Times New Roman"],              display: true },
        { name: "Palatino",             altNames: ["Times New Roman"],              display: true },
        { name: "Book Antiqua",         altNames: ["Palatino"],                     display: true },
        { name: "Cambria",              altNames: ["Caladea", "Times New Roman"],   display: true },
        { name: "Caladea",              altNames: ["Cambria", "Times New Roman"] },
        { name: "Thorndale",            altNames: ["Liberation Serif", "Times New Roman"] }, // old OOo default for serif
        { name: "Liberation Serif",     altNames: ["Thorndale", "Times New Roman"], display: FileFormatType.ODF }, // LO default for serif
        { name: "Calisto MT",           altNames: ["Book Antiqua"] }
    ]
}, {
    cssDefault: "monospace",
    pitch: "fixed",
    fonts: [
        { name: "Courier",              altNames: ["Courier New"] },
        { name: "Courier New",          altNames: ["Courier"],                      display: true },
        { name: "Andale Mono",          altNames: ["Courier New"],                  display: true },
        { name: "Consolas",             altNames: ["Courier New"],                  display: true },
        { name: "Cumberland",           altNames: ["Liberation Mono", "Courier New"] }, // old OOo default for monospace
        { name: "Liberation Mono",      altNames: ["Cumberland", "Courier New"],    display: FileFormatType.ODF } // LO default for monospace
    ]
}];

// a regular expression that matches valid font names
const RE_VALID_FONTNAME = /^[^<>[\]()"'&/\\]+$/;

// class FontCollection =======================================================

/**
 * Contains the definitions of all known fonts in a document.
 */
export default class FontCollection extends ModelObject<EditModel, FontCollectionEventMap> {

    readonly #fontMap = new Map<string, FontDescriptor>();
    readonly #presetMap = new Map<string, PresetFont & { cssDefault: CSSDefaultFont }>();
    readonly #fileFormat: FileFormatType;

    // constructor ------------------------------------------------------------

    /**
     * @param docModel
     *  The document model containing this instance.
     */
    constructor(docModel: EditModel) {

        // base constructor
        super(docModel);

        // special handling for different file formats
        this.#fileFormat = this.docApp.getFileFormat();

        // insert all predefined fonts into the font map
        PRESET_FONTS.forEach(presetFamily => {
            presetFamily.fonts.forEach(presetFont => {
                this.#fontMap.set(presetFont.name, {
                    altNames: presetFont.altNames,
                    display: presetFont.display ?? false,
                    cssDefault: presetFamily.cssDefault
                });
                this.#presetMap.set(presetFont.name, {
                    ...presetFont,
                    cssDefault: presetFamily.cssDefault
                });
            });
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the names of all preset fonts and registered fonts. Preset fonts
     * with the hidden flag that have not been registered will not be included.
     *
     * @returns
     *  The names of all preset fonts and registered fonts.
     */
    getFontNames(): string[] {
        return ary.from(this.#fontMap, ([fontName, fontDesc]) => {
            const matches = (fontDesc.display === true) || (fontDesc.display === this.#fileFormat);
            return matches ? fontName : undefined;
        });
    }

    /**
     * Returns the value of the CSS attribute "font-family" containing the
     * specified font name and all alternative font names.
     *
     * @param fontName
     *  The name of the font (case-insensitive).
     *
     * @returns
     *  The value of the CSS attribute "font-family" containing the specified
     *  font name and all alternative font names.
     */
    getCssFontFamily(fontName: string): string {

        if (!fontName) { return ""; } // increasing resilience

        // fonts are keyed by capitalized names
        fontName = str.capitalizeWords(fontName);

        // default for unregistered fonts: just the passed font name
        const fontDesc = this.#fontMap.get(fontName);
        if (!fontDesc) { return fontName; }

        // calculate and cache the CSS font family
        if (!is.string(fontDesc.cssFontFamily)) {
            fontDesc.cssFontFamily = this.#calculateCssFontFamily(fontName);
        }

        return fontDesc.cssFontFamily;
    }

    // operation handlers -----------------------------------------------------

    /**
     * Callback handler for the document operation "insertFont". Inserts a
     * new font entry into this collection.
     *
     * @param context
     *  A wrapper representing the "insertFont" document operation.
     *
     * @throws
     *  An `OperationError`, if applying the font operation fails, e.g. if a
     *  required property is missing in the operation.
     */
    applyInsertFontOperation(context: OperationContext): void {

        // fonts are keyed by capitalized names
        const fontName = str.capitalizeWords(context.getStr("fontName"));
        // extract font attributes
        const fontAttrs = json.deepClone(context.getDict("attrs"));

        // validate alternative names
        let altNames = is.array(fontAttrs.altNames) ? fontAttrs.altNames.filter(Boolean).filter(is.string).map(str.capitalizeWords) : [];
        // add predefined alternative names
        const presetFont = this.#presetMap.get(fontName);
        if (presetFont) { altNames.push(...presetFont.altNames); }
        // unify alternative names, remove own font name
        altNames = ary.unify(altNames).filter(name => name !== fontName);

        // create the font descriptor
        this.#fontMap.set(fontName, {
            altNames,
            display: true,
            cssDefault: presetFont?.cssDefault
        });

        // remove calculated CSS font families of all fonts (will be recalculated on first access)
        this.#fontMap.forEach(fontDesc2 => delete fontDesc2.cssFontFamily);

        // notify listeners
        this.trigger("insert:font", fontName);
    }

    // private methods --------------------------------------------------------

    /**
     * Calculates and returns the CSS font family attribute containing the
     * specified font name and all alternative font names.
     */
    #calculateCssFontFamily(fontName: string): string {

        // the resulting collection of CSS font names
        let cssFontNames = [fontName];
        // the last found CSS default font family
        let lastCssDefault: Opt<string>;

        // collects all alternative font names of the fonts in the passed array
        const collectAltNames = (...fontNames: string[]): void => {

            // alternative font names of the passed fonts
            let altFontNames: string[] = [];

            // collect all alternative font names
            fontNames.forEach(fontName2 => {
                const fontDesc = this.#fontMap.get(fontName2);
                if (fontDesc && is.array(fontDesc.altNames)) {
                    altFontNames.push(...fontDesc.altNames);
                    if (fontDesc.cssDefault) {
                        lastCssDefault = fontDesc.cssDefault;
                    }
                }
            });

            // unify the new alternative font names, remove font names already
            // collected in `cssFontNames` (prevent cyclic references)
            const cssFontNameSet = new Set(cssFontNames);
            altFontNames = ary.unify(altFontNames).filter(name => !cssFontNameSet.has(name));

            // insert the new alternative font names into result list, and
            // call recursively until no new font names have been found
            if (altFontNames.length > 0) {
                cssFontNames.push(...altFontNames);
                collectAltNames(...altFontNames);
            }
        };

        // collect alternative font names by starting with passed font name
        collectAltNames(fontName);

        // finally, add the CSS default family
        if (lastCssDefault) {
            cssFontNames.push(lastCssDefault);
        }

        // remove invalid/dangerous characters
        cssFontNames = cssFontNames.filter(name => RE_VALID_FONTNAME.test(name));
        // enclose all fonts with white-space into quote characters
        cssFontNames = cssFontNames.map(name => /\s/.test(name) ? `'${name.replace(/\s+/g, " ")}'` : name);
        // return the CSS font-family attribute value
        return cssFontNames.join(",");
    }
}
