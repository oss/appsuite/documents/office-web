/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/objects";

// types ======================================================================

/**
 * Type mapping for the events emitted by `IUndoManager` implementers.
 */
export interface IUndoManagerEventMap {

    /**
     * Will be emitted after the number of available undo/redo actions has
     * changed, either by adding a new undo action, or by cleaning the entire
     * stack.
     */
    "change:stack": [];
}

/**
 * A generic shape of an undo manager. All explicit undo managers that can
 * be registered at the document controller need to implement this
 * interface.
 *
 * Implementations **MUST** trigger the event "change:stack" (without data)
 * whenever the number of undo or redo actions in this undo manager has
 * changed.
 */
export interface IUndoManager<EvtMapT extends IUndoManagerEventMap> extends EObject<EvtMapT> {
    getUndoCount(): number;
    undo(): MaybeAsync;
    getRedoCount(): number;
    redo(): MaybeAsync;
}
