/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { Operation } from "@/io.ox/office/editframework/utils/operations";
import { OperationContext } from "@/io.ox/office/editframework/model/operation/context";
import { IUndoManagerEventMap, IUndoManager } from "@/io.ox/office/editframework/model/iundomanager";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

/**
 * A callback function representing a dynamic operation-less undo action.
 */
export type UndoCallbackFn = () => MaybeAsync<boolean>;

/**
 * Valid sources of JSON document operations for an undo action.
 */
export type UndoOperationSource = Operation | Operation[] | OperationContext | UndoCallbackFn | null;

/**
 * An instance of this class is used by the undo manager to store undo and redo
 * operations and to apply them in the document model.
 */
export class UndoAction {

    // the collector for the undo operations
    readonly undoOperations: readonly Operation[];

    // the collector for the redo operations
    readonly redoOperations: readonly Operation[];
}

/**
 * Additional configuration for an undo/redo action.
 */
export interface UndoActionConfig {

    /**
     * If set to true, the event handlers for "undo:after" and "redo:after"
     * MUST NOT set/modify the browser selection. Default value is `false`.
     */
    preventSelectionChange?: boolean;

    /**
     * The initial selection state that will be stored in the undo action.
     * If specified, the selection state will be restored after applying
     * the undo operations of this undo action. In grouped undo actions,
     * the selection state of the first embedded undo action will be used
     * to restore the selection state.
     *
     * See also:
     * - `EditModel.getSelectionState()`
     * - `EditModel.setSelectionState()`
     */
    undoSelectionState?: object;

    /**
     * The final selection state that will be stored in the undo action. If
     * specified, the selection state will be restored after applying the
     * redo operations of this undo action. In grouped undo actions, the
     * selection state of the last embedded undo action will be used to
     * restore the selection state.
     *
     * See also:
     * - `EditModel.getSelectionState()`
     * - `EditModel.setSelectionState()`
     */
    redoSelectionState?: object;

    /**
     * Additional application-specific user data to be stored for the undo
     * operations. Will be passed to event listeners and the OT manager.
     */
    undoUserData?: object;

    /**
     * Additional application-specific user data to be stored for the redo
     * operations. Will be passed to event listeners and the OT manager.
     */
    redoUserData?: object;
}

/**
 * Additional configuration passed to an undo/redo event.
 */
export interface UndoEventConfig {

    /**
     * If set to true, the event handler MUST NOT set/modify the browser
     * selection. Default value is `false`.
     */
    preventSelectionChange: boolean;

    /**
     * The selection state stored in the undo/redo action.
     */
    selectionState?: object;

    /**
     * Additional application-specific user data.
     */
    userData?: object;
}

export interface UndoManagerConfig {

    /**
     * A callback function that will be called while adding a new undo action
     * onto the undo stack, and that can try to merge the operations of the new
     * undo action into the operations of the last undo action already stored
     * in the undo stack.
     *
     * @param prevAction
     *  The last undo action stored in the undo stack.
     *
     * @param nextAction
     *  The new action about to be pushed onto the undo stack.
     *
     * @returns
     *  The value `true`, if the new undo action has been merged into the
     *  existing undo action, in that case the new undo action will be
     *  discarded. All other return values result in pushing the new undo
     *  action onto the undo stack.
     */
    mergeUndoActionHandler?: (prevAction: UndoAction, nextAction: UndoAction) => boolean | void;

    /**
     * Whether this undo manager saves and restores the document selection in
     * the generated undo actions by itself. Default value is `false`.
     */
    updateSelectionInUndo?: boolean;

    /**
     * If set to `true`, redo actions will be dropped if it would be needed to
     * transform them against external operations. Default value is `false`.
     */
    disableRedoWithOT?: boolean;
}

/**
 * Type mapping for the events emitted by `UndoManager` instances.
 */
export interface UndoManagerEventMap extends IUndoManagerEventMap {

    /**
     * Will be emitted before undo operations will be applied at the document
     * model by the method `UndoManager::undo`.
     *
     * @param operations
     *  The array of JSON document operations that will be applied for the undo
     *  action.
     *
     * @param config
     *  Additional configuration stored in the undo/redo action group, as
     *  passed to the method `UndoManager::enterUndoGroup`.
     */
    "undo:before": [operations: readonly Operation[], config: UndoEventConfig];

    /**
     * Will be emitted after undo operations have been applied at the document
     * model by the method `UndoManager::undo`.
     *
     * @param operations
     *  The array of JSON document operations that have been applied for the
     *  undo action.
     *
     * @param config
     *  Additional configuration stored in the undo/redo action group, as
     *  passed to the method `UndoManager::enterUndoGroup`.
     */
    "undo:after": [operations: readonly Operation[], config: UndoEventConfig];

    /**
     * Will be emitted before redo operations will be applied at the document
     * model by the method `UndoManager::redo`.
     *
     * @param operations
     *  The array of JSON document operations that will be applied for the redo
     *  action.
     *
     * @param config
     *  Additional configuration stored in the undo/redo action group, as
     *  passed to the method `UndoManager::enterUndoGroup`.
     */
    "redo:before": [operations: readonly Operation[], config: UndoEventConfig];

    /**
     * Will be emitted after redo operations have been applied at the document
     * model by the method `UndoManager::redo`.
     *
     * @param operations
     *  The array of JSON document operations that have been applied for the
     *  redo action.
     *
     * @param config
     *  Additional configuration stored in the undo/redo action group, as
     *  passed to the method `UndoManager::enterUndoGroup`.
     */
    "redo:after": [operations: readonly Operation[], config: UndoEventConfig];

    /**
     * Will be emitted directly after a new undo group has been opened by the
     * method `UndoManager::enterUndoGroup`.
     */
    "undogroup:open": [];

    /**
     * Will be emitted after an undo group has been closed by the method
     * `UndoManager::enterUndoGroup`.
     */
    "undogroup:close": [];

    /**
     * Will be emitted to request transforming a selection state in an undo
     * action. If there are external operations, it might be necessary to
     * update the selection state that is saved at the undo action.
     *
     * @param selectionState
     *  The selection state saved at the undo action.
     *
     * @param transformActions
     *  The external actions that might modify the selection state.
     */
    "transform:selectionstate": [selectionState: object, transformActions: Operation[]];
}

// class UndoManager ==========================================================

// declaration merging to add all properties of base interfaces automatically
export interface UndoManager extends IUndoManager<UndoManagerEventMap> { }

export class UndoManager extends ModelObject<EditModel, UndoManagerEventMap> implements IUndoManager<UndoManagerEventMap> {

    constructor(docModel: EditModel, config?: UndoManagerConfig);

    isUndoEnabled(): boolean;
    clearUndoActions(): void;

    enterUndoGroup<T>(callback: () => MaybeAsync<T>, context?: object, actionConfig?: UndoActionConfig): JPromise<T>;
    addUndo(undoOperations: UndoOperationSource, redoOperations: UndoOperationSource, actionConfig?: UndoActionConfig): void;
}
