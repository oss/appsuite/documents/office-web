/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, map, dict, json } from "@/io.ox/office/tk/algorithms";
import { EObject } from "@/io.ox/office/tk/objects";

import type { AttrFamiliesOf, AttrBagsOf, PtAttrBagsOf, AttrSetConstraint, PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import type { OperationContext } from "@/io.ox/office/editframework/model/operation/context";
import type { AttributeConfigBag, ExtendAttrsOptions } from "@/io.ox/office/editframework/model/attributefamilypool";
import { AttributeFamilyPool } from "@/io.ox/office/editframework/model/attributefamilypool";

// types ======================================================================

export type { ExtendAttrsOptions };

/**
 * Type configuration of an attribute family in a pool map, as tuple type with
 * one or two elements.
 *
 * A pool map is the foundation of the type configuration of an attribute set
 * pool (class `AttributeSetPool`). It is an interface that maps attribute
 * family names to these tuples.
 *
 * The first tuple element contains the type shape of the attribute bag
 * (interface as key/type map of all attributes).
 *
 * The optional second tuple element contains additional configuration options
 * supported by all attributes of the family.
 *
 * @example
 *  interface CellAttributes { ... } // names/types of cell attributes
 *  interface RowAttributes { ... }  // names/types of row attributes
 *  interface ColAttributes { ... }  // names/types of column attributes
 *  interface ExtColConfig { ... }   // additional config for column attributes
 *
 *  // the pool map expected by class `AttributeSetPool` (maps attribute family
 *  // names to `PoolMapEntry` tuples)
 *  interface MyPoolMap {
 *    cell: [CellAttributes];
 *    row:  [RowAttributes];
 *    col:  [ColAttributes, ExtColConfig];
 *  }
 */
export type PoolMapEntry<AttrsT extends object, ExtendedOptionsT = Empty> = [AttrsT, ExtendedOptionsT?];

/**
 * Helper type that restricts the type of the properties of attribute pool maps
 * to `PoolMapEntry` elements without interfering with the type of the keys
 * (attribute family names). Intended to be used in generic type constraints
 * ("extends" clauses).
 *
 * @example
 *  class MyClass<PoolMapT extends PoolMapConstraint<PoolMapT>> {
 *    ...
 *  }
 */
export type PoolMapConstraint<PoolMapT> = MapType<PoolMapT, PoolMapEntry<object>>;

/**
 * Extracts the type of the attribute bag of a specific attribute family from a
 * pool map.
 */
export type AttributeBagT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT>> = PoolMapT[FT][0];

/**
 * Value type of an iterator that visits attribute bags in an attribute set.
 * Contains the attribute family name, the actual attribute bag (plain object
 * with key/value pairs for all attributes), and the family pool instance with
 * configuration of all registered attributes.
 */
export interface AttrBagIteratorEntry<AttrSetT extends AttrSetConstraint<AttrSetT>> {
    family: AttrFamiliesOf<AttrSetT>;
    attrs: PtAttrBagsOf<AttrSetT>;
    pool: AttributeFamilyPool<AttrBagsOf<AttrSetT>>;
}

/**
 * Type mapping for the events emitted by `AttributePool` instances.
 */
export interface AttributePoolEventMap<PoolMapT extends PoolMapConstraint<PoolMapT>> {

    /**
     * Will be emitted after the default values of some formatting attributes
     * have been changed.
     *
     * @param changedSet
     *  The partial attribute map containing the changed default values.
     */
    "change:defaults": [changedSet: PtAttrBagSetT<PoolMapT>];
}

// private types --------------------------------------------------------------

// extracts the type of the extended configuration options of a specific attribute family from a pool map
type ExtT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT>> = PoolMapT[FT][1] extends Empty ? PoolMapT[FT][1] : Empty;

// the type of the configuration bag of a specific attribute family (interface `AttributeConfigBag`)
type ConfigBagT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT>> = AttributeConfigBag<AttributeBagT<PoolMapT, FT>, ExtT<PoolMapT, FT>>;

// the type of the attribute family pool of a specific attribute family (class `AttributeFamilyPool`)
type FamilyPoolT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT>> = AttributeFamilyPool<AttributeBagT<PoolMapT, FT>, ExtT<PoolMapT, FT>>;

// the type of an object with attribute bags of specific attribute families from a pool map
type AttrBagSetT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT> = KeysOf<PoolMapT>> = { [KT in FT]: AttributeBagT<PoolMapT, FT> };

// the type of an object with partial attribute bags of specific attribute families from a pool map
export type PtAttrBagSetT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT> = KeysOf<PoolMapT>> = { [KT in FT]?: Partial<AttributeBagT<PoolMapT, KT>> };

// the type of an attribute bag of a specific attribute family with `null` values
type NullBagT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT>> = PtRecord<KeysOf<AttributeBagT<PoolMapT, FT>>, null>;

// the type of an object with attribute bags of specific attribute families with `null` values
type NullBagSetT<PoolMapT extends PoolMapConstraint<PoolMapT>, FT extends KeysOf<PoolMapT>> = { [KT in FT]: NullBagT<PoolMapT, FT> };

// class AttributePool ========================================================

/**
 * A pool with configurations of all formatting attributes supported by an
 * application, grouped by attribute families.
 */
export class AttributePool<PoolMapT extends PoolMapConstraint<PoolMapT>> extends EObject<AttributePoolEventMap<PoolMapT>> {

    // all registered attribute family pools
    readonly #familyPools = this.member(new Map<string, AttributeFamilyPool<object>>());

    // public methods ---------------------------------------------------------

    /**
     * Registers new formatting attributes for a specific attribute family, or
     * updates the settings of formatting attributes that have been registered
     * already.
     *
     * @param family
     *  The name of the attribute family the new formatting attributes are
     *  associated with. MUST NOT be the string "styleId" which is reserved to
     *  be used as stylesheet identifier in attribute sets.
     *
     * @param configs
     *  The configuration of all formatting attributes to be registered.
     */
    registerAttrFamily<FT extends KeysOf<PoolMapT>>(family: FT, configs: Partial<ConfigBagT<PoolMapT, FT>>): void {

        // reserved top-level property names in attribute sets
        if (family === "styleId") { throw new TypeError("AttributePool: invalid attribute family 'styleId'"); }

        // resolve existing attribute family pool, or create a new on demand
        const familyPool = map.upsert(this.#familyPools, family, () => new AttributeFamilyPool(family));

        // process all passed attribute configurations
        familyPool.registerAttrs(configs);
    }

    /**
     * Returns the registry pool for a specific attribute family.
     *
     * @param family
     *  The name of an attribute family.
     *
     * @returns
     *  The registry pool for the specified attribute family.
     */
    getFamilyPool<FT extends KeysOf<PoolMapT>>(family: FT): FamilyPoolT<PoolMapT, FT> {
        return this.#familyPools.get(family)! as unknown as FamilyPoolT<PoolMapT, FT>;
    }

    /**
     * Returns whether this pool contains a specific attribute entry.
     *
     * @param family
     *  The name of the attribute family to be checked. Can be any string.
     *
     * @param name
     *  The name of the attribute to be checked. Can be any string.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  Whether this pool contains an entry for the specified attribute.
     */
    hasAttrEntry(family: string, name: string): boolean {
        return !!this.#familyPools.get(family)?.hasEntry(name);
    }

    /**
     * Returns the default value of a single attribute.
     *
     * @param family
     *  The name of an attribute family.
     *
     * @returns
     *  A deep copy of the default values of the specified attribute family.
     */
    getDefaultValue<FT extends KeysOf<PoolMapT>, KT extends KeysOf<AttributeBagT<PoolMapT, FT>>>(family: FT, name: KT): AttributeBagT<PoolMapT, FT>[KT] {
        // compatibility with old code: accept invalid family names, return `undefined`
        return this.getFamilyPool(family)?.getDefault(name);
    }

    /**
     * Returns the default values of an attribute family.
     *
     * @param family
     *  The name of an attribute family.
     *
     * @returns
     *  A deep copy of the default values of the specified attribute family.
     */
    getDefaultValues<FT extends KeysOf<PoolMapT>>(family: FT): AttributeBagT<PoolMapT, FT> {
        // compatibility with old code: accept invalid family names, return `undefined`
        return this.getFamilyPool(family)?.getDefaults();
    }

    /**
     * Returns an attribute set with the default values of all specified
     * attribute families.
     *
     * @param families
     *  An iterable source of family names (e.g. an array or set).
     *
     * @returns
     *  The attribute set containing the default values of all specified
     *  attribute families.
     */
    getDefaultValueSet<FT extends KeysOf<PoolMapT>>(families: Iterable<FT>): AttrBagSetT<PoolMapT, FT> {

        // the resulting attribute set
        const valueSet = dict.create() as AttrBagSetT<PoolMapT, FT>;

        // process the input parameter according to its type
        for (const family of families) {
            valueSet[family] = this.getDefaultValues(family);
        }

        return valueSet;
    }

    /**
     * Builds an attribute bag containing all formatting attributes of the
     * specified attribute family, set to the value `null`.
     *
     * @param family
     *  The name of an attribute family.
     *
     * @returns
     *  The attribute bag with `null` values for all attributes of the
     *  specified attribute family.
     */
    buildNullValues<FT extends KeysOf<PoolMapT>>(family: FT): PtRecord<KeysOf<AttributeBagT<PoolMapT, FT>>, null> {
        // compatibility with old code: accept invalid family names, return `undefined`
        return this.getFamilyPool(family)?.buildNullValues();
    }

    /**
     * Builds an attribute set containing all formatting attributes of the
     * specified attribute families, set to the value `null`.
     *
     * @param families
     *  An iterable source of strings (e.g. an array or set).
     *
     * @returns
     *  The attribute set with `null` values for all attributes of the
     *  specified attribute families.
     */
    buildNullValueSet<FT extends KeysOf<PoolMapT>>(families: Iterable<FT>): NullBagSetT<PoolMapT, FT> {

        // the resulting attribute set
        const valueSet = dict.create() as NullBagSetT<PoolMapT, FT>;

        // process the input parameter according to its type
        for (const family of families) {
            valueSet[family] = this.buildNullValues(family);
        }

        return valueSet;
    }

    /**
     * Creates an iterator for the attribute bags in an attribute set. The
     * iterator will provide family name, attribute bag, and the respective
     * family configuration pool.
     *
     * @param attrSet
     *  The attribute set to be visited.
     *
     * @yields
     *  The attribute bags in the passed attribute set.
     */
    *yieldAttrBags<AttrSetT extends AttrSetConstraint<AttrSetT>>(attrSet: PtAttrSet<AttrSetT>): IterableIterator<AttrBagIteratorEntry<AttrSetT>> {
        for (const [family, attrs] of dict.entries(attrSet)) {
            const pool = this.#familyPools.get(family);
            if (pool) {
                // family pool exists: attribute bag is supported by this pool (force all variables to expected types)
                yield {
                    family: family as unknown as AttrFamiliesOf<AttrSetT>,
                    attrs: attrs as unknown as AttrBagsOf<AttrSetT>,
                    pool: pool as unknown as AttributeFamilyPool<AttrBagsOf<AttrSetT>>
                };
            }
        }
    }

    /**
     * Extends an attribute set with another attribute set.
     *
     * If the configuration of a specific attribute contains a merger callback
     * function, and both attribute sets contain a value, that merger function
     * will be called to merge the values from both sets, otherwise the value
     * of the second set will be copied to the first set.
     *
     * @param attrSet1
     *  (in/out) The target attribute set to be extended in-place.
     *
     * @param attrSet2
     *  An attribute set to be merged over the first attribute set. If nullish,
     *  the first attribute set will not be changed.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The extended attribute set.
     */
    extendAttrSet<AttrSetT extends AttrSetConstraint<AttrSetT>>(attrSet1: PtAttrSet<AttrSetT>, attrSet2: Nullable<PtAttrSet<AttrSetT>>, options?: ExtendAttrsOptions): PtAttrSet<AttrSetT> {

        // flat-clone the first attribute set if specified (attribute bags will be cloned in family pools)
        if (options?.clone) {
            attrSet1 = json.flatClone(attrSet1);
        }

        // nothing to do without attributes to be merged
        if (!attrSet2) { return attrSet1; }

        // copy style identifier directly (may be string or `null`)
        if ((attrSet2.styleId === null) && options?.autoClear) {
            delete attrSet1.styleId;
        } else if (is.string(attrSet2.styleId)) {
            attrSet1.styleId = attrSet2.styleId;
        }

        // merge all attribute bags
        for (const { family, attrs: attrs2, pool } of this.yieldAttrBags(attrSet2)) {

            // extends first attribute bag with second bag
            let attrs1 = attrSet1[family] ?? dict.createPartial<AttrBagsOf<AttrSetT>>();
            attrs1 = pool.extendAttrs(attrs1, attrs2, options);

            // delete empty bags from the attribute set
            if (is.empty(attrs1)) {
                delete attrSet1[family];
            } else {
                attrSet1[family] = attrs1;
            }
        }

        return attrSet1;
    }

    // operation handlers -----------------------------------------------------

    /**
     * Callback handler for the document operation "changeConfig". Changes the
     * default values of all supported formatting attributes.
     *
     * @param context
     *  A wrapper representing the "changeConfig" document operation.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     *  property is missing in the operation.
     */
    applyChangeConfigOperation(context: OperationContext): void {

        // the attributes to be changed
        const valueSet = context.getDict("attrs");
        // all changed attributes
        const changedSet = dict.create() as PtAttrBagSetT<PoolMapT>;

        // update defaults of all attribute families
        for (const [family, value] of dict.entries(valueSet)) {
            const pool = this.#familyPools.get(family);
            if (pool && is.dict(value)) {
                const changed = pool.changeDefaults(pool.parseDict(value));
                if (changed) { changedSet[family as KeysOf<PoolMapT>] = changed; }
            }
        }

        // notify all change listeners
        if (!is.empty(changedSet)) {
            this.trigger("change:defaults", changedSet);
        }
    }
}
