/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, re, dict } from "@/io.ox/office/tk/algorithms";

import type { Position, Operation, OptAttrsOperation } from "@/io.ox/office/editframework/utils/operations";
import { clonePosition, equalPositions } from "@/io.ox/office/editframework/utils/operations";
import * as Op from "@/io.ox/office/editframework/utils/operations";
import type { OperationAction } from "@/io.ox/office/editframework/utils/operationutils";

import { setOperationRemoved, reduceProperties, reduceOperationAttributes, transformIndexInsert, transformIndexDelete } from "@/io.ox/office/editframework/utils/otutils";
import { TransformPrepareResult, OTEngine } from "@/io.ox/office/editframework/model/operation/otengine";

// re-exports =================================================================

export * from "@/io.ox/office/editframework/model/operation/otengine";

// types ======================================================================

/**
 * Configuration settings for the constructor of the class `BaseOTManager`.
 */
export interface BaseOTManagerConfig {

    /**
     * Default value for the `type` property of auto-style operations.
     */
    defaultAutoStyleType?: string;

    /**
     * Specifies which types of auto-styles are using an indexed naming scheme
     * (style identifiers are restricted to consecutive integral indexes with a
     * fixed prefix), and which prefix the style identifiers shall have for the
     * naming scheme.
     *
     * **Example:** `{cell:"a"}` specifies that auto-styles of type "cell" are
     * using indexed names with prefix "a", resulting in the style identifiers
     * "a0", "a1", "a2", and so on.
     */
    indexedAutoStylePrefixes?: Dict<string>;
}

/**
 * Generic base interface for operations with a target identifier.
 */
export interface AnyTargetIdOperation extends Operation {
    id?: string;
}

/**
 * Generic interface for operations that modify a component of the chart (axes,
 * gridlines, titles, or legend).
 */
export interface ChartCompOperation extends Op.DrawingOperation, OptAttrsOperation {
    axis?: number;
}

// constants ==================================================================

/**
 * A synthetic alias name for all operations that want to delete a specific
 * target object that will be addressed with the string property `target` by
 * other operations.
 */
export const OT_DELETE_TARGET_ALIAS = "$deleteTarget";

/**
 * A synthetic alias name for all operations that want to ignore a specified
 * target in OT processing.
 */
export const OT_IGNORE_TARGET_ALIAS = "$ignoreTarget";

/**
 * Synthetic alias name for auto-style operations.
 */
export const OT_AUTOSTYLE_ALIAS = "$autoStyle";

/**
 * Synthetic alias name for operations for chart data seris.
 */
export const OT_CHART_SERIES_ALIAS = "$chartSeries";

/**
 * Synthetic alias name for operations modifying a component of the chart
 * except data series (axes, gridlines, titles, legend).
 */
export const OT_CHART_COMP_ALIAS = "$chartComp";

/**
 * Synthetic alias name for all chart operations.
 */
export const OT_CHART_ALIAS = "$chartAll";

// private constants ----------------------------------------------------------

/**
 * The names of all operations that will be ignored in transformations.
 */
const IGNORE_OPS = [
    Op.NOOP,
    Op.CHANGE_CONFIG,
    Op.INSERT_FONT,
    Op.INSERT_THEME,
    Op.INSERT_STYLESHEET,
    Op.DELETE_STYLESHEET,
    Op.CHANGE_STYLESHEET
];

// private functions ==========================================================

/**
 * Returns whether both style sheet operations refer to the same style sheet.
 */
function isSameStyleSheet(styleOp1: Op.StyleSheetOperation, styleOp2: Op.StyleSheetOperation): boolean {
    return (styleOp1.type === styleOp2.type) && (styleOp1.styleId === styleOp2.styleId);
}

/**
 * Sets the passed "changeStyleSheet" operation to "removed" state, if it does
 * not contain any attributes or other settings.
 */
function checkChangeStyleSheetNoOp(changeOp: Op.ChangeStyleSheetOperation): void {
    if (!changeOp.attrs && !("styleName" in changeOp) && !("parent" in changeOp)) {
        setOperationRemoved(changeOp);
    }
}

/**
 * Returns whether both operations are referring to the same component in the
 * same chart object.
 */
function isSameChartComp(op1: ChartCompOperation, op2: ChartCompOperation): boolean {
    return equalPositions(op1.start, op2.start) && (op1.axis === op2.axis);
}

/**
 * Transforms the series index in a chart data series operation according to an
 * inserted data series in the same chart.
 */
function transformChartSeriesForInsert(seriesOp: Op.ChartSeriesOperation, insIdx: number): void {
    seriesOp.series = transformIndexInsert(seriesOp.series, insIdx, 1);
}

/**
 * Transforms the series index in a chart data series operation according to a
 * deleted data series in the same chart.
 */
function transformChartSeriesForDelete(seriesOp: Op.ChartSeriesOperation, delIdx: number): void {
    const xfIdx = transformIndexDelete(seriesOp.series, delIdx, 1);
    if (xfIdx === undefined) {
        setOperationRemoved(seriesOp);
    } else {
        seriesOp.series = xfIdx;
    }
}

/**
 * Sets the passed "changeChartAxis" operation to "removed" state, if it does
 * not contain any attributes or other settings.
 */
function checkChangeChartAxisNoOp(changeOp: Op.ChangeChartAxisOperation): void {
    if (is.empty(changeOp.attrs) && !("zAxis" in changeOp)) {
        setOperationRemoved(changeOp);
    }
}

// class BaseOTManager ========================================================

/**
 * Implements transformations for basic operations that are defined in the
 * edit framework. Adds generic handling for operations with a "target"
 * identifier.
 */
export class BaseOTManager extends OTEngine {

    // properties -------------------------------------------------------------

    readonly #defaultAutoStyleType: string;

    readonly #indexedAutoStyleREs = new Map<string, RegExp>();

    // constructor ------------------------------------------------------------

    constructor(config?: BaseOTManagerConfig) {
        super();

        // default type for auto-styles without `type` property
        this.#defaultAutoStyleType = config?.defaultAutoStyleType ?? "";

        // create the REs to parse indexed auto-style identifiers
        if (config?.indexedAutoStylePrefixes) {
            dict.forEach(config.indexedAutoStylePrefixes, (prefix, family) => {
                this.#indexedAutoStyleREs.set(family, new RegExp(`^(${re.escape(prefix)})(0|[1-9]\\d*)$`));
            });
        }

        // register alias names
        this.registerAliasName(OT_AUTOSTYLE_ALIAS,    Op.INSERT_AUTOSTYLE, Op.DELETE_AUTOSTYLE, Op.CHANGE_AUTOSTYLE);
        this.registerAliasName(OT_CHART_SERIES_ALIAS, Op.INSERT_CHART_SERIES, Op.DELETE_CHART_SERIES, Op.CHANGE_CHART_SERIES);
        this.registerAliasName(OT_CHART_COMP_ALIAS,   Op.CHANGE_CHART_AXIS, Op.CHANGE_CHART_GRID, Op.CHANGE_CHART_TITLE, Op.CHANGE_CHART_LEGEND);
        this.registerAliasName(OT_CHART_ALIAS,        OT_CHART_SERIES_ALIAS, Op.DELETE_CHART_AXIS, OT_CHART_COMP_ALIAS);

        // names of operations to be ignored in transformations
        this.registerIgnoreNames(IGNORE_OPS);

        // preprocessor for operations with target identifiers
        this.registerTransformPrepareHandler(this.#prepareTargetOperations);

        // changeConfig
        this.registerSelfTransformation(Op.CHANGE_CONFIG, this.#transform_changeConfig_changeConfig);

        // insertStyleSheet
        this.registerSelfTransformation(Op.INSERT_STYLESHEET, this.#transform_insertStyleSheet_insertStyleSheet);
        this.registerBidiTransformation(Op.INSERT_STYLESHEET, Op.DELETE_STYLESHEET, this.#transform_insertStyleSheet_deleteStyleSheet);
        this.registerBidiTransformation(Op.INSERT_STYLESHEET, Op.CHANGE_STYLESHEET, this.#transform_insertStyleSheet_changeStyleSheet);

        // deleteStyleSheet
        this.registerSelfTransformation(Op.DELETE_STYLESHEET, this.#transform_deleteStyleSheet_deleteStyleSheet);
        this.registerBidiTransformation(Op.DELETE_STYLESHEET, Op.CHANGE_STYLESHEET, this.#transform_deleteStyleSheet_changeStyleSheet);

        // changeStyleSheet
        this.registerSelfTransformation(Op.CHANGE_STYLESHEET, this.#transform_changeStyleSheet_changeStyleSheet);

        // auto-styles
        this.registerBidiTransformation(OT_AUTOSTYLE_ALIAS, OT_CHART_ALIAS, null);

        // insertAutoStyle
        this.registerSelfTransformation(Op.INSERT_AUTOSTYLE, this.#transform_insertAutoStyle_insertAutoStyle);
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.DELETE_AUTOSTYLE, this.#transform_insertAutoStyle_deleteAutoStyle);
        this.registerBidiTransformation(Op.INSERT_AUTOSTYLE, Op.CHANGE_AUTOSTYLE, this.#transform_insertAutoStyle_changeAutoStyle);

        // deleteAutoStyle
        this.registerSelfTransformation(Op.DELETE_AUTOSTYLE, this.#transform_deleteAutoStyle_deleteAutoStyle);
        this.registerBidiTransformation(Op.DELETE_AUTOSTYLE, Op.CHANGE_AUTOSTYLE, this.#transform_deleteAutoStyle_changeAutoStyle);

        // changeAutoStyle
        this.registerSelfTransformation(Op.CHANGE_AUTOSTYLE, this.#transform_changeAutoStyle_changeAutoStyle);

        // chart series
        this.registerBidiTransformation(OT_CHART_SERIES_ALIAS, Op.DELETE_CHART_AXIS, null);
        this.registerBidiTransformation(OT_CHART_SERIES_ALIAS, OT_CHART_COMP_ALIAS, null);

        // insertChartSeries
        this.registerSelfTransformation(Op.INSERT_CHART_SERIES, this.#transform_insertChartSeries_insertChartSeries);
        this.registerBidiTransformation(Op.INSERT_CHART_SERIES, Op.DELETE_CHART_SERIES, this.#transform_insertChartSeries_deleteChartSeries);
        this.registerBidiTransformation(Op.INSERT_CHART_SERIES, Op.CHANGE_CHART_SERIES, this.#transform_insertChartSeries_changeChartSeries);

        // deleteChartSeries
        this.registerSelfTransformation(Op.DELETE_CHART_SERIES, this.#transform_deleteChartSeries_deleteChartSeries);
        this.registerBidiTransformation(Op.DELETE_CHART_SERIES, Op.CHANGE_CHART_SERIES, this.#transform_deleteChartSeries_changeChartSeries);

        // changeChartSeries
        this.registerSelfTransformation(Op.CHANGE_CHART_SERIES, this.#transform_changeChartSeries_changeChartSeries);

        // deleteChartAxis
        this.registerSelfTransformation(Op.DELETE_CHART_AXIS, this.#transform_deleteChartAxis_deleteChartAxis);
        this.registerBidiTransformation(Op.DELETE_CHART_AXIS, OT_CHART_COMP_ALIAS, this.#transform_deleteChartAxis_changeChartComp);

        // changeChartAxis
        this.registerSelfTransformation(Op.CHANGE_CHART_AXIS, this.#transform_changeChartAxis_changeChartAxis);

        // changeChartComp
        this.registerSelfTransformation(OT_CHART_COMP_ALIAS, this.#transform_changeChartComp_changeChartComp);
    }

    // public methods ---------------------------------------------------------

    /**
     * Transforms an external JSON document position with all JSON operations
     * contained in the passed operation actions.
     *
     * @param position
     *  The JSON document position to be transformed with the passed actions.
     *  Can be the current local selection position that must be adapted
     *  because of an applied external action; or the remote selection of other
     *  users that must be adapted because of the stack of local operations
     *  that did not received an acknowledgement from the server.
     *
     * @param actionsOrOp
     *  The operation actions with all document operations to be used to
     *  transform the passed position; or a single JSON document operation that
     *  will be wrapped into a temporary action internally.
     *
     * @param [target]
     *  An optional target string that can be specified for the position.
     *
     * @returns
     *  The transformed JSON position; or `undefined`, if the position cannot
     *  be transformed (e.g. has been "deleted").
     */
    transformPosition(position: Position, actionsOrOp: OperationAction[] | Operation, target?: string): Opt<Position> {

        // create a synthetic operation with a `start` property that will be transformed
        const posOp: Op.PositionOperation = { name: Op.POSITION, start: clonePosition(position) };
        if (target) { posOp.target = target; }

        // convert sinle operation to array of operation actions
        const localActions = is.array(actionsOrOp) ? actionsOrOp : [{ operations: [actionsOrOp] }];

        // transform the synthetic operation
        const resultOps = this.transformOperation(posOp, localActions);

        // position may have been deleted
        if (resultOps.length === 0) { return undefined; }

        // check that the transformation handlers did not generate other operations
        if ((resultOps.length > 1) || (resultOps[0].name !== Op.POSITION)) {
            throw new Error("invalid transformation result, expected single position operation");
        }

        // return the new position
        return (resultOps[0] as Op.PositionOperation).start;
    }

    /**
     * Transforms the passed selection state object (as stored for example in
     * undo/redo actions) *in-place* against all passed document operations.
     *
     * Intended to be overwritten by subclasses. This default implementation
     * retains the passed selection state unmodified.
     *
     * @param _selectState
     *  The selection state to be transformed *in-place*.
     *
     * @param _transformActions
     *  The operation actions used to transform the sheet selection state.
     *
     * @returns
     *  Whether the transformed selection state remains valid (`true`); or has
     *  become invalid and needs to be dropped (`false`).
     */
    transformSelectionState(_selectState: Dict, _transformActions: OperationAction[]): boolean {
        return true;
    }

    // protected methods ------------------------------------------------------

    /**
     * Resolves the type identifier of the passed auto-style.
     *
     * @param styleOp
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  The type identifier of the passed auto-style. If the operation does not
     *  contain an explicit type identifier, the default type identifier passed
     *  to the constructor of this instance will be returned.
     */
    protected getAutoStyleType(styleOp: Op.AutoStyleOperation): string {
        return styleOp.type ?? this.#defaultAutoStyleType;
    }

    /**
     * Resolves the type identifier of the passed auto-styles. Returns the type
     * identifier only if both auto-styles have the same type.
     *
     * @param styleOp1
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @param styleOp2
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  The type identifier of the passed auto-styles; or `undefined` if the
     *  auto-styles do not have the same type.
     */
    protected getSameAutoStyleType(styleOp1: Op.AutoStyleOperation, styleOp2: Op.AutoStyleOperation): Opt<string> {
        const type1 = this.getAutoStyleType(styleOp1);
        const type2 = this.getAutoStyleType(styleOp2);
        return (type1 === type2) ? type1 : undefined;
    }

    /**
     * Returns whether the auto-style operation refers to an auto-style
     * collection with indexed naming scheme.
     *
     * @param styleOp
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  Whether the auto-style operation refers to an auto-style collection
     *  with indexed naming scheme.
     */
    protected isIndexedAutoStyle(styleOp: Op.AutoStyleOperation): boolean {
        return this.#indexedAutoStyleREs.has(this.getAutoStyleType(styleOp));
    }

    /**
     * Returns whether both auto-style operations refer to the same type.
     *
     * @param styleOp1
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @param styleOp2
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  Whether both auto-style operations refer to the same type.
     */
    protected isSameAutoStyleType(styleOp1: Op.AutoStyleOperation, styleOp2: Op.AutoStyleOperation): boolean {
        return this.getAutoStyleType(styleOp1) === this.getAutoStyleType(styleOp2);
    }

    /**
     * Returns whether both auto-style operations refer to the same auto-style.
     *
     * @param styleOp1
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @param styleOp2
     *  An arbitrary auto-style operation with optional `type` property.
     *
     * @returns
     *  Whether both operations refer to the same auto-style (type and
     *  identifier).
     */
    protected isSameAutoStyle(styleOp1: Op.AutoStyleOperation, styleOp2: Op.AutoStyleOperation): boolean {
        return this.isSameAutoStyleType(styleOp1, styleOp2) && (styleOp1.styleId === styleOp2.styleId);
    }

    /**
     * Transforms an indexed auto-style identifier in the passed record.
     *
     * @param record
     *  The record containing an auto-style identifier property.
     *
     * @param key
     *  The name of the auto-style identifier property in the record.
     *
     * @param type
     *  The type of the auto-styles referered by the record.
     *
     * @param styleOp
     *  The auto-style operation causing to transform the auto-style property
     *  in the record.
     *
     * @param insert
     *  Whether the passed auto-style operation is "insertAutoStyle" (`true`),
     *  or "deleteAutoStyle" (`false`).
     *
     * @returns
     *  Whether the property in the record has changed.
     */
    protected transformAutoStyleProperty<KT extends string>(
        record: PtRecord<KT, string>,
        key: KT,
        type: string,
        styleOp: Op.AutoStyleOperation,
        insert: boolean
    ): boolean {

        // check the type of the passed auto-style operation
        if (this.getAutoStyleType(styleOp) !== type) { return false; }

        // check if the record contains an auto-style identifier
        const styleId = record[key];
        if (!is.string(styleId)) { return false; }

        // try to get the parser RE for indexed auto-style identifiers
        const parseRE = this.#indexedAutoStyleREs.get(type);
        if (!parseRE) { return false; }

        // try to parse the style identifier of the record
        const xfMatches = parseRE.exec(styleId);
        this.ensure(xfMatches, `OP ERROR: invalid auto-style identifier "${styleId}" in property "${key}" (indexed mode)`);
        let xfIndex = parseInt(xfMatches[2], 10);

        // try to parse the style identifier of the auto-style operation
        const styleMatches = parseRE.exec(styleOp.styleId);
        this.ensure(styleMatches, `OP ERROR: invalid auto-style identifier "${styleOp.styleId}" in auto-style operation (indexed mode)`);
        const styleIndex = parseInt(styleMatches[2], 10);

        // transform the index of the record's auto-style identifier
        if (insert) {
            if (xfIndex < styleIndex) {
                return false;
            }
            xfIndex += 1;
        } else {
            if (xfIndex === styleIndex) {
                delete record[key];
                styleOp.otIndexShift = true;
                return true;
            }
            if (xfIndex <= styleIndex) {
                return false;
            }
            xfIndex -= 1;
        }

        // write the new style identifier back into the record
        record[key] = `${xfMatches[1]}${xfIndex}`;
        styleOp.otIndexShift = true;
        return true;
    }

    // preprocessors ----------------------------------------------------------

    /**
     * Preprocessor callback function for operations with target identifiers.
     * Matches targets (skips operations that address different targets), and
     * handles operations that will delete a target object.
     */
    #prepareTargetOperations(lclOp: AnyTargetIdOperation, extOp: AnyTargetIdOperation): TransformPrepareResult {

        // whether the operations delete their targets
        const lclDelTarget = this.hasAliasName(lclOp, OT_DELETE_TARGET_ALIAS);
        const extDelTarget = this.hasAliasName(extOp, OT_DELETE_TARGET_ALIAS);

        // Two operations want to delete a target object. If the target type and identifier are the same,
        // the target has been deleted locally and externally, so the operations can be set to "deleted" state.
        if (lclDelTarget && extDelTarget) {
            if ((lclOp.name === extOp.name) && (lclOp.id === extOp.id)) {
                setOperationRemoved(lclOp); // ignore the local operation (do not send to server)
                setOperationRemoved(extOp); // ignore the external operation (target already removed locally)
            }
            // always skip (different deleted targets do not influence each other)
            return TransformPrepareResult.IGNORE;
        }

        // whether the operations can ignore their targets
        const lclIgnoreTarget = this.hasAliasName(lclOp, OT_IGNORE_TARGET_ALIAS);
        const extIgnoreTarget = this.hasAliasName(extOp, OT_IGNORE_TARGET_ALIAS);

        // force transformation regardless of target settings
        const forceTransform = (lclDelTarget || extDelTarget) && (lclIgnoreTarget || extIgnoreTarget);

        // the target specifier of the operations (unless they ignore their targets)
        const lclTarget = lclIgnoreTarget ? undefined : lclOp.target;
        const extTarget = extIgnoreTarget ? undefined : extOp.target;

        // One operation wants to delete a target object, the other operation wants to modify the same target
        // object. The modifying operation can be set to "deleted" state (regardless if local or external).
        if (!forceTransform && (lclDelTarget !== extDelTarget)) {
            if (lclDelTarget && extTarget && (lclOp.id === extTarget)) {
                setOperationRemoved(extOp);
            } else if (extDelTarget && lclTarget && (extOp.id === lclTarget)) {
                setOperationRemoved(lclOp);
            }
            // the operations will not influence each other further
            return TransformPrepareResult.IGNORE;
        }

        // None of the operations deletes a target, but operations with different target
        // identifiers do not influence each other.
        return (forceTransform || (lclTarget === extTarget)) ? TransformPrepareResult.TRANSFORM : TransformPrepareResult.IGNORE;
    }

    // changeConfig -----------------------------------------------------------

    #transform_changeConfig_changeConfig(lclOp: Op.ChangeConfigOperation, extOp: Op.ChangeConfigOperation): void {
        // reduce both attribute sets, set empty operations to "removed" state
        reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true });
    }

    // insertStyleSheet -------------------------------------------------------

    #transform_insertStyleSheet_insertStyleSheet(lclOp: Op.InsertStyleSheetOperation, extOp: Op.InsertStyleSheetOperation): void {
        // local operation always wins
        if (isSameStyleSheet(lclOp, extOp)) {
            setOperationRemoved(extOp);
        }
    }

    #transform_insertStyleSheet_deleteStyleSheet(insertOp: Op.InsertStyleSheetOperation, deleteOp: Op.DeleteStyleSheetOperation): void {
        // delete operation always wins
        if (isSameStyleSheet(insertOp, deleteOp)) {
            setOperationRemoved(insertOp);
        }
    }

    #transform_insertStyleSheet_changeStyleSheet(insertOp: Op.InsertStyleSheetOperation, changeOp: Op.ChangeStyleSheetOperation): void {
        // insert operation always wins
        if (isSameStyleSheet(insertOp, changeOp)) {
            setOperationRemoved(changeOp);
        }
    }

    // deleteStyleSheet -------------------------------------------------------

    #transform_deleteStyleSheet_deleteStyleSheet(lclOp: Op.DeleteStyleSheetOperation, extOp: Op.DeleteStyleSheetOperation): void {
        if (isSameStyleSheet(lclOp, extOp)) {
            setOperationRemoved(lclOp, extOp);
        }
    }

    #transform_deleteStyleSheet_changeStyleSheet(deleteOp: Op.DeleteStyleSheetOperation, changeOp: Op.ChangeStyleSheetOperation): void {
        // delete operation always wins
        if (isSameStyleSheet(deleteOp, changeOp)) {
            setOperationRemoved(changeOp);
        }
    }

    // changeStyleSheet -------------------------------------------------------

    #transform_changeStyleSheet_changeStyleSheet(lclOp: Op.ChangeStyleSheetOperation, extOp: Op.ChangeStyleSheetOperation): void {

        // different style sheets are independent
        if (!isSameStyleSheet(lclOp, extOp)) { return; }

        // process the formatting attributes and properties
        reduceOperationAttributes(lclOp, extOp, { deleteEqual: true });
        reduceProperties(lclOp, extOp, ["styleName", "parent"], { deleteEqual: true });

        // ignore the entire operation, if all changes have been discarded
        checkChangeStyleSheetNoOp(lclOp);
        checkChangeStyleSheetNoOp(extOp);
    }

    // insertAutoStyle --------------------------------------------------------

    #transform_insertAutoStyle_insertAutoStyle(lclOp: Op.InsertAutoStyleOperation, extOp: Op.InsertAutoStyleOperation): void {

        // different auto-style types are independent
        const styleType = this.getSameAutoStyleType(lclOp, extOp);
        if (!styleType) { return; }

        // in feestyle mode, an auto-style cannot be inserted twice
        if (!this.isIndexedAutoStyle(lclOp)) {
            this.ensure(lclOp.styleId !== extOp.styleId, "NOT IMPLEMENTED: cannot insert auto-style twice");
            return;
        }

        // transform the auto-style identifier of the local operation first
        this.transformAutoStyleProperty(lclOp, "styleId", styleType, extOp, true);

        // transform the auto-style identifier of the external operation afterwards
        this.transformAutoStyleProperty(extOp, "styleId", styleType, lclOp, true);
    }

    #transform_insertAutoStyle_deleteAutoStyle(insertOp: Op.InsertAutoStyleOperation, deleteOp: Op.DeleteAutoStyleOperation): void {

        // different auto-style types are independent
        const styleType = this.getSameAutoStyleType(insertOp, deleteOp);
        if (!styleType) { return; }

        // in feestyle mode, an auto-style cannot be inserted twice
        if (!this.isIndexedAutoStyle(insertOp)) {
            this.ensure(insertOp.styleId !== deleteOp.styleId, "OP ERROR: cannot insert and delete same auto-style");
            return;
        }

        // transform the auto-style identifier of the delete operation first
        this.transformAutoStyleProperty(deleteOp, "styleId", styleType, insertOp, true);

        // transform the auto-style identifier of the insert operation afterwards (will not be deleted)
        this.transformAutoStyleProperty(insertOp, "styleId", styleType, deleteOp, false);
    }

    #transform_insertAutoStyle_changeAutoStyle(insertOp: Op.InsertAutoStyleOperation, changeOp: Op.ChangeAutoStyleOperation): void {

        // different auto-style types are independent
        const styleType = this.getSameAutoStyleType(insertOp, changeOp);
        if (!styleType) { return; }

        // in feestyle mode, an auto-style cannot be inserted if it exists already
        if (!this.isIndexedAutoStyle(insertOp)) {
            this.ensure(insertOp.styleId !== changeOp.styleId, "OP ERROR: cannot insert existing auto-style");
            return;
        }

        // transform the auto-style identifier of the change operation
        this.transformAutoStyleProperty(changeOp, "styleId", styleType, insertOp, true);
    }

    // deleteAutoStyle --------------------------------------------------------

    #transform_deleteAutoStyle_deleteAutoStyle(lclOp: Op.DeleteAutoStyleOperation, extOp: Op.DeleteAutoStyleOperation): void {

        // different auto-style types are independent
        const styleType = this.getSameAutoStyleType(lclOp, extOp);
        if (!styleType) { return; }

        // ignore multiple delete operations on the same auto-style
        if (lclOp.styleId === extOp.styleId) {
            setOperationRemoved(lclOp, extOp);
            return;
        }

        // transform the auto-style identifiers
        if (!this.transformAutoStyleProperty(lclOp, "styleId", styleType, extOp, false)) {
            this.transformAutoStyleProperty(extOp, "styleId", styleType, lclOp, false);
        }
    }

    #transform_deleteAutoStyle_changeAutoStyle(deleteOp: Op.DeleteAutoStyleOperation, changeOp: Op.ChangeAutoStyleOperation): void {

        // different auto-style types are independent
        const styleType = this.getSameAutoStyleType(deleteOp, changeOp);
        if (!styleType) { return; }

        // delete operation always wins over change operation on the same auto-style
        if (deleteOp.styleId === changeOp.styleId) {
            setOperationRemoved(changeOp);
            return;
        }

        // transform the auto-style identifier of the change operation
        this.transformAutoStyleProperty(changeOp, "styleId", styleType, deleteOp, false);
    }

    // changeAutoStyle --------------------------------------------------------

    #transform_changeAutoStyle_changeAutoStyle(lclOp: Op.ChangeAutoStyleOperation, extOp: Op.ChangeAutoStyleOperation): void {
        // reduce both attribute sets, set empty operations to "removed" state
        if (this.isSameAutoStyle(lclOp, extOp)) {
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true });
        }
    }

    // insertChartSeries ------------------------------------------------------

    #transform_insertChartSeries_insertChartSeries(lclOp: Op.InsertChartSeriesOperation, extOp: Op.InsertChartSeriesOperation): void {
        // different charts are independent
        if (equalPositions(lclOp.start, extOp.start)) {
            // same series index in both operations: shift local series index away
            transformChartSeriesForInsert(lclOp, extOp.series);
            // transform external operation with the new local insertion index
            transformChartSeriesForInsert(extOp, lclOp.series);
        }
    }

    #transform_insertChartSeries_deleteChartSeries(insertOp: Op.InsertChartSeriesOperation, deleteOp: Op.DeleteChartSeriesOperation): void {
        // different charts are independent
        if (equalPositions(insertOp.start, deleteOp.start)) {
            // "insert" (new series on either side) cannot collide with "delete" (existing series)
            transformChartSeriesForInsert(deleteOp, insertOp.series);
            // transform insert operation with the new deletion index
            transformChartSeriesForDelete(insertOp, deleteOp.series);
        }
    }

    #transform_insertChartSeries_changeChartSeries(insertOp: Op.InsertChartSeriesOperation, changeOp: Op.ChangeChartSeriesOperation): void {
        // different charts are independent
        if (equalPositions(insertOp.start, changeOp.start)) {
            transformChartSeriesForInsert(changeOp, insertOp.series);
        }
    }

    // deleteChartSeries ------------------------------------------------------

    #transform_deleteChartSeries_deleteChartSeries(lclOp: Op.DeleteChartSeriesOperation, extOp: Op.DeleteChartSeriesOperation): void {
        // different charts are independent
        if (equalPositions(lclOp.start, extOp.start)) {
            const lclIdx = lclOp.series, extIdx = extOp.series;
            transformChartSeriesForDelete(extOp, lclIdx);
            transformChartSeriesForDelete(lclOp, extIdx);
        }
    }

    #transform_deleteChartSeries_changeChartSeries(deleteOp: Op.DeleteChartSeriesOperation, changeOp: Op.ChangeChartSeriesOperation): void {
        // different charts are independent
        if (equalPositions(deleteOp.start, changeOp.start)) {
            transformChartSeriesForDelete(changeOp, deleteOp.series);
        }
    }

    // changeChartSeries ------------------------------------------------------

    #transform_changeChartSeries_changeChartSeries(lclOp: Op.ChangeChartSeriesOperation, extOp: Op.ChangeChartSeriesOperation): void {
        // different chart series are independent
        if (equalPositions(lclOp.start, extOp.start) && (lclOp.series === extOp.series)) {
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true });
        }
    }

    // deleteChartAxis --------------------------------------------------------

    #transform_deleteChartAxis_deleteChartAxis(lclOp: Op.DeleteChartAxisOperation, extOp: Op.DeleteChartAxisOperation): void {
        // set both operations to "removed" state, if they delete the same axis (otherwise they are independent)
        if (isSameChartComp(lclOp, extOp)) {
            setOperationRemoved(lclOp);
            setOperationRemoved(extOp);
        }
    }

    #transform_deleteChartAxis_changeChartComp(deleteOp: Op.DeleteChartAxisOperation, changeOp: ChartCompOperation): void {
        // delete operation always wins over change operation
        if (isSameChartComp(deleteOp, changeOp)) {
            setOperationRemoved(changeOp);
        }
    }

    // changeChartAxis --------------------------------------------------------

    // TODO DOCS-1783: remove this method when extra properties of "changeChartAxis" operation have moved into "attrs"
    #transform_changeChartAxis_changeChartAxis(lclOp: Op.ChangeChartAxisOperation, extOp: Op.ChangeChartAxisOperation): void {
        if (isSameChartComp(lclOp, extOp)) {
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true });
            reduceProperties(lclOp, extOp, "zAxis", { deleteEqual: true });
            if ((extOp.axPos === lclOp.axPos) && (extOp.crossAx === lclOp.crossAx)) {
                checkChangeChartAxisNoOp(lclOp);
                checkChangeChartAxisNoOp(extOp);
            } else {
                extOp.axPos = lclOp.axPos;
                extOp.crossAx = lclOp.crossAx;
            }
        }
    }

    // changeChartComp --------------------------------------------------------

    #transform_changeChartComp_changeChartComp(lclOp: ChartCompOperation, extOp: ChartCompOperation): void {
        // reduce both attribute sets, set empty operations to "removed" state
        if ((lclOp.name === extOp.name) && isSameChartComp(lclOp, extOp)) {
            reduceOperationAttributes(lclOp, extOp, { deleteEqual: true, removeEmptyOp: true });
        }
    }
}
