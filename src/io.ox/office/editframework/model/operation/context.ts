/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, to } from "@/io.ox/office/tk/algorithms";

import type { Position, Operation, AnyOperation } from "@/io.ox/office/editframework/utils/operations";
import { isPosition } from "@/io.ox/office/editframework/utils/operations";
import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

/**
 * Optional parameters for receiving strings from document operations.
 */
export interface GetStringOptions {

    /**
     * If `true`, the empty string is allowed as property value. By default,
     * the existing property must be a string with at least one character.
     */
    empty?: boolean;
}

/**
 * Generic base interface for event data objects emitted by model events when
 * applying document operations.
 */
export interface ContextEventBase {

    /**
     * Specifies whether the event originates from an incoming (external)
     * document operation. Default value is `false`.
     */
    external?: boolean;

    /**
     * Specifies whether the event originates from a document operation applied
     * during the initial import process. Default value is `false`.
     */
    importing?: boolean;
}

// class OperationContext =====================================================

/**
 * A small wrapper for a JSON document operation object providing useful helper
 * methods, used as calling context for operation handler callback functions.
 */
export class OperationContext<DocModelT extends EditModel = EditModel> {

    // properties -------------------------------------------------------------

    /**
     * The document model targeted by the document operation.
     */
    readonly docModel: DocModelT;

    /**
     * The JSON document operation wrapped by this instance.
     */
    readonly operation: AnyOperation;

    /**
     * Whether the operation has been received from the server.
     */
    readonly external: boolean;

    /**
     * Whether the operation is part of the initial document import process.
     */
    readonly importing: boolean;

    // constructor ------------------------------------------------------------

    constructor(docModel: DocModelT, operation: Operation, external?: boolean, importing?: boolean) {
        this.docModel = docModel;
        this.operation = operation as AnyOperation;
        this.external = !!external;
        this.importing = !!importing;
    }

    // public methods ---------------------------------------------------------

    /**
     * Throws an `OperationError` exception. Can be used as convenience
     * shortcut to prevent importing the `OperationError` code module.
     *
     * @param message
     *  The message text inserted into the `OperationError` exception. Can be a
     *  format string with "%s" and "%d" placeholders that will be replaced
     *  with all following parameters.
     *
     * @param args
     *  Additional values to be inserted into the format string passed in the
     *  parameter `message`.
     *
     * @throws
     *  This method always throws an `OperationError` exception.
     */
    error(message: string, ...args: unknown[]): never {
        message = message.replace(/%[a-z]/g, () => String(args.shift()));
        throw new OperationError(message, { operation: this.operation });
    }

    /**
     * Throws an `OperationError` exception, if the passed condition is falsy.
     *
     * @param condition
     *  If falsy, an `OperationError` exception will be thrown with the passed
     *  message text.
     *
     * @param message
     *  The message text inserted into the `OperationError` exception thrown in
     *  case the condition is falsy. Can be a format string with "%s" and "%d"
     *  placeholders that will be replaced with all following parameters.
     *
     * @param args
     *  Additional values to be inserted into the format string passed in the
     *  parameter `message`.
     *
     * @throws
     *  An `OperationError` with the passed message text, if the passed
     *  condition is falsy.
     */
    ensure(condition: unknown, message: string, ...args: unknown[]): asserts condition {
        if (!condition) { this.error(message, ...args); }
    }

    /**
     * Returns whether the wrapped JSON operation contains a property with the
     * specified name.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  Whether the operation contains a property with the specified name.
     */
    has(propName: string): boolean {
        return propName in this.operation;
    }

    /**
     * Returns the value of a required boolean property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  boolean property with the specified name.
     */
    getBool(propName: string): boolean {
        const value = this.operation[propName];
        this.ensure(is.boolean(value), `missing boolean property "${propName}"`);
        return value;
    }

    /**
     * Returns the value of an optional boolean property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [defValue=false]
     *  The default value that will be returned if the operation does not
     *  contain the specified property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a boolean.
     */
    optBool(propName: string, defValue = false): boolean {
        return this.has(propName) ? this.getBool(propName) : defValue;
    }

    /**
     * Returns the value of a required floating-point number property of the
     * wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  floating-point number property with the specified name.
     */
    getNum(propName: string): number {
        return this.#validateNum(propName, this.operation[propName]);
    }

    /**
     * Returns the value of an optional floating-point number property of the
     * wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [defValue=0]
     *  The default value that will be returned if the operation does not
     *  contain the specified property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a number.
     */
    optNum(propName: string, defValue = 0): number {
        return this.has(propName) ? this.getNum(propName) : defValue;
    }

    /**
     * Returns the value of a required integer property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property. If the property is a
     *  floating-point number, it will be truncated to an integer.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  number property with the specified name.
     */
    getInt(propName: string): number {
        return Math.floor(this.getNum(propName));
    }

    /**
     * Returns the value of an optional integer property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [defValue=0]
     *  The default value that will be returned if the operation does not
     *  contain the specified property.
     *
     * @returns
     *  The value of the specified operation property. If the property is a
     *  floating-point number, it will be truncated to an integer.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a number.
     */
    optInt(propName: string, defValue = 0): number {
        return Math.floor(this.optNum(propName, defValue));
    }

    /**
     * Returns the value of a required string property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  string property with the specified name.
     */
    getStr(propName: string, options?: GetStringOptions): string {
        return this.#validateStr(propName, this.operation[propName], options);
    }

    /**
     * Returns the value of an optional string property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [defValue=""]
     *  The default value that will be returned if the operation does not
     *  contain the specified property.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid string.
     */
    optStr(propName: string, options?: GetStringOptions): string;
    optStr(propName: string, defValue: string, options?: GetStringOptions): string;
    // implementation
    optStr(propName: string, arg1?: unknown, arg2?: GetStringOptions): string {
        const options = is.string(arg1) ? arg2 : arg1 as Opt<GetStringOptions>;
        const defValue = is.string(arg1) ? arg1 : "";
        return this.has(propName) ? this.getStr(propName, options) : defValue;
    }

    /**
     * Returns the value of a required primitive property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [options]
     *  Optional parameters for string values.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  primitive property with the specified name.
     */
    getPrim(propName: string, options?: GetStringOptions): number | string | boolean | null {
        const value = this.operation[propName];
        if (is.string(value)) { return this.#validateStr(propName, value, options); }
        if (is.number(value)) { return this.#validateNum(propName, value); }
        this.ensure((value === null) || is.boolean(value), `missing primitive property "${propName}"`);
        return value;
    }

    /**
     * Returns the value of an optional primitive property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The value of the specified operation property; or `null`, if the
     *  wrapped operation does not contain the property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid primitive.
     */
    optPrim(propName: string, options?: GetStringOptions): number | string | boolean | null {
        return this.has(propName) ? this.getPrim(propName, options) : null;
    }

    /**
     * Returns the value of a required enumeration property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param EnumType
     *  The expected string enumeration type.
     *
     * @returns
     *  The enumeration value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  string property with the specified name, or the string value cannot be
     *  converted to a value of the passed enumeration class.
     */
    getEnum<T>(propName: string, EnumType: T): T[keyof T] {
        const strValue = this.getStr(propName);
        const enumValue = to.enum(EnumType, strValue);
        this.ensure(enumValue, `invalid enumeration value "${strValue}" in property "${propName}"`);
        return enumValue;
    }

    /**
     * Returns the value of an optional enumeration property of the wrapped
     * JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param EnumType
     *  The expected string enumeration type.
     *
     * @param defValue
     *  The default value that will be returned if the operation does not
     *  contain the specified property.
     *
     * @returns
     *  The enumeration value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid string, or
     *  the string value cannot be converted to a value of the passed
     *  enumeration class.
     */
    optEnum<T>(propName: string, EnumType: T, defValue: T[keyof T]): T[keyof T] {
        return this.has(propName) ? this.getEnum(propName, EnumType) : defValue;
    }

    /**
     * Returns the value of a required object property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  object property with the specified name.
     */
    getDict(propName: string): Dict {
        const value = this.operation[propName];
        this.ensure(is.dict(value), `missing object property "${propName}"`);
        return value;
    }

    /**
     * Returns the value of an optional object property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [defValue]
     *  The default value that will be returned if the operation does not
     *  contain the specified property. If omitted, `null` will be returned
     *  instead (NOT an empty object!).
     *
     * @returns
     *  The value of the specified operation property; or `null`, if the
     *  property is missing, and no default value has been passed.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not an object.
     */
    optDict(propName: string, defValue: Dict): Dict;
    optDict(propName: string, defValue?: Opt<Dict>): Opt<Dict>;
    // implementation
    optDict(propName: string, defValue?: Dict): Opt<Dict> {
        return this.has(propName) ? this.getDict(propName) : defValue;
    }

    /**
     * Returns the value of a required array property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  array property with the specified name.
     */
    getArr(propName: string): unknown[] {
        const value = this.operation[propName];
        this.ensure(is.nativeArray(value), `missing array property "${propName}"`);
        return value;
    }

    /**
     * Returns the value of an optional array property of the wrapped JSON
     * operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @param [defValue]
     *  The default value that will be returned if the operation does not
     *  contain the specified property. If omitted, `null` will be returned
     *  instead (NOT an empty array!).
     *
     * @returns
     *  The value of the specified operation property; or `null`, if the
     *  property is missing, and no default value has been passed.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not an array.
     */
    optArr(propName: string, defValue: unknown[]): unknown[];
    optArr(propName: string, defValue?: unknown[]): Opt<unknown[]>;
    // implementation
    optArr(propName: string, defValue?: unknown[]): Opt<unknown[]> {
        return this.has(propName) ? this.getArr(propName) : defValue;
    }

    /**
     * Returns the value of a required logical document position property of
     * the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation does not contain a valid
     *  document position property with the specified name.
     */
    getPos(propName: string): Position {
        const position = this.getArr(propName);
        this.ensure(isPosition(position), `missing position property "${propName}"`);
        return position;
    }

    /**
     * Returns the value of an optional logical document position property of
     * the wrapped JSON operation.
     *
     * @param propName
     *  The name of the operation property.
     *
     * @returns
     *  The value of the specified operation property; or `null`, if the
     *  property is missing.
     *
     * @throws
     *  An `OperationError`, if the wrapped operation contains a property with
     *  the specified name, but the property value is not a valid logical
     *  document position.
     */
    optPos(propName: string): Position | null {
        return this.has(propName) ? this.getPos(propName) : null;
    }

    // private methods --------------------------------------------------------

    #validateNum(propName: string, value: unknown): number {
        this.ensure(Number.isFinite(value), `missing finite number property "${propName}"`);
        return value as number;
    }

    #validateStr(propName: string, value: unknown, options?: GetStringOptions): string {
        this.ensure(is.string(value), `missing string property "${propName}"`);
        this.ensure(options?.empty || value, `empty string property "${propName}"`);
        return value;
    }
}
