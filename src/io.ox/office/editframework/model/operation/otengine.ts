/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, itr, ary, map, dict } from "@/io.ox/office/tk/algorithms";
import { LogLevel } from "@/io.ox/office/tk/utils/logger";
import { EObject } from "@/io.ox/office/tk/objects";

import type { Operation } from "@/io.ox/office/editframework/utils/operations";
import type { OperationAction } from "@/io.ox/office/editframework/utils/operationutils";

import {
    OTError, otLogger,
    getOTKey, parseOTKey, isOperationRemoved,
    dbgFormatOp, dbgFormatOps, dbgFormatUserData, dbgFormatActions
} from "@/io.ox/office/editframework/utils/otutils";

// types ======================================================================

/**
 * Data type for methods taking an arbitrary list of operation names, as single
 * string parameters, or as string arrays.
 */
export type OpNameArgs = Array<string | string[]>;

/**
 * A callback function invoked before starting to transform all local document
 * operation against all existing external operations (global preparation), or
 * after all existing external operations have been transformed against all
 * local operation (global finalization).
 */
export type GlobalHandlerFn<ThisT extends OTEngine> = (this: ThisT) => void;

/**
 * A callback function invoked before starting to transform the operations of
 * the next local action against the external operations (action preparation),
 * or after all operations of a local action have been transformed against the
 * external operations (action finalization).
 *
 * @param extOps
 *  The current external operations.
 *
 * @param userData
 *  The user data contained in the lcoal action.
 */
export type ActionHandlerFn<ThisT extends OTEngine> = (this: ThisT, extOps: Operation[], userData: Opt<object>) => void;

/**
 * The result of an operation preparation callback function.
 */
export enum TransformPrepareResult {

    /**
     * The operations need to be transformed.
     */
    TRANSFORM = 0,

    /**
     * The operations can be ignored.
     */
    IGNORE = 1
}

/**
 * A callback function invoked for all processed combinations of JSON document
 * operations, before resolving and executing the transformation handler.
 *
 * param lclOp
 *  The local operation to be transformed by the external operation.
 *
 * @param extOp
 *  The external operation to be transformed by the local operation.
 *
 * @param swapped
 *  Whether the local and external operations have been effectively swapped.
 *
 * @param userData
 *  The custom user data passed to the transformation process.
 */
export type TransformPrepareFn<ThisT extends OTEngine> = (
    this: ThisT,
    lclOp: Operation,
    extOp: Operation,
    swapped: boolean,
    userData: Opt<object>
) => void | Opt<TransformPrepareResult>;

/**
 * A callback function invoked for all processed combinations of JSON document
 * operations, after the transformation handler has been resolved and executed.
 *
 * @param lclOp
 *  The local operation that has been transformed by the external operation; or
 *  `undefined`, if the local operation has been deleted.
 *
 * @param extOp
 *  The external operation that has been transformed by the local operation; or
 *  `undefined`, if the external operation has been deleted.
 *
 * @param swapped
 *  Whether the local and external operations have been effectively swapped.
 *
 * @param userData
 *  The custom user data passed to the transformation process.
 */
export type TransformFinalizeFn<ThisT extends OTEngine> = (
    this: ThisT,
    lclOp: Opt<Operation>,
    extOp: Opt<Operation>,
    swapped: boolean,
    userData: Opt<object>
) => void;

/**
 * Optional parameters for the method `OTEngine.transformOperations()`.
 */
export interface TransformOpsOptions {

    /**
     * If set to `true`, the transformations will be executed with swapped
     * operations: The local operations will be passed as external operations,
     * and vice versa. Default value is `false`.
     */
    swapped?: boolean;

    /**
     * Default application-specific user data to be processed for all local
     * actions without own user data during the transformations. If passed
     * here, the local actions MUST NOT contain own user data objects.
     */
    userData?: object;
}

/**
 * The optional result descriptor of an operation transformation handler.
 * Contains all new operations to be inserted into the arrays of operations
 * around the operation currently processed.
 */
export interface TransformOpsResult {

    /**
     * New operations to be inserted before the current local operation.
     */
    localOpsBefore?: Operation | Operation[];

    /**
     * New operations to be inserted after the current local operation.
     */
    localOpsAfter?: Operation | Operation[];

    /**
     * New operations to be inserted before the current external operation.
     */
    externalOpsBefore?: Operation | Operation[];

    /**
     * New operations to be inserted after the current external operation.
     */
    externalOpsAfter?: Operation | Operation[];
}

/**
 * The implementation of a transformation between two JSON document operations.
 *
 * The operations will be transformed so that the "external operation" happens
 * before the "local operation". It may be necessary to perform transformations
 * of the properties in either of the JSON operation objects.
 *
 * @param lclOp
 *  The local operation to be transformed by the external operation.
 *
 * @param extOp
 *  The external operation to be transformed by the local operation.
 *
 * @param swapped
 *  If set to `true`, the transformation handler has been called with swapped
 *  local and external operation parameters (this parameter will only be set to
 *  `true`, if the transformation handler has been registered with the method
 *  `OTEngine.registerBidiTransformation()`, and the external operation will be
 *  passed as first parameter to match the order of the operation names during
 *  registration).
 */
export type TransformOpsFn<ThisT extends OTEngine, LclOpT extends Operation, ExtOpT extends Operation> = (
    this: ThisT,
    lclOp: LclOpT,
    extOp: ExtOpT,
    swapped: boolean
) => void | Opt<TransformOpsResult>;

/**
 * The possible types that can be used to register an operation transformation.
 *
 * - Can be a transformation callback handler (type `TransformOpsFn`) for a
 *   supported transformation.
 *
 * - Can be a string that represents an error message for an `OTError`. This
 *   error will be thrown unconditionally for the transformation (e.g. for
 *   unconditional unresolvable conflicts).
 *
 * - Can be the value `null` for a no-op handler for independent operations.
 */
export type TransformOpsSpec<ThisT extends OTEngine, LclOpT extends Operation, ExtOpT extends Operation> =
    TransformOpsFn<ThisT, LclOpT, ExtOpT> |
    string |
    null;

/**
 * Optional parameters for the method `OTEngine.ensure()`.
 */
export interface EnsureOptions {

    /**
     * If set to `true`, the OT Error will trigger a reload, even if it was
     * caused by an undo-operation. In this case, it is not sufficient to clean
     * the undo stack.
     */
    forceReload?: boolean;
}

/**
 * Type mapping for the events emitted by `OTEngine` instances.
 */
export interface OTEngineEventMap {

    /**
     * Will be emitted when an `OTEngine` instance logs a warning via the
     * methods `warn` or `warnIf`.
     *
     * @param message
     *  The warning message that has been logged.
     */
    warning: [message: string];

    /**
     * Will be emitted when an `OTEngine` instance throws an `OTError` via the
     * methods `error` or `ensure`.
     *
     * @param message
     *  The error message that has been logged.
     */
    error: [message: string];
}

// private types --------------------------------------------------------------

/**
 * A transformation callback handler with generic type parameters.
 */
type GenericTransformOpsFn = TransformOpsFn<OTEngine, Operation, Operation>;

/**
 * A transformation specification with generic type parameters.
 */
type GenericTransformOpsSpec = TransformOpsSpec<OTEngine, Operation, Operation>;

/**
 * An optional transformation callback handler with generic type parameters.
 */
type OptTransformOpsFn = Opt<GenericTransformOpsFn>;

// constants ==================================================================

/**
 * Special transformation handler that does not do anything.
 */
const NO_OP_HANDLER: GenericTransformOpsFn = fun.undef;

// private functions ==========================================================

/**
 * Invokes the passed callback function for each string in the passed arguments
 * array.
 *
 * @param opNameArgs
 *  The strings to be iterated. Either array element can be a single string, or
 *  an array of strings.
 *
 * @param callback
 *  The callback function to be invoked for each string in the passed arguments
 *  array.
 */
function forEachOpName(opNameArgs: OpNameArgs, callback: (opName: string) => void): void {
    opNameArgs.forEach(opNames => ary.wrap(opNames).forEach(callback));
}

/**
 * Returns the total number of operations in the passed actions array.
 */
function countOperations(actions: OperationAction[]): number {
    return actions.reduce((c, a) => c + a.operations.length, 0);
}

/**
 * Returns a transformation result with swapped local and external properties.
 */
function swapOpTransformResult(result: TransformOpsResult): TransformOpsResult {
    return {
        localOpsBefore: result.externalOpsBefore,
        localOpsAfter: result.externalOpsAfter,
        externalOpsBefore: result.localOpsBefore,
        externalOpsAfter: result.localOpsAfter
    };
}

/**
 * Returns a number with following text in correct plural form. DEBUG ONLY!
 */
function formatCount(count: number, singular: string, plural?: string): string {
    const msg = (count === 1) ? singular : (plural ?? `${singular}s`);
    return `${count} ${msg}`;
}

/**
 * Writes the passed local actions, and external operations, to the console.
 */
function dumpTransformationData(headMsg: string, extOps: Operation[], lclActions: OperationAction[], userData?: object): void {
    otLogger.log(() => {
        const lines = [headMsg];
        const trace = otLogger.isLogLevelActive(LogLevel.TRACE);
        if (trace && userData) { lines.push(dbgFormatUserData(userData, 0)); }
        lines.push(`${formatCount(lclActions.length, "local action")} with ${formatCount(countOperations(lclActions), "operation")}`);
        if (trace) { lines.push(dbgFormatActions(lclActions, 1)); }
        lines.push(formatCount(extOps.length, "external operation"));
        if (trace) { lines.push(dbgFormatOps(extOps, 1)); }
        return lines.join("\n");
    });
}

// class OTEngine =============================================================

/**
 * This class is the base implementation engine for Operational Transformations
 * (OT). Subclasses are intended to add callback handlers for various
 * combinations of internal and external operations to be transformed against
 * each other.
 *
 * An instance transforms external operations to be applied locally, if there
 * are local operations that are not yet acknowledged by the server.
 *
 * Additionally, local operations without acknowledgement by the server need to
 * be transformed by two reasons:
 * 1. If they are not already sent to the server, they are transformed on
 *    client side, so that the server does not need to do it. This is
 *    especially important, if a client is only allowed to send his operations
 *    to the server after receiving the acknowledgement for his previous sent
 *    operations.
 * 2. Following external operations must be transformed with the modified local
 *    operations.
 */
export class OTEngine extends EObject<OTEngineEventMap> {

    // properties -------------------------------------------------------------

    /**
     * The local operation currently transformed. Used by the method `error()`
     * and friends to generate an exception containing the current operations.
     */
    protected lclOp: Opt<Operation>;

    /**
     * The external operation currently transformed. Used by the method
     * `error()` and friends to generate an exception containing the current
     * operations.
     */
    protected extOp: Opt<Operation>;

    /**
     * All global preparation handler functions.
     */
    readonly #globalPrepareHandlers = new Array<GlobalHandlerFn<any>>();

    /**
     * All global finalization handler functions.
     */
    readonly #globalFinalizeHandlers = new Array<GlobalHandlerFn<any>>();

    /**
     * All action preparation handler functions.
     */
    readonly #actionPrepareHandlers = new Array<ActionHandlerFn<any>>();

    /**
     * All action finalization handler functions.
     */
    readonly #actionFinalizeHandlers = new Array<ActionHandlerFn<any>>();

    /**
     * All transformation preparation handler functions.
     */
    readonly #transformPrepareHandlers = new Array<TransformPrepareFn<any>>();

    /**
     * All transformation finalization handler functions.
     */
    readonly #transformFinalizeHandlers = new Array<TransformFinalizeFn<any>>();

    /**
     * All registered transformation handler functions, by OT key.
     */
    readonly #rawHandlers = new Map<string, GenericTransformOpsFn>();

    /**
     * Maps real operation names to alias names that will be used to register
     * transformation handler functions.
     */
    readonly #rawAliasNames = new Map<string, Set<string>>();

    /**
     * Contains the names of all operations to be ignored silently.
     */
    readonly #ignoreNames = new Set<string>();

    /**
     * The transformation handler functions by real operation names resolved
     * from alias names (as used in the property `rawHandlers`).
     */
    readonly #resolvedHandlers = new Map<string, OptTransformOpsFn>();

    /**
     * Cache for checking operation names having a specific alias, by OT key.
     */
    readonly #resolvedAliasNames = new Map<string, boolean>();

    /**
     * The total number of single operation transformations performed.
     */
    #totalOTCount = 0;

    /**
     * The total number of single operation transformations skipped.
     */
    #totalSkipCount = 0;

    // constructor ------------------------------------------------------------

    protected constructor() {
        super();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed JSON document operation is covered by the
     * specified operation alias name (see `registerAliasName()` for details).
     *
     * @param operation
     *  The JSON document operation to be checked.
     *
     * @param aliasName
     *  The alias name to be checked.
     *
     * @returns
     *  Whether the operation is covered by the specified alias name.
     */
    hasAliasName(operation: Operation, aliasName: string): boolean {

        // try to get resolved state from cache, otherwise recursively check registry (alias of alias)
        return map.upsert(this.#resolvedAliasNames, getOTKey(operation.name, aliasName), () => {

            // returns whether an operation name is covered by an alias name (recursion helper)
            const checkAliasName = (opName: string): boolean => {
                const nameSet = this.#rawAliasNames.get(opName);
                return !!nameSet && (nameSet.has(aliasName) || itr.some(nameSet, otherAlias => checkAliasName(otherAlias)));
            };

            return checkAliasName(operation.name);
        });
    }

    /**
     * Returns whether this instance contains a transformation handler for the
     * operations with the specified exact names, without considering aliases.
     *
     * @param lclName
     *  The name of the local document operation.
     *
     * @param extName
     *  The name of the external document operation.
     *
     * @returns
     *  Whether this instance contains a transformation handler for the
     *  specified operations.
     */
    hasRawHandler(lclName: string, extName: string): boolean {
        return this.#rawHandlers.has(getOTKey(lclName, extName));
    }

    /**
     * Transforms an external JSON document operation with all JSON operations
     * contained in the passed operation actions.
     *
     * @param extOp
     *  The external JSON document operation to be transformed with the passed
     *  actions. The operation may be modified in-place.
     *
     * @param lclActions
     *  The local operation actions with all document operations to be used to
     *  transform the passed external operation.
     *
     * @returns
     *  The transformed JSON operations. The result array may contain multiple
     *  JSON operations, or may be empty, according to the transformations
     *  performed.
     */
    transformOperation(extOp: Operation, lclActions: OperationAction[]): Operation[] {

        // detailed logging for all processed operations
        dumpTransformationData("$badge{OTEngine} transformOperation: before transformation", [extOp], lclActions);

        // transform the operation
        const resultOps = otLogger.takeTime("$badge{OTEngine} transformOperation", () => {
            return this.#transformOperations([extOp], lclActions, false, undefined);
        }, LogLevel.INFO);

        // detailed logging for all processed operations
        dumpTransformationData("$badge{OTEngine} transformOperation: after transformation", resultOps, lclActions);
        return resultOps;
    }

    /**
     * Transforms multiple external JSON document operations with all JSON
     * operations contained in the passed operation actions.
     *
     * @param extOps
     *  The external JSON document operations to be transformed with the passed
     *  actions. The operations will be modified in-place, but the array itself
     *  remains unmodified (a new array with the modified operations, and
     *  possibly with new operations, will be returned from this method).
     *
     * @param lclActions
     *  The local operation actions with all document operations to be used to
     *  transform the passed external operation.
     *
     * @returns
     *  The transformed JSON operations.
     */
    transformOperations(extOps: Operation[], lclActions: OperationAction[], options?: TransformOpsOptions): Operation[] {

        // unpack optional data
        const swapped = !!options?.swapped;
        const userData = options?.userData;

        // detailed logging for all processed operations
        dumpTransformationData(`$badge{OTEngine} transformOperations: swapped=${swapped}, before transformation`, extOps, lclActions, userData);

        // transform the operations
        extOps = otLogger.takeTime("$badge{OTEngine} transformOperations", () => {

            // regular mode: process all external operations individually, collect transformed operations
            // in a result array (each external operation may result in an array of transformed operations)
            if (!swapped) {
                return this.#transformOperations(extOps, lclActions, false, userData);
            }

            // swapped mode: pass external operations as local action, and local actions as external operations
            for (const lclAction of lclActions) {
                // transform in swapped order; `extOps` will be modified in-place
                lclAction.operations = this.#transformOperations(lclAction.operations, [{ operations: extOps }], true, userData);
                // early exit, if `extOps` drains
                if (!extOps.length) { break; }
            }

            // `extOps` have been modified in-place
            return extOps;

        }, LogLevel.INFO);

        // detailed logging for all processed operations
        dumpTransformationData(`$badge{OTEngine} transformOperations: swapped=${swapped}, after transformation`, extOps, lclActions, userData);
        return extOps;
    }

    /**
     * Returns statistics about the total number of operation transformations
     * performed and skipped by this instance.
     *
     * @returns
     *  A descriptor with statistical data.
     */
    getStatistics(): { otCount: number; skipCount: number } {
        return { otCount: this.#totalOTCount, skipCount: this.#totalSkipCount };
    }

    // protected methods ------------------------------------------------------

    /**
     * Logs a warning message.
     *
     * Intended to be used from implementations of operation transformations in
     * situations where it is known that the transformation is incomplete but
     * does not cause to break the transformation process.
     *
     * @param message
     *  The message text for the warning.
     */
    protected warn(message: string): void {
        const error = new OTError(message, this.lclOp!, this.extOp!);
        otLogger.warn(() => `$badge{OTEngine} ${error.message}`);
        this.trigger("warning", message);
        // TODO: What else can be done? For example, collect warnings and show something in the UI.
    }

    /**
     * Logs a warning message, if the passed condition is truthy.
     *
     * Intended to be used from implementations of operation transformations in
     * situations where it is known that the transformation is incomplete but
     * does not cause to break the transformation process.
     *
     * @param condition
     *  If truthy, a warning message will be logged.
     *
     * @param message
     *  The message text for the warning.
     */
    protected warnIf(condition: unknown, message: string): void {
        if (condition) { this.warn(message); }
    }

    /**
     * Throws an `OTError` exception.
     *
     * Intended to be used from implementations of operation transformations if
     * the transformation cannot be performed, and the entire transformation
     * process needs to be aborted.
     *
     * @param message
     *  The message text inserted into the `OTError` exception.
     *
     * @throws
     *  This method always throws an `OTError` exception.
     */
    protected error(message: string, options?: EnsureOptions): never {
        const error = new OTError(message, this.lclOp!, this.extOp!, !!options?.forceReload);
        // OT errors are expected, log a warning only
        otLogger.warn(() => `$badge{OTEngine} ${error.message}`);
        this.trigger("error", error.message);
        throw error;
    }

    /**
     * Throws an `OTError` exception, if the passed condition is falsy.
     *
     * Intended to be used from implementations of operation transformations if
     * the transformation cannot be performed, and the entire transformation
     * process needs to be aborted.
     *
     * @param condition
     *  If falsy, an `OTError` exception will be thrown with the passed message
     *  text.
     *
     * @param message
     *  The message text inserted into the `OTError` exception thrown in case
     *  the condition is falsy.
     *
     * @throws
     *  An `OTError` with the passed message text, if the passed condition is
     *  falsy.
     */
    protected ensure(condition: unknown, message: string, options?: EnsureOptions): asserts condition {
        if (!condition) { this.error(message, options); }
    }

    /**
     * Registers a global preparation callback function. All handlers will
     * be executed once before starting to transform the local document
     * operations against all existing external operations.
     *
     * @param handlerFn
     *  The callback function to be registered.
     */
    protected registerGlobalPrepareHandler(handlerFn: GlobalHandlerFn<this>): void {
        this.#globalPrepareHandlers.push(handlerFn);
    }

    /**
     * Registers a global finalization callback function. All handlers will be
     * executed once after all local document operations have been transformed
     * against all existing external operations.
     *
     * @param handlerFn
     *  The callback function to be registered.
     */
    protected registerGlobalFinalizeHandler(handlerFn: GlobalHandlerFn<this>): void {
        this.#globalFinalizeHandlers.push(handlerFn);
    }

    /**
     * Registers a preparation callback function for local operation actions.
     * All handlers will be executed once before starting to transform the next
     * local document action against an external operation.
     *
     * @param handlerFn
     *  The callback function to be registered.
     */
    protected registerActionPrepareHandler(handlerFn: ActionHandlerFn<this>): void {
        this.#actionPrepareHandlers.push(handlerFn);
    }

    /**
     * Registers a finalization callback function for local operation actions.
     * All handlers will be executed once after a local document action has
     * been transformed against an external operation.
     *
     * @param handlerFn
     *  The callback function to be registered.
     */
    protected registerActionFinalizeHandler(handlerFn: ActionHandlerFn<this>): void {
        this.#actionFinalizeHandlers.push(handlerFn);
    }

    /**
     * Registers a callback function for preparing operation transformations.
     * All handlers will be executed for every pair of JSON document operations
     * to be transformed, before resolving and executing the transformation
     * handler.
     *
     * @param handlerFn
     *  The callback function to be registered.
     */
    protected registerTransformPrepareHandler(handlerFn: TransformPrepareFn<this>): void {
        this.#transformPrepareHandlers.push(handlerFn);
    }

    /**
     * Registers a callback function for finalizing operation transformations.
     * All handlers will be executed after resolving and executing the
     * transformation handler.
     *
     * @param handlerFn
     *  The callback function to be registered.
     */
    protected registerTransformFinalizeHandler(handlerFn: TransformFinalizeFn<this>): void {
        this.#transformFinalizeHandlers.push(handlerFn);
    }

    /**
     * Registers a single transformation handler for the specified combination
     * of a local and an external operation.
     *
     * Alias operation names (registered with the method `registerAliasName()`)
     * can be used for either operation name.
     *
     * Transformation handlers bound to explicit operation names will be
     * preferred over handlers bound to an alias names on one side, and these
     * will be preferred over handlers bound to alias names on both sides.
     *
     * @param lclName
     *  The name of the local operation handled by the transformation handler.
     *  Can be an alias name.
     *
     * @param extName
     *  The name of the external operation handled by the transformation
     *  handler. Can be an alias name.
     *
     * @param handlerSpec
     *  The operation transformation handler to be registered for the specified
     *  operations, or a message for an `OTError` to be thrown unconditionally,
     *  or the value `null` to do nothing for this combination.
     */
    protected registerTransformation<LclOpT extends Operation, ExtOpT extends Operation>(
        lclName: string,
        extName: string,
        handlerSpec: TransformOpsSpec<this, LclOpT, ExtOpT>
    ): void {

        // do not allow to register empty strings
        if (!lclName || !extName) {
            throw new Error("invalid registration of empty operation names");
        }

        // the unique key of the transformation between both operations
        const otKey = getOTKey(lclName, extName);

        // prevent double registration (to prevent typos in registration code)
        if (this.#rawHandlers.has(otKey)) {
            throw new Error(`transformation handler for "${lclName}" and "${extName}" already registered`);
        }

        // register the transformation handler
        const handlerFn = is.string(handlerSpec) ? this.error.bind(this, handlerSpec) : (handlerSpec ?? NO_OP_HANDLER);
        this.#rawHandlers.set(otKey, handlerFn as GenericTransformOpsFn);
    }

    /**
     * Registers a transformation handler for the specified operation that will
     * transform itself.
     *
     * @param opName
     *  The name of the operation handled by the transformation handler. Can be
     *  an alias name.
     *
     * @param handlerSpec
     *  The operation transformation handler to be registered for the specified
     *  operation, or a message for an `OTError` to be thrown unconditionally,
     *  or the value `null` to do nothing for this combination.
     */
    protected registerSelfTransformation<OpT extends Operation>(
        opName: string,
        handlerSpec: TransformOpsSpec<this, OpT, OpT>
    ): void {
        this.registerTransformation(opName, opName, handlerSpec);
    }

    /**
     * Registers transformation handlers for the specified combination of two
     * operations. Either operation can be a local or an external operation.
     *
     * @param opName1
     *  The name of the first operation handled by the transformation handler.
     *  Can be an alias name.
     *
     * @param opName2
     *  The name of the second operation handled by the transformation handler.
     *  Can be an alias name.
     *
     * @param handlerSpec
     *  The operation transformation handler to be registered for the specified
     *  operations, or a message for an `OTError` to be thrown unconditionally,
     *  or the value `null` to do nothing for this combination. The result
     *  object returned from the handler must contain the new operations to be
     *  inserted in order of the parameters: `localOpsBefore` and
     *  `localOpsAfter` are related to the first operation passed into the
     *  handler (regardless if this operation is the local or the external
     *  operation); and `externalOpsBefore` and `externalOpsAfter` are related
     *  to the second operation passed into the handler.
     */
    protected registerBidiTransformation<Op1T extends Operation, Op2T extends Operation>(
        opName1: string,
        opName2: string,
        handlerSpec: TransformOpsSpec<this, Op1T, Op2T>
    ): void {

        // operations in order: first operation is local, second operation is external
        this.registerTransformation(opName1, opName2, handlerSpec);

        // swapped operations: first operation is external, second operation is local
        this.registerTransformation<Op2T, Op1T>(opName2, opName1, is.function(handlerSpec) ? (op2, op1, swapped) => {
            const result = handlerSpec.call(this, op1, op2, !swapped);
            return result ? swapOpTransformResult(result) : undefined;
        } : handlerSpec);
    }

    /**
     * Registers multiple transformation handlers at once.
     *
     * @param handlers
     *  A two-dimensional map with operation transformation handlers. The outer
     *  map keys contain the names of local opereations, the inner keys contain
     *  the names of external operations.
     */
    protected registerTransformations(handlers: Dict<Dict<GenericTransformOpsSpec>>): void {
        dict.forEach(handlers, (handlerMap, lclName) => {
            dict.forEach(handlerMap, (handler, extName) => {
                this.registerTransformation(lclName, extName, handler);
            });
        });
    }

    /**
     * Registers an alias name for one or more operation names. The alias name
     * can be used in transformation maps to address all aliased operations at
     * once.
     *
     * @param aliasName
     *  The alias name to be set for the passed operation names. The name MUST
     *  start with a Dollar sign to be sure it cannot collide with existing or
     *  future operation names.
     *
     * @param opNameArgs
     *  The names of all operations to be aliased. Either parameter can be a
     *  single string, or a string array.
     */
    protected registerAliasName(aliasName: string, ...opNameArgs: OpNameArgs): void {
        forEachOpName(opNameArgs, opName => {
            const nameSet = map.upsert(this.#rawAliasNames, opName, () => new Set());
            if (nameSet.has(aliasName)) {
                throw new Error(`alias name "${aliasName}" for operation "${opName}" already registered`);
            }
            nameSet.add(aliasName);
        });
    }

    /**
     * Registers the names of all operations that will be skipped silently,
     * regardless if there are matching transformations registered.
     *
     * @param opNameArgs
     *  The names of all operations that will be skipped if no transformation
     *  handler has been registered. Either parameter can be a single string,
     *  or a string array.
     */
    protected registerIgnoreNames(...opNameArgs: OpNameArgs): void {
        forEachOpName(opNameArgs, opName => {
            if (this.#ignoreNames.has(opName)) {
                throw new Error(`operation "${opName}" already ignored`);
            }
            this.#ignoreNames.add(opName);
        });
    }

    /**
     * Removes everything for the specified operations from the internal maps
     * (transformation handlers, alias names, and ignored operations).
     *
     * @param opNameArgs
     *  The names of all operations to be unregistered. Either parameter can be
     *  a single string, or a string array.
     */
    protected unregisterOperationNames(...opNameArgs: OpNameArgs): void {
        forEachOpName(opNameArgs, opName => {
            for (const otKey of this.#rawHandlers.keys()) {
                const opNames = parseOTKey(otKey);
                if ((opNames.lclName === opName) || (opNames.extName === opName)) {
                    this.#rawHandlers.delete(otKey);
                }
            }
            this.#rawAliasNames.delete(opName);
            this.#ignoreNames.delete(opName);
        });
    }

    // private methods --------------------------------------------------------

    /**
     * Returns all alias names for the passed operation name.
     */
    #getAliasNames(opName: string): string[] {
        const nameSet = this.#rawAliasNames.get(opName);
        if (!nameSet) { return []; }
        const aliasNames = Array.from(nameSet);
        nameSet.forEach(aliasName => aliasNames.push(...this.#getAliasNames(aliasName)));
        return aliasNames;
    }

    /**
     * Picks a transformation handler from the map, and checks for unqiueness.
     * Intended to be used in `reduce` loops.
     */
    #pickHandler(prevHandler: OptTransformOpsFn, lclName: string, extName: string): OptTransformOpsFn {
        const nextHandler = this.#rawHandlers.get(getOTKey(lclName, extName));
        if (prevHandler && nextHandler && (prevHandler !== nextHandler)) {
            throw new Error(`ambiguous transformations found for local "${lclName}" and external "${extName}"`);
        }
        return prevHandler ?? nextHandler;
    }

    /**
     * Returns the transformation handler callback function that has been
     * registered for the specified transformation key.
     */
    #findHandler(otKey: string): OptTransformOpsFn {

        // try to lookup transformation handler from resolved cache (without alias handling)
        return map.upsert(this.#resolvedHandlers, otKey, () => {

            // prefer the transformation handler function for both real operation names
            const expHandler = this.#rawHandlers.get(otKey);
            if (expHandler) { return expHandler; }

            // parse the passed transformation key
            const { lclName, extName } = parseOTKey(otKey);
            // the alias names for the local operation
            const lclAliasNames = this.#getAliasNames(lclName);
            // the alias names for the external operation
            const extAliasNames = this.#getAliasNames(extName);

            // resolve a transformation handler for all local alias names with real external operation name
            // (throws an exception for any ambiguity, i.e. different handlers for multiple aliases)
            let aliasHandler = lclAliasNames.reduce<OptTransformOpsFn>((prevHandler, lclAliasName) => {
                return this.#pickHandler(prevHandler, lclAliasName, extName);
            }, undefined);

            // resolve a transformation handler for real local operation name and all external alias names
            // (throws an exception for any ambiguity, i.e. different handlers for multiple aliases)
            aliasHandler = extAliasNames.reduce((prevHandler, extAliasName) => {
                return this.#pickHandler(prevHandler, lclName, extAliasName);
            }, aliasHandler);

            // no handler found for single alias name: try alias names on both sides
            if (!aliasHandler) {
                aliasHandler = lclAliasNames.reduce<OptTransformOpsFn>((prevHandler1, lclAliasName) => {
                    return extAliasNames.reduce((prevHandler2, extAliasName) => {
                        return this.#pickHandler(prevHandler2, lclAliasName, extAliasName);
                    }, prevHandler1);
                }, undefined);
            }

            // insert existing transformation handler into cache
            if (aliasHandler) { return aliasHandler; }

            // check the ignore list for any of the operations (insert `undefined` into the cache)
            if (this.#ignoreNames.has(lclName) || this.#ignoreNames.has(extName)) {
                return undefined;
            }

            // otherwise fail (at least one transformation handler must exist)
            throw new Error(`no transformation found for "${lclName}" and "${extName}"`);
        });
    }

    /**
     * Transforms an external JSON document operation with all JSON operations
     * contained in the passed operation actions.
     */
    #transformOperation(extOperation: Operation, lclActions: OperationAction[], swapped: boolean, userData: Opt<object>): Operation[] {

        // the resulting transformed operations (may receive additional operations)
        const extOps = [extOperation];
        // current array index in `extOps` (used in following helper functions)
        let extIdx: number;
        // whether tracing level is enabled
        const trace = otLogger.isLogLevelActive(LogLevel.TRACE);

        // removes the current operation from `extOps` if it is marked with the "removed" flag
        function removeExtOperation(): void {
            if (isOperationRemoved(extOps[extIdx])) {
                extOps.splice(extIdx, 1);
                extIdx -= 1;
            }
        }

        // inserts new operations returned from transformation handler into `extOps`
        function insertExtOperations(newExtOps: Opt<Operation | Operation[]>, offset: number): void {
            if (is.array(newExtOps)) {
                extOps.splice(extIdx + offset, 0, ...newExtOps);
                extIdx += newExtOps.length;
            } else if (newExtOps) {
                extOps.splice(extIdx + offset, 0, newExtOps);
                extIdx += 1;
            }
        }

        // process the actions passed to this method (allow to exit loop early,
        // e.g. when all external operations have been removed)
        for (const lclAction of lclActions) {

            // invoke the action preparation handlers
            const lclUserData = lclAction.userData ?? userData;
            this.#actionPrepareHandlers.forEach(handlerFn => handlerFn.call(this, extOps, lclUserData));

            // the local operations to be processed
            const lclOps = lclAction.operations;

            // process all current local operations
            for (let lclIdx = 0; lclIdx < lclOps.length; lclIdx += 1) {

                // the local operation to be transformed
                const lclOp = this.lclOp = lclOps[lclIdx];

                // collect new local operations to be added before the current local operation
                const newLclOpsBefore: Operation[] = [];
                // collect new local operations to be added after the current local operation
                const newLclOpsAfter: Operation[] = [];

                // process all current external operations
                for (extIdx = 0; extIdx < extOps.length; extIdx += 1) {

                    // the external operation to be transformed
                    const extOp = this.extOp = extOps[extIdx];
                    // the unique key of the transformation between both operations
                    const otKey = getOTKey(lclOp.name, extOp.name);

                    otLogger.log(() => {
                        let msg = `transforming for OT key "${otKey}"...`;
                        if (trace) { msg += `\n lcl: ${dbgFormatOp(lclOp)}\n ext: ${dbgFormatOp(extOp)}`; }
                        return msg;
                    });

                    // run all registered preparation handlers
                    const skipTransform = this.#transformPrepareHandlers.some(prepareFn => {
                        const result = prepareFn.call(this, lclOp, extOp, swapped, lclUserData);
                        // early exit if handler returns `IGNORE`, or if any of the operations has been marked as removed
                        return (result === TransformPrepareResult.IGNORE) || isOperationRemoved(lclOp) || isOperationRemoved(extOp);
                    });

                    // skip the transformation entirely if specified by the pre-processors
                    if (skipTransform) {
                        this.#totalSkipCount += 1;
                        // immediately remove external operation marked as "removed" from the array
                        removeExtOperation();
                        // stop processing external operations if the local operation has been marked as "removed"
                        if (isOperationRemoved(lclOp)) { break; }
                        continue;
                    }

                    // lookup transformation handler
                    const handlerFn = this.#findHandler(otKey);

                    // skip operations without transformation handler (registered in the ignore list)
                    if (!handlerFn) {
                        this.#totalSkipCount += 1;
                        continue;
                    }

                    // invoke the transformation handler
                    this.#totalOTCount += 1;
                    const result = handlerFn.call(this, lclOp, extOp, swapped);

                    // insert new external operations before the transformed operation
                    if (result) { insertExtOperations(result.externalOpsBefore, 0); }

                    // immediately remove external operation marked as "removed" from the array
                    removeExtOperation();

                    // insert new external operations after the transformed operation
                    if (result) { insertExtOperations(result.externalOpsAfter, 1); }

                    // collect new local operations
                    if (result) {
                        newLclOpsBefore.push(...ary.wrap(result.localOpsBefore));
                        newLclOpsAfter.push(...ary.wrap(result.localOpsAfter));
                    }

                    // run registered finalization handlers for resulting local operations
                    if (this.#transformFinalizeHandlers.length) {
                        const optLclOp = isOperationRemoved(lclOp) ? undefined : lclOp;
                        const optExtOp = isOperationRemoved(extOp) ? undefined : extOp;
                        this.#transformFinalizeHandlers.forEach(finalizeFn => finalizeFn.call(this, optLclOp, optExtOp, swapped, lclUserData));
                    }

                    // early exit if the local operation has been marked as "removed" by the handler
                    if (isOperationRemoved(lclOp)) { break; }
                }

                // insert new local operations before the transformed operation
                if (newLclOpsBefore.length > 0) {
                    lclOps.splice(lclIdx, 0, ...newLclOpsBefore);
                    lclIdx += newLclOpsBefore.length;
                }

                // DOCS-1833: remove the local operation from the array, if it has been marked
                if (isOperationRemoved(lclOp)) {
                    lclOps.splice(lclIdx, 1);
                    lclIdx -= 1;
                }

                // insert new local operations after the transformed operation
                if (newLclOpsAfter.length > 0) {
                    lclOps.splice(lclIdx + 1, 0, ...newLclOpsAfter);
                    lclIdx += newLclOpsAfter.length;
                }

                // early exit if the array of external operations drains
                if (extOps.length === 0) { break; }
            }

            // invoke the action finalization handlers
            this.#actionFinalizeHandlers.forEach(handlerFn => handlerFn.call(this, extOps, lclUserData));

            // early exit if the array of external operations drains
            if (!extOps.length) { break; }
        }

        return extOps;
    }

    /**
     * Transforms multiple external JSON document operations with all JSON
     * operations contained in the passed operation actions.
     */
    #transformOperations(extOps: Operation[], lclActions: OperationAction[], swapped: boolean, userData: Opt<object>): Operation[] {

        // invoke all global preparation handlers
        this.#globalPrepareHandlers.forEach(handlerFn => handlerFn.call(this));

        // transform each external operation against all local operations
        extOps = extOps.reduce<Operation[]>((allOps, extOp) => allOps.concat(this.#transformOperation(extOp, lclActions, swapped, userData)), []);

        // invoke all global finalization handlers
        this.#globalFinalizeHandlers.forEach(handlerFn => handlerFn.call(this));

        return extOps;
    }
}
