/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, json } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ERROR_WHILE_MODIFYING_DOCUMENT } from "@/io.ox/office/baseframework/utils/clienterror";
import { GENERAL } from "@/io.ox/office/baseframework/utils/errorcontext";

import type { Operation, ChangeConfigOperation } from "@/io.ox/office/editframework/utils/operations";
import { CHANGE_CONFIG } from "@/io.ox/office/editframework/utils/operations";
import type { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

/**
 * All types that can be resolved to an array of JSON document operations.
 */
export type OperationSource = readonly Operation[] | Operation | OperationGenerator;

/**
 * Configuration options for for the class `OperationGenerator`.
 */
export interface GeneratorConfig {

    /**
     * Additional user data for OT to be stored with the document operations.
     */
    docUserData?: object;

    /**
     * Additional user data for OT to be stored with the undo operations.
     */
    undoUserData?: object;

    /**
     * If set to `true`, all document operations that will be generated and
     * inserted into the generator instance will be applied immediately at the
     * document model.
     *
     * In this case, it is NOT allowed to insert operations at the beginning of
     * the internal array of document operation (the option `prepend` MUST NOT
     * be used except for undo operations). It MUST be possible to apply the
     * operations without error. If the operation handler throws an exception,
     * the entire application will immediately switch to internal error state.
     *
     * Default value is `false`.
     */
    applyImmediately?: boolean;
}

/**
 * Optional parameters for various methods of the class `OperationGenerator`.
 */
export interface GeneratorUndoOptions {

    /**
     * If set to `true`, the undo operations array will be used for the action.
     * Default value is `false`.
     */
    undo?: boolean;
}

/**
 * Optional parameters for the method `OperationGenerator.insertOperations()`.
 */
export interface GeneratorInsertOptions extends GeneratorUndoOptions {

    /**
     * If set to `true`, the operations will be prepended, otherwise appended
     * to the operations array. Default value is `false`.
     */
    prepend?: boolean;
}

/**
 * Optional parameters for various methods of the class `OperationGenerator`
 * that generate new operations.
 */
export interface GeneratorOptions extends GeneratorInsertOptions {

    /**
     * If set to `true`, an operation for export filters will be created by
     * inserting the property `scope` set to the value "filter" (remote editor
     * clients will ignore the operations). Default value is `false`.
     */
    filter?: boolean;
}

/**
 * Optional parameters for the method `OperationGenerator.insertSubGenerator()`.
 */
export interface InsertSubGeneratorOptions {

    /**
     * If set to `true`, the undo operations array will be prepended to the
     * parent generator. Default value is `false`.
     */
    prependUndo?: boolean;
}

// public functions ===========================================================

/**
 * Converts the passed document operations to a JSON array.
 *
 * @param operations
 *  A single JSON operation, or an array of JSON operations, or an instance of
 *  `OperationsGenerator` with operations.
 *
 * @param [options]
 *  Optional parameters. The option `undo` specifies whether to return the undo
 *  operations from an operations generator instance contained in the parameter
 *  `operations`.
 *
 * @returns
 *  The document operations, as JSON array.
 */
export function resolveOperations(operations: OperationSource, options?: GeneratorUndoOptions): readonly Operation[] {
    return (operations instanceof OperationGenerator) ? operations.getOperations(options) :
        is.readonlyArray(operations) ? operations : [operations];
}

// class OperationGenerator ===================================================

/**
 * An instance of this class contains an array of JSON document operations, and
 * an array of associated undo operations, and provides methods to generate new
 * operations for either of the operation arrays.
 *
 * @param docModel
 *  The document model containing this generator.
 *
 * @param [config]
 *  Initial configuration.
 */
export class OperationGenerator<DocModelT extends EditModel = EditModel> {

    // properties -------------------------------------------------------------

    /**
     * The cached document model to be used in subclasses.
     */
    protected readonly docModel: DocModelT;

    /**
     * The buffer for regular document operations.
     */
    #docOperations: Operation[] = [];

    /**
     * The buffer for undo operations.
     */
    #undoOperations: Operation[] = [];

    /**
     * Additional user data for OT to be stored with the document operations.
     */
    #docUserData: Opt<object>;

    /**
     * Additional user data for OT to be stored with the undo operations.
     */
    #undoUserData: Opt<object>;

    /**
     * Whether to apply generated/inserted operations immediately.
     */
    readonly #applyImmediately: boolean;

    // constructor ------------------------------------------------------------

    constructor(docModel: DocModelT, config?: GeneratorConfig) {
        this.docModel = docModel;
        this.#docUserData = config?.docUserData;
        this.#undoUserData = config?.undoUserData;
        this.#applyImmediately = !!config?.applyImmediately;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the generated document operations will be applied
     * immediately at the document model (see constructor option
     * `applyImmediately` for details).
     *
     * @returns
     *  Whether the generated document operations will be applied immediately.
     */
    isImmediateApplyMode(): boolean {
        return this.#applyImmediately;
    }

    /**
     * Returns the number of document operations that have been generated so
     * far.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The number of document operations that have been generated so far.
     */
    getOperationCount(options?: GeneratorUndoOptions): number {
        return this.#get(options).length;
    }

    /**
     * Returns the array with all document operations that have been generated
     * so far. Note that this method does not remove the document operations
     * from this generator instance.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The array with all generated document operations. For performance
     *  reasons, this array is a reference to the internal storage, and MUST
     *  NOT be changed.
     */
    getOperations(options?: GeneratorUndoOptions): readonly Operation[] {
        return this.#get(options);
    }

    /**
     * Reverses the entire operations array in-place. This method MUST NOT be
     * used to reverse regular document operations, if they have been applied
     * immediately (see constructor option `applyImmediately` for details).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    reverseOperations(options?: GeneratorUndoOptions): this {

        // regular operations must not be reversed when they have been applied immediately
        this.#ensureNotImmediate(options, "cannot reverse operations that have been applied immediately");

        // reverse the specified operations array
        this.#get(options).reverse();
        return this;
    }

    /**
     * Removes the JSON operations from the operations array.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    clearOperations(options?: GeneratorUndoOptions): this {

        // regular operations must not be cleared when they have been applied immediately
        this.#ensureNotImmediate(options, "cannot clear operations that have been applied immediately");

        // clear the specified operations array
        this.#get(options).length = 0;
        return this;
    }

    /**
     * Appends the passed JSON operations to the operations array.
     *
     * @param operations
     *  A single JSON operation, or an array of JSON operations, or an instance
     *  of this class with operations to be appended to the operations array.
     *  Note that an operations generator passed to this method will NOT be
     *  cleared after its operations have been appended.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    appendOperations(operations: OperationSource, options?: GeneratorUndoOptions): this {

        // the own operations, and the new operations, as JS arrays
        const ownOperations = this.#get(options);
        const newOperations = resolveOperations(operations, options);

        // append the new operations (do not create new array instance for a single operation)
        if (newOperations.length === 1) {
            ownOperations.push(newOperations[0]);
        } else if (newOperations.length > 1) {
            this.#set(ownOperations.concat(newOperations), options);
        }

        // apply the new operations immediately if specified (but skip other operation generators
        // that are in immediate mode by themselves)
        if (this.#applyImmediately && !options?.undo && !((operations instanceof OperationGenerator) && operations.isImmediateApplyMode())) {
            try {
                this.docModel.trigger("operations:immediate"); // workaround for DOCS-1652
                this.docModel.invokeOperationHandlers(operations);
            } catch (err) {
                this.#throw(err);
            }
        }

        return this;
    }

    /**
     * Prepends the passed JSON operations to the operations array. This method
     * MUST NOT be used to insert regular document operations, if they will be
     * applied immediately (see constructor option `applyImmediately` for more
     * details).
     *
     * @param operations
     *  A single JSON operation, or an array of JSON operations, or an instance
     *  of this class with operations to be prepended to the operations array.
     *  Note that an operations generator passed to this method will NOT be
     *  cleared after its operations have been prepended.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    prependOperations(operations: OperationSource, options?: GeneratorUndoOptions): this {

        // regular operations must not be prepended when they will be applied immediately
        this.#ensureNotImmediate(options, "cannot prepend operations, and apply them immediately");

        // the own operations, and the new operations, as JS arrays
        const ownOperations = this.#get(options);
        const newOperations = resolveOperations(operations, options);

        // prepend the new operations (do not create new array instance for a single operation)
        if (newOperations.length === 1) {
            ownOperations.unshift(newOperations[0]);
        } else if (newOperations.length > 1) {
            this.#set(newOperations.concat(ownOperations), options);
        }

        return this;
    }

    /**
     * Appends or prepends the passed JSON operations to the operations array.
     * This method MUST NOT be used to insert regular document operations at
     * the beginning, if they will be applied immediately (see constructor
     * option `applyImmediately` for more details).
     *
     * @param operations
     *  A single JSON operation, or an array of JSON operations, or an instance
     *  of this class with operations to be inserted into the operations array.
     *  Note that an operations generator passed to this method will NOT be
     *  cleared after its operations have been inserted.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A reference to this instance.
     */
    insertOperations(operations: OperationSource, options?: GeneratorInsertOptions): this {
        return options?.prepend ?
            this.prependOperations(operations, options) :
            this.appendOperations(operations, options);
    }

    /**
     * Creates a new empty operation generator with the same settings as this
     * generator.
     *
     * @returns
     *  A new empty operation generator with the same settings.
     */
    createSubGenerator(): OperationGenerator {
        return new OperationGenerator(this.docModel, { applyImmediately: this.#applyImmediately });
    }

    /**
     * Inserts the operations of the passed generator into this generator.
     *
     * @param generator
     *  The generator with operations to be inserted into this generator.
     *
     * @param [options]
     *  Optional parameters.
     */
    insertSubGenerator(generator: OperationGenerator, options?: InsertSubGeneratorOptions): void {
        this.appendOperations(generator);
        this.insertOperations(generator, { undo: true, prepend: options?.prependUndo });
    }

    /**
     * Creates and appends a new operation to the operations array.
     *
     * @param name
     *  The name of the operation.
     *
     * @param [props]
     *  Additional properties that will be stored in the JSON operation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created JSON operation object.
     */
    generateOperation(name: string, props?: Dict, options?: GeneratorOptions): Operation {
        const operation: Operation = { name, ...json.deepClone(props) };
        if (options?.filter) { operation.scope = "filter"; }
        this.insertOperations(operation, options);
        return operation;
    }

    /**
     * Creates and appends a new operation to the operations array that changes
     * one or more global document properties.
     *
     * @param attributes
     *  The document attributes to be inserted into the operation, as simple
     *  key/value map.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The created operation object.
     */
    generateChangeConfigOperation(attributes: Dict, options?: GeneratorOptions): ChangeConfigOperation {
        return this.generateOperation(CHANGE_CONFIG, { attrs: { document: attributes } }, options) as ChangeConfigOperation;
    }

    /**
     * Returns the user data currently stored next to the document operations.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The user data stored next to the document operations.
     */
    getUserData(options?: GeneratorUndoOptions): Opt<object> {
        return options?.undo ? this.#undoUserData : this.#docUserData;
    }

    /**
     * Stores additional user data for OT next to the document operations.
     *
     * @param userData
     *  The user data to be stored in this generator.
     *
     * @param [options]
     *  Optional parameters.
     */
    setUserData(userData: object, options?: GeneratorUndoOptions): void {
        if (options?.undo) {
            this.#undoUserData = userData;
        } else {
            this.#docUserData = userData;
        }
    }

    // private methods --------------------------------------------------------

    #get(options?: GeneratorUndoOptions): Operation[] {
        return options?.undo ? this.#undoOperations : this.#docOperations;
    }

    #set(operations: Operation[], options?: GeneratorUndoOptions): void {
        if (options?.undo) {
            this.#undoOperations = operations;
        } else {
            this.#docOperations = operations;
        }
    }

    #throw(err: unknown): never {
        globalLogger.exception(err);
        this.docModel.docApp.setInternalError(ERROR_WHILE_MODIFYING_DOCUMENT, GENERAL);
        throw err;
    }

    #ensureNotImmediate(options: Opt<GeneratorUndoOptions>, msg: string): void {
        if (this.#applyImmediately && !options?.undo) {
            this.#throw(new Error(msg));
        }
    }
}
