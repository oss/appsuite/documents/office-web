/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ox from "$/ox";
// eslint-disable-next-line no-restricted-imports
import CryptoJS from "crypto-js";

import { is, fun, dict, pick, jpromise, debug } from "@/io.ox/office/tk/algorithms";
import { IOS_SAFARI_DEVICE } from "@/io.ox/office/tk/dom";
import { onceMethod } from "@/io.ox/office/tk/objects";
import { Logger, globalLogger } from "@/io.ox/office/tk/utils/logger";
import { BREAK, CHROME_ON_ANDROID, getFunctionOption } from "@/io.ox/office/tk/utils";

import { BaseModel } from "@/io.ox/office/baseframework/model/basemodel";
import { DEBUG } from "@/io.ox/office/editframework/utils/editconfig";
import * as Op from "@/io.ox/office/editframework/utils/operations";
import { handleMinifiedActions } from "@/io.ox/office/editframework/utils/operationutils";
import { OperationError } from "@/io.ox/office/editframework/utils/operationerror";
import { otLogger, isOperationRemoved } from "@/io.ox/office/editframework/utils/otutils";

import { AttributePool } from "@/io.ox/office/editframework/model/attributepool";
import { UndoManager } from "@/io.ox/office/editframework/model/undomanager";
import ModelAttributesMixin from "@/io.ox/office/editframework/model/modelattributesmixin";
import { OperationContext } from "@/io.ox/office/editframework/model/operation/context";
import { resolveOperations, OperationGenerator } from "@/io.ox/office/editframework/model/operation/generator";

// constants ==================================================================

// operations logger
const opLogger = new Logger("office:log-ops", { tag: "OP", tagColor: 0xFF00FF });

// class EditModel ============================================================

/**
 * The base class for editable document models. Adds generic support for
 * operations and undo action management.
 *
 * @mixes ModelAttributesMixin
 */
export class EditModel extends BaseModel {

    /**
     * @param {EditApplication} docApp
     *  The application instance containing this document model.
     *
     * @param {object} [config]
     *  Optional parameters that will be passed to the constructor of the undo
     *  manager (see class `UndoManager` for details). Additionally, the
     *  following options are supported:
     *  - {Function} [config.operationsFinalizer]
     *    A callback function that will be invoked before applying operations
     *    and sending them to the server. Receives an array of JSON operation
     *    objects, and MUST return an array of processed operations (the return
     *    value may be the same array modified in-place). Will be called in the
     *    context of this document model.
     *  - {Function} [config.storageOperationCollector]
     *    A function that can be used to collect the operations that are
     *    required to load a document from local storage without any additional
     *    operation. Will be called in the context of the model instance.
     *  - {String} [config.defaultStyleFamily]
     *    The default style attribute family used in style sheet operations,
     *    and auto-style operations if the "type" property is missing. If
     *    omitted, the "type" property cannot be omitted in these operations.
     *  - {boolean} [config.slideMode=false]
     *    Whether the model supports the slide mode (used in the Presentation
     *    application).
     */
    constructor(docApp, config) {

        // base constructor
        super(docApp);

        // public access to instance configuration
        this.config = { ...config };

        // create the default attribute pool
        this.defAttrPool = this.member(new AttributePool());

        // create the undo manager
        this.undoManager = this.member(new UndoManager(this, config));

        // deferred object for current asynchronous actions, will remain in
        // resolved/rejected state as long as no new actions will be applied
        this._actionsDef = null;

        // maps all operation names to operation handler functions
        this._operationHandlerMap = new Map/*<string, Function>*/();

        // whether operations are currently applied (prevent recursive calls)
        this._processingOperations = false;
        // whether internal operations are currently applied (prevent OT collisions)
        this._processingInternalOperations = false;
        // whether external operations are currently applied (prevent OT collisions)
        this._processingExternalOperations = false;

        // the total number of operations applied successfully
        this._totalOperations = 0;
        // a counter for the operations, that adds all single operations, taking care of merged
        // operations. So this number must be equal on every client, independent from merge
        // processes on client or server side.
        this._osn = -1;
        // the last document operation applied successfully
        this._lastOperation = null;

        // whether OT mode is enabled
        this._otEnabled = docApp.isOTEnabled();

        // flag to enable slide specific behavior
        this._slideMode = !!config?.slideMode;
        // flag to enable slide specific behavior in iOS and Android
        this._slideTouchMode = this._slideMode && (IOS_SAFARI_DEVICE || CHROME_ON_ANDROID);
        // a state indicating whether a drawing is currently changed (resized, moved, rotated, ...)
        this._isDrawingChangeActive = false;

        // the filter version sent with the fast load information
        this._fastLoadFilterVersion = null;

        // mix-ins (expecting a complete EditModel instance)
        ModelAttributesMixin.call(this, config);

        // forward changes of document attributes as "change:config" event
        this.defAttrPool.on("change:defaults", changedSet => {
            if (changedSet.document) {
                this.trigger("change:config", changedSet.document);
            }
        });

        // register operation handlers
        this.registerOperationHandler(Op.NOOP, fun.undef);
        this.registerOperationHandler(Op.CHANGE_CONFIG, context => this.applyChangeConfigOperation(context));
        this.registerOperationHandler(Op.INSERT_THEME, context => this.themeCollection.applyInsertThemeOperation(context));
        this.registerOperationHandler(Op.INSERT_FONT, context => this.fontCollection.applyInsertFontOperation(context));
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns readonly access to the global document attributes.
     */
    get globalConfig() {
        return this.defAttrPool.getDefaultValues("document");
    }

    // public methods ---------------------------------------------------------

    /**
     * Create an operation context (wrapper object for a specific JSON document
     * operation).
     *
     * This method can be overwritten in subclasses to create more specialized
     * context objects that will be passed to all operation handlers.
     *
     * @param {Operation} operation
     *  The JSON document operation to be wrapped by the context.
     *
     * @param {boolean} external
     *  Whether the operation has been generated locally (`false`), or has been
     *  received from the server (`true`).
     *
     * @param {boolean} importing
     *  Whether the document s currently being imported.
     */
    createOperationContext(operation, external, importing) {
        return new OperationContext(this, operation, external, importing);
    }

    /**
     * Registers a callback function that will be executed when a document
     * operation with the specified name will be applied.
     *
     * @param {string} name
     *  The name of the document operation, as contained in the property "name"
     *  of an operation JSON object, to be handled by the passed callback
     *  function.
     *
     * @param {Function} handler
     *  The callback function that will be invoked for every operation with the
     *  specified name. Receives the following parameters:
     *  1. {OperationContext} context
     *    A wrapper for the JSON operation object, representing the operation
     *    to be applied for this document.
     *
     *  Will be called in the context of this document model instance. If the
     *  operation handler throws an exception, further processing of other
     *  operations will be stopped immediately, and the event
     *  "operations:error" will be triggered. The return value of the callback
     *  function will be ignored.
     */
    registerOperationHandler(name, handler) {
        this._operationHandlerMap.set(name, handler);
    }

    /**
     * Registers a callback function that will be executed when a document
     * operation with the specified name will be applied.
     *
     * @deprecated
     *  Use method `registerOperationHandler` instead.
     *
     * @param {string} name
     *  The name of the document operation, as contained in the property "name"
     *  of an operation JSON object, to be handled by the passed callback
     *  function.
     *
     * @param {Function} handler
     *  The callback function that will be invoked for every operation with the
     *  specified name. Receives the following parameters:
     *  1. {Dict} operation
     *    The operation to be applied for this document, as JSON object.
     *  2. {boolean} external
     *    Whether the operations have been generated locally (`false`), or
     *    received from the server (`true`).
     *
     *  Will be called in the context of this document model instance. If the
     *  operation handler returns the Boolean value false, or throws an
     *  exception, further processing of other operations will be stopped
     *  immediately, and the event "operations:error" will be triggered. Every
     *  other return value (also undefined) indicates that the operation has
     *  been applied successfully.
     */
    registerPlainOperationHandler(name, handler) {
        this.registerOperationHandler(name, context => {
            // old operation handlers may return false for failed operation
            const result = handler.call(this, context.operation, context.external);
            context.ensure(result !== false, "operation handler failed");
        });
    }

    /**
     * Returns whether the "action processing mode" of this document model is
     * currently active (whether this model currently generates, applies, or
     * sends operation actions, either synchronously or asynchronously).
     *
     * @returns {boolean}
     *  Whether the "action processing mode" is currently active.
     */
    isProcessingActions() {
        return !!this._actionsDef && jpromise.isPending(this._actionsDef);
    }

    /**
     * Returns a promise representing the state of the "action processing
     * mode". If in pending state, this model currently generates, applies, or
     * sends operation actions (either synchronously or asynchronously). If in
     * resolved state, the model is ready to process new actions.
     *
     * @returns {JPromise}
     *  A promise representing the state of the "action processing mode".
     */
    waitForActionsProcessed() {
        // do not expose the failed state of the deferred object
        return this._actionsDef ? jpromise.fastThen(this._actionsDef, fun.undef, fun.undef) : this.createResolvedPromise();
    }

    /**
     * Returns whether operations are currently processed.
     *
     * @returns {boolean}
     *  Whether the model processes operations.
     */
    isProcessingOperations() {
        return this._processingOperations;
    }

    /**
     * Returns whether external operations are currently (asynchronously)
     * processed.
     */
    isProcessingExternalOperations() {
        return this._processingExternalOperations;
    }

    /**
     * Returns whether internal operations are currently (asynchronously)
     * processed.
     */
    isProcessingInternalOperations() {
        return this._processingInternalOperations;
    }

    /**
     * Sends the passed operations to the server without applying them locally.
     * In special situations, a client may choose to apply the operations
     * manually before they will be sent to the server. In this case, the
     * registered operation handlers must not be called.
     *
     * @param {OperationSource} operations
     *  A single JSON operation, or an array of JSON operations, or an
     *  operations generator instance with operations to be sent to the server.
     *  Note that an operations generator passed to this method will NOT be
     *  cleared after its operations have been sent.
     *
     * @param {SendOperationsOptions} [options]
     *  Optional parameters:
     *  - {boolean} [options.undo=false]
     *    If set to `true`, and the parameter "operations" is an instance of
     *    the class `OperationGenerator`, its undo operations will be sent to
     *    the server.
     *  - {object} [options.userData]
     *    Additional user data for OT to be inserted into the action.
     *
     * @returns {boolean}
     *  Whether registering all operations for sending was successful.
     */
    @opLogger.profileMethod("$badge{EditModel} sendOperations")
    sendOperations(operations, options) {
        try {
            this._applyOperationsSync(operations, false, { ...options, sendOnly: true });
            return true;
        } catch {
            return false;
        }
    }

    /**
     * Executes the handler functions for all passed operations in an
     * asynchronous background loop.
     *
     * @param {OperationSource} operations
     *  A single JSON operation, or an array of JSON operations, or an
     *  operations generator instance with operations to be applied at this
     *  document model. Note that an operations generator passed to this method
     *  will NOT be cleared after its operations have been applied.
     *
     * @param {SendOperationsOptions} [options]
     *  Optional parameters:
     *  - {boolean} [options.undo=false]
     *    If set to `true`, and the parameter "operations" is an instance of
     *    the class `OperationGenerator`, its undo operations will be sent to
     *    the server.
     *  - {object} [options.userData]
     *    Additional user data for OT to be inserted into the action.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after all operations have been applied
     *  successfully, or reject immediately with an `OperationError` after
     *  applying an operation has failed. The caller has to wait for the
     *  promise before new operations can be applied. The promise regularly
     *  sends progress updates while applying the operations.
     */
    @opLogger.profileMethod("$badge{EditModel} applyOperationsAsync")
    applyOperationsAsync(operations, options) {

        // apply the operations in action processing mode
        return this._enterProcessActionsMode(() => {
            return this._applyOperationsAsync(operations, false, options);
        });
    }

    /**
     * Executes the handler functions for all passed operations in a single
     * synchronous loop.
     *
     * @deprecated
     *  Danger of locked browser for large number of operation, no error
     *  propagation. Use `applyOperationsAsync` instead.
     *
     * @param {OperationSource} operations
     *  A single JSON operation, or an array of JSON operations, or an
     *  operations generator instance with operations to be applied at this
     *  document model. Note that an operations generator passed to this method
     *  will NOT be cleared after its operations have been applied.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.undo=false]
     *    If set to `true`, and the parameter "operations" is an instance of
     *    the class `OperationGenerator`, its undo operations will be applied.
     *
     * @returns {boolean}
     *  Whether applying all operations was successful.
     */
    @opLogger.profileMethod("$badge{EditModel} applyOperations")
    applyOperations(operations, options) {

        // apply the operations in action processing mode
        const promise = this._enterProcessActionsMode(() => {
            // will throw `OperationError` on any failure
            this._applyOperationsSync(operations, false, options);
        });

        // callback has been invoked immediately, return the result
        return jpromise.isFulfilled(promise);
    }

    /**
     * Executes the handler functions for the passed operations received from
     * the server (document import, or remote editors).
     *
     * @param {Operation[]} operations
     *  The document operations to be applied at this document model.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.expand=false]
     *    Whether the passed document operations are minified and need to be
     *    expanded.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after all operations have been applied
     *  successfully, or reject immediately with an `OperationError` after
     *  applying an operation has failed. The caller has to wait for the
     *  promise before new operations can be applied. The promise regularly
     *  sends progress updates while applying the operations.
     */
    @opLogger.profileMethod("$badge{EditModel} applyExternalOperations")
    applyExternalOperations(operations, options) {

        // no operations: early exit
        if (operations.length === 0) { return jpromise.resolve(); }

        // DOCS-892: decompress property keys and values in all operations
        if (options?.expand) {
            handleMinifiedActions([{ operations }], true);
        }

        // apply the operations in action processing mode
        return this._enterProcessActionsMode(() => {
            return this._applyOperationsAsync(operations, true);
        });
    }

    /**
     * Executes the operation handler callback functions for the passed
     * document operations locally, silently, and synchronously. No events will
     * be generated at all (the operations will not be sent to the server, the
     * internal OSN will not be changed, and they will not be processed in any
     * other way).
     *
     * @param {OperationSource} operations
     *  A single JSON operation, or an array of JSON operations, or an
     *  operations generator instance with operations to be applied locally and
     *  silently.
     *
     * @throws {OperationError}
     *  An `OperationError`, if one of the passed operations is invalid (wrong
     *  or missing name etc.), or the exception thrown by the operation handler
     *  implementation. Will contain the additional property `index` containing
     *  the array index of the failed operation.
     */
    invokeOperationHandlers(operations) {

        // suppress implicit undo operations generated e.g. in text framework
        this.undoManager.disableUndoSync(() => {

            // convert input parameter to a plain array of JSON operations
            resolveOperations(operations).forEach((operation, index) => {

                try {

                    // the operation context passed to the callback
                    const context = this.createOperationContext(operation, false, false);
                    // invoke the callback handler for the operation
                    this._getOperationHandler(context).call(this, context);

                } catch (err) {

                    // add the array index of the failed operation and rethrow
                    err.index = index;
                    throw err;
                }
            });
        });
    }

    /**
     * Creates and returns an initialized operations generator instance (an
     * instance of `OperationGenerator`). Intended to be overwritten by
     * subclasses.
     *
     * @param {object} [options]
     *  Optional parameters that will be passed to the constructor of the
     *  operations generator.
     *
     * @returns {OperationGenerator}
     *  A new operations generator instance.
     */
    createOperationGenerator(options) {
        return new OperationGenerator(this, options);
    }

    /**
     * Creates a new operations generator for document operations and undo
     * operations, invokes the callback function, applies all operations
     * contained in the generator, sends them to the server, and creates an
     * undo action with the undo operations that have been generated by the
     * callback function.
     *
     * @param {(generator: OperationGenerator) => T} callback
     *  The callback function. Receives the following parameters:
     *  1. {OperationGenerator} generator
     *    The operations generator to be filled with the document operations,
     *    and undo operations.
     *
     *  May return a promise to defer applying and sending the operations until
     *  the promise has been resolved. Will be called in the context of this
     *  model instance.
     *
     * @param {T extends GeneratorConfig, UndoActionConfig} [options]
     *  Optional parameters. Supports all options that are supported by the
     *  `OperationGenerator` class constructor, the configuration options of an
     *  undo action (to be passed to the undo action generated by this method),
     *  and the following options:
     *  - {boolean|Function} [options.storeSelection=false]
     *    If set to `true`, the selection states passed in these options
     *    (`undoSelectionState, and `redoSelectionState`) will be stored in the
     *    undo manager. If the options do not contain selection states, they
     *    will be queried from the document (the undo selection state before
     *    invoking the callback function, the redo selection state after
     *    invoking the callback function). If set to a function, it will be
     *    invoked after applying the generated operations, and after that the
     *    selection state will be queried and stored with the redo actions (can
     *    be used to select new contents in the document that have been created
     *    with the new operations).
     *  - {string} [options.undoMode="generate"]
     *    Specifies how to treat the undo operations contained in the
     *    operations generator. MUST be one of the following values:
     *    - "generate" (default): An undo action will be created in the undo
     *      manager.
     *    - "skip": No undo action will be generated, regardless of the
     *      contents of the operations generator.
     *    - "clear": No undo action will be generated, regardless of the
     *      contents of the operations generator, AND the undo manager will be
     *      cleared completely.
     *  - {OperationGenerator} [options.generator]
     *    The operation generator to be filled. If omitted, a new generator
     *    will be created by calling `createOperationGenerator` which may be
     *    overwritten by subclasses.
     *
     * @returns {JPromise<T>}
     *  A promise that will fulfil after the operations have been applied and
     *  sent successfully (with the result of the callback function); or
     *  reject, if the callback has returned a rejected promise (with the
     *  result of that promise), or if applying the operations has failed (with
     *  an `OperationError`).
     */
    @opLogger.profileMethod("$badge{EditModel} createAndApplyOperations")
    createAndApplyOperations(callback, options) {

        // create a custom operations generator type if requested
        const generator = options?.generator ?? this.createOperationGenerator(options);

        // whether to store the old and new selection state in the undo action
        const storeSelection = options?.storeSelection === true;
        const storeSelectionHandler = storeSelection ? fun.undef : getFunctionOption(options, "storeSelection", null);

        // whether the generator already has immediately applied all generated operations
        const immediateMode = generator.isImmediateApplyMode();

        // simulate actions processing mode for deferred callbacks
        return this._enterProcessActionsMode(() => {

            // addtional configuration for the undo/redo action to be created
            const undoActionConfig = {
                preventSelectionChange: !!options?.preventSelectionChange
            };

            // get the current selection state of the document
            if (storeSelectionHandler) {
                undoActionConfig.undoSelectionState = options?.undoSelectionState ?? this.getSelectionState();
            }

            // DOCS-1652: detect immediately applied operations for error handling
            let hasImmediateOps = false;
            function immediateOpsHandler() { hasImmediateOps = true; }
            this.on("operations:immediate", immediateOpsHandler);

            // invoke the callback function (may throw immediately)
            let promise = null;
            try {
                promise = this.convertToPromise(callback.call(this, generator));
            } catch (err) {
                promise = jpromise.reject(err);
            }

            // pick the result value of the callback (no need to route it through all steps of the promise chain)
            let result;
            promise.done(r => { result = r; });

            // DOCS-1652: stop listening to immediate operations
            promise.always(() => {
                this.off("operations:immediate", immediateOpsHandler);
            });

            // DOCS-1652: reload document when an error occurs in "applyImmediately" mode
            promise.fail(err => {
                if (immediateMode && hasImmediateOps && !is.scriptError(err)) {
                    opLogger.error("$badge{EditModel} createAndApplyOperations: failure with immediate operations, reloading...", err);
                    this.docApp.reloadDocument();
                }
            });

            // apply and send the operations after the callback has finished
            promise = jpromise.fastThen(promise, () => {

                // additional user data for OT to be stored in the action object
                const userData = generator.getUserData();
                const sendOptions = userData ? { userData } : undefined;

                // only send the generated operations in immediate mode
                if (immediateMode) {
                    return this.sendOperations(generator, sendOptions) ? null : jpromise.reject({ cause: "operation" });
                }

                // apply and send the operations
                return this._applyOperationsAsync(generator, false, sendOptions);
            });

            // invoke the post-processor callback function, and create the undo action with the new selection
            promise = jpromise.fastThen(promise, () => {

                // invoke the callback function to allow to change the document selection after applying the operations
                if (storeSelectionHandler) {
                    storeSelectionHandler.call(this);
                    undoActionConfig.redoSelectionState = (options && options.redoSelectionState) || this.getSelectionState();
                }

                // postprocessing after operations have been applied
                if (options?.afterApplyHandler) {
                    options.afterApplyHandler.call(this, generator);
                }

                // create the undo action with the collected operations and selection states
                switch (options?.undoMode ?? "generate") {
                    case "generate":
                        undoActionConfig.undoUserData = generator.getUserData({ undo: true });
                        undoActionConfig.redoUserData = generator.getUserData();
                        this.undoManager.addUndo(generator.getOperations({ undo: true }), generator.getOperations(), undoActionConfig);
                        break;
                    case "clear":
                        this.undoManager.clearUndoActions();
                        break;
                    case "skip":
                        break;
                    default:
                        globalLogger.error("$badge{EditModel} createAndApplyOperations: unknown undo mode");
                }

                // return the original result of the generator callback function
                return result;
            });

            return promise;
        });
    }

    /**
     * Returns the current selection state of the document. Used by the undo
     * manager to automatically restore selections after applying undo or redo
     * operations.
     *
     * This method is intended to be overwritten by subclasses. The default
     * implementation always returns `undefined`.
     *
     * @returns {Opt<object>}
     *  The current selection state of this document, if available.
     */
    getSelectionState() {
        return undefined;
    }

    /**
     * Changes the selection state of the document. Used by the undo manager to
     * automatically restore selections after applying undo or redo operations.
     *
     * This method is intended to be overwritten by subclasses. The default
     * implementation does nothing. It MUST BE implemented too, if the method
     * `getSelectionState` has been implemented.
     *
     * @param {object} _selectionState
     *  The new selection state to be set at this document.
     */
    setSelectionState(_selectionState) {
        globalLogger.error("$badge{EditModel} setSelectionState: missing implementation");
    }

    /**
     * Returns the total number of operations applied successfully.
     */
    getOperationsCount() {
        return this._totalOperations;
    }

    /**
     * Returns the operation state number of the document.
     */
    getOperationStateNumber() {
        return this._osn;
    }

    /**
     * Sets the operation state number of the document. Used when empty doc
     * is fast displayed.
     *
     * @param {Number} osn
     *  new osn number
     */
    setOperationStateNumber(osn) {
        if (this._osn !== osn) {
            this._osn = osn;
            // OT uses a different osn process
            if (!this._otEnabled) {
                this.trigger("change:osn", osn);
            }
        }
        return this;
    }

    /**
     * Returns the latest document operation applied successfully.
     *
     * @returns {Operation|null}
     *  The latest document operation applied successfully; or `null`, if no
     *  document operation has been applied yet.
     */
    getLastOperation() {
        return this._lastOperation;
    }

    /**
     * OT: Setting or removing the operation blocker. The blocker can be
     * removed, after the (internal) operations are registered in the OT cache.
     * This registration of internal operations happens in the "undogroup:close"
     * handle in the application. Complete registration of internal operations
     * is required to guarantee a valid transformation of optionally following
     * external operations that are queued during the internal blocker is
     * active. The blocker can also be removed, if the user cancels a long
     * running process or closes a dialog without generating an operation. In
     * this case the "undogroup:close" handler is never called.
     * Typically this function is combined with "this.setBlockKeyboardEvent" in
     * the model.
     *
     * Blocking of (external) operations is required as soon as local
     * operations are generated. If they are applied, it is already too late.
     * Examples are resolving of change tracks where the operations are
     * generated in step 1 and applied in step 2. Another example is the delete
     * handling. After generating the operations the user is sometimes asked,
     * if he really wants to delete the content, because it cannot be restored
     * anymore. During this dialog also no external operation can be applied.
     *
     * @param {boolean} state
     *  The new state for the blocking of external operations.
     */
    setInternalOperationBlocker(state) {

        if (!this._otEnabled) { return; } // this function is only valid in OT environment

        // setting "true" is always possible; "false" only, if there is no open undo group
        if (state) {
            this._processingInternalOperations = state;
            return;
        }

        // DOCS-2043: The internal operation blocker must not be released too early, when
        // "actions processing mode" is still running.
        // Relevant is the "result" in "enterProcessActionsMode". If this is NOT a promise,
        // the "actionsDef" promise is resolved synchronously (Text and Presentation). But
        // if the result is a promise (Spreadsheet) the "actionsDef" promise is resolved
        // asynchronously after setting "processingInternalOperations" to false, so that
        // incoming external operations are handled too early and cause a "recursive call".
        //
        // TODO: It might be better to call "setInternalOperationBlocker" always from an
        //       resolveHandler of the actionsDef and NOT from the "undogroup:close"
        //       handler.
        if (this._processingInternalOperations && this.isProcessingActions()) {
            this._actionsDef.always(() => this.setInternalOperationBlocker(state)); // come back later
            return;
        }

        // setting this value to false can only be done after the operations are registered
        // in the OT cache, so that external operations can be applied correctly.
        if (!this._processingInternalOperations || this.docApp.isRemovalOfInternalBlockerPossible()) {
            // switching from "true" to "false" -> the blocker can only be set to false,
            this._processingInternalOperations = state;
            // checking if cached external operations can start now, after the internal blocker no longer exists
            this.docApp.checkStartOfExternalOperations();
        }
    }

    /**
     * Setting a state, that a drawing is currently modified (resize, move,
     * rotate). If the state is true, update of drawings (change of drawing
     * height, font size, ...) is not always possible.
     * Also external operations must not be applied to early, if
     * "isDrawingChangeActive" is still true. This is relevant for OX Text and
     * OX Presentation. In these apps the DOM is modified after resize, move,
     * rotate, ... after the local change operations are completely applied and
     * the actionsDef is already resolved. But external operations can only be
     * applied successfully, after the DOM manipulations are done.
     *
     * @param {boolean} state
     *  The new state for currently modified drawings.
     */
    setDrawingChangeActive(state) {
        this._isDrawingChangeActive = state;
    }

    /**
     * Getting the state, whether a drawing is currently modified (resize,
     * move, rotate). If the state is true, update of drawings (change of
     * drawing height, font size, ...) is not always possible.
     *
     * @returns {boolean}
     *  Whether a drawing node is currently resized, moved or rotated.
     */
    isDrawingChangeActive() {
        return this._isDrawingChangeActive;
    }

    /**
     * Returns the undo manager of this document containing the undo action
     * stack and the redo action stack.
     *
     * @deprecated
     *  Use the `undoManager` property.
     *
     * @returns {UndoManager}
     *  The undo manager of this document.
     */
    getUndoManager() {
        return this.undoManager;
    }

    /**
     * Returns the theme targets of the passed DOM element, or the theme target
     * chain of the active (e.g. selected) element.
     *
     * This method is intended to be overriden by subclasses with support for
     * dynamic theme targets.
     *
     * @param {NodeOrJQuery} [_element]
     *  The DOM element to resolve the theme target for. If omitted, the active
     *  theme target will be returned.
     *
     * @returns {string[]}
     *  The theme target chain.
     */
    getThemeTargets(_element) {
        return [];
    }

    /**
     * Whether the slide mode needs to be used. This is typically the case
     * in the presentation application.
     *
     * @returns {boolean}
     *  Whether the slide mode is used for this application.
     */
    useSlideMode() {
        return this._slideMode;
    }

    /**
     * Whether the touch mode should be used in Presentation.
     *
     * @returns {boolean}
     *  Whether the touch mode is used in Presentation.
     */
    getSlideTouchMode() {
        return this._slideTouchMode;
    }

    /**
     * Returns an MD5 checksum for the contents of this document model.
     *
     * @returns {string}
     *  An MD5 checksum for the contents of this document model.
     */
    getDocumentChecksum() {
        const jsonModel = this.serializeToJSON();
        const checksum = CryptoJS.MD5(JSON.stringify(jsonModel)).toString();
        this.trigger("update:checksum", checksum);
        return checksum;
    }

    /**
     * Checking the undo operations after an (empty) redo.
     *
     * Intended to be overwritten by subclasses. This dummy method does
     * nothing.
     *
     * @param {Action[]} _externalActions
     *  The array of external actions.
     *
     * @param {Operation[]} _undoOperations
     *  The array containing the undo operations belonging to the 'empty' redo.
     */
    checkUndoOperations(_externalActions, _undoOperations) {
    }

    /**
     * Returns whether this document contains a comment thread with unsaved
     * changes.
     *
     * Intended to be overwritten by subclasses. This dummy method always
     * returns `false`.
     *
     * @returns {boolean}
     *  Whether any comment thread contains unsaved changes.
     */
    hasUnsavedComments() {
        return false;
    }

    /**
     * Delete unsaved changes in all comment threads in this document.
     *
     * Intended to be overwritten by subclasses. This dummy method does
     * nothing.
     */
    deleteUnsavedComments() {
    }

    /**
     * Convenience function at the model, to ask, whether the presentation mode
     * is active.
     *
     * Intended to be overwritten by subclasses. This dummy method always
     * return false.
     *
     * @returns {boolean}
     *  Whether the presentation mode is active.
     */
    isPresentationMode() {
        return false;
    }

    /**
     * Whether the remote selections shall be shown. This function can be
     * overwritten by the different subclasses.
     *
     * @returns {boolean}
     *  Whether the remote selections shall be shown.
     */
    showRemoteSelections() {
        return true;
    }

    /**
     * Whether popup menus must be blocked. This function can be overwritten by
     * the different subclasses.
     *
     * @returns {boolean}
     *  Whether popup menus must be blocked.
     */
    blockPopupMenus() {
        return false;
    }

    /**
     * Whether it is fine, that the document was loaded in read-only mode.
     *
     * Intended to be overwritten by subclasses. This dummy method always
     * return false.
     *
     * @returns {boolean}
     *  Whether readonly mode is valid after loading the document.
     */
    isValidReadOnlyMode() {
        return true;
    }

    /**
     * Setting character attributes at the current position.
     *
     * Intended to be overwritten by subclasses. This dummy method does
     * nothing.
     */
    addCharacterAttributes() {
    }

    /**
     * Whether it is necessary to check at the current position the existence
     * of character attributes that were not already sent to the backend.
     *
     * Intended to be overwritten by subclasses. This dummy method always
     * returns false.
     *
     * @returns {boolean}
     *  Whether it is necessary to check at the current position the existence
     *  of character attributes.
     */
    checkUnsentCharacterAttributes() {
        return false;
    }

    /**
     * Setting the filterVersion already with the information from the fastLoad
     * process. This is done, before the operation "setDocumentAttributes" is
     * evaluated.
     *
     * @param {number} version
     *  The fiterVersion received with the fastLoad information.
     */
    setFastLoadFilterVersion(version) {
        this._fastLoadFilterVersion = version;
    }

    /**
     * The current filter version specified in the setDocumentAttributes
     * operation sent by the filter, when the document is loaded. This version
     * was required for DOCS-4211, so that two different backends can be
     * supported by the same frontend.
     *
     * @returns {number}
     *  The filter version sent by the filter, or "0", if not specified.
     */
    getFilterVersion() {
        return this.globalConfig.fv || this._fastLoadFilterVersion || 0;
    }

    /**
     * Helper function for DOCS-4211 to differentiate between two filter
     * versions, that modify the behavior of character attributes in OX Text in
     * OOXML format.
     *
     * This function must be called after the filter version is specified by
     * operation or via fastLoad. If it is called too early, an invalid value
     * will be returned.
     *
     * @returns {boolean}
     *  Whether the client has to support the character handling supported by
     *  the filter starting with filter version "2" (DOCS-4211).
     */
    @onceMethod
    hasOoxmlTextCharacterFilterSupport() {
        return this.docApp.isOOXML() && this.docApp.isTextApp() && (this.getFilterVersion() >= 2);
    }

    // operation handlers -----------------------------------------------------

    /**
     * Callback handler for the document operation "changeConfig". Changes the
     * global document attributes, and/or the default values for various other
     * formatting attributes. These values override the defaults of the
     * attribute definitions passed in `AttributePool#registerAttrFamily`, and
     * will be used before the values of any style sheet attributes and
     * explicit element attributes will be resolved.
     *
     * @param {OperationContext} context
     *  A wrapper representing the "changeConfig" document operation.
     *
     * @throws
     *  An `OperationError` if applying the operation fails, e.g. if a required
     * property is missing in the operation.
     */
    applyChangeConfigOperation(context) {

        // change the default values of formatting attributes
        this.defAttrPool.applyChangeConfigOperation(context);

        // register author names
        const attrSet = context.getDict("attrs");
        let authors = pick.array(attrSet, "authors");
        if (authors) {
            for (const author of authors) {
                this.docApp.registerAuthorItem(author);
            }
            authors = authors.slice();
            authors.sort((a, b) => parseInt(a.authorId, 10) - parseInt(b.authorId, 10));
            this.docApp.addAuthors(authors.map(author => author.authorName));
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Returns an arbitrary but unambiguous JSON representation of this
     * document model. Used for example to calculate the MD5 checksum of the
     * document.
     *
     * @returns {object}
     *  An unambiguous JSON representation of this document model.
     */
    /*protected*/ serializeToJSON() {
        globalLogger.warn("$badge{EditModel} serializeToJSON: not implemented");
        return dict.create();
    }

    // private methods --------------------------------------------------------

    /**
     * Switches this document model into "action processing mode", and invokes
     * the passed callback function. As long as processing mode is active, the
     * method `getActionsPromise` returns a pending promise. The callback
     * function may return a promise to control the duration of the action
     * processing mode. This method MUST NOT be called recursively! The class
     * methods `applyOperations`, `applyOperationsSync`, and
     * `applyExternalOperations` must not be used (these methods will enter the
     * action processing mode by themselves).
     *
     * @param {Function} callback
     *  The callback function that will be invoked with activated action
     *  processing mode. May return a promise to extend the processing mode
     *  until the promise has been settled. May throw exceptions. Will be
     *  called in the context of this instance.
     *
     * @returns {JPromise}
     *  A promise that will fulfil when the callback function has finished, or
     *  reject on any caught error.
     */
    /*private*/ _enterProcessActionsMode(callback) {

        // prevent recursive invocations
        if (this.isProcessingActions()) {
            if (this._otEnabled) {
                if (this._processingOperations) {
                    if (this._processingExternalOperations) {
                        otLogger.info("$badge{EditModel} enterProcessActionsMode: Collision! External operations are currently applied");
                        this.trigger("docs:ot:collision");
                    } else if (this._processingInternalOperations) {
                        otLogger.info("$badge{EditModel} enterProcessActionsMode: Internal operations are currently applied");
                    }
                }
            }
            return OperationError.rejectCause("recursive", "$badge{EditModel} enterProcessActionsMode: recursive call");
        }

        // create a new deferred object which remains in pending state as long
        // as the callback function runs (and the promise it returns is pending)
        this._actionsDef = jpromise.deferred().always(() => {
            const state = this._actionsDef ? this._actionsDef.state().replace(/ed$/, "ing") : "destructed";
            opLogger.log(() => `$badge{EditModel} enterProcessActionsMode: ${state} promise...`);
        });

        // invoke the callback function with a pending "actionsDef" object
        const promise = jpromise.invoke(otLogger.profileMethod("$badge{EditModel} enterProcessActionsMode", callback.bind(this)));

        // fulfil or reject the actions promise (immediately, if the callback returns synchronously)
        promise.progress(state => this._actionsDef.notify(state));
        promise.done(result => this._actionsDef.resolve(result));
        promise.fail(error => this._actionsDef.reject(error));

        return promise;
    }

    /**
     * Called before an operations array will be applied at the document model.
     * Triggers an "operations:before" event.
     *
     * @param {Operation[]} operations
     *  The JSON operations to be applied. Will be passed to the listeners of
     *  the "operations:before" event.
     *
     * @param {boolean} external
     *  Whether the operations have been received from the server. Will be
     *  passed to the listeners of the "operations:before" event.
     *
     * @throws
     *  An `OperationError` with property `cause` set to "recursive", if this
     *  method has been called recursively, i.e. the model already processes
     *  operations. This is considered a fatal error.
     */
    /*private*/ _beginProcessOperations(operations, external) {

        // fail with a "recursive" error, if this method has been called without `endProcessOperations()`
        if (this._processingOperations) {
            if (this._otEnabled) {
                if (this._processingExternalOperations && !external) {
                    otLogger.info("$badge{EditModel} beginProcessOperations: Collision! Receiving internal operation during applying external operations");
                } else if (this._processingInternalOperations && external) {
                    otLogger.info("$badge{EditModel} beginProcessOperations: Collision! Receiving external operation during applying internal operations");
                } else {
                    otLogger.info(() => `$badge{EditModel} beginProcessOperations: Collision! State of external: ${external}, processingExternalOperations: ${this._processingExternalOperations}, processingInternalOperations: ${this._processingInternalOperations}`);
                }
            }
            OperationError.throwCause("recursive", "$badge{EditModel} beginProcessOperations: recursive call - currently processing operations.");
        }

        // always fail if the application is in error state
        if (this.docApp.isInternalError()) {
            OperationError.throwCause("recursive", "$badge{EditModel} beginProcessOperations: illegal call - application is in error state.");
        }

        // notify listeners, and enter processing state
        this.trigger("operations:before", operations, external);
        this._processingOperations = true;
        if (this._otEnabled) {
            if (external) {
                // activating the block that external operations are currently applied
                this._processingExternalOperations = true;
            } else {
                this.setInternalOperationBlocker(true);
            }
        }
    }

    /**
     * Called after an operations array has been applied at the document model.
     * Triggers an "operations:after" event, and one of the events
     * "operations:success" or "operations:error".
     *
     * @param {Opt<OperationError>} error
     *  The error caught while applying the operations has failed; or
     *  `undefined` on success.
     *
     * @param {Operation[]} operations
     *  The operations that have been applied. Will be passed to the listeners
     *  of the operations events.
     *
     * @param {boolean} external
     *  Whether the operations have been received from the server. Will be
     *  passed to the listeners of the operations events.
     *
     * @param {Opt<object>} userData
     *  Additional user data for OT to be inserted into the action object.
     */
    /*private*/ _endProcessOperations(error, operations, external, userData) {
        this._processingOperations = false;
        if (this._otEnabled) {
            // external operation blocker must be removed as soon as possible, so that the OT cache for local operations can be cleared.
            // internal operation blocker must be removed later, because the internal operations must be registered in the OT cache.
            this._processingExternalOperations = false;
        }
        if (error) {
            if (error.operation) {
                error.message += "; op=" + debug.stringify(error.operation);
            }
            opLogger.exception(error);
            this.docApp.sendLogMessage(error.message);
        }
        this.trigger("operations:after", operations, external, userData);
        this.trigger(error ? "operations:error" : "operations:success", operations, external, userData);
    }

    /**
     * Returns the implementation callback function for the document operation
     * wrapped in the passed operation context.
     *
     * @param {OperationContext} context
     *  The operation wrapper.
     *
     * @returns {Funtion}
     *  The implementation callback function for the operation.
     *
     * @throws {OperationError}
     *  If the operation does not exist.
     */
    /*private*/ _getOperationHandler(context) {
        const name = context.getStr("name");
        const handler = this._operationHandlerMap.get(name);
        context.ensure(handler, "unknown operation %s", name);
        return handler;
    }

    /**
     * Returns whether document operations can currently be applied.
     *
     * - External operations can always be applied.
     * - Internal operations can only be applied if the application has edit
     *   rights.
     *
     * @param {boolean} external
     *  Whether to apply external or internal document operations.
     *
     * @returns {boolean}
     *  Whether document operations can currently be applied.
     */
    /*private*/ _canApplyOperations(external) {
        return external || this.docApp.isEditable() || this.docApp.isRescueDocMode();
    }

    /**
     * Executes the handler function for the passed operation.
     *
     * @param {Operation} operation
     *  The JSON operation object to be applied at this document model.
     *
     * @param {boolean} external
     *  Whether the operation has been received from the server.
     *
     * @param {boolean} sendOnly
     *  Whether to prevent applying any operation locally (only send them to
     *  the server).
     *
     * @param {boolean} importing
     *  Whether the operation is part of the initial document import.
     *
     * @throws
     *  An `OperationError` if applying the passed operation has failed.
     */
    /*private*/ _applyOperation(operation, external, sendOnly, importing) {

        // log operation
        opLogger.log(() => `i=${this._totalOperations}, op=${operation}`);

        // check operation and operation name
        if (!is.dict(operation)) {
            throw new OperationError("$badge{EditModel} applyOperation: JSON object expected.", { operation });
        }

        // create the context (wrapper for JSON data) passed to the operation handler
        const context = this.createOperationContext(operation, external, importing);
        // the callback handler for the operation
        const handler = this._getOperationHandler(context);

        // Check the correct operation state number of an external operation. The OSN must be
        // identical on every client. The OSN is sent from server, when the document is loaded.
        if (external && context.has("osn")) {

            // the OSN carried in the operation
            const opOSN = context.getInt("osn");
            context.ensure(opOSN >= 0, "Invalid OSN: %d", opOSN);

            // During loading the document, the initial OSN is sent by the server.
            // If we don't have a valid value use that OSN.
            if (this._osn === -1) {
                this.setOperationStateNumber(opOSN);
            }

            // the current OSN must be equal to the OSN contained in the external operation
            if (!this._otEnabled && (this._osn !== opOSN)) {
                context.error("Wrong OSN (current=%d operation=%d)", this._osn, operation.osn);
            }
        }

        // insert current OSN and OPL into internal operations
        if (!external) {
            operation.osn = this._otEnabled ? this.docApp.getServerOSN() : this._osn;
            operation.opl = 1;
            // additional data for debugging reasons
            if (DEBUG) {
                operation.dbg_user_id = ox.user_id;
            }
        }

        // Tracing external operation, when they are applied together with the current OSN
        if (DEBUG && external) { this.trigger("operationtrace:apply:external", operation, this.docApp.getServerOSN()); }

        // update the current OSN of the document
        if (this._osn >= 0) {
            this.setOperationStateNumber(this._osn + context.optInt("opl", 1));
        }

        // DOCS-974: collect operation for local storage, if supported
        this.config.storageOperationCollector?.call(this, operation);

        // do not apply operations scoped to the export filter
        const scope = context.optStr("scope");

        // invoke the operation handler (unless the operation scope is "filter" for operations intended to be
        // executed in the filter component only, or the option `sendOnly` has been passed)
        if (!sendOnly && (scope !== "filter") && (!this._otEnabled || !isOperationRemoved(operation))) {
            if (DEBUG) { this.trigger("operation:apply:before", operation, this.docApp.getServerOSN(), external); }
            // may throw an `OperationError`
            handler.call(this, context);
            if (DEBUG) { this.trigger("operation:apply:after", operation, this.docApp.getServerOSN(), external); }
            // cache the last applied operation
            this._lastOperation = operation;
            if (this._otEnabled && !importing) {
                this.trigger(external ? "operation:external" : "operation:internal", operation);
            }
        }

        // operation handler succeeded: increase current OSN
        this._totalOperations += 1;
    }

    /**
     * Executes the handler functions for all passed operations. If the undo
     * manager is active, all applied operations will be embedded into a single
     * undo group action.
     *
     * @param {OperationSource} operations
     *  A single JSON operation, or an array of JSON operations, or an
     *  operations generator instance with operations to be applied at this
     *  document model. Note that an operations generator passed to this method
     *  will NOT be cleared after its operations have been sent.
     *
     * @param {boolean} external
     *  Whether the operation has been received from the server.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.sendOnly=false]
     *    Set to `true` to prevent applying any operation locally (only send
     *    them to the server).
     *  - {boolean} [options.undo=false]
     *    If set to `true`, and the parameter "operations" is an instance of
     *    the class `OperationGenerator`, its undo operations will be applied.
     *
     * @throws
     *  An `OperationError` if applying any of the operations has failed.
     */
    /*private*/ _applyOperationsSync(operations, external, options) {

        // whether to skip applying the operations
        const sendOnly = options?.sendOnly;
        // the user data to be stored in the action object
        const userData = options?.userData;
        // whether document import is still running
        const importing = !this.isImportFinished();

        // convert input parameter to an array of JSON operations
        operations = resolveOperations(operations, options);

        // if the operation's origin is not an external client and
        // we don't have edit rights then we skip the operations silently.
        if (!this._canApplyOperations(external) || (operations.length === 0)) {
            return true;
        }

        // allow subclasses to modify locally generated operations
        // -> this function must only be called for local and synchronous operations.
        // TODO: In the future it should never be called anymore.
        const operationsFinalizer = !external && this.config.operationsFinalizer;
        if (operationsFinalizer) { operations = operationsFinalizer.call(this, operations); }

        // put errors thrown by any operation handler into this variable
        let error = null;
        try {

            // enter operations mode (prevent recursive calls, may throw)
            this._beginProcessOperations(operations, external);
            // apply all operations at once, group into an undo action
            this.undoManager.enterUndoGroup(() => {
                try {
                    for (const operation of operations) {
                        this._applyOperation(operation, external, sendOnly, importing);
                    }
                } catch (err) {
                    error = err;
                }
            });
        } catch (err) {
            error = err;
        }

        // post-processing (trigger end events, leave operations mode),
        // also if `beginProcessOperations()` has failed (notify the error)
        this._endProcessOperations(error, operations, external, userData);

        // rethrow all caught errors
        if (error) { throw error; }
    }

    /**
     * Executes the handler functions for all passed operations in an
     * asynchronous background loop. If the undo manager is active, all applied
     * operations will be embedded into a single undo group action.
     *
     * @param {OperationSource} operations
     *  A single JSON operation, or an array of JSON operations, or an
     *  operations generator instance with operations to be applied at this
     *  document model. Note that an operations generator passed to this method
     *  will NOT be cleared after its operations have been sent.
     *
     * @param {boolean} external
     *  Whether the operation has been received from the server.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.sendOnly=false]
     *    Set to `true` to prevent applying any operation locally (only send
     *    them to the server).
     *  - {boolean} [options.undo=false]
     *    If set to `true`, and the parameter "operations" is an instance of
     *    the class `OperationGenerator`, its undo operations will be applied.
     *
     * @returns {JPromise}
     *  A promise that will fulfil after all operations have been applied
     *  successfully, or reject immediately after applying an operation has
     *  failed. The promise regularly sends progress updates while applying the
     *  operations.
     */
    /*private*/ _applyOperationsAsync(operations, external, options) {

        // whether to skip applying the operations
        const sendOnly = options?.sendOnly;
        // the user data to be stored in the action object
        const userData = options?.userData;
        // whether document import is still running
        const importing = !this.isImportFinished();
        // the global index of the first applied operation
        const startIndex = this._totalOperations;

        // convert input parameter to an array of JSON operations
        operations = resolveOperations(operations, options);

        // immediate success on empty operations array
        if (operations.length === 0) { return this.createResolvedPromise(null); }

        // enter operations mode (prevent recursive calls)
        try {
            this._beginProcessOperations(operations, external);
        } catch (err) {
            this._endProcessOperations(err, operations, external, userData);
            return this.createRejectedPromise(err);
        }

        // apply all operations in an asynchronous loop, group into an undo action
        const promise = this.undoManager.enterUndoGroup(() => {

            // return the promise to enterUndoGroup() to defer the open undo group
            return this.iterateArraySliced(operations, (operation, index) => {

                // bug 58298: losing edit rights during processing: shorten the array to operations already applied
                if (!this._canApplyOperations(external)) {
                    operations = operations.slice(0, index);
                    return BREAK;
                }

                // apply the operation (may throw an `OperrationError`)
                this._applyOperation(operation, external, sendOnly, importing);
            });
        });

        // all passed operations applied successfully: trigger end events, leave operations mode
        this.onFulfilled(promise, () => {
            this._endProcessOperations(undefined, operations, external, userData);
        });

        // operations failed: trigger end events, leave operations mode
        this.onRejected(promise, err => {
            if (err === "abort") {
                // manually aborted: notify applied operations only
                this._endProcessOperations(undefined, operations.slice(0, this._totalOperations - startIndex), external, userData);
            } else {
                // operations failed: notify all operations
                this._endProcessOperations(err, operations, external, userData);
            }
        });

        return promise;
    }
}
