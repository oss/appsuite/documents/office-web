/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { is, math, dict } from '@/io.ox/office/tk/algorithms';
import { Font } from '@/io.ox/office/tk/dom';

import { isColorThemed } from '@/io.ox/office/editframework/utils/attributeutils';
import { Color } from '@/io.ox/office/editframework/utils/color';
import { getCssBorder as _getCssBorder, getCssBorderAttributes as _getCssBorderAttributes } from '@/io.ox/office/editframework/utils/border';
import Gradient from '@/io.ox/office/editframework/utils/gradient';
import * as Op from '@/io.ox/office/editframework/utils/operations';
import FontCollection from '@/io.ox/office/editframework/model/fontcollection';
import ThemeCollection from '@/io.ox/office/editframework/model/themecollection';

// constants ==================================================================

// definitions for global document attributes
const DOCUMENT_ATTRIBUTES = {

    /**
     * ISO date/time of document creation.
     */
    created: { def: "" },

    /**
     * ISO date/time of last modification.
     */
    modified: { def: "" },

    /**
     * Middleware filter version.
     */
    fv: { def: 0 }
};

// mix-in class ModelAttributesMixin ==========================================

/**
 * A mix-in class for the document model class EditModel that provides the
 * global document attributes, the default values for all formatting
 * attributes of all registered attribute families, the style collections
 * for all attribute families used in a document, a font collection, and a
 * collection of themes.
 *
 * @property {ThemeCollection} themeCollection
 *  The collection with all registered style themes.
 *
 * @property {FontCollection} fontCollection
 *  The collection with all registered fonts.
 *
 * @param {Object} [initOptions]
 *  Optional parameters passed to the constructor of the class EditModel.
 */
export default function ModelAttributesMixin(initOptions) {

    // self reference (the document model)
    var self = this;

    // style sheet collections mapped by attribute family (map takes ownership)
    var styleCollectionMap = this.member(new Map/*<string, StyleCollection>*/());

    // auto-style collections mapped by attribute family (map takes ownership)
    var autoStyleCollectionMap = this.member(new Map/*<string, AutoStyleCollection>*/());

    // default attribute family for style sheets and auto-styles
    var defaultStyleFamily = initOptions?.defaultStyleFamily ?? null;

    // special handling for ODF documents
    var odf = this.docApp.isODF();

    // private methods ----------------------------------------------------

    /**
     * Returns the style family contained in the passed document operation.
     *
     * @param {OperationContext} context
     *  The wrapper for a document operation.
     *
     * @returns {String}
     *  The style family contained in the passed operation. If missing, the
     *  default style family passed to the constructor will be returned.
     *
     * @throws {OperationError}
     *  If the passed operation does not contain a style family, and no
     *  default style family has been specified in the constructor options.
     */
    function extractStyleFamily(context) {
        // bug 55237: default to type specified in constructor, if available
        return defaultStyleFamily ? context.optStr('type', defaultStyleFamily) : context.getStr('type');
    }

    /**
     * Returns the style sheet collection targeted by the passed document
     * operation.
     *
     * @param {OperationContext} context
     *  The wrapper for a document operation.
     *
     * @returns {StyleCollection}
     *  The style sheet collection targeted by the passed operation.
     *
     * @throws {OperationError}
     *  If the passed operation does not refer to an existing style sheet
     *  collection.
     */
    function extractStyleCollection(context) {

        // the style family name
        var styleFamily = extractStyleFamily(context);

        // try to get the style sheet collection (throw on error)
        var styleCollection = self.getStyleCollection(styleFamily);
        context.ensure(styleCollection, 'invalid style family \'%s\'', styleFamily);
        return styleCollection;
    }

    /**
     * Returns the auto-style collection targeted by the passed document
     * operation.
     *
     * @param {OperationContext} context
     *  The wrapper for a document operation.
     *
     * @returns {AutoStyleCollection}
     *  The auto-style collection targeted by the passed operation.
     *
     * @throws {OperationError}
     *  If the passed operation does not refer to an existing auto-style
     *  collection.
     */
    function extractAutoStyleCollection(context) {

        // the style family name
        var styleFamily = extractStyleFamily(context);

        // try to get the auto-style collection (throw on error)
        var styleCollection = self.getAutoStyleCollection(styleFamily);
        context.ensure(styleCollection, 'invalid style family \'%s\'', styleFamily);
        return styleCollection;
    }

    // public methods -----------------------------------------------------

    /**
     * Returns the default style attribute family used in style sheet
     * operations and auto-style operations if the "type" property is
     * missing.
     *
     * @returns {String|Null}
     *  The default style attribute family as passed to the constructor of
     *  this instance; or null, if there is no default style family
     *  available.
     */
    this.getDefaultStyleFamily = function () {
        return defaultStyleFamily;
    };

    /**
     * Adds a stylesheet collection to the document.
     *
     * @param {CollT extends StyleCollection} collection
     *  The new stylesheet collection to be inserted into the document.
     *
     * @returns {CollT}
     *  The passed stylesheet collection.
     */
    this.addStyleCollection = function (collection) {

        // prevent double registration
        var styleFamily = collection.getStyleFamily();
        if (styleCollectionMap.has(styleFamily)) {
            throw new Error('style family "' + styleFamily + '" already registered');
        }

        styleCollectionMap.set(styleFamily, collection);
        return collection;
    };

    /**
     * Returns the style sheet collection for the specified attribute
     * family.
     *
     * @param {String} styleFamily
     *  The name of the style attribute family.
     *
     * @returns {StyleCollection|Null}
     *  The specified style sheet collection if existing, otherwise null.
     */
    this.getStyleCollection = function (styleFamily) {
        return styleCollectionMap.get(styleFamily) ?? null;
    };

    /**
     * Adds an autostyle collection to the document.
     *
     * @param {CollT extends AutoStyleCollection} collection
     *  The new autostyle collection to be inserted into the document.
     *
     * @returns {CollT}
     *  The passed autostyle collection.
     */
    this.addAutoStyleCollection = function (collection) {

        // prevent double registration
        var styleFamily = collection.styleFamily;
        if (autoStyleCollectionMap.has(styleFamily)) {
            throw new Error('auto-style family "' + styleFamily + '" already registered');
        }

        autoStyleCollectionMap.set(styleFamily, collection);
        return collection;
    };

    /**
     * Returns the auto-style collection for the specified attribute
     * family.
     *
     * @param {String} styleFamily
     *  The name of the auto-style attribute family.
     *
     * @returns {AutoStyleCollection|Null}
     *  The specified auto-style collection if existing, otherwise null.
     */
    this.getAutoStyleCollection = function (styleFamily) {
        return autoStyleCollectionMap.get(styleFamily) ?? null;
    };

    /**
     * Returns whether a default theme exists for the specified target.
     *
     * @param {String} target
     *  A target string to specify the theme's target.
     *
     * @returns {Boolean}
     *  Whether a default theme exists for the specified target.
     */
    this.hasDefaultTheme = function (target) {
        return this.themeCollection.hasDefaultTheme(target);
    };

    /**
     * Returns the default theme from the document's theme collection for
     * an optional theme target.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be returned.
     *
     * @returns {ThemeModel}
     *  The model of the specified theme. If no target has been specified,
     *  the active target chain of the document will be used. If no theme
     *  has been inserted for the effective targets, the global default
     *  theme will be returned.
     */
    this.getThemeModel = function (targets) {
        return this.themeCollection.getDefaultModel(targets || this.getThemeTargets());
    };

    /**
     * Returns the default theme from the document's theme collection for
     * the passed DOM element.
     *
     * @param {NodeOrJQuery} element
     *  The DOM element node to resolve the theme targets for.
     *
     * @returns {ThemeModel}
     *  The theme model associated with the passed DOM element.
     */
    this.getThemeModelForNode = function (element) {
        return this.getThemeModel(this.getThemeTargets(element));
    };

    /**
     * Returns the effective font name for the passed attribute value.
     *
     * @param {String} fontName
     *  The name of a font (case-insensitive), or the internal key of a
     *  scheme font.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {String}
     *  The effective font name of the specified scheme font if existing;
     *  otherwise the passed font name as is.
     */
    this.resolveFontName = function (fontName, targets) {
        return this.getThemeModel(targets).getSchemeFont(fontName, fontName);
    };

    /**
     * Returns the value of the CSS 'font-family' attribute containing the
     * specified font name and all alternative font names.
     *
     * @param {String} fontName
     *  The name of of the font (case-insensitive).
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {String}
     *  The value of the CSS 'font-family' attribute containing the
     *  specified font name and all alternative font names.
     */
    this.getCssFontFamily = function (fontName, targets) {
        return this.fontCollection.getCssFontFamily(this.resolveFontName(fontName, targets));
    };

    /**
     * Creates and returns a font descriptor for rendering from the passed
     * character formatting attributes.
     *
     * @param {CharacterAttributes} charAttributes
     *  The character formatting attributes, as used in operations.
     *
     * @param {number} zoom
     *  The zoom factor used to scale the font size. The value 1 represents
     *  the normal zoom (of 100%).
     *
     * @param {string[]} [targets]
     *  An array of names to specify the target theme. If omitted, the active
     *  theme of the document will be used.
     *
     * @returns {Font}
     *  The resulting font. The property 'family' includes all available
     *  fall-back fonts for the font. The property 'size' contains the font
     *  size as number (scaled according to the zoom factor if specified).
     */
    this.getRenderFont = function (charAttributes, zoom, targets) {
        var fontFamily = this.getCssFontFamily(charAttributes.fontName, targets);
        var fontSize = math.roundp(charAttributes.fontSize * (zoom || 1), 0.1);
        return new Font(fontFamily, fontSize, charAttributes.bold, charAttributes.italic);
    };

    /**
     * Resolves the passed color. Scheme colors will be resolved using the
     * current document theme.
     *
     * @param {Color} color
     *  The color to be resolved.
     *
     * @param {Color|string} auto
     *  Additional information needed to resolve the automatic color. See
     *  method Color.resolve() for details about this parameter.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {ColorDescriptor}
     *  A descriptor for the resulting color.
     */
    this.resolveColor = function (color, auto, targets) {
        return color.resolve(auto, this.getThemeModel(targets));
    };

    /**
     * Resolves the passed color. Scheme colors will be resolved using the
     * current document theme.
     *
     * @param {OpColor} jsonColor
     *  The JSON representation of the color to be resolved, as used in
     *  document operations.
     *
     * @param {OpColor|string} jsonAuto
     *  Additional information needed to resolve the automatic color. See
     *  method Color.resolve() for details about this parameter.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {ColorDescriptor}
     *  A descriptor for the resulting color.
     */
    this.parseAndResolveColor = function (jsonColor, jsonAuto, targets) {
        var color = Color.parseJSON(jsonColor);
        var auto = _.isString(jsonAuto) ? jsonAuto : Color.parseJSON(jsonAuto);
        return this.resolveColor(color, auto, targets);
    };

    /**
     * Converts the passed color attribute value to a CSS color value.
     * Scheme colors will be resolved by using the current theme.
     *
     * @param {OpColor} jsonColor
     *  The JSON representation of the source color, as used in document
     *  operations.
     *
     * @param {OpColor|string} jsonAuto
     *  Additional information needed to resolve the automatic color. See
     *  method Color.resolve() for details about this parameter.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {string}
     *  The CSS color value converted from the passed color object.
     */
    this.getCssColor = function (jsonColor, jsonAuto, targets) {
        return this.parseAndResolveColor(jsonColor, jsonAuto, targets).css;
    };

    /**
     * Returns a color descriptor with details about the passed color,
     * assuming it to be a text color on a specific background style.
     * Scheme colors will be resolved using the current document theme. See
     * method Color.resolveText() for more details.
     *
     * @param {Color} textColor
     *  The color to be resolved.
     *
     * @param {Color[]} fillColors
     *  The source fill colors, from outermost to innermost level.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {ColorDescriptor}
     *  A descriptor for the resulting color.
     */
    this.resolveTextColor = function (textColor, fillColors, targets) {
        return textColor.resolveText(fillColors, this.getThemeModel(targets));
    };

    /**
     * Returns a color descriptor with details about the passed color,
     * assuming it to be a text color on a specific background style.
     * Scheme colors will be resolved using the current document theme. See
     * method Color.resolveText() for more details.
     *
     * @param {OpColor} jsonTextColor
     *  The JSON representation of the text color to be resolved, as used
     *  in document operations.
     *
     * @param {OpColor[]} jsonFillColors
     *  The source fill colors, from outermost to innermost level, as array
     *  of JSON objects used in document operations.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {ColorDescriptor}
     *  A descriptor for the resulting color.
     */
    this.parseAndResolveTextColor = function (jsonTextColor, jsonFillColors, targets) {
        var textColor = Color.parseJSON(jsonTextColor);
        var fillColors = jsonFillColors.map(Color.parseJSON);
        return this.resolveTextColor(textColor, fillColors, targets);
    };

    /**
     * Resolves the passed text color to an explicit color if it is set to
     * 'auto', according to the specified fill colors.
     *
     * @param {OpColor} jsonTextColor
     *  The JSON representation of the source text color, as used in
     *  document operations.
     *
     * @param {OpColor[]} jsonFillColors
     *  The JSON representation of the source fill colors, from outermost
     *  to innermost level, as used in document operations.
     *
     * @param {string|string[]|null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {string}
     *  The CSS text color value converted from the passed color objects.
     */
    this.getCssTextColor = function (jsonTextColor, jsonFillColors, targets) {
        return this.parseAndResolveTextColor(jsonTextColor, jsonFillColors, targets).css;
    };

    /**
     * Creates a JSON color object for a scheme color for usage in document
     * operations. The JSON color will contain the transformations needed
     * to create the specified shading, and the effective RGB value for ODF
     * files.
     *
     * @param {String} id
     *  The identifier of the scheme color, e.g. "dark1" or "accent6".
     *
     * @param {Number} shading
     *  The color shading, as signed percentage. The value 0 will result in
     *  the base scheme color. Positive values up to 100 will lighten the
     *  color toward white. Negative values down to -100 will darken the
     *  color towards black.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used. This
     *  parameter is needed to calculate the replacement RGB value for ODF.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Boolean} [options.drawing=false]
     *      If set to true, color shading will be created with "tint" and
     *      "shade" color transformations that are applicable for drawing
     *      objects only. By default, the color transformations "lumMod"
     *      and "lumOff" will be used.
     *
     * @returns {Object}
     *  The resulting JSON scheme color with the color transformations, and
     *  for ODF documents with the additional property "fallbackValue"
     *  containing the resulting RGB color value.
     */
    this.createSchemeColor = function (id, shading, targets, options) {

        // create a Color instance
        var color = Color.fromScheme(id);
        // whether to create DrawingML transformations
        var drawing = options && options.drawing;

        // add shading transformation
        if (shading < 0) {
            color.transform(drawing ? 'shade' : 'lumMod', Math.round((100 + shading) * 1000));
        } else if (shading > 0) {
            color.transform(drawing ? 'tint' : 'lumMod', Math.round((100 - shading) * 1000));
            if (!drawing) { color.transform('lumOff', Math.round(shading * 1000)); }
        }

        // create the JSON color object, add the fall-back RGB value for ODF
        var jsonColor = color.toJSON();
        if (odf) { jsonColor.fallbackValue = this.resolveColor(color, 'fill', targets).hex; }

        return jsonColor;
    };

    /**
     * Creates a JSON color object for the default hyperlink scheme color
     * for usage in document operations. The JSON color will contain the
     * effective RGB value for ODF files.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used. This
     *  parameter is needed to calculate the replacement RGB value for ODF.
     *
     * @returns {Object}
     *  The resulting JSON scheme color with the additional property
     *  "fallbackValue" containing the resulting RGB color value for ODF
     *  documents.
     */
    this.createHlinkColor = function (targets) {
        return this.createSchemeColor('hyperlink', 0, targets);
    };

    /**
     * @param {unknown} gradient
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {*}
     */
    this.getGradientValue = function (gradient, targets) {
        return Gradient.resolveColors(gradient, this, targets);
    };

    /**
     * @param {unknown} gradient
     *
     * @param {unknown} pathCoords
     *
     * @param {unknown} contextWrapper
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @returns {*}
     */
    this.getGradientFill = function (gradient, pathCoords, contextWrapper, targets) {
        return Gradient.parseFillStyle(this, gradient, contextWrapper.ctx, targets, pathCoords);
    };

    /**
     * Returns the effective CSS attributes for the passed border value.
     * Scheme colors will be resolved by using the current theme of an
     * optionally specified target.
     *
     * @param {Object} border
     *  The border object as used in operations.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Boolean} [options.preview=false]
     *      If set to true, the border will be rendered for a preview
     *      element in the GUI. The border width will be restricted to two
     *      pixels in that case.
     *  - {Object} [options.autoColor]
     *      If specified, a replacement color to be used in case the border
     *      object contains the automatic color.
     *
     * @returns {Object}
     *  A map with CSS border attributes. Contains the property 'style'
     *  with the effective CSS border style as string; the property 'width'
     *  with the effective border width in pixels (as number, rounded to
     *  entire pixels); and the property 'color' with the effective CSS
     *  color (as string with leading hash sign).
     */
    this.getCssBorderAttributes = function (border, targets, options) {
        // use the static helper function from module Border, pass current theme
        return _getCssBorderAttributes(border, this.getThemeModel(targets), options);
    };

    /**
     * Converts the passed border attribute object to a CSS border value.
     * Scheme colors will be resolved by using the current theme of an
     * optionally specified target.
     *
     * @param {Object} border
     *  The border object as used in operations.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A name or an array of names to specify the target theme. If omitted
     *  or set to null, the active theme of the document will be used.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Boolean} [options.clearNone=false]
     *      If set to true, the return value for invisible borders will be
     *      the empty string instead of the keyword 'none'.
     *  - {Boolean} [options.preview=false]
     *      If set to true, the border will be rendered for a preview
     *      element in the GUI. The border width will be restricted to two
     *      pixels in that case.
     *  - {Object} [options.autoColor]
     *      If specified, a replacement color to be used in case the border
     *      object contains the automatic color.
     *
     * @returns {String}
     *  The CSS border value converted from the passed border object.
     */
    this.getCssBorder = function (border, targets, options) {
        // use the static helper function from module Border, pass current theme
        return _getCssBorder(border, this.getThemeModel(targets), options);
    };

    /**
     * Adding the fallbackValue property to all themed colors in a specified object.
     * The themed colors are searched recursively inside the given object.
     * Info: The specified object is modified within this function.
     *
     * @param {Nullable<PtAttrSet<AttributeSet>>} [attrSet]
     *  An object, whose themed colors get the property 'fallbackValue'. This is
     *  especially required for ODF file format, because themed colors are not
     *  supported in this format.
     */
    this.addFallbackValueToThemeColors = function (attrSet) {
        if (odf && is.dict(attrSet)) {
            dict.forEach(attrSet, attrs => {
                if (is.dict(attrs)) {
                    dict.forEach(attrs, (value, key) => {
                        if (isColorThemed(value) && !value.fallbackValue) {
                            attrs[key] = {
                                ...value,
                                fallbackValue: this.parseAndResolveColor(value, '').hex
                            };
                        }
                    });
                }
            });
        }
    };

    // operation handlers -------------------------------------------------

    this.registerOperationHandler(Op.INSERT_STYLESHEET, function (context) {

        // the target style sheet collection
        var styleCollection = extractStyleCollection(context);

        // create undo operation (bug 40878: but only for custom style sheets, not for latent style sheets)
        var styleId = context.getStr('styleId');
        if (context.optBool('custom') && !context.external) {
            var undoOp = { name: Op.DELETE_STYLESHEET, type: styleCollection.getStyleFamily(), styleId };
            this.undoManager.addUndo(undoOp, context);
        }

        // apply the operation (throws on error)
        styleCollection.applyInsertStyleSheetOperation(context);
    });

    this.registerOperationHandler(Op.DELETE_STYLESHEET, function (context) {

        // the target style sheet collection
        var styleCollection = extractStyleCollection(context);

        // create undo operation
        if (!context.external) {
            var styleId = context.getStr('styleId');
            var undoOp = { name: Op.INSERT_STYLESHEET, type: styleCollection.getStyleFamily(), styleId };
            undoOp.styleName = styleCollection.getName(styleId);
            undoOp.parentId = styleCollection.getParentId(styleId);
            undoOp.attrs = styleCollection.getStyleSheetAttributeMap(styleId, true);
            undoOp.hidden = styleCollection.isHidden(styleId);
            undoOp.uiPriority = styleCollection.getUIPriority(styleId);
            undoOp.custom = styleCollection.isCustom(styleId);
            this.undoManager.addUndo(undoOp, context);
        }

        // apply the operation (throws on error)
        styleCollection.applyDeleteStyleSheetOperation(context);
    });

    this.registerOperationHandler(Op.CHANGE_STYLESHEET, function (context) {

        // the target style sheet collection
        var styleCollection = extractStyleCollection(context);

        // create undo operation
        if (!context.external) {
            var styleId = context.getStr('styleId');
            var undoOp = { name: Op.CHANGE_STYLESHEET, type: styleCollection.getStyleFamily(), styleId };
            if (context.has('styleName')) { undoOp.styleName = styleCollection.getName(styleId); }
            if (context.has('attrs')) { undoOp.attrs = styleCollection.getStyleSheetAttributeMap(styleId, true); }
            this.undoManager.addUndo(undoOp, context);
        }

        // apply the operation (throws on error)
        styleCollection.applyChangeStyleSheetOperation(context);
    });

    this.registerOperationHandler(Op.INSERT_AUTOSTYLE, function (context) {
        var styleCollection = extractAutoStyleCollection(context);
        styleCollection.applyInsertAutoStyleOperation(context);
    });

    this.registerOperationHandler(Op.CHANGE_AUTOSTYLE, function (context) {
        var styleCollection = extractAutoStyleCollection(context);
        styleCollection.applyChangeAutoStyleOperation(context);
    });

    this.registerOperationHandler(Op.DELETE_AUTOSTYLE, function (context) {
        var styleCollection = extractAutoStyleCollection(context);
        styleCollection.applyDeleteAutoStyleOperation(context);
    });

    // initialization -----------------------------------------------------

    // public properties
    this.themeCollection = this.member(new ThemeCollection(this));
    this.fontCollection = this.member(new FontCollection(this));

    // formatting attributes registration
    this.defAttrPool.registerAttrFamily("document", DOCUMENT_ATTRIBUTES);
}
