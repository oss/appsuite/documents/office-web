/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { is, to, set, dict, json } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { insertAttribute, updateAttribute } from "@/io.ox/office/editframework/utils/attributeutils";

// class AttributedModel ======================================================

/**
 * An abstract model object containing formatting attributes and the reference
 * to a stylesheet of a specific attribute family.
 */
export class AttributedModel extends ModelObject {

    /**
     * @param {EditModel} docModel
     *  The document model containing this attributed model object.
     *
     * @param {PtAttrSet<AttrSetT>} [attrSet]
     *  An attribute set with initial formatting attributes for the attributed
     *  model object.
     *
     * @param {AttributedModelConfig} [config]
     *  Configuration options.
     */
    constructor(docModel, attrSet, config) {

        // base constructor
        super(docModel);

        // the configuration options passed to the constructor
        /*public*/ this.config = { listenToStyles: true, listenToParent: true, ...config };

        // the attribute family of the stylesheets
        const styleFamily = config?.styleFamily;

        // the collection of stylesheets for the style family
        /*protected*/ this.styleSheets = styleFamily ? docModel.getStyleCollection(styleFamily) : null;
        // the attribute pool to be used for the formatting attributes
        /*protected*/ this.attrPool = this.styleSheets?.attrPool ?? config?.attrPool ?? docModel.defAttrPool;
        // the parent model used to build the merged attribute set
        /*protected*/ this.parentModel = config?.parentModel;

        // the (incomplete) explicit attributes of this model object
        /*private*/ this._explAttrSet = dict.create();
        // the (complete) cached merged attributes of this model object
        /*private*/ this._mergedAttrSet = null;

        // get all supported attribute families
        /*private*/ this._supportedFamilies = new Set(config?.families);
        if (this.styleSheets && !config?.skipStyleFamilies) {
            set.assign(this._supportedFamilies, this.styleSheets.yieldSupportedFamilies());
        }

        // initialize explicit/merged attributes sets (filters "attrSet" by supported attributes)
        this.setAttributes(to.dict(attrSet, true), { silent: true });

        // listen to changes in the parent object, update the merged attributes and notify own listeners
        if (this.parentModel && this.config.listenToParent) {
            this.listenTo(this.parentModel, "change:attributes", () => this._calculateMergedAttributeSet(false));
        }

        // listen to stylesheet changes, update the merged attributes and notify own listeners
        if (this.styleSheets && this.config.listenToStyles) {
            this.listenToAllEvents(this.styleSheets, () => this._calculateMergedAttributeSet(false));
        }

        // update merged attributes silently once after import (object may be destroyed before import finishes)
        this.waitForImportSuccess(alreadyFinished => {
            if (!alreadyFinished) { this._calculateMergedAttributeSet(true); }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this instances supports attributes of the specified
     * attribute family.
     *
     * @param {string} family
     *  The name of an attribute family.
     *
     * @returns {boolean}
     *  Whether this instances supports the specified attribute family.
     */
    supportsFamily(family) {
        return this._supportedFamilies.has(family);
    }

    /**
     * Returns the identifier of the stylesheet used to format this model.
     *
     * @returns {string|null}
     *  The identifier of the stylesheet used to format this model; or `null`,
     *  if this model does not support stylesheets.
     */
    getStyleId() {
        return this.styleSheets ? this._mergedAttrSet.styleId : null;
    }

    /**
     * Returns whether this model object contains any explicit attributes, or a
     * reference to a stylesheet.
     *
     * @returns {boolean}
     *  Whether this model object contains any explicit attributes, or a
     *  reference to a stylesheet.
     */
    hasExplicitAttributes() {
        return !is.empty(this._explAttrSet);
    }

    /**
     * Returns the explicit attribute set of this model object.
     *
     * @param {boolean} [direct=false]
     *  If set to `true`, the returned attribute set will be a reference to the
     *  original dictionary stored in this instance which MUST NOT be modified!
     *  By default, a deep clone of the attribute set will be returned that can
     *  be freely modified.
     *
     * @returns {PtAttrSet<AttrSetT>}
     *  A partial attribute set, containing values for all attributes that have
     *  been explicitly set.
     */
    getExplicitAttributeSet(direct) {
        return (direct === true) ? this._explAttrSet : json.deepClone(this._explAttrSet);
    }

    /**
     * Returns the merged attribute set of this model object.
     *
     * @param {boolean} [direct=false]
     *  If set to `true`, the returned attribute set will be a reference to the
     *  original dictionary stored in this instance which MUST NOT be modified!
     *  By default, a deep clone of the attribute set will be returned that can
     *  be freely modified.
     *
     * @returns {AttrSetT}
     *  A complete attribute set, containing values for all known attributes,
     *  collected from the document default values, the stylesheet currently
     *  referenced by this model object, and the explicit attributes of this
     *  model object.
     */
    getMergedAttributeSet(direct) {
        return (direct === true) ? this._mergedAttrSet : json.deepClone(this._mergedAttrSet);
    }

    /**
     * Returns a reduced copy of the passed attribute set that contains only
     * the attributes that are not equal to the attributes of this model
     * instance (i.e. all attributes that will change this model if applied as
     * a document operation).
     *
     * @param {PtAttrSet<AttrSetT>} attrSet
     *  An (incomplete) attribute set to be reduced according to the explicit
     *  attributes of this instance.
     *
     * @returns {PtAttrSet<AttrSetT>}
     *  A deep copy of the passed attribute set that will only contain the
     *  attribute values that are not equal to the explicit attibute values of
     *  this model instance (incluging missing explicit attributes); or `null`
     *  values that will delete an existing explicit attribute.
     */
    getReducedAttributeSet(attrSet) {

        // whether to remove explicit attributes equal to the attribute defaults
        const autoClear = !this.styleSheets && this.config.autoClear;
        // the resulting reduced attribute set
        const reducedAttrSet = dict.create();

        // copy all new attributes that change a value, or clear an existing value
        dict.forEach(attrSet, (newAttrs, family) => {

            // skip unsupported attribute families
            if (!this.supportsFamily(family)) { return; }

            // the explicit attributes of the current family
            const oldExplAttrs = this._explAttrSet[family];
            // the attribute values for comparison, according to "autoClear" mode
            const oldAttrValues = autoClear ? this._mergedAttrSet[family] : oldExplAttrs;

            // new attribute is null: clear existing attributes; otherwise copy attributes that will change the current values
            dict.forEach(newAttrs, (value, name) => {
                if (value === null) {
                    if (oldExplAttrs && (name in oldExplAttrs)) {
                        insertAttribute(reducedAttrSet, family, name, null);
                    }
                } else {
                    const hasAttrs = oldExplAttrs && (name in oldExplAttrs);
                    if ((!autoClear && !hasAttrs) || !_.isEqual(value, oldAttrValues[name])) {
                        insertAttribute(reducedAttrSet, family, name, value);
                    }
                }
            });
        });

        return reducedAttrSet;
    }

    /**
     * Returns an attribute set that will restore the current state of the
     * explicit attributes, after they have been changed according to the
     * passed attribute set. Intended to be used to create undo operations.
     *
     * @param {PtAttrSet<AttrSetT>} attrSet
     *  An (incomplete) attribute set to be merged over the current explicit
     *  attributes of this instance.
     *
     * @returns {PtAttrSet<AttrSetT>}
     *  An attribute set that will restore the current explicit attributes
     *  after they have been changed according to the passed attributes.
     */
    getUndoAttributeSet(attrSet) {

        const undoAttrSet = dict.create();

        // handle the stylesheet identifier
        if (this.styleSheets) {
            const oldStyleId = this._explAttrSet.styleId;
            const newStyleId = attrSet.styleId;
            if (is.string(newStyleId) && (newStyleId !== oldStyleId)) {
                undoAttrSet.styleId = is.string(oldStyleId) ? oldStyleId : null;
            } else if ((newStyleId === null) && is.string(oldStyleId)) {
                undoAttrSet.styleId = oldStyleId;
            }
        }

        // restore all old attributes that will be changed or cleared
        dict.forEach(this._explAttrSet, (oldAttrs, family) => {

            // the new attributes of the current family (nothing to undo, if the family will not be changed)
            const newAttrs = attrSet[family];
            if (!is.dict(newAttrs)) { return; }

            // copy the old attribute values to the undo attribute set, if it will change
            dict.forEach(oldAttrs, (value, name) => {
                if ((name in newAttrs) && !_.isEqual(value, newAttrs[name])) {
                    insertAttribute(undoAttrSet, family, name, value);
                }
            });
        });

        // clear all new attributes with the undo operation, that currently do not exist
        dict.forEach(attrSet, (newAttrs, family) => {

            // skip unsupported attribute families
            if (!this.supportsFamily(family)) { return; }

            // the explicit attributes of the current family
            const oldAttrs = this._explAttrSet[family];
            // the definitions of the current family
            const familyPool = this.attrPool.getFamilyPool(family);
            // all attributes to be restored in undo even if they will not be changed
            let restoreAttrs = null;

            // process all new attributes
            for (const { name, entry } of familyPool.yieldAttrs(newAttrs)) {

                // if the attribute does not exist, reset it in undo (with a null value)
                if (!oldAttrs || !(name in oldAttrs)) {
                    insertAttribute(undoAttrSet, family, name, null);
                }

                // if the attribute has been changed, and the definition contains additional
                // attributes to always be restored, add them to the undo attribute set
                const undoAttrs = undoAttrSet[family];
                if (oldAttrs && undoAttrs && (name in undoAttrs)) {
                    if ((entry.undo.length === 1) && (entry.undo[0] === "*")) {
                        restoreAttrs = oldAttrs;
                    } else if (entry.undo.length && (restoreAttrs !== oldAttrs)) {
                        restoreAttrs = to.dict(restoreAttrs, true);
                        for (const attrName of entry.undo) {
                            if (attrName in oldAttrs) {
                                restoreAttrs[attrName] = oldAttrs[attrName];
                            }
                        }
                    }
                }
            }

            // add all attributes to the undo attribute set that need to be updated
            // according to the attribte definition
            if (restoreAttrs) {
                undoAttrSet[family] = { ...restoreAttrs, ...undoAttrSet[family] };
            }
        });

        return undoAttrSet;
    }

    /**
     * Returns whether this attributed model contains the same explicit
     * attributes as the passed attributed model.
     *
     * @param {AttributedModel} otherModel
     *  The other attributed model to be compared to this model.
     *
     * @returns {boolean}
     *  Whether both attributed models contain the same explicit formatting
     *  attributes.
     */
    hasEqualAttributeSet(otherModel) {
        return (this === otherModel) || _.isEqual(this._explAttrSet, otherModel.getExplicitAttributeSet(true));
    }

    /**
     * Changes and/or removes specific explicit formatting attributes, or the
     * stylesheet reference of this model object.
     *
     * @param {PtAttrSet<AttrSetT>} attrSet
     *  An (incomplete) attribute set with all formatting attributes to be
     *  changed. To clear an explicit attribute value (thus defaulting to the
     *  current stylesheet), the respective value in this attribute set has to
     *  be set explicitly to the value `null`. Attributes missing in this
     *  attribute set will not be modified.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.silent=false]
     *    If set to `true`, the event "change:attributes" will never be
     *    triggered. By default, the event will be triggered if at least one
     *    formatting attribute has changed its value.
     *
     * @returns {boolean}
     *  Whether the attributes of this model object have actually changed.
     */
    setAttributes(attrSet, options) {

        // whether to remove explicit attributes equal to the attribute defaults
        const autoClear = this.config.autoClear && !this.styleSheets && !this.parentModel;
        // whether any explicit attribute or the style identifier has been changed
        let changed = false;

        // set new stylesheet or auto style
        if ("styleId" in attrSet) {

            // style collection must exist (this model must support stylesheets at all)
            if (!this.styleSheets) {
                globalLogger.error("$badge{AttributedModel} setAttributes: setting stylesheets not supported");
                return false;
            }

            // the stylesheet identifier from the passed attributes
            let styleId = attrSet.styleId;

            // remove explicit reference to default stylesheet (missing style
            // reference will automatically fall-back to the default stylesheet)
            if (!is.string(styleId) || (styleId === "") || (styleId === this.styleSheets.getDefaultStyleId())) {
                styleId = null;
            }

            // change the stylesheet identifier in the internal map, update the resulting changed flag
            if (updateAttribute(this._explAttrSet, "styleId", styleId)) {
                changed = true;
            }
        }

        // update the remaining explicit attributes of all supported attribute families
        this._supportedFamilies.forEach(family => {

            // attribute definitions of the current family
            const familyPool = this.attrPool.getFamilyPool(family);

            // check if new attributes exist in the passed attribute set
            if (!familyPool || !is.dict(attrSet[family])) { return; }

            // target attribute map for the current family
            const explAttrs = this._explAttrSet[family] ??= dict.create();

            // update the attribute map with the new attribute values
            for (const { name, value, def } of familyPool.yieldAttrs(attrSet[family])) {
                const clearDef = autoClear && _.isEqual(value, def);
                changed = updateAttribute(explAttrs, name, clearDef ? null : value) || changed;
            }

            // remove empty attribute value maps completely
            if (is.empty(explAttrs)) {
                delete this._explAttrSet[family];
            }
        });

        // recalculate merged attributes and notify all listeners
        if (changed || !this._mergedAttrSet) {
            this._calculateMergedAttributeSet(options?.silent);
        }

        return changed;
    }

    /**
     * Updates the merged attribute set, according to the current defaults, the
     * stylesheet and parent model referenced by this instance, and its
     * explicit attributes.
     */
    refreshMergedAttributeSet() {
        this._calculateMergedAttributeSet(true);
    }

    // private methods --------------------------------------------------------

    /**
     * Recalculates the merged attribute set.
     */
    /*private*/ _calculateMergedAttributeSet(silent) {

        // remember the old merged attributes
        const oldMergedAttrSet = this._mergedAttrSet;

        // start with default attribute values of all supported families
        this._mergedAttrSet = this.attrPool.getDefaultValueSet(this._supportedFamilies);

        // add merged attributes of parent model object
        if (this.parentModel) {
            this.attrPool.extendAttrSet(this._mergedAttrSet, this.parentModel.getMergedAttributeSet(true));
        }

        // add the stylesheet attributes to resulting merged attribute set
        let styleId = null;
        if (this.styleSheets) {
            styleId = this._explAttrSet.styleId || "";
            this.attrPool.extendAttrSet(this._mergedAttrSet, this.styleSheets.getStyleAttributeSet(styleId));
            styleId = this._mergedAttrSet.styleId;
        }

        // add the explicit attributes of this instance
        this.attrPool.extendAttrSet(this._mergedAttrSet, this._explAttrSet);

        // delete all unsupported attribute families
        dict.forEachKey(this._mergedAttrSet, family => {
            if (!this.supportsFamily(family)) {
                delete this._mergedAttrSet[family];
            }
        });

        // restore the effective stylesheet identifier
        if (this.styleSheets) {
            this._mergedAttrSet.styleId = styleId;
        }

        // notify all listeners
        if (!silent) {
            this.trigger("change:attributes", this._mergedAttrSet, oldMergedAttrSet);
        }
    }
}
