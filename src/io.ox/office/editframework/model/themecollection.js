/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { MAJOR_FONT_KEY, MINOR_FONT_KEY } from '@/io.ox/office/editframework/utils/attributeutils';

// constants ==============================================================

// the name of the default theme
const DEFAULT_THEME_NAME = 'Larissa';

// the color scheme of the built-in default theme for OOXML documents
const OOXML_BUILTIN_COLOR_SCHEME = {
    text1: '000000',
    text2: '1f497d',
    light1: 'ffffff',
    light2: 'eeece1',
    dark1: '000000',
    dark2: '1f497d',
    background1: 'ffffff',
    background2: 'eeece1',
    accent1: '4f81bd',
    accent2: 'c0504d',
    accent3: '9bbb59',
    accent4: '8064a2',
    accent5: '4bacc6',
    accent6: 'f79646',
    hyperlink: '0000ff',
    followedHyperlink: '800080'
};

// the font scheme of the built-in default theme for OOXML documents
const OOXML_BUILTIN_FONT_SCHEME = {};
OOXML_BUILTIN_FONT_SCHEME[MAJOR_FONT_KEY] = 'Calibri';
OOXML_BUILTIN_FONT_SCHEME[MINOR_FONT_KEY] = 'Calibri';

// the color scheme of the built-in default theme for ODF documents
const ODF_BUILTIN_COLOR_SCHEME = _.extend({}, OOXML_BUILTIN_COLOR_SCHEME, {
    accent1: '729fcf', // LibreOffice Color Shape default background
    accent2: 'ff420e', // LibreOffice Color "Chart 2"
    accent3: 'ffd320', // LibreOffice Color "Chart 3"
    accent4: '579d1c', // LibreOffice Color "Chart 4"
    accent5: '7e0021', // LibreOffice Color "Chart 5"
    accent6: 'ff950e'  // LibreOffice Color "Chart 10" (because 6 looks like 1)
});

// the font scheme of the built-in default theme for ODF documents
const ODF_BUILTIN_FONT_SCHEME = {};
ODF_BUILTIN_FONT_SCHEME[MAJOR_FONT_KEY] = 'Liberation Sans';
ODF_BUILTIN_FONT_SCHEME[MINOR_FONT_KEY] = 'Liberation Sans';

// the keys of all scheme colors, as array
const COLOR_KEYS = Object.keys(OOXML_BUILTIN_COLOR_SCHEME);

// the keys of all scheme fonts, as array
const FONT_KEYS = Object.keys(OOXML_BUILTIN_FONT_SCHEME);

// private functions ==========================================================

/**
 * Generates a unique map key from a theme's name and target.
 *
 * @param {String} name
 *  The name of the theme.
 *
 * @param {String} [target]
 *  The target of the theme. If omitted, an empty string is used.
 *
 * @returns {String}
 *  A unique map key from a theme's name and target.
 */
function generateKey(name, target) {
    return JSON.stringify([name, target || '']);
}

// class ThemeModel ===========================================================

/**
 * Contains the definitions of a single theme in a document.
 *
 * @param {String} name
 *  The name of the theme.
 *
 * @param {Object} colorScheme
 *  The color scheme. MUST be complete according to the specification of
 *  the 'insertTheme' document operation (i.e. MUSt contain RRGGBB strings
 *  for all 12 scheme colors).
 *
 * @param {String} [target]
 *  The optional target of the theme.
 */
class ThemeModel {

    constructor(name, colorScheme, fontScheme, target) {
        this.name = name;
        this.colorScheme = colorScheme;
        this.fontScheme = fontScheme;
        this.target = target;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the name of this theme model.
     *
     * @returns {String}
     *  The name of this theme.
     */
    getName() {
        return this.name;
    }

    /**
     * Returns the target of this theme model.
     *
     * @returns {String}
     *  The name of the target of this theme; or the empty string, if the
     *  theme is not associated with a specific target.
     */
    getTarget() {
        return this.target || '';
    }

    /**
     * Generates a unique map key for this theme's name and target.
     *
     * @returns {String}
     *  A unique map key for this theme's name and target.
     */
    getKey() {
        return generateKey(this.getName(), this.getTarget());
    }

    /**
     * Returns the RGB color value of the specified scheme color key.
     *
     * @param {String} colorKey
     *  The internal key of the scheme color.
     *
     * @param {String} [defRGB]
     *  The default RGB value to be returned, if the specified scheme color
     *  does not exist. If omitted, null will be returned in that case.
     *
     * @returns {String|Null}
     *  The RGB value of the scheme color as hexadecimal string, if
     *  existing, otherwise the passed default RGB value or null.
     */
    getSchemeColor(colorKey, defRGB) {
        const color = this.colorScheme[colorKey];
        return (typeof color === 'string') ? color : (defRGB || null);
    }

    /**
     * Returns the font name of the specified scheme font key.
     *
     * @param {String} fontKey
     *  The internal key of a scheme font.
     *
     * @param {String} [defFont]
     *  The default font name to be returned, if the specified scheme font
     *  does not exist. If omitted, null will be returned in that case.
     *
     * @returns {String|Null}
     *  The name of the specified scheme font, if existing; otherwise the
     *  passed default font name or null.
     */
    getSchemeFont(fontKey, defFont) {
        const fontName = this.fontScheme[fontKey];
        return (typeof fontName === 'string') ? fontName : (defFont || null);
    }
}

// class ThemeCollection ======================================================

/**
 * Contains the definitions of all themes in a document.
 *
 * @param {EditModel} docModel
 *  The document model containing this instance.
 */
export default class ThemeCollection extends ModelObject {

    // themes, mapped by identifier
    #themeMap = new Map/*<string, ThemeModel>*/();
    // the default themes, mapped by target names
    #defaultThemeMap = new Map/*<string, ThemeModel>*/();
    // global default theme (first inserted theme)
    #globalThemeModel;
    // whether the default theme is still the built-in theme
    #isBuiltInTheme = true;

    constructor(docModel) {

        // base constructor
        super(docModel);

        // global default theme (first inserted theme)
        this.#globalThemeModel = new ThemeModel(DEFAULT_THEME_NAME,
            this.docApp.isODF() ? ODF_BUILTIN_COLOR_SCHEME : OOXML_BUILTIN_COLOR_SCHEME,
            this.docApp.isODF() ? ODF_BUILTIN_FONT_SCHEME : OOXML_BUILTIN_FONT_SCHEME
        );

        // initialization -----------------------------------------------------

        // register the built-in default theme
        this.#themeMap.set(this.#globalThemeModel.getKey(), this.#globalThemeModel);
    }

    // protected methods --------------------------------------------------

    /**
     * Callback handler for the document operation 'insertTheme'. Inserts a
     * new theme into this collection.
     *
     * @param {OperationContext} context
     *  A wrapper representing the 'insertTheme' document operation.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is
     *  missing in the operation.
     */
    applyInsertThemeOperation(context) {

        // the name of the theme
        const name = context.getStr('themeName');
        // the color scheme
        const colorScheme = context.getDict('colorScheme');
        // the font scheme
        const fontScheme = context.getDict('fontScheme');
        // the target name for the theme (empty string for missing target allowed)
        let target = context.optStr('target', { empty: true });

        // validate the color scheme
        COLOR_KEYS.forEach(colorKey => {
            const value = colorScheme[colorKey];
            context.ensure((typeof value === 'string') && /^[0-9A-Z]{6}$/i.test(value), 'missing or invalid color ' + colorKey);
        });

        // validate the font scheme
        FONT_KEYS.forEach(fontKey => {
            const value = fontScheme[fontKey];
            context.ensure((typeof value === 'string') && (value.length > 0), 'missing or invalid font ' + fontKey);
        });

        // in Presentation app themes can be assigned to (document) slides -> using document slide id as target (55556)
        if (!target && this.docApp.isPresentationApp() && context.operation.start) { target = this.docModel.getSlideIdByPosition(context.operation.start); }
        // in Presentation app all insertTheme operations need to be saved for undo (63372)
        if (this.docApp.isPresentationApp()) { this.docModel.saveInsertThemeOperation(target, context.operation); }

        // create a theme object with all passed attributes, and insert it into the map
        const themeModel = new ThemeModel(name, colorScheme, fontScheme, target);
        this.#themeMap.set(themeModel.getKey(), themeModel);

        // set first inserted theme without target as new default theme
        if (!target && this.#isBuiltInTheme) {
            this.#globalThemeModel = themeModel;
            this.#isBuiltInTheme = false;
        }

        // set first theme with target as default theme
        if (target && !this.#defaultThemeMap.has(target)) {
            this.#defaultThemeMap.set(target, themeModel);
        }

        // notify listeners
        this.trigger('insert:theme', name, target);
    }

    // public methods -----------------------------------------------------

    /**
     * Returns whether a default theme exists for the specified target.
     *
     * @param {String} target
     *  A target string to specify the theme's target.
     *
     * @returns {Boolean}
     *  Whether a default theme exists for the specified target.
     */
    hasDefaultTheme(target) {
        return this.#defaultThemeMap.has(target);
    }

    /**
     * Returns the default theme for the specified target from this
     * collection.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A single target name or an array of target names to specify the
     *  theme's target.
     *
     * @returns {ThemeModel}
     *  The default theme for the specified target, or the global default
     *  theme.
     */
    getDefaultModel(targets) {

        // single string: resolve theme directly from the map of default themes
        if ((typeof targets === 'string') && (targets.length > 0)) {
            return this.#defaultThemeMap.get(targets) ?? this.#globalThemeModel;
        }

        // array of strings: find the first existing default theme
        if (_.isArray(targets)) {

            let themeModel = null;
            targets.some(target => {
                // empty string addresses the global default theme if inserted explicitly
                const useGlobal = (target === '') && !this.#isBuiltInTheme;
                themeModel = useGlobal ? this.#globalThemeModel : this.#defaultThemeMap.get(target);
                return !!themeModel;
            });

            // no theme found: fall back to global default theme
            return themeModel || this.#globalThemeModel;
        }

        // fall back to global default theme
        return this.#globalThemeModel;
    }

    /**
     * Returns a single theme from this collection.
     *
     * @param {String|Null} [name]
     *  The name of the theme to be returned. If empty or omitted or null
     *  or not existing, the default theme for the target, or the global
     *  default theme will be returned instead.
     *
     * @param {String|Array<String>|Null} [targets]
     *  A single target name or an array of target names to specify the
     *  theme's target.
     *
     * @returns {ThemeModel}
     *  The specified theme if existing, otherwise the default theme for a
     *  target, or the global default theme.
     */
    getThemeModel(name, targets) {

        // missing theme name: resolve default theme by target
        if (!name) { return this.getDefaultModel(targets); }

        // search theme for the specified name
        let themeModel;
        if (!targets || (typeof targets === 'string')) {
            themeModel = this.#themeMap.get(generateKey(name, targets));
        } else if (_.isArray(targets)) {
            targets.some(target => !!(themeModel = this.#themeMap.get(generateKey(name, target))));
        }

        // fall back to global default theme
        return themeModel || this.#globalThemeModel;
    }

}
