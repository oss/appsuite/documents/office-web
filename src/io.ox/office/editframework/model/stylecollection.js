/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import gt from 'gettext';

import { is, itr, set, map, dict, pick, json, uuid, jpromise } from '@/io.ox/office/tk/algorithms';
import { DEBUG } from '@/io.ox/office/tk/config';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import { INSERT_STYLESHEET } from '@/io.ox/office/editframework/utils/operations';
import { getElementStyleId, getExplicitAttributeSet, setExplicitAttributeSet } from '@/io.ox/office/editframework/utils/attributeutils';
import { OperationContext } from '@/io.ox/office/editframework/model/operation/context';

// constants ==============================================================

// the default name for unnamed ODF table styles
const DEFAULT_TABLE_STYLE_NAME = '[' + _.noI18n.fix(gt('Unnamed Style')) + ']';

// class StyleCollection ==================================================

/**
 * Collection for hierarchical style sheets of a specific attribute family.
 *
 * @param {EditModel} docModel
 *  The document model containing this instance.
 *
 * @param {String} styleFamily
 *  The main attribute family represented by the style sheets contained by
 *  instances of the derived collection class. The style sheets and the DOM
 *  elements referring to the style sheets must support all attributes of
 *  this attribute family.
 *
 * @param {Object} [initOptions]
 *  Optional parameters. All callback functions will be called in the
 *  context of this style sheet collection.
 *  @param {Boolean} [initOptions.styleSheetSupport=true]
 *      If set to false, this collection is not allowed to contain style
 *      sheets. It can only be used to format DOM elements with explicit
 *      formatting attributes. DEPRECATED!
 *  @param {AttributePool} [initOptions.attrPool]
 *      The attribute pool to be used. If omitted, the default attribute pool
 *      of the document will be used.
 *  @param {string[]} [initOptions.families]
 *      The names of additional attribute families supported by the style
 *      sheets of this collection. It is not needed to repeat the name of the
 *      style family (as passed to the 'styleFamily' parameter) here.
 *  @param {Object<String,Function>} [initOptions.parentResolvers]
 *      The parent style families whose associated style sheets can contain
 *      attributes of the family supported by this style sheet collection.
 *      The DOM elements referring to the style sheets of the specified
 *      style families must be ancestors of the DOM elements referring to
 *      the style sheets of this container. The passed object maps the
 *      attribute family to an ancestor element resolver function. The
 *      function receives the descendant DOM element as jQuery object in
 *      the first parameter, and returns the ancestor element of that DOM
 *      element which is associated to the parent style family used as map
 *      key.
 *  @param {Function} [initOptions.baseAttributesResolver]
 *      A function that can return additional base attributes for the
 *      specified DOM element. The callback function receives the following
 *      parameters:
 *      (1) {jQuery} element
 *          The DOM element to resolve the base attributes for.
 *      (2) {Object} baseAttributes
 *          The base attribute set already collected for the specified DOM
 *          element (e.g. from its parent elements). MUST NOT be changed by
 *          the callback function!
 *      (3) {Object} [newAttributes]
 *          An optional object containing the new attributes that will be
 *          assigned to the specified DOM element but are not yet assigned.
 *          This is especially important for resolving the attributes from
 *          layout or master slide, if the paragraph level has changed.
 *  @param {Function} [initOptions.styleAttributesResolver]
 *      A function that extracts the effective explicit attribute set from
 *      the original attribute definition of a style sheet. The attributes
 *      returned by this function may depend on a source element (e.g. on
 *      the position of this source element in its parents). The callback
 *      function receives the following parameters:
 *      (1) {Object} styleAttributes
 *          The original attribute definition of the style sheet. MUST NOT
 *          be changed by the callback function!
 *      (2) {jQuery} element
 *          The element referring to the style sheet.
 *      (3) {jQuery} [sourceNode]
 *          The descendant source node.
 *  @param {Function} [initOptions.elementAttributesResolver]
 *      A function that extracts and returns specific attributes from the
 *      explicit attribute set of an element. The attributes returned by
 *      this function may depend on a source element (e.g. on the position
 *      of this source element in its parents). The callback function
 *      receives the following parameters:
 *      (1) {Object} elementAttributes
 *          The explicit attribute set of the element. MUST NOT be changed
 *          by the callback function!
 *      (2) {jQuery} element
 *          The element whose explicit attributes have been passed in the
 *          first parameter.
 *      (3) {jQuery} [sourceNode]
 *          The descendant source node.
 */
export class StyleCollection extends ModelObject {

    // the attribute pool to be used for the formatting attributes
    attrPool;

    // The main attribute family represented by the style sheets contained by instances of the derived collection class
    #styleFamily;
    // The initOptions for the constructor
    #initOptions;
    // all registered style sheets, mapped by unique identifier
    #styleSheetMap = new Map/*<string, StyleSheetModel>*/();
    // whether this collection represents the default style family (no "type" property in operations)
    #isDefaultFamily = false;
    // the names of all supported attribute families, as flag set
    #supportedFamilySet;
    // identifier of the default style sheet
    #defaultStyleId = '';
    // whether the default style shall be used, if no style is specified at an element
    #useDefaultStyleIfMissing = true;
    // if set to `true`, the cached data in all style sheets need to be refreshed on next access
    #cachedAttrsInvalid = false;
    // whether this collection supports style sheets
    #styleSheetSupport = true;
    // all registered DOM formatting handlers
    #domFormatHandlers = [];
    // all registered DOM preview formatting handlers
    #domPreviewHandlers = [];

    constructor(docModel, styleFamily, initOptions) {

        // base constructor
        super(docModel);

        this.attrPool = initOptions?.attrPool ?? docModel.defAttrPool;

        this.#styleFamily = styleFamily;
        this.#initOptions = initOptions;
        this.#isDefaultFamily = styleFamily === docModel.getDefaultStyleFamily();
        this.#supportedFamilySet = new Set/*<string>*/([styleFamily]);
        this.#styleSheetSupport = initOptions?.styleSheetSupport ?? true;

        // initialization -----------------------------------------------------

        // add additional attribute families passed in the options
        set.assign(this.#supportedFamilySet, initOptions?.families);

        // delete all cached data when pool defaults have changed
        this.listenTo(this.attrPool, "change:defaults", changedSet => {
            if (this.#styleFamily in changedSet) { this.#cachedAttrsInvalid = true; }
        });
    }

    // public methods -----------------------------------------------------

    /**
     * Returns the style family name of this style sheet collection.
     *
     * @returns {String}
     *  The style family name of this style sheet collection.
     */
    getStyleFamily() {
        return this.#styleFamily;
    }

    /**
     * Returns whether the passed name is a supported attribute family.
     */
    isSupportedFamily(family) {
        return this.#supportedFamilySet.has(family);
    }

    /**
     * Returns an iterator with the names of all attribute families
     * supported by style sheets contained in this instance, including the
     * main style family.
     *
     * @returns {IterableIterator<string>}
     *  An iterator producing the names of all supported attribute
     *  families.
     */
    yieldSupportedFamilies() {
        return this.#supportedFamilySet.values();
    }

    /**
     * Removes all attribute maps from the passed attribute set in-place
     * that are not mapped by an attribute family supported by this style
     * sheet collection. Additionally, all existing but empty attribute
     * maps will be removed.
     *
     * @param {Object} attributeSet
     *  (in/out parameter) The attribute set to be filtered. All properties
     *  of this object whose key is not a supported attribute family will
     *  be deleted.
     *
     * @returns {Object}
     *  The filtered attribute set passed to this method (has been modified
     *  in-place), for convenience.
     */
    filterBySupportedFamilies(attributeSet) {

        // remove all properties not keyed by a supported attribute family
        _.each(attributeSet, (attributes, family) => {
            if (!this.#supportedFamilySet.has(family) || !is.dict(attributes) || is.empty(attributes)) {
                delete attributeSet[family];
            }
        });

        // return the modified attribute set for convenience
        return attributeSet;
    }

    /**
     * Removes all unsupported attributes from the passed attribute set
     * in-place. Additionally, all existing but empty attribute maps will
     * be removed.
     *
     * @param {Object} attributeSet
     *  (in/out parameter) The attribute set to be filtered.
     *
     * @returns {Object}
     *  The filtered attribute set passed to this method (has been modified
     *  in-place), for convenience.
     */
    filterBySupportedAttributes(attributeSet) {

        _.each(attributeSet, (attributes, family) => {

            // attribute definitions of the current family
            const familyPool = this.#supportedFamilySet.has(family) && this.attrPool.getFamilyPool(family);

            // process all attributes, if the current attribute family is supported
            if (familyPool) {
                dict.forEachKey(attributes, name => {
                    if (!familyPool.hasEntry(name)) {
                        delete attributes[name];
                    }
                });

                if (is.empty(attributes)) {
                    delete attributeSet[family];
                }
                return;
            }

            // delete the current property, unless it is a (supported) style sheet reference
            if (!this.#styleSheetSupport || (family !== 'styleId')) {
                delete attributeSet[family];
            }
        });

        // return the modified attribute set for convenience
        return attributeSet;
    }

    /**
     * Returns the identifier of the default style sheet.
     *
     * @returns {string}
     *  The identifier of the default style sheet; or the empty string, if
     *  no explicit default style sheet has been imported.
     */
    getDefaultStyleId() {
        return this.#defaultStyleId;
    }

    /**
     * Specifying whether the default style shall be used, if no style ID is specified
     * for an element.
     *
     * @param {Boolean} value
     *  Whether the default style shall be used, if no style ID is specified for an element.
     */
    setUseDefaultStyleIfMissing(value) {
        this.#useDefaultStyleIfMissing = value;
    }

    /**
     * Inserts a new 'dirty' (predefined) style sheet into this collection.
     *
     * @param {String} styleId
     *  The unique identifier of of the new style sheet.
     *
     * @param {String} styleName
     *  The user-defined name of of the new style sheet.
     *
     * @param {Object} attributeSet
     *  The formatting attributes contained in the new style sheet. The
     *  structure of this object is dependent on the style family of this
     *  collection.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {String} [options.parent]
     *      The identifier of of the parent style sheet the new style sheet
     *      will derive undefined attributes from.
     *  @param {String} [options.category]
     *      If specified, the name of a category that can be used to group
     *      style sheets into several sections in the user interface.
     *  @param {Number} [options.priority=0]
     *      The sorting priority of the style (the lower the value the
     *      higher the priority).
     *  @param {Boolean} [options.defStyle=false]
     *      True, if the new style sheet is the default style sheet of this
     *      style sheet collection. The default style will be used for all
     *      elements without explicit style sheet. Only the first style
     *      sheet that will be inserted with this flag will be permanently
     *      registered as default style sheet.
     *  @param {Boolean} [options.hidden=false]
     *      If set to true, the style sheet should be displayed in the user
     *      interface.
     *  @param {Boolean} [options.builtIn=false]
     *      Whether the style sheet is predefined (provided in a new empty
     *      document).
     *  @param {Boolean} [options.custom=false]
     *      Whether the style sheet has been customized (changed by the
     *      user, especially useful for built-in style sheets).
     *
     * @returns {Boolean}
     *  Whether the new style sheet has been created successfully.
     */
    createDirtyStyleSheet(styleId, styleName, attributeSet, options) {

        // the document operation that will be executed to insert the style sheet
        const operation = this.#createOperationProperties(styleId, { styleName, attrs: attributeSet, dirty: true });

        // add parent style identifier
        if (options?.parent) { operation.parent = options?.parent; }

        // add other options supported by the operation
        operation.uiPriority = options?.priority || 0;
        operation.hidden = !!options?.hidden;
        operation.custom = !!options?.custom;

        // apply the operation (should not throw, just in case)
        try {
            const context = new OperationContext(this.docModel, operation, false);
            this.applyInsertStyleSheetOperation(context, styleSheet => {
                styleSheet.category = options?.category ?? null;
                styleSheet.builtIn = !!options?.builtIn;
                styleSheet.dirty = true;
            });
            return true;
        } catch (error) {
            globalLogger.exception(error);
            return false;
        }
    }

    /**
     * Returns whether this collection contains the specified style sheet.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {Boolean}
     *  Whether this instance contains a style sheet with the passed
     *  identifier.
     */
    containsStyleSheet(styleId) {
        return !!styleId && this.#styleSheetMap.has(styleId);
    }

    /**
     * Returns whether this collection contains a style sheet with the
     * specified name (not identifier).
     *
     * @param {String} styleName
     *  The name of the style sheet.
     *
     * @returns {Boolean}
     *  Whether this instance contains a style sheet with the passed name.
     */
    containsStyleSheetByName(styleName) {
        styleName = styleName.toLowerCase();
        return itr.some(this.#styleSheetMap.values(), styleSheet => styleSheet.name.toLowerCase() === styleName);
    }

    /**
     * Returns the style identifier of the first style sheet found with the
     * specified name.
     *
     * @param {String} styleName
     *  The name of the style sheet.
     *
     * @returns {String|Null}
     *  The identifier of a style sheet with the passed name; or null, if
     *  no such style sheet exists.
     */
    getStyleIdByName(styleName) {
        styleName = styleName.toLowerCase();
        return itr.mapFirst(this.#styleSheetMap, ([styleId, styleSheet]) => {
            return (styleSheet.name.toLowerCase() === styleName) ? styleId : undefined;
        }) ?? null;
    }

    /**
     * Returns the names of all style sheets in a map, keyed by their
     * unique identifiers.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.skipHidden=false]
     *      If set to true, the names of hidden style sheets will not be
     *      returned.
     *
     * @returns {Object}
     *  A map with all style sheet names, keyed by style sheet identifiers.
     */
    getStyleSheetNames(options) {

        // whether to skip hidden style sheets
        const skipHidden = options?.skipHidden;
        // the result map
        const names = {};

        this.#styleSheetMap.forEach((styleSheet, styleId) => {
            if (!skipHidden || !styleSheet.hidden) {
                names[styleId] = styleSheet.name;
            }
        });
        return names;
    }

    /**
     * Returns whether a specific style sheet is a descendant of another
     * style sheet, including the case that the specified style sheets are
     * identical.
     *
     * @param {String} styleId
     *  The identifier of a style sheet that may be a descendant of the
     *  other style sheet.
     *
     * @param {String} parentStyleId
     *  The identifier of a style sheet that may be a parent of the other
     *  style sheet.
     *
     * @returns {Boolean}
     *  Whether the first style sheet is a descendant of the second style
     *  sheet, or both style sheet identifiers are equal.
     */
    isChildOfOther(styleId, parentStyleId) {

        // check the identifier itself, and all existing parent identifiers
        for (; styleId; styleId = this.getParentId(styleId)) {
            if (styleId === parentStyleId) { return true; }
        }

        return false;
    }

    /**
     * Returns the merged attributes from the specified style sheet and its
     * parent style sheets.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.skipDefaults=false]
     *      If set to true, the returned attribute set will only contain
     *      the explicit attributes of the style sheet and its ancestors,
     *      but NOT the default values for missing attributes. This means
     *      the attribute set returned from this method MAY NOT be
     *      complete.
     *
     * @returns {Dict}
     *  A complete (except when using the option 'skipDefaults', see above)
     *  attribute set (a map of attribute maps with name/value pairs, keyed
     *  by the attribute families, additionally containing the style sheet
     *  reference in the 'styleId' property), containing values for all
     *  supported attributes, collected from the specified style sheet, and
     *  all its parent style sheets, up to the map of default attributes.
     *  For efficiency, a reference to the cached attribute set will be
     *  returned. ATTENTION! For efficiency, a reference to the cached
     *  attribute set will be returned. This object MUST NOT be changed!
     */
    getStyleAttributeSet(styleId, options) {

        // delete all cached attribute sets from all style sheets once (after multiple invalidations)
        if (this.#cachedAttrsInvalid) {
            this.#styleSheetMap.forEach(styleSheet => {
                delete styleSheet.sparseAttrSet;
                delete styleSheet.mergedAttrSet;
            });
            this.#cachedAttrsInvalid = false;
        }

        // the style sheet entry
        const styleSheet = this.#getStyleSheet(this.#getEffectiveStyleId(styleId));
        // the cached incomplete merged style sheet attributes
        let sparseAttrSet = styleSheet ? styleSheet.sparseAttrSet : null;
        // the cached complete merged style sheet attributes
        const mergedAttrSet = styleSheet ? styleSheet.mergedAttrSet : null;

        // the effective style sheet attributes according to passed options
        let attrSet = options?.skipDefaults ? sparseAttrSet : mergedAttrSet;
        if (attrSet) { return attrSet; }

        // calculate the existing attributes of the style sheet, and its ancestors
        if (!sparseAttrSet) {
            sparseAttrSet = this.#resolveStyleAttributeSet(styleId);
            if (styleSheet) { styleSheet.sparseAttrSet = sparseAttrSet; }
        }

        // return the incomplete attributes if requested
        if (options?.skipDefaults) { return sparseAttrSet; }

        // start with the attribute default values, extend with attributes of specified style sheet
        attrSet = this.#resolveBaseAttributeSet();
        this.extendAttrSet(attrSet, sparseAttrSet);
        if (styleSheet) { styleSheet.mergedAttrSet = attrSet; }
        return attrSet;
    }

    /**
     * Returns the merged attribute set of the default style sheet of this
     * collection.
     *
     * @returns {Object}
     *  The complete merged attribute set of the default style sheet of
     *  this collection.
     *  ATTENTION! For efficiency, a reference to the cached attribute set
     *  will be returned. This object MUST NOT be changed!
     */
    getDefaultStyleAttributeSet() {
        return this.getStyleAttributeSet(this.#defaultStyleId);
    }

    /**
     * Returns the UI category for the specified style sheet.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {String}
     *  The UI category. If the style sheet does not exist, returns null.
     */
    getUICategory(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.category : null;
    }

    /**
     * Returns the UI priority for the specified style sheet.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {Number}
     *  The UI priority. If the style sheet does not exist, returns 0.
     */
    getUIPriority(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.priority : 0;
    }

    /**
     * Returns the identifier of the parent style sheet for the specified
     * style sheet.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {String|Null}
     *  The parent id of the style sheet; or null, if no parent exists.
     */
    getParentId(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.parentId : null;
    }

    /**
     * Returns whether the specified style sheet is hidden in the user
     * interface.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {Boolean|Null}
     *  True, if the specified style sheet is hidden; false, if the style
     *  sheet is visible; or null, if the style sheet does not exist.
     */
    isHidden(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.hidden : null;
    }

    /**
     * Returns the user defined name for the specified style sheet.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {String|Null}
     *  The user defined name of the style sheet; or null, if the style
     *  sheet does not exist.
     */
    getName(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.name : null;
    }

    /**
     * @param {String} styleId
     *
     * @returns {Boolean|Null}
     */
    isCustom(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.custom : null;
    }

    /**
     * Returns whether the specified style sheet is dirty.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @returns {Boolean|Null}
     *  The dirty state of the style sheet; or null, if the style sheet
     *  does not exist.
     */
    isDirty(styleId) {
        const styleSheet = this.#getStyleSheet(styleId);
        return styleSheet ? styleSheet.dirty : null;
    }

    /**
     * Changes the dirty state of the specified style sheet.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @param {Boolean} dirty
     *
     * @returns {StyleCollection}
     *  A reference to this instance.
     */
    setDirty(styleId, dirty) {
        this.#withStyleSheet(styleId, styleSheet => {
            styleSheet.dirty = dirty;
        });
        return this;
    }

    /**
     * Returns the explicit attribute set of the specified style sheet,
     * without resolving the parent style sheets, or converting to the
     * attributes of a specific attribute family.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @param {Boolean} [direct=false]
     *  If set to true, the returned attribute set will be a reference to
     *  the original map stored in this instance, which MUST NOT be
     *  modified! By default, a deep clone of the attribute set will be
     *  returned that can be freely modified.
     *
     * @returns {Object}
     *  The explicit attribute set contained in the style sheet.
     */
    getStyleSheetAttributeMap(styleId, direct) {
        const styleSheet = this.#getStyleSheet(styleId);
        return !styleSheet ? {} : (direct === true) ? styleSheet.attributes : _.copy(styleSheet.attributes, true);
    }

    /**
     * Changes all given attributes in the style sheet, does not trigger
     * any listener or change anything in the DOM element is in use for
     * attributes: priority, dirty, builtIn.
     *
     * @param {String} styleId
     *
     * @param {Object} [options]
     *
     * @returns {StyleCollection}
     *  A reference to this instance.
     */
    setStyleOptions(styleId, options) {
        this.#withStyleSheet(styleId, styleSheet => {
            _.extend(styleSheet, options);
        });
        return this;
    }

    /**
     * Builds a complete attribute set containing all formatting attributes
     * of the own style family, and of the additional attribute families
     * supported by the style sheets in this collection, all set to the
     * null value.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.style=false]
     *      If set to true, the resulting attribute set will contain an
     *      additional property 'styleId' (reference to the style sheet),
     *      also set to the value null.
     *
     * @returns {Object}
     *  A complete attribute set with null values for all attributes
     *  registered for all supported attribute families.
     */
    buildNullAttributeSet(options) {
        const attributeSet = this.attrPool.buildNullValueSet(this.#supportedFamilySet);
        if (this.#styleSheetSupport && options?.style) {
            attributeSet.styleId = null;
        }
        return attributeSet;
    }

    /**
     * Convenience shortcut to extend the passed attribute sets using the
     * attribute pool of this instance. See description of the method
     * `AttributePool#extendAttrSet` for more details.
     *
     * @param {Object} attrSet1
     *  (in/out) The first attribute set that will be extended in-place.
     *
     * @param {Object|Null} attrSet2
     *  The second attribute set, whose attribute values will be inserted
     *  into the first attribute set. If set to null, the first attribute
     *  set will bot be changed.
     *
     * @param {Object} [options]
     *  Optional parameters. See method `AttributePool#extendAttrSet` for
     *  more details.
     *
     * @returns {Object}
     *  A reference to the first passed and extended attribute set.
     */
    extendAttrSet(attrSet1, attrSet2, options) {
        return this.attrPool.extendAttrSet(attrSet1, attrSet2, options);
    }

    /**
     * Returns the merged values of the formatting attributes from a style
     * sheet and explicit attributes.
     *
     * @param {Object} attributes
     *  An (incomplete) attribute set containing an optional style sheet
     *  identifier in the property 'styleId', and explicit attributes.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.skipDefaults=false]
     *      If set to true, the returned attribute set will NOT contain the
     *      default values of attributes not contained in the style sheet
     *      (or its ancestors). This means the attribute set returned from
     *      this method MAY NOT be complete.
     *
     * @returns {Object}
     *  A complete (except when using the option 'skipDefaults', see above)
     *  merged attribute set with the values of all formatting attributes
     *  supported by this collection.
     */
    getMergedAttributes(attributes, options) {

        // the identifier of the style sheet
        let styleId = pick.string(attributes, 'styleId', '');
        // resulting merged attributes (start with defaults and style sheet attributes)
        const mergedAttributes = _.copy(this.getStyleAttributeSet(styleId, options), true);

        // add the passed explicit attributes (protect effective style sheet identifier)
        styleId = mergedAttributes.styleId;
        this.attrPool.extendAttrSet(mergedAttributes, attributes, options);
        mergedAttributes.styleId = styleId;

        // filter by supported attributes, according to the passed options
        return this.filterBySupportedAttributes(mergedAttributes);
    }

    /**
     * Updates the CSS formatting of the specified DOM element, according
     * to the passed merged attribute set.
     *
     * @param {HTMLElement|jQuery} element
     *  The element to be updated. If this object is a jQuery collection,
     *  uses the first DOM node it contains.
     *
     * @param {Object} mergedAttributes
     *  A complete attribute set with all attribute values merged from
     *  style sheet and explicit attributes.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.preview=false]
     *      If set to true, a GUI element used to preview the formatting of
     *      a style sheet will be rendered. Executes the 'preview' callback
     *      functions instead of the regular 'format' callback functions
     *      registered in the attribute definitions, and calls the preview
     *      handler instead of the format handler registered at this class
     *      instance.
     *  @param {Boolean} [options.async=false]
     *      If set to true, the registered format handler will be asked to
     *      execute asynchronously in a browser timeout loop. MUST NOT be
     *      used together with the option 'preview'.
     *  @param {Boolean} [options.duringImport=false]
     *      If set to true, all formatting handlers will be invoked while
     *      importing the document. By default, only the formatting
     *      handlers that have been registered with the 'duringImport' flag
     *      will be invoked in that case.
     *  @param {Array} [options.collector=undefined]
     *      An optional collector for data that can be collected in the
     *      formatting process. These data can be used after formatting
     *      for further settings (performance).
     *      This is for example used to collect cell border attributes
     *      during table formatting (DOCS-2400).
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the registered format or
     *  preview handlers have finished. If the option 'async' is set, and
     *  the format handlers support asynchronous execution, the promise
     *  will be resolved sometime in the future. Otherwise, the returned
     *  promise is already resolved when this method returns.
     */
    applyElementFormatting(element, mergedAttributes, options) {

        // the passed elemenet, as jQuery object
        const $element = $(element);
        // whether to render a GUI preview element
        const preview = options?.preview;
        // pass the 'async' flag to the format handlers
        const async = !preview && !!options?.async;
        // the array with all registered format handlers (according to preview mode)
        const currFormatHandlers = preview ? this.#domPreviewHandlers : this.#domFormatHandlers;
        // whether to skip element formatting while importing the document
        const skipDuringImport = !this.isImportFinished() && !options?.duringImport;
        // the return value of a format handler
        let formatHandlerValue = null;

        // update information for debugging
        if (!preview && DEBUG) {
            this.DBG_COUNT = (this.DBG_COUNT || 0) + 1;
        }

        // invokes the specified format handler
        const invokeHandler = handlerData => {
            if (!skipDuringImport || handlerData.import) {
                return handlerData.callback.call(this, $element, mergedAttributes, async, options.collector);
            }
        };

        // invoke all registered formatting handlers asynchronously if specified
        // Info: Async is only used for table formatting during loading the (text) document.
        if (async) {
            return this.iterateArraySliced(currFormatHandlers, invokeHandler);
        }

        // invoke all registered formatting handlers synchronously (TODO: some handlers still return pending promises!)
        currFormatHandlers.forEach(handlerData => {
            formatHandlerValue = invokeHandler(handlerData);
        });

        // INFO: Only the table format handler returns a promise.
        // TODO: There are two problems:
        //       1. Although async is NOT specified, this function returns an unresolved promise.
        //       2. If more than one format handler is specified, only the return value of the last
        //          format handler is used here.

        return jpromise.is(formatHandlerValue) ? formatHandlerValue : this.createResolvedPromise();
    }

    /**
     * Calculates the merged attribute set, and updates the CSS formatting
     * of the specified DOM element, according to its current explicit
     * attributes and style settings.
     *
     * @param {HTMLElement|jQuery} element
     *  The element to be updated. If this object is a jQuery collection,
     *  uses the first DOM node it contains.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Object} [options.baseAttributes]
     *      An attribute set with all attribute values of the ancestor of
     *      the passed element, merged from style sheet and explicit
     *      attributes, keyed by attribute family. Used internally while
     *      updating the formatting of all child elements of a node.
     *  @param {Boolean} [options.preview=false]
     *      If set to true, a GUI element used to preview the formatting of
     *      a style sheet will be rendered. Executes the 'preview' callback
     *      functions instead of the regular 'format' callback functions
     *      registered in the attribute definitions, and calls the preview
     *      handler instead of the format handler registered at this class
     *      instance.
     *  @param {Boolean} [options.async=false]
     *      If set to true, the registered format handler will be asked to
     *      execute asynchronously in a browser timeout loop. MUST NOT be
     *      used together with the option 'preview'.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved when the registered format or
     *  preview handlers have finished. If the option 'async' is set, and
     *  the format handlers support asynchronous execution, the promise
     *  will be resolved sometime in the future. Otherwise, the returned
     *  promise is already resolved when this method returns.
     */
    updateElementFormatting(element, options) {

        // the resulting merged attribute set
        let mergedAttributes = null;

        // caller may pass precalculated base attributes, e.g. of a parent DOM element
        if (options?.baseAttributes) {
            mergedAttributes = this.attrPool.getDefaultValueSet(this.#supportedFamilySet); // all families required
            this.attrPool.extendAttrSet(mergedAttributes, options.baseAttributes);
        } else {
            mergedAttributes = this.#resolveBaseAttributeSet(element);
        }

        // extend with custom base attributes returned by user callback function
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveCustomBaseAttributeSet(element, mergedAttributes));

        // add attributes of the style sheet and its parents
        const styleId = getElementStyleId(element);
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveStyleAttributeSet(styleId, element));

        // add the explicit attributes of the element
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveElementAttributeSet(element));

        // apply the CSS formatting of the DOM element
        return this.applyElementFormatting(element, mergedAttributes, { ...options, duringImport: true });
    }

    /**
     * Returns the values of the formatting attributes in the specified DOM
     * element.
     *
     * @param {HTMLElement|jQuery} element
     *  The element whose attributes will be returned. If this object is a
     *  jQuery collection, uses the first DOM node it contains.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {HTMLElement|jQuery} [options.sourceNode]
     *      A descendant of the passed element associated to a child
     *      attribute family. Will be passed to a style attribute resolver
     *      callback function where it might be needed to resolve the
     *      correct attributes according to the position of this source
     *      node.
     *  @param {Object} [options.baseAttributes]
     *      An attribute set with all attribute values of the ancestor of
     *      the passed element, merged from style sheet and explicit
     *      attributes, keyed by attribute family. Used internally while
     *      updating the formatting of all child elements of a node.
     *  @param {Boolean} [options.skipPoolDefaults=false]
     *      If set to true, the pool default values will not be included
     *      into the generated merged attributes. This is especially useful,
     *      when operations shall be generated with all available attributes,
     *      for example after copy/paste of template drawings. In this case
     *      it is better to omit those attributes, that are set by default.
     * @param {String} [options.ignoreFamily=undefined]
     *      An optionally specified string to specify an attribute family, that
     *      shall be ignored. For task DOCS-4211 this is the familiy "character",
     *      because the character attributes must not be inherited from ancestor
     *      nodes of text spans (paragraphs, drawings, ...) in OOXML.
     *
     * @returns {Object}
     *  A complete merged attribute set with the values of all formatting
     *  attributes supported by this collection.
     */
    getElementAttributes(element, options) {

        // the identifier of the style sheet referred by the element
        let styleId = getElementStyleId(element);
        // resulting merged attributes (start with defaults, parent element, and style sheet attributes)
        let mergedAttributes = null;
        // whether the default values should be part of the merged attributes
        const skipPoolDefaults = options?.skipPoolDefaults;
        // an attribute family that must be ignored for the specified element (DOCS-4211)
        const ignoreFamily = options?.ignoreFamily;

        if (options?.baseAttributes) {
            mergedAttributes = skipPoolDefaults ? {} : this.attrPool.getDefaultValueSet(this.#supportedFamilySet); // all families required
            this.attrPool.extendAttrSet(mergedAttributes, options.baseAttributes);
        } else {
            mergedAttributes = this.#resolveBaseAttributeSet(element, { skipPoolDefaults, ignoreFamily });
        }

        // extend with custom base attributes returned by user callback function
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveCustomBaseAttributeSet(element, mergedAttributes));

        // add attributes of the style sheet and its parents
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveStyleAttributeSet(styleId, element, options?.sourceNode));

        // add the explicit attributes of the element (protect effective style sheet identifier)
        styleId = mergedAttributes.styleId;
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveElementAttributeSet(element, options?.sourceNode, ignoreFamily), options);
        mergedAttributes.styleId = styleId;

        // filter by supported attributes, according to the passed options
        return this.filterBySupportedAttributes(mergedAttributes);
    }

    /**
     * Changes specific formatting attributes in the specified DOM element.
     *
     * @param {HTMLElement|jQuery} element
     *  The element whose attributes will be changed. If this object is a
     *  jQuery collection, uses the first DOM node it contains.
     *
     * @param {Object} attributes
     *  An (incomplete) attribute set with all formatting attributes to be
     *  changed. To clear an explicit attribute value (thus defaulting to
     *  the current style sheet), the respective value in this attribute
     *  set has to be set explicitly to the null value. Attributes missing
     *  in this attribute set will not be modified at the element.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.clear=false]
     *      If set to true, explicit element attributes that are equal to
     *      the attributes of the current style sheet will be removed from
     *      the element.
     *  @param {Boolean} [options.preview=false]
     *      If set to true, a GUI element used to preview the formatting of
     *      a style sheet will be rendered. Executes the 'preview' callback
     *      functions instead of the regular 'format' callback functions
     *      registered in the attribute definitions, and calls the preview
     *      handler instead of the format handler registered at this class
     *      instance.
     *  @param {Function} [options.changeListener]
     *      If specified, will be called if the attributes of the element
     *      have been changed. Will be called in the context of this style
     *      sheet collection. Receives the passed element as first
     *      parameter, the old explicit attribute set as second parameter,
     *      and the new explicit attribute set as third parameter.
     *
     * @returns {StyleCollection}
     *  A reference to this instance.
     */
    setElementAttributes(element, attributes, options) {

        // whether to remove element attributes equal to style attributes
        const clear = options?.clear;
        // whether a preview element is rendered
        const preview = options?.preview;
        // new style sheet identifier
        let styleId = attributes.styleId;

        // the existing explicit element attributes
        const oldElementAttributes = getExplicitAttributeSet(element, true);
        // new explicit element attributes (clone, there may be multiple elements pointing to the same data object)
        const newElementAttributes = _.copy(oldElementAttributes, true);
        // merged attribute values from style sheets and explicit attributes
        const mergedAttributes = this.#resolveBaseAttributeSet(element);

        // add/remove new style sheet identifier, or get current style sheet identifier
        if (is.string(styleId)) {
            newElementAttributes.styleId = styleId;
        } else if (is.null(styleId)) {
            delete newElementAttributes.styleId;
        } else {
            styleId = getElementStyleId(element);
        }

        // extend with custom base attributes returned by user callback function
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveCustomBaseAttributeSet(element, mergedAttributes, attributes));

        // collect all attributes of the new or current style sheet, and its parents
        this.attrPool.extendAttrSet(mergedAttributes, this.#resolveStyleAttributeSet(styleId, element));

        // add or remove the passed explicit attributes
        attributes = _.copy(attributes, true);
        this.filterBySupportedFamilies(attributes);
        _.each(attributes, (values, family) => {
            if (is.dict(values)) {

                // definitions of own attributes
                const familyPool = this.attrPool.getFamilyPool(family);
                // add an attribute map for the current family
                const elementAttributeValues = newElementAttributes[family] || (newElementAttributes[family] = {});

                // update the attribute map with the passed attributes
                for (const { value, name } of familyPool.yieldAttrs(values)) {
                    // check whether to clear the attribute
                    if ((value === null) || (clear && _.isEqual(mergedAttributes[family][name], value))) {
                        delete elementAttributeValues[name];
                    } else {
                        elementAttributeValues[name] = value;
                    }
                }

                // remove empty attribute value maps completely
                if (is.empty(elementAttributeValues)) {
                    delete newElementAttributes[family];
                }
            }
        });

        // check if any attributes have been changed
        if (preview || !_.isEqual(oldElementAttributes, newElementAttributes)) {

            // write back new explicit attributes to the element
            setExplicitAttributeSet(element, newElementAttributes);

            // merge explicit attributes into style attributes
            this.attrPool.extendAttrSet(mergedAttributes, this.#resolveElementAttributeSet(element), options);

            // apply CSS formatting of the DOM element
            this.applyElementFormatting($(element), mergedAttributes, { preview });

            // call the passed change listener
            options?.changeListener?.call(this, element, oldElementAttributes, newElementAttributes);
        }

        return this;
    }

    // operation generators -----------------------------------------------

    /**
     * Generates an "insertStyleSheet" operation, if the specified style
     * sheet exists in this style sheet collection, and is marked dirty.
     *
     * @param {StyleCache} styleCache
     *  The style operation cache to be filled with the operations.
     *
     * @param {string} styleId
     *  The identifier of the style sheet.
     */
    generateMissingStyleSheetOperations(styleCache, styleId) {

        const cacheKey = `${this.#styleFamily}:${styleId}`;
        if (styleCache.dirtyStyleIdCache.has(cacheKey)) { return; }
        styleCache.dirtyStyleIdCache.add(cacheKey);

        map.visit(this.#styleSheetMap, styleId, styleSheet => {

            // nothing to do, if the style sheet is not dirty
            if ((styleId === this.#defaultStyleId) || !styleSheet.dirty) { return; }

            // DOCS-1966: In concurrent editing, the style sheet may be reused by other users, which would cause
            // errors if this user deletes the style sheet with an undo action afterwards. Therefore, generated
            // style sheets will never be deleted from now on. This implies also, that a generated style sheet must
            // not be re-created from a REDO action of the undo manager. To achieve this, a special marker flag
            // will be added to the operation which will cause to filter the operation when creating the undo/redo
            // action.

            // the properties for the shyle sheet operations
            const properties = { styleName: styleSheet.name, attrs: styleSheet.attributes, _SKIP_REDO_: true };
            if (styleSheet.parentId) { properties.parent = styleSheet.parentId; }
            if (is.number(styleSheet.priority)) { properties.uiPriority = styleSheet.priority; }
            if (styleSheet.hidden) { properties.hidden = true; }
            if (styleSheet.custom) { properties.custom = true; }

            // generate the style sheet operations
            styleCache.generateStyleOperation(INSERT_STYLESHEET, this.#styleFamily, styleId, properties);
            // generator.generateStyleOperation(Operations.DELETE_STYLESHEET, this.#styleFamily, styleId, null, { undo: true, prepend: true });
        });
    }

    // protected methods --------------------------------------------------

    /**
     * Registers a callback function that will be called for every DOM
     * element whose attributes have been changed, and that will format the
     * DOM element according to the new attribute values.
     *
     * @param {Function} formatHandler
     *  The callback function that implements formatting of a DOM element.
     *  Receives the following parameters:
     *  (1) {jQuery} element
     *      The element whose attributes have been changed.
     *  (2) {Object} mergedAttributes
     *      The complete attribute set merged from style sheet attributes,
     *      and explicit attributes.
     *  (3) {Boolean} async
     *      If set to true, the format handler may execute asynchronously
     *      in a browser timeout loop. In this case, the handler MUST
     *      return a promise that will be resolved after execution.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.duringImport=false]
     *      If set to true, the format handler will also be called when the
     *      attributes of a DOM element have been changed while importing
     *      the document. By default, registered formatting handlers will
     *      not be called during import.
     *
     * @returns {StyleCollection}
     *  A reference to this instance.
     */
    registerFormatHandler(formatHandler, options) {
        this.#domFormatHandlers.push({
            callback: formatHandler,
            import: !!options?.duringImport
        });
        return this;
    }

    /**
     * Registers a callback function that will be called if a DOM element
     * used in the GUI to show a preview of a style sheet needs to be
     * formatted.
     *
     * @param {Function} previewHandler
     *  The callback function that implements preview formatting of a DOM
     *  element. Receives the following parameters:
     *  (1) {jQuery} element
     *      The element whose attributes have been changed.
     *  (2) {Object} mergedAttributes
     *      The complete attribute set merged from style sheets attributes,
     *      and explicit attribute.
     *  The function MUST run synchronously.
     *
     * @returns {StyleCollection}
     *  A reference to this instance.
     */
    registerPreviewHandler(previewHandler) {
        this.#domPreviewHandlers.push({
            callback: previewHandler,
            import: true
        });
        return this;
    }

    /**
     * Callback handler for the document operation 'insertStyleSheet'.
     * Inserts a new style sheet into this collection.
     *
     * @param {OperationContext} context
     *  A wrapper representing the 'insertStyleSheet' document operation.
     *
     * @param {Function} [callback]
     *  A callback that will be invoked with the new style sheet as first
     *  parameter. Used by internal code to create specialized style sheets
     *  with some non-standard properties.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is
     *  missing in the operation.
     */
    applyInsertStyleSheetOperation(context, callback) {

        // check that style sheet handling is enabled
        context.ensure(this.#styleSheetSupport, 'creating style sheets not allowed for style family \'%s\'', this.#styleFamily);

        // the unique identifier of the style sheet
        const styleId = context.getStr('styleId');

        // TODO: disallow multiple insertion of style sheets? (predefined style sheets may be created on demand without checking existence)
        let skipStyleSheet = false;
        map.visit(this.#styleSheetMap, styleId, styleSheet => {
            // bug 55470: do not overwrite an existing registered style sheet with a dirty style sheet
            if (styleSheet.dirty) {
                skipStyleSheet = false;
            } else if (context.operation.dirty) {
                skipStyleSheet = true;
            } else {
                // for now, warn but do not fail for duplicate style sheets
                globalLogger.warn('StyleCollection.applyInsertStyleSheetOperation(): style sheet \'%s\' exists already', styleId);
                skipStyleSheet = false;
            }
        });
        if (skipStyleSheet) { return; }

        // get or create a style sheet object, set identifier
        const styleSheet = map.upsert(this.#styleSheetMap, styleId, dict.create);
        styleSheet.id = styleId;

        // the user-defined name of the style sheet (empty string is allowed)
        styleSheet.name = context.optStr('styleName', styleId, { empty: true });

        // bug 56231: In ODF format there is not always a style name specified. In this case the filter sends
        // the style ID as style sheet name. The user should not see the ID in the dialogs. Therefore a default
        // table style name needs to be used.
        if (this.docModel.docApp.isODF() && (styleSheet.name === styleId) && uuid.is(styleId, true)) {
            styleSheet.name = DEFAULT_TABLE_STYLE_NAME;
        }

        // set parent of the style sheet, check for cyclic references
        styleSheet.parentId = context.optStr('parent');
        if (this.#isDescendantStyleSheet(this.#getStyleSheet(styleSheet.parentId), styleSheet)) {
            globalLogger.warn('StyleCollection.applyInsertStyleSheetOperation(): cyclic reference, cannot set style sheet parent "' + styleSheet.parentId + '"');
            styleSheet.parentId = null;
        }

        // set style sheet options
        styleSheet.hidden = context.optBool('hidden');
        styleSheet.priority = context.optInt('uiPriority');
        styleSheet.custom = context.optBool('custom');

        // bug 27716: set first visible style as default style, if imported default style is hidden
        this.#withStyleSheet(this.#defaultStyleId, defStyleSheet => {
            if (defStyleSheet.hidden && !styleSheet.hidden) {
                this.#defaultStyleId = styleId;
            }
        });

        // set default style sheet
        if (context.optBool('default')) {
            if (this.#defaultStyleId === '') {
                this.#defaultStyleId = styleId;
            } else if (this.#defaultStyleId !== styleId) {
                globalLogger.warn('StyleCollection.applyInsertStyleSheetOperation(): multiple default style sheets "' + this.#defaultStyleId + '" and "' + styleId + '"');
            }
        }

        // store a deep clone of the passed attributes
        styleSheet.attributes = json.deepClone(context.optDict('attrs')) ?? {};

        // reset the dirty flag
        styleSheet.dirty = false;

        // delete all cached merged attributes (parent tree may have changed)
        this.#cachedAttrsInvalid = true;

        // invoke user callback
        if (is.function(callback)) { callback.call(this, styleSheet); }

        // notify listeners
        this.trigger('insert:stylesheet', styleId);
    }

    /**
     * Callback handler for the document operation 'deleteStyleSheet'.
     * Removes an existing style sheet from this collection, and triggers a
     * 'delete:stylesheet' on success.
     *
     * @param {OperationContext} context
     *  A wrapper representing the 'deleteStyleSheet' document operation.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is
     *  missing in the operation.
     */
    applyDeleteStyleSheetOperation(context) {

        // get the style sheet to be deleted
        const styleId = context.getStr('styleId');
        const styleSheet = this.#getStyleSheet(styleId);
        context.ensure(styleSheet, 'style sheet \'%s\' does not exist', styleId);

        // default style sheet cannot be deleted
        context.ensure(styleId !== this.#defaultStyleId, 'cannot remove default style sheet');

        // update parent of all style sheets referring to the removed style sheet
        this.#styleSheetMap.forEach(childSheet => {
            if (styleId === childSheet.parentId) {
                childSheet.parentId = styleSheet.parentId;
            }
        });

        // special behavior: built-in style sheets will not be deleted but invalidated
        if (styleSheet.builtIn) {
            styleSheet.dirty = true;
            return;
        }

        // remove the style sheet from the map, and delete all cached merged attributes
        // (parent tree may have changed)
        this.#styleSheetMap.delete(styleId);
        this.#cachedAttrsInvalid = true;

        // notify listeners
        this.trigger('delete:stylesheet', styleId);
    }

    /**
     * Callback handler for the document operation 'changeStyleSheet'.
     * Changes the properties, or formatting attributes of an existing
     * style sheet in this collection, and triggers a 'change:stylesheet'
     * on success.
     *
     * @param {OperationContext} context
     *  A wrapper representing the 'changeStyleSheet' document operation.
     *
     * @throws {OperationError}
     *  If applying the operation fails, e.g. if a required property is
     *  missing in the operation.
     */
    applyChangeStyleSheetOperation(context) {

        // get the style sheet to be changed
        const styleId = context.getStr('styleId');
        const styleSheet = this.#getStyleSheet(styleId);
        context.ensure(styleSheet, 'style sheet \'%s\' does not exist', styleId);

        // change the readable name of the style sheet
        if (context.has('styleName')) { styleSheet.name = context.getStr('styleName'); }

        // change the formatting attributes
        if (context.has('attrs')) { styleSheet.attributes = context.getDict('attrs'); }

        // delete all cached merged attributes (parent tree, or descendant style sheets may have changed)
        this.#cachedAttrsInvalid = true;

        // notify listeners
        this.trigger('change:stylesheet', styleId);
    }

    // private methods ----------------------------------------------------

    /**
     * Returns the style sheet with the specified identifier.
     *
     * @param {String|Null} styleId
     *  The identifier of a style sheet.
     *
     * @returns {Object|Null}
     *  The style sheet model with the specified identifier.
     */
    #getStyleSheet(styleId) {
        return (styleId && this.#styleSheetMap.get(styleId)) ?? null;
    }

    /**
     * Invokes a callback function, if the style sheet with the specified
     * identifier exists.
     *
     * @param {String|Null} styleId
     *  The identifier of a style sheet.
     *
     * @param {Function} callback
     *  The callback function that will be invoked if the specified style
     *  sheet exists. Receives the style sheet mdoel as first parameter.
     */
    #withStyleSheet(styleId, callback) {
        const styleSheet = this.#getStyleSheet(styleId);
        if (styleSheet) { callback.call(this, styleSheet); }
    }

    /**
     * Returns the identifier of the style sheet that will be effectively
     * used for the specified style sheet identifier. If the specified
     * style sheet does not exist, but this collection contains a default
     * style sheet, its unique identifier will be returned instead.
     * Otherwise, the passed style sheet identifier will be returned.
     *
     * @param {String} styleId
     *  The unique identifier of a style sheet.
     *
     * @returns {String}
     *  The passed style sheet identifier, if such a style sheet exists in
     *  this collection, otherwise the identifier of the default style
     *  sheet.
     */
    #getEffectiveStyleId(styleId) {
        return (!this.containsStyleSheet(styleId) && this.containsStyleSheet(this.#defaultStyleId)) ? this.#defaultStyleId : styleId;
    }

    /**
     * Returns whether the passed style sheet is a descendant of the other
     * passed style sheet.
     */
    #isDescendantStyleSheet(styleSheet, ancestorStyleSheet) {
        while (styleSheet) {
            if (styleSheet === ancestorStyleSheet) { return true; }
            styleSheet = this.#getStyleSheet(styleSheet.parentId);
        }
        return false;
    }

    /**
     * Returns the complete attributes of an ancestor of the passed
     * element, if a parent style family has been registered and its
     * element resolver function returns a valid ancestor element, and
     * extends missing attributes by their current default values.
     *
     * @param {HTMLElement|jQuery} [element]
     *  An element whose ancestor will be searched and queried for its
     *  attributes. If this object is a jQuery collection, uses the first
     *  DOM node it contains. If missing, returns just the current default
     *  values of all supported attribute families.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.skipPoolDefaults=false]
     *      If set to true, the pool default values will not be included
     *      into the generated base attribute set. This is especially useful,
     *      when operations shall be generated with all available attributes,
     *      for example after copy/paste of template drawings. In this case
     *      it is better to omit those attributes, that are set by default.
     *  @param {Boolean} [options.ignoreFamily=undefined]
     *      The name of an attribute family that must be ignored for the
     *      specified element (DOCS-4211).
     *
     * @returns {Object}
     *  The formatting attributes of an ancestor of the passed element, as
     *  map of attribute value maps (name/value pairs), keyed by attribute
     *  family. If no ancestor element has been found, returns the current
     *  default values for all supported attribute families.
     */
    #resolveBaseAttributeSet(element, options) {

        // passed element, as jQuery object
        const $element = $(element);
        // the resulting merged attributes of the ancestor element
        const baseAttributeSet = {};
        // whether the default attributes shall be part of the base attribute set
        const skipPoolDefaults = options?.skipPoolDefaults;
        // an attribute family that must be ignored for the specified element (DOCS-4211)
        const ignoreFamilyOption = options?.ignoreFamily;

        // collect attributes from ancestor element if specified (only one parent style family must match at a time)
        if ($element.length > 0) {

            // find a matching ancestor element and its style family (only one parent must match at a time)
            let parentElement = null;
            let parentFamily = null;
            const parentResolvers = this.#initOptions?.parentResolvers;
            if (parentResolvers) {
                _.some(parentResolvers, (elementResolver, family) => {
                    parentElement = $(elementResolver.call(this, $element));
                    parentFamily = family;
                    return parentElement.length > 0;
                });
            }

            // add the element attributes of the ancestor element (only the supported attribute families)
            if (parentElement && (parentElement.length > 0)) {
                const parentStyleCollection = this.docModel.getStyleCollection(parentFamily);
                const ignoreFamily = ignoreFamilyOption || this.#initOptions.ignoreFamilyAtParents; // DOCS-4211
                const parentAttributes = parentStyleCollection.getElementAttributes(parentElement, { sourceNode: $element, skipPoolDefaults, ignoreFamily });
                this.extendAttrSet(baseAttributeSet, parentAttributes);
            }
        }

        // add missing default attribute values of supported families not found in the parent element
        if (!skipPoolDefaults) {
            for (const family of this.#supportedFamilySet) {
                if (!(family in baseAttributeSet)) {
                    baseAttributeSet[family] = this.attrPool.getDefaultValues(family);
                }
            }
        }

        // remove attribute maps of unsupported families (this also removes the style identifier of the parent element)
        this.filterBySupportedFamilies(baseAttributeSet);

        return baseAttributeSet;
    }

    /**
     * Invokes the custom resolver callback for additional base attributes.
     *
     * @param {jQuery|HTMLElement} element
     *  The affected DOM element. Will be passed to the custom attributes
     *  resolver (see the option 'baseAttributesResolver' passed to the
     *  constructor). If this object is a jQuery collection, uses the first
     *  DOM node it contains.
     *
     * @param {Object} baseAttributes
     *  An attribute set with the base attributes already collected for the
     *  passed DOM element.
     *
     * @param {Object} [newAttributes]
     *  An optional object containing the new attributes that will be assigned
     *  to the DOM element but are not yet assigned. This is especially important
     *  for resolving the attributes from layout or master slide, if the paragraph
     *  level has changed (resolveLayoutAttributes is called before the new
     *  attributes are assigned to the paragraph).
     */
    #resolveCustomBaseAttributeSet(element, baseAttributes, newAttributes) {
        const baseAttributesResolver = this.#initOptions?.baseAttributesResolver;
        return baseAttributesResolver ? baseAttributesResolver.call(this, $(element), baseAttributes, newAttributes) : null;
    }

    /**
     * Returns the attributes from the specified style sheet and its parent
     * style sheets. Does not add default attribute values, or attributes
     * from ancestors of the passed element.
     *
     * @param {String} styleId
     *  The unique identifier of the style sheet.
     *
     * @param {HTMLElement|jQuery} [element]
     *  An element referring to a style sheet in this collection whose
     *  attributes will be extracted. Will be passed to a custom style
     *  attributes resolver (see the option 'styleAttributesResolver'
     *  passed to the constructor). If this object is a jQuery collection,
     *  uses the first DOM node it contains.
     *
     * @param {HTMLElement|jQuery} [sourceNode]
     *  The source DOM node corresponding to a child attribute family that
     *  has initiated the call to this method. Will be passed to a custom
     *  style attributes resolver (see the option 'styleAttributesResolver'
     *  passed to the constructor). If this object is a jQuery collection,
     *  uses the first DOM node it contains.
     *
     * @returns {Object}
     *  The formatting attributes contained in the style sheet and its
     *  ancestor style sheets, as map of attribute value maps (name/value
     *  pairs), keyed by attribute family.
     */
    #resolveStyleAttributeSet(styleId, element, sourceNode) {

        // passed element, as jQuery object
        const $element = $(element);
        // passed source node, as jQuery object
        const $sourceNode = $(sourceNode);
        // the resulting merged attributes of the style sheet and its ancestors
        const styleAttributeSet = {};

        // collects style sheet attributes recursively through parents
        const collectStyleAttributes = styleSheet => {

            // call recursively to get the attributes of the parent style sheets
            this.#withStyleSheet(styleSheet.parentId, collectStyleAttributes);

            // add all attributes of the current style sheet, mapped directly by attribute family
            this.extendAttrSet(styleAttributeSet, styleSheet.attributes);

            // try user-defined resolver for style attributes mapped in non-standard structures
            const styleAttributesResolver = this.#initOptions?.styleAttributesResolver;
            if (styleAttributesResolver && ($element.length > 0)) {
                this.extendAttrSet(styleAttributeSet, styleAttributesResolver.call(this, styleSheet.attributes, $element, $sourceNode));
            }
        };

        // returning empty object, if styleId not specified and fallback to default style not allowed (55547)
        if (!styleId && !this.#useDefaultStyleIfMissing) { return styleAttributeSet; }

        // fall-back to default style sheet if passed identifier is invalid
        styleId = this.#getEffectiveStyleId(styleId);

        // collect attributes from the style sheet and its parents
        this.#withStyleSheet(styleId, collectStyleAttributes);

        // remove attribute maps of unsupported families (and the style identifier)
        this.filterBySupportedFamilies(styleAttributeSet);

        // restore identifier of the style sheet
        styleAttributeSet.styleId = styleId;

        return styleAttributeSet;
    }

    /**
     * Returns the explicit attributes from the specified element. Does not
     * add default attribute values. Uses a custom element attributes
     * resolver function if specified in the constructor of this style
     * sheet collection.
     *
     * @param {HTMLElement|jQuery} element
     *  The element whose explicit attributes will be extracted. Will be
     *  passed to a custom attributes resolver (see the option
     *  'elementAttributesResolver' passed to the constructor). If this
     *  object is a jQuery collection, uses the first DOM node it contains.
     *
     * @param {HTMLElement|jQuery} [sourceNode]
     *  The source DOM node corresponding to a child attribute family that
     *  has initiated the call to this method. Will be passed to a custom
     *  element attributes resolver (see the option
     *  'elementAttributesResolver' passed to the constructor). If this
     *  object is a jQuery collection, uses the first DOM node it contains.
     *
     * @param {String} [ignoreFamily=undefined]
     *  An optionally specified string to specify an attribute family, that
     *  shall be ignored. For task DOCS-4211 this is the familiy "character",
     *  because the character attributes must not be inherited from ancestor
     *  nodes of text spans (paragraphs, drawings, ...) in OOXML.
     *
     * @returns {Object}
     *  The explicit formatting attributes contained in the element, as map
     *  of attribute value maps (name/value pairs), keyed by attribute
     *  family.
     */
    #resolveElementAttributeSet(element, sourceNode, ignoreFamily) {

        // the explicit attributes of the element
        let explicitAttributes = getExplicitAttributeSet(element, true);

        // call custom element attribute resolver
        const elementAttributesResolver = this.#initOptions?.elementAttributesResolver;
        if (elementAttributesResolver) {
            explicitAttributes = elementAttributesResolver.call(this, explicitAttributes, $(element), $(sourceNode));
        }

        // removing an optionally specified attribute family (DOCS-4211)
        if (ignoreFamily && explicitAttributes[ignoreFamily] && this.docModel.hasOoxmlTextCharacterFilterSupport()) {
            explicitAttributes = _.copy(explicitAttributes); // deleting content only on a copy!
            delete explicitAttributes[ignoreFamily];
        }

        return explicitAttributes;
    }

    #createOperationProperties(styleId, properties) {
        properties = _.extend({ styleId }, properties);
        if (!this.#isDefaultFamily) { properties.type = this.#styleFamily; }
        return properties;
    }

}
