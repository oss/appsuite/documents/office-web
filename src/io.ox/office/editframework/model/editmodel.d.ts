/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery, ColorDescriptor, Font } from "@/io.ox/office/tk/dom";
import { JAbortablePromise } from "@/io.ox/office/tk/objects";
import { BaseModelEventMap, BaseModel } from "@/io.ox/office/baseframework/model/basemodel";
import { CharacterAttributes } from "@/io.ox/office/editframework/utils/attributeutils";
import FontCollection from "@/io.ox/office/editframework/model/fontcollection";
import { BaseFormatter } from "@/io.ox/office/editframework/model/formatter/baseformatter";
import { EditApplication } from "@/io.ox/office/editframework/app/editapplication";
import { OpColor, Color } from "@/io.ox/office/editframework/utils/color";
import { Operation } from "@/io.ox/office/editframework/utils/operations";
import { OperationContext } from "@/io.ox/office/editframework/model/operation/context";
import { GeneratorConfig, OperationSource, OperationGenerator } from "@/io.ox/office/editframework/model/operation/generator";
import { UndoActionConfig, UndoManagerConfig, UndoManager } from "@/io.ox/office/editframework/model/undomanager";
import { ThemeModel } from "@/io.ox/office/editframework/model/themecollection";
import { PoolMapConstraint, AttributeBagT, AttributePool } from "@/io.ox/office/editframework/model/attributepool";
import { StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import { AutoStyleCollection } from "@/io.ox/office/editframework/model/autostylecollection";

// types ======================================================================

/**
 * Global document attributes.
 */
interface DocumentAttributes {

    /**
     * ISO date/time of document creation.
     */
    created: string;

    /**
     * ISO date/time of last modification.
     */
    modified: string;

    /**
     * Middleware filter version.
     */
    fv: number;
}

export interface BaseAttributePoolMap {
    document:  [DocumentAttributes];
}

export interface SendOperationsOptions {
    undo?: boolean;
    userData?: object;
}

export interface ApplyExternalOperationsOptions {
    expand?: boolean;
}

export interface CreateAndApplyOperationsOptions<GeneratorT extends OperationGenerator = OperationGenerator> extends GeneratorConfig, UndoActionConfig {
    storeSelection?: boolean | FuncType<void>;
    undoMode?: "generate" | "skip" | "clear";
    generator?: GeneratorT;
    afterApplyHandler?(generator: GeneratorT): void;
}

export type DefAttributePool<DocModelT extends EditModel> = DocModelT["defAttrPool"];

export interface EditModelConfig extends UndoManagerConfig { }

/**
 * Type mapping for the events emitted by `EditModel` instances.
 */
export interface EditModelEventMap<
    PoolMapT extends PoolMapConstraint<PoolMapT> & BaseAttributePoolMap = BaseAttributePoolMap
> extends BaseModelEventMap {

    /**
     * Will be emitted before document operations will be applied.
     *
     * @param operations
     *  The document operations to be applied.
     *
     * @param external
     *  Whether the operations have been received form the server.
     */
    "operations:before": [operations: readonly Operation[], external: boolean];

    /**
     * Will be emitted after document operations have been applied, regardless
     * if successfully or not.
     *
     * @param operations
     *  The document operations that have been applied.
     *
     * @param external
     *  Whether the operations have been received form the server.
     *
     * @param userData
     *  Internal data for OT.
     */
    "operations:after": [operations: readonly Operation[], external: boolean, userData: unknown];

    /**
     * Will be emitted directly after the event "operations:after", if the
     * document operations have been applied successfully.
     *
     * @param operations
     *  The document operations that have been applied.
     *
     * @param external
     *  Whether the operations have been received form the server.
     *
     * @param userData
     *  Internal data for OT.
     */
    "operations:success": [operations: readonly Operation[], external: boolean, userData: unknown];

    /**
     * Will be emitted directly after the event "operations:after", if applying
     * the document operations has failed.
     *
     * @param operations
     *  The document operations that have been applied.
     *
     * @param external
     *  Whether the operations have been received form the server.
     *
     * @param userData
     *  Internal data for OT.
     */
    "operations:error": [operations: readonly Operation[], external: boolean, userData: unknown];

    /**
     * Will be emitted directly after applying a single operation handler for
     * an internal operation.
     */
    "operation:internal": [operation: Operation];

    /**
     * Will be emitted directly after applying a single operation handler for
     * an external operation.
     */
    "operation:external": [operation: Operation];

    /**
     * Will be emitted after the global document configuration has been changed
     * by a "changeConfig" document operation.
     *
     * @param changedAttrs
     *  The document attributes that have been changed.
     */
    "change:config": [changedAttrs: Partial<AttributeBagT<PoolMapT, "document">>];

    /**
     * Will be emitted after all fast load strings have been applied to the DOM
     * for an imported document, but before operations are handled.
     */
    "fastload:done": [];

    /**
     * Will be emitted after all fast load strings have been applied to the DOM
     * for a new empty document.
     */
    "fastemptyload:done": [];

    /**
     * Will be emitted after an operation has been applied immediately while it
     * has been inserted into an operation generator.
     */
    "operations:immediate": [];

    /**
     * Will be emitted after the contents checksum of the document model has
     * been calculated.
     *
     * @param checksum
     *  The contents checksum of the document model.
     */
    "update:checksum": [checksum: string];

    /**
     * Will be emitted after the OSN of the document model has been changed.
     *
     * @param osn
     *  The current OSN of the document model.
     */
    "change:osn": [osn: number];

    "operation:apply:before": [operation: Operation, serverOSN: number, external: boolean];
    "operation:apply:after": [operation: Operation, serverOSN: number, external: boolean];
    "operationtrace:apply:external": [operation: Operation, serverOSN: number];
    "operationtrace:merged:internal": [operation: Operation];

    "debug:clipboard": [contents: unknown];
}

// class EditModel ============================================================

export class EditModel<
    PoolMapT extends PoolMapConstraint<PoolMapT> & BaseAttributePoolMap = BaseAttributePoolMap,
    EvtMapT extends EditModelEventMap<PoolMapT> = EditModelEventMap<PoolMapT>
> extends BaseModel<EvtMapT> {

    declare readonly docApp: EditApplication;

    readonly fontCollection: FontCollection;
    readonly defAttrPool: AttributePool<PoolMapT>;
    readonly globalConfig: Readonly<AttributeBagT<PoolMapT, "document">>;
    readonly undoManager: UndoManager;
    readonly numberFormatter: BaseFormatter;

    protected constructor(docApp: EditApplication, config?: EditModelConfig);

    createOperationContext(operation: Operation, external: boolean, importing: boolean): OperationContext;
    registerOperationHandler(name: string, handler: (this: this, context: ReturnType<this["createOperationContext"]>) => void): void;
    registerPlainOperationHandler(name: string, handler: (this: this, operation: Operation, external: boolean) => void): void;

    isProcessingActions(): boolean;
    waitForActionsProcessed(): JPromise;

    getOperationStateNumber(): number;
    setOperationStateNumber(osn: number): void;
    getOperationsCount(): number;
    getDocumentChecksum(): string;
    getFilterVersion(): number;

    sendOperations(operations: OperationSource, options?: SendOperationsOptions): boolean;
    applyOperationsAsync(operations: OperationSource, options?: SendOperationsOptions): JAbortablePromise;
    applyOperations(operations: OperationSource, options?: SendOperationsOptions): boolean;
    applyExternalOperations(operations: Operation[], options?: ApplyExternalOperationsOptions): JPromise;
    invokeOperationHandlers(operations: OperationSource): void;
    createOperationGenerator(options?: Dict): OperationGenerator;
    createAndApplyOperations<RT, GT extends OperationGenerator>(callback: (this: this, generator: GT) => MaybeAsync<RT>, options?: CreateAndApplyOperationsOptions<GT>): JPromise<RT>;

    getSelectionState(): Opt<object>;
    setSelectionState(state: object): void;

    addStyleCollection<CollT extends StyleCollection<EditModel, any, any>>(collection: CollT): CollT;
    getStyleCollection(family: string): StyleCollection<this, Empty, Empty>;
    getDefaultStyleFamily(): string;

    addAutoStyleCollection<CollT extends AutoStyleCollection<any, any, any>>(collection: CollT): CollT;

    hasDefaultTheme(targets?: string[]): boolean;
    getThemeTargets(element?: NodeOrJQuery): string[];
    getThemeModel(targets?: string[]): ThemeModel;
    useSlideMode(): boolean;

    resolveFontName(fontName: string, targets?: string[]): string;
    getCssFontFamily(fontName: string, targets?: string[]): string;
    getRenderFont(charAttrs: CharacterAttributes, zoom: number, targets?: string[]): Font;

    resolveColor(color: Color, auto: Color | string, targets?: string[]): ColorDescriptor;
    parseAndResolveColor(opColor: OpColor, opAuto: OpColor | string, targets?: string[]): ColorDescriptor;
    getCssColor(opColor: OpColor, opAuto: string, targets?: string[]): string;

    resolveTextColor(textColor: Color, fillColors: Color[], targets?: string[]): ColorDescriptor;
    parseAndResolveTextColor(opTextColor: OpColor, opFillColors: OpColor[], targets?: string[]): ColorDescriptor;
    getCssTextColor(opColor: OpColor, opFillColors: OpColor[], targets?: string[]): string;

    hasUnsavedComments(): boolean;
    deleteUnsavedComments(): void;

    getSlideTouchMode(): boolean;
    addCharacterAttributes(charAttrs: CharacterAttributes): void;

    getUndoManager(): UndoManager;
}
