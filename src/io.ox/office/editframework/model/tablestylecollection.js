/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';

import { getBooleanOption } from '@/io.ox/office/tk/utils';
import { insertAttribute } from '@/io.ox/office/editframework/utils/attributeutils';
import { StyleCollection } from '@/io.ox/office/editframework/model/stylecollection';

// constants ==================================================================

/**
 * Numeric flags for a bitmask specifying the type of a cell in a table,
 * according to the various attribute sets of a table style sheet.
 */
export const TABLE_STYLE_ATTR_FLAGS /*: Record<TableStyleAttrSetKey, number>*/ = {
    wholeTable:     0x0000,
    // table columns
    firstCol:       0x0001,
    lastCol:        0x0002,
    band1Vert:      0x0004,
    band2Vert:      0x0008,
    // table rows
    firstRow:       0x0010,
    lastRow:        0x0020,
    band1Hor:       0x0040,
    band2Hor:       0x0080,
    // table corners
    northWestCell:  0x0100,
    northEastCell:  0x0200,
    southWestCell:  0x0400,
    southEastCell:  0x0800
};

// class TableStyleCollection =================================================

/**
 * Contains the style sheets for drawing formatting attributes.
 *
 * @param {EditModel} docModel
 *  The document model containing this instance.
 *
 * @param {Object} [initOptions]
 *  Optional parameters passed to the base class StyleCollection. The
 *  following additional options are supported:
 *  - {Boolean} [initOptions.rowBandsOverColBands=false]
 *      If set to true, the inner row band attributes will be merged over
 *      the inner column band attributes. By default, inner column band
 *      attributes will win over inner row band attributes
 *  - {Boolean} [initOptions.expandColBandsToOuterCols=false]
 *      If set to true, the inner column band attributes will be merged
 *      over the active first and last column attributes. By default, inner
 *      column bands will start after the active first column, and will end
 *      before the active last column.
 *  - {Boolean} [initOptions.restrictColBandsToInnerRows=false]
 *      If set to true, the inner column band attributes will not be merged
 *      into the header row nor the footer row.
 */
export default class TableStyleCollection extends StyleCollection {

    // whether row band attributes will be merged over column band attributes
    #rowBandsOverColBands;
    // whether to merge inner column bands over active first and/or last column
    #expandColBandsToOuterCols;
    // whether to exclude the column bands from the header and footer rows (default: merge into all rows)
    #restrictColBandsToInnerRows;

    constructor(docModel, initOptions) {

        // base constructor
        super(docModel, 'table', {
            ...initOptions,
            elementAttributesResolver: attrs => this.#tableExplicitElementResolver(attrs)
        });

        this.#rowBandsOverColBands = getBooleanOption(initOptions, 'rowBandsOverColBands', false);
        this.#expandColBandsToOuterCols = getBooleanOption(initOptions, 'expandColBandsToOuterCols', false);
        this.#restrictColBandsToInnerRows = getBooleanOption(initOptions, 'restrictColBandsToInnerRows', false);
    }

    // public methods -----------------------------------------------------

    /**
     * Return the attributes for a specific table cell, depending on its
     * position in the table.
     *
     * @param {Object} rawStyleAttrSet
     *  The raw style attribute set of a table style sheet.
     *
     * @param {Number|Object|Null} colRange
     *  The column index of the cell, or the column range of a merged cell
     *  (column span) with "start" and "end" integer properties.
     *
     * @param {Number|Object|Null} rowRange
     *  The row index of the cell, or the row range of a merged cell (row
     *  span) with "start" and "end" integer properties.
     *
     * @param {Integer} colCount
     *  the number of columns in the table.
     *
     * @param {Integer} rowCount
     *  The number of rows in the table.
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  @param {Boolean} [options.firstRow=false]
     *      Whether the first row (header row) is visible.
     *  @param {Boolean} [options.lastRow=false]
     *      Whether the last row (total row) is visible.
     *  @param {Boolean} [options.firstCol=false]
     *      Whether the first column attributes are used for the cell style
     *      attributes.
     *  @param {Boolean} [options.lastCol=false]
     *      Whether the last column attributes are used for the cell style
     *      attributes.
     *  @param {Boolean} [options.bandsHor=false]
     *      Whether the banded row attributes are used for the cell style
     *      attributes.
     *  @param {Boolean} [options.bandsVert=false]
     *      Whether the banded column attributes are used for the cell
     *      style attributes.
     *  @param {Object} [options.explicitAttributes]
     *      The full set of explicit attributes of family 'table'. This is
     *      required to collect the explicit border attributes at the table.
     *      See DOCS-3245 (loading without fastLoad).
     *
     * @returns {TableCellFormatResult}
     *  The resulting resolved attribute set for the specified cell, and a
     *  bitmask with a set bit for every applied attribute sub-set (from the
     *  constants in `TABLE_STYLE_ATTR_FLAGS`).
     */
    resolveCellAttributeSet(rawStyleAttrSet, colRange, rowRange, colCount, rowCount, options = {}) {

        // table size
        const lastTableRow = rowCount - 1;
        const lastTableCol = colCount - 1;

        // convert row index to interval, detect first row or last row hit
        if (typeof rowRange === 'number') { rowRange = { start: rowRange, end: rowRange }; }
        const isFirstRow = rowRange && (rowRange.start === 0);
        const isLastRow = rowRange && (rowRange.end === lastTableRow);

        // convert column index to interval, detect first column or last column hit
        if (typeof colRange === 'number') { colRange = { start: colRange, end: colRange }; }
        const isFirstCol = colRange && (colRange.start === 0);
        const isLastCol = colRange && (colRange.end === lastTableCol);

        // first and last row without the header and footer row
        const firstCellRow = rowRange && (rowRange.start === (options.firstRow ? 1 : 0));
        const lastCellRow = rowRange && (rowRange.end === lastTableRow - (options.lastRow ? 1 : 0));

        // the resulting style attributes according to the position of the table cell
        const cellAttrSet = {};

        // wholeTable: always
        this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "wholeTable", true, true, isFirstRow, isLastRow, isFirstCol, isLastCol);

        // OX Text: (At least) explicit border attributes at the table must be handled before the border attributes for firstColumn, firstRow, ... (DOCS-3245)
        if (this.docApp.isTextApp()) {
            const sourceAttrSet = this.#getExplicitBorderAttributes(options.explicitAttributes);
            if (sourceAttrSet) {
                this.#mergeAttributes(cellAttrSet, sourceAttrSet, true, true, isFirstRow, isLastRow, isFirstCol, isLastCol); // only border attributes!
            }
        }

        // flags for all applied attribute sub-sets from the table style
        let attrSetFlags = 0;

        // horizontal/vertical band attributes
        if (this.#rowBandsOverColBands) {
            attrSetFlags |= this.#addColBandAttributes(cellAttrSet, rawStyleAttrSet, colRange, isFirstRow, isLastRow, isFirstCol, isLastCol, options, firstCellRow, lastCellRow);
            attrSetFlags |= this.#addRowBandAttributes(cellAttrSet, rawStyleAttrSet, rowRange, isFirstRow, isLastRow, isFirstCol, isLastCol, options, firstCellRow, lastCellRow);
        } else {
            attrSetFlags |= this.#addRowBandAttributes(cellAttrSet, rawStyleAttrSet, rowRange, isFirstRow, isLastRow, isFirstCol, isLastCol, options, firstCellRow, lastCellRow);
            attrSetFlags |= this.#addColBandAttributes(cellAttrSet, rawStyleAttrSet, colRange, isFirstRow, isLastRow, isFirstCol, isLastCol, options, firstCellRow, lastCellRow);
        }

        // cell inside first/last row/column
        if (options.firstCol && isFirstCol) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "firstCol", false, true, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }
        if (options.lastCol && isLastCol) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "lastCol", false, true, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }
        if (options.firstRow && isFirstRow) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "firstRow", true, false, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }
        if (options.lastRow && isLastRow) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "lastRow", true, false, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }

        // single corner cells (only if inside active first/last row AND column areas)
        if (options.firstRow && options.firstCol && isFirstRow && isFirstCol) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "northWestCell", false, false, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }
        if (options.firstRow && options.lastCol && isFirstRow && isLastCol) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "northEastCell", false, false, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }
        if (options.lastRow && options.firstCol && isLastRow && isFirstCol) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "southWestCell", false, false, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }
        if (options.lastRow && options.lastCol && isLastRow && isLastCol) {
            attrSetFlags |= this.#mergeConditionalAttributes(cellAttrSet, rawStyleAttrSet, "southEastCell", false, false, isFirstRow, isLastRow, isFirstCol, isLastCol);
        }

        return { cellAttrSet, attrSetFlags };
    }

    // private methods ----------------------------------------------------

    // This resolver for explicit attributes is required, because of the special handling for explicit table border attributes, that was
    // introduced with DOCS-3245. In this scenario the explicit table borders must be handled, when the border style is evaluated. This
    // happens in 'resolveCellAttributeSet' when the function 'getExplicitBorderAttributes' is called. Therefore it is not allowed, that
    // the explicit border attributes are also evaluated a second time together with the other explicit table attributes.
    #tableExplicitElementResolver(origExplicitAttributes) {

        if (!this.docApp.isTextApp()) { return origExplicitAttributes; }

        const explicitAttributes = _.copy(origExplicitAttributes, true);

        if (explicitAttributes.table) {
            if (explicitAttributes.table.borderLeft) { delete explicitAttributes.table.borderLeft; }
            if (explicitAttributes.table.borderRight) { delete explicitAttributes.table.borderRight; }
            if (explicitAttributes.table.borderTop) { delete explicitAttributes.table.borderTop; }
            if (explicitAttributes.table.borderBottom) { delete explicitAttributes.table.borderBottom; }
            if (explicitAttributes.table.borderInsideVert) { delete explicitAttributes.table.borderInsideVert; }
            if (explicitAttributes.table.borderInsideHor) { delete explicitAttributes.table.borderInsideHor; }
        }

        return explicitAttributes;
    }

    // At least in OX Text at least the explicit border attributes at the table must be handled before the border attributes for firstColumn, firstRow, ...
    // If there are borders specified for the first row, they must be shown, even if the table has explicit border attributes to not show the borders.
    // The order seems to be the following:
    //   1. wholeTable
    //   2. explicit border attributes at the table
    //   3. border attributes specified for firstRow, firstColumn, ...
    // See DOCS-3245 (loading without fastlaod)
    #getExplicitBorderAttributes(explicitAttributes) {

        const borderAttrs = {};

        if (explicitAttributes) {
            if (explicitAttributes.borderLeft) { borderAttrs.borderLeft = explicitAttributes.borderLeft; }
            if (explicitAttributes.borderRight) { borderAttrs.borderRight = explicitAttributes.borderRight; }
            if (explicitAttributes.borderTop) { borderAttrs.borderTop = explicitAttributes.borderTop; }
            if (explicitAttributes.borderBottom) { borderAttrs.borderBottom = explicitAttributes.borderBottom; }
            if (explicitAttributes.borderInsideVert) { borderAttrs.borderInsideVert = explicitAttributes.borderInsideVert; }
            if (explicitAttributes.borderInsideHor) { borderAttrs.borderInsideHor = explicitAttributes.borderInsideHor; }
        }

        return _.isEmpty(borderAttrs) ? null : { table: borderAttrs };
    }

    // copies a border attribute (table or cell) to an outer cell border attribute
    #updateOuterCellBorder(targetAttrSet, sourceAttrSet, outerBorderName, innerBorderName, isOuterBorder) {

        // table and cell attributes (either may be missing)
        const tableAttrs = _.isObject(sourceAttrSet.table) ? sourceAttrSet.table : {};
        const cellAttrs = _.isObject(sourceAttrSet.cell) ? sourceAttrSet.cell : {};
        // the source border attribute value
        let border = null;

        if (isOuterBorder) {
            // copy outer table border to cell border if border is missing in cell attributes
            border = _.isObject(cellAttrs[outerBorderName]) ? null : tableAttrs[outerBorderName];
        } else {
            // copy inner table or cell border (cell border wins) to outer cell border
            border = _.isObject(cellAttrs[innerBorderName]) ? cellAttrs[innerBorderName] : tableAttrs[innerBorderName];
        }

        // insert existing border to the specified outer cell border
        if (_.isObject(border)) {
            insertAttribute(targetAttrSet, 'cell', outerBorderName, border);
        }
    }

    // copies a padding attribute from a table to a cell, if required
    #updateCellPadding(targetAttrSet, sourceAttrSet, paddingName) {

        // table and cell attributes (either may be missing)
        const tableAttrs = _.isObject(sourceAttrSet.table) ? sourceAttrSet.table : {};
        const cellAttrs = _.isObject(sourceAttrSet.cell) ? sourceAttrSet.cell : {};
        // the padding attribute value
        const padding = (paddingName in cellAttrs) ? cellAttrs[paddingName] : tableAttrs[paddingName];

        // insert existing border to the specified outer cell border
        if (_.isNumber(padding)) {
            insertAttribute(targetAttrSet, 'cell', paddingName, padding);
        }
    }

    // merges the specified conditional attributes into the target attribute set
    #mergeAttributes(targetAttrSet, sourceAttrSet, isRowBand, isColBand, isFirstRow, isLastRow, isFirstCol, isLastCol, isBandStart) {

        // copy all attributes from the style sheet to the result object
        this.extendAttrSet(targetAttrSet, sourceAttrSet);

        // INFO: In Presentation app it is possible, that 'borderInsideHor' and 'borderInsideVert' are set
        //       to the 'cell' family in table styles. Therefore it can happen, that these properties are
        //       set via the conditionalAttributes although this might not be wanted (48609). Then this
        //       properties need to be overwritten with hard attributes in operations.

        // copy inner borders to outer cell borders, if cell is located inside the current table area
        this.#updateOuterCellBorder(targetAttrSet, sourceAttrSet, 'borderTop',    'borderInsideHor',  !isColBand || isFirstRow);
        this.#updateOuterCellBorder(targetAttrSet, sourceAttrSet, 'borderBottom', 'borderInsideHor',  !isColBand || isLastRow);

        if (isBandStart) {
            this.#updateOuterCellBorder(targetAttrSet, sourceAttrSet, 'borderBottom', 'borderInsideHor',  !(isRowBand && isFirstRow));
        } else {
            this.#updateOuterCellBorder(targetAttrSet, sourceAttrSet, 'borderTop',    'borderInsideHor',  !(isRowBand && !isFirstRow));
        }

        this.#updateOuterCellBorder(targetAttrSet, sourceAttrSet, 'borderLeft',   'borderInsideVert', !isRowBand || isFirstCol);
        this.#updateOuterCellBorder(targetAttrSet, sourceAttrSet, 'borderRight',  'borderInsideVert', !isRowBand || isLastCol);

        this.#updateCellPadding(targetAttrSet, sourceAttrSet, 'paddingTop');
        this.#updateCellPadding(targetAttrSet, sourceAttrSet, 'paddingBottom');
        this.#updateCellPadding(targetAttrSet, sourceAttrSet, 'paddingLeft');
        this.#updateCellPadding(targetAttrSet, sourceAttrSet, 'paddingRight');
    }

    #mergeConditionalAttributes(targetAttrSet, rawStyleAttrSet, attrSetKey, isRowBand, isColBand, isFirstRow, isLastRow, isFirstCol, isLastCol, isBandStart) {
        const sourceAttrSet = rawStyleAttrSet[attrSetKey];
        if (!sourceAttrSet) { return 0; }
        this.#mergeAttributes(targetAttrSet, sourceAttrSet, isRowBand, isColBand, isFirstRow, isLastRow, isFirstCol, isLastCol, isBandStart);
        return TABLE_STYLE_ATTR_FLAGS[attrSetKey];
    }

    #addRowBandAttributes(targetAttrSet, rawStyleAttrSet, rowRange, isFirstRow, isLastRow, isFirstCol, isLastCol, options, firstCellRow, lastCellRow) {
        const outerRowActive = (options.firstRow && isFirstRow) || (options.lastRow && isLastRow);
        if (rowRange && !outerRowActive && options.bandsHor) {

            const index = rowRange.start - (options.firstRow ? 1 : 0);
            const bandStyle = this.#getBandStyle(index, rawStyleAttrSet.band1Hor, rawStyleAttrSet.band2Hor);
            const attrSetKey = (bandStyle === 1) ? "band1Hor" : (bandStyle === 2) ? "band2Hor" : "";

            if (attrSetKey) {
                // Needed if only inside horizontal border is active, the first cell of the band is border bottom and the other border top
                const isBandStart = index === 0 || this.#getBandStyle(index - 1, rawStyleAttrSet.band1Hor, rawStyleAttrSet.band2Hor) !== bandStyle;
                return this.#mergeConditionalAttributes(targetAttrSet, rawStyleAttrSet, attrSetKey, true, false, firstCellRow, lastCellRow, isFirstCol, isLastCol, isBandStart);
            }
        }
        return 0;
    }

    #addColBandAttributes(targetAttrSet, rawStyleAttrSet, colRange, isFirstRow, isLastRow, isFirstCol, isLastCol, options, firstCellRow, lastCellRow) {
        const outerRowActive = (options.firstRow && isFirstRow) || (options.lastRow && isLastRow);
        const outerColActive = (options.firstCol && isFirstCol) || (options.lastCol && isLastCol);
        if (colRange && !(outerRowActive && this.#restrictColBandsToInnerRows) && !(outerColActive && !this.#expandColBandsToOuterCols) && options.bandsVert) {

            const bandStyle = this.#getBandStyle(colRange.start - (options.firstCol && !this.#expandColBandsToOuterCols), rawStyleAttrSet.band1Vert, rawStyleAttrSet.band2Vert);
            const attrSetKey = (bandStyle === 1) ? "band1Vert" : (bandStyle === 2) ? "band2Vert" : "";
            if (attrSetKey) {
                return this.#mergeConditionalAttributes(targetAttrSet, rawStyleAttrSet, attrSetKey, false, true, firstCellRow, lastCellRow, isFirstCol, isLastCol);
            }
        }
        return 0;
    }

    /**
     * Calculate if the first row/col or second row/col style will be used for the cell.
     *
     * @param {type} index cell or row index of the cell.
     * @param {type} band1Attributes attributes for the first band style attributes
     * @param {type} band2Attributes attributes for the second band style attributes
     * @returns {Number} the number of the style 1 or 2. If there is no bandstyle return 0.
     */
    #getBandStyle(index, band1Attributes, band2Attributes) {

        let bandStyle = 0;

        const stripSize1 = band1Attributes ? _.isNumber(band1Attributes.stripesetSize) ? band1Attributes.stripesetSize : 1 : 0;
        let stripSize2 = band2Attributes ? _.isNumber(band2Attributes.stripesetSize) ? band2Attributes.stripesetSize : 1 : 0;

        if (band1Attributes) {
            if (index < stripSize1) {
                bandStyle = 1;
            } else if (stripSize2 === 0) {
                bandStyle = ((index + 1) % (stripSize1 + 1)) !== 0 ? 1 : -1;
            } else {
                const max = stripSize1 + stripSize2;
                bandStyle = 1;
                for (let i = 1; i <= stripSize2; i++) {

                    if ((index + i) % max === 0) {
                        bandStyle = 2;
                        break;
                    }
                }
            }
        } else if (band2Attributes) {
            stripSize2 = stripSize2 > 0 ? stripSize2 : 1;
            const rightShift = Math.max(0, stripSize1 - 1);
            bandStyle = ((index + rightShift) % (stripSize2 + 1)) !== 0 ? 2 : 0;
        }
        return bandStyle;
    }
}
