/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { AttrSetConstraint } from "@/io.ox/office/editframework/utils/attributeutils";
import { PoolMapConstraint } from "@/io.ox/office/editframework/model/attributepool";
import { StyleCollectionConfig, StyleCollection } from "@/io.ox/office/editframework/model/stylecollection";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

export type TableStyleAttrSetKey = "wholeTable" | "firstCol" | "lastCol" | "band1Vert" | "band2Vert" | "firstRow" | "lastRow" | "band1Hor" | "band2Hor" | "northWestCell" | "northEastCell" | "southWestCell" | "southEastCell";

/**
 * A set of flags determining which parts of a table stylesheet will be
 * used to format the cells of a table.
 */
export interface TableStyleFlags {
    firstRow: boolean;
    lastRow: boolean;
    firstCol: boolean;
    lastCol: boolean;
    bandsHor: boolean;
    bandsVert: boolean;
}

export interface TableCellIndexSpan {
    start: number;
    end: number;
}

export interface TableCellFormatResult {
    cellAttrSet: Dict; // TODO
    attrSetFlags: number;
}

// constants ==================================================================

export const TABLE_STYLE_ATTR_FLAGS: Record<TableStyleAttrSetKey, number>;

// class TableStyleCollection =================================================

export default class TableStyleCollection<
    DocModelT extends EditModel,
    PoolMapT extends PoolMapConstraint<PoolMapT>,
    AttrSetT extends AttrSetConstraint<AttrSetT>
> extends StyleCollection<DocModelT, PoolMapT, AttrSetT> {

    constructor(docModel: DocModelT, config?: StyleCollectionConfig<PoolMapT, AttrSetT>);

    resolveCellAttributeSet(rawAttrSet: object, colRange: number | TableCellIndexSpan, rowRange: number | TableCellIndexSpan, colCount: number, rowCount: number, options?: TableStyleFlags): TableCellFormatResult;
}
