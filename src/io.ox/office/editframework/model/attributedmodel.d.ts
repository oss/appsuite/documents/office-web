/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { AttrSetConstraint, PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { PoolMapConstraint, AttributePool } from "@/io.ox/office/editframework/model/attributepool";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

export interface AttributedModelConfig<
    DocModelT extends EditModel,
    PoolMapT extends PoolMapConstraint<PoolMapT>,
    AttrSetT extends AttrSetConstraint<AttrSetT>
> {

    /**
     * The attribute family of stylesheets that can be referred by this model.
     * If omitted, this object supports explicit attributes only. All attribute
     * families that are supported by these stylesheets will also be supported
     * by this model object.
     */
    styleFamily?: string;

    /**
     * The attribute families of explicit attributes supported by this model.
     */
    families?: ReadonlyArray<KeysOf<AttrSetT>>;

    /**
     * If set to `true`, adding the attribute families supported by the style
     * sheet container (as specified in the constructor of the respective
     * container) to the own supported attribute families will be skipped.
     * Default value is `false`.
     */
    skipStyleFamilies?: boolean;

    /**
     * If set to `false`, this instance will not listen to change events of the
     * stylesheet collection specified with the option "styleFamily". This may
     * be desired for performance reasons to reduce the number of event
     * listeners in collections with many instances of this class. Default
     * value is `true`!
     */
    listenToStyles?: boolean;

    /**
     * The attribute pool to be used. This option will be ignored, if this
     * model instance uses stylesheets (see option "styleFamily"). In this
     * case, the attribute pool of the stylesheet collection will be used
     * instead. If omitted and no style family has been specified, the default
     * attribute pool of the document will be used.
     */
    attrPool?: AttributePool<PoolMapT>;

    /**
     * A parent model object that supports all or a few of the attribute
     * families supported by this instance. The merged attributes of that
     * parent model will be added to the own merged attribute set before adding
     * the own style attributes and explicit attributes. Parent models can
     * cascade (the parent model may define its own parent model). Attribute
     * changes in the parent model will be forwarded to this instance (unless
     * the option "listenToParent" has been set to `false`, see below).
     */
    parentModel?: AttributedModel<DocModelT, PoolMapT, any>;

    /**
     * If set to `false`, this instance will not listen to change events of the
     * parent model specified with the option "parentModel". This may be
     * desired for performance reasons to reduce the number of event listeners
     * in collections with many instances of this class. default value is
     * `true`!
     */
    listenToParent?: boolean;

    /**
     * If set to `true`, explicit attributes that are equal to the default
     * attribute values (as specified in the attribute definitions of the
     * respective attribute families), will not be inserted into the explicit
     * attribute set, but the old explicit attribute will be removed from the
     * explicit attribute set, when using the method `setAttributes`. This
     * option has no effect, if this model instance uses stylesheets (see
     * option "styleFamily"). If this model refers to a parent attribute model
     * (see option "parentModel"), its attributes will be used for comparison,
     * instead of the default values defined in the attribute definitions.
     * Default value is `false`.
     */
    autoClear?: boolean;
}

export interface AttributedModelEventMap<AttrSetT extends AttrSetConstraint<AttrSetT>> {

    /**
     * Will be emitted after the explicit attributes or the stylesheet
     * identifier have been changed, either directly by using the method
     * `AttributedModel::setAttributes`, or indirectly after the stylesheet has
     * been changed.
     *
     * @param newAttrSet
     *  The current (new) merged attribute set.
     *
     * @param oldAttrSet
     *  The previous (old) merged attribute set.
     */
    "change:attributes": [newAttrSet: AttrSetT, oldAttrSet: AttrSetT];
}

// class AttributedModel ======================================================

export class AttributedModel<
    DocModelT extends EditModel,
    PoolMapT extends PoolMapConstraint<PoolMapT>,
    AttrSetT extends AttrSetConstraint<AttrSetT>,
    EvtMapT extends AttributedModelEventMap<AttrSetT> = AttributedModelEventMap<AttrSetT>
> extends ModelObject<DocModelT, EvtMapT> {

    readonly attrPool: AttributePool<PoolMapT>;

    constructor(docModel: DocModelT, attrSet?: PtAttrSet<AttrSetT>, config?: AttributedModelConfig<DocModelT, PoolMapT, AttrSetT>);

    hasExplicitAttributes(): boolean;
    getExplicitAttributeSet(direct?: boolean): PtAttrSet<AttrSetT>;
    getMergedAttributeSet(direct: true): Readonly<AttrSetT>;
    getMergedAttributeSet(direct?: boolean): AttrSetT;
    getReducedAttributeSet(attrSet: PtAttrSet<AttrSetT>): PtAttrSet<AttrSetT>;
    getUndoAttributeSet(attrSet: PtAttrSet<AttrSetT>): PtAttrSet<AttrSetT>;
    hasEqualAttributeSet(otherModel: this): boolean;

    setAttributes(attrSet: PtAttrSet<AttrSetT>, options?: { silent?: boolean }): boolean;
}
