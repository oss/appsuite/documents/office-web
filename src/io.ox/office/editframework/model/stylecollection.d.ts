/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeOrJQuery } from "@/io.ox/office/tk/dom";
import { ModelObject } from "@/io.ox/office/baseframework/model/modelobject";
import { AttrSetConstraint, PtAttrSet } from "@/io.ox/office/editframework/utils/attributeutils";
import { PoolMapConstraint, AttributePool } from "@/io.ox/office/editframework/model/attributepool";
import { StyleCache } from "@/io.ox/office/editframework/model/operation/stylecache";
import { EditModel } from "@/io.ox/office/editframework/model/editmodel";

// types ======================================================================

export interface StyleCollectionConfig<
    PoolMapT extends PoolMapConstraint<PoolMapT>,
    AttrSetT extends AttrSetConstraint<AttrSetT>
> {
    attrPool?: AttributePool<PoolMapT>;
    families?: ReadonlyArray<KeysOf<AttrSetT>>;
    styleSheetSupport?: boolean;
}

/**
 * The options for the "getElementAttributes" function
 */
export interface GetElementAttributesOptions {

    /**
     * A descendant of the passed element associated to a child
     * attribute family. Will be passed to a style attribute resolver
     * callback function where it might be needed to resolve the
     * correct attributes according to the position of this source
     * node.
     */
    sourceNode?: NodeOrJQuery;

    /**
     * An attribute set with all attribute values of the ancestor of
     * the passed element, merged from style sheet and explicit
     * attributes, keyed by attribute family. Used internally while
     * updating the formatting of all child elements of a node.
     */
    baseAttributes?: Dict; // TODO

    /**
     * If set to true, the pool default values will not be included
     * into the generated merged attributes. This is especially useful,
     * when operations shall be generated with all available attributes,
     * for example after copy/paste of template drawings. In this case
     * it is better to omit those attributes, that are set by default.
     */
    skipPoolDefaults?: boolean;

    /**
     * An optionally specified string to specify an attribute family, that
     * shall be ignored. For task DOCS-4211 this is the familiy "character",
     * because the character attributes must not be inherited from ancestor
     * nodes of text spans (paragraphs, drawings, ...) in OOXML.
     */
    ignoreFamily?: string;
}

/**
 * Type mapping for the events emitted by `StyleCollection` instances.
 */
export interface StyleCollectionEventMap {

    /**
     * Will be emitted after a new stylesheet has been inserted into the
     * stylesheet collection.
     *
     * @param styleId
     *  The identifier of the new stylesheet.
     */
    "insert:stylesheet": [styleId: string];

    /**
     * Will be emitted after a stylesheet has been deleted from the stylesheet
     * collection.
     *
     * @param styleId
     *  The identifier of the deleted stylesheet.
     */
    "delete:stylesheet": [styleId: string];

    /**
     * Will be emitted after the formatting attributes, or other properties of
     * a stylesheet in the stylesheet collection have been changed.
     *
     * @param styleId
     *  The identifier of the changed stylesheet.
     */
    "change:stylesheet": [styleId: string];
}

// class StyleCollection ======================================================

export class StyleCollection<
    DocModelT extends EditModel,
    PoolMapT extends PoolMapConstraint<PoolMapT>,
    AttrSetT extends AttrSetConstraint<AttrSetT>
> extends ModelObject<DocModelT, StyleCollectionEventMap> {

    readonly attrPool: AttributePool<PoolMapT>;

    constructor(docModel: DocModelT, styleFamily: KeysOf<AttrSetT>, config?: StyleCollectionConfig<PoolMapT, AttrSetT>);

    isSupportedFamily(family: string): boolean;
    yieldSupportedFamilies(): IterableIterator<KeysOf<AttrSetT>>;

    getDefaultStyleId(): string | null;
    containsStyleSheet(styleId: string): boolean;

    getDefaultStyleAttributeSet(): AttrSetT;
    getElementAttributes(node: NodeOrJQuery, options?: GetElementAttributesOptions): AttrSetT;
    getStyleAttributeSet(styleId: string): AttrSetT;
    getStyleSheetAttributeMap(styleId: string, direct?: boolean): PtAttrSet<AttrSetT>;
    getMergedAttributes(attrSet: PtAttrSet<AttrSetT>, options: object & { skipDefaults: true }): PtAttrSet<AttrSetT>;
    getMergedAttributes(attrSet: PtAttrSet<AttrSetT>, options?: object): AttrSetT;
    getName(styleId: string): string | null;
    getParentId(styleId: string): string | null;
    getUIPriority(styleId: string): number;

    setDirty(styleId: string, dirty: boolean): void;
    setElementAttributes(element: NodeOrJQuery, attrSet: PtAttrSet<AttrSetT>, options?: object): void;
    updateElementFormatting(element: NodeOrJQuery, options?: object): void;

    generateMissingStyleSheetOperations(generator: StyleCache, styleId: string): void;

    registerFormatHandler(cb: (elem: JQuery, attrs: AttrSetT, async: boolean) => void, options?: { duringImport?: boolean }): void;

    isDirty(styleId: string): boolean | null;
}
