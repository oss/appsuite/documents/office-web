/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str, re, ary, set, map, dict, json } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import type { ColorKey, DateTokens, ColorTokens } from "@/io.ox/office/tk/locale";
import { COLOR_KEYS, LOCALE_DATA, parseLocaleId, localeDataRegistry } from "@/io.ox/office/tk/locale";
import { DObject } from "@/io.ox/office/tk/objects";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import { NumberParser } from "@/io.ox/office/editframework/model/formatter/parse/numberparser";
import type { SimpleToken, LiteralTokenKind, DigitToken, DateToken, TimeToken, ParsedToken, SectionOperator } from "@/io.ox/office/editframework/model/formatter/parsedtoken";
import { TokenType } from "@/io.ox/office/editframework/model/formatter/parsedtoken";
import type { SectionCurrencyInfo, ParsedSection } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { FormatCategory, XLocaleCode, DigitCode, isAnyDateTime } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";

const { max } = Math;

// types ======================================================================

/**
 * Optional parameters for getting or using parsed number formats.
 */
export interface ParseFormatOptions {

    /**
     * Whether to use localized format codes (`true`, e.g. translated date/time
     * tokens) or native format codes (`false`, used in document operations).
     * Default value is `false`.
     */
    uiGrammar?: boolean;
}

/**
 * Optional parameters for getting or using parsed number formats.
 */
export interface StringifyFormatOptions {

    /**
     * Specifies if and how the number of decimal places are changed when creating the formatCode
     *
     */
    increaseDecimalPlaces?: boolean;
}

// private types --------------------------------------------------------------

// required properties must be compatible to interface `LocaleData`
interface CodeParserConfig {
    dec: string;
    group: string;
    dateSep: string;
    timeSep: string;
    stdToken: string;
    dateTokens: DateTokens;
    colorTokens: ColorTokens;
    opGrammar?: boolean;
}

// separator characters for parsing
interface SepTokens {
    dec: string;
    grp: string;
    date: string;
    time: string;
}

type WritableSection = Writable<ParsedSection>;

interface ParseSectionResult {
    section: ParsedSection;
    done: boolean;
}

// constants ==================================================================

// standard tokens used in document operations
export const OP_STD_TOKEN = "General";

// date/time tokens used in document operations
export const OP_DATE_TOKENS: DateTokens = {
    Y: "Y",
    M: "M",
    D: "D",
    h: "h",
    m: "m",
    s: "s",
    b: "b", // Buddhist years
    g: "g", // Japanese era name
    e: "e", // Japanese era year
    a: "a"  // name of weekday (OOXML)
};

// color tokens tokens used in document operations
export const OP_COLOR_TOKENS: ColorTokens = {
    black: "Black",
    blue: "Blue",
    green: "Green",
    cyan: "Cyan",
    red: "Red",
    magenta: "Magenta",
    yellow: "Yellow",
    white: "White"
};

/**
 * Special private-use LCID for system (UI) long date format.
 */
export const LCID_X_SYSDATE = 0xF800;

/**
 * Special private-use LCID for system (UI) time format.
 */
export const LCID_X_SYSTIME = 0xF400;

/**
 * Prefix locale tag for system date format
 */
export const SYSDATE_LOCALE_TAG = `[$-${LCID_X_SYSDATE.toString(16).toUpperCase()}]`;

/**
 * Prefix locale tag for system time format.
 */
export const SYSTIME_LOCALE_TAG = `[$-${LCID_X_SYSTIME.toString(16).toUpperCase()}]`;

// private constants ----------------------------------------------------------

const OP_PARSER_CONFIG: CodeParserConfig  = {
    dec: ".",
    group: ",",
    dateSep: "/",
    timeSep: ":",
    stdToken: OP_STD_TOKEN,
    dateTokens: OP_DATE_TOKENS,
    colorTokens: OP_COLOR_TOKENS,
    opGrammar: true
};

// all available text colors in format codes, ass CSS color, mapped by color keys
const CSS_COLORS: ColorTokens = {
    black: "#000000",
    blue: "#0000FF",
    green: "#00FF00",
    cyan: "#00FFFF",
    red: "#FF0000",
    magenta: "#FF00FF",
    yellow: "#FFFF00",
    white: "#FFFFFF"
};

// opposite operators map
const OPPOSITE_OPS: Record<SectionOperator, SectionOperator> = {
    "<":  ">=",
    "<=": ">",
    ">":  "<=",
    ">=": "<",
    "=":  "<>",
    "<>": "="
};

// maps special private-use LCIDs to locale codes
const LCID_X_MAP: Dict<XLocaleCode> = {
    1: XLocaleCode.EURO1,
    2: XLocaleCode.EURO2,
    [LCID_X_SYSDATE]: XLocaleCode.SYSDATE,
    [LCID_X_SYSTIME]: XLocaleCode.SYSTIME
};

// all native currency symbols
const CURRENCY_SYMBOLS = set.from(localeDataRegistry.yieldCurrencyData(), ([, currData]) => currData.symbol);
// all ISO currency symbols
const CURRENCY_SYMBOLS_ISO = set.from(localeDataRegistry.yieldCurrencyData(), ([, currData]) => currData.iso);

// all currency ISO codes and symbols
const ALL_CURRENCY_SYMBOLS = new Set([...CURRENCY_SYMBOLS, ...CURRENCY_SYMBOLS_ISO].sort((s1, s2) => s2.length - s1.length));
// all native single-letter currency symbols, but not latin letters (which may interfere with format tokens)
const SINGLE_CURRENCY_SYMBOLS = set.filterFrom(CURRENCY_SYMBOLS, symbol => (symbol.length === 1) && !/[a-z]/i.test(symbol));

// private functions ==========================================================

/**
 * Creates a new object implementing the `ParsedSection` interface.
 */
function createSection(category: FormatCategory, props?: Partial<ParsedSection>): WritableSection {
    return {
        tokens: [],
        category,
        matchFn: () => true,
        autoMinus: !isAnyDateTime(category) && (category !== FormatCategory.TEXT),
        fillToken: false,
        ...props,
    };
}

/**
 * Creates a new object implementing the `ParsedSection` interface with a
 * single `STD` token.
 */
function createStdSection(artificial?: boolean): WritableSection {
    const tokens: SimpleToken[] = [{ match: "", type: TokenType.STD }];
    return createSection(FormatCategory.STANDARD, { tokens, artificial });
}

/**
 * Creates a new object implementing the `ParsedSection` interface with a
 * single `TEXT` token.
 */
function createTextSection(artificial?: boolean): WritableSection {
    const tokens: SimpleToken[] = [{ match: "", type: TokenType.TEXT }];
    return createSection(FormatCategory.TEXT, { tokens, artificial });
}

/**
 * Changes the interval range covered by a code section.
 *
 * @param section
 *  The code section to be updated.
 *
 * @param op
 *  The operator defining the interval covered by this code section.
 *
 * @param arg
 *  The argument for the operator defining the interval.
 */
function setSectionRange(section: WritableSection, op: SectionOperator, arg: number): void {

    // store passed values in the section
    section.range = { op, arg };

    // initialize the match predicate function
    switch (op) {
        case "<":  section.matchFn = num => num < arg;   break;
        case "<=": section.matchFn = num => num <= arg;  break;
        case ">":  section.matchFn = num => num > arg;   break;
        case ">=": section.matchFn = num => num >= arg;  break;
        case "=":  section.matchFn = num => num === arg; break;
        case "<>": section.matchFn = num => num !== arg; break;
    }

    // initialize the "autoMinus" flag
    switch (op) {
        case "<":  section.autoMinus = arg > 0;  break;
        case "<=": section.autoMinus = arg >= 0; break;
        case ">":  // same auto-minus for both operators (e.g.: [>-1] and [>=-1] cover negative and zero, but [>0] and [>=0] do not)
        case ">=": section.autoMinus = arg < 0;  break;
        case "=":  section.autoMinus = false;    break;
        case "<>": section.autoMinus = true;     break;
    }
}

/**
 * Sets the opposite interval operator to the target section.
 */
function setOppositeRange(toSection: WritableSection, fromSection: WritableSection): void {
    if (fromSection.range) {
        setSectionRange(toSection, OPPOSITE_OPS[fromSection.range.op], fromSection.range.arg);
    }
}

/**
 * Sets the text color to be used to render a value formatted with a code
 * section.
 *
 * @param section
 *  The code section to be updated.
 *
 * @param colorKey
 *  A valid key of a text color.
 */
function setSectionColor(section: WritableSection, colorKey: ColorKey): void {
    section.color = { key: colorKey, css: CSS_COLORS[colorKey] };
}

/**
 * Tries to find a currency symbol in the passed text.
 *
 * @param symbols
 *  A set with currency symbols to be matched.
 *
 * @param text
 *  The text to be searched for a currency symbol.
 *
 * @param partial
 *  If set to `true`, tries to find currency symbols embedded in the text. By
 *  default, only exact matches will be returned.
 *
 * @returns
 *  The extracted currency symbol, if available; otherwise `undefined`.
 */
function findCurrencySymbol(symbols: Set<string>, text: string, partial?: boolean): Opt<string> {

    // try exact match first, for better performance
    if (symbols.has(text)) { return text; }

    // currency symbol may be surrounded by other literal text, e.g. whitespace
    if (partial) {
        for (const symbol of symbols) {
            if (text.includes(symbol)) { return symbol; }
        }
    }

    // nothing found
    return undefined;
}

/**
 * Returns a unique key to identify the locale settings of the current UI
 * locale.
 *
 * @returns
 *  The unique key to identify the current UI locale settings.
 */
export function getLocaleDataKey(): string {
    // everything that can be customized in the user settings must be included in the key
    return "ui:" + json.safeStringify([LOCALE_DATA.intl, LOCALE_DATA.dec, LOCALE_DATA.group, LOCALE_DATA.shortDate, LOCALE_DATA.longDate, LOCALE_DATA.shortTime, LOCALE_DATA.longTime, LOCALE_DATA.unit]);
}

// class FormatError ==========================================================

/**
 * A special exception for the format code parser.
 *
 * @param formatCode
 *  The format code causing the parse error.
 */
export class FormatError extends Error {

    override readonly name = "FormatError";

    readonly formatCode: string;

    constructor(message: string, formatCode: string) {
        super(message);
        this.formatCode = formatCode;
    }
}

// class FormatCodeParser =====================================================

/**
 * A tokenizer and compiler for number format codes.
 */
export class FormatCodeParser extends DObject {

    // global cache for all existing parsers by locale
    static readonly #instanceCache = new Map<string, FormatCodeParser>();

    // static functions -------------------------------------------------------

    static get(options?: ParseFormatOptions): FormatCodeParser {
        const uiGrammar = !!options?.uiGrammar;
        const cacheKey = uiGrammar ? getLocaleDataKey() : "op";
        return map.upsert(FormatCodeParser.#instanceCache, cacheKey, () => new FormatCodeParser(uiGrammar ? LOCALE_DATA : OP_PARSER_CONFIG));
    }

    // properties -------------------------------------------------------------

    // tokens
    readonly #stdToken: string;
    readonly #sepTokens: SepTokens;
    readonly #dateTokens: DateTokens;
    readonly #colorTokens: ColorTokens;

    // whether "month" and "minute" have the same token character (just differ in case)
    readonly #mmConflict: boolean;

    // REs for numeric tokens
    readonly #digitRE: RegExp;
    readonly #decRE: RegExp;

    // REs for date tokens
    readonly #yearRE: RegExp;
    readonly #bYearRE: RegExp;
    readonly #gYearRE: RegExp;
    readonly #gEraRE: RegExp;
    readonly #quarterRE: RegExp;
    readonly #monthRE: RegExp;
    readonly #weekNumRE: RegExp;
    readonly #dayRE: RegExp;
    readonly #weekDayRE: RegExp;
    readonly #weekDayOdfRE: RegExp;

    // REs for time tokens
    readonly #hourRE: RegExp;
    readonly #minRE: RegExp;
    readonly #secRE: RegExp;
    readonly #fsecRE: RegExp;
    readonly #hoursRE: RegExp;
    readonly #minsRE: RegExp;
    readonly #secsRE: RegExp;

    /**
     * Cache for all format codes already parsed. Instances of `ParsedFormat`
     * are immutable and can therefore be cached globally.
     */
    readonly #formatCache = new Map<string, ParsedFormat>();

    /**
     * The entire format code to be parsed. Will be initialized before parsing
     * a format code.
     */
    #formatCode!: string;

    /**
     * Remaining unparsed part of the format code. Will be initialized and
     * manipulated while parsing a format code.
     */
    #remainCode!: string;

    /**
     * The matches of a string or regular expressions. Will be initialized and
     * manipulated while parsing a format code. The internal `matchXY()`
     * methods will assign to `matches` and return a boolean whether the match
     * has succeeded, i.e. whether the variable has been initialized with a
     * match.
     */
    #matches!: string[];

    // constructor ------------------------------------------------------------

    private constructor(parserConfig: CodeParserConfig) {
        super({ _kind: "singleton" });

        // tokens
        this.#stdToken = parserConfig.stdToken;
        this.#sepTokens = {
            dec: parserConfig.dec,
            grp: parserConfig.group,
            date: parserConfig.dateSep,
            time: parserConfig.timeSep
        };
        this.#dateTokens = parserConfig.dateTokens;
        this.#colorTokens = parserConfig.colorTokens;

        // flag for special month/minute conflict handling
        this.#mmConflict = str.equalsICC(this.#dateTokens.M, this.#dateTokens.m);

        // escaped separators
        const dec = re.escape(this.#sepTokens.dec);
        const group = re.escape(this.#sepTokens.grp);
        const dates = dict.mapRecord(this.#dateTokens, re.escape);

        // REs for numeric tokens (with trailing group separators)
        this.#digitRE = new RegExp(`^([0-9#?]+)(${group}*)`, "i");
        this.#decRE = new RegExp(`^${dec}${group}*`, "i");

        // REs for date tokens
        this.#yearRE = new RegExp(`^${dates.Y}+`, "i");
        this.#bYearRE = new RegExp(`^${dates.b}+`, "i");
        this.#gYearRE = new RegExp(`^${dates.e}+`); // lower-case only (upper-case E is for scientific notation)
        this.#gEraRE = new RegExp(`^${dates.g}+`, "i");
        this.#quarterRE = /^QQ?/i; // ODF quarter token (not translated)
        this.#monthRE = new RegExp(`^${dates.M}+`, (parserConfig.opGrammar || !this.#mmConflict) ? "i" : "");
        this.#weekNumRE = /^WW/i; // ODF weeknum token (not translated)
        this.#dayRE = new RegExp(`^${dates.D}+`, "i");
        this.#weekDayRE = new RegExp(`^${dates.a}{3,}`, "i");
        this.#weekDayOdfRE = /^N{2,4}/i; // ODF weekday token (not translated)

        // REs for time tokens
        this.#hourRE = new RegExp(`^${dates.h}+`, "i");
        this.#minRE = new RegExp(`^${dates.m}+`, this.#mmConflict ? "" : "i");
        this.#secRE = new RegExp(`^${dates.s}+`, "i");
        this.#fsecRE = new RegExp(`^${dec}(0{1,3})`, "i"); // only zeros, only directly after separator
        this.#hoursRE = new RegExp(`^\\[(${dates.h}+)\\]`, "i");
        this.#minsRE = new RegExp(`^\\[(${dates.m}+)\\]`, "i"); // ignores month/minute conflict
        this.#secsRE = new RegExp(`^\\[(${dates.s}+)\\]`, "i");
    }

    // public methods ---------------------------------------------------------

    /**
     * Parses the passed format code, and returns an immutable instance of the
     * class `ParsedFormat`. If the format code has been parsed before by this
     * instance, the same `ParsedFormat` will be returned from the internal
     * cache.
     *
     * @param fileFormat
     *  The file format identifier specifying which format tokens will be
     *  recognized by the parser.
     *
     * @param formatCode
     *  The format code to be parsed.
     *
     * @returns
     *  The `ParsedFormat` instance for the passed format code. If the format
     *  code cannot be parsed successfully, the `ParsedFormat` will contain the
     *  category `ERROR`.
     */
    parse(fileFormat: FileFormatType, formatCode: string): ParsedFormat {

        // resolve existing parsed format from cache
        return map.upsert(this.#formatCache, `${fileFormat}:${formatCode}`, () => {

            try {

                // try to parse the format code (may throw)
                return this.#parseFormatCode(fileFormat, formatCode);

            } catch (err) {

                // notify script engine errors
                if (is.scriptError(err)) {
                    globalLogger.exception(err);
                }

                // create a `ParsedFormat` instance with ERROR category
                return new ParsedFormat(formatCode, FormatCategory.ERROR, [], createSection(FormatCategory.CUSTOM));
            }
        });
    }

    /**
     * Checks if at least one ParsedSection can be transformed
     *
     * @param numberSections
     *  An array of format code sections for different number intervals.
     *
     * @returns
     *  true if a numberSections can be transformed
     */
    isTransformable(numberSections: readonly ParsedSection[], options: StringifyFormatOptions): boolean {
        return numberSections.some(section => !!this.#transformSection(section, options));
    }

    /**
     * Creates a formatCode from ParsedSections
     *
     * @param parsedFormat
     *  The parsed format code to be transformed.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  The transformed format code.
     */
    transform(parsedFormat: ParsedFormat, options?: StringifyFormatOptions): string {

        // collect all original sections in the format code
        const parsedSections = parsedFormat.numberSections.filter(section => !section.artificial);
        if (!parsedFormat.textSection.artificial) { parsedSections.push(parsedFormat.textSection); }

        // transform sections, and stringify the resulting tokens back to a format code
        return parsedSections.map(parsedSection => {
            const tokens = this.#transformSection(parsedSection, options);
            return (tokens ?? parsedSection.tokens).map(token => token.match).join("");
        }).join(";");
    }

    // private methods --------------------------------------------------------

    /**
     * Throws a `FormatError` with the specified message text.
     */
    #throw(message: string): never {
        throw new FormatError(message, this.#formatCode);
    }

    /**
     * Throws a `FormatError` with the specified message text, if th epassed
     * condition is falsy.
     */
    #ensure(condition: unknown, message: string): asserts condition {
        if (!condition) { this.#throw(message); }
    }

    /**
     * Removes the specified number of characters from the beginning of the
     * unparsed format code.
     */
    #spliceText(len: number): string {
        const text = this.#remainCode.slice(0, len);
        this.#remainCode = this.#remainCode.slice(len);
        return text;
    }

    /**
     * Tries to match a constant text exactly (with character case). A match
     * will be stored as single element in the variable `matches`.
     */
    #matchText(text: string): boolean {
        if (this.#remainCode.startsWith(text)) {
            this.#matches = [this.#spliceText(text.length)];
            return true;
        }
        return false;
    }

    /**
     * Tries to match a constant text ignoring character case. A match will be
     * stored as single element in the variable `matches`.
     */
    #matchTextNoCase(text: string): boolean {
        if (str.startsWithICC(this.#remainCode, text)) {
            // use the actual format code text with correct character case (instead of `text`)
            this.#matches = [this.#spliceText(text.length)];
            return true;
        }
        return false;
    }

    /**
     * Tries to match a regular expression. The regex MUST start with a "^"
     * character! Match will be stored in the variable `matches`.
     */
    #matchRegExp(regExp: RegExp): boolean {
        const matches = regExp.exec(this.#remainCode);
        if (matches) {
            this.#matches = matches;
            this.#remainCode = this.#remainCode.slice(matches[0].length);
            return true;
        }
        return false;
    }

    /**
     * Parses the next tokens from `remainText`, and creates a `ParsedSection`
     * instance.
     */
    #parseNextSection(fileFormat: FileFormatType, preferText: boolean): ParseSectionResult {

        // the format section to be returned
        const tokens: ParsedToken[] = [];
        const section = createSection(FormatCategory.CUSTOM, { tokens });
        const parseResult: ParseSectionResult = { section, done: true };

        // special behaviour for file formats
        const oox = fileFormat === FileFormatType.OOX;
        const odf = fileFormat === FileFormatType.ODF;

        // whether this format section contains various tokens
        let hasStdToken = false;
        let hasDigitToken = false;
        let hasDecToken = false;
        let hasExpDecToken = false;
        let hasSlashToken = false;
        let hasAMPMToken = false;
        let hasAnyDTToken = false;
        let hasTextToken = false;

        // all digits for different parts of the number
        const digitTokensMap = new Map<TokenType, DigitToken[]>();
        const hasPatternChars = new Set<TokenType>();

        // denominator token in fraction formats
        let denomToken: Opt<DigitToken>;
        // number of percent signs found in the format code
        let pctSigns = 0;
        // first currency symbol found in the format code
        let currency: Opt<SectionCurrencyInfo>;

        // number of date and time tokens
        let dateTokens = 0;
        let timeTokens = 0;
        // the last pushed date/time token
        let lastDateTimeToken: Opt<DateToken | TimeToken>;
        // precision for fractional seconds
        let timePrec = 0;

        // creates a token array for digits for a specific type on demand
        const getDigitTokens = (type: DigitToken["type"]): DigitToken[] => {
            return map.upsert(digitTokensMap, type, () => []);
        };

        // creates and appends new simple token
        const pushSimpleToken = (type: SimpleToken["type"]): void => {
            tokens.push({ match: this.#matches[0], type });
        };

        // creates and appends new token for digit patterns
        const pushDigitToken = (type: DigitToken["type"], pattern: string, groups: number): DigitToken => {
            const size = pattern.replace(/[1-9]+/g, "").length;
            const digitTokens = getDigitTokens(type);
            const token: DigitToken = { match: this.#matches[0], type, pattern, offset: 0, size, groups };
            tokens.push(token);
            digitTokens.push(token);
            if (size > 0) { hasPatternChars.add(type); }
            hasDigitToken = true;
            return token;
        };

        // creates a new date token, and appends it to the token array
        const pushDateToken = (type: DateToken["type"]): void => {
            const size = (this.#matches[1] || this.#matches[0]).length;
            tokens.push(lastDateTimeToken = { match: this.#matches[0], type, size });
            hasAnyDTToken = true;
            dateTokens += 1;
        };

        // creates a new time token, and appends it to the token array
        const pushTimeToken = (type: TimeToken["type"], elapsed?: boolean): void => {
            const size = (this.#matches[1] || this.#matches[0]).length;
            tokens.push(lastDateTimeToken = { match: this.#matches[0], type, size, elapsed });
            hasAnyDTToken = true;
            timeTokens += 1;
        };

        // tries to expand the text of the last literal text token, if it has the same kind
        const pushLitToken = (kind: LiteralTokenKind, text: string): void => {
            const expandable = (kind === "bare") || (kind === "backslash") || (kind === "blind");
            const last = ary.last(tokens);
            if (expandable && last && (last.type === TokenType.LIT) && (last.kind === kind)) {
                last.match += this.#matches[0];
                last.text += text;
            } else {
                tokens.push({ match: this.#matches[0], type: TokenType.LIT, kind, text });
            }
        };

        // creates and appends a dummy token to retain the original format code when stringifying
        const pushDummyToken = (): void => {
            tokens.push({ match: this.#matches[0], type: TokenType.DUMMY });
        };

        // parse tokens until reaching the boundary of a code section
        while (this.#remainCode) {

            // section separator: exit the parser loop
            if (this.#matchText(";")) {
                parseResult.done = false;
                break;
            }

            // standard format token (before any other character-based token)
            if (this.#matchTextNoCase(this.#stdToken)) {
                this.#ensure(!hasStdToken, "multiple STD tokens");
                pushSimpleToken(TokenType.STD);
                hasStdToken = true;
                continue;
            }

            // digits pattern (with trailing group separators)
            // - numbers or mantissa of scientific format (integer part and fractional part, types INT/FRAC)
            // - exponent of scientific format (integer part and fractional part (sic!), types EINT/EFRAC)
            // - numerator/denominator of fraction formats (types NUMER/DENOM)
            if (this.#matchRegExp(this.#digitRE)) {
                if (hasSlashToken) {
                    this.#ensure(!denomToken, "multiple DENOM tokens");
                    denomToken = pushDigitToken(TokenType.DENOM, this.#matches[1], 0);
                } else {
                    const type = section.scientific ? (hasExpDecToken ? TokenType.EFRAC : TokenType.EINT) : (hasDecToken ? TokenType.FRAC : TokenType.INT);
                    pushDigitToken(type, this.#matches[1], section.scientific ? 0 : this.#matches[2].length);
                }
                continue;
            }

            // decimal separator (with trailing group separators that will be ignored)
            if (!hasAnyDTToken && this.#matchRegExp(this.#decRE)) {
                // missing integer digits: use "#" directly before decimal separator
                if (section.scientific) {
                    if (!hasPatternChars.has(TokenType.EINT)) { pushDigitToken(TokenType.EINT, DigitCode.HIDE, 0); }
                    hasExpDecToken = true;
                } else {
                    if (!hasPatternChars.has(TokenType.INT)) { pushDigitToken(TokenType.INT, DigitCode.HIDE, 0); }
                    hasDecToken = true;
                }
                // DEC token may occur multiple times (first is significant but all will be shown)
                pushSimpleToken(TokenType.DEC);
                continue;
            }

            // percent sign (may occur multiple times)
            if (this.#matchText("%")) {
                pushLitToken("bare", "%");
                pctSigns += 1;
                continue;
            }

            // slash character used in fraction codes
            if (!hasAnyDTToken && this.#matchText("/")) {
                this.#ensure(!hasSlashToken, "multiple SLASH tokens");
                this.#ensure(hasDigitToken, "unexpected SLASH token");
                pushLitToken("bare", "/");
                hasSlashToken = true;
                continue;
            }

            // scientific notation
            // - OOXML: always upper-case, always with trailing sign for exponent
            // - ODF: case-insensitive, optional sign for exponent
            if ((oox && this.#matchRegExp(/^(E)([-+])/)) || (odf && this.#matchRegExp(/^([eE])([-+])?/))) {
                this.#ensure(!section.scientific, "multiple EXP tokens");
                pushLitToken("bare", this.#matches[1]);
                section.scientific = { plusSign: this.#matches[2] === "+" };
                continue;
            }

            // date/time: date separator character
            if (hasAnyDTToken && this.#matchText(this.#sepTokens.date)) {
                pushSimpleToken(TokenType.DATESEP);
                continue;
            }

            // date/time: time separator character
            if (hasAnyDTToken && this.#matchText(this.#sepTokens.time)) {
                pushSimpleToken(TokenType.TIMESEP);
                continue;
            }

            // date/time: calendar type ("b1" for Gregorian, "b2" for Hijri)
            if (this.#matchRegExp(/^b(\d)/i)) {
                switch (this.#matches[1]) {
                    case "1":
                        break; // Gregorian
                    case "2":
                        globalLogger.warn("$badge{CodeParser} parseNextSection: unsupported calendar type Hijri");
                        break; // Hijri
                    default:
                        this.#throw("unsupported calendar type");
                }
                continue;
            }

            // date/time: 12 hours, long "AM/PM" marker
            if (this.#matchTextNoCase("AM/PM")) {
                tokens.push({ match: this.#matches[0], type: TokenType.AMPM });
                hasAMPMToken = hasAnyDTToken = true;
                continue;
            }

            // date/time: 12 hours, short "A/P" marker
            if (this.#matchTextNoCase("A/P")) {
                // OOXML: character case according to pattern; ODF: always lowercase
                const pattern = odf ? this.#matches[0].toLowerCase() : this.#matches[0];
                tokens.push({ match: this.#matches[0], type: TokenType.AMPM, fixed: { am: pattern[0], pm: pattern[2] } });
                hasAMPMToken = hasAnyDTToken = true;
                continue;
            }

            // date/time: year
            if (this.#matchRegExp(this.#yearRE)) {
                pushDateToken(TokenType.YEAR);
                continue;
            }

            // date/time: Buddhist year (OOXML)
            if (oox && this.#matchRegExp(this.#bYearRE)) {
                pushDateToken(TokenType.BYEAR);
                continue;
            }

            // date/time: Gengo year (OOXML)
            if (oox && this.#matchRegExp(this.#gYearRE)) {
                pushDateToken(TokenType.GYEAR);
                continue;
            }

            // date/time: Gengo era name (OOXML)
            if (oox && this.#matchRegExp(this.#gEraRE)) {
                pushDateToken(TokenType.GERA);
                continue;
            }

            // date/time: quarter (ODF)
            if (odf && this.#matchRegExp(this.#quarterRE)) {
                pushDateToken(TokenType.QUARTER);
                continue;
            }

            // date/time: month
            if (this.#matchRegExp(this.#monthRE)) {
                // in "mmConflict" mode, the uppercase MONTH token with one or two letters becomes type MIN, if it follows HOUR
                const asMinute = this.#mmConflict && (lastDateTimeToken?.type === TokenType.HOUR) && (this.#matches[0].length <= 2);
                if (asMinute) { pushTimeToken(TokenType.MIN); } else { pushDateToken(TokenType.MONTH); }
                continue;
            }

            // date/time: week number (ODF)
            if (odf && this.#matchRegExp(this.#weekNumRE)) {
                pushDateToken(TokenType.WEEK);
                continue;
            }

            // date/time: day
            if (this.#matchRegExp(this.#dayRE)) {
                pushDateToken(TokenType.DAY);
                continue;
            }

            // date/time: weekday
            if (this.#matchRegExp(this.#weekDayRE)) {
                // token uses the same length as DAY token
                pushDateToken(TokenType.DAY);
                continue;
            }

            // date/time: weekday (ODF)
            if (odf && this.#matchRegExp(this.#weekDayOdfRE)) {
                pushDateToken(TokenType.WEEKDAY);
                continue;
            }

            // date/time: hour
            if (this.#matchRegExp(this.#hourRE)) {
                pushTimeToken(TokenType.HOUR);
                continue;
            }

            // date/time: minute
            if (this.#matchRegExp(this.#minRE)) {
                pushTimeToken(TokenType.MIN);
                continue;
            }

            // date/time: second
            if (this.#matchRegExp(this.#secRE)) {
                // in "mmConflict" mode, a preceding uppercase MONTH token with one or two letters becomes type MIN
                if ((lastDateTimeToken?.type === TokenType.MONTH) && (lastDateTimeToken.size <= 2)) {
                    (lastDateTimeToken as unknown as TimeToken).type = TokenType.MIN;
                    dateTokens -= 1;
                    timeTokens += 1;
                }
                pushTimeToken(TokenType.SEC);
                continue;
            }

            // date/time: fractional seconds
            if (hasAnyDTToken && this.#matchRegExp(this.#fsecRE)) {
                pushTimeToken(TokenType.FSEC);
                timePrec = max(timePrec, this.#matches[1].length);
                continue;
            }

            // date/time: elapsed hours
            if (this.#matchRegExp(this.#hoursRE)) {
                pushTimeToken(TokenType.HOUR, true);
                continue;
            }

            // date/time: elapsed minutes
            if (this.#matchRegExp(this.#minsRE)) {
                pushTimeToken(TokenType.MIN, true);
                continue;
            }

            // date/time: elapsed seconds
            if (this.#matchRegExp(this.#secsRE)) {
                // this token does not change a preceding MONTH token to MIN in "mmConflict" mode
                pushTimeToken(TokenType.SEC, true);
                continue;
            }

            // placeholder for text value
            if (this.#matchText("@")) {
                pushSimpleToken(TokenType.TEXT);
                hasTextToken = true;
                continue;
            }

            // string literal (enclosed in double-quotes)
            if (this.#matchRegExp(/^"([^"]*)"/)) {
                pushLitToken("quote", this.#matches[1]);
                continue;
            }

            // character literal (preceded by backspace)
            if (this.#matchRegExp(/^\\(.)/)) {
                pushLitToken("backslash", this.#matches[1]);
                continue;
            }

            // blind character (preceded by underscore)
            if (this.#matchRegExp(/^_(.)/)) {
                pushLitToken("blind", this.#matches[1]);
                continue;
            }

            // fill character (preceded by asterisk)
            if (this.#matchRegExp(/^\*(.)/)) {
                pushLitToken("fill", this.#matches[1]);
                section.fillToken = true;
                continue;
            }

            // specifiers in brackets
            if (this.#matchRegExp(/^\[([^[\];]*)\]/)) {

                // add dummy token to retain the original format code when stringifying
                pushDummyToken();

                // the text inside the brackets to be parsed
                const value = this.#matches[1];

                // number interval (comparison operator and floating-point number)
                let matches = /^\s*(<>|=|<=?|>=?)\s*(\S+)\s*$/.exec(value);
                if (matches) {
                    this.#ensure(!section.range, "multiple interval operators");
                    const result = NumberParser.parseNumber(matches[2], { decSep: this.#sepTokens.dec, complete: true });
                    this.#ensure(result, "invalid number after interval operator");
                    setSectionRange(section, matches[1] as SectionOperator, result.number);
                    continue;
                }

                // text color name
                const colorKey = COLOR_KEYS.find(key => str.equalsICC(this.#colorTokens[key], value));
                if (colorKey) {
                    this.#ensure(!section.color, "multiple color tokens");
                    setSectionColor(section, colorKey);
                    continue;
                }

                // text color from palette
                matches = /^color(\d+)$/i.exec(value);
                if (matches) {
                    this.#ensure(!section.color, "multiple color tokens");
                    const colorIdx = parseInt(matches[1], 10);
                    this.#ensure((colorIdx >= 1) && (colorIdx <= 56), "invalid palette index");
                    globalLogger.warn(`$badge{FormatCodeParser} parseNextSection: unsupported palette color specifier [${value}]`);
                    continue;
                }

                // CJK numeral system (intentionally ignore garbage after digit, e.g. "[DBNum1xyz]" is [DBNum1])
                matches = oox ? /^dbnum(\d)/i.exec(value) : null;
                if (matches) {
                    this.#ensure(!section.cjkNumId, "multiple DBNum tokens");
                    const numId = section.cjkNumId = parseInt(matches[1], 10);
                    this.#ensure((numId >= 1) && (numId <= 4), "invalid DBNum index");
                    continue;
                }

                // locale data with optional currency symbol and LCID or locale code
                if (value.startsWith("$")) {
                    this.#ensure(!section.locale, "multiple locale specifiers");

                    // split value at first dash character
                    const dashPos = value.indexOf("-");

                    // empty currency symbol is syntactically valid, e.g. [$], [$-], [$-de-DE]
                    const symbol = (dashPos > 0) ? value.slice(1, dashPos) : value.slice(1);
                    if (symbol) {
                        currency = { symbol, blind: false };
                        tokens.pop(); // replace the dummy token with a literal token
                        pushLitToken("bare", symbol);
                    }

                    // missing locale information is allowed, e.g. [$], [$-], [$EUR], [$EUR-]
                    const localeSpec = (dashPos > 0) ? value.slice(dashPos + 1).toLowerCase() : "";
                    if (localeSpec) {

                        // hexadecimal LCID, calendar, numeral system code (prefer language names over hexadecimal
                        // numbers that consist of letter only, e.g. "da" is Danish not 0x00DA; ... luckily there does
                        // not exist any LCID with letters only!)
                        let locale: string;
                        let typeBits: number;
                        if (/^[0-9a-f]+$/.test(localeSpec) && !/^[a-f]+$/.test(localeSpec)) {
                            const bits = parseInt(localeSpec, 16);
                            const lcid = bits & 0xFFFF;
                            locale = LCID_X_MAP[lcid] || localeDataRegistry.resolveLCID(lcid)?.intl || `x-lcid-${lcid}`;
                            typeBits = bits >> 16;
                        } else {
                            // split trailing calendar type and numeral system specifier, e.g. [$-de-DE,202] or [$-,202]
                            matches = /^([a-z]+[-a-z0-9]*)?(?:,([0-9a-f]+))?$/.exec(localeSpec);
                            this.#ensure(matches, `invalid locale specifier "${localeSpec}"`);
                            locale = matches[1];
                            typeBits = matches[2] ? parseInt(matches[2], 16) : 0;
                        }

                        // create the `locale` property
                        const localeId = parseLocaleId(locale);
                        section.locale = {
                            lc: locale,
                            calType: typeBits & 0x7F,         // calendar type in bits 0-6 (bit 7 is for UI parsing)
                            numSysId: (typeBits >> 8) & 0x7F, // numeral system in bits 8-14 (bit 15 is for UI parsing)
                            localeId,
                            lcData: localeId && localeDataRegistry.getLocaleData(localeId),
                        };
                    }
                    continue;
                }

                // warn about, but ignore all other unknown specifiers without parse error
                globalLogger.warn(`$badge{FormatCodeParser} parseNextSection: unsupported section specifier [${value}]`);
                continue;
            }

            // unmatched double-quote, unmatched opening bracket, or incomplete escape sequence: syntax error
            if (this.#matchRegExp(/^["[\\_*]/)) {
                this.#throw("unmatched quote or bracket, or incomplete escape sequence");
            }

            // a few characters must result in a parse error in OOXML
            if (oox && this.#matchRegExp(/^[BENn]/)) {
                this.#throw("invalid token character");
            }

            // any non-matching token is considered a single-character literal
            this.#ensure(this.#matchRegExp(/^./), "cannot match next character");
            pushLitToken("bare", this.#matches[0]);
        }

        // whether the format code contains a decimal separator or an EXP token
        const hasDecExpToken = hasDecToken || section.scientific;

        // text format (no numeric tokens allowed; currency symbols are used literally)
        if (hasTextToken) {
            this.#ensure(!hasStdToken && !hasDigitToken && !hasDecExpToken && !pctSigns && !hasSlashToken, "TEXT token with numeric token");
            this.#ensure(!hasAnyDTToken, "TEXT token with date/time token");
            this.#ensure(!section.range, "unexpected interval operator in text section");
            section.category = FormatCategory.TEXT;
            return parseResult;
        }

        // section contains literal text only
        if (!hasStdToken && !hasDigitToken && !hasDecExpToken && !pctSigns && !hasSlashToken && !hasAnyDTToken && !section.range) {
            // CUSTOM category for number sections, otherwise TEXT category
            section.category = preferText ? FormatCategory.TEXT : FormatCategory.CUSTOM;
            return parseResult;
        }

        // standard format (no further numeric tokens allowed)
        if (hasStdToken) {
            // STD token can be combined with percent sign and currency symbol
            this.#ensure(!hasDigitToken && !hasDecExpToken && !hasSlashToken, "STD token with numeric token");
            this.#ensure(!hasAnyDTToken, "STD token with date/time token");
            // only the pure STD token is STANDARD category (literal text makes it CUSTOM)
            const otherToken = tokens.find(token => (token.type !== TokenType.STD) && (token.type !== TokenType.DUMMY));
            section.category = otherToken ? FormatCategory.CUSTOM : FormatCategory.STANDARD;
            return parseResult;
        }

        // date/time formats (no further numeric tokens allowed, currency symbols are used literally)
        if (hasAnyDTToken) {
            this.#ensure(!hasDigitToken && !hasDecExpToken && !pctSigns && !hasSlashToken, "date/time token with numeric token");
            section.category = !dateTokens ? FormatCategory.TIME : (timeTokens || hasAMPMToken) ? FormatCategory.DATETIME : FormatCategory.DATE;
            section.dateTime = { hours12: hasAMPMToken, precision: timePrec };
            return parseResult;
        }

        // in fraction codes, the last INT token becomes the numerator of the fraction
        const intTokens = getDigitTokens(TokenType.INT);
        const numerToken = hasSlashToken ? intTokens.pop() : undefined;
        if (numerToken) {
            numerToken.type = TokenType.NUMER;
            getDigitTokens(TokenType.NUMER).push(numerToken);
        }

        // add the first/last flags to the digit tokens
        digitTokensMap.forEach(digitTokens => {
            if (digitTokens.length) {
                digitTokens[0].first = true;
                ary.last(digitTokens)!.last = true;
            }
        });

        // calculates the pattern offsets in digit tokens for integer and fractional part
        const reduceOffsetFn = (offset: number, token: DigitToken): number => (token.offset = offset) + token.size;

        // initialize the formatting information for numbers
        const fracTokens = getDigitTokens(TokenType.FRAC);
        section.numbers = {
            // integral part of decimal numbers or mantissa (INT tokens): digit offsets count from right to left
            intDigits: intTokens.reduceRight(reduceOffsetFn, 0),
            // number of trailing "0"s in all INT tokens (minimum integer width of each number)
            minIntDigits: /0*$/.exec(intTokens.map(token => token.pattern).join(""))![0].length,
            // fractional part of numbers (FRAC tokens): digit offsets count from left to right
            fracDigits: fracTokens.reduce(reduceOffsetFn, 0),
            // number of leading "0"s in all FRAC tokens (minimum fraction width of each number)
            minFracDigits: /^0*/.exec(fracTokens.map(token => token.pattern).join(""))![0].length,
            // insert group separators if any but the last INT pattern contains a trailing separator character
            groupInt: intTokens.slice(0, -1).some(token => token.groups > 0),
            // trailing group separators (after integer and fraction patterns) cause reduction of formatted number
            groupBlocks: (ary.last(intTokens)?.groups ?? 0) + (ary.last(fracTokens)?.groups ?? 0),
            // number of percent signs in the format code
            pctSigns
        };

        // integral part of exponent (EINT tokens): digit offsets count from right to left
        const expIntTokens = getDigitTokens(TokenType.EINT);
        expIntTokens.reduceRight(reduceOffsetFn, 0);

        // try to find currency symbol in literal tokens
        if (!currency) {
            for (const token of tokens) {
                if (token.type === TokenType.LIT) {
                    let symbol: Opt<string>;
                    switch (token.kind) {
                        case "bare":    symbol = findCurrencySymbol(SINGLE_CURRENCY_SYMBOLS, token.text, true); break;
                        case "quote":   symbol = findCurrencySymbol(ALL_CURRENCY_SYMBOLS, token.text);          break;
                        case "blind":   symbol = findCurrencySymbol(ALL_CURRENCY_SYMBOLS, token.text, true);    break;
                        default:        break;
                    }
                    // early-exit the loop on first found currency symbol
                    if (symbol) {
                        currency = { symbol, blind: token.kind === "blind" };
                        break;
                    }
                }
            }
        }

        // fraction codes with and without integer part (e.g. "#/# and "# #/#")
        if (hasSlashToken) {
            this.#ensure(!hasDecExpToken, "SLASH token with numeric token");
            this.#ensure(numerToken, "SLASH token without NUMER token");
            this.#ensure(denomToken, "SLASH token without DENOM token");
            if (!pctSigns && !currency) { section.category = FormatCategory.FRACTION; }
            const numPattern = numerToken.pattern;
            const denPattern = denomToken.pattern;
            const intPattern = ary.last(intTokens)?.pattern || "";
            const fixedDenom = /^0*[1-9]\d*$/.test(denPattern);
            this.#ensure(fixedDenom || /^[0#?]+$/.test(denPattern), "invalid characters in DENOM token");
            const intMode = intPattern.endsWith(DigitCode.HIDE) ? DigitCode.HIDE : intPattern.endsWith(DigitCode.BLIND) ? DigitCode.BLIND : DigitCode.ZERO;
            const numerMode = /\d/.test(numPattern) ? DigitCode.ZERO : numPattern.includes("?") ? DigitCode.BLIND : DigitCode.HIDE;
            const denomMode = denPattern.startsWith(DigitCode.HIDE) ? DigitCode.HIDE : denPattern.startsWith(DigitCode.BLIND) ? DigitCode.BLIND : DigitCode.ZERO;
            section.fraction = {
                fixedDenom,
                denomSize: fixedDenom ? parseInt(denPattern, 10) : denPattern.length,
                shortFrac: !intTokens.length,
                intMode,
                numerMode,
                denomMode
            };
            return parseResult;
        }

        // category CUSTOM if any numeric tokens are missing
        if (!hasDigitToken) {
            return parseResult;
        }

        // scientific notation
        if (section.scientific) {
            this.#ensure(intTokens.length, "EXP token without mantissa");
            this.#ensure(expIntTokens.length, "EXP token without exponent");
            if (!pctSigns && !currency) { section.category = FormatCategory.SCIENTIFIC; }
            return parseResult;
        }

        // percentage
        if (pctSigns) {
            if (!currency) { section.category = FormatCategory.PERCENT; }
            return parseResult;
        }

        // currency or accounting
        if (currency) {
            section.category = section.fillToken ? FormatCategory.ACCOUNTING : FormatCategory.CURRENCY;
            section.currency = currency;
            return parseResult;
        }

        // regular number
        section.category = FormatCategory.NUMBER;
        return parseResult;
    }

    /**
     * Parses the passed format code, and returns a new instance of the class
     * `ParsedFormat`.
     *
     * @param fileFormat
     *  The file format identifier specifying which format tokens will be
     *  recognized by the parser.
     *
     * @param formatCode
     *  The format code to be parsed.
     *
     * @returns
     *  The `ParsedFormat` instance for the passed format code.
     *
     * @throws
     *  A `FormatError` if the format code cannot be parsed.
     */
    #parseFormatCode(fileFormat: FileFormatType, formatCode: string): ParsedFormat {

        // bug 46211: empty format code falls back to standard format
        this.#formatCode = this.#remainCode = (formatCode.length === 0) ? this.#stdToken : formatCode;

        // the resulting numeric sections
        const numberSections: WritableSection[] = [];
        // the resulting text section
        let textSection: Opt<WritableSection>;

        // parse until the entire format code has been consumed
        let done = false;
        while (!done) {

            // compile the parse tokens to the `ParsedSection` instance (may throw)
            const parseResult = this.#parseNextSection(fileFormat, numberSections.length === 3);
            const parsedSection = parseResult.section;
            done = parseResult.done;

            // store the new code section in the correct place
            if (parsedSection.category === FormatCategory.TEXT) {
                this.#ensure(done, "embedded text section");
                textSection = parsedSection;
            } else {
                this.#ensure(numberSections.length < 3, "too many number sections");
                numberSections.push(parsedSection);
            }
        }

        // determine category of the format code (before adding default sections)
        let category: FormatCategory;
        const sectionCategs = numberSections.map(section => section.category);
        if (sectionCategs.length === 0) {
            // text category, if there are no numeric sections at all
            category = FormatCategory.TEXT;
        } else if ((sectionCategs.length === 1) && (sectionCategs[0] === FormatCategory.STANDARD)) {
            // standard category only for standard format code (optionally with color)
            category = FormatCategory.STANDARD;
        } else {
            // use the category of the numeric sections, if they are all equal
            // DOCS-3327: ignore third section (e.g., zero part of accounting formats misses any digits)
            if (sectionCategs[2] === FormatCategory.CUSTOM) { sectionCategs.length = 2; }
            category = (new Set(sectionCategs).size === 1) ? sectionCategs[0] : FormatCategory.CUSTOM;
        }

        // initialize default operator intervals of numeric code sections
        switch (numberSections.length) {

            // text format only: format numbers with standard number format
            case 0:
                numberSections.push(createStdSection(true));
                break;

            // one number section for all numbers
            case 1: {
                const [numSec1] = numberSections;
                // if section is restricted, add standard section for remaining numbers
                // DOCS-4974: but not if section is a standard section by itself
                if (numSec1.range && (numSec1.category !== FormatCategory.STANDARD)) {
                    const numSec2 = createStdSection(true);
                    setOppositeRange(numSec2, numSec1);
                    numberSections.push(numSec2);
                }
                break;
            }

            // two number sections: depends on existence of interval range
            case 2: {
                const [numSec1, numSec2] = numberSections;
                if (!numSec1.range && !numSec2.range) {
                    // both section unrestricted: first section is for non-negative numbers
                    setSectionRange(numSec1, ">=", 0);
                    setSectionRange(numSec2, "<", 0);
                } else if (!numSec1.range) {
                    // trailing range: first section is for positive numbers only
                    setSectionRange(numSec1, ">", 0);
                } else if (!numSec2.range) {
                    // leading range: second section is for all remaining numbers
                    setOppositeRange(numSec2, numSec1);
                }
                break;
            }

            case 3: {
                const [numSec1, numSec2, numSec3] = numberSections;
                // operator in third section is not allowed (always for all remaining numbers)
                this.#ensure(!numSec3.range, "unexpected interval operator in third section");
                // first sections always defaults to positive numbers (without zero)
                if (!numSec1.range) { setSectionRange(numSec1, ">", 0); }
                // second sections always defaults to negative numbers (without zero)
                if (!numSec2.range) { setSectionRange(numSec2, "<", 0); }
                break;
            }

            // invalid count of number sections
            default:
                this.#throw("too many number sections");
        }

        // fall-back to standard text secion
        if (!textSection) {
            textSection = createTextSection(true);
            // use color of first number section, if it is the standard format code
            if (numberSections[0].color && (numberSections[0].category === FormatCategory.STANDARD)) {
                textSection.color = numberSections[0].color;
            }
        }

        // success: create and return the parsed format code instance
        return new ParsedFormat(formatCode, category, numberSections, textSection);
    }

    /**
     * Transforms the tokens of a parsed section.
     *
     * @param parsedSection
     *  The parsed format section to be transformed.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  An array of transformed `ParsedToken` objects. If nothing was
     *  transformed, the unchanged tokens of the passed `ParsedSection` will be
     *  returned.
     */
    #transformSection(parsedSection: ParsedSection, options?: StringifyFormatOptions): Opt<readonly ParsedToken[]> {

        const { tokens } = parsedSection;

        if (options?.increaseDecimalPlaces === undefined || parsedSection.category === FormatCategory.FRACTION) { return; } // nothing to do

        if (parsedSection.category === FormatCategory.STANDARD) {
            const tokenIdx = tokens.findIndex(token => token.type === TokenType.STD);
            const stdToken = tokens[tokenIdx];
            if (stdToken && options.increaseDecimalPlaces) { // increasing Standard token type... decreasing not possible, remains standard
                const transformedTokens = [...tokens];
                transformedTokens.splice(tokenIdx, 1,
                    { type: TokenType.INT, match: "0", pattern: "0", offset: 0, size: 1, groups: 0, last: true },
                    { type: TokenType.DEC, match: this.#sepTokens.dec },
                    { type: TokenType.FRAC, match: "0", pattern: "0", offset: 0, size: 1, groups: 0, last: true },
                );
                return transformedTokens;
            }
            return;  // nothing to do
        }

        let intTokenIndex = -1,
            decTokenIndex = -1, decTokenCount = 0,
            fracTokenIndex = -1, fracTokenCount = 0;

        tokens.forEach((token, index) => {
            if (token.type === TokenType.INT && token.last) {
                intTokenIndex = index;
            } else if (token.type === TokenType.DEC) {
                decTokenIndex = index;
                decTokenCount++;
            } else if (token.type === TokenType.FRAC) {
                fracTokenIndex = index;
                fracTokenCount++;
            }
        });
        if (intTokenIndex !== -1 && decTokenCount <= 1 && fracTokenCount <= 1) {
            if (options.increaseDecimalPlaces) {
                const transformedTokens = [...tokens];
                if (decTokenCount === 0) {
                    decTokenIndex = intTokenIndex + 1;
                    ary.insertAt(transformedTokens, decTokenIndex, { type: TokenType.DEC, match: this.#sepTokens.dec });
                }
                if (fracTokenCount === 0) {
                    fracTokenIndex = decTokenIndex + 1;
                    ary.insertAt(transformedTokens, fracTokenIndex, { type: transformedTokens[intTokenIndex].type === TokenType.EINT ? TokenType.EFRAC : TokenType.FRAC, match: "0", pattern: "0", offset: 0, groups: 0, size: 1 });
                } else {
                    const sourceToken = transformedTokens[fracTokenIndex] as DigitToken;
                    if (sourceToken.pattern.length >= 127) {
                        return;  // not using more than 127 decimal places
                    }
                    transformedTokens[fracTokenIndex] = {
                        type: sourceToken.type,
                        match: sourceToken.pattern + ary.last(sourceToken.pattern)!,
                        pattern: sourceToken.pattern + ary.last(sourceToken.pattern)!,
                        offset: sourceToken.offset,
                        groups: sourceToken.groups,
                        size: sourceToken.size + 1,
                    };
                }
                return transformedTokens;
            } else  if (fracTokenCount > 0 && decTokenCount > 0) {
                const transformedTokens = [...tokens];
                const sourceToken = transformedTokens[fracTokenIndex] as DigitToken;
                // check if frac and dec are to be removed
                if (sourceToken.size === 1) {
                    ary.deleteAt(transformedTokens, fracTokenIndex);
                    ary.deleteAt(transformedTokens, decTokenIndex);
                } else {
                    // decrease frac token size
                    transformedTokens[fracTokenIndex] = {
                        type: sourceToken.type,
                        match: sourceToken.pattern.slice(0, -1),
                        pattern: sourceToken.pattern.slice(0, -1),
                        offset: sourceToken.offset,
                        groups: sourceToken.groups,
                        size: sourceToken.size - 1,
                    };
                }
                return transformedTokens;
            }
        }
        return undefined;
    }
}
