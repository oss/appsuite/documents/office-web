/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { to, re, map } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import type { ParseValueBaseOptions, ParseValueBaseResult } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";

// types ======================================================================

/**
 * Types of floating-point numbers that can be parsed by a `NumberParser`.
 *
 * - "decimal": Decimal notation with optional exponent (scientific notation).
 * - "fractional": Fractional notation with numerator and denominator.
 */
export type ParseNumberType = "decimal" | "fractional";

/**
 * Optional parameters for parsing floating-point numbers from strings.
 */
export interface ParseNumberOptions extends ParseValueBaseOptions {

    /**
     * If set to `true`, the group separator of the current UI language will be
     * accepted in the integer part of the parsed text. May also be set to a
     * custom group separator character. If set to `false` or an empty string,
     * group separators will not be accepted at all in the parsed string.
     * Default value is `false`.
     */
    groupSep?: string | boolean;

    /**
     * If set to a specific type specifier, the parser will only parse the
     * specified type of numbers (decimal or fractional notation). By default,
     * the parser will try both types of numbers.
     */
    parseType?: ParseNumberType;
}

/**
 * The result descriptor for a floating-point number parsed from a string.
 */
export interface ParseNumberResult extends ParseValueBaseResult {

    /**
     * The parsed floating-point number.
     */
    number: number;

    /**
     * The extracted sign character. Will be the empty string, if no sign
     * exists in the parsed text, or if sign characters are not allowed in the
     * parsed text.
     */
    sign: string;

    /**
     * Whether the minus sign is present in the parsed text (also for negative
     * zero).
     */
    negative: boolean;

    /**
     * Whether the number in the parsed text contains at least one group
     * separator character.
     */
    grouped: boolean;

    /**
     * Whether the number in the parsed text contains a decimal separator, also
     * if it is not followed by fractional digits (e.g. "123.").
     */
    dec: boolean;

    /**
     * Whether the parsed text contains a number in scientific notation.
     * - `false`: decimal notation, e.g. "1.23".
     * - `true`: scientific notation, e.g. "1.23E+4".
     */
    scientific: boolean;

    /**
     * The particles of a number in fractional notation, e.g. the triple
     * `[1, 2, 3]` for the fraction "1 2/3".
     */
    fraction: Opt<[int: number, num: number, den: number]>;
}

// class NumberParser =========================================================

export class NumberParser {

    // static functions -------------------------------------------------------

    /** Cache for number parsers with localized regular expressions. */
    static readonly #parserCache = new Map<string, NumberParser>();

    /**
     * Tries to parse a floating-point number in decimal, scientific, or
     * fractional notation from the leading part of the passed string.
     *
     * @param text
     *  The text potentially starting with a floating-point number.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  A result descriptor, if the passed string starts with a valid floating-
     *  point number (according to the passed options); otherwise `undefined`.
     */
    static parseNumber(text: string, options?: ParseNumberOptions): Opt<ParseNumberResult> {

        // the localized decimal separator
        const decSep = options?.decSep ?? LOCALE_DATA.dec;
        // the localized group separator used to parse the integer
        const groupSep = (options?.groupSep === true) ? LOCALE_DATA.group : to.string(options?.groupSep, "");
        // cache key with effective separators
        const cacheKey = decSep + "\0" + groupSep;

        // resolve existing parser instance or create a new one; and parse the string
        const parser = map.upsert(NumberParser.#parserCache, cacheKey, () => new NumberParser(decSep, groupSep));
        return parser.#parseNumber(text, options);
    }

    // properties -------------------------------------------------------------

    /** Effective group separator. */
    readonly #groupSep: string;
    /** Regular expressions with capturing group indexes. */
    readonly #regExpList: Array<[ParseNumberType, RegExp]> = [];

    // constructor ------------------------------------------------------------

    private constructor(decSep: string, groupSep: string) {

        // properties
        this.#groupSep = groupSep;

        // separators may be periods and must be escaped in REs
        const decSepPtn = re.capture(re.escape(decSep));
        const groupSepPtn = re.escape(groupSep);
        // capturing pattern for optional leading sign
        const signPtn = re.capture("[-+]?") + " *";

        // capturing pattern for absolute integers with optional group separators
        // - each group except first must consist of at least three digits (e.g. "12,34" is not acceptable)
        // - groups may consist of more than 3 digits (e.g. "1,2345,6789" will be parsed to a grouped integer)
        // - group separator must not be leading, trailing, or repeated (e.g. ",123" or "123,,456")
        // - TODO: support other grouping patterns (e.g. Lakh/Crore "12,34,56,789")?
        const intPtn = re.capture("\\d+" + (groupSep ? (re.group(groupSepPtn + "\\d{3,}") + "*") : ""));
        // capturing group for optional exponent for scientific notation
        const expPtn = re.group.opt("E" + re.capture("[-+]?\\d+"));

        // regular expression for fractional notation with optional sign (e.g. "- 1 2/3")
        this.#registerRegExp("fractional", signPtn + intPtn + " " + re.capture("\\d+") + "/" + re.capture("\\d+"));

        // regular expression for decimal/scientific notation with existing integer
        this.#registerRegExp("decimal", signPtn + intPtn + re.group.opt(decSepPtn + re.capture("\\d*")) + expPtn);

        // regular expression for decimal/scientific notation without integer
        this.#registerRegExp("decimal", signPtn + re.capture("") + decSepPtn + re.capture("\\d+") + expPtn);
    }

    // private methods --------------------------------------------------------

    #registerRegExp(type: ParseNumberType, pattern: string): void {
        this.#regExpList.push([type, new RegExp("^" + pattern, "i")]);
    }

    #parseNumber(text: string, options: Opt<ParseNumberOptions>): Opt<ParseNumberResult> {

        // try to parse the input text with the different regular expressions
        for (const [type, regExp] of this.#regExpList) {

            // skip decimal or fractional notation if specified
            if (options?.parseType && (options.parseType !== type)) { continue; }

            // try to match the beginning of the input text
            const matches = regExp.exec(text);
            if (!matches) { continue; }

            // reject trailing garbage if specified in the passed options
            const remaining = text.slice(matches[0].length);
            if (remaining && options?.complete) { return; }

            // extract integer part, remove all group separators
            const intStr = matches[2];
            const grouped = !!this.#groupSep && intStr.includes(this.#groupSep);
            const intDigits = grouped ? intStr.replaceAll(this.#groupSep, "") : intStr;

            // create the result object
            const result: ParseNumberResult = {
                text: matches[0],
                remaining,
                number: 0,
                sign: matches[1],
                negative: matches[1] === "-",
                grouped,
                dec: false,
                scientific: false,
                fraction: undefined,
            };

            // parse fractional or decimal notation
            switch (type) {

                case "decimal": {
                    result.dec = !!matches[3];
                    // the floating-point text to be parsed (must be parsed entirely to prevent rounding errors)
                    let floatText = intDigits || "0";
                    if (matches[4]) { floatText += "." + matches[4]; }
                    if (matches[5]) { floatText += "e" + matches[5]; result.scientific = true; }
                    // let native function "parseFloat" parse the entire thing to reduce rounding errors
                    result.number = parseFloat(floatText);
                    if (!Number.isFinite(result.number)) { return; }
                    break;
                }

                case "fractional": {
                    const int = parseInt(intDigits, 10);
                    const numer = parseInt(matches[3], 10);
                    const denom = parseInt(matches[4], 10);
                    const quotient = int + (numer / denom);
                    if (!Number.isFinite(quotient)) { return; }
                    result.fraction = [int, numer, denom];
                    result.number = quotient;
                    break;
                }
            }

            // negate the resulting number
            if (result.negative) { result.number *= -1; }

            return result;
        }

        // no matching regular expression found
        return undefined;
    }
}
