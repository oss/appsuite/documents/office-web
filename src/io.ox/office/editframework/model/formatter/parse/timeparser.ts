/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re, map } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { type TimeComponents, MSEC_PER_DAY } from "@/io.ox/office/tk/utils/dateutils";

import type { ParseValueBaseOptions, ParseValueBaseResult } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";
import { createSliceMatcher } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";

// types ======================================================================

/**
 * Optional parameters for parsing time values from strings.
 */
export interface ParseTimeOptions extends ParseValueBaseOptions { }

/**
 * The result descriptor for a time value parsed from a string.
 */
export interface ParseTimeResult extends ParseValueBaseResult {

    /**
     * A UTC date object representing the parsed time.
     */
    time: Date;

    /**
     * The parsed time, as floating-point number (fraction of a day).
     */
    serial: number;

    /**
     * Whether the parsed text contains an AM/PM token.
     */
    hours12: boolean;

    /**
     * Whether the text contains a seconds part, even if it is zero.
     */
    seconds: boolean;

    /**
     * Whether the text contains a fractional seconds part, even if it is zero.
     */
    fraction: boolean;
}

// class TimeParser ===========================================================

export class TimeParser {

    // static functions -------------------------------------------------------

    /** Cache for time parsers with localized regular expressions. */
    static readonly #parserCache = new Map<string, TimeParser>();

    /**
     * Tries to parse a time value from the leading part of the passed string.
     *
     * @param text
     *  The text potentially starting with a time value.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  A result descriptor, if the passed string starts with a valid time
     * value (according to the passed options); otherwise `undefined`.
     */
    static parseTime(text: string, options?: ParseTimeOptions): Opt<ParseTimeResult> {

        // the localized decimal separator to be used to parse input texts
        const decSep = options?.decSep ?? LOCALE_DATA.dec;
        // cache key: "intl" for AM/PM tokens, plus customizable decimal separator
        const cacheKey = LOCALE_DATA.intl + "\0" + decSep;

        // resolve existing parser instance or create a new one; and parse the string
        const parser = map.upsert(TimeParser.#parserCache, cacheKey, () => new TimeParser(decSep));
        return parser.#parseTime(text, options);
    }

    // properties -------------------------------------------------------------

    /** Regular expressions with capturing group indexes. */
    readonly #regExpList: Array<[RegExp, Partial<TimeComponents & { h12: number }>]> = [];
    /** Reverse map for AM/PM string tokens to state flag (`true` => PM). */
    readonly #hourModeFlagMap: Map<string, boolean>;

    // constructor ------------------------------------------------------------

    private constructor(decSep: string) {

        // separators with optional surrounding whitespace (space characters only)
        const timeSepPtn = " *: *";
        const decSepPtn = " *" + re.escape(decSep) + " *";
        // pattern for leading hours or minutes (up to 4 digits, e.g. support for multiple days like "48:00:00")
        const leadIntPtn = re.capture("\\d{1,4}");
        // pattern for hours in range [1..12] with AM/PM token
        const hour12Ptn = re.capture("1[012]", "0?[1-9]");
        // pattern for trailing minutes or seconds (from 0 to 59)
        const minSecPtn = timeSepPtn + re.capture("[0-5]?\\d");
        // fractional seconds following the seconds
        const fracPtn = re.group(decSepPtn + re.capture("\\d+"));
        // pattern for minutes, optional seconds, and optional fractional seconds
        const minSecFracPtn = minSecPtn + re.group.opt(minSecPtn + fracPtn + "?");

        // capturing pattern and reverse map for 12-hour clock tokens
        const [hourModePtn, hourModeFlagMap] = createSliceMatcher([
            { match: LOCALE_DATA.hourModes.am, value: false },
            { match: LOCALE_DATA.hourModes.pm, value: true },
            { match: "am.", value: false }, // always include literal "am" and "pm"
            { match: "pm.", value: true },
        ], 1);

        // time format "[mm]:ss.00" with mandatory fractional seconds
        this.#registerRegExp(leadIntPtn + minSecPtn + fracPtn, { m: 1, s: 2, ms: 3 });

        // time format "hh:mm:ss.00 AM/PM" with optional minutes, seconds, and fractional seconds
        this.#registerRegExp(hour12Ptn + re.group.opt(minSecFracPtn) + " +" + hourModePtn, { h: 1, m: 2, s: 3, ms: 4, h12: 5 });

        // time format "[hh]:mm:ss.00" with optional seconds and fractional seconds
        this.#registerRegExp(leadIntPtn + minSecFracPtn, { h: 1, m: 2, s: 3, ms: 4 });

        // the 12-hours clock tokens mapping
        this.#hourModeFlagMap = hourModeFlagMap;
    }

    // private methods --------------------------------------------------------

    #registerRegExp(pattern: string, indexes: Partial<TimeComponents & { h12: number }>): void {
        this.#regExpList.push([new RegExp("^" + pattern, "i"), indexes]);
    }

    #parseTime(text: string, options: Opt<ParseTimeOptions>): Opt<ParseTimeResult> {

        // try to parse the input text with the different regular expressions
        for (const [regExp, indexes] of this.#regExpList) {

            // try to match the beginning of the input text
            const matches = regExp.exec(text);
            if (!matches) { continue; }

            // reject trailing garbage if specified in the passed options
            const remaining = text.slice(matches[0].length);
            if (remaining && options?.complete) { return; }

            // parse time components
            const hourStr = indexes.h ? matches[indexes.h] : "";
            let hour = hourStr ? parseInt(hourStr, 10) : 0;
            const minStr = indexes.m ? matches[indexes.m] : "";
            const min = minStr ? parseInt(minStr, 10) : 0;
            const secStr = indexes.s ? matches[indexes.s] : "";
            const sec = secStr ? parseInt(secStr, 10) : 0;
            const msecStr = indexes.ms ? matches[indexes.ms] : "";
            const msec = msecStr ? (parseFloat("0." + msecStr) * 1000) : 0;

            // parse the AM/PM token
            const hourMode = indexes.h12 ? matches[indexes.h12] : "";
            if (hourMode) {
                const pm = this.#hourModeFlagMap.get(hourMode.toLowerCase());
                hour = (hour % 12) + (pm ? 12 : 0);
            }

            // create the Date object and the serial number of the time
            const time = new Date(Date.UTC(1970, 0, 1, hour, min, sec, msec));
            const serial = time.getTime() / MSEC_PER_DAY;

            // create and return the result object
            return { text: matches[0], remaining, time, serial, hours12: !!hourMode, seconds: !!secStr, fraction: !!msecStr };
        }

        // no matching regular expression found
        return undefined;
    }
}
