/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, re, map } from "@/io.ox/office/tk/algorithms";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { type DateComponents, expandYear, getDaysInMonth } from "@/io.ox/office/tk/utils/dateutils";

import type { ParseValueBaseOptions, ParseValueBaseResult } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";
import { createSliceMatcher } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";

// types ======================================================================

/**
 * Optional parameters for parsing date values from strings.
 */
export interface ParseDateOptions extends ParseValueBaseOptions { }

/**
 * The result descriptor for a date value parsed from a string.
 */
export interface ParseDateResult extends ParseValueBaseResult {

    /**
     * A UTC date object representing the parsed date.
     */
    date: Date;

    /**
     * Whether the text contains a year part.
     */
    hasYear: boolean;

    /**
     * Whether the text contains a day part.
     */
    hasDay: boolean;
}

// class DateParser ===========================================================

export class DateParser {

    // static functions -------------------------------------------------------

    /** Cache for date parsers with localized regular expressions. */
    static readonly #parserCache = new Map<string, DateParser>();

    /**
     * Tries to parse a date value from the leading part of the passed string.
     *
     * @param text
     *  The text potentially starting with a date value.
     *
     * @param options
     *  Optional parameters.
     *
     * @returns
     *  A result descriptor, if the passed string starts with a valid date
     * value (according to the passed options); otherwise `undefined`.
     */
    static parseDate(text: string, options?: ParseDateOptions): Opt<ParseDateResult> {

        // the localized decimal separator to be used to parse input texts
        const decSep = options?.decSep ?? LOCALE_DATA.dec;
        // cache key: "intl" for month names, plus customizable decimal separator
        const cacheKey = LOCALE_DATA.intl + "\0" + decSep;

        // resolve existing parser instance or create a new one; and parse the string
        const parser = map.upsert(DateParser.#parserCache, cacheKey, () => new DateParser(decSep));
        return parser.#parseDate(text, options);
    }

    // properties -------------------------------------------------------------

    /** Regular expressions with capturing group indexes. */
    readonly #regExpList: Array<[RegExp, Partial<DateComponents & { name: true }>]> = [];
    /** Reverse map for month name tokens to month index. */
    readonly #monthNameMap: Map<string, number>;

    // constructor ------------------------------------------------------------

    private constructor(decSep: string) {

        // Some notes on token order and date separators supported by MS Excel:
        //
        // There are three locale-native date token orders: "D-M-Y", "M-D-Y", and "Y-M-D". Dates entered in native
        // token order will always be parsed. However, for some reason, MS Excel chooses to not parse dates with
        // literal month names in any other order than "D-M-Y". For example, in "en-US" locale using "M-D-Y" token
        // order, "2/28/99" is valid, but "Feb/28/99" is not valid while "28/Feb/99" actually is.
        //
        // The generic ISO date format "YYYY-MM-DD" will always be supported with long year, ordinal month, and all
        // kinds of date separators, e.g. "1999-02-28" is always valid, but "99-02-28" or "1999-Feb-28" may not.
        //
        // Different date separators (dash, slash, period, and space) will be used to parse dates regardless of the
        // locale-native date separator, with one exception: If the period is the decimal separator, it will not be
        // used as date separator at all. However, the group separator will be ignored. For example, in "de-DE" locale,
        // "2.1999" will be parsed as integer 21999 with group separator (although it is misplaced), but "Feb.1999"
        // will be parsed as date. The space character will only be used in conjunction with literal month names (e.g.,
        // "28 Feb 1999" is valid, but "28 2 1999" is always invalid). Additionally, the date separator may be omitted
        // completely next to the month name (e.g. "28Feb1999") which may be the reason that month names are only
        // accepted in the middle, who knows. Mixed date separators can appear in the string, e.g. "28.2/1999". The
        // space character limitation still applies, e.g. "28/2 1999" is invalid, but "28/Feb 1999" or "28.Feb1999" are
        // valid.

        // capturing patterns for short or full year
        const anyYearPtn = re.capture("\\d{4}", "\\d{1,2}");
        const fullYearPtn = re.capture("\\d{4}");
        // capturing pattern for one-based month index
        const monthIdxPtn = re.capture("1[012]", "0?[1-9]");
        // capturing pattern for one-based day index
        const dayPtn = re.capture("3[01]", "[12]\\d", "0?[1-9]");
        // date separator characters for dates with ordinal month (no space, not optional), and with month name
        const dateSepClass = re.class("-/", (decSep !== ".") && ".");
        const dateSepOrdPtn = " *" + dateSepClass + " *";
        const dateSepNamePtn = " *" + dateSepClass + "? *";
        const trailingDotPtn = re.group.opt(" *\\.");

        // capturing pattern and reverse map for month names
        const [monthNamePtn, monthNameMap] = createSliceMatcher(
            LOCALE_DATA.longMonths.map((monthName, monthIdx) => ({ match: monthName, value: monthIdx })),
            3   // match at least the first three characters of month names
        );

        // capturing group indexes for different date token orders
        const DMY: DateComponents = { D: 1, M: 2, Y: 3 };
        const MDY: DateComponents = { M: 1, D: 2, Y: 3 };
        const YMD: DateComponents = { Y: 1, M: 2, D: 3 };

        // all three date components in native locale and ISO order, with ordinal month
        const dmyOrdPtn = dayPtn + dateSepOrdPtn + monthIdxPtn + dateSepOrdPtn + anyYearPtn;
        const mdyOrdPtn = monthIdxPtn + dateSepOrdPtn + dayPtn + dateSepOrdPtn + anyYearPtn;
        const ymdOrdPtn = anyYearPtn + dateSepOrdPtn + monthIdxPtn + dateSepOrdPtn + dayPtn;
        if (LOCALE_DATA.leadingYear) {
            this.#registerRegExp(ymdOrdPtn, YMD);
        } else if (LOCALE_DATA.leadingMonth) {
            this.#registerRegExp(mdyOrdPtn, MDY);
            this.#registerRegExp(ymdOrdPtn, YMD);
        } else {
            this.#registerRegExp(dmyOrdPtn, DMY);
            this.#registerRegExp(ymdOrdPtn, YMD);
        }

        // all three date components with month name in "D-M-Y" or "Y-M-D" order (but not "M-D-Y")
        const dmyNamePtn = dayPtn + dateSepNamePtn + monthNamePtn + dateSepNamePtn + anyYearPtn;
        const ymdNamePtn = anyYearPtn + dateSepNamePtn + monthNamePtn + dateSepNamePtn + dayPtn;
        if (LOCALE_DATA.leadingYear) {
            this.#registerRegExp(ymdNamePtn, YMD, true);
            this.#registerRegExp(dmyNamePtn, DMY, true);
        } else {
            this.#registerRegExp(dmyNamePtn, DMY, true);
            this.#registerRegExp(ymdNamePtn, YMD, true);
        }

        // capturing group indexes for two date tokens
        const DM = { D: 1, M: 2 };
        const MD = { M: 1, D: 2 };
        const MY = { M: 1, Y: 2 };
        const YM = { Y: 1, M: 2 };

        // month/year without day in any order (full year only)
        const myOrdPtn = monthIdxPtn + dateSepOrdPtn + fullYearPtn;
        const ymOrdPtn = fullYearPtn + dateSepOrdPtn + monthIdxPtn;
        const myNamePtn = monthNamePtn + dateSepNamePtn + fullYearPtn;
        const ymNamePtn = fullYearPtn + dateSepNamePtn + monthNamePtn;
        if (LOCALE_DATA.leadingYear) {
            this.#registerRegExp(ymOrdPtn, YM);
            this.#registerRegExp(ymNamePtn, YM, true);
            this.#registerRegExp(myOrdPtn, MY);
            this.#registerRegExp(myNamePtn, MY, true);
        } else {
            this.#registerRegExp(myOrdPtn, MY);
            this.#registerRegExp(myNamePtn, MY, true);
            this.#registerRegExp(ymOrdPtn, YM);
            this.#registerRegExp(ymNamePtn, YM, true);
        }

        // day/month without year in native locale and "D-M" order (accepting additional trailing dot)
        if (LOCALE_DATA.leadingMonth) {
            this.#registerRegExp(monthIdxPtn + dateSepOrdPtn + dayPtn + trailingDotPtn, MD);
            this.#registerRegExp(monthNamePtn + dateSepNamePtn + dayPtn + trailingDotPtn, MD, true);
        }
        this.#registerRegExp(dayPtn + dateSepOrdPtn + monthIdxPtn + trailingDotPtn, DM);
        this.#registerRegExp(dayPtn + dateSepNamePtn + monthNamePtn + trailingDotPtn, DM, true);

        // the month name mapping
        this.#monthNameMap = monthNameMap;
    }

    // private methods --------------------------------------------------------

    #registerRegExp(pattern: string, indexes: Partial<DateComponents>, name?: true): void {
        this.#regExpList.push([new RegExp("^" + pattern, "i"), name ? { ...indexes, name: true } : indexes]);
    }

    #parseDate(text: string, options: Opt<ParseDateOptions>): Opt<ParseDateResult> {

        // try to parse the input text with the different regular expressions
        for (const [regExp, indexes] of this.#regExpList) {

            // try to match the beginning of the input text
            const matches = regExp.exec(text);
            if (!matches) { continue; }

            // parse date components
            const yearStr = indexes.Y ? matches[indexes.Y] : "";
            const year = !yearStr ? new Date().getFullYear() : (yearStr.length <= 2) ? expandYear(parseInt(yearStr, 10)) : parseInt(yearStr, 10);
            const monthStr = indexes.M ? matches[indexes.M] : "";
            const month = indexes.name ? this.#monthNameMap.get(monthStr.toLowerCase()) : (parseInt(monthStr, 10) - 1);
            const dayStr = indexes.D ? matches[indexes.D] : "";
            const day = dayStr ? parseInt(dayStr, 10) : 1;

            // validate month and day indexes
            if (!is.number(month) || (day > getDaysInMonth(year, month))) { continue; }

            // reject trailing garbage if specified in the passed options
            const remaining = text.slice(matches[0].length);
            if (remaining && options?.complete) { return; }

            // create the Date object
            const date = new Date(Date.UTC(year, month, day));

            // create and return the result object
            return { text: matches[0], remaining, date, hasYear: !!yearStr, hasDay: !!dayStr };
        }

        // no matching regular expression found
        return undefined;
    }
}
