/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { re, map } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * Common optional parameters for parsing values from strings.
 */
export interface ParseValueBaseOptions {

    /**
     * If specified, a custom decimal separator character. By default, the
     * decimal separator of the current UI language will be used.
     */
    decSep?: string;

    /**
     * If set to `true`, the entire string must represent a valid value. No
     * remaining "garbage" text will be accepted. Default value is `false`.
     */
    complete?: boolean;
}

/**
 * Common properties for result descriptors for values parsed from a string.
 */
export interface ParseValueBaseResult {

    /**
     * The original text portion from the parsed string that has been parsed to
     * the resulting value.
     */
    text: string;

    /**
     * The remaining characters from the parsed string following the resulting
     * value.
     */
    remaining: string;
}

/**
 * A configuration entry for `createSliceMatcher`.
 */
export interface SliceMatcherEntry<T> {

    /**
     * The complete text to be matched.
     */
    match: string;

    /**
     * A custom value associated to the text to be matched. Will be inserted
     * into the reverse map returned by `createSliceMatcher`.
     */
    value: T;
}

export type SliceMatcher<T> = [regExpPattern: string, reverseMap: Map<string, T>];

// functions ==================================================================

/**
 * Returns the capturing group pattern for a regular expression that matches
 * any of the strings in the passed array.
 *
 * @param list
 *  The array with strings to be matched by a regular expression.
 *
 * @returns
 *  The capturing group pattern for a regular expression that matches any of
 *  the strings in the passed array. The strings will be sorted by length
 *  (descending) in order to create a greedy regular expression.
 *
 *  _Example:_ Given the strings "a", "b", and "ab", the RE pattern `(ab|a|b)`
 *  will be created.
 */
export function createListPattern(list: readonly string[]): string {
    return re.capture(...list.slice().sort((n1, n2) => n2.length - n1.length).map(re.escape));
}

/**
 * Creates a regular expression pattern and a value reverse map for matching
 * leading slices of a predefined list of string tokens.
 *
 * Example: A regular expression pattern created for the full English month
 * names (with a reverse map containing the one-based month indexes) will match
 * the month February in the input string "febr-15". The reverse map will
 * translate all matched slices of "February" ("febr", "FEB", etc.) to the
 * value 2.
 *
 * @param entries
 *  All match entries to be recognized by the regular expression pattern.
 *
 * @param minLen
 *  The minimum length of the entry texts to be matched. Example with month
 *  names: If set to "3", the pattern will be able to recognize the month
 *  February from the input strings "feb-15", "febr-15", "febru-15" and so on,
 *  but will not match "fe-15".
 */
export function createSliceMatcher<T>(entries: ReadonlyArray<SliceMatcherEntry<T>>, minLen: number): SliceMatcher<T> {

    // the reverse map (maps all lower-case slices to the entry values)
    const revMap = new Map<string, T>();

    // collect all slices of all entry match strings
    const slices: string[] = [];
    for (const entry of entries) {
        const match = entry.match.toLowerCase();
        const len = match.length;
        for (let idx = Math.min(minLen, len); idx <= len; idx += 1) {
            map.upsert(revMap, match.slice(0, idx), slice => {
                slices.push(slice);
                return entry.value;
            });
        }
    }

    return [createListPattern(slices), revMap];
}
