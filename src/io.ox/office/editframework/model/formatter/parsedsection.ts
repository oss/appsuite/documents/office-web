/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ColorKey, LocaleId, LocaleData } from "@/io.ox/office/tk/locale";
import type { ParsedToken, SectionOperator } from "@/io.ox/office/editframework/model/formatter/parsedtoken";

// types ======================================================================

/**
 * The category of a format code or format code section.
 */
export enum FormatCategory {

    /**
     * All format codes with a syntax error.
     */
    ERROR = "error",

    /**
     * Reserved for the standard format code (maybe with additional tokens like
     * string literals, interval, or color).
     */
    STANDARD = "standard",

    /**
     * All numeric (decimal) format codes without special appearance.
     */
    NUMBER = "number",

    /**
     * All numeric format codes with scientific notation.
     */
    SCIENTIFIC = "scientific",

    /**
     * All numeric format codes shown as percentage.
     */
    PERCENT = "percent",

    /**
     * All numeric format codes shown as fractions.
     */
    FRACTION = "fraction",

    /**
     * All numeric format codes with a currency symbol, except accounting
     * formats.
     */
    CURRENCY = "currency",

    /**
     * All numeric format codes with a currency symbol, and left-aligned minus
     * sign.
     */
    ACCOUNTING = "accounting",

    /**
     * All format codes shown as a date (without time).
     */
    DATE = "date",

    /**
     * All format codes shown as a time (without date).
     */
    TIME = "time",

    /**
     * All format codes shown as combined date and time.
     */
    DATETIME = "datetime",

    /**
     * All format codes containing a text section only.
     */
    TEXT = "text",

    /**
     * All format codes not fitting into any other category.
     */
    CUSTOM = "custom"
}

/**
 * Optional parameters for comparing number format categories.
 */
export interface CompareCategoryOptions {

    /**
     * If set to `true`, the categories `DATE`, `TIME`, and `DATETIME` are
     * considered to be equal. Default value is `false` (exact comparison).
     */
    anyDateTime?: boolean;
}

/**
 * Special private-use locale codes.
 */
export enum XLocaleCode {

    /** System (UI) long date format. */
    SYSDATE = "x-sysdate",

    /** System (UI) time format. */
    SYSTIME = "x-systime",

    /** Euro currency with trailing symbol. */
    EURO1 = "x-euro1",

    /** Euro currency with leading symbol. */
    EURO2 = "x-euro2",

    /** Bitcoin currency with trailing symbol. */
    XBT1 = "x-xbt1",

    /** Bitcoin currency with leading symbol. */
    XBT2 = "x-xbt2"
}

/**
 * Information about the locale data to be used for a code section.
 */
export interface SectionLocaleInfo {

    /**
     * The lowercase dashed locale code of this section, e.g. "de-at". May also
     * be a private-use identifier from the enumeration `XLocaleCode`, e.g.
     * "x-sysdate" for specifying the long system date format of the current
     * locale.
     */
    readonly lc: string;

    /**
     * The calendar type identifier of this section.
     */
    readonly calType: number;

    /**
     * The numeral system identifier (from LCID/locale) of this section.
     */
    readonly numSysId: number;

    /**
     * The parsed locale code if available.
     */
    readonly localeId?: LocaleId;

    /**
     * The resolved locale data object if available.
     */
    readonly lcData?: LocaleData;
}

/**
 * Information about the text color to be used for a code section.
 */
export interface SectionColorInfo {

    /**
     * The key of the text color to be used to format a value.
     */
    readonly key: ColorKey;

    /**
     * The CSS value of the text color to be used to format a value.
     */
    readonly css: string;
}

/**
 * Information about the interval range to be used for a code section.
 */
export interface SectionRangeInfo {

    /**
     * The operator defining the interval covered by a code section.
     */
    readonly op: SectionOperator;

    /**
     * The argument value defining the interval covered by a code section.
     */
    readonly arg: number;
}

/**
 * Enumerates all digit pattern codes. Used to specify how to handle unmatched
 * pattern digits and literal text while formatting a value.
 */
export enum DigitCode {

    /**
     * Add a zero character for an unmatched digit pattern code, literal text,
     * whitespace for blind text, and the specified fill character to the
     * formatted result.
     */
    ZERO = "0",

    /**
     * Ignore unmatched digit pattern codes, literal text, blind text, and fill
     * characters completely.
     */
    HIDE = "#",

    /**
     * Add whitespace for unmatched digit pattern codes, literal text, blind
     * text, and fill characters.
     */
    BLIND = "?"
}

/**
 * Information about formatting numbers by a code section.
 */
export interface SectionNumbersInfo {

    /**
     * Number of integer digits to be formatted (needed for rounding).
     */
    readonly intDigits: number;

    /**
     * Number of integer digits that will always be visible (via "0" tokens).
     */
    readonly minIntDigits: number;

    /**
     * Number of fractional digits to be formatted (needed for rounding).
     */
    readonly fracDigits: number;

    /**
     * Number of fraction digits that will always be visible (via "0" tokens).
     */
    readonly minFracDigits: number;

    /**
     * Whether to insert group separators into the integer part of numbers.
     */
    readonly groupInt: boolean;

    /**
     * Number of trailing group separators used to reduce the number to be
     * formatted (e.g., if numbers will be grouped in thousands, a value of `2`
     * reduces a number by 1 million, i.e. 2 groups of 1000 each).
     */
    readonly groupBlocks: number;

    /**
     * Number of percent signs occuring in the format code. Each percent sign
     * causes to multiply the formatted number by 100.
     */
    readonly pctSigns: number;
}

/**
 * Information about a code section for scientific number formats.
 */
export interface SectionScientificInfo {

    /**
     * Whether to show a leading plus sign for non-negative exponents.
     */
    readonly plusSign: boolean;
}

/**
 * Information about a code section for fraction number formats.
 */
export interface SectionFractionInfo {

    /**
     * Whether the denominator of the fraction is a fixed number (e.g. in the
     * format code "??/10").
     */
    readonly fixedDenom: boolean;

    /**
     * Denominator of fraction formats. Either a fixed denominator (e.g. `4`
     * for the format code "#/4"), or the length of a digit pattern for dynamic
     * denominators (e.g. `2` for the format code "#/##").
     */
    readonly denomSize: number;

    /**
     * Specifies whether to format all numbers as short fractions (no separate
     * integer part), e.g. "7/2" for the number 3.5 instead of "3 1/2".
     */
    readonly shortFrac: boolean;

    /**
     * Specifies the type of the leading integer pattern.
     */
    readonly intMode: DigitCode;

    /**
     * Specifies the type of the numerator pattern.
     */
    readonly numerMode: DigitCode;

    /**
     * Specifies the type of the denominator pattern.
     */
    readonly denomMode: DigitCode;
}

/**
 * Information about a code section for date/time formats.
 */
export interface SectionDateTimeInfo {

    /**
     * Whether to use 12-hours mode with AM/PM tokens when formatting times.
     */
    readonly hours12: boolean;

    /**
     * The precision (number of digits) to round seconds to.
     */
    readonly precision: number;
}

/**
 * Information about a code section for currency or accounting formats.
 */
export interface SectionCurrencyInfo {

    /**
     * The currency symbol parsed from the format code.
     */
    readonly symbol: string;

    /**
     * Whether the currency symbol contains blind markers.
     */
    readonly blind: boolean;
}

/**
 * A comparator predicate function returning whether a number is covered by the
 * interval of a code section, according to the operator and argument value.
 */
export type SectionMatchFn = (num: number) => boolean;

/**
 * Represents the parsed information of a single section in a format code. Code
 * sections will be separated with semicolons in the format code, and can be
 * used to define different number formats for specific intervals of numbers,
 * e.g. for positive and negative numbers.
 *
 * _Example:_ The format code "0.00;[Red]0.00" contains the two code sections
 * "0.00" for non-negative numbers, and "[Red]0.00" for negative numbers.
 */
export interface ParsedSection {

    /**
     * An array with the parsed tokens of this section.
     */
    readonly tokens: readonly ParsedToken[];

    /**
     * The format category of this section.
     */
    readonly category: FormatCategory;

    /**
     * A comparator predicate function returning whether the passed number is
     * covered by the interval of this code section, according to the operator
     * and argument value.
     */
    readonly matchFn: SectionMatchFn;

    /**
     * Whether this code section will cause to automatically add a leading
     * minus sign when formatting negative numbers. Happens if the interval of
     * the code section covers at least one negative number, AND one
     * non-negative number (zero and/or positive numbers).
     */
    readonly autoMinus: boolean;

    /**
     * Specifies whether this code section contains a fill literal token.
     */
    readonly fillToken: boolean;

    /**
     * Data about the locale to be used for a code section.
     */
    readonly locale?: SectionLocaleInfo;

    /**
     * Data about the text color to be used to format a value. If omitted, the
     * section will not change the original text color of the value.
     */
    readonly color?: SectionColorInfo;

    /**
     * Data about the interval range to be used for a code section. If omitted,
     * the section will be used for all (remaining) numbers.
     */
    readonly range?: SectionRangeInfo;

    /**
     * Data about formatting numbers (digits, grouping). Omitted for date/time
     * format codes.
     */
    readonly numbers?: SectionNumbersInfo;

    /**
     * Data about scientific notation. If omitted, numbers will not be shown in
     * scientific notation.
     */
    readonly scientific?: SectionScientificInfo;

    /**
     * Data about fraction notation. If omitted, numbers will not be shown in
     * fraction notation.
     */
    readonly fraction?: SectionFractionInfo;

    /**
     * Data about date/time formats. If omitted, numbers will not be shown as
     * date/time.
     */
    readonly dateTime?: SectionDateTimeInfo;

    /**
     * The currency symbol found in the format code.
     */
    readonly currency?: SectionCurrencyInfo;

    /**
     * The CJK numeral system identifier (from a DBNum token) of this section.
     */
    readonly cjkNumId?: number;

    /**
     * Specifies if this is an artificial helper section that was not part of
     * the original format code.
     */
    readonly artificial?: boolean;
}

// public functions ===========================================================

/**
 * Returns whether the passed category is `CURRENCY` or `ACCOUNTING`.
 *
 * @returns
 *  Whether the passed category is `CURRENCY` or `ACCOUNTING`.
 */
export function isAnyCurrency(category: FormatCategory): boolean {
    return (category === FormatCategory.CURRENCY) || (category === FormatCategory.ACCOUNTING);
}

/**
 * Returns whether the passed category is `DATE` or `DATETIME`.
 *
 * @returns
 *  Whether the passed category is `DATE` or `DATETIME`.
 */
export function isAnyDate(category: FormatCategory): boolean {
    return (category === FormatCategory.DATE) || (category === FormatCategory.DATETIME);
}

/**
 * Returns whether the passed category is `TIME` or `DATETIME`.
 *
 * @returns
 *  Whether the passed category is `TIME` or `DATETIME`.
 */
export function isAnyTime(category: FormatCategory): boolean {
    return (category === FormatCategory.TIME) || (category === FormatCategory.DATETIME);
}

/**
 * Returns whether the passed category is `DATE`, `TIME`, or `DATETIME`.
 *
 * @returns
 *  Whether the passed category is `DATE`, `TIME`, or `DATETIME`.
 */
export function isAnyDateTime(category: FormatCategory): boolean {
    return (category === FormatCategory.DATE) || (category === FormatCategory.TIME) || (category === FormatCategory.DATETIME);
}

/**
 * Returns whether the passed categories are considered to be equal.
 *
 * @param category1
 *  The first category to be compared to the second category.
 *
 * @param category2
 *  The second category to be compared to the first category.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 * Whether the passed categories are considered to be equal.
 */
export function isEqualCategory(category1: FormatCategory, category2: FormatCategory, options?: CompareCategoryOptions): boolean {

    // immediately return for exact category matches
    if (category1 === category2) { return true; }

    // ignore differences in date/time categories if specified
    return !!options?.anyDateTime && isAnyDateTime(category1) && isAnyDateTime(category2);
}
