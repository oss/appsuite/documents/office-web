/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str, fun, map, dict } from "@/io.ox/office/tk/algorithms";
import type { LocaleId, LocaleSpec, CurrencyData } from "@/io.ox/office/tk/locale";
import { localeLogger, LOCALE_DATA, parseLocaleId, localeDataRegistry } from "@/io.ox/office/tk/locale";

// types ======================================================================

/**
 * The pattern type of a currency format code.
 * - `simple`: Simple currency code with leading minus sign.
 * - `currency`: Currency code with customized negative code section.
 * - `accounting`: Accounting code with customized negative code section.
 */
export type CurrencyCodePatternType = "simple" | "currency" | "accounting";

/**
 * Type of the negative code section of a currency/accounting code.
 * - `leading`: Minus sign in front (e.g. "-$1.00", "-1.00€").
 * - `between`: Minus sign between symbol/value (e.g. "$-1.00", "1.00-€").
 * - `trailing`: Minus sign at the end (e.g. "$1.00-", "1.00€-").
 * - `parentheses`: Enclosed in parentheses (e.g. "$(1.00)", "(1.00)€").
 */
export type CurrencyCodeNegativeMode = "leading" | "between" | "trailing" | "parentheses";

/**
 * Configuration options to generate currency or accounting format codes.
 */
export interface CurrencyCodeConfig {

    /**
     * The identifier of a locale, or a resolved `LocaleData` instance, to be
     * used to resolve the currency symbol, format pattern, and all other
     * settings. By default, the current UI locale will be used.
     */
    locale?: LocaleSpec;

    /**
     * The pattern type of the currency format code to be created. Default
     * value is "simple".
     */
    patternType?: CurrencyCodePatternType;

    /**
     * Specific settings of the currency used by the resolved locale (option
     * "locale") to be overridden.
     */
    currencyData?: Partial<CurrencyData>;

    /**
     * Whether to use the ISO currency code (`true`), or the local currency
     * code (`false`) of the specified locale. Ignored if a literal currency
     * symbol has been specified with the option  "currencySymbol". Default
     * value is `false`.
     */
    isoCurrency?: boolean;

    /**
     * Specifies how to modify the resolved currency symbol.
     * - "blind": Converts the currency symbol to a blind currency symbol (i.e.
     *   whitespace that is exactly as wide as the currency symbol), e.g.
     *   `0.00 _E_U_R` instead of `0.00 EUR`.
     * - "locale": Inserts the currency symbol and the locale code into a
     *   locale token in brackets, e.g. `0.00 [$€-de-DE]` instead of `0.00 €`.
     *
     * By default, the unmodified currency symbol will be used.
     */
    symbolModifier?: "blind" | "locale";

    /**
     * Position of the currency symbol.
     * - `false`: Currency symbol in front of the number (e.g. "$1.00").
     * - `true`: Currency symbol following the number (e.g. "1.00€").
     *
     * By default, the symbol position of the resolved locale (option "locale")
     * will be used.
     */
    trailingSymbol?: boolean;

    /**
     * Whether to add a whitespace character between value and currency symbol.
     * By default, the setting of the resolved locale (option "locale") will be
     * used.
     */
    spaceSeparator?: boolean;

    /**
     * Type of the negative code section. By default, the setting of the
     * resolved locale (option "locale") will be used.
     */
    negativeMode?: CurrencyCodeNegativeMode;

    /**
     * Whether to insert the color modifier token "[Red]" for negative numbers.
     * Default value is `false`.
     */
    negativeRed?: boolean;

    /**
     * The number of integer digits to be generated. Default value is `1`.
     */
    intDigits?: number;

    /**
     * The number of fractional digits to be generated. By default, the number
     * of decimal digits of the resolved locale's native currency will be used
     * (option "locale").
     */
    fracDigits?: number;

    /**
     * Whether to insert a group separator (a.k.a. thousands separator) into
     * the integer part. Default value is `false`.
     */
    groupSep?: boolean;

    /**
     * The code snippet for a number system modifier to be inserted into the
     * format code right before the number. Default value is the empty string.
     */
    digitModifier?: string;
}

// constants ==================================================================

type CurrencyCodePatternsMap = RoRecord<CurrencyCodeNegativeMode, RoRecord<CurrencyCodePatternType, readonly string[]>>;

const SIMPLE_CURRENCY_PATTERNS_LEADING = ["{MODIFIER}{SYMBOL}{NUMBER}", "{RED}{MODIFIER}-{SYMBOL}{NUMBER}"];
const SIMPLE_CURRENCY_PATTERNS_TRAILING = ["{MODIFIER}{NUMBER}{SYMBOL}", "{RED}{MODIFIER}-{NUMBER}{SYMBOL}"];

// currency format code patterns with leading currency symbol
const CURRENCY_PATTERNS_LEADING: CurrencyCodePatternsMap = {
    // minus-symbol-number variant, e.g.: -$42.00
    leading: {
        simple:     SIMPLE_CURRENCY_PATTERNS_LEADING,
        currency:   ["{MODIFIER}{SYMBOL}{NUMBER}",       "{RED}{MODIFIER}-{SYMBOL}{NUMBER}"],
        accounting: ["{MODIFIER}_-{SYMBOL}* {NUMBER}_-", "{RED}{MODIFIER}-{SYMBOL}* {NUMBER}_-",   "{MODIFIER}_-{SYMBOL}* {ZERO}_-", "_-@_-"],
    },
    // symbol-minus-number variant, e.g.: $-42.00
    between: {
        simple:     SIMPLE_CURRENCY_PATTERNS_LEADING,
        currency:   ["{MODIFIER}{SYMBOL}{NUMBER}",       "{RED}{MODIFIER}{SYMBOL}-{NUMBER}"],
        accounting: ["{MODIFIER}_ {SYMBOL}* {NUMBER}_ ", "{RED}{MODIFIER}_ {SYMBOL}* -{NUMBER}_ ", "{MODIFIER}_ {SYMBOL}* {ZERO}_ ", "_ @_ "],
    },
    // symbol-number-minus variant, e.g.: $42.00-
    trailing: {
        simple:     SIMPLE_CURRENCY_PATTERNS_LEADING,
        currency:   ["{MODIFIER}{SYMBOL}{NUMBER}_-",     "{RED}{MODIFIER}{SYMBOL}{NUMBER}-"],
        accounting: ["{MODIFIER}_-{SYMBOL}* {NUMBER}_-", "{RED}{MODIFIER}_-{SYMBOL}* {NUMBER}-",   "{MODIFIER}_-{SYMBOL}* {ZERO}_-", "_-@_-"],
    },
    // symbol-number-with-parentheses variant, e.g.: ($42.00)
    parentheses: {
        simple:     SIMPLE_CURRENCY_PATTERNS_LEADING,
        currency:   ["{MODIFIER}{SYMBOL}{NUMBER}_)",     "{RED}({MODIFIER}{SYMBOL}{NUMBER})"],
        accounting: ["_({MODIFIER}{SYMBOL}* {NUMBER}_)", "{RED}_({MODIFIER}{SYMBOL}* ({NUMBER})",  "_({MODIFIER}{SYMBOL}* {ZERO}_)", "_(@_)"],
    },
};

// currency format code patterns with trailing currency symbol
const CURRENCY_PATTERNS_TRAILING: CurrencyCodePatternsMap = {
    // minus-number-symbol variant, e.g.: -42.00$
    leading: {
        simple:     SIMPLE_CURRENCY_PATTERNS_TRAILING,
        currency:   ["{MODIFIER}{NUMBER}{SYMBOL}",         "{RED}{MODIFIER}-{NUMBER}{SYMBOL}"],
        accounting: ["{MODIFIER}_-* {NUMBER}{SYMBOL}_-",   "{RED}{MODIFIER}-* {NUMBER}{SYMBOL}_-",    "{MODIFIER}_-* {ZERO}{SYMBOL}_-",   "_-@_-"],
    },
    // number-minus-symbol variant, e.g.: 42.00-$
    between: {
        simple:     SIMPLE_CURRENCY_PATTERNS_TRAILING,
        currency:   ["{MODIFIER}{NUMBER}_-{SYMBOL}",       "{RED}{MODIFIER}{NUMBER}-{SYMBOL}"],
        accounting: ["{MODIFIER}_ * {NUMBER}_-{SYMBOL}_ ", "{RED}{MODIFIER}_ * {NUMBER}-{SYMBOL}_ ",  "{MODIFIER}_ * {ZERO}_-{SYMBOL}_ ", "_ @_ "],
    },
    // number-symbol-minus variant, e.g.: 42.00$-
    trailing: {
        simple:     SIMPLE_CURRENCY_PATTERNS_TRAILING,
        currency:   ["{MODIFIER}{NUMBER}{SYMBOL}_-",       "{RED}{MODIFIER}{NUMBER}{SYMBOL}-"],
        accounting: ["{MODIFIER}_-* {NUMBER}{SYMBOL}_-",   "{RED}{MODIFIER}_-* {NUMBER}{SYMBOL}-",    "{MODIFIER}_-* {ZERO}{SYMBOL}_-",   "_-@_-"],
    },
    // number-symbol-with-parentheses variant, e.g.: (42.00$)
    parentheses: {
        simple:     SIMPLE_CURRENCY_PATTERNS_TRAILING,
        currency:   ["{MODIFIER}{NUMBER}{SYMBOL}_)",       "{RED}({MODIFIER}{NUMBER}{SYMBOL})"],
        accounting: ["_ * {MODIFIER}{NUMBER}_){SYMBOL}_ ", "{RED}_ * ({MODIFIER}{NUMBER}){SYMBOL}_ ", "_ * {MODIFIER}{ZERO}_){SYMBOL}_ ", "_ @_ "],
    },
};

/**
 * Locale codes or region codes with a currency symbol following the number.
 */
const localesWithTrailingSymbol = new Set(str.splitTokens(
    "AD AE AL AM AO AX AZ BA BD BF BG BH BI BJ BL BY CD CG CI CM CV CY CZ DE DK DZ EE EG ES FI FO FR " +
    "GA GE GF GN GP GR GW HR HT HU IL IQ IS IT JO KG KH KM KW KZ LB LT LU LV LY MA MC MD ME MF MK ML MM MQ MR MV MZ " +
    "NC NE OM PF PL PM PT PY QA RE RO RS RU SA SD SE SI SK SM SN ST SY TF TG TJ TL TM TN UA UZ VA VN WF YE YT " +
    "fr-CA"
));

/**
 * Locale codes or region codes that use a space character between number and
 * currency symbol.
 */
const localesWithSpaceSeparator = new Set(str.splitTokens(
    "AD AE AF AL AM AO AR AT AW AX AZ BA BE BF BG BH BI BJ BL BN BO BQ BR BT BW BY CD CG CI CM CO CV CW CY CZ " +
    "DE DJ DK DM DZ EE EG ER ES ET FI FO FR GA GE GF GM GN GP GQ GR GS GT GW HN HR HT HU IQ IR IS IT JO " +
    "KE KG KM KP KW KZ LB LI LK LS LT LU LV LY MA MC MD ME MF MG MK ML MM MN MQ MR MU MV MW MZ NC NE NL NO NP OM " +
    "PA PE PF PG PK PL PM PS PT PY QA RE RO RS RU RW SA SC SD SE SG SI SJ SK SL SM SN SR ST SX SY " +
    "TD TF TG TJ TL TM TN TO TZ UA UG UY UZ VA VN VU WF YE YT ZA ZM ZW " +
    "fr-CA"
));

/**
 * Pattern modes for negative code sections, mapped by language code, region
 * code, or locale code.
 */
const localeNegativeModeMap = map.yieldFrom(function *() {

    const MAPPING: RoRecord<CurrencyCodeNegativeMode, string> = {
        leading:
            "AD AE AF AG AI AL AM AO AR AS AT AU AX AZ BA BB BD BE BF BG BH BI BJ BL BM BS BT BW BY " +
            "CA CB CC CD CG CI CK CL CM CX CU CV CY CZ DE DJ DK DM DZ EE EG ES ET FI FJ FK FM FO FP FR " +
            "GA GB GD GE GF GG GH GI GM GN GP GQ GR GS GU GW GY HM HN HR HT HU IE IM IO IQ IS IT JE JM JO JP " +
            "KE KG KH KI KM KN KP KR KW KY KZ LB LC LI LK LR LS LT LU LV LY " +
            "MA MC MD ME MF MG MH MK ML MM MN MP MQ MR MS MT MU MW MX MZ NA NC NE NF NG NO NP NR NU NZ OM " +
            "PE PF PG PL PM PN PS PT PW QA RE RO RS RU RW SA SB SC SD SE SH SI SJ SK SL SM SN SO SS ST SY SZ " +
            "TC TD TF TG TH TJ TK TL TM TN TO TR TV TW UA UG UM UZ VA VI VC VG VN VU WF WS YE YT ZA ZM",
        between:
            "AW BQ CF CH CN CW ER GL IL IN LA SR SX VE",
        trailing:
            "IR MV NL PK SY",
        parentheses:
            "BN BO BR BZ CO CR DO EC GT HK ID MO MY NI PA PH PR PY SG SV TT TZ US UY ZW fr-CA",
    };

    for (const [negativeMode, locales] of dict.entries(MAPPING)) {
        for (const locale of str.splitTokens(locales)) {
            yield [locale, negativeMode];
        }
    }
});

// functions ==================================================================

/**
 * Returns whether a set of locale codes and region codes contains a locale.
 */
function hasLocaleFlag(localeFlags: Set<string>, localeId: LocaleId): boolean {
    return localeFlags.has(localeId.intl) || (!!localeId.region && localeFlags.has(localeId.region));
}

/**
 * Generates a currency or accounting format code.
 *
 * @param config
 *  The configuration options for the currency format code.
 *
 * @returns
 *  The resulting native format code to be used in document operations.
 */
export function generateCurrencyCode(config?: CurrencyCodeConfig): string {

    // resolve locale identifier
    const localeId = (config?.locale ? parseLocaleId(config.locale) : undefined) ?? LOCALE_DATA;

    // resolve effective currency settings for the locale
    let currData = localeDataRegistry.getCurrencyData(localeId) ?? LOCALE_DATA.currency;
    if (config?.currencyData) { currData = { ...currData, ...config.currencyData }; }

    // resolve currency symbol to be used
    let currencySymbol = config?.isoCurrency ? currData.iso : currData.symbol;
    const trailingSymbol = config?.trailingSymbol ?? hasLocaleFlag(localesWithTrailingSymbol, localeId);

    // transform the currency symbol according to configuration
    switch (config?.symbolModifier) {
        case "blind":
            // blind currency symbol: precede all characters with underscores, e.g. "EUR" => "_E_U_R";
            // but do not insert a blind currency symbol at all, if it is located before the number
            currencySymbol = trailingSymbol ? currencySymbol.replace(/./g, "_$&") : "";
            break;
        case "locale":
            currencySymbol = `[$${currencySymbol}-${localeId.intl}]`;
            break;
        default:
            // always convert currency symbol to literal text
            currencySymbol = `"${currencySymbol}"`;
            break;
    }

    // leading or trailing space next of currency symbol
    if (config?.spaceSeparator ?? hasLocaleFlag(localesWithSpaceSeparator, localeId)) {
        currencySymbol = trailingSymbol ? (" " + currencySymbol) : (currencySymbol + " ");
    }

    // resolve pattern for negative code section for locale (prefer region over language)
    const negativeMode = config?.negativeMode ?? fun.do(() => {
        const lcMode = localeNegativeModeMap.get(localeId.intl);
        if (lcMode) { return lcMode; }
        const regionMode = localeId.region && localeNegativeModeMap.get(localeId.region);
        if (regionMode) { return regionMode; }
        const langMode = localeNegativeModeMap.get(localeId.language);
        if (langMode) { return langMode; }
        localeLogger.warn(`$badge{generateCurrencyCode} missing negativeMode for locale "${localeId.intl}"`);
        localeNegativeModeMap.set(localeId.intl, "leading");
        return "leading";
    });

    // assemble the format code portion for the integer part
    const intDigits = config?.intDigits ?? 1;
    const groupSep = config?.groupSep ?? false;
    const allDigits = Math.max(intDigits, groupSep ? 4 : 1);
    let intCode = "";
    for (let digit = 0; digit < allDigits; digit += 1) {
        if (groupSep && digit && !(digit % 3)) { intCode = "," + intCode; }
        intCode = ((digit < intDigits) ? "0" : "#") + intCode;
    }

    // assemble the format code portion for the fractional part
    const fracDigits = config?.fracDigits ?? currData.precision;
    const fracCode = fracDigits ? ("." + "0".repeat(fracDigits)) : "";
    const zeroCode = '"-"' + "?".repeat(fracDigits);

    // get the code patterns to be used to generate the format code
    const codePatternMap = (trailingSymbol ? CURRENCY_PATTERNS_TRAILING : CURRENCY_PATTERNS_LEADING)[negativeMode];
    const patternType = config?.patternType ?? "simple";
    const codePatterns = codePatternMap[patternType];

    // generate the format sections by replacing all placeholder tokens in the format pattern
    const codeSections = codePatterns.map(codePattern => codePattern
        .replace(/{RED}/g, config?.negativeRed ? "[Red]" : "")
        .replace(/{MODIFIER}/g, config?.digitModifier ?? "")
        .replace(/{SYMBOL}/g, () => currencySymbol) // MUST be function to prevent special behavior of "$" signs
        .replace(/{NUMBER}/g, intCode + fracCode)
        .replace(/{ZERO}/g, zeroCode));

    // remove negative sections, if it is equal to the positive sections (plus leading minus sign)
    if ((patternType === "simple") && (codeSections.length === 2) && ("-" + codeSections[0] === codeSections[1])) {
        codeSections.length = 1;
    }

    // join all code sections
    return codeSections.join(";");
}

/**
 * Yields the set of fixed currencies to be supported and offered in the
 * user interface. Always includes the currency of the current UI locale,
 * and a selected set of other currencies.
 *
 * @yields
 *  The fixed currencies to be supported.
 */
export function *yieldFixedCurrencyData(): KeyedIterator<Readonly<CurrencyData>> {

    // always start with currency of current UI locale
    const uiCurrData = LOCALE_DATA.currency;
    yield [LOCALE_DATA.intl, uiCurrData];

    // yield fixed currencies, but do not duplicate UI currency
    for (const locale of ["en-US", "de-DE", "en-GB"]) {
        const currData = localeDataRegistry.getCurrencyData(locale);
        if (currData && (uiCurrData.symbol !== currData.symbol)) {
            yield [locale, currData];
        }
    }
}
