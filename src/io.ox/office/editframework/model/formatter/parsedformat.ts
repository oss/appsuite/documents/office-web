/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

import {
    FormatCategory, type CompareCategoryOptions, type ParsedSection,
    isAnyDate, isAnyTime, isAnyDateTime, isEqualCategory
} from "@/io.ox/office/editframework/model/formatter/parsedsection";

// class ParsedFormat =========================================================

/**
 * Represents a parsed number format code.
 */
export class ParsedFormat {

    // properties -------------------------------------------------------------

    /**
     * The original format code this instance has been created from.
     */
    readonly formatCode: string;

    /**
     * The identifier of the category this format code is associated to.
     */
    readonly category: FormatCategory;

    /**
     * An array of format code sections for different number intervals.
     */
    readonly numberSections: ReadonlyArray<Readonly<ParsedSection>>;

    /**
     * A dedicated code section to format string values.
     */
    readonly textSection: Readonly<ParsedSection>;

    // constructor ------------------------------------------------------------

    constructor(
        formatCode: string,
        category: FormatCategory,
        numberSections: ParsedSection[],
        textSection: ParsedSection
    ) {
        this.formatCode = formatCode;
        this.category = category;
        this.numberSections = numberSections;
        this.textSection = textSection;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns an existing code section, according to the passed value.
     *
     * @param value
     *  The value used to pick a code section. For finite numbers, one of the
     *  numeric sections will be returned. For strings, the text section will
     *  be returned. For all other values, `undefined` will be returned.
     *
     * @returns
     *  The format code section for the passed value, if available; otherwise
     *  `undefined`.
     */
    getSection(value: number | string): Opt<Readonly<ParsedSection>> {

        // do not return sections, if the format code contains a syntax error
        if (this.isError()) { return undefined; }

        // return text section for strings
        if (is.string(value)) { return this.textSection; }

        // pick first matching section for numbers
        return this.numberSections.find(section => section.matchFn(value));
    }

    /**
     * Returns whether the format code of this parsed format contains a syntax
     * error.
     *
     * @returns
     *  Whether the format code of this parsed format contains a syntax error.
     */
    isError(): boolean {
        return this.category === FormatCategory.ERROR;
    }

    /**
     * Returns whether this parsed format is the standard format code (category
     * is set to `STANDARD`).
     *
     * @returns
     *  Whether this parsed format is the standard format code.
     */
    isStandard(): boolean {
        return this.category === FormatCategory.STANDARD;
    }

    /**
     * Returns whether this parsed format is in one of the categories `DATE`,
     * or `DATETIME`.
     *
     * @returns
     *  Whether this parsed format is in a date category.
     */
    isAnyDate(): boolean {
        return isAnyDate(this.category);
    }

    /**
     * Returns whether this parsed format is in one of the categories `TIME`,
     * or `DATETIME`.
     *
     * @returns
     *  Whether this parsed format is in a time category.
     */
    isAnyTime(): boolean {
        return isAnyTime(this.category);
    }

    /**
     * Returns whether this parsed format is in one of the categories `DATE`,
     * `TIME`, or `DATETIME`.
     *
     * @returns
     *  Whether this parsed format is in one of the date/time categories.
     */
    isAnyDateTime(): boolean {
        return isAnyDateTime(this.category);
    }

    /**
     * Returns whether this parsed format is for text only (category is set to
     * `TEXT`).
     *
     * @returns
     *  Whether this parsed format is a text format code.
     */
    isText(): boolean {
        return this.category === FormatCategory.TEXT;
    }

    /**
     * Returns whether this parsed format has the same category as the passed
     * parsed format.
     *
     * @param parsedFormat
     *  The parsed format whose category will be compared with the category of
     *  this instance.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  Whether this and the other parsed format have the same category.
     */
    hasEqualCategory(parsedFormat: ParsedFormat, options?: CompareCategoryOptions): boolean {
        return isEqualCategory(this.category, parsedFormat.category, options);
    }
}
