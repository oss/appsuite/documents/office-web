/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, ary } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import type { LocaleData } from "@/io.ox/office/tk/locale";
import { type DObjectConfig, DObject } from "@/io.ox/office/tk/objects";

import { OP_STD_TOKEN } from "@/io.ox/office/editframework/model/formatter/codeparser";
import { type CurrencyCodeConfig, generateCurrencyCode } from "@/io.ox/office/editframework/model/formatter/currencycodes";

// types ======================================================================

/**
 * The identifiers of important predefined format codes, as enum-like record.
 */
export const PresetCodeId = {

    /** Standard format with automatic decimal/scientific display mode. */
    STANDARD: 0,

    /** Short decimal notation (no fraction). */
    NUMBER_INT: 1,

    /** Default format for decimal notation. */
    NUMBER: 2,

    /** Short percentage notation (no fraction). */
    PERCENT_INT: 9,

    /** Default format for percentage notation. */
    PERCENT: 10,

    /** Default format for scientific notation. */
    SCIENTIFIC: 11,

    /** Default format for fraction notation (one-digit denominator). */
    FRACTION: 12,

    /** Long format for fraction notation (two-digit denominator). */
    FRACTION_LONG: 13,

    /** Default system date format. */
    SYS_DATE: 14,

    /** Fixed date format with short month name in D-M-Y order. */
    FIXED_DATE: 15,

    /** Fixed date format with day and month, but without year. */
    DAY_MONTH: 16,

    /** Fixed date format with month and year, but without day. */
    MONTH_YEAR: 17,

    /** Default short date/time format. */
    DATETIME_SHORT: 22,

    /** Short date format for Japanese Gengo calendar. */
    GENGO_DATE_SHORT: 27,

    /** Long date format for Japanese Gengo calendar. */
    GENGO_DATE_LONG: 28,

    /** Traditional date format for CJK locales. */
    CJK_DATE: 31,

    /** Traditional short time format for CJK locales. */
    CJK_TIME_SHORT: 32,

    /** Traditional long time format for CJK locales. */
    CJK_TIME_LONG: 33,

    /** Time format with total number of hours. */
    TIME_ELAPSED: 46,

    /** Time format with tenth of seconds. */
    TIME_TENTH: 47,

    /** Short scientific notation. */
    SCIENTIFIC_SHORT: 48,

    /** Default text format. */
    TEXT: 49,
} as const satisfies Dict<number>;

/**
 * All existing indexes of preset format codes in `PresetCodeId`.
 */
export type PresetCodeIdType = (typeof PresetCodeId)[keyof typeof PresetCodeId];

/**
 * Optional settings for creating format codes for numbers.
 */
export interface RedCodeOptions {

    /**
     * Whether to return a format code where negative numbers will be formatted
     * with red color.
     */
    red?: boolean;
}

/**
 * Optional settings for format codes for decimal numbers.
 */
export interface DecimalCodeOptions {

    /**
     * Whether to return a format code without fractional digits. Default value
     * is `false`.
     */
    int?: boolean;

    /**
     * Whether to return a format code with group separators. Default value is
     * `false`.
     */
    group?: boolean;
}

/**
 * Optional settings for accounting format codes.
 */
export interface AccountingCodeOptions {

    /**
     * Whether to return a format code without fractional digits. Default value
     * is `false`.
     */
    int?: boolean;

    /**
     * Whether to return a format code with a blind currency symbol (e.g.
     * "_E_U_R" instead of "EUR"). Default value is `false`.
     */
    blind?: boolean;
}

/**
 * Optional settings for currency format codes.
 */
export interface CurrencyCodeOptions extends AccountingCodeOptions, RedCodeOptions { }

/**
 * Optional settings for percentage format codes.
 */
export interface PercentCodeOptions {

    /**
     * Whether to return a format code without fractional digits. Default value
     * is `false`.
     */
    int?: boolean;
}

/**
 * Optional settings for scientific format codes.
 */
export interface ScientificCodeOptions {

    /**
     * Whether to return the short scientific format code(one fractional digit,
     * single-digit exponent). Default value is `false`.
     */
    short?: boolean;
}

/**
 * Optional settings for time format codes.
 */
export interface TimeCodeOptions {

    /**
     * Specifies how to show the hours of the time. If set to `true`, hours
     * will be shown as number from 0 to 23. If set to `false`, hours will be
     * shown as number from 1 to 12, and the format code will include the
     * "AM/PM" token. Default value is `false`.
     */
    hours24?: boolean;

    /**
     * Whether the number format includes the seconds of the time. Default
     * value is `false`.
     */
    seconds?: boolean;
}

// private types --------------------------------------------------------------

/**
 * Enumeration of different CJK scripts.
 */
const enum CJKScriptId {

    /**
     * CJK Unified Ideographs (Simplified Chinese).
     */
    SIMPLIFIED = "s",

    /**
     * CJK Unified Ideographs (Traditional Chinese, Japanese).
     */
    TRADITIONAL = "t",

    /**
     * HANGUL: Hangul alphabet (Korean).
     */
    HANGUL = "h"
}

/**
 * Names of internal date/time format tokens.
 */
type DateTimeTokenId = "Y" | "M" | "D" | "h" | "m" | "s";

/**
 * Format of the hour token for the time formats 18 to 22.
 */
const enum HourMode {

    /**
     * Use "h" for all time formats 18 to 22.
     */
    SHORT = 0,

    /**
     * Use "hh" for all time formats 18 to 22.
     */
    LONG = 1,

    /**
     * Use "h" for 12-hour time formats (formats 18 and 19), and "hh" for
     * 24-hour time formats (formats 20 to 22).
     */
    LONG24 = 2
}

/**
 * Optional settings for CJK date/time format codes.
 */
interface CJKDateTimeCodeOptions {

    /**
     * If set to `true`, the token "AM/PM" will be added in front of the hour
     * token inserted for the meta token `{H}`. Default value is `false`.
     */
    ampm?: boolean;

    /**
     * If set to `true`, the hour token inserted for the meta token `{H}` will
     * be "hh", otherwise "h". Default value is `false`.
     */
    long?: boolean;
}

// constants ==================================================================

/** The first user-defined number format. */
export const PRESET_ID_USER_START = 164;

// count of predefined number formats in a preset table
const PRESET_COUNT = 82;

// characters for CJK date/time format code templates
const CJK_DATE_TIME_CHARACTERS: Record<CJKScriptId, Record<DateTimeTokenId, string>> = {
    [CJKScriptId.SIMPLIFIED]:  { Y: "\u5e74", M: "\u6708", D: "\u65e5", h: "\u65f6", m: "\u5206", s: "\u79d2" },
    [CJKScriptId.TRADITIONAL]: { Y: "\u5e74", M: "\u6708", D: "\u65e5", h: "\u6642", m: "\u5206", s: "\u79d2" },
    [CJKScriptId.HANGUL]:      { Y: "\ub144", M: "\uc6d4", D: "\uc77c", h: "\uc2dc", m: "\ubd84", s: "\ucd08" }
};

// functions ==================================================================

/**
 * Returns the identifier of a preset number format for decimal numbers with a
 * fixed count of decimal places, and optionally with group separators.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The identifier of a preset number format for decimal numbers.
 */
export function getDecimalId(options?: DecimalCodeOptions): number {
    return (options?.int ? 1 : 2) + (options?.group ? 2 : 0);
}

/**
 * Returns the identifier of a preset currency format.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The identifier of a preset currency format.
 */
export function getCurrencyId(options?: CurrencyCodeOptions): number {
    return (options?.blind ? 37 : 5) + (options?.int ? 0 : 2) + (options?.red ? 1 : 0);
}

/**
 * Returns the identifier of a preset accounting format.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The identifier of a preset accounting format.
 */
export function getAccountingId(options?: AccountingCodeOptions): number {
    return 41 + (options?.blind ? 0 : 1) + (options?.int ? 0 : 2);
}

/**
 * Returns the identifier of a preset number format for percentage notation
 * with a fixed count of decimal places.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The identifier of a preset number format for percentage notation.
 */
export function getPercentId(options?: PercentCodeOptions): number {
    return options?.int ? PresetCodeId.PERCENT_INT : PresetCodeId.PERCENT;
}

/**
 * Returns the identifier of a preset number format for scientific notation
 * with a fixed count of decimal places.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The identifier of a preset number format for scientific notation.
 */
export function getScientificId(options?: ScientificCodeOptions): number {
    return options?.short ? PresetCodeId.SCIENTIFIC_SHORT : PresetCodeId.SCIENTIFIC;
}

/**
 * Returns the identifier of a preset time format.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The identifier of a preset time format.
 */
export function getTimeId(options?: TimeCodeOptions): number {
    return 18 + (options?.seconds ? 1 : 0) + (options?.hours24 ? 2 : 0);
}

// class PresetFormatTable ====================================================

/**
 * A table with predefined number format codes for a specific locale, as used
 * in the OOXML file format.
 */
export class PresetFormatTable extends DObject {

    // properties -------------------------------------------------------------

    // the locale data this table is based on
    #lcData!: LocaleData;

    // all format codes, mapped by preset format identifier
    readonly #formatCodes: string[] = [];

    // all preset format identifiers, mapped by format code
    readonly #invMap = new Map<string, number>();

    // all unresolved format codes for construction phase
    #initCodes!: Array<string | number>;

    // constructor ------------------------------------------------------------

    constructor(localeData: LocaleData, config?: DObjectConfig) {
        super(config);
        this.initialize(localeData);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the predefined format code for the specified format identifier.
     *
     * @param formatId
     *  The identifier of a predefined format code.
     *
     * @returns
     *  The predefined format code for the specified format identifier, if
     *  available; otherwise `null`.
     */
    getFormatCode(formatId: PresetCodeIdType): string;
    getFormatCode(formatId: number): string | null;
    // implementation
    getFormatCode(formatId: number): string | null {
        return this.#formatCodes[formatId] || null;
    }

    /**
     * Returns a format code for decimal numbers with a fixed count of decimal
     * places, and optionally with group separators.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The format code for decimal numbers.
     */
    getDecimalCode(options?: DecimalCodeOptions): string {
        return this.#formatCodes[getDecimalId(options)];
    }

    /**
     * Returns the currency format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The currency format code for the passed settings.
     */
    getCurrencyCode(options?: CurrencyCodeOptions): string {
        return this.#formatCodes[getCurrencyId(options)];
    }

    /**
     * Returns the accounting format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The accounting format code for the passed settings.
     */
    getAccountingCode(options?: AccountingCodeOptions): string {
        return this.#formatCodes[getAccountingId(options)];
    }

    /**
     * Returns a format code for percentages with a fixed count of decimal
     * places.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The format code for percentages.
     */
    getPercentCode(options?: PercentCodeOptions): string {
        return this.#formatCodes[getPercentId(options)];
    }

    /**
     * Returns a format code for scientific notation.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The format code for scientific notation.
     */
    getScientificCode(options?: ScientificCodeOptions): string {
        return this.#formatCodes[getScientificId(options)];
    }

    /**
     * Returns the time format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The time format code for the passed settings.
     */
    getTimeCode(options?: TimeCodeOptions): string {
        return this.#formatCodes[getTimeId(options)];
    }

    /**
     * Returns the predefined identifier of the specified format code.
     *
     * @param formatCode
     *  The format code to be converted to a format identifier.
     *
     * @returns
     *  The predefined identifier of the passed format code, if available;
     *  otherwise `null`.
     */
    getFormatId(formatCode: string): number | null {
        return this.#invMap.get(formatCode) ?? null;
    }

    /**
     * Full initialization of the format table.
     */
    initialize(lcData: LocaleData): void {
        this.#lcData = lcData;

        // initialize all formats with zeros
        this.#initCodes = ary.fill(PRESET_COUNT, PresetCodeId.STANDARD);

        // 0..4: decimal numbers
        this.#setFormats(0, OP_STD_TOKEN, "0", "0.00", "#,##0", "#,##0.00");

        // 5..8: currency formats with currency symbol
        this.#setCurrencyFormats(5);

        // 9..13: percentage, scientific, fractions
        this.#setFormats(9, "0%", "0.00%", "0.00E+00", "# ?/?", "# ??/??");

        // 14..22: defaults for date and time formats
        this.#setAllDateTimeFormats("D-MMM-YY");

        // 23..26: defaults for CJK currency formats (id 0)

        // 27..36: defaults for CJK date/time formats
        this.#setFormats(27, 14, 14, lcData.cjk ? 28 : 14, 14, 14, 21, 21, 21, 21, lcData.cjk ? 27 : 14);

        // 37..40: currency formats with blind currency symbol
        this.#setCurrencyFormats(37, { symbolModifier: "blind" });

        // 41..44: accounting formats
        this.#setAccountingFormats(41);

        // 45..49: additional formats
        this.#setFormats(45, "mm:ss", "[h]:mm:ss", "mm:ss.0", "0.0E+0", "@");

        // 50 to 58: defaults for CJK date/time formats
        if (lcData.cjk) {
            this.#setFormats(50, 27, 28, 34, 35, 28, 34, 35, 27, 28);
        } else {
            this.#setFormats(50, 14, 14, 14, 14, 14, 14, 14, 14, 14);
        }

        // 59 to 81: defaults for Thai formats with "t" modifier
        this.#setFormats(59, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 14, 15, 16, 17, 20, 21, 22, 45, 46, 47);

        // locale-dependent settings (especially date/time and currency formats)
        switch (lcData.lc) {

            case "ar_AE": // Arabic, U.A.E.
            case "ar_BH": // Arabic, Bahrain
            case "ar_EG": // Arabic, Egypt
            case "ar_IQ": // Arabic, Iraq
            case "ar_JO": // Arabic, Jordan
            case "ar_KW": // Arabic, Kuwait
            case "ar_LB": // Arabic, Lebanon
            case "ar_LY": // Arabic, Libya
            case "ar_OM": // Arabic, Oman
            case "ar_QA": // Arabic, Qatar
            case "ar_SA": // Arabic, Saudi Arabia
            case "ar_SY": // Arabic, Syria
            case "syr_SY": // Syriac, Syria
            case "ar_YE": // Arabic, Yemen
            case "fa_IR": // Farsi, Iran
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "ar_DZ": // Arabic, Algeria
            case "ar_MA": // Arabic, Morocco
            case "ar_TN": // Arabic, Tunisia
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "be_BY": // Belarusian, Belarus
            case "hu_HU": // Hungarian, Hungary
            case "hy_AM": // Armenian, Armenia
            case "ka_GE": // Georgian, Georgia
            case "ky_KG": // Kyrgyz, Kyrgyzstan
            case "tt_RU": // Tatar, Russian Federation
            case "uk_UA": // Ukrainian, Ukraine
                this.#setAllDateTimeFormats("DD.MMM.YY");
                break;

            case "bg_BG": // Bulgarian, Bulgaria
            case "cs_CZ": // Czech, Czech Republic
            case "et_EE": // Estonian, Estonia
            case "hr_BA": // Croatian, Bosnia and Herzegovina
            case "hr_HR": // Croatian, Croatia
            case "sk_SK": // Slovak, Slovakia
            case "sl_SI": // Slovenian, Slovenia
                this.#setAllDateTimeFormats("D.MMM.YY");
                break;

            case "bn_IN": // Bengali, India
            case "gu_IN": // Gujarati, India
            case "hi_IN": // Hindi, India
            case "kok_IN": // Konkani, India
            case "kn_IN": // Kannada, India
            case "ml_IN": // Malayalam, India
            case "mr_IN": // Marathi, India
            case "sa_IN": // Sanskrit, India
            case "ta_IN": // Tamil, India
            case "te_IN": // Telugu, India
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "da_DK": // Danish, Denmark
            case "fo_FO": // Faroese, Faroe Islands
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "de_AT": // German, Austria
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "de_CH": // German, Switzerland
            case "de_LI": // German, Liechtenstein
                this.#setAllDateTimeFormats("DD. MMM YY", HourMode.LONG24);
                break;

            case "de_DE": // German, Germany
                this.#setAllDateTimeFormats("DD. MMM YY", HourMode.LONG24);
                break;

            case "de_LU": // German, Luxembourg
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "div_MV": // Divehi, Maldives
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "el_GR": // Greek, Greece
                this.#setAllDateTimeFormats("D-MMM-YY");
                break;

            case "en_AU": // English, Australia
            case "en_NZ": // English, New Zealand
            case "mi_NZ": // Maori, New Zealand
                this.#setAllDateTimeFormats("D-MMM-YY");
                break;

            case "en_BZ": // English, Belize
            case "en_TT": // English, Trinidad and Tobago
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "en_CA": // English, Canada
            case "en_CB": // English, Caribbean
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "cy_GB": // Welsh, United Kingdom
            case "en_GB": // English, United Kingdom
            case "en_IE": // English, Ireland
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "en_JM": // English, Jamaica
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "en_PH": // English, Philippines
            case "en_US": // English, USA
            case "en_ZW": // English, Zimbabwe
                this.#setAllDateTimeFormats("D-MMM-YY");
                break;

            case "af_ZA": // Afrikaans, South Africa
            case "en_ZA": // English, South Africa
            case "nso_ZA": // Northern Sotho, South Africa
            case "tn_ZA": // Tswana, South Africa
            case "xh_ZA": // Xhosa, South Africa
            case "zu_ZA": // Zulu, South Africa
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "es_AR": // Spanish, Argentina
            case "es_HN": // Spanish, Honduras
            case "es_PE": // Spanish, Peru
            case "qu_PE": // Quechua, Peru
            case "es_VE": // Spanish, Venezuela
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "es_BO": // Spanish, Bolivia
            case "qu_BO": // Quechua, Bolivia
            case "es_CO": // Spanish, Colombia
            case "es_NI": // Spanish, Nicaragua
            case "es_PA": // Spanish, Panama
            case "es_PR": // Spanish, Puerto Rico
            case "es_PY": // Spanish, Paraguay
            case "es_UY": // Spanish, Uruguay
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "es_CL": // Spanish, Chile
            case "gl_ES": // Galizian, Spain
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "es_CR": // Spanish, Costa Rica
            case "es_DO": // Spanish, Dominican Republic
            case "es_GT": // Spanish, Guatemala
            case "es_SV": // Spanish, El Salvador
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "es_EC": // Spanish, Ecuador
            case "qu_EC": // Quechua, Ecuador
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "ca_ES": // Catalan, Spain
            case "es_ES": // Spanish, Spain
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "es_MX": // Spanish, Mexico
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "fi_FI": // Finnish, Finland
            case "se_FI": // Sami, Finland
                this.#setFormats(9, "0 %", "0.00 %");
                this.#setAllDateTimeFormats("D.MMM.YY");
                break;

            case "fr_BE": // French, Belgium
                this.#setAllDateTimeFormats("D-MMM-YY");
                break;

            case "fr_CA": // French, Canada
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "fr_CH": // French, Switzerland
            case "it_CH": // Italian, Switzerland
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "fr_FR": // French, France
            case "fr_LU": // French, Luxembourg
            case "fr_MC": // French, Monaco
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "he_IL": // Hebrew, Israel
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "id_ID": // Indonesian, Indonesia
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "is_IS": // Icelandic, Iceland
                this.#setAllDateTimeFormats("D.MMM.YY", HourMode.LONG24);
                break;

            case "it_IT": // Italian, Italy
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "ja_JP": // Japanese, Japan
                this.#setAllDateTimeFormats("DD-MMM-YY");
                this.#setCurrencyFormats(23, { locale: "en_US" });
                this.#setFormats(27, "[$-411]Ge.MM.DD");
                this.#setCJKDateTimeFormat(28, '[$-411]GGGe"{YEAR}"MM"{MONTH}"DD"{DAY}"', CJKScriptId.TRADITIONAL);
                this.#setFormats(30, "MM/DD/YY");
                this.#setCJKDateTimeFormat(31, 'YYYY"{YEAR}"MM"{MONTH}"DD"{DAY}"', CJKScriptId.TRADITIONAL);
                this.#setCJKTimeFormats(32, CJKScriptId.TRADITIONAL);
                this.#setCJKDateTimeFormat(34, 'YYYY"{YEAR}"MM"{MONTH}"', CJKScriptId.TRADITIONAL);
                this.#setCJKDateTimeFormat(35, 'MM"{MONTH}"DD"{DAY}"', CJKScriptId.TRADITIONAL);
                break;

            case "kk_KZ": // Kazakh, Kazakhstan
                this.#setAllDateTimeFormats("DD.MMM.YY");
                break;

            case "ko_KR": // Korean, South Korea
                this.#setAllDateTimeFormats("DD-MMM-YY");
                this.#setCurrencyFormats(23, { locale: "en_US" });
                this.#setCJKDateTimeFormat(27, "YYYY{YEAR} MM{MONTH} DD{DAY}", CJKScriptId.TRADITIONAL);
                this.#setFormats(28, "MM-DD");
                this.#setFormats(30, "MM-DD-YY");
                this.#setCJKDateTimeFormat(31, "YYYY{YEAR} MM{MONTH} DD{DAY}", CJKScriptId.HANGUL);
                this.#setCJKTimeFormats(32, CJKScriptId.HANGUL);
                this.#setFormats(34, "YYYY/MM/DD", 14);
                break;

            case "lt_LT": // Lithuanian, Lithuania
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "lv_LV": // Latvian, Latvia
                this.#setAllDateTimeFormats("DD.MMM.YY");
                break;

            case "mn_MN": // Mongolian, Mongolia
                this.#setAllDateTimeFormats("DD.MMM.YY");
                break;

            case "ms_BN": // Malay, Brunei Darussalam
            case "ms_MY": // Malay, Malaysia
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "mt_MT": // Maltese, Malta
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "nb_NO": // Norwegian (Bokmal), Norway
            case "nn_NO": // Norwegian (Nynorsk), Norway
            case "se_NO": // Sami, Norway
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "nl_BE": // Dutch, Belgium
                this.#setAllDateTimeFormats("D/MMM/YY");
                break;

            case "nl_NL": // Dutch, Netherlands
                this.#setAllDateTimeFormats("D-MMM-YY");
                break;

            case "pa_IN": // Punjabi, India
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG);
                break;

            case "pl_PL": // Polish, Poland
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "pt_BR": // Portugese, Brazil
                this.#setAllDateTimeFormats("D/MMM/YY", HourMode.LONG24);
                break;

            case "pt_PT": // Portugese, Portugal
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "ro_RO": // Romanian, Romania
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "ru_RU": // Russian, Russian Federation
                this.#setAllDateTimeFormats("DD.MMM.YY");
                break;

            case "sv_FI": // Swedish, Finland
                this.#setFormats(9, "0 %", "0.00 %");
                this.#setAllDateTimeFormats("D.MMM.YY", HourMode.LONG24);
                break;

            case "se_SE": // Sami, Sweden
            case "sv_SE": // Swedish, Sweden
                this.#setAllDateTimeFormats("DD-MMM-YY", HourMode.LONG24);
                break;

            case "sw_TZ": // Swahili, Tanzania
                this.#setAllDateTimeFormats("D-MMM-YY");
                break;

            case "th_TH": // Thai, Thailand
                this.#setAllDateTimeFormats("D-MMM-YY");
                this.#setFormats(59, "t0", "t0.00", "t#,##0", "t#,##0.00");
                this.#setCurrencyFormats(63, { currencyData: lcData.currency, digitModifier: "t" });
                this.#setFormats(67, "t0%", "t0.00%", "t# ?/?", "t# ??/??");
                this.#setFormats(71, "tD/M/EE", "tD-MMM-E", "tD-MMM", "tMMM-E", "th:mm", "th:mm:ss");
                this.#setFormats(77, "tD/M/EE h:mm", "tmm:ss", "t[h]:mm:ss", "tmm:ss.0", "D/M/E");
                break;

            case "syr_TR": // Syriac, Turkey
            case "tr_TR": // Turkish, Turkey
                this.#setAllDateTimeFormats("DD.MMM.YY", HourMode.LONG24);
                break;

            case "ur_PK": // Urdu, Pakistan
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "vi_VN": // Vietnamese, Viet Nam
                this.#setAllDateTimeFormats("DD-MMM-YY");
                break;

            case "zh_CN": // Chinese, China
                this.#setAllDateTimeFormats("D-MMM-YY");
                this.#setCurrencyFormats(23, { locale: "en_US" });
                this.#setCJKDateTimeFormat(27, 'YYYY"{YEAR}"M"{MONTH}"', CJKScriptId.SIMPLIFIED);
                this.#setCJKDateTimeFormat(28, 'M"{MONTH}"D"{DAY}"', CJKScriptId.SIMPLIFIED);
                this.#setFormats(30, "M-D-YY");
                this.#setCJKDateTimeFormat(31, 'YYYY"{YEAR}"M"{MONTH}"D"{DAY}"', CJKScriptId.SIMPLIFIED);
                this.#setCJKTimeFormats(32, CJKScriptId.SIMPLIFIED);
                this.#setCJKTimeFormats(34, CJKScriptId.SIMPLIFIED, { ampm: true });
                this.#setFormats(52, 27, 28);
                break;

            case "zh_HK": // Chinese, Hong Kong
                this.#setAllDateTimeFormats("D-MMM-YY");
                this.#setCurrencyFormats(23, { locale: "en_US", currencyData: { symbol: "US$" } });
                this.#setFormats(27, "[$-404]D/M/E");
                this.#setCJKDateTimeFormat(28, '[$-404]D"{DAY}"M"{MONTH}"E"{YEAR}"', CJKScriptId.TRADITIONAL);
                this.#setFormats(30, "M/D/YY");
                this.#setCJKDateTimeFormat(31, 'D"{DAY}"M"{MONTH}"YYYY"{YEAR}"', CJKScriptId.TRADITIONAL);
                this.#setCJKTimeFormats(32, CJKScriptId.TRADITIONAL);
                this.#setCJKTimeFormats(34, CJKScriptId.TRADITIONAL, { ampm: true });
                break;

            case "zh_MO": // Chinese, Macau
                this.#setAllDateTimeFormats("D-MMM-YY");
                this.#setCurrencyFormats(23, { locale: "en_US", currencyData: { symbol: "US$" } });
                this.#setFormats(27, "[$-404]D/M/e");
                this.#setCJKDateTimeFormat(28, '[$-404]D"{DAY}"M"{MONTH}"e"{YEAR}"', CJKScriptId.TRADITIONAL);
                this.#setFormats(30, "M/D/YY");
                this.#setCJKDateTimeFormat(31, 'D"{DAY}"M"{MONTH}"YYYY"{YEAR}"', CJKScriptId.TRADITIONAL);
                this.#setCJKTimeFormats(32, CJKScriptId.TRADITIONAL);
                this.#setCJKTimeFormats(34, CJKScriptId.TRADITIONAL, { ampm: true });
                break;

            case "zh_SG": // Chinese, Singapore
                this.#setAllDateTimeFormats("D-MMM-YY");
                this.#setCurrencyFormats(23, { locale: "en_US" });
                this.#setCJKDateTimeFormat(27, 'YYYY"{YEAR}"M"{MONTH}"', CJKScriptId.SIMPLIFIED);
                this.#setCJKDateTimeFormat(28, 'M"{MONTH}"D"{DAY}"', CJKScriptId.SIMPLIFIED);
                this.#setFormats(30, "M/D/YY");
                this.#setCJKDateTimeFormat(31, 'D"{DAY}"M"{MONTH}"YYYY"{YEAR}"', CJKScriptId.SIMPLIFIED);
                this.#setCJKTimeFormats(32, CJKScriptId.SIMPLIFIED);
                this.#setCJKTimeFormats(34, CJKScriptId.SIMPLIFIED, { ampm: true });
                break;

            case "zh_TW": // Chinese, Taiwan
                this.#setAllDateTimeFormats("D-MMM-YY", HourMode.LONG);
                this.#setCurrencyFormats(23, { locale: "en_US", currencyData: { symbol: "US$" } });
                this.#setFormats(27, "[$-404]e/M/D");
                this.#setCJKDateTimeFormat(28, '[$-404]e"{YEAR}"M"{MONTH}"D"{DAY}"', CJKScriptId.TRADITIONAL);
                this.#setFormats(30, "M/D/YY");
                this.#setCJKDateTimeFormat(31, 'YYYY"{YEAR}"M"{MONTH}"D"{DAY}"', CJKScriptId.TRADITIONAL);
                this.#setCJKTimeFormats(32, CJKScriptId.TRADITIONAL, { long: true });
                this.#setCJKTimeFormats(34, CJKScriptId.TRADITIONAL, { long: true, ampm: true });
                break;

            default:
                globalLogger.warn(`$badge{PresetFormatTable} initialize: unknown locale "${lcData.lc}"`);
        }

        // resolve all placeholder formats; create the inverse map (format
        // codes to identifiers, use first available identifier only)
        this.#formatCodes.length = 0;
        this.#invMap.clear();
        for (let id = 0; id < PRESET_COUNT; id += 1) {
            let format: string | number = id;
            do { format = this.#initCodes[format]; } while (is.number(format));
            this.#formatCodes.push(format);
            if (!this.#invMap.has(format)) { this.#invMap.set(format, id); }
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Defines one or more number format entries.
     *
     * @param id
     *  The identifier of the first number format to be defined.
     *
     * @param formats
     *  Literal format codes as string, or other number format identifiers to
     *  be cloned in the end.
     */
    #setFormats(id: number, ...formats: Array<string | number>): void {
        formats.forEach((format, i) => { this.#initCodes[id + i] = format; });
    }

    /**
     * Defines the date and time formats 14 to 22.
     *
     * @param fixedDateCode
     *  Fixed date code (for number formats 15 to 17). MUST be in strict order
     *  day-month-year, with some separator characters inbetween.
     *
     * @param [hourMode=SHORT]
     *  Format of the hour token.
     */
    #setAllDateTimeFormats(fixedDateCode: string, hourMode?: HourMode): void {
        const hour12 = (hourMode !== HourMode.LONG) ? "h" : "hh";
        const hour24 = ((hourMode === HourMode.LONG) || (hourMode === HourMode.LONG24)) ? "hh" : "h";
        this.#setFormats(14, this.#lcData.shortDate, fixedDateCode);
        this.#setFormats(16, fixedDateCode.replace(/[^M]+Y+$/, ""));
        this.#setFormats(17, fixedDateCode.replace(/^D+[^M]+/, ""));
        this.#setFormats(18, `${hour12}:mm AM/PM`, `${hour12}:mm:ss AM/PM`);
        this.#setFormats(20, `${hour24}:mm`, `${hour24}:mm:ss`);
        this.#setFormats(22, `${this.#lcData.shortDate} ${hour24}:mm`); // always 24-hours format
    }

    /**
     * Defines a time format for CJK locales.
     *
     * @param id
     *  The number format identifier for the format code.
     *
     * @param metaCode
     *  The date/time meta code. May contain the meta tokens `{YEAR}`,
     *  `{MONTH}`, `{DAY}`, `{HOUR}`, `{MINUTE}`, and `{SECOND}`, which will be
     *  replaced by the respective words according to the passed CJK script
     *  type (language); and the meta token `{H}` that will be replaced by an
     *  hour token according to the passed options.
     *
     * @param scriptId
     *  The CJK script type used for the meta tokens (see parameter "metaCode")
     *  added to the generated format code.
     *
     * @param [options]
     *  Optional parameters.
     */
    #setCJKDateTimeFormat(id: number, metaCode: string, scriptId: CJKScriptId, options?: CJKDateTimeCodeOptions): void {
        const chars = CJK_DATE_TIME_CHARACTERS[scriptId];
        this.#setFormats(id, metaCode
            .replace(/{H}/g, `${options?.ampm ? "AM/PM" : ""}${options?.long ? "hh" : "h"}`)
            .replace(/{YEAR}/g, chars.Y)
            .replace(/{MONTH}/g, chars.M)
            .replace(/{DAY}/g, chars.D)
            .replace(/{HOUR}/g, chars.h)
            .replace(/{MINUTE}/g, chars.m)
            .replace(/{SECOND}/g, chars.s)
        );
    }

    /**
     * Defines two time formats without, one trailing seconds, and one with
     * trailing seconds, for CJK locales.
     *
     * @param id
     *  First number format identifier for the format code without seconds.
     *  This method generates format codes for `id` (without seconds), and for
     *  `id+1` (with seconds).
     *
     * @param scriptId
     *  The CJK script type used to generate the time formats.
     *
     * @param [options]
     *  Optional parameters.
     */
    #setCJKTimeFormats(id: number, scriptId: CJKScriptId, options?: CJKDateTimeCodeOptions): void {
        const metaCode = '{H}"{HOUR}"mm"{MINUTE}"';
        this.#setCJKDateTimeFormat(id, metaCode, scriptId, options);
        this.#setCJKDateTimeFormat(id + 1, `${metaCode}ss"{SECOND}"`, scriptId, options);
    }

    /**
     * Defines a currency/accounting format according to the passed meta code.
     *
     * @param id
     *  The number format identifier for the currency format code.
     *
     * @param options
     *  Optional parameters.
     */
    #setCurrencyFormat(id: number, options?: CurrencyCodeConfig): void {
        this.#setFormats(id, generateCurrencyCode({ locale: this.#lcData, groupSep: true, ...options }));
    }

    /**
     * Defines four consecutive currency formats.
     *
     * @param id
     *  Number format identifier for the first currency format.
     *
     * @param [options]
     *  Optional parameters.
     */
    #setCurrencyFormats(id: number, options?: CurrencyCodeConfig): void {
        this.#setCurrencyFormat(id,     { ...options, patternType: "currency", negativeRed: false, fracDigits: 0 });
        this.#setCurrencyFormat(id + 1, { ...options, patternType: "currency", negativeRed: true,  fracDigits: 0 });
        this.#setCurrencyFormat(id + 2, { ...options, patternType: "currency", negativeRed: false });
        this.#setCurrencyFormat(id + 3, { ...options, patternType: "currency", negativeRed: true });
    }

    /**
     * Defines four consecutive accounting formats.
     *
     * @param id
     *  Number format identifier for the first accounting format.
     *
     * @param [options]
     *  Optional parameters.
     */
    #setAccountingFormats(id: number, options?: CurrencyCodeConfig): void {
        this.#setCurrencyFormat(id,     { ...options, patternType: "accounting", symbolModifier: "blind", fracDigits: 0 });
        this.#setCurrencyFormat(id + 1, { ...options, patternType: "accounting", fracDigits: 0 });
        this.#setCurrencyFormat(id + 2, { ...options, patternType: "accounting", symbolModifier: "blind" });
        this.#setCurrencyFormat(id + 3, { ...options, patternType: "accounting" });
    }
}
