/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type NumberComps, math } from "@/io.ox/office/tk/algorithms";
import type { Font } from "@/io.ox/office/tk/dom";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

// types ======================================================================

/**
 * A result descriptor for calculating the significant digits of a number.
 * Contains the mantissa and exponent of the rounded number, and the resulting
 * significant digits as string.
 */
export interface SignificantDigitsResult extends NumberComps {

    /**
     * The resulting significant digits, as string. Neither the first nor the
     * last digit will be zero. Example: "1023" for the numbers 0.01023, 1.023,
     * 102.3, and 102300.
     */
    digits: string;
}

/**
 * A result descriptor filled with the integer part and the fraction part of a
 * formatted decimal number with specific significant digits and exponent.
 */
export interface DigitPartsResult {

    /**
     * The integer part of the number. Example: "12" for the number 12.34 (with
     * significant digits "1234" and exponent 1).
     */
    int: string;

    /**
     * The fraction part of the number. Example: "34" for the number 12.34
     * (with significant digits "1234" and exponent 1).
     */
    frac: string;
}

/**
 * Options for auto-formatting a number to its text representation.
 */
export interface AutoFormatOptions {

    /**
     * The decimal separator character to be used. Default value is the decimal
     * separator of the current UI locale.
     */
    dec?: string;

    /**
     * The exponent separator character to be used. Default value is "E".
     */
    expSign?: string;

    /**
     * The maximum number of characters allowed for the absolute part of the
     * passed number, including the decimal separator and the complete exponent
     * in scientific notation, but without a leading minus sign. MUST be a
     * positive integer. Default value is `STD_LENGTH_AUTOFORMAT`.
     */
    stdLen?: number;

    /**
     * The minimum number of digits for the exponent in scientific notation
     * (without sign). If the resulting exponent is shorter, it will be filled
     * with leading zero digits. MUST be a positive integer; must be less than
     * or equal to 9. Default value is `2`.
     */
    expLen?: number;

    /**
     * The maximum width of the formatted number, in pixels. If omitted, the
     * width of the formatted value will not be restricted (but option `stdLen`
     * will still apply).
     */
    maxWidth?: number;

    /**
     * The font settings to be used for the option `maxWidth`. If omitted, the
     * option `maxWidth` will be ignored.
     */
    renderFont?: Font;
}

/**
 * The type of the resulting formatted text. Can either be a string to be
 * rendered, or the special value `null` denoting an unrenderable value, e.g.
 * due to insufficient space to render the number.
 */
export type DisplayText = string | null;

/**
 * Result settings after auto-formatting a number.
 */
export interface AutoFormatResult {

    /**
     * The finalized formatted number with correct sign, in either decimal or
     * scientific notation; or `null`, if the available space is too small to
     * take the number in either decimal or scientific notation.
     */
    text: DisplayText;

    /**
     * The width of the formatted number with sign, in pixels. Will be zero, if
     * no rendering font was available.
     */
    width: number;
}

// private types --------------------------------------------------------------

/**
 * Properties of a number to be auto-formatted.
 */
interface AutoFormatState extends NumberComps {

    /**
     * The original (signed) number to be auto-formatted.
     */
    num: number;

    /**
     * The decimal separator character to be used.
     */
    dec: string;

    /**
     * The exponent separator character to be used.
     */
    expSign: string;

    /**
     * The maximum number of characters to be used for the absolute number.
     */
    stdLen: number;

    /**
     * The minimum number of digits for the exponent.
     */
    minExpLen: number;

    /**
     * The maximum number of significant digits available for decimal notation.
     * May be much less than the maximum length, e.g. the number 0.00012 fits
     * into 7 characters, but can show only 2 significant digits. Can be
     * negative to indicate how many digits are missing to be able to create an
     * appropriately formatted number.
     */
    maxDecDigits: number;

    /**
     * The maximum number of significant digits available for scientific
     * notation. Can be negative to indicate how many digits are missing to be
     * able to create an appropriately formatted number.
     */
    maxSciDigits: number;

    /**
     * The number of digits required for the absolute value of the exponent
     * (according to the exponent, and the value of the property `minExpLen`).
     */
    expLen: number;

    /**
     * The maximum width of the formatted number, in pixels.
     */
    maxWidth: number;

    /**
     * The font settings to be used for the option `maxWidth`.
     */
    renderFont?: Font;
}

/**
 * The internal result object for a formatted number.
 */
interface AssembledResult {
    text: string;
    scientific: boolean;
    width: number;
}

// constants ==================================================================

/**
 * Default value for maximum number of characters in an auto-formatted number.
 */
export const STD_LENGTH_AUTOFORMAT = 11;

// private constants ----------------------------------------------------------

// different correction summands to workaround rounding errors caused by the limited
// internal precision of floating-point numbers
const EPSILON_1 = Number.EPSILON * 0.75;
const EPSILON_2 = Number.EPSILON * 1.5;
const EPSILON_4 = Number.EPSILON * 3;
const EPSILON_8 = Number.EPSILON * 6;

// private functions ==========================================================

/**
 * Calculates various properties of a number needed for auto-formatting.
 *
 * @param num
 *  The original (signed) number to be processed.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A result object with all properties of the passed number.
 */
function analyseNumber(num: number, options?: AutoFormatOptions): AutoFormatState {

    // Initialize the resulting state descriptor.
    const stdLen = options?.stdLen ?? STD_LENGTH_AUTOFORMAT;
    const state: AutoFormatState = {
        num,
        dec: options?.dec || LOCALE_DATA.dec,
        expSign: options?.expSign || "E",
        stdLen,
        minExpLen: math.clamp(options?.expLen ?? 2, 1, 9),
        mant: 0,
        expn: 0,
        maxDecDigits: 0,
        maxSciDigits: 0,
        expLen: 0,
        maxWidth: options?.maxWidth ?? Infinity,
        renderFont: options?.renderFont
    };

    // Fast exit, if the number is exactly or very close to zero (do not try to format denormalized numbers).
    if (math.isZero(num)) { return state; }

    // Start with the absolute value of the mantissa, and the exponent of the passed number.
    const comps = math.splitNumber(Math.abs(num));
    state.mant = comps.mant;
    state.expn = comps.expn;

    // Find the maximum number of digits available for decimal notation.
    //
    // Case 1: The maximum number of characters is less than or equal to the exponent. In this case, the integral part
    // of the number cannot fit into the available space, therefore the number cannot be formatted in decimal notation
    // at all. Example: The number 1.23E+08 (exponent is 8) will be formatted as "123000000" (9 digits). If the maximum
    // length is less than or equal to 8, the number would not fit into decimal notation.
    //
    // Case 2: Exponent is non-negative (the passed absolute number is equal to or greater than 1): The number of
    // available digits is equal to the passed maximum length.
    //
    // Case 3: Exponent is negative (the passed absolute number is less than 1): Available digits need to be reduced
    // due to leading zeros. Example: The number 1.23E-03 (exponent is -3) will be formatted as "0.00123" (3 zero
    // digits before the significant digits). Therefore, the number of available digits must be reduced by the absolute
    // value of the exponent.
    //
    state.maxDecDigits = (stdLen <= state.expn) ? (stdLen - state.expn) : (state.expn >= 0) ? stdLen : (stdLen + state.expn);

    // If the exponent increased by one is still less than the maximum number of characters, the formatted number
    // includes the decimal separator, thus the number of available digits needs to be reduced by one. For negative
    // exponents (case 3), this applies always.
    //
    if ((state.maxDecDigits > 0) && (state.expn + 1 < stdLen)) {
        state.maxDecDigits -= 1;
    }

    // The number of digits occupied by the absolute value of the exponent.
    //
    state.expLen =  Math.max(state.minExpLen, ((state.expn === 0) ? 0 : Math.floor(Math.log10(Math.abs(state.expn)))) + 1);

    // Find the maximum number of digits available for scientific notation.
    //
    // The passed maximum length will be reduced by the number of characters used by the complete exponent (including
    // the "E" character, and the following +/- sign character). If there are at least two characters available, one of
    // them is needed for the decimal separator.
    //
    state.maxSciDigits = stdLen - state.expLen - state.expSign.length - 1;
    if (state.maxSciDigits > 1) { state.maxSciDigits -= 1; }

    // Return the resulting properties.
    return state;
}

/**
 * Returns the passed formatted absolute number with the correct sign; or
 * `undefined`, if the string is too long to fit into the maximum length (may
 * happen when the conversion has rounded up a number to the next power of 10).
 *
 * Example: The number 99.5 needs to be formatted to a maximum length of two
 * characters. The code that decides whether a number fits runs before
 * rounding, therefore the number is considered to be valid. It will be rounded
 * to the next number with two significant digits which results in 100.
 * Finally, this result is too large for the maximum length, and this function
 * has to return `undefined`.
 *
 * @param state
 *  The data structure for the number to be formatted.
 *
 * @param mant
 *  The resulting formatted absolute mantissa for scientific notation, or the
 *  entire formatted absolute number for decimal notation.
 *
 * @param [expn]
 *  The resulting formatted exponent with sign for scientific notation. MUST be
 *  omitted for decimal notation.
 *
 * @returns
 *  A result object with the formatted number with correct sign, if the passed
 *  strings fit into the maximum length and width; otherwise `undefined`.
 */
function assembleNumber(state: AutoFormatState, mant: string, expn?: string): Opt<AssembledResult> {

    // build the resulting formatted number (without sign for now)
    const absText = expn ? `${mant}${state.expSign}${expn}` : mant;
    if (state.stdLen < absText.length) { return undefined; }

    // add the leading sign to the final text
    const text = (state.num < 0) ? `-${absText}` : absText;

    // the resulting pixel width
    const width = state.renderFont?.getTextWidth(text) ?? 0;
    if (state.maxWidth < width) { return undefined; }

    // return the result object
    return { text, scientific: !!expn, width };
}

/**
 * Auto-formats a number according to the passed settings.
 *
 * @param state
 *  The data structure for the number to be formatted.
 *
 * @returns
 *  A result object with the formatted number with correct sign, if the number
 *  fits into the maximum length; otherwise `undefined`.
 */
function formatNumberToLength(state: AutoFormatState): Opt<AssembledResult> {

    // Create local copies of the properties for temporary modifications below.
    let { mant, expn, maxDecDigits: decDigits, maxSciDigits: sciDigits } = state;
    const { dec, minExpLen } = state;

    // Special edge case, if the number cannot be formatted according to the passed properties: If the mantissa is at
    // least 5, and the exponent is a negative power of 10, the number can be rounded so that the length of the
    // exponent shrinks by one character. The available scientific digits must be exactly equal to zero which indicates
    // that reducing the exponent by one digit makes space for the required significant digit of the mantissa.
    //
    // Example: The formatted number "9E-100" would take 6 characters, but can be rounded to "1E-99" which takes only 5
    // characters. The workaround is not needed for the exponent -1. Such numbers (e.g. 0.5) will be formatted with
    // decimal notation (see next edge case below).
    //
    if ((decDigits <= 0) && (sciDigits === 0) && (mant >= 5) && (((expn === -10) && (minExpLen <= 1)) || ((expn === -100) && (minExpLen <= 2)))) {
        mant = 1;
        expn += 1;
        sciDigits = 1;
    }

    // Special edge case: Allow numbers less than 1 to be formatted as single-digit integer ("0" or "1").
    //
    // Example: The number 0.00123 cannot be formatted into one or two characters with at least one significant digit,
    // but it can be formatted as "0". Similarly, the number 0.987 does not fit, but can be formatted as "1".
    //
    if ((decDigits <= 0) && (sciDigits <= 0) && (expn < 0)) {
        mant = Math.round(Math.abs(state.num));
        expn = 0;
        decDigits = 1;
    }

    // Fast exit, if the number is exactly or very close to zero (do not try to format denormalized numbers).
    if (math.isZero(mant)) { return assembleNumber(state, "0"); }

    // Exit, if the number cannot be formatted according to the available digits.
    const maxDigits = Math.max(decDigits, sciDigits);
    if (maxDigits <= 0) { return undefined; }

    // Generate a rounded string representation of the mantissa with the significant digits.
    const digitsResult = getSignificantDigits(mant, expn, maxDigits);
    expn = digitsResult.expn;

    // Check if the decimal notation can be used to format the number. It will be preferred, if it can take all
    // significant digits, or if it can take at least as many digits as the scientific notation.
    const useDecFormat = (decDigits >= sciDigits) || (digitsResult.digits.length <= decDigits);

    // Split the significant digits into integer and fraction part, fill missing zero digits, and concatenate the parts
    // with the decimal separator. In scientific notation, always format mantissa with single-digit integer part.
    const numParts = formatNumberParts(digitsResult.digits, useDecFormat ? expn : 0);
    const numStr = `${numParts.int || 0}${numParts.frac ? (dec + numParts.frac) : ""}`;

    // Create the decimal notation for the number without processing the exponent.
    if (useDecFormat) { return assembleNumber(state, numStr); }

    // Generate the string representation of the absolute value of the exponent, and update the required length of the
    // exponent. The exponent length may increase e.g. when rounding the number 9.9E+99 (two digits for the exponent)
    // to 1E+100 (three digits for the exponent).
    const expStr = String(Math.abs(expn));
    const expLen = Math.max(minExpLen, expStr.length);

    // Create the scientific notation for the number.
    return assembleNumber(state, numStr, ((expn < 0) ? "-" : "+") + expStr.padStart(expLen, "0"));
}

/**
 * Auto-formats a number according to the passed settings including a maximum
 * pixel width.
 *
 * @param state
 *  The data structure for the number to be formatted.
 *
 * @returns
 *  A result object with the formatted number.
 */
function formatNumberToWidth(state: AutoFormatState): Opt<AssembledResult> {

    // shortcuts of state properties
    const { maxDecDigits, maxSciDigits, maxWidth, renderFont } = state;

    // Fast exit, if the number is exactly or very close to zero (do not try to format denormalized numbers).
    if (math.isZero(state.mant) || !renderFont) { return assembleNumber(state, "0"); }

    // Get font measures for digits and other characters.
    const digitData = renderFont.getDigitMeasures();
    const minDigitWidth = Math.max(1, digitData.minWidth);
    const maxDigitWidth = Math.max(1, digitData.maxWidth);
    const decSepWidth = renderFont.getTextWidth(state.dec);

    // Get the maximum pixel width available for the formatted absolute value.
    let maxAbsWidth = maxWidth;
    if (state.num < 0) { maxAbsWidth -= renderFont.getTextWidth("-"); }

    // Returns the number of digits fitting into the passed pixel width. Use the maximum width of a digit to determine
    // the available number of digits to be sure to not exceed the total pixel width.
    //
    function getDigitCount(availWidth: number, maxDigits: number): number {
        return Math.min(maxDigits, Math.floor(availWidth / maxDigitWidth));
    }

    // Make a first guess about how many significant digits may fit for decimal notation, according to the maximum
    // pixel width. If the exponent is negative, the appropriate number of leading zero digits needs to be removed from
    // the available pixel width. If the number of digits indicates that the decimal separator needs to be added,
    // reduce the available space by its width, and recalculate the number of available digits.
    //
    const availDecWidth = maxAbsWidth + Math.min(0, state.expn) * Math.max(1, digitData.widths[0]);
    state.maxDecDigits = getDigitCount(availDecWidth, maxDecDigits);
    if (state.expn + 1 < state.maxDecDigits) {
        state.maxDecDigits = getDigitCount(availDecWidth - decSepWidth, maxDecDigits);
    } else if (state.maxDecDigits <= state.expn) {
        state.maxDecDigits -= state.expn;
    }

    // Make a first guess about how many significant digits may fit for scientific notation, according to the maximum
    // pixel width. Remove the complete exponent from the available pixel width. If the number of digits is greater
    // than one, the decimal separator needs to be added. Reduce the available space by its width in this case, and
    // recalculate the number of available digits.
    //
    const availSciWidth = maxAbsWidth - renderFont.getTextWidth(`${state.expSign}${(state.expn < 0) ? "-" : "+"}`) - (state.expLen * maxDigitWidth);
    state.maxSciDigits = getDigitCount(availSciWidth, maxSciDigits);
    if (state.maxSciDigits > 1) {
        state.maxSciDigits = getDigitCount(availSciWidth - decSepWidth, maxSciDigits);
    }

    // Try to format the number with the (possibly reduced) digit counts calculated for the pixel width. If the number
    // cannot be formatted into the available width (using the maximum digit width), or if the widths of all digits of
    // the used font are equal (as is the case for most fonts), do not try further.
    //
    let result = formatNumberToLength(state);
    if (!result || (minDigitWidth === maxDigitWidth)) { return result; }

    // If the number has been formatted successfully, and there is still enough room for another digit, and the number
    // of available digits has been reduced before due to the pixel width, try again with one more digit per step until
    // the resulting text does not fit anymore, or no more digits can be added.
    //
    while (maxWidth - result.width >= minDigitWidth) {

        // Increase the appropriate digit count of decimal or scientific notation according to the current result (at
        // least by one, at most by the number of the widest digit that can fit into the remaining space, to find the
        // largest possible representation of the number). Exit the loop, if the number cannot be enlarged anymore due
        // to the maximum string length.
        //
        const incDigits = Math.max(1, Math.floor((maxWidth - result.width) / maxDigitWidth));
        if (result.scientific) {
            if (state.maxSciDigits === maxSciDigits) { break; }
            state.maxSciDigits = Math.min(maxSciDigits, state.maxSciDigits + incDigits);
        } else {
            if (state.maxDecDigits === maxDecDigits) { break; }
            state.maxDecDigits = Math.min(maxDecDigits, state.maxDecDigits + incDigits);
        }

        // Format the number again with the new settings. Check the pixel width of the new formatted version of the
        // number. If the new number still fits into the width, store the new better result, and continue searching for
        // a better version.
        //
        const newResult = formatNumberToLength(state);
        if (!newResult || (maxWidth < newResult.width)) { break; }
        result = newResult;
    }

    return result;
}

// public functions ===========================================================

/**
 * Calculates the significant digits contained in the specified number, rounded
 * to a specific precision.
 *
 * @param mant
 *  The mantissa of the number.
 *
 * @param expn
 *  The exponent of the number.
 *
 * @param precision
 *  The maximum number of significant digits to be returned. For example, a
 *  precision of `4` would round the mantissa to three fractional digits (one
 *  significant digit is always used by the integral part of the mantissa).
 *
 * @returns
 *  A result descriptor containing the significant digits as string (with a
 *  maximum length as passed in `precision`), and the resulting rounded
 *  mantissa and exponent. The original exponent may have been increased by
 *  one, for example when rounding the number 99.99 (mantissa 9.999, exponent
 *  1) to less than 4 significant digits (new mantissa 1, new exponent 2).
 */
export function getSignificantDigits(mant: number, expn: number, precision: number): SignificantDigitsResult {

    // Precision of -1 and less (round to multiples of 100 and more) always result in zero for all mantissas in the
    // range [1,10). `Number.toFixed()` cannot be used for this.
    //
    if (precision < 0) {
        return { mant: 0, expn: -Infinity, digits: "" };
    }

    // Increase the mantissa by a very little correction value.
    //
    // This correction is needed to workaround internal rounding issues of the native function `Number.toFixed`. The
    // correction value depends on the mantissa itself. It needs to be kept below the internal resolution of
    // double-precision numbers to prevent visible modification of the number. In the range from 1 to 2, the internal
    // resolution is 2^-52 (`Number.EPSILON`). In the range from 2 to 4, the resolution is `2*EPSILON`; in the range
    // from 4 to 8, the resolution is `4*EPSILON`; and in the range from 8 to 10, it is `8*EPSILON`.
    //
    // Example: Calling `(1.2345).toFixed(3)` directly would result in "1.234", therefore adding an epsilon value is
    // needed to get the correct result "1.235".
    //
    mant += (mant < 2) ? EPSILON_1 : (mant < 4) ? EPSILON_2 : (mant < 8) ? EPSILON_4 : EPSILON_8;

    // Precision of zero (round to multiples of 10) may result in 0 or 10. `Number.toFixed()` cannot be used for this.
    //
    if (precision === 0) {
        return (mant >= 5) ? { mant: 1, expn: expn + 1, digits: "1" } : { mant: 0, expn: -Infinity, digits: "" };
    }

    // Generate a rounded string representation of the mantissa to detect the number of significant digits.
    //
    // The precision value passed to `Number.toFixed()` needs to be restricted to 14 in total (one integer digit, and
    // 13 remaining fractional digits) to prevent further rounding errors near the precision limit of IEEE 754
    // double-precision numbers. In the end, this results in a string looking like "n.nnn000".
    //
    const digits = mant.toFixed(Math.min(13, precision - 1));

    // If the original mantissa is very close to 10, the resulting string may become "10.00" due to rounding of the
    // mantissa. In this case, the exponent of the resulting formatted number needs to be increased by 1.
    //
    // Example: The number 9999.99 (mantissa is 9.99999, exponent is 3), rounded to two significant digits, will result
    // in the string "10.0". Therefore, the new mantissa becomes 1, and the exponent becomes 4; the number will be
    // shown as "10000" or "1E+04".
    //
    if ((mant >= 5) && digits.startsWith("1")) {
        return { mant: 1, expn: expn + 1, digits: "1" };
    }

    // Otherwise, simply remove the decimal point, and the trailing insignificant zero digits from the string, to
    // convert the string "n.nnn000" to "nnnn" where neither the first nor the last digit is a zero.
    //
    return { mant: parseFloat(digits), expn, digits: digits.replace(".", "").replace(/0+$/, "") };
}

/**
 * Splits the passed significant digits string into the integer and fraction
 * part of the original number.
 *
 * @param digits
 *  The significant digits string (as returned e.g. from the public function
 *  `getSignificantDigits`).
 *
 * @param expn
 *  The exponent of the original number whose significant digits have been
 *  passed in `digits`.
 *
 * @returns
 *  A result object with the integer part and the fraction part to be used to
 *  format the number.
 */
export function formatNumberParts(digits: string, expn: number): DigitPartsResult {

    // special case for zero (negative infinite exponent)
    if (!Number.isFinite(expn)) { return { int: "", frac: "" }; }

    // the number of significant digits
    const len = digits.length;

    // Add trailing zeros to integral numbers. Trailing zeros are needed, if the number of significant digits is less
    // than or equal to the original (positive) exponent. Example: The original number 123400 (mantissa 1.234, exponent
    // is 5) results in the significant digits "1234" which is shorter than the exponent. The resulting formatted
    // number will contain 6 digits (one more than the exponent), therefore two zero digits need to be appended. This
    // includes the "special case" that no zeros will be added (the significant digits exactly represent the original
    // number).
    //
    if (len <= expn + 1) {
        return { int: digits.padEnd(expn + 1, "0"), frac: "" };
    }

    // Split inside the significant digits, if the exponent is not negative. Example: For the number 12.34 (exponent is
    // 1), the decimal separator needs to be inserted into the significant digits string "1234".
    //
    if (expn >= 0) {
        return { int: digits.slice(0, expn + 1), frac: digits.slice(expn + 1) };
    }

    // Exponent is negative: Integer part is empty (not "0", e.g. format code "#.###" leaves the integer part empty),
    // and zero characters need to be added to the fractional part before the significant digits (count is one less
    // than the absolute value of the exponent). Example: For the number 0.001234 (exponent is -3), two zeros need to
    // be inserted before the significant digits "1234".
    //
    return { int: "", frac: digits.padStart(len - expn - 1, "0") };
}

/**
 * Returns the display string of the passed number, auto-formatted to fit into
 * the specified text length or pixel width. Very large numbers, or numbers
 * very close to zero will be formatted with scientific notation if possible.
 *
 * @param num
 *  The number to be auto-formatted.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A result object with the formatted number with correct sign, and the pixel
 *  width.
 */
export function autoFormatNumber(num: number, options?: AutoFormatOptions): AutoFormatResult {

    // build a descriptor for the passed number
    const state = analyseNumber(num, options);

    // format the number according to the settings passed to this method
    const result = (state.renderFont && Number.isFinite(state.maxWidth)) ? formatNumberToWidth(state) : formatNumberToLength(state);
    return { text: result?.text ?? null, width: result?.width ?? 0 };
}
