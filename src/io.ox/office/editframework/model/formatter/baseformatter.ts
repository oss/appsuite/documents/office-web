/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, str, map, dict } from "@/io.ox/office/tk/algorithms";
import type { Font } from "@/io.ox/office/tk/dom";
import { MSEC_PER_DAY, getLocalNowAsUTC } from "@/io.ox/office/tk/utils/dateutils";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { EObject } from "@/io.ox/office/tk/objects";

import { FileFormatType } from "@/io.ox/office/baseframework/utils/apputils";
import type { DisplayText, AutoFormatOptions, AutoFormatResult } from "@/io.ox/office/editframework/model/formatter/autoformat";
import { STD_LENGTH_AUTOFORMAT, autoFormatNumber } from "@/io.ox/office/editframework/model/formatter/autoformat";
import { generateCurrencyCode, yieldFixedCurrencyData } from "@/io.ox/office/editframework/model/formatter/currencycodes";
import { createListPattern } from "@/io.ox/office/editframework/model/formatter/parse/parserutils";
import { NumberParser } from "@/io.ox/office/editframework/model/formatter/parse/numberparser";
import { type ParseDateOptions, type ParseDateResult, DateParser } from "@/io.ox/office/editframework/model/formatter/parse/dateparser";
import { type ParseTimeResult, TimeParser } from "@/io.ox/office/editframework/model/formatter/parse/timeparser";
import type { RedCodeOptions, DecimalCodeOptions, AccountingCodeOptions, CurrencyCodeOptions, PercentCodeOptions, ScientificCodeOptions, TimeCodeOptions } from "@/io.ox/office/editframework/model/formatter/presetformattable";
import { PresetCodeId, PRESET_ID_USER_START, PresetFormatTable, getDecimalId, getTimeId } from "@/io.ox/office/editframework/model/formatter/presetformattable";
import type { SectionColorInfo } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { FormatCategory, XLocaleCode } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { ParsedFormat } from "@/io.ox/office/editframework/model/formatter/parsedformat";
import type { ParseFormatOptions } from "@/io.ox/office/editframework/model/formatter/codeparser";
import { OP_STD_TOKEN, SYSDATE_LOCALE_TAG, SYSTIME_LOCALE_TAG, FormatCodeParser } from "@/io.ox/office/editframework/model/formatter/codeparser";
import { type FormatPortion, FormatterEngine } from "@/io.ox/office/editframework/model/formatter/formatterengine";

// re-exports =================================================================

export type { DisplayText, AccountingCodeOptions, CurrencyCodeOptions, ParseFormatOptions };
export { PresetCodeId, FormatCategory, PRESET_ID_USER_START, OP_STD_TOKEN, SYSDATE_LOCALE_TAG, SYSTIME_LOCALE_TAG, ParsedFormat };

// types ======================================================================

/**
 * All types that can be used to specify a format code. Can either be an
 * instance of `ParsedFormat`, or a format code as string that will be parsed
 * to a `FormatCode` automatically, or the index of a preset code that will be
 * resolved via the `PresetFormatTable` of the number formatter, or nullish to
 * use the standard format code.
 */
export type FormatCodeSpec = Nullable<ParsedFormat | string | number>;

/**
 * Configuration options for a `BaseFormatter` instance.
 */
export interface BaseFormatterConfig {

    /**
     * The maximum number of characters to be occupied by a STD token (the
     * absolute part of a number, including the decimal separator and the
     * complete exponent in scientific notation, but without a leading minus
     * sign). MUST be a positive integer. Default value is `11`.
     */
    stdLen?: number;

    /**
     * The "null date" used by this formatter (i.e. the date that will be
     * formatted for the serial number 0), as UTC date object, or as number of
     * milliseconds as returned by `Date.UTC()`. By default, the date
     * `1899-Nov-30` will be used as null date.
     */
    nullDate?: Date | number;
}

/**
 * Type mapping for the events emitted by `BaseFormatter` instances.
 */
export interface BaseFormatterEventMap {

    /**
     * Will be emitted after the UI locale was changed, and the document model
     * has updated itself.
     */
    "change:presets": [];
}

/**
 * Optional settings for creating format codes for decimal numbers.
 */
export interface DecimalFormatOptions extends DecimalCodeOptions, RedCodeOptions { }

/**
 * Optional settings for creating format codes for percentage notation.
 */
export interface PercentFormatOptions extends PercentCodeOptions, RedCodeOptions { }

/**
 * Optional settings for creating format codes for scientific notation.
 */
export interface ScientificFormatOptions extends ScientificCodeOptions, RedCodeOptions { }

/**
 * Optional settings for creating format codes for fractions.
 */
export interface FractionFormatOptions {

    /**
     * Whether to create a format code with a fixed-value denominator. Default
     * value is `false`.
     */
    fixedDenom?: boolean;

    /**
     * The size or value of the denominator. If option `fixedDenom` is `false`,
     * this is the maximum number of digits in the denominator (default value
     * will be `1`).  If option `fixedDenom` is set to `true`, this is the
     * fixed value the denominator (default value will be `10`).
     */
    denomSize?: number;

    /**
     * Whether to create a format code without integer part. Default value is
     * `false`.
     */
    shortFrac?: boolean;
}

/**
 * Optional settings for creating date format codes.
 */
export interface SystemDateFormatOptions {

    /**
     * Whether to return the long date format (with day and month names).
     * Default value is `false` (day and month as numbers).
     */
    longDate?: boolean;
}

/**
 * Optional settings for creating time format codes.
 */
export interface SystemTimeFormatOptions {

    /**
     * Whether to return the short time format (without seconds). Default value
     * is `false`.
     */
    shortTime?: boolean;
}

/**
 * Optional settings for creating format codes for combined date and time.
 */
export interface SystemDateTimeFormatOptions extends SystemDateFormatOptions, SystemTimeFormatOptions {

    /**
     * Whether to return a number format without date part (time only). Default
     * value is `false`.
     */
    hideDate?: boolean;

    /**
     * Whether to return a number format without time part (date only). Default
     * value is `false`.
     */
    hideTime?: boolean;
}

/**
 * Optional settings for fixed date format codes.
 */
export interface FixedDateFormatOptions {

    /**
     * Whether to remove the day token from the format code. Default value is
     * `false`.
     */
    hideDay?: boolean;

    /**
     * Whether to use the long month name in the format code. Default value is
     * `false` (short abbreviated month name).
     */
    longMonth?: boolean;

    /**
     * Whether to remove the year token from the format code. Default value is
     * `false`.
     */
    hideYear?: boolean;

    /**
     * Whether to use the short year (with 2 digits) in the format code.
     * Default value is `false` (full year with 4 digits). This option will not
     * have any effect, if `hideYear` is set.
     */
    shortYear?: boolean;
}

/**
 * Optional settings for fixed date/time format codes.
 */
export interface FixedDateTimeFormatOptions extends FixedDateFormatOptions, SystemTimeFormatOptions { }

/**
 * Optional parameters for formatting a value with a given format code.
 */
export interface FormatValueOptions extends ParseFormatOptions {

    /**
     * The maximum number of characters to be occupied by a STD token (the
     * absolute part of a number, including the decimal separator and the
     * complete exponent in scientific notation, but without a leading minus
     * sign). MUST be a positive integer. Default value is the configuration of
     * the number formatter instance.
     */
    stdLen?: number;

    /**
     * The maximum number of characters allowed for the formatted value. MUST
     * be a positive integer. If omitted, the length of the formatted value
     * will not be restricted (but option `maxWidth` will still apply).
     */
    maxLen?: number;

    /**
     * The maximum width of the formatted number, in pixels. If omitted, the
     * width of the formatted value will not be restricted (but option `stdLen`
     * will still apply).
     */
    maxWidth?: number;

    /**
     * The font settings to be used for the option `maxWidth`. If omitted, the
     * option `maxWidth` will be ignored.
     */
    renderFont?: Font;

    /**
     * Specifies whether string values are allowed to exceed the space limits
     * set with the options `maxLen` or `maxWidth`. However, if the format code
     * contains a FILL token, the resulting formatted text will never exceed
     * the specified limits. Default value is `false`.
     */
    exceedText?: boolean;

    /**
     * The separator character to be used to format date value. By default, the
     * date seperator of the current UI locale settings will be used.
     */
    dateSep?: string;

    /**
     * The separator character to be used to format time value. By default, the
     * time seperator of the current UI locale settings will be used.
     */
    timeSep?: string;
}

/**
 * Result object returned when formatting a value.
 */
export interface FormatValueResult {

    /**
     * The formatted value; or `null`, if the used format code is invalid, or
     * if the value cannot be formatted with the format code (e.g., a number is
     * too large to be formatted as date/time).
     */
    text: DisplayText;

    /**
     * Data about the text color to be used to format a value. If omitted, the
     * original text color of the value will not be overridden.
     */
    color?: SectionColorInfo;

    /**
     * The effective width of the formatted value, in pixels. Will be zero, if
     * no rendering font was available.
     */
    width: number;

    /**
     * The text portions to be rendered with a pixel-exact offset, taking the
     * exact width of blind text into account.
     */
    portions: readonly FormatPortion[];
}

/**
 * The result of preparing the raw format value passed to the method
 * `BaseFormatter.formatValue()`. Subclasses may implement conversion logic to
 * support additional input data types besides numbers and strings.
 */
export interface PreparedFormatValue {

    /**
     * The converted input value. Numbers and strings will be formatted
     * regularly, the value `null`will always result in an empty string, and
     * all other data types will result in a format error.
     */
    value: unknown;

    /**
     * Custom format code to be used to format the converted value. If omitted,
     * the original format code passed to the method `formatValue()` will be
     * used.
     */
    format?: ParsedFormat;
}

/**
 * A descriptor with the value, and a preset format identifier, representing a
 * parsed text.
 */
export interface ParseValueResult {

    /**
     * The resulting value with correct data type.
     */
    value: number | string;

    /**
     * The identifier of the best matching preset format code, or an explicit
     * format code as string, if the passed text has been converted to a
     * floating-point number in a specific significant format; or 0 (standard
     * number format) for simple floating-point numbers, or any other value
     * type. May not fit exactly to the text, e.g. if the text represents a
     * number in scientific notation, the identifier of the standard format
     * code for scientific numbers "0.00E+00" will always be returned.
     */
    format: string | number;
}

// constants ==================================================================

/**
 * The identifiers of locale-dependent currency and accounting formats that
 * need to be written into the document.
 */
export const CURRENCY_FORMAT_IDS = [5, 6, 7, 8, 37, 38, 39, 40, 41, 42, 43, 44];

// private constants ----------------------------------------------------------

// null date for 1900 date system
const NULL_DATE_1900 = Date.UTC(1899, 11, 30);

// cache for regular expressions for fixed currency symbols
const fixedSymbolsRegExpCache = new Map<string, [RegExp, Map<string, string>]>();

// class BaseFormatter ========================================================

/**
 * A formatter that converts numbers and other values to strings, according to
 * specific format codes and the current locale of the user interface.
 */
export class BaseFormatter extends EObject<BaseFormatterEventMap> {

    // properties -------------------------------------------------------------

    /**
     * The file format type of the edited document.
     */
    readonly fileFormat: FileFormatType;

    /**
     * The predefined format codes of the current UI locale.
     */
    readonly presetTable: PresetFormatTable;

    /**
     * The parsed number format descriptor for the standard number format.
     */
    readonly standardFormat: ParsedFormat;

    /**
     * Whether to implement the faulty behavior that 1900 was a yeap year for
     * compatibility with other spreadsheet applications, i.e. whether to treat
     * 1900-02-29 as a valid date (OOXML).
     */
    readonly leapYearBug: boolean;

    /**
     * Whether to support negative date values (ODF, for example, the number
     * `-1` results in the day before the null date). If set to `false`,
     * negative numbers cannot be converted to dates, and dates before the null
     * date cannot be converted to numbers (OOXML).
     */
    readonly negativeDates: boolean;

    /**
     * Whether to support special LCIDs for dynamic system date and time
     * formats (OOXML).
     */
    readonly #sysDateTime: boolean;

    /**
     * Maximum valid year for date formatting.
     */
    readonly #maxValidYear: number;

    /**
     * Number of characters for a standard format token. Can be changed at
     * runtime.
     */
    #stdLen = STD_LENGTH_AUTOFORMAT;

    /**
     * Null date as timestamp (corresponding to serial number zero). Can be
     * changed at runtime.
     */
    #nullDate = NULL_DATE_1900;

    // constructor ------------------------------------------------------------

    /**
     * @param fileFormat
     *  Identifier of the file format to be used.
     *
     * @param [config]
     *  Initial configuration options.
     */
    constructor(fileFormat: FileFormatType, config?: BaseFormatterConfig) {
        super();

        // public properties
        this.fileFormat = fileFormat;
        this.standardFormat = FormatCodeParser.get().parse(fileFormat, OP_STD_TOKEN);
        this.presetTable = this.member(new PresetFormatTable(LOCALE_DATA));
        this.leapYearBug = fileFormat === FileFormatType.OOX;
        this.negativeDates = fileFormat === FileFormatType.ODF;

        // private properties
        this.#sysDateTime = fileFormat === FileFormatType.OOX;
        this.#maxValidYear = (fileFormat === FileFormatType.OOX) ? 9999 : 32767;

        // apply passed configuration
        if (config) { this.configure(config); }

        // update preset number formats on changed UI locale
        this.listenToGlobal("change:locale", () => {
            this.presetTable.initialize(LOCALE_DATA);
            this.trigger("change:presets");
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Changes the configuration of this formatter instance.
     *
     * @param config
     *  All configuration properties to be changed.
     */
    configure(config: BaseFormatterConfig): void {

        // number of characters for the standard format token
        if (is.number(config.stdLen)) {
            this.#stdLen = Math.max(config.stdLen, 5);
        }

        // change the null date
        if (is.number(config.nullDate)) {
            this.#nullDate = config.nullDate;
        } else if (config.nullDate instanceof Date) {
            this.#nullDate = config.nullDate.getTime();
        }
    }

    /**
     * Returns whether the 1900 date system is used by this formatter.
     *
     * @returns
     *  Whether the 1900 date system is used by this formatter.
     */
    hasNullDate1900(): boolean {
        return this.#nullDate === NULL_DATE_1900;
    }

    /**
     * Returns whether the passed date object is valid to be processed by this
     * formatter. The year must not be negative, must not be greater than the
     * maximum allowed year depending on the file format.
     *
     * @param date
     *  The UTC date to be checked.
     *
     * @returns
     *  Whether the passed date object is valid.
     */
    isValidDate(date: Date): boolean {

        // milliseconds from native null date (1970-01-01)
        const timestamp = date.getTime();
        // the full (4-digit) year
        const year = date.getUTCFullYear();

        // check the year, check for negative dates
        return Number.isFinite(timestamp) && (this.negativeDates || (this.#nullDate <= timestamp)) && (year >= 0) && (year <= this.#maxValidYear);
    }

    /**
     * Returns a date object representing the passed floating-point number,
     * using the current null date of this formatter.
     *
     * @param num
     *  A floating-point number. The integral part represents the number of
     *  days elapsed since the null date of this formatter, the fractional part
     *  represents the time.
     *
     * @returns
     *  An UTC date object representing the passed number; or `null`, if the
     *  passed number cannot be converted to a valid date (e.g. too large).
     */
    convertNumberToDate(num: number): Date | null {

        // get correct date/time for current null date
        const date = new Date(this.#nullDate + Math.round(num * MSEC_PER_DAY));

        // return the date object if it is valid
        return this.isValidDate(date) ? date : null;
    }

    /**
     * Returns the floating-point number representing the passed date, using
     * the current null date of this formatter.
     *
     * @param date
     *  An UTC date object.
     *
     * @returns
     *  The floating-point number representing the passed date; or null, if
     *  the passed date cannot be converted to a number.
     */
    convertDateToNumber(date: Date): number | null {

        // error, if date is invalid, or the year is not valid
        if (!this.isValidDate(date)) { return null; }

        // return the floating-point number
        return (date.getTime() - this.#nullDate) / MSEC_PER_DAY;
    }

    /**
     * Returns the floating-point number representing the current local date
     * and time, using the current null date of this formatter.
     *
     * @returns
     *  The floating-point number representing the current local date and time.
     */
    convertNowToNumber(): number {
        return this.convertDateToNumber(getLocalNowAsUTC())!;
    }

    /**
     * Returns the display string of the passed number, auto-formatted to fit
     * into the specified text length or pixel width. Very large numbers, or
     * numbers very close to zero will be formatted with scientific notation if
     * possible.
     *
     * @param num
     *  The number to be auto-formatted.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A result object with the formatted number with correct sign, and the
     *  pixel width.
     */
    autoFormatNumber(num: number, options?: AutoFormatOptions): AutoFormatResult {
        return autoFormatNumber(num, { stdLen: this.#stdLen, ...options });
    }

    /**
     * Returns the parsed format for the specified format code.
     *
     * @param formatSpec
     *  A parsed format code, or a format code string, or the identifier of a
     *  preset code.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The parsed format of the specified format code.
     */
    getParsedFormat(formatSpec: FormatCodeSpec, options?: ParseFormatOptions): ParsedFormat {

        // immediately return parsed format codes
        if (formatSpec instanceof ParsedFormat) { return formatSpec; }

        // resolve preset codes from table
        const formatCode = is.number(formatSpec) ? this.presetTable.getFormatCode(formatSpec) : formatSpec;

        // parse the format code
        return is.string(formatCode) ? FormatCodeParser.get(options).parse(this.fileFormat, formatCode) : this.standardFormat;
    }

    /**
     * Returns the decimal format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The decimal format code for the passed settings.
     */
    getDecimalFormat(options?: DecimalFormatOptions): ParsedFormat {
        return this.#getRedFormat(this.presetTable.getDecimalCode(options), options);
    }

    /**
     * Returns the currency format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The currency format code for the passed settings.
     */
    getCurrencyFormat(options?: CurrencyCodeOptions): ParsedFormat {
        return this.getParsedFormat(this.presetTable.getCurrencyCode(options));
    }

    /**
     * Returns the accounting format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The accounting format code for the passed settings.
     */
    getAccountingFormat(options?: AccountingCodeOptions): ParsedFormat {
        return this.getParsedFormat(this.presetTable.getAccountingCode(options));
    }

    /**
     * Returns the percentage format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The percentage format code for the passed settings.
     */
    getPercentFormat(options?: PercentFormatOptions): ParsedFormat {
        return this.#getRedFormat(this.presetTable.getPercentCode(options), options);
    }

    /**
     * Returns the scientific format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The scientific format code for the passed settings.
     */
    getScientificFormat(options?: ScientificFormatOptions): ParsedFormat {
        return this.#getRedFormat(this.presetTable.getScientificCode(options), options);
    }

    /**
     * Returns the fraction format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The fraction format code for the passed settings.
     */
    getFractionFormat(options?: FractionFormatOptions): ParsedFormat {
        let formatCode = options?.shortFrac ? "" : "# ";
        if (options?.fixedDenom) {
            const denomValue = options.denomSize ?? 10;
            formatCode += `${"?".repeat(String(denomValue - 1).length)}/${denomValue}`;
        } else {
            const pattern = "?".repeat(options?.denomSize ?? 1);
            formatCode += `${pattern}/${pattern}`;
        }
        return this.getParsedFormat(formatCode);
    }

    /**
     * Returns the default date format of the UI locale.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The default date format.
     */
    getSystemDateFormat(options?: SystemDateFormatOptions): ParsedFormat {
        // DOCS-2623: no support of special LCIDs for system date and time in ODF
        return this.getParsedFormat(!options?.longDate ? LOCALE_DATA.shortDate :
            this.#sysDateTime ? `${SYSDATE_LOCALE_TAG}${LOCALE_DATA.longDate}` : LOCALE_DATA.longDate
        );
    }

    /**
     * Returns the default time format of the UI locale.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The default time format.
     */
    getSystemTimeFormat(options?: SystemTimeFormatOptions): ParsedFormat {
        // DOCS-2623: no support of special LCIDs for system date and time in ODF
        return this.getParsedFormat(options?.shortTime ? LOCALE_DATA.shortTime :
            this.#sysDateTime ? `${SYSTIME_LOCALE_TAG}${LOCALE_DATA.longTime}` : LOCALE_DATA.longTime
        );
    }

    /**
     * Returns the default combined date/time format of the UI locale.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The default combined date/time format.
     */
    getSystemDateTimeFormat(options?: SystemDateTimeFormatOptions): ParsedFormat {
        const dateCode = options?.hideDate ? "" : options?.longDate ? LOCALE_DATA.longDate : LOCALE_DATA.shortDate;
        const timeCode = options?.hideTime ? "" : options?.shortTime ? LOCALE_DATA.shortTime : LOCALE_DATA.longTime;
        return this.getParsedFormat(str.concatTokens(dateCode, timeCode));
    }

    /**
     * Returns the date format code for the passed settings, based on the fixed
     * date format of the UI locale (in D-M-Y order).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The date format code for the passed settings.
     */
    getFixedDateFormat(options?: FixedDateFormatOptions): ParsedFormat {
        let formatCode = this.presetTable.getFormatCode(PresetCodeId.FIXED_DATE);
        if (options?.hideDay) { formatCode = formatCode.replace(/^D+[^M]+/, ""); }
        formatCode = formatCode.replace(/M+/, options?.longMonth ? "MMMM" : "MMM");
        formatCode = options?.hideYear ? formatCode.replace(/[^M]+Y+$/, "") : formatCode.replace(/Y+/, options?.shortYear ? "YY" : "YYYY");
        return this.getParsedFormat(formatCode);
    }

    /**
     * Returns the time format code for the passed settings.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The time format code for the passed settings.
     */
    getTimeFormat(options?: TimeCodeOptions): ParsedFormat {
        return this.getParsedFormat(this.presetTable.getTimeCode(options));
    }

    /**
     * Returns the combined date/time format code for the passed settings,
     * based on the fixed date format of the UI locale (in D-M-Y order).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The combined date/time format code for the passed settings.
     */
    getFixedDateTimeFormat(options?: FixedDateTimeFormatOptions): ParsedFormat {
        const dateCode = this.getFixedDateFormat(options).formatCode;
        const timeCode = options?.shortTime ? LOCALE_DATA.shortTime : LOCALE_DATA.longTime;
        return this.getParsedFormat(str.concatTokens(dateCode, timeCode));
    }

    /**
     * Returns the default format code for the specified format category.
     *
     * @param category
     *  The identifier of a number format category.
     *
     * @returns
     *  The parsed format code for the specified number format category.
     */
    getCategoryFormat(category: FormatCategory): ParsedFormat {
        switch (category) {
            case FormatCategory.NUMBER:     return this.getParsedFormat(PresetCodeId.NUMBER);
            case FormatCategory.SCIENTIFIC: return this.getParsedFormat(PresetCodeId.SCIENTIFIC);
            case FormatCategory.PERCENT:    return this.getParsedFormat(PresetCodeId.PERCENT);
            case FormatCategory.FRACTION:   return this.getParsedFormat(PresetCodeId.FRACTION);
            case FormatCategory.CURRENCY:   return this.getCurrencyFormat();
            case FormatCategory.ACCOUNTING: return this.getAccountingFormat();
            case FormatCategory.DATE:       return this.getSystemDateFormat();
            case FormatCategory.TIME:       return this.getSystemTimeFormat();
            case FormatCategory.DATETIME:   return this.getSystemDateTimeFormat();
            case FormatCategory.TEXT:       return this.getParsedFormat(PresetCodeId.TEXT);
            default:                        return this.standardFormat;
        }
    }

    /**
     * Returns the format codes of all locale-dependent currency and accounting
     * number formats. These format codes will explicitly be injected into a
     * new document, and will persist when changing the UI locale.
     *
     * @returns
     *  The format codes of all locale-dependent currency and accounting number
     *  formats, mapped by the integer format identifiers.
     */
    getInitialFormatCodes(): Dict<string> {
        const initialFormats = dict.create<string>();
        CURRENCY_FORMAT_IDS.forEach(formatId => {
            initialFormats[formatId] = this.presetTable.getFormatCode(formatId)!;
        });
        return initialFormats;
    }

    /**
     * Converts the passed format code specifier to the identifier of an
     * existing preset format code.
     *
     * @param formatSpec
     *  A parsed format code, or a format code string, or the identifier of a
     *  preset code.
     *
     * @returns
     *  The identifier of an existing preset code matching the passed format
     *  code specifier; otherwise `null`.
     */
    resolveFormatId(formatSpec: FormatCodeSpec): number | null {

        // immediately return preset codes
        if (is.number(formatSpec)) { return formatSpec; }

        // resolve format code from parsed formats
        const formatCode = (formatSpec instanceof ParsedFormat) ? formatSpec.formatCode : formatSpec;

        // resolve the preset code
        return is.string(formatCode) ? this.presetTable.getFormatId(formatCode) : PresetCodeId.STANDARD;
    }

    /**
     * Formats the passed value according to the format code.
     *
     * @param formatSpec
     *  The parsed format code, or a format code string, or the identifier of a
     *  preset code, that will be used to format the value to a string.
     *
     * @param value
     *  The value to be formatted. Numbers will be formatted according to the
     *  numeric sections of the passed format code, dates (instances of `Date`)
     *  will be converted with the method `convertDateToNumber()` first, and
     *  strings will be formatted according to the text section. All other data
     *  types result in a format error (result object's property `text` will be
     *  set to `null`). Subclasses may accept other value types by implementing
     *  the method `convertFormatValue()` of this class.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A result object containing the formatted value.
     */
    formatValue(formatSpec: FormatCodeSpec, value: unknown, options?: FormatValueOptions): FormatValueResult {
        return this.#formatValue(formatSpec, value, options) ?? { text: null, width: 0, portions: [] };
    }

    /**
     * Formats the current local date and time according to the specified
     * date/time format code.
     *
     * @param formatSpec
     *  The parsed format code, or a format code string, or the identifier of a
     *  preset code, that will be used to format the current local date and
     *  time.
     *
     * @returns
     *  The formatted current date and time.
     */
    formatNow(formatSpec: FormatCodeSpec): DisplayText {
        const result = this.#formatValue(formatSpec, getLocalNowAsUTC());
        return result ? result.text : null;
    }

    /**
     * Tries to parse a date value from the passed string.
     *
     * @param text
     *  The text potentially containing a formatted date value.
     *
     * @returns
     *  The resulting UTC date, if the passed string consist of a valid date;
     *  otherwise `undefined`.
     */
    parseFormattedDate(text: string): Opt<Date> {

        // parse the passed text with the internal configuration
        const result = DateParser.parseDate(text, { complete: true });

        // check the resulting date according to the internal configuration (null date etc.)
        return (result && this.isValidDate(result.date)) ? result.date : undefined;
    }

    /**
     * Tries to parse the passed text to a floating-point number. Supports
     * decimal notation, scientigic notation, fractional notation, embedded
     * percent sign and currency symbols, and various date and time patterns.
     *
     * @param text
     *  The string to be converted to a floating-point number.
     *
     * @returns
     *  A result descriptor with the resulting value, and a preset format code
     *  identifier representing the original formatted text passed to this
     *  method.
     */
    parseFormattedValue(text: string): ParseValueResult {

        // always ignore leading/trailing whitespace (U+0020 only by intention!)
        const trimmed = text.replace(/^ +| +$/, "");
        if (trimmed) {

            // try to parse floating-point numbers, optionally with percent sign or currency symbol
            const numberResult = this.#parseNumber(trimmed);
            if (numberResult) { return numberResult; }

            // try to parse date, time, or combined date/time or time/date
            const dateResult = this.#parseDateTime(trimmed);
            if (dateResult) { return dateResult; }
        }

        // everything else is a regular string
        return { value: text, format: PresetCodeId.STANDARD };
    }

    // protected methods ------------------------------------------------------

    /**
     * Converts a value to be formatted with the public method `formatValue`.
     * Intended to be overwritten in subclasses to enable other input data
     * types than numbers and strings.
     *
     * @param value
     *  The value to be converted.
     *
     * @returns
     *  A result object with the converted value, and an optional format code
     *  override.
     */
    protected prepareFormatValue(value: unknown): PreparedFormatValue {
        return { value };
    }

    // private methods --------------------------------------------------------

    /**
     * Converts the passed format code to a format code with red negative
     * section if specified.
     */
    #getRedFormat(formatCode: string | null, options?: RedCodeOptions): ParsedFormat {
        return formatCode ? this.getParsedFormat(options?.red ? `${formatCode};[Red]-${formatCode}` : formatCode) : this.standardFormat;
    }

    /**
     * Formats the passed value according to the format code.
     */
    #formatValue(formatSpec: FormatCodeSpec, value: unknown, options?: FormatValueOptions): Opt<FormatValueResult> {

        // convert the input value to the format value
        const { value: prepValue, format: prepFormat } = this.prepareFormatValue(value);

        // check the value to be formatted (must be number, Date, or string)
        let prepNum = is.number(prepValue) ? prepValue : (prepValue instanceof Date) ? this.convertDateToNumber(prepValue) : is.string(prepValue) ? 0 : null;
        if ((prepNum === null) || !Number.isFinite(prepNum)) { return; }

        // replace denormalized numbers with zero
        if (math.isZero(prepNum)) { prepNum = 0; }

        // resolve the parsed format code instance
        const parsedFormat = prepFormat ?? this.getParsedFormat(formatSpec, options);
        // the section of the format code to be used for the passed value
        let section = parsedFormat.getSection(is.string(prepValue) ? prepValue : prepNum);

        // dynamically resolve format codes for system long date, and system time, according to current UI language
        switch (section?.locale?.lc) {
            case XLocaleCode.SYSDATE: section = this.getParsedFormat(LOCALE_DATA.longDate).getSection(0); break;
            case XLocaleCode.SYSTIME: section = this.getParsedFormat(LOCALE_DATA.longTime).getSection(0); break;
            default:                  break;
        }

        // no valid section found: return with empty result
        if (!section) { return; }

        // use the `FormatterEngine` for format the passed value according to the section
        return FormatterEngine.format(this, section, prepValue, prepNum, { stdLen: this.#stdLen, ...options });
    }

    /**
     * Parses the particles of floating-point numbers or fractions in a string,
     * i.e. signs, parentheses, decimal numbers, scientific numbers, fractional
     * notation. Everything may be separated by whitespace characters, e.g.
     * "( 1 )". Extra tokens may be present next to the number, or even
     * inbetween (e.g. "-%1" or "($1)"), but only exactly one token is allowed
     * to be in the string (everything else is invalid, e.g. "1%%" or "$1%"
     * etc.).
     */
    #parseNumber(text: string): Opt<ParseValueResult> {

        let remaining = text;       // remaining text to be parsed
        let sign: Opt<string>;      // the parsed plus or minus sign
        let open = false;           // opening parenthesis was parsed
        let percent = false;        // percent sign was parsed
        let symbol: Opt<string>;    // the parsed currency symbol

        // removes the leading "len" characters from the remaining string, and following whitespace
        const sliceRemaining = (len: number): void => {
            remaining = remaining.slice(len).replace(/^ +/, "");
        };

        // regular expression for fixed currency symbols to be recognized
        const [fixedSymbolsRE, fixedSymbolsMap] = map.upsert(fixedSymbolsRegExpCache, LOCALE_DATA.intl, () => {
            const symbols: string[] = [];
            const reverseMap = new Map<string, string>();
            for (const [locale, currData] of yieldFixedCurrencyData()) {
                symbols.push(currData.symbol, currData.iso);
                reverseMap.set(currData.symbol, locale);
                reverseMap.set(currData.iso, locale);
            }
            const regExp = new RegExp("^" + createListPattern(symbols));
            return [regExp, reverseMap];
        });

        // tries to parse a percernt sign or a fixed currency symbol from the remaining string
        const consumeExtraToken = (): boolean => {
            // try percent sign
            if (remaining.startsWith("%")) {
                if (percent || symbol) { return false; }
                percent = true;
                sliceRemaining(1);
                return true;
            }
            // try one of the fixed currency symbols
            const match = fixedSymbolsRE.exec(remaining)?.[0];
            if (match) {
                if (percent || symbol) { return false; }
                symbol = match;
                sliceRemaining(match.length);
                return true;
            }
            return true;
        };

        // start with leading extra token, e.g. "%-1" or "$(1)"
        if (!consumeExtraToken()) { return; }

        // a plus or minus sign character (can only occur before the number)
        if (remaining.startsWith("+") || remaining.startsWith("-")) {
            sign = remaining[0];
            sliceRemaining(1);
        }

        // an opening parenthesis (only before the number, not together with sign)
        if (remaining.startsWith("(")) {
            if (sign) { return; }
            open = true;
            sliceRemaining(1);
        }

        // try extra token after sign or parenthesis, e.g. "-%1" or "($1)"
        if (!consumeExtraToken()) { return; }

        // unsigned floating-point number must follow (decimal, scientific, or fractional)
        const result = NumberParser.parseNumber(remaining, { groupSep: true });
        if (!result || result.sign) { return; }
        sliceRemaining(result.text.length);

        // try extra token after number, e.g. "-1%" or "(1$)"
        if (!consumeExtraToken()) { return; }

        // closing parenthesis must follow opening parenthesis
        if (open) {
            if (!remaining.startsWith(")")) { return; }
            sliceRemaining(1);

            // try extra token after closing parenthesis, e.g. "(1)$"
            if (!consumeExtraToken()) { return; }
        }

        // fail on trailing garbage
        if (remaining) { return; }

        // negate the parsed number
        const value = (open || (sign === "-")) ? -result.number : result.number;

        // whether to prefer number format with decimal digits (e.g. when ignoring fractional notation for currency)
        const preferDec = result.dec || result.scientific || result.fraction;

        // currency symbol wins over percent or fractional notation
        const locale = symbol && fixedSymbolsMap.get(symbol);
        if (locale) {
            const format = generateCurrencyCode({ locale, patternType: "currency", negativeRed: true, groupSep: true, fracDigits: preferDec ? undefined : 0 });
            return { value, format };
        }

        // percent wins over fractional notation
        if (percent) {
            const format = preferDec ? PresetCodeId.PERCENT : PresetCodeId.PERCENT_INT;
            return { value: value / 100, format };
        }

        // fractional notation
        if (result.fraction) {
            const format = (result.fraction[2] >= 10) ? PresetCodeId.FRACTION_LONG : PresetCodeId.FRACTION;
            return { value, format };
        }

        // finally, decimal or scientific notation
        const format = result.scientific ? PresetCodeId.SCIENTIFIC : result.grouped ? getDecimalId({ int: !result.dec, group: true }) : PresetCodeId.STANDARD;
        return { value, format };
    }

    /**
     * Tries to parse the passed text to a date or time value.
     */
    #parseDateTime(text: string): Opt<ParseValueResult> {

        // try to parse a leading date, time, or combined date/time or time/date
        let dateResult: Opt<ParseDateResult>;
        let timeResult: Opt<ParseTimeResult>;

        // parses a date from the passed string, and validates it according to the own configuration
        const parseDate = (remaining: string, options?: ParseDateOptions): Opt<ParseDateResult> => {
            const result = DateParser.parseDate(remaining, options);
            return (result && this.isValidDate(result.date)) ? result : undefined;
        };

        // try date or date/time (separated with whitespace); then time or time/date
        if ((dateResult = parseDate(text))) {
            // trailing text (if existing) must be a valid time, otherwise return immediately
            const remaining = dateResult.remaining.replace(/^ +/, "");
            if (remaining && !(timeResult = TimeParser.parseTime(remaining, { complete: true }))) {
                return;
            }
        } else if ((timeResult = TimeParser.parseTime(text))) {
            // trailing text (if existing) must be a valid date, otherwise return immediately
            const remaining = timeResult.remaining.replace(/^ +/, "");
            if (remaining && !(dateResult = parseDate(remaining, { complete: true }))) {
                return;
            }
        }

        // convert parsed date/time components to the serial number, and number format (always use the standard date/time format)
        if (dateResult && timeResult) {
            // Always use format code DATETIME_SHORT consisting of the standard date format, followed by an hour/minute time format.
            const value = this.convertDateToNumber(dateResult.date)! + timeResult.serial;
            return { value, format: PresetCodeId.DATETIME_SHORT };
        }

        // convert parsed date components (without time) to the serial number, and number format
        if (dateResult) {
            // Find an identifier of a built-in date format code:
            // - no year given (day/month only): format code DAY_MONTH consists of day and month (e.g. "DD/MM") matching the standard date format,
            // - no day given (month/year only): format code MONTH_YEAR consists of month and year (e.g. "MM/YYYY") matching the standard date format,
            // - otherwise (day, month, year): format code SYS_DATE is the standard date format.
            const format = !dateResult.hasYear ? PresetCodeId.DAY_MONTH : dateResult.hasDay ? PresetCodeId.SYS_DATE : PresetCodeId.MONTH_YEAR;
            return { value: this.convertDateToNumber(dateResult.date)!, format };
        }

        // convert parsed time components (without date) to the serial number, and number format
        if (timeResult) {
            // Find an identifier of a built-in time format code:
            // - milliseconds given: format code TIME_TENTH contains a minute/second time format with one decimal place for tenth of seconds,
            // - time is greater than or equal to 24 hours: format code TIME_ELAPSED contains total hour code, e.g. "[h]:mm:ss",
            // - otherwise, use one of the standard time formats.
            const format = timeResult.fraction ? PresetCodeId.TIME_TENTH :
                (timeResult.serial >= 1) ? PresetCodeId.TIME_ELAPSED :
                getTimeId({ hours24: !timeResult.hours12, seconds: timeResult.seconds });
            return { value: timeResult.serial, format };
        }

        // no date or time available
        return undefined;
    }
}
