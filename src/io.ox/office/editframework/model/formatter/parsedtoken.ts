/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { HourModeTokens } from "@/io.ox/office/tk/locale";

// types ======================================================================

/**
 * Specifies the type of a single token in a parsed format code.
 */
export enum TokenType {

    /** The standard format for numbers (decimal or scientific notation). */
    STD = 0,

    /** Digit pattern for the integral part of a decimal number. */
    INT = 1,

    /** Digit pattern for the fractional part of a decimal number. */
    FRAC = 2,

    /** The decimal separator. */
    DEC = 3,

    /** Digit pattern for the integral part of the exponent. */
    EINT = 4,

    /** Digit pattern for the fractional part of the exponent. */
    EFRAC = 5,

    /** Digit pattern for numerator in fractions. */
    NUMER = 6,

    /** Digit pattern for denominator in fractions. */
    DENOM = 7,

    /** The date separator. */
    DATESEP = 10,

    /** The time separator. */
    TIMESEP = 11,

    /** Date token for year. */
    YEAR = 20,

    /** Date token for Buddhist year. */
    BYEAR = 21,

    /** Date token for Gengo year. */
    GYEAR = 22,

    /** Date token for Gengo era name. */
    GERA = 23,

    /** Date token for quarter of the year. */
    QUARTER = 24,

    /** Date token for month. */
    MONTH = 25,

    /** Date token for week number. */
    WEEK = 26,

    /** Date token for day. */
    DAY = 27,

    /** Date token for weekday. */
    WEEKDAY = 28,

    /** Time token for hour. */
    HOUR = 30,

    /** Time token for minute. */
    MIN = 31,

    /** Time token for second. */
    SEC = 32,

    /** Time token for fractional seconds. */
    FSEC = 33,

    /** AM/PM marker for 12-hours format. */
    AMPM = 34,

    /** The placeholder for the text string to be formatted (the "@" token). */
    TEXT = 40,

    /** Literal text. */
    LIT = 41,

    /** Dummy token, for internal use. */
    DUMMY = 99,
}

/**
 * Base interface of all parsed tokens in a number format code. Contains the
 * original text portion parsed from the format code.
 */
export interface BaseToken {
    match: string;
}

/**
 * A token for a parsed number format with a type, but without additional data.
 */
export interface SimpleToken extends BaseToken {
    type: TokenType.STD | TokenType.DEC | TokenType.DATESEP | TokenType.TIMESEP | TokenType.TEXT;
}

/**
 * Specific kind of a literal token:
 * - "bare": Bare (unquoted) literal text.
 * - "backslash": Literal text preceded by backslash characters.
 * - "quote": Literal text enclosed in double-quote characters.
 * - "blind": Blind text preceded by underscore characters.
 * - "fill": A fill character preceded by an asterisk.
 */
export type LiteralTokenKind = "bare" | "backslash" | "quote" | "blind" | "fill";

/**
 * A token for a parsed number format containing literal text to be inserted
 * into the formatted values.
 */
export interface LiteralToken extends BaseToken {
    type: TokenType.LIT;
    kind: LiteralTokenKind;
    text: string;
}

/**
 * A token for a parsed number format for digit patterns.
 */
export interface DigitToken extends BaseToken {
    type: TokenType.INT | TokenType.FRAC | TokenType.EINT | TokenType.EFRAC | TokenType.NUMER | TokenType.DENOM;
    pattern: string;    // digit pattern without group separators, e.g. "##0" or "0123??"
    offset: number;     // number of pattern digits in adjacent digit tokens (without non-zero digits)
    size: number;       // number of pattern digits in this digit token (without non-zero digits)
    groups: number;     // number of trailing group separator characters
    first?: boolean;    // whether the token is the first of its type in the section
    last?: boolean;     // whether the token is the last of its type in the section
}

/**
 * A date token for a parsed number format.
 */
export interface DateToken extends BaseToken {
    type: TokenType.YEAR | TokenType.BYEAR | TokenType.GYEAR | TokenType.GERA | TokenType.QUARTER | TokenType.MONTH | TokenType.WEEK | TokenType.DAY | TokenType.WEEKDAY;
    size: number;       // size of the pattern (e.g. 3 for "DDD")
}

/**
 * A time token for a parsed number format.
 */
export interface TimeToken extends BaseToken {
    type: TokenType.HOUR | TokenType.MIN | TokenType.SEC | TokenType.FSEC;
    size: number;       // size of the pattern (e.g. 2 for "hh")
    elapsed?: boolean;  // elapsed time mode (e.g. "[hh]" instead of "hh")
}

/**
 * A token for a parsed number format for the AM/PM marker.
 */
export interface AMPMToken extends BaseToken {
    type: TokenType.AMPM;
    fixed?: HourModeTokens; // original strings for short A/P notation
}

/**
 * A dummy token for internal usage.
 */
export interface DummyToken extends BaseToken {
    type: TokenType.DUMMY;
}

/**
 * A token for a parsed number format.
 */
export type ParsedToken = SimpleToken | LiteralToken | DigitToken | DateToken | TimeToken | AMPMToken | DummyToken;

/**
 * Operators used in number format codes to restrict a code section to specific
 * number intervals.
 */
export type SectionOperator = "<" | "<=" | ">" | ">=" | "=" | "<>";
