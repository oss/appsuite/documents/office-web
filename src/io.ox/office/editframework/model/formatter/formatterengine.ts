/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, fun, to, math, ary } from "@/io.ox/office/tk/algorithms";
import { onceMethod } from "@/io.ox/office/tk/objects";
import type { DateTimeComponents } from "@/io.ox/office/tk/utils/dateutils";
import { ISOWeekDay, MIN_PER_DAY, SEC_PER_DAY, MSEC_PER_DAY, splitUTCDateTime, addDaysToDate, getWeekNumber } from "@/io.ox/office/tk/utils/dateutils";
import type { LocaleId, LocaleData } from "@/io.ox/office/tk/locale";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import { STD_LENGTH_AUTOFORMAT, getSignificantDigits, formatNumberParts, autoFormatNumber } from "@/io.ox/office/editframework/model/formatter/autoformat";
import type { LiteralToken, DigitToken, DateToken, TimeToken, AMPMToken, ParsedToken } from "@/io.ox/office/editframework/model/formatter/parsedtoken";
import { TokenType } from "@/io.ox/office/editframework/model/formatter/parsedtoken";
import type { SectionNumbersInfo, SectionFractionInfo, SectionDateTimeInfo, ParsedSection } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import { DigitCode } from "@/io.ox/office/editframework/model/formatter/parsedsection";
import type { FormatValueOptions, FormatValueResult, BaseFormatter } from "@/io.ox/office/editframework/model/formatter/baseformatter";

// types ======================================================================

/**
 * A single text portion of a formatted value.
 */
export interface FormatPortion {

    /**
     * The type of this text portion.
     */
    type: "standard" | "literal" | "blind" | "fill";

    /**
     * The text value of this portion.
     */
    text: string;

    /**
     * The relative X offset of the text portion. Will be zero, if no rendering
     * font was available.
     */
    offset: number;

    /**
     * The width of the text portion. Will be zero, if no rendering font was
     * available.
     */
    width: number;
}

// private types --------------------------------------------------------------

/**
 * The string representation of different parts of a number.
 */
interface DigitsState {
    /** The integer part for numeric and fraction formats. */
    int: string;
    /** The fraction part for numeric and fraction formats. */
    frac: string;
    /** The absolute exponent for scientific formats. */
    expn: string;
    /** The numerator for fraction formats. */
    numer: string;
    /** The denominator for fraction formats. */
    denom: string;
    /** Whether the exponent for scientific notation is negative. */
    expneg: boolean;
}

/**
 * The numeric components of a fraction.
 */
interface FractionState {
    /** The integer part of the fraction. */
    int: number;
    /** The numerator of the fraction. */
    numer: number;
    /** The denominator of the fraction. */
    denom: number;
}

/**
 * The date/time components for the number to be formatted.
 */
interface DateTimeState extends DateTimeComponents {
    /** Week day (zero-based, starting with Sunday). */
    A: number;
    /** Week number (ISO 8601). */
    W: number;
    /** Original system timestamp (in milliseconds from 1970-01-01). */
    ts: number;
    /** Rounded serial number (in days, relative to null date). */
    sn: number;
    /** The separator character to be used for date values (DATESEP token). */
    dateSep: string;
    /** The separator character to be used for time values (TIMESEP token). */
    timeSep: string;
}

/**
 * Information about an era of the Gengo calendar.
 */
interface GengoEraInfo {
    /** Timestamp of the first day of the era. */
    timestamp: number;
    /** Formats to be applied for the `GERA` token. */
    tokens: string[];
}

// constants ==================================================================

// replacement text for digit codes, if formatted number is shorter
const DEF_DIGITS: Record<DigitCode, string> = {
    [DigitCode.ZERO]:  "0",
    [DigitCode.HIDE]:  "",
    [DigitCode.BLIND]: " "
};

// digits for all supported numeral systems
const NUMSYS_DIGITS_MAP: ReadonlyArray<Opt<string>> = [
    undefined,                                                      // 00
    undefined,                                                      // 01 ASCII
    "\u0660\u0661\u0662\u0663\u0664\u0665\u0666\u0667\u0668\u0669", // 02 Arabic-Indic
    "\u06F0\u06F1\u06F2\u06F3\u06F4\u06F5\u06F6\u06F7\u06F8\u06F9", // 03 Extended Arabic-Indic
    "\u0966\u0967\u0968\u0969\u096A\u096B\u096C\u096D\u096E\u096F", // 04 Devanagari
    "\u09E6\u09E7\u09E8\u09E9\u09EA\u09EB\u09EC\u09ED\u09EE\u09EF", // 05 Bengali
    "\u0A66\u0A67\u0A68\u0A69\u0A6A\u0A6B\u0A6C\u0A6D\u0A6E\u0A6F", // 06 Gurmukhi
    "\u0AE6\u0AE7\u0AE8\u0AE9\u0AEA\u0AEB\u0AEC\u0AED\u0AEE\u0AEF", // 07 Gujarati
    "\u0B66\u0B67\u0B68\u0B69\u0B6A\u0B6B\u0B6C\u0B6D\u0B6E\u0B6F", // 08 Oriya
    "\u0BE6\u0BE7\u0BE8\u0BE9\u0BEA\u0BEB\u0BEC\u0BED\u0BEE\u0BEF", // 09 Tamil
    "\u0C66\u0C67\u0C68\u0C69\u0C6A\u0C6B\u0C6C\u0C6D\u0C6E\u0C6F", // 0A Telugu
    "\u0CE6\u0CE7\u0CE8\u0CE9\u0CEA\u0CEB\u0CEC\u0CED\u0CEE\u0CEF", // 0B Kannada
    "\u0D66\u0D67\u0D68\u0D69\u0D6A\u0D6B\u0D6C\u0D6D\u0D6E\u0D6F", // 0C Malayalam
    "\u0E50\u0E51\u0E52\u0E53\u0E54\u0E55\u0E56\u0E57\u0E58\u0E59", // 0D Thai
    "\u0ED0\u0ED1\u0ED2\u0ED3\u0ED4\u0ED5\u0ED6\u0ED7\u0ED8\u0ED9", // 0E Lao
    "\u0F20\u0F21\u0F22\u0F23\u0F24\u0F25\u0F26\u0F27\u0F28\u0F29", // 0F Tibetan
    "\u1040\u1041\u1042\u1043\u1044\u1045\u1046\u1047\u1048\u1049", // 10 Myanmar
    "0\u1369\u136A\u136B\u136C\u136D\u136E\u136F\u1370\u1371",      // 11 Ethiopic
    "\u17E0\u17E1\u17E2\u17E3\u17E4\u17E5\u17E6\u17E7\u17E8\u17E9", // 12 Khmer
    "\u1810\u1811\u1812\u1813\u1814\u1815\u1816\u1817\u1818\u1819", // 13 Mongolian
    undefined,                                                      // 14
    undefined,                                                      // 15
    undefined,                                                      // 16
    undefined,                                                      // 17
    undefined,                                                      // 18
    undefined,                                                      // 19
    undefined,                                                      // 1A
    "\u3007\u4E00\u4E8C\u4E09\u56DB\u4E94\u516D\u4E03\u516B\u4E5D", // 1B CJK (DBNum1, Japanese, Ordinary)
    "\u3007\u58F1\u5F10\u53C2\u56DB\u4F0D\u516D\u4E03\u516B\u4E5D", // 1C CJK (DBNum2, Japanese, Banker's Numerals)
    "\uFF10\uFF11\uFF12\uFF13\uFF14\uFF15\uFF16\uFF17\uFF18\uFF19", // 1D CJK (DBNum3, Japanese, Fullwidth)
    "\u25CB\u4E00\u4E8C\u4E09\u56DB\u4E94\u516D\u4E03\u516B\u4E5D", // 1E CJK (DBNum1, Chinese Simplified, Ordinary)
    "\u96F6\u58F9\u8D30\u53C1\u8086\u4F0D\u9646\u67D2\u634C\u7396", // 1F CJK (DBNum2, Chinese Simplified, Banker's Numerals)
    "\uFF10\uFF11\uFF12\uFF13\uFF14\uFF15\uFF16\uFF17\uFF18\uFF19", // 20 CJK (DBNum3, Chinese Simplified, Fullwidth)
    "\u25CB\u4E00\u4E8C\u4E09\u56DB\u4E94\u516D\u4E03\u516B\u4E5D", // 21 CJK (DBNum1, Chinese Traditional, Ordinary)
    "\u96F6\u58F9\u8CB3\u53C3\u8086\u4F0D\u9678\u67D2\u634C\u7396", // 22 CJK (DBNum2, Chinese Traditional, Banker's Numerals)
    "\uFF10\uFF11\uFF12\uFF13\uFF14\uFF15\uFF16\uFF17\uFF18\uFF19", // 23 CJK (DBNum3, Chinese Traditional, Fullwidth)
    "\uFF10\u4E00\u4E8C\u4E09\u56DB\u4E94\uF9D1\u4E03\u516B\u4E5D", // 24 CJK (DBNum1, Korean, Ordinary)
    "\uF9B2\u58F9\u8CB3\uF96B\u56DB\u4F0D\uF9D1\u4E03\u516B\u4E5D", // 25 CJK (DBNum2, Korean, Banker's Numerals)
    "\uFF10\uFF11\uFF12\uFF13\uFF14\uFF15\uFF16\uFF17\uFF18\uFF19", // 26 CJK (DBNum3, Korean, Fullwidth)
    "\uC601\uC77C\uC774\uC0BC\uC0AC\uC624\uC721\uCE60\uD314\uAD6C"  // 27 CJK (DBNum4, Korean, Hangul Syllables)
];

// timestamp of 1900-Feb-28 in days (for handling of 1900 leep year bug)
const LEAP_DAY_1900 = Math.floor(Date.UTC(1900, 1, 28) / MSEC_PER_DAY);

// all eras of the Gengo calendar, sorted chronologically (!)
const GENGO_ERA_LIST: readonly GengoEraInfo[] = [
    { timestamp: Date.UTC(1868,  0, 25), tokens: ["M", "\u660E", "\u660E\u6CBB"] }, // Meiji
    { timestamp: Date.UTC(1912,  6, 30), tokens: ["T", "\u5927", "\u5927\u6B63"] }, // Taisho
    { timestamp: Date.UTC(1926, 11, 25), tokens: ["S", "\u662D", "\u662D\u548C"] }, // Showa
    { timestamp: Date.UTC(1989,  0,  8), tokens: ["H", "\u5E73", "\u5E73\u6210"] }, // Heisei
    { timestamp: Date.UTC(2019,  4,  1), tokens: ["R", "\u4EE4", "\u4EE4\u548C"] }  // Reiwa
];

// functions ==================================================================

/**
 * Returns whether the passed character is a valid digit placeholder code.
 */
function isDigitCode(digit: string): digit is DigitCode {
    return digit in DEF_DIGITS;
}

/**
 * Returns whether the specified digit in an integer will be followed by a
 * group separator character.
 *
 * @param expn
 *  The exponent of the digit, i.e. the zero-based digit index from the end.
 */
function isGroupDigit(expn: number): boolean {
    // TODO: locale-dependent grouping pattern (e.g. Lakh/Crore)
    return (expn > 0) && !(expn % 3);
}

/**
 * Converts "DBNum" token identifier to numeral system identifier.
 */
function resolveJCKNumSys(localeId: LocaleId, cjkNumId: number): Opt<number> {

    // Japanese: ignore country or script
    if (localeId.language === "ja") {
        return [0, 0x1B, 0x1C, 0x1D][cjkNumId];
    }

    // Korean: ignore country or script (only language with support for DBNum4)
    if (localeId.language === "ko") {
        return [0, 0x24, 0x25, 0x26, 0x27][cjkNumId];
    }

    // Chinese (select simplified/traditional by script or region)
    if (localeId.language === "zh") {
        const tradScript = (localeId.script === "Hant") || (localeId.region === "HK") || (localeId.region === "MO") || (localeId.region === "TW");
        return (tradScript ? [0, 0x21, 0x22, 0x23] : [0, 0x1E, 0x1F, 0x20])[cjkNumId];
    }

    return 0;
}

// class FormatterEngine ======================================================

// formatter function for a single token
type FormatTokenFn<TT extends ParsedToken> = (this: FormatterEngine, token: TT) => void;

/**
 * Prepared data and ongoing state about a single value to be formatted by the
 * number formatter.
 */
export class FormatterEngine {

    // static functions -------------------------------------------------------

    static format(formatter: BaseFormatter, section: ParsedSection, prepValue: unknown, prepNum: number, options?: FormatValueOptions): Opt<FormatValueResult> {

        // creae a new formatter engine for this run
        const engine = new FormatterEngine(formatter, section, prepValue, prepNum);

        // initialize engine according to the number format category, and process all format code tokens of the section
        return engine.#initialize(options) ? engine.#format(options) : undefined;
    }

    /**
     * Generates, caches, and returns the registry map of all token formatter
     * methods.
     */
    @onceMethod
    static #getFormatMap(): Record<TokenType, FormatTokenFn<any>> {
        return {
            // directly access class prototype to prevent adding extra callframe (arrow function)
            [TokenType.STD]:     FormatterEngine.prototype.formatStdToken,
            [TokenType.INT]:     FormatterEngine.prototype.formatIntToken,
            [TokenType.FRAC]:    FormatterEngine.prototype.formatFracToken,
            [TokenType.DEC]:     FormatterEngine.prototype.formatDecToken,
            [TokenType.EINT]:    FormatterEngine.prototype.formatExpIntToken,
            [TokenType.EFRAC]:   FormatterEngine.prototype.formatExpFracToken,
            [TokenType.NUMER]:   FormatterEngine.prototype.formatNumerToken,
            [TokenType.DENOM]:   FormatterEngine.prototype.formatDenomToken,
            [TokenType.DATESEP]: FormatterEngine.prototype.formatDateSepToken,
            [TokenType.TIMESEP]: FormatterEngine.prototype.formatTimeSepToken,
            [TokenType.YEAR]:    FormatterEngine.prototype.formatYearToken,
            [TokenType.BYEAR]:   FormatterEngine.prototype.formatBYearToken,
            [TokenType.GYEAR]:   FormatterEngine.prototype.formatGYearToken,
            [TokenType.GERA]:    FormatterEngine.prototype.formatGEraToken,
            [TokenType.QUARTER]: FormatterEngine.prototype.formatQuarterToken,
            [TokenType.MONTH]:   FormatterEngine.prototype.formatMonthToken,
            [TokenType.WEEK]:    FormatterEngine.prototype.formatWeekToken,
            [TokenType.DAY]:     FormatterEngine.prototype.formatDayToken,
            [TokenType.WEEKDAY]: FormatterEngine.prototype.formatWeekDayToken,
            [TokenType.HOUR]:    FormatterEngine.prototype.formatHourToken,
            [TokenType.MIN]:     FormatterEngine.prototype.formatMinToken,
            [TokenType.SEC]:     FormatterEngine.prototype.formatSecToken,
            [TokenType.FSEC]:    FormatterEngine.prototype.formatFSecToken,
            [TokenType.AMPM]:    FormatterEngine.prototype.formatAMPMToken,
            [TokenType.TEXT]:    FormatterEngine.prototype.formatTextToken,
            [TokenType.LIT]:     FormatterEngine.prototype.formatLitToken,
            [TokenType.DUMMY]:   fun.undef,
        };
    }

    // properties -------------------------------------------------------------

    /** Back-reference to the parent number formatter. */
    readonly #formatter: BaseFormatter;

    /** The parsed section used to format the value. */
    readonly #section: ParsedSection;

    /** The language of the locale/resource data. */
    readonly #language: string;

    /** The locale data to be used. */
    readonly #locale: LocaleData;

    /** Numeral system digits mapping. */
    readonly #numsys: Opt<string>;

    /** The string representation of different parts of a number. */
    readonly #digits: DigitsState = { int: "", frac: "", expn: "", numer: "", denom: "", expneg: false };

    /** The numeric components of a fraction. */
    readonly #fraction: FractionState = { int: 0, numer: 0, denom: 0 };

    /** The date/time components. */
    readonly #date: DateTimeState = { Y: 0, M: 0, D: 0, W: 0, A: 0, h: 0, m: 0, s: 0, ms: 0, ts: 0, sn: 0, dateSep: "/", timeSep: ":" };

    /** The original value to be formatted. */
    readonly #value: unknown;

    /** The original (signed) number, or 0 for string values. */
    readonly #num: number;

    /** The absolute number. */
    readonly #abs: number;

    /** The string value, or an empty string for numbers. */
    readonly #text: string;

    /** The text portions. */
    readonly #portions: FormatPortion[] = [];

    /** Specifies how to process literal text tokens. */
    #litMode = DigitCode.ZERO;

    // constructor ------------------------------------------------------------

    private constructor(formatter: BaseFormatter, section: ParsedSection, prepValue: unknown, prepNum: number) {

        // increase number for percent formats, decrease by group blocks (except if used scientific formats)
        if ((prepNum !== 0) && section.numbers && !section.scientific) {
            // TODO: define and consider locale-dependent grouping pattern (e.g. Lakh/Crore)
            const shiftExp = 2 * (section.numbers.pctSigns || 0) - 3 * (section.numbers.groupBlocks || 0);
            prepNum *= 10 ** shiftExp;
        }

        // resolve numeral system identifier
        let numSysId = section.locale?.numSysId;
        // convert DBNum token identifier to numeral system
        if (!numSysId && section.locale?.localeId && section.cjkNumId) {
            numSysId = resolveJCKNumSys(section.locale.localeId, section.cjkNumId);
        }

        // properties
        this.#formatter = formatter;
        this.#section = section;
        this.#language = section.locale?.localeId?.language ?? LOCALE_DATA.language;
        this.#locale = section.locale?.lcData ?? LOCALE_DATA;
        this.#numsys = numSysId ? NUMSYS_DIGITS_MAP[numSysId] : undefined;
        this.#value = prepValue;
        this.#num = prepNum;
        this.#abs = Math.abs(prepNum);
        this.#text = to.string(prepValue, "");
    }

    // token formatters -------------------------------------------------------

    /**
     * Processes a STD token. The formatted number will actually be inserted
     * later after all other tokens have been formatted.
     */
    protected formatStdToken(): void {
        this.#appendPortion("standard", "");
    }

    /**
     * Processes an INT token. Appends the text representation of the integer part.
     */
    protected formatIntToken(token: DigitToken): void {

        // format the integer part according to the digit pattern
        const group = this.#section.numbers?.groupInt ? LOCALE_DATA.group : undefined;
        this.#processIntToken(token, this.#digits.int, group);

        // switch to special mode for literal text between last INT token and NUMER token in fractions
        if (token.last) {
            this.#litMode = this.#getFractionLitMode(this.#fraction.int !== 0, this.#fraction.numer !== 0);
        }
    }

    /**
     * Processes a FRAC token. Appends the text representation of the fraction
     * part.
     */
    protected formatFracToken(token: DigitToken): void {
        this.#processFracToken(token, this.#digits.frac);
    }

    /**
     * Processes a DEC token. Appends the decimal separator.
     */
    protected formatDecToken(): void {
        this.#appendText(LOCALE_DATA.dec);
    }

    /**
     * Processes an EINT token. Appends the text representation of the
     * exponent.
     */
    protected formatExpIntToken(token: DigitToken): void {
        if (this.#digits.expneg) {
            this.#appendText("-");
        } else if (this.#section.scientific?.plusSign) {
            this.#appendText("+");
        }
        this.#processIntToken(token, this.#digits.expn);
    }

    /**
     * Processes an EFRAC token. Appends the text representation of the
     * fraction part of the exponent.
     */
    protected formatExpFracToken(token: DigitToken): void {
        this.#processFracToken(token, "");
    }

    /**
     * Processes a NUMER token. Appends the text representation of the
     * numerator.
     */
    protected formatNumerToken(token: DigitToken): void {

        // update literal text processing for numerator/denominator in long fractions
        if (!this.#section.fraction?.shortFrac) {
            this.#litMode = this.#getFractionLitMode(true, this.#fraction.numer !== 0);
        }

        // format the numerator according to the digit pattern, or create blind digit pattern
        switch (this.#litMode) {
            case DigitCode.ZERO:  this.#processIntToken(token, this.#digits.numer); break;
            case DigitCode.BLIND: this.#processBlindPattern(token.pattern);        break;
            case DigitCode.HIDE:  break;
        }
    }

    /**
     * Processes a DENOM token. Appends the text representation of the denominator.
     */
    protected formatDenomToken(token: DigitToken): void {

        // format the denominator, create blind digit pattern
        // - fixed-value token will be inserted into the result as is
        // - patterns with a leading question mark will be formatted from left to right (like the fraction part of a number)
        // - other patterns will be formatted from right to left (like the integer part of a number)
        switch (this.#litMode) {
            case DigitCode.ZERO:
                if (this.#section.fraction?.fixedDenom) {
                    this.#appendText(this.#convertDigits(this.#digits.denom, true));
                } else if (this.#section.fraction?.denomMode === DigitCode.BLIND) {
                    this.#processFracToken(token, this.#digits.denom);
                } else {
                    this.#processIntToken(token, this.#digits.denom);
                }
                break;
            case DigitCode.BLIND:
                this.#processBlindPattern(token.pattern);
                break;
            case DigitCode.HIDE:
                break;
        }

        // switch back to normal literal text processing
        this.#litMode = DigitCode.ZERO;
    }

    /**
     * Processes a DATESEP token. Appends the date separator character.
     */
    protected formatDateSepToken(): void {
        this.#appendText(this.#date.dateSep);
    }

    /**
     * Processes a TIMESEP token. Appends the time separator character.
     */
    protected formatTimeSepToken(): void {
        this.#appendText(this.#date.timeSep);
    }

    /**
     * Processes a YEAR token. Appends the text representation of the year.
     */
    protected formatYearToken(token: DateToken): void {
        const size = (token.size <= 2) ? 2 : 4;
        const year = this.#date.Y;
        this.#appendInt((size === 2) ? (year % 100) : year, size);
    }

    /**
     * Processes a BYEAR token. Appends the text representation of the Buddhist
     * year.
     */
    protected formatBYearToken(token: DateToken): void {
        const size = (token.size <= 2) ? 2 : 4;
        const year = this.#date.Y + 543;
        this.#appendInt((size === 2) ? (year % 100) : year, size);
    }

    /**
     * Processes a GYEAR token. Appends the text representation of the Gengo
     * year.
     */
    protected formatGYearToken(token: DateToken): void {
        // Japanese: year of Gengo era
        const eraInfo = (this.#language === "ja") ? this.#getGengoEra() : undefined;
        if (eraInfo) {
            const eraYear = (this.#date.Y - new Date(eraInfo.timestamp).getUTCFullYear()) + 1;
            this.#appendInt(eraYear, Math.min(2, token.size));
        } else {
            // all other locales: full year
            this.#appendInt(this.#date.Y, 4);
        }
    }

    /**
     * Processes a GERA token. Appends the text representation of the Gengo
     * era.
     */
    protected formatGEraToken(token: DateToken): void {
        // Japanese: year of Gengo era; empty string in all other locales
        const eraInfo = (this.#language === "ja") ? this.#getGengoEra() : undefined;
        if (eraInfo) {
            const eraIdx = math.clamp(token.size, 1, eraInfo.tokens.length);
            this.#appendText(eraInfo.tokens[eraIdx - 1]);
        }
    }

    /**
     * Processes a QUARTER token. Appends the text representation of the
     * quarter.
     */
    protected formatQuarterToken(token: DateToken): void {
        const quarters = (token.size === 1) ? this.#locale.shortQuarters : this.#locale.longQuarters;
        this.#appendText(quarters[Math.floor(this.#date.M / 3)]);
    }

    /**
     * Processes a MONTH token. Appends the text representation of the month.
     */
    protected formatMonthToken(token: DateToken): void {
        const month = this.#date.M;
        switch (token.size) {
            case 1:  this.#appendInt(month + 1, 1);                       break; // short month number
            case 2:  this.#appendInt(month + 1, 2);                       break; // month number with leading zero
            case 3:  this.#appendText(this.#locale.shortMonths[month]);   break; // abbreviated month name
            default: this.#appendText(this.#locale.longMonths[month]);    break; // full month name (default for oversized tokens)
            case 5:  this.#appendText(this.#locale.longMonths[month][0]); break; // first character of the full month name
        }
    }

    /**
     * Processes a WEEK token. Appends the text representation of the week
     * number.
     */
    protected formatWeekToken(): void {
        this.#appendInt(this.#date.W, 1); // never padded with zeros
    }

    /**
     * Processes a DAY token. Appends the text representation of the day.
     */
    protected formatDayToken(token: DateToken): void {
        switch (token.size) {
            case 1:  this.#appendInt(this.#date.D, 1); break; // short day number
            case 2:  this.#appendInt(this.#date.D, 2); break; // day number with leading zero
            case 3:  this.#appendWeekday(false);      break; // abbreviated name of week day
            default: this.#appendWeekday(true);       break; // full name of week day (default for oversized tokens)
        }
    }

    /**
     * Processes a WEEKDAY token. Appends the text representation of the week
     * day.
     */
    protected formatWeekDayToken(token: DateToken): void {
        switch (token.size) {
            case 2:  this.#appendWeekday(false);      break; // abbreviated name of week day
            case 3:  this.#appendWeekday(true);       break; // full name of week day
            default: this.#appendWeekday(true, ", "); break; // full name of week day with comma (TODO: localization of comma?)
        }
    }

    /**
     * Processes an HOUR token. Appends the text representation of the hours.
     */
    protected formatHourToken(token: TimeToken): void {
        if (token.elapsed) {
            this.#appendInt(Math.floor(this.#date.sn * 24), token.size);
        } else {
            const hour = this.#date.h;
            this.#appendInt(this.#section.dateTime?.hours12 ? (((hour + 11) % 12) + 1) : hour, Math.min(2, token.size));
        }
    }

    /**
     * Processes a MIN token. Appends the text representation of the minutes.
     */
    protected formatMinToken(token: TimeToken): void {
        if (token.elapsed) {
            this.#appendInt(Math.floor(this.#date.sn * MIN_PER_DAY), token.size);
        } else {
            this.#appendInt(this.#date.m, Math.min(2, token.size));
        }
    }

    /**
     * Processes a SEC token. Appends the text representation of the seconds.
     */
    protected formatSecToken(token: TimeToken): void {
        if (token.elapsed) {
            this.#appendInt(Math.floor(this.#date.sn * SEC_PER_DAY), token.size);
        } else {
            this.#appendInt(this.#date.s, Math.min(2, token.size));
        }
    }

    /**
     * Processes an FSEC token. Appends the text representation of the
     * fractional second.
     */
    protected formatFSecToken(token: TimeToken): void {
        const text = this.#formatInt(this.#date.ms, 3, true);
        this.#appendText(LOCALE_DATA.dec + text.slice(0, Math.min(3, token.size)));
    }

    /**
     * Processes an AMPM token. Appends the text representation of the part of
     * the day.
     */
    protected formatAMPMToken(token: AMPMToken): void {
        const hourModes = token.fixed ?? this.#locale.hourModes;
        this.#appendText((this.#date.h < 12) ? hourModes.am : hourModes.pm);
    }

    /**
     * Processes a TEXT token. Appends the string value to be formatted.
     */
    protected formatTextToken(): void {
        this.#appendText(this.#text);
    }

    /**
     * Processes a LIT token. Appends the literal text of the passed token.
     */
    protected formatLitToken(token: LiteralToken): void {
        switch (token.kind) {
            case "blind":
                if (this.#litMode !== DigitCode.HIDE) {
                    this.#appendBlindText(token.text);
                }
                break;
            case "fill":
                if (this.#litMode !== DigitCode.HIDE) {
                    const text = (this.#litMode === DigitCode.BLIND) ? " " : token.text[0];
                    this.#appendPortion("fill", text);
                }
                break;
            default:
                this.#processLiteralText(token.text, this.#litMode);
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Converts the digits in the passed string to the specified numeral
     * system.
     *
     * @param text
     *  The text to be converted.
     *
     * @param numsys
     *  If set to `true`, the configured numeral system will be used for the
     *  digits.
     *
     * @returns
     *  The converted text.
     */
    #convertDigits(text: string, numsys?: boolean): string {
        const digits = numsys ? this.#numsys : undefined;
        return digits ? text.replace(/\d/g, c => digits[c as unknown as number]) : text;
    }

    /**
     * Formats the passed integral number with leading zero characters.
     *
     * @param int
     *  The non-negative integral number to be formatted.
     *
     * @param size
     *  The target size of the formatted number. Must not be negative. If the
     *  resulting text is shorter, leading zero characters will be inserted; if
     *  it is already longer, it will NOT be truncated. If set to zero, and the
     *  passed "int" value is zero too, an empty string will be returned.
     *
     * @param numsys
     *  If set to `true`, the configured numeral system will be used for the
     *  digits.
     *
     * @returns
     *  The formatted number with leading zero characters as needed.
     */
    #formatInt(int: number, size: number, numsys?: boolean): string {
        if ((int === 0) && (size === 0)) { return ""; }
        let text: string;
        if (int < 1e20) {
            text = String(int);
        } else {
            const parts = math.splitNumber(int);
            const result = getSignificantDigits(parts.mant, parts.expn, 20);
            text = formatNumberParts(result.digits, result.expn).int;
        }
        return this.#convertDigits(text.padStart(size, "0"), numsys);
    }

    /**
     * Splits the passed number into integer and fraction part, according to
     * the format settings for decimal notation.
     */
    #initDecimalState(numbersInfo: SectionNumbersInfo): boolean {

        // Determine the number of significant digits used to round the mantissa. In decimal number formats, the number
        // of significant digits is equal to the length of the integer part (which is its decimal exponent increased by
        // 1) plus the number of fractional digits in the number format. Example: The number 123.4567 (mantissa
        // 1.234567, exponent 2) formatted with "0.000" (with three fractional digits) must be rounded to 6 significant
        // digits (exponent plus 1 for integer part, plus 3 for the fractional digits).
        //
        // This works also for numbers less than 1 where the negative exponent reduces the precision due to leading
        // zeros after the decimal separator. Example: The number 0.01234567 (mantissa 1.234567, exponent -2) formatted
        // with "0.000" must be rounded to 2 significant digits (exponent plus 1 plus three fractional digits results
        // in 2; formatted number will be "0.012" with two significant digits "12").
        //
        // The precision may become negative. This situation correctly reflects the case that the number is too small
        // to be formatted and will be shown as zero instead. Example: The number 0.000123 (mantissa 1.23, exponent -4)
        // formatted with "0.00" results in a precision of -1 (-4+1+2), the resulting formatted number will indeed be
        // "0.00".
        //
        const numComps = math.splitNumber(this.#abs);
        const precision = numComps.expn + 1 + numbersInfo.fracDigits;
        const digitsResult = getSignificantDigits(numComps.mant, numComps.expn, precision);

        // Split the significant digits string into integer part and fractional part. These strings will be used to
        // substitute the digit patterns of the INT and FRAC tokens with the actual digits.
        const partsResult = formatNumberParts(digitsResult.digits, digitsResult.expn);

        // put the formatted numbers into the format state object
        this.#digits.int = partsResult.int;
        this.#digits.frac = partsResult.frac;
        return true;
    }

    /**
     * Splits the passed number into integer and fraction part, according to
     * the format settings for scientific notation.
     */
    #initScientificState(numbersInfo: SectionNumbersInfo): boolean {

        // Determine the number of significant digits used to round the mantissa. In scientific number formats, the
        // number of significant digits is equal to or less than the number of integer and fractional digits in the
        // format code. First, the exponent will be rounded down to the next multiple of the length of the integer
        // part.
        //
        // Example: The format code "###.#E+0" formats a number with an exponent that is a multiple of 3. The mantissa
        // will contain 1 to 3 integer digits, and one fraction digit. The number 1.2345 will be formatted to the
        // string "1.2E+0" (2 significant digits), the number 12.345 will be formatted to "12.3E+0" (3 significant
        // digits), the number 123.45 will be formatted to "123.5E+0" (4 significant digits), and the number 1234.5
        // will be formatted to "1.2E+3" (2 significant digits again).
        //
        const numComps = math.splitNumber(this.#abs);
        const stepSize = Math.max(1, numbersInfo.intDigits);
        const precision = math.modulo(numComps.expn, stepSize) + 1 + numbersInfo.fracDigits;
        const digitsResult = getSignificantDigits(numComps.mant, numComps.expn, precision);

        // Correction for the exponent. Convert `-Infinity` (for number zero) to zero, and round down the exponent to
        // be shown in the result to a multiple of the integer pattern size.
        const origExpn = Number.isFinite(digitsResult.expn) ? digitsResult.expn : 0;
        const formatExpn = Math.floor(origExpn / stepSize) * stepSize;

        // Split the significant digits string into integer part and fractional part. These strings will be used to
        // substitute the digit patterns of the INT and FRAC tokens with the actual digits.
        const partsResult = formatNumberParts(digitsResult.digits, origExpn - formatExpn);

        // put the formatted numbers into the format state object
        this.#digits.int = partsResult.int || "0"; // zero always as "0", even if integer pattern is "#"
        this.#digits.frac = partsResult.frac;
        this.#digits.expn = this.#formatInt(Math.abs(formatExpn), 1); // zero always as "0", even if exponent pattern is "#"
        this.#digits.expneg = formatExpn < 0;
        return true;
    }

    /**
     * Splits the passed number into integer part, numerator, and denominator,
     * according to the fraction format settings.
     */
    #initFractionState(fractionInfo: SectionFractionInfo): boolean {

        // settings for fraction formatting
        const { fixedDenom, denomSize, shortFrac, intMode, numerMode } = fractionInfo;

        // split number into integer and fraction parts
        let int = Math.floor(this.#abs);
        let frac = this.#abs - int;

        // the resulting numerator and denominator
        let numer = 0;
        let denom = 1;

        // determine nominator and denominator according to format code settings
        if (fixedDenom) {
            // use a fixed-value denominator
            denom = denomSize;
            if (denom >= 1e10) { return false; }
            numer = Math.round(frac * denom);
        } else {
            // at most 5 digits in automatic denominator (MSXL uses 7, LOCalc uses 8)
            if (denomSize > 5) { return false; }
            // early exit for fractions near 0 or 1
            const maxDenom = 10 ** denomSize - 1;
            const maxNumer = frac * maxDenom;
            if (maxNumer <= 1) {
                // short-path for fractions in the interval [0, 1/maxDenom]
                // (e.g. for two-digit denominators, fractions close to zero become 0 or 1/99)
                if (maxNumer >= 0.5) {
                    numer = 1;
                    denom = maxDenom;
                }
            } else if (maxDenom - maxNumer <= 1) {
                // short-path for fractions in the interval [(maxDenom-1)/maxDenom, 1]
                // (e.g. for two-digit denominators, fractions close to one become 98/99 or 1)
                if (maxDenom - maxNumer <= 0.5) {
                    numer = 1;
                } else {
                    numer = maxDenom - 1;
                    denom = maxDenom;
                }
            } else if (denomSize === 1) {
                // find best-fitting fraction (try all denumerators with one digit)
                for (let dn = 1, err = 1; dn <= 9; dn += 1) {
                    const nm = Math.round(frac * dn);
                    const newErr = Math.abs(frac - nm / dn);
                    if (newErr < err) { numer = nm; denom = dn; err = newErr; }
                }
            } else {
                // find best-fitting fraction (based on Ian Richard's algorithm using Farey sequences)
                const maxErr = 10 ** -denomSize / 100;
                for (let i = 0, a = 0, b = 1, c = 1, d = 0; i < 100; i += 1) {
                    const f = Math.floor(frac);
                    const nm = a + f * c;
                    const dn = b + f * d;
                    a = c;
                    b = d;
                    c = nm;
                    d = dn;
                    frac = 1 / (frac - f);
                    // use the fraction only if denominator does not exceed the maximum length
                    if (dn <= maxDenom) { numer = nm; denom = dn; }
                    // exit loop if relative error goes below the limit
                    if (Math.abs(nm / dn - this.#abs) < maxErr) { break; }
                }
            }
        }

        // jump to next integer if numerator reaches denominator
        if (numer === denom) { int += 1; numer = 0; }

        // convert to short fraction (add integer part to numerator)
        if (shortFrac) {
            numer += int * denom;
            if (!Number.isFinite(numer)) { return false; }
            int = 0;
        }

        // put the formatted numbers into the format state object (zeros as empty strings or "0" depending on format code)
        const showZero = (int === 0) && (numer === 0) && ((intMode !== DigitCode.HIDE) || (numerMode !== DigitCode.ZERO));
        this.#digits.int = this.#formatInt(int, showZero ? 1 : 0);
        this.#digits.numer = this.#formatInt(numer, shortFrac ? 1 : 0);
        this.#digits.denom = this.#formatInt(denom, 0);

        // store resulting numeric components in the state object
        this.#fraction.int = int;
        this.#fraction.numer = numer;
        this.#fraction.denom = denom;

        return true;
    }

    /**
     * Prepares all date/time settings.
     */
    #initDateTimeState(dateTimeInfo: SectionDateTimeInfo, options: Opt<FormatValueOptions>): boolean {

        // convert to rounded serial number (according to number of digits for fractional second)
        const factor = SEC_PER_DAY * 10 ** Math.min(3, dateTimeInfo.precision);
        const serial = Math.round(this.#num * factor) / factor;

        // try to convert to a `Date` object (fails if serial number is out of bounds)
        let date = this.#formatter.convertNumberToDate(serial);
        if (!date) { return false; }

        // obtain week number and day of week *before* leap year bug correction
        this.#date.W = getWeekNumber(date, ISOWeekDay.MO, true);
        this.#date.A = date.getUTCDay();

        // correction for 1900 leap year bug (increase all days before real 1900-Feb-28)
        const daystamp = Math.floor(date.getTime() / MSEC_PER_DAY);
        if (this.#formatter.leapYearBug && (daystamp < LEAP_DAY_1900)) {
            date = addDaysToDate(date, 1);
        }

        // split date into date/time components, add timestamp numbers
        Object.assign(this.#date, splitUTCDateTime(date));
        this.#date.ts = date.getTime();
        this.#date.sn = serial;

        // correction for 1900 leap year bug (replace 1900-Feb-28 with non-existing 1900-Feb-29)
        if (this.#formatter.leapYearBug && (daystamp === LEAP_DAY_1900)) {
            this.#date.M = 1;
            this.#date.D = 29;
        }

        // in environments without negative date support, zero will be shown as 1900-01-00
        if (!this.#formatter.negativeDates && (serial < 1) && this.#formatter.hasNullDate1900()) {
            this.#date.Y = 1900;
            this.#date.M = 0;
            this.#date.D = 0;
        }

        // the time separator character
        this.#date.dateSep = options?.dateSep ?? this.#locale.dateSep;
        this.#date.timeSep = options?.timeSep ?? this.#locale.timeSep;

        return true;
    }

    /**
     * Initializes state properties according to the number format category.
     */
    #initialize(options: Opt<FormatValueOptions>): boolean {

        // fail for infinite numbers
        if (!Number.isFinite(this.#num)) { return false; }

        // date/time formatting
        if (this.#section.dateTime) {
            return this.#initDateTimeState(this.#section.dateTime, options);
        }

        // fractional notation
        if (this.#section.fraction) {
            return this.#initFractionState(this.#section.fraction);
        }

        // decimal or scientific numbers
        if (this.#section.numbers) {
            return this.#section.scientific ?
                this.#initScientificState(this.#section.numbers) :
                this.#initDecimalState(this.#section.numbers);
        }

        // no further initialization for other categories
        return true;
    }

    /**
     * Resolves the processing mode for numerator, denominator, and enclosed
     * literal text for a fraction format code, according to the actual number
     * to be formatted.
     */
    #getFractionLitMode(hasInt: boolean, hasFrac: boolean): DigitCode {

        // always format everything, if integer part and fraction part are present
        if (!this.#section.fraction || (hasInt && hasFrac)) { return DigitCode.ZERO; }

        // start with the mode of the numerator token
        const { intMode, numerMode, denomMode } = this.#section.fraction;
        let litMode = numerMode;

        // determine the effective processing mode
        switch (intMode) {
            case DigitCode.ZERO:
                if (hasFrac) {
                    litMode = DigitCode.ZERO;
                } else if ((litMode === DigitCode.HIDE) && (denomMode === DigitCode.BLIND)) {
                    litMode = DigitCode.BLIND;
                }
                break;
            case DigitCode.HIDE:
                if (!hasInt && (litMode === DigitCode.ZERO)) {
                    litMode = DigitCode.HIDE;
                } else if (!hasFrac && (litMode === DigitCode.HIDE) && (denomMode === DigitCode.BLIND)) {
                    litMode = DigitCode.BLIND;
                }
                break;
            case DigitCode.BLIND:
                if (hasFrac || (litMode === DigitCode.HIDE)) {
                    litMode = DigitCode.BLIND;
                }
                break;
        }

        return litMode;
    }

    /**
     * Returns data for the current Gengo era.
     */
    #getGengoEra(): Opt<GengoEraInfo> {
        return ary.fastFindLastValue(GENGO_ERA_LIST, eraInfo => eraInfo.timestamp <= this.#date.ts);
    }

    /**
     * Appends a custom text portion to the state properties.
     */
    #appendPortion(type: FormatPortion["type"], text: string): void {
        const lastPortion = ary.last(this.#portions);
        if (lastPortion?.type === type) {
            lastPortion.text += text;
        } else {
            this.#portions.push({ type, text, offset: 0, width: 0 });
        }
    }

    /**
     * Appends the passed literal text to the state properties.
     */
    #appendText(text: string): void {
        this.#appendPortion("literal", text);
    }

    /**
     * Appends the passed integer to the state properties.
     */
    #appendInt(int: number, size: number): void {
        this.#appendText(this.#formatInt(int, size, true));
    }

    /**
     * Appends whitespace for the passed literal text to the state properties.
     */
    #appendBlindText(text: string): void {
        this.#appendPortion("blind", text);
    }

    /**
     * Appends the name of a weekday to the state properties.
     */
    #appendWeekday(long: boolean, suffix?: string): void {
        const weekdays = long ? this.#locale.longWeekdays : this.#locale.shortWeekdays;
        this.#appendText(weekdays[this.#date.A] + (suffix ?? ""));
    }

    /**
     * Appends whitespace for the passed digit pattern. The whitespace will
     * occupy the width of as many zeros as characters are contained in the
     * pattern.
     */
    #processBlindPattern(pattern: string): void {
        this.#appendBlindText(this.#convertDigits("0".repeat(pattern.length), true));
    }

    /**
     * Appends the literal text according to the passed processing mode.
     */
    #processLiteralText(text: string, litMode: DigitCode): void {
        switch (litMode) {
            case DigitCode.ZERO:  this.#appendText(text);      break;
            case DigitCode.BLIND: this.#appendBlindText(text); break;
            case DigitCode.HIDE:  break;
        }
    }

    /**
     * Appends a digit for a number or the placeholder for missing digits, and
     * an optional group separator.
     */
    #processDigit(digit: string, pattern: DigitCode, expn: number, group?: string): void {
        if (!digit && (pattern === DigitCode.BLIND)) {
            this.#appendBlindText(this.#convertDigits("0", true));
        } else {
            this.#appendText(this.#convertDigits(digit || DEF_DIGITS[pattern], true));
        }
        if (group && isGroupDigit(expn)) {
            this.#processLiteralText(group, digit ? DigitCode.ZERO : pattern);
        }
    }

    /**
     * Appends the text representation of some integer digits.
     */
    #processIntToken(token: DigitToken, digits: string, group?: string): void {

        // digit pattern to be substituted with the passed digits
        const { pattern, offset, size, first } = token;

        // process all characters of the pattern
        const d0 = digits.length - offset - size;
        for (let pi = 0, pl = pattern.length, di = d0, expn = offset + size - 1; pi < pl; pi += 1) {

            // current `pattern` character
            const pc = pattern[pi];

            // literal digits 1-9 do not cause any processing (no conversion to numeral system)
            if (!isDigitCode(pc)) {
                this.#appendText(pc);
                continue;
            }

            // Add leading digits uncovered by the pattern before first pattern character (but after leading literal
            // digits). Example: the digit pattern "7080" causes to format the number 1234 as "712384".
            if (first && (d0 > 0) && (di === d0)) {
                if (group) {
                    expn = digits.length - 1;
                    for (let i = 0; i < d0; i += 1, expn -= 1) {
                        this.#processDigit(digits[i], DigitCode.ZERO, expn, group);
                    }
                } else {
                    // no grouping: add all digits
                    this.#appendText(this.#convertDigits(digits.slice(0, d0), true));
                }
            }

            // add the digit or the default placeholder, and the grouping separator
            this.#processDigit(digits[di], pc, expn, group);
            di += 1;
            expn -= 1;
        }
    }

    /**
     * Appends the text representation of some fractional digits.
     */
    #processFracToken(token: DigitToken, digits: string): void {

        // digit pattern to be substituted with the passed digits
        const { pattern } = token;

        // process all characters of the pattern
        for (let pi = 0, pl = pattern.length, di = token.offset; pi < pl; pi += 1) {
            const pc = pattern[pi]; // current `pattern` character
            if (isDigitCode(pc)) {
                this.#processDigit(digits[di], pc, 0);
                di += 1;
            } else {
                this.#appendText(pc); // literal digits 1-9 (no conversion to numeral system)
            }
        }
    }

    #format(options?: FormatValueOptions): Opt<FormatValueResult> {

        // start with the leading minus sign
        if (this.#section.autoMinus && !this.#section.dateTime && (this.#num < 0)) {
            this.#appendText("-");
        }

        // process all format code tokens of the section
        const formatMap = FormatterEngine.#getFormatMap();
        for (const token of this.#section.tokens) {
            formatMap[token.type].call(this, token);
        }

        // the rendering font to be used to render pixel-exact target width (e.g. STD token)
        const renderFont = options?.renderFont;
        // remaining character space for STD and FILL tokens
        let remainLen = options?.maxLen ?? Infinity;
        // remaining pixel space for STD and FILL tokens
        let remainWidth = (renderFont && options?.maxWidth) ?? Infinity;
        // the first STD token
        const stdPortion = this.#portions.find(portion => portion.type === "standard");
        // the last FILL token
        const fillPortion = ary.findValue(this.#portions, portion => portion.type === "fill", { reverse: true });

        // updates `remainLen`, `remainWidth`, `portion.text`, and `portion.width`
        const processPortion = (portion: FormatPortion): boolean => {
            remainLen -= portion.text.length;
            if (renderFont) {
                portion.width = Math.floor(renderFont.getTextWidth(portion.text));
                remainWidth -= portion.width;
            }
            return portion.text.length > 0;
        };

        // process the collected portions data, delete useless portions
        const portions = this.#portions.filter(portion => {
            switch (portion.type) {
                case "standard":  return portion === stdPortion; // filter multiple STD tokens
                case "literal":   return processPortion(portion);
                case "blind":     return processPortion(portion);
                case "fill":      return portion === fillPortion; // filter multiple FILL tokens
            }
        });

        // exit if there is not enough space available for the processed tokens
        const exceedText = options?.exceedText && is.string(this.#value) && !fillPortion;
        if (!exceedText && ((remainLen < 0) || (remainWidth < 0))) { return; }

        // process the STD token (consider remaining space)
        if (stdPortion) {
            const stdLen = Math.min(remainLen, options?.stdLen ?? STD_LENGTH_AUTOFORMAT);
            if ((stdLen <= 0) || (renderFont && (remainWidth < 0))) { return; }
            const stdText = autoFormatNumber(this.#abs, { renderFont, maxWidth: remainWidth, stdLen }).text;
            if (!stdText) { return; }
            // translate the formatted number to target numeral system
            stdPortion.text = this.#convertDigits(stdText, true);
            processPortion(stdPortion);
        }

        // expand the FILL token (unless available space is not restricted)
        if (fillPortion) {
            if (renderFont && Number.isFinite(remainWidth)) {
                const fillSize = Math.min(remainLen, Math.floor(remainWidth / renderFont.getTextWidth(fillPortion.text)));
                fillPortion.text = fillPortion.text.repeat(fillSize);
                fillPortion.width = remainWidth;
            } else {
                fillPortion.text = Number.isFinite(remainLen) ? fillPortion.text.repeat(remainLen) : "";
            }
        }

        // reduce portions to plain-text, and update the X offsets of the portions
        const formatResult: FormatValueResult = { text: "", width: 0, portions };
        for (const portion of portions) {
            const plainText = (portion.type === "blind") ? " ".repeat(portion.text.length) : portion.text;
            formatResult.text! += plainText;
            portion.offset = formatResult.width;
            formatResult.width += portion.width;
        }


        // add color information to the result
        const { color } = this.#section;
        if (color) { formatResult.color = { ...color }; }

        return formatResult;
    }
}
