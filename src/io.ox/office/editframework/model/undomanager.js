/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { ary, json, debug } from '@/io.ox/office/tk/algorithms';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';

import { ModelObject } from '@/io.ox/office/baseframework/model/modelobject';
import ErrorCode from '@/io.ox/office/baseframework/utils/errorcode';
import * as ClientError from '@/io.ox/office/baseframework/utils/clienterror';

import { OTError, otLogger } from '@/io.ox/office/editframework/utils/otutils';
import { OperationContext } from '@/io.ox/office/editframework/model/operation/context';

// types ==================================================================

/**
 * Enumerates processing modes of the undo manager.
 */
const /*enum*/ UndoProcessMode = {

    /**
     * The undo manager is processing the *undo* operations of an action.
     */
    UNDO: 1,

    /**
     * The undo manager is processing the *redo* operations of an action.
     */
    REDO: 2
};

// private functions ======================================================

/**
 * Helper function for the method `UndoAction.appendOperations()`.
 */
function insertOperations(array, source, mode) {

    // no insertion without actual operations
    if (!source) { return array; }

    // callback functions and operations cannot be mixed
    if ((array.length > 0) && (_.isFunction(source) !== _.isFunction(array[0]))) {
        globalLogger.error('$badge{UndoManager} callback function and JSON operations cannot be mixed!');
        return array;
    }

    // convert parameter to an array
    if (_.isFunction(source)) {
        source = [source];
    } else {

        // extract from context or convert to array; and create a deep clone of the operations
        source = ary.wrap(json.deepClone((source instanceof OperationContext) ? source.operation : source));

        // DOCS-1966: filter redo operations containing a special marker flag
        if (mode === UndoProcessMode.REDO) {
            source = source.filter(operation => { return !operation._SKIP_REDO_; });
        }
    }

    // concatenate array according to the passed direction (prepend for undo)
    return (mode === UndoProcessMode.UNDO) ? source.concat(array) : array.concat(source);
}

// class UndoAction ===========================================================

/**
 * An instance of UndoAction is used by the undo manager to store undo and redo
 * operations and to apply them in the document model.
 */
class UndoAction {

    // the collector for the undo operations
    undoOperations = [];
    // the collector for the redo operations
    redoOperations = [];
    // whether the change of selection shall be prevented
    preventSelectionChange;
    // the selection state for the undo operations
    undoSelectionState;
    // the selection state for the redo operations
    redoSelectionState;
    // the user data for the undo operations
    undoUserData;
    // the user data for the redo operations
    redoUserData;
    // original external actions to be restored when redoing this undo action
    restoreActions = null;
    // array index into the collected external actions to be transformed against this action
    otMarker = undefined;

    // constructor ------------------------------------------------------------

    /**
     * @param {UndoOperationSource} undoOperations
     *  One ore more operations that form a logical undo action. When the undo
     *  action is executed, the operations will be applied in the exact order
     *  as passed in this parameter; they will NOT be applied in reversed order
     *  as it would be the case if the operations have been added in multiple
     *  undo actions.
     *
     * @param {UndoOperationSource} redoOperations
     *  One ore more operations that form a logical redo action. When the redo
     *  action is executed, the operations will be applied in the exact order
     *  as passed in this parameter.
     *
     * @param {UndoActionConfig} [actionConfig]
     *  Additional configuration for the undo action.
     */
    constructor(undoOperations, redoOperations, actionConfig) {

        this.preventSelectionChange = !!(actionConfig && actionConfig.preventSelectionChange);

        this.undoSelectionState = actionConfig && actionConfig.undoSelectionState;
        this.redoSelectionState = actionConfig && actionConfig.redoSelectionState;

        this.undoUserData = actionConfig && actionConfig.undoUserData;
        this.redoUserData = actionConfig && actionConfig.redoUserData;

        this.appendOperations(undoOperations, redoOperations);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this undo action is empty, i.e. it does not contain any
     * undo or redo operations.
     *
     * @returns {Boolean}
     *  Whether this undo action is empty.
     */
    empty() {
        return (this.undoOperations.length === 0) && (this.redoOperations.length === 0);
    }

    /**
     * Appends the passed undo and redo operations to the own operation arrays.
     * Undo operations will be inserted at the beginning of the undo operations
     * array, and redo operations will be appended to the redo operations
     * array.
     *
     * @param {UndoOperationSource} undoOperations
     *  One ore more operations that form a logical undo action.
     *
     * @param {UndoOperationSource} redoOperations
     *  One ore more operations that form a logical redo action.
     */
    appendOperations(undoOperations, redoOperations) {

        // add the undo operations at the beginning of the array, accept an arbitrary callback as undo operation
        this.undoOperations = insertOperations(this.undoOperations, undoOperations, UndoProcessMode.UNDO);

        // add the redo operations at the end of the array, accept an arbitrary callback as redo operation
        this.redoOperations = insertOperations(this.redoOperations, redoOperations, UndoProcessMode.REDO);
    }
}

// class UndoManager ==========================================================

/**
 * A collection of undo actions for a document model. Each undo action
 * contains an arbitrary number of document operations to undo, and to redo
 * a specific user action.
 */
export class UndoManager extends ModelObject {

    // callback function that implements merging two actions to one action
    #mergeUndoActionHandler;
    // whether the selection for the undo operation shall be set within the 'addUndo' function
    #updateSelectionInUndo;
    // whether the selection for the undo operation shall be set within the 'addUndo' function
    #disableRedoWithOT;
    // special behaviour in concurrent editing mode
    #isOTEnabled;
    // all undo actions
    #actions = [];
    // a collector for the external operations
    #externalActions = null;
    // index of the next redo action in the action stack (one after the next undo action)
    #actionIdx = 0;
    // number of nested groups suppressing undo generation
    #disableLevel = 0;
    // number of nested action groups
    #groupLevel = 0;
    // current undo action in grouped mode
    #groupAction = null;
    // whether undo or redo operations are currently processed
    #processMode = null;

    // constructor ------------------------------------------------------------

    /**
     * @param {EditModel} docModel
     *  The document model containing this undo manager instance.
     *
     * @param {UndoManagerConfig} [config]
     *  Configuration options.
     */
    constructor(docModel, config) {

        // base constructor
        super(docModel);

        this.#mergeUndoActionHandler = config && config.mergeUndoActionHandler;
        this.#updateSelectionInUndo = config && config.updateSelectionInUndo;
        this.#disableRedoWithOT = config && config.disableRedoWithOT;
        this.#isOTEnabled = this.docApp.isOTEnabled();

        // initialization -----------------------------------------------------

        // collect external operations for transformations, if there are local undo operations available
        if (this.#isOTEnabled) {
            this.listenTo(this.docModel, 'operations:success', (operations, external) => {
                if (external && (this.#actions.length > 0)) {
                    if (!this.#externalActions) { this.#externalActions = []; }
                    this.#externalActions.push({ operations, sentToServer: true });
                    // DOCS-1968: drop non-transformable redo actions above current action
                    if (this.#disableRedoWithOT) {
                        otLogger.info('$badge{UndoManager} event "operations:success": dropping untransformable redo actions');
                        this.#truncateStack();
                    }
                }
            });
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the undo manager is enabled (importing the document
     * has been finished, and the undo manager is currently not processing
     * any undo or redo operations).
     *
     * If OT is enabled, the redo or undo operations will be replaced after
     * being applied, so that the redo is a refreshed undo of an undo.
     *
     * @returns {boolean}
     *  Whether the undo manager is enabled.
     */
    isUndoEnabled() {
        return (!this.#processMode || this.#isOTEnabled) && (this.#disableLevel === 0) && this.isImportFinished();
    }

    /**
     * Clears all undo actions from the stack of the undo manager.
     */
    clearUndoActions() {
        this.#actionIdx = 0;
        this.#truncateStack();
    }

    /**
     * Opens an undo action group and executes the specified callback
     * function. All undo operations added by the callback function will be
     * collected in the action group and act like a single undo action.
     * Nested calls are supported.
     *
     * @param {() => MaybeAsync<T>} callback
     *  The callback function that will be executed while the undo action
     *  group is open. If the function returns a promise, the undo group
     *  remains opened asynchronously until this object will be resolved or
     *  rejected by the callback.
     *
     * @param {object} [context]
     *  The calling context for the callback function.
     *
     * @param {UndoActionConfig} [actionConfig]
     *  Additional options that will be stored in the top-level undo action
     *  generated by this method, and that will be passed to the listeners
     *  of the events "undo:before", "undo:after", "redo:before", and
     *  "redo:after" triggered when applying undo/redo operations. Will be
     *  ignored for embedded undo action groups (embedded calls from method
     *  `enterUndoGroup()`)!
     *
     * @returns {JPromise<T>}
     *  A promise that will fulfil or reject according to the result of the
     *  callback function.
     */
    enterUndoGroup(callback, context, actionConfig) {

        // open the top-level undo group
        if (this.isUndoEnabled() && (this.#groupLevel === 0)) {

            // create a new undo/redo action group that collects all operations
            this.#groupAction = new UndoAction(null, null, actionConfig);

            // get the initial selection state from the document model
            if (this.#updateSelectionInUndo && !this.#groupAction.undoSelectionState) {
                this.#groupAction.undoSelectionState = this.docModel.getSelectionState();
            }

            // notify listeners
            this.trigger('undogroup:open');
        }

        // open a new undo group
        this.#groupLevel += 1;

        // invoke the callback function
        let promise = null;
        try {
            promise = this.convertToPromise(callback.call(context));
        } catch (err) {
            debug.logScriptError(err);
            promise = this.createRejectedPromise(err);
        }

        // wait for the result, clean up the undo group
        this.onSettled(promise, () => {

            // leave the undo group
            this.#groupLevel -= 1;

            // push existing group action to action stack on last group level
            if (this.#groupAction && (this.#groupLevel === 0)) {

                if (!this.#groupAction.empty() && !this.docApp.isOperationsBlockActive()) {

                    // store the final selection state received from the document model
                    if (this.#updateSelectionInUndo && !this.#groupAction.redoSelectionState) {
                        this.#groupAction.redoSelectionState = this.docModel.getSelectionState();
                    }

                    // push the complete action group onto the stack
                    this.#pushAction(this.#groupAction);
                }

                // notify listeners when top-level undo group has been closed
                this.#groupAction = null;
                this.trigger('undogroup:close');
            }
        });

        return promise;
    }

    /**
     * Executes the specified callback function synchronously, and drops
     * all undo operations added by the callback function to this instance.
     *
     * @param {() => T} callback
     *  The callback function to be executed.
     *
     * @param {object} [context]
     *  The calling context for the callback function.
     *
     * @returns {T}
     *  The return value of the callback function.
     *
     * @throws {unknown}
     *  Any exception thrown by the callback function.
     */
    disableUndoSync(callback, context) {
        try {
            this.#disableLevel += 1;
            return callback.call(context);
        } finally {
            this.#disableLevel -= 1;
        }
    }

    /**
     * Creates a new undo action that will apply the passed undo and redo
     * operations in the document model.
     *
     * @param {UndoOperationSource} undoOperations
     *  One ore more operations that form a logical undo action. When the
     *  undo action is executed, the operations will be applied in the
     *  exact order as passed in this parameter; they will NOT be applied
     *  in reversed order (as would be the case if the operations had been
     *  added in multiple undo actions). If the callback function returns
     *  the `false`, or a promise that will reject, undoing this action
     *  will be considered to be failed.
     *
     * @param {UndoOperationSource} redoOperations
     *  One ore more operations that form a logical redo action. When the
     *  redo action is executed, the operations will be applied in the
     *  exact order as passed in this parameter. If the callback function
     *  returns `false`, or a promise that will reject, redoing this action
     *  will be considered to be failed.
     *
     * @param {UndoActionConfig} [actionConfig]
     *  Additional options that will be stored in the top-level undo action
     *  generated by this method, and that will be passed to the listeners
     *  of the events "undo:before", "undo:after", "redo:before", and
     *  "redo:after" triggered when applying undo/redo operations. Will be
     *  ignored if an undo group is already open (embedded calls from
     *  method `enterUndoGroup()`)!
     */
    addUndo(undoOperations, redoOperations, actionConfig) {

        // check that undo manager is valid and either undo or redo operations have been passed
        // and undo and redo operations are only generated for the editor
        if (!this.docApp.isEditable() || !this.isUndoEnabled() || (!undoOperations && !redoOperations)) { return; }

        if (this.#groupAction) {

            // active group action: insert operations into its operation arrays
            this.#groupAction.appendOperations(undoOperations, redoOperations);

        } else {

            // save the initial selection for ungrouped operation
            if (this.#updateSelectionInUndo) {
                actionConfig = actionConfig || {};
                if (!actionConfig.undoSelectionState) {
                    actionConfig.undoSelectionState = this.docModel.getSelectionState();
                }
            }

            // create and insert a new action
            this.#pushAction(new UndoAction(undoOperations, redoOperations, actionConfig));
        }
    }

    /**
     * Returns the number of undo actions available on the undo stack.
     *
     * @returns {number}
     *  The number of undo actions available on the stack.
     */
    getUndoCount() {
        return this.#actionIdx;
    }

    /**
     * Applies the undo operations of the next undo action.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil or reject after the undo action has been
     *  applied completely.
     */
    undo() {

        // nothing to do without an undo action on the stack
        if (this.#actionIdx === 0) { return $.when(); }

        // apply the undo operations of the next undo action
        this.#actionIdx -= 1;
        const undoAction = this.#actions[this.#actionIdx];
        let operations = undoAction.undoOperations;
        const userData = undoAction.undoUserData;

        if (this.#isOTEnabled && this.#externalActions && !_.isFunction(operations[0])) {
            // OT: Transforming the local undo operation(s), but also transform the stack of external operations
            const resultDesc = this.#transformOperations(UndoProcessMode.UNDO, operations, undoAction.undoSelectionState, userData); // modifying the original operations on the undo stack(!)
            if (!resultDesc.operations) { return $.when(); } // for example, if an OTError occurred.
            operations = resultDesc.operations;
            if (!resultDesc.selection) { delete undoAction.undoSelectionState; }
        }

        // do not pass callback functions to the listeners
        const eventOps = _.isFunction(operations[0]) ? [] : operations;

        // build the configuration object for event listeners
        const eventConfig = { preventSelectionChange: undoAction.preventSelectionChange };
        const selectionState = undoAction.undoSelectionState;
        if (selectionState) { eventConfig.selectionState = selectionState; }
        if (userData) { eventConfig.userData = userData; }

        // notify listeners before the operations will be applied
        this.trigger('undo:before', eventOps, eventConfig);

        if (this.#isOTEnabled && (operations.length === 0)) {
            // 'pushAction' is not called in this case to update the redoOperations
            // problem -> empty redo cannot convert the changes done by this undo (67085)
            undoAction.redoOperations = []; // replacing redo with the undo of the curent undo
            this.#setOTMarker(undoAction); // updating the marker for the external operations in the current UndoAction (redo is now up-to-date)
        }

        // apply all undo operations
        const promise = this.#applyOperations(UndoProcessMode.UNDO, operations, selectionState, userData);

        // notify listeners after all operations have been applied
        this.onSettled(promise, () => {
            this.trigger('undo:after', eventOps, eventConfig);
        });

        return promise;
    }

    /**
     * Returns the number of redo actions available on the redo stack.
     *
     * @returns {number}
     *  The number of redo actions available on the stack.
     */
    getRedoCount() {
        return this.#actions.length - this.#actionIdx;
    }

    /**
     * Applies the redo operations of the next undo action.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil or reject after the redo action has been
     *  applied completely.
     */
    redo() {

        // nothing to do without an undo action on the stack
        if (this.#actionIdx === this.#actions.length) { return $.when(); }

        // apply the redo operations of the next undo action
        const undoAction = this.#actions[this.#actionIdx];
        let operations = undoAction.redoOperations;
        const userData = undoAction.redoUserData;

        if (this.#isOTEnabled && this.#externalActions && !_.isFunction(operations[0])) {
            // OT: Transforming the local redo operation(s), but also transform the stack of external operations
            const resultDesc = this.#transformOperations(UndoProcessMode.REDO, operations, undoAction.redoSelectionState, userData);
            if (!resultDesc.operations) { return $.when(); } // for example, if an OTError occurred.
            operations = resultDesc.operations;
            if (!resultDesc.selection) { delete undoAction.redoSelectionState; }
        }

        // do not pass callback functions to the listeners
        const eventOps = _.isFunction(operations[0]) ? [] : operations;

        // build the configuration object for event listeners
        const eventConfig = { preventSelectionChange: undoAction.preventSelectionChange };
        const selectionState = undoAction.redoSelectionState;
        if (selectionState) { eventConfig.selectionState = selectionState; }
        if (userData) { eventConfig.userData = userData; }

        // notify listeners before the operations will be applied
        this.trigger('redo:before', eventOps, eventConfig);

        // apply all redo operations
        const promise = this.#applyOperations(UndoProcessMode.REDO, operations, selectionState, userData);

        // notify listeners after all operations have been applied
        this.onSettled(promise, () => {
            this.#actionIdx += 1;
            this.trigger('redo:after', eventOps, eventConfig);
            if (this.#isOTEnabled && operations.length === 0) { this.docModel.checkUndoOperations(this.#externalActions, undoAction.undoOperations); }
        });

        return promise;
    }

    // private methods ----------------------------------------------------

    /**
     * Removes all redo actions from the stack that are above the current
     * undo/redo action.
     */
    #truncateStack() {

        // external actions for OT are no longer needed, if the stack drains
        if (this.#actionIdx === 0) {
            this.#externalActions = null;
        }

        // nothing to do if there are no actions to be removed
        if (this.#actionIdx < this.#actions.length) {
            // remove undone actions above current stack pointer
            this.#actions.splice(this.#actionIdx);
            // notify listeners
            this.trigger('change:stack');
        }
    }

    /**
     * Pushes the passed new undo action onto the undo stack. Shortens the
     * undo stack if the current undo action is not the topmost action on
     * the stack.
     *
     * @param {UndoAction} newAction
     *  The new undo action to be pushed onto the undo stack.
     */
    #pushAction(newAction) {

        // last action on action stack
        let lastAction = null;
        // the length of the undo stack
        const stackLength = this.#actions.length;

        // truncate main undo stack and push the new action
        if (this.#actionIdx < stackLength) {

            if (this.#processMode) {

                // `processing` can only be true, if OT is enabled
                if (!this.#isOTEnabled) {
                    globalLogger.error('$badge{UndoManager} pushAction: OT must be enabled if processing is true in function pushAction!');
                    return;
                }

                // replacing the current undo/redo operations with an updated version during running undo/redo (required for OT)
                const undoAction = this.#actions[this.#actionIdx];
                if (this.#processMode === UndoProcessMode.UNDO) {
                    undoAction.redoOperations = newAction.undoOperations; // replacing redo with the undo of the curent undo
                    this.#setOTMarker(undoAction); // updating the marker for the external operations in the current UndoAction (redo is now up-to-date)
                } else {
                    // undoAction.undoOperations = newAction.redoOperations; // maybe a TODO
                }

                return;
            }

            // remove undone actions, push new action without merging
            this.#truncateStack();

        } else if (this.#disableRedoWithOT && this.#processMode === UndoProcessMode.UNDO) {

            return; // ignore pushAction in undo mode in Spreadsheet (DOCS-2764)

        } else if (this.#mergeUndoActionHandler && (stackLength > 0)) {

            // custom callback function wants to merge operations of consecutive undo actions
            lastAction = _.last(this.#actions);
            if (this.#mergeUndoActionHandler(lastAction, newAction) === true) {
                lastAction.redoSelectionState = newAction.redoSelectionState;
                return;
            }
        }

        // registering the number of external operations at the new action
        this.#actions.push(newAction);
        this.#actionIdx = this.#actions.length;
        if (this.#isOTEnabled) { this.#setOTMarker(newAction); } // Registering number of external actions at new undo/redo action
        this.trigger('change:stack');
    }

    /**
     * Sets a marker at the passed undo/redo action. This is used after
     * calculating the new redo operation after triggering undo. This
     * marker shows, how many external operation are integrated into the
     * caculation of the redo.
     *
     * @param {UndoAction} undoAction
     *  An undo action to be updated.
     */
    #setOTMarker(undoAction) {
        undoAction.otMarker = this.#externalActions ? this.#externalActions.length : 0;
    }

    /**
     * Returns the action marker for the current action in the undo/redo
     * stack.
     *
     * @returns {Opt<number>}
     *  The OT marker with the number of external operations when the redo
     *  operation was generated, if available; otherwise `undefined`.
     */
    #getCurrentOTMarker() {
        return this.#actions[this.#actionIdx].otMarker;
    }

    /**
     * Invokes the passed callback functions sequentially. Each callback
     * function may run asynchronous code and return a promise.
     *
     * @param {Array<Function>} callbacks
     *  The callback functions to be invoked.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after all callbacks have been
     *  invoked and have finished successfully; ot that will be rejected,
     *  if any callback function returns a rejected promise, or the boolean
     *  value false.
     */
    #invokeOperationCallbacks(callbacks) {
        return callbacks.reduce((promise, callback) => {
            return promise.then(() => {
                const result = callback.call(this);
                return (result === false) ? $.Deferred().reject() : result;
            });
        }, this.createResolvedPromise());
    }

    /**
     * Applies the passed operations from an undo action.
     *
     * @param {UndoProcessMode} applyMode
     *  The processing mode (the kind of the passed operations).
     *
     * @param {Operation[]|Function[]} operations
     *  The operations to be applied, either as array of operation objects,
     *  or as array of callback functions.
     *
     * @param {Opt<object>} selectionState
     *  The new selection state to be set at the document model.
     *
     * @param {Opt<object>} userData
     *  Additional user data for OT to be passed to the document model.
     *
     * @returns {JPromise<void>}
     *  A promise that will fulfil after the operations have been applied
     *  successfully (or when no callback function has returned a rejected
     *  promise).
     */
    #applyOperations(applyMode, operations, selectionState, userData) {

        // initialize processing mode, controls e.g. behavior of `pushAction()` in OT
        this.#processMode = applyMode;

        // OT: block incoming operations during processing (TODO: move to `applyOperationsAsync()`?)
        this.docModel.setInternalOperationBlocker(true);

        // invoke the callback functions, or apply the document operations
        // (bug 30954: always create a deep copy of the operations)
        const promise = _.isFunction(operations[0]) ?
            this.#invokeOperationCallbacks(operations) :
            this.docModel.applyOperationsAsync(json.deepClone(operations), { userData });

        // restore the selection state, after all operations have been applied
        this.onFulfilled(promise, () => {
            if (_.isObject(selectionState)) {
                this.docModel.setSelectionState(selectionState);
            }
        });

        // cleanup after applying all operations
        this.onSettled(promise, () => {
            this.#processMode = null;
            // OT: stop blocking incoming operations during processing
            this.docModel.setInternalOperationBlocker(false);
        });

        return promise;
    }

    /**
     * Only for undo:
     * An undo can modify all external operations that were applied to the document, after the undo was generated.
     * This external operations must be saved in this step. The redo is completely new generated in the undo
     * process and cannot restore this external operations. But after executing undo and redo, the external
     * operations must not be modified. Therefore this steps:
     *  1. In the undo process the external operations that might be modified by the undo are saved in the
     *     options of the current action in the undo stack.
     *  2. In the redo process this saved external actions replace the existing external actions in the stack
     *     of external actions (see 'restoreExternalActionsModifiedByUndo').
     */
    #saveExternalActionsBeforeUndo(originalActions) {
        this.#actions[this.#actionIdx].restoreActions = (originalActions.length > 0) ? json.deepClone(originalActions) : null;
    }

    /**
     * Only for redo:
     * Because redo is calculated after every undo and is only transformed with the following external actions,
     * it is necessary to 'restore' the stack of external actions, because it might be modified by the previous
     * undo (see also 'saveExternalActionsModifiedByUndo').
     *
     * The external actions that need to be modified end at the current redo marker. The previous operations need
     * to be exchanged, because they might have been modified by the undo, that generated this redo.
     *
     * The following external actions (if there are any) will be modified in the following OT process.
     */
    #restoreExternalActionsModifiedByUndo() {

        const restoreActions = this.#actions[this.#actionIdx].restoreActions;
        this.#actions[this.#actionIdx].restoreActions = null;
        if (!restoreActions) { return; }

        const actionCount = restoreActions.length;
        const baseIdx = this.#getCurrentOTMarker() - actionCount;

        // remove the actions from `baseIdx` to current index, and insert the original actions
        // (use a single call to `Array.splice()` to remove and insert the array elements)
        this.#externalActions.splice(baseIdx, actionCount, ...restoreActions);
    }

    /**
     * Transforming the undo/redo operations with the stack of external operations. From the stack of external operations
     * those operations are used, that have a higher 'actionIdx' number than the undo/redo operation. In this
     * transformation process the local undo/redo operations are transformed, but also the stack of external operations
     * might be transformed.
     *
     * @param {UndoProcessMode} transformMode
     *  The processing mode (the kind of the passed operations).
     *
     * @param {Operation[]} operations
     *  An array with the stack of local undo/redo operations.
     *
     * @param {Opt<object>} selectionState
     *  The selection state that will be used to restore the document
     *  selection after the undo/redo action.
     *
     * @param {Opt<object>} userData
     *  Additional user-defined data stored in the undo/redo action.
     *
     * @returns {{ operations?: Operation[]; selection: boolean }}
     *  A result object with the following properties:
     *  - {Opt<Operation[]>} operations -- The transformed local undo/redo
     *    operations; or `undefined`, if an error occured during operation
     *    transformations.
     *  - {boolean} selection -- Whether the selection has been transformed
     *    successfully (if false, it needs to be deleted from the undo/redo
     *    action).
     */
    #transformOperations(transformMode, operations, selectionState, userData) {

        // transform only the external actions that have not been transformed yet
        const transformActions = this.#externalActions.slice(this.#getCurrentOTMarker());

        if (transformMode === UndoProcessMode.UNDO) {
            this.#saveExternalActionsBeforeUndo(transformActions); // undo must save original state of external operations
        } else {
            this.#restoreExternalActionsModifiedByUndo(); // redo must 'restore' external operations modified by undo
        }

        // nothing to transform: return passed operations
        const resultDesc = { operations, selection: true };
        if (transformActions.length === 0) { return resultDesc; }

        // DOCS-1968: drop non-transformable redo actions above current action,
        // when there is need to transform the undo operations
        if (this.#disableRedoWithOT && (transformMode === UndoProcessMode.UNDO)) {
            otLogger.info('$badge{UndoManager} transformOperations: dropping untransformable redo actions');
            this.#truncateStack();
        }

        try {

            // transform in swapped mode: undo operations are handled like external (incoming) operations
            otLogger.info(`$badge{UndoManager} transformOperations: stackIndex=${this.#actionIdx}, transforming operations...`);
            resultDesc.operations = this.docApp.otManager.transformOperations(operations, transformActions, { swapped: true, userData });

            // try to transform the selection state
            if (selectionState) {

                // let the OT manager transform the selection state
                resultDesc.selection = this.docApp.otManager.transformSelectionState(selectionState, transformActions);

                // adapt the selection state, so that the selection can be set correctly after undo/redo
                if (resultDesc.selection) {
                    this.trigger('transform:selectionstate', selectionState, transformActions);
                }
            }
        } catch (err) {
            if (err instanceof OTError) {
                otLogger.warn('$badge{UndoManager} transformOperations: transformation failed');
                if (err.forceReload) { this.docApp.trigger('docs:reload', new ErrorCode(ClientError.ERROR_CLIENT_OT_UNRESOLVABLE_OP_CONFLICT_ERROR)); }
                return {};
            } else {
                otLogger.exception(err, '$badge{UndoManager} transformOperations: transformation failed');
            }
            otLogger.warn('$badge{UndoManager} transformOperations: clearing undo stack');
            this.clearUndoActions();
            delete resultDesc.operations;
        }

        return resultDesc;
    }

}
