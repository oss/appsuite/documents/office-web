/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// re-exports =================================================================

export * as is from "@/io.ox/office/tk/algorithms/is";
export * as to from "@/io.ox/office/tk/algorithms/to";
export * as math from "@/io.ox/office/tk/algorithms/math";
export * as coord from "@/io.ox/office/tk/algorithms/coord";
export * as str from "@/io.ox/office/tk/algorithms/string";
export * as re from "@/io.ox/office/tk/algorithms/regexp";
export * as unicode from "@/io.ox/office/tk/algorithms/unicode";
export * as fmt from "@/io.ox/office/tk/algorithms/format";
export * as fun from "@/io.ox/office/tk/algorithms/function";
export * as hash from "@/io.ox/office/tk/algorithms/hash";
export * as itr from "@/io.ox/office/tk/algorithms/iterable";
export * as ary from "@/io.ox/office/tk/algorithms/array";
export * as map from "@/io.ox/office/tk/algorithms/map";
export * as set from "@/io.ox/office/tk/algorithms/set";
export * as dict from "@/io.ox/office/tk/algorithms/dict";
export * as pick from "@/io.ox/office/tk/algorithms/pick";
export * as json from "@/io.ox/office/tk/algorithms/json";
export * as uuid from "@/io.ox/office/tk/algorithms/uuid";
export * as proto from "@/io.ox/office/tk/algorithms/prototype";
export * as jpromise from "@/io.ox/office/tk/algorithms/jpromise";
export * as debug from "@/io.ox/office/tk/algorithms/debug";

// types cannot be exported with star-from bulk syntax
export type { Relation, CompareFn, NumberComps } from "@/io.ox/office/tk/algorithms/math";
export type { Point, Polar, Size } from "@/io.ox/office/tk/algorithms/coord";
export type { TokenList } from "@/io.ox/office/tk/algorithms/string";
export type { RomanDepth } from "@/io.ox/office/tk/algorithms/format";
export type { ArrayCallbackFn, ArrayPredicateFn, ReverseOptions, ArrayLoopOptions } from "@/io.ox/office/tk/algorithms/array";
export type { ValOf } from "@/io.ox/office/tk/algorithms/dict";
export type { PromiseExecutorFn, PromiseFulfilFn, PromiseRejectFn } from "@/io.ox/office/tk/algorithms/jpromise";
