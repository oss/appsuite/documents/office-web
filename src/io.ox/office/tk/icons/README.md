# Documents Icons

This package contains icon resource files (SVG and PNG) used to create the SVG sprite, PNG sprites, and CSS code for all icons used in Documents editor applications.

## How to Add or Change Icons

Icon resources will be built an served by dedicated Vite plugins. The only thing to do is adding/updating/removing source images, and adjust some configuration files if needed.

1. Add or change image files: Copy all new or changed source images to the subdirectory [images](`./images`).

   * SVG icons:
     * The icon files must contain the `.svg` file extension.

   * PNG icons:

     * The icon files must be in PNG image format, and must contain the `.png` file extension.
     * There must exist a 16 pixel version of the icon file (icon width _and_ height must be 16 pixels), and the file name must end with `_16.png`.
     * There must exist a 32 pixel version of the icon file (icon width _and_ height must be 32 pixels), and the file name must end with `_32.png`.

2. Update the icon mapping configuration. The files [svg-mapping.yaml](`./svg-mapping.yaml`) and [png-mapping.yaml](`./png-mapping.yaml`) contain the icon definitions as a big dictionary.

   * Each entry maps a unique icon identifier (used in source code) to a source image to be used for that icon.
   * The source image file can be specified directly as string, or as dictionary for localized icons (more details below).
   * Only the base name of the source image file must be specified relative to the "images" directory, i.e. it must not contain the size suffix (_PNG only_, e.g. `_16`, `_32`), nor a file extension (`.svg` or `.png`). _Example:_ `"path/to/OX_bold03"` may refer to the SVG file _./images/path/to/bold03.svg_, or to the PNG files _./images/path/to/bold03_16.png_ and _./images/path/to/bold03_32.png_.
   * Localized icons will be described by a dictionary mapping the ISO language identifiers (as comma-separated strings) to the image base names (as described above). The special locale code `"*"` is mandatory, and defines a default icon for unlisted locales. See example below.

3. Commit and push everything.

## Examples

### Example 1: Simple SVG Icon

Assign the icon with the identifier `my-icon` to the SVG image `images/path/to/image1.svg`.

```YAML
# svg-mapping.yaml

my-icon: path/to/image1
```

In source code, the icon can be used with the identifier `"svg:my-icon"`.

### Example 2: Simple PNG Icon

Assign the icon with the identifier `my-icon` to the PNG images `images/path/to/bitmap1_16.png` (16x16 pixels) and `images/path/to/bitmap1_32.png` (32x32 pixels).

```YAML
# png-mapping.yaml

my-icon: path/to/bitmap1
```

In source code, the icon can be used with the identifier `"png:my-icon"`.

### Example 3: Localized SVG Icon

* Assign the icon with the identifier `my-icon` to the SVG image `images/path/to/image1.svg` by default.
* Use the SVG image `images/path/to/image2.svg` in German and French UI.

```YAML
# svg-mapping.yaml

my-icon:
  "*":    path/to/image1
  de,fr:  path/to/image2
```

In source code, the icon can be used with the identifier `"svg:my-icon"`.

Applies to PNG icons as well.
