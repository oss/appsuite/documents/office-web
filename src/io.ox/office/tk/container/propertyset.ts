/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { dict } from "@/io.ox/office/tk/algorithms";
import { EventHub } from "@/io.ox/office/tk/events";

// types ======================================================================

/**
 * Configuration for a single property in a `PropertySet`.
 */
export interface PropertyConfig<VT> {

    /**
     * The initial default value of the property.
     */
    def: VT;

    /**
     * A validator function used to check or correct a new value for this
     * property. If omitted, all property values will be accepted as passed.
     *
     * @param value
     *  The new value of the property.
     *
     * @returns
     *  The original or corrected value for the property to be inserted into
     *  the property set; or `undefined` to ignore the new value and keep the
     *  current value.
     */
    validate?(value: VT): Opt<VT>;

    /**
     * A custom comparator function for the property values. Will be used to
     * determine, if an property value has actually been changed in order to
     * reduce the amount of change events fired. If omitted, the strict
     * equality operator `===` will be used to compare the values.
     */
    equals?(value1: VT, value2: VT): boolean;
}

/**
 * The full type-safe configuration of all properties in a `PropertySet`.
 */
export type PropertyConfigMap<SetT extends object> = {
    [KT in keyof SetT]: PropertyConfig<SetT[KT]>;
};

/**
 * Optional parameters for changing the values in a `PropertySet`.
 */
export interface PropertyUpdateOptions {

    /**
     * If set to `true`, the property values will be updated and a change event
     * will be fired without checking if their values will actually change.
     * Default value is `false`.
     */
    force?: boolean;
}

/**
 * Type mapping for the events emitted by a `PropertySet` instance.
 */
export interface PropertySetEventMap<SetT extends object> {

    /**
     * Will be emitted after some property values have been updated through the
     * setter methods of the property set.
     *
     * @param newProps
     *  A dictionary containing the *new* values of all *changed* properties.
     *
     * @param oldProps
     *  A dictionary containing the *old* values of all *changed* properties.
     */
    "change:props": [newProps: Readonly<Partial<SetT>>, oldProps: Readonly<Partial<SetT>>];
}

// private types --------------------------------------------------------------

// helper function that implements changing one property, used by the setter implementation
type PropSetterFn<SetT extends object> = <KT extends keyof SetT>(key: KT, value: SetT[KT]) => void;

// helper function that takes a setter (`PropSetterFn`), and implements changing multiple properties
type PropUpdateFn<SetT extends object> = (setterFn: PropSetterFn<SetT>) => void;

// class PropertySet ==========================================================

/**
 * A set of type-safe properties for arbitrary use.
 *
 * This class is an `EventHub` that emits a "change:props" event after some
 * property values have been updated through the setter methods. Event handlers
 * receive a dictionary containing the new values of all *changed* properties.
 */
export class PropertySet<SetT extends object> extends EventHub<PropertySetEventMap<SetT>> {

    // the configurations of all supported properties
    readonly #configs: PropertyConfigMap<SetT>;

    // the current values of the properties
    readonly #props: SetT;

    // constructor ------------------------------------------------------------

    constructor(configs: PropertyConfigMap<SetT>) {
        super();

        // the configurations of all supported properties
        this.#configs = configs;

        // initialize the property set from the default values in the configuration
        this.#props = dict.mapDict(configs, config => config.def) as SetT;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the value of a specific property.
     *
     * @param key
     *  The key of the property to be returned.
     *
     * @returns
     *  A reference to the original property value. Arrays or objects MUST NOT
     *  be changed in-place (this would miss change events). Use the setter
     *  methods instead!
     */
    get<KT extends keyof SetT>(key: KT): SetT[KT] {
        return this.#props[key];
    }

    /**
     * Returns the values of all properties in a plain dictionary.
     *
     * @returns
     *  A dictionary containing all property values. The return value is a flat
     *  clone of the internal dictionary. It may be changed by itself
     *  (assigning, adding, or removing properties), but the returned array or
     *  object values in the dictionary MUST NOT be changed in-place (this
     *  would miss change events). Use the setter methods instead!
     */
    all(): SetT {
        return { ...this.#props };
    }

    /**
     * Changes the value of a specific property. Fires a change event, if the
     * property value has been changed (or if the option `force` has been
     * passed).
     *
     * @param key
     *  The key of the property to be updated. If the passed key is unknown
     *  (i.e., not contained in the property configuration passed to the
     *  constructor), nothing will be done.
     *
     * @param value
     *  The new value for the property.
     *
     * @param [options]
     *  Optional parameters.
     */
    set<KT extends keyof SetT>(key: KT, value: SetT[KT], options?: PropertyUpdateOptions): void {
        this.#process(setterFn => setterFn(key, value), options);
    }

    /**
     * Changes the values of multiple properties. Fires a change event, if the
     * value of at least one property has been changed (or if the option
     * `force` has been passed).
     *
     * @param values
     *  The new values for the properties to be changed, mapped by their keys.
     *  Unknown keys (i.e., not contained in the property configuration passed
     *  to the constructor) will be ignored.
     *
     * @param [options]
     *  Optional parameters.
     */
    update(values: Partial<SetT>, options?: PropertyUpdateOptions): void {
        this.#process(setterFn => {
            dict.forEach(values, (value, key) => {
                if (value !== undefined) { setterFn(key, value!); }
            });
        }, options);
    }

    // private methods --------------------------------------------------------

    /**
     * Shared implementation of the public setter methods. The passed callback
     * function receives a setter function that can be called repeatedly with
     * all properties to be updated. Change detection and event handling is
     * done internally.
     */
    #process(updateFn: PropUpdateFn<SetT>, options?: PropertyUpdateOptions): void {

        // all changed properties, for event listeners
        const newProps = dict.createPartial<SetT>();
        const oldProps = dict.createPartial<SetT>();
        // whether to force update and notification
        const force = options?.force;
        // whether at least one property has changed
        let changed = false;

        // invoke the passed callback function that will call the setter function passed in
        updateFn((key, value) => {

            // execute validator
            const config = this.#configs[key];
            const validateFn = config.validate;
            const newValue = validateFn ? validateFn(value) : value;
            if (newValue === undefined) { return; }

            // change the property value
            const oldValue = this.#props[key];
            const equalsFn = force ? undefined : config.equals;
            if (force || !(equalsFn ? equalsFn(oldValue, newValue) : (oldValue === newValue))) {
                this.#props[key] = newProps[key] = newValue;
                oldProps[key] = oldValue;
                changed = true;
            }
        });

        // fire change event if any property has changed
        if (changed) { this.emit("change:props", newProps, oldProps); }
    }
}
