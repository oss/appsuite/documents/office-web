/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// class KeySet ===============================================================

/**
 * A special set that stores `Keyable` elements by their "key" property.
 */
export class KeySet<VT extends Keyable> extends Set<VT> {

    // value storage (base class `Set` remains empty all the time!)
    readonly #store = new Map<string, VT>();

    // constructor ------------------------------------------------------------

    constructor(source?: Iterable<VT>) {
        super();
        if (source) {
            for (const value of source) {
                this.#store.set(value.key, value);
            }
        }
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the number of elements in this set.
     */
    override get size(): number {
        return this.#store.size;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this set contains an element identified by the key of
     * the passed index value.
     *
     * @param index
     *  The index value to be looked up.
     *
     * @returns
     *  Whether this set contains the specified element.
     */
    override has(index: Keyable): boolean {
        return this.#store.has(index.key);
    }

    /**
     * Returns the element with the specified key from this set.
     *
     * This method is a non-standard extension that is not part of class `Set`.
     *
     * @param key
     *  The key of the element to be returned.
     *
     * @returns
     *  The element with the specified key from this set.
     */
    get(key: string): Opt<VT> {
        return this.#store.get(key);
    }

    /**
     * Returns the element from this set with the same key as the passed index
     * value.
     *
     * This method is a non-standard extension that is not part of class `Set`.
     *
     * @param index
     *  The index value to be used for lookup of a set element.
     *
     * @returns
     *  The element from this set with the same key as the passed index value.
     */
    at(index: Keyable): Opt<VT> {
        return this.#store.get(index.key);
    }

    /**
     * Adds an element to this set. Another element with the same key will be
     * overwritten.
     *
     * @param value
     *  The element to be inserted into this set.
     *
     * @returns
     *  A reference to this instance.
     */
    override add(value: VT): this {
        this.#store.set(value.key, value);
        return this;
    }

    /**
     * Removes the element identified by the key of the passed index value from
     * this set.
     *
     * @param index
     *  The index value to identify the element to be removed from this set.
     *
     * @returns
     *  Whether this set has contained an element that has been removed.
     */
    override delete(index: Keyable): boolean {
        return this.#store.delete(index.key);
    }

    /**
     * Removes all elements from this set.
     */
    override clear(): void {
        this.#store.clear();
    }

    /**
     * Returns an iterator for the elements in this set.
     *
     * @returns
     *  An iterator for the elements in this set.
     */
    override [Symbol.iterator](): IterableIterator<VT> {
        return this.#store.values();
    }

    /**
     * Invokes the passed callback function for all elements in this set.
     *
     * @param fn
     *  The callback function that will be invoked for each element.
     *
     * @param [context]
     *  The calling context for the callback function.
     */
    override forEach(fn: (value: VT, key: VT, set: this) => void, context?: object): void {
        for (const value of this.#store.values()) {
            fn.call(context, value, value, this);
        }
    }

    /**
     * Returns an iterator for the entries (pairs of elements) in this set.
     *
     * @yields
     *  The entries (pairs of elements) in this set.
     */
    override *entries(): IterableIterator<Pair<VT>> {
        for (const value of this.#store.values()) {
            yield [value, value];
        }
    }

    /**
     * Returns an iterator for the elements in this set.
     *
     * @yields
     *  The values in this set.
     */
    override *values(): IterableIterator<VT> {
        yield* this.#store.values();
    }

    /**
     * Returns an iterator for the keys (same as values) in this set.
     *
     * @yields
     *  The keys in this set (same as values).
     */
    override *keys(): IterableIterator<VT> {
        yield* this.#store.values();
    }
}
