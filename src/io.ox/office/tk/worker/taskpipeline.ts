/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, jpromise } from "@/io.ox/office/tk/algorithms";
import { type DObjectConfig, DObject } from "@/io.ox/office/tk/objects";
import { PromiseExecutor, AsyncWorker } from "@/io.ox/office/tk/worker/asyncworker";

// types ======================================================================

/**
 * Configuration options for a `TaskPipeline` instance.
 */
export interface TaskPipelineConfig extends DObjectConfig {

    /**
     * Initial delay before starting to process the first entry in the task
     * pipeline. By default, the pipeline will start processing without delay.
     */
    delay?: number;

    /**
     * Whether to set the pipeline into paused mode initially (pushing tasks
     * will not start the pipeline). Default value is `false`.
     */
    paused?: boolean;
}

/**
 * The callback function representing an asynchronous task in the pipeline.
 *
 * @returns
 *  The result value of the task. If the task performs asynchronous work, it
 *  shall return a promise.
 */
export type AsyncTaskFn<RT> = () => MaybeAsync<RT>;

/**
 * Optional parameters for the method `TaskPipeline#pushTask`.
 */
export interface AsyncTaskOptions {

    /**
     * If set to `true`, the task will be executed immediately after the task
     * currently running, but before all other pending tasks. Default value is
     * `false`, i.e. the task will be pushed to the end of the queue.
     */
    priority?: boolean;
}

// class TaskQueueEntry =======================================================

class TaskQueueEntry<RT> extends PromiseExecutor<RT> {
    readonly taskFn: AsyncTaskFn<RT>;
    constructor(taskFn: AsyncTaskFn<RT>) {
        super();
        this.taskFn = taskFn;
    }
}

// class TaskPipeline =========================================================

/**
 * A task pipeline contains a queue that stores multiple "tasks" (i.e. callback
 * functions) and executes these (asynchronous) tasks sequentially. More tasks
 * can be registered at any time.
 */
export class TaskPipeline extends DObject implements Abortable {

    // the queue with all pending tasks
    readonly #queue: Array<TaskQueueEntry<any>> = [];
    // the worker implementing the asynchronous task loop
    readonly #worker: AsyncWorker;
    // whether processing has been paused temporarily
    #paused: boolean;
    // error state after failed task
    #error = false;

    // constructor ------------------------------------------------------------

    constructor(config?: TaskPipelineConfig) {
        super(config);

        // create the asynchronous worker with abort support etc.
        this.#worker = this.member(new AsyncWorker({ ...config, slice: 0 }));
        // initialize paused mode
        this.#paused = !!config?.paused;

        // add an initial delay before starting to process the pipeline
        if (config?.delay) {
            this.#worker.steps.addDelay(config.delay);
        }

        // add iterator step that processes all available tasks in the queue
        this.#worker.steps.addStep(runner => {

            // use an iterator that shifts the values from the array
            return runner.iterate(ary.shiftValues(this.#queue), entry => {

                // await the result of the task function, forward to exported task promise
                const promise = jpromise.invoke(entry.taskFn);
                runner.onFulfilled(promise, result => entry.resolve(result));
                runner.onRejected(promise, reason => entry.reject(reason));

                // let the runner wait for the current task
                return promise;
            });
        });
    }

    protected override destructor(): void {
        this.#queue.length = 0;
        super.destructor();
    }

    // public getters ---------------------------------------------------------

    /**
     * Specifies whether this task pipeline is currently executing tasks.
     *
     * @returns
     *  Whether this task pipeline is currently executing tasks.
     */
    get running(): boolean {
        return this.#worker.running;
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a new task to be executed by this task pipeline. If it is
     * currently running, the task will be pushed onto the internal queue, and
     * will be executed after the other registered tasks.
     *
     * @param taskFn
     *  The task to be executed. If the function returns a pending promise,
     *  this instance will wait for its settlement before continuing with the
     *  next pending task in the queue.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A pending promise that will be resolved with the result of the callback
     *  function.
     */
    pushTask<RT>(taskFn: AsyncTaskFn<RT>, options?: AsyncTaskOptions): Promise<RT> {

        // create the queue entry with own deferred object to be resolved after the task has finished
        const entry = new TaskQueueEntry<RT>(taskFn);

        // insert the task at the end (or optionally at the beginning) of the queue
        if (options?.priority) {
            this.#queue.unshift(entry);
        } else {
            this.#queue.push(entry);
        }

        // start the task pipeline automatically
        this.#tryStart();

        return entry.promise;
    }

    /**
     * Stops processing of existing pending tasks in the queue. Pushing more
     * tasks will not start the pipeline automatically.
     */
    pause(): void {
        this.#worker.abort();
        this.#paused = true;
    }

    /**
     * Resumes processing of existing pending tasks in the queue. Pushing more
     * tasks will start the pipeline automatically.
     */
    resume(): void {
        this.#paused = false;
        this.#tryStart();
    }

    /**
     * Aborts this task pipeline immediately, and clears the internal queue.
     */
    abort(): void {
        this.#worker.abort();
        this.#queue.length = 0;
        this.#error = false;
    }

    // private methods --------------------------------------------------------

    /**
     * Starts this task pipeline, if it is not running and the queue contains
     * pending tasks.
     */
    #tryStart(): void {

        // nothing to do, if the task pipeline is running, or if there are no tasks waiting
        if (this.#paused || this.#error || this.#worker.running || !this.#queue.length) { return; }

        // Start the task pipeline. After completion, immediately try to restart the task pipeline. This is needed in
        // case a new task has been registered in the time between finishing the last task, and switching the worker to
        // pending state. This may take a few ticks due to internal promise handling. In this short time frame, the
        // call of `#tryStart` from `pushTask` would find a still running worker that would not be restarted then.
        this.#worker.start().then(() => this.#tryStart()).catch(() => { this.#error = true; });
    }
}
