/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import { type DObjectConfig, DObject } from "@/io.ox/office/tk/objects";
import { AsyncWorker } from "@/io.ox/office/tk/worker/asyncworker";

// types ======================================================================

/**
 * The callback function to be called repeatedly by an `IntervalRunner`.
 *
 * This function will be passed to `AsyncWorker#addLoop` thus it may call the
 * function `breakLoop` to abort the runner.
 *
 * @param index
 *  The zero-based index of the current iteration.
 *
 * @returns
 *  If the task performs asynchronous work, it shall return a promise. The
 *  runner will wait for its settlement before continuing with the loop.
 */
export type IntervalTaskFn = (index: number) => MaybeAsync;

/**
 * Configuration settings for the `IntervalRunner` class.
 */
export interface IntervalRunnerConfig extends DObjectConfig {

    /**
     * The initial delay time (in milliseconds) before starting the task the
     * first time. Default value is `0`.
     */
    delay?: number;

    /**
     * The interval time (in milliseconds) between two invocations of the task.
     */
    interval: number;

    /**
     * The maximum number of iterations to be executed. If omitted, the runner
     * will run forever (until the function `breakLoop` has been called).
     */
    count?: number;
}

// class IntervalRunner =======================================================

/**
 * An interval runner executes the same "task" (i.e. callback function) over
 * and over again in a defined time interval.
 *
 * @param taskFn
 *  The task callback function to be invoked repeatedly.
 *
 * @param [config]
 *  The timing configuration for the runner.
 */
export class IntervalRunner extends DObject implements Abortable {

    // the worker implementing the asynchronous task loop
    readonly #worker = this.member(new AsyncWorker({ interval: 0 }));

    // constructor ------------------------------------------------------------

    constructor(taskFn: IntervalTaskFn, config: IntervalRunnerConfig | number) {

        // normalize configuration
        if (is.number(config)) { config = { interval: config }; }

        super(config);

        // initial delay
        if (config?.delay) { this.#worker.steps.addDelay(config.delay); }

        // repeatedly invoke the task function
        this.#worker.steps.addLoop(index => taskFn(index), { ...config, slice: 0 });
    }

    // public getters ---------------------------------------------------------

    /**
     * Specifies whether this instance is currently running.
     *
     * @returns
     *  Whether this instance is currently running.
     */
    get running(): boolean {
        return this.#worker.running;
    }

    // public methods ---------------------------------------------------------

    /**
     * Starts this runner unless it is already running.
     */
    start(): void {
        if (!this.running) {
            jpromise.floating(this.#worker.start());
        }
    }

    /**
     * Aborts this runner immediately.
     */
    abort(): void {
        this.#worker.abort();
    }

    /**
     * Toggles the running state of this runner.
     *
     * @param state
     *  Whether to start (`true`) or abort (`false`) the runner.
     */
    toggle(state: boolean): void {
        if (state) { this.start(); } else { this.abort(); }
    }
}
