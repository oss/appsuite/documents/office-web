/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, jpromise } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/objects";
import { type AsyncWorkerConfig, AsyncWorker } from "@/io.ox/office/tk/worker/asyncworker";

// types ======================================================================

/**
 * The callback function to be called repeatedly by a `DataPipeline`.
 *
 * @param value
 *  The data entry from the queue to be processed.
 *
 * @param index
 *  The zero-based index of the data entry.
 *
 * @returns
 *  If the task performs asynchronous work, it shall return a promise. The
 *  runner will wait for its settlement before continuing with the loop.
 */
export type PipelineTaskFn<T> = (value: T, index: number) => MaybeAsync;

/**
 * Configuration options for a `DataPipeline` instance.
 */
export interface DataPipelineConfig extends AsyncWorkerConfig {

    /**
     * Initial delay before starting to process the first entry in the data
     * pipeline. By default, the pipeline will start processing without delay.
     */
    delay?: number;

    /**
     * Whether to set the pipeline into paused mode initially (pushing values
     * will not start the pipeline). Default value is `false`.
     */
    paused?: boolean;
}

// class DataPipeline =========================================================

/**
 * A data pipeline contains a queue that stores multiple data entries, and
 * executes a callback for each of the entries sequentially. More data entries
 * can be registered at any time.
 */
export class DataPipeline<T> extends DObject implements Abortable {

    // the queue with all pending data entries
    readonly #queue: T[] = [];
    // the worker implementing the asynchronous task loop
    readonly #worker: AsyncWorker;
    // the global index of the next data entry
    #index = 0;
    // whether processing has been paused temporarily
    #paused: boolean;

    // constructor ------------------------------------------------------------

    constructor(taskFn: PipelineTaskFn<T>, config?: DataPipelineConfig) {
        super(config);

        // create the asynchronous worker with abort support etc.
        this.#worker = this.member(new AsyncWorker(config));
        // initialize paused mode
        this.#paused = !!config?.paused;

        // add an initial delay before starting to process the pipeline
        if (config?.delay) {
            this.#worker.steps.addDelay(config.delay);
        }

        // add iterator step that processes all available entries in the queue
        this.#worker.steps.addStep(runner => {

            // use an iterator that shifts the values from the array
            return runner.iterate(ary.shiftValues(this.#queue), value => {

                // invoke the task function
                const promise = jpromise.invoke(taskFn, value, this.#index);
                this.#index += 1;

                // do not break the pipeline on caught errors
                return jpromise.fastCatch(promise);
            });
        });
    }

    protected override destructor(): void {
        this.#queue.length = 0;
        super.destructor();
    }

    // public getters ---------------------------------------------------------

    /**
     * Specifies whether this data pipeline is currently processing values.
     */
    get running(): boolean {
        return this.#worker.running;
    }

    /**
     * A readonly array of all pending values currently stored in the queue.
     */
    get values(): readonly T[] {
        return this.#queue;
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a new value to be processed by this data pipeline. If it is
     * currently running, the value will be pushed onto the internal queue, and
     * will be processed after the other registered values.
     *
     * @param value
     *  The value to be processed.
     */
    pushValue(value: T): void {

        // insert the value into the queue
        this.#queue.push(value);

        // start the data pipeline automatically
        this.#tryStart();
    }

    /**
     * Registers multiple values to be processed by this data pipeline. If it
     * is currently running, the values will be pushed onto the internal queue,
     * and will be processed after the other registered values.
     *
     * @param source
     *  The values to be processed.
     */
    pushValues(source: Iterable<T>): void {

        // insert the values into the queue
        for (const value of source) {
            this.#queue.push(value);
        }

        // start the data pipeline automatically
        this.#tryStart();
    }

    /**
     * Stops processing of existing pending values in the queue. Pushing more
     * values will not start the pipeline automatically.
     */
    pause(): void {
        this.#worker.abort();
        this.#paused = true;
    }

    /**
     * Resumes processing of existing pending values in the queue. Pushing more
     * values will start the pipeline automatically.
     */
    resume(): void {
        this.#paused = false;
        this.#tryStart();
    }

    /**
     * Aborts this task pipeline immediately, and clears the internal queue.
     */
    abort(): void {
        this.#worker.abort();
        this.#queue.length = 0;
    }

    // private methods --------------------------------------------------------

    /**
     * Starts this data pipeline, if it is not running and the queue contains
     * pending entries.
     */
    #tryStart(): void {

        // nothing to do, if the pipeline is running, or if there are no tasks waiting
        if (this.#paused || this.#worker.running || !this.#queue.length) { return; }

        // Start the pipeline. After completion, immediately try to restart the pipeline. This is needed in case a new
        // value has been registered in the time between finishing the last queue entry, and switching the worker to
        // pending state. This may take a few ticks due to internal promise handling. In this short time frame, the
        // call of `#tryStart` from `pushValue` would find a still running worker that would not be restarted then.
        jpromise.floating(this.#worker.start().then(() => this.#tryStart()));
    }
}
