/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, jpromise } from "@/io.ox/office/tk/algorithms";
import type { DObjectConfig, AsyncTimingOptions } from "@/io.ox/office/tk/objects";
import { BreakLoopToken, breakLoop, AbortError, EObject } from "@/io.ox/office/tk/objects";

// re-exports =================================================================

export { BreakLoopToken, breakLoop };

// types ======================================================================

/**
 * Configuration settings for an asynchronous worker instance.
 */
export interface AsyncWorkerConfig extends DObjectConfig, AsyncTimingOptions { }

/**
 * A callback function that will be invoked as a single step of an asynchronous
 * worker (a "worker step").
 *
 * @param runner
 *  The runner instance representing the execution cycle of an asynchronous
 *  worker. Its lifetime is bound strictly to that execution cycle. Contains
 *  useful methods for running asynchronous code and timers.
 */
export type WorkerStepFn<MT> = (runner: AsyncRunner<MT>) => MaybeAsync;

/**
 * A callback function that will be invoked repeatedly in an asynchronous loop
 * (a "worker loop step"). May call the function `breakLoop` to indicate to
 * leave the loop early without stopping the worker.
 *
 * @param index
 *  The zero-based index of the current iteration.
 *
 * @param runner
 *  The runner instance representing the execution cycle of an asynchronous
 *  worker. Its lifetime is bound strictly to that execution cycle. Contains
 *  useful methods for running asynchronous code and timers.
 */
export type WorkerLoopStepFn<MT> = (index: number, runner: AsyncRunner<MT>) => MaybeAsync;

/**
 * Optional parameters for a worker loop step that will be invoked repeatedly.
 */
export interface WorkerLoopOptions extends AsyncTimingOptions {

    /**
     * The maximum number of iterations to be executed. If omitted, the loop
     * will run forever (until the function `breakLoop` has been called).
     */
    count?: number;
}

/**
 * A callback function that will be invoked repeatedly in an asynchronous loop
 * (a "worker loop step") with the values provided by an iterator.
 *
 * May call the function `breakLoop` to indicate to leave the loop early
 * without stopping the worker. In this case, the result value of the worker
 * step will be the return value of the preceding invocation of the callback
 * function.
 *
 * @param value
 *  The value currently visited by the iterator.
 *
 * @param runner
 *  The runner instance representing the execution cycle of an asynchronous
 *  worker. Its lifetime is bound strictly to that execution cycle. Contains
 *  useful methods for running asynchronous code and timers.
 */
export type WorkerIteratorStepFn<MT, VT> = (value: VT, runner: AsyncRunner<MT>) => MaybeAsync;

/**
 * A result object that will be emitted in a "worker:finish" event of an
 * `AsyncWorker` if it has been finished its last worker step successfully.
 */
export interface AsyncWorkerDoneResult {
    state: "done";
}

/**
 * A result object that will be emitted in a "worker:finish" event of an
 * `AsyncWorker` if a worker step fails during execution of a worker. Contains
 * the rejection value of the failed worker step.
 */
export interface AsyncWorkerFailResult {
    state: "fail";
    error: unknown;
}

/**
 * A result object that will be emitted in a "worker:finish" event of an
 * `AsyncWorker` if the worker execution has been aborted. Contains a boolean
 * value specifying whether the worker is being destroyed.
 */
export interface AsyncWorkerAbortResult {
    state: "abort";
    destroyed: boolean;
}

/**
 * A result object that will be emitted in a "worker:finish" event of an
 * `AsyncWorker` if it has been finished.
 */
export type AsyncWorkerResult = AsyncWorkerDoneResult | AsyncWorkerFailResult | AsyncWorkerAbortResult;

/**
 * Type mapping for the events emitted by `AsyncWorker` instances.
 */
export interface AsyncWorkerEventMap {

    /**
     * Will be emitted when a worker starts executing its steps.
     */
    "worker:start": [];

    /**
     * Will be emitted when a worker has finished executing its steps.
     *
     * @param result
     *  The result object.
     */
    "worker:finish": [result: AsyncWorkerResult];
}

// class PromiseExecutor ======================================================

/**
 * Small wrapper object for a native promise that allows resolving or rejecting
 * from outside.
 */
export class PromiseExecutor<RT> {

    readonly promise: Promise<RT>;
    readonly resolve!: (value: RT) => void;
    readonly reject!: (error: unknown) => void;

    constructor() {
        this.promise = new Promise((resolve, reject) => {
            (this as Writable<this>).resolve = resolve;
            (this as Writable<this>).reject = reject;
        });
    }
}

// class WorkerError ==========================================================

/**
 * All real errors thrown by asynchronous worker.
 */
export class WorkerError extends Error {
    override readonly name = "WorkerError";
}

// class AsyncRunner ==========================================================

/**
 * A runner instance represents a single execution cycle of an asynchronous
 * worker. Its lifetime is bound strictly to that execution cycle.
 *
 * Useful for implementing the worker steps, especially asynchronous code,
 * timers, and loops. Aborting a worker execution cycle means destroying the
 * runner instance.
 *
 * @template MT
 *  The type of the custom metadata.
 */
export class AsyncRunner<MT> extends EObject implements Abortable {

    /**
     * Custom metadata, as passed to the worker method `start`.
     */
    readonly metadata: MT;

    // own configuration options passed to constructor
    readonly #config?: AsyncWorkerConfig;

    // pending worker to be awaited
    #worker: Opt<AsyncWorker<any>>;

    // constructor ------------------------------------------------------------

    constructor(metadata: MT, config?: AsyncWorkerConfig) {
        super({ _kind: "private" });
        this.metadata = metadata;
        this.#config = config;
    }

    override destructor(): void {
        this.abort();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Executes the passed worker.
     *
     * @param worker
     *  The callback function to be invoked. If it returns an `AsyncWorker`
     *  instance, it will be started, and its result will be awaited.
     *
     * @param metadata
     *  The metadata value to be passed to the worker.
     *
     * @returns
     *  A promise representing the execution cycle of the worker.
     */
    start<VT>(worker: AsyncWorker<VT>, metadata: VT): Promise<void> {
        this.#worker = worker;
        const promise = worker.start(metadata);
        this.onSettled(promise, () => { this.#worker = undefined; });
        return promise;
    }

    /**
     * Invokes the passed callback function repeatedly in an asynchronous loop.
     * Tries to pack multiple (synchronous) invocations of the callback into
     * the same script execution frame, until the specified total execution
     * time of a time slice is exceeded, and continues with a new time slice in
     * another browser timeout afterwards.
     *
     * @param fn
     *  The callback function to be invoked repeatedly in an asynchronous loop.
     *  Receives the zero-based loop index as first parameter.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise representing the state of the asynchronous loop.
     */
    loop(fn: (index: number) => MaybeAsync, options?: WorkerLoopOptions): JPromise {
        return this.asyncLoop(fn, { ...this.#config, ...options });
    }

    /**
     * Invokes the passed callback function for each value of the iterable data
     * source in an asynchronous loop. Tries to pack multiple (synchronous)
     * invocations of the callback into the same script execution frame, until
     * the specified total execution time of a time slice is exceeded, and
     * continues with a new time slice in another browser timeout afterwards.
     *
     * @param source
     *  The data source to be processed.
     *
     * @param fn
     *  The callback function to be invoked for each value in the data source
     *  in an asynchronous loop. Receives the value as first parameter.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise representing the state of the asynchronous loop.
     */
    iterate<VT>(source: Iterable<VT>, fn: (value: VT) => MaybeAsync, options?: WorkerLoopOptions): JPromise {
        return this.asyncForEach(source, fn, { ...this.#config, ...options });
    }

    /**
     * Aborts the running sub-worker this instance is waiting for.
     */
    abort(): void {
        this.#worker?.abort();
        this.#worker = undefined;
    }
}

// class AsyncWorkerSteps =====================================================

/**
 * A container for the list of asynchronous actions (called "worker steps") of
 * an `AsyncWorker` instance.
 */
export class AsyncWorkerSteps<MT> implements Iterable<WorkerStepFn<MT>> {

    // all registered worker steps
    readonly #steps: Array<WorkerStepFn<MT>> = [];

    // public methods ---------------------------------------------------------

    /**
     * Defines a worker step that waits the specified time.
     *
     * @param delay
     *  The delay time (in milliseconds) to postpone execution of the following
     *  worker steps. May be a callback function to be able to implement
     *  different delay times per worker cycle.
     */
    addDelay(delay: number | FuncType<number>): void {
        this.#steps.push(runner => new Promise<void>(resolve => {
            runner.setTimeout(resolve, is.function(delay) ? delay() : delay);
        }));
    }

    /**
     * Defines a worker step that invokes the specified callback function.
     *
     * @param step
     *  One of the following values:
     *  - A callback function representing the worker step.
     *  - An `AsyncWorker` instance to be started.
     */
    addStep(step: WorkerStepFn<MT> | AsyncWorker): void {
        this.#steps.push(is.function(step) ? step : (runner => runner.start(step, undefined)));
    }

    /**
     * Defines a worker step that invokes the specified callback function
     * repeatedly in an asynchronous loop (a "worker loop step").
     *
     * Worker loop steps try to pack multiple (synchronous) invocations of the
     * callback into the same script execution frame, until the specified total
     * execution time of a time slice is exceeded, and continues with a new
     * time slice in another browser timeout afterwards.
     *
     * @param loopStep
     *  One of the following values:
     *  - A callback function representing the worker step. Will be invoked
     *    repeatedly in an asynchronous loop. Receives the step index as first
     *    parameter.
     *  - An `AsyncWorker` instance to be started repeatedly in an asynchronous
     *    loop. Receives the step index as metadata value.
     *
     * @param [options]
     *  Optional parameters.
     */
    addLoop(
        loopStep: WorkerLoopStepFn<MT> | AsyncWorker<number>,
        options?: WorkerLoopOptions
    ): void {

        // convert worker to step function
        const stepFn: WorkerLoopStepFn<MT> = is.function(loopStep) ? loopStep :
            ((index, runner) => runner.start(loopStep, index));

        // add worker step that runs the loop until it will be aborted
        this.#steps.push(runner => runner.loop(index => stepFn(index, runner), options));
    }

    /**
     * Defines a worker step that calls a generator function returning an
     * iterable, and afterwards invokes the step function repeatedly in an
     * asynchronous loop for all values provided by the iterable.
     *
     * See method `addLoop` for more details about timing behavior.
     *
     * @param iterableFn
     *  The callback function that returns the iterable to be processed.
     *
     * @param loopStep
     *  One of the following values:
     *  - A callback function representing the worker step. Will be invoked
     *    repeatedly in an asynchronous loop for all values of the iterator.
     *  - An `AsyncWorker` instance to be started repeatedly. Receives the
     *    iterator value as metadata value.
     *
     * @param [options]
     *  Optional parameters.
     */
    addIterator<VT>(
        iterableFn: FuncType<Iterable<VT>>,
        loopStep: WorkerIteratorStepFn<MT, VT> | AsyncWorker<VT>,
        options?: WorkerLoopOptions
    ): void {

        // convert worker to step function
        const stepFn: WorkerIteratorStepFn<MT, VT> = is.function(loopStep) ? loopStep :
            ((value, runner) => runner.start(loopStep, value));

        // add worker step that creates a new iterator, and processes all iterator values
        this.#steps.push(runner => runner.iterate(
            iterableFn(),
            value => stepFn(value, runner),
            options
        ));
    }

    /**
     * Returns an iterator that visits the registered worker steps.
     *
     * @returns
     *  An iterator that visits the registered worker steps.
     */
    [Symbol.iterator](): IterableIterator<WorkerStepFn<MT>> {
        return this.#steps.values();
    }
}

// class AsyncWorker ==========================================================

/**
 * An asynchronous worker contains a list of asynchronous actions (called
 * "worker steps") that can be executed multiple times. A running worker can be
 * aborted immediately at any time.
 *
 * After construction, a worker is in "idle state" and needs to be filled with
 * the worker steps, using the method `build`. This method must be called once
 * before starting the worker for the first time.
 *
 * A worker can be started with the method `start`. This method sets the worker
 * to "running state", emits a "worker:start" event, invokes all registered
 * worker steps one after the other, takes care for asynchronous callback
 * functions, delays, etc, and returns a pending promise representing the
 * running worker.
 *
 * After executing the last worker step, a "worker:finish" event will be
 * emitted, the promise returned from method `start` will be fulfilled, and the
 * worker returns to idle state.
 *
 * If any of the worker steps fails (by throwing an exception, or returning a
 * promise that rejects), a "worker:finish" event with the exception or
 * rejection value will be emitted, the promise returned from method `start`
 * will be rejected with that value, and the worker returns to idle state.
 *
 * A running worker can be aborted at any time by calling the method `abort`.
 * In this case, a "worker:finish" event with the abort information will be
 * emitted, the promise returned from method `start` will be rejected with an
 * `AbortError`, and the worker returns to idle state.
 *
 * Destroying a running worker will also abort it immediately (with emitting
 * the "worker:finish" event, and rejecting the promise).
 *
 * @template MT
 *  The type of the custom metadata of a single execution cycle, passed to the
 *  methods `start` or `restart`, and carried in the runner instance.
 */
export class AsyncWorker<MT = void> extends EObject<AsyncWorkerEventMap> implements Abortable {

    /**
     * Registry for the worker steps this instance will execute. Needs to be
     * filled before running the worker for the first time.
     */
    readonly steps = new AsyncWorkerSteps<MT>();

    // configuration settings for this instance
    #config: Opt<AsyncWorkerConfig>;

    // the runner instance that represents the current execution cycle
    #runner: Opt<AsyncRunner<MT>>;

    // the promise executor
    #executor: Opt<PromiseExecutor<void>>;

    // constructor ------------------------------------------------------------

    constructor(config?: AsyncWorkerConfig) {
        super(config);
        this.#config = config;
    }

    protected override destructor(): void {
        // abort running worker
        if (this.running) {
            this.#finalize(false, new AbortError(true));
        }
        super.destructor();
    }

    // public getters ---------------------------------------------------------

    /**
     * Specifies whether this worker is currently in running state.
     *
     * @returns
     *  Whether this worker is currently in running state.
     */
    get running(): boolean {
        return !!this.#runner;
    }

    // public methods ---------------------------------------------------------

    /**
     * Replaces the configuration options passed in the constructor.
     *
     * @param config
     *  The new configuration options to be used by this instance.
     */
    configure(config?: AsyncWorkerConfig): void {
        this.#config = config;
    }

    /**
     * Starts executing all registered steps of this worker. Immediately emits
     * the "worker:start" event. Later on, a "worker:finish" event with the
     * result object will be emitted.
     *
     * @param metadata
     *  Custom metadata for the new execution cycle. Will be carried in the
     *  property "metadata" of the `AsyncRunner` instance passed to all worker
     *  steps.
     *
     * @returns
     *  A pending promise that represents execution of the worker steps. Will
     *  be fulfilled when the worker finishes successfully. Will be rejected
     *  with the exception or rejection value of a failed worker step. Will be
     *  rejected with an `AbortError`, if worker execution has been aborted.
     *
     * @throws
     *  When trying to start a running worker.
     */
    async start(metadata: MT): Promise<void> {
        await this.#run(metadata);
    }

    /**
     * Aborts this worker if it is running (by calling `abort`), and restarts
     * it afterwards (by calling `start`).
     *
     * @param metadata
     *  Custom metadata for the new execution cycle. Will be carried in the
     *  property "metadata" of the `AsyncRunner` instance passed to all worker
     *  steps.
     *
     * @returns
     *  A pending promise that represents execution of the worker steps. See
     *  method `start` for details.
     */
    async restart(metadata: MT): Promise<void> {
        if (this.running) { this.abort(); }
        await this.start(metadata);
    }

    /**
     * Aborts this worker immediately. If the worker is currently running, a
     * "worker:finish" event will be emitted.
     */
    abort(): void {
        this.#finalize(false, new AbortError());
    }

    // private methods --------------------------------------------------------

    /**
     * Finalizes the running worker. Emits a "worker:finish" event, and settles
     * the promise returned from the method `start` representing the worker
     * execution.
     *
     * @param success
     *  Whether to fulfil (`true`), or to reject (`false`) the result promise.
     *
     * @param [error]
     *  The resulting error value (ignored if `success` is `true`). If this
     *  value is an instance of `AbortError`, worker execution will be aborted.
     */
    #finalize(success: true): void;
    #finalize(success: false, error: unknown): void;
    // implementation
    #finalize(success: boolean, result?: unknown): void {

        // nothing to do in idle mode (check both properties for TS type checker)
        const runner = this.#runner;
        const executor = this.#executor;
        if (!runner || !executor) { return; }

        // return to idle state BEFORE notifying listeners, and destroying the runner
        this.#runner = this.#executor = undefined;

        // notify event handlers and promise listeners
        if (success) {
            this.trigger("worker:finish", { state: "done" });
            executor.resolve();
        } else if (result instanceof AbortError) {
            runner.abort();
            this.trigger("worker:finish", { state: "abort", destroyed: result.destroyed });
            executor.reject(result);
        } else {
            this.trigger("worker:finish", { state: "fail", error: result });
            executor.reject(result);
        }

        // destroy the runner instance
        runner.destroy();
    }

    /**
     * Starts executing the worker steps. Later on, a "worker:finish" event
     * will be emitted.
     *
     * @param metadata
     *  Custom metadata for the new execution cycle. Will be carried in the
     *  property "metadata" of the `AsyncRunner` instance passed to all worker
     *  steps.
     *
     * @returns
     *  A pending promise that represents execution of the worker steps. Will
     *  be fulfilled when the worker finishes successfully. Will be rejected
     *  with the exception or rejection value of a failed worker step. Will be
     *  rejected with an `AbortError`, if worker execution has been aborted.
     *
     * @throws
     *  When trying to start a running worker.
     */
    async #run(metadata: MT): Promise<void> {

        // prevent reentrance
        if (this.running) {
            throw new WorkerError("cannot restart a running worker");
        }

        // create a new runner (this sets the worker into running state)
        const runner = this.#runner = new AsyncRunner(metadata, this.#config);

        // create the promise that represents the execution cycle (will be settled in `#finalize`)
        const executor = this.#executor = new PromiseExecutor();

        // notify listeners
        this.trigger("worker:start");

        // start the worker cycle, handle the result (always start in an own timeout)
        runner.setTimeout(() => {
            const promise = runner.asyncForEach(this.steps, stepFn => jpromise.invoke(stepFn, runner), this.#config);
            runner.onFulfilled(promise, () => this.#finalize(true));
            runner.onRejected(promise, reason => this.#finalize(false, reason));
        });

        // await the promise representing the entire execution cycle
        await executor.promise;
    }
}
