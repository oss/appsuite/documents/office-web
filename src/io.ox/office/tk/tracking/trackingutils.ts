/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type Point, coord } from "@/io.ox/office/tk/algorithms";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import type { ResolveDOMEventKeys, ResolveDOMEvent } from "@/io.ox/office/tk/events";

// globals ====================================================================

/**
 * Debug console logger for tracking sequences.
 */
export const trackingLogger = new Logger("office:log-tracking", { tag: "TRACKING", tagColor: 0x00FFFF });

// public functions ===========================================================

/**
 * Converts page coordinates (properties "pageX"  and "pageY") of a mouse event
 * or touch point to a `Point` object.
 */
export function pagePoint(source: MouseEvent | Touch): Point {
    return coord.point(source.pageX, source.pageY);
}

/**
 * Extracts the page coordinates of a single touch point from a touch event.
 */
export function touchPoint(event: TouchEvent, end: boolean): Opt<Point> {
    const { touches, changedTouches } = event;
    const validTouchCount = (touches.length === (end ? 0 : 1)) && (changedTouches.length === 1);
    return validTouchCount ? pagePoint(changedTouches[0]) : undefined;
}

// class EventListenerContainer ===============================================

/**
 * Small helper that collects all event listeners registered at some DOM event
 * targets, and provides a method to remove all registered listeners at once.
 */
export class EventListenerContainer {

    readonly #listeners = new Set<VoidFunction>();

    // public methods ---------------------------------------------------------

    addEventListener<T extends EventTarget, K extends ResolveDOMEventKeys<T>>(target: T, type: K, listener: (event: ResolveDOMEvent<T, K>) => void, options?: AddEventListenerOptions): void {
        target.addEventListener(type, listener as EventListener, options);
        this.#listeners.add(() => target.removeEventListener(type, listener as EventListener, options));
    }

    removeEventListeners(): void {
        for (const fn of this.#listeners) { fn(); }
        this.#listeners.clear();
    }
}
