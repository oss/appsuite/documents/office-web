/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Point } from "@/io.ox/office/tk/algorithms";
import type { ModifierKeys } from "@/io.ox/office/tk/dom";

// types ======================================================================

/**
 * Type specifiers for DOM event families that can initiate tracking sequences.
 *
 * - `"mouse"`: Tracking sequence has started due to a "mousedown" event.
 * - `"touch"`: Tracking sequence has started due to a "touchstart" event.
 */
export type TrackingInputDevice = "mouse" | "touch";

/**
 * All DOM event types causing to emit a tracking observer notification.
 */
export type TrackingInputEvent = MouseEvent | TouchEvent | KeyboardEvent;

/**
 * All type specifiers for tracking notification records.
 */
export type TrackingRecordType = "start" | "move" | "scroll" | "repeat" | "end" | "cancel";

/**
 * Generic properties of all notification records submitted by a tracking
 * observer.
 */
export interface TrackingRecord {

    /**
     * The type specifier of the tracking record.
     */
    type: TrackingRecordType;

    /**
     * Custom identifier passed in the configuration options. Can be used to
     * distinguish between different observed DOM elements.
     */
    id: string;

    /**
     * The observed DOM element that initiated the tracking sequence.
     */
    current: HTMLElement;

    /**
     * The "target" property received from the corresponding original browser
     * GUI event of the "start" notification record. Will be passed to all
     * other notification records of a tracking sequence regardless where the
     * other GUI events (move, etc.) originate from.
     */
    target: EventTarget;

    /**
     * The type of the initial DOM event that caused to start a tracking
     * sequence.
     */
    inputDevice: TrackingInputDevice;

    /**
     * The original browser GUI event (mouse, touch, or keyboard event). Will
     * be `undefined` for the synthetic notifications "cancel", "scroll", and
     * "repeat".
     */
    inputEvent: Opt<TrackingInputEvent>;

    /**
     * Current state flags of all modifier keys (copied from the last original
     * GUI event).
     */
    modifiers: ModifierKeys;

    /**
     * The time stamp when the "start" notification record has been emitted.
     */
    startTime: number;

    /**
     * The duration in milliseconds between start time (as passed in the
     * property `startTime`), and the current time stamp.
     */
    duration: number;

    /**
     * Whether the tracking sequence has emitted at least one "move" record.
     */
    moved: boolean;

    /**
     * The start coordinates (value of property `point` of the "start" record).
     */
    start: Point;

    /**
     * The current page coordinates as received from the corresponding original
     * browser GUI event (mouse or touch). Will not be initialized for "cancel"
     * and "finally" notification records.
     */
    point: Point;

    /**
     * Additional property for "move" and "end" notification records. The
     * difference between the coordinates of the initial "start" record and the
     * current tracking coordinates (i.e. the difference between properties
     * `start` and `point`).
     */
    offset: Point;

    /**
     * Additional property for "move" notification records. The difference
     * between the coordinates of the previous "move" record (or the initial
     * "start" record, if this is the first "move" record) and the current
     * tracking coordinates.
     */
    move: Point;

    /**
     * Additional property for "scroll" notification records. The suggested
     * scrolling distances. This value will start at the minimum distances, and
     * will increase over time to the maximum configured distances.
     */
    scroll: Point;

    /**
     * Additional property for "end" notification records. Specifies whether
     * the tracking observer has detected a double click or double tap (two
     * complete tracking sequences shortly after another on the same page
     * coordinates), according to the configuration.
     */
    dblClick: boolean;

    /**
     * Changes the current mouse pointer shown during the tracking sequence.
     */
    cursor(this: void, cursor: string | null): void;

    /**
     * Cancels the tracking cycle immediately after the handler callback of
     * this notification record has finished. Does not have any effect for
     * "end" and "cancel" notification records.
     */
    cancel(this: void): void;
}
