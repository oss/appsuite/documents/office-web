/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, dict } from "@/io.ox/office/tk/algorithms";
import { trackingLogger } from "@/io.ox/office/tk/tracking/trackingutils";
import type { TrackingInputEvent, TrackingRecordType, TrackingRecord } from "@/io.ox/office/tk/tracking/trackingrecord";

// types ======================================================================

/**
 * A filter callback function that will be invoked before starting a new
 * tracking sequence.
 *
 * @param event
 *  The original DOM input event causing to start a tracking sequence.
 *
 * @returns
 *  One of the following values:
 *  - A non-empty string to allow starting the tracking sequence with a custom
 *    tracking sequence identifier (will be passed in the property "id" of the
 *    tracking notification records).
 *  - The value `true` to allow starting the tracking sequence with the default
 *    tracking sequence identifier set in the configuration options.
 *  - A falsy value (`false` or empty string) to prevent starting the tracking
 *    sequence.
 */
export type TrackingPrepareFn = (event: TrackingInputEvent) => boolean | string;

/**
 * All keys that can be used to register callback functions to handle tracking
 * notifications.
 *
 * The special key "finally" can be used for a callback function that will be
 * invoked directly after an "end" or "cancel" notification.
 */
export type TrackingCallbackKey = TrackingRecordType | "finally";

/**
 * A callback handler that receives a tracking notification record emitted by a
 * tracking observer.
 *
 * @param record
 *  The tracking notification record to be processed by the callback function.
 */
export type TrackingCallbackFn = (record: TrackingRecord) => void;

/**
 * Dictionary of callback functions to handle tracking notification records of
 * different types.
 *
 * Entry values in the dictionary may also be strings (notification record type
 * specifiers) referring to another entry in the dictionary (the same callback
 * handler for different types).
 */
export interface TrackingCallbackConfig extends PtRecord<TrackingCallbackKey, TrackingCallbackFn | TrackingCallbackKey> {

    /**
     * An additional filter predicate that will be invoked before starting a
     * new tracking sequence. If the predicate returns `false`, the tracking
     * sequence will not be started at all.
     */
    prepare?: TrackingPrepareFn;
}

/**
 * Resolved callback map without alias string entries.
 */
export interface TrackingCallbackMap extends PtRecord<TrackingCallbackKey, TrackingCallbackFn> {
    prepare?: TrackingPrepareFn;
}

// public functions ===========================================================

/**
 * Resolves all alias entries in the passed callback map (string values) with
 * the callbacks they actually refer to.
 *
 * @param callbacks
 *  The callback map to be resolved.
 *
 * @returns
 *  The resolved callback map without alias entries.
 */
export function resolveTrackingCallbacks(callbacks: TrackingCallbackConfig = {}): TrackingCallbackMap {
    return dict.mapDict(callbacks, callback => {
        while (is.string(callback)) { callback = callbacks[callback]; }
        return callback;
    });
}

/**
 * Invokes a "prepare" tracking callback and returns its result.
 *
 * @param callback
 *  The "prepare" callback function to be invoked. If set to `undefined`, this
 *  function will always return `true`.
 *
 * @param event
 *  The original DOM input event to be examined.
 *
 * @returns
 *  The return value of the "prepare" callback.
 */
export function invokeTrackingPrepare(callback: Opt<TrackingPrepareFn>, event: TrackingInputEvent): boolean | string {
    try {
        return !callback || callback(event);
    } catch (error) {
        trackingLogger.exception(error);
        return false;
    }
}

/**
 * Invokes a tracking sequence callback function.
 *
 * @param callback
 *  The tracking callback function to be invoked. If set to `undefined`,
 *  nothing will happen.
 *
 * @param record
 *  The tracking notification record to be passed to the callback function.
 */
export function invokeTrackingCallback(callback: Opt<TrackingCallbackFn>, record: TrackingRecord): void {
    try {
        callback?.(record);
    } catch (error) {
        trackingLogger.exception(error);
        record.cancel();
    }
}
