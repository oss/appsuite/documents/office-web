/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary } from "@/io.ox/office/tk/algorithms";
import { trackingLogger } from "@/io.ox/office/tk/tracking/trackingutils";
import type { TrackingInputEvent, TrackingRecord } from "@/io.ox/office/tk/tracking/trackingrecord";
import type { TrackingCallbackFn, TrackingCallbackConfig, TrackingCallbackMap } from "@/io.ox/office/tk/tracking/trackingcallback";
import { resolveTrackingCallbacks, invokeTrackingPrepare, invokeTrackingCallback } from "@/io.ox/office/tk/tracking/trackingcallback";
import { TrackingObserver } from "@/io.ox/office/tk/tracking/trackingobserver";

// types ======================================================================

export interface TrackingDispatcherCallbacks {

    /**
     * Will be called before starting a tracking sequence (after trying all
     * registered "prepare" callbacks, before emitting the "start" notification
     * record).
     */
    before?: TrackingCallbackFn;

    /**
     * Will be called after finalizing a tracking sequence (after emitting the
     * "finally" notification record).
     */
    after?: TrackingCallbackFn;
}

/**
 * Options for registering callbacks sets at a `TrackingDispatcher`.
 */
export interface TrackingDispatcherRegisterOptions {

    /**
     * Set to `true` to prepend the callback sets to the registry, i.e. to
     * process the "prepare" callback before the other callbacks that have
     * already been registered. Default value is `false`.
     */
    prepend?: boolean;
}

// private types --------------------------------------------------------------

interface RegistryEntry extends TrackingCallbackMap {
    id: string;
}

// class TrackingDispatcher ===================================================

/**
 * A specialized `TrackingObserver` that dispatches notification records of
 * tracking sequences to multiple sets of callbacks, depending on the tracked
 * descendant target element inside an observed DOM element, or other
 * conditions.
 *
 * When processing a DOM input event, the "prepare" callback of the registered
 * callback sets will be invoked (in registration order) until the first of
 * them returns `true`. The matching callback set will then be used to process
 * the tracking sequence.
 */
export class TrackingDispatcher extends TrackingObserver {

    /** Own generic callbacks independent of registered tracking callbacks. */
    readonly #callbacks: Opt<TrackingDispatcherCallbacks>;
    /** Sets of callbacks that want to handle notifications of the same tracking observer. */
    readonly #registry: RegistryEntry[] = [];

    /** Set of callbacks currently processing a tracking sequence. */
    #active: Opt<RegistryEntry>;

    // constructor ------------------------------------------------------------

    /**
     * @param [callbacks]
     *  Optional set of callbacks invoked for *every* active tracking sequence.
     */
    constructor(callbacks?: TrackingDispatcherCallbacks) {

        super({
            prepare:  event => this.#prepare(event),
            start:    record => this.#start(record),
            move:     record => this.#active?.move?.(record),
            scroll:   record => this.#active?.scroll?.(record),
            repeat:   record => this.#active?.repeat?.(record),
            end:      record => this.#active?.end?.(record),
            cancel:   record => this.#active?.cancel?.(record),
            finally:  record => this.#finally(record)
        });

        this.#callbacks = callbacks;
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a new set of processing callbacks for tracking sequences.
     *
     * @param id
     *  A unique non-empty identifier for the callbacks. Will be passed in the
     *  property "id" of all tracking notification records.
     *
     * @param callbacks
     *  The callback functions processing a tracking sequence. the callback
     *  "prepare" MUST be implemented. It is used to decide which set of
     *  registered callbacks will be used for a new tracking sequence.
     */
    register(id: string, callbacks: TrackingCallbackConfig, options?: TrackingDispatcherRegisterOptions): void {
        if (id && callbacks.prepare) {
            const entry = Object.assign(resolveTrackingCallbacks(callbacks), { id });
            if (options?.prepend) {
                this.#registry.unshift(entry);
            } else {
                this.#registry.push(entry);
            }
            trackingLogger.trace(`$badge{Dispatcher} registered callbacks for id="${id}"`);
        } else {
            trackingLogger.error(`$badge{Dispatcher} missing "id" or "prepare" callback for id="${id}"`);
        }
    }

    /**
     * Unregisters a set of processing callbacks for tracking sequences.
     *
     * @param id
     *  The unique identifier used to register the callbacks.
     */
    unregister(id: string): void {
        ary.deleteFirstMatching(this.#registry, entry => id === entry.id);
    }

    // private methods --------------------------------------------------------

    #prepare(event: TrackingInputEvent): boolean | string {

        // pick the first callback map that wants to process the event
        for (const entry of this.#registry) {
            if (invokeTrackingPrepare(entry.prepare, event)) {
                trackingLogger.trace(`$badge{Dispatcher} dispatching to id="${entry.id}"`);
                this.#active = entry;
                return entry.id;
            }
        }

        // no set of callbacks found that wants to start a tracking sequence
        trackingLogger.trace("$badge{Dispatcher} no dispatch target found for", event);
        return false;
    }

    #start(record: TrackingRecord): void {
        invokeTrackingCallback(this.#callbacks?.before, record);
        invokeTrackingCallback(this.#active?.start, record);
    }

    #finally(record: TrackingRecord): void {
        invokeTrackingCallback(this.#active?.finally, record);
        this.#active = undefined;
        invokeTrackingCallback(this.#callbacks?.after, record);
    }
}
