/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type Point, map } from "@/io.ox/office/tk/algorithms";

import { trackingLogger, pagePoint, touchPoint, EventListenerContainer } from "@/io.ox/office/tk/tracking/trackingutils";
import type { TrackingInputEvent, TrackingInputDevice } from "@/io.ox/office/tk/tracking/trackingrecord";
import { type TrackingConfig, resolveTrackingConfig } from "@/io.ox/office/tk/tracking/trackingconfig";
import type { TrackingCallbackConfig, TrackingCallbackMap } from "@/io.ox/office/tk/tracking/trackingcallback";
import { resolveTrackingCallbacks, invokeTrackingPrepare } from "@/io.ox/office/tk/tracking/trackingcallback";
import { TrackingSequence } from "@/io.ox/office/tk/tracking/trackingsequence";

// class TrackingObserverEntry ================================================

/**
 * All settings for a DOM element observed by a `TrackingObserver`.
 */
class TrackingObserverEntry extends EventListenerContainer {

    readonly config: Required<TrackingConfig>;

    constructor(config?: TrackingConfig) {
        super();
        this.config = resolveTrackingConfig(config);
    }
}

// class TrackingObserver =====================================================

/**
 * Observes one or more DOM elements for tracking sequences.
 *
 * If an observed DOM element is clicked or tapped, and dragged around while
 * holding the mouse button or keeping the device touched, this observer will
 * emit specific notification records of type "start", "move", "end", and
 * "cancel" that can be used to implement the desired behavior, similar to the
 * browser system mouse or touch events (for example: moving a DOM node around,
 * resizing a DOM node, all other kinds of selection behavior, painting into a
 * canvas).
 *
 * A complete tracking sequence consists of one "start" notification, zero or
 * more "move" notifications, one "end", and one "finally" notification.
 *
 * A canceled tracking sequence (e.g. when hitting the ESCAPE key during the
 * sequence) consists of one "start" notification, zero or more "move"
 * notifications, one "cancel", and one "finally" notification.
 *
 * A "tracking boundary box" will be used for special kinds of notifications,
 * e.g. "scroll" and "repeat". By default, this is the observed element itself,
 * but it can be changed with specific configuration options.
 *
 * The following types of notification records will be emitted:
 *
 * - "start": Directly after a tracking sequence has been started with a mouse
 *   click or by tapping the element.
 *
 * - "move": While dragging the mouse or touch point around.
 *
 * - "scroll": While dragging the mouse or touch point around, and the border
 *   of the tracking boundary box has been reached or left. However, no
 *   physical scrolling will be performed, this needs to be implemented in the
 *   observer callback.
 *
 * - "repeat": While holding mouse or touch point above the tracking boundary
 *   box, even without any movement. Repetition delay and interval can be
 *   configured.
 *
 * - "end": After releasing the mouse or touch point somewhere.
 *
 * - "cancel": When the active tracking sequence has been cancelled. This may
 *   happen by pressing the `ESCAPE` key, by using several touch points on a
 *   touch device, by canceling the current touch sequence somehow depending on
 *   the touch device, or by calling the static function
 *   `TrackingObserver.cancelActive()` directly.
 */
export class TrackingObserver implements Disconnectable {

    /** Global reference of the current tracking sequence (any observer). */
    static #active: Opt<TrackingSequence>;

    /**
     * Cancels the tracking sequence currently running, regardless which DOM
     * element it originates from.
     *
     * @returns
     *  Whether a tracking sequence was actually active and has been canceled.
     */
    static cancelActive(): boolean {
        if (TrackingObserver.#active) {
            TrackingObserver.#active.cancel();
            return true;
        }
        return false;
    }

    // properties -------------------------------------------------------------

    /** Callback functions that handle all notification records. */
    readonly #callbacks: TrackingCallbackMap;
    /** Registry for configurations for all observed DOM elements. */
    readonly #registry = new Map<HTMLElement, TrackingObserverEntry>();

    /** Instance of the current tracking sequence started by this observer. */
    #sequence: Opt<TrackingSequence>;

    // constructor ------------------------------------------------------------

    /**
     * @param callbacks
     *  A map with callback functions to be invoked with tracking notification
     *  records, while a tracking sequence of an observed DOM element is
     *  active. The values in the map may also be notification type specifiers
     *  referring to another entry in the map (same callback handler for
     *  different record types). The special key "finally" can be used to
     *  register a callback function that will be invoked directly after an
     *  "end" or a "cancel" notification. This is useful to implement common
     *  cleanup tasks after finishing all tracking sequences.
     */
    constructor(callbacks: TrackingCallbackConfig) {
        this.#callbacks = resolveTrackingCallbacks(callbacks);
        trackingLogger.trace("$badge{Observer} created");
    }

    // public methods ---------------------------------------------------------

    /**
     * Starts observing the specified DOM element for tracking sequences.
     * According to passed configuration, "mousedown" and/or "touchstart" DOM
     * events captured at the element will start a new tracking sequence for
     * that element.
     *
     * @param elem
     *  The DOM element to be observed.
     *
     * @param [config]
     *  Additional configuration options for the DOM element.
     */
    observe(elem: HTMLElement, config?: TrackingConfig): void {

        // stop observing the specified element
        this.unobserve(elem);

        // register the configuration for the element
        trackingLogger.trace("$badge{Observer} observing", elem);
        const entry = new TrackingObserverEntry(config);
        this.#registry.set(elem, entry);

        // listen to "mousedown" and "touchstart" events to start a tracking sequence
        entry.addEventListener(elem, "mousedown", event => this.#startSequence(event, elem, entry.config));
        entry.addEventListener(elem, "touchstart", event => this.#startSequence(event, elem, entry.config), { passive: entry.config.passiveTouch });
    }

    /**
     * Stops observing the specified DOM element. An active tracking sequence
     * for the specified element will be canceled automatically.
     *
     * @param elem
     *  The DOM element to not be observed anymore.
     */
    unobserve(elem: HTMLElement): void {

        // search entry in registry
        const entry = map.remove(this.#registry, elem);
        if (!entry) { return; }

        // remove entry from registry, remove all event listeners from observed element
        trackingLogger.trace("$badge{Observer} unobserving", elem);
        entry.removeEventListeners();

        // cancel active tracking sequence for the element
        if (elem === this.#sequence?.current) {
            this.#sequence.cancel();
        }
    }

    /**
     * Stops observing all registered DOM elements. An active tracking sequence
     * for any registered element will be canceled automatically.
     */
    disconnect(): void {
        for (const elem of this.#registry.keys()) {
            this.unobserve(elem);
        }
    }

    /**
     * Immediately processes the passed input event. Useful in situations when
     * an interesting event has been caught, a new tracking observer has been
     * created in response to the event, and the observer has to start a new
     * tracking sequence for the event immediately.
     *
     * @param elem
     *  The DOM element to be used as tracking element, instead of the event
     *  property "currentTarget". Must be an element that is being observed by
     *  this instance.
     *
     * @param event
     *  The DOM "mousedown" or "touchstart" event to be processed.
     */
    process(elem: HTMLElement, event: TrackingInputEvent): void {

        // try to resolve the configuration of the passed tracking element
        const entry = this.#registry.get(elem);
        if (!entry) {
            trackingLogger.warn("$badge{Observer} passed element is not being observed", elem);
            return;
        }

        // start the tracking sequence with the passed DOM event
        this.#startSequence(event, elem, entry.config);
    }

    /**
     * Cancels the tracking sequence currently processed by this observer.
     */
    cancel(): void {
        this.#sequence?.cancel();
    }

    // private methods --------------------------------------------------------

    #setActiveSequence(sequence: Opt<TrackingSequence>): void {
        TrackingObserver.#active = this.#sequence = sequence;
    }

    #startSequence(event: TrackingInputEvent, elem: HTMLElement, config: Required<TrackingConfig>): void {

        // ignore events not originating from an element
        if (!(event.target instanceof Element)) {
            trackingLogger.warn("$badge{Observer} event target is not an element", event.target);
            return;
        }

        // ignore multiple start events on the same element, this helps to work with Chrome's
        // touch emulation (sends touch and mouse events simultaneously)
        if (elem === this.#sequence?.current) { return; }

        // determine the tracking input device and start point
        let device: TrackingInputDevice;
        let point: Opt<Point>;
        if ((event instanceof MouseEvent) && (event.type === "mousedown")) {
            // ignore event, if only touch events will be processed
            if (config.inputDevice === "touch") { return; }
            // TODO: support other mouse buttons (per config option)?
            if (event.button !== 0) {
                TrackingObserver.#active?.cancel();
                return;
            }
            device = "mouse";
            point = pagePoint(event);
        } else if ((event instanceof TouchEvent) && (event.type === "touchstart")) {
            // ignore event, if only mouse events will be processed
            if (config.inputDevice === "mouse") { return; }
            device = "touch";
            point = touchPoint(event, false);
            if (!point) {
                TrackingObserver.#active?.cancel();
                return;
            }
        } else {
            trackingLogger.warn("$badge{Observer} cannot handle unsupported event", event);
            return;
        }

        // filter events by custom predicate
        trackingLogger.trace("$badge{Observer} prepare tracking sequence", event);
        const result = invokeTrackingPrepare(this.#callbacks.prepare, event);
        if (!result) {
            trackingLogger.trace("$badge{Observer} rejected event", event);
            return;
        }

        // cancel the active tracking sequence (originating from any DOM element)
        TrackingObserver.#active?.cancel();

        // create and store a new sequence instance (before emitting "start" record)
        const id = (result === true) ? config.id : result;
        const sequence = new TrackingSequence(id, elem, event.target, device, this.#callbacks, config);
        this.#setActiveSequence(sequence);

        // emit the "start" notification record
        sequence.start(event, point, () => this.#setActiveSequence(undefined));
    }
}
