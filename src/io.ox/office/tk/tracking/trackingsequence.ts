/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type Point, coord, dict } from "@/io.ox/office/tk/algorithms";
import { type ModifierKeys, TOUCH_DEVICE, containsNode, createDiv, Rectangle } from "@/io.ox/office/tk/dom";

import { trackingLogger, pagePoint, touchPoint, EventListenerContainer } from "@/io.ox/office/tk/tracking/trackingutils";
import type { TrackingInputDevice, TrackingInputEvent, TrackingRecordType, TrackingRecord } from "@/io.ox/office/tk/tracking/trackingrecord";
import type { TrackingConfig } from "@/io.ox/office/tk/tracking/trackingconfig";
import type { TrackingCallbackKey, TrackingCallbackMap } from "@/io.ox/office/tk/tracking/trackingcallback";
import { invokeTrackingCallback } from "@/io.ox/office/tk/tracking/trackingcallback";

// types ======================================================================

/**
 * State of the last detected click, needed to detect double clicks.
 */
interface ClickState {
    id: string;
    point: Point;
    device: TrackingInputDevice;
    time: number;
}

// class TrackingEngine =======================================================

/**
 * An instance of this class represents a running tracking sequence for a DOM
 * element according to the passed configuration.
 */
export class TrackingSequence {

    // properties -------------------------------------------------------------

    /** State of the last detected click, needed to detect double clicks. */
    static #lastClick: Opt<ClickState>;

    /** The tracking sequence identifier. */
    readonly id: string;
    /** The DOM tracking element. */
    readonly current: HTMLElement;
    /** The event target that initiated the tracking sequence (tracking element or descendant). */
    readonly target: Element;

    /** Callback function receiving all tracking records. */
    readonly #callbacks: TrackingCallbackMap;
    /** The resolved configuration. */
    readonly #config: Required<TrackingConfig>;

    /** The time stamp of the "start" notification. */
    readonly #startTime: number;
    /** The device type that initiated the tracking sequence. */
    readonly #inputDevice: TrackingInputDevice;
    /** Default mouse cursor of the target element. */
    readonly #defaultCursor: string;

    /** The overlay node to preserve the mouse pointer during tracking sequence. */
    readonly #overlayEl = createDiv({ id: "io-ox-office-tracking-overlay", classes: "abs", style: { zIndex: "10000" } });
    /** The mutation observer used to detect DOM node removals during the tracking sequence. */
    readonly #mutationObserver = new MutationObserver(records => this.#handleMutations(records));
    /** Set of document event handlers needed during the sequence. */
    readonly #documentListeners = new EventListenerContainer();

    /** State of all modifier keys received from browser events. */
    readonly #modifiers: ModifierKeys = { shiftKey: false, altKey: false, ctrlKey: false, metaKey: false };

    /** The initial tracking position. */
    #start = coord.point(0, 0);
    /** The tracking position passed in the last notification record. */
    #point = coord.point(0, 0);
    /** Whether the tracking point has been moved after tracking has started. */
    #moved = false;

    /** The timer used for emitting "scroll" notifications. */
    #scrollIntervalTimer = 0;
    /** The initial delay timer used for "repeat" notifications. */
    #repeatDelayTimer = 0;
    /** The interval timer used for "repeat" notifications. */
    #repeatIntervalTimer = 0;

    /** Whether the target node has been removed during a touch tracking sequence. */
    #targetRemoved = false;
    /** Prevent recursive cancelation from callbacks. */
    #leaving = false;
    /** External callback function to be invoked when finalizing the sequence. */
    #finalize: Opt<VoidFunction>;

    // constructor ------------------------------------------------------------

    constructor(id: string, current: HTMLElement, target: Element, device: TrackingInputDevice, callbacks: TrackingCallbackMap, config: Required<TrackingConfig>) {

        this.id = id;
        this.current = current;
        this.target = target;

        this.#callbacks = callbacks;
        this.#config = config;

        this.#startTime = Date.now();
        this.#inputDevice = device;
        this.#defaultCursor = config.cursor || window.getComputedStyle(target).cursor;

        // initialize interval timers for "scroll" and "repeat" notifications
        this.#initScrollEmitter();
        this.#initRepeatEmitter();

        // Touch tracking: Use a DOM mutation observer to keep the target node in the DOM during tracking. On touch
        // devices, the tracked DOM element (`this.#target`) must remain in the DOM as long as touch tracking is
        // active. If the node will be removed from the DOM, the browser will not trigger any further "touchmove"
        // or "touchend" events. Thus the target element will be reinserted into the tracking element as long as
        // the touch tracking sequence is active. Using a DOM observer ensures that all cases are covered that
        // remove the tracked target element, for example when re-rendering parts of the DOM while tracking. This
        // works even for code that removes the tracked element deferred in browser timeouts etc.
        this.#initTargetObserver();

        trackingLogger.trace("$badge{Sequence} created", current, target);
    }

    // public methods ---------------------------------------------------------

    start(event: TrackingInputEvent, point: Point, finalize?: VoidFunction): void {

        // register all event listeners for the tracking sequence
        if (this.#config.inputDevice !== "touch") {
            this.#addDocumentListener("mousemove", this.#handleMouseMove);
            this.#addDocumentListener("mouseup", this.#handleMouseUp);
        }
        if (this.#config.inputDevice !== "mouse") {
            this.#addDocumentListener("touchmove", this.#handleTouchMove, { passive: this.#config.passiveTouch });
            this.#addDocumentListener("touchend", this.#handleTouchEnd);
            this.#addDocumentListener("touchcancel", this.#handleTouchCancel);
        }
        if (this.#config.trackModifiers) {
            this.#addDocumentListener("keydown", this.#handleModifierKeys, { capture: true });
            this.#addDocumentListener("keyup", this.#handleModifierKeys, { capture: true });
        }
        this.#addDocumentListener("keydown", this.#handleEscapeKey, { capture: true });

        // store the external finalizer callback
        this.#finalize = finalize;

        // emit the initial "start" notification record
        this.#emitStart(event, point);
    }

    cancel(): void {
        this.#emitCancel();
    }

    // private methods --------------------------------------------------------

    /**
     * Adds an event handler to the DOM document.
     */
    #addDocumentListener<K extends keyof DocumentEventMap>(type: K, listener: (event: DocumentEventMap[K]) => void, options?: AddEventListenerOptions): void {
        this.#documentListeners.addEventListener(document, type, listener.bind(this), options);
    }

    /**
     * Stores the modifier key flags of the passed event in the own storage.
     */
    #storeModifierKeys(event: TrackingInputEvent): void {
        this.#modifiers.shiftKey = event.shiftKey;
        this.#modifiers.altKey = event.altKey;
        this.#modifiers.ctrlKey = event.ctrlKey;
        this.#modifiers.metaKey = event.metaKey;
    }

    /**
     * Initialize overlay mouse pointer from configuration, or from the target
     * element.
     */
    #setOverlayCursor(cursor: string | null): void {
        this.#overlayEl.style.cursor = cursor || this.#defaultCursor;
    }

    /**
     * Emits a notification record to the tracking observer.
     */
    #invokeHandler(key: TrackingCallbackKey, record: TrackingRecord): void {
        const callback = this.#callbacks[key];
        if (callback) {
            trackingLogger.trace(`$badge{Sequence} emitting key="${key}" record=`, record);
            invokeTrackingCallback(callback, record);
        }
    }

    /**
     * Generates and emits a notification record to the tracking observer.
     */
    #emitNotification(type: TrackingRecordType, event?: TrackingInputEvent, props?: Partial<TrackingRecord>): void {

        // cancelation flag
        let cancel = false;

        // create the notification record
        const record: TrackingRecord = {
            type,
            id: this.id,
            current: this.current,
            target: this.target,
            inputDevice: this.#inputDevice,
            inputEvent: event,
            modifiers: this.#modifiers,
            startTime: this.#startTime,
            duration: Date.now() - this.#startTime,
            moved: this.#moved,
            start: this.#start,
            point: this.#point,
            offset: coord.pointsDiff(this.#start, this.#point),
            move: props?.move ?? coord.point(0, 0),
            scroll: props?.scroll ?? coord.point(0, 0),
            dblClick: !!props?.dblClick,
            cursor: cursor => this.#setOverlayCursor(cursor),
            cancel() { cancel = true; }
        };

        // emit the notification record
        this.#invokeHandler(type, record);

        // emit synthetic "finally" notification after "end" and "cancel"
        if ((type === "end") || (type === "cancel")) {
            this.#invokeHandler("finally", record);
        }

        // callback handler has requested cancelation of tracking sequence
        if (cancel) { this.#emitCancel(); }
    }

    /**
     * Initializes an interval timer for emitting "scroll" notifications after
     * a tracking sequence has been started.
     */
    #initScrollEmitter(): void {

        // check whether the observer wants to handle "scroll" notifications
        const { scrollDirection } = this.#config;
        if ((scrollDirection === "none") || !this.#callbacks.scroll) { return; }

        // whether scrolling is enabled in either direction
        const isScrollX = scrollDirection !== "vertical";
        const isScrollY = scrollDirection !== "horizontal";

        // the time in milliseconds between "scroll" notifications
        const scrollInterval = Math.max(100, this.#config.scrollInterval);
        // the number of timer frames to skip before emitting notifications
        const scrollDelayFrames = Math.max(0, Math.floor(this.#config.scrollDelay / scrollInterval - 1));

        // the minimum and maximum scrolling speed
        const minSpeed = Math.max(1, this.#config.scrollMinSpeed);
        const maxSpeed = Math.max(minSpeed, this.#config.scrollMaxSpeed);

        // the speed acceleration between two "scroll" notifications
        const acceleration = Math.max(1.05, this.#config.scrollAcceleration);

        // the DOM element used as scrolling boundary box
        const boundaryEl = this.#config.scrollBoundary ?? this.current;

        // the inner and outer margin of the acceleration area
        const [innerDist, outerDist] = this.#config.scrollAccelBand;
        const bandSize = Math.max(0, outerDist - innerDist);

        // the last exact scroll speed, in horizontal and vertical direction
        let speedX = 0, speedY = 0;
        // delay counter before starting to emit notifications
        let delayCounter = 0;

        // returns the maximum speed for the passed distance to the border box
        const getMaxSpeed = (distance: number): number => {
            return !bandSize ? maxSpeed : (Math.min(1, (distance - 1) / bandSize) * (maxSpeed - minSpeed) + minSpeed);
        };

        // returns the new scrolling speed for the specified parameters
        const calcSpeed = (speed: number, dist1: number, dist2: number): number => {
            if (dist1 > 0) { return (speed > -minSpeed) ? -minSpeed : Math.max(speed * acceleration, -getMaxSpeed(dist1)); }
            if (dist2 > 0) { return (speed < minSpeed) ? minSpeed : Math.min(speed * acceleration, getMaxSpeed(dist2)); }
            return 0;
        };

        // setup the interval timer
        this.#scrollIntervalTimer = window.setInterval(() => {

            // resolve the current position of the boundary box
            const boundary = Rectangle.from(boundaryEl.getBoundingClientRect());
            boundary.expandSelf(innerDist);

            // the distances from inner edge of acceleration area to tracking position
            const distL = isScrollX ? (boundary.left - this.#point.x) : 0;
            const distR = isScrollX ? (this.#point.x - boundary.right()) : 0;
            const distT = isScrollY ? (boundary.top - this.#point.y) : 0;
            const distB = isScrollY ? (this.#point.y - boundary.bottom()) : 0;

            // calculate new scrolling speed
            const newSpeedX = calcSpeed(speedX, distL, distR);
            const newSpeedY = calcSpeed(speedY, distT, distB);

            // reset all variables if tracking point is inside the boundary rectangle
            if (!newSpeedX && !newSpeedY) {
                speedX = speedY = delayCounter = 0;
                return;
            }

            // delay emitting notifications after entering the boundary area
            if (delayCounter < scrollDelayFrames) {
                delayCounter += 1;
                return;
            }

            // store the new scrolling increment
            speedX = newSpeedX;
            speedY = newSpeedY;

            // emit the "scroll" notification
            const scroll = coord.point(Math.round(speedX), Math.round(speedY));
            if (scroll.x || scroll.y) {
                this.#emitNotification("scroll", undefined, { scroll });
            }
        }, scrollInterval);
    }

    /**
     * Initializes an interval timer for emitting "repeat" notifications after
     * a tracking sequence has been started.
     */
    #initRepeatEmitter(): void {

        // check whether the observer wants to handle "repeat" notifications
        const repeatInterval = Math.max(0, this.#config.repeatInterval ?? 0);
        if (!repeatInterval || !this.#callbacks.repeat) { return; }

        // setup the initial delay timer
        this.#repeatDelayTimer = window.setTimeout(() => {
            this.#repeatDelayTimer = 0;

            // setup the interval timer
            this.#repeatIntervalTimer = window.setInterval(() => {
                // emit notification if tracking point hovers the tracking element
                const boundary = Rectangle.from(this.current.getBoundingClientRect());
                if (boundary.containsPixel(this.#point)) {
                    this.#emitNotification("repeat");
                }
            }, Math.max(10, repeatInterval));
        }, Math.max(0, this.#config.repeatDelay ?? repeatInterval));
    }

    /**
     * Starts observing the wrapped tracking element to check if the current
     * tracking target element has been removed from the DOM.
     */
    #initTargetObserver(): void {
        if ((this.#inputDevice === "touch") && containsNode(this.current, this.target)) {
            this.#mutationObserver.observe(this.current, { childList: true, subtree: true });
        }
    }

    /**
     * Returns whether the absolute distance between the current tracking point
     * and the passed page point positions is small enough for a double click.
     */
    #isDblClickSmallDistance(pt: Point): boolean {
        return coord.pointsDist(this.#point, pt) <= this.#config.dblClickDistance;
    }

    /**
     * Tries to detect a double click after a tracking sequence.
     */
    #detectDoubleClick(): boolean {

        // store this click for the next iteration (before returning early from this function)
        const lastClick = TrackingSequence.#lastClick;
        TrackingSequence.#lastClick = { id: this.id, point: this.#start, device: this.#inputDevice, time: this.#startTime };

        // clicks must have been processed by the same tracking observer
        if (!lastClick || (this.id !== lastClick.id)) { return false; }

        // check matching configured input device and small distance between start and end point
        const matchesDevice = (this.#config.dblClickDevice === "all") || (this.#config.dblClickDevice === this.#inputDevice);
        if (!matchesDevice || !this.#isDblClickSmallDistance(this.#start)) { return false; }

        // used input devices of both clicks must match, check distance of click positions
        if ((lastClick.device !== this.#inputDevice) || !this.#isDblClickSmallDistance(lastClick.point)) { return false; }

        // check duration between clicks
        const duration = Date.now() - lastClick.time;
        trackingLogger.trace(`$badge{Sequence} total double click delay: $duration{${duration}}`);
        if (duration > this.#config.dblClickDuration) { return false; }

        // do not use following matching third click as next double click
        TrackingSequence.#lastClick = undefined;
        return true;
    }

    /**
     * Tears down tracking sequence after is has been finished or canceled.
     */
    #leaveSequence(): void {

        // remove all global event listeners for the tracking sequence
        this.#documentListeners.removeEventListeners();

        // remove the overlay element used for mouse cursor
        this.#overlayEl.remove();

        // stop interval timers for "scroll" and "repeat" notifications
        window.clearInterval(this.#scrollIntervalTimer);
        window.clearTimeout(this.#repeatDelayTimer);
        window.clearInterval(this.#repeatIntervalTimer);

        // stop observing the tracking element for mutations
        this.#mutationObserver.disconnect();

        // the removed target element kept in DOM during touch tracking can be removed now
        if (this.#targetRemoved) {
            this.target.remove();
        }

        // invoke external finalizer function
        this.#finalize?.();
        trackingLogger.trace("$badge{Sequence} finalized");
    }

    /**
     * Emits the initial "start" notification record.
     */
    #emitStart(event: TrackingInputEvent, point: Point): void {

        // stop propagating the original event
        if (this.#config.stopPropagation) {
            event.stopPropagation();
        }

        // initial state of all modifier keys
        this.#storeModifierKeys(event);

        // initialize overlay mouse pointer from configuration, or from the target element
        this.#setOverlayCursor(null);

        // emit the initial "start" notification record
        this.#start = this.#point = point;
        this.#emitNotification("start", event);
    }

    /**
     * Emits a "move" notification record, if the tracking position has been
     * changed since the last call of this method.
     */
    #emitMove(event: TrackingInputEvent, point: Point, force = false): void {

        // stop propagating the original event
        if (this.#config.stopPropagation) {
            event.stopPropagation();
        }

        // always store the current modifier keys
        this.#storeModifierKeys(event);

        // detect whether the tracking point has been moved
        const move = coord.pointsDiff(this.#point, point);
        if (!force && !move.x && !move.y) { return; }

        // insert overlay node on first move
        if (!TOUCH_DEVICE) {
            document.body.append(this.#overlayEl);
        }

        // emit a "move" notification record
        this.#point = point;
        this.#moved = true;
        this.#emitNotification("move", event, { move });
    }

    /**
     * Emits an "end" notification record, and tears down the sequence.
     */
    #emitEnd(event: TrackingInputEvent, point: Point): void {

        // update tracking position via "move" notification (in case callback handlers do not respect final position)
        this.#emitMove(event, point);

        // "move" callback handler may have canceled the tracking sequence
        if (this.#leaving) { return; }

        // prevent recursive cancelation from callback
        this.#leaving = true;

        // emit "end" notification record
        this.#emitNotification("end", event, { dblClick: this.#detectDoubleClick() });
        this.#leaveSequence();
    }

    /**
     * Emits a "cancel" notification record, and tears down the sequence.
     */
    #emitCancel(event?: TrackingInputEvent): void {

        // ignore cancelation requests triggered while leaving the tracking sequence
        if (this.#leaving) { return; }

        // prevent recursive cancelation from callback
        this.#leaving = true;

        // emit "cancel" notification record
        this.#emitNotification("cancel", event);
        this.#leaveSequence();
    }

    // event handlers ---------------------------------------------------------

    #handleMouseMove(event: MouseEvent): void {
        this.#emitMove(event, pagePoint(event));
    }

    #handleMouseUp(event: MouseEvent): void {
        if (event.button === 0) { // TODO: support other mouse buttons (per config option)?
            this.#emitEnd(event, pagePoint(event));
        } else {
            this.#emitCancel(event);
        }
    }

    #handleTouchMove(event: TouchEvent): void {
        const point = touchPoint(event, false);
        if (point) {
            this.#emitMove(event, point);
        } else {
            this.#emitCancel(event);
        }
    }

    #handleTouchEnd(event: TouchEvent): void {
        const point = touchPoint(event, true);
        if (point) {
            this.#emitEnd(event, point);
        } else {
            this.#emitCancel(event);
        }
    }

    #handleTouchCancel(event: TouchEvent): void {
        this.#emitCancel(event);
    }

    #handleModifierKeys(event: KeyboardEvent): void {
        if (!dict.equals(this.#modifiers, event, ["shiftKey", "altKey", "ctrlKey", "metaKey"])) {
            this.#emitMove(event, this.#point, true);
        }
    }

    #handleEscapeKey(event: KeyboardEvent): void {
        if (event.key === "Escape") {
            event.stopImmediatePropagation();
            event.stopPropagation();
            event.preventDefault();
            this.#emitCancel(event);
        }
    }

    #handleMutations(records: MutationRecord[]): void {

        // check all removed nodes of all mutation records
        for (const record of records) {
            for (const node of record.removedNodes) {

                // check whether the target node has been removed, or is part of a removed element
                if (containsNode(node, this.target, { allowSelf: true })) {
                    this.#mutationObserver.disconnect();
                    (this.target as HTMLElement).style.display = "hidden";
                    this.current.append(this.target);
                    this.#targetRemoved = true;
                    return; // escape both for-loops
                }
            }
        }
    }
}
