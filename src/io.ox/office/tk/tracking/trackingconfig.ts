/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { MAX_DBLCLICK_DURATION } from "@/io.ox/office/tk/dom";
import type { TrackingInputDevice } from "@/io.ox/office/tk/tracking/trackingrecord";

// types ======================================================================

/**
 * Configuration options for observing a specific DOM element for tracking
 * sequences.
 */
export interface TrackingConfig {

    /**
     * Custom identifier that can be used to distinguish between different
     * observed DOM elements. Default value is the empty string.
     */
    id?: string;

    /**
     * Specifies which input devices will be supported.
     *
     * - If set to `"mouse"`, only mouse events will be processed, and touch
     *   events will be ignored.
     * - If set to `"touch"`, only touch events will be processed, and mouse
     *   events will be ignored.
     * - If set to `"all"` (or omitted), mouse and touch events will be
     *   processed.
     *
     * Default value is `"all"`.
     */
    inputDevice?: TrackingInputDevice | "all";

    /**
     * Specifies whether to register passive event handlers for touch events.
     * Default value is `true`.
     */
    passiveTouch?: boolean;

    /**
     * Specifies whether to stop propagating all native (mouse and touch) input
     * events. Default value is `false`.
     */
    stopPropagation?: boolean;

    /**
     * If set to `true`, additional "move" notification records will be emitted
     * whenever the state of a modifier key (SHIFT, ALT, CONTROL, or META) has
     * changed. The record property "modifiers" will contain the updated state
     * of the modifier keys. Default value is `false`.
     */
    trackModifiers?: boolean;

    /**
     * If specified, the CSS mouse pointer that will be shown while a tracking
     * sequence is active. If omitted or set to `null`, the current mouse
     * pointer of the tracked target element will be used.
     */
    cursor?: string | null;

    /**
     * Specifies for which directions "scroll" notifications will be emitted.
     * Default value is "none".
     */
    scrollDirection?: "none" | "horizontal" | "vertical" | "all";

    /**
     * The minimum time in milliseconds between two "scroll" notifications
     * while auto-scrolling mode is active. Default value is `100`.
     */
    scrollInterval?: number;

    /**
     * The initial delay time before emitting the first "scroll" notification
     * after entering the scrolling boundary area, in milliseconds. Default
     * value is `500`.
     */
    scrollDelay?: number;

    /**
     * The minimum amount of pixels (absolute value) that will be emitted in
     * the "scroll" point property of "scroll" notification records. Default
     * value is `10`.
     */
    scrollMinSpeed?: number;

    /**
     * The maximum amount of pixels (absolute value) that will be emitted in
     * the "scroll" property of "scroll" notification records. Must not be less
     * than option "scrollMinSpeed". Default value is `100`.
     */
    scrollMaxSpeed?: number;

    /**
     * Acceleration factor while increasing the (absolute value of the)
     * scrolling distance between two "scroll" notifications from the minimum
     * to the maximum speed value (see options "scrollMinSpeed" and
     * "scrollMaxSpeed"). Default value is `1.2`.
     */
    scrollAcceleration?: number;

    /**
     * A custom DOM element whose border box will be used as scrolling boundary
     * box to decide whether to emit "scroll" notification records.
     *
     * Notification records will be emitted while the tracking point is located
     * outside the boundary box, or hovers its acceleration band around the
     * borders of the boundary box (see option "scrollAccelBand").
     *
     * If omitted or set to `null`, the border box of the observed DOM element
     * will be used instead.
     */
    scrollBoundary?: HTMLElement | null;

    /**
     * The position of the acceleration area around the scrolling boundary box
     * (see option "scrollBoundary"), as pair of distances relative to the
     * borders of the scrolling boundary box. Will be used to determine the
     * maximum scroll distance (maximum values in the "scroll" point property
     * of the "scroll" notification records) that can be reached while
     * accelerating.
     *
     * The first value of the pair specifies the distance of the inner edge,
     * and the second value specifies the distance of the outer edge of the
     * acceleration area. Negative values will pull the edge into the boundary
     * box, positive values will push the edge outside the boundary box.
     *
     * If the tracking point hovers the inner edge of the acceleration area,
     * the scroll distance will stick to the minimum scroll distance defined by
     * the option "scrollMinSpeed".
     *
     * At the outer edge and outside the acceleration area, the scroll distance
     * will accelerate from the minimum distance to the maximum distance
     * defined by the option "scrollMaxSpeed" over time.
     *
     * Inside the acceleration area, the maximum reachable scroll distance will
     * be between the defined limits, according to the current tracking point.
     *
     * Default value is `[0, 30]` (start to accelerate at minimum scrolling
     * speed when reaching the edges of the scrolling boundary box; accelerate
     * to maximum scrolling speed when tracking point has left the boundary box
     * by at least 30 pixels).
     */
    scrollAccelBand?: Pair<number>;

    /**
     * The delay between subsequent "repeat" notifications, in milliseconds.
     * Must be positive in order to emit notifications. Default value is `null`
     * (do not emit "repeat" notifications).
     */
    repeatInterval?: number | null;

    /**
     * The delay between initial "start" and first "repeat" notification, in
     * milliseconds. Default value is the value of option "repeatInterval".
     */
    repeatDelay?: number | null;

    /**
     * Specifies for which input devices the observer will detect double clicks
     * (property "dblClick" in the notification record). Default value is
     * `"all"`.
     */
    dblClickDevice?: "all" | TrackingInputDevice;

    /**
     * The maximum time in milliseconds for two single clicks that must be
     * completed in order to result in a double click. Default value is the
     * constant `MAX_DBLCLICK_DURATION`.
     */
    dblClickDuration?: number;

    /**
     * The maximum distance between the page positions of two single clicks to
     * result in a double click, in CSS pixels. Default value is `3`.
     */
    dblClickDistance?: number;
}

// public functions ===========================================================

/**
 * Resolves all missing entries in the passed configuration.
 *
 * @param config
 *  The partial tracking configuration to be completed.
 *
 * @returns
 *  The resolved tracking configuration.
 */
export function resolveTrackingConfig(config?: TrackingConfig): Required<TrackingConfig> {
    return {
        id: "",
        inputDevice: "all",
        passiveTouch: true,
        stopPropagation: false,
        trackModifiers: false,
        cursor: null,
        scrollDirection: "none",
        scrollBoundary: null,
        scrollInterval: 100,
        scrollDelay: 500,
        scrollMinSpeed: 10,
        scrollMaxSpeed: 100,
        scrollAcceleration: 1.2,
        scrollAccelBand: [0, 30],
        repeatInterval: null,
        repeatDelay: null,
        dblClickDevice: "all",
        dblClickDuration: MAX_DBLCLICK_DURATION,
        dblClickDistance: 3,
        ...config
    };
}
