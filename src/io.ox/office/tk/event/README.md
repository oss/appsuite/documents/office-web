# Type-Save Event Handling

This article wants to give an overview over the various kinds of event systems used in UI code, and how to use them in a type-safe way in TypeScript code.

## Event Systems

Events are an essential part of any web application. This package uses different kinds of event emitters:

- DOM event targets (implementing the interface [`EventTarget`](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget)).
- [JQuery](https://api.jquery.com/category/events/) collections of HTML elements, or other DOM objects (document, window).
- [Backbone](https://backbonejs.org/#Events) event hubs and models (e.g. CoreUI `App` instances, the `ox` singleton).
- CoreUI's class `Events` (module `io.ox/core/event`, e.g. the `AppWindow` instance of an application).
- DocumentsUI's class [`EventHub`](./eventhub.ts) (e.g. the `globalEvents` singleton).
- DocumentsUI's class [`EObject`](../object/eobject.ts).

### DOM

DOM events are already strongly typed in TypeScript code. For example, when registering a "mousedown" handler at a DOM element, TypeScript infers its "event" parameter to have type `MouseEvent`:

```ts
document.body.addEventListener("mousedown", event => {
  // "event" has been inferred as `MouseEvent` ("button" is not part of base `Event`)
  console.log(event.button);
});
```

To achieve this level of type safety, the DOM type definitions of TypeScript contain various event mapping interfaces for the different kinds of DOM event targets. The event mapping for `HTMLElement` is declared like this (simplified example):

```ts
interface HTMLElementEventMap {
  ...
  "keydown": KeyboardEvent;
  "keypress": KeyboardEvent;
  "keyup": KeyboardEvent;
  ...
  "mousedown": MouseEvent;
  "mousemove": MouseEvent;
  "mouseup": MouseEvent;
  ...
}

interface HTMLElement extends ... {
  ...
  addEventListener<K extends keyof HTMLElementEventMap>(
    type: K,
    listener: (this: HTMLElement, ev: HTMLElementEventMap[K]) => any,
    options?: ...
  ): void;
  ...
}
```

- The generic type parameter `K` is restricted to the property _keys_ of `HTMLElementEventMap`.
- The name of the event to start listening to is inferred from the "type" parameter (e.g., `K` becomes "mousedown").
- The type of the event listener's "ev" parameter is then inferred from the `HTMLElementEventMap` interface (e.g., `HTMLElementEventMap[K]` becomes `HTMLElementEventMap["mousedown"]` becomes `MouseEvent`).

This allows source editors to even give auto-completion for event names (all property keys of the event mapping interface).

### JQuery

JQuery defines a similar event type mapping for all wrapped DOM events:

```ts
$(document.body).on("mousedown", event => {
  // "event" has been inferred as `JQuery.MouseDownEvent<...>`
  console.log(event.button);
});
```

The type mapping interface looks like this:

```ts
interface TypeToTriggeredEventMap<...> {
  ...
  keydown: KeyDownEvent<...>;
  ...
  mousedown: MouseDownEvent<...>;
  ...
}
```

### Backbone's `EventsMixin`

Backbone allows to pass as many arguments as needed to event listeners (in difference to DOM events which always contain a single event object). Backbone events are not typed at all. The type definitions of Backbone allow to pass any string as event name, and event handler arguments are just `any`s:

```ts
interface EventHandler {
  (...args: any[]): void;
}

abstract class EventsMixin implements Events {
  on(eventName: string, callback: EventHandler, context?: any): this;
  ...
}
```

To add type support, this project [has added](https://gitlab.open-xchange.com/documents/office-web/-/merge_requests/586) alternative type definitions in the module [`$/backbone`](../../../../types/modules.d.ts) following the concept of DOM and JQuery type mappings.

```ts
export interface BackboneEmitter<E> {
    on<K extends keyof E>(type: K, callback: TypedEventHandlerFn<this, E, K>): this;
    ...
}
```

Here, the generic type parameter `E` is expected to be the event mapping interface to be used for a specific object or class using Backbone events. Because Backbone supports a variable number of event arguments, the interface must contain tuple types instead of a single event type (see following example). The helper type `TypedEventHandler<>` will resolve to the event handler function type for a specific event name.

#### Example

The event type mapping for the global `ox` singleton now looks like this:

```ts
export interface OXStaticEventMap {
  "app:init": [app: App];
  ...
  "change:document:title": [title: string];
}

export interface OXStatic extends BackboneEmitter<OXStaticEventMap> {
  ...
}

const ox: OXStatic;
export default ox;
```

In TypeScript code, event handlers are now strictly typed. In the past, `OXStatic` has extended `Backbone.EventsMixin`, and the following code has passed without type errors:

```ts
import ox from "$/ox";

// TypeError! `App` does not implement "wrongMethod"
ox.on("app:init", app => app.wrongMethod());

// TypeError! "wrong:event" is not a key of `OXStaticEventMap`
ox.on("wrong:event", () => { ... });

// TypeError! "app:init" triggers one argument only
ox.on("app:init", (app, more) => { ... });
```

### CoreUI's `Events`

The CoreUI class `Events` is an old event hub implementation used throughout the CoreUI project, in parallel to Backbone events and models. It is a thin wrapper around a JQuery object which acts as container for the registered event handlers.

CoreUI does not provide type definitions for its modules at all, therefore this project [has added](https://gitlab.open-xchange.com/documents/office-web/-/merge_requests/511) local [type definitions](../../../../types/io-ox-core.d.ts) with support for strict event type mappings. The type definitions should look quite similar to strictly typed Backbone events.

```ts
export interface CoreEmitter<E> {
  on<K extends keyof E>(type: K, handler: CoreEventHandlerFn<this, E, K>): this;
  ...
}

export default class Events<E> implements CoreEmitter<E> {
  ...
}
```

Due to the fact that class `Events` wraps a JQuery object internally, all event handlers will receive a `JQuery.Event` object as leading argument, followed by the custom arguments that have been passed when emitting the event. In most cases, this event object will not be used in the event handler. Additional functionality of the event object like `preventDefault` and `stopPropagation` is actually useless.

The helper type `CoreEventHandlerFn<>` will resolve to the event handler function type for a specific event name, including the leading `JQuery.Event` object.

The event type mapping interface must _not_ include the leading event argument emitted by `Events`. The following example shows the type definition for a class using `Events` for event handling:

```ts
export interface ExampleEventMap {
  change: [id: string, value: number];
}

export class Example extends Events<ExampleEventMap> {
  ...
}
```

Event handlers will receive strictly typed arguments:

```ts
const example = new Example();
example.on("change", (_event, id, value) => {
  // _event: JQuery.Event
  // id: string
  // value: number
});
example.trigger("change", "answer", 42);
```

### Class `EventHub`

Class [`EventHub`](./eventhub.ts) has been added as a lightweight, fast, and type-safe alternative for CoreUI's `Events` class. Initially, it was the only available class providing type-safety for custom events, before the type-safe `BackboneEmitter` and `CoreEmitter` interfaces have been introduced. It is still faster than the mentioned alternatives. Like Backbone events, and unlike `Events`, an `EventHub` does not pass an additional leading event object to the event handlers.

Event hubs can be instantiated directly, or used as base class or class members.

```ts
interface ExampleEventMap {
  change: [id: string, value: number];
}

const hub = new EventHub<ExampleEventMap>();
hub.on("change", (id, value) => { /* id: string, value: number */ });
hub.emit("change", "answer", 42);

class Example extends EventHub<ExampleEventMap> { ... }
```

### Class `EObject`

Class `EObject` contains an internal `EventHub` member used to store the registered event handlers (this is needed to be able to subclass `EObject` from the class `DObject` for destructor support).

In the past, `EObject` instances used to contain a CoreUI `Events` object, meaning that all event handlers have received leading JQuery event objects. As of September 2023, the `Events` container [has been replaced](https://gitlab.open-xchange.com/documents/office-web/-/merge_requests/591) with an `EventHub` container, and the event parameter [has been removed](https://gitlab.open-xchange.com/documents/office-web/-/merge_requests/596) from all event handlers in the project.

```ts
interface ExampleEventMap {
  change: [id: string, value: number];
}

class Example extends EObject<ExampleEventMap> { ... }

const example = new Example();
example.on("change", (id, value) => { /* id: string, value: number */ });
example.trigger("change", "answer", 42);
```

## Other Features

### One-Time Handlers

All event emitters provide a method to register one-time event handlers, that will be unregistered automatically when invoked for the first time.

| Emitter | Method | Example |
| -- | -- | -- |
| DOM | option `once:true` | `elem.addEventListener("type", handler, { once: true });` |
| JQuery | method `one()` | `$(elem).one("type", handler);` |
| Backbone | method `once()` | `model.once("type", handler);` |
| CoreUI `Events` | method `one()` | `events.one("type", handler);` |
| `EventHub` | option `once:true` | `hub.on("type", handler, { once: true });` |
| `EObject` | method `one()` | `object.one("type", handler);` |

Note the difference of `once` and `one` between Backbone and JQuery/CoreUI.

Class `EventHub` uses an option (like DOM emitters) to be able to support more options without having to replicate that in two methods. Class `EObject` still provides the method `one` for historical reasons. This may be changed in the future.

### All-Events Handlers

Event emitters for custom events (Backbone, CoreUI `Events`, `EventHub`, `EObject`) provide the ability to register event handlers that will be invoked for _all_ emitted events. For that, a special event name is reserved. Event handlers will receive the name of the event actually emitted as additional leading parameter (for CoreUI `Events`, _after_ the event object).

| Emitter | Event | Example |
| -- | -- | -- |
| Backbone | `"all"` | `model.on("all", (type, ...args) => { ... });` |
| CoreUI `Events` | `"triggered"` | `events.on("triggered", (_event, type, ...args) => { ... });` |
| `EventHub` | `"*"` | `hub.on("*", (type, ...args) => { ... });` |
| `EObject` | `"*"` | `object.on("*", (type, ...args) => { ... });` |

The DocumentsUI classes `EventHub` and `EObject` have chosen the asterisk to minimize the risk of accidential name collisions with actual events.

All-events handlers can also be registered as one-time handlers.

### `EObject.listenTo`

Class `Eobject` provides various `listenTo` methods to start listening to any event emitters. Using this method instead of `on()` etc. has a few advantages:

- Destroying the object will automatically remove all event handlers from the external event emitters that have been registered with these methods.
- A unified API hiding all the differences of the event systems described above (e.g., one-time handlers, all-events handlers).
- Automatic context binding of own class methods.
- Scope symbols for grouping multiple event handlers.
- Automatic unbinding on promise settlement.

The following methods are available:

| Bind method | Unbind method | Description |
| -- | -- | -- |
| `listenTo(emitter, type, handler, options?)` | `stopListeningTo(...)` | Start/stop listening to specific events. |
| `listenToAllEvents(emitter, handler, options?)` | `stopListeningToAllEvents(...)` | Start/stop listening to all emitted events. |
| `listenToGlobal(type, handler, options?)` | `stopListeningToGlobal(...)` |  Start/stop listening to the [global event hub](./globalevents.ts). |
| `listenToProp(propSet, propKey, handler, options?)` | n/a | Start listening to a [`PropertySet`](../container/propertyset.ts) instance. |
| n/a | `stopListeningToScope(scope)` | Removes all event handlers that have been registered with the scope symbol (see below). |
| n/a | `stopListening()` | Removes _all_ event handlers registered with any of the `listenTo` methods. |

#### Example Class Using `listenTo` Methods

The following examples show a class that registers

- a DOM event handler,
- a _one-time_ "change" handler for a Backbone model, and
- an _all-events_ handler on an `EventHub`.

Without using `listenTo` methods:

```ts
class Example1 extends EObject {

  #model: Backbone.Model;
  #hub: EventHub;

  #boundResizeHandler = this.#resizeHandler.bind(this);
  #boundChangeHandler = this.#changeHandler.bind(this);
  #boundAllHandler = this.#allHandler.bind(this);

  constructor(model: Backbone.Model, hub: EventHub<E>) {
    this.#model = model;
    this.#hub = hub;
    window.addEventListener("resize", this.#boundResizeHandler);
    this.#model.once("change:size", this.#boundChangeHandler);
    this.#hub.on("*", this.#boundAllHandler);
  }

  protected override destructor(): void {
    window.removeEventListener("resize", this.#boundResizeHandler);
    this.#model.off("change:size", this.#boundChangeHandler);
    this.#hub.off("*", this.#boundAllHandler);
  }

  #resizeHandler(event: UIEvent): void { ... }
  #changeHandler(size: number): void { ... }
  #allHandler(type: string, ...args: InferEventArgs<E>): void { ... }
}
```

Using `listenTo` methods:

```ts
class Example2 extends EObject {

  constructor(model: Backbone.Model, hub: EventHub<E>) {
    this.listenTo(window, "resize", this.#resizeHandler);
    this.listenTo(model, "change:size", this.#changeHandler, { once: true });
    this.listenToAllEvents(hub, this.#allHandler);
  }

  #resizeHandler(event: UIEvent): void { ... }
  #changeHandler(size: number): void { ... }
  #allHandler(type: string, ...args: InferEventArgs<E>): void { ... }
}
```

Event handlers may even be inlined to take advantage of automatic type inference:

```ts
class Example3 extends EObject {

  constructor(model: Backbone.Model, hub: EventHub<E>) {
    this.listenTo(window, "resize", event => { ... });
    this.listenTo(model, "change:size", size => { ... }, { once: true });
    this.listenToAllEvents(hub, (type, ...args) => { ... });
  }
}
```

#### Scope Symbols

All `listenTo` methods accept a `scope` option that can be set to a `symbol` which will be remembered internally. The method `stopListeningToScope` allows to remove all event handlers of a specific scope symbol at once.

This feature is useful in situations when needing to start/stop listening to multiple event emitters dynamically at runtime, without having to repeat the event emitters, types, and listeners in matching `stopListening` calls.

```ts
class Example extends EObject {

  #scope = Symbol("event scope");

  constructor() {
    this.listenTo(document, "mousedown", event => { ... }); // unscoped handler
  }

  start(hub: EventHub): void {
    this.listenTo(window, "resize", event => { ... }, { scope: this.#scope });
    this.listenToAllEvents(hub, () => { ... }, { scope: this.#scope, once: true });
  }

  stop(): void {
    this.stopListeningToScope(this.#scope); // does not remove the `document` handler
  }
}
```

Scope symbols are similar to JQuery's event namespaces feature, but using symbols instead of strings minimizes the risk of accidential name collisions in independent code.

#### Promise Settlement

All `listenTo` methods accept a `while` option that can be set to a pending `Promise` or `JQuery.Promise`. As soon as the promise settles (fulfils or rejects), the event handler will be removed automatically.

```ts
class Example extends EObject {

  doSomethingComplex(model: SomeModel): Promise<void> {
    const promise = model.startAsyncProcess();
    this.listenTo(model, "progress", () => { ... }, { while: promise });
    return promise;
  }
```

## Performance

The implementation of class `EventHub` has been kept simple. This can be observed when comparing the call stacks of event handlers of different event emitters. This should have some positive impact on runtime performance when dealing with events in the DocumentsUI apps.

### Test Code

```js
import Backbone from "$/backbone";
import Events from "$/io.ox/core/event";
import { EventHub } from "@/io.ox/office/tk/events";
import { EObject } from "@/io.ox/office/tk/objects";

function handler() { debugger; }
function test() {
  new Backbone.Model().on("test", handler).trigger("test");
  new Events().on("test", handler).trigger("test");
  const hub = new EventHub(); hub.on("test", handler); hub.emit("test");
  new EObject().on("test", handler).trigger("test");
}
test();
```

### Backbone

With 3 function calls between `Events.trigger` and `handler`:

```txt
handler (test.js:6)
1 triggerEvents (backbone.js:334)
2 triggerApi (backbone.js:322)
3 eventsApi (backbone.js:110)
Events.trigger (backbone.js:312)
test (test.js:7)
```

### CoreUI `Events`

With 7 function calls between `Events.trigger` and `handler`:

```txt
handler (test.js:6)
1 dispatch (jquery.js:5145)
2 elemData.handle (jquery.js:4949)
3 trigger (jquery.js:8629)
4 triggerHandler (jquery.js:8713)
5 trigger (event.js:72)
6 each (each.js:14)
7 _.<computed> (mixin.js:14)
Events.trigger (event.js:83)
test (test.js:8)
```

### `EventHub`

With 1 function call between `EventHub.emit` and `handler`:

```txt
handler (test.js:6)
1 invokeHandler (eventutils.ts:402)
emit (eventhub.ts:172)
test (test.js:9)
```

### `EObject`

With 2 function calls between `EObject.trigger` and `handler`:

```txt
handler (test.js:6)
1 invokeHandler (eventutils.ts:402)
2 emit (eventhub.ts:172)
trigger (eobject.ts:662)
test (test.js:10)
```

For comparison, the old version of `EObject`, with internal CoreUI `Events` hub, contained 8 function calls between `EObject.trigger` and `handler`:

```txt
handler (test.js:6)
1 dispatch (jquery.js:5145)
2 elemData.handle (jquery.js:4949)
3 trigger (jquery.js:8629)
4 triggerHandler (jquery.js:8713)
5 trigger (event.js:72)
6 each (each.js:14)
7 _.<computed> (mixin.js:14)
8 Events.trigger (event.js:83)
trigger (eobject.ts:662)
test (test.js:10)
```
