/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { map } from "@/io.ox/office/tk/algorithms";
import type { InferEventArgs, TypedEventHandlerFn, TypedAllEventsHandlerFn } from "@/io.ox/office/tk/event/eventutils";
import { resolveEventTypes, invokeHandler } from "@/io.ox/office/tk/event/eventutils";

// types ======================================================================

/**
 * Optional parameters for registering an event handler function.
 */
export interface EventHubOptions {

    /**
     * If set to `true`, the event handler will be invoked exactly once. When
     * the event hub emits the next event, it will remove the event handler
     * automatically before invocation. Default value is `false`.
     */
    once?: boolean;

    /**
     * __Internal__: The original event handler to be used as internal map key.
     * Used to attach a wrapper function instead of the original handler.
     */
    key?: AnyFunction;
}

// private types --------------------------------------------------------------

/**
 * A map with equal type for keys and values, used to store callback functions.
 */
type FnMap = Map<AnyFunction, AnyFunction<unknown[]>>;

// class EventHub =============================================================

/**
 * An event emitter for custom events with strongly typed arguments.
 *
 * @template E
 *  The type mapping interface for the supported event types. The interface
 *  keys are the supported event type keys, the interface property types are
 *  tuples for the types of the arguments emitted for that event.
 *
 * @example
 *  interface MyEventMap {
 *    event1: [number, number];
 *    event2: [string];
 *    event3: [];
 *  }
 *  const eventHub = new EventHub<MyEventMap>();
 *  eventHub.emit("event1", 1, 2);
 *  eventHub.emit("event2", "data");
 *  eventHub.emit("event3");
 */
export class EventHub<E> implements Destroyable {

    /** Registered event handlers, mapped by type and original callback function. */
    readonly #map = new Map<PropertyKey, FnMap>();

    // public methods ---------------------------------------------------------

    /**
     * Removes all event handlers from the internal registry.
     */
    destroy(): void {
        this.#map.clear();
    }

    /**
     * Registers an event handler callback function at this instance. Event
     * handlers will be called in order of their registration.
     *
     * @param type
     *  The event type key, or an array of event type keys, or the special key
     *  `"*"` to register an event handler that will be invoked for all emitted
     *  events.
     *
     * @param handler
     *  The callback function to be invoked if this instance emits an event of
     *  the specified type. Event handlers must be synchronous, and MUST NOT
     *  throw exceptions. An event handler that has already been registered
     *  will be moved to the end of the calling order of all registered
     *  handlers, but it will *not* be invoked multiple times.
     *
     * @param [options]
     *  Optional parameters.
     */
    on(type: "*", handler: TypedAllEventsHandlerFn<this, E>, options?: EventHubOptions): void;
    on<K extends keyof E>(type: OrArray<K>, handler: TypedEventHandlerFn<this, E, K>, options?: EventHubOptions): void;
    // implementation
    on(types: OrArray<PropertyKey>, handler: AnyFunction, options?: EventHubOptions): void {
        const bound = handler.bind(this);
        const key = options?.key ?? handler;
        for (const type of resolveEventTypes(types, "*")) {
            const handlers = map.upsert(this.#map, type, () => new Map(null));
            handlers.delete(key); // needed to move existing entry to end of calling order
            if (options?.once) {
                handlers.set(key, (...args) => {
                    handlers.delete(key);
                    bound(...args);
                });
            } else {
                handlers.set(key, bound);
            }
        }
    }

    /**
     * Removes one or more event handler callback functions from this instance.
     *
     * @param [type]
     *  The event type key, or an array of event type keys, or the special key
     *  `"*"` to unregister an event handler that will be invoked for all
     *  emitted events.
     *
     * @param [handler]
     *  The callback function to be removed from this instance. If omitted, all
     *  event handlers of the specified type will be removed.
     */
    off(type: "*", handler: TypedAllEventsHandlerFn<this, E>): void;
    off<K extends keyof E>(type?: OrArray<K>, handler?: TypedEventHandlerFn<this, E, K>): void;
    // implementation
    off(types?: OrArray<PropertyKey>, handler?: AnyFunction): void {
        if (types) {
            for (const type of resolveEventTypes(types, "*")) {
                const handlers = this.#map.get(type);
                if (handler) {
                    handlers?.delete(handler);
                } else {
                    handlers?.clear();
                }
            }
        } else {
            this.#map.clear();
        }
    }

    /**
     * Invokes all event handlers for the specified type, and afterwards all
     * event handlers registered for the special key `"*"`.
     *
     * @param type
     *  The event type key.
     *
     * @param args
     *  The arguments to be passed to all event handlers.
     */
    emit<K extends keyof E>(type: K, ...args: InferEventArgs<E, K>): void {
        const handlers = this.#map.get(type);
        if (handlers) {
            for (const handler of Array.from(handlers.values())) {
                invokeHandler(handler, ...args);
            }
        }
        const allHandlers = this.#map.get("*");
        if (allHandlers) {
            for (const handler of Array.from(allHandlers.values())) {
                invokeHandler(handler, type, ...args);
            }
        }
    }
}
