/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";
import Backbone, { type BackboneEmitter } from "$/backbone";

import Events, { type CoreEmitter } from "$/io.ox/core/event";

import { is, str, ary } from "@/io.ox/office/tk/algorithms";
import type { EventHub } from "@/io.ox/office/tk/event/eventhub";

// re-exports =================================================================

export type * from "$/io.ox/core/event";

export type { BackboneEmitter };

// types ======================================================================

/**
 * Extracts the arguments type tuple for a specific event type.
 *
 * @template E
 *  The type mapping interface for all event types and arguments.
 *
 * @template K
 *  The type key of the event.
 */
export type InferEventArgs<E, K extends keyof E = keyof E> = E[K] extends any[] ? E[K] : never;

/**
 * Resolves the event map interface of the specified DOM event target class.
 *
 * Example: `ResolveDOMEventMap<Window>` resolves to `WindowEventMap`.
 *
 * @template E
 *  The DOM event target class to be resolved to its event map interface.
 */
export type ResolveDOMEventMap<E extends EventTarget> =
    E extends Window ? WindowEventMap :
    E extends Document ? DocumentEventMap :
    E extends HTMLElement ? HTMLElementEventMap :
    E extends SVGElement ? SVGElementEventMap :
    E extends MathMLElement ? MathMLElementEventMap :
    Dict<Event>; // fallback: all unknown event types result in a base `Event`

/**
 * Resolves the event names of the specified DOM event target class.
 *
 * Example: `ResolveDOMEventKeys<Window>` resolves to `"resize"|"error"|...`.
 *
 * @template E
 *  The DOM event target class to be resolved to its supported event names.
 */
export type ResolveDOMEventKeys<E extends EventTarget> = keyof ResolveDOMEventMap<E> & string;

/**
 * Resolves the event type of the specified DOM event target class and event
 * name.
 *
 * Example: `ResolveDOMEvent<Window, "error">` resolves to `ErrorEvent`.
 *
 * @template E
 *  The DOM event target class to be resolved to a supported event type.
 *
 * @template K
 *  The name of the event to be resolved.
 */
export type ResolveDOMEvent<E extends EventTarget, K extends ResolveDOMEventKeys<E>> = ResolveDOMEventMap<E>[K];

/**
 * Optional parameters for registering an event handler.
 */
export interface EventBindOptions {

    /**
     * If set to `true`, the event handler will be invoked exactly once. When
     * the event emitter fires the next event, it will remove the event handler
     * automatically before invocation. Default value is `false`.
     */
    once?: boolean;

    /**
     * Associate the event handler to a scope symbol. Multiple event handlers
     * can be unbound later by specifying the scope symbol.
     */
    scope?: symbol;

    /**
     * If set to a pending promise, the event handler will be automatically
     * unbound, when the promise settles (regardless of the result state). If
     * the promise is already settled, the event handler will not be registered
     * at all.
     */
    while?: AnyPromise<unknown>;
}

/**
 * Optional parameters for registering an event handler at a native DOM event
 * emitter.
 */
export interface DOMBindOptions extends EventBindOptions {
    /** Whether to bind event handler to capturing phase. */
    capture?: boolean;
    /** Whether to bind event handler in passive mode. */
    passive?: boolean;
}

/**
 * Optional parameters for registering an event handler at a JQuery collection.
 */
export interface JQueryBindOptions extends EventBindOptions {
    /** A CSS selector for a delegating event handler. */
    delegated?: string;
}

/**
 * Type definition of an event handler function for an untyped event emitter,
 * bound to the specified object type as calling context.
 *
 * @template T
 *  The calling context.
 *
 * @template A
 *  The types of all arguments emitted with the event.
 */
export type EventHandlerFn<T, A extends unknown[]> = (this: T, ...args: A) => void;

/**
 * Type definition of an all-events handler function for an untyped event
 * emitter, bound to the specified object type as calling context.
 *
 * @template T
 *  The calling context.
 */
export type AllEventsHandlerFn<T> = (this: T, type: string, ...args: unknown[]) => void;

/**
 * Type of an event handler callback function that infers its argument types
 * from the passed event type mapping and event type key.
 *
 * @template T
 *  The calling context.
 *
 * @template E
 *  The type mapping interface for all event types and arguments.
 *
 * @template K
 *  The type key of the event.
 */
export type TypedEventHandlerFn<T, E, K extends keyof E> = (this: T, ...args: InferEventArgs<E, K>) => void;

/**
 * Type of an all-events handler callback function that infers its argument
 * types from the passed event type mapping.
 *
 * @template T
 *  The calling context.
 *
 * @template E
 *  The type mapping interface for all event types and arguments.
 */
export type TypedAllEventsHandlerFn<T, E, K extends keyof E = keyof E, A extends InferEventArgs<E, K> = InferEventArgs<E, K>> = (this: T, type: K, ...args: A) => void;

/**
 * Type definition of an event handler function for a DOM event target, bound
 * to the specified object type as calling context.
 *
 * @template T
 *  The calling context.
 *
 * @template E
 *  The exact type of the DOM event emitter.
 *
 * @template K
 *  The name of the DOM event.
 */
export type DOMEventHandlerFn<T, E extends EventTarget, K extends ResolveDOMEventKeys<E>> = (this: T, event: ResolveDOMEvent<E, K>) => void;

/**
 * Type definition of an event handler function for a JQuery collection
 * wrapping DOM event targets, bound to the specified object type as calling
 * context, with a specialized inferred event parameter.
 *
 * @template T
 *  The calling context.
 *
 * @template K
 *  The name of the DOM event.
 *
 * @returns
 *  May return `false` which will be evaluated by JQuery objects to prevent the
 *  default action of DOM events, and to stop event propagation through the
 *  DOM.
 */
export type JQueryEventHandlerFn<T, K extends keyof JTriggeredEventMap> = (this: T, event: JTriggeredEventMap[K]) => void | false;

/**
 * JQuery objects intended to be used as event targets for DOM events.
 */
export type JQueryEmitter = JQuery<EventTarget>;

/**
 * Shape of the internal event system used in Documents classes.
 *
 * @template E
 *  The event mapping interface.
 *
 * @example
 *  interface MyEventMap {
 *    event1: [number, number];
 *    event2: [string];
 *    event3: [];
 *  }
 *  const obj = new EObject<MyEventMap>();
 *  obj.trigger("event1", 1, 2);
 *  obj.trigger("event2", "data");
 *  obj.trigger("event3");
 */
export interface DocsEmitter<E> {
    readonly __typeTag: "DocsEmitter";

    on<K extends keyof E, A extends InferEventArgs<E, K>>(type: "*", handler: TypedAllEventsHandlerFn<this, E, K, A>): this;
    on<K extends keyof E>(type: OrArray<K>, handler: TypedEventHandlerFn<this, E, K>): this;

    one<K extends keyof E, A extends InferEventArgs<E, K>>(type: "*", handler: TypedAllEventsHandlerFn<this, E, K, A>): this;
    one<K extends keyof E>(type: OrArray<K>, handler: TypedEventHandlerFn<this, E, K>): this;

    off<K extends keyof E, A extends InferEventArgs<E, K>>(type: "*", handler?: TypedAllEventsHandlerFn<this, E, K, A>): this;
    off<K extends keyof E>(type?: OrArray<K>, handler?: TypedEventHandlerFn<this, E, K>): this;

    trigger<K extends keyof E>(type: K, ...args: InferEventArgs<E, K>): this;
}

/**
 * Types of all strongly typed event emitters that emit their event arguments
 * as specified in the event mapping interface (without extra arguments, such
 * as `CoreEmitter` which emit an extra leading `JEvents` argument).
 *
 * The types `TypedEventHandlerFn` and `TypedAllEventsHandlerFn` represent the
 * types of the event handler functions for these event emitters.
 */
export type TypedEmitter<E> = BackboneEmitter<E> | EventHub<E> | DocsEmitter<E>;

/**
 * Types of supported event emitters that emit specific synthetic "all" events
 * after each individual event:
 * - Backbone event emitters (the `"all"` event),
 * - `CoreEmitter` implementers (the `"triggered"` event),
 * - `EventHub` instances and `DocsEmitter` implementers (the `"*"` event).
 */
export type AllEventsEmitter<E> = TypedEmitter<E> | CoreEmitter<E>;

/**
 * Types of all supported event emitters.
 */
export type AnyEmitter<E> = EventTarget | JQueryEmitter | AllEventsEmitter<E>;

// public functions ===========================================================

/**
 * Returns whether the passed value is a DOM event emitter (implementing the
 * DOM interface `EventTarget`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a DOM event emitter.
 */
export function isDOMEmitter(data: unknown): data is EventTarget {
    // class `EventTarget` not supported in DOM.js (unit tests, cannot use `instanceof` to detect DOM elements)
    return is.object(data) && is.function(data.addEventListener);
}

/**
 * Returns whether the passed value is a JQuery event emitter.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a JQuery event emitter.
 */
export function isJQueryEmitter(data: unknown): data is JQueryEmitter {
    return data instanceof $;
}

/**
 * Returns whether the passed value is a Backbone event emitter (implementing
 * the interface `BackboneEmitter`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a Backbone event emitter.
 */
export function isBackboneEmitter(data: unknown): data is BackboneEmitter<any> {
    // duck type check needed (`Backbone.Events` is a pure instance mix-in and no class)
    // TODO: is this reliable/future-proof?
    return (data !== Backbone.Events) && is.object(data) && (data.on === Backbone.Events.on);
}

/**
 * Returns whether the passed value is a Core UI event emitter (implementing
 * the interface `CoreEmitter`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a Core UI event emitter.
 */
export function isCoreEmitter(data: unknown): data is CoreEmitter<any> {
    return (data instanceof Events) || (is.object(data) && (data.events instanceof Events));
}

/**
 * Returns whether the passed value is an internal event emitter (implementing
 * the interface `DocsEmitter`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is an internal event emitter.
 */
export function isDocsEmitter(data: unknown): data is DocsEmitter<any> {
    return is.object(data) && is.function(data.on) && (data.__typeTag === "DocsEmitter");
}

/**
 * Resolves an event types parameter to an array of single event type names.
 *
 * @param types
 *  The event types parameter to be resolved. A single string will be converted
 *  to an array. Space separated strings will be split into separate tokens.
 *  The special type `"*"` will be converted to an array containing the value
 *  passed with the parameter "allType".
 *
 * @param allType
 *  The type name of the virtual all-event to be returned for the value `"*"`.
 *  If omitted, this function will throw, if the value `"*"` has been passed in
 *  parameter "types".
 *
 * @returns
 *  An array of event type names.
 */
export function resolveEventTypes(types: OrArray<PropertyKey>, allType?: string): string[] {

    // resolve "all events" type
    if (types === "*") {
        if (!allType) { throw new Error("missing name for the 'catch-all-events' event type"); }
        return [allType];
    }

    // convert event types to array, and filter for non-empty strings
    const typesArr = ary.wrap(types).filter(Boolean).filter(is.string);
    if (!typesArr.length) { throw new Error("missing event type specifier"); }

    // split space-separated event types to single tokens
    return typesArr.flatMap(typesStr => {
        const typeTokens = str.splitTokens(typesStr);
        // if (typeTokens.length > 1) {
        //     window.console.warn(`deprecated space-separated event types list, switch to array syntax ("${typesStr}")`);
        // }
        return typeTokens;
    });
}

/**
 * Invokes the passed event handler. Catches and logs all exceptions thrown
 * from the event handler.
 *
 * @param handler
 *  The event handler to be invoked.
 *
 * @param args
 *  The arguments to be passed to the event handler.
 */
export function invokeHandler<RetT, ArgsT extends unknown[]>(handler: FuncType<RetT, ArgsT>, ...args: ArgsT): Opt<RetT> {
    try {
        return handler(...args);
    } catch (err) {
        window.console.error("unhandled exception in event handler\n", err);
        return undefined;
    }
}
