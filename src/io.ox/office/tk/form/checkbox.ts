/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type CreateElementLabelOptions, createInput, enableElement } from "@/io.ox/office/tk/dom";
import { type ControlConfig, Control } from "@/io.ox/office/tk/form/control";

import "@/io.ox/office/tk/form/checkbox.less";

// types ======================================================================

/**
 * The value type of a regular checkbox, or a tristate checkbox.
 */
export type CheckBoxValue<Tristate extends boolean> = Tristate extends true ? boolean | null : boolean;

/**
 * Configuration options for a `CheckBox` control.
 */
export interface CheckBoxConfig<Tristate extends boolean = false> extends ControlConfig<CheckBoxValue<Tristate>>, CreateElementLabelOptions { }

// class CheckBox =============================================================

/**
 * A checkbox control with a boolean state value.
 *
 * @template Tristate
 *  Whether the checkbox supports a third "ambiguous" state represented by the
 *  value `null`. The checkbox will be drawn in a special checked state with
 *  lighter colors.
 */
export class CheckBox<
    Tristate extends boolean = false,
    ConfT extends CheckBoxConfig<Tristate> = CheckBoxConfig<Tristate>,
> extends Control<CheckBoxValue<Tristate>, ConfT> {

    /**
     * The DOM `<input>` element for the checkbox.
     */
    readonly inputEl: HTMLInputElement;

    // constructor ------------------------------------------------------------

    /**
     * @param value
     *  The initial state of the checkbox.
     *
     * @param [config]
     *  Configuration options for the checkbox control.
     */
    public constructor(value: CheckBoxValue<Tristate>, config?: NoInfer<ConfT>) {
        super("checkbox", value, config);

        // create the `<input>` element for the checkbox
        this.inputEl = createInput({ type: "checkbox", id: this.id, classes: "btn btn-default" });
    }

    // protected methods ------------------------------------------------------

    /**
     * Renders the entire DOM structure of this checkbox.
     */
    protected override implRender(): void {

        // insert the checkbox element (before the caption)
        this.el.append(this.inputEl);

        // render the text label following the checkbox
        this.renderLabel();

        // listen to change events of the DOM element
        this.listenTo(this.inputEl, "change", event => {
            this.setValue(this.inputEl.checked, { event });
        });
    }

    /**
     * Updates the DOM structure according to the current model value.
     */
    protected override implRefresh(): void {
        this.inputEl.checked = this.value === true;
        this.inputEl.classList.toggle("ambiguous", this.value === null);
    }

    /**
     * Enables or disables this checkbox.
     */
    protected override implEnable(): void {
        enableElement(this.inputEl, this.enabled);
    }

    /**
     * Sets the browser focus to this checkbox.
     */
    protected override implFocus(): void {
        this.inputEl.focus();
    }
}
