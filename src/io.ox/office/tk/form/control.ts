/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type Size, is, itr, ary, set, pick, debug } from "@/io.ox/office/tk/algorithms";
import type { CreateElementOptions, ControlValueSerializerFn } from "@/io.ox/office/tk/dom";
import { containsFocus, setElementDataAttr, createDiv, createLabel, serializeControlValue, getCeilNodeSize } from "@/io.ox/office/tk/dom";
import { onceMethod } from "@/io.ox/office/tk/objects";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { type DObjectConfig, EObject } from "@/io.ox/office/tk/objects";

// types ======================================================================

/**
 * Reduces an array control value type to its element type.
 */
type ControlBaseT<VT> = VT extends readonly any[] ? VT[number] : VT;

/**
 * Common configuration options for form controls.
 */
export interface ControlConfig<VT> extends DObjectConfig, CreateElementOptions {

    /**
     * A custom key to be stored as `data-key` attribute at the root element of
     * the form control.
     */
    key?: string;

    /**
     * Custom equality predicate function used to determine changed control
     * value. For array values, the predicate function expects the single array
     * elements (comparing the array will be done internally). Default value is
     * `Object.is`.
     */
    equality?: (v1: ControlBaseT<VT>, v2: ControlBaseT<VT>) => boolean;

    /**
     * If set to `true`, the form control will not write its current value into
     * the "data-state" element attribute. Default value is `false`.
     */
    skipStateAttr?: boolean;

    /**
     * Converts a control value to a string used as element data attribute
     * values for automated testing (e.g. the "data-state" attribute). For
     * array values, the function expects the single array elements (the array
     * will be serialized internally).
     */
    valueSerializer?: ControlValueSerializerFn<ControlBaseT<VT>>;
}

/**
 * A callback function invoked when the value of a form control changes.
 *
 * @param value
 *  The new value of the form control.
 *
 * @param event
 *  The DOM source event that caused the changed control value.
 */
export type ControlChangeHandlerFn<VT> = (value: VT, event: Opt<Event>) => void;

/**
 * Options for the method `Control#setValue`.
 */
export interface ControlSetValueOptions {

    /**
     * The DOM source event that caused the changed control value.
     */
    event?: Event;

    /**
     * If ste to `true`, a changed control value will not be notified to the
     * change listeners of the control. Default value is `false`.
     */
    silent?: boolean;
}

/**
 * Options for the method `Control#onChange`.
 */
export interface ControlOnChangeOptions {

    /**
     * If set to `true`, the registered change callback handler will be invoked
     * immediately with the current value of the form control. Default value
     * is `false`.
     */
    invokeNow?: boolean;
}

/**
 * Helper interface for interoperability between new form controls based on the
 * class `Control` in this module, and old deprecated `Group` controls. Both
 * class families must implement the public API defined in this interface.
 */
export interface IGenericControl<VT = unknown> extends EObject<any> {

    readonly el: HTMLElement;

    readonly value: VT;
    readonly enabled: boolean;
    readonly visible: boolean;
    readonly focused: boolean;

    setValue(value: Opt<VT>, options?: ControlSetValueOptions): void;
    onChange(fn: ControlChangeHandlerFn<VT>, options?: ControlOnChangeOptions): void;

    show(key?: string): void;
    hide(key?: string): void;
    toggle(state?: boolean, key?: string): void;

    enable(state: boolean, key?: string): void;
}

/**
 * Type mapping for the events emitted by `Control` instances.
 */
export interface ControlEventMap<VT> {

    /**
     * Will be emitted when the control has changed its value.
     *
     * @param value
     *  The new value of the form control.
     */
    change: [value: VT, event: Opt<Event>];

    /**
     * Will be emitted while editing inside the control, before changing its
     * value (e.g. typing in a text field).
     */
    input: [];

    /**
     * Will be emitted after the control has been enabled.
     */
    enable: [];

    /**
     * Will be emitted after the control has been disabled.
     */
    disable: [];

    /**
     * Will be emitted after the control has been shown (and it was hidden
     * before).
     */
    show: [];

    /**
     * Will be emitted after the control has been hidden (and it was visible
     * before).
     */
    hide: [];

    /**
     * Will be emitted after the size of the root element has been changed.
     *
     * @param size
     *  The new width and height of the root element, in pixels.
     */
    resize: [size: Size];
}

// globals ====================================================================

/**
 * Specific logger for form controls.
 */
export const controlLogger = new Logger("office:log-control", { tag: "CONTROL", tagColor: 0x00FFFF });

// class Control ==============================================================

/**
 * Abstract base class for form control elements, defining a common API.
 *
 * @template VT
 *  The type of the value carried by the form control.
 *
 * @template ConfT
 *  The exact type of the configuration. Will become the type of the property
 *  `config`.
 */
export abstract class Control<
    VT = unknown,
    ConfT extends ControlConfig<VT> = ControlConfig<VT>,
    EvtMapT extends ControlEventMap<VT> = ControlEventMap<VT>
> extends EObject<EvtMapT> implements IGenericControl<VT> {

    /**
     * A globally unique identifier to be used as DOM attributes, especially
     * for the attributes "id" and "for".
     */
    readonly id: string;

    /**
     * The configuration options passed to the constructor.
     */
    readonly config: Readonly<ConfT>;

    /**
     * The `<div>` root container element of this form control.
     */
    readonly el: HTMLDivElement;

    // equality predicate for control values
    readonly #equality: (v1: ControlBaseT<VT>, v2: ControlBaseT<VT>) => boolean;
    // a set with all disabled flags
    readonly #disabled = new Set<string>();
    // a set with all hidden flags
    readonly #hidden = new Set<string>();

    // current identification key
    #key: Opt<string>;
    // current control value
    #value: VT;
    // the last cached size of the root element, used to detect changes
    #size: Opt<Size>;

    // workaround for type errors with extensible event maps
    get #evt(): Control<VT> { return this as Control<VT>; }

    // constructor ------------------------------------------------------------

    protected constructor(type: string, value: VT, config: NoInfer<Opt<ConfT>>) {
        super(config);

        // remove configuration options that will be applied at the root element
        // to prevent reapplying element classes, identifier, etc. at child elements
        const { classes, id, attrs, dataset, style, ...configRest } = config ?? {};

        // public properties
        this.id = `docs-${this.uid}`;
        this.config = configRest as ConfT;

        // create the root DOM element
        this.el = createDiv({ classes, id, attrs, dataset, style });
        this.el.classList.add("docs-control", `docs-${type}`);
        this.key = config?.key ?? "";

        // private properties
        this.#value = value;
        this.#equality = config?.equality ?? Object.is;

        // set "data-state" attribute for initial value
        this.#refreshStateAttr();

        // log a few events emitted by this form control
        this.logEvents(controlLogger, ["enable", "disable", "show", "hide"]);
        this.logEvents(controlLogger, "resize", size => `width=${size.width} height=${size.height}`);
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the current identification key of this form control.
     */
    get key(): Opt<string> {
        return this.#key;
    }

    /**
     * Changes the identification key of this form control.
     */
    set key(key: Opt<string>) {
        this.#key = key;
        setElementDataAttr(this.el, "key", key);
    }

    /**
     * Returns the current value of this form control.
     */
    get value(): VT {
        return this.#value;
    }

    /**
     * Changes the value of this form control.
     */
    set value(value: VT) {
        this.setValue(value);
    }

    /**
     * Returns whether this form control is currently enabled.
     */
    get enabled(): boolean {
        return !this.#disabled.size;
    }

    /**
     * Changes the default enabled state of this form control.
     */
    set enabled(state: boolean) {
        this.enable(state);
    }

    /**
     * Returns whether this form control is currently in "visible" state. Does
     * not tell anything about the effective rendering visibility (i.e. whether
     * the form control is attached to the DOM, and all parant elements are
     * visible).
     */
    get visible(): boolean {
        return !this.#hidden.size;
    }

    /**
     * Changes the default visibility state of this form control.
     */
    set visible(state: boolean) {
        this.toggle(state);
    }

    /**
     * Returns whether this form control contains the browser focus.
     */
    get focused(): boolean {
        return containsFocus(this.el, { allowSelf: true });
    }

    // public methods ---------------------------------------------------------

    /**
     * Renders the DOM structure of this form control (by calling the internal
     * subclass implementation).
     *
     * The distinction between construction and rendering is needed to be able
     * to use the complete instance with all its overridden or abstract methods
     * implemented in the subclasses. It can also be used for performance. Fast
     * construction of large UI components can be combined with lazy rendering
     * on demand.
     *
     * _Attention:_ This method must be called exactly once to finalize this
     * form control instance.
     *
     * @returns
     *  The DOM root element of this form control (`this.el`), for convenience.
     */
    @onceMethod
    @controlLogger.profileMethod("$badge{Control} render")
    render(): HTMLDivElement {
        this.implRender();
        this.refresh();
        return this.el;
    }

    /**
     * Updates the DOM structure according to the current control value.
     */
    refresh(): void {
        controlLogger.trace(() => `$badge{${this.constructor.name}} refresh: value=${debug.stringify(this.value)}`);
        this.implRefresh();
        this.#refreshStateAttr();
        this.updateSize();
    }

    /**
     * Changes the value of this control.
     */
    setValue(value: VT, options?: ControlSetValueOptions): void {

        // update internal value
        const changed = !this.equalsValue(value);
        this.#value = value;

        // update the "data-state" attribute of the root element used in automated tests
        // (DOCS-4080: also if value has not changed, state attribute may depend on other data)
        this.#refreshStateAttr();

        // refresh internal state (also in silent mode)
        if (changed) { this.refresh(); }

        // trigger change event for external handlers
        if (changed && !options?.silent) {
            this.#evt.trigger("change", value, options?.event);
        }
    }

    /**
     * Sets a handler for the "change" event triggered when changing the
     * control's value, and optionally invokes it immediately.
     *
     * @param fn
     *  The callback function to be invoked when the control value changes.
     *
     * @param [options]
     *  Optional parameters.
     */
    onChange(fn: ControlChangeHandlerFn<VT>, options?: ControlOnChangeOptions): void {
        this.on("change", fn);
        if (options?.invokeNow) { fn.call(this, this.#value, undefined); }
    }

    /**
     * Enables or disables this form control.
     *
     * @param state
     *  Whether to enable (`true`) or disable (`false`) this form control.
     *
     * @param [key="default"]
     *  A slot key that can be used to combine different independent enabled
     *  flags for this form control. The control will become enabled if and
     *  only if all known enabled flags are set.
     */
    enable(state: boolean, key?: string): void {

        // toggle the disabled flag (fallback to default key)
        const oldEnabled = this.enabled;
        set.toggle(this.#disabled, key || "default", !state);

        // update the DOM visibility
        const newEnabled = this.enabled;
        if (oldEnabled !== newEnabled) {
            this.el.classList.toggle("disabled", !newEnabled);
            this.implEnable();
            this.#evt.trigger(newEnabled ? "enable" : "disable");
        }
    }

    /**
     * Displays this form control, if it is currently hidden.
     *
     * @param [key="default"]
     *  A slot key that can be used to combine different independent visibility
     *  flags for this form control. The control will become visible if and
     *  only if all known visibility flags are set.
     */
    show(key?: string): void {
        this.toggle(true, key);
    }

    /**
     * Hides this form control, if it is currently visible.
     *
     * @param [key="default"]
     *  A slot key that can be used to combine different independent visibility
     *  flags for this form control. The control will become visible if and
     *  only if all known visibility flags are set.
     */
    hide(key?: string): void {
        this.toggle(false, key);
    }

    /**
     * Toggles the visibility of this form control.
     *
     * @param [state]
     *  If specified, shows or hides the form control depending on the boolean
     *  value. If omitted, toggles the current visibility of the form control.
     *
     * @param [key="default"]
     *  A slot key that can be used to combine different independent visibility
     *  flags for this form control. The control will become visible if and
     *  only if all known visibility flags are set.
     */
    toggle(state?: boolean, key?: string): void {

        // toggle the hidden flag (fallback to default key)
        const oldVisible = this.visible;
        set.toggle(this.#hidden, key || "default", is.boolean(state) ? !state : undefined);

        // update the DOM visibility
        const newVisible = this.visible;
        if (oldVisible !== newVisible) {
            this.el.style.display = newVisible ? "" : "none";
            this.#evt.trigger(newVisible ? "show" : "hide");
            this.updateSize(); // size may have changed while control was hidden
        }
    }

    /**
     * Sets the browser focus to this form control.
     *
     * @returns
     *  Whether the focus has been set into the form control. Will always
     *  return `false`, if this control is disabled. Some form controls may
     *  choose to not grab focus even if they are enabled.
     */
    focus(): boolean {
        return this.enabled && (this.implFocus() !== false);
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses implement a second construction step, after the complete
     * instance has been created (all subclass constructors have finished), to
     * be able to run abstract or overridden methods implemented in subclasses.
     */
    protected abstract implRender(): void;

    /**
     * Subclasses implement refreshing the DOM structure according to the
     * current value of the form control. This method will always be called
     * after the control value has changed (also in "silent mode" without
     * "change" event).
     */
    protected abstract implRefresh(): void;

    /**
     * Subclasses must implement to enable or disable the DOM form control
     * elements.
     */
    protected abstract implEnable(): void;

    /**
     * Subclasses must implement to focus the DOM form control element.
     */
    protected abstract implFocus(): void | boolean;

    /**
     * Renders a `<label>` DOM element into the root element, if the options
     * bag passed to the constructor contains a non-empty `label` property.
     */
    protected renderLabel(): void {
        // do not put element ID and CSS classes of control root element into the label
        const label = pick.string(this.config, "label");
        if (label) { this.el.append(createLabel({ label, for: this.id })); }
    }

    /**
     * Returns whether the passed value equals the current value of this form
     * control, using the optional equality predicate function passed in the
     * constructor.
     *
     * @param value
     *  The value to be compared with the current value of this form control.
     *
     * @returns
     *  Whether the passed value equals the current value of this form control.
     */
    protected equalsValue(value: VT): boolean {
        type BT = ControlBaseT<VT>;
        if (is.array(this.value) && is.array(value)) {
            return ary.equals(this.value as BT[], value as BT[], this.#equality.bind(this));
        }
        return this.#equality(this.value as BT, value as BT);
    }

    /**
     * Returns whether the passed value equals the current value of this form
     * control, using the optional equality predicate function passed in the
     * constructor.
     *
     * @param values
     *  A data source with the values to be searched.
     *
     * @param value
     *  The value to be searched in the passed data source.
     *
     * @returns
     *  Whether the value is contained in the data source.
     */
    protected includesValue(values: Iterable<ControlBaseT<VT>>, value: ControlBaseT<VT>): boolean {
        return itr.some(values, v => this.#equality(v, value));
    }

    /**
     * Serializes the passed control value using the serializer function passed
     * in the configuration of this instance.
     */
    protected serializeValue(value: VT | ControlBaseT<VT>): string | null {
        const serialized = serializeControlValue(value as OrArray<ControlBaseT<VT>>, this.config.valueSerializer);
        if (serialized && (serialized.length > 100)) {
            controlLogger.warn(() => [`$badge{${this.constructor.name}} oversized serialized value (key="${this.el?.dataset.key}"):`, serialized]);
        }
        return serialized;
    }

    /**
     * Reads the current outer size of the root element of this form control,
     * and triggers a "resize" event if the form control is visible and its
     * size has changed. Does nothing if the form control is currently hidden.
     */
    protected updateSize(): void {

        // do nothing if the control is not visible
        if (!this.visible) { return; }

        // read current element size, notify listeners if size has changed
        const size = getCeilNodeSize(this.el);
        if (!this.#size || (this.#size.width !== size.width) || (this.#size.height !== size.height)) {
            this.#size = size;
            this.#evt.trigger("resize", size);
        }
    }

    // private methods --------------------------------------------------------

    #refreshStateAttr(): void {
        if (!this.config.skipStateAttr) {
            setElementDataAttr(this.el, "state", this.serializeValue(this.#value));
        }
    }
}
