/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, itr, ary, map } from "@/io.ox/office/tk/algorithms";
import { EventHub } from "@/io.ox/office/tk/events";
import type { ElementStyleOptions, CreateElementLabelOptions, ControlCaptionOptions } from "@/io.ox/office/tk/dom";
import { type ControlConfig, type ControlEventMap, Control } from "@/io.ox/office/tk/form/control";

// types ======================================================================

/**
 * Generic configuration for a value entry in a list control.
 */
export interface ListControlEntry<VT> extends ElementStyleOptions, ControlCaptionOptions {

    /**
     * The value associated to the list entry.
     */
    value: VT;
}

/**
 * Generic data source for the list configuration entries of a `ListControl`.
 */
export type ListControlEntrySource<VT, ET extends ListControlEntry<VT>> = Iterable<ET> | ListControl<VT, ET, any, any>;

/**
 * Generic configuration options for a list control.
 *
 * If the option `label` will be passed, a `<label>` element will be created
 * and inserted before the form control.
 */
export interface ListControlConfig<VT> extends ControlConfig<VT>, CreateElementLabelOptions { }

/**
 * Type mapping for the events emitted by `ListControl` instances.
 */
export interface ListControlEventMap<VT> extends ControlEventMap<VT> { }

/**
 * A filter predicate function for list entries.
 */
export type ListControlFilterFn<ET extends ListControlEntry<any>> = (entry: ET) => boolean;

// class ListEntryProvider ====================================================

/**
 * Container for the list entries of a `ListControl`. Instances can be shared
 * between multiple control instances, e.g. a helper `ButtonList` control that
 * has been embedded in a `DropdownList` control.
 */
export class ListEntryContainer<VT, ET extends ListControlEntry<VT>> extends EventHub<{ refresh: [] }> {

    /** All registered list entries. */
    readonly #allEntries: readonly ET[];
    /** The matching filtered list entries. */
    readonly #matchingEntries: ET[] = [];
    /** The values of all matching list entries. */
    readonly #matchingValues = new Set<VT>();
    /** All registered entry filters. */
    readonly #entryFilters = new Map<string, ListControlFilterFn<ET>>();

    // constructor ------------------------------------------------------------

    constructor(entries: Iterable<ET>) {
        super();
        this.#allEntries = Array.from(entries);
        this.#refreshMatchingEntries();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the list entry with the passed value matches all
     * registered filters.
     *
     * @param value
     *  The list value to be checked.
     *
     * @returns
     *  Whether the list entry with the specified value matches all registered
     *  filters.
     */
    isMatching(value: VT): boolean {
        return this.#matchingValues.has(value);
    }

    /**
     * Returns the registered list entries.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be returned.
     *
     * @returns
     *  The registered list entries.
     */
    getEntries(matching?: boolean): readonly ET[] {
        return matching ? this.#matchingEntries : this.#allEntries;
    }

    /**
     * Refreshes the matching list entries according to the registered filters,
     * and emits a "refresh" event.
     */
    refreshEntries(): void {
        this.#refreshMatchingEntries();
        this.emit("refresh");
    }

    /**
     * Registers a filter predicate function for the list entries, and emits a
     * "refresh" event. A formerly registered filter will be dropped.
     *
     * @param filterKey
     *  The identification key of the filter.
     *
     * @param predicateFn
     *  The predicate function for the list entries.
     */
    filterEntries(filterKey: string, predicateFn: Opt<ListControlFilterFn<ET>>): void {
        map.toggle(this.#entryFilters, filterKey, predicateFn);
        this.refreshEntries();
    }

    // private methods --------------------------------------------------------

    /**
     * Refreshes the members `matchedEntries` and `matchingValues` according to
     * the results of the registered filters.
     */
    #refreshMatchingEntries(): void {
        this.#matchingEntries.length = 0;
        this.#matchingValues.clear();
        for (const entry of this.#allEntries) {
            if (itr.every(this.#entryFilters.values(), fn => fn(entry))) {
                this.#matchingEntries.push(entry);
                this.#matchingValues.add(entry.value);
            }
        }
    }
}

// class ListControl ==========================================================

/**
 * Abstract base class for a list control consisting of multiple list elements.
 */
export abstract class ListControl<VT, ET extends ListControlEntry<VT>, ConfT extends ListControlConfig<VT>, EvtMapT extends ListControlEventMap<VT> = ListControlEventMap<VT>>
    extends Control<VT, ConfT, EvtMapT> {

    // all registered list entries
    readonly #container: ListEntryContainer<VT, ET>;

    // constructor ------------------------------------------------------------

    protected constructor(type: string, entries: ListControlEntrySource<VT, ET>, value: VT, config?: NoInfer<ConfT>) {
        super(type, value, config);

        // private properties
        this.#container = (entries instanceof ListControl) ? entries.#container : new ListEntryContainer(entries);

        // refresh control after entry filters have been changed
        this.listenTo(this.#container, "refresh", () => {
            this.implRefreshEntries();
            this.refresh();
        });
    }

    // public methods ---------------------------------------------------------


    /**
     * Refreshes the list of matching list entries, according to the registered
     * filters. Useful when a filter depends on some dynamic runtime state.
     */
    refreshEntries(): void {
        // container will emit "refresh" event which will cause to refresh other listeners too
        this.#container.refreshEntries();
    }

    /**
     * Registers a filter predicate function for the list entries. A formerly
     * registered filter will be dropped.
     *
     * @param filterKey
     *  The identification key of the filter.
     *
     * @param predicateFn
     *  The predicate function for the list entries.
     */
    filterEntries(filterKey: string, predicateFn: Opt<ListControlFilterFn<ET>>): void {
        // container will emit "refresh" event which will cause to refresh other listeners too
        this.#container.filterEntries(filterKey, predicateFn);
    }

    /**
     * Returns whether the list entry with the passed value matches all
     * registered filters.
     *
     * @param value
     *  The control value to be checked.
     *
     * @returns
     *  Whether the list entry with the specified value matches all registered
     *  filters.
     */
    isMatchingValue(value: VT): boolean {
        return this.#container.isMatching(value);
    }

    /**
     * Returns whether the list entry with the passed index matches all
     * registered filters.
     *
     * @param index
     *  The index of the list entry to be checked. Negative indexes count from
     *  the end of the list.
     *
     * @returns
     *  Whether the list entry with the specified index matches all registered
     *  filters.
     */
    isMatchingIndex(index: number): boolean {
        const entry = ary.at(this.#container.getEntries(), index);
        return !!entry && this.#container.isMatching(entry.value);
    }

    /**
     * Returns the list entries contained in this control.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be returned.
     *
     * @returns
     *  The list entries contained in this control.
     */
    getEntries(matching?: boolean): readonly ET[] {
        return this.#container.getEntries(matching);
    }

    /**
     * Creates an iterator that visits all entries together with their index
     * and filter matching state.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be visited.
     *
     * @yields
     *  A list entry descriptor, consisting of entry index, entry value, entry
     *  configuration object, and filter matching state.
     */
    *yieldEntries(matching?: boolean): IterableIterator<{ index: number; value: VT; entry: ET; matching: boolean }> {
        for (const [index, entry] of this.#container.getEntries(matching).entries()) {
            yield { index, value: entry.value, entry, matching: this.#container.isMatching(entry.value) };
        }
    }

    /**
     * Returns the number of list entries in this control.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be counted.
     *
     * @returns
     *  The number of list entries in this control.
     */
    getEntryCount(matching?: boolean): number {
        return this.#container.getEntries(matching).length;
    }

    /**
     * Returns the configuration of the list entry with the specified index.
     *
     * @param index
     *  The index of the list entry to be looked up. Negative indexes count
     *  from the end of the list.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be counted.
     *
     * @returns
     *  The configuration of the list entry, if available.
     */
    getEntryAt(index: number, matching?: boolean): Opt<ET> {
        return ary.at(this.#container.getEntries(matching), index);
    }

    /**
     * Returns the configuration of the list entry with the specified value.
     *
     * @param value
     *  The value of the list entry to be looked up.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be searched.
     *
     * @returns
     *  The configuration of the list entry, if available.
     */
    getEntryOf(value: VT, matching?: boolean): Opt<ET> {
        return this.#container.getEntries(matching).find(entry => value === entry.value);
    }

    /**
     * Returns the index of the list entry with the specified value.
     *
     * @param value
     *  The value of the list entry to be looked up.
     *
     * @param matching
     *  If set to `true`, only list entries that match all registered filters
     *  will be counted.
     *
     * @returns
     *  The index of the list entry with the specified value, or `-1`, if no
     *  list entry is available.
     */
    getIndexOf(value: VT, matching?: boolean): number {
        return this.#container.getEntries(matching).findIndex(entry => value === entry.value);
    }

    /**
     * Returns the list entry that is currently selected.
     *
     * @returns
     *  The list entry that is currently selected.
     */
    getSelectedEntry(): Opt<ET> {
        return this.getEntryOf(this.value, true);
    }

    /**
     * Selects the list entry with the specified list index among the list
     * entries matching all registered filters.
     *
     * @param index
     *  The index of the list entry (matching all registered filters) to be
     *  selected. Negative indexes count from the end of the list.
     *
     * @returns
     *  The list entry that has been selected.
     */
    selectEntryAt(index: number): Opt<ET> {
        const entry = this.getEntryAt(index, true);
        if (entry) { this.setValue(entry.value); }
        return entry;
    }

    /**
     * Selects a predecessor of the list entry currently selected.
     *
     * @param dec
     *  The number of entries to proceed backwards.
     *
     * @returns
     *  The list entry that has been selected.
     */
    selectPrevEntry(dec = 1): Opt<ET> {
        const index = this.getIndexOf(this.value, true);
        const count = this.#container.getEntries(true).length;
        return this.selectEntryAt(math.clamp(index - dec, 0, count - 1));
    }

    /**
     * Selects a successor of the list entry currently selected.
     *
     * @param inc
     *  The number of entries to proceed ahead.
     *
     * @returns
     *  The list entry that has been selected.
     */
    selectNextEntry(inc = 1): Opt<ET> {
        const index = this.getIndexOf(this.value, true);
        const count = this.#container.getEntries(true).length;
        return this.selectEntryAt(math.clamp(index + inc, 0, count - 1));
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses must refresh the visibility of all list entries in the DOM
     * according to the registered filters.
     */
    protected abstract implRefreshEntries(): void;
}
