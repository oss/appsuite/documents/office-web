/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, re, fun } from "@/io.ox/office/tk/algorithms";
import { type CSSLengthUnit, convertHmmToLength, convertLengthToHmm } from "@/io.ox/office/tk/dom";
import { type LengthUnit, LOCALE_DATA } from "@/io.ox/office/tk/locale";

// types ======================================================================

/**
 * Configuration options for the class `TextValidator`.
 */
export interface TextValidatorConfig {

    /**
     * The maximum number of characters to be inserted into the text field. All
     * attempts to insert more characters will be rejected. default value is
     * `0x7fffffff` (2^31-1).
     */
    maxLength?: number;
}

/**
 * A callback function used to calculate the minimum and maximum boundary of a
 * `NumberValidator` dynamically.
 */
export type NumberBoundaryFn = () => number;

/**
 * The minimum and maximum boundary of a `NumberValidator`, either as constant
 * number, or as dynamic callback function.
 */
export type NumberBoundarySpec = number | NumberBoundaryFn;

/**
 * Configuration options for the class `NumberValidator`.
 */
export interface NumberValidatorConfig {

    /**
     * The minimum value allowed to enter. Default is unlimited.
     */
    min?: NumberBoundarySpec;

    /**
     * The maximum value allowed to enter. Default is unlimited.
     */
    max?: NumberBoundarySpec;

    /**
     * The precision used to round the current number. The number will be
     * rounded to multiples of this value. Must be positive. If omitted, the
     * default precision `1` will round to integers.
     */
    precision?: number;
}

/**
 * Configuration options for the class `LengthValidator`.
 */
export interface LengthValidatorConfig extends NumberValidatorConfig {

    /**
     * The length unit to be used for the display strings.
     */
    unit?: LengthUnit;
}

// constants ==================================================================

// RE pattern for integer part
const INT_PATTERN = "[-+]?\\d*";

// small step sizes for measurement units in spin field
const UNIT_STEPS: Record<LengthUnit, number> = { cm: 0.1, mm: 1, in: 0.05 };

// private functions ==========================================================

/**
 * Returns the RE pattern for the fractional part of a number with the current
 * decimal separator.
 */
function fracPattern(): string {
    // `LOCALE_DATA.dec` may change at runtime, therefore this is not a global constant
    return re.group.opt(re.escape(LOCALE_DATA.dec) + "\\d*");
}

/**
 * Returns the RE pattern for a decimal number with the current decimal
 * separator.
 */
function numPattern(): string {
    return INT_PATTERN + fracPattern();
}

// class AbstractValidator ====================================================

/**
 * Base class for text field validators used to convert between values of a
 * specific type and field display texts, and to validate the text field while
 * editing.
 */
export abstract class AbstractValidator<T> {

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed value to a text string to be inserted into a text
     * field.
     *
     * @param value
     *  The value to be converted to a text.
     *
     * @returns
     *  The text converted from the passed value, or an empty string, if the
     *  passed value cannot be converted to text.
     */
    abstract valueToText(value: T | null): string;

    /**
     * Converts the passed text to a value that will be passed to all event
     * listeners of a text field.
     *
     * @param text
     *  The text to be converted to a value.
     *
     * @returns
     *  The value converted from the passed text. The value null indicates that
     *  the text cannot be converted to a valid value.
     */
    abstract textToValue(text: string): T | null;

    /**
     * Validates the passed text that has been changed while editing a text
     * field. It is possible to return a new string value that will be inserted
     * into the text field, or a boolean value indicating whether to restore
     * the old state of the text field.
     *
     * @param text
     *  The current contents of the text field to be validated.
     *
     * @returns
     *  When returning a string, the text field will be updated to contain the
     *  returned value while restoring its selection (browsers may destroy the
     *  selection when changing the text). When returning the value `false`,
     *  the previous state of the text field (as it was after the last
     *  validation) will be restored. Otherwise, the value of the text field is
     *  considered to be valid and will not be modified.
     */
    abstract validate(text: string): string | boolean;
}

// class TextValidator ========================================================

/**
 * A validator for text fields that restricts the allowed values according to
 * the passed options.
 *
 * @param [config]
 *  Initial configuration options.
 */
export class TextValidator extends AbstractValidator<string> {

    // properties -------------------------------------------------------------

    readonly #maxLength: number;

    // properties -------------------------------------------------------------

    constructor(config?: TextValidatorConfig) {
        super();
        this.#maxLength = config?.maxLength ?? 0x7FFF_FFFF;
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed value to a text string that will be restricted to
     * the maximum length passed to the constructor of this instance.
     */
    valueToText(value: string | null): string {
        return value ? value.slice(0, this.#maxLength) : "";
    }

    /**
     * Returns the passed text as is.
     */
    textToValue(value: string): string | null {
        return value;
    }

    /**
     * Returns whether the length of the passed text does not exceed the
     * maximum text length of this instance.
     */
    validate(text: string): string | boolean {
        return text.length <= this.#maxLength;
    }
}

// class AbstractNumberValidator ==============================================

/**
 * Base class for text field validators that restricts the allowed values to
 * floating-point numbers.
 *
 * @param [config]
 *  Initial configuration options.
 */
export abstract class AbstractNumberValidator extends AbstractValidator<number> {

    // properties -------------------------------------------------------------

    /** The rounding precision. */
    readonly prec: number;

    // resolver function for the minimum value
    readonly #minFn: NumberBoundaryFn;
    // resolver function for the maximum value
    readonly #maxFn: NumberBoundaryFn;

    // constructor ------------------------------------------------------------

    constructor(config?: NumberValidatorConfig) {
        super();

        this.prec = config?.precision ?? 1;

        this.#minFn = is.function(config?.min) ? config.min : fun.const(config?.min ?? -Infinity);
        this.#maxFn = is.function(config?.max) ? config.max : fun.const(config?.max ?? Infinity);
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns the minimum value allowed to enter.
     */
    get min(): number {
        return this.#minFn();
    }

    /**
     * Returns the maximum value allowed to enter.
     */
    get max(): number {
        return this.#maxFn();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the passed text can be converted to a number.
     *
     * @param text
     *  The current contents of the text field to be validated.
     *
     * @returns
     *  Whether the passed text is empty, or can be converted to a number.
     */
    validate(text: string): string | boolean {
        return this.regex.test(text);
    }

    /**
     * Returns the passed number bound to the lower and upper limit.
     *
     * @param value
     *  The number to be converted. If not a finite number, it will be returned
     *  as passed.
     *
     * @returns
     *  The passed number bound to the lower and upper limit.
     */
    restrictValue(value: number): number {
        return Number.isFinite(value) ? math.clamp(value, this.#minFn(), this.#maxFn()) : value;
    }

    /**
     * Converts the passed canonical value to a unit that will be displayed in
     * a spin field, and that will be stepped and rounded by the spin field.
     *
     * Intended to be overwritten by subclasses.
     *
     * @param value
     *  The value to be converted to its step unit.
     *
     * @returns
     *  The converted value.
     */
    valueToStepValue(value: number): number {
        return value;
    }

    /**
     * Converts the passed step value used in a spin field to the canonical
     * value represented by this validator (and provided as value by the spin
     * field).
     *
     * Intended to be overwritten by subclasses.
     *
     * @param value
     *  The step value to be converted to the canonical value.
     *
     * @returns
     *  The canonical value.
     */
    stepValueToValue(value: number): number {
        return value;
    }

    // protected methods ------------------------------------------------------

    /**
     * Subclasses must return the pattern for a regular expression that will be
     * used in the method `validate()`.
     */
    protected abstract get pattern(): string;

    /**
     * Returns the regular expression used to match input text.
     */
    protected get regex(): RegExp {
        return new RegExp(`^${this.pattern}$`);
    }
}

// class NumberValidator ======================================================

/**
 * A validator for text fields that restricts the allowed values to
 * floating-point numbers.
 *
 * @param [config]
 *  Initial configuration options.
 */
export class NumberValidator extends AbstractNumberValidator {

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed value to a text string to be inserted into a text
     * field.
     *
     * @param value
     *  The value to be converted to a text. If not a finite number, this
     *  method returns an empty string.
     *
     * @returns
     *  The text converted from the passed number, or an empty string, if the
     *  passed value is not a finite number.
     */
    valueToText(value: number | null): string {
        return Number.isFinite(value) ? LOCALE_DATA.formatDecimal(math.roundp(value!, this.prec)) : "";
    }

    /**
     * Tries to convert the passed text to a number.
     *
     * @param text
     *  The text to be converted to a number.
     *
     * @returns
     *  The number converted from the passed text; or `null` to indicate that
     *  the text cannot be converted to a number.
     */
    textToValue(text: string): number | null {
        const value = LOCALE_DATA.parseDecimal(text);
        return (Number.isFinite(value) && (this.min <= value) && (value <= this.max)) ? math.roundp(value, this.prec) : null;
    }

    // protected methods ------------------------------------------------------

    /**
     * The pattern for a regular expression for the method `validate()`.
     */
    protected get pattern(): string {
        return (this.prec === Math.round(this.prec)) ? INT_PATTERN : numPattern();
    }
}

// class PercentValidator =====================================================

/**
 * A validator for spin fields that restricts the allowed values to integers
 * with percent unit.
 *
 * @param [config]
 *  Initial configuration options. By default, the allowed range of numbers
 *  will be set from a minimum of `0` to a maximum of `100`.
 */
export class PercentValidator extends AbstractNumberValidator {

    constructor(config?: NumberValidatorConfig) {
        super({ min: 0, max: 100, ...config });
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed value to a display string with percent sign.
     *
     * @param percent
     *  The percentage value to be converted.
     *
     * @returns
     *  The display string for the passed value, converted to percentage.
     */
    valueToText(percent: number | null): string {
        return `${this.restrictValue(Number.isFinite(percent) ? Math.round(percent!) : 0)} %`;
    }

    /**
     * Converts the passed display string of a percentage to a number.
     *
     * @param text
     *  The display string of a percentage value.
     *
     * @returns
     *  The parsed percentage; or `null`, if the passed text is valid.
     */
    textToValue(text: string): number | null {
        const matches = this.regex.exec(text);
        return this.restrictValue(matches ? parseInt(matches[1], 10) : 0);
    }

    // protected methods ------------------------------------------------------

    /**
     * The pattern for a regular expression for the method `validate()`.
     */
    protected get pattern(): string {
        return `(${INT_PATTERN})\\s*%?`;
    }
}

// class LengthValidator ======================================================

/**
 * A validator for spin fields that restricts the allowed values to numbers and
 * optionally an additional length unit. Supported units are the absolute CSS
 * length units "px", "pc", "pt", "in", "cm", and "mm". The numeric length
 * values are always in 1/100 mm.
 *
 * @param [config]
 *  Initial configuration options.
 */
export class LengthValidator extends AbstractNumberValidator {

    // properties ------------------------------------------------------------

    // the length unit used by this validator
    readonly #unit?: LengthUnit;

    // constructor ------------------------------------------------------------

    constructor(config?: LengthValidatorConfig) {
        super(config);
        this.#unit = config?.unit;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns the length unit to be used for the display strings.
     */
    get unit(): LengthUnit {
        return this.#unit ?? LOCALE_DATA.unit;
    }

    /**
     * Returns the standard step for modifying the specified length unit.
     */
    get step(): number {
        return UNIT_STEPS[this.unit];
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed value to a display string with length unit.
     *
     * @param value
     *  The length in 1/100 mm.
     *
     * @returns
     *  The display string for the passed value, with length unit. If the value
     *  is invalid, an empty string will be returned.
     */
    valueToText(value: number | null): string {
        value = this.valueToStepValue(this.restrictValue(Number.isFinite(value) ? value! : 0));
        return `${LOCALE_DATA.formatDecimal(value)} ${this.unit}`;
    }

    /**
     * Converts the passed display string of a length with measurement unit to
     * a number. Supports all absolute CSS length units.
     *
     * @param text
     *  The display string of a length with measurement unit.
     *
     * @returns
     *  The length in 1/100 mm or null, if the passed text is invalid.
     */
    textToValue(text: string): number | null {

        const matches = this.regex.exec(text);

        // parse the leading floating-point number
        const value = LOCALE_DATA.parseDecimal(matches?.[1] || "");
        if (!Number.isFinite(value)) { return this.restrictValue(0); }

        // parse the length unit (bug 48909: fall-back to standard unit)
        const unitStr = (matches?.[2] || "").toLowerCase();
        const unit = /^(px|pc|pt|in|cm|mm)$/.test(unitStr) ? (unitStr as CSSLengthUnit) : this.unit;

        // convert to 1/100 mm, and restrict to the allowed interval
        return this.restrictValue(convertLengthToHmm(value, unit));
    }

    /**
     * Converts the passed value from 1/100 mm to the length unit used by this
     * validator.
     *
     * @param value
     *  The length in 1/100 mm to be converted.
     *
     * @returns
     *  The length in the unit used by this validator.
     */
    override valueToStepValue(value: number): number {
        return convertHmmToLength(value, this.unit, this.step);
    }

    /**
     * Converts the passed value from the length unit used by this validator to
     * 1/100 mm.
     *
     * @param value
     *  The length to be converted to 1/100 mm.
     *
     * @returns
     *  The length in 1/100 mm.
     */
    override stepValueToValue(value: number): number {
        return convertLengthToHmm(value, this.unit);
    }

    // protected methods ------------------------------------------------------

    /**
     * The pattern for a regular expression for the method `validate()`.
     */
    protected get pattern(): string {
        return `(${numPattern()})\\s*([a-zA-Z]*)`;
    }
}
