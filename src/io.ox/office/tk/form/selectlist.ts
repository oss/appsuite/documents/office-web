/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { replaceChildren, createElement, createFragment, enableElement } from "@/io.ox/office/tk/dom";
import type { ListControlEntry, ListControlEntrySource, ListControlConfig } from "@/io.ox/office/tk/form/listcontrol";
import { ListControl } from "@/io.ox/office/tk/form/listcontrol";

import "@/io.ox/office/tk/form/selectlist.less";

// types ======================================================================

/**
 * Configuration for a value entry in a `SelectList` control.
 */
export interface SelectListEntry<VT> extends ListControlEntry<VT> {

    /**
     * The text label for the list entry.
     */
    label: string; // convert optional to required property

    /**
     * Tooltips not supported in native `<select>` controls.
     */
    tooltip?: never;

    /**
     * Icons not supported in native `<select>` controls.
     */
    icon?: never;
}

/**
 * Configuration options for a `SelectList` control.
 *
 * If the option `label` will be passed, a `<label>` element will be created
 * and inserted before the `<select>` element.
 */
export interface SelectListConfig<VT> extends ListControlConfig<VT> {

    /**
     * The number of visible entries in the `<select>` list. If set to `1`, the
     * list control will switch to dropdown appearance. Default value is `10`.
     */
    size?: number;
}

// class SelectList ===========================================================

/**
 * A wrapper for a `<select>` list control.
 */
export class SelectList<VT, ET extends SelectListEntry<VT> = SelectListEntry<VT>>
    extends ListControl<VT, ET, SelectListConfig<VT>> {

    /**
     * The DOM `<select>` element.
     */
    readonly selectEl: HTMLSelectElement;

    // constructor ------------------------------------------------------------

    constructor(entries: ListControlEntrySource<VT, ET>, value: VT, config?: SelectListConfig<VT>) {
        super("selectlist", entries, value, config);

        // create the <select> control
        this.selectEl = createElement("select", { id: this.id, classes: "form-control" });
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the number of entries shown in the `<select>` list element.
     */
    get size(): number {
        return this.selectEl.size;
    }

    /**
     * Changes the number of entries shown in the `<select>` list element.
     */
    set size(size: number) {
        this.selectEl.size = size;
    }

    /**
     * Returns the zero-based index of the selected entry in the `<select>`
     * list element. Only list elements that are not filtered will be counted.
     */
    get index(): number {
        return this.selectEl.selectedIndex;
    }

    /**
     * Selects the entry with the specified zero-based index in the `<select>`
     * list element. Only list elements that are not filtered will be counted.
     */
    set index(index: number) {
        this.selectEl.selectedIndex = index;
    }

    // public methods ---------------------------------------------------------

    /**
     * Sets the size (number of visible entries) of the `<select>` control to a
     * value that fits into the specified CSS pixel height.
     *
     * @param height
     *  The maximum pixel height for the `<select>` control (total height with
     *  borders).
     *
     * @param [minSize=2]
     *  The minimum number of list entries to be shown, regardless of available
     *  space.
     */
    fitSizeInto(height: number, minSize?: number): void {
        const borderSize = this.selectEl.offsetHeight - this.selectEl.clientHeight;
        const entryHeight = this.selectEl.clientHeight / this.selectEl.size;
        const size = Math.floor((height - borderSize) / entryHeight);
        this.selectEl.size = Math.max(size, minSize || 2);
    }

    /**
     * Scrolls the selected entry of the `<select>` control into the visible
     * area.
     */
    scrollToSelected(): void {
        const optionEl = this.selectEl.children.item(this.index);
        optionEl?.scrollIntoView({ block: "nearest" });
    }

    // protected methods ------------------------------------------------------
    /**
     * Renders the entire DOM structure of this list control.
     */
    override implRender(): void {

        // create an inline <label> element if specified
        this.renderLabel();

        // initialize the <select> control
        this.el.appendChild(this.selectEl);
        this.selectEl.size = this.config.size || 10;

        // generate all list entries
        this.implRefreshEntries();

        // update control value when selecting an entry in the list
        this.listenTo(this.selectEl, "change", event => {
            const entry = this.getEntryAt(this.index, true);
            if (entry) { this.setValue(entry.value, { event }); }
        });

        // always scroll selected entry into the view
        this.onChange(this.scrollToSelected);
    }

    /**
     * Updates the DOM structure according to the current control value.
     */
    protected override implRefresh(): void {
        // select an `<option>` element in the `<select>` element
        this.index = this.getIndexOf(this.value, true);
    }

    /**
     * Enables or disables the `<select>` element.
     */
    protected override implEnable(): void {
        enableElement(this.selectEl, this.enabled);
    }

    /**
     * Sets the browser focus to the `<select>` element.
     */
    protected override implFocus(): void {
        this.selectEl.focus();
    }

    /**
     * Refreshes the visibility of all option elements.
     */
    protected override implRefreshEntries(): void {
        const fragment = createFragment();
        for (const { entry } of this.yieldEntries(true)) {
            const optionEl = fragment.appendChild(createElement("option", entry));
            if (is.string(entry.value) || is.number(entry.value)) {
                optionEl.value = String(entry.value);
            }
        }
        replaceChildren(this.selectEl, fragment);
    }
}
