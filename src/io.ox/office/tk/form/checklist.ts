/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { is, ary } from "@/io.ox/office/tk/algorithms";
import { detachChildren, setElementDataAttr, createDiv, createFragment, createButton, createIcon } from "@/io.ox/office/tk/dom";
import { type ControlConfig, type ControlSetValueOptions, controlLogger, Control } from "@/io.ox/office/tk/form/control";
import { type CheckBoxConfig, CheckBox } from "@/io.ox/office/tk/form/checkbox";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

import "@/io.ox/office/tk/form/checklist.less";

// types ======================================================================

/**
 * Configuration for a checkbox entry in a `CheckList` control.
 */
export interface CheckListEntry<VT> extends CheckBoxConfig<true> {

    /**
     * The value associated to the list entry.
     */
    value: VT;

    /**
     * The text label for the list entry.
     */
    label: string; // convert optional to required property

    /**
     * Configuration for the child entries in tree views.
     */
    children?: this[];
}

/**
 * Configuration options for a `CheckList` control.
 */
export interface CheckListConfig<VT, ET extends CheckListEntry<VT>> extends ControlConfig<VT[]> {

    /**
     * If set to `true`, the checkboxes will be sorted by their labels. If set
     * to a comparator function, the list entries will be sorted accordingly.
     * Default value is `false`.
     */
    sort?: boolean | FuncType<number, Pair<ET>>;

    /**
     * If set to `true`, an additional special checkbox with the label "Select
     * all" will be shown on top of all other checkboxes. Clicking this
     * checkbox will switch the state of all other checkboxes to the new state
     * of the "Select all" checkbox.
     */
    checkAll?: boolean;
}

// constants ==================================================================

//#. Label of a special check box on top of an entire check list, that checks/unchecks all entries in the list
const SELECT_ALL_LABEL = gt.pgettext("check-list", "Select all");

// class CheckListNode ========================================================

/**
 * A checkbox with an indented child checkbox list. Renders a collapse/expand
 * button if the child list is not empty.
 */
class CheckListNode<VT, ET extends CheckListEntry<VT>> extends Control<readonly VT[]> {

    // the list entry configuration
    readonly #entry: ET;

    // the main checkbox representing the state of the child list
    readonly mainCbox: CheckBox<true, ET>;
    // the collapsable checklist control for the child checkboxes
    readonly #childList: Opt<CheckList<VT, ET>>;

    // constructor ------------------------------------------------------------

    constructor(entry: ET, config: CheckListConfig<VT, ET>) {

        // do not use "children" and "checkAll" parent configuration options
        const childConfig: CheckListConfig<VT, ET> = {
            equality: config.equality,
            valueSerializer: config.valueSerializer,
            skipStateAttr: true,
        };

        super("checklist-node", [], childConfig);

        // private properties
        this.#entry = entry;

        // create the checkbox control
        this.mainCbox = this.member(new CheckBox(false, entry));

        // update own checked state according to contents of the check list
        this.mainCbox.onChange(state => this.setValue(state ? [entry.value] : []));

        // create an embedded `CheckList` control for the subtree
        if (entry.children?.length) {
            this.#childList = this.member(new CheckList<VT, ET>(entry.children, [], {
                ...childConfig,
                sort: config.sort,
            }));
        }
    }

    // public accessors -------------------------------------------------------

    get size(): number {
        return this.#entry.children?.length ?? 0;
    }

    get checked(): boolean | null {
        return this.mainCbox.value;
    }

    // public methods ---------------------------------------------------------

    /**
     * Calculates and updates the own value according to the passed value of
     * the parent checklist.
     */
    refreshValue(values: readonly VT[]): void {
        const { value } = this.#entry;
        controlLogger.trace(() => `$badge{${this.constructor.name}} refreshValue [${this.serializeValue([value])}]: values=${this.serializeValue(values)}`);

        // check the checkbox if the *own* value is contained (subtree will be checked automatically)
        if (this.includesValue(values, value)) {
            this.setValue([value], { silent: true });
        } else if (this.#childList) {
            // restrict passed values to the values supported by the child list
            values = this.#childList.filterValues(values);
            this.setValue(values, { silent: true });
            controlLogger.trace(() => ` new childlist value: ${this.serializeValue(this.#childList!.value)}`);
        } else {
            this.setValue([], { silent: true });
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Renders the entire DOM structure of this control.
     */
    protected override implRender(): void {

        // set the entry value as data attribute to the root element
        setElementDataAttr(this.el, "value", this.serializeValue(this.#entry.value));

        // the container for the collapse button and checkbox
        const headEl = this.el.appendChild(createDiv("entry-head"));

        // render the contents of this control
        const childList = this.#childList;
        if (childList) {

            // update own checked state according to contents of the check list
            childList.onChange(() => {
                this.mainCbox.setValue(childList.checked, { silent: true });
                this.setValue(this.mainCbox.value ? [this.#entry.value] : childList.value);
            });

            // forward "resize" events
            childList.on("resize", () => this.updateSize());

            // the collapse button
            const button = headEl.appendChild(createButton({
                type: "bare",
                classes: "collapse-btn",
                action: "collapse",
                click: () => {
                    this.el.classList.toggle("collapsed");
                    this.updateSize();
                }
            }));

            // create the collapse/expand icons
            button.append(
                createIcon("bi:caret-right-fill", { classes: "collapsed" }),
                createIcon("bi:caret-down-fill", { classes: "expanded" })
            );

            // render the child checklist
            this.el.append(childList.render());
            this.el.classList.add("collapsed");

            // always show collapse buttons in embedded checklists (also without child entries)
            childList.el.classList.add("collapsable");

            // keyboard handlers
            this.el.addEventListener("keydown", event => this.#handleKeyEvent(event));

        } else {
            // placeholder for collapse button to correctly align list item
            headEl.append(createDiv("collapse-btn"));
            this.el.classList.add("collapsed");
        }

        // render the checkbox
        headEl.append(this.mainCbox.render());
    }

    /**
     * Updates the DOM structure of the child list according to the current
     * model value.
     */
    protected override implRefresh(): void {
        if (this.includesValue(this.value, this.#entry.value)) {
            this.#childList?.toggleAll(true, { silent: true });
            this.mainCbox.setValue(true, { silent: true });
        } else if (this.#childList) {
            this.#childList.setValue(this.value, { silent: true });
            this.mainCbox.setValue(this.#childList.checked, { silent: true });
        } else {
            this.mainCbox.setValue(false, { silent: true });
        }
    }

    /**
     * Enables or disables the checkboxes in the child list.
     */
    protected override implEnable(): void {
        this.mainCbox.enable(this.enabled);
        this.#childList?.enable(this.enabled);
    }

    /**
     * Sets the browser focus to the leading checkbox.
     */
    protected override implFocus(): void {
        this.mainCbox.focus();
    }

    // private methods --------------------------------------------------------

    #handleKeyEvent(event: KeyboardEvent): void {
        switch (event.key) {
            case "+": this.el.classList.remove("collapsed"); break;
            case "-": this.el.classList.add("collapsed");    break;
        }
    }
}

// class CheckList ============================================================

/**
 * A form control with multiple checkboxes.
 *
 * @template VT
 *  The type of the values represented by the checkboxes.
 *
 * @template ET
 *  The type of the checkbox configuration descriptor objects. May be a subtype
 *  of `CheckListEntry` with additional properties, e.g. for sorting.
 */
export class CheckList<VT, ET extends CheckListEntry<VT> = CheckListEntry<VT>>
    extends Control<readonly VT[], CheckListConfig<VT, ET>> {

    // the configuration of all checkboxes in the list
    #entries: ET[] = [];
    // all values of own list entries
    #ownValues: VT[] = [];
    // all values of own and deeply embedded list entries
    readonly #allValues: VT[] = [];

    // the "Select All" checkbox
    readonly #allCbox: CheckBox<true>;
    // container element for all list checkboxes
    readonly #container = createDiv("list-container");
    // all checkboxes in the checklist
    readonly #listNodes = this.member<Array<CheckListNode<VT, ET>>>([]);

    // constructor ------------------------------------------------------------

    /**
     * @param entries
     *  Descriptors for all entries (checkboxes) to be shown in the checklist.
     *
     * @param values
     *  The values of all checkboxes that will be checked initially.
     *
     * @param [config]
     *  Configuration options.
     */
    constructor(entries: Iterable<ET>, values: Iterable<VT>, config?: CheckListConfig<VT, ET>) {
        super("checklist", Array.from(values), config);

        // collect all list values from the entries
        this.#initEntries(entries);

        // create the leading "Select all" checkbox
        this.#allCbox = this.member(new CheckBox(false, {
            classes: "select-all",
            label: SELECT_ALL_LABEL,
        }));
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the number of toplevel checkboxes in this list (without the
     * "Select all" checkbox).
     */
    get size(): number {
        return this.#entries.length;
    }

    /**
     * Returns whether none of the checkboxes is checked or in ambiguous state.
     */
    get noneChecked(): boolean {
        // neither checked nor tristate checkboxes
        return this.#listNodes.every(listNode => listNode.checked === false);
    }

    /**
     * Returns whether all checkboxes are checked.
     */
    get allChecked(): boolean {
        // neither unchecked nor tristate checkboxes
        return this.#listNodes.every(listNode => listNode.checked === true);
    }

    /**
     * Returns a state flag for the checked state of all checkboxes (`true` if
     * all checkboxes are checked, `false` if no checkbox is checked, `null`
     * otherwise).
     */
    get checked(): boolean | null {
        return this.allChecked ? true : this.noneChecked ? false : null;
    }

    // public methods ---------------------------------------------------------

    /**
     * Replaces the checkboxes in this control with new checkboxes according to
     * the passed configuration.
     *
     * @param entries
     *  Configuration for all checkboxes to be created in the list.
     *
     * @param [value]
     *  A new value to be initialized for this checklist.
     */
    @controlLogger.profileMethod("$badge{CheckList} replaceEntries")
    replaceEntries(entries: Iterable<ET>, value?: VT[]): void {

        // restore settings after replacing the checkboxes
        const { focused } = this;

        // remove and destroy all checkboxes from the list (except "Select all")
        detachChildren(this.#container);
        ary.destroy(this.#listNodes);

        // initialize new settings
        this.#initEntries(entries);
        if (value) { this.setValue(value); }

        // render all checkboxes
        this.#renderListNodes();
        if (focused) { this.focus(); }
    }

    /**
     * Removes all checkboxes from this control.
     */
    clearEntries(): void {
        if (this.#entries.length) {
            this.replaceEntries([]);
        }
    }

    /**
     * Filters the passed array of list values to the values supported by this
     * checklist (covered by the list entries and their children).
     */
    filterValues(values: readonly VT[]): readonly VT[] {
        return values.filter(val => this.includesValue(this.#allValues, val));
    }

    /**
     * Checks or unchecks all list entries.
     */
    toggleAll(state: boolean, options?: ControlSetValueOptions): void {
        this.setValue(state ? this.#ownValues : [], options);
    }

    // protected methods ------------------------------------------------------

    /**
     * Renders the entire DOM structure of this form control.
     */
    protected override implRender(): void {

        // update checkboxes in list when "Select all" checkbox has changed
        this.#allCbox.onChange(state => {
            if (state !== null) { this.toggleAll(state); }
        });

        // render all checkboxes
        this.el.append(this.#allCbox.render(), this.#container);
        this.#renderListNodes();
    }

    /**
     * Updates the list nodes according to the current model value.
     */
    protected override implRefresh(): void {

        // update all checkboxes in the list
        for (const listNode of this.#listNodes) {
            listNode.refreshValue(this.value);
        }

        // update the "Select all" checkbox
        this.#allCbox.setValue(this.checked, { silent: true });
    }

    /**
     * Enables or disables this checkbox.
     */
    protected override implEnable(): void {
        this.#allCbox.enable(this.enabled);
        for (const listNode of this.#listNodes) {
            listNode.enable(this.enabled);
        }
    }

    /**
     * Sets the browser focus to this checkbox.
     */
    protected override implFocus(): void {
        (this.#checkAllVisible() ? this.#allCbox : this.#listNodes[0])?.focus();
    }

    // private methods --------------------------------------------------------

    /**
     * Recursively pushes the values from the passed entries and all their
     * descendants into the `#allValues` array.
     */
    #collectValues(entries: Iterable<ET>): void {
        for (const entry of entries) {
            this.#allValues.push(entry.value);
            if (entry.children) {
                this.#collectValues(entry.children);
            }
        }
    }

    /**
     * Stores the passed list entries and initializes other settings.
     */
    #initEntries(entries: Iterable<ET>): void {

        // copy list entries from data source
        this.#entries = Array.from(entries);

        // collect all own values in a flat array
        this.#ownValues = this.#entries.map(entry => entry.value);

        // collect all nested list values in a flat array
        this.#allValues.length = 0;
        this.#collectValues(this.#entries);
    }

    /**
     * Returns whether to show the "Check all" checkbox.
     */
    #checkAllVisible(): boolean {
        return !!this.config.checkAll && (this.#entries.length > 1);
    }

    /**
     * Creates and renders all checkboxes in the list.
     */
    #renderListNodes(): void {

        // sort all entries if specified
        const sorter = this.config.sort;
        if (is.function(sorter)) {
            this.#entries.sort(sorter);
        } else if (sorter) {
            LOCALE_DATA.getCollator({ numeric: true }).sortByProp(this.#entries, "label");
        }

        // the fragment used to collect all new checkboxes (performance)
        const fragment = createFragment();

        // create checkboxes for all toplevel entries
        for (const entry of this.#entries) {
            const listNode = new CheckListNode(entry, this.config);
            fragment.append(listNode.render());
            this.#listNodes.push(listNode); // array takes ownership
            // synchronize the own array value with changes of the checkboxes
            listNode.onChange(() => this.setValue(this.#listNodes.flatMap(n => n.value)));
            // forward "resize" events
            listNode.on("resize", () => this.updateSize());
        }

        // make room and show the collapse buttons, if any checkbox contains a sublist
        this.el.classList.toggle("collapsable", this.#listNodes.some(n => n.size > 0));

        // insert all checkboxes at once into the DOM
        this.#container.append(fragment);

        // show or hide "Select all" checkbox
        this.#allCbox.el.classList.toggle("hidden", !this.#checkAllVisible());

        // initialize checked states of all checkboxes
        this.refresh();
    }
}
