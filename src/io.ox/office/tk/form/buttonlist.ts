/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ary, map } from "@/io.ox/office/tk/algorithms";
import { createDiv, createButton, enableElement, setElementDataAttr } from "@/io.ox/office/tk/dom";
import type { ListControlEntry, ListControlEntrySource, ListControlConfig } from "@/io.ox/office/tk/form/listcontrol";
import { ListControl } from "@/io.ox/office/tk/form/listcontrol";

import "@/io.ox/office/tk/form/buttonlist.less";

// types ======================================================================

/**
 * Configuration for a value entry in a `ButtonList` control.
 */
export interface ButtonListEntry<VT> extends ListControlEntry<VT> {

    /**
     * The identifier of a section to insert the button element into. Sections
     * are top-level `<div>` elements containing all buttons. Default value is
     * the empty string.
     */
    section?: string;
}

/**
 * Configuration options for a `ButtonList` control.
 */
export interface ButtonListConfig<VT, ET extends ButtonListEntry<VT>> extends ListControlConfig<VT> {

    /**
     * Default alignment of the text labels. Default value is "center".
     */
    labelAlign?: ET["labelAlign"];

    /**
     * A custom render function that allows to render arbitrary contents into
     * the button elements. Will be called for every rendered button element,
     * after their default caption contents have been rendered.
     */
    renderButton?: (elem: HTMLButtonElement, entry: ET) => void;

    /**
     * Whether to display the buttons vertically instead of horizontally. Will
     * also influence keyboard shortcuts (up/down instead of left/right).
     * Default value is `false`.
     */
    vertical?: boolean;
}

// class ButtonList ===========================================================

/**
 * A wrapper for a list control consisting of multiple button elements.
 */
export class ButtonList<VT, ET extends ButtonListEntry<VT> = ButtonListEntry<VT>>
    extends ListControl<VT, ET, ButtonListConfig<VT, ET>> {

    // the section containers
    readonly #sections = new Map<string, HTMLDivElement>();
    // the DOM button elements
    readonly #buttons: HTMLButtonElement[] = [];

    // the selected button element
    #selectedButton: Opt<HTMLButtonElement>;

    // constructor ------------------------------------------------------------

    constructor(entries: ListControlEntrySource<VT, ET>, value: VT, config?: ButtonListConfig<VT, ET>) {
        super("buttonlist", entries, value, config);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the button element for the specified value.
     *
     * @param value
     *  The value of the button element to be looked up.
     *
     * @param matching
     *  If set to `true`, only visible button elements (for list entries that
     *  match all registered filters) will be counted.
     *
     * @returns
     *  The button element of the specified list entry.
     */
    getButtonOf(value: VT, matching?: boolean): Opt<HTMLButtonElement> {
        return this.#buttons[this.getIndexOf(value, matching)];
    }

    /**
     * Returns the button element with the specified list index.
     *
     * @param index
     *  The index of a list entry. Negative indexes count from the end of the
     *  list.
     *
     * @param matching
     *  If set to `true`, only visible button elements (for list entries that
     *  match all registered filters) will be counted.
     *
     * @returns
     *  The button element of the specified list entry.
     */
    getButtonAt(index: number, matching?: boolean): Opt<HTMLButtonElement> {
        if (!matching) { return ary.at(this.#buttons, index); }
        const entry = this.getEntryAt(index, true);
        return entry && this.#buttons[this.getIndexOf(entry.value)];
    }

    // protected methods ------------------------------------------------------

    /**
     * Renders the entire DOM structure of this list control.
     */
    override implRender(): void {

        // initialize root element
        setElementDataAttr(this.el, "vertical", this.config.vertical);

        // create an inline <label> element if specified
        this.renderLabel();

        // initialize all button elements
        const { labelAlign, renderButton } = this.config;
        for (const { value, entry } of this.yieldEntries()) {
            const sectionEl = map.upsert(this.#sections, entry.section ?? "", () => {
                return this.el.appendChild(createDiv("docs-buttonlist-section"));
            });
            const dataset = { value: this.serializeValue(value) };
            const button = sectionEl.appendChild(createButton({
                dataset,
                labelAlign,
                ...entry,
                type: "bare",
                click: event => this.setValue(entry.value, { event })
            }));
            renderButton?.(button, entry);
            this.#buttons.push(button);
        }

        // initialize visibility of all buttons
        this.implRefreshEntries();
    }

    /**
     * Updates the DOM structure according to the current model value.
     */
    protected override implRefresh(): void {
        this.#selectedButton?.classList.remove("selected");
        this.#selectedButton = this.getButtonOf(this.value);
        this.#selectedButton?.classList.add("selected");
    }

    /**
     * Enables or disables the button elements.
     */
    protected override implEnable(): void {
        for (const button of this.#buttons) {
            enableElement(button, this.enabled);
        }
    }

    /**
     * Sets the browser focus to the selected button element; ot to the first
     * button element, if none is selected.
     */
    protected override implFocus(): void {
        const button = this.#selectedButton ?? this.getButtonAt(0, true);
        button?.focus();
    }

    /**
     * Refreshes the visibility of all button elements.
     */
    protected override implRefreshEntries(): void {
        for (const { index, matching } of this.yieldEntries()) {
            this.#buttons[index].style.display = matching ? "" : "none";
        }
    }
}
