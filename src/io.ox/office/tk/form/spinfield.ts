/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math } from "@/io.ox/office/tk/algorithms";
import type { CreateElementLabelOptions } from "@/io.ox/office/tk/dom";
import { createDiv, createCaret, createInput, createButton, enableElement, setElementAttributes, handleDOMEvent, hasModifierKeys } from "@/io.ox/office/tk/dom";
import { type NumberValidatorConfig, NumberValidator } from "@/io.ox/office/tk/form/validators";
import { type ControlConfig, Control } from "@/io.ox/office/tk/form/control";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";

import "@/io.ox/office/tk/form/spinfield.less";

// types ======================================================================

/**
 * Configuration options for an `SpinField` control.
 *
 * If the option `label` will be passed, a `<label>` element will be created
 * and inserted before the `<input>` element.
 */
export interface SpinFieldConfig extends ControlConfig<number>, CreateElementLabelOptions, NumberValidatorConfig {

    /**
     * The distance of steps that will be used to increase or decrease the
     * current value of the spin field when using the spin buttons or the
     * cursor keys. Default value is `1`.
     */
    step?: number;

    /**
     * Multiplier for the option "step" for the distance of page steps that
     * will be used to increase or decrease the current value of the spin field
     * when using the PAGE-UP or PAGE-DOWN key. Must be an integer. Default
     * value is `10`.
     */
    page?: number;

    /**
     * If set to `true`, and the current value of the spin field will be
     * changed by typing text, the value will be rounded to entire multiples of
     * the step distance. Otherwise, the fractional part will remain in the
     * value. Default value is `false`.
     */
    round?: boolean;
}

// class SpinField ============================================================

/**
 * A wrapper for a numeric `<input>` control with up/down spin buttons.
 */
export class SpinField extends Control<number, SpinFieldConfig> {

    /**
     * The `<input>` element for the current number.
     */
    readonly inputEl: HTMLInputElement;

    // the `<button>` element of the "Up" spin button
    readonly #buttonUp: HTMLButtonElement;
    // the `<button>` element of the "Down" spin button
    readonly #buttonDn: HTMLButtonElement;

    // the validator for string/number conversion
    readonly #validator: NumberValidator;

    // the distance for small/large steps
    readonly #step: number;
    readonly #page: number;

    // constructor ------------------------------------------------------------

    /**
     * @param value
     *  The initial value of the textfield.
     *
     * @param config
     *  Configuration options for the textfield control.
     */
    constructor(value: number, config?: SpinFieldConfig) {
        super("spinfield", value, config);

        // private properties
        this.#validator = new NumberValidator(config);
        this.#step = config?.step ?? 1;
        this.#page = this.#step * Math.round(config?.page ?? 10);

        // create the text input element
        this.inputEl = createInput({ ...this.config, classes: "form-control", id: this.id });

        // create the spin button elements
        [this.#buttonUp, this.#buttonDn] = (["up", "down"] as const).map(dir => {
            const spinButton = createButton({ classes: `spin-button spin-${dir}`, focusable: false });
            spinButton.append(createCaret(dir));
            return spinButton;
        });
    }

    // protected methods ------------------------------------------------------

    protected override implRender(): void {

        // create an inline <label> element if specified
        this.renderLabel();

        // create element wrappers, and insert everything into the root element
        const spinWrapper = createDiv("spin-wrapper");
        spinWrapper.append(this.#buttonUp, this.#buttonDn);
        const inputWrapper = createDiv("input-wrapper");
        inputWrapper.append(this.inputEl, spinWrapper);
        this.el.append(inputWrapper);

        // update model when committing the input field
        // DOCS-3731: ENTER key does not cause "change" event in Firefox
        this.listenTo(this.inputEl, ["change", "blur", "keydown"], event => {
            if (!(event instanceof KeyboardEvent) || (event.key === "Enter")) {
                const newValue = this.#validator.textToValue(this.inputEl.value);
                if (Number.isFinite(newValue)) { this.value = newValue!; }
            }
        });

        // update value on cursor keys
        this.listenTo(this.inputEl, "keydown", handleDOMEvent(this.#inputKeyDownHandler));

        // forward "input" events for live validation
        this.listenTo(this.inputEl, "input", () => this.trigger("input"));

        // immediately return focus to `<input>` when clicking spin button (also disabled!)
        this.listenTo(spinWrapper, ["mousedown", "touchstart"], event => {
            event.preventDefault();
            this.setTimeout(this.focus);
        });

        // initialize tracking observer for spin buttons
        const observer = this.member(new TrackingObserver({
            prepare: () => this.enabled,
            start: record => this.#changeValue(this.#step, record.id === "up"),
            repeat: "start",
        }));

        // observe the spin buttons
        const TRACKING_CONFIG = { repeatDelay: 700, repeatInterval: 100 };
        observer.observe(this.#buttonDn, { ...TRACKING_CONFIG, id: "dn" });
        observer.observe(this.#buttonUp, { ...TRACKING_CONFIG, id: "up" });
    }

    /**
     * Updates the DOM structure according to the current model value.
     */
    protected override implRefresh(): void {

        // do not access the input element if the value does not change (browsers will destroy current selection)
        const text = this.#validator.valueToText(this.value);
        if (this.inputEl.value !== text) {
            this.inputEl.value = text;
        }

        // update spin button states
        const { min, max } = this.#validator;
        enableElement(this.#buttonUp, this.value < max);
        enableElement(this.#buttonDn, this.value > min);

        // update ARIA attributes
        setElementAttributes(this.inputEl, {
            role: "slider", // note: role "spinbutton" would be correct but does not work with VoiceOver
            "aria-valuenow": this.value,
            "aria-valuetext": text,
            "aria-valuemin": min,
            "aria-valuemax": max,
        });
    }

    /**
     * Enables or disables the `<input>` element.
     */
    protected override implEnable(): void {
        enableElement(this.inputEl, this.enabled);
    }

    /**
     * Sets the browser focus to the `<input>` element.
     */
    protected override implFocus(): void {
        this.inputEl.focus();
    }

    // private methods --------------------------------------------------------

    /**
     * Changes the current value of the spin field, according to the step and
     * the rounding mode passed to the constructor.
     */
    #changeValue(step: number, inc: boolean): void {

        const diff = step * (inc ? 1 : -1);

        // the current value of the spin field (converted to step unit)
        const oldStepValue = this.#validator.valueToStepValue(this.value);
        // the current value, rounded to the previous/next border
        const roundedValue = (diff < 0) ? math.floorp(oldStepValue, -diff) : math.ceilp(oldStepValue, diff);
        // the new value, depending on rounding mode
        const newStepValue = (!this.config.round || (oldStepValue === roundedValue)) ? (oldStepValue + diff) : roundedValue;

        // set new value after restricting to minimum/maximum
        this.value = this.#validator.restrictValue(this.#validator.stepValueToValue(newStepValue));
    }

    /**
     * Handler for keyboard events received from the text field.
     */
    #inputKeyDownHandler(event: KeyboardEvent): boolean {
        if (this.enabled && !hasModifierKeys(event)) {
            switch (event.key) {
                case "ArrowUp":     this.#changeValue(this.#step, true);    return true;
                case "ArrowDown":   this.#changeValue(this.#step, false);   return true;
                case "PageUp":      this.#changeValue(this.#page, true);    return true;
                case "PageDown":    this.#changeValue(this.#page, false);   return true;
            }
        }
        return false;
    }
}
