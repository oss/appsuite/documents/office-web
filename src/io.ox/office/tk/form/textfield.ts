/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str } from "@/io.ox/office/tk/algorithms";
import type { CreateInputOptions, ElementLabelOptions } from "@/io.ox/office/tk/dom";
import { createInput, enableElement } from "@/io.ox/office/tk/dom";
import { type ControlConfig, Control } from "@/io.ox/office/tk/form/control";

import "@/io.ox/office/tk/form/textfield.less";

// types ======================================================================

/**
 * Configuration options for an `<input>` text control.
 *
 * If the option `label` will be passed, a `<label>` element will be created
 * and inserted before the `<input>` element.
 */
export interface TextFieldConfig extends ControlConfig<string>, CreateInputOptions, ElementLabelOptions {

    /**
     * If set to `true`, leading and trailing whitespace will be trimmed from
     * the string. Default value is `false`.
     */
    autoTrim?: boolean;
}

// class TextField ============================================================

/**
 * A wrapper for an `<input>` text control.
 */
export class TextField extends Control<string, TextFieldConfig> {

    /**
     * The DOM `<input>` element.
     */
    readonly inputEl: HTMLInputElement;

    /**
     * Maximum allowed length in the text input field.
     */
    readonly maxLen: number;

    // constructor ------------------------------------------------------------

    /**
     * @param value
     *  The initial value of the textfield.
     *
     * @param config
     *  Configuration options for the textfield control.
     */
    constructor(value: string, config?: TextFieldConfig) {
        super("textfield", value, config);

        // public properties
        this.maxLen = Math.max(0, this.config.maxLength ?? 0);

        // create the <input> element
        this.inputEl = createInput({ ...this.config, classes: "form-control", id: this.id });
    }

    // accessors --------------------------------------------------------------

    /**
     * Returns the current raw text in the `<input>` element. While typing
     * inside the field, this value may be different from the property `value`
     * which reflects the model value after committing.
     */
    get rawText(): string {
        return this.inputEl.value;
    }

    // protected methods ------------------------------------------------------

    /**
     * Renders the entire DOM structure of this control.
     */
    protected override implRender(): void {

        // create an inline <label> element if specified
        this.renderLabel();

        // insert the <input> element
        this.el.append(this.inputEl);

        // update model when committing the input field
        // DOCS-3731: ENTER key does not cause "change" event in Firefox
        this.listenTo(this.inputEl, ["change", "blur", "keydown"], event => {
            if (!(event instanceof KeyboardEvent) || (event.key === "Enter")) {
                this.setValue(this.inputEl.value, { event });
            }
        });

        // forward "input" events for live validation
        this.listenTo(this.inputEl, "input", () => this.trigger("input"));
    }

    /**
     * Updates the DOM structure according to the current model value.
     */
    protected override implRefresh(): void {
        let value = this.value;
        if (this.config.autoTrim) { value = str.trimAll(value); }
        if (this.maxLen) { value = value.slice(0, this.maxLen); }
        this.inputEl.value = value;
        this.value = value;
    }

    /**
     * Enables or disables the `<input>` element.
     */
    protected override implEnable(): void {
        enableElement(this.inputEl, this.enabled);
    }

    /**
     * Sets the browser focus to the `<input>` element.
     */
    protected override implFocus(): void {
        this.inputEl.focus();
    }
}
