/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import type { CreateControlCaptionOptions, CreateButtonOptions } from "@/io.ox/office/tk/dom";
import { setElementAttributes, setElementStyles, setElementLabel, enableElement, createIcon, createButton, handleDOMEvent, isEscapeKey, getFocus, isTextInputElement } from "@/io.ox/office/tk/dom";
import type { ListControlEntrySource, ListControlConfig, ListControlEventMap } from "@/io.ox/office/tk/form/listcontrol";
import { ListControl } from "@/io.ox/office/tk/form/listcontrol";
import { type ButtonListEntry, ButtonList } from "@/io.ox/office/tk/form/buttonlist";
import { TextField } from "@/io.ox/office/tk/form/textfield";
import { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";

import "@/io.ox/office/tk/form/dropdownlist.less";

// types ======================================================================

/**
 * Configuration for an entry in a dropdown list control.
 */
export interface DropdownListEntry<VT> extends ButtonListEntry<VT> { }

/**
 * Configuration options for a dropdown selection list.
 */
export interface DropdownListConfig<VT, ET extends DropdownListEntry<VT>> extends ListControlConfig<VT>, CreateControlCaptionOptions {

    /**
     * The display style of the menu anchor button. Default value is "default".
     */
    type?: CreateButtonOptions["type"];

    /**
     * Label settings to be applied to the anchor button, if the current value
     * does not correspond to an existing entry in the dropdown list.
     */
    undef?: ET;

    /**
     * Default alignment of the text labels. Default value is "start".
     */
    labelAlign?: ET["labelAlign"];

    /**
     * A custom render function that allows to render arbitrary contents into
     * the button elements. Will be called for every rendered button element,
     * after their default caption contents have been rendered.
     */
    renderButton?: (elem: HTMLButtonElement, entry: ET) => void;

    /**
     * If set, shows a "Quick Search" text field in the dropdown menu and
     * filters the visible list entries.
     *
     * @param query
     *  The query string typed into the text field.
     *
     * @param entry
     *  The configuration of a list entry to be filtered.
     *
     * @returns
     *  Whether to show the respective dropdown list entry.
     */
    quickSearch?: (query: string, entry: ET) => boolean;
}

/**
 * Type mapping for the events emitted by `DropdownList` instances.
 */
export interface DropdownListEventMap<VT> extends ListControlEventMap<VT> {

    /**
     * Will be emitted before the dropdown menu will be shown.
     */
    "menu:beforeshow": [];

    /**
     * Will be emitted after the dropdown menu has been made visible.
     */
    "menu:show": [];

    /**
     * Will be emitted before the dropdown menu will be hidden.
     */
    "menu:beforehide": [];

    /**
     * Will be emitted after the dropdown menu has been hidden.
     */
    "menu:hide": [];
}

// class DropdownList =========================================================

/**
 * A wrapper for a dropdown selection control based on an enumeration of values
 * that shows the active value in a toggle button, styled as a link.
 */
export class DropdownList<VT, ET extends DropdownListEntry<VT> = DropdownListEntry<VT>>
    extends ListControl<VT, ET, DropdownListConfig<VT, ET>, DropdownListEventMap<VT>> {

    /**
     * The DOM `<button>` element representing the anchor button for toggling
     * the list popup menu.
     */
    readonly anchorEl: HTMLButtonElement;

    /**
     * The popup menu instance containing the button list.
     */
    readonly listMenu: BaseMenu;

    /** The option button elements for all list entries. */
    readonly #buttonList: ButtonList<VT, ET>;

    // constructor ------------------------------------------------------------

    constructor(entries: ListControlEntrySource<VT, ET>, value: VT, config?: DropdownListConfig<VT, ET>) {
        super("dropdownlist", entries, value, config);

        // create the dropdown anchor button
        this.anchorEl = createButton({ label: "\xa0", labelAlign: "start", ...this.config });

        // create the dropdown menu
        this.listMenu = this.member(new BaseMenu({
            classes: config?.classes,
            anchor: this.anchorEl,
            stretchToAnchor: true,
        }));

        // create the button list control to be inserted into the dropdown menu
        this.#buttonList = this.member(new ButtonList(this, value, {
            labelAlign: config?.labelAlign ?? "start",
            renderButton: config?.renderButton,
            vertical: true,
            valueSerializer: config?.valueSerializer
        }));

        // marker class for CSS
        this.listMenu.el.classList.add("docs-dropdownlist-menu");
    }

    // protected methods ------------------------------------------------------

    /**
     * Renders the entire DOM structure of this dropdown list.
     */
    protected override implRender(): void {

        // create an inline <label> element if specified
        this.renderLabel();

        // initialize the dropdown anchor button
        this.el.append(this.anchorEl);
        this.anchorEl.classList.add("caret-button");

        // add the caret icon except for link-style buttons
        if (this.config.type !== "link") {
            this.anchorEl.append(createIcon("bi:chevron-down"));
        }

        // insert the button list into the dropdown menu
        this.listMenu.bodyEl.append(this.#buttonList.render());

        // set the ARIA relation between button and menu with the "aria-owns" attribute
        // and use the ARIA label of the button also for the menu
        this.listMenu.el.id = this.listMenu.uid;
        setElementAttributes(this.listMenu.el, { "aria-label": this.anchorEl.getAttribute("aria-label") });
        setElementAttributes(this.anchorEl, { "aria-owns": this.listMenu.uid, "aria-haspopup": true, "aria-expanded": false });

        // register menu button for focus handling (menu remains visible while button is focused)
        this.listMenu.registerFocusableNodes(this.anchorEl);

        // scroll to selected entry when opening the dropdown menu
        this.listenTo(this.listMenu, "popup:show", () => this.#scrollToSelected("center"));

        // forward changed value in the button list
        this.#buttonList.onChange(listValue => this.setValue(listValue));

        // hide popup menu after click on option button
        this.listenTo(this.#buttonList.el, "click", () => this.listMenu.hide());

        // create the "Quick Search" edit field
        const queryPredicate = this.config.quickSearch;
        if (queryPredicate) {
            const searchField = this.member(new TextField("", { classes: "quick-search-input", placeholder: gt("Search ...") }));
            this.listMenu.headerEl.append(searchField.render());
            this.filterEntries("quicksearch", entry => queryPredicate(searchField.rawText, entry));
            this.listenTo(searchField.inputEl, "keydown", handleDOMEvent(this.#handleSelectKey));
            this.listenTo(searchField, ["input", "change"], () => {
                this.refreshEntries();
                this.#scrollToSelected();
            });
            this.listenTo(this.listMenu, "popup:beforeshow", () => searchField.setValue(""));
            this.listenTo(this.listMenu, "popup:show", () => searchField.focus());
        }

        // hide the dropdown menu when disabling this control
        this.on("disable", () => this.listMenu.hide());

        // toggle dropdown menu after clicks
        this.listenTo(this.anchorEl, "click", () => {
            if (this.enabled) { this.listMenu.toggle(); }
        });

        // handle keyboard events on anchor button
        this.listenTo(this.anchorEl, "keydown", handleDOMEvent(event => {
            if (isEscapeKey(event) && this.listMenu.isVisible()) {
                this.listMenu.hide();
                return true;
            }
            if (this.enabled && event.altKey && ((event.key === "ArrowUp") || (event.key === "ArrowDown"))) {
                this.listMenu.toggle();
                return true;
            }
            return this.#handleSelectKey(event);
        }));

        // handle keyboard events in dropdown menu
        this.listenTo(this.#buttonList.el, "keydown", handleDOMEvent(this.#handleSelectKey));

        // return focus to anchor button after Escape key in menu
        this.listenTo(this.listMenu, "menu:close:keyboard", () => this.focus());

        // forward dropdown menu events
        this.listenTo(this.listMenu, "popup:beforeshow", () => { this.trigger("menu:beforeshow"); });
        this.listenTo(this.listMenu, "popup:show",       () => { this.trigger("menu:show"); });
        this.listenTo(this.listMenu, "popup:beforehide", () => { this.trigger("menu:beforehide"); });
        this.listenTo(this.listMenu, "popup:hide",       () => { this.trigger("menu:hide"); });
    }

    /**
     * Updates the DOM structure according to the current model value.
     */
    protected override implRefresh(): Opt<ET> {
        // forward own value to button list
        this.#buttonList.setValue(this.value, { silent: true });
        // scroll selected buttons into view
        this.#scrollToSelected();
        // update the caption of the anchor button
        const entry = this.getSelectedEntry() ?? this.config.undef;
        if (entry) {
            setElementStyles(this.anchorEl, entry.style, { clear: true });
            setElementLabel(this.anchorEl.querySelector(".label-node")!, entry);
            return entry;
        }
        return undefined;
    }

    /**
     * Enables or disables the anchor button element.
     */
    protected override implEnable(): void {
        enableElement(this.anchorEl, this.enabled);
    }

    /**
     * Sets the browser focus to the anchor button element.
     */
    protected override implFocus(): void {
        this.anchorEl.focus();
    }

    /**
     * Refreshes the visibility of all list entries.
     */
    protected implRefreshEntries(): void {
        // nothing to render
    }

    // private methods --------------------------------------------------------

    #scrollToSelected(position?: ScrollLogicalPosition): void {
        const button = this.#buttonList.getButtonOf(this.value);
        button?.scrollIntoView({ block: position ?? "nearest" });
    }

    #getPageSize(): number {
        const menuHeight = this.listMenu.bodyEl.clientHeight;
        const buttonCount = this.getEntryCount(true);
        const buttonHeight = this.#buttonList.el.offsetHeight / buttonCount;
        return Math.floor(menuHeight / buttonHeight) || 10;
    }

    #handleSelectKey(event: KeyboardEvent): boolean {
        if (this.enabled && !event.altKey && !event.metaKey) {

            switch (event.key) {
                case "ArrowUp":     this.selectPrevEntry();                     return true;
                case "ArrowDown":   this.selectNextEntry();                     return true;
                case "PageUp":      this.selectPrevEntry(this.#getPageSize());  return true;
                case "PageDown":    this.selectNextEntry(this.#getPageSize());  return true;
            }

            // HOME/END inside quick-search field only with CTRL key
            if (event.ctrlKey || !isTextInputElement(getFocus())) {
                switch (event.key) {
                    case "Home":        this.selectEntryAt(0);  return true;
                    case "End":         this.selectEntryAt(-1); return true;
                }
            }
        }

        return false;
    }
}
