/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import ox from "$/ox";
import gt from "gettext";

import CoreLocale from "$/io.ox/core/locale";

import type { ReverseOptions } from "@/io.ox/office/tk/algorithms";
import { is, to, str, fmt, map, dict, pick, jpromise } from "@/io.ox/office/tk/algorithms";
import { getStr, setValue } from "@/io.ox/office/tk/config";
import { globalEvents } from "@/io.ox/office/tk/events";

import { localeLogger, getDateSep, getTimeSep, isLeadingYear, isLeadingMonth, isHours24 } from "@/io.ox/office/tk/locale/localeutils";
import { CollatorWrapper } from "@/io.ox/office/tk/locale/collatorwrapper";
import { type LocaleSpec, parseLocaleId } from "@/io.ox/office/tk/locale/localeid";
import type { LengthUnit, LocaleData } from "@/io.ox/office/tk/locale/localedata";
import { LENGTH_UNITS } from "@/io.ox/office/tk/locale/localedata";
import { localeDataRegistry } from "@/io.ox/office/tk/locale/localeregistry";

// types ======================================================================

/**
 * Options for comparing strings for equality.
 */
export interface LocaleEqualStringOptions {

    /**
     * If set to `true`, character case will be considered (e.g. "A" is not
     * equal to "a"). Default value is `false`.
     */
    withCase?: boolean;

    /**
     * If set to `true`, character accents will be ignored (e.g. "á" is equal
     * to "a").
     */
    ignoreAccents?: boolean;
}

/**
 * Options for comparing strings, or sorting arrays with strings. The option
 * `reverse` causes comparison in descending order.
 */
export interface LocaleCompareStringOptions extends ReverseOptions, LocaleEqualStringOptions {

    /**
     * If set to `true`, embedded multi-digit numbers will be considered (e.g.
     * "a2" is less than "a10"). Default value is `false`.
     */
    numeric?: boolean;
}

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted when any properties of the `LOCALE_DATA` singleton
         * have been changed.
         *
         * @param changedProps
         *  A partial `LocaleData` structure containing the changed properties
         * only with their current values.
         */
        "change:locale": [changedProps: Partial<LocaleData>];
    }
}

// constants ==================================================================

/**
 * The user configuration key for the standard length unit used in the GUI.
 */
const STANDARD_UNIT_KEY = "units/standard";

/**
 * Codes of all languages that will always include the region name (also for
 * the default region) when querying translated language names of locale
 * identifiers.
 */
const LANGUAGES_ALWAYS_WITH_REGION = new Set(["en", "pt", "zh"]);

// functions ==================================================================

/**
 * Translates the passed CLDR format code for a date to a format code as used
 * in Documents applications.
 *
 * @param format
 *  The CLDR date format code to be translated.
 *
 * @returns
 *  The date format code as used in the Documents applications.
 */
function translateDateCode(format: string): string {
    return format
        .replaceAll("d", "D")       // day as number
        .replaceAll("y", "Y")       // year
        .replaceAll("EEEE", "DDDD") // long weekday
        .replaceAll("E", "DDD")     // short weekday
        .replaceAll("'", '"');      // literals are enclosed in apostrophes
}

/**
 * Translates the passed CLDR format code for a time to a format code as used
 * in Documents applications.
 *
 * @param format
 *  The CLDR time format code to be translated.
 *
 * @returns
 *  The time format code as used in the Documents applications.
 */
function translateTimeCode(format: string): string {
    return format
        .replaceAll(".", ":")       // always colons in time format codes (dots would cause too much confusion with dates, decimal, or group separator)
        .replaceAll("H", "h")       // hours
        .replaceAll("a", "AM/PM");  // AM/PM token
}

// class LocaleDataUI =========================================================

// declaration merging to add all properties of base interfaces automatically
interface LocaleDataUI extends LocaleData { }

class LocaleDataUI implements LocaleData {

    /**
     * Cache for collators created for the current locale.
     */
    readonly #collators = new Map<string, CollatorWrapper>();

    /**
     * The native locale data settings without customized properties.
     */
    #localeData!: Readonly<LocaleData>;

    /**
     * Provider for translated names of languages.
     */
    #languageNames!: Intl.DisplayNames;

    /**
     * Provider for translated names of regions.
     */
    #regionNames!: Intl.DisplayNames;

    /**
     * Provider for translated names of currencies.
     */
    #currencyNames!: Intl.DisplayNames;

    // constructor ------------------------------------------------------------

    constructor() {

        // get settings for the current UI locale
        this.#initialize();

        // update locale settings when changed
        // OXUIB-249: listen to "change:locale" event that will be triggered
        // after all locale settings have been updated (after a server request
        // done to fetch locale data, listening to settings events is too early)
        ox.on("change:locale", () => this.#initialize());

        // update property `unit` when changed in server configuration
        globalEvents.on("change:config:docs", () => this.#updateProperties(this.#resolveSettings()));
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns the native settings of the current locale without user changes.
     */
    get nativeData(): Readonly<LocaleData> {
        return this.#localeData;
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed string to a number, using the decimal separator of
     * the current UI locale. This is a simple method that uses the native
     * `parseFloat()` conversion function with all its weaknesses.
     *
     * @param text
     *  The text to be converted to a number.
     *
     * @returns
     *  The passed text, converted to a number. If the passed text does not
     *  represent a floating-point number, `NaN` will be returned. If the text
     *  consists of a number with following garbage (e.g. "123abc"), the
     *  leading number will be returned (same behavior as the native function
     *  `parseFloat()`).
     */
    parseDecimal(text: string): number {
        return parseFloat(text.replace(this.dec, "."));
    }

    /**
     * Converts the passed number to a string, using the decimal separator of
     * the current UI locale. This is a simple method that uses the native
     * conversion from numbers to strings.
     *
     * @param value
     *  The number to be converted.
     *
     * @returns
     *  The passed number, converted to string, with the decimal separator of
     *  the current UI locale.
     */
    formatDecimal(value: number): string {
        return String(value).replace(/\./g, this.dec);
    }

    /**
     * Returns a locale collator object to be used to compare strings according
     * to the current UI locale.
     *
     * @param options
     *  Optional configuration parameters for the collator.
     *
     * @returns
     *  A locale collator object for the passed settings.
     */
    getCollator(options?: LocaleCompareStringOptions): CollatorWrapper {
        const sensitivity: Intl.CollatorOptions["sensitivity"] = options?.withCase ? (options?.ignoreAccents ? "case" : "variant") : (options?.ignoreAccents ? "base" : "accent");
        const cacheKey = sensitivity + (options?.numeric ? ":n" : "");
        return map.upsert(this.#collators, cacheKey, () => new CollatorWrapper(this.intl, { sensitivity, numeric: options?.numeric }));
    }

    /**
     * Returns the translated language name for the passed locale.
     *
     * Returns the pure language name regardless of the specified region. Use
     * method `getLanguageAndRegionName` to get the language and region name in
     * one string.
     *
     * @param locale
     *  The locale code as string (using underscores or dashes as separator,
     *  e.g. "de-DE" or "de_DE"), or a resolved `LocaleId` object.
     *
     * @returns
     *  The translated language name for the specified locale; or `undefined`,
     *  if no translated name exists for the passed locale.
     */
    getLanguageName(locale: LocaleSpec): Opt<string> {
        const localeId = parseLocaleId(locale);
        return localeId && str.noI18n(this.#languageNames.of(localeId.language));
    }

    /**
     * Returns the translated name of a region.
     *
     * @param locale
     *  The locale code as string (using underscores or dashes as separator,
     *  e.g. "de-DE" or "de_DE"), or a resolved `LocaleId` object.
     *
     * @returns
     *  The translated name for the region of the specified locale; or
     *  `undefined`, if no translated name exists for the passed locale.
     */
    getRegionName(locale: LocaleSpec): Opt<string> {
        const localeId = parseLocaleId(locale);
        return localeId?.region && str.noI18n(this.#regionNames.of(localeId.region));
    }

    /**
     * Returns the translated names of the language and region of a locale.
     *
     * @param locale
     *  The locale code as string (using underscores or dashes as separator,
     *  e.g. "de-DE" or "de_DE"), or a resolved `LocaleId` object.
     *
     * @param skipDefaultRegion
     *  If set to `true`, and the passed locale identifier refers to the
     *  default region of the language, the translated name of the region will
     *  be omitted (e.g. "German" for "de_DE", but still "German (Austria)" for
     *  the "de_AT"). For a few selected languages, the region name will always
     *  be added regardless of this flag (e.g. English, Portuguese, Chinese).
     *
     * @returns
     *  The translated names for the language and region of the specified
     *  locale; or `undefined`, if no translated names exist for the passed
     *  locale.
     */
    getLanguageAndRegionName(locale: LocaleSpec, skipDefaultRegion?: boolean): Opt<string> {

        // parse the passed locale identifier
        const localeId = parseLocaleId(locale);
        if (!localeId) { return; }

        // resolve the language name
        const { language, region } = localeId;
        const languageName = str.noI18n(this.#languageNames.of(language));
        if (!languageName || !region) { return languageName; }

        // return plain language name unless region name must be added
        if (skipDefaultRegion && !LANGUAGES_ALWAYS_WITH_REGION.has(language) && (localeDataRegistry.getDefaultRegion(language) === region)) {
            return languageName;
        }

        // try to add region name
        const regionName = str.noI18n(this.#regionNames.of(region));
        //#. translated name of a language with region added, e.g.: "Portuguese (Brazil)"
        //#. %1$s language name
        //#. %2$s region name
        return regionName ? gt.pgettext("language", "%1$s (%2$s)", languageName, regionName) : languageName;
    }

    /**
     * Returns the translated currency name for the passed ISO currency code.
     *
     * @param iso
     *  The ISO currency code.
     *
     * @returns
     *  The translated name for the specified currency; or `undefined`, if no
     *  translated name exists for the passed ISO code.
     */
    getCurrencyName(iso: string): Opt<string> {
        return str.noI18n(this.#currencyNames.of(iso));
    }

    /**
     * Changes the standard length unit in the user configuration.
     *
     * @param unit
     *  The new standard length unit to be used in the GUI.
     *
     * @returns
     *  A promise that will fulfil when the standard unit has been saved
     *  successfully in the user configuration. However, this instance provides
     *  the new value in its property `unit` immediately.
     */
    setUnit(unit: LengthUnit): JPromise {

        // nothing to do if the unit does not change
        if (this.unit === unit) { return jpromise.resolve(); }

        // immediately change the property value and emit the change event
        this.#updateProperties({ unit });

        // write new unit to configuration
        return setValue(STANDARD_UNIT_KEY, unit);
    }

    // private methods --------------------------------------------------------

    /**
     * Parses the current custom locale settings (separators, date/time, etc.),
     * and translates them to relevant properties of the interface `LocaleData`.
     */
    #resolveCustomData(): Partial<LocaleData> {

        const customData = CoreLocale.getLocaleData();
        localeLogger.info("$badge{LocaleDataUI} resolveCustomData: data=", customData);

        // extract number pattern with current separator characters
        // DOCS-1909: group separator may be missing
        const pattern = pick.string(customData, "number", "");
        const separators = pattern.replace(/\d+/g, "");
        if ((separators.length < 1) || (separators.length > 2)) {
            localeLogger.warn(() => `$badge{LocaleDataUI} resolveCustomData: unrecognized number separators "${pattern}"`);
        }

        // decimal separator character (trailing character of `separators`)
        let dec = (separators.length > 0) ? separators.slice(-1) : this.#localeData.dec;
        if (!/^[.,]$/.test(dec)) {
            localeLogger.warn(() => `$badge{LocaleDataUI} resolveCustomData: unexpected decimal separator "${dec}" (U+${fmt.formatInt(dec.charCodeAt(0), 16, { digits: 4 })})`);
            dec = this.#localeData.dec;
        }

        // group separator character (leading character of `separators`)
        let group = (separators.length === 2) ? separators[0] : this.#localeData.group;
        if (group === "\xa0") {
            group = " ";
        } else if (group === "\u2019") {
            group = "'";
        } else if (!/^[., ']$/.test(group)) {
            localeLogger.warn(() => `$badge{LocaleDataUI} resolveCustomData: unexpected group separator "${group}" (U+${fmt.formatInt(group.charCodeAt(0), 16, { digits: 4 })})`);
            group = this.#localeData.group;
        }

        // short date format
        let shortDate = pick.string(customData, "date", "");
        let dateSep: string;
        let leadingMonth: boolean;
        let leadingYear: boolean;
        if (shortDate) {
            dateSep = getDateSep(shortDate);
            shortDate = translateDateCode(shortDate);
            leadingMonth = isLeadingMonth(shortDate);
            leadingYear = isLeadingYear(shortDate);
        } else {
            shortDate = this.#localeData.shortDate;
            dateSep = this.#localeData.dateSep;
            leadingMonth = this.#localeData.leadingMonth;
            leadingYear = this.#localeData.leadingYear;
        }

        // short time format (without seconds)
        let shortTime = pick.string(customData, "time", "");
        let timeSep: string;
        let hours24: boolean;
        if (shortTime) {
            timeSep = getTimeSep(shortTime);
            shortTime = translateTimeCode(shortTime);
            hours24 = isHours24(shortTime);
        } else {
            shortTime = this.#localeData.shortTime;
            timeSep = this.#localeData.timeSep;
            hours24 = this.#localeData.hours24;
        }

        // long time format (with seconds)
        let longTime = pick.string(customData, "timeLong", "");
        longTime = longTime ? translateTimeCode(longTime) : this.#localeData.longTime;

        // return the translated properties
        return {
            dec,
            group,
            shortDate,
            dateSep,
            leadingMonth,
            leadingYear,
            shortTime,
            longTime,
            timeSep,
            hours24
        };
    }

    /**
     * Returns all relevant settings from the server configuration.
     */
    #resolveSettings(): Partial<LocaleData> {

        // standard measurement unit used e.g. in `LengthField` controls
        const unit = to.enum<LengthUnit>(LENGTH_UNITS, getStr(STANDARD_UNIT_KEY), this.#localeData.unit);

        // return the translated properties
        return { unit };
    }

    /**
     * Updates the specified properties of this instance, and fires a change
     * event if anything has changed.
     */
    #updateProperties(updateProps: Partial<LocaleData>): void {

        // update and collect all changed properties
        const self = this as unknown as Dict;
        const changedProps = dict.reduce(updateProps, dict.create(), (props, value, key) => {
            if (!_.isEqual(self[key], value)) {
                self[key] = props[key] = value!;
            }
        }) as Partial<LocaleData>;

        // notify change handlers if something has changed
        if (!is.empty(changedProps)) {
            globalEvents.emit("change:locale", changedProps);
        }
    }

    /**
     * Initializes all locale data properties for the curent UI locale, and
     * fires a change event if anything has changed.
     */
    #initialize(): void {

        // get native settings of the current locale
        const locale = CoreLocale.current();
        localeLogger.info(`$badge{LocaleDataUI} initialize: locale="${locale}"`);

        // store native locale data (without user-defined changes)
        this.#localeData = localeDataRegistry.getLocaleData(locale);
        const { intl } = this.#localeData;

        // the collator wrappers for different settings
        this.#collators.clear();

        // providers for translated names of languages and regions
        this.#languageNames = new Intl.DisplayNames(intl, { type: "language" });
        this.#regionNames = new Intl.DisplayNames(intl, { type: "region" });
        this.#currencyNames = new Intl.DisplayNames(intl, { type: "currency" });

        // set all properties to this instance
        this.#updateProperties({
            ...this.#localeData,
            ...this.#resolveCustomData(),
            ...this.#resolveSettings(),
        });
    }
}

// singleton ------------------------------------------------------------------

/**
 * The settings of the current UI locale, merged with other locale-related user
 * configuration.
 *
 * - The properties `dec`, `group`, `shortDate`, `shortTime`, and `longTime`
 *   will be set to the configured locale settings.
 * - The property `unit` will be set to the current length unit from the
 *   server-side user configuration.
 *
 * To receive the standard settings of the current locale (without local user
 * configuration), use `localeDataRegistry.getLocaleData(LOCALE_DATA.lc)`.
 */
export const LOCALE_DATA: Readonly<LocaleDataUI> = localeLogger.takeTime("$badge{LocaleDataUI} constructor", () => new LocaleDataUI());
