/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, str } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * Particles of a locale identifier.
 */
export interface LocaleId {

    /**
     * The full locale code with underscore, e.g. "bs_Cyrl_BA".
     */
    lc: string;

    /**
     * The full locale code with dashes, as used in the `Intl` API, e.g.
     * "bs-Cyrl-BA".
     */
    intl: string;

    /**
     * The lowercase language identifier of the locale code, e.g. "bs" in the
     * locale code "bs-Cyrl-BA".
     */
    language: string;

    /**
     * The lowercase dialect identifier of the locale code, e.g. "yue" in the
     * locale code "zh-yue-HK".
     */
    dialect: Opt<string>;

    /**
     * The capitalized script identifier of the locale code, e.g. "Cyrl" in the
     * locale code "bs-Cyrl-BA".
     */
    script: Opt<string>;

    /**
     * The uppercase region identifier of the locale code, or an M49 area code,
     * e.g. "BA" in the locale code "bs-Cyrl-BA", or "001" in the locale code
     * "eo-001".
     */
    region: Opt<string>;

    /**
     * The lowercase variant of a region, or a private-use specifier, e.g.
     * "valencia" in the locale code "ca-ES-valencia", or "x-genlower" in the
     * locale code "ru-RU-x-genlower".
     */
    variant: Opt<string>;
}

/**
 * Specification key for a locale, either a locale code as string (using
 * underscores or dashes as separator, e.g. "de-DE" or "de_DE"), or a resolved
 * `LocaleId` object.
 */
export type LocaleSpec = string | Readonly<LocaleId>;

// functions ==================================================================

/**
 * Creates a `LocaleId` instance from the passed components of a locale code.
 */
export function createLocaleId(language: string, dialect?: string, script?: string, region?: string, variant?: string): Readonly<LocaleId> {
    language = language.toLowerCase();
    dialect = dialect ? dialect.toLowerCase() : undefined;
    script = script ? str.capitalizeFirst(script) : undefined;
    region = region ? region.toUpperCase() : undefined;
    variant = variant ? variant.toLowerCase() : undefined;
    const lc = [language, dialect, script, region, variant].filter(Boolean).join("_");
    const intl = lc.replace(/_/g, "-");
    return { lc, intl, language, dialect, script, region, variant };
}

/**
 * Parses the passed locale code.
 *
 * @param locale
 *  The locale code to be parsed. If this value is already a `LocaleId`, it
 *  will be returned as is.
 *
 * @returns
 *  The components of the passed locale code; or `undefined`, if the string
 *  cannot be parsed.
 */
export function parseLocaleId(locale: LocaleId): Readonly<LocaleId>;
export function parseLocaleId(locale: LocaleSpec): Opt<Readonly<LocaleId>>;
// implementation
export function parseLocaleId(locale: LocaleSpec): Opt<Readonly<LocaleId>> {
    if (!is.string(locale)) { return locale; }
    // parse the normalized string (lowercase with dashes)
    locale = locale.toLowerCase().replace(/_/g, "-");
    const matches = /^([a-z]{2,3})(?:-([a-z]{3}|ploc[a-z]?))?(?:-([a-z]{4}))?(?:-([a-z]{2}|\d{3}))?(?:-((x-)?[a-z]{2,}))?$/.exec(locale);
    // create the `LocaleId` instance (will convert all tokens to correct character case)
    return matches ? createLocaleId(matches[1], matches[2], matches[3], matches[4], matches[5]) : undefined;
}
