/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { str, fun, ary, set, map, dict } from "@/io.ox/office/tk/algorithms";
import { registerDebugCommand } from "@/io.ox/office/tk/config";

import { localeLogger } from "@/io.ox/office/tk/locale/localeutils";
import type { LocaleId, LocaleSpec } from "@/io.ox/office/tk/locale/localeid";
import { parseLocaleId } from "@/io.ox/office/tk/locale/localeid";
import type { DateTokens, ColorTokens, HourModeTokens, CurrencyData, LocaleData } from "@/io.ox/office/tk/locale/localedata";
import { COLOR_KEYS, getNativeSeparators, createNativeCurrencyData, yieldCurrencyCodes } from "@/io.ox/office/tk/locale/localedata";
import * as FormatCode from "@/io.ox/office/tk/locale/localeutils";

import rawLocaleData from "@/io.ox/office/tk/locale/localedata.json";

// constants ==================================================================

// ISO codes of languages with right-to-left writing direction
const RTL_LANGUAGES = new Set(["ar", "fa", "he"]);

// ISO codes of all CJK languages
const CJK_LANGUAGES = new Set(["ja", "ko", "zh"]);

// regions using imperial length units (inch)
const IMPERIAL_UNIT_REGIONS = new Set(["LR", "MM", "US"]);

// fallback date/time tokens in English used if JSON data entry is missing
const EN_DATE_TOKENS: DateTokens = {
    Y: "Y",
    M: "M",
    D: "D",
    h: "h",
    m: "m",
    s: "s",
    b: "b",
    g: "g",
    e: "e",
    a: "a",
};

// fallback color tokens in English used if JSON data entry is missing
const EN_COLOR_TOKENS: ColorTokens = {
    black: "Black",
    blue: "Blue",
    green: "Green",
    cyan: "Cyan",
    red: "Red",
    magenta: "Magenta",
    yellow: "Yellow",
    white: "White",
};

// fallback names for date/time components in English used if JSON data entry is missing
const EN_SHORT_QUARTES: readonly string[] = ary.generate(4, i => `Q${i + 1}`);
const EN_LONG__QUARTES: readonly string[] = ary.generate(4, i => `Quarter${i + 1}`);
const EN_SHORT_MONTHS: readonly string[] = str.splitTokens("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec");
const EN_LONG_MONTHS: readonly string[] = str.splitTokens("January February March April May June July August September October November December");
const EN_SHORT_WEEKDAYS: readonly string[] = str.splitTokens("Sun Mon Tue Wed Thu Fri Sat");
const EN_LONG_WEEKDAYS: readonly string[] = str.splitTokens("Sunday Monday Tuesday Wednesday Thursday Friday Saturday");
const EN_HOUR_MODE_TOKENS: HourModeTokens = { am: "AM", pm: "PM" };

// class ResourceMap ==========================================================

/**
 * Helper class to store and resolve localized resource data.
 */
class ResourceMap<T> {

    readonly #store = new Map<string, T>();
    readonly #default: T;
    readonly #message: string;

    // constructor ------------------------------------------------------------

    constructor(defValue: T, message: string) {
        this.#default = defValue;
        this.#message = message;
    }

    // public methods ---------------------------------------------------------

    insert(locales: string, value: T): void {
        for (const key of locales.split("|")) {
            this.#store.set(key, value);
        }
    }

    extract(localeId: LocaleId): T {
        let key = localeId.intl;
        while (key) {
            const value = this.#store.get(key);
            if (value !== undefined) { return value; }
            const dashPos = key.lastIndexOf("-");
            if (dashPos < 1) { break; }
            key = key.slice(0, dashPos);
        }
        localeLogger.warn(`$badge{ResourceMap} missing ${this.#message} for locale "${localeId.intl}"`);
        return this.#default;
    }
}

// class LocaleDataRegistry ===================================================

class LocaleDataRegistry {

    // the locale data cache for all locales, mapped by canonical locale code
    readonly #localeDataMap = new Map<string, LocaleData>();

    // the currency data cache for all locales, mapped by canonical locale code
    readonly #currDataMap = new Map<string, CurrencyData>();

    // default regions for languages
    readonly #defRegionMap = new Map<string, string>();

    // default languages for regions
    readonly #defLangMap = new Map<string, string>();

    // maps MS locale identifiers to locale codes
    readonly #lcidMap = new Map<number, LocaleId>();

    // localized resources, mapped by canonical locale code or plain language identifier
    readonly #resourceMaps = {
        shortDateCodes: new ResourceMap("DD/MM/YYYY", "short date format code"),
        longDateCodes:  new ResourceMap("DD MMMM YYYY", "long date format code"),
        shortTimeCodes: new ResourceMap("hh:mm", "short time format code"),
        longTimeCodes:  new ResourceMap("hh:mm:ss", "long time format code"),
        stdTokens:      new ResourceMap("General", "name for 'Standard' format code token"),
        dateTokens:     new ResourceMap(EN_DATE_TOKENS, "format code date/time tokens"),
        colorTokens:    new ResourceMap(EN_COLOR_TOKENS, "color name tokens"),
        shortQuarters:  new ResourceMap(EN_SHORT_QUARTES, "short quarter names"),
        longQuarters:   new ResourceMap(EN_LONG__QUARTES, "long quarter names"),
        shortMonths:    new ResourceMap(EN_SHORT_MONTHS, "short month names"),
        longMonths:     new ResourceMap(EN_LONG_MONTHS, "long month names"),
        shortWeekdays:  new ResourceMap(EN_SHORT_WEEKDAYS, "short weekday names"),
        longWeekdays:   new ResourceMap(EN_LONG_WEEKDAYS, "long weekday names"),
        hourModes:      new ResourceMap(EN_HOUR_MODE_TOKENS, "12-hour clock mode tokens"),
    };

    // constructor ------------------------------------------------------------

    constructor() {

        // parse the default regions mapping, and fill `defRegionMap`
        for (const [region, langs] of dict.entries(rawLocaleData.defRegions)) {
            for (const lang of langs.split("|")) {
                this.#defRegionMap.set(lang, region);
            }
        }

        // parse the default languages mapping, and fill `defLangMap`
        for (const [lang, regions] of dict.entries(rawLocaleData.defLangs)) {
            for (const region of regions.split("|")) {
                this.#defLangMap.set(region, lang);
            }
        }

        // parse the LCID mapping, and fill `lcidMap`
        for (const [key, locale] of dict.entries(rawLocaleData.lcidMap)) {
            const localeId = parseLocaleId(locale);
            if (localeId) {
                this.#lcidMap.set(parseInt(key, 16), localeId);
            } else {
                localeLogger.error(`$badge{LocaleDataRegistry} invalid locale ID: "${locale}"`);
            }
        }

        // extract localized resources (single string values)
        for (const propKey of ["shortDateCodes", "longDateCodes", "shortTimeCodes", "longTimeCodes", "stdTokens"] as const) {
            const resourceMap = this.#resourceMaps[propKey];
            for (const [locales, formatCode] of dict.entries(rawLocaleData[propKey] as Dict<string>)) {
                resourceMap.insert(locales, formatCode);
            }
        }

        // extract localized resources (string arrays)
        for (const propKey of ["shortQuarters", "longQuarters", "shortMonths", "longMonths", "shortWeekdays", "longWeekdays"] as const) {
            const resourceMap = this.#resourceMaps[propKey];
            for (const [locales, tokens] of dict.entries(rawLocaleData[propKey] as Dict<string>)) {
                resourceMap.insert(locales, tokens.split("|"));
            }
        }

        // extract localized date code tokens
        for (const [locales, tokens] of dict.entries(rawLocaleData.dateTokens)) {
            const [Y, M, D, h, m, s, b, g, e, a] = tokens;
            const dateTokens: DateTokens = { Y, M, D, h, m, s, b, g, e, a };
            this.#resourceMaps.dateTokens.insert(locales, dateTokens);
        }

        // extract localized color code tokens
        for (const [locales, tokens] of dict.entries(rawLocaleData.colorTokens)) {
            const colorTokensArr = tokens.split("|");
            const colorTokens: ColorTokens = dict.generateRec(COLOR_KEYS, (_k, i) => colorTokensArr[i]);
            this.#resourceMaps.colorTokens.insert(locales, colorTokens);
        }

        // extract localized AM/PM tokens
        for (const [locales, tokens] of dict.entries(rawLocaleData.hourModes)) {
            const [am, pm] = tokens.split("|");
            this.#resourceMaps.hourModes.insert(locales, { am, pm });
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns an existing locale data instance if available, or creates and
     * caches a new locale data instance.
     *
     * @param locale
     *  The locale code as string (using underscores or dashes as separator,
     *  e.g. "de-DE" or "de_DE"), or a resolved `LocaleId` object.
     *
     * @returns
     *  The locale data entry for the specified locale.
     */
    getLocaleData(locale: LocaleSpec): Readonly<LocaleData> {

        // parse a string identifier
        let localeId = parseLocaleId(locale) ?? fun.do(() => {
            localeLogger.error(`$badge{LocaleDataRegistry} getLocaleData: invalid locale ID: "${locale as string}"`);
            return parseLocaleId("en-US")!;
        });

        // try to resolve existing locale data entry, or create a new entry
        return map.upsert(this.#localeDataMap, localeId.intl, (): LocaleData => {
            localeLogger.trace(`$badge{LocaleDataRegistry} getLocaleData: creating locale data for "${localeId.intl}"`);

            // destructure locale identifier
            const language = localeId.language;
            const region = localeId.region ?? this.getDefaultRegion(language) ?? "001"; // fallback to "world"
            localeId = { ...localeId, region };

            // extract default format codes for date/time from resource maps
            const shortDate = this.#resourceMaps.shortDateCodes.extract(localeId);
            const longDate = this.#resourceMaps.longDateCodes.extract(localeId);
            const shortTime = this.#resourceMaps.shortTimeCodes.extract(localeId);
            const longTime = this.#resourceMaps.longTimeCodes.extract(localeId);

            // create and return the new `LocaleData` object
            return {
                ...localeId,
                // language flags
                rtl: RTL_LANGUAGES.has(language),
                cjk: CJK_LANGUAGES.has(language),
                // number separators
                ...getNativeSeparators(localeId),
                // length unit and currency
                unit: IMPERIAL_UNIT_REGIONS.has(region) ? "in" : "cm",
                currency: this.getCurrencyData(localeId) ?? this.getCurrencyData("en-US")!,
                // date format codes
                shortDate,
                longDate,
                dateSep: FormatCode.getDateSep(shortDate),
                leadingMonth: FormatCode.isLeadingMonth(shortDate),
                leadingYear: FormatCode.isLeadingYear(shortDate),
                // time format codes
                shortTime,
                longTime,
                timeSep: FormatCode.getTimeSep(shortTime),
                hours24: FormatCode.isHours24(shortTime),
                // format code tokens
                stdToken: this.#resourceMaps.stdTokens.extract(localeId),
                dateTokens: this.#resourceMaps.dateTokens.extract(localeId),
                colorTokens: this.#resourceMaps.colorTokens.extract(localeId),
                // translated resource for formatting dates and times
                shortQuarters: this.#resourceMaps.shortQuarters.extract(localeId),
                longQuarters: this.#resourceMaps.longQuarters.extract(localeId),
                shortMonths: this.#resourceMaps.shortMonths.extract(localeId),
                longMonths: this.#resourceMaps.longMonths.extract(localeId),
                shortWeekdays: this.#resourceMaps.shortWeekdays.extract(localeId),
                longWeekdays: this.#resourceMaps.longWeekdays.extract(localeId),
                hourModes: this.#resourceMaps.hourModes.extract(localeId),
            };
        });
    }

    /**
     * Returns the default currency settings for the specified locale.
     *
     * @param locale
     *  The locale code as string (using underscores or dashes as separator,
     *  e.g. "de-DE" or "de_DE"), or a resolved `LocaleId` object.
     *
     * @returns
     *  The default currency settings for the specified locale; or `undefined`,
     *  if no data is available, or the locale code was invalid.
     */
    getCurrencyData(locale: LocaleSpec): Opt<Readonly<CurrencyData>> {

        // parse locale code as string
        const localeId = parseLocaleId(locale);
        if (!localeId) {
            localeLogger.warn(`$badge{getCurrencyData} failed to resolve locale identifier ${locale as string}`);
            return;
        }

        // create and cache currency data lazily on demand
        return map.upsert(this.#currDataMap, localeId.intl, () => createNativeCurrencyData(localeId));
    }

    /**
     * Returns an iterator that provides the currency settings of all known
     * currencies.
     *
     * @yields
     *  The locale identifiers (as keys) and currency settings (as values) of
     *  all known currencies.
     */
    *yieldCurrencyData(): KeyedIterator<Readonly<CurrencyData>> {

        // process all available regions
        for (const [region, iso] of yieldCurrencyCodes()) {

            // resolve default language of region
            const language = this.getDefaultLanguage(region);
            if (!language) {
                localeLogger.warn(() => `$badge{yieldCurrencyData} failed to resolve default language for region ${region} (currency ${iso})`);
                continue;
            }

            // resolve currency data for the region
            const locale = language + "-" + region;
            const currData = this.getCurrencyData(locale);
            if (!currData) {
                localeLogger.warn(() => `$badge{yieldCurrencyData} failed to resolve currency data for region ${region} (currency ${iso})`);
                continue;
            }

            yield [locale, currData];
        }
    }

    /**
     * Returns the default region of the passed language code.
     *
     * @param language
     *  The language code, e.g. "en" or "de".
     *
     * @returns
     *  The default region of the passed language, e.g. "US" or "DE"; or
     *  `undefined`, if the passed language is unknown.
     */
    getDefaultRegion(language: string): Opt<string> {
        return this.#defRegionMap.get(language.toLowerCase());
    }

    /**
     * Returns the default language of the passed region code.
     *
     * @param region
     *  The region code, e.g. "US" or "DE".
     *
     * @returns
     *  The default language of the passed region, e.g. "en" or "de"; or
     *  `undefined`, if the passed region is unknown.
     */
    getDefaultLanguage(region: string): Opt<string> {
        return this.#defLangMap.get(region.toUpperCase());
    }

    /**
     * Returns the resolved locale identifier corresponding to the passed MS
     * locale identifier.
     *
     * @param lcid
     *  The MS locale identifier.
     *
     * @returns
     *  The locale identifier for the passed LCID, if available; otherwise
     *  `undefined`.
     */
    resolveLCID(lcid: number): Opt<Readonly<LocaleId>> {
        return this.#lcidMap.get(lcid);
    }
}

// singleton ------------------------------------------------------------------

/**
 * Global registry of `LocaleData` objects for all known locales.
 */
export const localeDataRegistry = localeLogger.takeTime("$badge{LocaleDataRegistry} constructor", () => new LocaleDataRegistry());

// debug commands =============================================================

// The debug command "office.debug.locales" prints a list with important
// regional settings provided by the browser's `Intl` API (number separators,
// currencies, etc.).
registerDebugCommand("locales", () => {
    const extractValue = (parts: Intl.NumberFormatPart[], type: Intl.NumberFormatPartTypes): string[] =>
        Array.from(set.from(parts, part => (part.type === type) ? part.value : undefined));
    const lines: string[] = ['"lc","dec","group","groups","iso","symbol","pattern"'];
    for (const [region, iso] of yieldCurrencyCodes()) {
        const lang = localeDataRegistry.getDefaultLanguage(region);
        if (lang) {
            const locale = `${lang}-${region}`;
            const parts1 = new Intl.NumberFormat(locale).formatToParts(-123456789.123);
            const dec = extractValue(parts1, "decimal").join("");
            const group = extractValue(parts1, "group").join("");
            const groups = parts1.reduce((ptn, part) => `${ptn}${(part.type === "integer") ? part.value.length : ""}`, "");
            const parts2 = new Intl.NumberFormat(locale, { style: "currency", currency: iso }).formatToParts(-1);
            const symbol = extractValue(parts2, "currency")[0];
            const pattern = parts2.reduce((ptn, part) => {
                switch (part.type) {
                    case "integer":     return ptn + "0";
                    case "currency":    return ptn + "$";
                    case "minusSign":   return ptn + "-";
                    case "literal":     return ptn + (/^\s+$/.test(part.value) ? "_" : "");
                    default:            return ptn;
                }
            }, "");
            lines.push(`"${locale}","${dec}","${group}","${groups}","${iso}","${symbol || "\ufffd"}","${pattern}"`);
        } else {
            localeLogger.warn(`no language for region ${region}`);
        }
    }
    window.console.log(lines.join("\n"));
});
