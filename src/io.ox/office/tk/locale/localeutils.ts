/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Logger } from "@/io.ox/office/tk/utils/logger";

// globals ====================================================================

export const localeLogger = new Logger("office:log-locale", { tag: "LOCALE" });

// functions ==================================================================

/**
 * Returns the date separator character of the passed date format code.
 */
export function getDateSep(dateCode: string): string {
    return /[-/.]/.exec(dateCode)?.[0] || "/";
}

/**
 * Returns the time separator character of the passed date format code.
 */
export function getTimeSep(timeCode: string): string {
    return /[:.]/.exec(timeCode)?.[0] || ":";
}

/**
 * Returns whether the month is in front of the day in the passed date format
 * code (regardless of the year's position).
 */
export function isLeadingMonth(dateCode: string): boolean {
    return /M.*D/.test(dateCode);
}

/**
 * Returns whether the year in front of the day and month in the passed date
 * format code.
 */
export function isLeadingYear(dateCode: string): boolean {
    return /Y.*M/.test(dateCode);
}

/**
 * Returns whether the 24-hours time format is preferred over the 12-hours
 * AM/PM time format in the passed time format code.
 */
export function isHours24(timeCode: string): boolean {
    return !timeCode.includes("AM/PM");
}
