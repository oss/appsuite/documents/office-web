/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import createLocaleIndexOf, { type LocaleIndexOfFn } from "locale-index-of";

import { type Relation, type CompareFn, math } from "@/io.ox/office/tk/algorithms";

// class CollatorWrapper ======================================================

/**
 * A wrapper for `Intl.Collator` instances implementing various comparison and
 * sorting algorithms not available at the native collators.
 */
export class CollatorWrapper {

    readonly #searchCollator: Intl.Collator;
    readonly #sortCollator: Intl.Collator;
    readonly #localeIndexOf: LocaleIndexOfFn;

    // constructor ------------------------------------------------------------

    constructor(locale: string, options?: Intl.CollatorOptions) {
        this.#searchCollator = new Intl.Collator(locale, { ...options, usage: "search" });
        this.#sortCollator = new Intl.Collator(locale, { ...options, usage: "sort" });
        this.#localeIndexOf = createLocaleIndexOf(Intl);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether the two passed strings are considered equal according to
     * the locale and collator options passed to the constructor.
     *
     * @param text1
     *  The first string for comparison.
     *
     * @param text2
     *  The second string for comparison.
     *
     * @returns
     *  Whether the passed strings are considered to be equal.
     */
    equals(text1: string, text2: string): boolean {
        return !this.#searchCollator.compare(text1, text2);
    }

    /**
     * Returns the sorting relation of the two passed strings according to the
     * locale and collator options passed to the constructor.
     *
     * @param text1
     *  The first string to be compared.
     *
     * @param text2
     *  The second string to be compared.
     *
     * @returns
     *  The relation of the passed strings.
     */
    compare(text1: string, text2: string): Relation {
        return math.sgn(this.#sortCollator.compare(text1, text2));
    }

    /**
     * Returns the character index of a substring in another string according
     * to the locale and collator options passed to the constructor.
     *
     * @param text
     *  The string to search the specified substring in.
     *
     * @param query
     *  The substring to be searched in the specified string.
     *
     * @returns
     *  The character index of the first occurrence of the substring, or `-1`
     *  if nothing was found.
     */
    indexOf(text: string, query: string): number {
        return query ? this.#localeIndexOf(text, query, this.#searchCollator) : 0;
    }

    /**
     * Sorts the passed string array in-place according to the locale and
     * collator options passed to the constructor.
     *
     * @param arr
     *  The array to be sorted in-place.
     *
     * @param reverse
     *  Whether to reverse the sorting order.
     *
     * @returns
     *  A reference to the sorted array passed to this method.
     */
    sort<AT extends string[]>(arr: AT, reverse?: boolean): AT {
        return arr.sort(this.#getCompareFn(reverse));
    }

    /**
     * Sorts the passed array in-place by a specific string property according
     * to the locale and collator options passed to the constructor.
     *
     * @param arr
     *  The array to be sorted in-place.
     *
     * @param fn
     *  A callback function that converts array elements to strings.
     *
     * @param reverse
     *  Whether to reverse the sorting order.
     *
     * @returns
     *  A reference to the sorted array passed to this method.
     */
    sortBy<VT, AT extends VT[]>(arr: AT, fn: (value: VT) => string, reverse?: boolean): AT {
        const compareFn = this.#getCompareFn(reverse);
        return arr.sort((el1, el2) => compareFn(fn(el1), fn(el2)));
    }

    /**
     * Sorts the passed array in-place by a specific string property according
     * to the locale and collator options passed to the constructor.
     *
     * @param arr
     *  The array to be sorted in-place.
     *
     * @param prop
     *  The name of the string property used to sort the array elements.
     *
     * @param reverse
     *  Whether to reverse the sorting order.
     *
     * @returns
     *  A reference to the sorted array passed to this method.
     */
    sortByProp<KT extends PropertyKey, AT extends Array<Record<KT, string>>>(arr: AT, prop: KT, reverse?: boolean): AT {
        const compareFn = this.#getCompareFn(reverse);
        return arr.sort((el1, el2) => compareFn(el1[prop], el2[prop]));
    }

    // private methods --------------------------------------------------------

    /**
     * Returns a comparator function according to the passed settings.
     */
    #getCompareFn(reverse?: boolean): CompareFn<string> {
        const sortFn = this.compare.bind(this);
        return reverse ? (e1, e2) => sortFn(e2, e1) : sortFn;
    }
}
