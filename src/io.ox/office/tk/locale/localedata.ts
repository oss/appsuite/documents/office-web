/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import REGION_TO_ISO_CURRENCY from "country-to-currency";

import { is, itr, set, dict, pick } from "@/io.ox/office/tk/algorithms";
import { localeLogger } from "@/io.ox/office/tk/locale/localeutils";
import type { LocaleId } from "@/io.ox/office/tk/locale/localeid";

// types ======================================================================

/**
 * Configurable measurement units to be used in GUI elements.
 */
export type LengthUnit = (typeof LENGTH_UNITS)[number];

/**
 * Tokens for date/time components used in number format codes.
 */
export interface DateTokens {

    /** The translated format token for years. */
    readonly Y: string;

    /** The translated format token for months. */
    readonly M: string;

    /** The translated format token for days. */
    readonly D: string;

    /** The translated format token for hours. */
    readonly h: string;

    /** The translated format token for minutes. */
    readonly m: string;

    /** The translated format token for seconds. */
    readonly s: string;

    /** The translated format token for Buddhist years (OOXML). */
    readonly b: string;

    /** The translated format token for Gengo era names (OOXML). */
    readonly g: string;

    /** The translated format token for Gengo era years (OOXML). */
    readonly e: string;

    /** The translated format token for the name of the day (OOXML). */
    readonly a: string;
}

/**
 * The internal key of a text color token used in number format codes.
 */
export type ColorKey = (typeof COLOR_KEYS)[number];

/**
 * Translated tokens for text colors used in number format codes.
 */
export type ColorTokens = RoRecord<ColorKey, string>;

/**
 * Translated tokens for formatted times in 12-hour clock mode.
 */
export type HourModeTokens = RoRecord<"am" | "pm", string>;

/**
 * Settings for a specific currency.
 */
export interface CurrencyData {

    /**
     * The ISO currency code (three uppercase letters).
     */
    iso: string;

    /**
     * The currency symbol (non-empty string).
     */
    symbol: string;

    /**
     * Default precision (number of decimal digits) to be shown.
     */
    precision: number;
}

/**
 * Settings for a specific locale.
 */
export interface LocaleData extends LocaleId {

    /**
     * The default writing direction (`false` for left-to-right, or `true` for
     * right-to-left).
     */
    rtl: boolean;

    /**
     * Whether the locale represents one of the languages Chinese, Japanese, or
     * Korean.
     */
    cjk: boolean;

    /**
     * The decimal separator used in numbers (single character).
     */
    dec: string;

    /**
     * The group separator (a.k.a. thousands separator in most locales) used in
     * numbers (single character).
     */
    group: string;

    /**
     * The default length unit to be used in the GUI.
     */
    unit: LengthUnit;

    /**
     * The default currency symbol (non-empty string).
     */
    currency: Readonly<CurrencyData>;

    /**
     * The number format code for a short date (month as number).
     */
    shortDate: string;

    /**
     * The number format code for a long date (with month name).
     */
    longDate: string;

    /**
     * Standard separator character for date formats.
     */
    dateSep: string;

    /**
     * Whether short dates will be formatted with the month in front of the day
     * (regardless of the year's position).
     */
    leadingMonth: boolean;

    /**
     * Whether short dates will be formatted with the year in front of the day
     * and month.
     */
    leadingYear: boolean;

    /**
     * The number format code for a short time (without seconds).
     */
    shortTime: string;

    /**
     * The number format code for a time (with seconds).
     */
    longTime: string;

    /**
     * Separator character used in formatted time values.
     */
    timeSep: string;

    /**
     * Whether the 24-hours time format is preferred over the 12-hours AM/PM
     * time format.
     */
    hours24: boolean;

    /**
     * The translated number format token for the "General" format code.
     */
    stdToken: string;

    /**
     * The translated number format tokens for date/time format codes.
     */
    dateTokens: DateTokens;

    /**
     * The translated tokens for text colors used in format codes.
     */
    colorTokens: ColorTokens;

    /**
     * Translated short names of the four quarters of the year.
     */
    shortQuarters: readonly string[];

    /**
     * Translated full names of the four quarters of the year.
     */
    longQuarters: readonly string[];

    /**
     * Translated short names of all months in a year.
     */
    shortMonths: readonly string[];

    /**
     * Translated full names of all months in a year.
     */
    longMonths: readonly string[];

    /**
     * Translated short names of all days in a week, starting with Sunday.
     */
    shortWeekdays: readonly string[];

    /**
     * Translated full names of all days in a week, starting with Sunday.
     */
    longWeekdays: readonly string[];

    /**
     * Translated suffixes for 12-hour clock mode.
     */
    hourModes: HourModeTokens;
}

// constants ==================================================================

/**
 * Configurable measurement units to be used in GUI elements.
 */
export const LENGTH_UNITS = ["cm", "mm", "in"] as const;

/**
 * The internal keys of text color tokens used in number format codes.
 */
export const COLOR_KEYS = ["black", "blue", "green", "cyan", "red", "magenta", "yellow", "white"] as const;

// regions in "REGION_TO_ISO_CURRENCY" to be skipped
const SKIP_REGIONS = new Set([
    "AN",   // Netherlands Antilles (dissolved in 2010)
    "AQ",   // Antarctic
    "BV",   // Bouvet Island (dependency of Norway, uninhabited)
    "EH",   // Western Sahara
]);

/**
 * Corrections for "REGION_TO_ISO_CURRENCY". Maps ISO region codes to ISO
 * currency codes, e.g. `US: "USD"`.
 */
const CURRENCY_CODE_OVERRIDES: Dict<string> = {
};

/**
 * Additional currency symbols currently missing or wrong in the `Intl` API
 * data of various browsers. Different browsers may provide different symbols
 * for the same currency, or may lack a symbol and return the ISO code instead.
 */
const CURRENCY_SYMBOL_OVERRIDES: Dict<string | Dict<string>> = {
    ALL: "Lekë",
    AMD: "֏",
    AZN: "₼",
    BAM: "KM",
    BIF: "FBu",
    BTN: "Nu.",
    BWP: "P",
    BYN: "Br",
    CDF: "FC",
    CHF: "Fr.",
    CUC: "$",
    CVE: "$",
    DOP: "$",
    DKK: { FO: "kr", default: "kr." },
    ERN: "Nfk",
    FKP: "£",
    GEL: "₾",
    HUF: "Ft",
    ISK: "kr.",
    JPY: "¥",
    KGS: "сом",
    KHR: "៛",
    KPW: "₩",
    KZT: "₸",
    LAK: "₭",
    LKR: "රු.",
    LSL: "M",
    MGA: "Ar",
    MKD: "ден.",
    MMK: "K",
    MNT: "₮",
    MVR: "ރ.",
    NGN: "₦",
    NOK: "kr",
    NPR: "नेरू",
    PYG: "₲",
    RON: "lei",
    RSD: "дин.",
    RWF: "RF",
    SLL: "Le",
    SOS: "S",
    SZL: "E",
    TJS: "сом.",
    TMT: "m",
    TOP: "T$",
    UAH: "₴",
    USD: "$",
    UZS: "soʻm",
    ZWL: "Z$",
};

// functions ==================================================================

/**
 * Extracts and returns the native decimal and group separator characters for
 * the passed locale.
 *
 * @param localeId
 *  The resolved identifier of a locale.
 *
 * @returns
 *  The native decimal and group separator characters for the passed locale.
 */
export function getNativeSeparators(localeId: Readonly<LocaleId>): Pick<LocaleData, "dec" | "group"> {

    // region must exist to be able to resolve regional settings
    if (!localeId.region) {
        localeLogger.warn(`$badge{getNativeSeparators} missing region for language ${localeId.language}`);
        return { dec: ".", group: "," };
    }

    // generate a formatted number
    const formatter = new Intl.NumberFormat(localeId.intl);
    const parts = formatter.formatToParts(-123456789.123);

    // extract decimal separator
    const decSeps = set.from(parts, part => (part.type === "decimal") ? part.value : undefined);
    let dec = itr.first(decSeps);
    if (!dec) {
        localeLogger.warn(`$badge{getNativeSeparators} no decimal separator found for locale ${localeId.intl}`);
        dec = ".";
    } else {
        if (dec.length > 1) {
            localeLogger.warn(`$badge{getNativeSeparators} multi-character decimal separators found for locale ${localeId.intl}`);
            dec = dec[0];
        }
        if (decSeps.size > 1) {
            localeLogger.warn(`$badge{getNativeSeparators} multiple decimal separators found for locale ${localeId.intl}`);
        }
    }

    // extract group separator
    const groupSeps = set.from(parts, part => (part.type === "group") ? part.value : undefined);
    let group = itr.first(groupSeps)?.replace(/\xa0/g, " ");
    if (!group) {
        localeLogger.warn(`$badge{getNativeSeparators} no group separator found for locale ${localeId.intl}`);
        group = (dec !== ",") ? "," : ".";
    } else {
        if (group.length > 1) {
            localeLogger.warn(`$badge{getNativeSeparators} multi-character group separators found for locale ${localeId.intl}`);
            group = group[0];
        }
        if (groupSeps.size > 1) {
            localeLogger.warn(`$badge{getNativeSeparators} multiple group separators found for locale ${localeId.intl}`);
        }
    }

    if (dec === group) {
        localeLogger.error(`$badge{getNativeSeparators} equal decimal and group separator for locale ${localeId.intl}`);
    }

    return { dec, group };
}

/**
 * Returns a locally defined replacement for a currency symbol.
 */
function getOverrideSymbol(iso: string, region: string): Opt<string> {
    const entry = CURRENCY_SYMBOL_OVERRIDES[iso];
    return is.string(entry) ? entry : entry ? (entry[region] ?? entry.default) : undefined;
}

/**
 * Returns the default ISO currency code for a locale region.
 *
 * @param region
 *  The code of a region.
 *
 * @returns
 *  The default ISO currency code of the specified region.
 */
export function getCurrencyCode(region: string): Opt<string> {
    region = region.toUpperCase();
    if (SKIP_REGIONS.has(region)) { return undefined; }
    return CURRENCY_CODE_OVERRIDES[region] ?? pick.string(REGION_TO_ISO_CURRENCY, region);
}

/**
 * Returns an iterator that yields the default ISO currency codes for all known
 * locale regions.
 *
 * @yields
 *  The default ISO currency codes (as values) for all known locale regions (as
 *  keys).
 */
export function *yieldCurrencyCodes(): KeyedIterator<string> {
    for (const region of dict.keys(REGION_TO_ISO_CURRENCY)) {
        const iso = getCurrencyCode(region);
        if (iso) { yield [region, iso]; }
    }
}

/**
 * Creates a new `CurrencyData` object with the native currency settings for
 * the passed locale.
 *
 * @param localeId
 *  The resolved identifier of a locale.
 *
 * @returns
 *  The new `CurrencyData` object for the passed locale; or `undefined`, if no
 *  currency data is available, or the locale identifier is invalid.
 */
export function createNativeCurrencyData(localeId: Readonly<LocaleId>): Opt<CurrencyData> {

    // region must exist to be able to resolve currency settings
    if (!localeId.region) {
        localeLogger.warn(`$badge{createNativeCurrencyData} missing region for language ${localeId.language}`);
        return;
    }
    const { region } = localeId;

    // resolve ISO currency code used in the region
    const iso = getCurrencyCode(region);
    if (!iso) {
        localeLogger.warn(`$badge{createNativeCurrencyData} failed to resolve currency for region ${region}`);
        return;
    }

    // generate a formatted currency value
    const formatter = new Intl.NumberFormat(localeId.intl, { style: "currency", currency: iso });
    const parts = formatter.formatToParts(-1);

    // extract currency symbol
    const symbolIdx = parts.findIndex(entry => entry.type === "currency");
    const symbol = getOverrideSymbol(iso, region) ?? parts[symbolIdx]?.value ?? iso;
    if (symbol === iso) {
        localeLogger.warn(`$badge{createNativeCurrencyData} no currency symbol for code ${iso}`);
    }

    // extract number of fractional digits
    const fractionIdx = parts.findIndex(entry => entry.type === "fraction");
    const precision = parts[fractionIdx]?.value.length ?? 0;

    return { iso, symbol, precision };
}
