/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { type Size, is, jpromise } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { type DObjectConfig, DObject } from "@/io.ox/office/tk/object/dobject";

import type { NodeOrJQuery, ElementBaseOptions, ElementStyleOptions, ElementWrapper } from "@/io.ox/office/tk/dom/domutils";
import { toNode, createCanvas, insertHiddenNodes } from "@/io.ox/office/tk/dom/domutils";
import { type RectangleLike, Rectangle } from "@/io.ox/office/tk/dom/rectangle";

import { Canvas2DContext } from "@/io.ox/office/tk/canvas/canvas2dcontext";
import { CanvasPixelContext } from "@/io.ox/office/tk/canvas/canvaspixelcontext";

// types ======================================================================

/**
 * The logical location represented by the canvas DOM element; or just the size
 * of the canvas with default start position `0,0`.
 *
 * - The X coordinate (property `left`) specifies the global horizontal
 *   translation, in pixels. A pixel drawn at this position will appear at the
 *   left border of the canvas area.
 *
 * - The Y coordinate (property `top`) specifies the global vertical
 *   translation, in pixels. A pixel drawn at this position will appear at the
 *   top border of the canvas area.
 *
 * - The property `width` specifies the width of the bitmap in the canvas area,
 *   in pixels. Will also be set as element size of the `<canvas>` DOM element.
 *
 * - The property `height` specifies the height of the bitmap in the canvas
 *   area, in pixels. Will also be set as element size of the `<canvas>` DOM
 *   element.
 */
export type CanvasLocation = RectangleLike | Size;

/**
 * Configuration for the `Canvas` class constructor.
 */
export interface CanvasConfig extends DObjectConfig, ElementBaseOptions, ElementStyleOptions {

    /**
     * An existing `<canvas>` element to be wrapped by the `Canvas` instance.
     * The current size of that canvas will be retained. By default, a new
     * `<canvas>` element with zero size will be created automatically.
     */
    node?: NodeOrJQuery<HTMLCanvasElement>;

    /**
     * The logical location represented by the canvas DOM element.
     */
    location?: CanvasLocation;

    /**
     * Number of physical pixels in each direction of the coordinate system
     * represented by one pixel offered by the API of the canvas.
     */
    resolution?: number;

    /**
     * Optional settings for the internal 2D rendering context.
     */
    settings?: CanvasRenderingContext2DSettings;
}

/**
 * A callback function invoked for rendering.
 *
 * @param context
 *  The rendering context wrapper of the canvas element, which provides a more
 *  convenient rendering API than the native `CanvasRenderingContext2D`
 *  rendering context of the canvas.
 *
 * @param width
 *  The current width of the canvas area, in canvas pixels.
 *
 * @param height
 *  The current height of the canvas area, in canvas pixels.
 */
export type Canvas2DRenderCallback<T = void> = (context: Canvas2DContext, width: number, height: number) => T;

/**
 * A callback function invoked for rendering with a pixel-based rendering
 * context.
 *
 * @param context
 *  The pixel data wrapper of the canvas element, which provides a more
 *  convenient rendering API than the native `ImageData` class.
 *
 * @param width
 *  The current width of the canvas area, in canvas pixels.
 *
 * @param height
 *  The current height of the canvas area, in canvas pixels.
 */
export type CanvasPixelRenderCallback<T = void> = (context: CanvasPixelContext, width: number, height: number) => T;

// class Canvas ===============================================================

/**
 * A wrapper class for a `<canvas>` DOM element with additional convenience
 * methods to render into the canvas.
 */
export class Canvas extends DObject implements ElementWrapper<HTMLCanvasElement> {

    // helper canvas for relocation
    static readonly #relocateCanvas = new Canvas({ _kind: "singleton" });

    // static functions -------------------------------------------------------

    /**
     * Creates a new canvas instance that contains the picture data of the
     * passed image element.
     *
     * @param imageEl
     *  The image element to be copied into the canvas.
     *
     * @param [settings]
     *  Configuration options for the native canvas rendering context.
     *
     * @returns
     *  The new canvas instance.
     */
    static fromImage(imageEl: HTMLImageElement, settings?: CanvasRenderingContext2DSettings): Canvas {
        const canvas = new Canvas({ location: { width: imageEl.naturalHeight, height: imageEl.naturalHeight }, settings });
        canvas.render(context => context.drawImage(imageEl, 0, 0));
        return canvas;
    }

    // properties -------------------------------------------------------------

    /**
     * The wrapped DOM `<canvas>` element.
     */
    readonly el: HTMLCanvasElement;

    /**
     * The wrapped DOM `<canvas>` element, as JQuery collection.
     */
    readonly $el: JQuery<HTMLCanvasElement>;

    // the 2D rendering context of the <canvas> element
    readonly #ctxt: CanvasRenderingContext2D;
    // whether this instance wraps an external <canvas> element
    readonly #extern: boolean;

    // the current offset of the rectangle represented by the canvas element
    #x0 = 0;
    #y0 = 0;

    // the current pixel size of the location represented by the canvas element
    #width: number;
    #height: number;

    // pixel resolution (physical pixels per canvas pixel in one direction)
    #res: number;

    // constructor ------------------------------------------------------------

    /**
     * @param [config]
     *  Configuration options.
     */
    constructor(config?: CanvasConfig) {
        super(config);

        // the canvas DOM element
        const extNode = toNode(config?.node);
        this.el = extNode ?? createCanvas(0, 0, config);
        this.$el = $(this.el);

        // additional properties
        this.#ctxt = this.el.getContext("2d", config?.settings)!;
        this.#extern = !!extNode;

        // the current size of the rectangle represented by the canvas element (bitmap size, not CSS size)
        this.#width = this.el.width;
        this.#height = this.el.height;

        // pixel resolution
        this.#res = config?.resolution ?? 1;

        // put own `<canvas>` element into hidden storage
        // (Chrome reports different/wrong text widths, if the canvas element remains detached)
        if (!this.#extern) {
            insertHiddenNodes(this.el);
        }

        // initialize custom location
        if (config?.location) {
            this.initialize(config.location, this.#res);
        }
    }

    protected override destructor(): void {

        // Remove own `<canvas>` element from DOM (but not an external node passed to c'tor).
        // Before, shrink the canvas element to 1x1 size to workaround sticky cache problems in Safari.
        if (!this.#extern) {
            this.el.width = this.el.height = 1;
            this.#ctxt.clearRect(0, 0, 1, 1);
            this.el.remove();
        }

        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the pixel resolution used by this canvas (the number of physical
     * pixels in each direction of the coordinate system represented by one
     * pixel offered by the API of this canvas).
     *
     * @returns
     *  The pixel resolution used by this canvas.
     */
    getResolution(): number {
        return this.#res;
    }

    /**
     * Returns the rectangle currently represented by this canvas wrapper.
     *
     * @returns
     *  The rectangle currently represented by this canvas wrapper.
     */
    getRectangle(): Rectangle {
        return new Rectangle(this.#x0, this.#y0, this.#width, this.#height);
    }

    /**
     * Returns a data URL with the current contents of the canvas.
     *
     * @returns
     *  A data URL with the current contents of the canvas.
     */
    getDataURL(): string {
        return this.el.toDataURL();
    }

    /**
     * Returns a new blob with the current contents of the canvas.
     *
     * @returns
     *  A promise that will fulfil with the blob containing the contents of the
     *  canvas.
     */
    async getBlob(): Promise<Blob> {
        return await new Promise((resolve, reject) => {
            this.el.toBlob(blob => blob ? resolve(blob) : reject(new Error("cannot create blob")));
        });
    }

    /**
     * Returns the bitmap data of the specified rectangle in the canvas.
     *
     * @returns
     *  The bitmap data of the specified rectangle in the canvas.
     */
    getImageData(rect: RectangleLike): ImageData;
    getImageData(x: number, y: number, w: number, h: number): ImageData;
    // implementation
    getImageData(x: RectangleLike | number, y?: number, w?: number, h?: number): ImageData {
        if (!is.number(x)) { y = x.top; w = x.width; h = x.height; x = x.left; }
        return this.#ctxt.getImageData(x - this.#x0, y! - this.#y0, w!, h!);
    }

    /**
     * Changes the size and global translation of the canvas element, and
     * clears all its contents.
     *
     * @param location
     *  The logical location represented by the canvas DOM element.
     *
     * @param [resolution]
     *  Number of physical pixels in each direction of the coordinate system
     *  represented by one pixel offered by the API of this canvas.
     *
     * @returns
     *  A reference to this instance.
     */
    initialize(location: CanvasLocation, resolution = 1): this {

        // rescue old settings for performance optimizations
        const oldCssWidth = this.$el.width();
        const oldCssHeight = this.$el.height();
        const oldRes = this.#res;

        // set global translation
        this.#x0 = ("left" in location) ? location.left : 0;
        this.#y0 = ("top" in location) ? location.top : 0;
        const width = this.#width = location.width;
        const height = this.#height = location.height;

        // set new pixel resolution
        this.#res = resolution;

        // the new internal size of the canvas bitmap, according to pixel resolution
        const canvasWidth = Math.ceil(width * resolution);
        const canvasHeight = Math.ceil(height * resolution);
        const changedBitmapSize = (this.el.width !== canvasWidth) || (this.el.height !== canvasHeight);

        // do not touch canvas bitmap size, if it remains the same (may improve performance)
        if (changedBitmapSize) {
            this.el.width = canvasWidth;
            this.el.height = canvasHeight;
        }

        // initialize the visible size of the canvas element
        if ((oldCssWidth !== this.#width) || (oldCssHeight !== this.#height)) {
            this.$el.css({ width, height });
        }

        // initialize global transformation (has been reset after changing canvas size)
        if (changedBitmapSize || (oldRes !== resolution)) {
            try {
                this.#ctxt.setTransform(resolution, 0, 0, resolution, 0, 0);
            } catch (err) {
                globalLogger.error(`Canvas.initialize(): setTransform() failed (${resolution})`);
                globalLogger.exception(err);
            }
        }

        // always clear all contents
        return this.clear();
    }

    /**
     * Changes the location covered by this canvas element. Moves the bitmap
     * contents of the canvas to the new location, if the current location and
     * the passed location overlap each other.
     *
     * @param rectangle
     *  The new location covered by this canvas element.
     *
     * @param [scale]
     *  The scaling factor used when copying the old bitmap contents of this
     *  canvas element to the passed target location. MUST be a positive
     *  number.
     *
     * @returns
     *  The locations of all remaining canvas areas that could not be filled
     *  with the old contents of the canvas element.
     */
    relocate(rectangle: RectangleLike, scale = 1): Rectangle[] {

        // the old rectangle in the source canvas (previous zoom state)
        const sourceOldRect = this.getRectangle();
        // the new rectangle in target canvas (scaled to the new zoom state)
        const targetNewRect = Rectangle.from(rectangle);

        // most common case of relocation is scrolling without changing the bitmap size; in this case,
        // the canvas does not need to be reinitialized, and the common bitmap can be moved directly
        if ((this.#width === rectangle.width) && (this.#height === rectangle.height) && (scale === 1)) {

            // update global translation
            this.#x0 = rectangle.left;
            this.#y0 = rectangle.top;

            return this.render(context => {

                // move the common part of old and new rectangle to the new location
                const targetCommonRect = sourceOldRect.intersect(targetNewRect);
                if (targetCommonRect) {
                    const sourceCommonRect = targetCommonRect.clone();
                    sourceCommonRect.left += (targetNewRect.left - sourceOldRect.left);
                    sourceCommonRect.top += (targetNewRect.top - sourceOldRect.top);
                    context.copyRect(sourceCommonRect, targetCommonRect.left, targetCommonRect.top);
                }

                // clear the remaining areas
                const remainingAreas = targetCommonRect ? targetNewRect.remaining(sourceOldRect) : [targetNewRect];
                for (const remainingArea of remainingAreas) {
                    context.clearRect(remainingArea);
                }
                return remainingAreas;
            });
        }

        // the new rectangle in the source canvas (scaled back to the previous zoom state)
        const sourceNewRect = targetNewRect.clone().scaleSelf(1 / scale);
        // the old rectangle in target canvas (scaled to the new zoom state)
        const targetOldRect = sourceOldRect.clone().scaleSelf(scale);
        // the common part of old and new rectangle in source canvas (previous zoom state)
        const sourceCommonRect = sourceOldRect.intersect(sourceNewRect);
        // the common part of old and new rectangle in target canvas (current zoom state)
        const targetCommonRect = targetOldRect.intersect(targetNewRect);

        // if possible, copy common areas to secondary canvas, and swap the canvases
        if (sourceCommonRect && targetCommonRect) {

            // copy bitmap data to temporary canvas (using a temporary canvas element
            // is up to 100 times faster than using getImageData/putImageData)
            Canvas.#relocateCanvas.initialize(targetCommonRect, this.#res).render(context => {
                context.drawImage(this, targetCommonRect, sourceCommonRect);
            });

            // initialize this canvas element, and copy the (already scaled) image data back
            this.initialize(targetNewRect, this.#res).render(context => {
                context.drawImage(Canvas.#relocateCanvas, targetCommonRect);
            });

            // return the remaining empty areas in the canvas
            return targetNewRect.remaining(targetOldRect);
        }

        // nothing to copy (no common areas)
        this.initialize(targetNewRect, this.#res);

        // return the entire area as resulting empty space
        return [targetNewRect];
    }

    /**
     * Clears the contents of the entire canvas, i.e. fills the canvas with
     * fully transparent pixels.
     *
     * @returns
     *  A reference to this instance.
     */
    clear(): this {
        this.#ctxt.clearRect(0, 0, this.#width, this.#height);
        return this;
    }

    /**
     * Saves the current state of the canvas (colors, transformation, clipping,
     * etc.), invokes the passed callback function, and restores the canvas
     * state afterwards.
     *
     * @param [clipRect]
     *  An optional clipping rectangle.
     *
     * @param callback
     *  The callback function to be invoked for rendering.
     *
     * @returns
     *  The return value of the callback function.
     */
    render<T>(callback: Canvas2DRenderCallback<T>): T;
    render<T>(clipRect: RectangleLike, callback: Canvas2DRenderCallback<T>): T;
    // implementation
    render<T>(arg1: Canvas2DRenderCallback<T> | RectangleLike, arg2?: Canvas2DRenderCallback<T>): T {

        // the context wrapper for this invocation
        const contextWrapper = new Canvas2DContext(this.#ctxt, this.#x0, this.#y0);

        // resolve function arguments (missing clipping rectangle)
        const clipRect = is.function(arg1) ? undefined : arg1;
        const callback = is.function(arg1) ? arg1 : arg2!;

        // apply clipping if specified, invoke callback function
        return jpromise.invokeWithFinally(() => {
            const invokeCb = (): T => callback(contextWrapper, this.#width, this.#height);
            return clipRect ? contextWrapper.clip(clipRect, invokeCb) : contextWrapper.render(invokeCb);
        }, () => {
            contextWrapper.destroy();
        });
    }

    /**
     * Invokes the rendering callback function, and returns a data URL with the
     * resulting contents of the canvas. Convenience shortcut for the methods
     * `render` and `getDataURL`.
     *
     * @param [clipRect]
     *  An optional clipping rectangle.
     *
     * @param callback
     *  The callback function to be invoked for rendering.
     *
     * @returns
     *  A data URL with the resulting contents of the canvas after the callback
     *  function has been executed.
     */
    renderToDataURL(callback: Canvas2DRenderCallback): string;
    renderToDataURL(clipRect: RectangleLike, callback: Canvas2DRenderCallback): string;
    // implementation
    renderToDataURL(arg1: Canvas2DRenderCallback | RectangleLike, arg2?: Canvas2DRenderCallback): string {
        if (is.function(arg1)) {
            this.render(arg1);
        } else {
            this.render(arg1, arg2!);
        }
        return this.getDataURL();
    }

    /**
     * Invokes the passed callback function, and provides a helper context
     * needed to manipulate the single pixels of this canvas.
     *
     * @param callback
     *  The callback function to be invoked.
     *
     * @returns
     *  The return value of the callback function.
     */
    renderPixels<T>(callback: CanvasPixelRenderCallback<T>): T {

        // the image data wrapper for this invocation
        const pixelWrapper = new CanvasPixelContext(this.#ctxt, this.#x0, this.#y0);

        // invoke callback function, and flush the pixel data
        return jpromise.invokeWithFinally(() => {
            return callback(pixelWrapper, this.#width, this.#height);
        }, () => {
            pixelWrapper.flush();
            pixelWrapper.destroy();
        });
    }
}
