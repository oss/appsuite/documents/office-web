/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, jpromise } from "@/io.ox/office/tk/algorithms";
import { resolveCssVar, isJQuery } from "@/io.ox/office/tk/dom/domutils";
import type { RectangleLike } from "@/io.ox/office/tk/dom/rectangle";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";

import { Path } from "@/io.ox/office/tk/canvas/path";
import { CanvasContextBase } from "@/io.ox/office/tk/canvas/canvascontextbase";
import { PathGenerator } from "@/io.ox/office/tk/canvas/pathgenerator";
import { Canvas } from "@/io.ox/office/tk/canvas/canvas";

// types ======================================================================

/**
 * Possible values for the global composite operation of a canvas element.
 */
export type CanvasCompositeOperation =
    "source-over" | "source-in" | "source-out" | "source-atop" |
    "destination-over" | "destination-in" | "destination-out" | "destination-atop" |
    "lighter" | "copy" | "xor" | "multiply" | "screen" | "overlay" | "darken" | "lighten" |
    "color-dodge" | "color-burn" | "hard-light" | "soft-light" |
    "difference" | "exclusion" | "hue" | "saturation" | "color" | "luminosity";

/**
 * The definition of a color stop for a canvas gradient.
 */
export interface CanvasColorStop {

    /**
     * The relative position of the color stop in the gradient. MUST be a
     * floating-point number in the interval `[0;1]`. The value `0` represents
     * the starting point of the gradient, and the value `1` represents its end
     * point.
     */
    offset: number;

    /**
     * The CSS color to be rendered at the specified offset. May be the name of
     * a CSS variable that will be resolved against the canvas DOM element.
     */
    color: string;
}

/**
 * A bitmap source object. Can be any DOM element supporting the DOM helper
 * type `CanvasImageSource` (e.g. `<img>`, `<canvas>`, or `<video>` elements),
 * or a `JQuery` collection wrapping a DOM element mentioned before, or an
 * instance of the class `Canvas` itself.
 */
export type BitmapSource = CanvasImageSource | JQuery<CanvasImageSource> | Canvas;

/**
 * Repetition modes for fill patterns.
 *
 * - "repeat": Repeat the bitmap in both directions.
 * - "repeat-x": Repeat the bitmap horizontally only.
 * - "repeat-y": Repeat the bitmap vertically only.
 * - "no-repeat": No repetition of the bitmap.
 */
export type CanvasPatternRepeat = "repeat" | "repeat-x" | "repeat-y" | "no-repeat";

/**
 * A fill style for rendering into a canvas.
 *
 * - A string specifying a single CSS color.
 * - A string specifying the name of a CSS variable that will be resolved
 *   against the canvas DOM element.
 * - An instance of `CanvasGradient` for linear or radial gradient styles.
 * - An instance of `CanvasPattern` for pattern or bitmap fill styles.
 */
export type CanvasFillStyle = CanvasFillStrokeStyles["fillStyle"];

/**
 * The definition for a line dash pattern.
 *
 * - As pairs of numbers in a flat array. Each number in the array MUST be
 *   non-negative, and the sum of all numbers (the length of the pattern) MUST
 *   be greater than zero. The first entry of each pair in this array
 *   represents the length of a visible line segment, the second entry of each
 *   pair represents the length of the gap to the next line segment. By passing
 *   multiple pairs it is possible to specify more complex dash patterns like
 *   dash-dot-dotted lines etc. If the number of elements in the array is odd,
 *   the resulting dash pattern will be the concatenation of two copies of the
 *   array.
 *
 * - A number uses a simple pattern with equally sized segments and gaps.
 *
 * - The value `null` removes the current line dash pattern, and returns to
 *   rendering solid lines.
 */
export type CanvasLineDashPattern = number[] | number | null;

/**
 * A collection of all settings for rendering path outlines into a canvas.
 */
export interface CanvasLineStyle {

    /**
     * The fill style for rendering line segments of a path.
     */
    style?: CanvasFillStyle;

    /**
     * The width of the line segments.
     */
    width?: number;

    /**
     * The style of the line end points.
     */
    cap?: CanvasLineCap;

    /**
     * The style of the line connection points.
     */
    join?: CanvasLineJoin;

    /**
     * The line dash pattern.
     */
    pattern?: CanvasLineDashPattern;

    /**
     * The initial pattern offset for every subpath in a path stroked with a
     * dash pattern.
     */
    patternOffset?: number;
}

/**
 * A collection of all settings for rendering shadows into a canvas.
 */
export interface CanvasShadowStyle {

    /**
     * The CSS color for the shadow, or the name of a CSS variable that will be
     * resolved against the canvas DOM element.
     */
    color: string;

    /**
     * The size of the blur effect.
     */
    blur: number;

    /**
     * The offset of the shadow relative to the path in X direction.
     */
    offsetX: number;

    /**
     * The offset of the shadow relative to the path in Y direction.
     */
    offsetY: number;
}

/**
 * A collection of all settings for rendering text into a canvas.
 */
export interface CanvasFontStyle {

    /**
     * The font style, as expected by the property `font` of the canvas
     * rendering context.
     */
    font: string;

    /**
     * The horizontal alignment (accepts all values supported by the property
     * `textAlign` of a canvas rendering context).
     */
    align: CanvasTextAlign;

    /**
     * The text baseline mode (accepts all values supported by the property
     * `textBaseline` of a canvas rendering context).
     */
    baseline: CanvasTextBaseline;

    /**
     * The writing direction for the text.
     */
    rtl: boolean;
}

/**
 * The rendering modes for a path.
 *
 * - `all`: The path will be filled, and afterwards the path outline will be
 *   rendered.
 *
 * - `fill`: The path will be filled, but the path outline will NOT be
 *   rendered, regardless of the current line style in the canvas.
 *
 * - `stroke`: The path outline will be rendered, but the path will NOT be
 *   filled, regardless of the current fill style in the canvas.
 */
export type PathMode = "all" | "fill" | "stroke";

/**
 * List of point coordinates, e.g. for defining polygons.
 */
export type PointList = Array<Pair<number>>;

// private functions ==========================================================

/**
 * Converts the passed data to a DOM `CanvasImageSource` (`<img>` or `<canvas>`
 * elements, bitmap data as `ImageBitmap`).
 */
function toImageSource(source: BitmapSource): CanvasImageSource {
    return (source instanceof Canvas) ? source.el : isJQuery(source) ? (source[0] || new Image()) : source;
}

/**
 * Returns whether the size of the passed rectangle is zero.
 */
function isEmptyRect(rect: RectangleLike): boolean {
    return (rect.width === 0) || (rect.height === 0);
}

/**
 * Returns whether the passed value is a valid canvas fill style.
 */
function isCanvasFillStyle(value: unknown): value is CanvasFillStyle {
    return is.string(value) || (value instanceof CanvasGradient) || (value instanceof CanvasPattern);
}

// class Canvas2DContext ======================================================

/**
 * This wrapper class provides a higher-level API for a 2D rendering context of
 * a canvas element.
 *
 * @param context2D
 *  The wrapped rendering context of a canvas element.
 *
 * @param x0
 *  The global translation on the X axis that will be applied to all path
 *  operations.
 *
 * @param y0
 *  The global translation on the Y axis that will be applied to all path
 *  operations.
 */
export class Canvas2DContext extends CanvasContextBase {

    // properties -------------------------------------------------------------

    // generator for native paths in the rendering context
    readonly #pathGenerator: PathGenerator;

    // constructor ------------------------------------------------------------

    constructor(context2D: CanvasRenderingContext2D, x0: number, y0: number) {
        super(context2D, x0, y0);
        this.#pathGenerator = this.member(new PathGenerator(context2D, x0, y0));

        // Do not install a global translation at the native canvas element, to workaround
        // the limited floating-point precision for large offsets provided by the canvas.
        // Browsers usually only implement single-precision instead of double-precision
        // as required by the W3C (see bug 35653, and http://stackoverflow.com/a/8874802).

        // Instead, all rendering operations will calculate the global translation manually
        // using the class properties `x0` and `y0` provided by the base class.
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a descriptor for a linear gradient, that can be used as fill
     * style for the inner area of rendering paths, or as stroke style for the
     * outline of a rendering path.
     *
     * @param x1
     *  The X coordinate of the starting point of the linear gradient.
     *
     * @param y1
     *  The Y coordinate of the starting point of the linear gradient.
     *
     * @param x2
     *  The X coordinate of the end point of the linear gradient.
     *
     * @param y2
     *  The Y coordinate of the end point of the linear gradient.
     *
     * @param stops
     *  The color stops for the gradient.
     *
     * @returns
     *  The descriptor for a linear gradient.
     */
    createLinearGradient(x1: number, y1: number, x2: number, y2: number, stops: CanvasColorStop[]): CanvasGradient {
        const gradient = this.ctx.createLinearGradient(x1 - this.x0, y1 - this.y0, x2 - this.x0, y2 - this.y0);
        this.#addGradientColorStops(gradient, stops);
        return gradient;
    }

    /**
     * Creates a descriptor for a radial gradient, that can be used as fill
     * style for the inner area of rendering paths, or as stroke style for the
     * outline of a rendering path.
     *
     * @param x1
     *  The X coordinate of the start circle of the radial gradient.
     *
     * @param y1
     *  The Y coordinate of the start circle of the radial gradient.
     *
     * @param r1
     *  The radius of the start circle of the radial gradient.
     *
     * @param x2
     *  The X coordinate of the end circle of the radial gradient.
     *
     * @param y2
     *  The Y coordinate of the end circle of the radial gradient.
     *
     * @param r2
     *  The radius of the end circle of the radial gradient.
     *
     * @param stops
     *  The color stops for the gradient.
     *
     * @returns
     *  The descriptor for a radial gradient.
     */
    createRadialGradient(x1: number, y1: number, r1: number, x2: number, y2: number, r2: number, stops: CanvasColorStop[]): CanvasGradient {
        const gradient = this.ctx.createRadialGradient(x1 - this.x0, y1 - this.y0, r1, x2 - this.x0, y2 - this.y0, r2);
        this.#addGradientColorStops(gradient, stops);
        return gradient;
    }

    /**
     * Creates a descriptor for a fill pattern, that can be used as fill style
     * for the inner area of rendering paths, or as stroke style for the
     * outline of a rendering path.
     *
     * @param source
     *  The bitmap source object.
     *
     * @param [repeat="repeat"]
     *  The repetition mode.
     *
     * @returns
     *  The canvas pattern object.
     */
    createPattern(source: BitmapSource, repeat?: CanvasPatternRepeat): CanvasPattern {
        return this.ctx.createPattern(toImageSource(source), repeat || "repeat")!;
    }

    /**
     * Sets the fill style for rendering the inner area of a path.
     *
     * @param style
     *  The new fill style for rendering the inner fill area.
     *
     * @returns
     *  A reference to this instance.
     */
    setFillStyle(style: CanvasFillStyle): this {
        this.ctx.fillStyle = this.#resolveStyle(style);
        return this;
    }

    /**
     * Sets the stroke style, and other line properties, for rendering the
     * outline of a path.
     *
     * @param style
     *  The new stroke style settings. Can be a an object with different line
     *  style settings (omitting a property will not change the current value
     *  of the respective style setting), or a canvas fill style (used to set
     * the `strokeStyle` property of the rendering context only).
     *
     * @returns
     *  A reference to this instance.
     */
    setLineStyle(style: CanvasFillStyle | CanvasLineStyle): this {

        // convenience shortcut: pass fill style value directly
        if (isCanvasFillStyle(style)) {
            this.ctx.strokeStyle = this.#resolveStyle(style);
            return this;
        }

        // process all properties of a `CanvasLineStyle`
        if (style.style) { this.ctx.strokeStyle = this.#resolveStyle(style.style); }
        if (is.number(style.width)) { this.ctx.lineWidth = style.width; }
        if (style.cap) { this.ctx.lineCap = style.cap; }
        if (style.join) { this.ctx.lineJoin = style.join; }
        if (style.pattern !== undefined) { this.#initLineDash(style.pattern); }
        if (is.number(style.patternOffset)) { this.ctx.lineDashOffset = style.patternOffset; }
        return this;
    }

    /**
     * Sets the shadow style for rendering a path in the canvas.
     *
     * @param style
     *  The new shadow style settings. Omitting a property will not change the
     *  current value of the respective style setting.
     *
     * @returns
     *  A reference to this instance.
     */
    setShadowStyle(style: Partial<CanvasShadowStyle>): this {

        if (style.color) { this.ctx.shadowColor = this.#resolveStyle(style.color); }
        if (is.number(style.blur)) { this.ctx.shadowBlur = style.blur; }
        if (is.number(style.offsetX)) { this.ctx.shadowOffsetX = style.offsetX; }
        if (is.number(style.offsetY)) { this.ctx.shadowOffsetY = style.offsetY; }

        return this;
    }

    /**
     * Sets the font style, and other font properties, for rendering of texts
     * in the canvas.
     *
     * @param style
     *  The new font style parameters. Omitting a property will not change the
     *  current value of the respective style setting.
     *
     * @returns
     *  A reference to this instance.
     */
    setFontStyle(style: Partial<CanvasFontStyle>): this {

        if (style.font) { this.ctx.font = style.font; }
        if (style.align) { this.ctx.textAlign = style.align; }
        if (style.baseline) { this.ctx.textBaseline = style.baseline; }

        // The property `direction` of the canvas rendering context is hardly supported in real-life,
        // but as a workaround, the `dir` element attribute of the `<canvas>` element will do the job.
        if ("rtl" in style) { this.ctx.canvas.dir = style.rtl ? "rtl" : "ltr"; }

        return this;
    }

    /**
     * Sets the global opaqueness for all drawing operations (fill, stroke,
     * text, and images).
     *
     * @param alpha
     *  The new global opaqueness for all drawing operations. The value `1`
     *  represents full opacity, the value `0` represents full transparency.
     *  Numbers outside the interval [0,1] will be clamped automatically.
     *
     * @returns
     *  A reference to this instance.
     */
    setGlobalAlpha(alpha: number): this {
        this.ctx.globalAlpha = math.clamp(alpha, 0, 1);
        return this;
    }

    /**
     * Sets the global composite operation for all drawing operations (fill,
     * stroke, text, and images).
     *
     * @param operation
     *  The new global composite operation for all drawing operations.
     *
     * @returns
     *  A reference to this instance.
     */
    setGlobalComposite(operation: CanvasCompositeOperation): this {
        this.ctx.globalCompositeOperation = operation;
        return this;
    }

    /**
     * Changes the translation of the coordinate system.
     *
     * @param dx
     *  The horizontal translation.
     *
     * @param dy
     *  The vertical translation.
     *
     * @returns
     *  A reference to this instance.
     */
    translate(dx: number, dy: number): this {
        this.#pathGenerator.translate(dx, dy);
        return this;
    }

    /**
     * Changes the scaling factors of the coordinate system.
     *
     * @param fx
     *  The horizontal scaling factor.
     *
     * @param fy
     *  The vertical scaling factor.
     *
     * @returns
     *  A reference to this instance.
     */
    scale(fx: number, fy: number): this {
        this.#pathGenerator.scale(fx, fy);
        return this;
    }

    /**
     * Changes the rotation angle of the coordinate system.
     *
     * @param a
     *  The rotation angle (radiant).
     *
     * @returns
     *  A reference to this instance.
     */
    rotate(a: number): this {
        this.#pathGenerator.rotate(a);
        return this;
    }

    /**
     * Saves the current state of the canvas (colors, transformation, clipping,
     * etc.), invokes the passed callback function, and restores the canvas
     * state afterwards.
     *
     * @param callback
     *  The callback function to be invoked.
     *
     * @returns
     *  The return value of the callback function.
     */
    render<T>(callback: FuncType<T>): T {
        return jpromise.invokeWithFinally(() => {
            this.ctx.save();
            return callback();
        }, () => {
            this.ctx.restore();
        });
    }

    /**
     * Saves the current state of the rendering context (all stroke, fill, and
     * text styles, transformation, clipping, etc.), sets up (or adds) a
     * rectangular clipping region, invokes the passed callback function, and
     * restores the saved context state afterwards.
     *
     * @param rectangle
     *  The clipping rectangle.
     *
     * @param callback
     *  The callback function to be invoked with active clipping region.
     *
     * @returns
     *  The return value of the callback function.
     */
    clip<T>(rectangle: RectangleLike, callback: FuncType<T>): T {
        return this.render(() => {
            this.ctx.beginPath();
            this.ctx.rect(rectangle.left - this.x0, rectangle.top - this.y0, rectangle.width, rectangle.height);
            this.ctx.clip();
            return callback();
        });
    }

    /**
     * Creates a new empty path object. Path objects are expected by other
     * rendering methods of this canvas wrapper. A caller may create as many
     * path objects as needed at the same time.
     *
     * @returns
     *  A new empty path object.
     */
    createPath(): Path {
        return new Path();
    }

    /**
     * Renders the specified path into the wrapped canvas element. First, the
     * path will be filled according to the current fill style. Afterwards, the
     * path outline will be rendered according to the current line style.
     *
     * @param path
     *  The path to be rendered.
     *
     * @param [mode]
     *  The rendering mode for the path.
     *
     * @returns
     *  A reference to this instance.
     */
    drawPath(path: Path, mode: PathMode = "all"): this {

        // whether to actually fill and stroke the path
        const fill = mode !== "stroke";
        const stroke = mode !== "fill";

        // generate the path in the rendering context, and render the path
        if (fill || stroke) { this.#pathGenerator.generatePath(path); }
        if (fill) { this.ctx.fill("evenodd"); }
        if (stroke) { this.ctx.stroke(); }

        return this;
    }

    /**
     * Renders a straight line into the wrapped canvas element.
     *
     * @param x1
     *  The X coordinate of the starting point of the line.
     *
     * @param y1
     *  The Y coordinate of the starting point of the line.
     *
     * @param x2
     *  The X coordinate of the end point of the line.
     *
     * @param y2
     *  The Y coordinate of the end point of the line.
     *
     * @returns
     *  A reference to this instance.
     */
    drawLine(x1: number, y1: number, x2: number, y2: number): this {
        return this.drawPath(new Path().pushLine(x1, y1, x2, y2), "stroke");
    }

    /**
     * Renders multiple straight lines into the wrapped canvas element.
     *
     * @param points
     *  An arbitrary number of coordinate pairs (x/y) representing the points
     *  of the lines added to the chain.
     *
     * @returns
     *  A reference to this instance.
     */
    drawLineChain(points: PointList): this {
        return this.drawPath(new Path().pushLineChain(points), "stroke");
    }

    /**
     * Renders a closed polygon formed by multiple straight lines into the
     * wrapped canvas element.
     *
     * @param points
     *  An arbitrary number of coordinate pairs (x/y) representing the points
     *  of the lines added to the chain.
     *
     * @param [mode]
     *  The rendering mode for the polygon.
     *
     * @returns
     *  A reference to this instance.
     */
    drawPolygon(points: PointList, mode: PathMode = "all"): this {
        return this.drawPath(new Path().pushPolygon(points), mode);
    }

    /**
     * Renders a rectangle into the wrapped canvas element.
     *
     * @param rect
     *  The rectangle to be rendered.
     *
     * @param x
     *  The X coordinate of the starting point of the rectangle.
     *
     * @param y
     *  The Y coordinate of the starting point of the rectangle.
     *
     * @param w
     *  The width of the rectangle.
     *
     * @param h
     *  The height of the rectangle.
     *
     * @param [mode]
     *  The rendering mode for the rectangle. Default value is "all".
     *
     * @returns
     *  A reference to this instance.
     */
    drawRect(rect: RectangleLike, mode?: PathMode): this;
    drawRect(x: number, y: number, w: number, h: number, mode?: PathMode): this;
    // implementation
    drawRect(arg1: RectangleLike | number, arg2?: number | PathMode, w?: number, h?: number, mode?: PathMode): this {
        if (is.number(arg1)) {
            this.drawPath(new Path().pushRect(arg1, arg2 as number, w!, h!), mode);
        } else {
            this.drawPath(new Path().pushRect(arg1), arg2 as Opt<PathMode>);
        }
        return this;
    }

    /**
     * Renders a circle into the wrapped canvas element.
     *
     * @param x
     *  The X coordinate of the center point of the circle.
     *
     * @param y
     *  The Y coordinate of the center point of the circle.
     *
     * @param r
     *  The radius of the circle.
     *
     * @param [mode]
     *  The rendering mode for the circle.
     *
     * @returns
     *  A reference to this instance.
     */
    drawCircle(x: number, y: number, r: number, mode: PathMode = "all"): this {
        return this.drawPath(new Path().pushCircle(x, y, r), mode);
    }

    /**
     * Renders an ellipse into the wrapped canvas element.
     *
     * @param x
     *  The X coordinate of the center point of the ellipse.
     *
     * @param y
     *  The Y coordinate of the center point of the ellipse.
     *
     * @param rx
     *  The radius of the ellipse on the X axis.
     *
     * @param ry
     *  The radius of the ellipse on the Y axis.
     *
     * @param [a=0]
     *  The rotation angle (radiant) of the ellipse.
     *
     * @param [mode]
     *  The rendering mode for the ellipse. Default value is "all".
     *
     * @returns
     *  A reference to this instance.
     */
    drawEllipse(x: number, y: number, rx: number, ry: number, mode?: PathMode): this;
    drawEllipse(x: number, y: number, rx: number, ry: number, a: number, mode?: PathMode): this;
    // implementation
    drawEllipse(x: number, y: number, rx: number, ry: number, a?: number | PathMode, mode?: PathMode): this {
        if (is.string(a)) { mode = a; a = 0; }
        return this.drawPath(new Path().pushEllipse(x, y, rx, ry, a), mode);
    }

    /**
     * Clears a rectangle (sets all pixels to full transparency) in the wrapped
     * canvas element.
     *
     * @param rect
     *  The rectangle to be cleared.
     *
     * @param x
     *  The X coordinate of the starting point of the rectangle.
     *
     * @param y
     *  The Y coordinate of the starting point of the rectangle.
     *
     * @param w
     *  The width of the rectangle.
     *
     * @param h
     *  The height of the rectangle.
     *
     * @returns
     *  A reference to this instance.
     */
    clearRect(rect: RectangleLike): this;
    clearRect(x: number, y: number, w: number, h: number): this;
    // implementation
    clearRect(arg1: RectangleLike | number, y?: number, w?: number, h?: number): this {
        if (is.number(arg1)) {
            this.ctx.clearRect(arg1 - this.x0, y! - this.y0, w!, h!);
        } else {
            this.ctx.clearRect(arg1.left - this.x0, arg1.top - this.y0, arg1.width, arg1.height);
        }
        return this;
    }

    /**
     * Copies all pixels of a rectangle to another location.
     *
     * @param rect
     *  The rectangle to be copied.
     *
     * @param x
     *  The X coordinate of the starting point of the rectangle.
     *
     * @param y
     *  The Y coordinate of the starting point of the rectangle.
     *
     * @param w
     *  The width of the rectangle.
     *
     * @param h
     *  The height of the rectangle.
     *
     * @param dx
     *  The destination X coordinate to copy the rectangle to.
     *
     * @param dy
     *  The destination Y coordinate to copy the rectangle to.
     *
     * @returns
     *  A reference to this instance.
     */
    copyRect(rect: RectangleLike, dx: number, dy: number): this;
    copyRect(x: number, y: number, w: number, h: number, dx: number, dy: number): this;
    // implementation
    copyRect(arg1: RectangleLike | number, arg2: number, arg3: number, h?: number, dx?: number, dy?: number): this {
        if (is.number(arg1)) {
            this.ctx.drawImage(
                this.ctx.canvas,
                arg1 - this.x0, arg2 - this.y0, arg3, h!,   // source rectangle
                dx! - this.x0, dy! - this.y0, arg3, h!      // destination rectangle
            );
        } else {
            this.ctx.drawImage(
                this.ctx.canvas,
                arg1.left - this.x0, arg1.top - this.y0, arg1.width, arg1.height,   // source rectangle
                arg2 - this.x0, arg3 - this.y0, arg1.width, arg1.height             // destination rectangle
            );
        }
        return this;
    }

    /**
     * Draws the bitmap contents of the passed image source into the wrapped
     * canvas element.
     *
     * @param source
     *  The bitmap source object.
     *
     * @param x
     *  The target X coordinate where to draw the entire unscaled source image
     *  into this context.
     *
     * @param y
     *  The target Y coordinate where to draw the entire unscaled source image
     *  into this context.
     *
     * @param targetRect
     *  A rectangle specifying the target area in this context to draw the
     *  scaled source image to.
     *
     * @param [sourceRect]
     *  An optional rectangle specifying the partial area in the source image
     *  to be drawn into this context. If omitted, the entire source image will
     *  be scaled into the target rectangle. Note that in difference to the
     *  native method `CanvasRenderingContext2D.drawImage()`, the target
     *  rectangle ALWAYS precedes the source rectangle! If the bitmap source
     *  object is an instance of the class `Canvas`, its global translation and
     *  resolution will be taken into account when interpreting this parameter.
     *
     * @returns
     *  A reference to this instance.
     */
    drawImage(source: BitmapSource, x: number, y: number): this;
    drawImage(source: BitmapSource, targetRect: RectangleLike, sourceRect?: RectangleLike): this;
    // implementation
    drawImage(source: BitmapSource, arg1: number | RectangleLike, arg2?: number | RectangleLike): this {

        // convert to plain DOM image source
        const imgSource = toImageSource(source);

        // coordinates as numbers
        if (is.number(arg1)) {
            this.ctx.drawImage(imgSource, arg1 - this.x0, (arg2 as number) - this.y0);
            return this;
        }

        // nothing to do, if the target rectangle scales to zero size
        if (isEmptyRect(arg1)) {
            return this;
        }

        // pixel resolution in source canvas
        let sres = 1;
        // global translation in source canvas
        let sx0 = 0, sy0 = 0;

        // get source canvas settings
        if (source instanceof Canvas) {
            sres = source.getResolution();
            const sourceRect = source.getRectangle();
            sx0 = sourceRect.left;
            sy0 = sourceRect.top;
        }

        // the effective target rectangle
        const tx = arg1.left - this.x0;
        const ty = arg1.top - this.y0;
        const tw = arg1.width;
        const th = arg1.height;

        const src = arg2 as Opt<RectangleLike>;
        if (!src) {
            // target rectangle only
            this.ctx.drawImage(imgSource, tx, ty, tw, th);
        } else if (!isEmptyRect(src)) {
            // source and target rectangle (native method expects source before target)
            const sx = (src.left - sx0) * sres;
            const sy = (src.top - sy0) * sres;
            const sw = src.width * sres;
            const sh = src.height * sres;
            this.ctx.drawImage(imgSource, sx, sy, sw, sh, tx, ty, tw, th);
        }
        return this;
    }

    /**
     * Renders the specified text into the wrapped canvas element.
     *
     * @param text
     *  The text to be rendered.
     *
     * @param x
     *  The X coordinate of the starting point of the text.
     *
     * @param y
     *  The Y coordinate of the starting point of the text.
     *
     * @param [mode]
     *  The rendering mode for the text. See description of `drawPath` for
     *  details.
     *
     * @returns
     *  A reference to this instance.
     */
    drawText(text: string, x: number, y: number, mode: PathMode = "all"): this {

        // fill the text, if a fill style has been set
        if (mode !== "stroke") {
            this.ctx.fillText(text, x - this.x0, y - this.y0);
        }

        // stroke the text outline, if a stroke style has been set
        if (mode !== "fill") {
            this.ctx.strokeText(text, x - this.x0, y - this.y0);
        }

        return this;
    }

    /**
     * Returns the width of the passed string.
     *
     * @param text
     *  The text whose width will be calculated.
     *
     * @returns
     *  The width of the passed string, as floating-point pixels.
     */
    getTextWidth(text: string): number {
        return this.ctx.measureText(text).width;
    }

    /**
     * Returns the width of the passed single character. To increase precision
     * for different browsers (some browsers return a rounded integral width
     * for single characters), the character will be repeated a hundred times,
     * and a hundredth of the width of the resulting string will be returned.
     *
     * @param char
     *  A single character.
     *
     * @returns
     *  The width of the passed character, as floating-point pixels.
     */
    getCharacterWidth(char: string): number {
        return this.getTextWidth(char.repeat(100)) / 100;
    }

    // private methods --------------------------------------------------------

    /**
     * If the passed rendering style is the name of a CSS variable (a string
     * string with two dashes), the value of the variable will be resolved.
     */
    #resolveStyle<T>(style: T | string): T | string {
        return (is.string(style) && style.startsWith("--")) ? resolveCssVar(style, this.ctx.canvas) : style;
    }

    /**
     * Adds the passed color stops to the canvas gradient, and catches any
     * exceptions that may occur due to invalid input values.
     *
     * @param gradient
     *  A canvas gradient descriptor.
     *
     * @param stops
     *  The color stops.
     */
    #addGradientColorStops(gradient: CanvasGradient, stops: CanvasColorStop[]): void {
        try {
            for (const stop of stops) {
                gradient.addColorStop(stop.offset, this.#resolveStyle(stop.color));
            }
        } catch (err) {
            globalLogger.exception(err);
        }
    }

    /**
     * Sets the current line dash pattern.
     *
     * @param pattern
     *  The new dash pattern.
     */
    #initLineDash(pattern: CanvasLineDashPattern): void {
        this.ctx.setLineDash(is.number(pattern) ? [pattern] : (pattern ?? []));
    }
}
