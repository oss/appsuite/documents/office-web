/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import { RGBModel, resolveCssColor } from "@/io.ox/office/tk/dom/colorutils";

import { CanvasContextBase } from "@/io.ox/office/tk/canvas/canvascontextbase";

// constants ==================================================================

// color byte array for full transparence
const TRANSPARENT = new Uint8ClampedArray(4);

// class CanvasPixelContext ===================================================

/**
 * This wrapper class provides a higher-level API for an `ImageData` object of
 * a canvas element with methods to manipulate individual pixels.
 *
 * @param context2D
 *  The wrapped rendering context of a canvas element.
 *
 * @param x0
 *  The global translation on the X axis that will be applied to all path
 *  operations.
 *
 * @param y0
 *  The global translation on the Y axis that will be applied to all path
 *  operations.
 */
export class CanvasPixelContext extends CanvasContextBase {

    // properties -------------------------------------------------------------

    readonly data: Uint8ClampedArray;

    readonly #w: number;
    readonly #h: number;
    readonly #bitmap: ImageData;
    #color = TRANSPARENT;

    // constructor ------------------------------------------------------------

    constructor(context2D: CanvasRenderingContext2D, x0: number, y0: number) {
        super(context2D, x0, y0);
        this.#w = context2D.canvas.width;
        this.#h = context2D.canvas.height;
        this.#bitmap = context2D.getImageData(0, 0, this.#w, this.#h);
        this.data = this.#bitmap.data;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the RGBA components of the specified canvas pixel.
     *
     * @param x
     *  The X coordinate of the pixel.
     *
     * @param y
     *  The Y coordinate of the pixel.
     *
     * @returns
     *  The RGBA components of the specified canvas pixel, as array with four
     *  bytes.
     */
    getPixelBytes(x: number, y: number): Uint8ClampedArray {
        const offset = this.#getOffset(x, y);
        return this.data.slice(offset, offset + 4);
    }

    /**
     * Returns the CSS color of the specified canvas pixel.
     *
     * @param x
     *  The X coordinate of the pixel.
     *
     * @param y
     *  The Y coordinate of the pixel.
     *
     * @returns
     *  The CSS color of the specified canvas pixel.
     */
    getPixelCssColor(x: number, y: number): string {
        const bytes = this.getPixelBytes(x, y);
        return RGBModel.parseBytes(bytes).toCSS();
    }

    /**
     * Sets the color for new pixels drawn into the canvas.
     *
     * @param color
     *  The new pixel color, as RGBA byte array, or CSS color string.
     *
     * @returns
     *  A reference to this instance.
     */
    setColor(color: ArrayLike<number> | string): this {
        this.#color = is.string(color) ? resolveCssColor(color) : Uint8ClampedArray.from(color);
        return this;
    }

    /**
     * Sets the specified pixel to the current pixel color of this instance.
     *
     * @param x
     *  The X coordinate of the pixel.
     *
     * @param y
     *  The Y coordinate of the pixel.
     *
     * @returns
     *  A reference to this instance.
     */
    drawPixel(x: number, y: number): this {
        this.data.set(this.#color, this.#getOffset(x, y));
        return this;
    }

    /**
     * Sets the specified pixel to full transparence.
     *
     * @param x
     *  The X coordinate of the pixel.
     *
     * @param y
     *  The Y coordinate of the pixel.
     *
     * @returns
     *  A reference to this instance.
     */
    clearPixel(x: number, y: number): this {
        this.data.set(TRANSPARENT, this.#getOffset(x, y));
        return this;
    }

    /**
     * Sets all pixels in the specified rectangle to the current pixel color of
     * this instance.
     *
     * @param x
     *  The X coordinate of the starting point of the rectangle.
     *
     * @param y
     *  The Y coordinate of the starting point of the rectangle.
     *
     * @param w
     *  The width of the rectangle.
     *
     * @param h
     *  The height of the rectangle.
     *
     * @returns
     *  A reference to this instance.
     */
    fillRect(x: number, y: number, w: number, h: number): this {
        this.#writeRect(x, y, w, h, this.#color);
        return this;
    }

    /**
     * Sets all pixels in the specified rectangle to full transparence.
     *
     * @param x
     *  The X coordinate of the starting point of the rectangle.
     *
     * @param y
     *  The Y coordinate of the starting point of the rectangle.
     *
     * @param w
     *  The width of the rectangle.
     *
     * @param h
     *  The height of the rectangle.
     *
     * @returns
     *  A reference to this instance.
     */
    clearRect(x: number, y: number, w: number, h: number): this {
        this.#writeRect(x, y, w, h, TRANSPARENT);
        return this;
    }

    /**
     * Renders the manipulated pixel data back to the canvas.
     */
    flush(): void {
        this.ctx.putImageData(this.#bitmap, 0, 0);
    }

    // private methods --------------------------------------------------------

    #getOffset(x: number, y: number): number {
        return 4 * ((y - this.y0) * this.#w + x - this.x0);
    }

    #writeRect(x: number, y: number, w: number, h: number, color: Uint8ClampedArray): void {

        // prepare a row of bytes to be blit into the rectangle
        const bytes = new Uint8ClampedArray(w * 4);
        for (let i = 0; i < w; i += 1) { bytes.set(color, i * 4); }

        // copy the row bytes into the rectangle
        const { data } = this;
        for (let y1 = y, y2 = y + h; y1 < y2; y1 += 1) {
            data.set(bytes, this.#getOffset(x, y1));
        }
    }
}
