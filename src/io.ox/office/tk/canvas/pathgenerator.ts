/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PathOperation, Path } from "@/io.ox/office/tk/canvas/path";
import { CanvasContextBase } from "@/io.ox/office/tk/canvas/canvascontextbase";

// class PathGenerator ========================================================

/**
 * This class implements the rendering path API of a 2D rendering context of a
 * canvas element.
 *
 * It uses the native methods of the wrapped rendering context, but applies a
 * fixed global translation to all operations. This is needed to workaround the
 * limited floating-point precision provided by the canvas (single-precision
 * instead of double-precision, see http://stackoverflow.com/a/8874802, and bug
 * 35653).
 *
 * @param context2D
 *  The wrapped rendering context of a canvas element.
 *
 * @param x0
 *  The global translation on the X axis that will be applied to all path
 *  operations.
 *
 * @param y0
 *  The global translation on the Y axis that will be applied to all path
 *  operations.
 */
export class PathGenerator extends CanvasContextBase implements PathOperation {

    // constructor ------------------------------------------------------------

    public constructor(context2D: CanvasRenderingContext2D, x0: number, y0: number) {
        super(context2D, x0, y0);
    }

    // path API ---------------------------------------------------------------

    beginPath(): void {
        this.ctx.beginPath();
    }

    save(): void {
        this.ctx.save();
    }

    restore(): void {
        this.ctx.restore();
    }

    translate(dx: number, dy: number): void {
        this.ctx.translate(dx, dy);
    }

    scale(fx: number, fy: number): void {
        this.ctx.translate(-this.x0, -this.y0);
        this.ctx.scale(fx, fy);
        this.ctx.translate(this.x0, this.y0);
    }

    rotate(a: number): void {
        this.ctx.translate(-this.x0, -this.y0);
        this.ctx.rotate(a);
        this.ctx.translate(this.x0, this.y0);
    }

    lineCap(value: CanvasLineCap): void {
        this.ctx.lineCap = value;
    }

    moveTo(x: number, y: number): void {
        this.ctx.moveTo(x - this.x0, y - this.y0);
    }

    lineTo(x: number, y: number): void {
        this.ctx.lineTo(x - this.x0, y - this.y0);
    }

    arcTo(x1: number, y1: number, x2: number, y2: number, r: number): void {
        this.ctx.arcTo(x1 - this.x0, y1 - this.y0, x2 - this.x0, y2 - this.y0, r);
    }

    quadraticCurveTo(cx: number, cy: number, x: number, y: number): void {
        this.ctx.quadraticCurveTo(cx - this.x0, cy - this.y0, x - this.x0, y - this.y0);
    }

    bezierCurveTo(cx1: number, cy1: number, cx2: number, cy2: number, x: number, y: number): void {
        this.ctx.bezierCurveTo(cx1 - this.x0, cy1 - this.y0, cx2 - this.x0, cy2 - this.y0, x - this.x0, y - this.y0);
    }

    arc(x: number, y: number, r: number, a1: number, a2: number, ccw?: boolean): void {
        this.ctx.arc(x - this.x0, y - this.y0, r, a1, a2, ccw);
    }

    ellipse(x: number, y: number, rx: number, ry: number, a0: number, a1: number, a2: number, ccw?: boolean): void {
        this.ctx.ellipse(x - this.x0, y - this.y0, rx, ry, a0, a1, a2, ccw);
    }

    closePath(): void {
        this.ctx.closePath();
    }

    // public methods ---------------------------------------------------------

    /**
     * Generates the passed path in the wrapped rendering context using its
     * native implementation.
     *
     * @param path
     *  The description of the rendering path to be generated.
     */
    generatePath(path: Path): void {
        this.beginPath();
        path.ops.forEach(op => op(this));
    }
}
