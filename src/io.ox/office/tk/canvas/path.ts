/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";
import type { RectangleLike } from "@/io.ox/office/tk/dom/rectangle";

const { sin, cos } = Math;

// constants ==================================================================

const PI2 = 2 * Math.PI;

// interface PathOperation ====================================================

/**
 * Atomic path operations that can be stored in a `Path` object.
 */
export interface PathOperation {

    save(): void;
    restore(): void;

    translate(dx: number, dy: number): void;
    scale(fx: number, fy: number): void;
    rotate(a: number): void;

    lineCap(value: CanvasLineCap): void;

    moveTo(x: number, y: number): void;
    lineTo(x: number, y: number): void;
    arcTo(x1: number, y1: number, x2: number, y2: number, r: number): void;
    quadraticCurveTo(cx: number, cy: number, x: number, y: number): void;
    bezierCurveTo(cx1: number, cy1: number, cx2: number, cy2: number, x: number, y: number): void;
    arc(x: number, y: number, r: number, a1: number, a2: number, ccw?: boolean): void;
    ellipse(x: number, y: number, rx: number, ry: number, a0: number, a1: number, a2: number, ccw?: boolean): void;

    closePath(): void;
}

// class Path =================================================================

/**
 * Represents a rendering path for a canvas rendering context, consisting of
 * several path operations.
 */
export class Path {

    // properties -------------------------------------------------------------

    /**
     * All recorded operations of this path.
     */
    readonly ops: Array<FuncType<void, [PathOperation]>> = [];

    // public methods ---------------------------------------------------------

    /**
     * Opens a new empty subpath in this path, starting at the specified
     * position.
     *
     * @param x
     *  The X coordinate of the starting point of the new subpath.
     *
     * @param y
     *  The Y coordinate of the starting point of the new subpath.
     *
     * @returns
     *  A reference to this instance.
     */
    moveTo(x: number, y: number): this {
        return this.#pushOp("moveTo", x, y);
    }

    /**
     * Adds a new position to the current (last) subpath in this path that will
     * be connected to the previous position with a straight line.
     *
     * @param x
     *  The X coordinate of the new position in the current subpath.
     *
     * @param y
     *  The Y coordinate of the new position in the current subpath.
     *
     * @returns
     *  A reference to this instance.
     */
    lineTo(x: number, y: number): this {
        return this.#pushOp("lineTo", x, y);
    }

    /**
     * Adds a circular arc to the current (last) subpath in this path. The
     * start position of the arc will be connected to the end position of the
     * subpath with a straight line.
     *
     * @param x
     *  The X coordinate of the center point of the arc.
     *
     * @param y
     *  The Y coordinate of the center point of the arc.
     *
     * @param r
     *  The radius of the arc.
     *
     * @param a1
     *  The angle (radiant) of the arc's start position. The value 0 represents
     *  a position on the positive part of the X axis.
     *
     * @param a2
     *  The angle (radiant) of the arc's end position. The value 0 represents a
     *  position on the positive part of the X axis.
     *
     * @param [ccw]
     *  Whether to draw the arc counterclockwise from its start position to its
     *  end position. By default, the arc will be drawn clockwise.
     *
     * @returns
     *  A reference to this instance.
     */
    arc(x: number, y: number, r: number, a1: number, a2: number, ccw = false): this {
        return this.#pushOp("arc", x, y, r, a1, a2, ccw);
    }

    /**
     * Adds an elliptic arc to the current (last) subpath in this path. The
     * start position of the arc will be connected to the end position of the
     * subpath with a straight line.
     *
     * @param x
     *  The X coordinate of the center point of the arc.
     *
     * @param y
     *  The Y coordinate of the center point of the arc.
     *
     * @param rx
     *  The radius of the arc on the X axis.
     *
     * @param ry
     *  The radius of the arc on the Y axis.
     *
     * @param a0
     *  The rotation angle (radiant) of the entire ellipse, including the
     *  following start and end angles.
     *
     * @param a1
     *  The angle (radiant) of the arc's start position. The value 0 represents
     *  a position on the positive part of the X axis.
     *
     * @param a2
     *  The angle (radiant) of the arc's end position. The value 0 represents a
     *  position on the positive part of the X axis.
     *
     * @param [ccw]
     *  Whether to draw the arc counterclockwise from its start position to its
     *  end position. By default, the arc will be drawn clockwise.
     *
     * @returns
     *  A reference to this instance.
     */
    ellipse(x: number, y: number, rx: number, ry: number, a0: number, a1: number, a2: number, ccw = false): this {
        return this.#pushOp("ellipse", x, y, rx, ry, a0, a1, a2, ccw);
    }

    /**
     * Adds a new quadratic bezier subpath starting from the current point.
     *
     * @param cp1x
     *  The X coordinate of the control point.
     *
     * @param cp1y
     *  The Y coordinate of the control point.
     *
     * @param x
     *  The X coordinate of the end point of the line.
     *
     * @param y
     *  The Y coordinate of the end point of the line.
     *
     * @returns
     *  A reference to this instance.
     */
    quadraticCurveTo(cp1x: number, cp1y: number, x: number, y: number): this {
        return this.#pushOp("quadraticCurveTo", cp1x, cp1y, x, y);
    }

    /**
     * Adds a new cubic bezier subpath starting from the current point.
     *
     * @param cp1x
     *  The X coordinate of the first control point.
     *
     * @param cp1y
     *  The Y coordinate of the first control point.
     *
     * @param cp2x
     *  The X coordinate of the second control point.
     *
     * @param cp2y
     *  The Y coordinate of the second control point.
     *
     * @param x
     *  The X coordinate of the end point of the line.
     *
     * @param y
     *  The Y coordinate of the end point of the line.
     *
     * @returns
     *  A reference to this instance.
     */
    bezierCurveTo(cp1x: number, cp1y: number, cp2x: number, cp2y: number, x: number, y: number): this {
        return this.#pushOp("bezierCurveTo", cp1x, cp1y, cp2x, cp2y, x, y);
    }

    /**
     * Adds a new elliptic arc subpath.
     *
     * @param cx
     *  The x start point of the arc.
     *
     * @param cy
     *  The Y start point of the arc.
     *
     * @param rx
     *  The radius of the arc on the X axis.
     *
     * @param ry
     *  The radius of the arc on the Y axis.
     *
     * @param a0
     *  The start angle (radiant) of the arc. The value 0 represents a position
     *  on the positive part of the X axis.
     *
     * @param a1
     *  The end angle (radiant) of the arc. The value 0 represents a position
     *  on the positive part of the X axis.
     *
     * @param [ccw]
     *  Whether to draw the arc counterclockwise from its start position to its
     *  end position. By default, the arc will be drawn clockwise.
     *
     * @returns
     *  A reference to this instance.
     */
    ellipseTo(cx: number, cy: number, rx: number, ry: number, a0: number, a1: number, ccw = false): this {
        const dx = rx * cos(a0);
        const dy = ry * sin(a0);
        return this.#pushOp("save").#pushOp("translate", cx - dx, cy - dy).ellipse(0, 0, rx, ry, 0, a0, a1, ccw).#pushOp("restore");
    }

    /**
     * Closes the current (last) subpath by adding a straight line to its start
     * position, and opens a new subpath at the start position of the current
     * subpath.
     *
     * @returns
     *  A reference to this instance.
     */
    close(): this {
        return this.#pushOp("closePath");
    }

    /**
     * Adds a new subpath consisting of a straight line. After that, opens
     * another new subpath at the starting point of the line.
     *
     * @param x1
     *  The X coordinate of the starting point of the line.
     *
     * @param y1
     *  The Y coordinate of the starting point of the line.
     *
     * @param x2
     *  The X coordinate of the end point of the line.
     *
     * @param y2
     *  The Y coordinate of the end point of the line.
     *
     * @returns
     *  A reference to this instance.
     */
    pushLine(x1: number, y1: number, x2: number, y2: number): this {
        return this.moveTo(x1, y1).lineTo(x2, y2).moveTo(x1, y1);
    }

    /**
     * Adds a new subpath consisting of multiple straight lines. After that,
     * opens another new subpath at the starting point of the line chain.
     *
     * @param points
     *  An arbitrary number of coordinate pairs (x/y) representing the points
     *  of the lines added to the chain.
     *
     * @returns
     *  A reference to this instance.
     */
    pushLineChain(points: Array<Pair<number>>): this;
    pushLineChain(...points: number[]): this;
    // implementation
    pushLineChain(...args: Array<Array<Pair<number>>> | number[]): this {
        if (is.array(args[0])) {
            const points = args[0];
            const [x1, y1] = points[0];
            this.moveTo(x1, y1);
            for (let index = 1, length = points.length; index < length; index += 1) {
                this.lineTo(points[index][0], points[index][1]);
            }
            this.moveTo(x1, y1);
        } else {
            const coords = args as number[];
            const [x1, y1] = coords;
            this.moveTo(x1, y1);
            for (let index = 2, length = coords.length; index < length; index += 2) {
                this.lineTo(coords[index], coords[index + 1]);
            }
            this.moveTo(x1, y1);
        }
        return this;
    }

    /**
     * Adds a new subpath consisting of a closed polygon formed by multiple
     * straight lines. After that, opens another new subpath at the starting
     * point of the polygon.
     *
     * @param points
     *  An arbitrary number of coordinate pairs (x/y) representing the polygon
     *  corner points.
     *
     * @returns
     *  A reference to this instance.
     */
    pushPolygon(points: Array<Pair<number>>): this;
    pushPolygon(...points: number[]): this;
    // implementation
    pushPolygon(...args: Array<Array<Pair<number>>> | number[]): this {
        if (is.array(args[0])) {
            const points = args[0];
            this.moveTo(points[0][0], points[0][1]);
            for (let index = 1, length = points.length; index < length; index += 1) {
                this.lineTo(points[index][0], points[index][1]);
            }
        } else {
            const coords = args as number[];
            this.moveTo(coords[0], coords[1]);
            for (let index = 2, length = coords.length; index < length; index += 2) {
                this.lineTo(coords[index], coords[index + 1]);
            }
        }
        return this.close();
    }

    /**
     * Adds a new subpath consisting of a closed rectangle. After that, opens
     * another new subpath at the starting corner of the rectangle.
     *
     * @param rect
     *  A complete rectangle.
     *
     * @param x
     *  The X coordinate of the starting point of the rectangle.
     *
     * @param y
     *  The Y coordinate of the starting point of the rectangle.
     *
     * @param w
     *  The width of the rectangle.
     *
     * @param h
     *  The height of the rectangle.
     *
     * @returns
     *  A reference to this instance.
     */
    pushRect(rect: RectangleLike): this;
    pushRect(x: number, y: number, w: number, h: number): this;
    // implementation
    pushRect(x: RectangleLike | number, y?: number, w?: number, h?: number): this {
        if (!is.number(x)) { y = x.top; w = x.width; h = x.height; x = x.left; }
        const x2 = x + w!, y2 = y! + h!;
        return this.pushPolygon(x, y!, x2, y!, x2, y2, x, y2);
    }

    /**
     * Adds a new subpath consisting of a complete circle. After that, opens
     * another new subpath at the center point of the circle.
     *
     * @param x
     *  The X coordinate of the center point of the circle.
     *
     * @param y
     *  The Y coordinate of the center point of the circle.
     *
     * @param r
     *  The radius of the circle.
     *
     * @returns
     *  A reference to this instance.
     */
    pushCircle(x: number, y: number, r: number): this {
        return this.moveTo(x + r, y).arc(x, y, r, 0, PI2).moveTo(x, y);
    }

    /**
     * Adds a new subpath consisting of a complete ellipse. After that, opens
     * another new subpath at the center point of the ellipse.
     *
     * @param x
     *  The X coordinate of the center point of the ellipse.
     *
     * @param y
     *  The Y coordinate of the center point of the ellipse.
     *
     * @param rx
     *  The radius of the ellipse on the X axis.
     *
     * @param ry
     *  The radius of the ellipse on the Y axis.
     *
     * @param [a]
     *  The rotation angle (radiant) of the ellipse.
     *
     * @returns
     *  A reference to this instance.
     */
    pushEllipse(x: number, y: number, rx: number, ry: number, a = 0): this {
        const x1 = x + rx * cos(a);
        const y1 = y + rx * sin(a);
        return this.moveTo(x1, y1).ellipse(x, y, rx, ry, a, 0, PI2).moveTo(x, y);
    }

    /**
     * Adds a new subpath consisting of an arc. After that, opens another new
     * subpath at the center point of the arc.
     *
     * @param x
     *  The X coordinate of the center point of the arc.
     *
     * @param y
     *  The Y coordinate of the center point of the arc.
     *
     * @param r
     *  The radius of the arc.
     *
     * @param a1
     *  The angle (radiant) of the arc's start position. The value 0 represents
     *  a position on the positive part of the X axis.
     *
     * @param a2
     *  The angle (radiant) of the arc's end position. The value 0 represents a
     *  position on the positive part of the X axis.
     *
     * @param [ccw]
     *  Whether to draw the arc counterclockwise from its start position to its
     *  end position. By default, the arc will be drawn clockwise.
     *
     * @returns
     *  A reference to this instance.
     */
    pushArc(x: number, y: number, r: number, a1: number, a2: number, ccw = false): this {
        const x1 = x + r * cos(a1);
        const y1 = y + r * sin(a1);
        return this.moveTo(x1, y1).arc(x, y, r, a1, a2, ccw).moveTo(x, y);
    }

    /**
     * Adds a new subpath consisting of a complete diamond. After that, opens
     * another new subpath at the center point of the diamond.
     *
     * @param x
     *  The X coordinate of the center point of the diamond.
     *
     * @param y
     *  The Y coordinate of the center point of the diamond.
     *
     * @param rx
     *  The distance of the diamond on the X axis.
     *
     * @param ry
     *  The distance of the diamond on the Y axis.
     *
     * @param [a]
     *  The rotation angle (radiant) of the diamond.
     *
     * @returns
     *  A reference to this instance.
     */
    pushDiamond(x: number, y: number, rx: number, ry: number, a = 0): this {
        return this.#pushOp("save")
            .#pushOp("translate", x, y)
            .#pushOp("rotate", a)
            .pushPolygon(rx, 0, 0, -ry, -rx, 0, 0, ry, rx, 0)
            .moveTo(0, 0)
            .#pushOp("restore");
    }

    /**
     * Adds a new subpath consisting of a complete triangle. After that, opens
     * another new subpath at the center point of the triangle.
     *
     * @param x
     *  The X coordinate of the top point of the triangle.
     *
     * @param y
     *  The Y coordinate of the top point of the triangle.
     *
     * @param rx
     *  The distance of the triangle on the X axis.
     *
     * @param ry
     *  The distance of the triangle on the Y axis.
     *
     * @param [a]
     *  The rotation angle (radiant) of the triangle.
     *
     * @param [toRight]
     *  The direction of the triangle in the horizontal direction.
     *
     * @returns
     *  A reference to this instance.
     */
    pushTriangle(x: number, y: number, rx: number, ry: number, a = 0, toRight = false): this {
        const sign = toRight ? -1 : 1;
        const doubleX = sign * 2 * rx;
        return this.#pushOp("save")
            .#pushOp("translate", x, y)
            .#pushOp("rotate", a)
            .pushPolygon(0, 0, doubleX, ry, doubleX, -ry)
            .moveTo(0, 0)
            .#pushOp("restore");
    }

    /**
     * Adds a new subpath consisting of a complete stealth arrow. After that,
     * opens another new subpath at the center point of the stealth arrow.
     *
     * @param x
     *  The X coordinate of the center point of the stealth arrow.
     *
     * @param y
     *  The Y coordinate of the center point of the stealth arrow.
     *
     * @param rx
     *  The distance of the stealth arrow on the X axis.
     *
     * @param ry
     *  The distance of the stealth arrow on the Y axis.
     *
     * @param [a]
     *  The rotation angle (radiant) of the stealth arrow.
     *
     * @param [toRight]
     *  The direction of the stealth arrow in the horizontal direction.
     *
     * @returns
     *  A reference to this instance.
     */
    pushStealth(x: number, y: number, rx: number, ry: number, a = 0, toRight = false): this {
        const sign = toRight ? -1 : 1;
        const doubleX = sign * 2 * rx; // the same as triangle, but not as long as in arrow
        const stealthPoint = doubleX / 2; // the same as triangle, but not as long as in arrow
        return this.#pushOp("save")
            .#pushOp("translate", x, y)
            .#pushOp("rotate", a)
            // TODO: Should be an arc (?), stealth point is half of the width
            .pushPolygon(0, 0, doubleX, ry, stealthPoint, 0, doubleX, -ry)
            .moveTo(0, 0)
            .#pushOp("restore");
    }

    /**
     * Adds a new subpath consisting of an arrow. After that, opens another new
     * subpath at the arrow head.
     *
     * @param x
     *  The X coordinate of the arrow head.
     *
     * @param y
     *  The Y coordinate of the arrow head.
     *
     * @param rx
     *  The horizontal length of the arrow head on the X axis. This will be
     *  used twice, because also the vertically specified value is
     *  automatically used twice (above and below the X axis).
     *
     * @param ry
     *  The vertical length of the arrow head on the Y axis. The specified
     *  value is used above the X axis and below the X axis.
     *
     * @param [a]
     *  The rotation angle (radiant) of the arrow.
     *
     * @param [toRight]
     *  The direction of the arrow in the horizontal direction.
     *
     * @returns
     *  A reference to this instance.
     */
    pushArrow(x: number, y: number, rx: number, ry: number, a = 0, toRight = false): this {
        const sign = toRight ? -1 : 1;
        const doubleX = sign * 2 * rx;
        const increasedY = 1.5 * ry;  // increasing the arrow a little bit compared to x axis value
        return this.#pushOp("lineCap", "round") // round line cap at arrow, before "save" and "restore"!
            .#pushOp("save")
            .#pushOp("translate", x, y)
            .#pushOp("rotate", a)
            .pushLineChain(doubleX, increasedY, 0, 0, doubleX, -increasedY)
            .moveTo(0, 0)
            .#pushOp("restore");
    }

    // private methods --------------------------------------------------------

    /**
     * Pushes a single path operation to the internal operations array.
     *
     * @param name
     *  The name of the path operation (the name of the related native method
     *  of the rendering context).
     *
     * @param [args]
     *  All arguments to be passed to the native context method.
     *
     * @returns
     *  A reference to this instance.
     */
    #pushOp<KT extends keyof PathOperation>(name: KT, ...args: Parameters<PathOperation[KT]>): this {
        this.ops.push(target => (target[name] as AnyFunction)(...args));
        return this;
    }
}
