/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { DObject } from "@/io.ox/office/tk/object/dobject";

// class CanvasContextBase ====================================================

/**
 * Common base class for classes wrapping a canvas rendering context with
 * global translation.
 */
export class CanvasContextBase extends DObject {

    // properties -------------------------------------------------------------

    /**
     * The raw DOM rendering context wrapped by this instance.
     */
    readonly ctx: CanvasRenderingContext2D;

    protected readonly x0: number;
    protected readonly y0: number;

    // constructor ------------------------------------------------------------

    /**
     * @param context2d
     *  The wrapped rendering context of a canvas element.
     *
     * @param x0
     *  The global translation on the X axis that will be applied to all path
     *  operations.
     *
     * @param y0
     *  The global translation on the Y axis that will be applied to all path
     *  operations.
     */
    protected constructor(context2d: CanvasRenderingContext2D, x0: number, y0: number) {
        super();
        this.ctx = context2d;
        this.x0 = x0;
        this.y0 = y0;
    }
}
