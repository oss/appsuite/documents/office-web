/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun } from "@/io.ox/office/tk/algorithms";
import { YES_LABEL, NO_LABEL, createLabel } from "@/io.ox/office/tk/dom";
import { type BaseDialogConfig, OK_ACTION, BaseDialog } from "@/io.ox/office/tk/dialog/basedialog";

// types ======================================================================

/**
 * Specifies the behavior of the dialog regarding the "No" button, an optional
 * additional "Cancel" button, and automatic closing via the `close` method.
 */
export enum QueryDialogButtonMode {

    /**
     * Default mode. The dialog will display a "Yes" and "No" button.
     *
     * - The "Yes" button will fulfil the dialog promise with the value `true`.
     * - The "No" button will reject the dialog promise.
     * - The method `close` will reject the dialog promise.
     */
    NO_BUTTON_REJECT = "noButtonReject",

    /**
     * The dialog will display a "Yes" and "No" button.
     *
     * - The "Yes" button will fulfil the dialog promise with the value `true`.
     * - The "No" button will fulfil the dialog promise with the value `false`.
     * - The method `close` will reject the dialog promise.
     */
    NO_BUTTON_FULFIL = "noButtonFulfil",

    /**
     * The dialog will display a "Yes", "No", and "Cancel" button.
     *
     * - The "Yes" button will fulfil the dialog promise with the value `true`.
     * - The "No" button will fulfil the dialog promise with the value `false`.
     * - The "Cancel" button will reject the dialog promise.
     * - The method `close` will reject the dialog promise.
     */
    CANCEL_BUTTON = "cancelButton"
}

/**
 * Configuration options for a `QueryDialog` instance.
 */
export interface QueryDialogConfig extends BaseDialogConfig {

    /**
     * The message text shown in the dialog body element. If omitted, an empty
     * query dialog will be created which can be filled with more customized
     * content afterwards.
     */
    message?: string;

    /**
     * Behavior and appearance of the action buttons "No" and "Cancel". Default
     * value is `NO_BUTTON_REJECT`.
     */
    buttonMode?: QueryDialogButtonMode;

    /**
     * The label of the "No" button. If omitted, the button will be created
     * with the translated default label "No".
     */
    noLabel?: string;
}

// class QueryDialog ==========================================================

/**
 * Creates a simple dialog with a message text and "Yes" and "No" buttons.
 * Optionally, the dialog may contain an additional "Cancel" button next to the
 * "Yes" and "No" buttons.
 *
 * By default, the promise returned by the method `show()` of will fulfil with
 * `true` when pressing the "Yes" button, and rejected when pressing the "No"
 * button.
 *
 * If the dialog has been opened with an additional "Cancel" button (with the
 * configuration option `cancel`), the promise returned by the method `show()`
 * will behave differently: When pressing the "Yes" button, the promise will
 * fulfil with `true`. When pressing the "No" button, the promise will fulfil
 * with `false`. Otherwise (dialog canceled), the promise will reject.
 *
 * @template ConfT
 *  Type of configuration options that a subclass allows to pass to the
 *  constructor.
 */
export class QueryDialog<ConfT extends QueryDialogConfig = QueryDialogConfig> extends BaseDialog<boolean, ConfT> {

    /**
     * @param config
     *  Configuration options that control the appearance and behavior of the
     *  dialog. Supports all options of the base class `BaseDialog`. The
     *  default value of the option `okLabel` will be changed to the translated
     *  string "Yes", and the default value of the option `cancelLabel` will be
     *  changed to the translated string "No".
     */
    public constructor(config: NoInfer<ConfT>) {

        // the label text for the "No" button
        const noLabel = config.noLabel || NO_LABEL;
        // action button mode
        const buttonMode = config.buttonMode ?? QueryDialogButtonMode.NO_BUTTON_REJECT;

        // configuration for standard Cancel button
        // - NO_BUTTON_REJECT: create Cancel button with label "No", do not create extra No button
        // - NO_BUTTON_FULFIL: do not create a Cancel button
        // - CANCEL_BUTTON: create standard Cancel button, and an additional No button
        const cancelOption = (buttonMode === QueryDialogButtonMode.NO_BUTTON_REJECT) ? { cancelLabel: noLabel } :
            (buttonMode === QueryDialogButtonMode.NO_BUTTON_FULFIL) ? { cancelLabel: null } : null;

        // base constructor
        super({
            width: 400,
            okLabel: YES_LABEL,
            ...config,
            ...cancelOption
        });

        // create a <label> element with the message text
        if (config.message) {
            this.bodyNode.appendChild(createLabel({ label: config.message }));
        }

        // return true for OK (Yes) button
        this.setOkHandler(fun.true);

        // add an own "No" action button that will fulfil with value `false`
        if (buttonMode !== QueryDialogButtonMode.NO_BUTTON_REJECT) {
            this.createActionButton("no", noLabel, {
                insertBefore: OK_ACTION,
                handler: fun.false
            });
        }
    }
}
