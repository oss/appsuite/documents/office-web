/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import gt from 'gettext';

import FolderAPI from '$/io.ox/core/folder/api';

import { FlatFolderPicker, FolderPicker } from '@/io.ox/office/tk/utils/driveutils';
import { BaseDialog } from '@/io.ox/office/tk/dialog/basedialog';

// class FolderPickerDialog ===================================================

/**
 * A dialog that allows to pick a Drive folder.
 *
 * @param {object} [config]
 *  Configuration options that control the appearance and behavior of the
 *  dialog. Supports all options of the base class `BaseDialog`, of the
 *  classes `FolderPicker` and `FlatFolderPicker`, and the following
 *  options:
 *  - {boolean} [config.flatPicker=false]
 *    Whether to create a `FlatFolderPicker` instance instead of a regular
 *    `FolderPicker` instance.
 *  - {boolean} [config.createFolderButton=false]
 *    If set to `true`, the dialog will provide an additional push button
 *    that allows to create new subfolders.
 */
export default class FolderPickerDialog extends BaseDialog {

    constructor(config) {

        // base constructor
        super(config);

        // the folder picker list control embedded in this dialog
        const folderPicker = this._folderPicker = this.member(
            config?.flatPicker ? new FlatFolderPicker(config) : new FolderPicker(config)
        );

        // add the folder picker to the body of this dialog
        this.$bodyNode.append(folderPicker.getNode());

        // add a "create folder" button
        if (config?.createFolderButton) {
            this.createActionButton('create', gt('Create folder'), {
                handler() { return this._createFolder(); },
                validator() { return this.getFolderId(); },
                alignLeft: true,
                keepOpen: true
            });
        }

        // validate after changing the folder selection
        this.addOkValidator(() => !!this.getFolderId());
        this.validateOnEvent(folderPicker, 'change');

        // pass the folder identifier as result for the OK button
        this.setOkHandler(() => this.getFolderId());
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the folder picker control contained in this dialog.
     *
     * @returns {FolderPicker|FlatFolderPicker}
     *  The folder picker control contained in this dialog.
     */
    getFolderPicker() {
        return this._folderPicker;
    }

    /**
     * Returns the identifier of the selected folder.
     *
     * @returns {String}
     *  The identifier of the selected folder.
     */
    getFolderId() {
        return this._folderPicker.getFolderId();
    }

    // private methods --------------------------------------------------------

    /*private*/ async _createFolder() {

        // request and open the "Create Folder" dialog
        try {
            const { default: addAction } = await import('$/io.ox/core/folder/actions/add');

            const folderId = this.getFolderId();
            const folderModule = FolderAPI.pool.getModel(folderId).get('module');
            const newFolder = await addAction(folderId, { module: folderModule });
            if (!newFolder?.id) { return; }

            // select the new folder
            const folderPicker = this._folderPicker;
            folderPicker.selectFolder(newFolder.id);
            // add additonal 15ms to tree nodes debounced onSort handler
            await new Promise(resolve => {
                window.setTimeout(() => {
                    folderPicker.selectFolder(newFolder.id);
                    resolve();
                }, 15);
            });
        } catch { }
    }
}
