/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from '$/jquery';
import gt from 'gettext';

import { getStringOption } from '@/io.ox/office/tk/utils';
import FolderPickerDialog from '@/io.ox/office/tk/dialog/folderpickerdialog';

// class SaveAsDialog =====================================================

/**
 * A generic dialog to save a file in a Drive folder. This dialog provides
 * a folder picker and a simple text input.
 *
 * @param {Object} [config]
 *  Optional parameters that control the appearance and behavior of the
 *  dialog. Supports all options also supported by the base class
 *  FolderPickerDialog. Additionally, the following options are supported:
 *  - {string} [config.value=""]
 *    The initial value of the text field.
 *  - {string} [config.placeholder=""]
 *    The place-holder text that will be shown in the empty text field.
 */
export default class SaveAsDialog extends FolderPickerDialog {

    constructor(config) {

        // base constructor
        super({
            okLabel: gt('Save'),
            classes: 'save-as-dialog',
            height: 350,
            focus: "#save-as-filename",
            ...config,
            showReadOnly: true
        });

        // label for the save as input node
        const labelNode = $('<label for="save-as-filename">').text(gt('File name'));

        // the text field control
        this._inputNode = $('<input type="text" id="save-as-filename" class="form-control" tabindex="0">').attr({
            placeholder: getStringOption(config, 'placeholder', ''),
            value: getStringOption(config, 'value', '')
        });

        // add the folder tree and text field
        this.$bodyNode.append(labelNode, this._inputNode);

        // focus and select the text in the input field
        this.getFolderPicker().on('select:item', () => this.grabFocus());

        // disable OK button if no file name is available
        this.addOkValidator(() => !!this.getFileName());

        // resolve dialog promise with folder identifier and file name
        this.setOkHandler(() => ({ folderId: this.getFolderId(), fileName: this.getFileName() }));
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the file name contained in the text input element.
     *
     * @returns {string}
     *  The file name contained in the text input element.
     */
    getFileName() {
        return $.trim(this._inputNode.val());
    }
}
