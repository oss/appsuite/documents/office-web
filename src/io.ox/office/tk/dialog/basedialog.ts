/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import ModalDialog, { type ModelDialogFocusOptions } from "$/io.ox/backbone/views/modal";

import { type Point, is, math, fun, map, jpromise, debug } from "@/io.ox/office/tk/algorithms";
import type { DocsEmitter, AllEventsEmitter, AnyEmitter } from "@/io.ox/office/tk/events";
import { globalEvents } from "@/io.ox/office/tk/events";
import type { ElementBaseOptions } from "@/io.ox/office/tk/dom";
import {
    Rectangle, OK_LABEL, CANCEL_LABEL, SMALL_DEVICE,
    getWindowWidth, addDeviceMarkers, setElementClasses,
    createLabel, enableElement, getFocus, clearBrowserSelection,
    hasKeyCode, matchKeyCode, isEscapeKey
} from "@/io.ox/office/tk/dom";

import type { Control } from "@/io.ox/office/tk/form";
import { EObject } from "@/io.ox/office/tk/objects";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import type { Group } from "@/io.ox/office/tk/control/group";

import "@/io.ox/office/tk/dialog/style.less";

// re-exports =================================================================

export * from "@/io.ox/office/tk/dom/labels";

// types ======================================================================

/**
 * Configuration options for a `BaseDialog` instance.
 */
export interface BaseDialogConfig extends ElementBaseOptions, ModelDialogFocusOptions {

    /**
     * The width of the dialog, in pixels. default value is 500 pixels.
     */
    width?: number;

    /**
     * The title of the dialog window that will be shown in a larger font.
     */
    title: string;

    /**
     * The label of the primary button that triggers the default action of the
     * dialog. If omitted, the button will be created with the translated
     * default label "OK".
     */
    okLabel?: string;

    /**
     * The label of the cancel button that closes the dialog without performing
     * any action. If omitted, the button will be created with the translated
     * default label "Cancel". If explicitly set to `null`, the Cancel button
     * will be hidden.
     */
    cancelLabel?: string | null;

    /**
     * If set to `true`, the dialog will be made movable by dragging its header
     * area. If the dialog has a unique identifier (option `id`), the position
     * of the dialog will be saved locally (during a browser session), and will
     * be restored when opening the dialog again. Default value is `false`.
     */
    movable?: boolean;

    /**
     * If set to `true`, the action buttons in the footer area will not be
     * expanded and stacked on small devices, but will be rendered side-by-side
     * like on large devices. This helps to gain more space for the dialog
     * contents in the dialog body area. Default value is `false`.
     */
    smallMobileFooter?: boolean;
}

/**
 * The callback function to be invoked when validating a form control in a
 * modal dialog.
 *
 * Will be called in the context of the dialog instance. If the function
 * returns `false` or throws, the form control will be disabled.
 */
export type ControlValidatorFn = () => boolean;

/**
 * Configuration for a form control that will be rendered in the dialog.
 */
export interface RenderControlOptions {

    /**
     * If set to a string, a `<label>` element will be rendered in front of the
     * rendered form control.
     */
    label?: string;

    /**
     * A validator callback function. This option is a convenience shortcut
     * that calls the method `BaseDialog#addControlValidator` for the form
     * control internally.
     */
    validator?: ControlValidatorFn;
}

/**
 * The callback function to be invoked when activating an action button of a
 * dialog (i.e., a button in its footer area).
 *
 * Will be called in the context of the dialog instance. Receives the action
 * key as first parameter (to be able to reuse the same handler function for
 * similar actions). Must return the final result of the dialog. May return a
 * promise that will fulfil or reject later. According to the result of the
 * callback function, the promise returned by the method `BaseDialog#show` will
 * fulfil or reject with the result value.
 */
export type ActionHandlerFn<ValT = unknown> = (action: string) => MaybeAsync<ValT>;

/**
 * Optional settings for the handler of an action button.
 */
export interface ActionHandlerOptions {

    /**
     * Specifies when to keep open the dialog after activating an action
     * button.
     *
     * - By default (value `false`), the dialog will be closed when clicking
     *   the action button (after the action handler has finished).
     * - If set to `true`, the dialog will be kept open regardless of the
     *   result of the action handler (success or failure).
     * - If set to the string "done", the dialog will be kept open if the
     *   action succeeds (synchronous return value, or resolved promise).
     * - If set to the string "fail", the dialog will be kept open if the
     *   action fails (exception thrown, or rejected promise).
     */
    keepOpen?: boolean | "done" | "fail";
}

/**
 * A callback function to be invoked when validating the action buttons in the
 * footer area).
 *
 * Will be called in the context of the dialog instance. Receives the action
 * key of the button as first parameter. If the function returns `false` or
 * throws, the action button will be disabled.
 */
export type ActionValidatorFn = (action: string) => boolean;

/**
 * Configuration for an action button shown in the footer area of a dialog.
 */
export interface ActionButtonOptions extends ActionHandlerOptions {

    /**
     * If set to `true`, the button will be inserted at the left border of the
     * footer. By default, the button will be inserted at the right border
     * behind all existing buttons (except when using the option
     * `insertBefore`).
     */
    alignLeft?: boolean;

    /**
     * The action key of an existing button that should be located right of the
     * new button.
     */
    insertBefore?: string;

    /**
     * The appearance of the button. Must be one of the following values:
     *
     * - "primary": The button will appear as the primary (OK) button.
     * - "success": The button will appear as a success button (green).
     * - "warning": The button will appear as a warning button (yellow).
     * - "danger": The button will appear as a danger button (red).
     *
     * If omitted, the button will appear with its default style.
     */
    buttonStyle?: "primary" | "success" | "warning" | "danger";

    /**
     * An action handler callback function. This option is a convenience
     * shortcut that calls the method `BaseDialog#setActionHandler` for the
     * button action internally.
     */
    handler?: ActionHandlerFn;

    /**
     * A validator callback function. This option is a convenience shortcut
     * that calls the method `BaseDialog#addActionValidator` for the button
     * action internally.
     */
    validator?: ActionValidatorFn;
}

/**
 * Type mapping for the events emitted by `BaseDialog` instances.
 */
export interface BaseDialogEventMap {

    /**
     * Will be emitted after the dialog has been displayed.
     */
    show: [];

    /**
     * Will be emitted after the dialog has been closed.
     */
    close: [];
}

// private types --------------------------------------------------------------

// map entry for form control validators
interface ControlValidatorEntry {
    fns: ControlValidatorFn[];
    control: Control<any>;
}

// map entry for action button validators
interface ActionValidatorEntry {
    fns: ActionValidatorFn[];
    action: string;
}

// map entry for action button handlers
interface ActionHandlerEntry {
    fn: ActionHandlerFn;
    options?: ActionHandlerOptions;
}

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted when a modal dialog (an instance of `BaseDialog` has
         * been constructed (*not* shown yet).
         */
        "dialog:construct": [dialog: BaseDialog, config: BaseDialogConfig];
    }
}

// constants ==================================================================

/**
 * The action key of the primary button (the OK button).
 */
export const OK_ACTION = "ok";

/**
 * The action key of the Cancel button.
 */
export const CANCEL_ACTION = "cancel";

// private constants ----------------------------------------------------------

/**
 * Cache for positions of movable dialogs, mapped by dialog DOM identifiers.
 */
const positionCache = new Map<string, Rectangle>();

// private functions ==========================================================

/**
 * Returns a dialog width suitable for the current device type, and for the
 * specified desired width.
 *
 * @param config
 *  Configuration options passed to the constructor of a dialog. The value of
 *  the option `width` will be used as preferred dialog width.
 *
 * @returns
 *  The effective width of the dialog, in pixels. Will not be greater than 95%
 *  of the current screen width. On small devices, the dialog width returned by
 *  this function will be fixed to 95% of the screen width, regardless of the
 *  passed width.
 */
function getBestDialogWidth(config: Opt<BaseDialogConfig>): number {
    const width = config?.width || 500;
    const maxWidth = Math.round(getWindowWidth() * 0.95);
    return SMALL_DEVICE ? maxWidth : Math.min(width, maxWidth);
}

// public functions ===========================================================

/**
 * Returns the root node of the modal dialog that is currently open. In case
 * multiple dialogs are open (stacked), the root node of the top-most dialog
 * will be returned.
 *
 * @returns
 *  The root node of the modal dialog that is currently open. If no modal
 *  dialog is currently open, `undefined` will be returned instead.
 */
export function getActiveDialogNode(): Opt<HTMLElement> {
    const nodeList = document.querySelectorAll(".modal .modal-content");
    return nodeList[nodeList.length - 1] as Opt<HTMLElement>;
}

/**
 * Returns whether a modal dialog is currently open.
 *
 * @returns
 *  Whether a modal dialog is currently open.
 */
export function isDialogOpen(): boolean {
    return !!getActiveDialogNode();
}

// class BaseDialog ===========================================================

/**
 * Base class for modal dialogs.
 *
 * Conceptually, a dialog instance is a one-way object. After creating an
 * instance, it can be shown *once*, and will destroy itself afterwards.
 *
 * @template ValT
 *  Type of the result value returned by the `show` method.
 *
 * @template ConfT
 *  Type of configuration options that a subclass allows to pass to the
 *  constructor.
 */
export class BaseDialog<ValT = unknown, ConfT extends BaseDialogConfig = BaseDialogConfig> extends EObject<BaseDialogEventMap> {

    // properties -------------------------------------------------------------

    /**
     * The configuration passed to the constructor.
     */
    protected readonly config: ConfT;

    /**
     * The root node of the dialog.
     */
    protected readonly rootNode: HTMLElement;

    /**
     * The root node of the dialog, as JQuery collection.
     */
    protected readonly $rootNode: JQuery;

    /**
     * The root node of the dialog's header area containing the dialog title.
     */
    protected readonly headerNode: HTMLElement;

    /**
     * The root node of the dialog's header area containing the dialog title,
     * as JQuery collection.
     */
    protected readonly $headerNode: JQuery;

    /**
     * The root node of the dialog's body area taking all form controls.
     */
    protected readonly bodyNode: HTMLElement;

    /**
     * The root node of the dialog's body area taking all form controls, as
     * JQuery collection.
     */
    protected readonly $bodyNode: JQuery;

    /**
     * The root node of the dialog's footer area containing the action buttons.
     */
    protected readonly footerNode: HTMLElement;

    /**
     * The root node of the dialog's footer area containing the action buttons,
     * as JQuery collection.
     */
    protected readonly $footerNode: JQuery;

    // the core dialog implementation
    readonly #dialog: ModalDialog;

    // the dialog frame node used for move tracking (parent of content node)
    readonly #$moveNode: JQuery;

    // the dialog content node (with header/body/footer)
    readonly #$contentNode: JQuery;

    // validator callbacks for form controls
    readonly #controlValidators = new Map<Control<any>, ControlValidatorEntry>();

    // validator callbacks for action buttons, mapped by action key
    readonly #actionValidators = new Map<string, ActionValidatorEntry>();

    // action button handler callbacks, mapped by action key
    readonly #actionHandlers = new Map<string, ActionHandlerEntry>();

    // pending promise representing the life cycle of this dialog
    readonly #deferred: JDeferred<ValT>;

    // whether the dialog is busy while running an action handler
    #busy = false;

    // constructor ------------------------------------------------------------

    /**
     * @param config
     *  Configuration options that control the appearance and behavior of the
     *  modal dialog.
     */
    protected constructor(config: NoInfer<ConfT>) {
        super();

        // create the core dialog
        const dialog = this.#dialog = new ModalDialog({
            title: config.title,
            width: getBestDialogWidth(config),
            focus: config.focus,
            async: true,
            previousFocus: false // core dialog shall not try to restore old focus
        });

        // store configuration for use in methods of this class and subclasses
        this.config = config;

        // important DOM nodes of the dialog
        this.rootNode = dialog.el;
        this.$rootNode = dialog.$el;
        this.headerNode = dialog.$header[0];
        this.$headerNode = dialog.$header;
        this.bodyNode = dialog.$body[0];
        this.$bodyNode = dialog.$body;
        this.footerNode = dialog.$footer[0];
        this.$footerNode = dialog.$footer;

        // additional elements needed for movable dialogs
        this.#$moveNode = dialog.$el.children(".modal-dialog");
        this.#$contentNode = this.#$moveNode.children(".modal-content");

        // create the pending promise representing the lifetime
        this.#deferred = jpromise.deferred();

        // add own CSS markers to the root node of the dialog
        this.$rootNode.addClass("io-ox-office-main io-ox-office-dialog");
        setElementClasses(this.rootNode, config);
        addDeviceMarkers(this.rootNode);
        this.rootNode.classList.toggle("small-mobile-footer", !!config.smallMobileFooter);

        // create the Cancel button (unless `null` has been passed)
        if (config.cancelLabel !== null) {
            this.createActionButton(CANCEL_ACTION, config.cancelLabel || CANCEL_LABEL);
        }

        // create the default action button (OK button)
        this.createActionButton(OK_ACTION, config.okLabel || OK_LABEL, { buttonStyle: "primary" });

        // bug 32244: handle explicit "dialog.close()" calls
        dialog.on("close", () => {
            this.close = fun.undef; // prevent recursive calls from promise callbacks
            this.#deferred.reject("close");
            this.trigger("close");
            this.destroy();
        });

        // handle action button clicks in the dialog footer
        dialog.on("action", (action: string) => this.#actionButtonHandler(action));

        // clean-up after closing the dialog
        this.#deferred.always(() => this.close());

        // emit a global event for generic processing for the dialog
        globalEvents.emit("dialog:construct", this, config);
    }

    // public methods ---------------------------------------------------------

    show(): JPromise<ValT> {

        // Bug 41397: When focus leaves a node with browser selection (e.g. a clipboard
        // node, or a content-editable node), the browser selection MUST be destroyed.
        // Otherwise, all keyboard events bubbling to the document body will cause to
        // focus that previous element having the browser selection regardless whether
        // it is actually focused. This may cause for example to be able to edit a
        // document while a modal dialog is open.
        clearBrowserSelection();

        // listen to key events for additional keyboard handling
        this.bodyNode.addEventListener("keydown", event => {
            if (hasKeyCode(event, "ENTER")) {
                this.#inputEnterHandler();
                event.preventDefault();
                event.stopPropagation();
            }
        });

        // register global key handler in case the browser focus leaves the dialog frame
        this.listenTo(document, "keydown", this.#globalKeyDownHandler);

        // render the dialog frame
        this.#dialog.open();

        // initialize movable dialogs (not when core dialog has been maximized on small devices)
        if (this.config.movable && !this.rootNode.classList.contains("maximize")) {
            this.rootNode.classList.add("movable");

            // restore saved position of the dialog root node
            const savedPos = this.#getSavedPosition();
            if (savedPos) { this.#moveFrameNode(savedPos); }

            // initialize tracking observer
            let startPos!: Rectangle;
            const observer = this.member(new TrackingObserver({
                start:  () => { startPos = this.#getFramePosition(); },
                move:   record => this.#moveFrameNode(startPos, record.offset),
                end:    () => { if (this.config.id) { positionCache.set(this.config.id, this.#getFramePosition()); } },
                cancel: () => this.#moveFrameNode(startPos)
            }));

            // start observing the header element
            this.headerNode.style.cursor = "move";
            observer.observe(this.headerNode);

            // keep dialog inside screen
            this.listenTo(window, "resize", () => this.#moveFrameNode());
        }

        // initialize action button states
        this.validate();

        // notify listeners of this dialog
        this.trigger("show");

        // set focus to an element when dialog is visible, and all "show" handlers are done
        if (this.config.focus) {
            this.setTimeout(() => this.grabFocus(), 0);
        }

        // return the own promise that will be resolved/rejected after finishing the dialog
        return this.#deferred.promise();
    }

    /**
     * Closes and destroys this dialog.
     */
    close(): void {
        // closing the dialog frame will trigger the "close" event which causes to destroy this instance
        this.#dialog.close();
    }

    // protected methods ------------------------------------------------------

    /**
     * Takes ownership of the passed form control, renders it by calling its
     * `render()` method, and appends its rendered DOM structure to the passed
     * parent container element.
     *
     * @param parent
     *  The parent element to insert the new form control into.
     *
     * @param control
     *  The form control instance. MUST BE a new instance, that has neither
     *  been rendered yet, nor has passed its ownership to some instance.
     *
     * @param [options]
     *  Optional parameters.
     */
    protected renderControl(parent: HTMLElement, control: Control<any>, options?: RenderControlOptions): void {

        // render a leading `<label>` element
        if (options?.label) {
            parent.append(createLabel({ label: options.label, for: control.id }));
        }

        // take ownership of the control
        this.member(control);

        // render the control's DOM structure, and insert it into the parent element
        parent.append(control.render());

        // register control validator
        if (options?.validator) {
            this.addControlValidator(control, options.validator);
        }

        // validate on "input" events, triggered without model change (e.g. typing in text fields)
        this.validateOnEvent(control, ["change", "input"]);
    }

    /**
     * Adds a validator predicate callback for the specified form control. If
     * at least one of the registered validators fails, the form control will
     * be disabled.
     *
     * All validators will be executed when calling method `validate`, or when
     * an event registered with `validateOnEvent` or `validateOnAllEvents` has
     * fired.
     *
     * @param control
     *  The form control to bind the validator to.
     *
     * @param validatorFn
     *  The validator predicate function. If it returns `false` or throws, the
     *  associated form control will bew disabled. Multiple validators may be
     *  added for the same name.
     */
    protected addControlValidator(control: Control<any>, validatorFn: ControlValidatorFn): void {
        map.update(this.#controlValidators, control, entry => {
            (entry ??= { fns: [], control }).fns.push(validatorFn);
            return entry;
        });
    }

    /**
     * Creates an action button and inserts it into the footer area of this
     * dialog.
     *
     * @param action
     *  The action key for the button. Can be any non-empty string but the keys
     *  of an existing action button, or of the internal action "close".
     *
     * @param label
     *  The caption label to be inserted into the button.
     *
     * @param [options]
     *  Optional settings for the action button.
     *
     * @returns
     *  The new `<button>` element.
     */
    protected createActionButton(action: string, label: string, options?: ActionButtonOptions): HTMLButtonElement {

        // check validity of passed action key
        if (!action || ["open", "show", "close", "action"].includes(action) || this.getActionButton(action)) {
            throw new RangeError(`BaseDialog: invalid action button key "${action}"`);
        }

        // add the button using base class functionality (the method does not return the button!)
        const alignLeft = options?.alignLeft;
        const className = `btn-${options?.buttonStyle || "default"}`;
        if (alignLeft) {
            this.#dialog.addAlternativeButton({ action, label, className });
        } else {
            this.#dialog.addButton({ action, label, className });
        }

        // fetch the new button from the dialog
        const actionButton = this.getActionButton(action)!;

        // move the button in front of another button, or append new button to the right border
        const insertBefore = options?.insertBefore;
        const nextButton = insertBefore ? this.getActionButton(insertBefore) : undefined;
        if (nextButton) {
            nextButton.parentNode!.insertBefore(actionButton, nextButton);
        } else if (!alignLeft) {
            this.$footerNode.append(actionButton);
        }

        // register action handler
        if (options?.handler) {
            this.setActionHandler(action, options.handler, options);
        }

        // register action validator
        if (options?.validator) {
            this.addActionValidator(action, options.validator);
        }

        return actionButton;
    }

    /**
     * Returns the button element from the footer of this dialog bound to the
     * specified action.
     *
     * @param action
     *  The key of the action the button is bound to.
     *
     * @returns
     *  The specified `<button>` element.
     */
    protected getActionButton(action: string): Opt<HTMLButtonElement> {
        return this.$footerNode.find<HTMLButtonElement>(`[data-action="${action}"]`)[0];
    }

    /**
     * Returns the OK button of this dialog.
     *
     * @returns
     *  The `<button>` element of the OK button.
     */
    protected getOkButton(): HTMLButtonElement {
        return this.getActionButton(OK_ACTION)!;
    }

    /**
     * Registers a callback function that will be invoked when pressing the
     * button with the specified action key.
     *
     * @param action
     *  The key of the action the callback function is bound to.
     *
     * @param handlerFn
     *  The callback function that will invoked when the action button has been
     *  activated.
     *
     * @param [options]
     *  Optional parameters.
     */
    protected setActionHandler(action: string, handlerFn: ActionHandlerFn, options?: ActionHandlerOptions): void {
        this.#actionHandlers.set(action, { fn: handlerFn, options });
    }

    /**
     * Registers a callback function that will be invoked when pressing the
     * primary button (the OK button) of the dialog.
     *
     * @param handlerFn
     *  The callback function that will invoked when the primary button has
     *  been activated.
     *
     * @param [options]
     *  Optional parameters.
     */
    protected setOkHandler(handlerFn: ActionHandlerFn<ValT>, options?: ActionHandlerOptions): void {
        this.setActionHandler(OK_ACTION, handlerFn, options);
    }

    /**
     * Registers a callback function that will be invoked when pressing the
     * Cancel button of the dialog. The dialog cannot be kept open when
     * pressing the Cancel button.
     *
     * @param handlerFn
     *  The callback function that will be invoked when the Cancel button has
     *  been activated.
     */
    protected setCancelHandler(handlerFn: ActionHandlerFn): void {
        this.setActionHandler(CANCEL_ACTION, handlerFn);
    }

    /**
     * Adds a validator predicate for the specified action button. If at least
     * one of the registered validators fails, the respective action button
     * will be disabled.
     *
     * All validators will be executed when calling method `validate`, or when
     * an event registered with `validateOnEvent` or `validateOnAllEvents` has
     * fired.
     *
     * @param action
     *  The key of the action button the validator is bound to.
     *
     * @param validatorFn
     *  The validator predicate function.  If it returns `false` or throws, the
     *  associated form control will bew disabled. Multiple validators may be
     *  added for the same action.
     */
    protected addActionValidator(action: string, validatorFn: ActionValidatorFn): void {
        map.update(this.#actionValidators, action, entry => {
            (entry ??= { fns: [], action }).fns.push(validatorFn);
            return entry;
        });
    }

    /**
     * Adds a validator predicate for the primary button (the OK button). If at
     * least one of the registered validators fails, the primary button will be
     * disabled.
     *
     * @param validatorFn
     *  The validator predicate.
     */
    protected addOkValidator(validatorFn: ActionValidatorFn): void {
        this.addActionValidator(OK_ACTION, validatorFn);
    }

    /**
     * Runs all registered validators, and updates the state of all action
     * buttons.
     */
    protected validate(): void {

        // validate all registered form controls
        for (const { control, fns } of this.#controlValidators.values()) {
            control.enabled = fun.try(() => fns.every(fn => fn.call(this)), false);
        }

        // validate all action buttons
        for (const { action, fns } of this.#actionValidators.values()) {
            const button = this.getActionButton(action);
            if (button) {
                const enabled = fun.try(() => fns.every(fn => fn.call(this, action)), false);
                enableElement(button, enabled);
            }
        }
    }

    /**
     * Registers an event handler at the specified event emitter that executes
     * all registered control validators.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param type
     *  The type name of an event to start listening to, or an array of event
     *  names.
     */
    protected validateOnEvent<E>(emitter: AnyEmitter<E>, type: OrArray<keyof E>): void {
        this.listenTo(emitter as DocsEmitter<E>, type, this.validate);
    }

    /**
     * Registers an all-events handler at the specified event emitter that
     * executes all registered control validators.
     *
     * @param emitter
     *  The event emitter to start listening to.
     */
    protected validateOnAllEvents<E>(emitter: AllEventsEmitter<E>): void {
        this.listenToAllEvents(emitter as DocsEmitter<E>, this.validate);
    }

    /**
     * Sets the browser focus to the default focus element of the dialog.
     */
    protected grabFocus(): void {
        this.#dialog.setFocus(this.config);
    }

    /**
     * Listens to "group:commit" events at the passed form control, and closes
     * this dialog, if the control event originates from an ENTER key event.
     *
     * @param control
     *  The form control to be listened to, e.g. a text field.
     */
    protected registerEnterGroup(control: Group): void {
        this.listenTo(control, "group:commit", (_value, options) => {
            const { sourceEvent } = options;
            if ((sourceEvent instanceof KeyboardEvent) && hasKeyCode(sourceEvent, "ENTER")) {
                this.#inputEnterHandler();
            }
        });
    }

    protected busy(): void {
        this.#dialog.busy();
    }

    protected idle(): void {
        this.#dialog.idle();
    }

    // private methods --------------------------------------------------------

    /**
     * Event handler that triggers the primary button of the dialog, after
     * the ENTER key has been pressed in a text input control.
     */
    #inputEnterHandler(): boolean {
        const okEnabled = !this.getOkButton().disabled;
        if (okEnabled) { this.#dialog.invokeAction(OK_ACTION); }
        return !okEnabled;
    }

    /**
     * Event handler for global key events triggered from outside the dialog
     * frame. The TAB key will return the browser focus to the dialog, and the
     * ESCAPE key will close the dialog even without having the focus inside.
     */
    #globalKeyDownHandler(event: KeyboardEvent): void {

        // handle key events from document body only
        if (getFocus() !== document.body) { return; }

        // handle TAB and ESCAPE keys
        if (matchKeyCode(event, "TAB", { shift: null })) {
            this.grabFocus();
            event.preventDefault();
        } else if (isEscapeKey(event)) {
            this.close();
        }
    }

    /**
     * Event handler for all events triggered by this dialog instance. The
     * events will be filtered by action button keys, and will fulfil or reject
     * the promise returned by the method `show()` according to the result of
     * the action.
     */
    #actionButtonHandler(action: string): void {

        // do not invoke action handler recursively, check that a button exists for the action key
        if (this.#busy || !this.getActionButton(action)) { return; }

        // switch dialog to busy mode
        this.#busy = true;
        this.#dialog.$header.busy();

        // whether the Cancel button has been activated
        const isCancel = action === CANCEL_ACTION;
        // the callback handler and configuration of the action
        const entry = this.#actionHandlers.get(action);
        // keep open mode (not for Cancel action)
        const keepOpen = (!isCancel && entry?.options?.keepOpen) || false;

        // returns the dialog to idle mode after running an action handler
        const returnToIdle = (): void => {
            this.#busy = false;
            this.#dialog.$header.idle();
            this.#dialog.idle();
            // validate after leaving busy mode (`this.idle()` tries to restore button states)
            this.validate();
            this.grabFocus();
        };

        // invoke the action handler callback
        const promise = entry ? jpromise.invoke(() => entry.fn.call(this, action)) : jpromise.resolve();

        // on success: resolve the promise, decide whether to close the dialog
        this.onFulfilled(promise, value => {
            if ((keepOpen === true) || (keepOpen === "done")) {
                returnToIdle();
            } else if (isCancel) {
                this.#deferred.reject(action);
            } else {
                this.#deferred.resolve(value as ValT);
            }
        }, { async: true });

        // on error: reject the deferred, decide whether to close the dialog
        this.onRejected(promise, err => {
            debug.logScriptError(err);
            if (is.scriptError(err)) {
                this.#deferred.reject(err);
            } else if ((keepOpen === true) || (keepOpen === "fail")) {
                returnToIdle();
            } else {
                this.#deferred.reject(isCancel ? action : err);
            }
        }, { async: true });
    }

    /**
     * Returns the saved position of the dialog frame, in CSS pixels.
     */
    #getSavedPosition(): Opt<Rectangle> {
        return this.config.id ? positionCache.get(this.config.id) : undefined;
    }

    /**
     * Returns the absolute position of the dialog frame, in CSS pixels.
     */
    #getFramePosition(): Rectangle {
        const offset = this.#$contentNode.offset()!;
        const width = this.#$contentNode.width()!;
        const height = this.#$contentNode.height()!;
        return new Rectangle(offset.left, offset.top, width, height);
    }

    /**
     * Sets all CSS properties for manually moving the dialog frame.
     */
    #moveFrameNode(position?: Rectangle, move?: Point): void {
        position ??= this.#getFramePosition();
        const margin = SMALL_DEVICE ? 5 : 0;
        const height = SMALL_DEVICE ? this.#dialog.$header.outerHeight()! : position.height;
        const maxW = window.innerWidth - 2 * margin;
        const maxH = window.innerHeight - 2 * margin;
        const maxX = window.innerWidth - Math.min(maxW, position.width) - margin;
        const maxY = window.innerHeight - Math.min(maxH, height) - margin;
        const left = math.clamp(position.left + (move?.x ?? 0), margin, maxX);
        const top = math.clamp(position.top + (move?.y ?? 0), margin, maxY);
        this.rootNode.classList.add("moved");
        this.#$moveNode.css({ left, top, maxWidth: maxW });
    }
}
