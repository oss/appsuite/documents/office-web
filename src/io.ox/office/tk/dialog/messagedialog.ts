/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createLabel } from "@/io.ox/office/tk/dom";
import { type BaseDialogConfig, BaseDialog } from "@/io.ox/office/tk/dialog/basedialog";

// types ======================================================================

/**
 * Configuration options for a `MessageDialog` instance.
 */
export interface MessageDialogConfig extends BaseDialogConfig {

    /**
     * The message text shown in the dialog body element. If omitted, an empty
     * message dialog will be created which can be filled with more customized
     * content afterwards.
     */
    message?: string;
}

// class MessageDialog ========================================================

/**
 * Creates a simple dialog with a message text and an OK button.
 *
 * @template ConfT
 *  Type of configuration options that a subclass allows to pass to the
 *  constructor.
 */
export class MessageDialog<ConfT extends MessageDialogConfig = MessageDialogConfig> extends BaseDialog<void, ConfT> {

    /**
     * @param config
     *  Configuration options that control the appearance and behavior of the
     *  dialog. Supports all options of the base class `BaseDialog`. The option
     *  `cancelLabel` will be ignored (the "Cancel" button of the dialog will
     *  always be hidden).
     */
    public constructor(config: NoInfer<ConfT>) {

        // base constructor
        super({
            width: 400,
            ...config,
            cancelLabel: null // hide the Cancel button
        });

        // create a <label> element with the message text
        if (config.message) {
            this.bodyNode.appendChild(createLabel({ label: config.message }));
        }
    }
}
