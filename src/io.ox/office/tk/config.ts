/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import ox from "$/ox";
import { locationHash } from "$/url";

import type { Settings } from "$/io.ox/core/settings";
import TabApi from "$/io.ox/core/api/tab";
import Theming from "$/io.ox/core/theming/main";

import { is, to, fun, map, pick, json } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";

import { settings as coreSettings } from "$/io.ox/core/settings";
import { settings as docsSettings } from "@/io.ox/office/settings";

const { floor, max } = Math;

// types ======================================================================

/**
 * Event data used to propagate changed configuration settings.
 */
export interface ConfigChangedEvent {

    /**
     * Path of the changed configuration item.
     */
    path: string;

    /**
     * Current value of the changed configuration item.
     */
    value: unknown;

    /**
     * Previous value of the changed configuration item.
     */
    previous: unknown;

    /**
     * Whether the configuration item has been changed locally (`false`), or in
     * another browser tab (`true`).
     */
    remote: boolean;
}

/**
 * Options for getting or setting boolean flags in the anchor part of the URL.
 */
export interface UrlFlagOptions {

    /**
     * The default state to be assumed, if the boolean flag has not been set in
     * the URL (neither enabled nor disabled).
     */
    default?: boolean;

    /**
     * The string value corresponding to the state `true` of the boolean flag.
     * Can be a list of values separated with the pipe character. Default value
     * is `"1|true|on"`.
     */
    enable?: string;

    /**
     * The string value corresponding to the state `false` of the boolean flag.
     * Can be a list of values separated with the pipe character. Default value
     * is `"0|false|off"`.
     */
    disable?: string;
}

/**
 * Options for getting or setting debug flags.
 */
export interface DebugFlagOptions extends UrlFlagOptions {

    /**
     * Whether the option needs to be written into the URL to take any effect.
     */
    hash?: boolean;
}

// TabAPI events ==============================================================

interface TabSettingsChangedEventMap {
    "core-settings-changed": [event: ConfigChangedEvent];
    "office-settings-changed": [event: ConfigChangedEvent];
}

// mix the new communication events into core event map
declare module "$/io.ox/core/api/tab" {
    export interface TabCommunicationEventMap extends TabSettingsChangedEventMap { }
}

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted when an item in the Core-UI configuration has been
         * changed.
         */
        "change:config:core": [data: ConfigChangedEvent];

        /**
         * Will be emitted when an item in the Documents configuration has been
         * changed.
         */
        "change:config:docs": [data: ConfigChangedEvent];

        /**
         * Will be emitted if something in the debug configuration in the local
         * storage has changed.
         */
        "change:config:debug": [];

        /**
         * Will be emitted when the current UI theme has been changed.
         */
        "change:theme": [theme: string];
    }
}

// constants ==================================================================

/**
 * The key of the item in the local storage that stores all debug configuration
 * settings of the Documents applications. This key must match the key that
 * will be restored in `io.ox/core/cache/localstorage.js`.
 */
const DEBUG_STORAGE_KEY = "appsuite.office-debug";

// globals ====================================================================

const debugFlagMap = new Map<string, Opt<DebugFlagOptions>>();

const debugCommandRegistry = new Map<string, VoidFunction[]>();

// private functions ==========================================================

function implGetInt(settings: Settings, key: string, def?: number, lower?: number): number {
    const value = settings.get(key, null);
    let num = is.number(value) ? value : (def || 0);
    if (is.number(lower)) { num = max(num, lower); }
    return floor(num);
}

function implGetEnableValues(options?: UrlFlagOptions): string[] {
    // ignore empty strings
    return options?.enable ? options.enable.split("|") : ["true", "1", "on"];
}

function implGetDisableValues(options?: UrlFlagOptions): string[] {
    // ignore empty strings
    return options?.disable ? options.disable.split("|") : ["false", "0", "off"];
}

function implGetUrlFlag(key: string, options?: UrlFlagOptions): boolean | null {
    let value = locationHash(key);
    const def = is.boolean(options?.default) ? options.default : null;
    if (!value || !is.string(value)) { return def; }
    value = value.toLowerCase();
    const enable = implGetEnableValues(options);
    if (enable.includes(value)) { return true; }
    const disable = implGetDisableValues(options);
    if (disable.includes(value)) { return false; }
    return def;
}

function implGetDebugFlag(key: string): boolean {
    // resolve debug flag configuration
    const options = debugFlagMap.get(key);
    // URL wins over local storage
    const urlFlag = implGetUrlFlag(key, { ...options, default: undefined });
    if (is.boolean(urlFlag)) { return urlFlag; }
    // get flag from local storage
    const value = getDebugValue(key);
    return to.boolean(value, !!options?.default);
}

function implSetDebugFlag(key: string, value: boolean): void {
    // resolve debug flag configuration
    const options = debugFlagMap.get(key);
    // update flag in local storage
    const data = (value === !!options?.default) ? null : value;
    setDebugValue(key, data);
    // remove flag from URL (or explicitly write it into URL if specified)
    if (!options?.hash || (data === null)) {
        locationHash(key, null);
    } else {
        const values = data ? implGetEnableValues(options) : implGetDisableValues(options);
        locationHash(key, values[0]);
    }
}

/**
 * Registers event handlers that listen to local configuration changes, and to
 * configuration changes in other browser tabs, and forwards these changes to
 * the other side respectively.
 */
function observeConfig(settings: Settings, tabEventKey: keyof TabSettingsChangedEventMap, fn: (event: ConfigChangedEvent) => void): void {

    // recursion guard (`true` while changing/sending configuration items
    let changing = false;

    // listen to local configuration changes, and propagate them to other browser tabs
    settings.on("change", (path, value, previous) => {
        if (!changing && !_.isEqual(value, previous)) {
            if (ox.tabHandlingEnabled) {
                changing = true;
                TabApi.propagate(tabEventKey, { path, value, previous, remote: false });
                changing = false;
            }
            fn({ path, value, previous, remote: false });
        }
    });

    // listen to changes from other browser tabs, and propagate them to local configuration
    if (ox.tabHandlingEnabled) {
        TabApi.communicationEvents.on(tabEventKey, event => {
            if (!changing) {
                changing = true;
                settings.set(event.path, event.value);
                changing = false;
                fn({ ...event, remote: true });
            }
        });
    }
}

// observe Core-UI configuration
observeConfig(coreSettings, "core-settings-changed", event => {
    globalEvents.emit("change:config:core", event);
    // detect changed UI theme and emit another dedicated global event
    if (event.path === "theming/current") {
        if (event.remote) { Theming.restoreCurrent(); }
        globalEvents.emit("change:theme", pick.string(event.value, "theme", ""));
    }
});

// observe Documents configuration
observeConfig(docsSettings, "office-settings-changed", event => {
    globalEvents.emit("change:config:docs", event);
});

// public functions ===========================================================

/**
 * Returns the value of a specific configuration property, or the complete set
 * of all available configuration properties.
 *
 * @param key
 *  The key of a specific configuration property.
 *
 * @param [def]
 *  The default value to be returned, if the configuration property specified
 *  with the parameter `key` does not exist.
 *
 * @returns
 *  The value of the specified configuration property.
 */
export function getValue(key: string, def?: unknown): unknown {
    return docsSettings.get(key, def);
}

/**
 * Returns the state of a specific boolean configuration property.
 *
 * @param key
 *  The key of a specific boolean configuration property.
 *
 * @param [def=false]
 *  The default state to be returned, if the configuration property specified
 *  with the parameter `key` does not exist.
 *
 * @returns
 *  The state of the specified configuration property. If the property does not
 *  exist, this function will return the default value.
 */
export function getFlag(key: string, def = false): boolean {
    return to.boolean(getValue(key), def);
}

/**
 * Returns the state of a specific string configuration property.
 *
 * @param key
 *  The key of a specific string configuration property.
 *
 * @param [def=""]
 *  The default value to be returned, if the configuration property specified
 *  with the parameter `key` does not exist.
 *
 * @returns
 *  The value of the specified configuration property. If the property does not
 *  exist, this function will return the default value.
 */
export function getStr(key: string, def = ""): string {
    return to.string(getValue(key), def);
}

/**
 * Returns the state of a specific integer configuration property.
 *
 * @param key
 *  The key of a specific integer configuration property.
 *
 * @param [def=0]
 *  The default value to be returned, if the configuration property specified
 *  with the parameter `key` does not exist.
 *
 * @param [lower]
 *  The lower bound for the value (inclusive). If omitted, all integer values
 *  will be allowed.
 *
 * @returns
 *  The value of the specified configuration property. If the property does not
 *  exist, this function will return the default value.
 */
export function getInt(key: string, def?: number, lower?: number): number {
    return implGetInt(docsSettings, key, def, lower);
}

/**
 * Returns the state of a specific array configuration property.
 *
 * @param key
 *  The key of a specific array configuration property.
 *
 * @returns
 *  The value of the specified configuration property. If the property does not
 *  exist, this function will return an empty array.
 */
export function getArray(key: string): unknown[] {
    return to.array(getValue(key), true);
}

/**
 * Sets a value of a specific configuration property. Be careful to only set
 * single properties.
 *
 * @param key
 *  The key of a specific configuration property.
 *
 * @param value
 *  The value to be set for the specified configuration key. If the value is an
 *  array, you cannot delete single entries from it. Better call
 *  `set(key, null)` and then save the correct array again.
 *
 * @returns
 *  A promise that will fulfil when the server request has been sent
 *  successfully.
 */
export function setValue(key: string, value: unknown): JPromise {

    // create a object tree with the value as property
    const updateTree = key.split("/").reduceRight((val, dir) => ({ [dir]: val }), value);

    // Bug 65386: Value must be saved immediately in order to be available in the local cache,
    // but reload settings and save the value again to workaround problems with remote changes
    // (e.g. the "shown" flag of welcome tours interferring with the recent list etc.).
    // Call `merge()` with update tree to prevent that the whole tree is sent to the backend.
    return docsSettings.set(key, value).reload().then(() => docsSettings.set(key, value, { silent: true }).merge(updateTree));
}

/**
 * Returns the value of the specified item from the local storage.
 *
 * @param key
 *  The identifier of the configuration item.
 *
 * @param [def]
 *  The default value to be returned, if the configuration item does not exist.
 *
 * @returns
 *  The value of the configuration item; or the default value, if the item does
 *  not exist in the local storage.
 */
export function getStorageValue(key: string, def?: unknown): unknown {
    const value = window.localStorage.getItem(key);
    return json.safeParse(value || "", def);
}

/**
 * Returns the value of the specified boolean item from the local storage.
 *
 * @param key
 *  The identifier of the configuration item.
 *
 * @param [def=false]
 *  The default state to be returned, if the configuration item does not exist,
 *  or is not a boolean.
 *
 * @returns
 *  The value of the configuration item.
 */
export function getStorageFlag(key: string, def?: boolean): boolean {
    const value = getStorageValue(key);
    return is.boolean(value) ? value : (def === true);
}

/**
 * Returns the value of the specified string item from the local storage.
 *
 * @param key
 *  The identifier of the configuration item.
 *
 * @param [def=""]
 *  The default value to be returned, if the configuration item does not exist,
 *  or is not a string.
 *
 * @returns
 *  The value of the configuration item.
 */
export function getStorageStr(key: string, def?: string): string {
    const value = getStorageValue(key);
    return is.string(value) ? value : (def || "");
}

/**
 * Returns the value of the specified integer item from the local storage.
 *
 * @param key
 *  The identifier of the configuration item.
 *
 * @param [def=0]
 *  The default value to be returned, if the configuration item does not exist,
 *  or is not an integer.
 *
 * @param [lower]
 *  The lower bound for the value (inclusive). If omitted, all integer values
 *  will be allowed.
 *
 * @returns
 *  The value of the configuration item.
 */
export function getStorageInt(key: string, def?: number, lower?: number): number {
    const value = getStorageValue(key);
    let num = is.number(value) ? value : (def || 0);
    if (is.number(lower)) { num = max(num, lower); }
    return floor(num);
}

/**
 * Changes the value of the specified configuration item in the local storage.
 *
 * @param key
 *  The identifier of the configuration item.
 *
 * @param value
 *  The new value for the configuration item. If set to `null`, the value will
 *  be removed from the local storage.
 *
 * @returns
 *  Whether the configuration item has been written successfully to the local
 *  storage.
 */
export function setStorageValue(key: string, value: unknown): boolean {
    try {
        if (is.nullish(value)) {
            window.localStorage.removeItem(key);
        } else {
            window.localStorage.setItem(key, json.tryStringify(value));
        }
        return true;
    } catch {
        window.console.warn("config::setStorageValue - cannot write configuration item to local storage");
        return false;
    }
}

/**
 * Removes the specified configuration item from the local storage.
 *
 * @param key
 *  The identifier of the configuration item.
 *
 * @returns
 *  Whether the configuration item has been removed successfully from the local
 *  storage.
 */
export function removeStorageValue(key: string): boolean {
    return setStorageValue(key, null);
}

/**
 * Returns the state of the specified boolean flag in the anchor part of the
 * browser URL.
 *
 * @param key
 *  The name of the URL anchor flag.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The state of the specified flag in the anchor part of the URL.
 */
export function getUrlFlag(key: string, options?: UrlFlagOptions): boolean {
    return !!implGetUrlFlag(key, options);
}

/**
 * Returns the value of the specified debug configuration item.
 *
 * All debug configuration items will be stored in a special dictionary in the
 * local storage. This dictionary is protected from deletion when the local
 * storage will be cleaned after global UI updates.
 *
 * @param key
 *  The identifier of the debug configuration item.
 *
 * @returns
 *  The value of the configuration item; or `null`, if the item does not exist
 *  in the local storage.
 */
export function getDebugValue(key: string): unknown {

    // try to receive and parse existing dictionary (ignore all failures)
    const configDict = to.dict(getStorageValue(DEBUG_STORAGE_KEY));

    // return the value form the dictionary
    return (configDict && (key in configDict)) ? configDict[key] : null;
}

/**
 * Changes the value of the specified debug configuration item.
 *
 * All debug configuration items will be stored in a special dictionary in the
 * local storage. This dictionary is protected from deletion when the local
 * storage will be cleaned after global UI updates.
 *
 * @param key
 *  The identifier of the debug configuration item.
 *
 * @param value
 *  The new value for the configuration item. If set to `null`, the value will
 *  be removed from the local storage.
 *
 * @returns
 *  Whether the configuration item has been written successfully to the local
 *  storage.
 */
export function setDebugValue(key: string, value: unknown): boolean {

    // try to receive and parse existing dictionary (ignore all failures)
    const configDict = to.dict(getStorageValue(DEBUG_STORAGE_KEY), true);

    // update the configuration item in the dictionary
    let changed: boolean;
    if (is.nullish(value)) {
        changed = key in configDict;
        delete configDict[key];
    } else {
        changed = configDict[key] !== value;
        configDict[key] = value;
    }

    // nothing to do if the value does not change
    if (!changed) { return true; }

    // write the updated dictionary back to the storage
    if (!setStorageValue(DEBUG_STORAGE_KEY, configDict)) { return false; }

    // notify listeners
    globalEvents.emit("change:config:debug");
    return true;
}

/**
 * Removes all debug settings from the local storage.
 */
export function resetAllDebugFlags(): void {

    // remove the debug configuration dictionary from the local storage
    removeStorageValue(DEBUG_STORAGE_KEY);

    // notify listeners
    globalEvents.emit("change:config:debug");
}

export function registerDebugFlag(key: string, options?: DebugFlagOptions): void {
    debugFlagMap.set(key, options);
}

/**
 * Returns the state of the specified debug configuration flag. Without debug
 * mode, this function simply returns `false`.
 *
 * @param key
 *  The name of the debug flag.
 *
 * @returns
 *  The state of the specified flag.
 */
export function getDebugFlag(key: string): boolean {
    return DEBUG && implGetDebugFlag(key);
}

/**
 * Changes the state of the specified debug configuration flag. Without debug
 * mode, this function does nothing.
 *
 * @param key
 *  The name of the debug flag.
 *
 * @param value
 *  The new state of the specified flag.
 */
export function setDebugFlag(key: string, value: boolean): void {
    if (DEBUG) { implSetDebugFlag(key, value); }
}

/**
 * Returns the state of the debug flag "office:sticky-popups".
 */
export function isStickyPopupsMode(): boolean {
    return getDebugFlag("office:sticky-popups");
}

/**
 * Defines a new (or extends an existing) debug command for the browser
 * console. Debug commands can be triggered by executing the property
 * `office.debug.command` (or `window.office.debug.command`) in the browser
 * console.
 *
 * @param key
 *  The key of the debug command. Will be assigned to the `office.debug`
 *  dictionary (as property getter, i.e. the command will be executed by simply
 *  reading the property from the `office.debug` dictionary instead of having
 *  to invoke it).
 *
 * @param handler
 *  A handler that will be executed when the debug command will be invoked in
 *  the browser console. All handlers registered for the same debug command
 *  will be executed in registration order.
 */
export function registerDebugCommand(key: string, handler: VoidFunction): void {
    if (window.office) {
        const handlers = map.upsert(debugCommandRegistry, key, () => []);
        handlers.push(handler);
        if (!(key in window.office.debug)) {
            Object.defineProperty(window.office.debug, key, {
                get: () => handlers.forEach(fn => fn()),
                enumerable: true
            });
        }
    }
}

/**
 * Registers an event handler for the specified debug configuration flag.
 */
export function onDebugFlag(key: string, toggleHandler: (state: boolean) => void): void;
export function onDebugFlag(key: string, enableHandler: FuncType<void>, disableHandler: FuncType<void>): void;
// implementation
export function onDebugFlag(key: string, enableHandler: FuncType<void, any[]>, disableHandler?: FuncType<void>): void {
    let currState = getDebugFlag(key);
    const invokeHandler = disableHandler ? (() => currState ? enableHandler() : disableHandler()) : (() => enableHandler(currState));
    invokeHandler();
    globalEvents.on("change:config:debug", () => {
        const newState = getDebugFlag(key);
        if (currState !== newState) {
            currState = newState;
            invokeHandler();
        }
    });
}

// constants ==================================================================

/**
 * The maximum number of keys allowed in a JSON object sent to remote services.
 */
export const MAX_JSON_KEYS = implGetInt(coreSettings, "jsonMaxSize", 2500, 0);

/**
 * Specifies whether the local storage of the browser is available (may be
 * unavailable in "Incognito mode" of some browsers).
 */
export const STORAGE_AVAILABLE = fun.do(() => {
    try {
        const key = `__localTestKey__${Date.now()}`;
        window.localStorage.setItem(key, "1");
        window.localStorage.removeItem(key);
        return true;
    } catch {
        return false;
    }
});

/**
 * Specifies whether the debug mode is enabled in the server configuration.
 *
 * _Attention:_ This constant reflects the default behavior when the state of
 * the debug mode has not been specified locally, but does not reflect whether
 * debug mode will be effectively active locally (see constant `DEBUG`).
 */
export const DEBUG_AVAILABLE = getFlag("module/debugavailable");

/**
 * Specifies whether the debug mode can be enabled locally, even if it has not
 * been activated in the server configuration.
 */
export const DEBUG_ON_DEMAND = getFlag("module/debugondemand");

/**
 * Specifies whether the debug mode is enabled in all Documents applications.
 *
 * Debug mode will be enabled, if the server-side configuration property
 * `DEBUG_AVAILABLE` is set to `true`, and the client-side debug flag
 * `office:enable-debug` is NOT explicitly set to `false` (either `true` or not
 * defined); or if the server-side configuration property `DEBUG_ON_DEMAND` is
 * set to `true`, and the client-side debug flag `office:enable-debug` is set
 * to `true`.
 */
export const DEBUG = fun.do(() => {
    // register default value for "office:enable-debug" depending on server configuration
    registerDebugFlag("office:enable-debug", { default: DEBUG_AVAILABLE });
    // server-side flag is active: accept missing debug flag in local configuration
    if (DEBUG_AVAILABLE) { return implGetDebugFlag("office:enable-debug"); }
    // server-side flag `debugondemand` is active: debug flag must be present in local configuration
    return DEBUG_ON_DEMAND && implGetDebugFlag("office:enable-debug");
});

/**
 * Specifies whether the current platform is an automated test environment.
 */
export const AUTOTEST = implGetDebugFlag("office:testautomation");

/**
 * Specifies whether the current platform is a unit test environment.
 */
export const UNITTEST = ox.office?._UNITTEST === true;

/**
 * Specifies whether the error and other important data for bug hunting will be
 * sent to and logged by the server.
 */
export const LOG_ERROR_DATA = getFlag("module/logErrorData", false) && !implGetDebugFlag("office:prevent-logErrorData");

/**
 * Specifies whether detailed performance data will be sent to and logged by
 * the server.
 */
export const LOG_PERFORMANCE_DATA = getFlag("module/logPerformanceData", false);

// static initialization ======================================================

// add console controller for debugging
if (DEBUG_AVAILABLE || DEBUG_ON_DEMAND) {

    // create the global `office.debug` dictionary
    window.office = { debug: { settings: docsSettings } };

    // enables or disables debug mode, and reloads the browser
    const toggleDebugMode = (enable: boolean): void => {
        window.console.warn(`%c${enable ? "Enabling" : "Disabling"} Documents debug mode...`, "color:red");
        implSetDebugFlag("office:enable-debug", enable);
        window.location.reload();
    };

    // register the "enable" and "disable" debug commands
    registerDebugCommand("enable", () => toggleDebugMode(true));
    registerDebugCommand("disable", () => toggleDebugMode(false));

    // show a warning in the console if debug mode has been disabled locally (no debug toolbar to re-enable)
    if (!DEBUG) {
        const message = DEBUG_AVAILABLE ? "Debug mode for Documents has been disabled locally!" : "Debug mode for Documents can be enabled locally!";
        window.console.warn(`%c${message} To enable, type "office.debug.enable" in the browser console.`, "color:red");
    }

    // fire change events if other browser tabs have changed debug items
    window.addEventListener("storage", event => {
        if ((event.storageArea === localStorage) && (event.key === DEBUG_STORAGE_KEY)) {
            globalEvents.emit("change:config:debug");
        }
    });
}
