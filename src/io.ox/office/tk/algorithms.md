# Basic Algorithms

## Overview

Module [tk/algorithms.ts](./algorithms.ts) exports various low-level helper functions to handle data structures of various types.

To reduce the number of exported symbols, and to prevent name clashes with similar functions, the module actually exports namespace objects grouping related functions.

Each namespace object covers one implementation module from subdirectory [tk/algorithms/](./algorithms).

## Type Detection: [`is`](./algorithms/is.ts)

This namespace contains helper function for type assertions. The functions can take any value, and return a boolean whether the value is of a certain type, together with a TypeScript type assertion. Contains some special functions not available in libraries such as Underscore.

- Primitives: `undefined`, `null`, `nullish`, `number`, `bigint`, `boolean`, `string`, `symbol`, `primitive`.
- Objects: `dict`, `object`, `destroyable`, `abortable`.
- Arrays: `array`, `readonlyArray`, `nativeArray`, `arrayLike`.
- Others: `function`, `empty`, `scriptError`.

## Type Conversion: [`to`](./algorithms/to.ts)

This namespace contains helper function for type detection with fallback values. These functions can take any value, and return that value if it is of a certain type, otherwise a default value.

- Primitives: `number`, `bigint`, `boolean`, `string`, `enum`.
- Objects, arrays: `dict`, `array`, `function`.

## Math Helpers: [`math`](./algorithms/math.ts)

This namespace contains additional mathematical functions beside the builtin `Math` namespace.

- Basic functions: `sgn`, `inflate`, `clamp`, `modulo`, `radius`, `isZero`, `randomInt`, `splitNumber`.
- Rounding with arbitrary precision: `floorp`, `ceilp`, `roundp`, `truncp`, `inflatep`.
- Functional comparison: `compare`, `less`, `comparePairs`.

## Coordinate Helpers: [`coord`](./algorithms/coord.ts)

This namespace contains additional types and functions for two-dimensional cartesian and polar coordinates.

- Types: `Point`, `Polar`, `Size`.
- Cartesian point helpers: `point`, `pointsDiff`, `pointsDist`.
- Polar point helpers: `polar`.
- Conversion: `pointToPolar`, `polarToPoint`.

## String Helpers: [`str`](./algorithms/string.ts)

This namespace contains additional string functions not available in `String.prototype`.

- Comparison ignoring case: `startsWithICC`, `endsWithICC`, `compareICC`, `equalsICC`.
- Substring handling: `insertSubStr`, `replaceSubStr`.
- Trimming/cleaning NPCs: `trimAll`, `cleanNPC`, `trimAndCleanNPC`.
- Token lists (space-separated): `splitTokens`, `concatTokens`.
- Internationalization: `noI18n`.

## Unicode Helpers: [`unicode`](./algorithms/unicode.ts)

This namespace contains functions for strings with Unicode surrogate pairs.

## Regular Expression Helpers: [`re`](./algorithms/regexp.ts)

This namespace contains functions to build patterns for regular expressions.

- Pattern building: `escape`, `alt`, `capture`, `group`, `ahead`, `behind`, `class`.

## Integer Formatting: [`fmt`](./algorithms/format.ts)

This namespace contains functions for formatting integers to strings, and parsing strings to integers.

- Integer safe parsing, formatting with radix and padding: `formatInt`, `parseInt`.
- Alphabetic numbers (A, B, C, ...): `formatAlphabetic`, `parseAlphabetic`.
- Roman numbers with custom depth: `formatRoman`, `parseRoman`.

## Functional Helpers: [`fun`](./algorithms/function.ts)

This namespace contains helper functions for functional programming.

- Constant generators: `const`, `undef`, `null`, `false`, `true`.
- Conversion: `not`.
- Projection: `identity`, `pluck2nd`, `pluck3rd`, `pluck4th`.
- Expression blocks: `do`, `yield`, `try`.
- Guarding and memoization: `guard`, `once`, `memoize`.

## Native Iterables/Iterators: [`itr`](./algorithms/iterable.ts)

This namespace contains helper functions for native iterables and iterators.

- Generators: `empty`, `once`, `forever`, `yieldFirst`, `sequence`, `interval`.
- Aggregators: `some`, `every`, `reduce`, `size`, `count`, `group`, `split`.
- Searching: `shift`, `first`, `find`, `findLast`, `mapFirst`, `mapLast`, `nearest`.
- Transforms: `indexed`, `chain`, `flatten`, `filter`, `map`, `unify`, `merge`.

Most functions can be used with explicit iterators (e.g. `itr.some(map.values(), ...)`), or directly with iterable containers (e.g. `itr.some(map, ...)`).

__TODO:__ Consider using an external iterator extension library.

## Array Helpers: [`ary`](./algorithms/array.ts)

This namespace contains additional functions for native arrays not available in `Array.prototype`.

- Constructors: `from`, `filterFrom`, `flatFrom`, `sortFrom`, `yieldFrom`, `wrap`, `concat`, `sequence`, `fill`, `generate`.
- Comparison: `equals`, `compare`.
- Iteration (also for array-like objects, reverse and slice): `entries`, `values`, `interval`, `forEach`.
- Aggregation (also for array-like objects, reverse and slice): `some`, `every`, `count`, `group`, `split`.
- Inplace modification: `append`, `insert`, `insertAt`, `deleteAt`, `deleteFirst`, `deleteAll`, `insertSorted`, `deleteSorted`, `sortBy`, `unify`, `permute`, `destroy`.
- Element access: `at`, `last`.
- Transforms: `initial`.
- Searching (also for array-like objects, reverse and slice): `findIndex`, `findValue`, `nearestIndex`, `nearestValue`.
- Binary search (also for array-like objects, reverse and slice): `fastFindFirstIndex`, `fastFindFirstValue`, `fastFindLastIndex`, `fastFindLastValue`.
- Draining generators: `shiftValues`, `popValues`.

## Native Map Helpers: [`map`](./algorithms/map.ts)

This namespace contains additional functions for native maps not available in `Map.prototype`.

- Constructors: `from`, `filterFrom`, `yieldFrom`.
- Inplace modification: `destroy`.
- Element access: `toggle`, `remove`, `visit`, `upsert`, `update`, `add`, `move`, `assign`.
- Searching: `first`, `find`, `findKey`, `findEntry`.
- Draining generators: `shiftValues`, `shiftKeys`, `shiftEntries`.

## Native Set Helpers: [`set`](./algorithms/set.ts)

This namespace contains additional functions for native sets not available in `Set.prototype`.

- Constructors: `from`, `filterFrom`, `yieldFrom`.
- Inplace modification: `destroy`.
- Element access: `toggle`, `assign`.
- Searching: `find`.
- Draining generators: `shiftValues`.

## Dictionary Helpers: [`dict`](./algorithms/dict.ts)

This namespace contains helper functions for dictionaries (plain JavaScript objects).

The looping methods do not suffer from the "array-like pitfall" like Underscore or Lodash which _intentionally_ treat all objects with a numeric property `length` as array-likes. This behaviour has already caused bugs, for example for [comment mentions](https://confluence.open-xchange.com/display/DOCS/General+Operations#GeneralOperations-Mention) in document operations.

- Constructors: `create`, `createRec`, `createPartial`, `from`, `fill`, `fillRec`, `generate`, `generateRec`.
- Comparison: `equals`.
- Iteration: `forEach`, `forEachKey`
- Aggregation: `some`, `someKey`, `every`, `everyKey`, `reduce`.
- Searching: `find`, `findKey`, `keyOf`, `singleKey`, `anyKey`.
- Iteration: `values`, `keys`, `entries`.
- Transformation: `mapDict`, `mapRecord`.

## Property Access: [`pick`](./algorithms/pick.ts)

This namespace contains functions to pick typed properties from unknown source data.

- Untyped: `prop`.
- Primitives: `number`, `bigint`, `boolean`, `string`, `enum`.
- Objects, arrays: `dict`, `array`, `function`.

## Safe Parse/Format JSON: [`json`](./algorithms/json.ts)

This namespace contains functions similar to builtin `JSON` namespace but with improved type safety.

- Cloning: `flatClone`, `deepClone`.
- Parsing: `tryParse`, `safeParse`.
- Serialization: `tryStringify`, `safeStringify`.

## Universally Unique Identifiers: [`uuid`](./algorithms/uuid.ts)

This namespace contains functions to create and parse UUIDs.

- Test: `is`.
- Generator: `random`.

## Class Prototype: [`proto`](./algorithms/prototype.ts)

This namespace contains helper functions for accessing the own properties and prototype.

- Own properties: `hasOwn`, `getOwn`.
- Prototype (type-safe): `getPrototypeOf`.

## JQuery Promises: [`jpromise`](./algorithms/jpromise.ts)

This namespace contains helper functions for JQuery promises which behave slightly different to native promises.

- Type assertions: `isAnyPromise`, `is`.
- Constructors: `new`, `deferred`, `resolve`, `reject`, `from`, `invoke`.
- State: `isPending`, `isSettled`, `isFulfilled`, `isRejected`.
- Floating promises: `floating`, `invokeFloating`, `wrapFloating`.
- Fast promise chains: `fastThen`, `fastCatch`, `fastFinally`, `fastChain`.

## Debug Logging: [`debug`](./algorithms/debug.ts)

This namespace contains helper functions for debug logging.

- Helpers: `stringify`, `logScriptError`.
