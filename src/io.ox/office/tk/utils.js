/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';
import DOMPurify from 'dompurify';

import { is, math } from '@/io.ox/office/tk/algorithms';
import { TOUCH_DEVICE, SMALL_DEVICE, getScreenHeight, isPortrait, isLandscape } from '@/io.ox/office/tk/dom/device';
import { containsNode, parseCssLength } from '@/io.ox/office/tk/dom/domutils';

import '@/io.ox/office/tk/dom/style.less';

// re-exports =================================================================

export * from '@/io.ox/office/tk/dom/device';
export * from '@/io.ox/office/tk/dom/domutils';
export * from '@/io.ox/office/tk/dom/formutils';

// constants ==================================================================

/**
 * A unique object used as return value in callback functions of iteration
 * loops to break the iteration process immediately.
 */
export const BREAK = {};

/**
 * A Boolean flag specifying whether softkeyboard toggle mode should be enabled.
 */
export const SOFTKEYBORD_TOGGLE_MODE = SMALL_DEVICE;

/**
 * A Boolean flag specifying whether the current device is Android and the
 * browser is Chrome.
 */
export const CHROME_ON_ANDROID = !!(_.browser.Android && _.browser.Chrome);

/**
 * A Boolean flag specifying whether the current (really) browser supports the
 * clipboard API.
 */
export const CLIPBOARD_API_SUPPORTED = !_.browser.Edge || _.browser.EdgeChromium;

/**
 * min 200ms for detection of a valid move operation. This can be used to
 * avoid move operations that are triggered during selecting a drawing or
 * activating the edit mode on touch devices.
 */
export const MIN_MOVE_DURATION = 200;

// public functions ===========================================================

/**
 * Extracts the system clipboard data from the passed browser paste event.
 *
 * @param {jQuery.Event} event
 *  The browser paste event, wrapped as a jQuery event object.
 *
 * @returns {Object|Null}
 *  The system clipboard data from the passed event object, if the browser
 *  supports the clipboard API; otherwise null.
 */
export const getClipboardData = CLIPBOARD_API_SUPPORTED ? function (event) {
    return (event && event.originalEvent && event.originalEvent.clipboardData) || null;
} : _.constant(null);

// generic JS object helpers ----------------------------------------------

/**
 * Adds a property to all objects in the passed list.
 *
 * @param {Dict[]|Dict<Dict>} list
 *  The objects to be extended.
 *
 * @param {string} key
 *  The name of the property to be added.
 *
 * @param {any} value
 *  The value of theproperty to be added.
 *
 * @returns {Dict[]|Dict<Dict>}
 *  The list passed to this function.
 */
export function addProperty(list, key, value) {
    _.forEach(list, function (object) { object[key] = value; });
    return list;
}

/**
 * Deletes a property from all objects in the passed list.
 *
 * @param {Dict[]|Dict<Dict>} list
 *  The objects to be shortened.
 *
 * @param {string} key
 *  The name of the property to be deleted.
 *
 * @returns {Dict[]|Dict<Dict>}
 *  The list passed to this function.
 */
export function deleteProperty(list, key) {
    _.forEach(list, function (object) { delete object[key]; });
    return list;
}

// calculation, conversion, string manipulation ---------------------------

export const convertToEMValue = (function () {

    var // the conversion factors between pixels and other units
        FACTORS = {
            px: 1 / 16,
            pt: 1 / 12
        };

    function convertToEM(value, fromUnit) {
        var em = (value * (FACTORS[fromUnit] || 1));
        em = Math.round(em * 10000) / 10000;
        return em;
    }
    return convertToEM;
}());

export function convertToEM(value, fromUnit) {
    return convertToEMValue(value, fromUnit) + 'em';
}

/**
 * Parses the passed HTML mark-up text, and removes insecure mark-up. In
 * detail, all scripting elements, and all elements that contain event
 * handlers called automatically by the browser will be removed.
 *
 * @param {String} markup
 *  The HTML mark-up to be parsed.
 *
 * @returns {jQuery}
 *  The parsed and sanitized mark-up, as jQuery collection.
 */
export function parseAndSanitizeHTML(markup) {

    // replace NUL characters created by Firefox
    markup = markup.replace(/\0+/g, '');

    // sanitize HTML, see: https://github.com/cure53/DOMPurify for all available options
    var sanitized = DOMPurify.sanitize(markup, { SAFE_FOR_JQUERY: true, ADD_TAGS: ['#comment'] });

    // $() is not able to handle DOM fragments, so use $.parseHTML() instead
    var result = $($.parseHTML(sanitized));

    // remove all 'position' CSS attributes to avoid clipboard content being visible
    result.find('*').addBack().css('position', '');

    return result;
}

// options object ---------------------------------------------------------

/**
 * Extracts a property value from the passed object. If the property does
 * not exist, returns the specified default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property.
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getOption(options, name, def) {
    return (_.isObject(options) && (name in options)) ? options[name] : def;
}

/**
 * Extracts a string property from the passed object. If the property does
 * not exist, or is not a string, returns the specified default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the string property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not a string. May be any value (not only strings).
 *
 * @param {Boolean} [nonEmpty=false]
 *  If set to true, only non-empty strings will be returned from the
 *  options object. Empty strings will be replaced with the specified
 *  default value.
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getStringOption(options, name, def, nonEmpty) {
    var value = getOption(options, name);
    return (_.isString(value) && (!nonEmpty || (value.length > 0))) ? value : def;
}

/**
 * Extracts a boolean property from the passed object. If the property does
 * not exist, or is not a boolean value, returns the specified default
 * value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the boolean property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not a boolean value. May be any value (not only booleans).
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getBooleanOption(options, name, def) {
    var value = getOption(options, name);
    return _.isBoolean(value) ? value : def;
}

/**
 * Extracts a floating-point property from the passed object. If the
 * property does not exist, or is not a number, returns the specified
 * default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the floating-point property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not a number. May be any value (not only numbers).
 *
 * @param {Number} [min]
 *  If specified and a number, set a lower bound for the returned value. Is
 *  not used, if neither the property nor the passed default are numbers.
 *
 * @param {Number} [max]
 *  If specified and a number, set an upper bound for the returned value.
 *  Is not used, if neither the property nor the passed default are
 *  numbers.
 *
 * @param {Number} [precision]
 *  If specified, the resulting number will be rounded to the nearest
 *  multiple of this value. Must be positive.
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getNumberOption(options, name, def, min, max, precision) {
    var value = getOption(options, name);
    var finite = Number.isFinite(value);
    if (!finite) {
        value = def;
        finite = Number.isFinite(value);
    }
    if (finite) {
        if (Number.isFinite(min) && (value < min)) { value = min; }
        if (Number.isFinite(max) && (value > max)) { value = max; }
        return (Number.isFinite(precision)) ? math.roundp(value, precision) : value;
    }
    return value;
}

/**
 * Extracts an integer property from the passed object. If the property
 * does not exist, or is not a number, returns the specified default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the integer property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not a number. May be any value (not only numbers).
 *
 * @param {Number} [min]
 *  If specified and a number, set a lower bound for the returned value. Is
 *  not used, if neither the property nor the passed default are numbers.
 *
 * @param {Number} [max]
 *  If specified and a number, set an upper bound for the returned value.
 *  Is not used, if neither the property nor the passed default are
 *  numbers.
 *
 * @returns {Any}
 *  The value of the specified property, or the default value, rounded to
 *  an integer.
 */
export function getIntegerOption(options, name, def, min, max) {
    return getNumberOption(options, name, def, min, max, 1);
}

/**
 * Extracts an object property from the passed object. If the property does
 * not exist, or is not an object, returns the specified default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not an object. May be any value (not only objects).
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getObjectOption(options, name, def) {
    var value = getOption(options, name);
    return (_.isObject(value) && !_.isFunction(value) && !_.isArray(value) && !_.isRegExp(value)) ? value : def;
}

/**
 * Extracts a function from the passed object. If the property does not
 * exist, or is not a function, returns the specified default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not an object. May be any value (not only functions).
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getFunctionOption(options, name, def) {
    var value = getOption(options, name);
    return _.isFunction(value) ? value : def;
}

/**
 * Extracts a array from the passed object. If the property does not exist,
 * or is not an array, returns the specified default value.
 *
 * @param {Object|Undefined} options
 *  An object containing some properties. May be undefined.
 *
 * @param {String} name
 *  The name of the property to be returned.
 *
 * @param {Any} [def]
 *  The default value returned when the options parameter is not an object,
 *  or if it does not contain the specified property, or if the property is
 *  not an array. May be any value.
 *
 * @param {Boolean} [nonEmpty=false]
 *  If set to true, only non-empty arrays will be returned from the options
 *  object. Empty arrays will be replaced with the specified default value.
 *
 * @returns {Any}
 *  The value of the specified property, or the default value.
 */
export function getArrayOption(options, name, def, nonEmpty) {
    var value = getOption(options, name);
    return (_.isArray(value) && (!nonEmpty || (value.length > 0))) ? value : def;
}

/**
 * Creates and returns a merged options map from the passed objects. Unlike
 * Underscore's extend() method, does not modify the passed objects, but
 * creates and returns a clone. Additionally, extends embedded plain JS
 * objects deeply instead of replacing them, for example, extending the
 * objects {a:{b:1}} and {a:{c:2}} will result in {a:{b:1,c:2}}.
 *
 * @param {Object[]} args
 *  One or more objects whose properties will be inserted into the resulting
 *  object.
 *
 * @returns {Object}
 *  A new object containing all properties of the passed objects.
 */
export function extendOptions(...args) {

    var // the resulting options
        result = {};

    function extend(options, extensions) {
        _.each(extensions, function (value, name) {
            if (is.dict(value)) {
                // extension value is a plain object: ensure that the options map contains an embedded object
                if (!is.dict(options[name])) {
                    options[name] = {};
                }
                extend(options[name], value);
            } else {
                // extension value is not a plain object: clear old value, even if it was an object
                options[name] = value;
            }
        });
    }

    // add all objects to the clone
    for (const arg of args) {
        if (_.isObject(arg)) {
            extend(result, arg);
        }
    }

    return result;
}

// generic DOM/CSS helpers ------------------------------------------------

/**
 * A jQuery function selector that returns true if the DOM node bound to
 * the 'this' symbol is a text node. Can be used in all helper functions
 * that expect a jQuery selector including functions.
 */
export const JQ_TEXTNODE_SELECTOR = function () { return this.nodeType === 3; };

/**
 * Converts the passed object to a DOM node object.
 *
 * @param {Node|jQuery} node
 *  If the object is a DOM node object, returns it unmodified. If the
 *  object is a jQuery collection, returns its first node.
 *
 * @returns {Node}
 *  The DOM node object.
 */
export function getDomNode(node) {
    return (node instanceof $) ? node.get(0) : node;
}

/**
 * Returns whether the passed node is a specific DOM element node.
 *
 * @param {Node|jQuery|Null|Undefined} node
 *  The DOM node to be checked. May be null or undefined.
 *
 * @param {String|Function|Node|jQuery} [selector]
 *  A jQuery selector that can be used to check the passed node for a
 *  specific type etc. The selector will be passed to the jQuery method
 *  jQuery.is(). If this selector is a function, it will be called with the
 *  DOM node bound to the symbol 'this'. See the jQuery API documentation
 *  at http://api.jquery.com/is for details.
 *
 * @returns {Boolean}
 *  Whether the passed node is an element node that matches the passed
 *  selector.
 */
export function isElementNode(node, selector) {
    if (!node) { return false; }
    return (getDomNode(node).nodeType === 1) && (!selector || $(node).is(selector));
}

/**
 * Returns the lower-case name of a DOM node object.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose name will be returned. If this object is a jQuery
 *  collection, uses the first node it contains.
 *
 * @returns {String}
 *  The lower-case name of the DOM node object. If the node is a text node,
 *  the string '#text' will be returned.
 */
export function getNodeName(node) {
    return getDomNode(node).nodeName.toLowerCase();
}

/**
 * Returns an integer indicating how the two passed nodes are located to
 * each other.
 *
 * @param {Node|jQuery} node1
 *  The first DOM node tested if it is located before the second node. If
 *  this object is a jQuery collection, uses the first node it contains.
 *
 * @param {Node|jQuery} node2
 *  The second DOM node. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @returns {Number}
 *  The value zero, if the nodes are equal, a negative number, if node1
 *  precedes or contains node2, or a positive number, if node2 precedes or
 *  contains node1.
 */
export function compareNodes(node1, node2) {
    node1 = getDomNode(node1);
    node2 = getDomNode(node2);
    return (node1 === node2) ? 0 : ((node1.compareDocumentPosition(node2) & 4) === 4) ? -1 : 1;
}

/**
 * Iterates over all descendant DOM nodes of the specified element.
 *
 * @param {HTMLElement|jQuery} element
 *  A DOM element object whose descendant nodes will be iterated. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param {Function} iterator
 *  The iterator function that will be called for every node. Receives the
 *  DOM node as first parameter. If the iterator returns the Utils.BREAK
 *  object, the iteration process will be stopped immediately. The iterator
 *  can remove the visited node descendants from the DOM. It is also
 *  allowed to remove any siblings of the visited node, as long as the
 *  visited node will not be removed itself.
 *
 * @param {Object} [context]
 *  If specified, the iterator will be called with this context (the symbol
 *  'this' will be bound to the context inside the iterator function).
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.children=false]
 *      If set to true, only direct child nodes will be visited.
 *  - {Boolean} [options.reverse=false]
 *      If set to true, the descendant nodes are visited in reversed order.
 *
 * @returns {Utils.BREAK | void}
 *  A reference to the Utils.BREAK object, if the iterator has returned
 *  Utils.BREAK to stop the iteration process, otherwise undefined.
 */
export function iterateDescendantNodes(element, iterator, context, options) {

    var // only child nodes
        childrenOnly = getBooleanOption(options, 'children', false),
        // iteration direction
        reverse = getBooleanOption(options, 'reverse', false),
        // visited node, and the next or previous sibling of the visited node
        childNode = null, nextSibling = null;

    // visit all child nodes
    element = getDomNode(element);
    for (childNode = reverse ? element.lastChild : element.firstChild; childNode; childNode = nextSibling) {

        // get next/previous sibling in case the iterator removes the node
        nextSibling = reverse ? childNode.previousSibling : childNode.nextSibling;

        // call iterator for child node; if it returns Utils.BREAK, exit loop and return too
        if (iterator.call(context, childNode) === BREAK) { return BREAK; }

        // iterate grand child nodes (only if the iterator did not remove the node from the DOM)
        if (containsNode(element, childNode)) {

            // refresh next sibling (iterator may have removed the old one, or may have inserted another one)
            nextSibling = reverse ? childNode.previousSibling : childNode.nextSibling;

            // if iterator for any descendant node returns Utils.BREAK, return too
            if (!childrenOnly && (childNode.nodeType === 1) && (iterateDescendantNodes(childNode, iterator, context, options) === BREAK)) {
                return BREAK;
            }
        }
    }
}

/**
 * Iterates over selected descendant DOM nodes of the specified element.
 *
 * @param {HTMLElement|jQuery} element
 *  A DOM element object whose descendant nodes will be iterated. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} selector
 *  A jQuery selector that will be used to decide whether to call the
 *  iterator function for the current DOM node. The selector will be passed
 *  to the jQuery method jQuery.is() for each node. If this selector is a
 *  function, it will be called with the current DOM node bound to the
 *  symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details.
 *
 * @param {Function} iterator
 *  The iterator function that will be called for every matching node.
 *  Receives the DOM node as first parameter. If the iterator returns the
 *  Utils.BREAK object, the iteration process will be stopped immediately.
 *  The iterator can remove visited nodes from the DOM.
 *
 * @param {Object} [context]
 *  If specified, the iterator will be called with this context (the symbol
 *  'this' will be bound to the context inside the iterator function).
 *
 * @param {Object} [options]
 *  Optional parameters. Supports all options that are supported by the
 *  method Utils.iterateDescendantNodes().
 *
 * @returns {Utils.BREAK|Undefined}
 *  A reference to the Utils.BREAK object, if the iterator has returned
 *  Utils.BREAK to stop the iteration process, otherwise undefined.
 */
export function iterateSelectedDescendantNodes(element, selector, iterator, context, options) {

    return iterateDescendantNodes(element, function (node) {
        if ($(node).is(selector) && (iterator.call(context, node) === BREAK)) {
            return BREAK;
        }
    }, context, options);
}

/**
 * Returns the first descendant DOM node in the specified element that
 * passes a truth test using the specified jQuery selector.
 *
 * @param {HTMLElement|jQuery} element
 *  A DOM element object whose descendant nodes will be searched. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} selector
 *  A jQuery selector that will be used to find the DOM node. The selector
 *  will be passed to the jQuery method jQuery.is() for each node. If this
 *  selector is a function, it will be called with the current DOM node
 *  bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details.
 *
 * @param {Object} [options]
 *  Optional parameters. Supports all options that are supported by the
 *  method Utils.iterateDescendantNodes().
 *
 * @returns {Node|Null}
 *  The first descendant DOM node that matches the passed selector. If no
 *  matching node has been found, returns null.
 */
export function findDescendantNode(element, selector, options) {

    var // the node to be returned
        resultNode = null;

    // find the first node passing the iterator test
    iterateSelectedDescendantNodes(element, selector, function (node) {
        resultNode = node;
        return BREAK;
    }, undefined, options);

    return resultNode;
}

/**
 * Returns a child node of the passed node, that is at a specific index in
 * the array of all matching child nodes.
 *
 * @param {HTMLElement|jQuery} element
 *  A DOM element object whose child nodes will be visited. If this object
 *  is a jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} selector
 *  A jQuery selector that will be used to decide which child nodes are
 *  matching while searching to the specified index. The selector will be
 *  passed to the jQuery method jQuery.is() for each node. If this selector
 *  is a function, it will be called with the current DOM node bound to the
 *  symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details.
 *
 * @param {Number} index
 *  The zero-based index of the child node in the set of child nodes
 *  matching the selector that will be returned.
 *
 * @returns {Node|Null}
 *  The 'index'-th child node that matches the selector; or null, if the
 *  index is outside the valid range.
 */
export function getSelectedChildNodeByIndex(element, selector, index) {

    var // the node to be returned
        resultNode = null;

    // find the 'index'-th matching child node
    iterateSelectedDescendantNodes(element, selector, function (node) {
        // node found: store and escape from loop
        if (index === 0) {
            resultNode = node;
            return BREAK;
        }
        index -= 1;
    }, undefined, { children: true });

    return resultNode;
}

/**
 * Finds the closest ancestor of the passed node that matches the specified
 * jQuery selector. If the specified node itself matches the selector, it
 * will be returned. In difference to the jQuery method jQuery.closest(),
 * the passed node selector can be a function.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The DOM root node that will not be left while searching for a matching
 *  node. If this object is a jQuery collection, uses the first node it
 *  contains.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose chain of ancestors will be searched for a matching
 *  node. Must be a descendant of the passed root node. If this object is a
 *  jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} selector
 *  A jQuery selector that will be used to find a matching DOM node. The
 *  selector will be passed to the jQuery method jQuery.is() for each node.
 *  If this selector is a function, it will be called with the current DOM
 *  node bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details.
 *
 * @returns {Node|Null}
 *  The passed node, or the first ancestor node that matches the passed
 *  selector, and is contained in or equal to the root node; or null, if no
 *  node has been found.
 */
export function findClosest(rootNode, node, selector) {

    rootNode = getDomNode(rootNode);
    node = getDomNode(node);

    do {
        if ($(node).is(selector)) { return node; }
        if (node === rootNode) { return null; }
        node = node.parentNode;
    } while (node);

    return null;
}

/**
 * Finds the farthest ancestor of the passed node that matches the
 * specified jQuery selector. If the specified node itself matches the
 * selector but none of its ancestors, the node will be returned itself. In
 * difference to most jQuery methods, the passed node selector can be a
 * function.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The DOM root node that will not be left while searching for matching
 *  nodes. If this object is a jQuery collection, uses the first node it
 *  contains.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose chain of ancestors will be searched for a matching
 *  node. Must be a descendant of the passed root node. If this object is a
 *  jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} selector
 *  A jQuery selector that will be used to find a matching DOM node. The
 *  selector will be passed to the jQuery method jQuery.is() for each node.
 *  If this selector is a function, it will be called with the current DOM
 *  node bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details.
 *
 * @returns {Node|Null}
 *  The passed node, or the farthest ancestor node that matches the passed
 *  selector, and is contained in or equal to the root node; or null, if no
 *  node has been found.
 */
export function findFarthest(rootNode, node, selector) {

    // the last found matching node
    var matchingNode = null;

    rootNode = getDomNode(rootNode);
    node = getDomNode(node);

    do {
        if ($(node).is(selector)) { matchingNode = node; }
        if (node === rootNode) { return matchingNode; }
        node = node.parentNode;
    } while (node);

    return null;
}

/**
 * Finds the closest previous sibling node of the passed node that matches
 * a jQuery selector.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose previous matching sibling will be returned. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} [selector]
 *  A jQuery selector that will be used to find a matching sibling. The
 *  selector will be passed to the jQuery method jQuery.is() for each node.
 *  If this selector is a function, it will be called with the current DOM
 *  node bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details. If omitted, this method returns
 *  the direct previous sibling of the passed node.
 *
 * @returns {Node|Null}
 *  The previous matching sibling of the passed node; or null, no previous
 *  sibling node has been found.
 */
export function findPreviousSiblingNode(node, selector) {
    node = getDomNode(node).previousSibling;
    if (!_.isUndefined(selector)) {
        while (node && !$(node).is(selector)) {
            node = node.previousSibling;
        }
    }
    return node;
}

/**
 * Finds the closest next sibling node of the passed node that matches a
 * jQuery selector.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose next matching sibling will be returned. If this
 *  object is a jQuery collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} [selector]
 *  A jQuery selector that will be used to find a matching sibling. The
 *  selector will be passed to the jQuery method jQuery.is() for each node.
 *  If this selector is a function, it will be called with the current DOM
 *  node bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details. If omitted, this method returns
 *  the direct next sibling of the passed node.
 *
 * @returns {Node|Null}
 *  The next matching sibling of the passed node; or null, no next sibling
 *  node has been found.
 */
export function findNextSiblingNode(node, selector) {
    node = getDomNode(node).nextSibling;
    if (!_.isUndefined(selector)) {
        while (node && !$(node).is(selector)) {
            node = node.nextSibling;
        }
    }
    return node;
}

/**
 * Finds a previous sibling node of the passed node in the DOM tree. If the
 * node is the first child of its parent, bubbles up the chain of parent
 * nodes and continues with the previous sibling of the first ancestor that
 * has more siblings. An optional jQuery selector can be specified to
 * search through the previous siblings until a matching node has been
 * found.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The DOM root node whose sub node tree will not be left when searching
 *  for the previous sibling node. If this object is a jQuery collection,
 *  uses the first node it contains.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose previous sibling will be returned. Must be a
 *  descendant of the passed root node. If this object is a jQuery
 *  collection, uses the first node it contains.
 *
 * @param {String|Function|Node|jQuery} [selector]
 *  A jQuery selector that will be used to find a DOM node. The selector
 *  will be passed to the jQuery method jQuery.is() for each node. If this
 *  selector is a function, it will be called with the current DOM node
 *  bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details. If omitted, this method returns
 *  the direct next sibling of the passed node.
 *
 * @param {String|Function|Node|jQuery} [skipSiblingsSelector]
 *  A jQuery selector that will be used to to identify DOM nodes whose
 *  siblings will be ignored completely, even if they match the specified
 *  selector. If omitted, this method visits all descendant nodes of all
 *  siblings while searching for a matching node.
 *
 * @returns {Node|Null}
 *  The previous sibling of the passed node in the DOM sub tree of the root
 *  node; or null, no node has been found.
 */
export function findPreviousNode(rootNode, node, selector, skipSiblingsSelector) {

    // set 'node' to the previous sibling (of itself or one of its parents)
    function previousNode() {

        // go to previous sibling if existing
        if (node && node.previousSibling) {
            node = node.previousSibling;

            // start with deepest last descendant node in the previous sibling
            while (node && (_.isUndefined(skipSiblingsSelector) || !$(node).is(skipSiblingsSelector)) && node.lastChild) {
                node = node.lastChild;
            }
        } else {
            // otherwise, go to parent, but do not leave the root node
            node = (node.parentNode === rootNode) ? null : node.parentNode;
        }
    }

    rootNode = getDomNode(rootNode);
    node = getDomNode(node);

    previousNode();
    if (!_.isUndefined(selector)) {
        while (node && !$(node).is(selector)) { previousNode(); }
    }

    return node;
}

/**
 * Finds a next sibling node of the passed node in the DOM tree. If the
 * node is the last child of its parent, bubbles up the chain of parent
 * nodes and continues with the next sibling of the first ancestor that has
 * more siblings. An optional jQuery selector can be specified to search
 * through the next siblings until a matching node has been found.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The DOM root node whose sub node tree will not be left when searching
 *  for the next sibling node. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @param {Node|jQuery} node
 *  The DOM node whose next sibling will be returned. Must be a descendant
 *  of the passed root node. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @param {String|Function|Node|jQuery} [selector]
 *  A jQuery selector that will be used to find a DOM node. The selector
 *  will be passed to the jQuery method jQuery.is() for each node. If this
 *  selector is a function, it will be called with the current DOM node
 *  bound to the symbol 'this'. See the jQuery API documentation at
 *  http://api.jquery.com/is for details. If omitted, this method returns
 *  the direct next sibling of the passed node.
 *
 * @param {String|Function|Node|jQuery} [skipSiblingsSelector]
 *  A jQuery selector that will be used to to identify DOM nodes whose
 *  siblings will be ignored completely, even if they match the specified
 *  selector. If omitted, this method visits all descendant nodes of all
 *  siblings while searching for a matching node.
 *
 * @returns {Node|Null}
 *  The next sibling of the passed node in the DOM sub tree of the root
 *  node; or null, no node has been found.
 */
export function findNextNode(rootNode, node, selector, skipSiblingsSelector) {

    var // visit descendant nodes
        visitDescendants = false;

    // set 'node' to the next sibling (of itself or one of its parents)
    function nextNode() {

        // visit descendant nodes if specified
        if (visitDescendants && node && (_.isUndefined(skipSiblingsSelector) || !$(node).is(skipSiblingsSelector)) && node.firstChild) {
            node = node.firstChild;
            return;
        }

        // for next call: always visit descendant nodes of own or parent siblings
        visitDescendants = true;

        // find first node up the tree that has a next sibling
        while (node && !node.nextSibling) {
            // do not leave the root node
            node = (node.parentNode === rootNode) ? null : node.parentNode;
        }

        // go to that next sibling
        node = node && node.nextSibling;
    }

    rootNode = getDomNode(rootNode);
    node = getDomNode(node);

    nextNode();
    if (!_.isUndefined(selector)) {
        while (node && !$(node).is(selector)) { nextNode(); }
    }

    return node;
}

/**
 * Checking if a specified node is the last descendant of a specified root
 * node. The last descendant means, that the current node and all its
 * parents until the root node do not have a next sibling.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  The DOM root node whose sub node tree will not be left when searching
 *  for the next sibling node. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @param {Node|jQuery} node
 *  The DOM node, that is checked, if it is the last descendant of the passed
 *  root node. Must be a descendant this root node. If this object is a
 *  jQuery collection, uses the first node it contains.
 *
 * @returns {Boolean}
 *  Whether the passed node is the last descendant of the passed root node.
 */
export function isLastDescendant(rootNode, node) {
    return findNextNode(rootNode, node) === null;
}

// positioning and scrolling ----------------------------------------------

/**
 * Returns whether the passed DOM element contains a visible vertical
 * scroll bar.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @returns {Boolean}
 *  Whether the DOM element contains a visible vertical scroll bar.
 */
export function hasVerticalScrollBar(node) {
    return $(node).width() > getDomNode(node).clientWidth;
}

/**
 * Returns whether the passed DOM element contains a visible horizontal
 * scroll bar.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @returns {Boolean}
 *  Whether the DOM element contains a visible horizontal scroll bar.
 */
export function hasHorizontalScrollBar(node) {
    return $(node).height() > getDomNode(node).clientHeight;
}

/**
 * Returns the size of the entire document page.
 *
 * @returns {Object}
 *  An object with numeric properties representing the size of the document
 *  page, in pixels:
 *  - {Number} width
 *      The width of the document page.
 *  - {Number} height
 *      The height of the document page.
 */
export function getPageSize() {

    // the OX root node (<body> element does NOT cover the entire browser window)
    var coreNode = $('#io-ox-core')[0];

    return { width: coreNode.offsetWidth, height: coreNode.offsetHeight };
}

/**
 * Returns the position and size of the specified node inside the entire
 * document page. This includes the distances of all four borders of the
 * node to the borders of the document.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element whose position relative to the document page will be
 *  calculated. If this object is a jQuery collection, uses the first node
 *  it contains.
 *
 * @returns {Object}
 *  An object with numeric properties representing the position and size of
 *  the node relative to the document page, in pixels:
 *  - {Number} left
 *      The distance of the left border of the node to the left border of
 *      the document page.
 *  - {Number} top
 *      The distance of the top border of the node to the top border of the
 *      document page.
 *  - {Number} right
 *      The distance of the right border of the node to the right border of
 *      the document page.
 *  - {Number} bottom
 *      The distance of the bottom border of the node to the bottom border
 *      of the document page.
 *  - {Number} width
 *      The outer width of the node (including its borders).
 *  - {Number} height
 *      The outer height of the node (including its borders).
 */
export function getNodePositionInPage(node) {

    // the passed node, as jQuery object
    var $node = $(node);
    // the offset of the node, relative to the browser window
    var position = $node.offset();
    // the size of the entire document page
    var pageSize = getPageSize();

    position.width = $node.outerWidth();
    position.height = $node.outerHeight();
    position.right = pageSize.width - position.left - position.width;
    position.bottom = pageSize.height - position.top - position.height;

    return position;
}

/**
 * Returns the position and size of the client area of the specified node
 * inside the entire document page. This includes the distances of all four
 * borders of the client area to the borders of the document.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element whose client area position relative to the document
 *  page will be calculated. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @returns {Object}
 *  An object with numeric properties representing the position and size of
 *  the client area of the node relative to the document page, in pixels:
 *  - {Number} left
 *      The distance of the left border of the node's client area to the
 *      left border of the document page.
 *  - {Number} top
 *      The distance of the top border of the node's client area to the top
 *      border of the document page.
 *  - {Number} right
 *      The distance of the right border of the node's client area to the
 *      right border of the document page.
 *  - {Number} bottom
 *      The distance of the bottom border of the node's client area to the
 *      bottom border of the document page.
 *  - {Number} width
 *      The width of the node's client area.
 *  - {Number} height
 *      The height of the node's client area.
 */
export function getClientPositionInPage(node) {

    // the passed node, as jQuery object
    var $node = $(node);
    // the offset of the node, relative to the browser window
    var position = $node.offset();
    // the size of the entire document page
    var pageSize = getPageSize();

    position.left += parseCssLength($node, 'borderLeftWidth');
    position.top += parseCssLength($node, 'borderTopWidth');
    position.width = $node[0].clientWidth;
    position.height = $node[0].clientHeight;
    position.right = pageSize.width - position.left - position.width;
    position.bottom = pageSize.height - position.top - position.height;

    return position;
}

/**
 * Returns the position of the visible area of the passed container node.
 * This includes the size of the visible area without scroll bars (if
 * shown), and the distances of all four borders of the visible area to the
 * borders of the entire scroll area.
 *
 * @param {HTMLElement|jQuery} containerNode
 *  The DOM container element. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @returns {Object}
 *  An object with numeric properties representing the position and size of
 *  the visible area relative to the entire scroll area of the container
 *  node in pixels:
 *  - {Number} left
 *      The distance of the left border of the visible area to the left
 *      border of the entire scroll area.
 *  - {Number} top
 *      The distance of the top border of the visible area to the top
 *      border of the entire scroll area.
 *  - {Number} right
 *      The distance of the right border of the visible area (without
 *      scroll bar) to the right border of the entire scroll area.
 *  - {Number} bottom
 *      The distance of the bottom border of the visible area (without
 *      scroll bar) to the bottom border of the entire scroll area.
 *  - {Number} width
 *      The width of the visible area (without scroll bar).
 *  - {Number} height
 *      The height of the visible area (without scroll bar).
 */
export function getVisibleAreaPosition(containerNode, options) {

    containerNode = getDomNode(containerNode);

    var clientHeight = containerNode.clientHeight;
    if (getBooleanOption(options, 'regardSoftkeyboard', false)) {
        //here comes the special code for softkeyboards
        //if the touch lands in the lower area we fake that its outsice of the client-rect,
        //so we scroll before the browser scroll the complete body
        clientHeight -= getSoftKeyboardHeight();
    }

    return {
        left: containerNode.scrollLeft,
        top: containerNode.scrollTop,
        right: containerNode.scrollWidth - containerNode.clientWidth - containerNode.scrollLeft,
        bottom: containerNode.scrollHeight - clientHeight - containerNode.scrollTop,
        width: containerNode.clientWidth,
        height: clientHeight
    };
}

/**
 * Returns the position and size of the specified document page rectangle
 * inside the passed DOM container node. This includes the size of the
 * rectangle as specified, and the distances of all four borders of the
 * rectangle to the borders of the visible area or entire scroll area of
 * the container node.
 *
 * @param {HTMLElement|jQuery} containerNode
 *  The container DOM element. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @param {Object} pageRect
 *  The document page rectangle whose position will be converted relatively
 *  to the position of the container node. Must provide the properties
 *  'left', 'top', 'width', and 'height' in pixels. The properties 'left'
 *  and 'top' are interpreted relatively to the entire document page.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.visibleArea=false]
 *      If set to true, calculates the distances of the rectangle to the
 *      visible area of the container node. Otherwise, calculates the
 *      distances of the rectangle to the entire scroll area of the
 *      container node.
 *
 * @returns {Object}
 *  An object with numeric properties representing the position and size of
 *  the page rectangle relative to the entire scroll area or visible area
 *  of the container node, in pixels:
 *  - {Number} left
 *      The distance of the left border of the rectangle to the left border
 *      of the container node.
 *  - {Number} top
 *      The distance of the top border of the rectangle to the top border
 *      of the container node.
 *  - {Number} right
 *      The distance of the right border of the rectangle to the right
 *      border of the container node.
 *  - {Number} bottom
 *      The distance of the bottom border of the rectangle to the bottom
 *      border of the container node.
 *  - {Number} width
 *      The width of the rectangle, as passed.
 *  - {Number} height
 *      The height of the rectangle, as passed.
 */
export function getRectanglePositionInNode(containerNode, pageRect, options) {

    var // the offset of the container node, relative to the document body
        containerOffset = $(containerNode).offset(),
        // the width of the left and top border of the scrollable node, in pixels
        leftBorderWidth = parseCssLength(containerNode, 'borderLeftWidth'),
        topBorderWidth = parseCssLength(containerNode, 'borderTopWidth'),
        // position and size of the visible area of the container node
        visiblePosition = getVisibleAreaPosition(containerNode, options),

        // the position and size, relative to the visible area of the container node
        position = {
            left: pageRect.left + leftBorderWidth - containerOffset.left,
            top: pageRect.top + topBorderWidth - containerOffset.top,
            width: pageRect.width,
            height: pageRect.height
        };

    // add right and bottom distance of child node to visible area
    position.right = visiblePosition.width - position.left - position.width;
    position.bottom = visiblePosition.height - position.top - position.height;

    // add distances to entire scroll area, if option 'visibleArea' is not set
    if (!getBooleanOption(options, 'visibleArea', false)) {
        _.each(['left', 'top', 'right', 'bottom'], function (border) {
            position[border] += visiblePosition[border];
        });
    }

    return position;
}

/**
 * Returns the position and size of the specified child node inside its
 * ancestor container node. This includes the outer size of the child node,
 * and the distances of all four borders of the child node to the borders
 * of the visible area or entire scroll area of the container node.
 *
 * @param {HTMLElement|jQuery} containerNode
 *  The DOM container element. If this object is a jQuery collection, uses
 *  the first node it contains.
 *
 * @param {HTMLElement|jQuery} childNode
 *  The DOM element whose dimensions will be calculated. Must be contained
 *  in the specified container element. If this object is a jQuery
 *  collection, uses the first node it contains.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.visibleArea=false]
 *      If set to true, calculates the distances of the child node to the
 *      visible area of the container node. Otherwise, calculates the
 *      distances of the child node to the entire scroll area of the
 *      container node.
 *
 * @returns {Object}
 *  An object with numeric properties representing the position and size of
 *  the child node relative to the entire scroll area or visible area of
 *  the container node, in pixels:
 *  - {Number} left
 *      The distance of the left border of the child node to the left
 *      border of the container node.
 *  - {Number} top
 *      The distance of the top border of the child node to the top border
 *      of the container node.
 *  - {Number} right
 *      The distance of the right border of the child node to the right
 *      border of the container node.
 *  - {Number} bottom
 *      The distance of the bottom border of the child node to the bottom
 *      border of the container node.
 *  - {Number} width
 *      The outer width of the child node (including its borders).
 *  - {Number} height
 *      The outer height of the child node (including its borders).
 */
export function getChildNodePositionInNode(containerNode, childNode, options) {
    var pageRect = getNodePositionInPage(childNode);
    return getRectanglePositionInNode(containerNode, pageRect, options);
}

/**
 * Scrolls the passed page rectangle into the visible area of the specified
 * container node.
 *
 * @param {HTMLElement|jQuery} containerNode
 *  The DOM container element. If this object is a jQuery collection, uses
 *  the first node it contains. This method works independent from the
 *  current overflow mode of the container node.
 *
 * @param {Object} pageRect
 *  The document page rectangle that will be made visible by scrolling the
 *  container node. Must provide the properties 'left', 'top', 'width', and
 *  'height' in pixels. The properties 'left' and 'top' are interpreted
 *  relatively to the entire document page.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Number} [options.padding=0]
 *      Minimum distance between the borders of the visible area in the
 *      container node, and the rectangle scrolled into the visible area.
 *  - {Boolean} [options.forceToTop=false]
 *      Whether the specified pageRect shall be at the top border of the
 *      visible area of the scrolling node.
 */
export function scrollToPageRectangle(containerNode, pageRect, options) {

    var // dimensions of the rectangle in the visible area of the scrollable node
        position = getRectanglePositionInNode(containerNode, pageRect, { visibleArea: true, regardSoftkeyboard: getBooleanOption(options, 'regardSoftkeyboard', false) }),
        // padding between scrolled element and border of visible area
        padding = getIntegerOption(options, 'padding', 0, 0);

    function updateScrollPosition(leadingChildOffset, trailingChildOffset, scrollAttributeName, opts) {

        var maxPadding = math.clamp((leadingChildOffset + trailingChildOffset) / 2, 0, padding),
            offset = Math.max(leadingChildOffset - Math.max(maxPadding - trailingChildOffset, 0), maxPadding);

        if (getBooleanOption(opts, 'forceToTop', false)) {
            getDomNode(containerNode)[scrollAttributeName] = leadingChildOffset + padding;
        } else {
            getDomNode(containerNode)[scrollAttributeName] -= (offset - leadingChildOffset);
        }
    }

    updateScrollPosition(position.left, position.right, 'scrollLeft');
    updateScrollPosition(position.top, position.bottom, 'scrollTop', { forceToTop: getBooleanOption(options, 'forceToTop', false) });
}

/**
 * Scrolls the passed child node into the visible area of the specified DOM
 * container node.
 *
 * @param {HTMLElement|jQuery} containerNode
 *  The DOM container element that contains the specified child node. If
 *  this object is a jQuery collection, uses the first node it contains.
 *
 * @param {HTMLElement|jQuery} childNode
 *  The DOM element that will be made visible by scrolling the specified
 *  container element. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Number} [options.padding=0]
 *      Minimum distance between the borders of the visible area and the
 *      child node.
 *  - {Boolean} [options.forceToTop=false]
 *      Whether the specified node shall be at the top border of the
 *      visible area of the scrolling node.
 */
export function scrollToChildNode(containerNode, childNode, options) {
    var pageRect = getNodePositionInPage(childNode);
    scrollToPageRectangle(containerNode, pageRect, options);
}

// simulating function caretRangeFromPoint --------------------------------

/**
 * Simulates the method document.caretRangeFromPoint() for all browsers.
 *
 * @param {Number} x
 *  The horizontal coordinate corresponding to the current window.
 *
 * @param {Number} y
 *  The vertical coordinate corresponding to the current window.
 *
 * @returns {TextRange}
 *  The range object specified by two coordinates.
 */
export const caretRangeFromPoint = (function () {

    // poly-fill document.caretRangeFromPoint if not available
    if (!_.isFunction(document.caretRangeFromPoint)) {

        // FireFox supports a similar method 'caretPositionFromPoint'
        if (_.isFunction(document.caretPositionFromPoint)) {

            document.caretRangeFromPoint = function (x, y) {

                var range = document.createRange(),
                    point = document.caretPositionFromPoint(x, y);

                // Bug 1025815 in Mozilla: If the zoom factor is not 100%, the calculated offset is wrong.
                // The node is always correct.

                if (point.offsetNode) {
                    range.setStart(point.offsetNode, point.offset);
                    range.setEnd(point.offsetNode, point.offset);
                }

                return range;
            };

        } else if ((document.body || document.createElement('body')).createTextRange) {

            document.caretRangeFromPoint = function (x, y) {

                var // try to create a text range at the specified location
                    range = document.body.createTextRange(),
                    // the element received from a specified point
                    elem = null;

                try {
                    elem = document.elementFromPoint(x - 1, y - 1);
                    range = document.createRange();
                    range.setStartAfter(elem);
                    range._usedElementFromePoint_ = true; // marking the range for further evaluation
                    return range;
                } catch {
                    return null;
                }
            };
        }
    }

    return function (x, y) {
        return document.caretRangeFromPoint(x, y);
    };
}());

// device screen and keyboard ---------------------------------------------

/**
 * return true if a standard soft keyboard is opened.
 *
 * adding a fixed positioned transparent element as a child of the body,
 * it is going to keep the track of the parent using its offsetTop property
 *
 */
export function isSoftKeyboardOpen() {
    var mobileScrollDetector = null;
    var keyboard_shown = false;
    var offsetTop = 0;

    if (TOUCH_DEVICE) {
        mobileScrollDetector = $('<div class="mobile-scroll-detector" style="position: fixed; top: 0;" />');
        $('body').append(mobileScrollDetector);

        offsetTop = mobileScrollDetector.offset().top;
        keyboard_shown = offsetTop < 0 && true;

        mobileScrollDetector.remove();
    }

    return keyboard_shown;
}

/**
 * return the estimated keyboard height for touch devices.
 *
 *  => used only for/by IOS
 */
export function getSoftKeyboardHeight() {
    var relKeyHeight = 0,
        screenHeight = getScreenHeight();

    // cellphone in portrait-mode
    if (SMALL_DEVICE && isPortrait()) {

        if (screenHeight <= 568) {          // iPhone SE, iPhone 5, iPhone 5s
            relKeyHeight = 1 / 2.3;
        } else if (screenHeight <= 667) {   // iPhone 6, iPhone 6s, iPhone 7
            relKeyHeight = 1 / 2.6;
        } else if (screenHeight > 667) {    // iPhone 6+, iPhone 6s+, iPhone 7+
            relKeyHeight = 1 / 2.8;
        }

    // cell-phone OR any device in landscape-mode
    } else if (isLandscape() || SMALL_DEVICE) {
        relKeyHeight = 1.1 / 2;

    // tablet in portrait-mode
    } else {
        relKeyHeight = 1 / 3;
    }

    return Math.round(relKeyHeight * screenHeight);
}

/**
 * returns true if Device is Small Device or
 * Tablet is in nn Landscape Mode
 */
export function shouldHideSoftKeyboard() {
    if (_.browser.iOS || _.browser.Android) {
        if (SMALL_DEVICE || isLandscape()) {
            var popupNode = $('body>.io-ox-office-main.popup-container');
            // when there is a popup-container and it's not a context menu
            // the softKeyboard should be hidden
            if (popupNode.length && !(popupNode.hasClass('context-menu'))) {
                return true;
            }
        }
    }
    return false;
}

/**
 *  Wether a key down event was registered at the DOM document within a time of n milliseconds.
 *
 * @param {Number} ms
 *  Time in milliseconds.
 *
 * @returns {Boolean}
 *  Whether a key down was registered at the DOM document within a time of n milliseconds.
 */
export function wasKeyPressedWithinTime(ms) {
    //    console.log('inactivCheck KEYS.... ', Utils.deltaToCurrentTime(cachedKeyEvent.timeStamp), cachedKeyEvent.timeStamp);
    return cachedKeyEvent !== null && deltaToCurrentTime(cachedKeyEvent.timeStamp) <= ms;
}

/**
 * Detects if a given timestamp is a DOMHighResTimeStamp or DOMTimeStamp (they have a
 * different time origin).
 *
 * Note:
 * This function is useful when event.timeStamps are used. At the time (2017),
 * Chrome and Firefox provide DOMHighResTimeStamp and all other brothers DOMTimeStamp.
 *
 * @param {Number} timeStamp
 *  A timeStamp as a Number.
 *
 * @returns {Boolean}
 *  Whether the timeStamp is a DOMHighResTimeStamp or DOMTimeStamp.
 */
export function detectHighResTimeStamp(timeStamp) {
    // We have:
    // - DOMHighResTimeStamp, with a time origin based on the time when the browsing context is first created (have a look at the w3c spec).
    // - DOMTimeStamp, with a time origin based on Unix time, so it shown the time elapsed since 1970.
    //
    // When the absolute value from a given timeStamp is smaller than 631139040000ms (= 20 years), we can
    // be pretty sure that it's a DOMHighResTimeStamp, because it's very unlikely that a browser context
    // is created before 20 years. In comparison, a timestamps based on unix time must be greater than 47 years (we have 2017)
    // when the system time is correct. In case it's wrong, we still have a safety margin from about 17+ years (we have 2017),
    // because it's just checked against 20 years and not against the current time.

    // 631139040000ms = 20 years
    return timeStamp < 631139040000;
}

/**
 * Returns the difference between a given timeStamp and the current time.
 *
 * @param {Number} timeStamp
 *  A timeStamp as a Number.
 *
 * @returns {Number}
 *  The difference between the timeStamp and the current time.
 */
export function deltaToCurrentTime(timeStamp) {
    // use the appropriate getter for the current time (which must have the same time origin) as the timeStamp
    return detectHighResTimeStamp(timeStamp) ? window.performance.now() - timeStamp : _.now() - timeStamp;
}

/**
 * Returns if a 'contextmenu' event was invoked by keyboard.
 *
 * @param {event} contextEvent
 *  A 'contextmenu' event in the handler where this function is used.
 *
 * @returns {Boolean}
 *  Whether the 'contextmenu' event was triggered by keyboard.
 */
export function isContextEventTriggeredByKeyboard(contextEvent) {
    // Detect if the context event was invoked by keyboard -> when the last keydown event before the
    // context event matches the following conditions it's considered as a keyboard invoked context event:
    // The event is not null and the last key was the 'context' key (93) or f10 (121) and the key event
    // was invoked max. 600 ms before this context event (so the key was pressed directly before the context event).
    //
    // Bug 49870: The time was raised from 150ms to 600ms, because there are some seemingly random invoked context events
    // that have a much higher delay (especially in firefox). In firefox the timeStamp from the keyup and context event are
    // equal (at least at the time of testing), but in other browsers they are not. So beside this advantage in firefox, using
    // the keyup event, for which a additional global listener must be implemented with all performance disadvantages,
    // is not much beneficial to use, since keydown events are also fired while the key is pressed for a longer time by the user.
    return (cachedKeyEvent !== null) && (cachedKeyEvent.keyCode === 93 || cachedKeyEvent.keyCode === 121) && (contextEvent.timeStamp - cachedKeyEvent.timeStamp < 600);
}

// global initialization ==================================================

var cachedKeyEvent = null;
document.addEventListener('keydown', function (keyEvent) { cachedKeyEvent = keyEvent; }, true);
