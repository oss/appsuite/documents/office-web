/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * Represents possible encoding types for input strings.
 */
type Sha256InputEncoding  = "base64" | "utf-8";

// static functions ===========================================================

function base64ToUint8Array(base64Msg: string): Uint8Array {
    return Uint8Array.from(atob(base64Msg), m => m.codePointAt(0)!);
}

function utf8ToUint8Array(utf8Msg: string): Uint8Array {
    // encode as (utf-8) Uint8Array
    return new TextEncoder().encode(utf8Msg);
}

// public functions ===========================================================
/**
 * Creates a sha256 hash for a given message.
 *
 * @param message
 *  The message to be hashed.
 *
 * @param messageEncoding
 *  The encoding of the message.
 *
 * @returns
 *  The sha256 hash of the input message represented as hex string.
 */
export async function sha256(message: string, messageEncoding: Sha256InputEncoding): Promise<string> {
    let msgUint8: Uint8Array;
    if (messageEncoding === "utf-8") { msgUint8 = utf8ToUint8Array(message); }
    if (messageEncoding === "base64") { msgUint8 = base64ToUint8Array(message); }

    const hash = await crypto.subtle.digest("SHA-256", msgUint8!);
    // https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest#converting_a_digest_to_a_hex_string
    const hashByteArray = Array.from(new Uint8Array(hash));
    // convert bytes to hex string
    const hashHex = hashByteArray
      .map(b => b.toString(16).padStart(2, "0"))
      .join("");
    return hashHex;
}
