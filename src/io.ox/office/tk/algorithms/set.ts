/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// public functions ===========================================================

// set generators -------------------------------------------------------------

/**
 * Creates a new set from a data source and a mapping function. Note that
 * creating a set from a data source without mapping function is possible
 * directly with the `Set` constructor.
 *
 * @param source
 *  The data source to be converted to a set.
 *
 * @param fn
 *  A mapper function that converts the source values to the set elements. If
 *  the function returns `undefined`, the source value will be skipped.
 *
 * @returns
 *  The new set with the return values of the mapper function.
 */
export function from<RT, VT>(source: Iterable<VT>, fn: (value: VT) => Opt<RT>): Set<RT> {
    const set = new Set<RT>();
    for (const value of source) {
        const result = fn(value);
        if (result !== undefined) { set.add(result); }
    }
    return set;
}

/**
 * Creates a new set from matching values of a data source.
 *
 * @param source
 *  The data source to be converted to a set.
 *
 * @param fn
 *  The predicate callback function invoked for the source values.
 *
 * @returns
 *  A new set with all matching values from the data source.
 */
export function filterFrom<VT>(source: Iterable<VT>, fn: (value: VT) => boolean): Set<VT> {
    const set = new Set<VT>();
    for (const value of source) {
        if (fn(value)) { set.add(value); }
    }
    return set;
}

/**
 * Creates a new set from the results of a custom generator function.
 *
 * @param fn
 *  The generator function to be invoked immediately.
 *
 * @param [context]
 *  The calling context for the callback generator function (which cannot be
 *  written as arrow function).
 *
 * @returns
 *  A new set with all values yielded by the generator function.
 */
export function yieldFrom<VT>(fn: FuncType<Iterable<VT>>, context?: object): Set<VT> {
    return new Set(context ? fn.call(context) : fn());
}

/**
 * Destroys all elements in the passed set, and clears the set afterwards.
 *
 * @param set
 *  The set to be destroyed and cleared.
 */
export function destroy<VT extends Destroyable>(set: Set<VT>): void {
    for (const value of set) { value.destroy(); }
    set.clear();
}

// element access -------------------------------------------------------------

/**
 * Toggles a value in a set.
 *
 * @param set
 *  The set to be manipulated.
 *
 * @param value
 *  The value to be toggled in the set.
 *
 * @param state
 *  If specified, inserts (`true`), or removes (`false`) the value. If omitted,
 *  toggles the value (inserts it if not inside, or removes it if inside the
 *  set).
 *
 * @returns
 *  Whether the state of the value has been changed (i.e., when inserting, was
 *  not in the set before; or when deleting, was in the set before).
 */
export function toggle<VT>(set: Set<VT>, value: VT, state?: boolean): boolean;
export function toggle<VT extends object>(set: WeakSet<VT>, value: VT, state?: boolean): boolean;
// implementation
export function toggle<VT>(set: Set<VT> | WeakSet<any>, value: VT, state?: boolean): boolean {

    // resolve old and new flags
    const oldFlag = set.has(value);
    const newFlag = (state === true) || (!oldFlag && (state !== false));
    if (oldFlag === newFlag) { return false; }

    // toggle the specified flag
    if (newFlag) { set.add(value); } else { set.delete(value); }
    return true;
}

/**
 * Inserts the values of one or more data sources into a set. Works similar to
 * `Object.assign` but for native sets.
 *
 * @param set
 *  The set to be extended.
 *
 * @param sources
 *  The data sources to be assigned to the set.
 *
 * @returns
 *  The extended set passed as first parameter.
 */
export function assign<VT>(set: Set<VT>, ...sources: Array<Nullable<Iterable<VT>>>): Set<VT>;
export function assign<VT extends object>(set: WeakSet<VT>, ...sources: Array<Nullable<Iterable<VT>>>): WeakSet<VT>;
// implementation
export function assign<VT>(set: Set<VT> | WeakSet<any>, ...sources: Array<Nullable<Iterable<VT>>>): Set<VT> | WeakSet<any> {
    for (const source of sources) {
        if (source) {
            for (const value of source) {
                set.add(value);
            }
        }
    }
    return set;
}

// element comparison ---------------------------------------------------------

/**
 * Returns whether the passed sets contain the exact same elements. Insertion
 * orders in the sets do not matter.
 *
 * @param set1
 *  The first set that will be compared to the second set.
 *
 * @param set2
 *  The second set that will be compared to the first set.
 *
 * @returns
 *  Whether the two sets contain equal elements.
 */
export function equals<VT>(set1: Set<VT>, set2: Set<VT>): boolean {

    // references to the same set
    if (set1 === set2) { return true; }

    // compare set size first
    if (set1.size !== set2.size) { return false; }

    // check all set elements
    for (const value of set1) {
        if (!set2.has(value)) { return false; }
    }

    return true;
}

/**
 * Returns whether the second set contains all elements of the first set, i.e.
 * whether the first set is a subset of the second set (including equality).
 *
 * @param set1
 *  The first set expected to b a subset of the second set.
 *
 * @param set2
 *  The second set expected to b a superset of the first set.
 *
 * @returns
 *  Whether the two sets contain equal elements.
 */
export function subset<VT>(set1: Set<VT>, set2: Set<VT>): boolean {

    // references to the same set
    if (set1 === set2) { return true; }

    // check all elements of the subset
    for (const value of set1) {
        if (!set2.has(value)) { return false; }
    }

    return true;
}

// find algorithms ------------------------------------------------------------

/**
 * Returns the first value in the set that passed a truth test.
 *
 * @param set
 *  The set to be searched for a matching value.
 *
 * @param fn
 *  The predicate callback function invoked for the set elements.
 *
 * @returns
 *  The value of the first matching set element (in insertion order); or
 *  `undefined`, if no set element has been found.
 */
export function find<VT>(set: Set<VT>, fn: (value: VT) => boolean): Opt<VT> {
    for (const value of set) {
        if (fn(value)) { return value; }
    }
    return undefined;
}

// draining generators --------------------------------------------------------

/**
 * Creates a destructive iterator that shifts the values from the passed set.
 *
 * @param set
 *  The set to be iterated. It will be emptied inplace during iteration.
 *
 * @yields
 *  The values of the set.
 */
export function *shiftValues<VT>(set: Set<VT>): IterableIterator<VT> {
    for (const value of set) {
        set.delete(value);
        yield value;
    }
}
