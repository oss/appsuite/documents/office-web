/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as fun from "@/io.ox/office/tk/algorithms/function";

// types ======================================================================

/**
 * Options for formatting integer numbers to strings.
 */
export interface FormatLetterOptions {

    /**
     * If set to `true`, the resulting formatted number will contain lower-case
     * letters. Default value is `false`.
     */
    lower?: boolean;
}

/**
 * Options for parsing strings to integer numbers.
 */
export interface ParseIntOptions {

    /**
     * If set to `true`, the passed text must not contain any leading sign
     * characters (neither "plus" nor "minus"). Default value is `false`.
     */
    unsigned?: boolean;
}

/**
 * Options for converting integer numbers to a specific radix.
 */
export interface FormatIntOptions extends ParseIntOptions, FormatLetterOptions {

    /**
     * The minimum number of digits in the formatted number (filled with zeros
     * on demand). Default value is `0`.
     */
    digits?: number;
}

/**
 * The conversion method to be used for alphabetic numbering.
 */
export interface ParseAlphabeticOptions {

    /**
     * - If omitted or set to `false`, the letters will be used as independent
     *   digits. The numbers `27` to `52` will be converted to the alphabetic
     *   numbers `AA` to `AZ` respectively. The numbers `53` to `78` will be
     *   converted to `BA` to `BZ`, and so on (up to `ZZ` for the number `702`,
     *   followed by `AAA`, `AAB`, ...).
     *
     * - If set to `true`, a single letter will be repeated in the result. The
     *   numbers `27` to `52` will be converted to the alphabetic numbers `AA`
     *   to `ZZ` respectively. The numbers `53` to `78` will be converted to
     *   the strings `AAA` to `ZZZ`, and so on.
     */
    repeat?: boolean;
}

/**
 * Options for converting numbers to alphabetic numbering.
 */
export interface FormatAlphabeticOptions extends ParseAlphabeticOptions, FormatLetterOptions { }

/**
 * The degree of simplification made for the decimal digits `4` and `9` when
 * creating roman numbers.
 *
 * - `-1`: The decimal digits `4` and `9` will not be simplified, e.g. the
 *   decimal number `499` will be converted to `"CCCCLXXXXVIIII"` ("4 times
 *   100, plus 50, plus 4 times 10, plus 5, plus 4 times 1").
 *
 * - `0`: The decimal digits `4` and `9` will be replaced with the respective
 *   roman "1-less-5" or "1-less-10" combination, e.g. the decimal number `499`
 *   will be converted to `"CDXCIX"` ("100 less than 500, plus 10 less than
 *   100, plus 1 less than 10").
 *
 * - `1`: The decimal numbers will be simplified using roman numbers with
 *   "1-less-" or "5-less-" combinations, e.g. the decimal number `499` will be
 *   converted to `"LDVLIV"` ("50 less than 500, plus 5 less than 50, plus 1
 *   less than 5").
 *
 * - `2`: The degree of simplification will be increased to the respective
 *   "1-less-50" or "1-less-100" combinations, e.g. the decimal number `499`
 *   will be converted to `"XDIX"` ("10 less than 500, plus 1 less than 10").
 *
 * - `3`: The degree of simplification will be increased to "5-less-500" or
 *   "5-less-1000" combinations, e.g. the decimal number `499` will be
 *   converted to `"VDIV"` ("5 less than 500, plus 1 less than 5").
 *
 * - `4`: The degree of simplification will be increased to "1-less-500" or
 *   "1-less-1000" combinations, e.g. the decimal number `499` will be
 *   converted to `"ID"` ("1 less than 500").
 */
export type RomanDepth = -1 | 0 | 1 | 2 | 3 | 4;

/**
 * The conversion method to be used for alphabetic numbering.
 */
export interface FormatRomanOptions extends FormatLetterOptions {

    /**
     * The degree of simplification made for the decimal digits `4` and `9`
     * when creating roman numbers. Default value is `0`.
     */
    depth?: RomanDepth;
}

// constants ==================================================================

// regular expression patterns for function `parseInt`
const PARSE_INT_RE_PATTERNS: Array<Opt<string>> = fun.do(() => {
    const array: Array<Opt<string>> = [undefined, undefined];
    for (let digit = 1; digit <= 9; digit += 1) {
        array.push(`[0-${digit}]+`);
    }
    for (let code = 65; code <= 90; code += 1) {
        array.push(`[0-9A-${String.fromCharCode(code)}]+`);
    }
    return array;
});

// regular expressions for function `parseInt` for unsigned integers
const PARSE_INT_UNSIGNED_RE: Array<Opt<RegExp>> = PARSE_INT_RE_PATTERNS.map(pattern => pattern ? new RegExp(`^${pattern}$`, "i") : undefined);

// regular expressions for function `parseInt` for signed integers
const PARSE_INT_SIGNED_RE: Array<Opt<RegExp>> = PARSE_INT_RE_PATTERNS.map(pattern => pattern ? new RegExp(`^[-+]?${pattern}$`, "i") : undefined);

// digits of roman numbers, and their decimal values, sorted *descending*
const ROMAN_DIGITS_LIST: Array<[string, number]> = [
    ["M", 1000],
    ["D", 500],
    ["C", 100],
    ["L", 50],
    ["X", 10],
    ["V", 5],
    ["I", 1]
];

// the values of all available roman digits, mapped by the respective roman digit letter
const ROMAN_DIGITS_MAP = new Map(ROMAN_DIGITS_LIST);

// the number of available roman digits
const ROMAN_DIGITS_COUNT = ROMAN_DIGITS_LIST.length;

// public functions ===========================================================

/**
 * Converts the passed integer to a string.
 *
 * @param value
 *  The number to be converted. Will be truncated to an integer.
 *
 * @param radix
 *  The radix used in the text representation.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The string representation of the passed integer number.
 */
export function formatInt(value: number, radix: number, options?: FormatIntOptions): string {
    let result = Math.floor(Math.max(0, value)).toString(radix);
    result = options?.lower ? result.toLowerCase() : result.toUpperCase();
    return options?.digits ? result.padStart(options.digits, "0") : result;
}

/**
 * Tries to parse the passed text as an integer. Unlike the built-in global
 * function `parseInt`, this function does not accept trailing garbage text.
 *
 * @param text
 *  The text representation of an integer.
 *
 * @param radix
 *  The radix used in the text representation.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The parsed integer; or `NaN`, if the text contains invalid characters.
 */
export function parseInt(text: string, radix: number, options?: ParseIntOptions): number {
    const re = (options?.unsigned ? PARSE_INT_UNSIGNED_RE : PARSE_INT_SIGNED_RE)[radix];
    return re?.test(text) ? globalThis.parseInt(text, radix) : Number.NaN;
}

/**
 * Converts the passed decimal number to an alphabetic number string. The
 * numbers `1` to `26` will be converted to single letters `A` to `Z`
 * respectively. The numbers `27` to `52` will be converted to multi-letter
 * numbers using a specific conversion method, according to the passed options.
 *
 * @param value
 *  The number to be converted. Will be truncated to an integer.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The alphabetic number representation of the passed number; or an empty
 *  string for the number zero or negative numbers.
 */
export function formatAlphabetic(value: number, options?: FormatAlphabeticOptions): string {

    value = Math.floor(value);
    if (value <= 0) { return ""; }

    // ASCII code of lower or upper A
    const aCode = options?.lower ? 97 : 65;

    // repeated-letters mode
    if (options?.repeat) {
        value -= 1;
        return String.fromCharCode(aCode + value % 26).repeat(Math.floor(value / 26) + 1);
    }

    // letters-as-digits mode
    let result = "";
    for (value -= 1; value >= 0; value = Math.floor(value / 26) - 1) {
        result = String.fromCharCode(aCode + value % 26) + result;
    }
    return result;
}

/**
 * Tries to parse the passed text as an alphabetically encoded number.
 *
 * @param text
 *  The text to be parsed as an alphabetic number (case-insensitive).
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The decimal value of the passed alphabetically encoded number (zero for an
 *  empty string); or `NaN`, if the passed text contains invalid characters.
 */
export function parseAlphabetic(text: string, options?: ParseAlphabeticOptions): number {

    // accept empty strings
    if (text.length === 0) { return 0; }

    // always work on uppercase letters
    text = text.toUpperCase();

    // repeated-letters mode
    if (options?.repeat) {
        if (!/^([A-Z])\1*$/.test(text)) { return Number.NaN; }
        return (text.length - 1) * 26 + text.charCodeAt(0) - 64;
    }

    // letters-as-digits mode
    if (!/^[A-Z]+$/.test(text)) { return Number.NaN; }
    let result = 0;
    for (const char of text) {
        result = result * 26 + char.charCodeAt(0) - 64;
    }
    return result;
}

/**
 * Converts the passed decimal number to a roman number string.
 *
 * @param value
 *  The number to be converted. Will be truncated to an integer. Must be
 *  non-negative and less than `4000` in order to produce a valid result.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The roman number representation of the passed number; or an empty string,
 *  if the passed number is not in the supported range.
 */
export function formatRoman(value: number, options?: FormatRomanOptions): string {

    value = Math.floor(value);
    if ((value <= 0) || (value > 3999)) { return ""; }

    // conversion mode for digits 4 and 9
    const depth = options?.depth ?? 0;

    let result = "";
    for (let index = 0; (index < ROMAN_DIGITS_COUNT) && (value > 0); index += 2) {

        // the letter and value of the current roman digit
        const [currChar, currValue] = ROMAN_DIGITS_LIST[index];
        // current decimal digit of the number to be processed
        const digit = Math.floor(value / currValue);

        // Special handling for digits 4 and 9: Put a smaller roman digit before a larger one,
        // for example IV which means 5 - 1 = 4; or XM which means 1000 - 10 = 990.
        if ((depth >= 0) && (digit % 5 === 4)) {

            // get the nerest larger roman digit (e.g., 100 for 99, or 500 for 490)
            const [prevChar, prevValue] = ROMAN_DIGITS_LIST[index - ((digit === 4) ? 1 : 2)];
            // array index to the smaller roman digit (e.g., 50 for 99, or 100 for 490)
            let nextIndex = index;
            // try to find a smaller roman digit according to the passed depth
            let remaining = Math.min(depth, ROMAN_DIGITS_COUNT - index - 1);

            // try to find a smaller roman digit so that the difference does not exceed the
            // remaining number; but stop searching if the passed depth has been reached
            while (remaining > 0) {
                remaining -= 1;
                if (value < prevValue - ROMAN_DIGITS_LIST[nextIndex + 1][1]) { break; }
                nextIndex += 1;
            }

            // add the combination of roman digits to the result, and reduce the number
            const [nextChar, nextValue] = ROMAN_DIGITS_LIST[nextIndex];
            result += nextChar + prevChar;
            value += nextValue - prevValue;

        } else {

            // decimal digit is in range 5 to 9: add the roman digit for 5, 50, or 500
            if (digit >= 5) { result += ROMAN_DIGITS_LIST[index - 1][0]; }

            // repeat the digit for the current power of 10, and reduce the number
            result += currChar.repeat(digit % 5);
            value %= currValue;
        }
    }

    // convert to lower-case if specified
    return options?.lower ? result.toLowerCase() : result;
}

/**
 * Tries to parse the passed text as a roman number.
 *
 * @param text
 *  The text to be parsed as a roman number (case-insensitive).
 *
 * @returns
 *  The decimal value of the passed roman number (zero for the empty string);
 *  or `NaN`, if the passed text contains any invalid characters.
 */
export function parseRoman(text: string): number {

    // the intermediate result
    let result = 0;
    // the largest roman digit found so far
    let maxDigit = 1;

    // process all roman digits in reversed order
    text = text.toUpperCase();
    for (let ichar = text.length - 1; ichar >= 0; ichar -= 1) {
        const letter = text[ichar];

        // convert digit to the decimal number, exit on invalid characters
        const value = ROMAN_DIGITS_MAP.get(letter);
        if (!value) { return Number.NaN; }

        // if the digit is less than the maximum digit found so far, subtract it from the result
        result += (value < maxDigit) ? -value : value;
        maxDigit = Math.max(maxDigit, value);
    }

    return result;
}
