/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as fun from "@/io.ox/office/tk/algorithms/function";
import * as debug from "@/io.ox/office/tk/algorithms/debug";

// global types ===============================================================

declare global {

    /**
     * The states of a promise, as used by `JPromise`.
     */
    type JPromiseState = ReturnType<JQuery.Promise<any>["state"]>;

    /**
     * Reduced shape of a `JQuery.Promise` to restrict supported functionality
     * and silence linter warnings.
     *
     * @template FT
     *  The type of the fulfilment value.
     */
    interface JPromise<FT = void> {
        state(): JPromiseState;
        promise(): JPromise<FT>;
        then<RT>(doneFn: (result: FT) => MaybeAsync<RT>, failFn?: (error: unknown) => MaybeAsync<RT>): JPromise<RT>;
        catch<RT>(failFn: (error: unknown) => MaybeAsync<RT>): JPromise<RT>;
        // return type `void` silences "floating promises" linter warnings, but also prevents method chaining
        done(doneFn: (result: FT) => unknown): void;
        fail(failFn: (error: unknown) => unknown): void;
        progress(progressFn: (progress: number) => unknown): void;
        always(alwaysFn: () => unknown): void;
    }

    /**
     * Reduced shape of a `JQuery.Deferred` to restrict supported functionality
     * and silence linter warnings.
     *
     * @template FT
     *  The type of the fulfilment value.
     */
    interface JDeferred<FT = void> extends JPromise<FT> {
        resolve(result: FT): unknown;
        reject(error?: unknown): unknown;
        notify(progress: number): unknown;
        promise(): JPromise<FT>;
    }

    /**
     * Type of a value that is wrapped in a native `Promise`, or in a
     * `JQuery.Promise`.
     *
     * @template FT
     *  The type of the fulfilment value.
     */
    type AnyPromise<FT = void> = Promise<FT> | JPromise<FT>;

    /**
     * Type of a value that may be given literally, or may be wrapped in a native
     * `Promise`, or in a `JQuery.Promise`.
     *
     * @template FT
     *  The type of the fulfilment value.
     */
    type MaybeAsync<FT = void> = FT | AnyPromise<FT>;
}

// types ======================================================================

/**
 * Type of callback functions used to fulfil a pending promise. Can be a unary
 * function for promises fulfilling with a value, or a nullary function for
 * void promises.
 */
export type PromiseFulfilFn<FT> = FT extends void ? (result?: AnyPromise) => void : (result: MaybeAsync<FT>) => void;

/**
 * Type of callback functions used to reject a pending promise. Rejection
 * functions must be prepared to deal with all kinds of values (from thrown
 * exceptions).
 */
export type PromiseRejectFn = (reason?: unknown) => void;

/**
 * Type of an executor function for new promises.
 *
 * @param fulfilFn
 *  The function that needs to be called to fulfil the new promise.
 *
 * @param rejectFn
 *  The function that needs to be called to reject the new promise.
 */
export type PromiseExecutorFn<FT = void> = (fulfilFn: PromiseFulfilFn<FT>, rejectFn: PromiseRejectFn) => void;

// public functions ===========================================================

/**
 * Returns whether the passed data is a native promise, or a JQuery promise.
 *
 * @param data
 *  The value to be checked.
 *
 * @returns
 *  Whether the passed data is a native promise, or a JQuery promise.
 */
export function isAnyPromise(data: unknown): data is AnyPromise<unknown> {
    return (data instanceof Promise) || isfn(data);
}

/**
 * Creates a new JQuery promise, invokes the passed callback function, and
 * returns the promise. The callback function is responsible to settle the
 * promise at some point by calling one of the provided functions `fulfil` or
 * `reject`.
 *
 * This function mimics the behavior of the native `Promise` constructor.
 *
 * @param executorFn
 *  The callback function that will be invoked immediately. Receives the
 *  callback functions `fulfilFn` and `rejectFn` as parameters. If this
 *  function throws an exception, the returned promise will be rejected.
 *
 * @returns
 *  The new JQuery promise.
 */
function newfn<FT = void>(executorFn: PromiseExecutorFn<FT>): JPromise<FT> {
    const def = deferred<FT>();
    const fulfilFn = ((value: FT): void => { def.resolve(value); }) as PromiseFulfilFn<FT>;
    const rejectFn: PromiseRejectFn = err => { def.reject(err); };
    try {
        executorFn(fulfilFn, rejectFn);
    } catch (err) {
        debug.logScriptError(err);
        rejectFn(err);
    }
    return def.promise();
}
export { newfn as new };

/**
 * Creates and returns a new pending `JDeferred` object that can be fulfilled
 * or rejected manually by calling its methods `resolve()` or `reject()`
 * respectively.
 *
 * @returns
 *  A new pending `JDeferred` object.
 */
export function deferred<FT = void>(): JDeferred<FT> {
    // eslint-disable-next-line no-restricted-properties
    return $.Deferred() as JDeferred<FT>;
}

/**
 * Creates and returns a fulfilled JQuery promise. This function works like
 * `Promise.resolve` for native promises.
 *
 * @param value
 *  The value to fulfil the promise with.
 *
 * @returns
 *  The JQuery promise fulfilled with the passed value.
 */
export function resolve(): JPromise;
export function resolve<FT>(value: FT): JPromise<FT>;
// implementation
export function resolve<FT>(value?: FT): JPromise<FT> {
    // eslint-disable-next-line no-restricted-properties
    return $.Deferred<FT, unknown>().resolve(value!).promise() as JPromise<FT>;
}

/**
 * Creates and returns a rejected JQuery promise. This function works like
 * `Promise.reject` for native promises.
 *
 * @param value
 *  The value to reject the promise with.
 *
 * @returns
 *  The JQuery promise rejected with the passed value.
 */
export function reject(value?: unknown): JPromise<never> {
    // eslint-disable-next-line no-restricted-properties
    return $.Deferred<never, unknown>().reject(value).promise() as JPromise<never>;
}

/**
 * Returns whether the passed value is a JQuery promise-like object (i.e.
 * `JQuery.Promise` and `JQuery.Deferred` objects).
 *
 * @param data
 *  The value to be tested.
 *
 * @returns
 *  Whether the passed value is a JQuery promise object. JQuery promises are
 *  plain JavaScript objects (instead of some class instances) and therefore
 *  need to be detected via duck-typing. Returns `false` for native promises!
 */
function isfn<T = unknown>(data: unknown): data is JPromise<T> {
    // deferreds and promises are plain objects in JQuery
    return is.dict(data) && is.function(data.state) && is.function(data.promise) && is.function(data.then) && is.function(data.done);
}
export { isfn as is };

/**
 * Returns whether the passed JQuery promise is in pending state.
 *
 * @param promise
 *  The JQuery promise to be tested.
 *
 * @returns
 *  Whether the passed JQuery promise value is in pending state.
 */
export function isPending(promise: JPromise<any>): boolean {
    return promise.state() === "pending";
}

/**
 * Returns whether the passed JQuery promise is in settled state (fulfilled or
 * rejected).
 *
 * @param promise
 *  The JQuery promise to be tested.
 *
 * @returns
 *  Whether the passed JQuery promise value is in settled state.
 */
export function isSettled(promise: JPromise<any>): boolean {
    return promise.state() !== "pending";
}

/**
 * Returns whether the passed JQuery promise is in fulfilled state.
 *
 * @param promise
 *  The JQuery promise to be tested.
 *
 * @returns
 *  Whether the passed JQuery promise value is in fulfilled state.
 */
export function isFulfilled(promise: JPromise<any>): boolean {
    return promise.state() === "resolved";
}

/**
 * Returns whether the passed JQuery promise is in rejected state.
 *
 * @param promise
 *  The JQuery promise to be tested.
 *
 * @returns
 *  Whether the passed JQuery promise is in rejected state.
 */
export function isRejected(promise: JPromise<any>): boolean {
    return promise.state() === "rejected";
}

/**
 * Converts the passed value to a JQuery promise.
 *
 * @param value
 *  The value to be converted to a JQuery promise.
 *
 * @returns
 *  A JQuery promise representing the passed value:
 *  - A JQuery promise will be returned as received.
 *  - A native promise will be converted to a (pending!) JQuery promise.
 *  - Other values will be wrapped into a fulfilled JQuery promise.
 */
export function from<FT>(value: MaybeAsync<FT>): JPromise<FT> {
    // convert native promises to JQuery promises
    if (value instanceof Promise) {
        return resolve().then(() => value);
    }
    // wrap synchronous values into fulfilled promise
    return isfn(value) ? value : resolve(value);
}

/**
 * Invokes the passed callback function, and converts the result to a JQuery
 * promise.
 *
 * @param fn
 *  The callback function to be invoked. May return a native promise, a JQuery
 *  promise, or any other synchronous result value, or may throw exceptions.
 *
 * @param args
 *  The arguments to be passed to the callback function.
 *
 * @returns
 *  A JQuery promise representing the return value of the passed callback
 *  function:
 *  - A JQuery promise will be returned as received.
 *  - A native promise will be converted to a (pending!) JQuery promise.
 *  - Other return values will be wrapped into a fulfilled JQuery promise.
 *  - A thrown exception will be wrapped into a rejected JQuery promise.
 */
export function invoke<FT, ArgsT extends unknown[]>(fn: FuncType<MaybeAsync<FT>, ArgsT>, ...args: ArgsT): JPromise<FT> {
    try {
        return from(fn(...args));
    } catch (err) {
        debug.logScriptError(err);
        return reject(err);
    }
}

/**
 * Ignores the fulfilment of the passed promise. Promise rejections will be
 * caught and thus silenced. Internal script errors will be logged to the
 * browser console.
 *
 * Needed to silence warnings from linters and other code analyzers about
 * unused promises that intentionally run in the background.
 *
 * @example
 *  // triggers ESLint rule "no-floating-promises"
 *  promise.then(...);
 *  // silences ESLint, but triggers Coverity's "Expression with no effect"
 *  void promise.then(...);
 *  // passes all code analyzers, and logs exceptions/rejections
 *  jpromise.floating(promise.then(...));
 *
 * @param arg
 *  The argument to be ignored.
 *
 * @param [failFn]
 *  If specified, will be invoked when the promise has been rejected.
 */
export function floating<FT>(arg: MaybeAsync<FT>, failFn?: PromiseRejectFn): void {
    const handler = (err: unknown): void => {
        debug.logScriptError(err);
        failFn?.(err);
    };
    if (arg instanceof Promise) {
        arg.catch(handler);
    } else if (isfn(arg)) {
        arg.catch(handler);
    }
}

/**
 * Invokes the passed callback function, and ignores the fulfilment of the
 * promise it returns. Promise rejections will be caught and thus silenced.
 * Internal script errors will be logged to the browser console.
 *
 * Needed to silence warnings from linters and other code analyzers about
 * unused promises that intentionally run in the background.
 *
 * @example
 *  // triggers ESLint rule "no-floating-promises"
 *  jpromise.invoke(asyncFunc);
 *  // silences ESLint, but triggers Coverity's "Expression with no effect"
 *  void jpromise.invoke(asyncFunc);
 *  // passes all code analyzers, and logs exceptions/rejections
 *  jpromise.invokeFloating(asyncFunc);
 *
 * @param fn
 *  The callback function to be invoked. May return a native promise, a JQuery
 *  promise, or any other synchronous result value, or may throw exceptions.
 *
 * @param args
 *  The arguments to be passed to the callback function.
 */
export function invokeFloating<FT, ArgsT extends unknown[]>(fn: FuncType<MaybeAsync<FT>, ArgsT>, ...args: ArgsT): void {
    floating(invoke(fn, ...args));
}

/**
 * Wraps the passed callback function (but does not invoke it immediately).
 * When called, ignores the fulfilment of the promise it returns. Promise
 * rejections will be caught and thus silenced. Internal script errors will be
 * logged to the browser console.
 *
 * Needed to silence warnings from linters and other code analyzers about async
 * functions passed as synchronous callbacks (e.g., event handlers).
 *
 * @example
 *  // triggers ESLint rule "no-floating-promises"
 *  $node.on("click", asyncFunc);
 *  // passes all code analyzers, and logs exceptions/rejections
 *  $node.on("click", jpromise.wrapFloating(asyncFunc));
 *
 * @param fn
 *  The callback function to be wrapped. May return a native promise, a JQuery
 *  promise, or any other synchronous result value, or may throw exceptions.
 *
 * @returns
 *  The wrapper function ignoring the asynchronous return value.
 */
export function wrapFloating<ThisT extends object, ArgsT extends unknown[]>(fn: ThisFuncType<ThisT, unknown, ArgsT>): ThisFuncType<ThisT, void, ArgsT> {
    return function (this: ThisT, ...args: ArgsT): void {
        floating(fn.apply(this, args));
    };
}

/**
 * Invokes the specified function and returns its result. A finally handler
 * will be called as early as possible, regarding asynchronous results.
 *
 * @param fn
 *  The callback function to be invoked.
 *
 * @param finalFn
 *  The finally handler to be invoked when the callback function has finished.
 *  Asynchronous callback functions will be awaited before calling the handler.
 *
 * @returns
 *  The original return value of the callback function. If it has been thrown
 *  an exception, it will be rethrown after running the finally handler.
 */
export function invokeWithFinally<T>(fn: FuncType<T>, finalFn: VoidFunction): T {
    try {
        const result = fn();
        floating(fastFinally(result, finalFn));
        return result;
    } catch (error) {
        finalFn();
        throw error;
    }
}

/**
 * Tries to invoke the fulfilment or rejection handler for a promise-like value
 * as quick as possible.
 *
 * @param arg
 *  The argument to be chained with the promise handlers. If this value is not
 *  a promise, it will be treated like a JQuery promise that has been fulfilled
 *  with that value.
 *
 * @param doneFn
 *  The callback handler to be invoked if the promise will be fulfilled. If the
 *  leading argument passed to this function is a settled JQuery promise (or a
 *  plain value), the callback handler will be invoked immediately. Otherwise,
 *  if the argument is a native promise, or a pending JQuery promise, the usual
 *  asynchronous promise chaining will take place.
 *
 * @param [failFn]
 *  The callback handler to be invoked if the promise will be rejected. If the
 *  leading argument passed to this function is a rejected JQuery promise, the
 *  callback handler will be called immediately. Otherwise, if the argument is
 *  a native promise, or a pending JQuery promise, the usual asynchronous
 *  promise chaining will take place.
 *
 * @returns
 *  A JQuery promise that represents the passed initial argument chained with
 *  the result of one of the callback functions.
 */
export function fastThen<FT1, FT2>(arg: MaybeAsync<FT1>, doneFn: (result: FT1) => MaybeAsync<FT2>, failFn?: (reason: unknown) => MaybeAsync<FT2>): JPromise<FT2> {

    // standard chain for native promises
    if (arg instanceof Promise) {
        return failFn ? from(arg).then(doneFn, failFn) : from(arg).then(doneFn);
    }

    // immediately invoke `doneFn`, if the passed value is not a promise
    if (!isfn(arg)) {
        return invoke(doneFn, arg);
    }

    // immediately invoke `doneFn`, if the passed promise is already fulfilled
    if (isFulfilled(arg)) {
        let result!: FT1;
        arg.done(r => (result = r)); // callback executes immediately!
        return invoke(doneFn, result);
    }

    // immediately invoke `failFn`, if the passed promise is already rejected
    if (isRejected(arg)) {
        let error!: unknown;
        arg.fail(e => (error = e)); // callback executes immediately!
        return failFn ? invoke(failFn, error) : reject(error);
    }

    // standard chain for pending JQuery promises
    return failFn ? arg.then(doneFn, failFn) : arg.then(doneFn);
}

/**
 * Tries to invoke the rejection handler for a promise-like value as quick as
 * possible.
 *
 * @param arg
 *  The argument to be chained with the promise handler. If this value is not a
 *  promise, it will be treated like a JQuery promise that has been fulfilled
 *  with that value.
 *
 * @param failFn
 *  The callback handler to be invoked if the promise will be rejected. If the
 *  leading argument passed to this function is a rejected JQuery promise, the
 *  callback handler will be called immediately. Otherwise, if the argument is
 *  a native promise, or a pending JQuery promise, the usual asynchronous
 *  promise chaining will take place.
 *
 * @returns
 *  A JQuery promise that represents the passed initial argument chained with
 *  the result of the callback function.
 */
export function fastCatch<FT>(arg: MaybeAsync<FT>): JPromise<Opt<FT>>;
export function fastCatch<FT1, FT2>(arg: MaybeAsync<FT1>, failFn: (reason: unknown) => MaybeAsync<FT2>): JPromise<FT1 | FT2>;
// implementation
export function fastCatch(arg: MaybeAsync<unknown>, failFn?: (reason: unknown) => MaybeAsync<unknown>): JPromise<unknown> {

    // standard chain for native promises
    if (arg instanceof Promise) {
        return from(arg).catch(failFn ?? fun.undef);
    }

    // nothing to do, if the passed value is not a promise, or a fulfilled JPromise
    if (!isfn(arg)) {
        return resolve(arg);
    }

    // nothing to do, if the passed promise is already fulfilled
    if (isFulfilled(arg)) {
        return arg;
    }

    // immediately invoke `failFn`, if the passed promise is already rejected
    if (isRejected(arg)) {
        if (!failFn) { return resolve(); }
        let error!: unknown;
        arg.fail(e => (error = e)); // callback executes immediately!
        return invoke(failFn, error);
    }

    // standard chain for pending JQuery promises
    return arg.catch(failFn ?? fun.undef);
}

/**
 * Tries to invoke the final handler for a promise-like value as quick as
 * possible.
 *
 * @param arg
 *  The argument to be chained with the promise handler. If this value is not a
 *  promise, it will be treated like a JQuery promise that has been fulfilled
 *  with that value.
 *
 * @param finalFn
 *  The callback handler to be invoked when the promise has been settled. If
 *  the leading argument passed to this function is a settled JQuery promise
 *  (or a plain value), the callback handler will be called immediately.
 *  Otherwise, if the argument is a native promise, or a pending JQuery
 *  promise, the usual asynchronous promise chaining will take place.
 *
 * @returns
 *  A JQuery promise that represents the passed initial argument chained with
 *  the result of the callback function.
 */
export function fastFinally<FT>(arg: MaybeAsync<FT>, finalFn: () => MaybeAsync<unknown>): JPromise<FT> {

    // standard chain for native promises
    if (arg instanceof Promise) {
        return from(arg.finally(finalFn));
    }

    // invokes the passed callback function, and returns a new promise with correct state/value
    const invokeFinal = (fulfilled: boolean, value: unknown): JPromise<FT> => {
        // finally callback fulfilled: restore original state/value
        // finally callback rejected: forward new error
        const doneFn = fulfilled ? (() => value as FT) : (() => { throw value; });
        return fastThen(invoke(finalFn), doneFn);
    };

    // invoke the local handler function for fulfilment and rejection
    return fastThen(arg, result => invokeFinal(true, result), error => invokeFinal(false, error));
}

/**
 * Tries to invoke the passed (optionally) asynchronous functions one after the
 * other as quick as possible. If a function returns a synchronous result, the
 * following function will be invoked immediately without the usual forced
 *  asynchronous timeout enforced by the promise's `then` method.
 *
 * @param funcs
 *  The callback functions to be invoked one after the other. Each nullish
 *  argument will be skipped silently to easily support conditional entries in
 *  the chain.
 *
 * @returns
 *  A JQuery promise that represents the chained callback functions.
 */
export function fastChain<RT>(...funcs: [...Array<Nullable<FuncType<MaybeAsync>>>, FuncType<MaybeAsync<RT>>]): JPromise<RT>;
export function fastChain(...funcs: Array<Nullable<FuncType<MaybeAsync>>>): JPromise;
// implementation
export function fastChain(...funcs: Array<Nullable<FuncType<MaybeAsync>>>): JPromise {
    return funcs.reduce<Opt<JPromise>>((result, func) => func ? fastThen(result, func) : result, undefined) ?? resolve();
}
