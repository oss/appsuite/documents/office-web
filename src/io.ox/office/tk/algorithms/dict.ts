/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as str from "@/io.ox/office/tk/algorithms/string";

// types ======================================================================

/**
 * Extracts the key type of the properties in an object type.
 *
 * @template DT
 *  The object type to extract the key type from.
 */
export type KeyOf<DT extends object> = Extract<keyof DT, string>;

/**
 * Extracts the value type of the properties in an object type.
 *
 * @template DT
 *  The object type to extract the value type from.
 */
export type ValOf<DT extends object> = DT[KeyOf<DT>];

/**
 * A callback function that will be invoked to generate elements of a record or
 * dictionary.
 *
 * @template RT
 *  The return type of the callback function.
 *
 * @template KT
 *  The type of the property keys in the dictionary.
 *
 * @param key
 *  The key of the property to be created.
 *
 * @param index
 *  The array index of the key.
 */
export type DictGeneratorFn<RT, KT extends string = string> = (key: KT, index: number) => RT;

/**
 * A callback function that will be invoked for each element of a record or
 * dictionary.
 *
 * @template RT
 *  The return type of the callback function.
 *
 * @template DT
 *  The type of the entire record or dictionary.
 *
 * @param value
 *  The value of the current property.
 *
 * @param key
 *  The key of the current property.
 */
export type DictCallbackFn<RT, DT extends object> = (value: ValOf<DT>, key: KeyOf<DT>) => RT;

/**
 * A callback function that will be invoked for each element of a record or
 * dictionary. In difference to `DictCallbackFn`, this function takes the key
 * first.
 *
 * @template RT
 *  The return type of the callback function.
 *
 * @template DT
 *  The type of the entire record or dictionary.
 *
 * @param key
 *  The key of the current property.
 *
 * @param value
 *  The value of the current property.
 */
export type DictKeyCallbackFn<RT, DT extends object> = (key: KeyOf<DT>, value: ValOf<DT>) => RT;

/**
 * An aggregator callback function that will be invoked for each property in a
 * record or dictionary, intended to reduce the property values to a single
 * result value.
 *
 * @template RT
 *  The return type of the callback function.
 *
 * @template DT
 *  The type of the entire record or dictionary.
 *
 * @param reduced
 *  The return value of the previous invocation of the callback function (or
 *  the initial value on first invocation).
 *
 * @param value
 *  The value of the current property.
 *
 * @param key
 *  The key of the current property.
 *
 * @returns
 *  The return value of the callback function will become the new result value
 *  that will be passed as parameter `reduced` with the next element.
 */
export type DictReduceFn<RT, DT extends object> = (reduced: RT, value: ValOf<DT>, key: KeyOf<DT>) => RT | void;

/**
 * An equality predicate callback function for properties of two dictionaries.
 *
 * @param a
 *  The first value to be compared.
 *
 * @param b
 *  The second value to be compared.
 *
 * @param key
 *  The key of the current property.
 *
 * @returns
 *  Whether `a` is considered equal to `b`.
 */
export type PropertyEqualsFn<T, KT extends string> = (a: T, b: T, key: KT) => boolean;

// public functions ===========================================================

// dictionary generators ------------------------------------------------------

/**
 * Creates and returns an empty dictionary with element type annotation. The
 * dictionary will be really empty (without prototype object, i.e. it will not
 * contain derived object prototype properties like `constructor`).
 *
 * @returns
 *  An empty dictionary.
 */
export function create<VT = unknown>(): Dict<VT> {
    return Object.create(null) as Dict<VT>;
}

/**
 * Creates and returns an empty record with element type annotation. The record
 * will be really empty (without prototype object, i.e. it will not contain
 * derived object prototype properties like `constructor`).
 *
 * @returns
 *  An empty record.
 */
export function createRec<KT extends string, VT = unknown>(): PtRecord<KT, VT> {
    return Object.create(null) as PtRecord<KT, VT>;
}

/**
 * Creates and returns an empty object typed as a specific interface with all
 * optional properties. The record will be really empty (without prototype
 * object, i.e. it will not contain derived object prototype properties like
 * `constructor`).
 *
 * @returns
 *  An empty interface object.
 */
export function createPartial<DT extends object>(): Partial<DT> {
    return Object.create(null) as Partial<DT>;
}

/**
 * Creates a dictionary from the passed iterable providing key/value pairs.
 *
 * @param source
 *  The data source to be converted to a dictionary (e.g. a native `Map`).
 *
 * @returns
 *  The dictionary created from the passed iterable providing key/value pairs.
 */
export function from<KT extends string, VT>(source: EntryIterable<KT, VT>): PtRecord<KT, VT>;
export function from<KT extends string, RT, VT>(source: Iterable<VT>, fn: (value: VT) => Opt<[KT, RT]>): PtRecord<KT, RT>;
// implementation
export function from<KT extends string, VT>(source: Iterable<unknown>, fn?: (value: unknown) => Opt<[KT, VT]>): PtRecord<KT, VT> {
    const result = Object.create(null) as PtRecord<KT, VT>;
    if (fn) {
        for (const value of source) {
            const entry = fn(value);
            if (entry) { result[entry[0]] = entry[1]; }
        }
    } else {
        for (const [key, value] of source as EntryIterable<KT, VT>) {
            result[key] = value;
        }
    }
    return result;
}

/**
 * Creates a dictionary from the passed list of keys, and a constant value.
 *
 * @param srcKeys
 *  The names of the properties to be inserted into the dictionary, as array of
 *  strings, or as single string with whitespace-separated key tokens.
 *
 * @param value
 *  The value to be set for all properties.
 *
 * @returns
 *  The dictionary containing some or all of the specified keys.
 */
export function fill<VT>(srcKeys: string | readonly string[], value: VT): Dict<VT> {
    const list = is.string(srcKeys) ? str.splitTokens(srcKeys) : srcKeys;
    const result = create<VT>();
    for (const key of list) { result[key] = value; }
    return result;
}

/**
 * Creates a record from the passed list of keys, and a constant value. In
 * difference to the function `create`, this function returns an object with a
 * fixed set of property keys.
 *
 * @param srcKeys
 *  The names of the properties to be inserted into the record.
 *
 * @param value
 *  The value to be set for all properties.
 *
 * @returns
 *  The record containing all specified keys.
 */
export function fillRec<KT extends string, VT>(srcKeys: readonly KT[], value: VT): Record<KT, VT> {
    const result = Object.create(null) as Record<KT, VT>;
    for (const key of srcKeys) { result[key] = value; }
    return result;
}

/**
 * Creates a dictionary from the passed list of keys, and assigns the result of
 * a callback function to the properties.
 *
 * @param srcKeys
 *  The names of the properties to be inserted into the dictionary, as array of
 *  strings, or as single string with whitespace-separated key tokens.
 *
 * @param fn
 *  The callback function that will be invoked for each passed key. The return
 *  value becomes the value of the property in the dictionary. If the return
 *  value is `undefined`, no property will be created in the dictionary.
 *
 * @returns
 *  The new dictionary containing some or all of the specified keys.
 */
export function generate<KT extends string, VT>(srcKeys: readonly KT[], fn: DictGeneratorFn<Opt<VT>, KT>): PtRecord<KT, VT>;
export function generate<VT>(srcKeys: string | readonly string[], fn: DictGeneratorFn<Opt<VT>>): Dict<VT>;
// implementation
export function generate<VT>(srcKeys: string | readonly string[], fn: DictGeneratorFn<Opt<VT>>): Dict<VT> {
    const list = is.string(srcKeys) ? str.splitTokens(srcKeys) : srcKeys;
    const result = create<VT>();
    for (let ai = 0, len = list.length; ai < len; ai += 1) {
        const key = list[ai];
        const value = fn(key, ai);
        if (value !== undefined) { result[key] = value; }
    }
    return result;
}

/**
 * Creates a record from the passed list of keys, and assigns the result of a
 * callback function to the properties. In difference to the function
 * `generate`, this function intentionally returns an object with a fixed set
 * of property keys, and does not support filtering.
 *
 * @param srcKeys
 *  The names of the properties to be inserted into the record.
 *
 * @param fn
 *  The callback function that will be invoked for each passed key. The return
 *  value becomes the value of the property in the record.
 *
 * @returns
 *  The new record containing all specified keys.
 */
export function generateRec<KT extends string, VT>(srcKeys: readonly KT[], fn: DictGeneratorFn<VT, KT>): Record<KT, VT> {
    const result = Object.create(null) as Record<KT, VT>;
    for (let idx = 0, len = srcKeys.length; idx < len; idx += 1) {
        const key = srcKeys[idx];
        result[key] = fn(key, idx);
    }
    return result;
}

// property comparison --------------------------------------------------------

/**
 * Returns whether the specified properties of the passed dictionaries are
 * equal, while ignoring all other properties. If a property is missing in both
 * dictionaries, it is considered to be equal. Uses strict equality or a custom
 * comparator to compare the property values.
 *
 * @param dict1
 *  The first dictionary to be compared to the other.
 *
 * @param dict2
 *  The second dictionary to be compared to the other.
 *
 * @param srcKeys
 *  The keys of all properties that will be compared in the dictionaries.
 *
 * @param [equalsFn]
 *  A custom comparator callback function for the properties. Will not be
 *  called, if neither of the dictionaries contains the property. If omitted,
 *  the built-in strict comparison will be used.
 *
 * @returns
 *  Whether all properties in both dictionaries are equal.
 */
export function equals<
    DT extends object,
    KT extends Extract<keyof DT, string>,
    VT extends Exclude<DT[KT], undefined>
>(
    dict1: DT,
    dict2: DT,
    srcKeys: readonly KT[] | KT,
    equalsFn?: PropertyEqualsFn<VT, KT>
): boolean {
    return (is.string(srcKeys) ? [srcKeys] : srcKeys).every(key => {
        const has1 = key in dict1;
        const has2 = key in dict2;
        return (!has1 || !has2) ? (!has1 && !has2) : equalsFn ? equalsFn(dict1[key] as VT, dict2[key] as VT, key) : (dict1[key] === dict2[key]);
    });
}

// property iteration ---------------------------------------------------------

/**
 * Visits each property in the passed dictionary.
 *
 * _Attention:_ This function should be used instead of `_.forEach` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be visited.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary.
 */
export function forEach<DT extends object>(source: DT, fn: DictCallbackFn<void, DT>): void {
    for (const key in source) {
        fn(source[key], key);
    }
}

/**
 * Visits each property in the passed dictionary. In difference to the function
 * `forEach`, this function passes the property *keys* before the property
 * _values_ to the callback function.
 *
 * _Attention:_ This function should be used instead of `_.forEach` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be visited.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary.
 */
export function forEachKey<DT extends object>(source: DT, fn: DictKeyCallbackFn<void, DT>): void {
    for (const key in source) {
        fn(key, source[key]);
    }
}

/**
 * Returns whether the passed dictionary contains at least one property that
 * fulfils the predicate.
 *
 * _Attention:_ This function should be used instead of `_.some` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be checked.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. If it returns `true`, this function will immediately
 *  return `true` by itself without visiting the remaining properties.
 *
 * @returns
 *  Whether the dictionary contains at least one property that fulfils the
 *  predicate.
 */
export function some<DT extends object>(source: DT, fn: DictCallbackFn<boolean, DT>): boolean {
    for (const key in source) {
        if (fn(source[key], key)) { return true; }
    }
    return false;
}

/**
 * Returns whether the passed dictionary contains at least one property that
 * fulfils the predicate. In difference to the function `some`, this function
 * passes the property *keys* before the property *values* to the callback
 * function.
 *
 * _Attention:_ This function should be used instead of `_.some` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be checked.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. If it returns `true`, this function will immediately
 *  return `true` by itself without visiting the remaining properties.
 *
 * @returns
 *  Whether the dictionary contains at least one property that fulfils the
 *  predicate.
 */
export function someKey<DT extends object>(source: DT, fn: DictKeyCallbackFn<boolean, DT>): boolean {
    for (const key in source) {
        if (fn(key, source[key])) { return true; }
    }
    return false;
}

/**
 * Returns whether the passed dictionary contains properties that all fulfil
 * the predicate.
 *
 * _Attention:_ This function should be used instead of `_.every` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be checked.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. If it returns `false`, this function will immediately
 *  return `false` by itself without visiting the remaining properties.
 *
 * @returns
 *  Whether the dictionary contains properties that all fulfil the predicate.
 */
export function every<DT extends object>(source: DT, fn: DictCallbackFn<boolean, DT>, context?: object): boolean {
    for (const key in source) {
        if (!fn.call(context, source[key], key)) { return false; }
    }
    return true;
}

/**
 * Returns whether the passed dictionary contains properties that all fulfil
 * the predicate. In difference to the function `every`, this function passes
 * the property *keys* before the property *values* to the callback function.
 *
 * _Attention:_ This function should be used instead of `_.every` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be checked.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. If it returns `false`, this function will immediately
 *  return `false` by itself without visiting the remaining properties.
 *
 * @returns
 *  Whether the dictionary contains properties that all fulfil the predicate.
 */
export function everyKey<DT extends object>(source: DT, fn: DictKeyCallbackFn<boolean, DT>): boolean {
    for (const key in source) {
        if (!fn(key, source[key])) { return false; }
    }
    return true;
}

/**
 * Reduces the properties of a dictionary to a single value.
 *
 * _Attention:_ This function should be used instead of `_.reduce` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be reduced.
 *
 * @param initial
 *  The initial value that will be passed to the first invocation of the
 *  callback function.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. Receives the return value of its preceding invocation.
 *  If the function returns `undefined`, the current result value passed to the
 *  function will be used (useful for e.g. aggregating arrays or records).
 *
 * @returns
 *  The reduced value (the return value of the last invocation of the callback
 *  function).
 */
export function reduce<DT extends object, RT>(source: DT, initial: RT, fn: DictReduceFn<RT, DT>): RT {
    for (const key in source) {
        const result = fn(initial, source[key], key);
        if (result !== undefined) { initial = result; }
    }
    return initial;
}

// find algorithms ------------------------------------------------------------

/**
 * Returns the first property that fulfils the predicate.
 *
 * _Attention:_ This function should be used instead of `_.find` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be searched.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. If it returns `true`, this function will immediately
 *  return the property value without visiting the remaining properties.
 *
 * @returns
 *  The value of the first matching property, otherwise `undefined`.
 */
export function find<DT extends object>(source: DT, fn: DictCallbackFn<boolean, DT>): Opt<ValOf<DT>> {
    for (const key in source) {
        if (fn(source[key], key)) { return source[key]; }
    }
    return undefined;
}

/**
 * Returns the key of the first property that fulfils the predicate. Similar to
 * method `Array#findIndex` but for dictionaries.
 *
 * _Attention:_ This function should be used instead of `_.findKey` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be searched.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. If it returns `true`, this function will immediately
 *  return the property key without visiting the remaining properties.
 *
 * @returns
 *  The key of the first matching property, otherwise `undefined`.
 */
export function findKey<DT extends object>(source: DT, fn: DictCallbackFn<boolean, DT>): Opt<string> {
    for (const key in source) {
        if (fn(source[key], key)) { return key; }
    }
    return undefined;
}

/**
 * Returns the key of the a property equal to a value. Similar to method
 * `Array#indexOf` but for dictionaries.
 *
 * @param source
 *  The source dictionary whose properties will be searched.
 *
 * @param value
 *  The value to be searched in the dictionary.
 *
 * @returns
 *  The key of the first property equal to the passed value.
 */
export function keyOf<DT extends object>(source: DT, value: unknown): Opt<keyof DT> {
    for (const key in source) {
        if (source[key] === value) { return key; }
    }
    return undefined;
}

/**
 * Returns whether the passed dictionary contains exactly one property.
 *
 * @param source
 *  The source dictionary to be checked for its properties.
 *
 * @returns
 *  Whether the passed dictionary contains exactly one property.
 */
export function singleKey(source: object): boolean {
    // avoid helpers with O(n) complexity, such as _.size() or Object.keys()
    let count = 0;
    for (const _key in source) {
        count += 1;
        if (count > 1) { break; }
    }
    return count === 1;
}

/**
 * Returns a single property key of the passed dictionary.
 *
 * @param source
 *  The source dictionary to extract a property from.
 *
 * @returns
 *  The key of an arbitrary existing property in the passed dictionary; or
 *  `undefined`, if the dictionary is empty. Repeated invocations of this
 *  function will always return the same property key (unless the dictionary
 *  has been modified).
 */
export function anyKey<DT extends object>(source: DT): Opt<KeysOf<DT>> {
    // eslint-disable-next-line no-unreachable-loop
    for (const key in source) { return key; }
    return undefined;
}

// iterators ------------------------------------------------------------------

/**
 * Creates a new iterator with the property values of the dictionary.
 *
 * @param source
 *  The source dictionary whose property values will be iterated.
 *
 * @yields
 *  The property values of the dictionary.
 */
export function *values<DT extends object>(source: DT): IterableIterator<ValOf<DT>> {
    for (const key in source) {
        yield source[key];
    }
}

/**
 * Creates a new iterator with the property keys of the dictionary.
 *
 * @param source
 *  The source dictionary whose property keys will be iterated.
 *
 * @yields
 *  The property keys of the dictionary.
 */
export function *keys<DT extends object>(source: DT): IterableIterator<KeyOf<DT>> {
    for (const key in source) {
        yield key;
    }
}

/**
 * Creates a new iterator with the property entries of the dictionary.
 *
 * @param source
 *  The source dictionary whose properties will be iterated.
 *
 * @yields
 *  The property entries of the dictionary.
 */
export function *entries<DT extends object>(source: DT): EntryIterator<KeyOf<DT>, ValOf<DT>> {
    for (const key in source) {
        yield [key, source[key]];
    }
}

// transformation algorithms --------------------------------------------------

/**
 * Creates a new dictionary with the same keys as the passed dictionary, and
 * assigns the result of a callback function to the properties.
 *
 * _Attention:_ This function should be used instead of `_.mapObject` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source dictionary whose properties will be mapped.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source dictionary. The return value becomes the value of the property in
 *  the new dictionary. If the return value is `undefined`, no property will be
 *  created in the new dictionary.
 *
 * @returns
 *  The new dictionary containing some or all of keys of the source dictionary.
 */
export function mapDict<DT extends object, RT>(source: DT, fn: DictCallbackFn<Opt<RT>, DT>): Dict<RT> {
    const result = create<RT>();
    for (const key in source) {
        const value = fn(source[key], key);
        if (value !== undefined) { result[key] = value; }
    }
    return result;
}

/**
 * Creates a new record with the same keys as the passed record, and assigns
 * the result of a callback function to the properties. In difference to the
 * function `mapDict`, this function intentionally returns an object with a
 * fixed set of property keys, and does not support filtering.
 *
 * _Attention:_ This function should be used instead of `_.mapObject` which
 * (intentionally) handles objects with numeric `length` property as
 * array-likes! See https://github.com/jashkenas/underscore/issues/1590.
 *
 * @param source
 *  The source record whose properties will be mapped.
 *
 * @param fn
 *  The callback function that will be invoked for each property in the passed
 *  source record. The return value becomes the value of the property in the
 *  new record.
 *
 * @returns
 *  The new record containing all keys of the source record.
 */
export function mapRecord<KT extends string, RT, VT>(source: Record<KT, VT>, fn: DictCallbackFn<RT, Record<KT, VT>>): Record<KT, RT> {
    const result = Object.create(null) as Record<KT, RT>;
    for (const key in source) {
        result[key] = fn(source[key], key);
    }
    return result;
}
