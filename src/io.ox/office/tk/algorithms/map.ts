/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// public functions ===========================================================

// map generators -------------------------------------------------------------

/**
 * Creates a new map from a data source and a mapping function. Note that
 * creating a map from a key/value data source without mapping function is
 * possible directly with the `Map` constructor.
 *
 * @param source
 *  The data source to be converted to a map.
 *
 * @param fn
 *  A mapper function that converts the source values to the map key/value
 *  pairs. If the function returns `undefined`, the source value will be
 *  skipped.
 *
 * @returns
 *  The new map with the return values of the mapper function.
 */
export function from<KT, RT, VT>(source: Iterable<VT>, fn: (value: VT) => Opt<[KT, RT]>): Map<KT, RT> {
    const map = new Map<KT, RT>();
    for (const value of source) {
        const result = fn(value);
        if (result !== undefined) { map.set(result[0], result[1]); }
    }
    return map;
}

/**
 * Creates a new map from matching entries of a data source.
 *
 * @param source
 *  The data source to be converted to a map.
 *
 * @param fn
 *  The predicate callback function invoked for the source entries.
 *
 * @returns
 *  A new map with all matching entries from the data source.
 */
export function filterFrom<KT, VT>(source: EntryIterable<KT, VT>, fn: (value: VT, key: KT) => boolean): Map<KT, VT> {
    const map = new Map<KT, VT>();
    for (const [key, value] of source) {
        if (fn(value, key)) { map.set(key, value); }
    }
    return map;
}

/**
 * Creates a new map from the results of a custom generator function.
 *
 * @param fn
 *  The generator function to be invoked immediately.
 *
 * @param [context]
 *  The calling context for the callback generator function (which cannot be
 *  written as arrow function).
 *
 * @returns
 *  A new map with all entries yielded by the generator function.
 */
export function yieldFrom<KT, VT>(fn: FuncType<EntryIterable<KT, VT>>, context?: object): Map<KT, VT> {
    return new Map(context ? fn.call(context) : fn());
}

/**
 * Destroys all elements in the passed map, and clears the map afterwards.
 *
 * @param map
 *  The map to be destroyed and cleared.
 */
export function destroy<KT, VT extends Destroyable>(map: Map<KT, VT>): void {
    for (const value of map.values()) { value.destroy(); }
    map.clear();
}

// element access -------------------------------------------------------------

/**
 * Inserts or deletes a map element with the specified key. In difference to
 * the class method `Map#set`, this function will delete existing map elements
 * when passing `undefined` as value.
 *
 * @param map
 *  The map to be manipulated.
 *
 * @param key
 *  The key of the map element to be updated.
 *
 * @param value
 *  The value to be inserted into the map; or `undefined` to delete an existing
 *  map element.
 *
 * @returns
 *  The passed value.
 */
export function toggle<KT, VT>(map: Map<KT, VT>, key: KT, value: VT): VT;
export function toggle<KT, VT>(map: Map<KT, VT>, key: KT, value: undefined): undefined;
export function toggle<KT, VT>(map: Map<KT, VT>, key: KT, value: Opt<VT>): Opt<VT>;
export function toggle<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, value: VT): VT;
export function toggle<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, value: undefined): undefined;
export function toggle<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, value: Opt<VT>): Opt<VT>;
// implementation
export function toggle<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, key: KT, value: Opt<VT>): Opt<VT> {
    if (value === undefined) { map.delete(key); } else { map.set(key, value); }
    return value;
}

/**
 * Removes an element from the map, and returns its value.
 *
 * @param map
 *  The map to be manipulated.
 *
 * @param key
 *  The key of the map element to be removed.
 *
 * @returns
 *  The value of the removed map element; or `undefined`, if the map did not
 *  contain an element with the specified key.
 */
export function remove<KT, VT>(map: Map<KT, VT>, key: KT): Opt<VT>;
export function remove<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT): Opt<VT>;
// implementation
export function remove<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, key: KT): Opt<VT> {
    const value = map.get(key);
    map.delete(key);
    return value;
}

/**
 * Invokes the callback function for an existing map element.
 *
 * @param map
 *  The map to be updated.
 *
 * @param key
 *  The key of the map element to be visited.
 *
 * @param fn
 *  A callback function invoked for existing map elements.
 *
 * @returns
 *  The value of the visited map element, otherwise `undefined`.
 */
export function visit<KT, VT>(map: Map<KT, VT>, key: KT, fn: (value: VT, key: KT) => void): Opt<VT>;
export function visit<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, fn: (value: VT, key: KT) => void): Opt<VT>;
// implementation
export function visit<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, key: KT, fn: (value: VT, key: KT) => void): Opt<VT> {
    if (map.has(key)) {
        const value = map.get(key)!;
        fn(value, key);
        return value;
    }
    return undefined;
}

/**
 * Returns the map element with the specified key. If such an element does not
 * exist, the generator callback function will be invoked to create a new map
 * element.
 *
 * @param map
 *  The map to be updated.
 *
 * @param key
 *  The key of the map element to be returned.
 *
 * @param fn
 *  A generator callback function for missing map elements. The return value of
 *  this function (unless `undefined`) will be inserted as new map element.
 *
 * @returns
 *  The map element for the specified key.
 */
export function upsert<KT, VT>(map: Map<KT, VT>, key: KT, fn: (key: KT) => VT): VT;
export function upsert<KT, VT>(map: Map<KT, VT>, key: KT, fn: (key: KT) => Opt<VT>): Opt<VT>;
export function upsert<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, fn: (key: KT) => VT): VT;
export function upsert<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, fn: (key: KT) => Opt<VT>): Opt<VT>;
// implementation
export function upsert<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, key: KT, fn: (key: KT) => Opt<VT>): Opt<VT> {
    if (map.has(key)) { return map.get(key)!; }
    const value = fn(key);
    if (value !== undefined) { map.set(key, value); }
    return value;
}

/**
 * Inserts, deletes, or replaces a map element.
 *
 * @param map
 *  The map to be updated.
 *
 * @param key
 *  The key of the map element to be updated.
 *
 * @param fn
 *  A callback function that will be invoked with the current value of the map
 *  element (or `undefined`, if the map does not contain an element with the
 *  passed key). The return value of this function will become the new value of
 *  the map element. If it is `undefined`, no value will be inserted, and an
 *  existing map element will be removed.
 *
 * @returns
 *  The new value of the map element.
 */
export function update<KT, VT>(map: Map<KT, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => VT): VT;
export function update<KT, VT>(map: Map<KT, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => undefined): undefined;
export function update<KT, VT>(map: Map<KT, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => Opt<VT>): Opt<VT>;
export function update<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => VT): VT;
export function update<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => undefined): undefined;
export function update<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => Opt<VT>): Opt<VT>;
// implementation
export function update<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, key: KT, fn: (value: Opt<VT>, key: KT) => Opt<VT>): Opt<VT> {
    const value = fn(map.get(key), key);
    if (value === undefined) { map.delete(key); } else { map.set(key, value); }
    return value;
}

/**
 * Adds a number to an existing element in this map, or inserts the number as
 * new map element.
 *
 * @param map
 *  The map containing the element to be modified.
 *
 * @param key
 *  The key of the map element to be modified.
 *
 * @param inc
 *  The value to be added to the map element.
 *
 * @returns
 *  The new value of the map element.
 */
export function add<KT>(map: Map<KT, number>, key: KT, inc: number): number {
    return update(map, key, value => (value ?? 0) + inc);
}

/**
 * Moves an element in this map to a new key.
 *
 * @param map
 *  The map containing the element to be moved.
 *
 * @param key
 *  The key of the map element to be moved.
 *
 * @param to
 *  The new key of the map element.
 *
 * @returns
 *  The value of the moved map element; or `undefined`, if the map does not
 *  contain an element with the specified key.
 */
export function move<KT, VT>(map: Map<KT, VT>, key: KT, to: KT): Opt<VT>;
export function move<KT extends object, VT>(map: WeakMap<KT, VT>, key: KT, to: KT): Opt<VT>;
// implementation
export function move<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, key: KT, to: KT): Opt<VT> {
    if (!map.has(key)) { return undefined; }
    const value = map.get(key)!;
    map.set(to, value);
    map.delete(key);
    return value;
}

/**
 * Inserts the values of one or more data sources into a map. Works similar to
 * `Object.assign` but for native maps.
 *
 * @param map
 *  The map to be extended.
 *
 * @param sources
 *  The data sources to be assigned to the map.
 *
 * @returns
 *  The extended map passed as first parameter.
 */
export function assign<KT, VT>(map: Map<KT, VT>, ...sources: Array<Nullable<EntryIterable<KT, VT>>>): Map<KT, VT>;
export function assign<KT extends object, VT>(map: WeakMap<KT, VT>, ...sources: Array<Nullable<EntryIterable<KT, VT>>>): WeakMap<KT, VT>;
// implementation
export function assign<KT, VT>(map: Map<KT, VT> | WeakMap<any, VT>, ...sources: Array<Nullable<EntryIterable<KT, VT>>>): Map<KT, VT> | WeakMap<any, VT> {
    for (const source of sources) {
        if (source) {
            for (const [key, value] of source) {
                map.set(key, value);
            }
        }
    }
    return map;
}

// element comparison ---------------------------------------------------------

/**
 * Compares that the passed maps contain the exact same elements. Insertion
 * orders in the maps do not matter.
 *
 * @param map1
 *  The first map that will be compared to the second map.
 *
 * @param map2
 *  The second map that will be compared to the first map.
 *
 * @param [equalsFn]
 *  A custom comparator callback function for the map elements. If omitted, the
 *  built-in strict comparison will be used.
 *
 * @returns
 *  Whether the two maps contain equal elements.
 */
export function equals<KT, VT>(map1: Map<KT, VT>, map2: Map<KT, VT>, equalsFn?: (v1: VT, v2: VT) => boolean): boolean {

    // references to the same map
    if (map1 === map2) { return true; }

    // compare map size first
    if (map1.size !== map2.size) { return false; }

    // compare all map elements with the passed callback, or with the equals
    // operator (keep the "if" outside the loops for better performance)
    if (equalsFn) {
        for (const [key, value] of map1) {
            if (!map2.has(key) || !equalsFn(value, map2.get(key)!)) { return false; }
        }
    } else {
        for (const [key, value] of map1) {
            if (!map2.has(key) || (value !== map2.get(key))) { return false; }
        }
    }

    return true;
}

// find algorithms ------------------------------------------------------------

/**
 * Returns the first value in the map.
 *
 * @param map
 *  The map to return a value from.
 *
 * @returns
 *  The value of the first map element (in insertion order); or `undefined`, if
 *  the map is empty.
 */
export function first<KT, VT>(map: Map<KT, VT>): Opt<VT> {
    const result = map.values().next();
    return result.done ? undefined : result.value;
}

/**
 * Returns the first value in the map that passed a truth test.
 *
 * @param map
 *  The map to be searched for a matching value.
 *
 * @param fn
 *  The predicate callback function invoked for the map elements.
 *
 * @returns
 *  The value of the first matching map element (in insertion order); or
 *  `undefined`, if no map element has been found.
 */
export function find<KT, VT>(map: Map<KT, VT>, fn: (value: VT, key: KT) => boolean): Opt<VT> {
    for (const [key, value] of map) {
        if (fn(value, key)) { return value; }
    }
    return undefined;
}

/**
 * Returns the first key in the map that passed a truth test.
 *
 * @param map
 *  The map to be searched for a matching key.
 *
 * @param fn
 *  The predicate callback function invoked for the map elements.
 *
 * @returns
 *  The key of the first matching map element (in insertion order); or
 *  `undefined`, if no map element has been found.
 */
export function findKey<KT, VT>(map: Map<KT, VT>, fn: (value: VT, key: KT) => boolean): Opt<KT> {
    for (const [key, value] of map) {
        if (fn(value, key)) { return key; }
    }
    return undefined;
}

/**
 * Returns the first entry in the map that passed a truth test.
 *
 * @param map
 *  The map to be searched for a matching entry.
 *
 * @param fn
 *  The predicate callback function invoked for the map entries.
 *
 * @returns
 *  The first matching map entry (in insertion order); or `undefined`, if no
 *  map entry has been found.
 */
export function findEntry<KT, VT>(map: Map<KT, VT>, fn: (entry: [KT, VT]) => boolean): Opt<[KT, VT]> {
    for (const entry of map) {
        if (fn(entry)) { return entry; }
    }
    return undefined;
}

// draining generators --------------------------------------------------------

/**
 * Creates a destructive iterator that shifts the entries from the passed map,
 * and yields their values.
 *
 * @param map
 *  The map to be iterated. It will be emptied inplace during iteration.
 *
 * @yields
 *  The values of the map.
 */
export function *shiftValues<KT, VT>(map: Map<KT, VT>): IterableIterator<VT> {
    for (const [key, value] of map) {
        map.delete(key);
        yield value;
    }
}

/**
 * Creates a destructive iterator that shifts the entries from the passed map,
 * and yields their keys.
 *
 * @param map
 *  The map to be iterated. It will be emptied inplace during iteration.
 *
 * @yields
 *  The keys of the map.
 */
export function *shiftKeys<KT, VT>(map: Map<KT, VT>): IterableIterator<KT> {
    for (const key of map.keys()) {
        map.delete(key);
        yield key;
    }
}

/**
 * Creates a destructive iterator that shifts the entries from the passed map.
 *
 * @param map
 *  The map to be iterated. It will be emptied inplace during iteration.
 *
 * @yields
 *  The entries of the map.
 */
export function *shiftEntries<KT, VT>(map: Map<KT, VT>): IterableIterator<[KT, VT]> {
    for (const entry of map) {
        map.delete(entry[0]);
        yield entry;
    }
}
