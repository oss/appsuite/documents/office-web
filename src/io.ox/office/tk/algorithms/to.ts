/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";

// public functions ===========================================================

/**
 * Tries to narrow the passed data to a number.
 *
 * @param data
 *  The data to be returned if it is a number.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a number.
 *
 * @returns
 *  The passed data if it is a number; otherwise the default value.
 */
export function number(data: unknown, def: number): number;
export function number(data: unknown, def?: number): Opt<number>;
// implementation
export function number(data: unknown, def?: number): Opt<number> {
    return is.number(data) ? data : def;
}

/**
 * Tries to narrow the passed data to a bigint.
 *
 * @param data
 *  The data to be returned if it is a bigint.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a bigint.
 *
 * @returns
 *  The passed data if it is a bigint; otherwise the default value.
 */
export function bigint(data: unknown, def: bigint): bigint;
export function bigint(data: unknown, def?: bigint): Opt<bigint>;
// implementation
export function bigint(data: unknown, def?: bigint): Opt<bigint> {
    return is.bigint(data) ? data : def;
}

/**
 * Tries to narrow the passed data to a string.
 *
 * @param data
 *  The data to be returned if it is a string.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a string.
 *
 * @returns
 *  The passed data if it is a string; otherwise the default value.
 */
export function string(data: unknown, def: string): string;
export function string(data: unknown, def?: string): Opt<string>;
// implementation
export function string(data: unknown, def?: string): Opt<string> {
    return is.string(data) ? data : def;
}

/**
 * Tries to narrow the passed data to an enumeration value.
 *
 * @param EnumValues
 *  A list of enumeration values, or the string `enum` type to be converted to.
 *
 * @param value
 *  The value to be converted to an enumeration value. This value must be a
 *  string for successful conversion.
 *
 * @param [def]
 *  The default value to be returned, if the passed value cannot be converted
 *  to an enumeration value.
 *
 * @returns
 *  The enumeration value with the specified string value, if existing.
 */
function enumfn<T extends string>(EnumValues: readonly T[], value: unknown, def: T): T;
function enumfn<T extends string>(EnumValues: readonly T[], value: unknown, def?: T): Opt<T>;
function enumfn<T>(EnumValues: T, value: unknown, def: T[keyof T]): T[keyof T];
function enumfn<T>(EnumValues: T, value: unknown, def?: T[keyof T]): Opt<T[keyof T]>;
// implementation
function enumfn(EnumType: readonly string[] | Dict<string>, value: unknown, def?: string): Opt<string> {
    if (!is.string(value)) { return def; }
    if (is.readonlyArray(EnumType)) {
        return EnumType.includes(value) ? value : def;
    }
    for (const key in EnumType) {
        if (EnumType[key] === value) {
            return value;
        }
    }
    return def;
}
export { enumfn as enum };

/**
 * Tries to narrow the passed data to a boolean value.
 *
 * @param data
 *  The data to be returned if it is a boolean value.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a boolean value.
 *
 * @returns
 *  The passed data if it is a boolean value; otherwise the default value.
 */
export function boolean(data: unknown, def: boolean): boolean;
export function boolean(data: unknown, def?: boolean): Opt<boolean>;
// implementation
export function boolean(data: unknown, def?: boolean): Opt<boolean> {
    return is.boolean(data) ? data : def;
}

/**
 * Tries to narrow the passed data to a symbol.
 *
 * @param data
 *  The data to be returned if it is a symbol.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a symbol.
 *
 * @returns
 *  The passed data if it is a boolean value; otherwise the default value.
 */
export function symbol(data: unknown, def: symbol): symbol;
export function symbol(data: unknown, def?: symbol): Opt<symbol>;
// implementation
export function symbol(data: unknown, def?: symbol): Opt<symbol> {
    return is.symbol(data) ? data : def;
}

/**
 * Tries to narrow the passed data to a dictionary.
 *
 * @param data
 *  The data to be returned if it is a dictionary.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a dictionary. If
 *  set to `true`, and the passed value is not a dictionary, a new empty
 *  dictionary will be created and returned.
 *
 * @returns
 *  The passed data if it is a dictionary; otherwise the default value, or
 *  `undefined` if omitted.
 */
export function dict(data: unknown, def: Dict | true): Dict;
export function dict(data: unknown, def?: Dict | true): Opt<Dict>;
// implementation
export function dict(data: unknown, def?: Dict | true): Opt<Dict> {
    return is.dict(data) ? data : (def === true) ? (Object.create(null) as Dict) : def;
}

/**
 * Tries to narrow the passed data to an array.
 *
 * @param data
 *  The data to be returned if it is an array.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not an array. If set
 *  to `true`, and the passed value is not an array, a new empty array will be
 *  created and returned.
 *
 * @returns
 *  The passed data if it is an array; otherwise the default value, or
 *  `undefined` if omitted.
 */
export function array(data: unknown, def: unknown[] | true): unknown[];
export function array(data: unknown, def?: unknown[] | true): Opt<unknown[]>;
// implementation
export function array(data: unknown, def?: unknown[] | true): Opt<unknown[]> {
    return is.array(data) ? data : (def === true) ? [] : def;
}

/**
 * Tries to narrow the passed data to a function.
 *
 * @param data
 *  The data to be returned if it is a function.
 *
 * @param [def]
 *  The default value to be returned if the passed data is not a function.
 *
 * @returns
 *  The passed data if it is a function; otherwise the default value, or
 *  `undefined` if omitted.
 */
function functionfn(data: unknown, def: AnyFunction): AnyFunction;
function functionfn(data: unknown, def?: AnyFunction): Opt<AnyFunction>;
// implementation
function functionfn(data: unknown, def?: AnyFunction): Opt<AnyFunction> {
    return is.function(data) ? data : def;
}
export { functionfn as function };
