/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * The relation between two values.
 * - The value `-1`, if the first value is less than the second value.
 * - The value `1`, if the first value is greater than the second value.
 * - The value `0`, if the two values are equal.
 */
export type Relation = -1 | 0 | 1;

/**
 * A comparator callback function for two values.
 *
 * @param a
 *  The first value to be compared.
 *
 * @param b
 *  The second value to be compared.
 *
 * @returns
 *  The relation of the passed values.
 */
export type CompareFn<T> = (a: T, b: T) => Relation;

/**
 * Represents the normalized decimal mantissa and exponent of a floating-point
 * number.
 */
export interface NumberComps {

    /**
     * The signed mantissa of the number in the interval `+-[1;10)`. If the
     * number is zero, the mantissa will be set to `0`.
     */
    mant: number;

    /**
     * The exponent of the number, so that `mant*10^expn` is equal to the
     * original number. If the number is zero, the exponent will be set to
     * `-Infinity`.
     */
    expn: number;
}

// constants ==================================================================

/**
 * The smallest positive normalized floating-point number (browsers may support
 * smaller numbers though, which are stored with a denormalized mantissa, see
 * `Number.MIN_VALUE`).
 */
export const MIN_NORM_NUMBER = 2 ** -1022;

// public functions ===========================================================

/**
 * Returns the sign of the passed number.
 *
 * @param num
 *  The number to be converted.
 *
 * @returns
 *  The sign of the passed number.
 */
export function sgn(num: number): Relation {
    return (num < 0) ? -1 : (num > 0) ? 1 : 0;
}

/**
 * Rounds the passed number to the next integer away from zero (the opposite
 * behavior of Math.trunc`).
 *
 * @param num
 *  The number to be inflated.
 *
 * @returns
 *  The inflated number.
 */
export function inflate(num: number): number {
    return (num < 0) ? Math.floor(num) : Math.ceil(num);
}

/**
 * Clamps the passed number into the specified inclusive interval.
 *
 * @param num
 *  The number to be clamped into the specified interval.
 *
 * @param lower
 *  The lower boundary of the interval.
 *
 * @param upper
 *  The upper boundary of the interval. MUST NOT be less than the "lower"
 *  parameter, otherwise the behavior of this function is undefined.
 *
 * @returns
 *  The clamped number (unmodified if inside the passed interval, otherwise
 *  either the lower or upper border).
 */
export function clamp(num: number, lower: number, upper: number): number {
    return Math.min(Math.max(num, lower), upper);
}

/**
 * Fix for the weird behavior of the built-in modulo operator with negative
 * dividends.
 *
 * @param dividend
 *  The dividend.
 *
 * @param divisor
 *  The divisor.
 *
 * @returns
 *  The remainder of the division. The result will have the same sign as the
 *  divisor (instead of the dividend). *Example:* While the expression `-5 % 3`
 *  results in -2, the expression `modulo(-5, 3)` results in 1 as expected.
 */
export function modulo(dividend: number, divisor: number): number {
    return (dividend % divisor + divisor) % divisor;
}

/**
 * Returns the distance between the origin of a cartesian coordinate system and
 * the specified point.
 *
 * @param x
 *  The X coordinate of the point.
 *
 * @param y
 *  The Y coordinate of the point.
 *
 * @returns
 *  The distance between the zero point and the specified point.
 */
export function radius(x: number, y: number): number {
    return Math.sqrt(x * x + y * y);
}

/**
 * Rounds the passed floating-point number to the nearest multiple of the
 * specified precision.
 *
 * @param num
 *  The floating-point number to be rounded.
 *
 * @param precision
 *  The precision used to round the number. The number 1 will round to integers
 *  (exactly like the function `Math.round()`). Must be positive. If less than
 *  1, must be the inverse of an integer to prevent further internal rounding
 *  errors.
 *
 * @returns
 *  The rounded number.
 */
export function roundp(num: number, precision: number): number {
    // Multiplication with small number may result in rounding errors (e.g.,
    // 227*0.1 results in 22.700000000000003), division by inverse number works
    // sometimes (e.g. 227/(1/0.1) results in 22.7), rounding the inverse
    // before division finally should work in all(?) cases, but restricts valid
    // precisions to inverses of integer numbers.
    num = Math.round((precision < 1) ? (num * Math.round(1 / precision)) : (num / precision));
    return (precision < 1) ? (num / Math.round(1 / precision)) : (num * precision);
}

/**
 * Rounds the passed floating-point number down to the nearest multiple of the
 * specified precision.
 *
 * @param num
 *  The floating-point number to be rounded.
 *
 * @param precision
 *  The precision used to round the number. The number 1 will round down to
 *  integers (exactly like the function `Math.floor()`). Must be positive. If
 *  less than 1, must be the inverse of an integer to prevent further internal
 *  rounding errors.
 *
 * @returns
 *  The rounded number.
 */
export function floorp(num: number, precision: number): number {
    // see comment in `roundp()`
    num = Math.floor((precision < 1) ? (num * Math.round(1 / precision)) : (num / precision));
    return (precision < 1) ? (num / Math.round(1 / precision)) : (num * precision);
}

/**
 * Rounds the passed floating-point number up to the nearest multiple of the
 * specified precision.
 *
 * @param num
 *  The floating-point number to be rounded.
 *
 * @param precision
 *  The precision used to round the number. The number 1 will round up to
 *  integers (exactly like the function `Math.ceil()`). Must be positive. If
 *  less than 1, must be the inverse of an integer to prevent further internal
 *  rounding errors.
 *
 * @returns
 *  The rounded number.
 */
export function ceilp(num: number, precision: number): number {
    // see comment in `roundp()`
    num = Math.ceil((precision < 1) ? (num * Math.round(1 / precision)) : (num / precision));
    return (precision < 1) ? (num / Math.round(1 / precision)) : (num * precision);
}

/**
 * Rounds the passed floating-point number to the next multiple of the
 * specified precision towards zero.
 *
 * @param num
 *  The floating-point number to be rounded.
 *
 * @param precision
 *  The precision used to round the number. The number 1 will truncate to
 *  integers (exactly like the function `Math.trunc()`). Must be positive. If
 *  less than 1, must be the inverse of an integer to prevent further internal
 *  rounding errors.
 *
 * @returns
 *  The rounded number.
 */
export function truncp(num: number, precision: number): number {
    // see comment in `roundp()`
    num = Math.trunc((precision < 1) ? (num * Math.round(1 / precision)) : (num / precision));
    return (precision < 1) ? (num / Math.round(1 / precision)) : (num * precision);
}

/**
 * Rounds the passed floating-point number to the next multiple of the
 * specified precision away from zero.
 *
 * @param num
 *  The floating-point number to be rounded.
 *
 * @param precision
 *  The precision used to round the number. The number 1 will inflate to
 *  integers (exactly like the function `inflate()`). Must be positive. If less
 *  than 1, must be the inverse of an integer to prevent further internal
 *  rounding errors.
 *
 * @returns
 *  The rounded number.
 */
export function inflatep(num: number, precision: number): number {
    // see comment in `roundp()`
    num = inflate((precision < 1) ? (num * Math.round(1 / precision)) : (num / precision));
    return (precision < 1) ? (num / Math.round(1 / precision)) : (num * precision);
}

/**
 * Returns whether the passed number is considered to be equal to zero. Beside
 * positive and negative zero, all denormalized numbers (all numbers with an
 * absolute value less than 2^-1022) will be handled as zero too.
 *
 * @param num
 *  The finite number to be checked.
 *
 * @returns
 *  Whether the passed number is zero, or denormalized. Returns `false` for
 *  positive/negative infinite, or for `NaN`.
 */
export function isZero(num: number): boolean {
    return Math.abs(num) < MIN_NORM_NUMBER;
}

/**
 * Calculates the decimal mantissa and exponent of the passed number.
 *
 * @param num
 *  The number to be split into mantissa and exponent.
 *
 * @returns
 *  The mantissa and exponent of the passed number.
 */
export function splitNumber(num: number): NumberComps {

    // special handling for zero
    if (num === 0) { return { mant: 0, expn: -Infinity }; }

    // split number into mantissa and exponent
    let expn = Math.floor(Math.log10(Math.abs(num)));
    let mant = num / Math.pow(10, expn);

    // due to rounding errors, the absolute value of the mantissa may become less than 1
    if (Math.abs(mant) < 1) { mant /= Math.abs(mant); }

    // due to rounding errors, the mantissa may become +-10 instead +-1, e.g. for the number 1000
    if (Math.abs(mant) >= 10) { mant /= 10; expn += 1; }

    return { mant, expn };
}

/**
 * Returns a random integer (equal distribution) inside the specified closed
 * interval.
 *
 * @param lower
 *  The lower boundary of the closed interval.
 *
 * @param upper
 *  The upper boundary of the closed interval. Must not be less than `lower`.
 *
 * @returns
 *  The random integer inside the specified interval.
 */
export function randomInt(lower: number, upper: number): number {
    return lower + Math.floor(Math.random() * (upper - lower + 1));
}

/**
 * Compares two values and returns an integral relation indicator.
 *
 * @param val1
 *  The first value to be compared.
 *
 * @param val2
 *  The second value to be compared.
 *
 * @returns
 *  The relation of the two value.
 */
export function compare<T>(val1: T, val2: T): Relation {
    return (val1 < val2) ? -1 : (val2 < val1) ? 1 : 0;
}

/**
 * The native less-than operator, as function.
 *
 * @param val1
 *  The first value to be compared.
 *
 * @param val2
 *  The second value to be compared.
 *
 * @returns
 *  The result of `val1 < val2`.
 */
export function less<T>(val1: T, val2: T): boolean {
    return val1 < val2;
}

/**
 * Compares two pairs of values. Defines a natural order for sorting with the
 * following behavior (let A and B be the pairs): If the first element of A is
 * less than the first element of B; or if both first elements are equal, and
 * the second element of A is less than the second element of B, then A is
 * considered less than B.
 *
 * @param a1
 *  The first element of the first pair.
 *
 * @param a2
 *  The second element of the first pair.
 *
 * @param b1
 *  The first element of the second pair.
 *
 * @param b2
 *  The second element of the second pair.
 *
 * @param [compareFn]
 *  A comparator function to be used for the passed values. If omitted, the
 *  standard utility function `compare()` will be used.
 *
 * @returns
 *  the relation of the passed pairs.
 */
export function comparePairs<T>(a1: T, a2: T, b1: T, b2: T, compareFn?: CompareFn<T>): Relation {
    compareFn = compareFn ?? compare;
    return sgn(compareFn(a1, b1) || compareFn(a2, b2));
}
