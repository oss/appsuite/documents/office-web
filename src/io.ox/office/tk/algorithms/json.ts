/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as fun from "@/io.ox/office/tk/algorithms/function";

// types ======================================================================

/**
 * Options for serializing JSON data.
 */
export interface StringifyJSONOptions {

    /**
     * Indentation size (space characters) in arrays and objects.
     */
    indent?: number;

    /**
     * If set to `true`, object keys will be sorted before serialization.
     *
     * Example: `{a:1,b:2}` and `{b:2,a:1}` may give different results when
     * passed to `JSON.stringify` directly, although the objects are equal.
     * With this option, both objects will be serialized to `{"a":1,"b":2}`.
     *
     * Default value is `false`.
     */
    sortKeys?: boolean;

    /**
     * Serializer callback function for elements found in objects and arrays,
     * and for the stringified value itself.
     */
    serializeFn?: (value: unknown) => unknown;
}

/**
 * Options for serializing JSON data safely.
 */
export interface SafeStringifyJSONOptions extends StringifyJSONOptions {

    /**
     * Fallback value to be returned when serialization of a JSON value fails.
     * Default value when omitting this option is the string "null" that
     * represents the stringified value `null`.
     */
    default?: string;
}

// private functions ==========================================================

/**
 * Implementation helper to generate serialized JSON with sorted object keys.
 */
function sortedStringifyJSON(value: unknown, baseIndent: string, nextIndent: string): string {

    // serialize array elements recursively
    if (is.array(value)) {
        if (value.length === 0) { return "[]"; }
        const newIndent = baseIndent + nextIndent;
        let result = "";
        for (const elem of value) {
            if (result) { result += ","; }
            result += newIndent + sortedStringifyJSON(elem, newIndent, nextIndent);
        }
        return `[${result}${baseIndent}]`;
    }

    // sort and serialize object properties
    if (is.dict(value)) {
        if (is.empty(value)) { return "{}"; }
        const newIndent = baseIndent + nextIndent;
        let result = "";
        for (const key of Object.keys(value).sort()) {
            if (result) { result += ","; }
            // eslint-disable-next-line no-restricted-properties
            result += `${newIndent}${JSON.stringify(key)}:${sortedStringifyJSON(value[key], newIndent, nextIndent)}`;
        }
        return `{${result}${baseIndent}}`;
    }

    // serialize everything else directly
    // eslint-disable-next-line no-restricted-properties
    return JSON.stringify(value);
}

// public functions ===========================================================

/**
 * Creates a flat clone of the passed value. Primitive values will be returned
 * as passed. Array elements and object properties will be copied by value.
 *
 * @param value
 *  The value to be cloned.
 *
 * @returns
 *  The flat clone of the passed value.
 */
export function flatClone<T>(value: T): T {
    if (is.primitive(value)) { return value; }
    if (is.array(value)) { return [...value] as T; }
    return { ...value };
}

/**
 * Creates a deep clone of the passed value. Primitive values will be returned
 * as passed. The values of array elements and object properties will be cloned
 * recursively.
 *
 * @param value
 *  The value to be cloned.
 *
 * @returns
 *  The deep clone of the passed value.
 */
export function deepClone<T>(value: T): T {
    if (is.primitive(value)) { return value; }
    if (is.array(value)) { return value.map(deepClone) as T; }
    const clone = Object.create(null) as T;
    for (const key in value) {
        clone[key] = deepClone(value[key]);
    }
    return clone;
}

/**
 * Same as `JSON.parse` but returns `unknown` instead of the unsafe `any`.
 * Throws if the passed string contains syntax errors! See function `safeParse`
 * for safe parsing.
 *
 * @param value
 *  The serialized JSON value to be parsed.
 *
 * @returns
 *  The parsed JSON value.
 */
export function tryParse(value: string): unknown {
    // eslint-disable-next-line no-restricted-properties
    return JSON.parse(value);
}

/**
 * Similar to `JSON.parse` but returns `unknown` instead of the unsafe `any`.
 * Catches exceptions and returns the specified default value instead.
 *
 * @param value
 *  The serialized JSON value to be parsed.
 *
 * @param [def]
 *  The default value to be returned if parsing the string fails.
 *
 * @returns
 *  The parsed JSON value.
 */
export function safeParse(value: string, def?: unknown): unknown {
    // eslint-disable-next-line no-restricted-properties
    return fun.try(() => JSON.parse(value), def);
}

/**
 * Similar to `JSON.stringify` but takes `unknown` instead of the unsafe `any`,
 * and provides additional options for serialization. Throws if the passed
 * object cannot be stringified! See `safeStringify` for safe serialization.
 *
 * @param value
 *  The JSON value to be serialized.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The serialized JSON value.
 */
export function tryStringify(value: unknown, options?: StringifyJSONOptions): string {

    // indentation size (zero does not use indentation at all)
    const indent = options?.indent ?? 0;

    // generate string with sorted object keys
    if (options?.sortKeys) {
        return sortedStringifyJSON(value, indent ? "\n" : "", " ".repeat(indent));
    }

    const replaceFn = options?.serializeFn ? (_k: string, v: unknown) => options.serializeFn!(v) : undefined;
    // eslint-disable-next-line no-restricted-properties
    return JSON.stringify(value, replaceFn, indent);
}

/**
 * Similar to `JSON.stringify` but takes `unknown` instead of the unsafe `any`,
 * and provides additional options for serialization. Catches exceptions and
 * returns the specified default value instead.
 *
 * @param value
 *  The JSON value to be serialized.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The serialized JSON value.
 */
export function safeStringify(value: unknown, options?: SafeStringifyJSONOptions): string {
    return fun.try(() => tryStringify(value, options), options?.default ?? "null");
}
