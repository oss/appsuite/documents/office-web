/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as json from "@/io.ox/office/tk/algorithms/json";

// public functions ===========================================================

/**
 * Works similarly to `JSON.stringify()`, but removes the quote characters of
 * object keys for better readability.
 *
 * **ATTENTION:** The result is *not* JSON. Use for debugging purposes only!
 *
 * @param value
 *  The JSON value to be stringified.
 *
 * @returns
 *  The stringified JSON value.
 */
export function stringify(value: unknown): string {
    return (value === undefined) ? "" : json.safeStringify(value, { default: "<error>" }).replace(/"(\w+)":/g, "$1:");
}

/**
 * Sends an error message to the browser console, if the passed value is an
 * internal JavaScript exception object (according to `is.scriptError`), or a
 * `DOMException`.
 *
 * @param data
 *  The data to be tested.
 *
 * @param [message]
 *  The error message to be sent to the browser console, if the passed value is
 *  an internal script error.
 */
export function logScriptError(data: unknown, message?: string): void {
    if (is.scriptError(data) || (data instanceof DOMException)) {
        window.console.error(message || "unhandled script exception", data);
    }
}
