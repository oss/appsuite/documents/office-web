/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as to from "@/io.ox/office/tk/algorithms/to";

// public functions ===========================================================

/**
 * Returns a property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist.
 *
 * @returns
 *  The value of the specified property if existing; otherwise the specified
 *  default value, or `undefined` if omitted.
 */
export function prop(data: unknown, key: string, def?: unknown): unknown {
    return (is.dict(data) && (key in data)) ? data[key] : def;
}

/**
 * Returns a number property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  a number.
 *
 * @returns
 *  The value of the specified property if existing and a number; otherwise the
 *  specified default value, or `undefined` if omitted.
 */
export function number(data: unknown, key: string, def: number): number;
export function number(data: unknown, key: string, def?: number): Opt<number>;
// implementation
export function number(data: unknown, key: string, def?: number): Opt<number> {
    const value = prop(data, key);
    return is.number(value) ? value : def;
}

/**
 * Returns a bigint property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  a bigint.
 *
 * @returns
 *  The value of the specified property if existing and a bigint; otherwise the
 *  specified default value, or `undefined` if omitted.
 */
export function bigint(data: unknown, key: string, def: bigint): bigint;
export function bigint(data: unknown, key: string, def?: bigint): Opt<bigint>;
// implementation
export function bigint(data: unknown, key: string, def?: bigint): Opt<bigint> {
    const value = prop(data, key);
    return is.bigint(value) ? value : def;
}

/**
 * Returns a string property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  a string.
 *
 * @returns
 *  The value of the specified property if existing and a string; otherwise the
 *  specified default value, or `undefined` if omitted.
 */
export function string(data: unknown, key: string, def: string): string;
export function string(data: unknown, key: string, def?: string): Opt<string>;
// implementation
export function string(data: unknown, key: string, def?: string): Opt<string> {
    const value = prop(data, key);
    return is.string(value) ? value : def;
}

/**
 * Returns an enumeration property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param EnumType
 *  The string enumeration type to be converted to.
 *
 * @param [def]
 *  The default value to be returned, if the passed value cannot be converted
 *  to an enumeration value.
 *
 * @returns
 *  The value of the specified property if existing and valid member of the
 *  enumeration; otherwise the specified default value, or `undefined` if
 *  omitted.
 */
function enumfn<T>(data: unknown, key: string, EnumType: T, def: T[keyof T]): T[keyof T];
function enumfn<T>(data: unknown, key: string, EnumType: T, def?: T[keyof T]): Opt<T[keyof T]>;
// implementation
function enumfn<T>(data: unknown, key: string, EnumType: T, def?: T[keyof T]): Opt<T[keyof T]> {
    return to.enum(EnumType, prop(data, key), def);
}
export { enumfn as enum };

/**
 * Returns a boolean property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  a boolean.
 *
 * @returns
 *  The value of the specified property if existing and a boolean; otherwise
 *  the specified default value, or `undefined` if omitted.
 */
export function boolean(data: unknown, key: string, def: boolean): boolean;
export function boolean(data: unknown, key: string, def?: boolean): Opt<boolean>;
// implementation
export function boolean(data: unknown, key: string, def?: boolean): Opt<boolean> {
    const value = prop(data, key);
    return is.boolean(value) ? value : def;
}

/**
 * Returns a dictionary property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  a dictionary. If the value `true` has been passed, an empty dictionary will
 *  be returned as default value.
 *
 * @returns
 *  The value of the specified property if existing and a dictionary; otherwise
 *  the specified default value, or `undefined` if omitted.
 */
export function dict(data: unknown, key: string, def: Dict | true): Dict;
export function dict(data: unknown, key: string, def?: Dict | true): Opt<Dict>;
// implementation
export function dict(data: unknown, key: string, def?: Dict | true): Opt<Dict> {
    return to.dict(prop(data, key), def);
}

/**
 * Returns an array property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  an array. If the value `true` has been passed, an empty array will be
 *  returned as default value.
 *
 * @returns
 *  The value of the specified property if existing and an array; otherwise the
 *  specified default value, or `undefined` if omitted.
 */
export function array(data: unknown, key: string, def: unknown[] | true): unknown[];
export function array(data: unknown, key: string, def?: unknown[] | true): Opt<unknown[]>;
// implementation
export function array(data: unknown, key: string, def?: unknown[] | true): Opt<unknown[]> {
    return to.array(prop(data, key), def);
}

/**
 * Returns a function property value from the passed dictionary.
 *
 * @param data
 *  The source data. If this value is not a dictionary, the default value will
 *  be returned from this function.
 *
 * @param key
 *  The name of the property whose value will be returned.
 *
 * @param [def]
 *  The default value to be returned if the property does not exist, or is not
 *  a function.
 *
 * @returns
 *  The value of the specified property if existing and a function; otherwise
 *  the specified default value, or `undefined` if omitted.
 */
function functionfn(data: unknown, key: string, def: AnyFunction): AnyFunction;
function functionfn(data: unknown, key: string, def?: AnyFunction): Opt<AnyFunction>;
// implementation
function functionfn(data: unknown, key: string, def?: AnyFunction): Opt<AnyFunction> {
    return to.function(prop(data, key), def);
}
export { functionfn as function };
