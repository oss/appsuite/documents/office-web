/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as math from "@/io.ox/office/tk/algorithms/math";
import * as itr from "@/io.ox/office/tk/algorithms/iterable";
import * as map from "@/io.ox/office/tk/algorithms/map";

// types ======================================================================

/**
 * A callback function for elements of an array-like object.
 *
 * @template RT
 *  The return type of the callback function.
 *
 * @template VT
 *  The value type of the array elements.
 *
 * @param value
 *  The value of the current element.
 *
 * @param index
 *  The index of the current element.
 */
export type ArrayCallbackFn<RT, VT> = (value: VT, index: number) => RT;

/**
 * A predicate callback function for elements of an array-like object.
 *
 * @template VT
 *  The value type of the array elements.
 *
 * @param value
 *  The value of the array element to be tested.
 *
 * @param index
 *  The array index of the element.
 *
 * @returns
 *  Whether the array element passes the truth test.
 */
export type ArrayPredicateFn<VT> = ArrayCallbackFn<boolean, VT>;

/**
 * A comparison function for insertion and deletion in a sorted array.
 *
 * @param value
 *  The value to be inserted into, or removed from a sorted array.
 *
 * @param elem
 *  An array element of the sorted array to be compared with the value.
 *
 * @param index
 *  The array index of the element (parameter "elem").
 *
 * @returns
 *  The relation between value and array element (in this order). The array
 *  algorithms assume that the array is sorted in a way that ALL "less-than"
 *  array elements (negative return value) are located before ALL "equal" array
 *  elements (return value `0`), which in turn are located before ALL
 *  "greater-than" array elements (positive return value).
 */
export type ArraySorterFn<VT, ET> = (value: VT, elem: ET, index: number) => number;

/**
 * Generic "reverse" option for array iterators, loop algorithms, etc.
 */
export interface ReverseOptions {

    /**
     * If set to `true`, the iterator or algorithm will process the data source
     * in reversed order, from its last to its first element. Default value is
     * `false`.
     */
    reverse?: boolean;
}

/**
 * Configuration options for looping over arrays.
 */
export interface ArrayLoopOptions extends ReverseOptions {

    /**
     * If specified, only the array elements with an index greater than or
     * equal to this value (in reversed mode: with an index less than or equal
     * to this value) will be visited. If omitted, all elements from the
     * beginning (in reverse mode: from the end) of the array will be visited.
     */
    begin?: number;

    /**
     * If specified, only the array elements with an index less than this value
     * (in reverse mode: with an index greater than this value) will be visited
     * (half-open interval!). If omitted, all elements to the end (in reverse
     * mode: to the beginning) of the array will be visited.
     */
    end?: number;
}

/**
 * Options for inserting values into a sorted array.
 */
export interface ArrayInsertSortedOptions {

    /**
     * If set to `true`, and the insert algorithm finds an array element that
     * is considered to be equal with the new value, the existing array element
     * will be removed from the array. By default, the new value will be
     * inserted after all existing equal array elements.
     */
    unique?: boolean;
}

// private functions ==========================================================

interface ArrayLoopParams {
    begin: number;
    end: number;
    step: number;
    reverse: boolean;
    cont: (ai: number) => boolean;
}

function getLoopParams(array: ArrayLike<unknown>, options?: ArrayLoopOptions): ArrayLoopParams {
    const reverse = !!options?.reverse;
    const begin = is.number(options?.begin) ? options.begin : reverse ? (array.length - 1) : 0;
    const end = is.number(options?.end) ? options.end : reverse ? -1 : array.length;
    const step = reverse ? -1 : 1;
    const cont = reverse ? ((ai: number) => ai > end) : ((ai: number) => ai < end);
    return { begin, end, step, reverse, cont };
}

// public functions ===========================================================

// array generators -----------------------------------------------------------

/**
 * Creates a new array from a data source and a mapping function. Works like
 * `Array.from` but supports inplace filtering (by returning `undefined`).
 *
 * @param source
 *  The data source to be converted to an array.
 *
 * @param fn
 *  A mapper function that converts the source values to the array elements. If
 *  the function returns `undefined`, the source value will be skipped.
 *
 * @returns
 *  The new array with the return values of the mapper function.
 */
export function from<RT, VT>(source: Iterable<VT>, fn: (value: VT) => Opt<RT>): RT[] {
    const array: RT[] = [];
    for (const value of source) {
        const result = fn(value);
        if (result !== undefined) { array.push(result); }
    }
    return array;
}

/**
 * Creates a new array from matching values of a data source. Works like
 * `Array#filter` but creates the array from any iterable.
 *
 * @param source
 *  The data source to be converted to an array.
 *
 * @param fn
 *  The predicate callback function invoked for the source values.
 *
 * @returns
 *  The new array with the matching values of the data source.
 */
export function filterFrom<VT>(source: Iterable<VT>, fn: (value: VT) => boolean): VT[] {
    const array: VT[] = [];
    for (const value of source) {
        if (fn(value)) { array.push(value); }
    }
    return array;
}

/**
 * Creates a new array from a data source and a mapping function. Works like
 * `Array#flatMap` but supports any iterable data source, any iterable result
 * value returned from the callback function, and better inplace filtering (by
 * returning `undefined` instead of an empty data source).
 *
 * @param source
 *  The data source to be converted to an array.
 *
 * @param fn
 *  A mapper function that converts the source values to the array elements. If
 *  the function returns `undefined`, the source value will be skipped.
 *
 * @returns
 *  The new array with the return values of the mapper function.
 */
export function flatFrom<RT, VT>(source: Iterable<VT>, fn: (value: VT) => Opt<Iterable<RT>>): RT[] {
    const array: RT[] = [];
    for (const value of source) {
        const result = fn(value);
        if (result) { append(array, result); }
    }
    return array;
}

/**
 * Creates a new sorted array from a data source.
 *
 * @param source
 *  The data source to be converted to an array, and sorted.
 *
 * @param fn
 *  The callback function invoked for each value in the data source to receive
 *  a sort index relative to the other values. The sort indexes will be
 *  compared with the builtin `<` operator.
 *
 * @returns
 *  The new sorted array with the values of the data source.
 */
export function sortFrom<VT>(source: Iterable<VT>, fn: (value: VT) => unknown): VT[] {
    return sortBy(Array.from(source), fn);
}

/**
 * Creates a new array from the results of a custom generator function.
 *
 * @param fn
 *  The generator function to be invoked immediately.
 *
 * @param [context]
 *  The calling context for the callback generator function (which cannot be
 *  written as arrow function).
 *
 * @returns
 *  A new array with all values yielded by the generator function.
 */
export function yieldFrom<VT>(fn: FuncType<Iterable<VT>>, context?: object): VT[] {
    return Array.from(context ? fn.call(context) : fn());
}

/**
 * Converts the passed value to an array.
 *
 * @param data
 *  The value to be converted to an array. If the value is a native array
 *  (i.e., instance of class `Array`), it will be returned as passed (no
 *  shallow copy). The value `undefined` will be converted to an empty array.
 *  All other values (including `null`, array-like values such as `Arguments`,
 *  `NodeList`, or `JQuery`) will be wrapped in a one-element array. Use
 *  `Array.from` to convert array-like values to arrays instead.
 *
 * @returns
 *  The passed value, if it is an array; otherwise an array containing that
 *  value as single element.
 */
export function wrap<T>(data: Opt<T | T[]>): T[];
export function wrap<T>(data: Opt<T | readonly T[]>): readonly T[];
// implementation
export function wrap<T>(data: Opt<T | T[]>): T[] {
    return is.array(data) ? data : (data === undefined) ? [] : [data];
}

/**
 * Returns a new array containing the passed values. Arrays as values will be
 * concatenated flatly. In difference to `wrap`, this function always returns a
 * new array, also for a single array as parameter.
 *
 * @param data
 *  The values to be concatenated flatly. If the value is a native array (i.e.,
 *  instance of class `Array`), its elements will be appended to the resulting
 *  array. The value `undefined` will be skipped. All other values (including
 *  `null`, array-like values such as `Arguments`, `NodeList`, or `JQuery`)
 *  will be pushed as single array element.
 */
export function concat<T>(...data: Array<Opt<T | T[]>>): T[];
export function concat<T>(...data: Array<Opt<T | readonly T[]>>): readonly T[];
// implementation
export function concat<T>(...data: Array<Opt<T | T[]>>): T[] {
    let result: T[] = [];
    for (const value of data) {
        if (is.array(value)) {
            result = result.concat(value);
        } else if (value !== undefined) {
            result.push(value);
        }
    }
    return result;
}

/**
 * Creates an array with the specified length, filled with a sequence of
 * increasing or decreasing numbers.
 *
 * @param length
 *  The length of the new array.
 *
 * @param offset
 *  The value of the first array element.
 *
 * @param step
 *  The difference between adjacent array elements.
 *
 * @returns
 *  The new array filled with the number sequence.
 */
export function sequence(length: number, offset = 0, step = 1): number[] {
    const array = new Array<number>(length);
    for (let ai = 0; ai < length; ai += 1) {
        array[ai] = ai * step + offset;
    }
    return array;
}

/**
 * Creates an array with the specified length, filled with a constant value.
 *
 * @param length
 *  The length of the new array.
 *
 * @param value
 *  The value to be set as array elements.
 *
 * @returns
 *  The new array filled with the specified value.
 */
export function fill<VT>(length: number, value: VT): VT[] {
    const array = new Array<VT>(length);
    for (let ai = 0; ai < length; ai += 1) {
        array[ai] = value;
    }
    return array;
}

/**
 * Creates an array with the specified length, filled with the results of a
 * callback function.
 *
 * @param length
 *  The length of the new array.
 *
 * @param fn
 *  The generator callback function that will be invoked for each new array
 *  element. The return values become the values of the array elements.
 *
 * @returns
 *  The new array filled with the results of the generator callback function.
 */
export function generate<VT>(length: number, fn: (index: number) => VT): VT[] {
    const array = new Array<VT>(length);
    for (let ai = 0; ai < length; ai += 1) {
        array[ai] = fn(ai);
    }
    return array;
}

/**
 * Destroys all elements in the passed array, and clears the array afterwards.
 *
 * @param array
 *  The array to be destroyed and cleared.
 */
export function destroy<VT extends Destroyable>(array: VT[]): void {
    for (const value of array) { value.destroy(); }
    array.length = 0;
}

// element comparison ---------------------------------------------------------

/**
 * Returns whether the passed arrays contain the exact same elements.
 *
 * @param array1
 *  The first array that will be compared to the second array.
 *
 * @param array2
 *  The second array that will be compared to the first array.
 *
 * @param [equalsFn]
 *  A custom comparator callback function for the array elements. If omitted,
 *  the built-in strict comparison will be used.
 *
 * @returns
 *  Whether the two arrays contain equal elements.
 */
export function equals<VT>(array1: ArrayLike<VT>, array2: ArrayLike<VT>, equalsFn?: (v1: VT, v2: VT) => boolean): boolean {

    // references to the same array
    if (array1 === array2) { return true; }

    // compare array length first
    if (array1.length !== array2.length) { return false; }

    // length of both arrays
    const { length } = array1;

    // compare all array elements with the passed callback, or with the equals
    // operator (keep the "if" outside the loops for better performance)
    if (equalsFn) {
        for (let ai = 0; ai < length; ai += 1) {
            if (!equalsFn(array1[ai], array2[ai])) { return false; }
        }
    } else {
        for (let ai = 0; ai < length; ai += 1) {
            if (array1[ai] !== array2[ai]) { return false; }
        }
    }

    return true;
}

/**
 * Compares the elements of the two passed arrays, starting with the first
 * element of both arrays, and visiting the following elements until a pair of
 * elements is different.
 *
 * @param array1
 *  The first array that will be compared to the second array.
 *
 * @param array2
 *  The second array that will be compared to the first array.
 *
 * @param [compareFn]
 *  A comparator function to be used for the passed values. If omitted, the
 *  standard utility function `compare` for arbitrary values will be used.
 *
 * @returns
 *  The relation of the two arrays.
 */
export function compare<VT>(array1: ArrayLike<VT>, array2: ArrayLike<VT>, compareFn: (v1: VT, v2: VT) => number = math.compare): math.Relation {

    // references to the same array
    if (array1 === array2) { return 0; }

    // minimum length of both arrays
    const length = Math.min(array1.length, array2.length);

    // compare all array elements
    for (let ai = 0; ai < length; ai += 1) {
        const result = compareFn(array1[ai], array2[ai]);
        if (result) { return math.sgn(result); }
    }

    // all compared elements are equal: array with more elements is considered greater
    return math.compare(array1.length, array2.length);
}

// element iteration ----------------------------------------------------------

/**
 * Creates an entry iterator for array elements. Works similar to the native
 * method `Array#entries` but supports array-like data sources, reverse
 * iteration, and element slices. Does not create an intermediate array in
 * memory but iterates directly on the data source.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @yields
 *  The entries (index/value pairs) of the passed array.
 */
export function *entries<VT>(array: ArrayLike<VT>, options?: ArrayLoopOptions): IndexedIterator<VT> {
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        yield [ai, array[ai]];
    }
}

/**
 * Creates a value iterator for array elements. Works similar to the native
 * method `Array#values` but supports array-like data sources, reverse
 * iteration, and element slices. Does not create an intermediate array in
 * memory but iterates directly on the data source.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @yields
 *  The values of the passed array.
 */
export function *values<VT>(array: ArrayLike<VT>, options?: ArrayLoopOptions): IterableIterator<VT> {
    // do not reuse `entries` to skip creation of intermediate arrays for each entry
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        yield array[ai];
    }
}

/**
 * Creates a value iterator for a closed interval of array elements. Does not
 * create an intermediate array in memory but iterates directly on the data
 * source.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param lower
 *  The lower index of the first array element in the interval.
 *
 * @param upper
 *  The upper index of the last array element in the interval (inclusive).
 *
 * @param [reverse]
 *  If set to `true`, the array elements will be visited in reversed order,
 *  from `upper` to `lower`.
 *
 * @yields
 *  The values inside the interval of the passed array.
 */
export function *interval<VT>(array: ArrayLike<VT>, lower: number, upper: number, reverse = false): IterableIterator<VT> {
    yield* values(array, { begin: reverse ? upper : lower, end: reverse ? (lower - 1) : (upper + 1), reverse });
}

/**
 * Visits the elements in the passed array. Works similar to the native method
 * `Array#forEach` but supports array-like data sources, reverse iteration, and
 * element slices.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param fn
 *  The callback function invoked for the array elements.
 *
 * @param [options]
 *  Optional parameters.
 */
export function forEach<VT>(array: ArrayLike<VT>, fn: ArrayCallbackFn<void, VT>, options?: ArrayLoopOptions): void {
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        fn(array[ai], ai);
    }
}

// aggregation algorithms -----------------------------------------------------

/**
 * Returns whether at least one element in the passed array passes a truth
 * test. Works similar to the the native method `Array#some` but supports
 * array-like data sources, reverse iteration, and element slices.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param fn
 *  The predicate callback function invoked for the array elements. Returns
 *  whether the element passes the test.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether at least one element in the passed array passes the truth test.
 */
export function some<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, options?: ArrayLoopOptions): boolean {
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        if (fn(array[ai], ai)) { return true; }
    }
    return false;
}

/**
 * Returns whether all elements in the passed array pass a truth test. Works
 * similar to the method `Array#every` but supports array-like data sources,
 * reverse iteration, and element slices.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param fn
 *  The predicate callback function invoked for the array elements. Returns
 *  whether the element passes the test.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether all elements in the passed array pass the truth test.
 */
export function every<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, options?: ArrayLoopOptions): boolean {
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        if (!fn(array[ai], ai)) { return false; }
    }
    return true;
}

/**
 * Counts the elements in the array that pass a truth test. Supports array-like
 * data sources, reverse iteration, and element slices.
 *
 * @param array
 *  An array-like sequence to be counted.
 *
 * @param fn
 *  A predicate callback function for the array elements. Returns whether the
 *  element shall be counted.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The number of elements passing the truth test.
 */
export function count<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, options?: ArrayLoopOptions): number {
    let result = 0;
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        if (fn(array[ai], ai)) { result += 1; }
    }
    return result;
}

/**
 * Distributes the elements of an array into a map of arrays that are keyed by
 * the return value of a callback function. Supports array-like data sources,
 * reverse iteration, and element slices.
 *
 * @param array
 *  An array-like sequence to be grouped.
 *
 * @param fn
 *  The callback function invoked for the values. The return value will be used
 *  as map key for the result. Return `undefined` to skip the value.
 *
 * @returns
 *  A map with multiple arrays containing the array elements grouped by the
 *  results of the callback function.
 */
export function group<KT, VT>(array: ArrayLike<VT>, fn: ArrayCallbackFn<Opt<KT>, VT>, options?: ArrayLoopOptions): Map<KT, VT[]> {
    const result = new Map<KT, VT[]>();
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        const value = array[ai];
        const key = fn(value, ai);
        if (key !== undefined) {
            map.upsert(result, key, () => []).push(value);
        }
    }
    return result;
}

/**
 * Distributes the elements of an array into a pair of arrays according to the
 * return value of a predicate callback function. Supports array-like data
 * sources, reverse iteration, and element slices.
 *
 * @param array
 *  An array-like sequence to be split.
 *
 * @param fn
 *  The predicate callback function invoked for the array elements. The return
 *  value will be used as pair element index (`false`: first element, `true`:
 *  second element).
 *
 * @returns
 *  A pair of arrays containing the array elements split by the results of the
 *  callback function.
 */
export function split<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, options?: ArrayLoopOptions): Pair<VT[]> {
    const result: Pair<VT[]> = [[], []];
    const { begin, cont, step } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        const value = array[ai];
        result[fn(value, ai) ? 1 : 0].push(value);
    }
    return result;
}

// in-place modification ------------------------------------------------------

/**
 * Appends the values of an iterable data source to an array _in-place_.
 *
 * @param array
 *  The array to be extended.
 *
 * @param source
 *  The iterable data source to be appended to the array.
 *
 * @param [fn]
 *  A mapper function that converts the values of the data source, and appends
 *  the results to the array. If omitted, the source values will be appended
 *  directly.
 *
 * @returns
 *  A reference to the passed array.
 */
export function append<VT>(array: VT[], source: Iterable<VT>): VT[];
export function append<VT, ST>(array: VT[], source: Iterable<ST>, fn: (value: ST) => VT): VT[];
// implementation
export function append<VT>(array: VT[], source: Iterable<unknown>, fn?: (value: unknown) => VT): VT[] {
    if (fn) {
        for (const value of source) {
            array.push(fn(value));
        }
    } else {
        for (const value of source) {
            array.push(value as VT);
        }
    }
    return array;
}

/**
 * Inserts the values of an iterable data source into an array _in-place_.
 *
 * @param array
 *  The array to be extended.
 *
 * @param index
 *  The new array index of the first element to be inserted. If negative,
 *  counts from the end of the array, like `Array#slice`.
 *
 * @param source
 *  The iterable data source to be inserted into the array.
 *
 * @param [fn]
 *  A mapper function that converts the values of the data source, and inserts
 *  the results to the array. If omitted, the source values will be inserted
 *  directly.
 *
 * @returns
 *  A reference to the passed array.
 */
export function insert<VT>(array: VT[], index: number, source: Iterable<VT>): VT[];
export function insert<VT, ST>(array: VT[], index: number, source: Iterable<ST>, fn: (value: ST) => VT): VT[];
// implementation
export function insert<VT>(array: VT[], index: number, source: Iterable<unknown>, fn?: (value: unknown) => VT): VT[] {
    const result = fn ? Array.from(source, fn) : (Array.from(source) as VT[]);
    array.splice(index, 0, ...result);
    return array;
}

/**
 * Inserts a value into an array _in-place_, shifts all following elements to
 * the end.
 *
 * @param array
 *  The array to insert the value into.
 *
 * @param index
 *  The new array index of the element to be inserted. If negative, counts from
 *  the end of the array, like `Array#slice`.
 *
 * @param value
 *  The value to be inserted into the array.
 *
 * @returns
 *  The passed value.
 */
export function insertAt<VT, UT extends VT>(array: VT[], index: number, value: UT): UT {
    array.splice(index, 0, value);
    return value;
}

/**
 * Removes an element from an array _in-place_, shifts all following elements
 * to front.
 *
 * @param array
 *  The array to remove an element from.
 *
 * @param index
 *  The array index of the element to be removed. If negative, counts from the
 *  end of the array, like `Array#slice`.
 *
 * @returns
 *  The value of the element removed from this array.
 */
export function deleteAt<VT>(array: VT[], index: number): Opt<VT> {
    return (index >= -array.length) ? array.splice(index, 1)[0] : undefined;
}

/**
 * Removes the first element from an array _in-place_ that equals the passed
 * value.
 *
 * @param array
 *  The array to remove an element from.
 *
 * @param value
 *  The value of the element to be removed from the array.
 *
 * @returns
 *  Whether the value was part of the array (the array has been shortened).
 */
export function deleteFirst<VT>(array: VT[], value: VT): boolean {
    const ai = array.indexOf(value);
    if (ai < 0) { return false; }
    array.splice(ai, 1);
    return true;
}

/**
 * Removes all elements from an array _in-place_ that equal the passed value.
 *
 * @param array
 *  The array to remove elements from.
 *
 * @param value
 *  The value of the elements to be removed from the array.
 *
 * @returns
 *  The number of elements removed from the array.
 */
export function deleteAll<VT>(array: VT[], value: VT): number {
    const { length } = array;
    for (let ai = length - 1; ai >= 0; ai -= 1) {
        if (array[ai] === value) { array.splice(ai, 1); }
    }
    return length - array.length;
}

/**
 * Removes the first element from the array _in-place_ that passes a truth
 * test.
 *
 * @param array
 *  The array to remove an element from.
 *
 * @param fn
 *  A predicate callback function for the array elements. Returns whether the
 *  element shall be removed from the array.
 *
 * @returns
 *  Whether an element was removed from the array.
 */
export function deleteFirstMatching<VT>(array: VT[], fn: ArrayPredicateFn<VT>): boolean {
    const ai = array.findIndex(fn);
    if (ai < 0) { return false; }
    array.splice(ai, 1);
    return true;
}

/**
 * Removes all elements from the array _in-place_ that pass a truth test.
 *
 * @param array
 *  The array to remove elements from.
 *
 * @param fn
 *  A predicate callback function for the array elements. Returns whether the
 *  element shall be removed from the array.
 *
 * @returns
 *  The number of elements removed from the array.
 */
export function deleteAllMatching<VT>(array: VT[], fn: ArrayPredicateFn<VT>): number {

    // original length of the array
    const { length } = array;

    // remove all non-matching elements in-place
    for (let ai = length - 1; ai >= 0; ai -= 1) {
        if (fn(array[ai], ai)) { array.splice(ai, 1); }
    }

    // return the number of removed elements
    return length - array.length;
}

/**
 * Inserts a value into a sorted array, using binary search for determining the
 * insertion position.
 *
 * @param array
 *  The sorted array to insert the value into.
 *
 * @param value
 *  The value to be inserted into the array.
 *
 * @param fn
 *  A comparison function for the value and the array elements.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The value of the element removed from this array.
 */
export function insertSorted<ET, VT extends ET>(array: ET[], value: VT, fn: ArraySorterFn<VT, ET>, options?: ArrayInsertSortedOptions): VT {
    // binary search for insert position (first array element greater than the passed value)
    const ai = fastFindFirstIndex(array, (el, ai) => fn(value, el, ai) < 0);
    // ensure uniqueness: replace preceding equalish element
    if (options?.unique && (ai > 0) && !fn(value, array[ai - 1], ai - 1)) {
        array.splice(ai - 1, 1, value);
    } else {
        // otherwise, insert new value after all (possibly existing) equalish elements
        array.splice(ai, 0, value);
    }
    return value;
}

/**
 * Removes a value from a sorted array, using binary search for determining the
 * deletion position.
 *
 * @param array
 *  The sorted array to delete a value from.
 *
 * @param value
 *  The value to be deleted from the array.
 *
 * @param fn
 *  A comparison function for the value and the array elements. The algorithm
 *  will remove the first array element that is considered to be equal
 * according to this callback function (without checking value equality).
 *
 * @returns
 *  The passed value.
 */
export function deleteSorted<ET, VT extends ET>(array: ET[], value: VT, fn: ArraySorterFn<VT, ET>): Opt<ET> {
    // binary search for deletion position (first array element greater than or equal to the passed value)
    const ai = fastFindFirstIndex(array, (el, ai) => fn(value, el, ai) <= 0);
    // check for equality using the passed comparison function
    return ((ai < array.length) && !fn(value, array[ai], ai)) ? array.splice(ai, 1)[0] : undefined;
}

/**
 * Sorts the elements of an array _in-place_. The elements will be sorted
 * according to the return values of the callback function.
 *
 * @param array
 *  The array to be sorted.
 *
 * @param fn
 *  The callback function that will be invoked for each element in the array to
 *  receive a sort index relative to the other elements. The sort indexes will
 *  be compared with the builtin `<` operator.
 *
 * @returns
 *  A reference to the passed array.
 */
export function sortBy<VT>(array: VT[], fn: (value: VT) => unknown): VT[] {
    return array.sort((v1, v2) => math.compare(fn(v1), fn(v2)));
}

/**
 * Removes all duplicates from the array _in-place_.
 *
 * @param array
 *  The array to be unified.
 *
 * @returns
 *  A reference to the passed array.
 */
export function unify<VT>(array: VT[]): VT[] {
    const seen = new Set<VT>();
    for (let ai = 0; ai < array.length;) {
        const value = array[ai];
        if (seen.has(value)) {
            deleteAt(array, ai);
        } else {
            seen.add(value);
            ai += 1;
        }
    }
    return array;
}

/**
 * Reorders the elements of an array _in-place_.
 *
 * @param array
 *  An array-like sequence to be reordered.
 *
 * @param permutation
 *  The permutation vector for the array elements. The *values* in this vector
 *  represent the old positions (indexes) of the elements in the array, that
 *  will be moved to the respective location (the *index*) in the permutation
 *  vector.
 *
 *  Example: The permutation vector `[2,0,1]` will move the array element with
 *  index 2 (first value in `permutation`) to index 0, the array element with
 *  index 0 (second value) to index 1, and the element with index 1 (third
 *  value) to index 2.
 *
 * @returns
 *  A reference to the passed array.
 */
export function permute<VT>(array: VT[], permutation: ArrayLike<number>): VT[] {
    array.splice(0).forEach((_value, index, orig) => array.push(orig[permutation[index]]));
    return array;
}

// find algorithms ------------------------------------------------------------

/**
 * Returns the element at the specified index. Works similar to the method
 * `Array#at` but supports array-like data sources.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param index
 *  The index of the element to be returned. Negative indexes count from the
 *  end of the array (`-1` returns the last element).
 *
 * @returns
 *  The array element, or `undefined`.
 */
export function at<VT>(array: ArrayLike<VT>, index: number): Opt<VT> {
    if (index < 0) { index += array.length; }
    return array[index];
}

/**
 * Returns the last element of the array-like sequence.
 *
 * @param array
 *  An array-like sequence.
 *
 * @returns
 *  The last array element, or `undefined`.
 */
export function last<VT>(array: ArrayLike<VT>): Opt<VT> {
    return array[array.length - 1];
}

/**
 * Returns everything but the last entry of the array. If n is
 * specified with a positive number, the last n elements are
 * removed.
 * If n is specified with a number smaller than 1, no element
 * is removed.
 *
 * @param array
 *  An array-like sequence.
 *
 * @param [n]
 *  The positive number of elements to be removed from the end
 *  of the specified array.
 *
 * @returns
 *  An array without the last n elements.
 */
export function initial<VT>(array: ArrayLike<VT>, n = 1): VT[] {
    if (n < 1) { return Array.from(array); }
    const result = [];
    const length = array.length - n;
    for (let i = 0; i < length; i += 1) { result[i] = array[i]; }
    return result;
}

/**
 * Returns the array index of the first element in the passed array that passes
 * a truth test. Works similar to the method `Array#findIndex` but supports
 * array-like data sources, reverse iteration (find last element), and element
 * slices.
 *
 * @param array
 *  An array-like sequence to be searched for a matching element.
 *
 * @param fn
 *  The predicate callback function implementing the truth test.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The index of the first matching array element; or the length of the array
 *  (`-1` in reverse mode), if no matching array element has been found.
 */
export function findIndex<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, options?: ArrayLoopOptions): number {
    const { begin, cont, step, reverse } = getLoopParams(array, options);
    for (let ai = begin; cont(ai); ai += step) {
        if (fn(array[ai], ai)) { return ai; }
    }
    return reverse ? -1 : array.length;
}

/**
 * Returns the first element in the passed array that passes a truth test.
 * Works similar to the method `Array#find` but supports array-like data
 * sources, reverse iteration (find last element), and element slices.
 *
 * @param array
 *  An array-like sequence to be searched for a matching element.
 *
 * @param fn
 *  The predicate callback function implementing the truth test.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The first matching array element; or `undefined`, if no matching array
 *  element has been found.
 */
export function findValue<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, options?: ArrayLoopOptions): Opt<VT> {
    return array[findIndex(array, fn, options)];
}

/**
 * Returns the array index of the first element in an ordered array that passes
 * a truth test. Supports array-like data sources.
 *
 * _Attention:_ This function uses a fast binary search to find the array
 * element. The elements in the passed array are assumed to be sorted in a way
 * that no element that passes the truth test precedes an element that does not
 * pass, i.e. _all non-matching_ elements in front, _all matching_ elements at
 * the end of the array.
 *
 * @param array
 *  An array-like sequence to be searched for a matching element.
 *
 * @param fn
 *  The predicate callback function implementing the truth test.
 *
 * @param [begin]
 *  Array index of the element to start from. If omitted, the entire array will
 *  be searched.
 *
 * @returns
 *  The index of the first matching array element; or the length of the array,
 *  if no matching array element has been found.
 */
export function fastFindFirstIndex<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, begin?: number): number {

    // fast binary search
    let ai1 = begin ?? 0, ai2 = array.length;
    while (ai1 < ai2) {
        const ai = Math.floor((ai1 + ai2) / 2);
        if (fn(array[ai], ai)) {
            ai2 = ai;
        } else {
            ai1 = ai + 1;
        }
    }
    return ai1;
}

/**
 * Returns the first element in an ordered array that passes a truth test.
 * Supports array-like data sources.
 *
 * _Attention:_ This function uses a fast binary search to find the array
 * element. The elements in the passed array are assumed to be sorted in a way
 * that no element that passes the truth test precedes an element that does not
 * pass, i.e. _all non-matching_ elements in front, _all matching_ elements at
 * the end of the array.
 *
 * @param array
 *  An array-like sequence to be searched for a matching element.
 *
 * @param fn
 *  The predicate callback function implementing the truth test.
 *
 * @param [begin]
 *  Array index of the element to start from. If omitted, the entire array will
 *  be searched.
 *
 * @returns
 *  The first matching array element; or `undefined`, if no matching array
 *  element has been found.
 */
export function fastFindFirstValue<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, begin?: number): Opt<VT> {
    return array[fastFindFirstIndex(array, fn, begin)];
}

/**
 * Returns the array index of the last element in an ordered array that passes
 * a truth test. Supports array-like data sources.
 *
 * _Attention:_ This function uses a fast binary search to find the array
 * element. The elements in the passed array are assumed to be sorted in a way
 * that no element that passes the truth test precedes an element that does not
 * pass, i.e. _all matching_ elements in front, _all non-matching_ elements at
 * the end of the array.
 *
 * @param array
 *  An array-like sequence to be searched for a matching element.
 *
 * @param fn
 *  The predicate callback function implementing the truth test.
 *
 * @param [begin]
 *  Array index of the element to start from (in reversed direction towards the
 *  beginning of the array). If omitted, the entire array will be searched.
 *
 * @returns
 *  The index of the first matching array element; or `-1`, if no matching
 *  array element has been found.
 */
export function fastFindLastIndex<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, begin?: number): number {

    // fast binary search
    let ai1 = -1, ai2 = begin ?? (array.length - 1);
    while (ai1 < ai2) {
        const ai = Math.ceil((ai1 + ai2) / 2);
        if (fn(array[ai], ai)) {
            ai1 = ai;
        } else {
            ai2 = ai - 1;
        }
    }
    return ai2;
}

/**
 * Returns the last element in an ordered array that passes a truth test.
 * Supports array-like data sources.
 *
 * _Attention:_ This function uses a fast binary search to find the array
 * element. The elements in the passed array are assumed to be sorted in a way
 * that no element that passes the truth test precedes an element that does not
 * pass, i.e. _all matching_ elements in front, _all non-matching_ elements at
 * the end of the array.
 *
 * @param array
 *  An array-like sequence to be searched for a matching element.
 *
 * @param fn
 *  The predicate callback function implementing the truth test.
 *
 * @param [begin]
 *  Array index of the element to start from (in reversed direction towards the
 *  beginning of the array). If omitted, the entire array will be searched.
 *
 * @returns
 *  The first matching array element; or `undefined`, if no matching array
 *  element has been found.
 */
export function fastFindLastValue<VT>(array: ArrayLike<VT>, fn: ArrayPredicateFn<VT>, begin?: number): Opt<VT> {
    return array[fastFindLastIndex(array, fn, begin)];
}

/**
 * Returns the array index of the element in an array that is the nearest to a
 * given offset, according to the results of a callback function.
 *
 * @param array
 *  An array-like sequence to be searched for an element.
 *
 * @param offset
 *  The target offset.
 *
 * @param fn
 *  The callback function returning an offset for the array elements.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The index of the array element that is nearest to the given offset, or zero
 *  for empty arrays.
 */
export function nearestIndex<VT>(array: ArrayLike<VT>, offset: number, fn: ArrayCallbackFn<number, VT>, options?: ArrayLoopOptions): number {
    const result = itr.nearest(entries(array, options), offset, entry => fn(entry[1], entry[0]));
    return result ? result[0] : 0;
}

/**
 * Returns the element in an array that is the nearest to a given offset,
 * according to the results of a callback function.
 *
 * @param array
 *  An array-like sequence to be searched for an element.
 *
 * @param offset
 *  The target offset.
 *
 * @param fn
 *  The callback function returning an offset for the array elements.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The element that is nearest to the given offset, or `undefined` for empty
 *  arrays.
 */
export function nearestValue<VT>(array: ArrayLike<VT>, offset: number, fn: ArrayCallbackFn<number, VT>, options?: ArrayLoopOptions): Opt<VT> {
    return array[nearestIndex(array, offset, fn, options)];
}

// draining generators --------------------------------------------------------

/**
 * Creates a destructive iterator that shifts the first element from the passed
 * array.
 *
 * @param array
 *  The array to be iterated. It will be emptied inplace during iteration.
 *
 * @yields
 *  The first element of the remaining array.
 */
export function *shiftValues<VT>(array: VT[]): IterableIterator<VT> {
    while (array.length) {
        yield array.shift()!;
    }
}

/**
 * Creates a destructive iterator that pops the last element from the passed
 * array.
 *
 * @param array
 *  The array to be iterated. It will be emptied inplace during iteration.
 *
 * @yields
 *  The last element of the remaining array.
 */
export function *popValues<VT>(array: VT[]): IterableIterator<VT> {
    while (array.length) {
        yield array.pop()!;
    }
}
