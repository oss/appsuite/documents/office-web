/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as math from "@/io.ox/office/tk/algorithms/math";

// constants ==================================================================

/**
 * Multiplicator for converting high surrogates to Unicode code point values.
 */
const SURR_DATA_SHIFT = 2 ** 10;

/**
 * Bit mask for extracting the data bits from UTF-16 surrogates.
 */
const SURR_DATA_MASK = SURR_DATA_SHIFT - 1;

/**
 * Bit mask for detecting UTF-16 surrogates.
 */
const SURR_MARK_MASK = 0xFFFF ^ SURR_DATA_MASK;

/**
 * Marker bits of a UTF-16 high surrogate.
 */
const HI_SURR_MARK = 0xD800;

/**
 * Marker bits of a UTF-16 low surrogate.
 */
const LO_SURR_MARK = 0xDC00;

// private functions ==========================================================

/**
 * Returns whether the passed UTF-16 code unit is a UTF-16 high surrogate (in
 * the range `U+D800...U+DBFF`).
 */
function isHighSurrCode(code: number): boolean {
    return (code & SURR_MARK_MASK) === HI_SURR_MARK; // will not pass for `NaN`
}

/**
 * Returns whether the passed UTF-16 code unit is a UTF-16 low surrogate (in
 * the range `U+DC00...U+DFFF`).
 */
function isLowSurrCode(code: number): boolean {
    return (code & SURR_MARK_MASK) === LO_SURR_MARK; // will not pass for `NaN`
}

// public functions ===========================================================

/**
 * Returns whether a UTF-16 code unit in the passed string is a high surrogate
 * (a code unit in the range `U+D800`-`U+DBFF`).
 *
 * @param text
 *  The UTF-16 encoded text to be checked.
 *
 * @param offset
 *  The offset (native array index) of the UTF-16 code unit in the string. Note
 *  that this is *not* the Unicode character index (see function `offset` to
 *  convert a Unicode character index to a UTF-16 string offset).
 *
 * @returns
 *  Whether the specified UTF-16 code unit is a high surrogate.
 */
export function hasHighSurrogate(text: string, offset: number): boolean {
    return isHighSurrCode(text.charCodeAt(offset));
}

/**
 * Returns whether a UTF-16 code unit in the passed string is a low surrogate
 * (a code unit in the range `U+DC00`-`U+DFFF`).
 *
 * @param text
 *  The UTF-16 encoded text to be checked.
 *
 * @param offset
 *  The offset (native array index) of the UTF-16 code unit in the string. Note
 *  that this is *not* the Unicode character index (see function `offset` to
 *  convert a Unicode character index to a UTF-16 string offset).
 *
 * @returns
 *  Whether the specified UTF-16 code unit is a low surrogate.
 */
export function hasLowSurrogate(text: string, offset: number): boolean {
    return isLowSurrCode(text.charCodeAt(offset));
}

/**
 * Returns whether two consecutive UTF-16 code units in the passed string form
 * a surrogate pair.
 *
 * @param text
 *  The UTF-16 encoded text to be checked.
 *
 * @param offset
 *  The offset (native array index) of the (first) UTF-16 code unit in the
 *  string. Note that this is *not* the Unicode character index (see function
 *  `offset` to convert a Unicode character index to a UTF-16 string offset).
 *
 * @returns
 *  Whether two consecutive UTF-16 code units in the passed string form a
 *  surrogate pair.
 */
export function hasSurrogatePair(text: string, offset: number): boolean {
    return isHighSurrCode(text.charCodeAt(offset)) && isLowSurrCode(text.charCodeAt(offset + 1));
}

/**
 * Decreases the passed offset of a UTF-16 code unit in a string, if it points
 * to the second part of a UTF-16 surrogate pair (the low surrogate).
 *
 * @param text
 *  The UTF-16 encoded text to be examined.
 *
 * @param offset
 *  The offset (native array index) of a UTF-16 code unit in the string.
 *
 * @returns
 *  The original offset, if it points to a valid code unit; or the offset
 *  decreased by one, if it points to the second part of a UTF-16 surrogate
 *  pair (the low surrogate).
 */
export function sanitizeOffset(text: string, offset: number): number {
    return ((offset > 0) && hasSurrogatePair(text, offset - 1)) ? (offset - 1) : offset;
}

/**
 * Returns the number of Unicode characters in the passed string. UTF-16
 * surrogate pairs count as single Unicode characters.
 *
 * @param text
 *  The UTF-16 encoded text to be checked.
 *
 * @returns
 *  The number of Unicode characters in the passed text.
 */
export function length(text: string): number {
    let count = 0;
    for (let offset = 0, len = text.length; (offset < len); offset += 1, count += 1) {
        if (hasSurrogatePair(text, offset)) { offset += 1; }
    }
    return count;
}

/**
 * Returns the offset of the (first) UTF-16 code unit of a Unicode character in
 * the passed string. UTF-16 surrogate pairs count as single Unicode
 * characters.
 *
 * @param text
 *  The UTF-16 encoded text to be checked.
 *
 * @param index
 *  The index of the Unicode character. UTF-16 surrogate pairs count as single
 *  Unicode character. Negative values count from the end of the string (like
 *  the native method `String#slice`).
 *
 * @param [clamped]
 *  If set to `true`, the returned offset will be clamped to the range of valid
 *  offsets according to the length of the string (the array index interval
 *  `[0...text.length]`). By default, invalid indexes result in `NaN`.
 *
 * @returns
 *  The offset (array index) of the (first) UTF-16 code unit of the specified
 *  Unicode character in the passed text; or `NaN` for invalid character
 *  indexes.
 */
function offsetfn(text: string, index: number, clamped = false): number {

    // empty string: `NaN` (except for clamped mode)
    // index is zero: offset is zero (except for empty text)
    if (!text || !index) { return (clamped || (text && !index)) ? 0 : Number.NaN; }

    // count the code units from beginning or end of the string
    let offset: number;
    const len = text.length;
    if (index >= 0) {
        offset = 0;
        while ((index > 0) && ((offset < len))) {
            offset += hasSurrogatePair(text, offset) ? 2 : 1;
            index -= 1;
        }
    } else {
        offset = len;
        while ((index < 0) && (offset >= 0)) {
            offset -= hasSurrogatePair(text, offset - 2) ? 2 : 1;
            index += 1;
        }
    }

    // offset out of bounds: `NaN` (clamped mode: clamp to string length)
    return clamped ? math.clamp(offset, 0, len) : ((offset >= 0) && (offset < len)) ? offset : Number.NaN;
}
export { offsetfn as offset };

/**
 * Returns the offset interval of the UTF-16 code units covering the specified
 * slice of Unicode characters in the passed string. UTF-16 surrogate pairs
 * count as single Unicode characters. The returned offset interval can be used
 * with the native method `String#slice`.
 *
 * @param text
 *  The UTF-16 encoded text to be checked.
 *
 * @param [begin=0]
 *  The index of the first Unicode character to be included in the slice.
 *  UTF-16 surrogate pairs count as single Unicode characters. Negative values
 *  count from the end of the string. If omitted, zero will be returned as
 *  start offset (like the native method `String#slice`).
 *
 * @param [end]
 *  The index of the Unicode character behind the last character to be included
 *  in the slice (half-open interval). UTF-16 surrogate pairs count as single
 *  Unicode characters. Negative values count from the end of the string. If
 *  omitted, the length of the string will be returned as end offset (like the
 *  native method `String#slice`).
 *
 * @returns
 *  The UTF-16 offset interval of the specified Unicode character slice, ready
 *  to be passed to the native method `String#slice`. The offsets will always
 *  be non-negative (negative input parameters will be converted to effective
 *  UTF-16 offsets).
 */
export function sliceOffsets(text: string, begin?: number, end?: number): Pair<number> {

    // find the start offset (native array index) in the string;
    // if start point is behind end of string, no further processing needed
    const offset1 = is.number(begin) ? offsetfn(text, begin, true) : 0;
    if (offset1 === text.length) { return [offset1, offset1]; }

    // find the end offset (native array index) in the string
    const offset2 = is.number(end) ? Math.max(offset1, offsetfn(text, end, true)) : text.length;
    return [offset1, offset2];
}

/**
 * Returns a Unicode character from the passed string. UTF-16 surrogate pairs
 * count as single Unicode characters.
 *
 * @param text
 *  The UTF-16 encoded text to be examined.
 *
 * @param index
 *  The index of the Unicode character to be returned. UTF-16 surrogate pairs
 *  count as single Unicode characters. Negative values count from the end of
 *  the string (like the native method `String#slice`).
 *
 * @returns
 *  The specified Unicode character from the passed text (may be a UTF-16
 *  surrogate pair), or the empty string for invalid character indexes (like
 *  the native method `String#charAt`).
 */
export function charAt(text: string, index: number): string {
    const offset = offsetfn(text, index);
    return !Number.isFinite(offset) ? "" : hasSurrogatePair(text, offset) ? text.substr(offset, 2) : text[offset];
}

/**
 * Returns a subslice of Unicode characters from the passed string. UTF-16
 * surrogate pairs count as single Unicode characters.
 *
 * @param text
 *  The UTF-16 encoded text to be sliced.
 *
 * @param [begin=0]
 *  The index of the first Unicode character to be returned. UTF-16 surrogate
 *  pairs count as single Unicode characters. Negative values count from the
 *  end of the string. If omitted, all characters from the beginning of the
 *  string will be returned (like the native method `String#slice`).
 *
 * @param [end]
 *  The index of the Unicode character behind the last character to be returned
 *  (half-open interval). UTF-16 surrogate pairs count as single Unicode
 *  characters. Negative values count from the end of the string. If omitted,
 *  all characters to the end of the string will be returned (like the native
 *  method `String#slice`).
 *
 * @returns
 *  The specified Unicode character slice from the passed text.
 */
export function slice(text: string, begin?: number, end?: number): string {
    return text.slice(...sliceOffsets(text, begin, end));
}
