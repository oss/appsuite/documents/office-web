/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// constants ==================================================================

const { hasOwnProperty } = Object.prototype;

// public functions ===========================================================

/**
 * Returns whether the object contains an own property with the specified key.
 *
 * This function is a polyfill for the upcoming function `Object.hasOwn`.
 */
export function hasOwn(instance: object, key: PropertyKey): boolean {
    return hasOwnProperty.call(instance, key);
}

/**
 * Returns an own property with the specified key of the object if available.
 */
export function getOwn(instance: object, key: PropertyKey): unknown {
    return Object.getOwnPropertyDescriptor(instance, key)?.value;
}

/**
 * Returns the prototype of the passed object with correct type.
 *
 * This function is a more type-safe alternative for `Object.getPrototypeOf`.
 */
export const getPrototypeOf = Object.getPrototypeOf as FuncType<object | null, [unknown]>;
