/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as math from "@/io.ox/office/tk/algorithms/math";

// types ======================================================================

/**
 * Represents the two-dimensional cartesian coordinates of a point.
 */
export interface Point {

    /**
     *  The X coordinate of the point.
     */
    x: number;

    /**
     *  The Y coordinate of the point.
     */
    y: number;
}

/**
 * Represents the two-dimensional polar coordinates of a point (the length and
 * angle of its vector from the origin).
 */
export interface Polar {

    /**
     * The radius, i.e. the distance between origin and the point.
     */
    r: number;

    /**
     * The angle between the positive X axis and the vector, in radians.
     *
     * This value will be positive, if the Y coordinate of the point is
     * positive. This value will be zero, if the point is equal to the origin.
     */
    a: number;
}

/**
 * Represents the two-dimensional size of an object.
 */
export interface Size {

    /**
     *  The width of the object.
     */
    width: number;

    /**
     *  The height of the object.
     */
    height: number;
}

// public functions ===========================================================

/**
 * Creates a new `Point` object.
 *
 * @param x
 *  The X coordinate of the point.
 *
 * @param y
 *  The Y coordinate of the point.
 *
 * @returns
 *  The new `Point` object.
 */
export function point(x: number, y: number): Point {
    return { x, y };
}

/**
 * Creates a new `Polar` object.
 *
 * @param r
 *  The radius, i.e. the distance between origin and the point.
 *
 * @param a
 *  The angle between the positive X axis and the vector, in radians.
 *
 * @returns
 *  The new `Polar` object.
 */
export function polar(r: number, a: number): Polar {
    return { r, a };
}

/**
 * Converts cartesian coordinates to polar coordinates.
 *
 * @param x
 *  The X coordinate of the point.
 *
 * @param y
 *  The Y coordinate of the point.
 *
 * @returns
 *  The resulting polar coordinates of the point.
 */
export function pointToPolar(x: number, y: number): Polar {
    return polar(math.radius(x, y), Math.atan2(y, x));
}

/**
 * Converts polar coordinates to cartesian coordinates.
 *
 * @param r
 *  The radius, i.e. the distance between origin and the point.
 *
 * @param a
 *  The angle between the positive X axis and radius, in radians.
 *
 * @returns
 *  The resulting cartesian coordinates of the point.
 */
export function polarToPoint(r: number, a: number): Point {
    return { x: r * Math.cos(a), y: r * Math.sin(a) };
}

/**
 * Returns the horizontal and vertical signed differences between two cartesian
 * points.
 *
 * @param p1
 *  The start point.
 *
 * @param p2
 *  The end point.
 *
 * @returns
 *  The signed differences between the coordinates of start and end point.
 */
export function pointsDiff(p1: Point, p2: Point): Point {
    return point(p2.x - p1.x, p2.y - p1.y);
}

/**
 * Returns the absolute distance between two cartesian points.
 *
 * @param p1
 *  The start point.
 *
 * @param p2
 *  The end point.
 *
 * @returns
 *  The distance between start and end point.
 */
export function pointsDist(p1: Point, p2: Point): number {
    return math.radius(p2.x - p1.x, p2.y - p1.y);
}
