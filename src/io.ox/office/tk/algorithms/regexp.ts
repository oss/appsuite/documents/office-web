/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * Argument type for functions taking RE patterns as arguments. Falsy values
 * (`undefined`, `null`, `false`, and empty strings) will be ignored.
 */
export type Pattern = Nullable<string | false>;

// public functions ===========================================================

/**
 * Escapes all special characters for regular expressions in the passed string.
 * The resulting string can be used to create regular expression objects that
 * will match the original string literally.
 *
 * @param text
 *  The string to be converted to a literal regular expression pattern.
 *
 * @returns
 *  A regular expression pattern that will match the passed string literally.
 */
export function escape(text: string): string {
    return text.replace(/([$^*+?!:=.|(){}[\]\\])/g, (_, c) => `\\${c}`);
}

/**
 * Creates a list of alternative RE patterns, separated with pipe characters.
 *
 * @param patterns
 *  The RE patterns to be concatenated to a list of alternatives. All falsy
 *  values will be filtered.
 *
 * @returns
 *  The resulting pattern containing all alternatives, separated with pipe
 *  characters.
 */
export function alt(...patterns: Pattern[]): string {
    return patterns.filter(Boolean).join("|");
}

/**
 * Encloses the passed RE patterns into parentheses to create a capturing
 * group. Multiple patterns will be separated with pipe characters (see
 * function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to a capturing group. All falsy values will
 *  be filtered.
 *
 * @returns
 *  The resulting capturing group pattern containing all alternatives.
 */
export function capture(...patterns: Pattern[]): string {
    return "(" + alt(...patterns) + ")";
}

/**
 * Encloses the passed RE patterns into parentheses to create a non-capturing
 * group. Multiple patterns will be separated with pipe characters (see
 * function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to a non-capturing group. All falsy values
 *  will be filtered.
 *
 * @returns
 *  The resulting non-capturing group pattern containing all alternatives.
 */
export function group(...patterns: Pattern[]): string {
    return "(?:" + alt(...patterns) + ")";
}

/**
 * Encloses the passed RE patterns into parentheses and appends a question mark
 * to create an optional non-capturing group. Multiple patterns will be
 * separated with pipe characters (see function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to an optional non-capturing group. All
 *  falsy values will be filtered.
 *
 * @returns
 *  The resulting optional non-capturing group pattern containing all
 *  alternatives.
 */
group.opt = (...patterns: Pattern[]): string => {
    return group(...patterns) + "?";
};

/**
 * Creates a lookahead assertion for the passed RE patterns. Multiple patterns
 * will be separated with pipe characters (see function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to a lookahead assertion. All falsy values
 *  will be filtered.
 *
 * @returns
 *  The resulting lookahead assertion pattern.
 */
export function ahead(...patterns: Pattern[]): string {
    return "(?=" + alt(...patterns) + ")";
}

/**
 * Creates a negative lookahead assertion for the passed RE patterns. Multiple
 * patterns will be separated with pipe characters (see function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to a negative lookahead assertion. All
 *  falsy values will be filtered.
 *
 * @returns
 *  The resulting negative lookahead assertion pattern.
 */
ahead.neg = (...patterns: Pattern[]): string => {
    return "(?!" + alt(...patterns) + ")";
};

/**
 * Creates a lookbehind assertion for the passed RE patterns. Multiple patterns
 * will be separated with pipe characters (see function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to a lookbehind assertion. All falsy values
 *  will be filtered.
 *
 * @returns
 *  The resulting lookbehind assertion pattern.
 */
export function behind(...patterns: Pattern[]): string {
    return "(?<=" + alt(...patterns) + ")";
}

/**
 * Creates a negative lookbehind assertion for the passed RE patterns. Multiple
 * patterns will be separated with pipe characters (see function `alt`).
 *
 * @param patterns
 *  The RE patterns to be converted to a negative lookbehind assertion. All
 *  falsy values will be filtered.
 *
 * @returns
 *  The resulting negative lookbehind assertion pattern.
 */
behind.neg = (...patterns: Pattern[]): string => {
    return "(?<!" + alt(...patterns) + ")";
};

/**
 * Creates an RE character class pattern, enclosed in brackets.
 *
 * @param ranges
 *  The characters or character ranges to be concatenated to a character class
 *  pattern. All falsy values will be filtered.
 *
 * @returns
 *  The resulting pattern containing all passed character ranges, enclosed in
 *  brackets.
 */
function classfn(...ranges: Pattern[]): string {
    return "[" + ranges.filter(Boolean).join("") + "]";
}
export { classfn as class };

/**
 * Creates a negative RE character class pattern, enclosed in brackets.
 *
 * @param ranges
 *  The characters or character ranges to be concatenated to a character class
 *  pattern. All falsy values will be filtered.
 *
 * @returns
 *  The resulting pattern excluding all passed character ranges, enclosed in
 *  brackets.
 */
classfn.neg = (...ranges: Pattern[]): string => {
    return classfn("^", ...ranges);
};
