/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { v4, validate } from "uuid";

// public functions ===========================================================

/**
 * Check whether the passed string represents a UUID (any version).
 *
 * @param text
 *  The string to be checked.
 *
 * @param [braces]
 *  If set to `true`, the passed string must start and end with curly braces.
 *
 * @returns
 *  Whether the passed string represents a UUID (any version).
 */
export function is(text: string, braces = false): boolean {
    if (braces) {
        if (!text.startsWith("{") || !text.endsWith("}")) { return false; }
        text = text.slice(1, -1);
    }
    return validate(text);
}

/**
 * Generates a random UUID version 4.
 *
 * @param [braces]
 *  If set to `true`, the UUID will be enclosed in a pair of curly braces.
 *
 * @returns
 *  A random UUID version 4.
 */
export function random(braces = false): string {
    const uuid = v4();
    return braces ? `{${uuid}}` : uuid;
}
