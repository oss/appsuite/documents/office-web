/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// constants ==================================================================

// return values of the `typeof` operator considered to be JS objects
const OBJ_TYPES = new Set(["object", "function"]);

// class names of internal JS engine errors
const SCRIPT_ERRORS = new Set(["EvalError", "InternalError", "RangeError", "ReferenceError", "SyntaxError", "TypeError", "URIError"]);

// public functions ===========================================================

/**
 * Returns whether the passed value is undefined.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is undefined.
 */
function undefinedfn(data: unknown): data is undefined {
    return data === undefined;
}
export { undefinedfn as undefined };

/**
 * Returns whether the passed value is null.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is null.
 */
function nullfn(data: unknown): data is null {
    return data === null;
}
export { nullfn as null };

/**
 * Returns whether the passed value is nullish (undefined or null).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is nullish (undefined or null).
 */
export function nullish(data: unknown): data is nullish {
    return (data === undefined) || (data === null);
}

/**
 * Returns whether the passed value is a number primitive.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a number primitive.
 */
export function number(data: unknown): data is number {
    return typeof data === "number";
}

/**
 * Returns whether the passed value is a bigint primitive.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a bigint primitive.
 */
export function bigint(data: unknown): data is bigint {
    return typeof data === "bigint";
}

/**
 * Returns whether the passed value is a boolean primitive.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a boolean primitive.
 */
export function boolean(data: unknown): data is boolean {
    return typeof data === "boolean";
}

/**
 * Returns whether the passed value is a string primitive.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a string primitive.
 */
export function string(data: unknown): data is string {
    return typeof data === "string";
}

/**
 * Returns whether the passed value is a symbol.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a symbol.
 */
export function symbol(data: unknown): data is symbol {
    return typeof data === "symbol";
}

/**
 * Returns whether the passed value is a primitive (nullish, number, bigint,
 * string, boolean, or symbol).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a primitive value.
 */
export function primitive(data: unknown): data is primitive {
    return nullish(data) || !OBJ_TYPES.has(typeof data);
}

/**
 * Returns whether the passed value is a simple JavaScript object literal. Does
 * not let pass instances of other classes than `Object`. The prototype of the
 * object must be the prototype of `Object` (as for example in `{...}` object
 * literals); or `null` (as returned from `Object.create(null)`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a simple JavaScript object literal.
 */
export function dict(data: unknown): data is Dict {
    // null is an object in JS, but must not match here
    if (!data || (typeof data !== "object")) { return false; }
    const proto = Object.getPrototypeOf(data) as unknown;
    return (proto === null) || (proto === Object.prototype);
}

/**
 * Returns whether the passed value is any JavaScript object with a prototype
 * (including all subtypes such as arrays, dates, functions, boxed, RegExp,
 * etc.).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is an object with a prototype. Returns `false` for
 *  bare objects returned from `Object.create(null)`.
 */
export function object(data: unknown): data is Dict {
    // `null` is an object in JS, but must not match here
    // `data instanceof Object` does not work in Jest tests for DOM objects
    return !!data && OBJ_TYPES.has(typeof data) && !!Object.getPrototypeOf(data);
}

/**
 * Returns whether the passed value is an array (including subclasses of class
 * `Array`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is an array.
 */
export function array(data: unknown): data is unknown[] {
    // `Array.isArray` returns false for array subclasses with broken prototype
    return data instanceof Array;
}

/**
 * Returns whether the passed value is a read-only array (including subclasses
 * of class `Array`). The only difference to the function `array` is that the
 * parameter type will be narrowed to a read-only array type.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a read-only array.
 */
export function readonlyArray(data: unknown): data is readonly unknown[] {
    // `Array.isArray` returns false for array subclasses with broken prototype
    return data instanceof Array;
}

/**
 * Returns whether the passed value is a native JavaScript array literal (i.e.
 * a direct instance of the class `Array` but no subclasses).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a native JavaScript array literal.
 */
export function nativeArray(data: unknown): data is unknown[] {
    return (data instanceof Array) && (data.constructor === Array);
}

/**
 * Returns whether the passed value is an array-like (an object with numeric
 * `length` property that is not negative) except for strings.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is an array-like object.
 */
export function arrayLike(data: unknown): data is ArrayLike<unknown> {
    if (!object(data) || string(data) || (data instanceof String)) { return false; }
    const len = data.length;
    return Number.isSafeInteger(len) && ((len === 0) || ((len as number >= 1) && (0 in data)));
}

/**
 * Returns whether the passed value is a function.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a function.
 */
function functionfn(data: unknown): data is AnyFunction {
    return typeof data === "function";
}
export { functionfn as function };

/**
 * Returns whether the passed value is considered empty. Empty values are all
 * falsy values (`undefined`, `null`, `0`, `NaN`, `0n`, `false`, empty string),
 * as well as empty dictionaries (simple JavaScript objects, e.g. `{}`), and
 * empty arrays.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is considered empty.
 */
export function empty(data: unknown): boolean {
    // all falsy values (undefined, null, false, 0, NaN, 0n, "")
    if (!data) { return true; }
    // empty arrays (including any subclass of Array)
    if (array(data)) { return data.length === 0; }
    // all other values than (empty) dictionaries cannot be empty
    if (!dict(data)) { return false; }
    // check if the dictionary is empty
    // eslint-disable-next-line no-unreachable-loop
    for (const _key in data) { return false; }
    return true;
}

/**
 * Returns whether the passed value is an internal JavaScript exception object
 * (an instance of `EvalError`, `InternalError`, `RangeError`, `ReferenceError`,
 * `SyntaxError`, `TypeError`, or `URIError`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is an internal JavaScript exception object.
 */
export function scriptError(data: unknown): data is Error {
    return (data instanceof Error) && SCRIPT_ERRORS.has(data.name);
}

/**
 * Returns whether the passed value implements the `Destroyable` interface.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value implements the `Destroyable` interface.
 */
export function destroyable(data: unknown): data is Destroyable {
    return object(data) && functionfn(data.destroy);
}

/**
 * Returns whether the passed value implements the `Disconnectable` interface.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value implements the `Disconnectable` interface.
 */
export function disconnectable(data: unknown): data is Disconnectable {
    return object(data) && functionfn(data.disconnect);
}

/**
 * Returns whether the passed value implements the `Abortable` interface.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value implements the `Abortable` interface.
 */
export function abortable(data: unknown): data is Abortable {
    return object(data) && functionfn(data.abort);
}
