/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { upsert } from "@/io.ox/office/tk/algorithms/map";

// class FnResult =============================================================

/**
 * Helper class that caches the result of a function invocation (either a
 * return value, or a thrown exception).
 *
 * @template RT
 *  The return type of the wrapped function.
 *
 * @template AT
 *  The types of the function arguments.
 *
 * @param fn
 *  The function to be invoked immediately in the constructor. The result (the
 *  return value, or the exception thrown) will be stored internally.
 *
 * @param args
 *  The arguments to be passed to the function.
 */
class FnResult<RT, AT extends unknown[]> {

    readonly #result: unknown;
    readonly #thrown: boolean;

    constructor(fn: FuncType<RT, AT>, args: AT) {
        try {
            this.#result = fn(...args);
            this.#thrown = false;
        } catch (err) {
            this.#result = err;
            this.#thrown = true;
        }
    }

    /**
     * Provides the result of the function that has been passed to, and called
     * in the constructor.
     *
     * @returns
     *  The return value of the function invocation.
     *
     * @throws
     *  The exception that has been thrown by the function.
     */
    resolve(): RT {
        if (this.#thrown) { throw this.#result; }
        return this.#result as RT;
    }
}

// public functions ===========================================================

/**
 * A function that constantly returns the passed value.
 */
function constfn<T>(value: T): FuncType<T> { return () => value; }
export { constfn as const };

/**
 * A function that constantly returns `undefined`.
 */
export function undef(): undefined { return undefined; }

/**
 * A function that constantly returns `null`.
 */
function nullfn(): null { return null; }
export { nullfn as null };

/**
 * A function that constantly returns `false`.
 */
function falsefn(): false { return false; }
export { falsefn as false };

/**
 * A function that constantly returns `true`.
 */
function truefn(): true { return true; }
export { truefn as true };

/**
 * A function that negates its boolean argument.
 */
export function not(value: boolean): boolean { return !value; }

/**
 * Returns the first argument passed to this function, and drops all others.
 */
export function identity<T>(arg: T): T { return arg; }

/**
 * Returns the second argument passed to this function, and drops all others.
 */
export function pluck2nd<T>(_1: unknown, arg: T): T { return arg; }

/**
 * Returns the third argument passed to this function, and drops all others.
 */
export function pluck3rd<T>(_1: unknown, _2: unknown, arg: T): T { return arg; }

/**
 * Returns the fourth argument passed to this function, and drops all others.
 */
export function pluck4th<T>(_1: unknown, _2: unknown, _3: unknown, arg: T): T { return arg; }

/**
 * Simulates a `do` code block. Invokes the passed function immediately, and
 * returns its result. This function is intended to be used to create a local
 * scope similar to an IIFE, but is a little bit better readable.
 *
 * @param fn
 *  The function to be invoked immediately.
 *
 * @returns
 *  The return value of the passed callback function.
 */
function dofn<T>(fn: FuncType<T>): T {
    return fn();
}
export { dofn as do };

/**
 * Simulates a `yield` code block for a custom generator. Invokes the passed
 * generator function immediately, and yields its result. This function is
 * intended to be used to create a local scope similar to an IIFE, but is a
 * little bit better readable.
 *
 * @param fn
 *  The generator function to be invoked immediately.
 *
 * @param [context]
 *  The calling context for the callback generator function (which cannot be
 *  written as arrow function).
 *
 * @yields
 *  The return value of the passed callback function.
 */
function *yieldfn<T>(fn: FuncType<Iterable<T>>, context?: object): IterableIterator<T> {
    yield* context ? fn.call(context) : fn();
}
export { yieldfn as yield };

/**
 * Simulates a `try/catch` code block that drops all exceptions. Invokes the
 * passed function immediately, and returns its result. If the function throws,
 * the specified default value will be returned instead.
 *
 * @param fn
 *  The function to be invoked immediately.
 *
 * @param [def]
 *  The default value to be returned if the function throws. If omitted, the
 *  value `undefined` will be returned for exceptions.
 *
 * @returns
 *  The return value of the passed callback function, or the default value.
 */
function tryfn<T>(fn: FuncType<T>, def: T): T;
function tryfn<T>(fn: FuncType<T>, def?: T): Opt<T>;
// implementation
function tryfn<T>(fn: FuncType<T>, def?: T): Opt<T> {
    try { return fn(); } catch { return def; }
}
export { tryfn as try };

/**
 * Returns a wrapped version of the passed function that prevents recursive
 * calls to itself. If the resulting wrapped function will be called
 * recursively, a specific default value will be returned immediately.
 *
 * @param fn
 *  The function implementation to be guarded.
 *
 * @param [def]
 *  The default value to be returned if the function is currently running.
 *
 * @returns
 *  The wrapped version of the passed function.
 */
export function guard<T, AT extends unknown[]>(fn: FuncType<T, AT>, def: T): FuncType<T, AT>;
export function guard<T, AT extends unknown[]>(fn: FuncType<T, AT>, def?: T): FuncType<Opt<T>, AT>;
// implementation
export function guard<T, AT extends unknown[]>(fn: FuncType<T, AT>, def?: T): FuncType<Opt<T>, AT> {
    let running = false;
    return (...args) => {
        if (running) { return def; }
        try {
            running = true;
            return fn(...args);
        } finally {
            running = false;
        }
    };
}

/**
 * Returns a wrapped version of the passed function. On first invocation, the
 * wrapper function will call the original function and store the result (a
 * return value or a thrown exception) in an internal cache. Subsequent
 * invocations of the function will always result in the same behaviour,
 * ignoring the passed arguments (either return the cached value, or rethrow
 * the cached exception).
 *
 * @param fn
 *  The function to be wrapped.
 *
 * @returns
 *  The wrapped version of the passed function.
 */
export function once<T, AT extends unknown[]>(fn: FuncType<T, AT>): FuncType<T, AT> {
    return memoize(fn, nullfn); // same hash key for all arguments
}

/**
 * Returns a wrapped version of the passed function. On every invocation, the
 * hash function will be called with the arguments passed to the function
 * first. If the internal cache contains a result for the hash value, it will
 * be replicated (either by returning the cached value, or by rethrowing the
 * cached exception). Otherwise, the original function will be called, and the
 * result will be stored in the cache.
 *
 * @param fn
 *  The function to be wrapped.
 *
 * @param hash
 *  The hash function. Receives the arguments passed to the wrapper function.
 *
 * @returns
 *  The wrapped version of the passed function.
 */
export function memoize<T, KT, AT extends unknown[]>(fn: FuncType<T, AT>, hash: FuncType<KT, AT>): FuncType<T, AT> {
    const results = new Map<KT, FnResult<T, AT>>();
    return (...args: AT) => upsert(results, hash(...args), () => new FnResult(fn, args)).resolve();
}
