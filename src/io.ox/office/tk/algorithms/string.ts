/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import * as is from "@/io.ox/office/tk/algorithms/is";
import * as math from "@/io.ox/office/tk/algorithms/math";

// types ======================================================================

/**
 * A list of string tokens with optional nullish values that will be ignored
 * when processing the list.
 */
export type TokenList = Array<Nullable<string>>;

// constants ==================================================================

/**
 * The Unicode character for a horizontal ellipsis.
 */
export const ELLIPSIS_CHAR = "\u2026";

/**
 * The Unicode character for a placeholder rectangle.
 */
export const RECTANGLE_CHAR = "\u25af";

// public functions ===========================================================

/**
 * Returns whether the passed string starts with a specific prefix, ignoring
 * character case.
 *
 * @param text
 *  The original text.
 *
 * @param prefix
 *  The prefix to be checked in the passed text.
 *
 * @returns
 *  Whether the passed string starts with the specified prefix.
 */
export function startsWithICC(text: string, prefix: string): boolean {
    return text.toLowerCase().startsWith(prefix.toLowerCase());
}

/**
 * Returns whether the passed string ends with a specific suffix, ignoring
 * character case.
 *
 * @param text
 *  The original text.
 *
 * @param suffix
 *  The suffix to be checked in the passed text.
 *
 * @returns
 *  Whether the passed string ends with the specified suffix.
 */
export function endsWithICC(text: string, suffix: string): boolean {
    return text.toLowerCase().endsWith(suffix.toLowerCase());
}

/**
 * Compares two strings ignoring character case.
 *
 * @param text1
 *  The first string to be compared.
 *
 * @param text2
 *  The second string to be compared.
 *
 * @returns
 *  The relation between the two value.
 */
export function compareICC(text1: string, text2: string): math.Relation {
    return math.compare(text1.toLowerCase(), text2.toLowerCase());
}

/**
 * Returns whether two strings are equal ignoring character case.
 *
 * @param text1
 *  The first string to be compared.
 *
 * @param text2
 *  The second string to be compared.
 *
 * @returns
 *  Whether the two strings are equal ignoring character case.
 */
export function equalsICC(text1: string, text2: string): boolean {
    return text1.toLowerCase() === text2.toLowerCase();
}

/**
 * Returns the passed text where a substring has been inserted.
 *
 * @param text
 *  The original text.
 *
 * @param pos
 *  The position of the substring to be inserted. If negative, the absolute
 *  number specifies the character count from the end of the original string.
 *
 * @param insert
 *  The new text to be inserted into the original text.
 *
 * @returns
 *  The resulting text with the inserted substring.
 */
export function insertSubStr(text: string, pos: number, insert: string): string {
    return text.slice(0, pos) + insert + text.slice(pos);
}

/**
 * Returns the passed text where a substring has been replaced with another
 * string.
 *
 * @param text
 *  The original text.
 *
 * @param start
 *  The start position of the substring to be replaced.
 *
 * @param end
 *  The end position of the substring to be replaced.
 *
 * @param replacement
 *  The new text to be inserted into the original text.
 *
 * @returns
 *  The resulting text with the replaced substring.
 */
export function replaceSubStr(text: string, start: number, end: number, replacement: string): string {
    return text.slice(0, start) + replacement + text.slice(end);
}

/**
 * Trims all non-printable characters (NPC) and whitespace characters at the
 * beginning and end of the passed string.
 *
 * In difference to the native method `String#trim`, this function trims all
 * ASCII NPC characters (`U+0000`-`U+001F` and `U+0080`-`U+009F`) too.
 *
 * @param text
 *  The text to be trimmed.
 *
 * @returns
 *  The trimmed text.
 */
export function trimAll(text: string): string {
    return text.replace(/^[\x00-\x1f\x80-\x9f\s]+|[\x00-\x1f\x80-\x9f\s]+$/g, "");
}

/**
 * Replaces all ASCII non-printable characters (`U+0000`-`U+001F` and
 * `U+0080`-`U+009F`) in the passed string.
 *
 * @param text
 *  The text to be cleaned from non-printable characters.
 *
 * @param [replacement=" "]
 *  The character to be inserted for any control character. Defaults to the
 *  ASCII space character (`U+0020`).
 *
 * @returns
 *  The new text without non-printable characters.
 */
export function cleanNPC(text: string, replacement = " "): string {
    return text.replace(/[\0-\x1f\x80-\x9f]/g, replacement);
}

/**
 * Trims non-printable characters and whitespace characters at the beginning
 * and end of the passed string, and replaces all embedded non-printable
 * characters (Unicode characters `U+0000`-`U+001F` and `U+0080`-`U+009F`).
 *
 * @param text
 *  The text to be trimmed and cleaned from non-printable characters.
 *
 * @param [replacement=" "]
 *  The character to be inserted for any control character. Defaults to the
 *  ASCII space character (`U+0020`).
 *
 * @returns
 *  The new trimmed text without non-printable characters.
 */
export function trimAndCleanNPC(text: string, replacement = " "): string {
    return cleanNPC(trimAll(text), replacement);
}

/**
 * Splits a single string with a list of tokens separated by whitespace into an
 * array of strings. Leading and trailing whitespace will be filtered.
 *
 * @param tokens
 *  The list of string tokens, separated by whitespace characters.
 *
 * @param [limit]
 *  The maximum number of tokens to be returned in the string array. Subsequent
 *  tokens in the passed list will be dropped.
 *
 * @returns
 *  The array of tokens.
 */
export function splitTokens(tokens: string, limit?: number): string[] {
    tokens = tokens.replace(/^\s+|\s+$/g, "");
    // empty string would split into an array with a single empty string
    return tokens ? tokens.split(/\s+/, limit) : [];
}

/**
 * Concatenates multiple tokens to a list of whitespace separated tokens.
 *
 * @param tokens
 *  The list of tokens to be concatenated. Empty parameters (empty string, or
 *  nullish values) will be ignored.
 *
 * @returns
 *  The concatenated token list.
 */
export function concatTokens(...tokens: TokenList): string {
    // use a "reduce" loop to prevent creating a temporary array when using "filter" and "join"
    return tokens.reduce<string>((token1, token2) => (token1 && token2) ? `${token1} ${token2}` : (token1 || token2 || ""), "");
}

/**
 * Joins the passed string by appending the separator to each array entry (also
 * to the last array element in difference to the native method `Array#join`).
 *
 * @example
 *  ["a", "b"].join("\n") === "a\nb"
 *  joinTail(["a", "b"], "\n") === "a\nb\n"
 *
 * @param tokens
 *  The strings to be joined.
 *
 * @param sep
 *  The separator text to be used to join the strings.
 *
 * @returns
 *  The generated string.
 */
export function joinTail(tokens: string[], sep: string): string {
    return tokens.length ? (tokens.join(sep) + sep) : "";
}

/**
 * Returns the passed text with a capitalized first character.
 *
 * @param text
 *  The text to be converted.
 *
 * @returns
 *  The passed text with a capitalized first character.
 */
export function capitalizeFirst(text: string): string {
    return (text.length > 0) ? (text[0].toUpperCase() + text.slice(1)) : "";
}

/**
 * Returns the passed text with capitalized words.
 *
 * @param text
 *  The text to be converted.
 *
 * @returns
 *  The passed text with capitalized words.
 */
export function capitalizeWords(text: string): string {
    return text.split(/(\s+)/).map(capitalizeFirst).join("");
}

/**
 * Wraps the passed literal text into special marker characters for localized
 * UI labels. These texts will not be reported as "untranslated" in I18N debug
 * mode.
 *
 * @param text
 *  The text to be marked as "translated".
 *
 * @returns
 *  The marked text.
 */
export function noI18n<T>(text: T): T {
    return is.string(text) ? (_.noI18n(text) as T) : text;
}
