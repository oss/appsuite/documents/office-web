/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";
import gt from "gettext";

import { is, math, ary } from "@/io.ox/office/tk/algorithms";
import { KeyCode, containsNode, getFocus, setFocus } from "@/io.ox/office/tk/dom";
import { getArray, setValue } from "@/io.ox/office/tk/config";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import {
    BUTTON_SELECTOR, VISIBLE_SELECTOR, SELECTED_SELECTOR,
    setToolTip, showNodes, getCaptionText,
    checkButtonNodes, checkMatchingButtonNodes,
    createButtonNode, filterButtonNodes, filterCheckedButtonNodes,
    getButtonValue, setButtonValue, getButtonOptions, setButtonOptions, setButtonKeyHandler
} from "@/io.ox/office/tk/forms";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { BaseMenu } from "@/io.ox/office/tk/popup/basemenu";

import "@/io.ox/office/tk/popup/listmenu.less";

// constants ==================================================================

// the internal identifier for the most recently used section
const MRU_SECTION_ID = "__MRU__";

// CSS selector for visible option buttons
const VISIBLE_BUTTON_SELECTOR = BUTTON_SELECTOR + VISIBLE_SELECTOR;

// private functions ==========================================================

// default callback for sorting buttons
function defaultSorter(index1, index2) {
    if (is.string(index1) && is.string(index2)) { return LOCALE_DATA.getCollator({ numeric: true }).compare(index1, index2); }
    if (is.number(index1) && is.number(index2)) { return math.compare(index1, index2); }
    globalLogger.warn("$badge{defaultSorter} incompatible/unsupported sort indexes", index1, index2);
    return 0;
}

// public functions ===========================================================

/**
 * Returns the sorting index of an option button in a list.
 *
 * @param {NodeOrJQuery} button
 *  The button element.
 *
 * @returns {unknown}
 *  The sorting index of the list button element, as specified with the option
 *  "sortIndex" at its creation. If no sorting index has been specified, the
 *  label text of the button will be returned instead.
 */
export function getButtonSortIndex(button) {
    const sortIndex = getButtonOptions(button).sortIndex;
    return is.nullish(sortIndex) ? _.noI18n.fix(getCaptionText(button)) : sortIndex;
}

/**
 * Changes the sorting index of an option button in a list.
 *
 * @param {NodeOrJQuery} button
 *  The button element.
 *
 * @param {unknown} sortIndex
 *  The new sorting index of the list button element. The values `null` or
 *  `undefined` will reset the sorting index (the button will be sorted by its
 *  label text).
 */
export function setButtonSortIndex(button, sortIndex) {
    setButtonOptions(button, { sortIndex });
}

/**
 * Sorts the passed array of button elements.
 *
 * @param {HTMLElement[]} buttons
 *  A plain JavaScript array of HTML button elements that will be sorted
 *  in-place.
 *
 * @param {CompareFn<unknown>} [sorter]
 *  If set to a comparator function, it will be invoked with two sorting
 *  indexes from the method `Array#sort`. Otherwise, the buttons will be sorted
 *  with a default algorithm (by numbers, or by case-insensitive locale-aware
 *  strings).
 *
 * @returns {HTMLElement[]}
 *  The button array passed to this function that has been sorted in-place.
 */
export function sortOptionButtons(buttons, sorter) {
    const sortFn = is.function(sorter) ? sorter : defaultSorter;
    buttons.sort((btn1, btn2) => sortFn(getButtonSortIndex(btn1), getButtonSortIndex(btn2)));
    return buttons;
}

// class ListMenuSection ======================================================

/**
 * Represents a section (a container for option buttons with optional header
 * label) in a `ListMenu` instance.
 */
export class ListMenuSection {

    constructor(id, config, options) {

        // settings for grid layout
        const gridLayout = config.listLayout === "grid";

        // the section identifier
        this.id = id;
        // the configuration of the list menu
        this.config = config;
        // root element of the section
        this.$el = $(`<div data-section="${id}">`);
        // container for option buttons (class "group" needed for basic button styles)
        this.$container = $('<div class="button-container group">');
        // filter callback for button visibility
        this.filter = options?.filter ?? config.filter;
        // sorter flag or callback
        this.sorter = options?.sorted ?? config.sorted;
        // number of columns in grid layout
        this.columns = gridLayout ? (options?.gridColumns ?? config.gridColumns) : 1;
        // single scrollable row mode
        this.scrollRow = gridLayout && !!options?.scrollRow;

        // add the header label
        if (options?.label) {
            this.$el.append($(`<div class="header-label" aria-hidden="true">`).text(options.label));
        }

        // append the button container after the header label
        this.$el.append(this.$container);

        // add ARIA role for grid layout
        if (gridLayout) {
            this.$el.attr({ role: "grid", "aria-label": config.tooltip || null });
            this.$container.css("grid-template-columns", `repeat(${this.columns}, 1fr)`);
            this.$container.toggleClass("scroll-row", this.scrollRow);
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a new option button.
     */
    addOption(value, options) {
        const $button = createButtonNode(options);
        $button.attr("role", "option").data("options", { ...options });
        setButtonValue($button, value, this.config.valueSerializer);
        setToolTip($button, options);
        this.$container.append($button);
        return $button;
    }

    /**
     * Returns the option buttons of this section.
     */
    getOptions(options) {
        const selector = options?.visible ? VISIBLE_BUTTON_SELECTOR : BUTTON_SELECTOR;
        return this.$container.children(selector);
    }

    /**
     * Returns the option buttons with the specified value.
     */
    findOptions(value, options) {
        const $buttons = this.getOptions(options);
        return filterButtonNodes($buttons, value, this.config);
    }

    /**
     * Removes all option buttons from this section.
     */
    clearOptions() {
        this.$container.empty();
        showNodes(this.$el, false);
    }

    /**
     * Refreshes visibility of all option buttons, and the section itself.
     */
    refreshOptions() {

        // update visibility of all option buttons
        if (this.filter) {
            for (const button of this.getOptions().get()) {
                const visible = this.filter(getButtonValue(button), this.id);
                if (is.boolean(visible)) { showNodes(button, visible); }
            }
        }

        // hide sections without visible option buttons
        const visible = this.getOptions({ visible: true }).length > 0;
        showNodes(this.$el, visible);

    }

    /**
     * Sorts the option buttons in this section.
     */
    sortOptions() {
        if (this.sorter) {
            const buttons = this.getOptions().get();
            this.$container.append(sortOptionButtons(buttons, this.sorter));
        }
    }

    /**
     * Returns settings for grid layout according to the visible option buttons
     * in this section.
     */
    getGridSettings() {
        const container = this.$container[0];
        const $buttons = this.getOptions({ visible: true });
        const count = $buttons.length;
        const scroll = this.scrollRow;
        const cols = scroll ? count : this.columns;
        const rows = scroll ? 1 : Math.ceil(count / cols);
        const first = scroll ? Math.ceil(container.scrollLeft / container.scrollWidth * count) : 0;
        return { $buttons, count, scroll, cols, rows, first };
    }

    /**
     * Scrolls the button container to the specified option button.
     */
    scrollToOption(index) {
        const container = this.$container[0];
        const settings = this.getGridSettings();
        const btnWidth = settings.$buttons[0].offsetWidth;
        container.scrollLeft = index * btnWidth;
    }
}

// class ListMenu =============================================================

/**
 * Wraps a popup menu element, shown on top of the application window, and
 * relative to an arbitrary DOM node. The menu will contain a set of option
 * button controls that can be stacked vertically, or can be arranged in a
 * tabular layout. The option buttons can be highlighted. The buttons can be
 * divided into vertically stacked section nodes.
 *
 * @param {object} [config]
 *  Configuration options for the list menu. Supports all options supported by
 *  the base class `BaseMenu`.
 *
 *  Additionally, the following options are supported:
 *  - {Function} [config.matcher=_.isEqual]
 *    A comparison predicate function that returns whether an option button
 *    should be selected, deselected, or kept unmodified according to a
 *    specific value. Receives an arbitrary value as first parameter, and the
 *    value of an existing option button as second parameter. The function must
 *    return `true` to select the respective option button, `false` to deselect
 *    the option button, or any other value to leave the option button
 *    unmodified. If omitted, uses `_.isEqual` which compares arrays and
 *    objects deeply, and does not skip any option button.
 *  - {boolean} [options.multiSelect=true]
 *    If set to `false`, only the first matching element will be selected.
 *  - {string} [config.listLayout="list"]
 *    The layout mode of the option buttons in the list:
 *    - "list": All option buttons will be stacked vertically, and will occupy
 *      the complete width of the popup menu. Highlighted options will be drawn
 *      with a changed background color.
 *    - "grid": All option buttons will be arranged in a grid layout per menu
 *      section. Highlighted options will be drawn with a thick border, leaving
 *      the background untouched.
 *  - {(value: unknown, section: string) => boolean|null} [options.filter]
 *    A predicate function that specifies whether an option button is visible.
 *    Will be invoked every time this list will be refreshed. It receives the
 *    value of an option button, and the identifier of its section. Must return
 *    a boolean flag whether the option button will be visible, or `null` to
 *    keep the visibility of the button untouched.
 *  - {boolean|CompareFn<unknown>} [config.sorted=false]
 *    If set to `true` or a callback function, the option buttons will be
 *    sorted according to the sort order that has been specified via the
 *    "sortIndex" options of each inserted option button (see method
 *    `ListMenu#addOption` for details). The value `true` will cause to sort
 *    the elements by their natural order (strings with case-insensitive
 *    locale-aware natural sorting). If a callback function has been passed
 *    instead, it will be used as comparator callback function for the method
 *    `Array#sort`. This option can be overridden for single menu sections by
 *    passing this option to the method `ListMenu#addSection`.
 *  - {number} [options.gridColumns=10]
 *    The default number of columns in each menu section in "grid" layout mode
 *    (see option "listLayout" above). This value can be overridden for single
 *    menu sections by passing this option to the method `ListMenu#addSection`.
 *  - {number} [options.mruSize=0]
 *    The maximum number of entries shown in the most recently used list. If
 *    the list size is exceeded, the oldest MRU option will be removed.
 *  - {string} [options.mruSettingsKey]
 *    If specified, the contents of the most recently used list will be stored
 *    into the user settings under that key.
 *  - {ControlValueSerializerFn} [options.valueSerializer]
 *    A callback function that converts the control value to a string that will
 *    be used in "data-value" and "data-state" element attributes for automated
 *    testing.
 */
export class ListMenu extends BaseMenu {

    constructor(config) {

        // base constructor
        super({
            scrollPadding: 5,
            preferFocusFilter: SELECTED_SELECTOR,
            matcher: _.isEqual,
            multiSelect: true,
            listLayout: "list",
            sorted: false,
            gridColumns: 10,
            mruSize: 0,
            ...config
        });

        // registry for menu sections, mapped by section identifiers
        this._sections = new Map/*<string, ListMenuSection>*/();
        // the current section to be filled with options
        this._section = null;
        // sections to be sorted debounced
        this._pendingSortSections = new Set/*<ListMenuSection>*/();

        // comparator for option values
        this._matcher = config?.matcher ?? _.isEqual;
        // the key to store the MRU list in the user settings
        this._mruSettingsKey = config?.mruSettingsKey ? `mru/${config.mruSettingsKey}` : null;
        // the current list of most recently used options
        this._mruList = null;
        // the option button currently focused (restore focus after repaint)
        this._focusEl = null;

        // add CSS marker for option button styling
        this.$el.addClass("list-menu borderless-buttons").attr({ "data-layout": this.config.listLayout, role: "listbox" });

        // register event handlers
        this.listenTo(this.$el, "keydown", event => {
            // the option button to be focused
            const button = this.getOptionForKey(event)[0];
            // stop event propagation for all supported keys
            if (button) {
                setFocus(button);
                this.scrollToChildNode(button);
                return false;
            }
        });

        // convert ENTER, SPACE, and TAB keys in the menu to click events
        setButtonKeyHandler(this.$el, { tab: true });

        // initialize the MRU list
        if (this.config.mruSize) {
            this.addSection(MRU_SECTION_ID, { label: gt("Recently used"), sorted: false });
            this.config.multiSelect = false;
            this._mruList = this._mruSettingsKey ? getArray(this._mruSettingsKey).slice(0, this.config.mruSize) : [];
            this.on("menu:afterrepaint", this._repaintMRUList);
        }

        // switch button containers in "scroll single row" mode to zero width before
        // positioning the popup menu to exclude them from max-width calculation
        this.on("popup:beforelayout", () => {
            this.$el.find(".button-container.scroll-row").css("width", 1); // Safari does not like 0px!
        });

        // force button containers in "scroll single row" mode to fixed width
        this.on("popup:layout", () => {
            this.$el.find(".button-container.scroll-row").css("width", this.$el.width());
        });

        // restore focused option button after repaint
        this.on("menu:beforerepaint", this._saveFocusButton);
        this.on("menu:afterrepaint", this._restoreFocusButton);
    }

    // public methods ---------------------------------------------------------

    /**
     * Invokes the passed callback function which wants to modify the contents
     * of this dropdown list. The previously focused option button will be
     * focused again afterwards.
     *
     * @param {Function} fn
     *  The callback function that will be invoked while remembering the option
     *  button currently focused.
     */
    guardFocusedOption(fn, context) {
        this._saveFocusButton();
        fn.call(context);
        this._restoreFocusButton();
    }

    /**
     * Adds a new section to this popup menu, if it does not exist yet.
     *
     * @param {string} id
     *  The unique identifier of the menu section.
     *
     * @param {object|string} [options]
     *  Optional parameters. Can be set to a simple string to specify the title
     *  of the section (convenience shortcut for the option "label").
     *  - {string} [options.label]
     *    If specified, a heading label will be created for the section.
     *  - {boolean|CompareFn<unknown>} [options.sorted=false]
     *    Whether the option buttons will be sorted according to the sort order
     *    that has been specified via the "sortIndex" options of each inserted
     *    option button (see method `ListMenu#addOption`). If omitted, the
     *    default value for this option as specified in the constructor options
     *    will be used.
     *  - {number} [options.gridColumns]
     *    The number of columns in the menu section, if this menu is in "grid"
     *    layout mode (see option "listLayout" passed to the constructor). If
     *    omitted, the default value for this option as specified in the
     *    constructor options will be used.
     *  - {boolean} [options.scrollRow=false]
     *    If set to `true`, all option buttons will be shown in a single,
     *    horizontally scrollable row. Used only, if this menu is in "grid"
     *    layout mode (see option "listLayout" passed to the constructor). The
     *    option "gridColumns" will be ignored.
     */
    addSection(id, options) {
        // try to find an existing section node, or create a new section
        // (use the selected section as default target for new option buttons)
        this._section = this._getOrCreateSection(id, is.string(options) ? { label: options } : options);
    }

    /**
     * Returns the specified section container in this popup menu.
     *
     * @param {string} id
     *  The unique identifier of the menu section.
     *
     * @returns {Opt<ListMenuSection>}
     *  The specified section container if available.
     */
    getSection(id) {
        return this._sections.get(id);
    }

    /**
     * Adds a new option button to this list. If the option buttons will be
     * sorted (see the sort options passed to the constructor), the option
     * button will be inserted according to these settings.
     *
     * @param {unknown} value
     *  The unique value associated to the option button. MUST NOT be `null`.
     *
     * @param {object} [options]
     *  Optional parameters. Supports all formatting options for button
     *  elements supported by the function `createButtonNode()`.
     *
     *  Additionally, the following options are supported:
     *  - {string} [options.section]
     *    The unique identifier of the menu section the new option button will
     *    be inserted into. If a section with this identifier does not exist
     *    yet, it will be created without a header label. If omitted, the
     *    option button will be inserted into the default section (the last
     *    call of `ListMenu#addSection`). If no section exists at all, a new
     *    section with the identifier "default" will be created and used.
     *  - {unknown} [options.sortIndex]
     *    Sorting index for the new option button. All option buttons will be
     *    sorted relatively to their index. Sorting will be stable, multiple
     *    option buttons with the same index will remain in insertion order.
     *
     * @returns {JQuery}
     *  The new option button, as JQuery object. The options passed to this
     *  method have been stored as JQuery data property "options".
     */
    addOption(value, options) {

        // get or create section container
        const id = options?.section ?? this._section?.id ?? "default";
        const sectionOptions = options && options.gridColumns ? { gridColumns: options.gridColumns } : undefined;
        const section = this._getOrCreateSection(id, sectionOptions);

        // create the option button, notify listeners
        const $button = section.addOption(value, options);
        this.trigger("list:option:add", $button, value, id);

        // sort the button nodes in the section debounced
        if (section.sorter) {
            this._pendingSortSections.add(section);
            this._sortAllSections();
        }

        this.refresh();
        return $button;
    }

    /**
     * Removes all option buttons with a specific value from this list.
     *
     * @param {unknown} value
     *  The value associated to the option button. MUST NOT be `null`.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The unique identifier of a menu section to delete the option button
     *    from. If omitted, all option buttons with the passed value will be
     *    deleted.
     */
    deleteOption(value, options) {

        // delete option button from one or all sections
        this._collectOptions(options, section => {
            const $buttons = section.findOptions(value, options);
            $buttons.get().forEach(button => this.trigger("list:option:delete", $(button), value, section.id));
            $buttons.remove();
        });

        // refresh popup element layout
        this.refresh();
    }

    /**
     * Removes all option buttons from this list.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The unique identifier of a menu section to delete the option button
     *    from. If omitted, all sections will be cleared.
     */
    clearOptions(options) {
        this._forEachSection(options, section => section.clearOptions());
        this.refresh();
    }

    /**
     * Returns all option buttons of this list.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The unique identifier of a menu section. If omitted, all option
     *    buttons from all sections will be returned.
     *  - {boolean} [options.visible]
     *    If set to `true`, only visible option buttons will be returned.
     *
     * @returns {JQuery}
     *  The option buttons in this list, as JQuery collection.
     */
    getOptions(options) {
        return this._collectOptions(options, section => section.getOptions(options));
    }

    /**
     * Returns the option buttons with the specified value.
     *
     * @param {unknown} value
     *  The value of an option button.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The unique identifier of a menu section to search in. If omitted, all
     *    existing sections will be searched for the option buttons.
     *  - {boolean} [options.visible]
     *    If set to `true`, only visible option buttons will be returned.
     *
     * @returns {JQuery}
     *  The option buttons with the specified value, as JQuery collection.
     */
    findOptions(value, options) {
        return this._collectOptions(options, section => section.findOptions(value, options));
    }

    /**
     * Returns all selected option buttons in this list.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {string} [options.section]
     *    The unique identifier of a menu section. If omitted, all existing
     *    sections will be searched for selected option buttons.
     *  - {boolean} [options.visible]
     *    If set to `true`, only visible option buttons will be returned.
     *
     * @returns {JQuery}
     *  The selected option buttons, as JQuery collection.
     */
    getSelectedOptions(options) {
        return filterCheckedButtonNodes(this.getOptions(options));
    }

    /**
     * Returns the option button to be focused or selected after moving the
     * cursor according to the passed keyboard event.
     *
     * @param {AnyKeyEvent} event
     *  The event object caught for a "keydown" event.
     *
     * @param {NodeOrJQuery} [button]
     *  The option button the cursor movement originates from. If omitted, the
     *  button element targeted by the keyboard event will be used. If no
     *  button is available, the first or last option button will be selected
     *  instead according to the key code. All keys in "down" orientation
     *  (CURSOR_DOWN, PAGE_DOWN) and the HOME key will select the first option
     *  button, all keys in "up" orientation (CURSOR_UP, PAGE_UP) and the END
     *  key will select the last option button.
     *
     * @returns {JQuery}
     *  The option button to be focused or selected. If the passed key event
     *  does not describe any key supported for cursor movement, or the passed
     *  button node is invalid, an empty JQuery collection will be returned
     *  instead.
     */
    getOptionForKey(event, button) {

        // current button to start from
        const $button = button ? $(button) : $(event.target).closest("a.btn");
        // all visible button nodes
        const $allButtons = this.getOptions({ visible: true });

        // returns the number of option buttons currently visible
        const getListPageSize = () => {
            const height = $allButtons.first().outerHeight();
            return (height > 0) ? Math.max(1, Math.floor(this.el.clientHeight / height) - 1) : 1;
        };

        // returns the first or last option button according to the passed distance
        const getInitialButton = diff => (diff > 0) ? $allButtons.first() : $allButtons.last();

        // returns the previous or next option button according to the passed distance
        const getNextButton = (diff, wrap) => {

            // no button selected: start with first/last button
            if (!$button.length) { return getInitialButton(diff); }

            // index of the focused button
            let index = $allButtons.index($button);

            if (index < 0) {
                index = (diff < 0) ? ($allButtons.length - 1) : 0;
            } else if (wrap) {
                index += (diff % $allButtons.length) + $allButtons.length;
                index %= $allButtons.length;
            } else {
                index = math.clamp(index + diff, 0, $allButtons.length - 1);
            }
            return $allButtons.eq(index);
        };

        // returns the grid settings of the passed section element
        const getSectionGridSettings = $section => {
            return this.getSection($section.attr("data-section")).getGridSettings();
        };

        // returns the option button at a specific grid position
        const getSectionGridButton = (settings, col, row) => {
            return settings.$buttons.eq(Math.min(settings.count - 1, row * settings.cols + col));
        };

        // returns the option button in the previous or next row in "grid" layout
        const getButtonInNextRow = diff => {

            // no button selected: start with first/last button
            if (!$button.length) { return getInitialButton(diff); }

            // all option button of the section containing the current button
            let $section = $button.closest("[data-section]");
            let settings = getSectionGridSettings($section);

            // column and row index in the section table
            const index = settings.$buttons.index($button);
            let col = index % settings.cols;
            let row = Math.floor(index / settings.cols);
            const ratio = settings.scroll ? 0 : (col / settings.cols);

            // move inside the table (snap to last button in last row)
            row += diff;
            if ((row >= 0) && (row < settings.rows)) {
                return getSectionGridButton(settings, col, row);
            }

            // jump to preceding/following section
            $section = (diff < 0) ? $section.prev("[data-section]") : $section.next("[data-section]");
            if (!$section.length) { return $(); }
            settings = getSectionGridSettings($section);
            col = settings.scroll ? settings.first : Math.floor(ratio * settings.cols);
            row = (diff > 0) ? 0 : (settings.rows - 1);
            return getSectionGridButton(settings, col, row);
        };

        // returns the target option button for "list" layout
        const getButtonForList = () => {
            switch (event.keyCode) {
                case KeyCode.UP_ARROW:   return getNextButton(-1, true);
                case KeyCode.DOWN_ARROW: return getNextButton(1, true);
                case KeyCode.PAGE_UP:    return getNextButton(-getListPageSize());
                case KeyCode.PAGE_DOWN:  return getNextButton(getListPageSize());
            }
            return $();
        };

        // returns the target option button for "grid" layout
        const getButtonForGrid = () => {
            switch (event.keyCode) {
                case KeyCode.LEFT_ARROW:  return getNextButton(-1, true);
                case KeyCode.RIGHT_ARROW: return getNextButton(1, true);
                case KeyCode.UP_ARROW:    return getButtonInNextRow(-1);
                case KeyCode.DOWN_ARROW:  return getButtonInNextRow(1);
            }
            return $();
        };

        // generic key shortcuts
        switch (event.keyCode) {
            case KeyCode.HOME: return $allButtons.first();
            case KeyCode.END:  return $allButtons.last();
        }

        // special shortcuts dependent on layout mode
        switch (this.config.listLayout) {
            case "list": return getButtonForList();
            case "grid": return getButtonForGrid();
        }

        return $();
    }

    /**
     * Selects the option buttons with the passed value, and deselects all
     * other option buttons.
     *
     * @param {unknown} value
     *  The value of the option buttons to be selected. All option button will
     *  be selected or deselected according to the matcher callback function
     *  passed to the constructor (option "matcher").
     *
     * @returns {JQuery}
     *  The selected option buttons, as JQuery collection.
     */
    selectOption(value) {
        checkMatchingButtonNodes(this.getOptions(), value, { matcher: this.config.matcher, multiSelect: this.config.multiSelect });
        return this.getSelectedOptions();
    }

    /**
     * Selects the specified option button, and deselects all other option
     * buttons.
     *
     * @param {number} index
     *  The zero-based index of the option button to be selected.
     *
     * @param {ListSelectOptionOptions} [options]
     *  Optional parameters:
     *  - {boolean} [options.scroll=false]
     *    If set to `true`, the menu will scroll to the selected option button
     *    automatically.
     *
     * @returns {JQuery}
     *  The selected option button. If the passed index is invalid, an empty
     *  JQuery collection will be returned.
     */
    selectOptionAt(index, options) {
        return this._selectOption(this.getOptions()[index], options);
    }

    /**
     * Selects the option button after moving the cursor according to the
     * passed keyboard event.
     *
     * @param {AnyKeyEvent} event
     *  The event object caught for a "keydown" event.
     *
     * @param {NodeOrJQuery} [button]
     *  The option button the cursor movement originates from. If omitted, the
     *  button element targeted by the keyboard event will be used.  If no
     *  button is available, the first or last option button will be selected
     *  instead according to the key code. All keys in "down" orientation
     *  (CURSOR_DOWN, PAGE_DOWN) and the HOME key will select the first option
     *  button, all keys in "up" orientation (CURSOR_UP, PAGE_UP) and the END
     *  key will select the last option button.
     *
     * @returns {boolean}
     *  Whether the event has been processed, and an option button has been
     *  selected.
     */
    selectOptionForKey(event, button) {
        button = this.getOptionForKey(event, button)[0];
        if (!button) { return false; }
        this._selectOption(button, { scroll: true });
        return true;
    }

    /**
     * Returns the contents of the MRU list.
     *
     * @returns {unknown[]|null}
     *  The contents of the MRU list, as plain array; or `null`, if the MRU
     *  list is not enabled for this list popup node. The last element of the
     *  array is the option that has been added most recently to the list.
     */
    getMRUList() {
        return this._mruList?.slice() ?? null;
    }

    /**
     * Registers the specified value as most recently used, and updates the MRU
     * section of this list popup node.
     *
     * @param {unknown} value
     *  The value to be registered as most recently used.
     */
    setMRUValue(value) {

        // nothing to do without MRU list support
        if (!this._mruList) { return; }

        // update the MRU list (remove existing option, new option to the end)
        ary.deleteFirst(this._mruList, value);
        if (this._mruList.length === this.config.mruSize) { this._mruList.shift(); }
        this._mruList.push(value);

        // save MRU list to user configuration
        if (this._mruSettingsKey) {
            setValue(this._mruSettingsKey, this._mruList);
        }

        // repaint the entire MRU list
        this._repaintMRUList();
    }

    // protected methods ------------------------------------------------------

    /**
     * Updates the visibility of the option buttons.
     */
    /*protected override*/ implPrepareLayout() {
        // update visibility of the option buttons
        this._sections.forEach(section => section.refreshOptions());
        // repaint MRU list depending on numner of *visible* option buttons
        this._repaintMRUList();
    }

    // private methods --------------------------------------------------------

    /**
     * Remembers the focused element. Useful while manipulating/repainting the
     * menu to be able to restore the focus for a specific option button.
     */
    /*private*/ _saveFocusButton() {
        this._focusEl = this.hasFocus() ? getFocus() : null;
    }

    /**
     * Restores the focused option button after manipulating/repainting the
     * menu.
     */
    /*private*/ _restoreFocusButton() {
        if (this._focusEl && (this._focusEl !== getFocus()) && $(this._focusEl).is("a.btn")) {
            // if the focused button is not part of the menu anymore, resolve new option button by value
            if (!containsNode(this.el, this._focusEl)) {
                this._focusEl = this.findOptions(getButtonValue(this._focusEl))[0];
            }
            if (this._focusEl) {
                setFocus(this._focusEl);
            }
        }
        this._focusEl = null;
    }

    /**
     * Creates a section node.
     */
    /*private*/ _getOrCreateSection(id, options) {

        // try to find an existing section node
        let section = this.getSection(id);
        if (section) { return section; }

        // create and insert the section
        section = new ListMenuSection(id, this.config, options);
        this._sections.set(id, section);
        this.$body.append(section.$el);

        // notify listeners
        this.trigger("list:section:add", section);
        return section;
    }

    /**
     * Iterates all sections, or a specific section.
     */
    /*private*/ _forEachSection(options, fn) {
        if (options?.section) {
            const section = this.getSection(options.section);
            if (section) { fn(section); }
        } else {
            this._sections.forEach(fn);
        }
    }

    /**
     * Sorts all registered sections.
     */
    @debounceMethod({ delay: "microtask" })
    /*private*/ _sortAllSections() {

        // save the focused option button
        this._saveFocusButton();

        // process all registered section nodes
        this._pendingSortSections.forEach(section => section.sortOptions());
        this._pendingSortSections.clear();

        // restore the focused option button (may have been detached/attached)
        this._restoreFocusButton();
    }

    /**
     * Collects all option buttons returned from a callback function for one or
     * all sections.
     */
    /*private*/ _collectOptions(options, fn) {
        let $buttons = $();
        this._forEachSection(options, section => {
            $buttons = $buttons.add(fn(section));
        });
        return $buttons;
    }

    /**
     * Selects the specified option buttons, and deselects all other option
     * buttons.
     *
     * @param {NodeOrJQuery} button
     *  The option buttons to be selected, as plain DOM node, or as JQuery
     *  collection that may contain multiple elements.
     *
     * @param {ListSelectOptionOptions} [options]
     *  Optional parameters:
     *  - {boolean} [options.scroll=false]
     *    If set to `true`, the menu will scroll to the first selected option
     *    button automatically.
     *
     * @returns {JQuery}
     *  The selected option buttons, as JQuery collection.
     */
    /*private*/ _selectOption(button, options) {
        const $button = $(button);
        checkButtonNodes(this.getOptions(), false);
        checkButtonNodes($button, true);
        if (options?.scroll && $button.length) {
            this.scrollToChildNode($button[0]);
        }
        return $button;
    }

    /**
     * Repaints all entries of the MRU list.
     */
    /*private*/ _repaintMRUList() {
        const section = this.getSection(MRU_SECTION_ID);
        if (section && this._mruList) {
            section.clearOptions();
            if (this.config.mruSize < this.getOptions({ visible: true }).length - 2) {
                // last option in the MRU list is the most recently used
                ary.forEach(this._mruList, value => {
                    const options = getButtonOptions(this.findOptions(value));
                    section.addOption(value, { ...options });
                }, { reverse: true });
            }
        }
    }
}
