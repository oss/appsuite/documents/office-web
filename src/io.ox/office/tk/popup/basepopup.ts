/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { type Size, is, math, ary, dict, debug } from "@/io.ox/office/tk/algorithms";
import type { RectangleLike, ElementPosition, ElementWrapper, ElementBaseOptions, RefreshOptions, CSSBorderPos, CSSStyleSpec, NodeOrJQuery, FocusableElement } from "@/io.ox/office/tk/dom";
import {
    SMALL_DEVICE, IOS_SAFARI_DEVICE, Rectangle,
    getDocumentSize, getCeilNodeSize, getElementPosition, isVerticalPosition,
    setElementClasses, setElementStyles, isTextInputElement, isNodeSource, toNode, createDiv, getFocus
} from "@/io.ox/office/tk/dom";
import { Logger } from "@/io.ox/office/tk/utils/logger";
import { scrollToChildNode, getSoftKeyboardHeight, isSoftKeyboardOpen } from "@/io.ox/office/tk/utils";
import { type DObjectConfig, EObject, guardMethod, debounceMethod } from "@/io.ox/office/tk/objects";

import "@/io.ox/office/tk/popup/basepopup.less";

// types ======================================================================

/**
 * The specification of a rectangle in the document page. Can be one of the
 * following types:
 *
 * - `Partial<RectangleLike>`: A constant rectangle (with "left" and "top"
 *   properties). All properties are optional and default to zero.
 *
 * - `DOMRect`: A constant DOM rectangle (with "x" and "y" properties).
 *
 * - `NodeOrJQuery`: The reference to a DOM element, or a JQuery collection
 *   wrapping a DOM element.
 *
 * - nullish: There is no rectangle available.
 */
export type PopupAnchorType = Partial<RectangleLike> | DOMRect | NodeOrJQuery | nullish;

/**
 * The specification of a rectangle in the document page, either as constant
 * value of type `PopupAnchorType`, or as callback function that will be
 * invoked as needed.
 */
export type PopupAnchorSpec = PopupAnchorType | FuncType<PopupAnchorType>;

/**
 * All configuration options for an instance of `BasePopup`.
 */
export interface BasePopupConfig extends DObjectConfig, ElementBaseOptions {

    /**
     * Specifies how to display the popup element on screen.
     *
     * - `normal`: On top of all other elements. Clicks somewhere outside the
     *   popup element will be handled normally by the application.
     *
     * - `blocking`: A blocker element catching mouse clicks will be placed
     *   over the entire browser window. Clicking somewhere outside the popup
     *   element will simply hide it, but will not be processed otherwise.
     *
     * - `embedded`: The popup element will be embedded into the core container
     *   element "#io-ox-core", i.e. it will be covered by other normal popup
     *   elements as well as modal dialogs.
     *
     * Default value is "normal".
     */
    displayType?: "normal" | "blocking" | "embedded";

    /**
     * The specification of a rectangle in the document page used to restrict
     * the position and size of the popup node.
     *
     * If set to a scrollable DOM element, uses its visible client area. The
     * position of the bounding box will be updated dynamically while
     * displaying the popup node, in case the position and/or size of the DOM
     * element changes, e.g. due to scrolling or zooming.
     *
     * Default value is `null` (document page will be used as bounding box).
     */
    boundingBox?: PopupAnchorSpec;

    /**
     * The minimum distance between the popup node and the borders of the
     * bounding box (see option "boundingBox" above), in pixels. Default value
     * is `6`.
     */
    boundingPadding?: number;

    /**
     * The specification of an anchor rectangle used to calculate the position
     * and size of the popup node. The popup node will always be placed next to
     * the anchor (see following options for specifying the preferred behavior
     * when calculating the position and size of the popup node).
     *
     * In case of DOM elements, the outer size of the anchor node is used
     * instead of the inner client area.
     *
     * If no anchor has been specified, the popup node will keep its position
     * and will be expanded to the size of its contents, but it will always be
     * kept inside the bounding box.
     *
     * Default value is `null`.
     */
    anchor?: PopupAnchorSpec;

    /**
     * The specification of a rectangle used to decide whether the anchor
     * rectangle (see option "anchor" above) is still visible. If the current
     * anchor leaves the anchor box rectangle, the popup node will be closed
     * automatically.
     *
     * Default value is `null`. In this case, a constant anchor rectangle is
     * considered to be always visible. DOM anchor nodes are checked for their
     * own visibility.
     */
    anchorBox?: PopupAnchorSpec;

    /**
     * The distance between popup node and anchor rectangle (see option
     * "anchor" above), in pixels. Default value is `1`.
     */
    anchorPadding?: number;

    /**
     * The preferred positions of the popup node relative to the anchor. The
     * implementation picks the best position according to the available space
     * around the anchor. If more than one position can be used (without
     * needing to shrink the popup node), the first matching position token in
     * this list will be used.
     *
     * Default value is `["bottom", "top"]`.
     */
    anchorBorder?: OrArray<CSSBorderPos>;

    /**
     * The preferred alignment of the popup node relative to the anchor.
     *
     * - The value "leading" tries to align the left borders (if shown above or
     *   below) respectively the top borders (if shown left or right) of the
     *   popup node and the anchor position.
     *
     * - The value "trailing" tries to align the right/bottom borders
     *   respectively.
     *
     * - The value "center" tries to place the popup node centered to its
     *   anchor.
     *
     * Default value is "leading".
     */
    anchorAlign?: "leading" | "trailing" | "center";

    /**
     * The specification of a rectangle used to calculate the position of the
     * popup node according to the alignment mode (see option "anchorAlign"
     * above).
     *
     * Example: If the popup node will be shown below the anchor rectangle
     * (option "anchorBorder" has been set to "bottom"), the vertical position
     * of the popup node will be set below the anchor rectangle, and the
     * horizontal position will be calculated according to the option
     * "anchorAlign" and the rectangle calculated from this option.
     *
     * Default value is `null`. In this case, the anchor position will be used
     * as alignment box.
     */
    anchorAlignBox?: PopupAnchorSpec;

    /**
     * If set to `true`, the anchor can be covered by the popup node if there
     * is not enough space around the anchor. If set to `false`, the popup node
     * will be shrunken in order to fit it next to the anchor without covering
     * the anchor rectangle.
     *
     * Default value is `true` on small displays, otherwise `false`.
     */
    coverAnchor?: boolean;

    /**
     * If set to `true`, the popup node will not be positioned and sized
     * automatically as the position or size of the anchor changes, but retains
     * its initial position when it has been shown. Default value is `false`.
     */
    detachedAnchor?: boolean;

    /**
     * If set to `true`, the minimum width or height of popup node (according
     * to the effective anchor border) will be set to the respective size of
     * the anchor rectangle, so that the popup node is always at least as large
     * as the anchor. Default value is `false`.
     */
    stretchToAnchor?: boolean;

    /**
     * Minimum distance between the borders of the visible area and the child
     * node used by the method `BasePopup::scrollToChildNode`. Default value is
     * `0`.
     */
    scrollPadding?: number;

    /**
     * If set to `true`, the text contents in the popup node will be selectable
     * via standard browser selection mechanisms. Default value is `false`.
     */
    textSelect?: boolean;

    /**
     * The ARIA role attribute that will be set at the content DOM node.
     */
    role?: string;
}

/**
 * Optional result object for the method `BasePopup::implPrepareLayout`.
 */
export interface PopupPrepareLayoutResult {

    /**
     * A temporary override for the constructor option "anchorBorder".
     */
    anchorBorder?: BasePopupConfig["anchorBorder"];

    /**
     * A temporary override for the constructor option "anchorAlign".
     */
    anchorAlign?: BasePopupConfig["anchorAlign"];
}

// private types --------------------------------------------------------------

interface BoundRectangle extends ElementPosition {
    dynamic?: boolean;
}

interface AnchorSettings {
    /** The effective bounding rectangle to restrict the popup element into. */
    boundRect: BoundRectangle;
    /** The effective anchor rectangle to attach the popup element to. */
    anchorRect: BoundRectangle;
    /** Whether any positioning specification in the configuration is dynamic. */
    dynamic: boolean;
}

/**
 * Optional parameters for `BasePopup::#tryRefreshPosition`.
 */
interface RefreshPositionOptions {

    /**
     * Precalculated anchor settings, as returned from the private method
     * `BasePopup::#resolveAnchorSettings`. If omitted, that method will be
     * invoked.
     */
    anchorSettings?: AnchorSettings;

    /**
     * If set to `true`, the current position of the anchor will not be
     * checked, and the position and size of this popup node will always be
     * refreshed. Default value is `false`.
     */
    forceLayout?: boolean;

    /**
     * If set to `true`, the popup node will be hidden automatically when the
     * anchor node becomes invisible (may happen either directly, e.g. by
     * removing or hiding the anchor node; or indirectly, e.g. when hiding any
     * parent nodes of the anchor, or when scrolling the anchor outside the
     * visible area). Default value is `false`.
     */
    autoHide?: boolean;
}

/**
 * Type mapping for the events emitted by `BasePopup` instances.
 */
export interface BasePopupEventMap {

    /**
     * Will be emitted before the popup node will be shown. Receives an `abort`
     * callback function. If invoked by any event handler, the popup node will
     * not be shown.
     */
    "popup:beforeshow": [abort: VoidFunction];

    /**
     * Will be emitted after the popup node has been shown.
     */
    "popup:show": [];

    /**
     * Will be emitted before the popup node will be hidden.
     */
    "popup:beforehide": [];

    /**
     * Will be emitted after the popup node has been hidden.
     */
    "popup:hide": [];

    /**
     * Will be emitted before the contents of the popup node, the absolute
     * position of the anchor node, or the browser window size has changed, and
     * the position and size of the popup node needs to be adjusted.
     */
    "popup:beforelayout": [];

    /**
     * Will be emitted after the contents of the popup node, the absolute
     * position of the anchor node or the browser window size has changed, and
     * the position and size of the popup node has been adjusted (in
     * auto-layout mode).
     */
    "popup:layout": [];

    /**
     * Will be emitted after the popup node has switched to busy mode (see
     * method `BasePopup::busy` for details).
     */
    "popup:busy": [];

    /**
     * Will be emitted after the popup node has left the busy mode (see method
     * `BasePopup::idle` for details).
     */
    "popup:idle": [];
}

// globals ====================================================================

// specific logger for popup menus
export const popupLogger = new Logger("office:log-popup", { tag: "POPUP", tagColor: 0x00FFFF });

// private functions ==========================================================

/**
 * Returns a new rectangle with the additional properties "right" and "bottom",
 * containing the distances of that rectangle to the respective borders of the
 * document page.
 *
 * @param rectangle
 *  The rectangle to be extended with "right" and "bottom" properties.
 *
 * @returns
 *  A clone of the passed rectangle, with the additional properties "right" and
 *  "bottom".
 */
function addTrailingDistances(rectangle: Partial<RectangleLike>): BoundRectangle {
    const docSize = getDocumentSize();
    const { left = 0, top = 0, width = 0, height = 0 } = rectangle;
    const right = docSize.width - width - left;
    const bottom = docSize.height - height - top;
    return { left, top, right, bottom, width, height };
}

function equalRects(rect1: Opt<RectangleLike>, rect2: RectangleLike): boolean {
    return !!rect1 && (rect1.left === rect2.left) && (rect1.top === rect2.top) && (rect1.width === rect2.width) && (rect1.height === rect2.height);
}

// class BasePopup ============================================================

/**
 * Wrapper class for a DOM node used as popup window, shown on top of the
 * application window, and relative to an arbitrary DOM node.
 */
export class BasePopup<ConfT extends BasePopupConfig = BasePopupConfig, EvtMapT extends BasePopupEventMap = BasePopupEventMap> extends EObject<EvtMapT> implements ElementWrapper<HTMLDivElement> {

    /**
     * The resolved configuration of this instance.
     */
    readonly config: ConfT;

    /**
     * The root DOM node of the popup element.
     */
    readonly el = createDiv("io-ox-office-main popup-container");

    /**
     * The root DOM node of the popup element, as JQuery wrapper.
     */
    readonly $el = $(this.el);

    /**
     * The menu header element.
     */
    readonly headerEl = this.el.appendChild(createDiv("popup-header"));

    /**
     * The menu header element, as JQuery object.
     */
    readonly $header = $(this.headerEl);

    /**
     * The resizable/scrollable content element between header and footer.
     */
    readonly bodyEl = this.el.appendChild(createDiv("popup-content"));

    /**
     * The body element, as JQuery object.
     */
    readonly $body = $(this.bodyEl);

    /**
     * The menu footer element.
     */
    readonly footerEl = this.el.appendChild(createDiv("popup-footer"));

    /**
     * The menu footer element, as JQuery object.
     */
    readonly $footer = $(this.footerEl);

    /** The width and height of the virtual keyboard on touch devices. */
    #keyboard: Size = { width: 0, height: 0 };
    /** Cached anchor settings for detecting changed anchor/boundary. */
    #lastAnchorSettings = dict.createPartial<AnchorSettings & { activeElement: FocusableElement | null }>();

    /** Whether a debounced refresh is pending. */
    #pendingRefresh = false;
    /** Whether to refresh the node automatically in a background loop. */
    #autoRefreshLoop = false;

    // workaround for type errors with extensible event maps
    get #evt(): BasePopup { return this as BasePopup; }

    // constructor ------------------------------------------------------------

    /**
     * @param [config]
     *  Configuration options.
     */
    constructor(config?: NoInfer<ConfT>) {
        super(config);

        // the configuration options passed to the constructor
        this.config = {
            displayType: "normal",
            boundingPadding: 6,
            anchorPadding: 1,
            anchorBorder: ["bottom", "top"],
            anchorAlign: "leading",
            coverAnchor: SMALL_DEVICE,
            scrollPadding: 0,
            ...config
        } as unknown as ConfT;

        // classes and ARIA role
        setElementClasses(this.el, config);
        this.bodyEl.role = config?.role || null;

        // disable dragging of controls (otherwise, it is possible to drag buttons and other controls around)
        this.$el.on("drop dragstart dragenter dragexit dragover dragleave", false);

        // allow text content selection
        if (config?.textSelect) {
            this.el.classList.add("popup-text-select");
        }

        // log layout events
        this.logEvents(popupLogger, ["popup:beforeshow", "popup:show", "popup:beforehide", "popup:hide"]);
    }

    protected override destructor(): void {
        this.el.remove();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Requests a refresh of this popup element. Checks the current position of
     * anchor and bounding box, and repositions the popup root node if anything
     * has changed.
     *
     * @param [options]
     *  Optional parameters.
     */
    refresh(options?: RefreshOptions): void {
        this.#pendingRefresh = true;
        if (options?.immediate) {
            this.#execRefresh();
        } else {
            this.#debouncedRefresh();
        }
    }

    /**
     * Returns whether the popup element is currently visible.
     *
     * @returns
     *  Whether the popup element is currently visible.
     */
    isVisible(): boolean {
        return !!this.el.parentNode;
    }

    /**
     * Shows the popup element.
     */
    @guardMethod
    show(): void {

        // do nothing if the popup node is already open
        if (this.isVisible()) { return; }

        // notify listeners before showing the popup node, event handlers may veto
        let aborted = false;
        const abort: VoidFunction = () => { aborted = true; };
        this.#evt.trigger("popup:beforeshow", abort);
        if (aborted) { return; }

        // do nothing if the anchor is invisible (check here, event handlers of
        // the "popup:beforeshow" event may have initialized the current anchor)
        const anchorSettings = this.#resolveAnchorSettings();
        if (!anchorSettings) { return; }

        // Create the parent storage node for the root element of this popup.
        // Bug 68111: Must not be the <body> element, because it fires touch events in passive mode which would break
        // tracking in some cases.
        // DOCS-3910: Must not be the "#io-ox-core" element because it will be covered by modal dialogs.
        // The class "smart-dropdown-container" enables CSS core styling, and special focus handling in modal dialogs.
        let storageNode: Nullable<HTMLElement>;
        if (this.config.displayType === "embedded") {
            storageNode = document.getElementById("io-ox-core");
        } else {
            storageNode = document.body.appendChild(createDiv("io-ox-office-main popup-storage smart-dropdown-container"));
            if (this.config.displayType === "blocking") {
                const blockerNode = storageNode.appendChild(createDiv("popup-storage-blocker"));
                ["mousedown", "touchstart", "contextmenu"].forEach(type => {
                    blockerNode.addEventListener(type, event => { this.hide(); event.preventDefault(); });
                });
            }
        }

        if (!storageNode) {
            popupLogger.error(() => `$badge{${this.constructor.name}} show: missing storage node`);
            return;
        }

        // insert the popup node into the storage element
        storageNode.appendChild(this.el);

        // show and position the popup node, notify all listeners
        this.#tryRefreshPosition({ anchorSettings, forceLayout: true });
        this.#evt.trigger("popup:show");

        // "show" event handlers must not hide the popup node (TODO: veto via preventDefault?)
        if (!this.isVisible()) {
            popupLogger.error(() => `$badge{${this.constructor.name}} show: "popup:show" event handler has hidden the popup node`);
            return;
        }

        // start a timer that regularly refreshes the position of the popup node, and
        // hides the popup node automatically when the anchor node becomes inaccessible
        this.#autoRefreshLoop = !this.config.detachedAnchor && anchorSettings.dynamic;
        if (this.#autoRefreshLoop) {
            // start automatic refresh loop (calls itself when `this._autoRefreshLoop` is set)
            this.#autoRefreshNode();
        } else {
            this.listenTo(window, "resize", this.#autoRefreshNode);
        }
    }

    /**
     * Hides the popup node.
     */
    @guardMethod
    hide(): void {

        // do nothing if the popup node is not visible
        if (!this.isVisible()) { return; }

        // stop the automatic refresh timer
        this.stopListeningTo(window, "resize", this.#autoRefreshNode);

        // hide the popup node, notify all listeners
        this.#evt.trigger("popup:beforehide");
        const storageEl = this.el.parentElement;
        this.el.remove();
        if (storageEl?.classList.contains("smart-dropdown-container")) { storageEl.remove(); }
        this.#evt.trigger("popup:hide");
    }

    /**
     * Toggles the visibility of the popup node.
     */
    toggle(): void {
        if (this.isVisible()) { this.hide(); } else { this.show(); }
    }

    /**
     * Returns whether this popup element is empty (does not contain any child
     * nodes).
     *
     * @returns
     *  Whether this popup element is empty.
     */
    empty(): boolean {
        return !this.bodyEl.hasChildNodes();
    }

    /**
     * Returns whether the popup element is currently in busy mode (the regular
     * contents of the popup node are hidden, and a busy spinner element is
     * shown instead).
     *
     * @returns
     *  Whether the popup element is currently in busy mode.
     */
    isBusy(): boolean {
        return this.el.classList.contains("popup-busy");
    }

    /**
     * Switches this popup element to busy mode showing a busy spinner only,
     * regardless of the actual contents.
     */
    busy(): void {

        // do nothing if the popup node is in busy mode
        if (this.isBusy()) { return; }

        // hide content node, and show busy node
        this.el.classList.add("popup-busy");
        this.$body.busy({ immediate: true });
        this.refresh();

        // notify listeners
        this.#evt.trigger("popup:busy");
    }

    /**
     * Leaves the busy mode of this popup element started.
     */
    idle(): void {

        // do nothing if the popup node is not in busy mode
        if (!this.isBusy()) { return; }

        // hide busy node, and show content node
        this.el.classList.remove("popup-busy");
        this.$body.idle();
        this.refresh();

        // notify listeners
        this.#evt.trigger("popup:idle");
    }

    /**
     * Scrolls the passed child node into the visible area of this popup node.
     *
     * @param childNode
     *  The DOM element that will be made visible by scrolling this popup node.
     *  If this object is a JQuery collection, uses the first node it contains.
     */
    scrollToChildNode(childNode: NodeOrJQuery): void {
        if (this.isVisible()) {
            scrollToChildNode(this.bodyEl, childNode, { padding: this.config.scrollPadding });
        }
    }

    /**
     * Changes the anchor specification of this popup element (the value passed
     * with the "anchor" constructor option) dynamically.
     *
     * @param anchor
     *  The new anchor specification.
     *
     * @param [options]
     *  Additional configuration options to be changed.
     */
    setAnchor(anchor: PopupAnchorSpec, options?: BasePopupConfig): void {
        this.config.anchor = anchor;
        Object.assign(this.config, options);
        this.refresh({ immediate: true });
    }

    // protected methods ------------------------------------------------------

    /**
     * Will be invoked while updating the position and size of the popup node,
     * before calculating the total size of the popup contents, and the
     * position of the popup node relative to the anchor.
     *
     * @param _anchorRect
     *   The effective anchor position.
     *
     * @param _availableSizes
     *  A dictionary with multiple rectangle sizes available for the contents
     *  of the popup node around the anchor, mapped by the border side names
     *  "left", "right", "top", and "bottom".
     *
     * @returns
     *  May return a result object with configuration options to be changed
     *  temporarily.
     */
    protected implPrepareLayout(_anchorRect: ElementPosition, _availableSizes: Record<CSSBorderPos, Size>): PopupPrepareLayoutResult | void {
        // intentionally empty, supposed to be overridden by subclasses
        return undefined;
    }

    // private methods --------------------------------------------------------

    /**
     * Returns the effective position and size of a position specification.
     *
     * @param positionSpec
     *  A position specification.
     *
     * @param [inner]
     *  If set to `true`, this method will return the position and size of the
     *  inner client area of a DOM node (inner area without borders and scroll
     *  bars), instead of the outer node position and size.
     *
     * @returns
     *  The resolved location of the passed position specification; or
     *  `undefined`, if no location could be resolved. The property "dynamic"
     *  specifies whether the position specification was a DOM element or
     *  callback function (`true`), or a fixed rectangle (`false`).
     */
    #resolvePositionSpec(positionSpec: PopupAnchorSpec, inner = false): Opt<BoundRectangle> {

        let boundRect: Opt<BoundRectangle>;
        let dynamic = is.function(positionSpec);

        // resolve callback functions
        const position = is.function(positionSpec) ? positionSpec.call(this) : positionSpec;

        // resolve position of DOM element or jQuery object
        const node = isNodeSource(position) ? toNode(position) : null;
        if (node) {
            boundRect = getElementPosition(node, { inner });
            dynamic = true; // DOM elements always dynamic
        }

        // DOM rectangle with "x" and "y" coordinates
        if (position instanceof DOMRect) {
            boundRect = addTrailingDistances(new Rectangle(position.x, position.y, position.width, position.height));
        }

        // rectangles, or other partial rectangle-like objects
        if ((position instanceof Rectangle) || is.dict(position)) {
            // add right/bottom distance to the rectangle
            boundRect = addTrailingDistances(position);
        }

        // return the result object
        if (boundRect && dynamic) { boundRect.dynamic = true; }
        return boundRect;
    }

    /**
     * Resolves all settings for popup anchor and bounding box.
     *
     * @returns
     *  An object with the anchor settings; or `undefined`, if no valid anchor
     *  could be resolved (popup element will not be rendered).
     */
    #resolveAnchorSettings(): Opt<AnchorSettings> {

        // Current position/size of the visible area of the browser window. The properties
        // `window.inner(Width|Height)` on their own are not reliable when virtual keyboard
        // on touch devices is visible. This "hack" works for applications with fixed page size.
        const docSize = getDocumentSize();
        const windowArea = new Rectangle(
            window.pageXOffset,
            window.pageYOffset,
            math.clamp(docSize.width - this.#keyboard.width, 0, window.innerWidth),
            math.clamp(docSize.height - this.#keyboard.height, 0, window.innerHeight)
        );

        // workaround for flickering and self closing popups (compound menus)
        // `window.pageYOffset` is higher than zero because the body is moved for a short moment.
        if (IOS_SAFARI_DEVICE) {
            windowArea.top = 0;
        }

        // the current position of the bounding box (client area, may become null)
        const boundPos = this.#resolvePositionSpec(this.config.boundingBox, true);

        // intersect window area with the bounding box, if existing
        // (results in `null`, if bounding box is not part of the window area)
        const boundRect = boundPos ? windowArea.intersect(boundPos) : windowArea;
        if (!boundRect) { return; }

        // remove padding from the bounding box
        const padding = this.config.boundingPadding!;
        boundRect.left += padding;
        boundRect.top += padding;
        boundRect.width = Math.max(0, boundRect.width - 2 * padding);
        boundRect.height = Math.max(0, boundRect.height - 2 * padding);

        // reject bounding boxes with invalid size
        if ((boundRect.width <= 0) || (boundRect.height <= 0)) { return; }

        // resolve the anchor position
        const anchorPos = this.#resolvePositionSpec(this.config.anchor);
        if (!anchorPos) { return; }

        // reduce anchor position to anchor box if specified (anchor may become null)
        let anchorRect: Nullable<Rectangle> = Rectangle.from(anchorPos);
        const anchorBoxPos = this.#resolvePositionSpec(this.config.anchorBox, true);
        if (anchorBoxPos) { anchorRect = anchorRect.intersect(anchorBoxPos); }
        if (!anchorRect) { return; }

        // anchor must be located inside the bounding box (except for detached anchor mode)
        if (!this.config.detachedAnchor && !boundRect.overlaps(anchorRect)) { return; }

        // return the result object
        return {
            boundRect: addTrailingDistances(boundRect),
            anchorRect: addTrailingDistances(anchorRect),
            dynamic: boundPos?.dynamic || anchorPos.dynamic || !!anchorBoxPos?.dynamic
        };
    }

    /**
     * Checks whether the position of this popup element needs to be updated,
     * according to the current position of the anchor node (a scrollable
     * parent node may have been scrolled). In that case, the events
     * "popup:beforelayout" and "popup:layout" will be triggered to all
     * listeners. In automatic layout mode, the position and size of the popup
     * node will be updated automatically (the events will be triggered
     * anyway).
     *
     * @param [options]
     *  Optional parameters.
     */
    #tryRefreshPosition(options?: RefreshPositionOptions): void {

        // reset internal states when layout update is forced by caller
        let needsLayout = options?.forceLayout;
        if (needsLayout) { this.#lastAnchorSettings = {}; }

        // close popup node, if the bounding box or the anchor is not visible anymore
        const anchorSettings = options?.anchorSettings ?? this.#resolveAnchorSettings();
        if (!anchorSettings) {
            if (options?.autoHide) { this.hide(); }
            return;
        }

        // on touch devices, get the size of the virtual keyboard
        const activeElement = getFocus();
        if (IOS_SAFARI_DEVICE && (this.#lastAnchorSettings.activeElement !== activeElement)) {
            this.#lastAnchorSettings.activeElement = activeElement;
            needsLayout = true;
            this.#keyboard = { width: 0, height: 0 };
            if (isSoftKeyboardOpen() || isTextInputElement(activeElement)) {
                this.#keyboard.height = getSoftKeyboardHeight();
            }
        }

        // check whether the visible area of the browser window has changed, or the position
        // of the anchor node (for example, by scrolling a parent node of the anchor node)
        const { boundRect, anchorRect } = anchorSettings;
        needsLayout ||= !equalRects(this.#lastAnchorSettings.boundRect, boundRect) || !equalRects(this.#lastAnchorSettings.anchorRect, anchorRect);
        Object.assign(this.#lastAnchorSettings, anchorSettings);

        // refreshing node may not be needed
        if (needsLayout) {
            this.#execRefreshPosition(anchorSettings);
        }
    }

    @popupLogger.profileMethod("$badge{BasePopup} execRefreshPosition")
    #execRefreshPosition(anchorSettings: AnchorSettings): void {

        // notify listeners to be able to update popup contents immediately before node refresh
        this.#evt.trigger("popup:beforelayout");

        const { boundRect, anchorRect } = anchorSettings;

        // maximum available space for the popup element in the bounding box
        const maxWidth = boundRect.width;
        const maxHeight = boundRect.height;
        // padding distance to anchor node
        const anchorPadding = this.config.anchorPadding!;
        // rescue scroll position of body element
        const { scrollLeft, scrollTop } = this.bodyEl;

        // the sizes available for the popup contents at every side of the anchor
        const availableSizes = {
            left:   { width: math.clamp(anchorRect.left  - boundRect.left  - anchorPadding, 0, maxWidth), height: maxHeight },
            right:  { width: math.clamp(anchorRect.right - boundRect.right - anchorPadding, 0, maxWidth), height: maxHeight },
            top:    { width: maxWidth, height: math.clamp(anchorRect.top    - boundRect.top    - anchorPadding, 0, maxHeight) },
            bottom: { width: maxWidth, height: math.clamp(anchorRect.bottom - boundRect.bottom - anchorPadding, 0, maxHeight) }
        };

        // call layout preparation callback handler if available
        const result = this.implPrepareLayout(anchorRect, availableSizes);

        // remove minimum/maximum size attributes, get natural size of the popup node
        setElementStyles(this.el, { minWidth: null, minHeight: null, maxWidth: null, maxHeight: null, left: null, right: null, top: null, bottom: null });
        const { width: popupWidth, height: popupHeight } = getCeilNodeSize(this.el);
        const popupArea = Math.max(1, popupWidth * popupHeight);

        // calculate the ratio of the popup node being visible at every side of the anchor
        const ratios = dict.mapRecord(availableSizes, size => {
            const effectiveWidth = Math.min(popupWidth, size.width);
            const effectiveHeight = Math.min(popupHeight, size.height);
            return effectiveWidth * effectiveHeight / popupArea;
        });

        // pick the best border that can take the popup node, in the given order
        const anchorBorder = ary.wrap(result?.anchorBorder ?? this.config.anchorBorder);
        let preferredBorder!: CSSBorderPos;
        let maxRatio = 0;
        for (const border of anchorBorder) {
            const ratio = ratios[border];
            if (ratio > maxRatio) {
                preferredBorder = border;
                maxRatio = ratio;
                if (ratio === 1) { break; }
            }
        }

        // expand size of popup node to anchor size
        const vertical = isVerticalPosition(preferredBorder);
        const minWidth = (vertical && this.config.stretchToAnchor) ? anchorRect.width : 0;
        const minHeight = (!vertical && this.config.stretchToAnchor) ? anchorRect.height : 0;

        // the resulting size available for the popup node
        const availableSize: Size = this.config.coverAnchor ? { width: maxWidth, height: maxHeight } : availableSizes[preferredBorder];
        const finalWidth = Math.min(Math.max(popupWidth, minWidth), availableSize.width);
        const finalHeight = Math.min(Math.max(popupHeight, minHeight), availableSize.height);

        // the resulting CSS properties to be set at the root node
        const cssProps: CSSStyleSpec = {
            minWidth: minWidth ? Math.min(minWidth, availableSize.width) : null,
            minHeight: minHeight ? Math.min(minHeight, availableSize.height) : null,
            maxWidth: availableSize.width,
            maxHeight: availableSize.height
        };

        // first part of the position (keep in bounding box, outside of the anchor)
        switch (preferredBorder) {
            case "top":    cssProps.bottom = getDocumentSize().height - anchorRect.top    + anchorPadding; break;
            case "bottom": cssProps.top    = anchorRect.top           + anchorRect.height + anchorPadding; break;
            case "left":   cssProps.right  = getDocumentSize().width  - anchorRect.left   + anchorPadding; break;
            case "right":  cssProps.left   = anchorRect.left          + anchorRect.width  + anchorPadding; break;
        }

        // second part of the position (use specified alignment mode, keep in bounding box)
        const alignOffsetProp = vertical ? "left" : "top";
        const alignSizeProp = vertical ? "width" : "height";
        const alignPos = this.#resolvePositionSpec(this.config.anchorAlignBox) ?? anchorRect;
        const alignOffset = alignPos[alignOffsetProp];
        const alignSize = alignPos[alignSizeProp];
        const anchorOffset = anchorRect[alignOffsetProp];
        const anchorSize = anchorRect[alignSizeProp];
        const finalSize = vertical ? finalWidth : finalHeight;
        switch (result?.anchorAlign || this.config.anchorAlign) {
            case "trailing": cssProps[alignOffsetProp] = math.clamp(alignOffset + alignSize, anchorOffset, anchorOffset + anchorSize) - finalSize;                     break;
            case "center":   cssProps[alignOffsetProp] = Math.floor(math.clamp(alignOffset + alignSize / 2, anchorOffset, anchorOffset + anchorSize) - finalSize / 2); break;
            default:         cssProps[alignOffsetProp] = math.clamp(alignOffset, anchorOffset, anchorOffset + anchorSize);
        }

        // restrict all existing positioning attributes to the bounding box, reset all other properties
        cssProps.left   = is.number(cssProps.left)   ? Math.max(boundRect.left,   Math.min(cssProps.left,   boundRect.left   + boundRect.width  - finalWidth))  : "";
        cssProps.right  = is.number(cssProps.right)  ? Math.max(boundRect.right,  Math.min(cssProps.right,  boundRect.right  + boundRect.width  - finalWidth))  : "";
        cssProps.top    = is.number(cssProps.top)    ? Math.max(boundRect.top,    Math.min(cssProps.top,    boundRect.top    + boundRect.height - finalHeight)) : "";
        cssProps.bottom = is.number(cssProps.bottom) ? Math.max(boundRect.bottom, Math.min(cssProps.bottom, boundRect.bottom + boundRect.height - finalHeight)) : "";

        // apply final CSS formatting
        setElementStyles(this.el, cssProps);
        this.el.dataset.border = preferredBorder;

        // restore scroll position of body element
        this.bodyEl.scrollLeft = scrollLeft;
        this.bodyEl.scrollTop = scrollTop;

        // notify listeners after refresh is done
        this.#evt.trigger("popup:layout");

        // debug logging
        popupLogger.trace(() =>
            "popup node layout:\n" +
            ` bounding:  ${debug.stringify(boundRect)}\n` +
            ` anchor:    ${debug.stringify(anchorRect)}\n` +
            ` available: ${debug.stringify(availableSizes)}\n` +
            ` preferred: ${preferredBorder}\n` +
            ` result:    ${debug.stringify(cssProps)}`
        );
    }

    /**
     * Refreshes this popup element synchronously. This is the actual refresh
     * implementation that will be invoked from synchronous and asynchronous
     * refresh requests.
     */
    #execRefresh(): void {

        // nothing to do, if no refresh request is pending (anymore), or if popup has been hidden
        if (!this.#pendingRefresh || !this.isVisible()) { return; }

        // force refreshing the root node, automatically hide when anchor invalidates
        this.#tryRefreshPosition({ forceLayout: true, autoHide: true });

        // start accepting more refresh requests
        this.#pendingRefresh = false;
    }

    /**
     * Collects multiple refresh requests, and decides whether to actually
     * execute the refresh (depending on whether another synchronous refresh
     * was executed before).
     */
    @debounceMethod({ delay: "animationframe" })
    #debouncedRefresh(): void {
        this.#execRefresh();
    }

    // handler for window resize to refresh the node
    @debounceMethod({ delay: SMALL_DEVICE ? 200 : 100, maxDelay: 1000 })
    #autoRefreshNode(): void {

        // nothing to do, if popup has been hidden (especially, break auto-refresh loop)
        if (!this.isVisible()) { return; }

        // refresh the root node, automatically hide when anchor invalidates
        this.#tryRefreshPosition({ autoHide: true });

        // keep background loop running, if auto-refresh flag is set
        if (this.#autoRefreshLoop) { this.#autoRefreshNode(); }
    }
}
