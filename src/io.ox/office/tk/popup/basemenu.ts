/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { is, ary } from "@/io.ox/office/tk/algorithms";
import type { CoreEmitter, AllEventsEmitter, AnyEmitter } from "@/io.ox/office/tk/events";
import { isStickyPopupsMode } from "@/io.ox/office/tk/config";
import type { NodeSource, FocusableElement } from "@/io.ox/office/tk/dom";
import { CLOSE_LABEL, isNodeSource, containsNode, createDiv, createButton, setFocus, containsFocus, findFocusable, isTextInputElement, isEscapeKey, isFunctionKey } from "@/io.ox/office/tk/dom";
import { isSoftKeyboardOpen, shouldHideSoftKeyboard } from "@/io.ox/office/tk/utils";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import type { BasePopupConfig, BasePopupEventMap } from "@/io.ox/office/tk/popup/basepopup";
import { BasePopup } from "@/io.ox/office/tk/popup/basepopup";

import "@/io.ox/office/tk/popup/basemenu.less";

// types ======================================================================

export type FocusableNodesResolverFn = (target: Element) => NodeSource;

/**
 * Specification type of additional DOM nodes located outside the root node of
 * a popup menu that do not cause to close the menu automatically while they
 * are focused. Can be a function that will be called on every focus change,
 * and that must return a DOM node source.
 */
export type FocusableNodesSpec = string | NodeSource | FocusableNodesResolverFn;

/**
 * Configuration options for class `BaseMenu`.
 */
export interface BaseMenuConfig extends BasePopupConfig {

    /**
     * A title label to be shown in the header area of the menu.
     */
    title?: string;

    /**
     * If set to `true`, a "Close" button will be shown in the header area of
     * the menu. Default value is `false`.
     */
    closable?: boolean;

    /**
     * If set to `true` (or omitted), the browser focus will be moved into the
     * popup menu after it has been opened. If set to `false`, the browser
     * focus will not be changed. In this case, the node currently focused must
     * be contained in the option "focusableNodes" to prevent that the menu
     * will be closed immediately afterwards due to missing menu focus.
     *
     * _Attention:_ Default value is `true`!
     */
    autoFocus?: boolean;

    /**
     * If set to `true` (or omitted), and the browser focus leaves the popup
     * menu node (due to outside clicks, TAB key, etc.), the menu will be
     * closed automatically.
     *
     * _Attention:_ Default value is `true`!
     */
    autoClose?: boolean;

    /**
     * Additional DOM nodes located outside the root node of this popup menu
     * that do not cause to close the menu automatically while they are
     * focused. More focusable nodes can be registered at runtime with the
     * method `BaseMenu::registerFocusableNodes`. Can be a function that will
     * be called on every focus change, and that must return a DOM node source.
     */
    focusableNodes?: FocusableNodesSpec;

    /**
     * A JQuery selector that filters the available focusable control elements
     * to a list of preferred controls, that will be used when moving the
     * browser focus into this popup menu using the method
     * `BaseMenu::grabFocus`. The selector will be passed to the JQuery method
     * `filter`. If this selector is a function, it will be called with the DOM
     * node bound to the symbol "this". See the JQuery API documentation at
     * <http://api.jquery.com/filter> for details.
     */
    preferFocusFilter?: string | ((this: HTMLElement, index: number, element: HTMLElement) => boolean);

    /**
     * If set to `true`, the menu will be repainted every time it becomes
     * visible. By default, the menu will only be repainted when becoming
     * visible the first time, and when repaints have been requested by the
     * methods `BaseMenu::requestMenuRepaint`, `requestMenuRepaintOnEvent`,
     * `requestMenuRepaintOnAllEvents`, or `requestMenuRepaintOnGlobalEvent`.
     */
    repaintAlways?: boolean;
}

/**
 * Type mapping for the events emitted by `BaseMenu` instances.
 */
export interface BaseMenuEventMap extends BasePopupEventMap {

    /**
     * Will be emitted before the menu will be repainted (immediately before
     * `BaseMenu::implRepaintMenu` will be called).
     */
    "menu:beforerepaint": [];

    /**
     * Will be emitted while the menu is being repainted (immediately after
     * `BaseMenu::implRepaintMenu` has been called, and before
     * `BaseMenu::implAfterRepaintMenu` will be called).
     */
    "menu:implrepaint": [];

    /**
     * Will be emitted after the menu has been repainted (immediately after
     * `BaseMenu::implAfterRepaintMenu` has been called).
     */
    "menu:afterrepaint": [];

    /**
     * Will be emitted before the menu will be closed due to a keyboard event,
     * e.g. the ESCAPE button.
     */
    "menu:close:keyboard": [];

    /**
     * Will be emitted after the "Close" button shown in the menu header has
     * been clicked.
     */
    "menu:userclose": [];
}

/**
 * Optional parameters for the method `BaseMenu::grabFocus`.
 */
export interface BaseMenuGrabFocusOptions {

    /**
     * If set to `true`, the last available entry of the popup menu will be
     * focused instead of the first. Default value is `false`.
     */
    bottom?: boolean;
}

// class BaseMenu =============================================================

/**
 * Wrapper class for a DOM node used as interactive popup menu element, shown
 * on top of the application window, and relative to an arbitrary DOM node. The
 * class adds basic browser focus handling. On opening, the browser focus will
 * be moved invisibly into the menu element to enable keyboard events (focus
 * navigation) inside the menu. When the browser focus leaves the menu node
 * (regardless how that happened, e.g. clicks or taps anywhere outside the
 * menu, or special key shortcuts to move the focus away), the menu will close
 * itself automatically.
 */
export class BaseMenu<ConfT extends BaseMenuConfig = BaseMenuConfig, EvtMapT extends BaseMenuEventMap = BaseMenuEventMap> extends BasePopup<ConfT, EvtMapT> {

    // static collection of DOM elements preventing auto-close of the menu
    #$focusableNodes = $();
    // CSS selectors for additional focusable DOM elements preventing auto-close
    readonly #focusableSelectors: string[] = [];
    // dynamic callbacks for additional focusable DOM elements preventing auto-close
    readonly #focusableResolvers: FocusableNodesResolverFn[] = [];

    // scope for event handlers to be unregistered when hiding the menu
    readonly #eventScope = Symbol("event scope");

    // dirty marker for lazy repaint
    #pendingRepaint = true;

    // workaround for type errors with extensible event maps
    get #evt(): BaseMenu { return this as BaseMenu; }

    // constructor ------------------------------------------------------------

    /**
     * @param config
     *  Configuration options.
     */
    constructor(config?: ConfT) {

        // base constructor
        super({ autoFocus: true, autoClose: true, ...config } as unknown as ConfT);

        // add basic popup menu styling
        this.el.classList.add("popup-menu");

        // create the title label in the menu header
        if (config?.title) {
            this.headerEl.append(createDiv({ classes: "title-label", label: config.title }));
        }

        // create the close button element
        if (config?.closable) {
            this.headerEl.append(createButton({
                type: "bare",
                action: "close",
                icon: "bi:x-lg",
                tooltip: CLOSE_LABEL,
                // DOCS-4201: defer closing this menu to workaround conflict with Core-UI popups, e.g. halo boxes
                click: () => this.setTimeout(() => { this.#evt.trigger("menu:userclose").hide(); })
            }));
        }

        // initialize static focusable nodes and dynamic callbacks for focusable nodes
        if (config?.focusableNodes) {
            this.registerFocusableNodes(config.focusableNodes);
        }

        // make the content node focusable to be able to provide keyboard support
        // without actually focusing a visible node inside the menu
        this.bodyEl.tabIndex = -1;
        this.registerFocusableNodes(this.el);

        // register event handlers
        this.on("popup:beforeshow", this.#baseMenuBeforeShowHandler);
        this.on("popup:show", this.#baseMenuShowHandler);
        this.on("popup:beforehide", this.#baseMenuBeforeHideHandler);
        this.on("popup:idle", this.#baseMenuIdleHandler);
        this.listenTo(this.el, "keydown", this.#baseMenuKeyDownHandler);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this popup menu contains the browser focus.
     *
     * @returns
     *  Whether this popup menu contains the browser focus.
     */
    hasFocus(): boolean {
        return containsFocus(this.el);
    }

    /**
     * Sets the browser focus into the first focusable control element in this
     * popup menu element.
     *
     * @param [options]
     *  Optional parameters:
     */
    grabFocus(options?: BaseMenuGrabFocusOptions): void {

        // do nothing if a descendant of the content node is already focused
        if (containsFocus(this.bodyEl)) { return; }

        // all focusable control elements
        const $focusable = findFocusable(this.el);
        // the preferred controls returned by the callback function
        let $preferred = this.config.preferFocusFilter ? $focusable.filter(this.config.preferFocusFilter) : $focusable;

        // fall back to all focusable controls if no preferred controls are available
        if ($preferred.length === 0) {
            $preferred = ($focusable.length > 0) ? $focusable : this.$body;
        }
        setFocus(options?.bottom ? $preferred.last() : $preferred.first());
    }

    /**
     * Registers one or more DOM nodes located outside the root node of this
     * popup menu that do not cause to close the menu automatically while they
     * are focused.
     *
     * @param sourceSpec
     *  The DOM node(s) to be added to the list of focusable elements. Can be a
     *  function that will be called on every focus change, and that must
     *  return a DOM node source.
     */
    registerFocusableNodes(sourceSpec: FocusableNodesSpec): void {
        if (is.string(sourceSpec)) {
            this.#focusableSelectors.push(sourceSpec);
        } else if (is.function(sourceSpec)) {
            this.#focusableResolvers.push(sourceSpec);
        } else if (sourceSpec && isNodeSource(sourceSpec)) {
            this.#$focusableNodes = this.#$focusableNodes.add(sourceSpec as JQuery);
        }
    }

    /**
     * Unregisters one or more DOM nodes, that have been registered with the
     * method `registerFocusableNodes`.
     *
     * @param sourceSpec
     *  The DOM node(s) to be removed from the list of focusable elements.
     */
    unregisterFocusableNodes(sourceSpec: FocusableNodesSpec): void {
        if (is.string(sourceSpec)) {
            ary.deleteFirst(this.#focusableSelectors, sourceSpec);
        } else if (is.function(sourceSpec)) {
            ary.deleteFirst(this.#focusableResolvers, sourceSpec);
        } else if (sourceSpec && isNodeSource(sourceSpec)) {
            this.#$focusableNodes = this.#$focusableNodes.not(sourceSpec as JQuery);
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Invalidates this menu (sets it to pending repaint state). When opening
     * the menu, or if it is currently visible, a full repaint of this menu
     * will be started.
     */
    protected requestMenuRepaint(): void {
        this.#pendingRepaint = true;
        if (this.isVisible()) {
            this.#debouncedRepaintPendingMenu();
        }
    }

    /**
     * Invalidates this menu (sets it to pending repaint state), when an event
     * emitter triggers specific events. After receiving such events, when
     * opening the menu, or if it is currently visible, a full repaint of this
     * menu will be started.
     */
    protected requestMenuRepaintOnEvent<E>(emitter: AnyEmitter<E>, type: OrArray<keyof E>): void {
        this.listenTo(emitter as CoreEmitter<E>, type, this.requestMenuRepaint);
    }

    /**
     * Invalidates this menu (sets it to pending repaint state), when the event
     * emitter has emitted an event. After receiving such events, when opening
     * the menu, or if it is currently visible, a full repaint of this menu
     * will be started.
     */
    protected requestMenuRepaintOnAllEvents<E>(emitter: AllEventsEmitter<E>): void {
        this.listenToAllEvents(emitter as CoreEmitter<E>, this.requestMenuRepaint);
    }

    /**
     * Invalidates this menu (sets it to pending repaint state), when an event
     * emitter triggers specific events. After receiving such events, when
     * opening the menu, or if it is currently visible, a full repaint of this
     * menu will be started.
     */
    protected requestMenuRepaintOnGlobalEvent(type: keyof DocsGlobalEventMap): void {
        this.listenToGlobal(type, this.requestMenuRepaint);
    }

    /**
     * Callback method for lazy repaint of the menu contents. This method will
     * be called:
     * - before the menu becomes visible the first time,
     * - before the menu becomes visible, and has been invalidated before,
     * - when the menu will be invalidated while it is visible.
     */
    protected implRepaintMenu(): void {
        // to be overwritten by subclasses
    }

    /**
     * Callback method for lazy repaint of the menu contents. This method will
     * be called:
     * - after calling `implRepaintMenu`,
     * - before the menu becomes visible the first time,
     * - before the menu becomes visible, and has been invalidated before,
     * - when the menu will be invalidated while it is visible.
     */
    protected implAfterRepaintMenu(): void {
        // to be overwritten by subclasses
    }

    // private methods --------------------------------------------------------

    /**
     * Returns whether the passed DOM node is contained in the list of
     * focusable nodes which prevent auto-closing the menu if it loses the
     * browser focus.
     */
    #isFocusableNode(node: Element | null): boolean {

        if (!node) { return false; }

        // check whether the element matches a string selector
        if (this.#focusableSelectors.some(selector => $(node).is(selector))) {
            return true;
        }

        // collect all focusable nodes (start with static nodes)
        let $focusableNodes = this.#$focusableNodes;

        // add dynamic focusable nodes
        for (const resolver of this.#focusableResolvers) {
            $focusableNodes = $focusableNodes.add(resolver.call(this, node) as JQuery);
        }

        // check whether the passed node is contained in the focusable nodes (directly, or as descendant)
        return ($focusableNodes.filter(node).length > 0) || ($focusableNodes.has(node).length > 0);
    }

    /**
     * Closes this menu if the passed node is not included in the registered
     * focusable nodes.
     */
    #hideMenuOnFocusNode(node: Element | null): boolean {
        if (this.config.autoClose && node && !this.#isFocusableNode(node)) {
            // do not close the menu, if the debug flag "office:sticky-popups" is set (but still return true)
            if (!isStickyPopupsMode()) { this.hide(); }
            return true;
        }
        return false;
    }

    /**
     * Moves the browser focus to the content node, if automatic focus handling
     * is enabled. If the virtual keyboard is open, this focus change will
     * close it (e.g. virtual keyboard is open and you open a popup).
     */
    #autoFocusContentNode(): void {
        if (this.config.autoFocus && (!isSoftKeyboardOpen() || shouldHideSoftKeyboard())) {
            setFocus(this.bodyEl);
        }
    }

    /**
     * Hides the menu node automatically when the browser focus leaves.
     */
    #changeFocusHandler(focusNode: FocusableElement | null): void {

        // close the menu unless the focus has been moved to the registered nodes
        if (this.#hideMenuOnFocusNode(focusNode)) {
            return;
        }

        // scroll the popup menu to make the focused node visible
        if (focusNode && containsNode(this.bodyEl, focusNode)) {
            this.scrollToChildNode(focusNode as HTMLElement);
        }
    }

    /**
     * Handles mouse clicks everywhere on the page, and closes the context
     * menu automatically.
     */
    #globalClickHandler(event: UIEvent): void {
        this.#hideMenuOnFocusNode(event.target as Element);
    }

    /**
     * If this menu is in pending repaint state:
     * - the event "menu:beforerepaint" will be triggered,
     * - the implementation method `implRepaintMenu` will be called,
     * - the event "menu:implrepaint" will be triggered,
     * - the implementation method `implAfterRepaintMenu` will be called,
     * - and the event "menu:afterrepaint" will be triggered.
     */
    #repaintPendingMenu(): void {
        if (this.config.repaintAlways || this.#pendingRepaint) {
            this.#pendingRepaint = false;
            this.#evt.trigger("menu:beforerepaint");
            this.implRepaintMenu();
            this.#evt.trigger("menu:implrepaint");
            this.implAfterRepaintMenu();
            this.#evt.trigger("menu:afterrepaint");
            this.refresh({ immediate: true });
        }
    }

    /**
     * Runs a debounced repaint, if this menu is in pending repaint state.
     */
    @debounceMethod({ delay: "animationframe" })
    #debouncedRepaintPendingMenu(): void {
        if (this.isVisible()) {
            this.#repaintPendingMenu();
        }
    }

    /**
     * Performs lazy repaint of the menu contents before this menu will be
     * opened.
     */
    #baseMenuBeforeShowHandler(): void {
        this.#repaintPendingMenu();
    }

    /**
     * Initializes the popup menu after it has been opened.
     */
    #baseMenuShowHandler(): void {

        // move focus into the menu node, for keyboard handling
        this.#autoFocusContentNode();

        // listen to focus events, hide the menu when focus leaves the menu
        this.listenToGlobal("change:focus", this.#changeFocusHandler, { scope: this.#eventScope });

        // Add global click handler that closes the menu automatically when
        // clicking somewhere in the page without changing the browser focus
        // (elements without tab-index). The event handler must be attached
        // deferred, otherwise it would close the context menu immediately
        // after it has been opened with a mouse click while the event bubbles
        // up to the document root.
        this.setTimeout(() => {
            if (this.isVisible()) {
                this.listenTo(document, ["mousedown", "touchstart"], this.#globalClickHandler, { scope: this.#eventScope });
            }
        });
    }

    /**
     * Deinitializes the popup menu before it will be closed.
     */
    #baseMenuBeforeHideHandler(): void {
        this.stopListeningToScope(this.#eventScope);
    }

    /**
     * Initializes the popup menu after it has left the busy mode.
     */
    #baseMenuIdleHandler(): void {
        // move focus into the menu node, for keyboard handling
        this.#autoFocusContentNode();
    }

    /**
     * Hides the popup menu, if the ESCAPE key has been pressed inside.
     */
    #baseMenuKeyDownHandler(event: KeyboardEvent): void {

        if (isEscapeKey(event)) {

            // Bugfix 42347: in case the basemenu is destroyed on hide(), other key listeners
            // should not be called on that destroyed object anymore
            event.stopImmediatePropagation();
            event.preventDefault();

            // listeners may want to adjust focus before closing the menu
            this.#evt.trigger("menu:close:keyboard");

            this.hide();
        }

        // we need to call preventDefault() because of focus traveling back to the document when this menu has no input fields
        if (!isTextInputElement(event.target) && !isFunctionKey(event)) {
            event.preventDefault();
        }
    }
}
