/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { isVerticalPosition } from "@/io.ox/office/tk/dom";
import { APP_TOOLTIP_CLASS } from "@/io.ox/office/tk/forms";
import { BasePopup } from "@/io.ox/office/tk/popup/basepopup";

import "@/io.ox/office/tk/popup/tooltip.less";

// class ToolTip ==============================================================

/**
 * Wrapper class for a DOM node used as tooltip info element, shown on top of
 * the application window, and relative to an arbitrary DOM node.
 *
 * @param {object} [config]
 *  Initial options to control the properties of the tooltip element. Supports
 *  all options also supported by the base class `BasePopup`. The default value
 *  of the option "anchorPadding" will be set to 3 though.
 */
export class ToolTip extends BasePopup {

    constructor(config) {

        // base constructor
        super({
            anchorPadding: 3,
            ...config
        });

        // add tooltip styling
        this.$el.addClass("popup-tooltip");

        // veto showing the tooltip if it is empty
        this.on("popup:beforeshow", abort => {
            if (this.empty()) { abort(); }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Inserts the passed plain text into the tooltip.
     *
     * @param {string} text
     *  The text to be inserted into the tooltip.
     */
    setText(text) {
        this.$body.empty();
        if (text) { this.$body.text(text); }
        // immediately refresh position (prevent display glitches with oversized and thus wrapping text)
        this.refresh({ immediate: true });
    }
}

// global hover tooltip =======================================================

class HoverToolTip extends ToolTip {

    constructor(selector, config) {

        // base constructor
        super({
            anchorAlign: "center",
            ...config,
            anchor: () => $anchor,
            _kind: "singleton"
        });

        // the current anchor node hovered by the mouse
        let $anchor = null;

        // create the arrow node
        const $arrow = $('<div class="hover-arrow">');
        this.$el.addClass("hover-style").append($arrow);

        // initialize and show tooltip when hovering any matching element
        $(document.body).on("mouseenter", selector, event => {

            // set tooltip text from anchor node attribute, and show the tooltip
            $anchor = $(event.currentTarget);
            this.setText($anchor.attr("data-original-title"));
            this.show();

            // adjust position of arrow node (always point to the center of the anchor node)
            const isVertical = isVerticalPosition(this.$el.attr("data-border"));
            const propName = isVertical ? "left" : "top";
            const anchorPos = $anchor.offset()[propName];
            const toolTipPos = this.$el.offset()[propName];
            const anchorSize = isVertical ? $anchor.outerWidth() : $anchor.outerHeight();
            const arrowSize = isVertical ? $arrow.outerWidth() : $arrow.outerHeight();
            $arrow.css(propName, Math.round(anchorPos + anchorSize / 2 - toolTipPos - arrowSize / 2));
        });

        // hide tooltip when clicking or leaving any matching element
        $(document.body).on("mouseleave mousedown mouseup", selector, () => {
            this.hide();
        });
    }
}

// the global tooltip instance (will live forever)
// eslint-disable-next-line no-new
new HoverToolTip(`.io-ox-office-main .${APP_TOOLTIP_CLASS}`);
