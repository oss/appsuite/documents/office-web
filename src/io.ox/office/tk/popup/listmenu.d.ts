/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { CompareFn } from "@/io.ox/office/tk/algorithms";
import { NodeOrJQuery, AnyKeyEvent, ControlValueSerializerFn } from "@/io.ox/office/tk/dom";
import { CreateButtonNodeOptions } from "@/io.ox/office/tk/forms";
import { BaseMenuConfig, BaseMenuEventMap, BaseMenu } from "@/io.ox/office/tk/popup/basemenu";

// types ======================================================================

export interface ListSectionSettings {
    sorted?: boolean | CompareFn<unknown>;
    gridColumns?: number;
}

export interface ListMenuConfig<VT> extends BaseMenuConfig, ListSectionSettings {
    title?: string;
    multiSelect?: boolean;
    listLayout?: "list" | "grid";
    mruSize?: number;
    mruSettingsKey?: string;
    matcher?: (value: VT, option: VT) => boolean | null;
    filter?: (value: VT, section: string) => boolean | null;
    valueSerializer?: ControlValueSerializerFn<VT>;
}

export interface ListSectionOptions {
    section?: string;
}

export interface ListAddSectionOptions extends ListSectionSettings {
    label?: string;
}

export interface ListAddOptionOptions extends ListSectionOptions, CreateButtonNodeOptions {
    sortIndex?: unknown;
}

export interface ListSelectOptionOptions {
    scroll?: boolean;
}

export class ListMenuSection {
    readonly id: string;
}

/**
 * Type mapping for the events emitted by `ListMenu` instances.
 */
export interface ListMenuEventMap<VT> extends BaseMenuEventMap {

    /**
     * Will be emitted after a new section has been created with the method
     * `ListMenu::addSection`.
     *
     * @param section
     *  The new section instance.
     */
    "list:section:add": [section: ListMenuSection];

    /**
     * Will be emitted after a new option button has been created with the
     * method `ListMenu::addOption`.
     *
     * @param $button
     *  The new option button.
     *
     * @param value
     *  The value of the option button.
     *
     * @param section
     *  The identifier of the target section.
     */
    "list:option:add": [$button: JQuery, value: VT, section: string];

    /**
     * Will be emitted before an option button will be deleted with the method
     * `ListMenu::deleteOption`.
     *
     * @param $button
     *  The option button to be deleted.
     *
     * @param value
     *  The value of the option button.
     */
    "list:option:delete": [$button: JQuery, value: VT];
}

// class ListMenu =============================================================

export class ListMenu<VT, ConfT extends ListMenuConfig<VT> = ListMenuConfig<VT>, EvtMapT extends ListMenuEventMap<VT> = ListMenuEventMap<VT>> extends BaseMenu<ConfT, EvtMapT> {

    constructor(config?: ConfT);

    addSection(id: string, options?: ListAddSectionOptions | string): void;
    addOption(value: VT, options?: ListAddOptionOptions): JQuery;
    deleteOption(value: VT, options?: ListSectionOptions): void;
    clearOptions(options?: ListSectionOptions): void;
    getOptions(options?: ListSectionOptions): JQuery;
    findOptions(value: VT, options?: ListSectionOptions): JQuery;
    getSelectedOptions(options?: ListSectionOptions): JQuery;
    getOptionForKey(event: AnyKeyEvent, button?: NodeOrJQuery): JQuery;
    selectOption(value: VT): JQuery;
    selectOptionAt(index: number, options?: ListSelectOptionOptions): JQuery;
    selectOptionForKey(event: AnyKeyEvent, button?: NodeOrJQuery): boolean;
}
