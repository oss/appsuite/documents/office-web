/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, fun } from "@/io.ox/office/tk/algorithms";
import { createCaret, hasKeyCode } from "@/io.ox/office/tk/dom";
import { DEFAULT_CLICK_TYPE, createButtonNode, checkButtonNodes, getButtonValue, setButtonKeyHandler, setToolTip, touchAwareListener } from "@/io.ox/office/tk/forms";
import { openNewWindow } from "@/io.ox/office/tk/utils/tabutils";
import { Group } from "@/io.ox/office/tk/control/group";
import { CaptionControl } from "@/io.ox/office/tk/control/captioncontrol";

// class Button ===============================================================

/**
 * Creates a container element used to hold a push button or a toggle button.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the Group base class, and
 *  all formatting options for button control elements supported by the
 *  function `createButtonNode()`.
 *
 *  Additionally, the following options are supported:
 *  - {unknown} [config.value]
 *    A value, object, or function that will be used as fixed value for the
 *    button when it has been clicked. Must not be null. A function will be
 *    evaluated every time the button has been activated. If another control
 *    group (instance of class Group) has been passed, the current value of
 *    that group will be used and forwarded as button value instead. Will be
 *    ignored, if this button is a toggle button (see option "toggle" below,
 *    value is always a boolean value in this case).
 *  - {boolean} [config.toggle=false]
 *    If set to `true`, the button represents a boolean value and toggles its
 *    state when clicked.
 *  - {Function|boolean} [config.highlight]
 *    A predicate function that will be called every time after the value of
 *    the button has been set. The button will be highlighted if this handler
 *    function returns `true`. Receives the value of the button as first
 *    parameter. Will be called in the context of this button instance. Toggle
 *    buttons will highlight themselves automatically, if this option is
 *    omitted.
 */
export class Button extends CaptionControl {

    constructor(config) {

        // base constructor
        super({ skipStateAttr: !config?.toggle, ...config });

        // create the DOM button element
        this.$button = createButtonNode({ ...config, classes: config?.btnClasses });
        setToolTip(this.$button, config);

        // set the fixed value of push buttons (replace `Group` instances by a
        // anonymous function that returns the current group value)
        if (config?.value instanceof Group) {
            this.setButtonValue(this.$button, () => config.value.getValue());
        } else if (config?.value !== undefined) {
            this.setButtonValue(this.$button, config.value);
        }

        // callback function for highlighting the button node
        this._highlightFn = is.boolean(config?.highlight) ? fun.const(config.highlight) : config?.highlight;

        // show a caret icon
        if (config?.caret) {
            this.$button.addClass("caret-button").append(createCaret("down"));
        }

        // insert the button into this group
        this.$el.append(this.$button);

        // special handling for hyperlink buttons
        const href = this.$button.attr("href");
        if (href) {

            this.$button.on("keyup", event => {
                if (hasKeyCode(event, "ENTER", "SPACE")) {
                    openNewWindow(href);
                }
            });

        } else {

            // convert ENTER and SPACE keys to click events
            setButtonKeyHandler(this.$button);

            // button click handler
            touchAwareListener(this.$button, event => {
                const value = config?.toggle ? !this.getValue() : getButtonValue(this.$button);
                this.triggerCommit(value, { sourceEvent: event });
            });

            // handler for "remote" events triggered at the group root node
            this.$el.on("remote", (_event, command) => {
                if (command === "click") {
                    this.$button.trigger(DEFAULT_CLICK_TYPE);
                }
            });
        }
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);
        if (this._highlightFn) {
            checkButtonNodes(this.$button, this._highlightFn(value), { tristate: true, haspopup: this.config.haspopup });
        } else if (this.config.toggle) {
            checkButtonNodes(this.$button, value, { tristate: true, haspopup: this.config.haspopup });
        }
    }
}
