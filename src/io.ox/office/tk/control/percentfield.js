/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { PercentValidator } from "@/io.ox/office/tk/form";
import { SpinField } from "@/io.ox/office/tk/control/spinfield";

// class PercentField =====================================================

/**
 * A specialized spin field used to represent and input a percentage value.
 *
 * @param {Object} [config]
 *  Initial configuration options. Supports all options of the base class
 *  `SpinField`. If the option `validator` exists, it MUST be an instance
 *  of the class `PercentValidator` (or a subclass). Alternatively, the
 *  options supported by the constructor of the class `PercentValidator`
 *  can be passed. In this case, a new instance of `PercentValidator` will
 *  be created automatically based on these options.
 */
export class PercentField extends SpinField {

    constructor(config) {

        // base constructor
        super({
            ...config,
            // bug 31765: force keyboard type to "text", Android does not like characters in numeric text fields
            keyboard: "text",
            validator: config?.validator || new PercentValidator(config),
            smallStep: 1,
            largeStep: 5,
            roundStep: true
        });

        // add marker class for styling
        this.$el.addClass("percent-field");
    }
}
