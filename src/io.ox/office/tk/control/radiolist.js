/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, str } from "@/io.ox/office/tk/algorithms";
import {
    BUTTON_SELECTOR, checkButtonNodes, createButtonNode,
    getButtonValue, getButtonOptions, getCaptionText, isCheckedButtonNode, setButtonKeyHandler,
    createCaptionNode, setToolTip, touchAwareListener
} from "@/io.ox/office/tk/forms";
import { CaptionControl } from "@/io.ox/office/tk/control/captioncontrol";
import { XListControl } from "@/io.ox/office/tk/control/xlistcontrol";

// private functions ==========================================================

function parseUpdateCaptionMode(config) {
    const updateMode = config?.updateCaptionMode ?? "all";
    switch (updateMode) {
        case "all":  return ["icon", "iconStyle", "label", "labelStyle"];
        case "none": return [];
        default:     return str.splitTokens(updateMode);
    }
}

// class RadioList ============================================================

/**
 * Creates a dropdown list control used to hold a set of radio buttons.
 *
 * @param {object} [config]
 *  Optional parameters. Supports all options of the base class `Group`, of the
 *  mixin class `XListControl`, the class `ListMenu` used for the dropdown
 *  menu, and all formatting options for button elements supported by the
 *  function `createButtonNode()`.
 *
 *  Additionally, the following options are supported:
 *  - {boolean|Function} [config.highlight=false]
 *    If set to `true`, the dropdown button will be highlighted, if an existing
 *    option button in the dropdown menu is active. The dropdown button will
 *    not be highlighted, if the value of this group is undetermined (value
 *    `null`), or if there is no option button present with the current value
 *    of this group. If set to `false`, the dropdown button will never be
 *    highlighted, even if an option button in the dropdown menu is active. If
 *    set to a function, it will be called every time after an option button
 *    will be activated. The dropdown button will be highlighted if this
 *    handler function returns `true`. Receives the value of the selected or
 *    activated option button as first parameter (also if this value does not
 *    correspond to any existing option button). Will be called in the context
 *    of this instance.
 *  - {unknown} [config.toggleValue]
 *    If set to a non-nullish value, the option button that is currently active
 *    can be clicked to be switched off. In that case, this radio group will
 *    activate the button associated to the value specified in this option, and
 *    the action handler will return this value instead of the value of the
 *    button that has been switched off.
 *  - {unknown} [config.splitValue]
 *    If set to a non-nullish value, a separate push button will be inserted
 *    before the dropdown button, representing the value of this option.
 *  - {boolean} [config.updateSplitValue=false]
 *    If set to `true`, the value of the split button (intialized with the
 *    option "splitValue") will change automatically when the user has selected
 *    an option from the dropdown list. By default, the split value remains
 *    constant.
 *  - {string} [config.updateCaptionMode="all"]
 *    Specifies how to update the caption of the dropdown button when an option
 *    button in the dropdown menu has been activated. Supports the keywords
 *    "all" and "none", or a space separated list containing the string tokens
 *    "icon", "iconStyle", "label", and "labelStyle".
 *    - "icon": copies the icon of the option button to the dropdown button.
 *    - "iconStyle": copies the icon CSS formatting of the option button to the
 *      dropdown button.
 *    - "label": copies the label of the option button to the dropdown button.
 *    - "labelStyle": copies the label CSS formatting of the option button to
 *      the label of the dropdown button.
 *    The keyword "all" is equivalent to the string "icon iconStyle label
 *    labelStyle". The keyword "none" is equivalent to the empty string.
 */
export class RadioList extends XListControl.mixin(CaptionControl) {

    constructor(config) {

        // separate split button in split mode
        const $splitButton = is.nullish(config?.splitValue) ? undefined : createButtonNode(config);
        // the button containing the caret for the dropdown menu
        const menuButtonOptions = $splitButton ? { tooltip: config?.tooltip || "" } : config;
        const $menuButton = createButtonNode(menuButtonOptions);

        // base constructor
        super($menuButton, config);

        // private properties
        this._$splitButton = $splitButton;

        // initialize the split button (before the dropdown menu)
        if ($splitButton) {

            // insert split button before `$menuButton` which is the dropdown caret button now
            setToolTip($splitButton, config);
            this.setButtonValue($splitButton, config?.splitValue);
            this.$el.append($splitButton);

            // trigger commit event on split button clicks
            touchAwareListener($splitButton, event => {
                this.triggerCommit(this.getSplitValue(), { sourceEvent: event });
            });

            // convert ENTER and SPACE keys to click events
            setButtonKeyHandler($splitButton);

            // automatically update the value of the split button
            if (config?.updateSplitValue) {
                this.on("group:commit", newSplitValue => this.setSplitValue(newSplitValue));
            }
        }

        // add the dropdown button to the group
        setToolTip($menuButton, menuButtonOptions);
        this.$el.append($menuButton);

        // CSS marker class
        this.$el.addClass("radio-list");

        // handle click/touch events in the list
        touchAwareListener(this.menu.$el, BUTTON_SELECTOR, event => {
            const toggleValue = config?.toggleValue;
            const toggleClick = !is.nullish(toggleValue) && isCheckedButtonNode(event.currentTarget);
            const value = toggleClick ? toggleValue : getButtonValue(event.currentTarget);
            const buttonOptions = getButtonOptions(event.currentTarget);
            if (buttonOptions.customHandler) {
                this.triggerCancel({ sourceEvent: event });
                buttonOptions.customHandler(value);
            } else {
                this.triggerCommit(value, { sourceEvent: event });
            }
        });

        // scroll to the selected option button when displaying the dropdown menu
        this.listenTo(this.menu, "popup:show", () => {
            const button = this.menu.getSelectedOptions()[0];
            if (button && this.menu.isVisible()) {
                this.menu.scrollToChildNode(button);
            }
        });
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this control has a separate split button.
     *
     * @returns {boolean}
     *  Whether this control has a separate split button.
     */
    isSplitMode() {
        return !!this._$splitButton;
    }

    /**
     * Returns the current value of the split button.
     *
     * @returns {unknown}
     *  The current value of the split button; or `undefined`, if this control
     *  does not contain a split button.
     */
    getSplitValue() {
        return this._$splitButton ? getButtonValue(this._$splitButton) : undefined;
    }

    /**
     * Changes the value of the split button, and repaints it.
     *
     * @param {unknown} value
     *  The new value for the split button.
     */
    setSplitValue(value) {
        if (this._$splitButton) {
            this.setButtonValue(this._$splitButton, value);
            this.refresh({ immediate: true });
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Activates an option button in this radio group.
     */
    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);

        // refresh the visibility of the option buttons
        this.menu.refresh({ immediate: true });

        // activate the matching option buttons in the list (always with value, also in automatic split mode)
        const $selectedButtons = this.menu.selectOption(value);
        // automatic split mode: use current split value for drop-down button appearance
        const updateSplitValue = this._$splitButton && this.config.updateSplitValue;
        const $activeButtons = updateSplitValue ? this.menu.findOptions(this.getSplitValue()) : $selectedButtons;

        // highlight the dropdown menu button (call custom handler, if available)
        let isHighlighted;
        if (value === null) {
            isHighlighted = false;
        } else if (is.function(this.config.highlight)) {
            isHighlighted = this.config.highlight.call(this, value);
        } else {
            isHighlighted = !!this.config.highlight && ($activeButtons.length > 0);
        }
        checkButtonNodes(this.$el.children(".btn"), isHighlighted);

        // update the caption of the dropdown menu button
        const updateModes = parseUpdateCaptionMode(this.config);
        if (updateModes.length > 0) {
            const captionOptions = { ...this.config };
            // caption will switch back to default configuration, if no active button is available
            if ($activeButtons.length) {
                const buttonOptions = getButtonOptions($activeButtons);
                updateModes.forEach(updateMode => {
                    if (updateMode === "label") {
                        captionOptions.label = getCaptionText($activeButtons[0]);
                    } else if (updateMode in buttonOptions) {
                        captionOptions[updateMode] = buttonOptions[updateMode];
                    }
                });
            }
            this.$caption.replaceWith(createCaptionNode(captionOptions));
        }
    }
}
