/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { createCaptionNode, setToolTip } from "@/io.ox/office/tk/forms";
import { CaptionControl } from "@/io.ox/office/tk/control/captioncontrol";

// class Label ================================================================

/**
 * Creates a label control listening to update requests.
 *
 * By registering an update handler that modifies the caption it is even
 * possible to update the label dynamically with the method `Group#setValue`
 * based on any arbitrary value.
 *
 * @param {object} [config]
 *  Configuration options.
 */
export class Label extends CaptionControl {

    constructor(config) {

        // base constructor
        super(config);

        this.$el.addClass("label-group");
        setToolTip(this.$el, config);

        // insert the caption node into this group
        this.$el.append(createCaptionNode(config));
    }
}
