/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is, ary, set, dict } from '@/io.ox/office/tk/algorithms';
import { globalEvents } from '@/io.ox/office/tk/events';
import { getCeilNodeSize, setElementClasses, createDiv, convertCssLength, setFocusInto, focusLogger, hasKeyCode, serializeControlValue } from '@/io.ox/office/tk/dom';
import { controlLogger } from '@/io.ox/office/tk/form';
import { BUTTON_SELECTOR, FOCUSED_CLASS, enableNodes, setButtonValue, showNodes } from '@/io.ox/office/tk/forms';
import { EObject, debounceMethod } from '@/io.ox/office/tk/objects';

import '@/io.ox/office/tk/control/style.less';

// globals ====================================================================

// all existing groups and their focusable DOM nodes (stored globally to be able
// to use a single focus handler instead of one listener per group instance)
const focusableNodesMap = {};

// the unique IDs of the group instances currently focused (as stack, groups may be embedded)
const focusStack = [];

// global focus handling ======================================================

function registerFocusGroup(group) {
    focusableNodesMap[group.uid] = { group, nodes: group.$el };
}

function registerFocusableNodes(group, nodes) {
    var entry = focusableNodesMap[group.uid];
    entry.nodes = entry.nodes.add(nodes);
}

function unregisterFocusGroup(group) {
    delete focusableNodesMap[group.uid];
    ary.deleteFirst(focusStack, group.uid);
}

globalEvents.on("change:focus", focusNode => {

    // update CSS focus class, trigger event
    function changeFocusState(group, focused) {
        const type = focused ? 'group:focus' : 'group:blur';
        group.$el.toggleClass(FOCUSED_CLASS, focused);
        group.trigger(type);
    }

    // returns whether the focus node is inside one of the focusable nodes
    function containsFocusNode(uid) {
        const focusableNodes = focusableNodesMap[uid].nodes;
        return (focusableNodes.filter(focusNode).length > 0) || (focusableNodes.has(focusNode).length > 0);
    }

    // remove all groups from the stack that are not focused anymore
    while ((focusStack.length > 0) && (!focusNode || !containsFocusNode(_.last(focusStack)))) {
        const uid = focusStack.pop();
        // check that group has not been destroyed already
        if (uid in focusableNodesMap) {
            changeFocusState(focusableNodesMap[uid].group, false);
        }
    }

    // push all groups onto the stack that contain the focus node (and are not yet in the stack)
    if (focusNode) {
        dict.forEachKey(focusableNodesMap, uid => {
            if (!focusStack.includes(uid) && containsFocusNode(uid)) {
                focusStack.push(uid);
                changeFocusState(focusableNodesMap[uid].group, true);
            }
        });
    }
});

// class Group ================================================================

/**
 * Creates a container element used to hold a control. All controls shown in
 * toolbars must be inserted into such group containers. This is the base class
 * for specialized groups and does not add any specific functionality to the
 * inserted controls.
 */
export class Group extends EObject {

    /**
     * @param {GroupConfig} [config]
     *  Configuration options.
     */
    constructor(config) {

        // base constructor
        super();

        // create the group container element
        this.el = createDiv("docs-control group");
        this.$el = $(this.el);

        // the configuration options passed to the constructor
        this.config = { ...config };

        // a set with all hidden flags
        this._hidden = new Set/*<string>*/();
        // a set with all disabled flags
        this._disabled = new Set/*<string>*/();
        // the current value of this control group (must be undefined initially, NOT null)
        this._value = undefined;

        // the last cached size of the root node, used to detect changes
        this._size = null;
        // whether the control is in "edit mode" (update handlers will not be called)
        this._editMode = false;
        // whether the control is in "shrunken mode" (reduced contents for small displays)
        this._shrinkMode = false;

        // classes and ARIA role
        setElementClasses(this.el, config);
        this.$el.attr('role', config?.role || null);

        // set the initial width of the control root element
        if (config?.width) {
            this.$el.css("width", config.width);
        }

        // initialize visibility
        if (config?.hidden) { this.hide(); }
        if (config?.shrinkVisible) { this.hide("control:shrink"); }

        // register for global focus handling
        registerFocusGroup(this);

        // hide groups only shown in shrunken mode
        this.toggleDropDownMode(false);

        // log focus and layout events
        this.logEvents(focusLogger, ["group:focus", "group:blur"]);
        this.logEvents(controlLogger, ["group:beforeshow", "group:show", "group:beforehide", "group:hide"]);
        this.logEvents(controlLogger, "group:resize", size => `width=${size.width} height=${size.height}`);
    }

    destructor() {
        unregisterFocusGroup(this);
        this.$el.remove();
        super.destructor();
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the current value of this form control.
     */
    get value() {
        return this._value;
    }

    /**
     * Returns whether this form control is currently enabled.
     */
    get enabled() {
        return !this._disabled.size;
    }

    /**
     * Returns whether this form control is currently in "visible" state. Does
     * not tell anything about the effective rendering visibility (i.e. whether
     * the form control is attached to the DOM, and all parant elements are
     * visible).
     */
    get visible() {
        return !this._hidden.size;
    }

    /**
     * Returns whether this form control contains the browser focus.
     */
    get focused() {
        return this.hasFocus();
    }

    // public methods ---------------------------------------------------------

    render() {
        return this.el;
    }

    /**
     * Requests a refresh of this form control, updating its appearance by
     * invoking the method `implUpdate` with the current control value.
     *
     * @param {RefreshOptions} [options]
     *  Optional parameters.
     */
    refresh(options) {
        this._pendingRefresh = true;
        if (options?.immediate) {
            this._execRefresh();
        } else {
            this._debouncedRefresh();
        }
    }

    /**
     * Returns whether this group is currently in focused state, either because
     * the browser focus is located inside the group itself, or it is located
     * in any of the additional DOM nodes that have been registered with the
     * method `Group#registerFocusableNodes`.
     *
     * @returns {boolean}
     *  Whether the group currently owns the browser focus.
     */
    hasFocus() {
        return focusStack.includes(this.uid);
    }

    /**
     * Sets the focus to the first form control element in this instance,
     * unless it is already focused.
     */
    grabFocus() {
        if (!this.hasFocus()) { setFocusInto(this.$el); }
    }

    /**
     * Returns whether this control group is configured to be visible via the
     * methods `Group#show`, `Group#hide`, or `Group#toggle`. Does not tell
     * anything about the effective rendering visibility (i.e. whether the
     * control is attached to the DOM, and all parant elements are visible).
     */
    isVisible() {
        return !this._hidden.size;
    }

    /**
     * Displays this form control, if it is currently hidden.
     *
     * @param {string} [key="default"]
     *  A key that can be used to combine different independent visibility
     *  flags for this form control. The control will become visible if and
     *  only if all known visibility flags are set.
     */
    show(key) {
        this.toggle(true, key);
    }

    /**
     * Hides this form control, if it is currently visible.
     *
     * @param {string} [key="default"]
     *  A key that can be used to combine different independent visibility
     *  flags for this form control. The control will become visible if and
     *  only if all known visibility flags are set.
     */
    hide(key) {
        this.toggle(false, key);
    }

    /**
     * Toggles the visibility of this form control.
     *
     * @param {boolean} [state]
     *  If specified, shows or hides the form control depending on the boolean
     *  value. If omitted, toggles the current visibility of the form control.
     *
     * @param {string} [key="default"]
     *  A key that can be used to combine different independent visibility
     *  flags for this form control. The control will become visible if and
     *  only if all known visibility flags are set.
     */
    toggle(state, key) {

        // toggle the hidden flag (fallback to default key)
        const oldVisible = this.isVisible();
        set.toggle(this._hidden, key || "default", is.boolean(state) ? !state : undefined);

        // update the DOM visibility
        const newVisible = this.isVisible();
        if (oldVisible !== newVisible) {
            this.trigger(newVisible ? "group:beforeshow" : "group:beforehide");
            showNodes(this.$el, newVisible);
            this.trigger(newVisible ? "group:show" : "group:hide");
            this._updateNodeSize();
        }
    }

    /**
     * Returns whether this control group is enabled.
     */
    isEnabled() {
        return !this._disabled.size;
    }

    /**
     * Enables or disables this control group.
     *
     * @param {boolean} state
     *  Whether to enable (`true`) or disable (`false`) this form control.
     *
     * @param {string} [key="default"]
     *  A slot key that can be used to combine different independent enabled
     *  flags for this form control. The control will become enabled if and
     *  only if all known enabled flags are set.
     *
     * @returns {this}
     *  A reference to this instance.
     */
    enable(state, key) {

        // toggle the disabled flag (fallback to default key)
        const oldEnabled = this.isEnabled();
        set.toggle(this._disabled, key || "default", !state);

        // enable/disable the entire group node with all its descendants
        const newEnabled = this.isEnabled();
        if (oldEnabled !== newEnabled) {
            enableNodes(this.$el, newEnabled);
            this.$el.find(BUTTON_SELECTOR).attr('aria-disabled', newEnabled ? null : true);
            this.trigger('group:enable', newEnabled);
        }
        return this;
    }

    /**
     * Returns the current value of this control group.
     *
     * @returns {unknown}
     *  The current value of this control group.
     */
    getValue() {
        return this._value;
    }

    /**
     * Updates the controls in this group with the specified value, by calling
     * the registered update handlers.
     *
     * @param {unknown} value
     *  The new value to be displayed in the controls of this group.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.ignoreEditMode=false]
     *    If set to `true`, the update handlers will be invoked during edit
     *    mode of this control. By default, a form control will not refresh
     *    itself during edit mode.
     *
     * @returns {this}
     *  A reference to this instance.
     */
    setValue(value, options) {
        this._updateValue(value, options);
        return this;
    }

    /**
     * Sets a change handler callback function for the value of this control,
     * and optionally invokes it immediately.
     *
     * @param {ControlChangeHandlerFn<VT>} handlerFn
     *  The callback function to be invoked when the control value changes.
     *
     * @param {ControlChangeOptions} [options]
     *  Optional parameters.
     */
    onChange(handlerFn, options) {
        this.on("group:change", value => handlerFn.call(this, value, undefined));
        if (options?.invokeNow) { handlerFn.call(this, this._value, undefined); }
    }

    /**
     * Triggers a "group:commit" event at this instance, and inserts special
     * focus options into the event object. If the control group is disabled, a
     * "group:cancel" event will be triggered instead.
     *
     * @param {unknown} value
     *  The new value for this group to be set and notified.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {JEvent} [options.sourceEvent]
     *    The source event that has caused the new event. This source event may
     *    influence the focus behavior, (e.g.: browser focus is kept on a
     *    button, if the SPACE key has been used).
     *  - {string} [options.sourceType]
     *    The value of the property `sourceType` to be set in the commit event.
     *    If omitted, the source type will be determined by the type of the
     *    `sourceEvent`.
     *  - {HTMLElement|jQuery|Object|Function} [options.focusTarget]
     *    A DOM node that will receive the browser focus after this group has
     *    been activated, an object that provides a `grabFocus()` method, or a
     *    callback function that returns a focus target dynamically. Default
     *    focus target is the document view.
     *  - {boolean} [options.preserveFocus]
     *    If specified, forces to preserve the browser focus regardless of the
     *    passed source event.
     */
    triggerCommit(value, options) {
        controlLogger.trace('$badge{Group} triggerCommit: value=', value);
        if ((value !== null) && this.isEnabled()) {
            this.setValue(value);
            this.trigger('group:commit', value, this._getEventOptions(options));
        } else {
            this.triggerCancel(options);
        }
    }

    /**
     * Triggers a "group:cancel" event at this instance, and inserts special
     * focus options into the event object, according to the passed source
     * event.
     *
     * @param {object} [options]
     *  Optional parameters. See method `Group#triggerCommit` for details.
     */
    triggerCancel(options) {
        controlLogger.trace('$badge{Group} triggerCancel');
        this.trigger('group:cancel', this._getEventOptions(options));
    }

    /**
     * Returns whether this controls supports flexible width.
     *
     * @returns {boolean}
     *  Whether this controls supports flexible width.
     */
    hasFlexWidth() {
        return !!(this.config.width && this.config.shrinkWidth);
    }

    /**
     * Changes the width of this control, depending on the flexible width set
     * with the configuration options "width" (maximum width) and "shrinkWidth"
     * (minimum width).
     *
     * @param {number} relWidth
     *  The relative width of this control, as number in the interval `[0,1]`.
     */
    setFlexWidth(relWidth) {
        const { width, shrinkWidth } = this.config;
        if (width && shrinkWidth) {
            const maxWidth = convertCssLength(width, "px");
            const minWidth = convertCssLength(shrinkWidth, "px");
            const newWidth = (maxWidth - minWidth) * relWidth + minWidth;
            this.el.style.width = `${Math.floor(newWidth)}px`;
        }
    }

    /**
     * Toggles the "reduced contents" mode of this form control. This mode will
     * be controlled by parent containers such as toolbars to fit multiple form
     * controls into reduced available space, e.g. on small devices.
     *
     * @param {boolean} shrink
     *  Whether to enable or disable auto-shrink mode.
     */
    toggleShrinkMode(shrink) {
        if (this._shrinkMode !== shrink) {
            this._shrinkMode = shrink;
            this.implToggleShrinkMode(shrink);
        }
    }

    /**
     * Shows or hides this control group according to the passed state and the
     * dropdown configuration passed to the constructor.
     *
     * @param {boolean} shrink
     *  Whether the container is in shrunken mode. This control group will only
     *  be updated, if the dropdown configuration passed to the constructor
     *  contains a 'visible' option. The control will be shown, if the flag
     *  'shrink' equals the 'visible' option, otherwise it will be hidden.
     */
    toggleDropDownMode(shrink) {
        const ddOptions = this.config.dropDownVersion;
        if (is.boolean(ddOptions?.visible)) {
            this.toggle(shrink === ddOptions.visible);
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Registers one or more DOM nodes located outside the root node of this
     * instance, which contain other control nodes that will be included into
     * the focus handling and generation of "group:focus" and "group:blur"
     * events. While the browser focus is located inside the passed nodes, this
     * group will not trigger a "group:blur" event.
     *
     * @param {NodeOrJQuery} nodes
     *  The DOM node(s) to be added to the internal focus handling.
     */
    /*protected*/ registerFocusableNodes(nodes) {
        registerFocusableNodes(this, nodes);
    }

    /**
     * Starts the edit mode for this control. During edit mode, the method
     * `implUpdate` will not be called after a new value has been set to this
     * control with the method `Group#setValue` (e.g. from external GUI
     * updates). Thus, the visual state of the control (depending on the value,
     * e.g. the contents of a text field currently edited by the user) will not
     * be changed as long as edit mode is active.
     */
    /*protected*/ enterEditMode() {
        this._editMode = true;
    }

    /**
     * Leaves the edit mode of this control that has been started with the
     * method `Group#enterEditMode`.
     */
    /*protected*/ leaveEditMode() {
        this._editMode = false;
    }

    /**
     * Returns whether the edit mode of this control is currently active (see
     * method `Group#enterEditMode` for details).
     *
     * @returns {boolean}
     *  Whether the edit mode of this control is currently active.
     */
    /*protected*/ isEditMode() {
        return this._editMode;
    }

    /**
     * Stores the passed value in the "value" data attribute of the button
     * element, and creates a "data-value" attribute for automated testing.
     *
     * @param {HTMLElement|jQuery} buttonNode
     *  The button element. If this object is a jQuery collection, modifies all
     *  nodes it contains.
     *
     * @param {unknown} value
     *  A value, object, or function that will be copied to the "value" data
     *  attribute of the button element.
     */
    /*protected*/ setButtonValue(buttonNode, value) {
        setButtonValue(buttonNode, value, this.config.valueSerializer);
    }

    /**
     * Subclasses may implement updating the DOM structure according to the
     * current control value. Also used for example when changing the value
     * silently (without running external change listeners).
     *
     * @param {unknown} newValue
     *  The current value of the form control.
     */
    /*protected*/ implUpdate(/*newValue*/) {
        // nothing to do
    }

    /**
     * Subclasses may implement additional actions for toggling the shrink mode
     * of this form control. Shrunken mode will be controlled by parent
     * containers such as toolbars to fit multiple form controls into reduced
     * available space, e.g. on small devices.
     *
     * @param {boolean} shrink
     *  Whether to enable or disable shrink mode.
     */
    /*protected*/ implToggleShrinkMode(shrink) {

        // shortcuts to configuration
        const { shrinkVisible } = this.config;

        // toggle CSS class for styling
        this.$el.toggleClass("shrink-mode", shrink);

        // toggle visibility depending on shrink mode
        if (is.boolean(shrinkVisible)) {
            this.toggle(shrink === shrinkVisible, "control:shrink");
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Reads the current outer size of the root node of this group, and
     * triggers a "group:resize" event if the group is visible and its size has
     * changed. Does nothing if the group is currently hidden.
     */
    /*private*/ _updateNodeSize() {

        // do nothing if the group is not visible
        if (!this.isVisible()) { return; }

        // read current node size, notify listeners if size has changed
        const size = getCeilNodeSize(this.el);
        if (!this._size || (this._size.width !== size.width) || (this._size.height !== size.height)) {
            this._size = size;
            this.trigger("group:resize", size);
        }
    }

    /**
     * Updates the current value of this group, and invokes the registered
     * update handlers which will update the visual appearance of this group.
     */
    /*private*/ _updateValue(value, options) {

        // set new value
        const oldValue = this._value;
        const changed = !_.isEqual(oldValue, value);
        this._value = value;

        // update the "data-state" attribute of the root node used in automated tests
        // (DOCS-4080: also if value has not changed, state attribute may depend on other data)
        if (!this.config.skipStateAttr) {
            const serialized = serializeControlValue(value, this.config.valueSerializer);
            if (serialized && (serialized.length > 100)) {
                controlLogger.warn(() => [`$badge{${this.constructor.name}} updateValue: oversized serialized state (key="${this.el.dataset.key}"):`, serialized]);
            }
            this.$el.attr('data-state', serialized);
        }

        // invoke the update handlers which refresh the appearance of the group
        // (for now, also if the value does not change)
        if (!this._editMode || options?.ignoreEditMode) {
            this.implUpdate(value);
        }

        // trigger change event for external handlers
        if (changed && !options?.silent) {
            this.trigger("group:change", value, oldValue);
        }

        // size of the form control may have changed due to new value
        this._updateNodeSize();
    }

    /**
     * Refreshes this form control synchronously. This is the actual refresh
     * implementation that will be invoked from synchronous and asynchronous
     * refresh requests.
     */
    /*private*/ _execRefresh() {

        // nothing to do, if no refresh request is pending (anymore), or if popup has been hidden
        if (!this._pendingRefresh || !this.isVisible()) { return; }

        // run full update with the current control value
        this._updateValue(this._value);

        // start accepting more refresh requests
        this._pendingRefresh = false;
    }

    /**
     * Collects multiple refresh requests, and decides whether to actually
     * execute the refresh (depending on whether another synchronous refresh
     * was executed before).
     */
    @debounceMethod({ delay: "animationframe" })
    /*private*/ _debouncedRefresh() {
        this._execRefresh();
    }

    /**
     * Returns the focus options for the passed source event, intended to be
     * inserted into a "group:commit" or "group:cancel" event triggered by this
     * control group.
     *
     * @param {object} [options]
     *  Optional parameters. See method `Group#triggerCommit` for details.
     *
     * @returns {object}
     *  An options object with the following properties:
     *  - {string} sourceType -- An abstract type identifier for the source
     *    event: "click" for regular mouse click events; "keyboard" for
     *    keyboard events (including click events originating from SPACE or
     *    RETURN keys), otherwise the value of `options.sourceType` if
     *    available, or "custom" as default value.
     *  - {boolean} preserveFocus
     *    Whether the passed source event leads to preserving the current
     *    browser focus after performing the action associated to this control
     *    group (usually, after SPACE or TAB keys).
     */
    /*private*/ _getEventOptions(options) {

        // resulting properties for the event object
        const eventOptions = {
            sourceType: options?.sourceType || 'custom',
            preserveFocus: false
        };

        // do not explicitly add value `undefined` to "eventOptions"
        if (options?.focusTarget) {
            eventOptions.focusTarget = options.focusTarget;
        }

        // the source event object
        const sourceEvent = options?.sourceEvent;

        // special handling for specific events
        if (sourceEvent) {

            // use target of the passed source event as source node
            eventOptions.sourceEvent = sourceEvent;

            // handle specific event types
            switch (sourceEvent.type) {
                case 'tap':
                case 'click':
                    // Forms.setButtonKeyHandler() triggers 'click' events with 'keyCode' field
                    eventOptions.sourceType = is.number(sourceEvent.keyCode) ? 'keyboard' : 'click';
                    // click events from keyboard: keep focus on button after SPACE or TAB key (see Forms.setButtonKeyHandler() method)
                    eventOptions.preserveFocus = hasKeyCode(sourceEvent, 'SPACE', 'TAB');
                    break;
                case 'keydown':
                case 'keypress':
                case 'keyup':
                    eventOptions.sourceType = 'keyboard';
                    break;
            }
        }

        // override the option "preserveFocus"
        if (is.boolean(options?.preserveFocus)) {
            eventOptions.preserveFocus = options.preserveFocus;
        }

        return eventOptions;
    }
}
