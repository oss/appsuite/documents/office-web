/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { createSpan } from "@/io.ox/office/tk/dom";
import { controlLogger } from "@/io.ox/office/tk/form";
import { getCaptionNode, setCaptionIcon, getCaptionText, setCaptionText, setToolTip } from "@/io.ox/office/tk/forms";
import { Group } from "@/io.ox/office/tk/control/group";

// class CaptionControl =======================================================

/**
 * Generic base class for form controls with methods to manipulate a caption
 * (icon and/or label text) of the form control. Subclasses are expected to add
 * a caption node into the own DOM tree.
 */
export class CaptionControl extends Group {

    constructor(config) {
        super(config);

        // check labels for shrink mode
        if (config?.label && config?.shrinkLabel && (config.label.length <= config.shrinkLabel.length)) {
            controlLogger.warn(`$badge{CaptionControl} expected shrinkLabel="${config.shrinkLabel}" to be shorter than label="${config.label}"`);
        }

        // special behavior, if this control has been moved from toolbar into dropdown menu
        this.setTimeout(() => this._initDDCaption(), 0);
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the caption container node of the control.
     *
     * @type JQuery
     */
    get $caption() {
        return getCaptionNode(this.$el);
    }

    // public methods ---------------------------------------------------------

    /**
     * Changes the icon of the wrapped control element.
     *
     * @param {string|null} iconId
     *  The icon identifier. If set to `null` or an empty string, the current
     *  icon will be removed.
     *
     * @param {CreateIconOptions} [options]
     *  Optional parameters.
     */
    setIcon(iconId, options) {
        setCaptionIcon(this.$el, iconId, options);
    }

    /**
     * Returns the current label text of the wrapped control element.
     *
     * @returns {string}
     *  The current text label.
     */
    getLabel() {
        return getCaptionText(this.$el);
    }

    /**
     * Changes the label text of the wrapped control element.
     *
     * @param {string|null} text
     *  The new label text. If set to `null` or an empty string, the current
     *  label will be removed.
     *
     * @param {CreateElementLabelOptions} [options]
     *  Optional parameters.
     */
    setLabel(text, options) {
        setCaptionText(this.$el, text, options);
    }

    /**
     * Sets a tool tip text at the wrapped control element.
     *
     * @param {string} text
     *  The tool tip text to be set at the control.
     */
    setToolTip(text) {
        setToolTip(this.$el, text);
    }

    /**
     * Removes the tool tip text from the wrapped control element.
     */
    removeToolTip() {
        setToolTip(this.$el, "");
    }

    // protected methods ------------------------------------------------------

    /**
     * Additional processing on the caption label and icon for "reduced
     * contents" mode.
     */
    /*protected override*/ implToggleShrinkMode(shrink) {
        super.implToggleShrinkMode(shrink);

        // shortcuts to own properties
        const { icon, label, shrinkIcon, shrinkLabel } = this.config;

        // show another icon in shrunken mode
        if (shrinkIcon) {
            this.setIcon(shrink ? shrinkIcon : icon, this.config);
        }

        // show another label in shrunken mode, or hide label if an icon is available
        if (shrinkLabel) {
            this.setLabel(shrink ? shrinkLabel : label, this.config);
        } else {
            this.$el.toggleClass("show-icon-only", shrink);
        }

        // set label as tooltip in shrunken mode (shortened or hidden label)
        if ((shrinkIcon || icon) && label && !this.config.tooltip) {
            this.setToolTip(shrink ? label : "");
        }

        // disable fixed width in shrunken mode (not for flexible controls)
        if (this.config.width && !this.config.shrinkWidth) {
            this.$el.css({ width: shrink ? "" : this.config.width });
            this.$el.css({ maxWidth: shrink ? this.config.width : "" });
        }
    }

    // private methods --------------------------------------------------------

    /*private*/ _initDDCaption() {
        const ddConfig = this.config.dropDownVersion;
        if (ddConfig?.label) {
            this.$caption.append(createSpan({ classes: "drop-down-only", label: ddConfig.label }));
        }
        if (ddConfig?.tooltip) {
            setToolTip(this.$el, ddConfig.tooltip);
        }
    }
}
