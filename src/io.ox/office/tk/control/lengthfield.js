/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { LengthValidator } from "@/io.ox/office/tk/form";
import { SpinField } from "@/io.ox/office/tk/control/spinfield";

// class LengthField ==========================================================

/**
 * A specialized spin field used to represent and input a length value together
 * with a measurement unit. The internal value represented by the property
 * `fieldValue` is always in 1/100 mm.
 *
 * @param {Object} [config]
 *  Initial configuration options. Supports all options of the base class
 *  `SpinField`. If the option `validator` exists, it MUST be an instance
 *  of the class `LengthValidator` (or a subclass). Alternatively, the
 *  options supported by the constructor of the class `LengthValidator` can
 *  be passed. In this case, a new instance of `LengthValidator` will be
 *  created automatically based on these options.
 *
 *  Additionally, the following options are supported:
 *  - {LengthUnit} [config.unit]
 *    The length unit to be displayed in this spin field. By default, the
 *    standard length unit from the user configuration will be used.
 */
export class LengthField extends SpinField {

    constructor(config) {

        // the validator taking care of unit conversion and display strings
        const validator = config?.validator || new LengthValidator(config);

        // base constructor
        super({
            ...config,
            // bug 31765: force keyboard type to "text", Android does not like characters in numeric text fields
            keyboard: "text",
            validator,
            smallStep: validator.step,
            largeStep: 5 * validator.step,
            roundStep: true
        });

        // add marker class for styling
        this.$el.addClass("length-field");

        // update display unit when changed in user configuration
        this.listenToGlobal("change:locale", changedProps => {
            if (changedProps.unit) {
                // DOCS-2579: ignore edit mode when measurement unit has changed
                this.setValue(this.getValue(), { ignoreEditMode: true });
            }
        });
    }
}
