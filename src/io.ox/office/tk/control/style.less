/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";

/* ========================================================================= */
/* Variables                                                                 */
/* ========================================================================= */

// settings for generic control layout
// - can be overwritten in more specific selectors
// - outside of ".group" selector for common usage
.io-ox-office-main {
    --control-height: var(--std-control-height);
    --control-padding: var(--std-control-padding);
    --control-distance: var(--std-control-distance);
    --control-border-radius: var(--std-control-border-radius);
}

/* ========================================================================= */
/* Form Controls                                                             */
/* ========================================================================= */

.io-ox-office-main .group {
    display: flex;
    position: relative;
    box-sizing: border-box;
    height: var(--control-height);
    border-radius: var(--control-border-radius);
    font-size: var(--std-font-size);
    color: var(--text);
    white-space: nowrap;

    // common control settings ================================================

    // disabled groups
    &.disabled {
        opacity: 0.5;
    }

    // disabled groups ("important" to overrule styles from other rules)
    &.disabled, &.disabled *, .disabled {
        cursor: default !important;
        pointer-events: none;
    }

    // dynamic shrink mode for small devices
    &.shrink-mode {
        min-width: 0 !important;

        // hide label text if an icon is available
        &.show-icon-only .caption > [data-icon-id] ~ span {
            display: none;
        }
    }

    // common settings for button elements ====================================

    a.btn {
        display: flex;
        position: relative;
        flex-grow: 1;
        padding: 0 var(--control-padding);
        border: 1px solid var(--border);
        border-radius: 0;
        overflow: hidden;
        color: var(--text);

        // background color for selected buttons */
        &.selected {
            background: var(--control-selected-fill-color);
            color: var(--control-selected-text-color);
        }

        // generic additions for caret symbols
        > .caret-node {
            display: flex;
            flex: 0 0 auto;
            opacity: 0.5;

            // the caret icon element
            > [data-icon-id] {
                width: var(--caret-width);
                height: var(--caret-width);
                align-self: center;
            }
        }

        // caret symbol next to caption element
        .caption:not(:empty) + .caret-node {
            margin-left: var(--caption-spacing);
        }

        // do not stretch caret button in popup menus
        &.caret-button:not(:first-child) {
            flex: 0 0 auto;
            border-left: none;
        }

        // focus effect for buttons (but not checkboxes or radio buttons with
        // custom boxed checkmark which have their own focus around checkmark)
        &:not(.custom-checkmark.boxed-mode) {
            .focus-border-shadow({
                // raise focused control element to make all borders visible
                z-index: 2;
            }) !important;
        }
    }

    // hover effects for enabled button elements
    :root:not(.touch) &:not(.disabled) .btn:not(.disabled) {
        .hover-overlay(@custom-rules: {
            // display drop-down caret with full opacity
            > .caret-node { opacity: 1; }
        });
    }

    // border radius for first direct button child
    > a.btn:first-child {
        .border-left-radius(var(--control-border-radius));
    }

    // border radius for last direct button child
    > a.btn:last-child {
        .border-right-radius(var(--control-border-radius));
    }

    > a.btn.option-button {
        border-radius: var(--control-border-radius);
    }

    // background color for caret buttons with open dropdown menu
    &.dropdown-open a.btn:not(.selected) {
        .dropdown-open-colors();
        .caret-node { opacity: 1; }
    }

    // class Label ============================================================

    &.label-group {
        padding: 0 var(--control-padding);
    }

    // class CheckGroup =======================================================

    &.check-group > .btn.select-all {
        font-style: italic;
    }

    // class TextField ========================================================

    &.text-field {
        display: flex;
        height: calc(var(--control-height) - 4px);
        border-radius: var(--control-border-radius);

        > .input-wrapper {

            display: flex;
            flex-grow: 1;
            box-sizing: border-box;
            background: var(--text-field-background);
            border: 1px solid var(--border);
            cursor: text;

            // remove some Core/Bootstrap stylings from <input> element
            > input {
                flex-grow: 1;
                padding: 0 var(--control-padding);
                background: none;
                border: none;
                box-shadow: none;
                line-height: normal;
                text-overflow: ellipsis;

                &:focus, &:focus-within {
                    box-shadow: none;
                }
            }

            // small icon before the <input> element
            > [data-icon-id]:first-child {
                align-self: center;
                margin-left: 3px;
                pointer-events: none;
                opacity: 0.5;
            }

            // info badge after <input> element
            > .info-badge {
                align-self: center;
                height: 15px;
                padding: 0 4px;
                background: var(--accent);
                border-radius: 7px;
                line-height: 14px;
                font-size: 10px;
                font-weight: bold;
                color: white;
                opacity: 0.75;
                pointer-events: none;

                &:empty {
                    display: none;
                }
            }

            // small X button at the end to clear the text field
            > .clear-button {
                align-self: center;
                box-sizing: content-box;
                padding: 3px;
                cursor: pointer;
                pointer-events: inherit;

                &:not(:hover, :active) {
                    opacity: 0.5;
                }
            }
        }

        // hide "Clear" button in disabled controls with text contents
        &.disabled > .input-wrapper > .clear-button {
            display: none;
        }

        // left border radius for leading element
        > :first-child {
            .border-left-radius(var(--control-border-radius));
        }

        // right border radius for trailing element
        > :last-child {
            .border-right-radius(var(--control-border-radius));
        }

        // focus border on all additional buttons (spin buttons, caret button)
        &:focus-within {
            box-shadow: var(--focus-box-shadow);
            > .input-wrapper, .btn {
                border-color: var(--focus-border-color) !important;
            }
        }
    }

    // class SpinField ========================================================

    &.spin-field {
        width: 5.5em;

        // additional settings for the <input> element
        input {
            text-align: right;
        }

        // container for the up/down spin buttons
        > .spin-wrapper {
            display: flex;
            flex-direction: column;
            background: var(--text-field-background);

            > .spin-button {
                height: 50%;
                border-left: none;
                padding: 0 var(--control-padding);

                // do not fade out the border of a disabled spin button
                &[disabled] {
                    filter: none;
                    opacity: 1;
                    > .caret-node { opacity: 0.25; }
                }

                &.spin-up {
                    border-bottom: none;
                    border-radius: 0 var(--control-border-radius) 0 0;
                }

                &.spin-down {
                    border-radius: 0 0 var(--control-border-radius) 0;
                }
            }
        }
    }

    // class LengthField ======================================================

    &.spin-field.length-field {
        width: 7em;
    }

    // class PercentField =====================================================

    &.spin-field.percent-field {
        width: 6em;
    }

    // class ComboField =======================================================

    &.combo-field {

        // caret button with white background as <input> field
        > .caret-button {
            flex-shrink: 0;
            background: var(--text-field-background);
        }
    }
}

/* ========================================================================= */
/* Borderless Buttons                                                        */
/* ========================================================================= */

.io-ox-office-main.borderless-buttons, .io-ox-office-main .borderless-buttons {

    a.btn:not(.boxed) {
        border-color: transparent;
        &:hover {
            border-color: var(--hover-overlay-color);
        }
    }
}
