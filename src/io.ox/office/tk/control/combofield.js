/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is, str } from "@/io.ox/office/tk/algorithms";
import { TOUCH_DEVICE, getInputSelection, setInputSelection, KeyCode, matchKeyCode } from "@/io.ox/office/tk/dom";
import { BUTTON_SELECTOR, createButtonNode, enableToolTip, getButtonValue, touchAwareListener } from "@/io.ox/office/tk/forms";
import { TextField } from "@/io.ox/office/tk/control/textfield";
import { XListControl } from "@/io.ox/office/tk/control/xlistcontrol";

// class ComboField ===========================================================

/**
 * Creates a text field control with attached dropdown list showing predefined
 * values for the text field.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the base class `TextField`,
 *  and the `ListMenu` class constructor used for the dropdown menu element.
 *
 *  Additionally, the following options are supported:
 *  - {boolean} [config.typeAhead=false]
 *    If set to `true`, the label of the first option button that starts with
 *    the text currently edited will be inserted into the text field. The
 *    remaining text appended to the current text will be selected.
 */
export class ComboField extends XListControl.mixin(TextField) {

    constructor(config) {

        // the dropdown button that will be added next to the <input> field
        const $menuButton = createButtonNode({ classes: "boxed", focusable: false });

        // base constructor
        super($menuButton, { ...config, ariaOwner: "input", listLayout: "list" });

        // add special marker class used to adjust formatting
        this.$el.addClass("combo-field").attr({ role: "form" });

        // attributes for the input element
        this.$input.attr({ role: "combobox", "aria-autocomplete": "inline" });

        // inser the menu button into this control
        this.$el.append($menuButton);

        // register event handlers
        this.on("group:validate", this._groupValidateHandler);
        this.listenTo(this.$input, "keydown", this._inputKeyDownHandler);

        // handle click/touch events in the dropdown list
        touchAwareListener(this.menu.$el, BUTTON_SELECTOR, event => {
            this.triggerCommit(getButtonValue(event.currentTarget), { sourceEvent: event });
        });

        // register pop-up event handlers
        this.listenTo(this.menu, "popup:show", this._popupShowHandler);
        this.listenTo(this.menu, "popup:hide", this._popupHideHandler);
        this.listenTo(this.menu, "menu:afterrepaint", () => this._selectOption(this.fieldValue));

        // register input control for focus handling (to keep menu visible)
        this.menu.registerFocusableNodes(this.$input);

        // keep focus in text field when clicking in the dropdown menu
        this.listenTo(this.menu.$el, "focusin", this._returnFocusToTextField);
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);
        this._selectOption(value);
    }

    // private methods --------------------------------------------------------

    /**
     * Returns browser focus to the text field element.
     */
    /*private*/ _returnFocusToTextField() {
        // not on touch devices to prevent flickering keyboard etc.
        if (!TOUCH_DEVICE) {
            // deferred to not confuse focus listeners due to mixed focus events triggered from mouse click and focus() call
            // DOCS-3214: check that focus is still inside the pop-up menu (and not already back in application area)
            this.setTimeout(() => this.menu.hasFocus() && this.grabFocus(), 0);
        }
    }

    /**
     * Scrolls the dropdown menu to make the specified option button visible.
     */
    /*private*/ _scrollToOption($button) {
        if (($button.length > 0) && this.menu.isVisible()) {
            this.menu.scrollToChildNode($button);
        }
    }

    /**
     * Handles "popup:show" events, moves the focus to the text field and
     * disables the tooltip.
     */
    /*private*/ _popupShowHandler() {
        enableToolTip(this.$el, false);
        this._returnFocusToTextField();
        this._scrollToOption(this.menu.getSelectedOptions());
    }

    /**
     * Handles "popup:hide" events and enables the tooltip.
     */
    /*private*/ _popupHideHandler() {
        enableToolTip(this.$el, true);
    }

    /**
     * Activates an option button if it matches the passed value.
     */
    /*private*/ _selectOption(value) {
        // activate an option button
        const $button = this.menu.selectOption(value);
        // scroll to make the element visible
        this._scrollToOption($button);
    }

    /**
     * Handles keyboard events in the input control. Moves the active list
     * entry according to cursor keys.
     */
    /*private*/ _inputKeyDownHandler(event) {

        // option button currently selected
        const $button = this.menu.getSelectedOptions().first();

        // open dropdown menu on specific key sequence
        if (matchKeyCode(event, "SPACE", { alt: true, ctrl: true })) {
            this.menu.show();
            return;
        }

        // open dropdown menu on specific navigation keys
        switch (event.keyCode) {
            case KeyCode.UP_ARROW:
            case KeyCode.DOWN_ARROW:
            case KeyCode.PAGE_UP:
            case KeyCode.PAGE_DOWN:
                this.menu.show();
                break;
        }

        // do nothing, if the dropdown menu is not visible
        if (!this.menu.isVisible()) { return; }

        switch (event.keyCode) {
            case KeyCode.ESCAPE:
                this.menu.hide();
                // Bug 28215: IE needs explicit selection again, otherwise text cursor is hidden
                setInputSelection(this.$input[0], Infinity);
                // let the Group base class not trigger the "group:cancel" event
                event.preventDefault();
                break;
            case KeyCode.SPACE:
                // Bug 28208: SPACE key with open dropdown menu and selected option button: trigger change
                if ($button.length > 0) {
                    this.menu.hide();
                    this.triggerCommit(getButtonValue($button), { sourceEvent: event, preserveFocus: true });
                    // do not insert the space character into the text field
                    return false;
                }
                break;
        }

        // move selection in dropdown list
        const nextButton = this.menu.getOptionForKey(event, $button)[0];
        if (nextButton) {
            // call the update handler to update the text field and list selection
            this.fieldValue = getButtonValue(nextButton);
            // select entire text field
            this.$input.select();
            return false;
        }
    }

    /**
     * Handler that will be called after the text field has been validated
     * while editing. Will try to insert auto-completion text according to
     * existing entries in the dropdown list.
     */
    /*private*/ _groupValidateHandler(currText, prevState) {

        // find the first button whose text representation starts with the current text
        const button = this.menu.getOptions().get().find(btn => {
            const text = this.valueToText(getButtonValue(btn));
            return is.string(text) && str.startsWithICC(text, currText);
        });

        // get value and text representation from the button
        const buttonValue = button ? getButtonValue(button) : null;
        const buttonText = button ? this.valueToText(buttonValue) : null;

        // try to add the remaining text of an existing option button, but only if the text field
        // does not contain a selection, and something has been appended to the old text
        var selection = getInputSelection(this.$input[0]);
        if (this.config.typeAhead && (selection.start === currText.length) &&
            // matching button must start with entered text
            is.string(buttonText) && (currText.length < buttonText.length) &&
            // append new text, or replace selected end of text (continue typing with type-ahead text)
            (prevState.start < currText.length) && (prevState.end === prevState.text.length)
        ) {
            this.fieldValue = buttonValue;
            setInputSelection(this.$input[0], currText.length, buttonText.length);
        }

        // select entry in dropdown list, if value (not text representation) is equal
        this._selectOption(this.fieldValue);
    }
}
