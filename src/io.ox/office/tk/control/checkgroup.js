/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import gt from "gettext";

import { is } from "@/io.ox/office/tk/algorithms";
import { OPTION_BUTTON_SELECTOR, checkButtonNodes, checkMatchingButtonNodes, createCheckboxNode,
    filterCheckedButtonNodes, filterUncheckedButtonNodes, getButtonValue, isCheckedButtonNode,
    setButtonKeyHandler, touchAwareListener } from "@/io.ox/office/tk/forms";
import { ButtonGroup } from "@/io.ox/office/tk/control/buttongroup";

// constants ==================================================================

//#. Label of a special checkbox on top of an entire check list, that checks/unchecks all entries in the list
const SELECT_ALL_LABEL = gt.pgettext("check-list", "Select all");

// private functions ==========================================================

/**
 * Returns the values of all passed button nodes as plain array.
 *
 * @param {JQuery} $buttons
 *  The button nodes whose values will be returned.
 *
 * @returns {unknown[]}
 *  The values of all passed button nodes.
 */
function getButtonValues($buttons) {
    return $buttons.get().map(getButtonValue);
}

// class CheckGroup ===========================================================

/**
 * Creates a control that contains a set of option buttons, and allows to
 * select multiple buttons at the same time. The value of this control group is
 * an array containing the values of all selected option buttons.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the base class `ButtonGroup`.
 *
 *  Additionally, the following options are supported:
 *  - {boolean} [config.boxed=false]
 *    If set to `true`, the icons to visualize the unchecked and checked state
 *    will contain rectangular boxes. Otherwise, unchecked state is simply
 *    empty space, and checked state is a simple check mark.
 *  - {boolean} [config.checkAll=false]
 *    If set to `true`, an additional special checkbox with the label "Select
 *    all" will be shown on top of all other checkboxes. Clicking this checkbox
 *    will switch the state of all other checkboxes to the new state of the
 *    "Select all" checkbox.
 */
export class CheckGroup extends ButtonGroup {

    constructor(config) {

        // base constructor
        super("checkbox", {
            ...config,
            matcher: (valueArray, buttonValue) => this._arrayMatcher(valueArray, buttonValue)
        });

        // the "Select all" checkbox
        this.$selectAll = null;

        // local copy of the value matcher (`this.config.matcher` from baseclass contains the array matcher)
        this._matcher = config?.matcher || _.isEqual;
        // rendering mode for the check icons
        this._checkmark = config?.boxed ? "boxed" : "flat";

        // add special marker classes used to adjust formatting
        this.$el.addClass("check-group");

        // option button click handler (convert ENTER and SPACE keys to click events)
        setButtonKeyHandler(this.$el);
        touchAwareListener(this.$el, OPTION_BUTTON_SELECTOR, event => this._buttonClickHandler(event));

        // add a "Select all" checkbox on top
        if (config?.checkAll) {

            // create and insert the "Select all" checkbox
            this.$selectAll = createCheckboxNode({ checkmark: this._checkmark, label: SELECT_ALL_LABEL }).addClass("select-all");
            checkButtonNodes(this.$selectAll, false);
            this.$el.append(this.$selectAll);

            // handle clicks on the checkbox separately
            touchAwareListener(this.$selectAll, event => this._selectAllClickHandler(event));
        }
    }

    // public methods ---------------------------------------------------------

    /*public override*/ addOption(value, options) {
        return super.addOption(value, { ...options, role: "checkbox", checkmark: this._checkmark });
    }

    /**
     * Returns whether at least one checkbox entry is not checked.
     *
     * @returns {boolean}
     *  Whether at least one checkbox entry is not checked.
     */
    hasUncheckedOptions() {
        return filterUncheckedButtonNodes(this.getOptions()).length > 0;
    }

    /**
     * Returns whether at least one checkbox entry is checked.
     *
     * @returns {boolean}
     *  Whether at least one checkbox entry is checked.
     */
    hasCheckedOptions() {
        return filterCheckedButtonNodes(this.getOptions()).length > 0;
    }

    // protected methods ------------------------------------------------------

    /**
     * Checks all option buttons in this group that match one of the elements
     * in the passed array.
     */
    /*protected override*/ implUpdate(values) {
        super.implUpdate(values);

        // all existing option buttons
        const $buttons = this.getOptions();

        // check all buttons matching the array elements
        if (is.array(values)) {
            checkMatchingButtonNodes($buttons, values, this.config);
        } else {
            // uncheck all buttons for ambiguous value
            checkButtonNodes($buttons, false);
        }

        // update state of the "Select all" checkbox
        if (this.$selectAll) {
            this.$selectAll.toggle($buttons.length > 1);
            const checkedCount = filterCheckedButtonNodes($buttons).length;
            const state = (checkedCount === 0) ? false : (checkedCount === $buttons.length) ? true : null;
            checkButtonNodes(this.$selectAll, state, { tristate: true });
        }
    }

    // private methods --------------------------------------------------------

    /**
     * A matcher predicate that compares elements of a value array and the
     * values of the option buttons in this control, using the value matcher
     * passed to the constructor.
     *
     * @param {unknown} valueArray
     *  Any value. If this parameter is an array, its elements will be matched
     *  against the passed button value.
     *
     * @param {unknown} buttonValue
     *  Any button value to be matched against the array elements.
     *
     * @returns {boolean}
     *  Whether the parameter "valueArray" is an array, and the matcher passed
     *  to the constructor returns `true` for at least one of its elements and
     *  the button value.
     */
    /*private*/ _arrayMatcher(valueArray, buttonValue) {
        return is.array(valueArray) && valueArray.some(value => this._matcher(value, buttonValue) === true);
    }

    /**
     * Handles click events of regular checkboxes in this group, and toggles
     * the clicked checkbox.
     */
    /*private*/ _buttonClickHandler(event) {

        // the value of the clicked button
        const clickedButton = event.currentTarget;
        const buttonValue = getButtonValue(clickedButton);

        // if the current value is not an array (ambiguous state), the new
        // value is the clicked button only (otherwise the value of the button
        // will be toggled in the current value array)
        if (!is.array(this.getValue())) {
            this.trigger("check:buttons", [buttonValue], true);
            this.triggerCommit([buttonValue], { sourceEvent: event });
            return;
        }

        // notify the changed button state
        const newChecked = !isCheckedButtonNode(clickedButton);
        this.trigger("check:buttons", [buttonValue], newChecked);

        // commit the new value of this control group
        let $buttons = filterCheckedButtonNodes(this.getOptions());
        $buttons = newChecked ? $buttons.add(clickedButton) : $buttons.not(clickedButton);
        this.triggerCommit(getButtonValues($buttons), { sourceEvent: event });
    }

    /**
     * Handles click events of the "Select all" checkbox in this group, and
     * toggles all checkboxes.
     */
    /*private*/ _selectAllClickHandler(event) {

        // the new state for all checkboxes
        const newChecked = !isCheckedButtonNode(this.$selectAll);
        // all option buttons of this control
        const $buttons = this.getOptions();

        // notify all changed option buttons
        const filterFn = newChecked ? filterUncheckedButtonNodes : filterCheckedButtonNodes;
        const changedValues = getButtonValues(filterFn($buttons));
        if (changedValues.length === 0) { return; }
        this.trigger("check:buttons", changedValues, newChecked);

        // commit the new value of this control group
        const valueArray = newChecked ? getButtonValues($buttons) : [];
        this.triggerCommit(valueArray, { sourceEvent: event });
    }
}
