/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { math } from "@/io.ox/office/tk/algorithms";
import { createDiv, createButton, createCaret, enableElement, KeyCode, hasModifierKeys } from "@/io.ox/office/tk/dom";
import { TrackingObserver } from "@/io.ox/office/tk/tracking";
import { NumberValidator } from "@/io.ox/office/tk/form";
import { TextField } from "@/io.ox/office/tk/control/textfield";

// private functions ==========================================================

/**
 * Creates and returns a spin button.
 */
function createSpinButton(increase) {
    const spinButton = createButton({ classes: "spin-button", focusable: false });
    spinButton.classList.add(increase ? "spin-up" : "spin-down");
    spinButton.append(createCaret(increase ? "up" : "down"));
    return spinButton;
}

// class SpinField ============================================================

/**
 * Creates a numeric field control with additional controls used to spin
 * (decrease and increase) the value.
 *
 * @param {object} [config]
 *  Initial configuration options. Supports all options of the base class
 *  `TextField`. If the option "validator" exists, it MUST be an instance of
 *  the class `NumberValidator` (or a subclass). Alternatively, the options
 *  supported by the constructor of the class `NumberValidator` can be passed.
 *  In this case, a new instance of `NumberValidator` will be created
 *  automatically based on these options.
 *
 *  Additionally, the following options are supported:
 *  - {number} [config.smallStep=1]
 *    The distance of small steps that will be used to increase or decrease the
 *    current value of the spin field when using the spin buttons or the cursor
 *    keys.
 *  - {number} [config.largeStep=10]
 *    The distance of large steps that will be used to increase or decrease the
 *    current value of the spin field when using the PAGE-UP or PAGE-DOWN key.
 *  - {boolean} [config.roundStep=false]
 *    If set to `true`, and the current value of the spin field will be
 *    changed, the value will be rounded to entire multiples of the step
 *    distance. Otherwise, the fractional part will remain in the value.
 *  - {boolean} [config.lazyCommit=false]
 *    If set to `true`, changes made with the spin buttons will not be
 *    committed immediately as "group:commit" events, but only when the focus
 *    leaves the spin field.
 */
export class SpinField extends TextField {

    constructor(config) {

        // base constructor
        super({
            keyboard: "number",
            ...config,
            validator: config?.validator || new NumberValidator(config)
        });

        // the distance for small/large steps
        this._smallStep = config?.smallStep ?? 1;
        this._largeStep = config?.largeStep ?? 10;

        // spin buttons, and a wrapper node
        this._upButton = createSpinButton(true);
        this._dnButton = createSpinButton(false);
        const spinWrapper = this.el.appendChild(createDiv("spin-wrapper"));
        spinWrapper.append(this._upButton, this._dnButton);

        // add classes and ARIA attributes
        this.el.classList.add("spin-field");
        this._updateAriaAttrs();

        // register event handlers
        this.listenTo(this.$input, "keydown", this._inputKeyDownHandler);

        // initialize tracking observer for spin buttons
        const observer = this.member(new TrackingObserver({
            prepare: () => this.isEnabled(),
            start: record => {
                const step = this._smallStep * ((record.id === "up") ? 1 : -1);
                // bug 46846: initial click on a spin button before this control is focused needs
                // to be deferred until base class `TextField` has processed the focus events
                if (this.hasFocus()) {
                    this._changeValue(record.inputEvent, step);
                } else {
                    this.one("group:focus", () => this._changeValue(record.inputEvent, step));
                }
            },
            repeat: "start",
            finally: () => this.setTimeout(this.grabFocus, 0)
        }));

        // observe the spin buttons
        const TRACKING_CONFIG = { repeatDelay: 700, repeatInterval: 100 };
        observer.observe(this._dnButton, { ...TRACKING_CONFIG, id: "dn" });
        observer.observe(this._upButton, { ...TRACKING_CONFIG, id: "up" });
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);
        this.$input.attr({ "aria-valuenow": value, "aria-valuetext": this.$input.val() });
        enableElement(this._upButton, value < this.validator.max);
        enableElement(this._dnButton, value > this.validator.min);
        this._updateAriaAttrs();
    }

    // private methods --------------------------------------------------------

    /*private*/ _updateAriaAttrs() {
        const { min, max } = this.validator;
        // note: role "spinbutton" would be correct but does not work with VoiceOver
        this.$input.attr({ role: "slider", "aria-valuemin": min, "aria-valuemax": max });
    }

    /**
     * Changes the current value of the spin field, according to the passed
     * step and the rounding mode passed to the constructor.
     */
    /*private*/ _changeValue(sourceEvent, step) {

        // the current value of the spin field (converted to step unit)
        const oldStepValue = this.validator.valueToStepValue(this.fieldValue);
        // the current value, rounded to the previous/next border
        const roundedValue = (step < 0) ? math.floorp(oldStepValue, -step) : math.ceilp(oldStepValue, step);
        // the new value, depending on rounding mode
        const newStepValue = (!this.config.roundStep || (oldStepValue === roundedValue)) ? (oldStepValue + step) : roundedValue;

        // set new value after restricting to minimum/maximum
        const newValue = this.fieldValue = this.validator.restrictValue(this.validator.stepValueToValue(newStepValue));

        // immediately commit the value (not in lazy mode)
        if (!this.config.lazyCommit) {
            this.triggerCommit(newValue, { sourceEvent, preserveFocus: true });
        }
    }

    /**
     * Handles keyboard events in the text field.
     */
    /*private*/ _inputKeyDownHandler(event) {

        if (hasModifierKeys(event)) { return; }

        switch (event.keyCode) {
            case KeyCode.UP_ARROW:
                this._changeValue(event, this._smallStep);
                return false;
            case KeyCode.DOWN_ARROW:
                this._changeValue(event, -this._smallStep);
                return false;
            case KeyCode.PAGE_UP:
                this._changeValue(event, this._largeStep);
                return false;
            case KeyCode.PAGE_DOWN:
                this._changeValue(event, -this._largeStep);
                return false;
        }
    }
}
