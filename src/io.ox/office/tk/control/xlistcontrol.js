/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { ListMenu } from "@/io.ox/office/tk/popup/listmenu";
import { XMenuControl } from "@/io.ox/office/tk/control/xmenucontrol";

// private functions ==========================================================

function mixinListControl(BaseClass) {

    return class extends XMenuControl.mixin(BaseClass) {

        /**
         * @param {NodeOrJQuery} button
         *  The button element that will toggle the dropdown menu when clicked.
         *
         * @param {object} [config]
         *  Configuration options for the base class, and the `ListMenu`
         *  instance.
         */
        constructor(button, config) {

            // base constructor (`XMenuControl` mixin class expects button and menu instance)
            super(button, new ListMenu({
                stretchToAnchor: true,
                ...config,
                anchor: () => this.$el
            }), config);

            // refresh layout when adding new entries to the dropdown menu
            this.listenTo(this.menu, "list:option:add", () => this.refresh());

            // update dynamic visibility (via callback functions) of the option buttons before the menu will be shown
            this.listenTo(this.menu, "popup:beforeshow", () => this.refresh({ immediate: true }));
        }

        // public methods -----------------------------------------------------

        /**
         * Adds a new section to the dropdown menu, if it does not exist yet.
         *
         * @param {string} id
         *  The unique identifier of the menu section.
         *
         * @param {object} [options]
         *  Optional parameters. Supports all options that are supported by the
         *  method `ListMenu#addSection`.
         */
        addSection(id, options) {
            this.menu.addSection(id, options);
        }

        /**
         * Returns all option buttons as JQuery collection.
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {string} [options.section]
         *    The unique identifier of a menu section. If omitted, all option
         *    buttons from all sections will be returned.
         *  - {boolean} [options.visible]
         *    If set to `true`, only visible option buttons will be returned.
         *
         * @returns {JQuery}
         *  The collection with all existing option buttons.
         */
        getOptions(options) {
            return this.menu.getOptions(options);
        }

        /**
         * Adds a new option button to this radio list.
         *
         * @param {unknown} value
         *  The unique value associated to the option button. MUST NOT be
         *  `null`.
         *
         * @param {object} [options]
         *  Optional parameters. Supports all options supported by the method
         *  `ListMenu#addOption`.
         *
         * @returns {JQuery}
         *  The new option button.
         */
        addOption(value, options) {
            return this.menu.addOption(value, options);
        }

        /**
         * Removes an option button from this radio list.
         *
         * @param {unknown} value
         *  The unique value associated to the option button. MUST NOT be
         *  `null`.
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {string} [options.section]
         *    The unique identifier of a menu section. If omitted, the matching
         *    option buttons from all sections will be deleted.
         */
        deleteOption(value, options) {
            this.menu.deleteOption(value, options);
        }

        /**
         * Removes all option buttons from this radio list.
         *
         * @param {object} [options]
         *  Optional parameters:
         *  - {string} [options.section]
         *    The unique identifier of a menu section to delete the option
         *    button from. If omitted, all sections will be cleared.
         */
        clearOptions(options) {
            this.menu.clearOptions(options);
        }
    };
}

// exports ====================================================================

export const XListControl = {

    /**
     * Creates and returns a subclass of the passed form control class that
     * owns and controls a `ListMenu` element. The mixin class `XMenuControl`
     * will be mixed in automatically into the passed base class.
     *
     * @param {CtorType<ModelT extends Group>} BaseClass
     *  The control base class to be extended.
     *
     * @returns {CtorType<ModelT & XListControl>}
     *  The extended control class with support for a list menu.
     */
    mixin: mixinListControl
};
