/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Size } from "@/io.ox/office/tk/algorithms";
import { NodeOrJQuery, ElementWrapper, ElementBaseOptions, ControlValueSerializerFn, IconId } from "@/io.ox/office/tk/dom";
import { ControlSetValueOptions, ControlChangeHandlerFn, ControlOnChangeOptions, IGenericControl } from "@/io.ox/office/tk/form";
import { EObject } from "@/io.ox/office/tk/object/eobject";

// types ======================================================================

export interface GroupDropdownVersion {
    label?: string;
}

/**
 * Configuration options for class `Group` and subclasses.
 */
export interface GroupConfig<VT = unknown> extends ElementBaseOptions {

    /**
     * An ARIA role attribute that will be set at the root DOM node of the form
     * control.
     */
    role?: string;

    /**
     * If set to `true`, the form control will be initialized in hidden state.
     * Default value is `false`.
     */
    hidden?: boolean;

    /**
     * The width of the root element of the form control, preferably as string
     * with CSS length unit (e.g. "20em"), or as number in CSS pixels.
     */
    width?: string | number;

    /**
     * A callback function that converts the value of the form control to a
     * string that will be used in "data-value" and "data-state" element
     * attributes for automated testing.
     */
    valueSerializer?: ControlValueSerializerFn<VT>;

    /**
     * If set to `true`, the form control will not write its current value into
     * the "data-state" element attribute. Default value is `false`.
     */
    skipStateAttr?: boolean;

    /**
     * If set to a boolean value, the form control will only be visible when
     * the "dynamic shrink mode" for small devices is in the respective state.
     */
    shrinkVisible?: boolean;

    /**
     * If set, and the form control switches to "dynamic shrink mode" for small
     * devices, the specified icon will be set. Applies only to form controls
     * with a caption element (e.g. buttons).
     */
    shrinkIcon?: IconId;

    /**
     * If form the control switches to "dynamic shrink mode" for small devices,
     * the specified label text will be set. Applies only to form controls with
     * a caption element (e.g. buttons).
     */
    shrinkLabel?: string;

    /**
     * The flexible minimum width of the form control, preferably as string
     * with CSS length unit (e.g. "20em"), or as number in CSS pixels.
     */
    shrinkWidth?: string | number;

    /**
     * Configuration options to be used when this control will be moved into a
     * dropdown menu.
     */
    dropDownVersion?: GroupDropdownVersion;
}

/**
 * The cause specifier of a triggered form control. Can be one of the following
 * types:
 *
 * - "click: The form control has triggered due to a button click, or a similar
 *   GUI action such as a tap on a touch device.
 *
 * - "keyboard": The form control has triggered due to a standard keyboard
 *   event, e.g. pressing the ENTER key on buttons or text fields, pressing the
 *   SPACE key on buttons.
 *
 * - "blur": The form control has triggered due to a form control losing focus,
 *   e.g. in a text input field.
 *
 * - "custom": Any other cause (e.g. programmatic execution).
 */
export type ControlTriggerType = "click" | "keyboard" | "blur" | "custom";

/**
 * Options for restoring the browser focus after a control has triggered.
 */
export interface ControlTriggerOptions {

    /**
     * If set to `true`, the current browser focus shall not be changed after
     * the form control has triggered.
     */
    preserveFocus?: boolean;

    /**
     * A DOM node that shall receive the browser focus after the form control
     * has triggered. Has no effect, if the option "preserveFocus" is set to
     * `true`.
     */
    focusTarget?: NodeOrJQuery;

    /**
     * The DOM event that caused to trigger the form control.
     */
    sourceEvent?: JEvent;

    /**
     * The cause specifier for the triggered form control. Default value is
     * "custom".
     */
    sourceType?: ControlTriggerType;
}

/**
 * Type mapping for the events emitted by `Group` instances.
 */
export interface GroupEventMap<VT> {

    /**
     * Will be emitted before the form control will be shown (while it is
     * currently hidden).
     */
    "group:beforeshow": [];

    /**
     * Will be emitted after the form control has been shown (and it was hidden
     * before).
     */
    "group:show": [];

    /**
     * Will be emitted before the form control will be hidden (while it is
     * currently visible).
     */
    "group:beforehide": [];

    /**
     * Will be emitted after the form control has been hidden (and it was
     * visible before).
     */
    "group:hide": [];

    /**
     * Will be emitted after the size of the root node has been changed.
     *
     * @param size
     *  The new size of the root node, in pixels.
     */
    "group:resize": [size: Size];

    /**
     * Will be emitted after the form control has been enabled or disabled.
     *
     * @param state
     *   The new enabled state.
     */
    "group:enable": [state: boolean];

    /**
     * Will be emitted after the form control has been focused, by initially
     * focusing any of its focusable child nodes. As long as the focus remains
     * inside the form control (even if the focus moves to another DOM node in
     * the form control), no further "group:focus" event will be triggered.
     */
    "group:focus": [];

    /**
     * Will be emitted after the form control has lost the browser focus, after
     * focusing any other DOM node outside the form control. As long as the
     * focus remains inside the form control (even if the focus moves to
     * another DOM node in the form control), the "group:blur" event will not
     * be triggered.
     */
    "group:blur": [];

    /**
     * Will be emitted after the form control has changed its intermediate
     * value during editing (e.g. in a text field), before the value will
     * actually be committed.
     *
     * @param value
     *  The intermediate value of the form control during editing.
     */
    "group:preview": [value: VT];

    /**
     * Will be emitted after the form control has changed its value.
     *
     * @param value
     *  The new value committed by the form control.
     *
     * @param options
     *  Additional information about the origin of the event.
     */
    "group:commit": [value: VT, options: ControlTriggerOptions];

    /**
     * Will be emitted when the focus needs to be returned to the application,
     * e.g. when the ESCAPE key is pressed, or when a click on a dropdown
     * button closes the opened dropdown menu.
     *
     * @param options
     *  Additional information about the origin of the event.
     */
    "group:cancel": [options: ControlTriggerOptions];
}

// class Group ================================================================

export class Group<VT = unknown, EvtMapT extends GroupEventMap<VT> = GroupEventMap<VT>> extends EObject<EvtMapT> implements IGenericControl<VT>, ElementWrapper<HTMLDivElement> {

    readonly el: HTMLDivElement;
    readonly $el: JQuery<HTMLDivElement>;
    readonly config: GroupConfig<VT>;

    protected constructor(config?: GroupConfig<VT>);

    key: string;
    readonly value: VT;
    readonly enabled: boolean;
    readonly visible: boolean;
    readonly focused: boolean;

    render(): HTMLElement;

    show(key?: string): void;
    hide(key?: string): void;
    toggle(state?: boolean, key?: string): void;

    enable(state: boolean, key?: string): void;

    getValue(): Opt<VT>;
    setValue(value: Opt<VT>, options?: ControlSetValueOptions): void;
    onChange(fn: ControlChangeHandlerFn<VT>, options?: ControlOnChangeOptions): void;

    triggerCommit(value: VT): void;
    triggerCancel(): void;

    hasFlexWidth(): boolean;
    setFlexWidth(relWidth: number): void;
    toggleShrinkMode(state: boolean): void;
    toggleDropDownMode(state: boolean): void;
}
