/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import $ from "$/jquery";

import { is } from "@/io.ox/office/tk/algorithms";
import { createCaret, setFocus, isFocusable, KeyCode, hasKeyCode, matchModifierKeys, matchKeyCode, isEscapeKey } from "@/io.ox/office/tk/dom";
import { findNextNode, findPreviousNode } from "@/io.ox/office/tk/utils";
import { isVisibleNode, setButtonKeyHandler, touchAwareListener, triggerClickForKey } from "@/io.ox/office/tk/forms";

// constants ==================================================================

// marker CSS class for elements containing an open dropdown menu
const OPEN_CLASS = "dropdown-open";

// private functions ==========================================================

function mixinMenuControl(BaseClass) {

    /**
     * @property {JQuery} $menuButton
     *  The button element that toggles the dropdown menu when clicked. Will be
     *  an empty JQuery collection if there is no menu button available.
     *
     * @property {MenuT extends BaseMenu} menu
     *  The dropdown menu instance owned and controlled by the form control.
     */
    return class extends BaseClass {

        /**
         * @param {NodeOrJQuery} button
         *  The button element that will toggle the dropdown menu when clicked.
         *
         * @param {BaseMenu} menu
         *  The dropdown menu instance that will be bound to the form control.
         *  Can be the instance of any subclass of `BaseMenu`.
         *
         *  ATTENTION: The control instance will take ownership of the dropdown
         *  menu instance and will destroy it by itself on destruction.
         *
         * @param {object} [config]
         *  Additional configuration options:
         *  - {NodeOrJQuery|string} [config.ariaOwner]
         *    The contextual parent DOM element that acts as owner of the menu.
         *    Exposes the relationship between the parent and the menu element
         *    to assistive technologies where the DOM hierarchy does not
         *    represent the relationship. If set to a string, it is used as CSS
         *    selector of a descendant element of this control. If omitted, the
         *    dropdown button element passed in parameter "button" will be used
         *    instead.
         *  - {boolean} [config.tabNavigate=false]
         *    If set to `true`, the TAB key will move the browser focus through
         *    the form controls of the menu. By default, the TAB key will leave
         *    and close the menu.
         *  - {boolean} [config.autoCloseParent=true]
         *    Whether to trigger a "group:cancel" event after closing the menu
         *    with a click on the menu button.
         */
        constructor(button, menu, config) {

            // base constructor
            super(config);

            // the button controlling the dropdown menu
            this.$menuButton = $(button);

            // take ownership of the passed menu instance
            this.menu = this.member(menu);

            // the ARIA owner node
            const ariaOwner = config?.ariaOwner;
            this._$ariaOwner = is.string(ariaOwner) ? this.$el.find(ariaOwner) : ariaOwner ? $(ariaOwner) : this.$menuButton;

            // special CSS marker for all groups with a dropdown button
            this.$el.addClass("dropdown-group");

            // convert ENTER and SPACE keys to click events
            setButtonKeyHandler(this.$menuButton);

            // add the caret symbol if specified
            const toRight = ["left", "right"].includes(menu.config.anchorBorder[0]);
            this.$menuButton.addClass("caret-button").append(createCaret(toRight ? "right" : "down"));

            // set the ARIA relation between button and menu with the "aria-owns" attribute
            // and use the ARIA label of the button also for the menu
            menu.$el.attr({ id: menu.uid, "aria-label": this._$ariaOwner.attr("aria-label") || null });
            this._$ariaOwner.attr({ "aria-owns": menu.uid, "aria-haspopup": true, "aria-expanded": false });

            // register menu node for focus handling (group remains in focused state while focus is in menu node)
            this.registerFocusableNodes(menu.$el);

            // register menu button for focus handling (menu remains visible while button is focused)
            menu.registerFocusableNodes(this.$menuButton);

            // register event handlers for the group
            this.on("group:beforehide", () => { this.menu.hide(); });
            this.on("group:enable", state => { if (!state) { this.menu.hide(); } });
            this.listenTo(this.$el, "keydown", this._keyDownHandler);
            this.listenTo(this.$el, "remote", this._remoteCommandHandler);

            // register event handlers for the menu button
            touchAwareListener(this.$menuButton, event => this._menuButtonClickHandler(event));
            this.listenTo(this.$menuButton, "keydown", this._menuButtonKeyDownHandler);

            // register event handlers for the dropdown menu
            this.listenTo(menu, "popup:show", this._popupShowHandler);
            this.listenTo(menu, "popup:beforehide", this._popupBeforeHideHandler);
            this.listenTo(menu, "menu:close:keyboard", this._menuCloseKeyboardHandler);
            this.listenTo(menu.$el, "keydown", this._menuKeyDownHandler);

            // repaint the dropdown menu by calling the own implementation method `implRepaintMenu`
            this.listenTo(menu, "menu:implrepaint", () => {
                this.implRepaintMenu();
                this.implAfterRepaintMenu();
            });
        }

        // protected methods --------------------------------------------------

        /**
         * Callback method for lazy repaint of the dropdown menu contents. This
         * method will be called:
         * - before the dropdown menu becomes visible the first time,
         * - before the dropdown menu becomes visible, and has been invalidated
         *   before,
         * - when the dropdown menu will be invalidated while it is visible.
         */
        /*protected*/ implRepaintMenu() {
            // to be overwritten by subclasses
        }

        /**
         * Callback method for lazy repaint of the menu contents. This method
         * will be called:
         * - after calling `implRepaintMenu`,
         * - before the menu becomes visible the first time,
         * - before the menu becomes visible, and has been invalidated before,
         * - when the menu will be invalidated while it is visible.
         */
        /*protected*/ implAfterRepaintMenu() {
            // to be overwritten by subclasses
        }

        // private methods ----------------------------------------------------

        /**
         * Handles keyboard events received on the root element.
         */
        /*private*/ _keyDownHandler(event) {
            if (this.isEnabled() && isVisibleNode(this.$menuButton)) {
                if (["DOWN_ARROW", "UP_ARROW"].some(code => matchKeyCode(event, code, { alt: true }))) {
                    event.target = this.$menuButton[0];
                    triggerClickForKey(event);
                }
            }
        }

        /**
         * Handler for "remote" events that can be triggered manually at the
         * root node of the form control to change the visibility of the
         * dropdown menu from external code.
         */
        /*private*/ _remoteCommandHandler(_event, command) {

            switch (command) {
                case "show":
                    this.menu.show();
                    // keep menu open regardless of browser focus
                    if (this.menu.config.autoClose) {
                        this.menu.config.autoClose = false;
                        this.menu.one("popup:hide", () => { this.menu.config.autoClose = true; });
                    }
                    break;
                case "hide":
                    this.menu.hide();
                    break;
            }
        }

        /**
         * Toggles the dropdown menu when clicking the dropdown button.
         */
        /*private*/ _menuButtonClickHandler(event) {

            // do nothing (but trigger the "group:cancel" event) if the group is disabled
            if (this.isEnabled()) {

                // toggle the dropdown menu, this triggers the appropriate event
                this.menu.toggle();

                // set focus to a control in the menu, if click was triggered by a key
                if (this.menu.isVisible() && is.number(event.keyCode)) {
                    this.menu.grabFocus({ bottom: hasKeyCode(event, "UP_ARROW") });
                }
            }

            // trigger "group:cancel" event, if the menu has been closed with mouse click,
            // or after click on a disabled group, but never after key events
            const autoCloseParent = this.config.autoCloseParent ?? true;
            if (autoCloseParent && !this.menu.isVisible() && !is.number(event.keyCode)) {
                this.triggerCancel({ sourceEvent: event });
            }
        }

        /**
         * Handles keyboard events in the focused dropdown button.
         */
        /*private*/ _menuButtonKeyDownHandler(event) {
            if (isEscapeKey(event) && this.menu.isVisible()) {
                this.menu.hide();
                return false;
            }
        }

        /**
         * Initializes the dropdown menu after it has been shown.
         */
        /*private*/ _popupShowHandler() {
            // add CSS marker class to the root element
            this.$el.addClass(OPEN_CLASS);
            this._$ariaOwner.attr("aria-expanded", true);
        }

        /**
         * Deinitializes the dropdown menu before it will be hidden.
         */
        /*private*/ _popupBeforeHideHandler() {
            // remove CSS marker class from the root element
            this.$el.removeClass(OPEN_CLASS);
            this._$ariaOwner.attr("aria-expanded", false);
        }

        /**
         * Moves browser focus to menu button, if dropdown menu is focused and
         * user presses ESC or F6.
         */
        /*private*/ _menuCloseKeyboardHandler() {
            if (this.menu.hasFocus()) {
                setFocus(this.$menuButton);
            }
        }

        /**
         * Handles keyboard events inside the open dropdown menu.
         */
        /*private*/ _menuKeyDownHandler(event) {

            switch (event.keyCode) {
                case KeyCode.TAB: {
                    if (!this.config.tabNavigate && matchModifierKeys(event, { shift: null })) {
                        // To prevent problems with event bubbling (Firefox continues to bubble to the parent of the
                        // menu node, while Chrome always bubbles from the focused DOM node, in this case from the menu
                        // *button*), stop propagation of the original event, and focus the next control manually.
                        const findNodeFn = event.shiftKey ? findPreviousNode : findNextNode;
                        const nextFocusNode = findNodeFn(document, this.$menuButton, (_i, node) => isFocusable(node));
                        setFocus(nextFocusNode);
                        this.menu.hide();
                        return false;
                    }
                    break;
                }
                case KeyCode.F6:
                    // Hide dropdown menu on global F6 focus traveling. This will set the focus back to the dropdown
                    // button, so that F6 will jump to the next toolbar. Jumping directly from the dropdown menu (which
                    // is located near the body element in the DOM) would select the wrong element. Ignore all modifier
                    // keys here, even on non-MacOS systems where F6 traveling is triggered by Ctrl+F6 (browsers will
                    // move the focus away anyway).
                    this.menu.trigger("menu:close:keyboard");
                    this.menu.hide();
                    break;
            }
        }
    };
}

// exports ====================================================================

export const XMenuControl = {

    /**
     * Creates and returns a subclass of the passed form control class that
     * owns and controls a dropdown menu element. Implements mouse and keyboard
     * event handling for a specific dropdown button (open and close the
     * dropdown menu, close the dropdown menu automatically on browser focus
     * navigation). Adds new methods to control the dropdown button and menu.
     *
     * @param {CtorType<ModelT extends Group>} BaseClass
     *  The control base class to be extended.
     *
     * @returns {CtorType<ModelT & XMenuControl>}
     *  The extended control class with support for a dropdown menu.
     */
    mixin: mixinMenuControl
};
