/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { is } from "@/io.ox/office/tk/algorithms";
import { DEFAULT_CLICK_TYPE, OPTION_BUTTON_SELECTOR, checkButtonNodes, checkMatchingButtonNodes,
    getButtonValue, isCheckedButtonNode, setButtonKeyHandler, touchAwareListener } from "@/io.ox/office/tk/forms";
import { ButtonGroup } from "@/io.ox/office/tk/control/buttongroup";

// class RadioGroup ===========================================================

/**
 * Creates a control that contains a set of radio buttons. At most one of the
 * buttons can be selected at a time. The value of this control group is equal
 * to the value of the selected button.
 *
 * @param {object} [config]
 *  Configuration options. Supports all options of the base class `ButtonGroup`.
 *
 *  Additionally, the following options are supported:
 *  - {unknown} [config.toggleValue]
 *    If set to a non-nullish value, the option button that is currently
 *    selected can be clicked to be switched off. In that case, this radio
 *    group will activate the button associated to the value specified in this
 *    option (if existing), and the action handler will return this value
 *    instead of the value of the button that has been switched off.
 */
export class RadioGroup extends ButtonGroup {

    constructor(config) {

        // base constructor
        super("radio", { role: "radiogroup", ...config });

        // bug 46872: prevent changing the radiobutton value on "taphold" to stay open the contextmenu
        let active = true;
        this.$el.on("taphold", () => { active = false; });
        this.$el.on("touchend", () => { active = true; });

        // option button click handler (convert ENTER and SPACE keys to click events)
        setButtonKeyHandler(this.$el);
        touchAwareListener(this.$el, OPTION_BUTTON_SELECTOR, event => {
            // get out, if "taphold" was triggered before. In this case we want to (stay) open the contextmenu and not
            // change the value of the radiobutton
            if (active) {
                this._clickHandler(event);
            } else {
                active = true;
            }
        });

        // handler for "remote" events at the group root node to activate a specific option button
        this.$el.on("remote", (_event, command, value) => {
            if (command === "click") {
                this.findOptions(value).first().trigger(DEFAULT_CLICK_TYPE);
            }
        });
    }

    // protected methods ------------------------------------------------------

    /**
     * Activates an option button in this radio group.
     */
    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);
        const $buttons = this.getOptions();
        if (value === null) {
            checkButtonNodes($buttons, false);
        } else {
            checkMatchingButtonNodes($buttons, value, this.config);
        }
    }

    // private methods --------------------------------------------------------

    /**
     * Handler for clicks on option buttons.
     */
    /*private*/ _clickHandler(event) {

        // target value for toggle click
        const { toggleValue } = this.config;
        // check whether a click on a selected button toggles state back
        const toggleClick = !is.nullish(toggleValue) && isCheckedButtonNode(event.currentTarget);

        // trigger the new control value
        const value = toggleClick ? toggleValue : getButtonValue(event.currentTarget);
        this.triggerCommit(value, { sourceEvent: event });
    }
}
