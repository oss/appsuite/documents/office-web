/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { createSpan } from "@/io.ox/office/tk/dom";
import { VISIBLE_SELECTOR, OPTION_BUTTON_CLASS, OPTION_BUTTON_SELECTOR, createButtonNode, filterButtonNodes, getCaptionNode, setToolTip, setButtonOptions } from "@/io.ox/office/tk/forms";
import { debounceMethod } from "@/io.ox/office/tk/objects";
import { sortOptionButtons } from "@/io.ox/office/tk/popup/listmenu";
import { Group } from "@/io.ox/office/tk/control/group";

// constants ==================================================================

// CSS selector for visible option buttons
const VISIBLE_BUTTON_SELECTOR = OPTION_BUTTON_SELECTOR + VISIBLE_SELECTOR;

// class ButtonGroup ==========================================================

/**
 * Creates a control that contains a set of buttons. This class does not
 * add any special behavior to the control. Used as base class for controls
 * such as radio groups, or check lists.
 */
export class ButtonGroup extends Group {

    /**
     * @param {string} buttonRole
     *  The ARIA role for the option buttons.
     *
     * @param {object} [config]
     *  Configuration options. Supports all options of the base class `Group`.
     *
     *  Additionally, the following options are supported:
     *  - {Function} [config.matcher=_.isEqual]
     *    A comparison function that returns whether a button element in this
     *    group should match a specific value, e.g. for selection. Receives an
     *    arbitrary value as first parameter, and the value of a button element
     *    as second parameter. The function must return `true` to match the
     *    respective button element, `false` to not match the button element,
     *    or any other value to skip the button element (e.g. to keep the
     *    current selection state of the button unmodified). If omitted, uses
     *    `_.isEqual` which compares arrays and objects deeply, and does not
     *    skip any button element.
     *  - {boolean|CompareFn<unknown>} [config.sorted=false]
     *    If set to `true` or a callback function, the button elements will be
     *    sorted according to the button label (or custom sort index specified
     *    via the "sortIndex" options of each inserted button, see method
     *    `addOption` for details). The value `true` will cause to sort the
     *    elements by their natural order (strings with case-insensitive
     *    locale-aware natural sorting). If a callback function has been passed
     *    instead, it will be used as comparator callback function for the
     *    method `Array::sort`.
     *  - {string} [config.tooltip]
     *    A tooltip text shown for all button elements of this button group.
     */
    /*protected*/ constructor(buttonRole, config) {

        // base constructor
        super(config);

        // the ARIA role for the option buttons
        this._buttonRole = buttonRole;

        // CSS marker class and tooltip
        this.$el.addClass("button-group");
        setToolTip(this.$el, config);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns all option buttons as JQuery collection.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible]
     *    If set to `true`, only visible option buttons will be returned.
     *
     * @returns {JQuery}
     *  The JQuery collection with all existing option buttons.
     */
    getOptions(options) {
        const selector = options?.visible ? VISIBLE_BUTTON_SELECTOR : OPTION_BUTTON_SELECTOR;
        return this.$el.children(selector);
    }

    /**
     * Returns all option buttons that represent the specified value.
     *
     * @param {unknown} value
     *  The value to filter the option buttons for.
     *
     * @param {object} [options]
     *  Optional parameters:
     *  - {boolean} [options.visible]
     *    If set to `true`, only visible option buttons will be returned.
     *
     * @returns {JQuery}
     *  A JQuery collection with all existing option buttons matching the
     *  passed value.
     */
    findOptions(value, options) {
        const matcher = this.config.matcher || _.isEqual;
        return filterButtonNodes(this.getOptions(options), value, { matcher });
    }

    /**
     * Adds a new option button to this button group.
     *
     * @param {unknown} value
     *  The unique value associated to the button. Must not be `null`.
     *
     * @param {object} [options]
     *  Optional parameters. Supports all formatting options for button
     *  elements supported by the function `createButtonNode()`.
     *
     *  Additionally, the following options are supported:
     *  - {unknown} [options.sortIndex]
     *    Sorting index for the new button. If omitted, the button will be
     *    sorted by its label text. Sorting will be stable, multiple buttons
     *    with the same effective index will remain in relative insertion
     *    order.
     *
     * @returns {JQuery}
     *  The button element for the new option button.
     */
    addOption(value, options) {

        // the new button
        const $button = createButtonNode({ role: this._buttonRole, ...options });

        // initialize the button element, and insert it into the control
        $button.addClass(OPTION_BUTTON_CLASS);
        this.setButtonValue($button, value);
        setToolTip($button, options);
        setButtonOptions($button, options);
        this.$el.append($button);

        // special content when shown in dropdown menu instead of toolbar (shrink mode)
        const ddVersion = options?.dropDownVersion;
        if (ddVersion?.label) {
            getCaptionNode($button).append(
                createSpan({ classes: "drop-down-only", label: ddVersion.label })
            );
        }
        if (ddVersion?.tooltip) {
            setToolTip($button, ddVersion);
        }

        // sort the buttons debounced by their index
        this._sortOptions();

        // notify listeners
        this.trigger("create:button", $button, value, options);

        this.refresh();
        return $button;
    }

    /**
     * Removes all option buttons from this button group.
     */
    clearOptions() {
        this.getOptions().remove();
    }

    // private methods --------------------------------------------------------

    /**
     * Sorts all option buttons in this group.
     */
    @debounceMethod({ delay: "microtask" })
    /*private*/ _sortOptions() {
        const { sorted } = this.config;
        if (sorted) {
            const buttons = this.getOptions().detach().get();
            this.$el.append(sortOptionButtons(buttons, sorted));
        }
    }
}
