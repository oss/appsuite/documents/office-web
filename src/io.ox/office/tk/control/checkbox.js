/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import { checkButtonNodes } from "@/io.ox/office/tk/forms";
import { Button } from "@/io.ox/office/tk/control/button";

// class CheckBox =========================================================

/**
 * Creates a container element used to hold a toggle button, styled like a
 * regular check box.
 *
 * @param {Object} [config]
 *  Optional parameters. Supports all options of the base class `Button`,
 *  except for the option "toggle" option which will always be set to `true`,
 *  and "highlight" (check boxes will never be highlighted).
 *
 *  Additionally, the following options are supported:
 *  - {boolean} [config.boxed=false]
 *    If set to `true`, the icons to visualize the unchecked and checked state
 *    will contain rectangular boxes. Otherwise, unchecked state is simply
 *    empty space, and checked state is a simple check mark.
 *  - {boolean} [config.tristate=false]
 *    If set to `true`, a third "ambiguous" state (value `null`) will be
 *    represented by a specific distinct icon. Otherwise, the icon for the
 *    unchecked state will be used.
 */
export class CheckBox extends Button {

    constructor(config) {

        // base constructor
        super({
            role: "checkbox", // default ARIA role
            checkmark: config?.boxed ? "boxed" : "flat",
            ...config,
            toggle: true,
            highlight: false // never highlight the check box
        });

        // add special marker classes used to adjust formatting
        this.$el.addClass("check-box");
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(state) {
        super.implUpdate(state);
        checkButtonNodes(this.$button, state, { tristate: this.config.tristate });
    }
}
