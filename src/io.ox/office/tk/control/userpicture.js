/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";

import { createContactPicture, setContactPicture } from "@/io.ox/office/tk/utils/contactutils";
import { setToolTip } from "@/io.ox/office/tk/forms";
import { Group } from "@/io.ox/office/tk/control/group";

// class UserPicture ==========================================================

/**
 * A form control that displays the photo uploaded by a specific user.
 *
 * @param {object} [config]
 *  Optional parameters. Supports all options of the base class `Group`.
 *  Additionally, the following options are supported:
 *  - {number} [config.size=48]
 *    The size of the picture element, in pixels.
 */
export class UserPicture extends Group {

    constructor(config) {

        // base constructor
        super({
            ...config,
            valueSerializer: currData => String(currData.userId)
        });

        // CSS marker class
        this.$el.addClass("user-picture");

        // the element containing the photo (as CSS background)
        this.$picture = createContactPicture(null, { size: config?.size ?? 48 });
        this.$el.append(this.$picture);
    }

    // protected methods ------------------------------------------------------

    /*protected override*/ implUpdate(userData) {
        super.implUpdate(userData);

        // request the picture via Contacts API
        const userId = userData?.userId ?? -1;
        setContactPicture(this.$picture, { userId });

        // show user name as tooltip
        const userName = _.noI18n(userData?.userName ?? "");
        setToolTip(this.$picture, userName);
    }
}
