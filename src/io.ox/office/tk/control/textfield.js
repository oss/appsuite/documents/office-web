/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from "$/underscore";
import $ from "$/jquery";

import { createInputNode, setToolTip, showNodes, touchAwareListener } from "@/io.ox/office/tk/forms";
import { IOS_SAFARI_DEVICE, setElementLabel, createSpan, createIcon, setFocus, getInputSelection, setInputSelection, KeyCode } from "@/io.ox/office/tk/dom";
import { TextValidator } from "@/io.ox/office/tk/form";
import { Group } from "@/io.ox/office/tk/control/group";

// global singletons ==========================================================

// global instance of the default validator
const defaultValidator = new TextValidator();

// class TextField ============================================================

/**
 * Creates a container element used to hold a text input field.
 *
 * @param {object} [config]
 *  Initial configuration options. Supports all options of the base class
 *  `Group`, and all formatting options for input elements supported by the
 *  function `createInputNode()`.
 *
 *  Additionally, the following options are supported:
 *  - {boolean} [config.select=false]
 *    If set to `true`, the entire text will be selected after the text field
 *    has been clicked, or if the method `grabFocus()` has been called. The
 *    text will always be selected (independently from this option), if the
 *    text field gets focus via keyboard navigation.
 *  - {AbstractValidator} [config.validator]
 *    A text validator that will be used to convert the control values (passed
 *    to the method `Group#setValue`) to the text representation used in this
 *    text field, to validate the text while typing in the text field, and to
 *    convert the entered text to the control value (returned by the method
 *    `Group#getValue`). If no validator has been specified, a default
 *    validator will be used that does not perform any conversions.
 *  - {string} [config.icon]
 *    The CSS class name of an icon to be shown in the text field.
 *  - {boolean} [config.clearButton=false]
 *    If set to `true`, a button will be shown in the text field that will
 *    clear the text contents.
 */
export class TextField extends Group {

    constructor(config) {

        // base constructor
        super(config);

        // the <input> element
        this.$input = createInputNode(config);

        // the validator used to convert and validate values
        this.validator = config?.validator || defaultValidator;

        // the info badge element
        this._$infoBadge = createSpan("info-badge");
        // the clear button to clear the textbox
        this._$clearButton = null;
        // saved state of the text field, used to restore while validating
        this._inputState = null;
        // initial value of text field when focused
        this._initialText = null;
        // resolved value for the current text in the input field
        this._fieldValue = null;
        // whether the group is focused (needed to detect whether to restore selection after focus change inside the group)
        this._groupFocused = false;

        // add special marker class used to adjust formatting
        this.$el.addClass("text-field");
        setToolTip(this.$el, config);

        // create the wrapper node for the input field (IE renders input fields with padding wrong)
        const $wrapper = $('<div class="input-wrapper">').append(
            config?.icon ? createIcon(config.icon) : null,
            this.$input,
            this._$infoBadge
        );

        // create an embedded "clear contents" button
        if (config?.clearButton) {
            this._$clearButton = createIcon("bi:x-circle-fill", "clear-button");
            $wrapper.append(this._$clearButton);
            this.listenTo(this._$clearButton, "click", () => {
                this.fieldValue = this.textToValue("");
                this.setValue("");
                this.grabFocus();
            });
        }

        // insert the text field into this group
        this.$el.append($wrapper);

        // register handlers for base class events
        this.on("group:focus", () => this._focusHandler("group:focus"));
        this.on("group:blur", () => this._focusHandler("group:blur"));
        this.on("group:enable", this._enableHandler);
        this.on("group:commit", this._commitHandler);

        // register handlers for DOM events from <input> element
        this.listenTo(this.$input, "blur", () => this._focusHandler("blur"));
        this.listenTo(this.$input, "keydown keypress keyup", this._keyHandler);
        this.listenTo(this.$input, "input", this._inputHandler);

        // listen to global focus events (to be able to distinguish between mouse and keyboard focus)
        this.listenToGlobal("change:focus", this._globalFocusHandler);

        // forward clicks on the wrapper node (left/right padding beside the input field)
        touchAwareListener($wrapper, event => {
            if (this.isEnabled() && (event.target === event.currentTarget)) {
                setFocus(this.$input);
            }
        });

        // initialize field state
        this._saveInputState();
    }

    // public accessors -------------------------------------------------------

    /**
     * Returns the effective value represented by the current text in the text
     * input control.
     */
    get fieldValue() {
        return this._fieldValue;
    }

    /**
     * Sets the effective value to be displayed in the text input control. The
     * input control will display the text for this value as returned by the
     * validator of this control group.
     */
    set fieldValue(value) {
        this._fieldValue = value;
        this.$input.val(this.valueToText(value));
        this._validateInputText();
        this.implUpdate(this._fieldValue);
    }

    // public methods ---------------------------------------------------------

    /**
     * Converts the passed value to a display text using the validator of this
     * text field.
     *
     * @param {unknown} value
     *  The value to be converted to its display text. The value `null` will
     *  always be converted to an empty string.
     *
     * @returns {string}
     *  The display text for the passed value.
     */
    valueToText(value) {
        return (value === null) ? "" : this.validator.valueToText(value);
    }

    /**
     * Converts the passed display text to a value using the validator of this
     * text field.
     *
     * @param {string} text
     *  The display text to be converted to a value.
     *
     * @returns {unknown}
     *  The value for the passed display text; or `null`, if the text cannot be
     *  converted.
     */
    textToValue(text) {
        return this.validator.textToValue(text);
    }

    /**
     * Sets the browser focus into the text input element.
     *
     * @returns {this}
     *  A reference to this instance.
     */
    grabFocus() {
        setFocus(this.$input);
        return this;
    }

    /**
     * Shows a little badge with the specified text behind the text input
     * element.
     *
     * @param {string} label
     *  The text label to be shown. If set to an empty string, the info badge
     *  will be hidden.
     *
     * @param {string} [tooltip]
     *  A tooltip for the badge. If omitted, or set to an empty string, the
     *  info badge will not contain a tooltip.
     */
    setInfoBadge(label, tooltip) {
        setElementLabel(this._$infoBadge, { label, tooltip, clear: true });
    }

    // protected methods ------------------------------------------------------

    /**
     * The update handler for this text field.
     */
    /*protected override*/ implUpdate(value) {
        super.implUpdate(value);

        // do not access the input element if the value does not change (browsers will destroy current selection)
        const text = this.valueToText(value);
        if (this.$input.val() !== text) {
            this.$input.val(text);
        }

        this._fieldValue = value;
        this._saveInputState();
    }

    // private methods --------------------------------------------------------

    /**
     * Saves the current value and selection of the text field node.
     */
    /*private*/ _saveInputState() {

        // save selection and text of the <input> element
        this._inputState = getInputSelection(this.$input[0]);
        this._inputState.value = this._fieldValue;
        const text = this._inputState.text = this.$input.val();

        // show or hide the embedded "Clear" button
        if (this._$clearButton) {
            showNodes(this._$clearButton, text.length > 0);
        }
    }

    /**
     * Restores the current value and selection of the text field.
     */
    /*private*/ _restoreInputState() {
        this._fieldValue = this._inputState.value;
        this.$input.val(this._inputState.text);
        setInputSelection(this.$input[0], this._inputState);
    }

    /**
     * Selects the entire text of the input node.
     */
    /*private*/ _selectAll() {

        // Bug 43829: Workaround to select the whole text when 'touching' in the input field on iOS,
        // because select() doesn't work. It's an iOS problem.
        // see http://stackoverflow.com/questions/3272089/programmatically-selecting-text-in-an-input-field-on-ios-devices-mobile-safari
        if (IOS_SAFARI_DEVICE) {
            this.setTimeout(()  => this.$input[0].setSelectionRange(0, 9999), 1);
            return;
        }

        // this is how selecting the whole text is supposed to work
        this.$input.select();
    }

    /**
     * Handles all focus events of the text field.
     */
    /*private*/ _focusHandler(type) {

        switch (type) {
            case "group:focus":
                this.enterEditMode();
                // save current value, if this group receives initial focus
                this._fieldValue = this._inputState.value = this.getValue();
                this._initialText = this._inputState.text = this.$input.val();
                // wait until the <input> element gets focused (see below)
                this._groupFocused = false;
                break;

            case "blur":
                // save field state to be able to restore selection
                this._saveInputState();
                break;

            case "group:blur":
                // Bug 27175: always commit value when losing focus
                this.fieldValue = this._fieldValue;
                if (this.isEditMode() && (this._initialText !== this.$input.val())) {
                    // pass option "preserveFocus" to not interfere with current focus handling
                    this.triggerCommit(this.fieldValue, { sourceType: "blur", preserveFocus: true });
                }
                this.leaveEditMode();
                this._fieldValue = this._initialText = null;
                this._groupFocused = false;
                break;
        }
    }

    /**
     * Handles global focus events.
     *
     * Updates the internal state of this instance after the `<input>` DOM
     * element has received or lost the browser focus. Selects the entire text
     * according to the focus cause (mouse versus keyboard).
     */
    /*private*/ _globalFocusHandler(focusNode, blurNode, keyEvent) {

        // the <input> element has received browser focus
        if (this.$input.is(focusNode)) {

            // <input> element focused again (e.g. from drop-down menu): restore selection
            if (this._groupFocused) {
                this._restoreInputState();
                return;
            }

            // on first focus for the <input> element: remember that this entire
            // group is focused, until the "group:blur" event will be triggered
            this._groupFocused = true;

            // remove previous "mouseup" handler (see below)
            this.$input.off("mouseup");

            // always select entire text when reaching the field with keyboard, or when specified via option
            if (keyEvent || this.config.select) {
                this._selectAll();

                // Bug 53025: Suppress next "mouseup" event to prevent that the browser (especially MS Edge, see
                // https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/8229660/) changes the selection
                // to a cursor. Simply preventing default browser action ("preventDefault") does not work though.
                if (!keyEvent) {
                    this.$input.one("mouseup", () => this._selectAll());
                }
            }

            // save current contents and selection
            this._saveInputState();
            return;
        }

        // the <input> element has lost browser focus
        if (this.$input.is(blurNode)) {
            // always remove the dummy "mouseup" handler when losing focus (see above)
            this.$input.off("mouseup");
        }
    }

    /**
     * Handles enable/disable events of the group.
     */
    /*private*/ _enableHandler(state) {
        this.$input.attr("readonly", state ? null : "readonly");
    }

    /**
     * Handles "group:commit" events triggered by any action, also by
     * additional controls from sub classes which are not under control of
     * this instance. Prevents triggering a second change event caused by
     * the "group:blur" event.
     */
    /*private*/ _commitHandler() {
        // prevent another trigger from "group:blur" handler
        if (this.isEditMode()) {
            this._initialText = this.$input.val();
        }
    }

    /**
     * Handles keyboard events.
     */
    /*private*/ _keyHandler(event) {

        switch (event.keyCode) {
            case KeyCode.ENTER:
                if (event.type === "keyup") {
                    this.fieldValue = this._fieldValue;
                    this.triggerCommit(this.fieldValue, { sourceEvent: event });
                }
                return false;
            case KeyCode.ESCAPE:
                if (event.type === "keydown") {
                    this.fieldValue = this.getValue();
                }
                break;
            case KeyCode.HOME:
            case KeyCode.END:
                // do not let these key codes bubble up, just do the default action
                event.stopPropagation();
                break;
        }
    }

    /**
     * Validates the current text in the input field with the current
     * validator.
     */
    /*private*/ _validateInputText() {

        // do not perform validation if nothing has changed
        const currText = this.$input.val();
        if (currText === this._inputState.text) { return; }

        // previous state of the input element
        const prevState = _.clone(this._inputState);

        // validate the current field text
        const result = this.validator.validate(currText);

        // update the text field according to the validation result
        if (result === false) {
            // false: restore the old field state
            this._restoreInputState();
        } else if (_.isString(result) && (result !== currText)) {
            // insert the validation result and restore the old selection
            this._inputState.value = this.textToValue(result);
            this._inputState.text = result;
            this._restoreInputState();
        }

        // trigger "group:validate" event to listeners, pass new text and old state
        this.trigger("group:validate", this.$input.val(), prevState);

        // update current state of the text field
        this._saveInputState();

        // trigger "group:preview" event to listeners for dynamic preview functionality
        this.trigger("group:preview", this.$input.val());
    }

    /**
     * Handles input events triggered when the text changes while typing.
     * Performs live validation with the current validator.
     */
    /*private*/ _inputHandler() {
        this._fieldValue = this.textToValue(this.$input.val());
        this._validateInputText();
    }
}
