/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import '@open-xchange/jquery-touch-events/src/jquery.mobile-events';

import { is, str } from '@/io.ox/office/tk/algorithms';
import {
    TOUCH_DEVICE, IOS_SAFARI_DEVICE,
    containsNode, createElement, createSpan, createIcon, setFocus, findFocusable,
    KeyCode, hasModifierKeys, hasKeyCode, matchKeyCode, serializeControlValue
} from '@/io.ox/office/tk/dom';
import { getNodePositionInPage } from '@/io.ox/office/tk/utils';

import '@/io.ox/office/tk/dom/style.less';

// constants ==================================================================

/**
 * CSS class name for caption elements
 */
export const CAPTION_CLASS = 'caption';

/**
 * CSS selector for caption elements in control nodes
 */
export const CAPTION_SELECTOR = '.' + CAPTION_CLASS;

/**
 * CSS class for hidden DOM elements.
 */
export const HIDDEN_CLASS = 'hidden';

/**
 * CSS selector for hidden DOM elements.
 */
export const HIDDEN_SELECTOR = '.' + HIDDEN_CLASS;

/**
 * CSS selector for visible DOM elements.
 */
export const VISIBLE_SELECTOR = ':not(' + HIDDEN_SELECTOR + ')';

/**
 * CSS class for disabled DOM elements.
 */
export const DISABLED_CLASS = 'disabled';

/**
 * CSS selector for disabled DOM elements.
 */
export const DISABLED_SELECTOR = '.' + DISABLED_CLASS;

/**
 * CSS selector for enabled DOM elements.
 */
export const ENABLED_SELECTOR = ':not(' + DISABLED_SELECTOR + ')';

/**
 * CSS class for selected (active) DOM elements.
 */
export const SELECTED_CLASS = 'selected';

/**
 * CSS selector for selected (active) DOM elements.
 */
export const SELECTED_SELECTOR = '.' + SELECTED_CLASS;

/**
 * CSS class for DOM container elements that contain the browser focus.
 */
export const FOCUSED_CLASS = 'focused';

/**
 * CSS class for button elements.
 */
export const BUTTON_CLASS = 'button';

/**
 * CSS selector for button elements.
 */
export const BUTTON_SELECTOR = '.' + BUTTON_CLASS;

/**
 * CSS class name for option button elements in a button group.
 */
export const OPTION_BUTTON_CLASS = 'option-button';

/**
 * CSS element selector for option button elements in a button group.
 */
export const OPTION_BUTTON_SELECTOR = BUTTON_SELECTOR + '.' + OPTION_BUTTON_CLASS;

/**
 * CSS class for DOM container elements that contain the browser focus.
 */
export const DEFAULT_CLICK_TYPE = TOUCH_DEVICE ? 'tap' : 'click';

/**
 * CSS class name for elements that show the global tool tip while hovered.
 */
export const APP_TOOLTIP_CLASS = 'app-tooltip';

// private constants ----------------------------------------------------------

// CSS selector for icon elements in control captions
var ICON_SELECTOR = '>[data-icon-id]';

// CSS selector for text spans in control captions
var SPAN_SELECTOR = '>span';

// CSS class for buttons with ambiguous value
var AMBIGUOUS_CLASS = 'ambiguous';

// attribute name for selected/checked buttons
var DATA_CHECKED_ATTR = 'data-checked';

// CSS selector for selected buttons
var DATA_CHECKED_SELECTOR = '[' + DATA_CHECKED_ATTR + '="true"]';

// private functions ======================================================

/**
 * Creates a DOM element.
 *
 * @param {String} nodeName
 *  The tag name of the DOM element to be created.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Dict} [options.attributes]
 *      The element attributes to be inserted into the DOM element. The
 *      attribute names can be given in camel-case notation. MUST NOT
 *      contain the attribute 'style' if the option 'style' (see below) is
 *      in use.
 *  - {Partial<CSSStyleDeclaration>} [options.style]
 *      Additional CSS style attributes for the control element. Will be
 *      inserted into the 'style' attribute of the element. Can be a string
 *      that will be inserted into the 'style' attribute as is, or can be
 *      an object that maps CSS properties to their values. The property
 *      names can be given in camel-case notation.
 *
 * @returns {JQuery}
 *  The DOM element.
 */
function createElementNode(nodeName, options) {
    const $node = $(`<${nodeName}>`);
    if (options?.attributes) { $node.attr(options.attributes); }
    if (options?.style) { $node.css(options.style); }
    return $node;
}

// public functions ===========================================================

/**
 * Enables or disables the tooltips of the specified DOM elements.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The DOM elements to be manipulated. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Boolean} state
 *  Whether to enable (true) or disable (false) the tooltips.
 */
export function enableToolTip(nodes, state) {
    if (!TOUCH_DEVICE) {
        $(nodes).toggleClass(APP_TOOLTIP_CLASS, state);
    }
}

/**
 * Adds a tooltip to the specified DOM elements.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The DOM elements to be manipulated. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Object|String} [options]
 *  Optional parameters:
 *  - {String} [options.tooltip=null]
 *      The text for the tooltip text shown when the mouse hovers one of
 *      the nodes. If omitted or set to an empty string, the nodes will not
 *      show a tooltip anymore.
 *  If the parameter is a simple string, it will be set directly as the
 *  tooltip.
 */
export function setToolTip(nodes, options) {

    var tooltip = (typeof options === 'string') ? options : (options?.tooltip || '');

    if (tooltip.length === 0) {
        $(nodes).removeAttr('data-original-title title aria-label');
        enableToolTip(nodes, false);
    } else if (TOUCH_DEVICE) {
        $(nodes).attr({ title: tooltip });
    } else {
        $(nodes).attr({ 'aria-label': tooltip, 'data-original-title': tooltip });
        enableToolTip(nodes, true);
    }
}

/**
 * Returns whether the specified DOM element is in visible state. If node
 * contains the CSS class Forms.HIDDEN_CLASS, it is considered being
 * hidden.
 *
 * _Attention:_ Checks the existence of the CSS class Forms.HIDDEN_CLASS only;
 * does NOT check the effective visibility of the DOM node according to its CSS
 * formatting attributes.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @returns {Boolean}
 *  Whether the element is visible.
 */
export function isVisibleNode(node) {
    return $(node).first().is(VISIBLE_SELECTOR);
}

/**
 * Returns whether the specified DOM element and all its ancestors are in
 * visible state. If there is any ancestor containing the CSS class
 * Forms.HIDDEN_CLASS, or the node is not part of the document DOM, it is
 * considered being hidden.
 *
 * _Attention:_ Checks the existence of the CSS class Forms.HIDDEN_CLASS only;
 * does NOT check the effective visibility of the DOM node according to its CSS
 * formatting attributes.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @returns {Boolean}
 *  Whether the element is visible.
 */
export function isDeeplyVisibleNode(node) {
    node = $(node).first();
    return containsNode(document.body, node[0]) && (node.closest(HIDDEN_SELECTOR).length === 0);
}

/**
 * Filters the visible elements from the passed jQuery collection.
 *
 * _Attention:_ Checks the existence of the CSS class Forms.HIDDEN_CLASS only;
 * does NOT check the effective visibility of the DOM nodes according to their
 * CSS formatting attributes.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The elements to be filtered.
 *
 * @returns {jQuery}
 *  A jQuery collection with all visible elements.
 */
export function filterVisibleNodes(nodes) {
    return $(nodes).filter(VISIBLE_SELECTOR);
}

/**
 * Filters the hidden elements from the passed jQuery collection.
 *
 * _Attention:_ Checks the existence of the CSS class Forms.HIDDEN_CLASS only;
 * does NOT check the effective visibility of the DOM nodes according to their
 * CSS formatting attributes.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The elements to be filtered.
 *
 * @returns {jQuery}
 *  A jQuery collection with all hidden elements.
 */
export function filterHiddenNodes(nodes) {
    return $(nodes).not(VISIBLE_SELECTOR);
}

/**
 * Shows or hides the specified DOM elements.
 *
 * _Attention:_ Modifies the existence of the CSS class Forms.HIDDEN_CLASS
 * only; does NOT modify the 'display' or any other CSS properties of the DOM
 * node.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The DOM elements to be manipulated. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Boolean} state
 *  Whether to show (true) or hide (false) the DOM elements.
 */
export function showNodes(nodes, state) {
    nodes = $(nodes);
    nodes.toggleClass(HIDDEN_CLASS, !state);
    nodes.attr('aria-hidden', state ? null : true);
}

/**
 * Returns whether the specified DOM element is in enabled state.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @returns {Boolean}
 *  Whether the element is enabled.
 */
export function isEnabledNode(node) {
    return $(node).first().is(ENABLED_SELECTOR);
}

/**
 * Filters the enabled elements from the passed jQuery collection.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The elements to be filtered.
 *
 * @returns {jQuery}
 *  A jQuery collection with all enabled elements.
 */
export function filterEnabledNodes(nodes) {
    return $(nodes).filter(ENABLED_SELECTOR);
}

/**
 * Filters the disabled elements from the passed jQuery collection.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The elements to be filtered.
 *
 * @returns {jQuery}
 *  A jQuery collection with all disabled elements.
 */
export function filterDisabledNodes(nodes) {
    return $(nodes).not(ENABLED_SELECTOR);
}

/**
 * Enables or disables the specified DOM elements.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The DOM elements to be manipulated. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Boolean} state
 *  Whether to enable (true) or disable (false) the DOM elements.
 */
export function enableNodes(nodes, state) {
    nodes = $(nodes);
    nodes.toggleClass(DISABLED_CLASS, !state);
    nodes.attr('aria-disabled', state ? null : true);
}

/**
 * Returns whether the specified DOM element is in selected state.
 *
 * @param {HTMLElement|jQuery} node
 *  The DOM element. If this object is a jQuery collection, uses the first
 *  node it contains.
 *
 * @returns {Boolean}
 *  Whether the element is selected.
 */
export function isSelectedNode(node) {
    return $(node).first().is(SELECTED_SELECTOR);
}

/**
 * Filters the selected elements from the passed jQuery collection.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The elements to be filtered.
 *
 * @returns {jQuery}
 *  A jQuery collection with all selected elements.
 */
export function filterSelectedNodes(nodes) {
    return $(nodes).filter(SELECTED_SELECTOR);
}

/**
 * Filters the unselected elements from the passed jQuery collection.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The elements to be filtered.
 *
 * @returns {jQuery}
 *  A jQuery collection with all unselected elements.
 */
export function filterUnselectedNodes(nodes) {
    return $(nodes).not(SELECTED_SELECTOR);
}

/**
 * Selects or deselects the specified DOM elements.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The DOM elements to be manipulated. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Boolean} state
 *  Whether to select (true) or deselect (false) the DOM elements.
 */
export function selectNodes(nodes, state) {
    $(nodes).toggleClass(SELECTED_CLASS, state);
}

/**
 * Triggers a 'click' event at the target node of the passed keyboard
 * event. The new click event will contain additional information about the
 * original keyboard event:
 * - {Event} originalEvent
 *      The original keyboard DOM event.
 * - {Number} keyCode
 *      The key code of the original event.
 * - {Boolean} shiftKey
 *      The state of the SHIFT key.
 * - {Boolean} altKey
 *      The state of the ALT key.
 * - {Boolean} ctrlKey
 *      The state of the CTRL key.
 * - {Boolean} metaKey
 *      The state of the META key.
 *
 * @param {jQuery.Event} event
 *  The original event, as jQuery event object.
 */
export function triggerClickForKey(event) {
    $(event.target).trigger(new $.Event('click', {
        originalEvent: event.originalEvent,
        keyCode: event.keyCode,
        shiftKey: event.shiftKey,
        altKey: event.altKey,
        ctrlKey: event.ctrlKey,
        metaKey: event.metaKey
    }));
}

// browser focus ----------------------------------------------------------

/**
 * Enables focus navigation with cursor keys in the specified DOM element.
 *
 * @param {HTMLElement|jQuery} rootNode
 *  A DOM element whose descendant focusable form controls can be focused
 *  with the cursor keys.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.homeEnd=false]
 *      If set to true, the HOME key will focus the first, and the END key
 *      will focus the last available form control.
 */
export function enableCursorFocusNavigation(rootNode, options) {

    // whether to use HOME/END keys
    var homeEnd = options?.homeEnd;
    // anchor node for vertical movement
    var anchorVertNode = null;
    // anchor node for horizontal movement
    var anchorHorNode = null;

    function setFocusAndResetAnchor(focusNode, resetAnchor) {
        setFocus(focusNode);
        if (resetAnchor || !anchorVertNode) { anchorVertNode = focusNode; }
        if (resetAnchor || !anchorHorNode) { anchorHorNode = focusNode; }
    }

    function moveFocus(focusNode, direction) {

        // all focusable nodes contained in the root node
        var focusableNodes = findFocusable(rootNode);
        // the index of the focused node
        var focusIndex = focusableNodes.index(focusNode);
        // whether to move forward
        var forward = /^(right|down)$/.test(direction);
        // whether to move up/down
        var vertical = /^(up|down)$/.test(direction);
        // position and size of the focused node
        var focusPosition = null;

        // any other node was focused (e.g. the root node itself): start with first/last available node
        if (focusIndex < 0) {
            setFocusAndResetAnchor(forward ? focusableNodes.first() : focusableNodes.last(), true);
            return;
        }

        // resolve the positions and sizes of all focus nodes
        focusableNodes = focusableNodes.get().map(function (node) {
            var position = getNodePositionInPage(node);
            return _.extend(position, {
                node,
                centerX: position.left + position.width / 2,
                centerY: position.top + position.height / 2
            });
        });

        // remember position of current focus node
        focusPosition = focusableNodes[focusIndex];

        // filter out all nodes that are not reachable in the specified direction
        focusableNodes = focusableNodes.filter(function (nodePosition) {
            switch (direction) {
                case 'left':
                    return nodePosition.left + nodePosition.width <= focusPosition.left + 1; // add 1 pixel to avoid browser rounding effects
                case 'right':
                    return nodePosition.right + nodePosition.width <= focusPosition.right + 1;
                case 'up':
                    return nodePosition.top + nodePosition.height <= focusPosition.top + 1;
                case 'down':
                    return nodePosition.bottom + nodePosition.height <= focusPosition.bottom + 1;
            }
        });

        // return early, if no focusable nodes left
        if (focusableNodes.length === 0) { return; }

        // insert the centered anchor position into the focus position, if available
        (function () {
            var anchorNode = vertical ? anchorVertNode : anchorHorNode;
            var anchorPosition = anchorNode ? getNodePositionInPage(anchorNode) : null;
            if (anchorPosition) {
                if (vertical) {
                    focusPosition.centerX = anchorPosition.left + anchorPosition.width / 2;
                } else {
                    focusPosition.centerY = anchorPosition.top + anchorPosition.height / 2;
                }
            }
        }());

        // sort nodes by distance to current focus node
        focusableNodes = _.sortBy(focusableNodes, function (nodePosition) {
            return Math.pow(nodePosition.centerX - focusPosition.centerX, 2) + Math.pow(nodePosition.centerY - focusPosition.centerY, 2);
        });

        // clear the old anchor node for the opposite direction
        if (vertical) { anchorHorNode = null; } else { anchorVertNode = null; }

        // focus the nearest node
        setFocusAndResetAnchor(focusableNodes[0].node);
    }

    function keyDownHandler(event) {

        // ignore all keyboard events with any modifier keys (only plain cursor keys supported)
        if (hasModifierKeys(event)) { return; }

        // LEFT/RIGHT cursor keys (not in text fields)
        if (!$(event.target).is('input,textarea')) {
            switch (event.keyCode) {
                case KeyCode.LEFT_ARROW:
                    moveFocus(event.target, 'left');
                    return false;
                case KeyCode.RIGHT_ARROW:
                    moveFocus(event.target, 'right');
                    return false;
            }
        }

        // UP/DOWN keys (always)
        switch (event.keyCode) {
            case KeyCode.UP_ARROW:
                moveFocus(event.target, 'up');
                return false;
            case KeyCode.DOWN_ARROW:
                moveFocus(event.target, 'down');
                return false;
        }

        // HOME/END keys (if configured)
        if (homeEnd) {
            switch (event.keyCode) {
                case KeyCode.HOME:
                    setFocus(findFocusable(rootNode).first(), true);
                    return false;
                case KeyCode.END:
                    setFocus(findFocusable(rootNode).last(), true);
                    return false;
            }
        }
    }

    $(rootNode).first().on('keydown', keyDownHandler);
}

// control captions -------------------------------------------------------

/**
 * Creates a caption node with icons and text labels for a form control.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {string} [options.textAlign="left"]
 *    Text alignment of the label (one of "left", "center", or "right").
 *  - {boolean} [options.wrapText=false]
 *    Whether to allow label text to wrap into multiple lines.
 *  - {String} [options.icon]
 *      The CSS class name of the icon. If omitted, no icon will be shown.
 *  - {String|Object} [options.iconClasses]
 *      The names of all CSS classes to be added to the icon element, as
 *      space separated string.
 *  - {Partial<CSSStyleDeclaration>} [options.iconStyle]
 *      Additional CSS style attributes for the icon. Will be inserted into
 *      the "style" attribute of the `<i>` element. Can be a string that
 *      will be inserted into the 'style' attribute as is, or can be an
 *      object that maps CSS properties to their values. The property names
 *      can be given in camel-case notation.
 *  - {String} [options.label]
 *      The text label. If omitted, no text will be shown.
 *  - {String|Object} [options.labelClasses]
 *      The names of all CSS classes to be added to the `<span>` element,
 *      as space separated string.
 *  - {Partial<CSSStyleDeclaration>} [options.labelStyle]
 *      Additional CSS style attributes for the label. Will be inserted
 *      into the "style" attribute of the `<span>` element. Can be a string
 *      that will be inserted into the 'style' attribute as is, or can be
 *      an object that maps CSS properties to their values. The property
 *      names can be given in camel-case notation.
 *
 * @returns {JQuery}
 *  The complete caption node.
 */
export function createCaptionNode(options) {

    // create the icon element
    const $icon = options?.icon ? createIcon(options.icon, { classes: options.iconClasses, style: options.iconStyle }) : null;
    // create the label element
    const $label = options?.label ? createSpan({ classes: options.labelClasses, style: options.labelStyle, label: options.label }) : null;

    // text alignment
    const textAlign = options?.textAlign || "left";
    const classes = str.concatTokens(CAPTION_CLASS, `text-${textAlign}`, options?.wrapText ? "wrap-text" : "");

    // create the caption element
    return $(`<span class="${classes}">`).append($icon, $label);
}

/**
 * Returns the caption container node of the passed control element.
 *
 * @param {NodeOrJQuery} controlNode
 *  The form control element. If this object is a JQuery collection, uses the
 *  first node it contains.
 *
 * @returns {jQuery}
 *  The caption container node of the control element.
 */
export function getCaptionNode(controlNode) {
    return $(controlNode).find(CAPTION_SELECTOR).first();
}

/**
 * Sets or changes the icon in the captions of the passed control elements.
 *
 * @param {NodeOrJQuery} controlNode
 *  The form control element. If this object is a JQuery collection, uses the
 *  first node it contains.
 *
 * @param {string|null} iconId
 *  The icon identifier. If set to `null` or an empty string, the current icon
 *  will be removed.
 *
 * @param {CreateIconOptions} [options]
 *  Optional parameters.
 */
export function setCaptionIcon(controlNode, iconId, options) {
    const captionNode = getCaptionNode(controlNode);
    captionNode.find(ICON_SELECTOR).remove();
    if (iconId) { captionNode.prepend(createIcon(iconId, options)); }
}

/**
 * Returns the current label text of the passed control element.
 *
 * @param {NodeOrJQuery} controlNode
 *  The form control element. If this object is a JQuery collection, uses the
 *  first node it contains.
 *
 * @returns {String}
 *  The current text label.
 */
export function getCaptionText(controlNode) {
    return $(controlNode).first().find(CAPTION_SELECTOR + SPAN_SELECTOR).text();
}

/**
 * Changes the label texts in the captions of the passed control elements.
 *
 * @param {NodeOrJQuery} controlNode
 *  The form control element. If this object is a JQuery collection, uses the
 *  first node it contains.
 *
 * @param {string|null} label
 *  The new label text. If set to `null` or an empty string, the current label
 *  will be removed.
 *
 * @param {CreateElementLabelOptions} [options]
 *  Optional parameters.
 */
export function setCaptionText(controlNode, label, options) {
    const captionNode = getCaptionNode(controlNode);
    captionNode.find(SPAN_SELECTOR).remove();
    if (label) { captionNode.append(createSpan({ ...options, label })); }
}

// button elements --------------------------------------------------------

/**
 * Returns a button control as JQuery object.
 *
 * @param {object} [options]
 *  Optional parameters. Supports all options supported by the functions
 *  `createElementNode` and `createCaptionNode`. Additionally, the following
 *  options are supported:
 *  - {string} [options.role="button"]
 *    The role of the button: One of "button" (default), "checkbox", or "radio".
 *  - {boolean} [options.focusable=true]
 *    If set to `false`, the 'tabindex' attribute of the button element will be
 *    set to the value -1 in order to remove the button from the keyboard focus
 *    chain.
 *  - {string|Pair<string>} [options.bmpIcon]
 *    The URL of a bitmap icon, or two URLs for bitmap icons (first URL will be
 *    used for unselected button, second URL for selected button).
 *  - {"flat"|"boxed"} [options.checkmark]
 *    Whether to add a custom checkmark for checkboxes and radio buttons
 *    (option "role" must have been passed). Value "flat" (checkboxes only")
 *    will leave empty space for unselected checkboxes, and will draw a flat
 *    checkmark for selected checkboxes. Value "boxed" will paint a small box
 *    (square for checkboxes, circle for radio buttons) for unselected buttons,
 *    that will be filled with color and a checkmark icon when selected.
 *
 * @returns {JQuery}
 *  The button control, as JQuery object.
 */
export function createButtonNode(options) {

    var attributes = options?.attributes;
    var focusable = options?.focusable ?? true;

    // add more element attributes, return the mark-up
    const role = options?.role || "button";
    attributes = { tabindex: focusable ? 0 : -1, role, ...attributes };
    if (!focusable) { attributes = { 'aria-hidden': true, ...attributes }; }
    attributes.class = str.concatTokens(BUTTON_CLASS, "btn", attributes?.class, options?.classes);

    var ariaOwns = options?.["aria-owns"];
    if (ariaOwns) { attributes.ariaOwns = ariaOwns; }

    if (options?.href) {
        attributes.href = options.href;
        attributes.target = options.target ?? "_blank";
        // bug 47916: prevent content spoofing
        // see: https://mathiasbynens.github.io/rel-noopener/
        // and: https://github.com/danielstjules/blankshield
        if (attributes.target === '_blank') {
            attributes.rel = 'noopener';
        }
    }

    // create the empty button element
    const $buttonNode = createElementNode("a", { ...options, attributes });

    // create a custom checkmark for checkboxes and radio buttons
    const checkmark = options?.checkmark;
    if (checkmark && ((role === "checkbox") || (role === "radio"))) {
        $buttonNode.addClass("custom-checkmark");
        if (checkmark === "boxed") { $buttonNode.addClass("boxed-mode"); }
        const $checkmark = $('<span class="checkmark">').appendTo($buttonNode);
        if (checkmark !== "boxed") {
            $checkmark.append(createIcon((role === "radio") ? "bi:circle-fill" : "bi:check"));
        }
    }

    // set custom bitmap icons (before the caption element)
    if (options?.bmpIcon) {
        const $btnIcons = $('<span class="bmp-icons">').appendTo($buttonNode);
        const icon1Url = is.string(options.bmpIcon) ? options.bmpIcon : options.bmpIcon[0];
        const icon2Url = is.string(options.bmpIcon) ? null : options.bmpIcon[1];
        const icon1Classes = str.concatTokens("for-unselected", icon2Url ? null : "for-selected");
        $btnIcons.append(`<img class="${icon1Classes}" src="${icon1Url}">`);
        if (icon2Url) { $btnIcons.append(`<img class="for-selected" src="${icon2Url}">`); }
    }

    // create the caption element
    return $buttonNode.append(createCaptionNode(options));
}

/**
 * Returns a checkbox control as JQuery object.
 *
 * @param {object} [options]
 *  All options supported by `createButtonNode()`, except for option "role"
 *  which is fixed to "checkbox". Default value for option "checkmark" is
 *  "boxed".
 *
 * @returns {JQuery}
 *  The checkbox control, as JQuery object.
 */
export function createCheckboxNode(options) {
    const $button = createButtonNode({ checkmark: "boxed", ...options, role: "checkbox" });
    checkButtonNodes($button, false);
    return $button;
}

/**
 * Returns a radio button control as JQuery object.
 *
 * @param {object} [options]
 *  All options supported by `createButtonNode()`, except for option "role"
 *  which is fixed to "radio", and option "checkmark" fixed to "boxed".
 *
 * @returns {JQuery}
 *  The radio button control, as JQuery object.
 */
export function createRadioButtonNode(options) {
    const $button = createButtonNode({ ...options, role: "radio", checkmark: "boxed" });
    checkButtonNodes($button, false);
    return $button;
}

/**
 * Returns the value stored in the 'value' data attribute of the specified
 * button element. If the stored value is a function, calls that function
 * (with the button node as first parameter) and returns its result.
 *
 * @param {HTMLElement|jQuery} buttonNode
 *  The button element. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @returns {Any}
 *  The value stored at in the button element.
 */
export function getButtonValue(buttonNode) {
    var value = $(buttonNode).first().data('value');
    return _.isFunction(value) ? value($(buttonNode)) : value;
}

/**
 * Stores the passed value in the 'value' data attribute of the specified
 * button elements.
 *
 * @param {HTMLElement|jQuery} buttonNode
 *  The button element. If this object is a jQuery collection, modifies all
 *  nodes it contains.
 *
 * @param {unknown} value
 *  A value, object, or function that will be copied to the 'value' data
 *  attribute of the button element (see method Forms.getButtonValue() for
 *  details).
 *
 * @param {ControlValueSerializerFn} [serializerFn]
 *  A callback function that converts the control value to a string. By
 *  default, the value will be serialized with `JSON.stringify`.
 */
export function setButtonValue(buttonNode, value, serializerFn) {
    var $buttonNode = $(buttonNode).data('value', value);
    $buttonNode.attr('data-value', serializeControlValue(value, serializerFn));
}

/**
 * Returns the configuration options of a button element.
 *
 * @param {NodeOrJQuery} buttonNode
 *  The button element.
 *
 * @returns {object}
 *  The configuration options of the button element.
 */
export function getButtonOptions(buttonNode) {
    return $(buttonNode).data("options") ?? {};
}

/**
 * Updates the configuration options of a button element.
 *
 * @param {NodeOrJQuery} buttonNode
 *  The button element.
 *
 * @param {object} options
 *  The configuration options to be merged with the existing options.
 *
 * @returns {object}
 *  The merged configuration options of the button element.
 */
export function setButtonOptions(buttonNode, options) {
    const mergedOptions = { ...$(buttonNode).data("options"), ...options };
    $(buttonNode).data("options", mergedOptions);
    return mergedOptions;
}

/**
 * Filters the passed button elements by comparing to a specific value.
 *
 * @param {HTMLElement|jQuery} buttonNodes
 *  The button elements to be filtered. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Any} value
 *  The button value to be filtered for.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Function} [options.matcher=_.isEqual]
 *      A comparison function that returns whether the actual button values
 *      should be included in the result. Receives the passed value as
 *      first parameter, and the value of the current button element as
 *      second parameter. The function must return the Boolean value true
 *      in order to include the respective button element in the result. If
 *      omitted, uses _.isEqual() which compares arrays and objects deeply.
 *
 * @returns {jQuery}
 *  A jQuery collection with all button nodes whose value is equal to the
 *  passed value.
 */
export function filterButtonNodes(buttonNodes, value, options) {
    var matcher = options?.matcher || _.isEqual;
    return $(buttonNodes).filter(function () {
        return matcher(value, getButtonValue(this)) === true;
    });
}

/**
 * Returns whether the specified button element is in selected/checked
 * state.
 *
 * @param {HTMLElement|jQuery} buttonNode
 *  The button element. If this object is a jQuery collection, uses the
 *  first node it contains.
 *
 * @returns {Boolean}
 *  Whether the button node is selected/checked.
 */
export function isCheckedButtonNode(buttonNode) {
    return $(buttonNode).first().is(BUTTON_SELECTOR + DATA_CHECKED_SELECTOR);
}

/**
 * Filters the selected/checked button elements from the passed jQuery
 * collection.
 *
 * @param {HTMLElement|jQuery} buttonNodes
 *  The button elements to be filtered for.
 *
 * @returns {jQuery}
 *  A jQuery collection with all selected/checked button elements.
 */
export function filterCheckedButtonNodes(buttonNodes) {
    return $(buttonNodes).filter(BUTTON_SELECTOR + DATA_CHECKED_SELECTOR);
}

/**
 * Filters the unselected/unchecked button elements from the passed jQuery
 * collection.
 *
 * @param {HTMLElement|jQuery} buttonNodes
 *  The button elements to be filtered for.
 *
 * @returns {jQuery}
 *  A jQuery collection with all unselected/unchecked button elements.
 */
export function filterUncheckedButtonNodes(buttonNodes) {
    return $(buttonNodes).filter(BUTTON_SELECTOR + ':not(' + DATA_CHECKED_SELECTOR + ')');
}

/**
 * Selects or deselects the passed button elements, or sets them into the
 * mixed state in case the value represented by the buttons is ambiguous.
 *
 * @param {HTMLElement|jQuery} buttonNodes
 *  The button elements to be manipulated. If this object is a jQuery
 *  collection, uses all nodes it contains.
 *
 * @param {Boolean|Null} state
 *  The new state of the button nodes. The Boolean value true results in a
 *  selected/checked design. The Boolean value false results in an
 *  unselected/unchecked design. The value null is treated as ambiguous
 *  state and results in a specific icon, if the option 'tristate' is set.
 *  Otherwise, this will be the unselected/unchecked design.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {boolean} [options.tristate=false]
 *    If set to `true`, a third "ambiguous" state (value `null`) will be
 *    represented in a specific "tristate" style. Without this option, the
 *    design for the unselected state will be used for the ambiguous state.
 */
export function checkButtonNodes(buttonNodes, state, options) {

    // selected state (without ambiguous state)
    const selected = state === true;
    // effective ambiguous state (only if supported in passed options)
    const ambiguous = (state === null) && !!options?.tristate;

    buttonNodes = $(buttonNodes);

    // update button contents/formatting according to design mode
    buttonNodes.get().forEach(buttonNode => {
        const $checkmark = $(buttonNode).find('>.checkmark');
        if ($checkmark.length) {
            $checkmark.attr("data-state", ambiguous ? "null" : String(state));
        } else {
            selectNodes(buttonNode, selected);
            $(buttonNode).attr('aria-selected', selected);
        }
    });

    // update button states
    buttonNodes.toggleClass(AMBIGUOUS_CLASS, ambiguous);
    buttonNodes.attr(DATA_CHECKED_ATTR, selected);

    // update aria attributes
    if (options?.haspopup) {
        buttonNodes.attr({ 'aria-haspopup': true, 'aria-expanded': selected });
    } else {
        buttonNodes.attr({ 'aria-pressed': selected, 'aria-checked': selected });
    }
}

/**
 * Selects or deselects the button elements with a specific value.
 *
 * @param {HTMLElement|jQuery} buttonNodes
 *  The button elements to be selected or deselected. If this object is a
 *  jQuery collection, uses all nodes it contains.
 *
 * @param {Any} value
 *  The button value to be filtered for.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Function} [options.matcher=_.isEqual]
 *      A comparison function that returns whether the actual button values
 *      should be selected, deselected, or skipped. Receives the passed
 *      value as first parameter, and the value of the current button
 *      element as second parameter. The function must return the Boolean
 *      value true to select the respective button element, the Boolean
 *      value false to deselect the button element, or any other value to
 *      leave the button element unmodified. If omitted, uses _.isEqual()
 *      which compares arrays and objects deeply, and does not skip any
 *      button element.
 *  - {Boolean} [options.multiSelect=true]
 *      If false it stops if the first node matched the value, otherwise
 *      all nodes will be checked.
 */
export function checkMatchingButtonNodes(buttonNodes, value, options) {

    // the matcher predicate function
    var matcher = options?.matcher || _.isEqual;
    var multiSelect = options?.multiSelect ?? true;
    var firstMatched = false;

    $(buttonNodes).each(function () {
        var state = matcher(value, getButtonValue(this));
        if (_.isBoolean(state)) {
            if (state && !multiSelect) {
                if (firstMatched) {
                    state = false;
                }
                firstMatched = true;
            }
            checkButtonNodes(this, state, options);
        }
    });
}

/**
 * Adds a keyboard event handler to the passed button elements, which
 * listens to the ENTER and SPACE keys, and triggers 'click' events at the
 * target node of the received event. The event object will contain the
 * additional property 'keyCode' set to the correct key code. Bubbling of
 * the original keyboard events ('keydown', 'keypress', and 'keyup') will
 * be suppressed, as well as the default browser action. The 'click' event
 * will be triggered after receiving the 'keyup' event.
 *
 * @param {HTMLElement|jQuery} nodes
 *  The nodes to bind the event handles to. May contain button elements, or
 *  any other container elements with descendant button elements.
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.tab=false]
 *      If set to true, 'click' events will also be triggered for the TAB
 *      key and the SHIFT+TAB key combination (no other modifier keys must
 *      be pressed).
 */
export function setButtonKeyHandler(nodes, options) {

    // target node of the last caught 'keydown' event
    var lastKeyDownTarget = null;

    // Bug 28528: ENTER key must be handled explicitly, <a> elements without
    // 'href' attribute do not trigger click events. The 'href' attribute
    // has been removed from the buttons to prevent useless tooltips with the
    // link address. Additionally, SPACE key must always be handled manually.

    // handler for the 'keyup' events
    function keyHandler(event) {

        // whether the target of a 'keyup' event matches the target of preceding 'keydown'
        var isKeyUpWithMatchingTarget = false;

        // Bug 37870: Remember target node of a 'keydown' event, check that 'keyup' event is for the
        // same target. Needed to prevent triggering a click event when pressing ENTER one one node,
        // the changing the browser focus to one of the nodes covered by this method, then receiving
        // the 'keyup' event. This happens for example when pressing the ENTER key on the OK button
        // of a modal dialog. The 'keydown' event causes to close the dialog which restores the old
        // focus node, e.g. a button in a tool bar. The following 'keyup' event for the ENTER key
        // would immediately trigger that button which will open the modal dialog again.
        if (event.type === 'keydown') {
            lastKeyDownTarget = event.target;
        } else if (event.type === 'keyup') {
            isKeyUpWithMatchingTarget = lastKeyDownTarget === event.target;
            lastKeyDownTarget = null;
        }

        // ENTER and SPACE key: wait for keyup event
        if (hasKeyCode(event, 'ENTER', 'SPACE')) {
            if (isKeyUpWithMatchingTarget) {
                triggerClickForKey(event);
            }
            return false; // stop propagation of ALL keyboard events
        }

        if (options?.tab && (event.type === 'keydown') && matchKeyCode(event, 'TAB', { shift: null })) {
            triggerClickForKey(event);
            // let all TAB key events bubble up for focus navigation
        }
    }

    // directly bind the event handler to all button nodes
    $(nodes).filter(BUTTON_SELECTOR).on('keydown keypress keyup', keyHandler);
    // bind the event handler to all descendant button nodes
    $(nodes).not(BUTTON_SELECTOR).on('keydown keypress keyup', BUTTON_SELECTOR, keyHandler);
}

// text input elements ----------------------------------------------------

/**
 * Creates an `<input>` element.
 *
 * @param {Object} [options]
 *  Optional parameters. Supports all options supported by the method
 *  Forms.createElementNode(). Additionally, the following options are
 *  supported:
 *  - {String} [options.placeholder='']
 *      A place holder text that will be shown in an empty text field.
 *  - {String} [options.keyboard='text']
 *      Specifies which virtual keyboard should occur on touch devices.
 *      Supported types are 'text' (default) for a generic text field with
 *      alphanumeric keyboard, 'number' for floating-point numbers with
 *      numeric keyboard, 'url' for an alphanumeric keyboard with additions
 *      for entering URLs, or 'email' for an alphanumeric keyboard with
 *      additions for entering e-mail addresses.
 *  - {Number} [options.maxLength]
 *      If specified and a positive number, sets the 'maxLength' attribute
 *      of the text field, used to restrict the maximum length of the text
 *      that can be inserted into the text field.
 *
 * @returns {JQuery}
 *  The new text field element.
 */
export function createInputNode(options) {

    const keyboard = options?.keyboard ?? 'text';

    // add more element attributes, return the mark-up
    const attributes = {
        type: TOUCH_DEVICE ? keyboard : 'text',
        tabindex: options?.tabindex || 0,
        class: str.concatTokens('form-control', (keyboard === 'number') ? 'text-right' : null),
        ...options?.attributes
    };

    if (options?.placeholder) {
        attributes.placeholder = options?.placeholder;
    }

    var maxLength = options?.maxLength || 0;
    if (maxLength > 0) { attributes.maxlength = maxLength; }

    if (IOS_SAFARI_DEVICE) {
        attributes.autocorrect = 'off';
        attributes.autocapitalize = 'none';
    }

    return createElementNode('input', { ...options, attributes });
}

// simple or bootstrap elements -------------------------------------------

/**
 * Creates a simple `<select>` list control with an optional leading label.
 *
 * @param {Array} entries
 *  An array with descriptor objects for all list entries. Each object MUST
 *  contain the following properties:
 *  - {string} value
 *      The value associated to the list item.
 *  - {string} label
 *      The text label for the list item.
 *
 * @returns {JQuery}
 *  The complete `<select>` element group.
 */
export function createSelectListNode(entries) {
    const selectNode = createElement("select", "form-control");
    entries.forEach(entry => {
        const optionNode = selectNode.appendChild(createElement("option", { label: entry.label }));
        optionNode.setAttribute("value", entry.value);
    });
    return $(selectNode);
}

/**
 * A special event listener function which works for touch and click events
 * and prevents unwanted behaviour from jQuery invoked events on touch devices.
 * It uses the 'faster' tap event on touch devices and a click otherwise.
 *
 * We are in a jQuery/jQuery mobile environment, so native touch events invoke a lot
 * of unnecessary events due to jQuery. The invoked events can even be different on
 * a longer tap or a short tap. There is also some odd behaviour when the
 * focus is changed while jQuery still invokes it's events.
 * All this could lead to unwanted behaviour (e.g. the famous jQuery 'ghost click',
 * or when a menu is closed on touchend, the following events are 'routed'
 * to the underlying target).
 *
 *
 * @param {JQuery} node
 *  The 'node' for the event listener.
 *
 * @param {string} [delegate]
 *  The jQuery selector for delegation (see .on() function).
 *
 * @param {Function} handler
 *  The handler function which is attached to the event listener.
 */
export function touchAwareListener(node, delegate, handler) {

    // normalize parameters
    if (_.isFunction(delegate)) {
        handler = delegate;
        delegate = null;
    }

    var listener = handler;

    if (DEFAULT_CLICK_TYPE === 'tap') {
        listener = function (e) {
            handler.call(this, e);
            e.preventDefault();
        };
    }

    if (delegate) {
        node.on(DEFAULT_CLICK_TYPE, delegate, listener);
    } else {
        node.on(DEFAULT_CLICK_TYPE, listener);
    }
}
