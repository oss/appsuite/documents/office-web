/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// known bug in LESS plugin: https://github.com/ssivanatarajan/stylelint-less/issues/6
/* stylelint-disable less/color-no-invalid-hex, less/no-duplicate-variables */

/* ========================================================================= */
/* Media Breakpoints                                                         */
/* ========================================================================= */

/**
 * Adds a media query that applies the passed ruleset for smartphones only.
 */
.media-smartphone-only(@rules) {
    @smartphone-breakpoint: 540px; // same as "$smartphone-breakpoint" in Core UI
    @media all and (max-width: @smartphone-breakpoint), all and (max-device-width: @smartphone-breakpoint) {
        @rules();
    }
}

/* ========================================================================= */
/* Element Positioning                                                       */
/* ========================================================================= */

.absolute-with-distance(@top; @right; @bottom; @left) {
    position: absolute;
    top: @top;
    right: @right;
    bottom: @bottom;
    left: @left;
}

.absolute-with-distance(@top; @left-right; @bottom) {
    .absolute-with-distance(@top; @left-right; @bottom; @left-right);
}

.absolute-with-distance(@top-bottom; @left-right) {
    .absolute-with-distance(@top-bottom; @left-right; @top-bottom; @left-right);
}

.absolute-with-distance(@distance) {
    .absolute-with-distance(@distance; @distance; @distance; @distance);
}

.flex-center(@direction: row) {
    display: flex;
    flex-direction: @direction;
    align-items: center;
    justify-content: center;
}

/* ========================================================================= */
/* Text Effects                                                              */
/* ========================================================================= */

/**
 * Sets overflow mode to hidden: Text contents will be rendered in a single
 * line (white-space mode "nowrap") and shortened with an ellipsis character.
 */
.text-overflow-hidden(@white-space: nowrap) {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: @white-space;
}

/* ========================================================================= */
/* Border Effects                                                            */
/* ========================================================================= */

/**
 * Shortcut for setting a border radius at top-left and top-right corner.
 */
.border-top-radius(@size) {
    border-top-left-radius: @size;
    border-top-right-radius: @size;
}

/**
 * Shortcut for setting a border radius at bottom-left and bottom-right corner.
 */
.border-bottom-radius(@size) {
    border-bottom-left-radius: @size;
    border-bottom-right-radius: @size;
}

/**
 * Shortcut for setting a border radius at top-left and bottom-left corner.
 */
.border-left-radius(@size) {
    border-top-left-radius: @size;
    border-bottom-left-radius: @size;
}

/**
 * Shortcut for setting a border radius at top-right and bottom-right corner.
 */
.border-right-radius(@size) {
    border-top-right-radius: @size;
    border-bottom-right-radius: @size;
}

/* ========================================================================= */
/* Fill Effects                                                              */
/* ========================================================================= */

/**
 * Applies the standard colors for menu buttons of open dropdown menus to the
 * matching elements.
 */
.dropdown-open-colors() {
    background: var(--dropdown-open-fill-color);
    border-color: var(--dropdown-open-border-color);
}

/**
 * Applies the standard list selection colors to the matching elements.
 */
.list-selected-colors() {
    background: var(--listmenu-selected-fill-color);
    color: var(--listmenu-selected-text-color);
}

/* ========================================================================= */
/* Shadow Effects                                                            */
/* ========================================================================= */

/**
 * Shadow effect for elements hovering over their background, evenly around the
 * element.
 */
.box-shadow(@size: 5px) {
    box-shadow: 0 0 @size fade(black, 20%);
}

/**
 * Shadow effect for elements hovering over their background, dropped to
 * bottom.
 */
.drop-shadow(@size: 5px) {
    box-shadow: 0 @size (2 * @size) fade(black, 20%);
}

/**
 * Heavy shadow effect for elements hovering over their background, dropped to
 * bottom.
 */
.drop-shadow-heavy() {
    box-shadow: 0 15px 70px 10px fade(black, 50%);
}

/* ========================================================================= */
/* Hover Effects                                                             */
/* ========================================================================= */

.hover-overlay-effect(@overlay-color: var(--hover-overlay-color)) {
    &::after {
        content: "";
        .absolute-with-distance(0);
        background: @overlay-color;
        border-radius: inherit;
        pointer-events: none;
    }
}

.hover-overlay(@overlay-color: var(--hover-overlay-color); @custom-rules: {}) {
    &:hover, &:active {
        .hover-overlay-effect(@overlay-color);
        @custom-rules();
    }
}

.hover-overlay-off(@custom-rules: {}) {
    &:hover, &:active {
        &::after {
            background: transparent !important;
        }
        @custom-rules();
    }
}

/* ========================================================================= */
/* Focus Effects                                                             */
/* ========================================================================= */

/**
 * Removes browser-internal focus outline effects from the current element.
 */
.focus-no-outline() {
    &:focus, &:active {
        outline: none;
    }
}

// Border and shadow effect ---------------------------------------------------

/**
 * Adds a border color and box shadow with the standard focus colors to the
 * selected elements.
 */
.focus-border-shadow-effect() {
    border-color: var(--focus-border-color);
    box-shadow: var(--focus-box-shadow);
    outline: none;
}

/**
 * Adds a border color and box shadow to the selected elements, while they are
 * focused but not active.
 */
.focus-border-shadow(@custom-rules: {}) {
    &:focus:not(:active) {
        .focus-border-shadow-effect();
        @custom-rules();
    }
}

/**
 * Adds a border color and box shadow to the selected elements, while they are
 * focused or active.
 */
.focus-active-border-shadow(@custom-rules: {}) {
    &:focus, &:active {
        .focus-border-shadow-effect();
        @custom-rules();
    }
}

// Double-shadow effect -------------------------------------------------------

/**
 * Adds a two box shadows (solid one-pixel line, and semi-transparent outline)
 * with the standard focus colors to the selected elements. Useful for elements
 * with their own custom border that shall not be re-used for focus display.
 */
.focus-shadow-shadow-effect() {
    box-shadow: 0 0 0 1px var(--focus-border-color), var(--focus-box-shadow);
    outline: none;
}

/**
 * Adds two box shadows (solid one-pixel line, and semi-transparent outline) to
 * the selected elements, while they are focused but not active. Useful for
 * elements with their own custom border that shall not be re-used for focus
 * display.
 */
.focus-shadow-shadow(@custom-rules: {}) {
    &:focus:not(:active) {
        .focus-shadow-shadow-effect();
        @custom-rules();
    }
}

/**
 * Adds two box shadows (solid one-pixel line, and semi-transparent outline) to
 * the selected elements, while they are focused or active. Useful for elements
 * with their own custom border that shall not be re-used for focus display.
 */
.focus-active-shadow-shadow(@custom-rules: {}) {
    &:focus, &:active {
        .focus-shadow-shadow-effect();
        @custom-rules();
    }
}
