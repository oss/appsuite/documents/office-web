/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixin/generics";

/* ========================================================================= */
/* Selection Border Handles                                                  */
/* ========================================================================= */

/**
 * Adds a fill color and a hover glow effect to border handle elements used
 * e.g. in a selection box of a specific object node.
 *
 * This mixin must be applied to the root node with a child border container
 * node marked with the CSS class "borders". The border container node must
 * contain border handle nodes, each with the attribute "data-pos" containing
 * the position of the border handle.
 *
 * @handle-color is the CSS color for the border handles.
 *
 * If @hover-glow is set to `true`, and one of the border handles is hovered
 * with the mouse, all border handles will show a light glow effect.
 *
 * If @active-glow is set to `true`, all border handles will show a more
 * intense glow effect while the root node contains the CSS class
 * "tracking-active".
 */
.border-handle-color-styles(
    @handle-color;
    @hover-glow: false;
    @active-glow: false;
) {

    /* border color */
    > .borders > [data-pos]::before {
        background-color: @handle-color;
    }

    /* glow effect for hovering border handles */
    &:not(.tracking-active) > .borders:hover > [data-pos]::before when (@hover-glow) {
        box-shadow: 0 0 2px @handle-color;
    }

    /* glow effect for border handles while tracking is active */
    &.tracking-active > .borders > [data-pos]::before when (@active-glow) {
        box-shadow: 0 0 3px @handle-color;
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Adds formatting styles to border handle elements used e.g. in a selection
 * box of a specific object node.
 *
 * This mixin must be applied to the root node with a child border container
 * node marked with the CSS class "borders". The border container node must
 * contain border handle nodes, each with the attribute "data-pos" containing
 * the position of the border handle. Supported values are "l", "r", "t", or
 * "b".
 *
 * @line-width is the width or height (according to the position) of the
 * visible border line.
 *
 * @hover-padding is the additional area around the border line that will cause
 * the hover effect. The resulting size of the border handle nodes will include
 * the specified line width and twice the padding size.
 *
 * @handle-offset is the position offset of the border handles relative to the
 * default position. By default, the border lines will be placed centered on
 * top of the root node borders (if the line width is an odd number of pixels,
 * the middle pixel of the line will be placed on the outer pixel inside the
 * root node). Positive values for this parameter will shift the lines inside
 * the area of the root node, negative values will shift the lines outside.
 *
 * @handle-color is the color of the border line and glow effect. Can be set to
 * the value `null` to skip setting any color attributes.
 *
 * If @hover-glow is set to `true`, and one of the border handles is hovered
 * with the mouse, all border handles will show a light glow effect.
 *
 * If @active-glow is set to `true`, all border handles will show a more
 * intense glow effect while the root node contains the CSS class
 * "tracking-active".
 *
 * If @move-cursor is set to `true`, the mouse pointer of the border lines will
 * be set to the value "move". Otherwise, the mouse pointer will be derived
 * from the root node.
 */
.border-handle-styles(
    @line-width;
    @hover-padding;
    @handle-offset;
    @handle-color;
    @hover-glow: false;
    @active-glow: false;
    @move-cursor: false;
) {

    @outer-shift: (@handle-offset - (@line-width / 2) - @hover-padding);
    @total-size: (@line-width + 2 * @hover-padding);

    > .borders {
        position: static;
        width: 0;
        height: 0;
        overflow: visible;

        > [data-pos] {
            position: absolute;

            &[data-pos="l"], &[data-pos="r"] {
                top: (@outer-shift + @line-width);
                bottom: (@outer-shift + @line-width);
                width: @total-size;
            }

            &[data-pos="t"], &[data-pos="b"] {
                left: @outer-shift;
                right: @outer-shift;
                height: @total-size;
            }

            &[data-pos="l"] { left: @outer-shift; }
            &[data-pos="r"] { right: @outer-shift; }
            &[data-pos="t"] { top: @outer-shift; }
            &[data-pos="b"] { bottom: @outer-shift; }

            /* add move cursor if specified */
            & when (@move-cursor) { cursor: move; }

            /* visible part of the border handle */
            &::before {
                content: "";
                .absolute-with-distance(@hover-padding);
            }
        }
    }

    /* add all color-dependent styles */
    & when not (@handle-color = null) {
        .border-handle-color-styles(@handle-color; @hover-glow; @active-glow);
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Adds a z-index for the border handle elements.
 */
.border-handle-z-index(@z-index) {
    > .borders > [data-pos] {
        z-index: @z-index;
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Allows to add a complex background style for the border handle elements.
 */
.border-handle-background-style(@background) {
    > .borders > [data-pos]::before {
        background: @background;
    }
}

/* ========================================================================= */
/* Selection Resizer Handles                                                 */
/* ========================================================================= */

/**
 * Adds a border color and a hover glow effect to the resizer handle elements
 * used e.g. in a selection box of a specific object node.
 *
 * This mixin must be applied to the root node with a child resizer container
 * node marked with the CSS class "resizers". The resizer container node must
 * contain resizer handle nodes, each with the attribute "data-pos" containing
 * the position of the resizer handle. Supported values are "tl", "tr", "bl",
 * or "br" for corner positions, and "l", "r", "t", or "b" for the center
 * positions of the root node borders.
 *
 * @handle-color is the color of the border line and glow effect of the resizer
 * elements.
 *
 * If @hover-glow is set to `true`, and one of the resizer handles is hovered
 * with the mouse, all resizer handles will show a light glow effect.
 *
 * If @active-glow is set to `true`, all resizer handles will show a more
 * intense glow effect while the root node contains the CSS class
 * "tracking-active".
 */
.resizer-handle-color-styles(
    @handle-color;
    @hover-glow: false;
    @active-glow: false;
) {

    /* border color */
    > .resizers > [data-pos]::before {
        border-color: @handle-color;
    }

    /* glow effect for hovering resizer handles */
    &:not(.tracking-active) > .resizers:hover > [data-pos]::before when (@hover-glow) {
        box-shadow: 0 0 3px @handle-color;
    }

    /* glow effect for resizer handles while tracking is active */
    &.tracking-active > .resizers > [data-pos]::before when (@active-glow) {
        box-shadow: 0 0 4px @handle-color;
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Adds formatting styles to resizer handle elements used e.g. in a selection
 * box of a specific object node.
 *
 * This mixin must be applied to the root node with a child resizer container
 * node marked with the CSS class "resizers". The resizer container node must
 * contain resizer handle nodes, each with the attribute "data-pos" containing
 * the position of the resizer handle. Supported values are "tl", "tr", "bl",
 * or "br" for corner positions, and "l", "r", "t", or "b" for the center
 * positions of the root node borders.
 *
 * @handle-size is the width and height of the visible area of the resizer.
 *
 * @hover-padding is the additional area around the resizer that will cause the
 * hover effect. The resulting size of the resizer handle nodes will include
 * the specified visible size and twice the padding size.
 *
 * @line-width is the line width of the resizer handle borders.
 *
 * @handle-offset is the position offset of the resizer handles relative to the
 * default position. By default, the handles will be placed centered on top of
 * the root node borders (if the size of the handles is an odd number of
 * pixels, the middle pixel of the handle will be placed on the outer pixel
 * inside the root node). Positive values for this parameter will shift the
 * handles inside the area of the root node, negative values will shift the
 * handles outside.
 *
 * @handle-color is the color of the border line and glow effect of the
 * resizer. Can be set to the value `null` to skip setting any color
 * attributes.
 *
 * If @hover-glow is set to `true`, and one of the resizer handles is hovered
 * with the mouse, all resizer handles will show a light glow effect.
 *
 * If @active-glow is set to `true`, all resizer handles will show a more
 * intense glow effect while the root node contains the CSS class
 * "tracking-active".
 *
 * If @move-cursor is set to `true`, the mouse pointer of the resizer handles
 * will be set to appropriate values according to the position of the handles.
 * Otherwise, the mouse pointer will be derived from the root node.
 */
.resizer-handle-styles(
    @handle-size;
    @hover-padding;
    @line-width;
    @handle-offset;
    @handle-color;
    @hover-glow: false;
    @active-glow: false;
    @move-cursor: false;
) {

    /* round pixels but not "em"s to integer */
    @half-size: if(ispixel(@handle-size), floor(@handle-size / 2), (@handle-size / 2));
    @outer-shift: (@handle-offset - @half-size - @hover-padding);
    @total-size: (@handle-size + 2 * @hover-padding);

    > .resizers {
        position: static;
        width: 0;
        height: 0;
        overflow: visible;

        > [data-pos] {
            position: absolute;
            width: @total-size;
            height: @total-size;

            &[data-pos="tl"], &[data-pos="t"], &[data-pos="tr"] { top: @outer-shift; }
            &[data-pos="l"], &[data-pos="r"] { top: 50%; margin-top: @outer-shift; }
            &[data-pos="bl"], &[data-pos="b"], &[data-pos="br"] { bottom: @outer-shift; }

            &[data-pos="tl"], &[data-pos="l"], &[data-pos="bl"] { left: @outer-shift; }
            &[data-pos="t"], &[data-pos="b"] { left: 50%; margin-left: @outer-shift; }
            &[data-pos="tr"], &[data-pos="r"], &[data-pos="br"] { right: @outer-shift; }

            /* add move cursor if specified */
            & when (@move-cursor) {
                &[data-pos="tl"] { cursor: nw-resize; }
                &[data-pos="t"] { cursor: n-resize; }
                &[data-pos="tr"] { cursor: ne-resize; }
                &[data-pos="r"] { cursor: e-resize; }
                &[data-pos="br"] { cursor: se-resize; }
                &[data-pos="b"] { cursor: s-resize; }
                &[data-pos="bl"] { cursor: sw-resize; }
                &[data-pos="l"] { cursor: w-resize; }
            }

            /* visible part of the resizer handle */
            &::before {
                content: "";
                .absolute-with-distance(@hover-padding);
                background-color: white;
                border: @line-width solid black;
                border-radius: 50%;
                box-shadow: 0 0 0 @line-width fade(white, 50%);
            }
        }
    }

    /* add all color-dependent styles */
    & when not (@handle-color = null) {
        .resizer-handle-color-styles(@handle-color; @hover-glow; @active-glow);
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Adds a z-index for the resizer handle elements.
 */
.resizer-handle-z-index(@z-index) {
    > .resizers > [data-pos] {
        z-index: @z-index;
    }
}

/* ========================================================================= */
/* Selection Rotation Handles                                                */
/* ========================================================================= */

/**
 * Adds a border color and a hover glow effect for the rotation handle.
 *
 * This mixin must be applied to the root node with a child rotate handle node
 * marked with the CSS class "rotate-handle".
 *
 * @handle-color is the color of the border line and glow effect of the resize
 * handle and its connector line.
 *
 * If @hover-glow is set to `true`, and the rotate handle is hovered with the
 * mouse, it will show a light glow effect.
 *
 * If @active-glow is set to `true`, the rotate handle will show a more intense
 * glow effect while the root node contains the CSS class "tracking-active".
 */
.rotate-handle-color-styles(
    @handle-color;
    @hover-glow: false;
    @active-glow: false;
) {

    /* border color of the vertical line, and the handle circle */
    > .rotate-handle {
        &::before { background-color: fade(@handle-color, 70%); }
        &::after { border-color: @handle-color; }
    }

    /* glow effect for hovering border handle */
    &:not(.tracking-active) > .rotate-handle:hover::after when (@hover-glow) {
        box-shadow: 0 0 3px @handle-color;
    }

    /* glow effect for rotate handle while tracking is active */
    &.tracking-active > .rotate-handle::after when (@active-glow) {
        box-shadow: 0 0 4px @handle-color;
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Adds formatting styles to the rotate handle element used e.g. in a selection
 * box of a specific object node. The rotate handle will be connected with a
 * vertical line to its container node.
 *
 * This mixin must be applied to the root node with a child rotate handle node
 * marked with the CSS class "rotate-handle".
 *
 * @handle-size is the width and height of the visible area of the rotate
 * handle.
 *
 * @hover-padding is the additional area around the rotate handle that will
 * cause the hover effect. The resulting size of the rotate handle node will
 * include the specified visible size and twice the padding size.
 *
 * @line-width is the line width of the rotate handle borders.
 *
 * @conn-dist is the distance of the center point of the rotate handle to the
 * upper border of its container node.
 *
 * @conn-width is the line width of the connector line between the rotate
 * handle and the container node.
 *
 * @handle-color is the color of the border line and glow effect of the rotate
 * handle and its connector line. Can be set to the value `null` to skip
 * setting any color attributes.
 *
 * If @hover-glow is set to `true`, and the rotate handle is hovered with the
 * mouse, it will show a light glow effect.
 *
 * If @active-glow is set to `true`, the rotate handle will show a more intense
 * glow effect while the root node contains the CSS class "tracking-active".
 */
.rotate-handle-styles(
    @handle-size;
    @hover-padding;
    @line-width;
    @conn-dist;
    @conn-width;
    @handle-color;
    @hover-glow: false;
    @active-glow: false;
) {

    /* round pixels but not "em"s to integer */
    @half-size: if(ispixel(@handle-size), floor(@handle-size / 2), (@handle-size / 2));
    @conn-offset: if(ispixel(@conn-width), floor(@conn-width / 2), (@conn-width / 2));
    @handle-offset: (@half-size + @hover-padding);
    @total-size: (@handle-size + 2 * @hover-padding);

    > .rotate-handle {
        position: absolute;
        left: 50%;
        top: (-@conn-dist);
        margin-left: (-@handle-offset);
        margin-top: (-@handle-offset);
        width: @total-size;
        height: @total-size;
        cursor: crosshair;

        /* connector line */
        &::before {
            content: "";
            position: absolute;
            left: (@handle-offset - @conn-offset);
            top: @handle-offset;
            width: @conn-width;
            height: @conn-dist;
            background-color: black;
        }

        /* visible part of the rotate handle */
        &::after {
            content: "";
            .absolute-with-distance(@hover-padding);
            background-color: white;
            border: @line-width solid black;
            border-radius: 50%;
            box-shadow: 0 0 0 1px fade(white, 50%);
        }
    }

    /* add all color-dependent styles */
    & when not (@handle-color = null) {
        .rotate-handle-color-styles(@handle-color; @hover-glow; @active-glow);
    }
}

/* ------------------------------------------------------------------------- */

/**
 * Adds a z-index for the rotate handle element.
 */
.rotate-handle-z-index(@z-index) {
    > .rotate-handle {
        z-index: @z-index;
    }
}
