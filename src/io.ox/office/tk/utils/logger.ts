/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { locationHash } from "$/url";

import { is, math, str, unicode, fun, ary, json, jpromise, debug } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import { DEBUG, AUTOTEST, LOG_ERROR_DATA, getDebugValue, getDebugFlag, setDebugValue } from "@/io.ox/office/tk/config";
import { isNodeList, isJQuery } from "@/io.ox/office/tk/dom/domutils";
import type { AnyMethodDecorator } from "@/io.ox/office/tk/objects";

const { max, round } = Math;

// types ======================================================================

/**
 * Active logging level.
 */
export enum LogLevel {

    /**
     * Logging is disabled completely.
     */
    OFF = 0,

    /**
     * Error messages will be logged only.
     */
    ERROR = 1,

    /**
     * Error and warning messages will be logged.
     */
    WARN = 2,

    /**
     * Error, warning, and info messages will be logged.
     */
    INFO = 3,

    /**
     * Error, warning, info, and debug messages will be logged.
     */
    DEBUG = 4, // eslint-disable-line @typescript-eslint/no-shadow

    /**
     * All messages will be logged.
     */
    TRACE = 5
}

/**
 * Configuration options for instances of class `Logger`.
 */
export interface LoggerConfig {

    /**
     * Specifies whether the logger is enabled. By default, the logger is only
     * enabled in debug mode, and disabled in production mode.
     */
    enabled?: boolean;

    /**
     * Specifies the default log level of the logger to be used if the debug
     * configuration does not contain an entry. Default value is `WARN`.
     */
    defLogLevel?: LogLevel;

    /**
     * If set to `true`, all log messages will be recorded in a global message
     * list, regardless of the active log level. By default, all messages
     * written to the console will also be recorded. However, warnings and
     * error messages will *always* be recorded. Default value is `false`.
     */
    recordAll?: boolean;

    /**
     * If specified, all messages will be opened with the passed tag text. In
     * pretty console logs, the tag will be painted as colored badge, otherwise
     * it will be wrapped in bracket characters.
     */
    tag?: string;

    /**
     * The fill color for the message tags in pretty console logs, as 8-bit RGB
     * value (0xRRGGBB). Will be applied with 80% transparency to support light
     * and dark debugger themes. Default value is 0x808080 (medium gray).
     */
    tagColor?: number;
}

/**
 * Optional parameters for the decorator method `Logger#profileMethod`.
 */
export interface ProfileMethodOptions {

    /**
     * A custom log level for the message. Default value is "DEBUG".
     */
    logLevel?: LogLevel;

    /**
     * A predicate function to restrict logging to specific conditions. Every
     * truthy return value will cause to log the invocation. By default, all
     * invocations will be logged.
     */
    when?: () => unknown;
}

// private types --------------------------------------------------------------

type ParensType = "round" | "square";

interface CommandInfo {
    text?: string;
    css?: string;
    parens?: ParensType;
    prettyParens?: true;
}

type CommendProcessorFn = (value: string) => CommandInfo;

// constants ==================================================================

// prevent collapsing space characters in browser console
const INDENT_PARTICLE = "\xa0 ";

// whether to use CSS formatting for console output
const PRETTY_PRINT = !getDebugFlag("office:simple-log") && !AUTOTEST;

// maximum number of entries in the global message list
const MAX_MESSAGE_LIST_SIZE = 40_000;

// maximum number of characters per line in the global message list
const MAX_MESSAGE_LINE_SIZE = 1_000;

// maps log levels to console methods
const CONSOLE_FNS: Dict<(...args: unknown[]) => void> = {
    [LogLevel.ERROR]: (...args) => window.console.error(...args),
    [LogLevel.WARN]:  (...args) => window.console.warn(...args),
    [LogLevel.INFO]:  (...args) => window.console.info(...args),
    [LogLevel.DEBUG]: (...args) => window.console.log(...args),
    [LogLevel.TRACE]: (...args) => window.console.log(...args)
};

// regular expression for inline commands
const COMMAND_RE = /\$([a-z]+)\{((?:[^}]*\\\})*[^}]*)\}/g;

// CSS formatting for badges
const BADGE_CSS = "padding:0 4px;border:1px solid rgba(128,128,128,.2);border-radius:8px;font-size:90%;font-weight:bold;";

// all available badge code processors
const COMMAND_PROCESSORS: Dict<CommendProcessorFn> = {

    // arbitrary badge (enclosed in brackets in plain-text mode)
    badge(value) {
        return processBadge(value, { parens: "square" });
    },

    // tag badge (always enclosed in brackets)
    tag(value) {
        return processBadge(value, { parens: "square", prettyParens: true });
    },

    // event type marker (enclosed in brackets in plain-text mode)
    event(value) {
        return processBadge("#ff0000|@" + value, { parens: "square" });
    },

    // duration in milliseconds: "$duration{123.4}" or "$duration{t=123.4}"
    duration(value) {
        const index = value.lastIndexOf("=") + 1;
        const time = parseFloat(value.slice(index));
        const text = "#ffff40|" + value.slice(0, index) + ((time < 100) ? `${time}ms` : (time < 999.5) ? `${round(time)}ms` : `${math.roundp(time / 1000, 0.01)}s`);
        return processBadge(text);
    },

    // text with CSS color: "$color{#ff0000|text}" or "$color{red|text}"
    color(value) {
        const [, color, text] = /^(.+?)\|(.*)$/i.exec(value) ?? [null, null, value];
        return { text, css: color ? `color:${color};` : undefined };
    },

    // text with CSS background color: "$back{#ff0000|text}" or "$back{red|text}"
    back(value) {
        const [, color, text] = /^(.+?)\|(.*)$/i.exec(value) ?? [null, null, value];
        return { text, css: color ? `background-color:${color};` : undefined };
    }
};

const TAKETIME_RESOLVED = " $badge{#00FF00|async}";
const TAKETIME_REJECTED = " $badge{#FF0000|async}";
const TAKETIME_THROWN = " $badge{#FF0000|thrown}";

// private functions ==========================================================

/**
 * Parses a log level from its string representation.
 */
function parseLogLevel(value: unknown): LogLevel | null {

    // no string value available
    if (!value || !is.string(value)) { return null; }

    // log levels as numbers or as strings
    switch (value.toLowerCase()) {
        case "0": case "off":   return LogLevel.OFF;
        case "1": case "error": return LogLevel.ERROR;
        case "2": case "warn":  return LogLevel.WARN;
        case "3": case "info":  return LogLevel.INFO;
        case "4": case "debug": return LogLevel.DEBUG;
        case "5": case "trace": return LogLevel.TRACE;
        // backwards compatibility
        case "false":           return LogLevel.OFF;
        case "true": case "on": return LogLevel.TRACE;
    }

    return null;
}

/**
 * Converts the value of an inline command to a badge with fill and border CSS
 * formatting.
 */
function processBadge(value: string, options?: CommandInfo): CommandInfo {
    const matches = /^(?:#([0-9a-f]{6})\|)?(.*)$/i.exec(value)!; // matches always
    const rgb = matches[1] ? parseInt(matches[1], 16) : 0x808080;
    const fill = `background-color:rgba(${(rgb >> 16) & 0xFF},${(rgb >> 8) & 0xFF},${rgb & 0xFF},.2);`;
    return { ...options, text: matches[2], css: BADGE_CSS + fill };
}

/**
 * Converts all inline commands in the passed string according to plain-text or
 * pretty formatting mode.
 */
function processCommands(pattern: string, prettyArgs: string[] | null): string {
    return pattern.replace(COMMAND_RE, (match: string, id: string, value: string) => {

        // resolve command processor
        const processor = COMMAND_PROCESSORS[id];
        if (!processor) { return match; }

        // process the command code and value
        const result = processor.call(COMMAND_PROCESSORS, value);
        let { text } = result;
        const { css, parens, prettyParens } = result;

        // enclose text in parentheses
        if (text && parens && (!prettyArgs || prettyParens)) {
            switch (parens) {
                case "round":  text = `(${text})`; break;
                case "square": text = `[${text}]`; break;
            }
        }

        if (prettyArgs) {
            if (css) { prettyArgs.push(css); }
            if (text && css) { prettyArgs.push(""); }
            return (text && css) ? `%c${text}%c` : css ? "%c" : (text || "");
        }

        return text || "";
    });
}

/**
 * Converts a DOM element to a CSS-selector-like readable text.
 */
function serializeNode(node: Node): string {
    const selectors = [];
    for (let i = 0, next: Node | null = node; next && (i < 3); next = next.parentNode, i += 1) {
        let sel = (next instanceof Element) ? next.tagName.toLowerCase() : (next instanceof Text) ? "#text" : (next instanceof Document) ? "#document" : `type=${next.nodeType}`;
        if (next instanceof Element) {
            if (next.id) { sel += `#${next.id}`; }
            const classes = (next instanceof HTMLHtmlElement) ? "" : next.getAttribute("class");
            if (classes) { sel += `.${classes.replace(/ /g, ".")}`; }
            const { dataset } = next as unknown as HTMLOrSVGElement;
            for (const key in dataset) {
                sel += `[data-${key.replace(/[A-Z]/g, c => `-${c.toLowerCase()}`)}=${dataset[key]}]`;
            }
        } else if (next instanceof Text) {
            sel += `[${next.nodeValue}]`;
        }
        selectors.unshift(sel);
    }
    return selectors.join(">");
}

/**
 * Converts the passed value to a string somehow, for debug logging.
 *
 * @param value
 *  The value to be stringified.
 *
 * @returns
 *  The stringified value.
 */
function stringifyForLogging(value: unknown): string {

    // serializer for the passed value, array elements, and object properties
    const serializeFn = (v: unknown): unknown => {
        // convert exceptions to their message text
        if (v instanceof Error) { return `Error: ${v.message}`; }
        // convert DOM node to a readable text
        if (v instanceof Node) { return serializeNode(v); }
        // convert DOM node list to a readable text
        if (isNodeList(v)) { return Array.from(v); }
        // convert JQuery collection to a readable text
        if (isJQuery(v)) { return v.get(); }
        // other values will be processed by `JSON.stringify` itself
        return v;
    };

    // try to convert the value to JSON
    try { return json.tryStringify(value, { serializeFn }); } catch { }

    // try to stringify the value by its `toString()` method
    try { return String(value); } catch { }

    return "[Object object]";
}

// public functions ===========================================================

/**
 * Returns the log level that is defined in the debug configuration.
 *
 * @param configKey
 *  The name of the debug log level option.
 *
 * @returns
 *  The configured log level.
 */
export function getLogLevel(configKey: string, defLevel?: LogLevel): LogLevel {
    // URL wins over local storage
    const urlLevel = parseLogLevel(locationHash(configKey));
    if (urlLevel !== null) { return urlLevel; }
    // get log level from from local storage
    const logLevel = getDebugValue(configKey);
    return is.number(logLevel) ? logLevel : is.number(defLevel) ? defLevel : LogLevel.WARN;
}

/**
 * Changes the log level in the debug configuration.
 *
 * @param configKey
 *  The name of the debug log level option.
 *
 * @param logLevel
 *  The log level to be set.
 */
export function setLogLevel(configKey: string, logLevel: LogLevel, defLevel?: LogLevel): void {
    // update log level in local storage
    if (!is.number(defLevel)) { defLevel = LogLevel.WARN; }
    setDebugValue(configKey, (logLevel === defLevel) ? null : logLevel);
    // remove log level from URL
    locationHash(configKey, null);
}

// class Logger ===============================================================

/**
 * Provides methods to create logging messages in the browser console.
 *
 * Logging will always be disabled without global debug mode specified in the
 * server-side configuration option `debugavailable`.
 *
 * All log messages from all instances of this class will be recorded in an
 * internal global plain-text message list.
 *
 * The first string argument of a log message may contain inline commands for
 * formatted ("pretty") output to the browser console. Inline commands may
 * appear multiple times at any place in the string argument. They will be
 * filtered in the global plain-text message list.
 *
 * Inline commands start with a dollar sign, followed by the command name,
 * followed by the command argument enclosed in curly brackets. The following
 * inline commands are supported:
 *
 * - `$badge{text}` or `$badge{#rrggbb|text}`: A text snippet that will be
 *   painted as a badge with a background color and rounded borders. The fill
 *   color can be specified as hexadecimal RGB value with leading hash
 *   character preceding the text, separated with a pipe character. In
 *   plain-text logs, the badge text will be enclosed in brackets, and the
 *   color will be ignored.
 *
 * - `$tag{text}` or `$tag{#rrggbb|text}`: Very similar to the `badge` command.
 *   The only difference is that the text snippet will always be enclosed in
 *   brackets, also in pretty mode. This command is used by all loggers for the
 *   leading logger tag passed to the constructor.
 *
 * - `$event{text}`: Similar to the `badge` command, but especially used for
 *   event type names. The badge will have a fixed color, and an at-sign will
 *   be shown before the event name.
 *
 * - `$duration{number}` or `$duration{text=number}`: Prints a duration. The
 *   command expects a floating-point number in milliseconds. The value will be
 *   rounded automatically to seconds if it exceeds 1000. An optional prefix
 *   text, separated with an equals sing, can be specified that will be added
 *   to the output.
 *
 * - `$color{color|text}`: Changes the text color for the specified text. The
 *   color can be any valid CSS color string, e.g. `$color{#ff0000|text}` or
 *   `$color{red|text}`.
 *
 * - `$back{color|text}`: Changes the background color for the specified text.
 *   The color can be any valid CSS color string, e.g. `$back{#ff0000|text}` or
 *   `$back{red|text}`.
 *
 * @param configKey
 *  The key of the debug configuration item that will be used to store and
 *  resolve the log level at runtime. The log level will be updated at runtime,
 *  if the respective configuration item has changed.
 *
 * @param [config]
 *  Configuration options for the new logger.
 */
export class Logger {

    // statics ----------------------------------------------------------------

    // the global message list filled by all active loggers
    static readonly #messages: string[] = [];

    // number of active locks on the global message list
    static #locks = 0;

    /**
     * Returns the global message list collected from all active loggers.
     *
     * @returns
     *  The global message list collected from all active loggers.
     */
    static getRecordedMessages(): readonly string[] {
        return Logger.#messages;
    }

    /**
     * Deactivates the global message recorder. Calls to any methods of any
     * logger will not be added to the message list.
     *
     * Repeated calls of this function will be counted internally. Only the
     * last matching call of the function `unlockRecorder()` will actually
     * reactivate the message recorder.
     */
    static lockRecorder(): void {
        Logger.#locks += 1;
    }

    /**
     * Activates the global message recorder, if the internal call counter for
     * the function `lockRecorder()` reaches zero.
     */
    static unlockRecorder(): void {
        Logger.#locks -= 1;
    }

    // properties -------------------------------------------------------------

    // whether the logger is enabled at all (debug mode, or by option)
    readonly #anyEnabled: boolean;

    // whether the logger is enabled to write to the browser console
    readonly #logEnabled: boolean;

    // the active log level
    #logLevel: LogLevel;

    // current indentation for profiling blocks
    #indent = "";

    // whether to record all messages, regardless of the log level
    readonly #recordAll: boolean;

    // the leading tag
    readonly #tag: Opt<string>;

    // constructor ------------------------------------------------------------

    constructor(configKey: string, config?: LoggerConfig) {

        // whether the logger is enabled at all
        this.#anyEnabled = config?.enabled ?? (DEBUG || LOG_ERROR_DATA);

        // whether the logger is enabled to write to the browser console
        this.#logEnabled = config?.enabled ?? DEBUG;

        // resolve the initial log level from configuration
        const defLogLevel = config?.defLogLevel ?? LogLevel.WARN;
        this.#logLevel = getLogLevel(configKey, defLogLevel);

        // update log level dynamically when changed in the configuration
        globalEvents.on("change:config:debug", () => (this.#logLevel = getLogLevel(configKey, defLogLevel)));

        // whether to record all messages regardless of the log level
        this.#recordAll = !!config?.recordAll;

        // prepare arguments for the leading tag if configured
        if (config?.tag) {
            const tagColor = config.tagColor ?? 0x808080;
            this.#tag = `$tag{#${tagColor.toString(16).padStart(6, "0")}|${config.tag}}`;
        }
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns whether this logger is enabled, i.e. whether it ever may log any
     * messages regardless of its current log level.
     *
     * @returns
     *  Whether this logger is enabled.
     */
    isLoggingEnabled(): boolean {
        return this.#anyEnabled;
    }

    /**
     * Returns whether the specified log level is effectively active.
     *
     * @param logLevel
     *  The log level to be checked.
     *
     * @returns
     *  Whether the specified log level is effectively active.
     */
    isLogLevelActive(logLevel: LogLevel): boolean {
        return this.#anyEnabled && (logLevel <= this.#logLevel);
    }

    /**
     * Returns the effective log level (the more verbose of regular log level
     * and global message recorder).
     *
     * @returns
     *  The effective log level.
     */
    getEffectiveLogLevel(): LogLevel {
        return this.#recordAll ? LogLevel.TRACE : max(this.#logLevel, LogLevel.WARN);
    }

    /**
     * Invokes the passed callback function if and only if this logger is
     * enabled, and the specified log level is active. Can be used to prevent
     * running rather complex calculations that are needed for debug logging
     * only.
     *
     * @param logLevel
     *  The log level to be used.
     *
     * @param callbackFn
     *  The callback function that will only be invoked if the specified log
     *  level is active.
     *
     * @param [context]
     *  The context to be bound to the callback function.
     */
    withLogLevel(logLevel: LogLevel, callbackFn: FuncType<void>, context?: object): void {
        if (this.isLogLevelActive(logLevel)) {
            callbackFn.call(context);
        }
    }

    /**
     * Writes a log message with the specified level to the browser output
     * console.
     *
     * @param logLevel
     *  The log level to be used.
     *
     * @param args
     *  The values to be written to the console.
     */
    write(logLevel: LogLevel, ...args: unknown[]): void {
        this.#write(logLevel, args);
    }

    /**
     * Writes a trace message to the browser output console.
     *
     * @param args
     *  The values to be written to the console.
     */
    trace(...args: unknown[]): void {
        this.#write(LogLevel.TRACE, args);
    }

    /**
     * Writes a log message to the browser output console.
     *
     * @param args
     *  The values to be written to the console.
     */
    log(...args: unknown[]): void {
        this.#write(LogLevel.DEBUG, args);
    }

    /**
     * Writes an info message to the browser output console.
     *
     * @param args
     *  The values to be written to the console.
     */
    info(...args: unknown[]): void {
        this.#write(LogLevel.INFO, args);
    }

    /**
     * Writes a warning message to the browser output console.
     *
     * @param args
     *  The values to be written to the console.
     */
    warn(...args: unknown[]): void {
        this.#write(LogLevel.WARN, args);
    }

    /**
     * Writes an error message to the browser output console.
     *
     * @param args
     *  The values to be written to the console.
     */
    error(...args: unknown[]): void {
        this.#write(LogLevel.ERROR, args);
    }

    /**
     * Writes an error message to the browser output console describing the
     * passed exception object.
     *
     * @param err
     *  The exception as caught in a try/catch.
     *
     * @param args
     *  Additional values to be written to the console.
     */
    exception(err: unknown, ...args: unknown[]): void {
        this.error("Exception caught:", err, ...args);
    }

    /**
     * Writes a warning message to the browser output console, if the passed
     * condition is falsy.
     *
     * @param condition
     *  The condition to be tested. If this value is falsy, a warning message
     *  will be written.
     *
     * @param args
     *  The values to be written to the console.
     */
    assert(condition: unknown, ...args: unknown[]): void {
        if (!condition) { this.#write(LogLevel.WARN, args); }
    }

    /**
     * Executes the passed callback function and writes a message to the
     * browser console containing the execution time of the callback. If the
     * callback returns a pending promise, the time needed to settle that
     * promise will be measured.
     *
     * The callback function will ALWAYS be invoked, regardless whether logging
     * is enabled or not.
     *
     * @param message
     *  The initial message printed to the browser console before invoking the
     *  callback function. The execution time will be shown after the callback
     *  function has returned.
     *
     * @param callback
     *  The callback function to be executed.
     *
     * @param [logLevel=DEBUG]
     *  A custom log level for the message.
     *
     * @returns
     *  The return value of the callback function.
     */
    takeTime<T>(message: string | FuncType<string>, callback: FuncType<T>, logLevel = LogLevel.DEBUG): T {

        // disabled logger: directly invoke callback function
        if (!this.isLogLevelActive(logLevel)) {
            return callback();
        }

        // resolve message callback
        const msg = is.function(message) ? message() : message;

        const endMessage = (state?: string): void => {
            this.#indent = this.#indent.substring(INDENT_PARTICLE.length);
            this.write(logLevel, `<= $duration{${math.roundp(window.performance.now() - t0, 0.001)}}${state || ""} ${msg}`);
        };

        this.write(logLevel, `=> ${msg}`);
        this.#indent += INDENT_PARTICLE;
        const t0 = window.performance.now();

        try {
            const result = callback();
            if (result instanceof Promise) {
                // eslint-disable-next-line promise/catch-or-return -- logging only
                result.then(() => endMessage(TAKETIME_RESOLVED), () => endMessage(TAKETIME_REJECTED));
            } else if (jpromise.is(result)) {
                if (jpromise.isFulfilled(result)) {
                    endMessage();
                } else if (jpromise.isRejected(result)) {
                    endMessage(TAKETIME_THROWN);
                } else {
                    result.done(() => endMessage(TAKETIME_RESOLVED));
                    result.fail(() => endMessage(TAKETIME_REJECTED));
                }
            } else {
                endMessage();
            }
            return result;
        } catch (err) {
            endMessage(TAKETIME_THROWN);
            throw err;
        }
    }

    /**
     * Creates and returns a version of the passed callback function that shows
     * the execution time of each its invocation. If the callback returns a
     * pending promise, the time needed to settle that promise will be measured
     * (wraps the callback function in a call to the method `takeTime()`
     * internally).
     *
     * In TypeScript, this method can also be used as method decorator. In this
     * case, the parameter `method` must be omitted.
     *
     * @param message
     *  The initial message printed to the browser console before invoking the
     *  callback function. The execution time will be shown after the callback
     *  function has returned.
     *
     * @param [method]
     *  The callback function to be profiled. Must be omitted if this method is
     *  used as decorator.
     *
     * @param [options]
     *  Optional parameters (either an options bag, or a log level constant as
     *  shortcut for the option "logLevel").
     *
     * @returns
     *  The passed callback function wrapped into the method `takeTime()` of
     *  this logger. Forwards the current calling context to the callback
     *  function, and returns its return value.
     */
    profileMethod<RT, AT extends unknown[]>(message: string, method: FuncType<RT, AT>, options?: ProfileMethodOptions | LogLevel): FuncType<RT, AT>;
    profileMethod(message: string, options?: ProfileMethodOptions | LogLevel): AnyMethodDecorator;
    // implementation
    profileMethod(message: string, arg1?: unknown, arg2?: unknown): unknown {

        // bound "takeTime" for callback functions with custom "this"
        const takeTime = this.takeTime.bind(this);

        // resolve the passed options
        const options = (is.function(arg1) ? arg2 : arg1) as Opt<ProfileMethodOptions | LogLevel>;
        const logLevel = is.number(options) ? options : options?.logLevel;
        const whenFn = (!is.number(options) && options?.when) || fun.true;

        // direct call with function: create and return a wrapped function
        if (is.function(arg1)) {

            // disabled logger: return the passed function as is
            if (!this.#anyEnabled) { return arg1; }

            // return a function that uses its own calling context for `method`
            return function (this: unknown, ...args: unknown[]): unknown {
                const invoke = (): unknown => arg1.apply(this, args);
                return whenFn() ? takeTime(message, invoke, logLevel) : invoke();
            };
        }

        // called as class method decorator: return decorator handler function
        return (method: AnyFunction): AnyFunction => {

            if (!this.#anyEnabled) { return method; }

            // redefine the instance method
            return function (this: object, ...args: unknown[]): unknown {
                const invoke = (): unknown => method.apply(this, args);
                return whenFn() ? takeTime(message, invoke, logLevel) : invoke();
            };
        };
    }

    // private methods --------------------------------------------------------

    /**
     * Writes a log message with the specified level to the browser output
     * console.
     *
     * @param logLevel
     *  The log level to be used.
     *
     * @param args
     *  The values to be written to the console.
     */
    #write(logLevel: LogLevel, args: unknown[]): void {

        // do nothing for disabled loggers
        if (!this.#anyEnabled) { return; }

        // whether the console logger is active
        const loggerActive = this.#logEnabled && (logLevel <= this.#logLevel);
        // whether to record the message (always for warnings and errors)
        const recordActive = (Logger.#locks === 0) && (this.#recordAll || (logLevel <= this.#logLevel) || (logLevel <= LogLevel.WARN));

        // early exit if nothing will be logged at all
        if (!loggerActive && !recordActive) { return; }

        // resolve callback argument
        if ((args.length === 1) && is.function(args[0])) {
            try {
                args = ary.wrap(args[0]());
            } catch (error) {
                debug.logScriptError(error);
                return;
            }
        }

        // return early if nothing will be logged
        if (args.every(is.nullish)) { return; }

        // create first argument (logger tag, indentation, first string argument passed in)
        let arg0 = (this.#tag || "") + this.#indent;
        if (is.string(args[0])) { arg0 += ` ${args.shift()}`; }

        // process inline commands, and call the console method
        if (loggerActive) {
            const consoleFn = CONSOLE_FNS[logLevel];
            const convArgs = AUTOTEST ? args.map(stringifyForLogging) : args;
            if (arg0) {
                const formatArgs: string[] = [];
                formatArgs.unshift(processCommands(arg0, PRETTY_PRINT ? formatArgs : null));
                consoleFn(...formatArgs, ...convArgs);
            } else {
                consoleFn(...convArgs);
            }
        }

        // process inline commands, and extend the global message list
        if (recordActive) {
            if (arg0) { args.unshift(processCommands(arg0, null)); }

            // limit number of entries in the global message list
            const messages = Logger.#messages;
            if (messages.length === MAX_MESSAGE_LIST_SIZE) {
                messages.shift();
            }

            // stringify everything that is not a string to retain the data
            const strArgs = args.map(stringifyForLogging);
            let message = `${new Date().toISOString()}: ${strArgs.join(" ")}`;

            // limit length of the text line (do not split UTF-16 surrogate pairs)
            if (message.length > MAX_MESSAGE_LINE_SIZE) {
                const length = unicode.sanitizeOffset(message, MAX_MESSAGE_LINE_SIZE - 1);
                message = message.slice(0, length) + str.ELLIPSIS_CHAR;
            }

            // insert the entry into the message list
            messages.push(message);
        }
    }
}

// singletons =================================================================

/**
 * Generic unrestricted logger without prefix.
 */
export const globalLogger = new Logger("office:log-global", { defLogLevel: LogLevel.TRACE });

/**
 * Logger for GUI layouting code, bound to the debug configuration flag
 * "office:log-layout".
 */
export const layoutLogger = new Logger("office:log-layout", { tag: "LAYOUT", tagColor: 0x00FFFF });

/**
 * Logger for A11Y announcements, bound to the debug configuration flag
 * "office:log-a11y".
 */
export const a11yLogger = new Logger("office:log-a11y", { tag: "A11Y" });
