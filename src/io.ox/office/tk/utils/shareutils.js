/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import ext from '$/io.ox/core/extensions';
import Capabilities from '$/io.ox/core/capabilities';
import Collection from '$/io.ox/core/collection';
import { invoke } from '$/io.ox/backbone/views/actions/util';

import { jpromise } from '@/io.ox/office/tk/algorithms';
import { authorizeGuard, getFileModelFromDescriptor } from '@/io.ox/office/tk/utils/driveutils';

import '$/io.ox/files/actions'; // is required for the 'io.ox/files/toolbar/share' extPoint

// static initialization ==================================================

var actions = {};

ext.point('io.ox/files/toolbar/share').each(function (action) {
    actions[action.id] = action;
});

// constants ==============================================================

/**
 * Whether the action "Invite a guest" is available.
 */
export const INVITE_USER_AVAILABLE = Capabilities.has('invite_guests');

/**
 * The settings (e.g. text label and description) for the "Invite a guest"
 * action.
 */
export const INVITE_USER_CONFIG = actions.invite;

/**
 * Whether the action "Create sharing link" is available.
 */
export const CREATE_LINK_AVAILABLE = Capabilities.has('share_links');

/**
 * The settings (e.g. text label and description) for the "Create sharing
 * link" action.
 */
export const CREATE_LINK_CONFIG = actions.getalink;

/**
 * Whether any sharing action is available.
 */
export const SHARING_AVAILABLE = INVITE_USER_AVAILABLE || CREATE_LINK_AVAILABLE;

/**
 * Specifies whether the user is the only user in the associated context/tenant.
 */
export const SINGLE_CONTEXT_DEPLOYMENT = Capabilities.has('alone');

// private functions ======================================================

function invokeLocal(actionId, fileDesc) {

    // resolve file model from descriptor
    var promise = getFileModelFromDescriptor(fileDesc);

    // create a baton object
    promise = promise.then(function (fileModel) {
        var fileDesc = fileModel.toJSON();
        var collection = new Collection(fileDesc);
        return collection.getProperties().then(function () {
            return new ext.Baton({ data: fileDesc, models: [fileModel], collection });
        });
    });

    promise = promise.then(function (baton) {

        // If the file is encrypted show the authorization dialog and store authorization to the session until used
        if (baton.data.encrypted) {
            return authorizeGuard(baton, { minSingleUse: true }).then(function () {
                return baton;
            }, function () {
                return jpromise.reject();
            });
        }

        return baton;
    });

    // invoke the action
    return promise.then(function (baton) {
        return invoke(actions[actionId].ref, baton);
    });
}

// public functions =======================================================

/**
 * Opens the "Invite people" dialog for read/write sharing for explicit
 * users.
 */
export function inviteUser(fileDesc) {
    return invokeLocal('invite', fileDesc);
}

/**
 * Opens the "Create sharing link" dialog for read-only sharing via link.
 */
export function shareLink(fileDesc) {
    return invokeLocal('getalink', fileDesc);
}
