/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";
import $ from "$/jquery";
import ox from "$/ox";

import type { OpenTabOptions } from "$/io.ox/core/api/tab";
import  TabAPI from "$/io.ox/core/api/tab";
import type { VisApiEvent } from "$/io.ox/core/tk/visibility-api-util";
import VisApi from "$/io.ox/core/tk/visibility-api-util";

import { pick, json } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import { Logger } from "@/io.ox/office/tk/utils/logger";

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted when the browser tab becomes visible.
         */
        "tab:show": [];

        /**
         * Will be emitted when the browser tab becomes invisible.
         */
        "tab:hide": [];
    }
}

// constants ==================================================================

/**
 * Specifies whether browser tab support is enabled.
 */
export const BROWSER_TAB_SUPPORT = !!ox.tabHandlingEnabled && !TabAPI.getGuestMode();

/**
 * Specifies whether the application can activate another existing browser tab
 * programmatically (i.e. whether the function `TabHandling.openParent()` will
 * work).
 */
export const ACTIVATE_TAB_SUPPORT = !_.browser.ie;

/**
 * The property names of all URL parameters to be excluded always.
 */
const EXCLUDED_PARAMS = ["new", "target_filename", "target_folder_id", "crypto_action", "auth_code", "convert", "destfilename", "destfolderid", "folder", "id", "preserveFileName", "settings", "section"];

// globals ====================================================================

export const tabLogger = new Logger("office:log-tabs", { tag: "TABS", tagColor: 0x00FFFF });

// global events ==============================================================

// use the visibility API helper to emit the global browser tab events
$(VisApi).on("visibility-changed", (_event, data: VisApiEvent) => {
    const isVisible = !data.currentHiddenState;
    tabLogger.info(`$badge{TabUtils} browser tab ${isVisible ? "becomes visible" : "has been hidden"}`);
    globalEvents.emit(isVisible ? "tab:show" : "tab:hide");
});

// private functions ==========================================================

function parseTargetTabName(targetTab?: Window | null): Opt<OpenTabOptions> {
    if (!targetTab) { return undefined; }
    try {
        const parsed = json.tryParse(targetTab.name);
        const windowName = pick.string(parsed, "windowName");
        const windowType = pick.string(parsed, "windowType");
        const parentName = pick.string(parsed, "parentName");
        if (!windowName || !windowType) {
            tabLogger.warn(`$badge{TabUtils} parseTargetTabName: missing window settings in target tab settings: "${targetTab.name}"`);
            return undefined;
        }
        const options: OpenTabOptions = { windowName, windowType };
        if (parentName) { options.parentName = parentName; }
        return options;
    } catch {
        tabLogger.warn(`$badge{TabUtils} parseTargetTabName: cannot parse target tab settings: "${targetTab.name}"`);
        return undefined;
    }
}

/**
 * Opens a child browser tab.
 *
 * @param moduleName
 *  The full module name to be inserted into the URL.
 *
 * @param appType
 *  The base name of the application to be inserted as URL query parameter for
 *  FavIcon disambiguation.
 *
 * @param params
 *  Additional anchor parameters to be added to the URL, as key/value map. The
 *  parameter `app` will be created automatically according to the specified
 *  application base name.
 *
 * @param [targetTab]
 *  An existing browser tab to be used to open the application.
 *
 * @returns
 *  The new browser tab window.
 */
function openChildTab(moduleName: string, appType: string, params?: Dict, targetTab?: Window | null): Window | null {
    tabLogger.info(`$badge{TabUtils} openChildTab: opening browser tab for module "${moduleName}"`);
    const url = createDocsURL(appType, { ...params, app: moduleName });
    const targetOptions = parseTargetTabName(targetTab);
    return targetOptions ? TabAPI.openNewTab(url, targetOptions) : TabAPI.openChildTab(url);
}

// public functions ===========================================================

/**
 * Returns whether the current Browser tab is visible.
 *
 * @returns
 *  Whether the current Browse tab is visible.
 */
export function isTabVisible(): boolean {
    return !VisApi.isHidden;
}

/**
 * Returns whether this application can open a new Documents applications in a
 * separate browser tab. For this, browser tab support must be enabled, and
 * this application must NOT run in a Documents child tab by itself.
 */
export function canOpenDocsTab(): boolean {
    return BROWSER_TAB_SUPPORT && !ox.office?.isOffice;
}

/**
 * Returns whether this browser tab is a Documents child tab.
 */
export function isDocsChildTab(): boolean {
    return BROWSER_TAB_SUPPORT && !!ox.office?.isOffice;
}

/**
 * Creates the URL for a new browser tab containing a Documents application.
 * Adds the anchor parameters of the current URL (except for specific
 * parameters) to the new URL.
 *
 * @param appType
 *  The base name of the application (e.g. `"text"`) to be inserted as URL
 *  query parameter for FavIcon disambiguation.
 *
 * @param [params]
 *  Additional anchor parameters to be added to the URL, as key/value map.
 *
 *  @returns
 *   The URL for a new browser tab.
 */
export function createDocsURL(appType: string, params?: Dict): string {
    return TabAPI.createUrl(params, { exclude: EXCLUDED_PARAMS, suffix: `office?app=${appType}` });
}

/**
 * Opens an empty child browser tab.
 *
 * @returns
 *  The new browser tab window.
 */
export function openEmptyChildTab(): Window | null {
    tabLogger.info("$badge{TabUtils} openEmptyChildTab: opening empty browser tab");
    return TabAPI.openChildTab("");
}

/**
 * Opens a child browser tab containing a Documents editor application.
 *
 * @param appType
 *  The base name of the application (e.g. `"text"`) to be inserted as URL
 *  query parameter for FavIcon disambiguation.
 *
 * @param [params]
 *  Additional anchor parameters to be added to the URL, as key/value map. The
 *  parameter `app` will be created automatically according to the specified
 *  application base name.
 *
 * @param [targetTab]
 *  An existing browser tab to be used to open the application (e.g. created
 *  with the function `openEmptyChildTab()`).
 *
 * @returns
 *  The new browser tab window.
 */
export function openEditorChildTab(appType: string, params?: Dict, targetTab?: Window | null): Window | null {
    return openChildTab(`io.ox/office/${appType}`, appType, params, targetTab);
}

/**
 * Opens a child browser tab containing a Documents portal application.
 *
 * @param appType
 *  The base name of the application (e.g. `"text"`) to be inserted as URL
 *  query parameter for FavIcon disambiguation.
 *
 * @param [targetTab]
 *  An existing browser tab to be used to open the application (e.g. created
 *  with the function `openEmptyChildTab()`).
 *
 * @returns
 *  The new browser tab window.
 */
export function openPortalChildTab(appType: string, targetTab?: Window | null): Window | null {
    return openChildTab(`io.ox/office/portal/${appType}`, appType, undefined, targetTab);
}

/**
 * Opens a child browser tab containing a Documents presenter application.
 *
 * @param params
 *  Additional anchor parameters to be added to the URL, as key/value map. The
 *  parameter `app` will be created automatically.
 *
 * @param [targetTab]
 *  An existing browser tab to be used to open the application (e.g. created
 *  with the function `openEmptyChildTab()`).
 *
 * @returns
 *  The new browser tab window.
 */
export function openPresenterChildTab(params: Dict, targetTab?: Window | null): Window | null {
    return openChildTab("io.ox/office/presenter", "presenter", params, targetTab);
}

/**
 * Opens the specified file in the Drive app in a parent browser tab.
 *
 * @param folderId
 *  The identifier of the Drive folder containing the file to be shown.
 *
 * @param fileId
 *  The identifier of the file to be shown.
 */
export function openDriveParentTab(folderId: string, fileId: string): void {
    tabLogger.info("$badge{TabUtils} openDriveParentTab: opening browser tab for Drive app");
    TabAPI.openParentTab({ folder: folderId, app: "io.ox/files" });
    TabAPI.propagate("show-in-drive", { folder_id: folderId, id: fileId, targetWindow: TabAPI.getParentWindowName() });
}

/**
 * Opens a new browser tab.
 *
 * @param url
 *  The url which should be opened in the new tab
 *
 */
export function openNewWindow(url: string): void {
    tabLogger.info("$badge{TabUtils} openNewWindow: opening new browser window");
    // be careful when changing this line, "noopener, noreferrer" are used for a reason
    window.open(url, "_blank", "noopener, noreferrer");
}
