/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import AutoComplete from "$/io.ox/core/api/autocomplete";
import ContactsAPI from "$/io.ox/contacts/api";
import type { PictureDataOptions } from "$/io.ox/contacts/api";

import { AbortError } from "@/io.ox/office/tk/objects";
import { is } from "@/io.ox/office/tk/algorithms";
import { type NodeOrJQuery, toNode, createDiv, getDeviceResolution } from "@/io.ox/office/tk/dom";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

const { ceil } = Math;

// types ======================================================================

/**
 * The contents of a user contact.
 */
export interface ContactData {

    /**
     * First name if available, empty string otherwise.
     */
    firstName: string;

    /**
     * Last name if available, empty string otherwise.
     */
    lastName: string;

    /**
     * Full name if available, empty string otherwise.
     */
    fullName: string;

    /**
     * Mail address if available, empty string otherwise.
     */
    mailAddr: string;

    /**
     * Internal user identifier.
     */
    userId: string | number;

    /**
     * Identifier of the contact database folder if available.
     */
    folderId?: string | number;

    /**
     * Identifier of the contact if available.
     */
    contactId?: string | number;
}

/**
 * Options needed to set the contact picture of a user at a DOM element.
 */
export interface ContactPictureOptions {

    /**
     * The desired size of the image, in CSS (!) pixels. On retina displays,
     * the size of the requested bitmap data will be enlarged automatically.
     *
     * If omitted, the picture bitmap will loaded with default size, and no
     * CSS size attributes will be set at the picture element.
     */
    size?: number;
}

/**
 * Configuration options for the class `ContactFetcher`.
 */
export interface ContactFetcherConfig {

    /**
     * Maximum number of contacts to be fetched in a query. Default value is
     * `100`.
     */
    limit?: number;

    /**
     * Whether to sort the fetched contacts by full name. Default is `false`.
     */
    sort?: boolean;
}

// public functions ===========================================================

/**
 * Sets the contact picture of the specified user as background image into the
 * passed DOM element.
 *
 * @param target
 *  The DOM element to be filled with the contact picture.
 *
 * @param [contact]
 *  The user contact to create the picture for. The properties `userId`,
 *  `folderId`, and `contactId` will be used to load the appropriate contact
 *  picture. If omitted or set to `null`, or if `userId` is a negative number,
 *  a placeholder image will be inserted instead.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setContactPicture(target: NodeOrJQuery, contact: Nullable<Partial<ContactData>>, options?: ContactPictureOptions): void {

    // the target node, as single-element JQuery collection
    const pictureNode = toNode(target);
    if (!pictureNode) { return; }

    // destructure the picture options
    const { userId, folderId, contactId } = contact ?? {};
    const newUserId = is.string(userId) ? parseInt(userId, 10) : (userId ?? -1);

    // bug 46976: immediately remove the old user picture of *another* user
    const oldUserId = parseInt(pictureNode.dataset.userId || "", 10);
    if ((oldUserId >= 0) && (oldUserId !== newUserId)) { pictureNode.style.backgroundImage = ""; }
    pictureNode.dataset.userId = String(newUserId);

    // initialize user data to be passed to the Contacts API (negative user ID will remove the picture)
    const apiData: PictureDataOptions = (newUserId >= 0) ? { user_id: newUserId } : { };
    if (folderId !== undefined) { apiData.folder_id = folderId; }
    if (contactId !== undefined) { apiData.contact_id = contactId; }

    // ask the Contacts API for the contact picture, or the fallback picture
    const cssSize = options?.size ?? parseInt(pictureNode.dataset.size ?? "", 10);
    const bmpSize = Number.isFinite(cssSize) ? ceil(getDeviceResolution() * cssSize) : 0;
    ContactsAPI.pictureHalo($(pictureNode), apiData, bmpSize ? { width: bmpSize, height: bmpSize } : undefined);

    // set explicit element size if specified and valid
    if (cssSize > 0) {
        pictureNode.style.width = pictureNode.style.height = `${cssSize}px`;
        pictureNode.dataset.size = String(cssSize);
    }
}

/**
 * Creates a new `<div>` element with a contact picture of the specified user.
 *
 * @param [contact]
 *  The user contact to create the picture for. The properties `userId`,
 *  `folderId`, and `contactId` will be used to load the appropriate contact
 *  picture. If omitted or set to `null`, or if `userId` is a negative number,
 *  an element with a placeholder image will be created instead.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A new `<div>` element with a contact picture of the specified user.
 */
export function createContactPicture(contact?: Nullable<Partial<ContactData>>, options?: ContactPictureOptions): HTMLDivElement {
    const pictureNode = createDiv("contact-picture");
    setContactPicture(pictureNode, contact, options);
    return pictureNode;
}

// class ContactFetcher =======================================================

/**
 * Helper for fetching user contact data by name. Maintains an internal cache
 * to speed up subsequent queries.
 */
export class ContactFetcher {

    readonly #api: AutoComplete;
    readonly #sort: boolean;
    #run: number;

    // constructor ------------------------------------------------------------

    constructor(config?: ContactFetcherConfig) {
        this.#api = new AutoComplete({ contacts: true, limit: config?.limit ?? 100 });
        this.#sort = !!config?.sort;
        this.#run = 0;
    }

    // public methods ---------------------------------------------------------

    /**
     * Fetches all user contacts whose name matches the passed query string.
     *
     * @param query
     *  The user name to be matched. If omitted or set to an empty string, all
     *  contacts will be fetched (up to the configured limit).
     *
     * @returns
     *  A promise that will fuulfil with an array of user contacts, or that
     *  will reject with an `AbortError`, if another query has been started in
     *  the meantime before this query has finished.
     */
    async fetch(query?: string): Promise<ContactData[]> {

        // protection against concurrent queries
        const run = (this.#run += 1);

        // fetch all matching contacts from core API
        const apiData = await this.#api.search(query || "");

        // throw `AbortError`, if another query has been started in the meantime
        if (run !== this.#run) { throw new AbortError(); }

        // convert passed raw API user data to `ContactData` instances
        const contacts: ContactData[] = apiData.map(apiEntry => ({
            firstName: apiEntry.first_name || "",
            lastName: apiEntry.last_name || "",
            fullName: apiEntry.display_name || "",
            mailAddr: (apiEntry.field && apiEntry[apiEntry.field]) || "",
            userId: apiEntry.internal_userid,
            folderId: apiEntry.folder_id,
            contactId: apiEntry.id
        }));

        // sort contacts if specified
        return this.#sort ? LOCALE_DATA.getCollator({ numeric: true }).sortByProp(contacts, "fullName") : contacts;
    }
}
