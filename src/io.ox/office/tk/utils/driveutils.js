/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import FilesAPI from '$/io.ox/files/api';
import TabAPI from '$/io.ox/core/api/tab';
import FolderAPI from '$/io.ox/core/folder/api';
import FolderNode from '$/io.ox/core/folder/node';
import TreeView from '$/io.ox/core/folder/tree';
import Selection from '$/io.ox/core/tk/selection';
import pUtil from '$/io.ox/files/permission-util';
import Permissions from '$/io.ox/files/share/permissions';
import { settings as DriveSettings } from '$/io.ox/files/settings';

import { re } from '@/io.ox/office/tk/algorithms';
import * as Config from '@/io.ox/office/tk/config';
import { createIcon, setFocus, hasKeyCode } from '@/io.ox/office/tk/dom';
import { globalLogger } from '@/io.ox/office/tk/utils/logger';
import * as Utils from '@/io.ox/office/tk/utils';
import { BROWSER_TAB_SUPPORT, openEmptyChildTab } from '@/io.ox/office/tk/utils/tabutils';
import { EObject } from '@/io.ox/office/tk/objects';

// static class DriveUtils ================================================

var GUARD_EXTENSION = 'pgp';

var GET_DECRYPTED_RE = new RegExp('^(.+?)\\.' + re.escape(GUARD_EXTENSION) + '$', 'i');

var GET_FILECOMPS_RE = new RegExp('^(.*?)\\.([^.]+)(\\.' + re.escape(GUARD_EXTENSION) + ')?$', 'i');

var GENERAL_ERROR = { errorForErrorCode: true, error: 'GENERAL_UNKNOWN_ERROR' };

var ERROR_MAP = {
    'FILE_STORAGE-0026':    { errorForErrorCode: true, error: 'GENERAL_FILE_NOT_FOUND_ERROR.LOAD', errorClass: 2 },
    'IFO-0400':             { errorForErrorCode: true, error: 'WARNING_NO_PERMISSION_FOR_DOC', errorClass: 1 },
    'IFO-0438':             { errorForErrorCode: true, error: 'GENERAL_FILE_NOT_FOUND_ERROR.LOAD', errorClass: 2 }
};

// cached property whether a file can be created in the documents default folder
let _canCreateInDefaultFolder = false;

// dummy
var dummyTree = {
    options: {
        customize: $.noop,
        disable: $.noop,
        filter: $.noop
    },
    // DOCS-3240: (hack for the hack) "once" method needed because of fix for OXUIB-707
    // https://gitlab.open-xchange.com/frontend/core/commit/2b3913f79a991fd08fe1f19a143ec04a7b083c7f
    once: _.noop
};

function optString(arg) {
    if (_.isNull(arg) || _.isUndefined(arg)) {
        return arg;
    } else {
        return String(arg);
    }
}

function optFolder(folder) {
    folder = optString(folder);
    if (!folder) {
        return getMyFilesId();
    } else {
        return folder;
    }
}

function generalFailHandler(error) {
    if (error) {
        globalLogger.error('DriveUtils fn failed:' + error.error_desc + ' code:"' + error.code + '" error:"' + error.error + '"');
        throw ERROR_MAP[error.code] || GENERAL_ERROR; // triggering a rejected promise in jQuery 3
    }
    globalLogger.error('DriveUtils fn failed');
    throw GENERAL_ERROR;
}

/**
 * disabled is different to "no write access" because
 * children of disabled folders can be enabled!
 */
function shouldBeDisabled(folder_id, allowReadOnlySelection) {

    if (/^virtual/.test(folder_id)) {
        return true;
    }

    if (allowReadOnlySelection) {
        return false;
    }

    // check write and create permissions
    var folderModel = FolderAPI.pool.models[folder_id];
    if (!folderModel) {
        globalLogger.warn('shouldBeDisabled() is called to early, folder is not cached yet. better call getWriteState() folder_id: ' + folder_id);
        return true;
    }
    var json = folderModel.toJSON();
    return !FolderAPI.can('write', json) || !FolderAPI.can('create', json);
}

/**
 * Returns the sanitized file name from the passed file descriptor received
 * from the Files API.
 *
 * @param {Object|Null} fileDesc
 *  The file descriptor received from the Files API.
 *
 * @returns {String}
 *  The sanitized file name; or an empty string, if no file name exists in
 * the passed file descriptor, or if the file descriptor itself does not
 * exist.
 */
export function getSanitizedFileName(fileDesc) {
    // bug 52066: RTLO characters can be used to obfuscate file names
    return (_.isObject(fileDesc) && (fileDesc['com.openexchange.file.sanitizedFilename'] || fileDesc.filename)) || '';
}

/**
 * The file extension of encrypted files with the leading period character.
 */
export const GUARD_EXT = '.' + GUARD_EXTENSION;

/**
 * Returns whether the file with the passed name is encrypted with Guard.
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {Boolean}
 *  Whether the file with the passed name is encrypted with Guard.
 */
export function hasGuardExt(fileName) {
    return GET_DECRYPTED_RE.test(fileName);
}

/**
 * Returns the passed file name, without encryption file extension. If the
 * real extension of the passed file name is 'pgp' it will be removed (e.g.
 * the file name 'example.xlsx.pgp' will be shortened to 'example.xlsx').
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {String}
 *  The unencrypted version of the passed file name.
 */
export function removeGuardExt(fileName) {
    var matches = GET_DECRYPTED_RE.exec(fileName);
    return matches ? matches[1] : fileName;
}

/**
 * Prompts for the Guard password.
 *
 * @param {object} [baton]
 *  An optional Baton object with a file descriptor in its `data` property.
 *  Can be omitted for something like a "Create new encrypted document"
 *  functionality.
 *
 * @param {object} [callbacks]
 *  Callback functions passed to the Guard authorizer.
 *  - {() => void)} [callbacks.callback]
 *      A callback function that will be called directly after user
 *      authentication succeeded.
 *  - {() => void)} [callbacks.preAuthCallback]
 *      A callback function that will be called directly before user
 *      authentication starts (after clicking the OK button of the Guard
 *      password dialog, or immediately if the password is cached).
 *  - {() => void)} [callbacks.failAuthCallback]
 *      A callback function that will be called after user authentication
 *      has failed.
 *
 * @returns {Promise<string>}
 *  A promise that will fulfil with the authentication code, or will reject
 *  on error.
 */
export async function authorizeGuard(baton, callbacks) {
    const { default: Authorizer } = await import('$/io.ox/guard/auth/authorizer');
    return Authorizer.authorize(baton, callbacks);
}

/**
 * Prompts for the Guard password, and opens a new browser tab if possible.
 *
 * @param {object} [baton]
 *  An optional Baton object with a file descriptor in its `data` property.
 *  Can be omitted for something like a "Create new encrypted document"
 *  functionality.
 *
 * @returns {Promise<{ authCode: string; targetTab: Window | null}>}
 *  A promise that will fulfil with the authentication code and the new
 *  browser tab window (or null, if the document will be opened in the same
 *  browser tab), or will reject on error.
 */
export async function authorizeGuardWithTab(baton) {
    var targetTab = null;
    const authCode = await authorizeGuard(baton, {
        callback() { targetTab = BROWSER_TAB_SUPPORT ? openEmptyChildTab() : null; }, // not preAuthCallback (DOCS-3905, GUARD-308)
        failAuthCallback() { if (targetTab) { targetTab.close(); } }
    });
    return { authCode, targetTab };
}

/**
 * Returns the base name of the passed file name, without file extension
 * (e.g. the file name 'example.xlsx' will be shortened to 'example'). If
 * the real extension of the passed file name is 'pgp', then the preceding
 * embedded extension will be removed too (e.g. 'example.xlsx.pgp' will be
 * shortened to 'example' too).
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {String}
 *  The base name of the passed file name.
 */
export function getFileBaseName(fileName) {
    var matches = GET_FILECOMPS_RE.exec(fileName);
    return matches ? matches[1] : fileName;
}

/**
 * Returns the lower-case extension of the passed file name (e.g. the text
 * 'xlsx' for the file name 'example.xlsx'). If the real extension of the
 * passed file name is 'pgp', then the preceding embedded extension will be
 * returned (e.g. 'xlsx' for the file name 'example.xlsx.pgp').
 *
 * @param {String} fileName
 *  The file name (case-insensitive).
 *
 * @returns {String}
 *  The lower-case extension of the passed file name.
 */
export function getFileExtension(fileName) {
    var matches = GET_FILECOMPS_RE.exec(fileName);
    return matches ? matches[2].toLowerCase() : '';
}

/**
 * Replaces the file extension in the passed file name. If the real
 * extension of the passed file name is 'pgp', then the preceding embedded
 * extension will be replaced (e.g. the extension 'xlsx' in the file name
 * 'example.xlsx.pgp').
 *
 * @param {String} fileName
 *  The file name to be modified.
 *
 * @param {String} fileExt
 *  The new file extension.
 *
 * @returns {String}
 *  The file name with the new file extension.
 */
export function replaceFileExtension(fileName, fileExt) {
    var newFileName = getFileBaseName(fileName) + '.' + fileExt;
    if (hasGuardExt(fileName)) { newFileName += GUARD_EXT; }
    return newFileName;
}

/**
 * instead of moving file in trash this function really deletes the assigned file
 *
 * @param {Object} file is a file-descriptor
 *
 * @returns {Deferred}
 */
export function purgeFile(file) {
    return FilesAPI.remove([file], true);
}

/**
 * copy a single file
 *
 * @param {Object} file
 *  the fileDescriptor of the file which should be copied
 * @param {String} targetFolderId
 *  the target folder id to copy the file
 * @param {Boolean} ignoreWarnings
 *  if warnings should be ignored
 *
 * @returns {Deferred}
 */
export function copyFile(file, targetFolderId, ignoreWarnings) {
    return FilesAPI.copy([file], targetFolderId, ignoreWarnings);
}

/**
 * returns true if user is guest or anonymous
 *
 * if user has no MyFiles he is probably a guest
 */
export function isGuest() {
    return getMyFilesId() === null;
}

/**
 * @param {String} folderId is the "id" of the folder, you want the content of.
 */
export function getAllFiles(folderId) {
    return FilesAPI.getAll(folderId);
}

/**
 * @param {object} file
 *
 * @param {string} type
 *
 * @param {Object} [options]
 *  Optional parameters:
 *  - {Boolean} [options.version]
 *      When false, no version will be appended. This can be useful for
 *      getting the most recent version. (for more details see function
 *      'getUrl' in Api.js)
 */
export function getFileUrl(file, type, options) {
    return FilesAPI.getUrl(file, type, options);
}

/**
 * @param {Object} file is a file-descriptor
 *
 *  this can have:
 *       {String} file.folder_id (parent folder)
 *       {String} file.id
 *       {String} file.filename
 *       {Number} file.version
 *       {String} file.source (for example: drive, mail etc)
 *       {String} file.attachment (don't know)
 *
 * @returns {Deferred}
 */
export function propagateChangeFile(file) {
    if (BROWSER_TAB_SUPPORT) {
        // this block must always be snchronous
        TabAPI.propagate('refresh-file', _.pick(file, 'folder_id', 'id'));
    }
    return FilesAPI.propagate('add:version', file).then(function () {
        return getFile(file, { cache: false, columns: '1,2,3,5,20,23,108,109,700,701,702,703,704,705,706,707,708,709,710,711,7010,7030' });
    });
}

/**
 * Function for broadcasting file propagates for other browser tabs
 *
 * @param {String} propagate
 *  which propagate should be called
 *  e.g.: add:version
 *
 * @param {Object|null} parameters
 *  additional parameters for propagate
 *  e.g.: FileDescriptor
 */
export function tabBroadcast(propagate, parameters) {
    Config.setStorageValue('appsuite.drive-propagate', { propagate, parameters, date: Date.now() });
}

export function showInDriveBroadcast(parameters) {
    Config.setStorageValue('appsuite.show-in-drive', { folder_id: parameters.folder_id, id: parameters.id, date: Date.now() });
}

/**
 * @param {Object} file is a file-descriptor
 *  this can have:
 *       {String} file.folder_id (parent folder)
 *       {String} file.id
 *
 * @param {Object} options
 *  directly assigned to FilesAPI
 *
 * @returns {Deferred}
 */
export function getFile(file, options) {
    return FilesAPI.get(file, options).catch(generalFailHandler);
}

/**
 * Propagates an "add:file" event to the filesAPI. If browser tab support
 * is enabled, an additional broadcast event for the localStorage is fired.
 *
 * @param {Object} file is a FileDescriptor
 *  model which has to be propagated because it has been added
 *
 * @returns {Deferred}
 */
export function propagateNewFile(file) {
    if (BROWSER_TAB_SUPPORT) {
        // this block must always be snchronous
        TabAPI.propagate('add-file', _.pick(file, 'folder_id', 'id'));
    }
    FilesAPI.propagate('add:file', file);
    return getFile(file);
}

/**
 * returns true if file.source is drive or undefined
 *
 * @returns {Boolean}
 *  true if file is in drive
 */
export function isDriveFile(file) {
    if (file && !file.source) {
        globalLogger.warn('assigned file has no source. please change that.');
        //TODO: use new files-models
    }
    return file && (file.source === 'drive' || !file.source);
}

/**
 * returns the corresponding backbone based file model ('io.ox/files/api').
 *
 * already used by file ['io.ox/office/presenter/main'] > 'start-presentation' > getFileSuccess
 *
 * @returns {Deferred} Deferred with FileAPI.Model as result
 */
export function getFileModelFromDescriptor(fileDescriptor) {
    return getFile(fileDescriptor).then(function (file) {
        var model = FilesAPI.pool.get('detail').get(_.cid(file));
        if (!model) {
            throw new Error('getFileModelFromDescriptor() model is null ' + fileDescriptor);
        }
        return model.clone();
    });
}

/**
 * @param {Object} file is a file-descriptor
 *  this can have:
 *       {String} file.folder_id (parent folder)
 *       {String} file.id
 *
 * @param {Object} options
 *  directly assigned to FilesAPI
 *
 * @returns {Deferred}
 *  Array of versions
 *  can be empty if Drive does not support versions!
 */
export function getFileVersions(file, options) {
    return FilesAPI.versions.load(file, options).catch(generalFailHandler);
}

/**
 * Returns a promise indicating if the given file descriptor represents
 * the current version of the file.
 *
 * @param {Object} file
 *  The file descriptor.
 *
 * @param {Object} options
 *  directly assigned to FilesAPI
 *
 * @returns {jQuery.Promise}
 *  A Promise that will resolve with a Boolean value,
 *  specifying whether the file descriptor represents
 *  the current version.
 */
export function getCurrentFileVersionState(file, options) {
    return FilesAPI.versions.getCurrentState(file, options).catch(generalFailHandler);
}

// subclasses -------------------------------------------------------------

/**
 * a generic FolderPicker for own groups of folders
 * the folders real structure is seen as breadcrumb view
 *
 * An instance of this class triggers the following events:
 * - 'change'
 *      After the selected folder has been changed (by user or by 'preselect')
 *      the 'change' event is triggered
 *
 * @param {Object} [initOptions]
 *  Optional parameters:
 *  - {String} [initOptions.folderId]
 *      Drive-File-Id to preselect a folder
 */
export class FlatFolderPicker extends EObject {

    constructor(initOptions) {

        super();

        var self = this;

        var preselect = Utils.getStringOption(initOptions, 'folderId');
        var folderTree = $('<div class="folder-tree visible-selection dropzone" role="navigation" data-dropzones=".selectable">');
        var selectedFolder = null;

        // private methods ----------------------------------------------------

        function selectNode() {
            selectedFolder = folderTree.find('.folder.selected').attr('data-id');
            self.trigger('change');
        }

        /**
         * gives a fake FolderNode, which look similar like the FolderNode of
         * the FolderPicker and its TreeView.
         * But it is not used for a tree, it is used for a flat view.
         *
         * Reacts on KeyUp and KeyDown to select only one FolderNode at once and iss clickable.
         *
         * @returns {Object}
         *  fake FolderNode
         */
        function newGroupNode(title) {
            var node = new EObject();
            var el = $('<li class="section folder selectable">').append(
                $('<div class="folder-node" role="presentation" style="padding-left: 0px;">').append(
                    $('<div class="folder-arrow">'),
                    $('<div class="folder-icon">'),
                    $('<div class="folder-label" style="font-weight: bold;">').append($('<div>').text(title))
                )
            );
            bindFolderNodeHandlers(el, node);
            var folderArrow = el.find('.folder-arrow');
            folderArrow.addClass('open');
            folderArrow.append(createIcon('bi:caret-down-fill'));

            function open() {
                el.parent().find('li.folder').show();
                folderArrow.removeClass('closed');
                folderArrow.addClass('open');
                folderArrow.children('[data-icon-id]').remove();
                folderArrow.append(createIcon('bi:caret-down-fill'));
            }

            function close() {
                el.parent().find('li.folder:not(.section)').hide();
                folderArrow.removeClass('open');
                folderArrow.addClass('closed');
                folderArrow.children('[data-icon-id]').remove();
                folderArrow.append(createIcon('bi:caret-right-fill'));
            }

            el.on('keydown', function (evt) {
                if (hasKeyCode(evt, 'LEFT_ARROW')) {
                    close();
                } else if (hasKeyCode(evt, 'RIGHT_ARROW')) {
                    open();
                }
            });

            folderArrow.parent().on('click', function () {
                if (folderArrow.hasClass('open')) {
                    close();
                } else {
                    open();
                }
            });

            node.render = function () {
                return {
                    $el: el
                };
            };

            return node;
        }

        // public methods -----------------------------------------------------

        /**
         * adds a new group of folders
         *
         * @param {String} name
         *  localized String which is shown over the new group of folders
         *
         * @param {Array} folders
         *  Array of folder_ids which will be checked for users' "create" state
         *  (TODO: create as param???) and will be shown as breadcrumb view
         */
        this.addFolders = function (name, folders) {
            if (!folders.length) { return; }

            var treeContainer = $('<ul class="tree-container f6-target" role="tree">');
            treeContainer.appendTo(folderTree);

            var head = newGroupNode(name);
            head.on('select', selectNode);
            head.render().$el.appendTo(treeContainer);

            _.each(folders, function (id) {
                // deleteFromCache(id);

                var el = $('<li>');
                el.appendTo(treeContainer);

                getCreateState(id).always(function (state) {

                    if (state) {
                        var node = newFolderPickerNode(id);

                        node.on('select', selectNode);

                        getPath(id).done(function (path) {
                            if (path.length > 2) {
                                var show = path[1].title;
                                for (var i = 2; i < path.length; i++) {
                                    show += ' / ' + path[i].title;
                                }
                                show = getShortenedTitle(show, 40);

                                node.render().$el.find('.folder-label').text(show);
                            }
                        });

                        el.replaceWith(node.render().$el);

                        if (id === preselect) {
                            node.render().$el.click();
                        }
                        head.render().$el.attr('data-id', treeContainer.find('li[data-id]').first().attr('data-id'));
                    } else {
                        el.remove();
                        if (treeContainer.children().length <= 1) {
                            treeContainer.remove();
                        }
                    }
                });
            });
        };

        /**
         * @returns {jQuery}
         *  The jQuery node for using this object in the DOM.
         */
        this.getNode = function () {
            return folderTree;
        };

        /**
         * Returns the identifier of the selected folder.
         *
         * @returns {String|Null}
         *  The identifier of the selected folder; or null, if there is no
         *  selection available.
         */
        this.getFolderId = function () {
            return selectedFolder;
        };
    }
}

/**
 * with help of ox.TreeView this is a generic FolderPicker
 *
 * An instance of this class triggers the following events:
 * - 'change'
 *      After the selected folder has been changed (by user or by 'preselect')
 *      the 'change' event is triggered
 *
 * @param {Object} [initOptions]
 *  Optional parameters:
 *  - {String} [initOptions.folderId]
 *      Drive-File-Id to preselect a folder
 */
export class FolderPicker extends EObject {

    constructor(initOptions) {

        super();

        var self = this;
        var selectedFolder = null;

        var preselect = Utils.getStringOption(initOptions, 'folderId');
        var allowReadOnlySelection = Utils.getBooleanOption(initOptions, 'allowReadOnlySelection', false);

        var folderTree = new TreeView({
            context: 'popup',
            persistent: false,
            tabindex: 0,
            root: getRootId(),
            all: false,
            module: 'infostore',
            abs: false,
            filter(_parent, model) {
                return !FolderAPI.is('trash', model.toJSON());
            },
            customize(baton) {
                if (shouldBeDisabled(baton.data.id)) {
                    this.addClass('disabled');
                }
            },
            highlightclass: Utils.SMALL_DEVICE ? 'visible-selection-smartphone' : 'visible-selection'
        });

        folderTree.render();
        var treeNode = folderTree.$el;

        // extend filesPane with CoreSelection functionalities, plus keyboard selection support
        Selection.extend(folderTree, treeNode, {});

        treeNode.addClass('drive-folder-list');

        folderTree.on('change virtual', function (folder_id) {
            if (shouldBeDisabled(folder_id, allowReadOnlySelection)) {
                selectedFolder = null;
            } else {
                selectedFolder = folder_id;
            }
            self.trigger('change');
        });

        folderTree.on('select:item', function () {
            self.trigger('select:item');
        });

        if (preselect) {
            preselectFolder(folderTree, preselect);
        }

        // methods -----------------------------------------------------------------

        /**
         * @returns {jQuery}
         *  The jQuery node for using this object in the DOM.
         */
        this.getNode = function () {
            return treeNode;
        };

        /**
         * Returns the identifier of the selected folder.
         *
         * @returns {String|Null}
         *  The identifier of the selected folder; or null, if there is no
         *  selection available.
         */
        this.getFolderId = function () {
            return selectedFolder;
        };

        /**
         * Selects a folder by given id
         *
         * @param  {String} folder_id
         *  Folder-id
         */
        this.selectFolder = function (folder_id) {
            preselectFolder(folderTree, folder_id);
        };
    }
}

// private functions ------------------------------------------------------

function getState(fnName, state, id) {
    checkId(id);
    return getPath(id).then(function (list) {
        var last = _.last(list);
        return FolderAPI[fnName](state, last);
    }, function () {
        return null;
    });
}

function handleFolderPermissions(fnName, state, id) {
    return FolderAPI.get(id, { cache: false }).then(function (data) {

        var result = false;
        _.each(data.permissions, function (perm, index) {
            if (!index) {
                //owner permission
                return;
            }
            if (FolderAPI[fnName](state, { own_rights: perm.bits })) {
                result = true;
            }
        });
        return result;
    });
}

function checkId(id) {
    if (!_.isString(id)) {
        globalLogger.error('id is no string:', id);
        throw new Error('missing identifier');
    }
}

function bindFolderNodeHandlers(el, node) {
    el.find('.folder-node>.folder-label div').css('overflow', 'visible');

    el.on('click', function () {
        var others = el.parent().parent().find('li.folder.selectable');
        others.removeClass('selected');
        others.attr('aria-selected', false);
        others.attr('tabindex', -1);

        el.addClass('selected');
        el.attr('aria-selected', true);
        el.attr('tabindex', 0);
        setFocus(el);

        node.trigger('select');
    });
    el.on('keydown', function (evt) {
        var offset = 0;
        if (hasKeyCode(evt, 'UP_ARROW')) {
            offset--;
        } else if (hasKeyCode(evt, 'DOWN_ARROW')) {
            offset++;
        } else {
            return;
        }
        var allFolders = el.parent().parent().find('li.folder.selectable:visible');
        var index = null;
        _.find(allFolders, function (folder, idx) {
            if (el[0].isSameNode(folder)) {
                index = idx;
                return true;
            }
        });
        $(allFolders[index + offset]).trigger('click');
    });
}

// deferred functions -----------------------------------------------------

/**
 * checks if the assigned Drive-File-ID is the trash-folder
 * or in the trash-folder
 *  used by 'filter' of FolderPicker and its TreeView
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 *
 * @returns {Deferred}
 *  resolves with {Boolean} true if it is the trashfolder
 */
export function getTrashState(id) {
    return getState('is', 'trash', id);
}

/**
 * checks if current user can create files in the folder of the assigned id
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 *
 * @returns {Deferred}
 *  resolves with {Boolean} true if user can create files in the folder
 */
export function getCreateState(id) {
    return getState('can', 'create', id);
}

/**
 * checks if current user can write/edit files in the folder of the assigned id
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 *
 * @returns {Deferred}
 *  resolves with {Boolean} true if user can write/edit files in the folder
 */
export function getWriteState(id) {
    return getState('can', 'write', id);
}

/**
 * checks if current user can edit the assigned file-descriptor
 *
 * _Attention:_ assigned folder must be already in use by OX-Drive
 * because this function is syncron and can only communicate with cache of FolderAPI
 *
 * @param {Object} file is a file-descriptor
 *  the Drive-File of files
 *
 * @returns {Boolean}
 *  return true if user can edit current file
 */
export function canModify(file) {
    if (!file) {
        globalLogger.error('canModify() no file assigned');
        return false;
    }
    var model = FilesAPI.pool.get('detail').get(_.cid(file));

    // check file permissions
    var permission = pUtil.getOwnPermission(model.get('object_permissions'));
    var canModifyFile = Boolean(permission && (permission.bits >= 2));
    // when no file modify permissions, check the parent folder in a next step
    if (canModifyFile) { return true; }

    // check parent folder permissions
    var folderModel = FolderAPI.pool.models[model.get('folder_id')];
    if (!folderModel) {
        globalLogger.warn('canModify() is called too early, folder is not cached yet. better call getWriteState() file: ' + file);
        return false;
    }
    return FolderAPI.can('write', folderModel.toJSON());
}

export function isShareable(file) {
    if (!file) {
        globalLogger.error('isShareable() no file assigned');
        return false;
    }
    var folderModel = FolderAPI.pool.models[file.folder_id];
    if (!folderModel) {
        globalLogger.warn('isShareable() is called too early, folder is not cached yet. better call getWriteState() file: ' + file);
        return false;
    }
    return folderModel.isShareable();
}

/**
 * Check s if the file is from an external folder.
 *
 * @param {Object} file descriptor
 * @returns {Deferred} true if the file folder is an external folder otherwise false.
 */
export function isExternalFolder(file) {
    if (!file || file.folder_id === undefined) {
        globalLogger.error('isExternalFolder() no file assigned');
        return $.when(false);
    }
    return FolderAPI.get(file.folder_id).then(function (folder) {
        if (folder) {
            // Do not use FileStorage.isExternal! After browser refresh FileStorage rampupDone flag is false and so it can not check if it is a external storage
            return folder && FolderAPI.isExternalFileStorage(folder);
        }
        return false;
    });
}

/**
 * checks if assigned id is the trash folder
 *
 * _Attention:_ assigned folder must be already in use by OX-Drive
 * because this function is syncron and can only communicate with cache of FolderAPI
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 *
 * @returns {Boolean}
 *  return true if folder is the trash folder
 */
export function isTrash(id) {
    var model = FolderAPI.pool.models[id];
    if (!model) {
        globalLogger.warn('isTrash() is called to early, folder is not cached yet. better call getTrashState() id: ' + id);
        return false;
    }
    return FolderAPI.is('trash', model.toJSON());
}

/**
 * gives the complete path of the assigned Drive-File-ID includes itself
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 *
 * @returns {Deferred}
 *  resolves with {Array} path with all parent-folders and assigned file or folder
 */
export function getPath(id) {
    checkId(id);
    return FolderAPI.path(id);
}

/**
 * checks for all users which have permissions on assigned folder (except the owner)
 * if the have read access, if one has read access, promise is called with true
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 *
 * @returns {Deferred}
 *  resolves with {Boolean} if one user has read permission (true), or no user (false)
 */
export function hasFolderReadPermissions(id) {
    return handleFolderPermissions('can', 'read', id);
}

// normal functions -------------------------------------------------------

/**
 * return real Drive-File-ID of users documents folder
 * (My Files/Documents)
 *
 * @returns {String}
 *  Drive-File-ID
 */
export function getStandardDocumentsFolderId() {
    return optFolder(DriveSettings.get('folder/documents'));
}

/**
 * return real Drive-File-ID of users pictures folder
 * (My Files/Pictures)
 *
 * @returns {String}
 *  Drive-File-ID
 */
export function getStandardPicturesFolderId() {
    return optFolder(DriveSettings.get('folder/pictures'));
}

/**
 * return real Drive-File-ID of users template folder
 * (My Files/Document/Templates)
 *
 * @returns {String}
 *  Drive-File-ID
 */
export function getStandardTemplateFolderId() {
    return optFolder(DriveSettings.get('folder/templates'));
}

/**
 * return real Drive-File-ID of users trash folder
 * (Trash)
 *
 * @returns {String}
 *  Drive-File-ID
 */
export function getStandardTrashFolderId() {
    return optFolder(DriveSettings.get('folder/trash'));
}

/**
 * return real Drive-File-ID of users folder
 * (My Files)
 *
 * @returns {String}
 *  Drive-File-ID
 */
export function getMyFilesId() {
    return optString(FolderAPI.getDefaultFolder('infostore'));
}

/**
 * return real Drive-File-ID of root folder
 * (parent of My Files or Public Files);
 *
 * @returns {String}
 *  Drive-File-ID
 */
export function getRootId() {
    return '9';
}

/**
 * gives an extended FolderNode, which look similar like the FolderNode of
 * the FolderPicker and its TreeView.
 * But it is not used for a tree, it is used for a flat view.
 *
 * Reacts on KeyUp and KeyDown to select only one FolderNode at once and iss clickable.
 *
 * @returns {FolderNode}
 *  used by FolderPicker and its TreeView
 */
export function newFolderPickerNode(id) {
    var node = new FolderNode({
        indent: true,
        empty: true,
        subfolders: false,
        folder: id,
        tree: dummyTree,
        parent: dummyTree
    });
    var el = node.render().$el;
    el.find('.folder-node').css('pointer-events', 'none');
    bindFolderNodeHandlers(el, node);
    return node;
}

/**
 * if the assigned title is longer than assigned length,
 * it returns a new String with "..." in the middle,
 * if not the original String is returned
 *
 * @param {String} title
 *
 * @param {Number} length
 *  max length of the result String
 *
 * @returns {String}
 *  the shortened title if it was longer than assigned length
 */
export function getShortenedTitle(title, length) {
    return FolderAPI.getFolderTitle(title, length);
}

/**
 * Drive only only fetches once per session the file and folder datas
 * with this function you can clear the cache of a single file or folder,
 * so the next call fetches the data fresh
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 */
export function deleteFromCache(id) {
    try {
        delete FolderAPI.pool.models[id];
        //on this way i force a refresh of the folder, if its deleted
    } catch (e) {
        globalLogger.warn('cannot delete FolderAPI.pool.models', e);
    }
}

/**
 * show Drive permission dialog for assigned folder
 *
 * @param {String} fileId
 *  File identifer.
 */
export function showPermissionsDialog(fileId) {
    const fileModel = new FilesAPI.Model(FolderAPI.pool.getModel(fileId).toJSON());
    Permissions.showByModel(fileModel, { hasLinkSupport: false, share: true });
}

/**
 * listens to TreeViews first appearance
 *
 * if an id is assigned this function fetches its path
 * and iterates through the tree until it finds the correct folder
 * and selects this folder
 *
 * if there is no folder assigned it selects the first folder
 *
 * @param {TreeView} treeView
 *
 * @param {String} id
 *  the Drive-File-ID of files or folders
 */
export function preselectFolder(treeView, id) {
    var timeout = null;
    function open() {
        var repeatCounter = 0; // counting the failures trying to find the specified folder
        window.clearTimeout(timeout);
        treeView.off('appear', open);
        if (id) {
            var preselect = function (index, path) {
                var current = path[index].id;
                var folderEl = treeView.$el.find('.folder[data-id="' + current + '"]');
                if (folderEl.length) {
                    // element found, resetting counter
                    repeatCounter = 0;
                    //listen to appear:id
                    var next = path[index + 1];
                    if (!next) {
                        //done
                        folderEl.click();
                        treeView.trigger('select:item');
                        return;
                    }
                    treeView.once('appear:' + next.id, function () {
                        preselect(index + 1, path);
                    });

                    // if the current folder is collapsed
                    var collapsed = (folderEl.attr('aria-expanded') === 'false');
                    if (collapsed) {
                        //simulate click to open the current folder
                        folderEl.find('.folder-arrow').click();
                    }

                    // trigger appearing next folder
                    treeView.appear({ folder: next.id });

                } else {
                    // folder not found, trying later again (but at maximum 3 times)
                    if (repeatCounter < 3) {
                        window.setTimeout(function () {
                            preselect(index, path); // calling 'preselect' again, if folder was not found (58060)
                        }, 200);
                    }
                    repeatCounter++;
                }
            };
            getPath(id).done(function (path) {
                preselect(1, path);
            });
        } else {
            var selection = treeView.selection;
            if (selection) {
                selection.updateIndex().selectFirst();
                setFocus(treeView.$el.find('.selected'));
            }
        }
    }

    //FIXME: stupid hacks
    treeView.once('appear', open);
    timeout = window.setTimeout(open, 250);
}

export function preparePath(paths) {
    var names = _.pluck(_.rest(paths), 'title');
    _.each(names, function (name, index) {
        names[index] = name.replace(/\s/g, '\xA0');
    });
    var path = _.pluck(_.rest(paths), 'title').join('\xA0/ ');
    return path;
}

/**
 * Returns whether a document can be created in a given folder.
 * This check is synchronous, thus the folder model is received from
 * the pool and not by a new request.
 *
 * Make sure that the modes does exists in the pool before calling this function.
 *
 * @returns {String}
 *  Drive-Folder-ID
 */
export function canCreateDocInFolder(id) {
    var folderModel = FolderAPI.pool.models[id];
    if (!folderModel) {
        globalLogger.warn('Folder', id, 'is not in pool');
        return false;
    }
    return folderModel.can('create') && folderModel.can('write') && !folderModel.is('trash');
}

/**
 * Returns whether a file can be created in the the document default folder in Drive.
 * This value is cached for performance reasons and the whole functions is synchronous.
 *
 * To get the lastest state from the server call 'updateStateCreateInDefaultFolder()'
 * and make sure 'updateStateCreateInDefaultFolder()'  was at least called one time
 * before to have a correct init state.
 *
 * @returns {Boolean}
 *  Whether a file can be created in the documents default folder in Drive.
 */
export function canCreateInDefaultFolder() {
    return _canCreateInDefaultFolder;
}

/**
 * Request the current folder model from the server and update the cached property whether
 * a file can be created in the document default folder.
 *
 * @param {Boolean} [cache]
 *  With the default 'false' the required model is requested from the server,
 *  with 'true'the model is received from the cached model pool in Drive.
 *
 * @returns {jQuery.Promise}
 *  A promise that will be fulfilled when the update has been finished.
 */
export function updateStateCreateInDefaultFolder(cache) {
    // it's possible that there isn't a default forlder set
    if (!getStandardDocumentsFolderId()) {
        _canCreateInDefaultFolder = false;
        return $.when();
    }

    return FolderAPI.get(getStandardDocumentsFolderId(), { cache: !!cache }).then(function (data) {
        if (_.isObject(data) && _.isString(data.id)) {
            _canCreateInDefaultFolder = canCreateDocInFolder(data.id);
        }
    });
}
