/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * The components of a date without time as numbers: Day, month, and year.
 */
export interface DateComponents {

    /**
     * The full year (with century).
     */
    Y: number;

    /**
     * The **zero-based** month (`0` to `11`).
     */
    M: number;

    /**
     * The **one-based** day of the month (`1` to `31`).
     */
    D: number;
}

/**
 * The components of a time without date as numbers: Hours, minutes, seconds,
 * and milliseconds.
 */
export interface TimeComponents {

    /**
     * The number of hours (`0` to `23`).
     */
    h: number;

    /**
     * The number of minutes (`0` to `59`).
     */
    m: number;

    /**
     * The number of seconds (`0` to `59`).
     */
    s: number;

    /**
     * The number of milliseconds (`0` to `999`).
     */
    ms: number;
}

/**
 * The components of a date and time as numbers.
 */
export interface DateTimeComponents extends DateComponents, TimeComponents { }

/**
 * Enumeration of weekdays, starting with Monday according to ISO 8601.
 */
export enum ISOWeekDay { MO = 0, TU = 1, WE = 2, TH = 3, FR = 4, SA = 5, SU = 6 }

/**
 * Specifies how to handle the day in the resulting date when adding months or
 * years to a date.
 */
export enum DateAddMethod {

    /**
     * The day of the passed original date will be retained. If the resulting
     * month is shorter and does not contain that day, the last valid day of
     * the new month will be used. If the original day was a leap day (February
     * 29th), and the new year is not a leap year, the date will be set to
     * February 28th.
     */
    NORMAL = 0,

    /**
     * When adding months, the day in the resulting date will be set to the
     * value `1`. When adding years, the resulting date will be set to January
     * 1st.
     */
    FIRST = 1,

    /**
     * When adding months, the day in the resulting date will be set to the
     * last day of the resulting month. When adding years, the resulting date
     * will be set to December 31st.
     */
    LAST = 2
}

// constants ==================================================================

/**
 * The number of minutes per day.
 */
export const MIN_PER_DAY = 1_440;

/**
 * The number of seconds per day.
 */
export const SEC_PER_DAY = 86_400;

/**
 * The number of seconds per week.
 */
export const SEC_PER_WEEK = SEC_PER_DAY * 7;

/**
 * The number of milliseconds per day.
 */
export const MSEC_PER_DAY = SEC_PER_DAY * 1_000;

/**
 * The number of milliseconds per week.
 */
export const MSEC_PER_WEEK = MSEC_PER_DAY * 7;

// private constants ----------------------------------------------------------

// number of days per month (regular years)
const DAYS_PER_MONTH = [31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

// public functions ===========================================================

/**
 * Returns whether the passed number is a leap year.
 *
 * @param year
 *  The year to be checked.
 *
 * @returns
 *  Whether the passed number is a leap year.
 */
export function isLeapYear(year: number): boolean {
    return (((year % 4) === 0) && ((year % 100) !== 0)) || ((year % 400) === 0);
}

/**
 * Returns the number of days in the specified year.
 *
 * @param year
 *  The full year.
 *
 * @returns
 *  The number of days in the specified year.
 */
export function getDaysInYear(year: number): number {
    return isLeapYear(year) ? 366 : 365;
}

/**
 * Returns the number of days in the month February of the specified year.
 *
 * @param year
 *  The year.
 *
 * @returns
 *  The number of days in February of the specified year.
 */
export function getDaysInFebruary(year: number): number {
    return isLeapYear(year) ? 29 : 28;
}

/**
 * Returns the number of days in the specified month.
 *
 * @param year
 *  The year of the month.
 *
 * @param month
 *  The month. MUST be a zero-based integer (`0` to `11`).
 *
 * @returns
 *  The number of days in the specified month.
 */
export function getDaysInMonth(year: number, month: number): number {
    // special handling for February in leap years
    return (month === 1) ? getDaysInFebruary(year) : DAYS_PER_MONTH[month];
}

/**
 * Converts a one-digit or two-digit year to a four-digit year using the
 * current local century, according to the passed century threshold value.
 *
 * @param year
 *  A year number. MUST be non-negative. Values less than `100` will be
 *  converted to a year in the current, preceding, or following century,
 *  according to the passed threshold.
 *
 * @param [threshold=20]
 *  An integer in the range `0` to `99` that specifies the number of years to
 *  go into the future (from current year) for the years to be included in the
 *  result. If the passed number equals the remainder of one of these future
 *  years (modulo 100), the number will be converted to that respective year in
 *  the future, otherwise it will be converted to the nearest year in the past.
 *
 *  _Example 1_: In the year 2020, with the default threshold of 20, the input
 *  values 20...40 will be converted to the years 2020..2040 (up to 20 years
 *  into the future), the input values 0...19 will be converted to 2000...2019
 *  (preceding years), and the input values 41...99 will be converted to
 *  1941...1999 (preceding years).
 *
 *  _Example 2_: In the year 2090, with the default threshold of 20, the input
 *  values 90...99 will be converted to the years 2090...2099, and the input
 *  values 0...10 will be converted to 2100...2110 (up to 20 years into the
 *  future). The input numbers 11...89 will be converted to the years
 *  2011...2089 (preceding years).
 *
 * @returns
 *  The resulting four-digit year number.
 */
export function expandYear(year: number, threshold?: number): number {
    year = Math.floor(year);
    threshold ??= 20;
    if (year >= 100) { return year; }
    const currY = new Date().getFullYear();
    const diff = math.modulo(year - currY, 100);
    return (diff <= threshold) ? (currY + diff) : (currY + diff - 100);
}

/**
 * Returns the components of the passed UTC date value as single numeric
 * properties.
 *
 * @param utcDate
 *  The date to be converted, as UTC date value.
 *
 * @returns
 *  The date components.
 */
export function splitUTCDate(utcDate: Date): DateComponents {
    return {
        Y: utcDate.getUTCFullYear(),
        M: utcDate.getUTCMonth(),
        D: utcDate.getUTCDate()
    };
}

/**
 * Returns the date components of the passed local date value as single numeric
 * properties.
 *
 * @param lclDate
 *  The date to be converted, as local date value.
 *
 * @returns
 *  The date components.
 */
export function splitLocalDate(lclDate: Date): DateComponents {
    return {
        Y: lclDate.getFullYear(),
        M: lclDate.getMonth(),
        D: lclDate.getDate()
    };
}

/**
 * Returns the date and time components of the passed UTC date value as single
 * numeric properties.
 *
 * @param utcDateTime
 *  The date to be converted, as UTC date value.
 *
 * @returns
 *  The date and time components.
 */
export function splitUTCDateTime(utcDateTime: Date): DateTimeComponents {
    const comps = splitUTCDate(utcDateTime) as DateTimeComponents;
    comps.h = utcDateTime.getUTCHours();
    comps.m = utcDateTime.getUTCMinutes();
    comps.s = utcDateTime.getUTCSeconds();
    comps.ms = utcDateTime.getUTCMilliseconds();
    return comps;
}

/**
 * Returns the date and time components of the passed local date value as
 * single numeric properties.
 *
 * @param lclDateTime
 *  The date to be converted, as local date value.
 *
 * @returns
 *  The date and time components.
 */
export function splitLocalDateTime(lclDateTime: Date): DateTimeComponents {
    const comps = splitLocalDate(lclDateTime) as DateTimeComponents;
    comps.h = lclDateTime.getHours();
    comps.m = lclDateTime.getMinutes();
    comps.s = lclDateTime.getSeconds();
    comps.ms = lclDateTime.getMilliseconds();
    return comps;
}

/**
 * Creates a UTC date value from the passed date components.
 *
 * @param comps
 *  The date components.
 *
 * @returns
 *  The UTC date value representing the specified date. The time will be set to
 *  midnight.
 */
export function makeUTCDate(comps: DateComponents): Date {
    return new Date(Date.UTC(comps.Y, comps.M, comps.D));
}

/**
 * Creates a local date value from the passed date components.
 *
 * @param comps
 *  The date components.
 *
 * @returns
 *  The local date value representing the specified date. The time will be set
 *  to midnight.
 */
export function makeLocalDate(comps: DateComponents): Date {
    return new Date(comps.Y, comps.M, comps.D);
}

/**
 * Creates a UTC date value from the passed date and time components.
 *
 * @param comps
 *  The date and time components.
 *
 * @returns
 *  The UTC date value representing the specified date and time.
 */
export function makeUTCDateTime(comps: DateTimeComponents): Date {
    return new Date(Date.UTC(comps.Y, comps.M, comps.D, comps.h, comps.m, comps.s, comps.ms));
}

/**
 * Creates a local date value from the passed date and time components.
 *
 * @param comps
 *  The date and time components.
 *
 * @returns
 *  The local date value representing the specified date and time.
 */
export function makeLocalDateTime(comps: DateTimeComponents): Date {
    return new Date(comps.Y, comps.M, comps.D, comps.h, comps.m, comps.s, comps.ms);
}

/**
 * Creates a UTC date and time value (without timezone offset) looking like the
 * passed local date and time (with timezone offset), without changing the
 * date/time components.
 *
 * Example: If the passed local date and time is 2000-01-01 at 12:00:00, the
 * result will be the date value `2000-01-01T12:00:00Z` (UTC) regardless of the
 * local timezone.
 *
 * @returns
 *  The UTC date value looking like the passed local date and time value.
 */
export function getLocalDateAsUTC(lclDateTime: Date): Date {
    return makeUTCDateTime(splitLocalDateTime(lclDateTime));
}

/**
 * Creates a local date and time value (with timezone offset) looking like the
 * passed UTC date and time (without timezone offset), without changing the
 * date/time components.
 *
 * Example: If the passed UTC date and time is 2000-01-01 at 12:00:00, the
 * result will be the date value `2000-01-01T12:00:00+nnnn` in the local
 * timezone `nnnn`.
 *
 * @returns
 *  The local date value looking like the passed UTC date and time value.
 */
export function getUTCDateAsLocal(utcDateTime: Date): Date {
    return makeLocalDateTime(splitUTCDateTime(utcDateTime));
}

/**
 * Creates a UTC date value (without timezone offset) looking like the current
 * local date (with timezone offset), without changing the date components.
 *
 * Example: If the current local date is 2000-01-01, the result will be the
 * date value `2000-01-01T00:00:00Z` (UTC) regardless of the local timezone.
 *
 * @returns
 *  The UTC date value looking like the current local date. The time will be
 *  set to midnight.
 */
export function getLocalTodayAsUTC(): Date {
    return makeUTCDate(splitLocalDate(new Date()));
}

/**
 * Creates a local date value (with timezone offset) looking like the current
 * UTC date (without timezone offset), without changing the date components.
 *
 * Example: If the current UTC date is 2000-01-01, the result will be the date
 * value `2000-01-01T00:00:00+nnnn` in the local timezone `nnnn`.
 *
 * @returns
 *  The local date value looking like the current UTC date. The time will be
 *  set to midnight.
 */
export function getUTCTodayAsLocal(): Date {
    return makeLocalDate(splitUTCDate(new Date()));
}

/**
 * Creates a UTC date value (without timezone offset) looking like the current
 * date and time (with timezone offset), without changing the date/time
 * components.
 *
 * Example: If the current local date and time is 2000-01-01 at 12:00:00, the
 * result will be the date value `2000-01-01T12:00:00Z` (UTC) regardless of the
 * local timezone.
 *
 * @returns
 *  The UTC date value looking like the current local date and time.
 */
export function getLocalNowAsUTC(): Date {
    return makeUTCDateTime(splitLocalDateTime(new Date()));
}

/**
 * Creates a local date value (with timezone offset) looking like the current
 * UTC date and time (without timezone offset), without changing the date and
 * time components.
 *
 * Example: If the current UTC date and time is 2000-01-01 at 12:00:00, the
 * result will be the date value `2000-01-01T12:00:00+nnnn` in the local
 * timezone `nnnn`.
 *
 * @returns
 *  The local date value looking like the current UTC date and time.
 */
export function getUTCNowAsLocal(): Date {
    return makeLocalDateTime(splitUTCDateTime(new Date()));
}

/**
 * Adds the specified number of days to the passed UTC date and time, and
 * returns the resulting date value.
 *
 * @param utcDateTime
 *  The date and time, as UTC date value.
 *
 * @param days
 *  The number of days to be added. If negative, a date in the past will be
 *  returned accordingly. MUST be an integral number.
 *
 * @returns
 *  The new date, as UTC date value.
 */
export function addDaysToDate(utcDateTime: Date, days: number): Date {
    return new Date(utcDateTime.getTime() + days * MSEC_PER_DAY);
}

/**
 * Adds the specified number of weeks to the passed UTC date and time, and
 * returns the resulting date value.
 *
 * @param utcDateTime
 *  The date and time, as UTC date value.
 *
 * @param weeks
 *  The number of weeks to be added. If negative, a date in the past will be
 *  returned accordingly. MUST be an integral number.
 *
 * @returns
 *  The new date, as UTC date value.
 */
export function addWeeksToDate(utcDateTime: Date, weeks: number): Date {
    return new Date(utcDateTime.getTime() + weeks * MSEC_PER_WEEK);
}

/**
 * Adds the specified number of months to the passed UTC date and time, and
 * returns the resulting date value.
 *
 * @param utcDateTime
 *  The date and time, as UTC date value.
 *
 * @param months
 *  The number of months to be added. If negative, a date in the past will be
 *  returned accordingly. MUST be an integral number.
 *
 * @param [method]
 *  Specifies how to handle the day in the resulting date.
 *
 * @returns
 *  The new date, as UTC date value.
 */
export function addMonthsToDate(utcDateTime: Date, months: number, method = DateAddMethod.NORMAL): Date {

    // extract the date components from the passed date
    const comps = splitUTCDateTime(utcDateTime);

    // calculate the new month and year
    const month = comps.M + months;
    comps.Y += Math.floor(month / 12);
    comps.M = math.modulo(month, 12);

    // validate the day (new month may have less days than the day passed in the original date)
    switch (method) {
        case DateAddMethod.FIRST:
            comps.D = 1;
            break;
        case DateAddMethod.LAST:
            comps.D = getDaysInMonth(comps.Y, comps.M);
            break;
        default:
            comps.D = Math.min(comps.D, getDaysInMonth(comps.Y, comps.M));
    }

    // build and return the new date
    return makeUTCDateTime(comps);
}

/**
 * Adds the specified number of years to the passed UTC date and time, and
 * returns the resulting date value.
 *
 * @param utcDateTime
 *  The date and time, as UTC date value.
 *
 * @param years
 *  The number of years to be added. If negative, a date in the past will be
 *  returned accordingly. MUST be an integral number.
 *
 * @param [method]
 *  Specifies how to handle the day in the resulting date.
 *
 * @returns
 *  The new date, as UTC date value.
 */
export function addYearsToDate(utcDateTime: Date, years: number, method = DateAddMethod.NORMAL): Date {

    // extract the date components from the passed date
    const comps = splitUTCDateTime(utcDateTime);

    // calculate the new year
    comps.Y += years;

    // validate the month and day (correct old leap day in non-leap years)
    switch (method) {
        case DateAddMethod.FIRST:
            comps.M = 0;
            comps.D = 1;
            break;
        case DateAddMethod.LAST:
            comps.M = 11;
            comps.D = 31;
            break;
        default:
            if ((comps.M === 1) && (comps.D === 29) && !isLeapYear(comps.Y)) {
                comps.D = 28;
            }
    }

    // build and return the new date
    return makeUTCDateTime(comps);
}

/**
 * Returns the weekday index of the passed UTC date for weeks starting with
 * Monday (as defined in ISO 8601).
 *
 * @param utcDate
 *  The date, as UTC date value.
 *
 * @returns
 *  The weekday index. Value `0` represents Monday, and `6` represents Sunday.
 */
export function getISOWeekDay(utcDate: Date): ISOWeekDay {
    return (utcDate.getUTCDay() + 6) % 7;
}

/**
 * Returns the date of the first day of the week the passed UTC date is part of
 * (i.e. the date of Monday as defined in ISO 8601).
 *
 * @param utcDate
 *  The date, as UTC date value.
 *
 * @returns
 *  The date of the first day of the week the passed UTC date is part of.
 */
export function getFirstDayOfISOWeek(utcDate: Date): Date {
    return addDaysToDate(utcDate, -getISOWeekDay(utcDate));
}

/**
 * Returns the date of the first day of the month the passed UTC date is part
 * of.
 *
 * @param utcDate
 *  The date, as UTC date value.
 *
 * @returns
 *  The date of the first day of the month the passed UTC date is part of.
 */
export function getFirstDayOfMonth(utcDate: Date): Date {
    return new Date(Date.UTC(utcDate.getUTCFullYear(), utcDate.getUTCMonth(), 1));
}

/**
 * Returns the date of the first day of the year the passed UTC date is part
 * of.
 *
 * @param utcDate
 *  The date, as UTC date value.
 *
 * @returns
 *  The date of the first day of the year the passed UTC date is part of.
 */
export function getFirstDayOfYear(utcDate: Date): Date {
    return new Date(Date.UTC(utcDate.getUTCFullYear(), 0, 1));
}

/**
 * Returns the week number for the passed UTC date. Implementation helper for
 * the sheet functions `WEEKNUM` and `ISOWEEKNUM`.
 *
 * @param utcDate
 *  The date, as UTC date value.
 *
 * @param firstDayOfWeek
 *  The index of the first day of the week to be used for calculation (starting
 *  with `0` as Monday according to ISO 8601).
 *
 * @param iso8601
 *  Set to `true` to use ISO 8601 mode (week of January 1st is the first week,
 *  if at least 4 days are in the new year). Set to `false` to use US mode
 *  (weeks are counted strictly inside the year, first and last week in the
 *  year may consist of less than 7 days).
 *
 * @returns
 *  The resulting one-based week number for the passed date.
 *
 * @throws
 *  The `#NUM!` error code, if the date mode is not valid.
 */
export function getWeekNumber(utcDate: Date, firstDayOfWeek: ISOWeekDay, iso8601: boolean): number {

    // returns the date of the first day of the first week, according to specified first day in week
    function getNullDateOfYear(year: number): Date {

        // base date (that is always in the first week of the year; ISO: Jan-04, otherwise: Jan-01)
        const baseDate = makeUTCDate({ Y: year, M: 0, D: iso8601 ? 4 : 1 });
        // week day of the base date
        const baseDay = getISOWeekDay(baseDate);

        // first day of first week in the year, according to date mode
        return addDaysToDate(baseDate, -math.modulo(baseDay - firstDayOfWeek, 7));
    }

    // the year of the passed date
    const currYear = utcDate.getUTCFullYear();
    // first day of first week in the year, according to date mode
    let nullDate = getNullDateOfYear(currYear);

    // adjustment for ISO mode: passed date may be counted for the previous year
    // (for Jan-01 to Jan-03), or for the next year (for Dec-29 to Dec-31)
    if (iso8601) {
        if (utcDate < nullDate) {
            // passed date is counted for previous year (e.g. Jan-01-2000 is in week 52 of 1999)
            nullDate = getNullDateOfYear(currYear - 1);
        } else {
            const nextNullDate = getNullDateOfYear(currYear + 1);
            if (nextNullDate <= utcDate) {
                // passed date is counted for next year (e.g. Dec-31-2001 is in week 1 of 2002)
                nullDate = nextNullDate;
            }
        }
    }

    return Math.floor((utcDate.getTime() - nullDate.getTime()) / MSEC_PER_WEEK) + 1;
}
