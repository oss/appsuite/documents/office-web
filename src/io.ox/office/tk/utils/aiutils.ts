/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { httpClient } from "$/io.ox/core/http-client";
import { settings } from "$/io.ox/core/settings";
import switchboardApi from "$/io.ox/switchboard/api";
import { ary, is } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * The answer returned from the AI service.
 */
interface PromptResponse {
    content: string;
}

// constants ==================================================================

const hostname = settings.get("ai/hostname");
const aiModel = settings.get("ai/openai/model", "");
const useAzure = settings.get("ai/openai/useAzure", false);

const hooks = {
    beforeRequest: [
        async (request: Request) => {
            const jwt = await switchboardApi.getJwt();
            request.headers.set("Authorization", `Bearer ${jwt}`);
        }
    ]
};

const searchParams = new URLSearchParams();
if (aiModel && is.string(aiModel)) { searchParams.set("model", aiModel); }

const aiServiceClient = httpClient.extend({
    prefixUrl: `https://${hostname}/api`,
    timeout: 120000,
    hooks,
    searchParams
});

// public functions ===========================================================

/**
 * Execute one or more prompts.
 *
 * @param prompt
 *  The prompt or the list of prompts for the AI service.
 *
 * @returns
 *  A promise that will be resolved with the string that contains the AI's response(s)
 *  inside an object with the property "content".
 */
export async function executePrompt(prompt: string | string[]): Promise<string | string[]> {
    if (!hostname) { throw new Error("ai/executePrompt: Missing hostname"); }
    const allResponses = [];

    for (const onePrompt of ary.wrap(prompt)) {
        const response = await aiServiceClient.post(useAzure ? "azure" : "chatgpt", { json: { payload: onePrompt } }).json<PromptResponse>();
        allResponses.push(response.content.trim());
    }

    return allResponses.length > 1 ? allResponses : allResponses[0];
}
