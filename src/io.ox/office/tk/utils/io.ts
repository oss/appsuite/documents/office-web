/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ResponsePromise } from "$/io.ox/core/http";
import CoreHTTP from "$/io.ox/core/http";

import { is, jpromise } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * An enumeration of HTTP methods supported by the function `sendRequest()`.
 */
export type SendRequestMethod = "GET" | "POST" | "PUT" | "DELETE" | "UPLOAD";

/**
 * Optional parameters for the function `sendRequest()`.
 */
export interface SendRequestOptions {

    /**
     * The HTTP request method. Default value is "GET".
     */
    method?: SendRequestMethod;

    /**
     * The data type of the POST data, e.g. "json", "text", or "xml". Default
     * value is "json".
     */
    dataType?: string;
}

/**
 * An abortable promise that will fulfil with remote JSON data.
 */
export type { ResponsePromise };

// constants ==================================================================

/**
 * The name of the document filter server module.
 */
export const FILTER_MODULE_NAME = "oxodocumentfilter";

/**
 * The name of the document converter server module.
 */
export const CONVERTER_MODULE_NAME = "oxodocumentconverter";

// public functions ===========================================================

/**
 * Sends a JSON request to the server and returns a promise waiting for the
 * response.
 *
 * @param module
 *  The name of the server module that will receive the request.
 *
 * @param params
 *  Parameters that will be inserted into the request URL (method `GET`), or
 *  into the request body (method `POST`).
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A promise that will fulfil with the JSON data of the response. The promise
 *  object contains the additional method `abort()` that allows to abort the
 *  running request which rejects the promise immediately.
 */
export function sendRequest(module: string, params: Dict, options?: SendRequestOptions): ResponsePromise {

    // the HTTP method
    const method = options?.method ?? "GET";

    // options passed to the core function
    const coreOptions: Dict = { module };

    // add the parameters passed to this function
    const paramProp = (method === "GET") ? "params" : "data";
    coreOptions[paramProp] = params;

    // add data type for the ajax response when provided
    if (options?.dataType) { coreOptions.dataType = options.dataType; }

    // deferred for synchronous abort (54803)
    const deferred = jpromise.deferred<unknown>();
    // the corresponding promise object
    const promise = deferred.promise() as ResponsePromise;

    // send the AJAX request
    const request = CoreHTTP[method](coreOptions);

    // evaluate the server response
    request.done(response => {
        if (is.dict(response) && (response.hasErrors === true)) {
            deferred.reject(response);
        } else {
            deferred.resolve(response);
        }
    });

    // reject the exported promise, if the server request fails
    request.fail(response => deferred.reject(response));

    // implement the `abort()` method of `ResponsePromise`
    promise.abort = (): void => {
        request.abort();
        deferred.reject("abort");
    };

    return promise;
}
