/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

import { is, math, str, fmt, fun, json } from "@/io.ox/office/tk/algorithms";
import { SMALL_DEVICE } from "@/io.ox/office/tk/dom/device";
import type { ElementLabelOptions, CreateElementOptions } from "@/io.ox/office/tk/dom/domutils";
import { createElement, createSpan, setElementAttribute } from "@/io.ox/office/tk/dom/domutils";
import { type IconId, createIcon } from "@/io.ox/office/tk/dom/iconutils";
import { hasKeyCode } from "@/io.ox/office/tk/dom/keycodes";

// types ======================================================================

/**
 * The type of a DOM text input element (`<input>` or `<textarea>` elements).
 */
export type HTMLTextElement = HTMLInputElement | HTMLTextAreaElement;

/**
 * The type of an element with attribute "disabled".
 */
export type HTMLDisabledElement = HTMLButtonElement | HTMLTextElement | HTMLSelectElement | HTMLOptionElement | HTMLOptGroupElement | HTMLFieldSetElement;

/**
 * A callback function that converts a form control value to a string. These
 * strings are used to augment DOM elements, e.g. for automated tests.
 *
 * @param value
 *  The value to be serialized.
 *
 * @returns
 *  The serialized value; or `null` to indicate a non-serializable value.
 */
export type ControlValueSerializerFn<T> = (value: T) => string;

/**
 * Options for adding an icon, a text label, and a tooltip to a form control.
 */
export interface ControlCaptionOptions extends ElementLabelOptions {

    /**
     * The identifier of an icon to be inserted into the form control.
     */
    icon?: IconId;

    /**
     * Additional icon classes.
     */
    iconClasses?: string;

    /**
     * Alignment of the text label. Default value depends on the type of the
     * form control.
     */
    labelAlign?: "start" | "center" | "end";
}

/**
 * Generic options for creating a new form control element with an icon, a text
 * label, and a tooltip.
 */
export interface CreateControlCaptionOptions extends CreateElementOptions, ControlCaptionOptions {

    /**
     * A custom render function that allows to render arbitrary contents into
     * the form control. Will be called once when rendering the form control
     * (no automatic reactive updates), after the default caption contents have
     * been rendered (e.g. options "label" and "icon").
     */
    renderCaption?: (elem: HTMLElement) => void;
}

/**
 * Options for creating a `<fieldset>` element.
 */
export interface CreateFieldSetOptions extends CreateControlCaptionOptions {

    /**
     * If specified, the `<fieldset>` element will contain a `<legend>` element
     * with the specified label text.
     */
    legend?: string;

    /**
     * If set to `true`, the child elements of the `<fieldset>` will be
     * indented (except for the leading `<legend>` element). Default value is
     * `false`.
     */
    indent?: boolean;
}

/**
 * Options for creating a new `<label>` element.
 */
export interface CreateLabelOptions extends CreateControlCaptionOptions {

    /**
     * An identifier for the `for` element attribute.
     */
    for?: string;
}

/**
 * The direction for a caret symbol element.
 */
export type CaretDirection = "up" | "down" | "left" | "right";

/**
 * Options for creating a new `<button>` element.
 */
export interface CreateButtonOptions extends CreateControlCaptionOptions {

    /**
     * The appearance of the button (Bootstrap button type specifier). The
     * following custom types will be supported:
     *
     * - "bare": will create a borderless button that behaves like a default
     *   button (hover effects etc.).
     * - "delete": will create a borderless button (like "bare") with a heavy
     *   red hover effect.
     *
     * Default value is "default".
     */
    type?: "default" | "primary" | "secondary" | "success" | "warning" | "danger" | "info" | "link" | "bare" | "delete";

    /**
     * Show a caret icon at the end of the button into the specified direction.
     * By default, no caret icon will be created.
     */
    caret?: CaretDirection;

    /**
     * A custom action key that will be stored as "data-action" attribute.
     */
    action?: string;

    /**
     * If set to `true`, the control element will contain the "disabled"
     * attribute. Default value is `false`.
     */
    disabled?: boolean;

    /**
     * If set to `false`, the control element will not be focusable. Default
     * value is `true`.
     */
    focusable?: boolean;

    /**
     * A callback handler for the "click" event.
     */
    click?: (event: Event) => void;
}

/**
 * Shared options for creating a new `<input>` or `<textarea>` element.
 */
export interface TextControlOptions {

    /**
     * If set to `true`, the control element will contain the "disabled"
     * attribute. Default value is `false`.
     */
    disabled?: boolean;

    /**
     * The placeholder text that will be shown in the empty control element. If
     * omitted, no placeholder will be shown.
     */
    placeholder?: string;

    /**
     * If specified, the maximum number of characters that can be entered. If
     * omitted, the text length will not be restricted.
     */
    maxLength?: number;

    /**
     * Whether to enable spellchecking for the text control. Default value is
     * `false`.
     */
    spellcheck?: boolean;
}

/**
 * Options for creating a new `<input>` element.
 */
export interface CreateInputOptions extends CreateElementOptions, TextControlOptions {

    /**
     * The type of the `<input>` element (the value of its "type" attribute).
     * Default value is "text".
     */
    type?: string;

    /**
     * Filter for accepted files. Only valid, if "type" has been set to "file".
     */
    accept?: string;
}

/**
 * Options for creating a new `<textarea>` element.
 */
export interface CreateTextAreaOptions extends CreateElementOptions, TextControlOptions { }

/**
 * The selection state of a text input form control (`<input>` or `<textarea>`
 * elements).
 */
export interface InputSelection {

    /**
     * The index of the first selected character.
     */
    start: number;

    /**
     * The index of the first character after the section.
     */
    end: number;
}

/**
 * Options for refreshing a view element.
 */
export interface RefreshOptions {

    /**
     * Specifies whether to refresh the view element immediately (i.e.,
     * synchronously). By default, multiple refresh requests will be collected
     * and executed once debounced.
     */
    immediate?: boolean;
}

// private functions ==========================================================

/**
 * Converts the first ASCII character of a string to its two-digit hexadecimal
 * representation.
 */
function serializeControlChar(c: string): string {
    const code = c.charCodeAt(0);
    return code ? `\\x${fmt.formatInt(code, 16, { digits: 2 })}` : "\\0";
}

/**
 * Helper function to serialize a discrete control value, e.g. a single element
 * of an array.
 */
function implSerializeValue<T>(value: T, serializerFn: Opt<ControlValueSerializerFn<T>>): string | null {

    // do not serialize `undefined` and functions
    if ((value === undefined) || is.function(value)) { return null; }

    // special handling for value `null` used for ambiguous state in the UI
    if (value === null) { return ":mixed"; }

    // try to convert the value to a string (or `null`)
    let result: string | null;
    if (serializerFn) {
        result = fun.try(() => serializerFn(value), null);
    } else if (is.string(value)) {
        result = value.replace(/\\/g, "\\\\").replace(/[\0-\x1f\x80-\x9f]/g, serializeControlChar);
    } else {
        result = fun.try(() => json.tryStringify(value), null);
    }

    // convert empty strings to `null`
    return result || null;
}

/**
 * Initializes a new `<input>` or `<textarea>` control element.
 *
 * @param elem
 *  The text control to be initialized.
 *
 * @param [options]
 *  Optional parameters for initialization.
 */
function initTextControl(elem: HTMLTextElement, options?: TextControlOptions): void {
    if (options?.disabled) { enableElement(elem, false); }
    if (options?.placeholder) {
        elem.placeholder = options.placeholder;
        elem.setAttribute("aria-label", options.placeholder);
    }
    if (options?.maxLength) { elem.maxLength = options.maxLength; }
    elem.spellcheck = !!options?.spellcheck;
    elem.autocomplete = "off";
    if (SMALL_DEVICE) {
        elem.setAttribute("autocapitalize", "none");
        elem.setAttribute("autocorrect", "off");
    }
}

// public functions ===========================================================

/**
 * Updates the attributes "disabled" and "aria-disabled" of the specified DOM
 * element.
 *
 * @param elem
 *  The DOM element to be manipulated. Must be a form control element that
 *  supports the "disabled" attribute.
 *
 * @param state
 *  Whether to enable (`true`) or disable (`false`) the element.
 */
export function enableElement(elem: HTMLDisabledElement, state: boolean): void {
    elem.disabled = !state;
    setElementAttribute(elem, "aria-disabled", state ? null : true);
}

// control values -------------------------------------------------------------

/**
 * Converts the passed form control value to a string. These strings are used
 * to add data attributes to DOM elements that can be used in CSS selectors,
 * e.g. for automated tests.
 *
 * @param value
 *  The form control value to be serialized.
 *
 * @param [serializerFn]
 *  A callback function that converts the passed value to a string. By default,
 *  the value will be serialized with `json.tryStringify` if possible.
 *
 * @returns
 *  The serialized value; or `null` to indicate a non-serializable value.
 */
export function serializeControlValue<T>(value: OrArray<T>, serializerFn?: ControlValueSerializerFn<T>): string | null {

    // serialize single array elements
    if (is.readonlyArray(value)) {
        return value.map(v => implSerializeValue(v, serializerFn)).join(",");
    }

    return implSerializeValue(value, serializerFn);
}

// container elements ---------------------------------------------------------

/**
 * Creates a `<fieldset>` element. Optionally, creates a `<legend>` element
 * with a text label.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new `<fieldset>` element.
 */
export function createFieldSet(options?: CreateFieldSetOptions | string): HTMLFieldSetElement {
    const fieldSet = createElement("fieldset", options);
    if (options && !is.string(options)) {
        if (options.indent) { fieldSet.classList.add("indent"); }
        if (options.legend) { fieldSet.append(createElement("legend", { label: options.legend })); }
    }
    return fieldSet;
}

// label elements -------------------------------------------------------------

/**
 * Creates a new `<label>` control element.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The new `<label>` control element.
 */
export function createLabel(options?: CreateLabelOptions): HTMLLabelElement {
    const elem = createElement("label", { ...options, label: null });
    if (options?.for) { elem.setAttribute("for", options.for); }
    if (options?.icon) { elem.append(createIcon(options.icon)); }
    if (options?.label) { elem.append(createSpan({ label: options.label })); }
    options?.renderCaption?.(elem);
    return elem;
}

// button elements ------------------------------------------------------------

/**
 * Creates a `<span>` element with CSS marker class "caret-node" containing a
 * dropdown caret symbol intended to be inserted into a button control element.
 *
 * @param direction
 *  The direction for the caret symbol.
 *
 * @returns
 *  The `<span>` element containing a caret icon.
 */
export function createCaret(direction: CaretDirection): HTMLSpanElement {
    const elem = createSpan("caret-node");
    elem.append(createIcon(`bi:caret-${direction}-fill`));
    return elem;
}

/**
 * Creates a new `<button>` control element.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The new `<button>` control element.
 */
export function createButton(options?: CreateButtonOptions): HTMLButtonElement {
    const elem = createElement("button", { ...options, label: null });
    elem.type = "button";
    elem.classList.add("btn", `btn-${options?.type ?? "default"}`);
    if (options?.action) { elem.dataset.action = options.action; }
    if (options?.disabled) { enableElement(elem, false); }
    if (options?.focusable === false) { // focusable: `true` and `undefined` (default)
        elem.tabIndex = -1;
        elem.setAttribute("aria-hidden", "true");
    }
    if (options?.icon) { elem.append(createIcon(options.icon)); }
    if (options?.label) {
        const labelNode = elem.appendChild(createSpan({ classes: "label-node", label: options.label }));
        if (options?.labelAlign) { labelNode.style.textAlign = options.labelAlign; }
    }
    if (options?.caret) { elem.append(createCaret(options.caret)); }
    options?.renderCaption?.(elem);
    if (options?.click) {
        elem.addEventListener("click", options.click);
        elem.addEventListener("keyup", event => {
            if (hasKeyCode(event, "ENTER")) {
                options.click!(event);
            }
        });
    }
    return elem;
}

/**
 * Creates a new `<button>` control element with a trash icon to be used in
 * lists to delete the list item.
 *
 * @param clickFn
 *  The callback handler for the "click" event.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The new `<button>` control element.
 */
export function createDeleteButton(clickFn: VoidFunction, options?: CreateButtonOptions): HTMLButtonElement {
    return createButton({
        action: "delete",
        type: "delete",
        icon: "bi:trash",
        tooltip: gt("Delete"),
        click: clickFn,
        ...options
    });
}

// text control elements ------------------------------------------------------

/**
 * Creates a new `<input>` control element.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new `<input>` control element.
 */
export function createInput(options?: CreateInputOptions | string): HTMLInputElement {
    const elem = createElement("input", options);
    options = is.string(options) ? { classes: options } : options;
    initTextControl(elem, options);
    elem.type = options?.type || "text";
    if ((elem.type === "file") && options?.accept) { elem.accept = options.accept; }
    return elem;
}

/**
 * Creates a new `<textarea>` control element.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new `<textarea>` control element.
 */
export function createTextArea(options?: CreateTextAreaOptions | string): HTMLTextAreaElement {
    const elem = createElement("textarea", options);
    options = is.string(options) ? { classes: options } : options;
    initTextControl(elem, options);
    return elem;
}

/**
 * Returns whether the passed DOM element is either an `<input>` element or an
 * `<textarea>` element.
 *
 * @param elem
 *  The element to be checked.
 *
 * @returns
 *  Whether the passed DOM element is a text input control.
 */
export function isTextInputElement(elem: unknown): elem is HTMLTextElement {
    return (elem instanceof HTMLInputElement) || (elem instanceof HTMLTextAreaElement);
}

/**
 * Returns the current selection in the passed text input control.
 *
 * @param elem
 *  A text input element (an `<input>` or `<textarea>` element).
 *
 * @returns
 *  The selection state of the passed text input control.
 */
export function getInputSelection(elem: HTMLTextElement): InputSelection {

    // bug 31286: property access may throw (Mobile Chrome input elements of type "number" do not support selection)
    try {
        const start = elem.selectionStart;
        const end = elem.selectionEnd;
        if (is.number(start) && is.number(end)) { return { start, end }; }
    } catch { }

    // fallback to cursor position behind text
    const pos = elem.value.length;
    return { start: pos, end: pos };
}

/**
 * Returns whether the current selection in the passed text input control is a
 * simple text cursor (no characters selected).
 *
 * @param elem
 *  A text input element (an `<input>` or `<textarea>` element).
 *
 * @returns
 *  Whether the current selection in the passed text input control is a simple
 *  text cursor (no characters selected).
 */
export function isCursorSelection(elem: HTMLTextElement): boolean {
    const selection = getInputSelection(elem);
    return selection.start === selection.end;
}

/**
 * Changes the current selection in the passed text input control.
 *
 * @param elem
 *  A text input element (an `<input>` or `<textarea>` element).
 *
 * @param [selection]
 *  The new selection for the text input control. If omitted, the entire text
 *  will be selected.
 *
 * @param start
 *  The start character offset of the new selection in the text input control.
 *
 * @param [end=start]
 *  The end character offset of the new selection in the text input control. If
 *  omitted, sets a text cursor according to the passed start position.
 */
export function setInputSelection(elem: HTMLTextElement, selection?: InputSelection): void;
export function setInputSelection(elem: HTMLTextElement, start: number, end?: number): void;
// implementation
export function setInputSelection(elem: HTMLTextElement, param?: InputSelection | number, end?: number): void {

    // bug 31286: property access may throw (Mobile Chrome input elements of type "number" do not support selection)
    try {
        const start = Math.min(is.number(param) ? param : param ? param.start : 0, elem.value.length);
        end = math.clamp(is.number(param) ? (end ?? start) : param ? param.end : elem.value.length, start, elem.value.length);
        elem.setSelectionRange(start, end);
    } catch { }
}

/**
 * Replaces the current selection of the text input control with the specified
 * text, and places a simple text cursor behind the new text.
 *
 * @param elem
 *  A text input element (an `<input>` or `<textarea>` element).
 *
 * @param [text=""]
 *  The text used to replace the selected text in the text input control. If
 *  omitted, the selected text will be deleted.
 *
 * @param [pos]
 *  The new cursor position to be set (relative in the inserted text). If
 *  negative, the text cursor will be placed from the end of the text. If
 *  omitted, the cursor will be set to the end of the inserted text.
 */
export function replaceInputSelection(elem: HTMLTextElement, text?: string, pos?: number): void {

    // replace the selected text
    const { start, end } = getInputSelection(elem);
    elem.value = str.replaceSubStr(elem.value, start, end, text || "");

    // set cursor as selection behind inserted text
    let newPos = start + (text?.length ?? 0);
    if (is.number(pos)) {
        if (pos < 0) { newPos += pos; } else { newPos = start + pos; }
    }
    setInputSelection(elem, newPos);
}
