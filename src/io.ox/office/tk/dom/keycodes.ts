/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * A string enumeration with the valid names of all key codes.
 */
// do not include digit keys as numbers (as done by `keyof` operator) but as strings to prevent typechecker confusion
export type KeyCodeName = KeysOf<typeof KeyCode> | "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9";

/**
 * Type alias for a native DOM keyboard event, or a generic jQuery event type.
 */
export type AnyKeyEvent = KeyboardEvent | JEvent;

/**
 * State flags of the modifier keys, as used for example in DOM events.
 */
export interface ModifierKeys {
    shiftKey: boolean;
    altKey: boolean;
    ctrlKey: boolean;
    metaKey: boolean;
}

/**
 * The expected settings of the modifier keys to match a key event against.
 */
export interface MatchModifierOptions {

    /**
     * Specifies how to test the property `shiftKey` of the key event. If set
     * to `true`, the property must be set in the event. If set to `false`, the
     * property must not be set in the event. If set to `null`, the current
     * state of the property will be ignored. Default value is `false`.
     */
    shift?: boolean | null;

    /**
     * Specifies how to test the property `altKey` of the key event. If set to
     * `true`, the property must be set in the event. If set to `false`, the
     * property must not be set in the event. If set to `null`, the current
     * state of the property will be ignored. Default value is `false`.
     */
    alt?: boolean | null;

    /**
     * Specifies how to test the property `ctrlKey` of the key event. If set to
     * `true`, the property must be set in the event. If set to `false`, the
     * property must not be set in the event. If set to `null`, the current
     * state of the property will be ignored. Default value is `false`.
     */
    ctrl?: boolean | null;

    /**
     * Specifies how to test the property `metaKey` of the key event. If set to
     * `true`, the property must be set in the event. If set to `false`, the
     * property must not be set in the event. If set to `null`, the current
     * state of the property will be ignored. Default value is `false`.
     */
    meta?: boolean | null;

    /**
     * Convenience option. If set to `true`, the key event matches, if either
     * the property `altKey` or the property `metaKey` is set (the options
     * `alt` and `meta` will be ignored). Default value is `false`.
     */
    altOrMeta?: boolean;

    /**
     * Convenience option. If set to `true`, the key event matches, if either
     * the property `ctrlKey` or the property `metaKey` is set (the options
     * `ctrl` and `meta` will be ignored). Default value is `false`.
     */
    ctrlOrMeta?: boolean;
}

// constants ==================================================================

/**
 * A map of key codes used in 'keydown' and 'keyup' browser events.
 */
export const KeyCode = {

    BACKSPACE:      8,
    TAB:            9,
    ENTER:          13,
    SHIFT:          16,
    CONTROL:        17,
    ALT:            18,
    BREAK:          19,
    CAPS_LOCK:      20,
    ESCAPE:         27,
    SPACE:          32,
    PAGE_UP:        33,
    PAGE_DOWN:      34,
    END:            35,
    HOME:           36,
    LEFT_ARROW:     37,
    UP_ARROW:       38,
    RIGHT_ARROW:    39,
    DOWN_ARROW:     40,
    PRINT:          44,
    INSERT:         45,
    DELETE:         46,

    0:              48,
    1:              49,
    2:              50,
    3:              51,
    4:              52,
    5:              53,
    6:              54,
    7:              55,
    8:              56,
    9:              57,

    MOZ_SEMICOLON:  59,     // Semicolon in Firefox (otherwise: 186 SEMICOLON)
    MOZ_OPEN_ANGLE: 60,     // Open angle in Firefox, German keyboard (otherwise: 226 OPEN_ANGLE)
    MOZ_EQUAL_SIGN: 61,     // Equal sign in Firefox (otherwise: 187 EQUAL_SIGN)

    A:              65,
    B:              66,
    C:              67,
    D:              68,
    E:              69,
    F:              70,
    G:              71,
    H:              72,
    I:              73,
    J:              74,
    K:              75,
    L:              76,
    M:              77,
    N:              78,
    O:              79,
    P:              80,
    Q:              81,
    R:              82,
    S:              83,
    T:              84,
    U:              85,
    V:              86,
    W:              87,
    X:              88,
    Y:              89,
    Z:              90,

    LEFT_WINDOWS:   91,     // Windows only
    LEFT_COMMAND:   91,     // Mac only (but Firefox: 224 MOZ_COMMAND)
    RIGHT_WINDOWS:  92,
    SELECT:         93,     // Windows only
    RIGHT_COMMAND:  93,     // Mac only (but Firefox: 224 MOZ_COMMAND)

    NUM_0:          96,     // attention: numpad keys totally broken in Opera
    NUM_1:          97,
    NUM_2:          98,
    NUM_3:          99,
    NUM_4:          100,
    NUM_5:          101,
    NUM_6:          102,
    NUM_7:          103,
    NUM_8:          104,
    NUM_9:          105,

    NUM_MULTIPLY:   106,
    NUM_PLUS:       107,
    NUM_MINUS:      109,
    NUM_POINT:      110,
    NUM_DIVIDE:     111,

    F1:             112,
    F2:             113,
    F3:             114,
    F4:             115,
    F5:             116,
    F6:             117,
    F7:             118,
    F8:             119,
    F9:             120,
    F10:            121,
    F11:            122,
    F12:            123,

    NUM_LOCK:       144,
    SCROLL_LOCK:    145,

    MOZ_HASH:       163,    // Hash sign in Firefox, German keyboard (otherwise: 191 SLASH)
    MOZ_PLUS:       171,    // Plus sign in Firefox, German keyboard (otherwise: 187 EQUAL_SIGN)
    MOZ_DASH:       173,    // Dash sign in Firefox (otherwise: 189 DASH)

    SEMICOLON:      186,    // (but Firefox: 59 MOZ_SEMICOLON)
    EQUAL_SIGN:     187,    // (but Firefox: 61 MOZ_EQUAL_SIGN)
    COMMA:          188,
    DASH:           189,    // (but Firefox: 173 MOZ_DASH)
    /* enable when needed
    PERIOD:         190,
    SLASH:          191,
    GRAVE:          192,
    OPEN_BRACKET:   219,
    BACKSLASH:      220,
    CLOSE_BRACKET:  221,
    APOSTROPH:      222,
    */

    MOZ_COMMAND:    224,    // Mac/Firefox only (otherwise: 91 LEFT_COMMAND, 93 RIGHT_COMMAND)

    /* enable when needed
    OPEN_ANGLE:     226,    // Open angle, German keyboard (but Firefox: 60 MOZ_OPEN_ANGLE)
    */

    IME_INPUT:      229     // indicates an IME input session
};

/**
 * A map of keys that only trigger 'keyup' and 'keydown', but no 'keypress' events.
 */
export const KeyUpDownOnly = {

    BACKSPACE:      1,
    TAB:            1,
    SHIFT:          1,
    CONTROL:        1,
    ALT:            1,
    // BREAK:          1,
    CAPS_LOCK:      1,
    ESCAPE:         1,
    PAGE_UP:        1,
    PAGE_DOWN:      1,
    END:            1,
    HOME:           1,
    LEFT_ARROW:     1,
    UP_ARROW:       1,
    RIGHT_ARROW:    1,
    DOWN_ARROW:     1,
    PRINT:          1, // only keyup
    INSERT:         1,
    DELETE:         1,

    // LEFT_WINDOWS:   9,     // Windows only
    // LEFT_COMMAND:   9,     // Mac only (but Firefox: 224 MOZ_COMMAND)
    // RIGHT_WINDOWS:  9,
    // SELECT:         9,     // Windows only
    // RIGHT_COMMAND:  9,     // Mac only (but Firefox: 224 MOZ_COMMAND)

    NUM_0:          1,     // only, if NUM_LOCK not set
    NUM_1:          1,
    NUM_2:          1,
    NUM_3:          1,
    NUM_4:          1,
    NUM_5:          1,
    NUM_6:          1,
    NUM_7:          1,
    NUM_8:          1,
    NUM_9:          1,

    F1:             1,
    F2:             1,
    F3:             1,
    F4:             1,
    F5:             1,
    F6:             1,
    F7:             1,
    F8:             1,
    F9:             1,
    F10:            1,
    F11:            1,
    F12:            1
};

// private functions ==========================================================

function isMatching(currentState: boolean, expectedState?: boolean | null): boolean {
    return (expectedState === null) || (currentState === (expectedState === true));
}

// public functions ===========================================================

/**
 * Returns whether the key code in the passed keyboard event object matches the
 * specified key code, regardless of any modifier keys.
 *
 * @param event
 *  The keyboard event to be checked.
 *
 * @param keyCodes
 *  All key codes to be checked, as numeric key code, or the upper-case name of
 *  a key code, as defined in the `KeyCode` map. Be careful with digit keys,
 *  for example, the number `9` matches the TAB key (KeyCode.TAB), but the
 *  string `"9"` matches the digit 9 key (`KeyCode["9"]` with the key code 57).
 *
 * @returns
 *  Whether the passed event object matches the key code.
 */
export function hasKeyCode(event: AnyKeyEvent, ...keyCodes: Array<number | KeyCodeName>): boolean {
    return keyCodes.some(keyCode => event.keyCode === (is.string(keyCode) ? KeyCode[keyCode] : keyCode));
}

/**
 * Returns whether the passed key event contains the key code of the ESCAPE
 * key, regardless of any modifier keys.
 *
 * @param event
 *  The keyboard event to be checked.
 *
 * @returns
 *  Whether the passed event contains the ESCAPE key code.
 */
export function isEscapeKey(event: AnyKeyEvent): boolean {
    return hasKeyCode(event, KeyCode.ESCAPE);
}

/**
 * Returns whether the passed key event contains the key code of the SHIFT
 * key, regardless of any modifier keys.
 *
 * @param event
 *  The keyboard event to be checked.
 *
 * @returns
 *  Whether the passed event contains the SHIFT key code.
 */
export function isShiftKey(event: AnyKeyEvent): boolean {
    return hasKeyCode(event, KeyCode.SHIFT);
}

/**
 * Returns whether the passed key event contains the key code of a function key
 * (F1 to F12), regardless of any modifier keys.
 *
 * @param event
 *  The keyboard event to be checked.
 *
 * @returns
 *  Whether the passed event contains the key code of a function key.
 */
export function isFunctionKey(event: AnyKeyEvent): boolean {
    return is.number(event.keyCode) && (event.keyCode >= KeyCode.F1) && (event.keyCode <= KeyCode.F12);
}

/**
 * Returns whether the passed key event contains the key code for IME input.
 *
 * @param event
 *  The keyboard event to be checked.
 *
 * @returns
 *  Whether the passed event contains the IME input key code.
 */
export function isIMEInputKey(event: AnyKeyEvent): boolean {
    return hasKeyCode(event, KeyCode.IME_INPUT);
}

/**
 * Returns whether any modifier key flag is set in the passed object.
 *
 * @param modifiers
 *  The object to be checked. Can be any DOM event with modifier flags (classes
 *  `KeyboardEvent`, `MouseEvent`, or `TouchEvent`), or a `JQuery.Event`.
 *
 * @returns
 *  Whether any modifier key is pressed in the passed event object.
 */
export function hasModifierKeys(modifiers: Partial<ModifierKeys>): boolean {
    return (modifiers.shiftKey || modifiers.ctrlKey || modifiers.altKey || modifiers.metaKey) === true;
}

/**
 * Returns whether the states of the modifier keys matches the specified
 * definition.
 *
 * @param modifiers
 *  The object to be checked. Can be any DOM event with modifier flags (classes
 *  `KeyboardEvent`, `MouseEvent`, or `TouchEvent`), or a `JQuery.Event`.
 *
 * @param [options]
 *  The expected settings of the modifier keys to match the event against.
 *
 * @returns
 *  Whether the passed event object matches the modifier key settings specified
 *  in the passed options.
 */
export function matchModifierKeys(modifiers: Partial<ModifierKeys>, options?: MatchModifierOptions): boolean {

    // in case any browser mixes false and undefined
    const shiftKey = !!modifiers.shiftKey;
    const ctrlKey = !!modifiers.ctrlKey;
    const altKey = !!modifiers.altKey;
    const metaKey = !!modifiers.metaKey;

    // SHIFT key must always match
    if (!isMatching(shiftKey, options?.shift)) {
        return false;
    }

    // when "altOrMeta" is set, ignore "alt" and "meta" options
    if (options?.altOrMeta) {
        return (altKey !== metaKey) && isMatching(ctrlKey, options.ctrl);
    }

    // when "ctrlOrMeta" is set, ignore "ctrl" and "meta" options
    if (options?.ctrlOrMeta) {
        return (ctrlKey !== metaKey) && isMatching(altKey, options.alt);
    }

    // otherwise, ALT, CTRL, and META keys must match
    return isMatching(altKey, options?.alt) && isMatching(ctrlKey, options?.ctrl) && isMatching(metaKey, options?.meta);
}

/**
 * Returns whether the key code and the states of the modifier keys in the
 * passed keyboard event object matches the specified definition.
 *
 * @param event
 *  The keyboard event to be checked.
 *
 * @param keyCode
 *  The numeric key code, or the upper-case name of a key code, as defined in
 *  the `KeyCode` map. Be careful with digit keys, for example, the number `9`
 *  matches the TAB key (KeyCode.TAB), but the string `"9"` matches the digit 9
 *  key (`KeyCode["9"]` with the key code 57).
 *
 * @param [options]
 *  The expected settings of the modifier keys to match the event against.
 *
 * @returns
 *  Whether the passed event object matches the key code and the modifier key
 *  settings specified in the options.
 */
export function matchKeyCode(event: AnyKeyEvent, keyCode: number | KeyCodeName, options?: MatchModifierOptions): boolean {
    return hasKeyCode(event, keyCode) && matchModifierKeys(event, options);
}

/**
 * Returns whether the passed keyboard event represents a "Copy to clipboard"
 * action.
 *
 * @param event
 *  The keyboard event to be checked. MUST be a "keydown" or "keyup" event.
 *
 * @returns
 *  Whether the passed keyboard event represents a "Copy to clipboard" action.
 */
export function isCopyKeyEvent(event: AnyKeyEvent): boolean {
    return matchKeyCode(event, "C", { ctrlOrMeta: true }) || matchKeyCode(event, "INSERT", { ctrlOrMeta: true });
}

/**
 * Returns whether the passed keyboard event represents a "Cut to clipboard"
 * action.
 *
 * @param event
 *  The keyboard event to be checked. MUST be a "keydown" or "keyup" event.
 *
 * @returns
 *  Whether the passed keyboard event represents a "Cut to clipboard" action.
 */
export function isCutKeyEvent(event: AnyKeyEvent): boolean {
    return matchKeyCode(event, "X", { ctrlOrMeta: true }) || matchKeyCode(event, "DELETE", { shift: true });
}

/**
 * Returns whether the passed keyboard event represents a "Paste from
 * clipboard" action.
 *
 * @param event
 *  The keyboard event to be checked. MUST be a "keydown" or "keyup" event.
 *
 * @returns
 *  Whether the passed keyboard event represents a "Paste from clipboard"
 *  action.
 */
export function isPasteKeyEvent(event: AnyKeyEvent): boolean {
    return matchKeyCode(event, "V", { ctrlOrMeta: true }) || matchKeyCode(event, "INSERT", { shift: true });
}

/**
 * Returns whether the passed keyboard event represents a clipboard action
 * (copy, cut, or paste). This function is aconvenience shortcut that calls
 * `isCopyKeyEvent()`, `isCutKeyEvent()`, and `isPasteKeyEvent()`.
 *
 * @param event
 *  The keyboard event to be checked. MUST be a "keydown" or "keyup" event.
 *
 * @returns
 *  Whether the passed keyboard event represents a clipboard action.
 */
export function isClipboardKeyEvent(event: AnyKeyEvent): boolean {
    return isCopyKeyEvent(event) || isCutKeyEvent(event) || isPasteKeyEvent(event);
}

/**
 * Returns whether the passed keyboard event represents the "Select all"
 * action.
 *
 * @param event
 *  The keyboard event to be checked. MUST be a "keydown" or "keyup" event.
 *
 * @returns
 *  Whether the passed keyboard event represents the "Select all" action.
 */
export function isSelectAllKeyEvent(event: AnyKeyEvent): boolean {
    return matchKeyCode(event, "A", { ctrlOrMeta: true });
}
