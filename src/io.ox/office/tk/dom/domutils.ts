/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import type { Size, TokenList } from "@/io.ox/office/tk/algorithms";
import { is, math, str, fmt, fun, dict } from "@/io.ox/office/tk/algorithms";
import type { RectangleLike } from "@/io.ox/office/tk/dom/rectangle";

import "@/io.ox/office/tk/dom/style.less";

const { floor, ceil, round } = Math;

// types ======================================================================

/**
 * A type specifier for a source specifier for DOM nodes. Can be any of:
 * - A single DOM node of a specific type.
 * - An array of DOM nodes of a specific type (may be empty).
 * - A DOM `NodeList` with nodes of a specific type (may be empty).
 * - A `JQuery` collection containing nodes of a specific type.
 * - The values `undefined` or `null` for an empty node source.
 */
export type NodeSource<NT extends Node = HTMLElement> = Nullable<NT> | NT[] | NodeListOf<NT> | JQuery<NT>;

/**
 * A type specifier for a mandatory DOM node. Can be any of:
 * - A single DOM node of a specific type.
 * - A non-empty `JQuery` collection containing nodes of a specific type (the
 *   non-emptiness cannot be enforced on type-level however).
 */
export type NodeOrJQuery<NT extends Node = HTMLElement> = NT | JQuery<NT>;

/**
 * The location of an element inside a specific boundary, with distances to all
 * four borders.
 */
export interface ElementPosition extends RectangleLike {

    /**
     * Distance of right border of the node to the right boundary border.
     */
    right: number;

    /**
     * Distance of bottom border of the node to the bottom boundary border.
     */
    bottom: number;
}

/**
 * Options for resolving the position of an element in the document.
 */
export interface ElementPositionOptions {

    /**
     * If set to `true`, the position of the inner client rectangle will be
     * returned (the visible area inside the borders and scrollbars). By
     * default, the offset rectangle will be returned (including borders, but
     * without outer margins).
     */
    inner?: boolean;
}

/**
 * Supported CSS length units in conversion functions.
 */
export type CSSLengthUnit = "px" | "pc" | "pt" | "in" | "cm" | "mm" | "em";

/**
 * Names of border position attributes used in CSS.
 */
export type CSSBorderPos = "top" | "bottom" | "left" | "right";

/**
 * Optional parameters for the function `containsNode()`.
 */
export interface ContainsNodeOptions {

    /**
     * If set to `true`, an inner node that is the same as the outer node will
     * be considered to be contained by the outer node. Default value is
     * `false`.
     */
    allowSelf?: boolean;
}

/**
 * Options for settings the CSS classes and identifier of a DOM element.
 */
export interface ElementBaseOptions {

    /**
     * The names of all CSS classes to be added to the DOM element, as one of:
     * - space-separated token list,
     * - array of tokens with optional nullish values that will be ignored,
     * - dictionary mapping class names to toggle flags (properties with value
     *   `true` will be added, properties with value `false` will be removed).
     */
    classes?: string | TokenList | Dict<Nullable<boolean>>;

    /**
     * The identifier to be set to the DOM element.
     */
    id?: string;
}

/**
 * Value type of element attribute. Numeric and boolean properties will be
 * converted to strings automatically. The values `undefined`, `null`, and the
 * empty string will remove the attribute.
 */
export type AttrValueType = string | number | boolean | nullish;

/**
 * A dictionary with multiple attributes to be added to the DOM element.
 * Numeric and boolean properties will be converted to strings automatically.
 * Properties set to the values `undefined`, `null`, and the empty string will
 * remove the respective attribute.
 */
export type AttrValueDict = Dict<AttrValueType>;

/**
 * Options for setting attributes of a DOM element.
 */
export interface ElementAttributeOptions {

    /**
     * Attributes to be added to the DOM element.
     */
    attrs?: Nullable<AttrValueDict>;

    /**
     * Custom data attributes to be added to the DOM element.
     */
    dataset?: Nullable<AttrValueDict>;
}

/**
 * Valid keys for pure CSS value attributes in a `CSSStyleDeclaration` (without
 * the readonly properties "length" and "parentRule", and without the function
 * properties).
 */
export type CSSStyleKey = Exclude<KeysOf<CSSStyleDeclaration>, "length" | "parentRule" | "getPropertyPriority" | "getPropertyValue" | "item" | "removeProperty" | "setProperty">;

/**
 * The value of a style attribute to be applied at a DOM element. Numeric
 * values will be converted to CSS length with pixel unit automatically. An
 * empty string or the value `null` will remove a style attribute.
 */
export type CSSStyleValue = string | number | null;

/**
 * A partial record of CSS styles to be applied at a DOM element.
 */
export type CSSStyleSpec = PtRecord<CSSStyleKey, CSSStyleValue>;

/**
 * Options for styling a DOM element.
 */
export interface ElementStyleOptions {

    /**
     * The CSS styles to be applied at the DOM element. Numeric properties
     * will be converted to CSS length with pixel unit automatically.
     * Properties set to the value `null` will be converted to an empty string
     * (used to remove a CSS attribute).
     */
    style?: Nullable<CSSStyleSpec>;
}

/**
 * Options for adding text and tooltips to a DOM element.
 */
export interface ElementLabelOptions {

    /**
     * The text contents to be inserted into the DOM element. If omitted or set
     * to `null`, no text node will be inserted (an existing text node will be
     * retained). If set to an empty string, an explicit empty text node will
     * be inserted.
     */
    label?: string | null;

    /**
     * The tooltip to be shown when hovering the DOM element.
     */
    tooltip?: string | null;
}

/**
 * Options for updating properties of a DOM element.
 */
export interface UpdateElementOptions {

    /**
     * If set to `true`, all properties of the element about to be updated will
     * be cleared first, regardless if they are mentioned in the options bag.
     * By default, the element will be updated incrementally only with the
     * options that have been specified explicitly.
     */
    clear?: boolean;
}

/**
 * Generic options for creating a new HTML element.
 */
export interface CreateElementOptions extends ElementBaseOptions, ElementAttributeOptions, ElementStyleOptions { }

/**
 * Generic options for creating a new HTML element with label and tooltip.
 */
export interface CreateElementLabelOptions extends CreateElementOptions, ElementLabelOptions { }

/**
 * Options for creating a new SVG element.
 */
export interface CreateSVGElementOptions extends CreateElementOptions { }

/**
 * Options for creating a new `<canvas>` element.
 */
export interface CreateCanvasElementOptions extends CreateElementOptions { }

/**
 * Options for creating a new `<style>` element.
 */
export interface CreateStyleElementOptions extends CreateElementOptions { }

/**
 * Options for creating a new `<img>` element.
 */
export interface CreateImageElementOptions extends CreateElementOptions { }

/**
 * A descriptive interface for objects wrapping a DOM element, and exposing
 * that element via public properties.
 */
export interface ElementWrapper<ET extends Element = HTMLElement> {

    /**
     * The wrapped DOM element.
     */
    readonly el: ET;

    /**
     * The wrapped DOM element, as JQuery collection.
     */
    readonly $el: JQuery<ET>;
}

// constants ==================================================================

/**
 * Maximum duration between two click/tap events to be recognized as a double
 * click event.
 */
export const MAX_DBLCLICK_DURATION = 600;

/**
 * The maximum explicit size of single DOM nodes, in pixels. The size is
 * limited by browsers; by using larger sizes the elements may collapse to zero
 * size, or may lead to rendering errors (wrong positions, blurring).
 */
export const MAX_NODE_SIZE = 8388000;

// private constants ----------------------------------------------------------

// regex pattern to check for invalid XML characters
const INVALID_XML_CHARS_PATTERN =
    // reserved/invalid characters in the BMP
    "[\0-\\x08\\x0B\\x0C\\x0E-\\x1F\\x7F-\\x84\\x86-\\x9F\\uFDD0-\\uFDEF\\uFFFE\\uFFFF]|" +
    // code points ending in `FFFE` and `FFFF` are reserved in *all* planes
    "[\\uD83F\\uD87F\\uD8BF\\uD8FF\\uD93F\\uD97F\\uD9BF\\uD9FF\\uDA3F\\uDA7F\\uDABF\\uDAFF\\uDB3F\\uDB7F\\uDBBF\\uDBFF][\\uDFFE\\uDFFF]";

// regular expression to check for invalid XML characters
const INVALID_XML_CHARS_RE = new RegExp(INVALID_XML_CHARS_PATTERN, "g");

// globals ====================================================================

// the global hidden storage element used to store DOM elements without showing them
const $hiddenStorageNode = $('<div id="io-ox-office-temp" class="io-ox-office-main noI18n" aria-hidden="true">').prependTo(document.body);

// private functions ==========================================================

function styleToString(value: CSSStyleValue): string {
    return is.number(value) ? (value ? `${value}px` : "0") : (value || "");
}

/**
 * Returns the conversion factors between pixels and other CSS units.
 */
const getUnitFactors = fun.once((): Record<CSSLengthUnit, number> => ({
    px: 1,
    pc: 1 / 9,
    pt: 4 / 3,
    in: 96,
    cm: 96 / 2.54,
    mm: 96 / 25.4,
    em: parseFloat(resolveCssVar("--std-font-size")) || 14
}));

// public functions ===========================================================

/**
 * Replaces HTML mark-up characters (angle brackets, ampersand, double quotes,
 * and apostrophes) in the passed text with the respective HTML entities.
 *
 * @param text
 *  The text containing special HTML mark-up characters.
 *
 * @returns
 *  The passed text, with all special HTML mark-up characters replaced with the
 *  respective HTML entities.
 */
export function escapeHTML(text: string): string {
    return text
        .replace(/&/g, "&amp;")      // replace the literal ampersand (must be done first!)
        .replace(/</g, "&lt;")       // replace the left angle bracket (start of element tag)
        .replace(/>/g, "&gt;")       // replace the right angle bracket (end of element tag)
        .replace(/\//g, "&#47;")     // replace the slash character (end element marker)
        .replace(/"/g, "&quot;")     // replace the double quote character
        .replace(/'/g, "&#39;")      // replace the apostrophe (&apos; is not an HTML entity!)
        .replace(/\xa0/g, "&nbsp;"); // replace the non-breakable space
}

/**
 * Removes all characters from the passed text that are invalid in XML.
 *
 * @param text
 *  The text to be cleaned.
 *
 * @returns
 *  The cleaned text containing valid XML characters only.
 */
export function cleanXML(text: string): string {
    return text.replace(INVALID_XML_CHARS_RE, "");
}

/**
 * Wraps the passed DOM event handler function, and optionally causes to
 * prevent further propagation, and to suppress the event's default action.
 *
 * @param fn
 *  The DOM event handler to be wrapped. If `true` will be returned, further
 *  event propagation will be prevented, and the event's default action will be
 *  suppressed.
 *
 * @returns
 *  The wrapper function for the event handler, to be attached to a DOM event.
 */
export function handleDOMEvent<T extends object, E extends Event>(fn: (this: T, event: E) => boolean): (event: E) => void {
    return function (this: T, event: E) {
        if (fn.call(this, event)) {
            event.preventDefault();
            event.stopPropagation();
        }
    };
}

// DOM node source ------------------------------------------------------------

/**
 * Returns whether the passed value is a DOM node list.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a DOM node list.
 */
export function isNodeList<NT extends Node>(data: unknown): data is NodeListOf<NT> {
    return data instanceof NodeList;
}

/**
 * Returns whether the passed value is a JQuery collection.
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a JQuery collection.
 */
export function isJQuery<NT>(data: unknown): data is JQuery<NT> {
    return data instanceof $;
}

/**
 * Returns whether the passed value is a DOM node source value (instance of
 * `Node` including `document` and text nodes, a `NodeList`, a `JQuery`
 * collection, or a nullish value).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a DOM node source value.
 */
export function isNodeSource<NT extends Node>(data: unknown): data is NodeSource<NT> {
    return (data instanceof Node) || is.array(data) || (data instanceof NodeList) || (data instanceof $) || is.nullish(data);
}

/**
 * Converts the passed value to a single DOM node instance if possible.
 *
 * @param source
 *  The DOM node source. May be a single DOM node, a `NodeList`, a `JQuery`
 *  collection of nodes, or nullish for an empty node source. If the source
 *  object contains multiple DOM elements, the first member will be returned.
 *  If the source is empty (nullish or an empty collection), this function
 *  returns `null`.
 *
 * @returns
 *  The DOM node as passed, or extracted from a node list or JQuery collection;
 *  or `null`, if the passed source object was empty.
 */
export function toNode(source: nullish): null;
export function toNode<NT extends Node>(source: NT): NT;
export function toNode<NT extends Node>(source: NodeSource<NT>): NT | null;
// implementation
export function toNode<NT extends Node>(source: NodeSource<NT>): NT | null {
    return (source instanceof Node) ? source : ((source ? source[0] : source) ?? null);
}

/**
 * Converts the passed DOM node source to a JQuery collection.
 *
 * @param source
 *  The DOM node source. May be a single DOM node, a `NodeList`, a `JQuery`
 *  collection of nodes, or nullish for an empty node source. If the source is
 *  empty (nullish or an empty collection), this function returns an empty
 *  JQuery collection.
 *
 * @param [single=false]
 *  If set to `true`, a `NodeList`, or a `JQuery` collection will be reduced to
 *  its first element. By default, all nodes in the collection will be included
 *  in the result.
 *
 * @returns
 *  The JQuery collection created from the passed value.
 */
export function toJQuery<NT extends Element>(source: NodeSource<NT>, single?: boolean): JQuery<NT> {
    if (source instanceof Node) { return $(source); }
    if (is.array(source) || (source instanceof NodeList)) { return single ? $(source[0]) : ($(source as any) as JQuery<NT>); }
    if (isJQuery(source)) { return single ? source.eq(0) : source; }
    return $();
}

// DOM manipulation -----------------------------------------------------------

/**
 * Removes all child nodes from the passed DOM node.
 *
 * @param node
 *  The node to be emptied.
 *
 * @returns
 *  The passed DOM node.
 */
export function detachChildren<NT extends Node>(node: NT): NT {
    node.textContent = "";
    return node;
}

/**
 * Inserts all passed nodes into a container element at a specific position.
 *
 * @param parent
 *  The DOM container element that becomes the parent of the inserted nodes.
 *
 * @param index
 *  The insertion index for the nodes. Negative values count from the last
 *  existing child node (-1 appends to the end, -2 inserts between next-to-last
 *  and last, etc.).
 *
 * @param nodes
 *  The DOM nodes to be inserted into the container element.
 */
export function insertChildren(parent: ParentNode, index: number, ...nodes: Node[]): void {
    const { childNodes } = parent;
    index = (index < 0) ? (childNodes.length + index + 1) : index;
    if ((index < 0) || (index > childNodes.length)) {
        throw new DOMException("invalid child index");
    }
    const nextNode = childNodes.item(index);
    if (nextNode) {
        nextNode.before(...nodes);
    } else {
        parent.append(...nodes);
    }
}

/**
 * Inserts all passed nodes into the parent element, after detaching all its
 * current child nodes.
 *
 * @param parent
 *  The DOM container element that becomes the parent of the inserted nodes.
 *
 * @param nodes
 *  The DOM nodes to be inserted into the container element.
 */
export function replaceChildren(parent: ParentNode, ...nodes: Node[]): void {
    detachChildren(parent);
    parent.append(...nodes);
}

/**
 * Updates CSS classes, and the identifier of the passed DOM element.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing the CSS classes as
 *  convenience shortcut for the option `classes`.
 */
export function setElementClasses(elem: HTMLElement | SVGElement, options?: (ElementBaseOptions & UpdateElementOptions) | string): void {

    // shortcut for CSS classes only (incl. empty string)
    const short = is.string(options);
    const classes = short ? options : options?.classes;
    const id = short ? undefined : options?.id;
    const clear = !short && !!options?.clear;

    // update the element identifier
    if (clear || is.string(id)) { elem.id = id || ""; }

    // clear all existing CSS classes
    if (clear) { elem.setAttribute("class", ""); }

    // add specified CSS classes to existing classes
    if (classes) {
        if (is.string(classes)) {
            elem.classList.add(...str.splitTokens(classes));
        } else if (is.array(classes)) {
            elem.classList.add(...classes.filter(Boolean));
        } else if (is.dict(classes)) {
            dict.forEach(classes, (flag, key) => elem.classList.toggle(key, !!flag));
        }
    }
}

/**
 * Adds, changes, or removes the specified attribute of a DOM element using the
 * methods `Element::setAttribute` and `Element::removeAttribute`.
 *
 * Note that the similar method `Element::toggleAttribute` does not support
 * setting a custom value as this function offers.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param attr
 *  The name of the attribute to be updated (as used in `Element::setAttribute`
 *  and `Element::removeAttribute`).
 *
 * @param value
 *  The value of the attribute. If set to `undefined`, `null`, or the empty
 *  string, the attribute will be removed. Otherwise, the attribute will be set
 *  to the specified value.
 */
export function setElementAttribute(elem: Element, attr: string, value: AttrValueType): void {
    if (is.nullish(value) || (value === "")) {
        elem.removeAttribute(attr);
    } else {
        elem.setAttribute(attr, String(value));
    }
}

/**
 * Updates the attributes of the passed DOM element.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param attrs
 *  The attributes to be added to the element.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setElementAttributes(elem: HTMLElement | SVGElement, attrs: Nullable<AttrValueDict>): void {
    if (attrs) {
        for (const [key, value] of dict.entries(attrs)) {
            setElementAttribute(elem, key, value);
        }
    }
}

/**
 * Adds, changes, or removes a data attribute of the passed DOM element.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param key
 *  The key of the data attribute (camel-case as used in the `dataset` property
 *  of the element).
 *
 * @param value
 *  The new value for the data attribute. If set to `undefined`, `null`, or the
 *  empty string, the attribute will be removed. Otherwise, the data attribute
 *  will be set to the specified value.
 */
export function setElementDataAttr(elem: HTMLElement | SVGElement, key: string, value: AttrValueType): void {
    if (is.nullish(value) || (value === "")) {
        delete elem.dataset[key];
    } else {
        elem.dataset[key] = String(value);
    }
}

/**
 * Updates the data attributes of the passed DOM element.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param dataset
 *  The data attributes to be added to the element.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setElementDataSet(elem: HTMLElement | SVGElement, dataset: Nullable<AttrValueDict>, options?: UpdateElementOptions): void {

    // clear all data attributes if specified
    if (options?.clear) {
        dict.forEachKey(elem.dataset, key => delete elem.dataset[key]);
    }

    // add data attributes
    if (dataset) {
        for (const [key, value] of dict.entries(dataset)) {
            setElementDataAttr(elem, key, value);
        }
    }
}

/**
 * Updates a single inline style attribute of the passed DOM element.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param key
 *  The key of the style attribute to be updated.
 *
 * @param value
 *  The new value for the style attribute. The value `null` will remove the
 *  style attribute from the element.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setElementStyle(elem: HTMLElement | SVGElement, key: CSSStyleKey, value: CSSStyleValue, options?: UpdateElementOptions): void {

    // clear all styles if specified
    if (options?.clear) { elem.removeAttribute("style"); }

    // set/update inline styles
    elem.style[key] = styleToString(value);
}

/**
 * Updates the inline styles of the passed DOM element.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param style
 *  The CSS styles to be added to the element.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setElementStyles(elem: HTMLElement | SVGElement, style: Nullable<CSSStyleSpec>, options?: UpdateElementOptions): void {

    // clear all styles if specified
    if (options?.clear) { elem.removeAttribute("style"); }

    // set/update inline styles
    if (style) {
        dict.forEach(style, (value, key) => { elem.style[key] = styleToString(value!); });
    }
}

/**
 * Detaches all child elements of the passed node, and inserts a single text
 * child node.
 *
 * @param elem
 *  The DOM element to be updated.
 *
 * @param [label]
 *  The text to be inserted.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setElementLabel(elem: HTMLElement, label: string, options?: UpdateElementOptions): void;
export function setElementLabel(elem: HTMLElement, options?: ElementLabelOptions & UpdateElementOptions): void;
// implementation
export function setElementLabel(elem: HTMLElement, arg1?: string | (ElementLabelOptions & UpdateElementOptions), arg2?: UpdateElementOptions): void {

    // shortcut for label only (incl. empty string)
    const label = is.string(arg1) ? arg1 : arg1?.label;
    const tooltip = is.string(arg1) ? undefined : arg1?.tooltip;
    const clear = !is.string(arg1) && arg2?.clear;

    // DOCS-3026, DOCS-3028: do not use `elem.textContent` property which would not
    // create a text node for an empty string (this is required in text framework)
    if (clear || is.string(label)) {
        detachChildren(elem).appendChild(createText(label));
    }

    // add or remove tooltip
    if (clear || is.string(tooltip)) {
        setElementAttribute(elem, "title", tooltip);
    }
}

/**
 * Tries to parse an element attribute value as integer.
 *
 * @param elem
 *  The DOM element whose attribute will be parsed.
 *
 * @param attr
 *  The name of the element attribute.
 *
 * @param [def]
 *  A default value in case the element does not contain the attribute, or its
 *  value cannot be parsed as an integer.
 *
 * @returns
 *  The attribute value parsed as integer, or the default value.
 */
export function parseIntAttribute(elem: Element, attr: string, def: number): number;
export function parseIntAttribute(elem: Element, attr: string, def?: number): Opt<number>;
// implementation
export function parseIntAttribute(elem: Element, attr: string, def?: number): Opt<number> {
    const text = elem.getAttribute(attr);
    const num = text ? fmt.parseInt(text, 10) : Number.NaN;
    return Number.isFinite(num) ? num : def;
}

/**
 * Returns whether the passed outer node contains the passed inner node.
 *
 * @param outer
 *  The outer DOM node. If this object is a JQuery collection, uses the first
 *  node it contains.
 *
 * @param inner
 *  The inner DOM node. If this object is a JQuery collection, uses the first
 *  node it contains.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether the inner node is a descendant of the outer node.
 */
export function containsNode(outer: NodeSource<Node>, inner: NodeSource<Node>, options?: ContainsNodeOptions): boolean {

    // convert nodes to DOM node object
    let outerNode = toNode(outer);
    let innerNode = toNode(inner);
    if (!outerNode || !innerNode) { return false; }

    // check equality according to passed option
    if (outerNode === innerNode) {
        return !!options?.allowSelf;
    }

    // IE does not support contains() method at document
    if (outerNode === document) {
        outerNode = document.body;
        if (innerNode === outerNode) { return true; }
    }

    // outer node must be an element; be sure that a node does not contain itself
    if ((outerNode.nodeType !== Node.ELEMENT_NODE) || (outerNode === innerNode)) {
        return false;
    }

    // IE does not accept a DOM text node as parameter for the method `Node.contains()`
    if (innerNode.nodeType !== Node.ELEMENT_NODE) {
        innerNode = innerNode.parentNode;
        if (!innerNode) { return false; }
        // the parent of the inner text node may be the outer node itself
        if (outerNode === innerNode) { return true; }
    }

    // finally use the native method `Node.contains()`
    return outerNode.contains(innerNode);
}

// DOM node geometry ----------------------------------------------------------

/**
 * Returns the size of the entire document.
 *
 * @returns
 *  The size of the entire document.
 */
export function getDocumentSize(): Size {
    const el = document.documentElement;
    return { width: el.offsetWidth, height: el.offsetHeight };
}

/**
 * Returns the exact outer size (with padding and borders) of the passed node,
 * in floating-point pixels.
 *
 * Especially elements containing text may have a fractional width or height,
 * but browsers usually report node sizes rounded (down) to integers when using
 * properties like 'offsetWidth', 'scrollWidth', etc.
 *
 * @param elem
 *  The DOM element whose size will be returned.
 *
 * @returns
 *  The outer size of the element, with padding and borders.
 */
export function getExactNodeSize(elem: Element): Size {
    const rect = elem.getBoundingClientRect();
    return { width: rect?.width ?? 0, height: rect?.height ?? 0 };
}

/**
 * Returns the outer size (with padding and borders) of the passed node, in
 * pixels, rounded up to the next integers.
 *
 * @param elem
 *  The DOM element whose size will be returned.
 *
 * @returns
 *  The outer size of the element, with padding and borders, rounded up to the
 *  next integers.
 */
export function getCeilNodeSize(elem: Element): Size {
    const size = getExactNodeSize(elem);
    size.width = ceil(size.width);
    size.height = ceil(size.height);
    return size;
}

/**
 * Returns the outer size (with padding and borders) of the passed node, in
 * pixels, rounded down to the next integers.
 *
 * @param elem
 *  The DOM element whose size will be returned.
 *
 * @returns
 *  The outer size of the element, with padding and borders, rounded down to
 *  the next integers.
 */
export function getFloorNodeSize(elem: Element): Size {
    const size = getExactNodeSize(elem);
    size.width = floor(size.width);
    size.height = floor(size.height);
    return size;
}

/**
 * Returns the absolute position and size of the specified element in the
 * viewport. This includes the distances of all four borders of the
 * node to the borders of the document.
 *
 * @param elem
 *  The DOM element whose position and size will be calculated.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The absolute position and size of the specified element. In difference to
 *  the result of `Element::getBoundingClientRect`, the properties "right" and
 *  "bottom" contain the distances between right/bottom border of the element
 *  and right/bottom border of the viewport.
 */
export function getElementPosition(elem: HTMLElement, options?: ElementPositionOptions): ElementPosition {
    const docSize = getDocumentSize();
    const rect = elem.getBoundingClientRect();
    const inner = options?.inner;
    const left = (rect?.left ?? 0) + (inner ? elem.clientLeft : 0);
    const top = (rect?.top ?? 0) + (inner ? elem.clientTop : 0);
    const width = inner ? elem.clientWidth : elem.offsetWidth;
    const height = inner ? elem.clientHeight : elem.offsetHeight;
    const right = docSize.width - left - width;
    const bottom = docSize.height - top - height;
    return { left, top, right, bottom, width, height };
}

// DOM node generators --------------------------------------------------------

/**
 * Creates the DOM text node.
 *
 * @param [label]
 *  The text contents.
 *
 * @returns
 *  The new DOM text node.
 */
export function createText(label?: Nullable<string | number>): Text {
    return document.createTextNode(is.number(label) ? String(label) : (label || ""));
}

/**
 * Creates the specified HTML element.
 *
 * @param tagName
 *  The tag name of the HTML element.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new HTML element instance.
 */
export function createElement<ET extends keyof HTMLElementTagNameMap>(tagName: ET, options?: CreateElementLabelOptions | string): HTMLElementTagNameMap[ET] {

    // create the element with CSS classes and identifier
    const elem = document.createElement(tagName);
    setElementClasses(elem, options);

    // add styles, label, and tooltip
    if (options && !is.string(options)) {
        setElementAttributes(elem, options.attrs);
        setElementDataSet(elem, options.dataset);
        setElementStyles(elem, options.style);
        setElementLabel(elem, options);
    }

    return elem;
}

/**
 * Creates an HTML `<div>` element. This function is a convenience shortcut for
 * `createElement("div", options)`.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new HTML `<div>` element instance.
 */
export function createDiv(options?: CreateElementLabelOptions | string): HTMLDivElement {
    return createElement("div", options);
}

/**
 * Creates an HTML `<span>` element. This function is a convenience shortcut
 * for `createElement("span", options)`.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new HTML `<span>` element instance.
 */
export function createSpan(options?: CreateElementLabelOptions | string): HTMLSpanElement {
    return createElement("span", options);
}

/**
 * Creates an HTML `<a>` element.
 *
 * @param href
 *  The value for the `href` attribute of the anchor element.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new HTML `<span>` element instance.
 */
export function createAnchor(href: string, options?: CreateElementLabelOptions | string): HTMLAnchorElement {
    const elem = createElement("a", options);
    elem.href = href;
    return elem;
}

/**
 * Creates the specified SVG element.
 *
 * @param tagName
 *  The tag name of the SVG element.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new SVG element instance.
 */
export function createSvg<ET extends keyof SVGElementTagNameMap>(tagName: ET, options?: CreateSVGElementOptions | string): SVGElementTagNameMap[ET] {

    // create the element with CSS classes and identifier
    const elem = document.createElementNS("http://www.w3.org/2000/svg", tagName);
    setElementClasses(elem, options);

    // add inline styles
    if (options && !is.string(options)) {
        setElementStyles(elem, options.style);
    }

    return elem;
}

/**
 * Creates a `<canvas>` element.
 *
 * @param width
 *  The width of the canvas bitmap, in pixels (this is not the CSS width of the
 *  HTML element).
 *
 * @param height
 *  The height of the canvas bitmap, in pixels (this is not the CSS height of
 *  the HTML element).
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new `<canvas>` element instance.
 */
export function createCanvas(width: number, height: number, options?: CreateCanvasElementOptions | string): HTMLCanvasElement {
    const elem = createElement("canvas", options);
    elem.width = width;
    elem.height = height;
    return elem;
}

/**
 * Creates a `<style>` element.
 *
 * @param markup
 *  The CSS markup to be inserted into the style element, as plain string, or
 *  as array of text lines.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new `<style>` element instance.
 */
export function createStyle(markup: string | string[], options?: CreateStyleElementOptions | string): HTMLStyleElement {
    const elem = createElement("style", options);
    elem.setAttribute("type", "text/css");
    elem.innerHTML = is.array(markup) ? str.joinTail(markup, "\n") : markup;
    return elem;
}

/**
 * Creates a new `<img>` element, loads the image from the specified URL, and
 * resolves or rejects a promise according to the load result.
 *
 * @param src
 *  The source URL of the image to be loaded.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`.
 *
 * @returns
 *  The new `<img>` element instance.
 */
export async function loadImage(src: string, options?: CreateImageElementOptions | string): Promise<HTMLImageElement> {
    return await new Promise<HTMLImageElement>((resolve, reject) => {
        const elem = createElement("img", options);
        elem.addEventListener("load", () => resolve(elem));
        elem.addEventListener("error", () => reject(new Error("error loading image")));
        elem.src = src;
    });
}

/**
 * Creates a new document fragment with optional child nodes.
 *
 * @param nodes
 *  The DOM nodes to be inserted into the fragment initially.
 *
 * @returns
 *  The new document fragment.
 */
export function createFragment(...nodes: Node[]): DocumentFragment {
    const fragment = document.createDocumentFragment();
    // `ParentNode.append(...nodes)` not supported in IE
    nodes.forEach(node => fragment.appendChild(node));
    return fragment;
}

/**
 * Creates a new empty node list for DOM nodes of a specific type.
 *
 * @returns
 *  The new node list.
 */
export function createNodeList<NT extends ChildNode = ChildNode>(): NodeListOf<NT> {
    return document.createDocumentFragment().childNodes as NodeListOf<NT>;
}

// measurement units ----------------------------------------------------------

/**
 * Converts a length value from a CSS length unit into another CSS length unit.
 *
 * @param value
 *  The length value to convert.
 *
 * @param fromUnit
 *  The source CSS length unit of the passed value.
 *
 * @param toUnit
 *  The target CSS length unit.
 *
 * @param [precision]
 *  If specified, the resulting length will be rounded to the nearest multiple
 *  of this value. Must be positive.
 *
 * @returns
 *  The length value converted to the target unit.
 */
export function convertLength(value: number, fromUnit: CSSLengthUnit, toUnit: CSSLengthUnit, precision?: number): number {
    const factors = getUnitFactors();
    value *= (factors[fromUnit] || 1) / (factors[toUnit] || 1);
    return Number.isFinite(precision) ? math.roundp(value, precision!) : value;
}

/**
 * Converts a length value from a CSS length unit into 1/100 of millimeters.
 *
 * @param value
 *  The length value to convert.
 *
 * @param fromUnit
 *  The source CSS length unit of the passed value.
 *
 * @returns
 *  The length value converted to 1/100 of millimeters, as integer.
 */
export function convertLengthToHmm(value: number, fromUnit: CSSLengthUnit): number {
    return round(convertLength(value, fromUnit, "mm") * 100);
}

/**
 * Converts a length value from 1/100 of millimeters into a CSS length unit.
 *
 * @param value
 *  The length value in 1/100 of millimeters to convert.
 *
 * @param toUnit
 *  The target CSS length unit.
 *
 * @param [precision]
 *  If specified, the resulting length will be rounded to the nearest multiple
 *  of this value. Must be positive.
 *
 * @returns
 *  The length value converted to the target unit.
 */
export function convertHmmToLength(value: number, toUnit: CSSLengthUnit, precision?: number): number {
    return convertLength(value / 100, "mm", toUnit, precision);
}

/**
 * Converts a CSS length value with length unit into a value of another CSS
 * length unit.
 *
 * @param valueAndUnit
 *  The value with its length unit to be converted (e.g. "20pt"), or a number
 *  that will be interpreted as CSS pixels (similar to JQuery).
 *
 * @param toUnit
 *  The target CSS length unit.
 *
 * @param [precision]
 *  If specified, the resulting length will be rounded to the nearest multiple
 *  of this value. Must be positive.
 *
 * @returns
 *  The length value converted to the target unit.
 */
export function convertCssLength(valueAndUnit: number | string, toUnit: CSSLengthUnit, precision?: number): number {

    // parse the leading floating-point number
    let value = is.number(valueAndUnit) ? valueAndUnit : parseFloat(valueAndUnit);
    if (!Number.isFinite(value)) { value = 0; }

    // extract the source CSS unit, convert the value
    const fromUnit = is.number(valueAndUnit) ? "px" : ((value !== 0) && (valueAndUnit.length > 2)) ? valueAndUnit.slice(-2) : "";
    if (fromUnit in getUnitFactors()) {
        value = convertLength(value, fromUnit as CSSLengthUnit, toUnit, precision);
    }

    return value;
}

/**
 * Converts a CSS length value with length unit into 1/100 of millimeters.
 *
 * @param valueAndUnit
 *  The value with its length unit to be converted (e.g. "20pt"), or a number
 *  that will be interpreted as CSS pixels (similar to JQuery).
 *
 * @returns
 *  The length value converted to 1/100 of millimeters, as integer.
 */
export function convertCssLengthToHmm(valueAndUnit: string | number): number {
    return round(convertCssLength(valueAndUnit, "mm") * 100);
}

/**
 * Converts a length value from 1/100 of millimeters into a CSS length unit and
 * returns the CSS length with its unit as string.
 *
 * @param value
 *  The length value in 1/100 of millimeters to convert.
 *
 * @param toUnit
 *  The target CSS length unit.
 *
 * @param [precision]
 *  If specified, the resulting length will be rounded to the nearest multiple
 *  of this value. Must be positive.
 *
 * @returns
 *  The length value converted to the target unit, followed by the unit name.
 */
export function convertHmmToCssLength(value: number, toUnit: CSSLengthUnit, precision?: number): string {
    return `${convertHmmToLength(value, toUnit, precision)}${toUnit}`;
}

/**
 * Converts the computed value of the specified CSS length attribute to pixels.
 *
 * @param element
 *  An HTML element, or a JQuery collection whose first element will be used.
 *
 * @param attr
 *  The name of the CSS attribute to be parsed and returned.
 *
 * @returns
 *  The length value, converted to pixels, rounded to integer.
 */
export function parseCssLength(element: NodeOrJQuery, attr: KeysOf<CSSStyleDeclaration>): number {
    const elem = toNode(element);
    const value = elem ? window.getComputedStyle(elem)[attr] : undefined;
    return is.string(value) ? convertCssLength(value, "px", 1) : 0;
}

/**
 * Sets the position of a rectangle as inline style to the passed DOM element.
 *
 * @param element
 *  The element to assign the position to.
 *
 * @param rect
 *  The rectangle to be assigned to the element.
 *
 * @param [unit="px"]
 *  The CSS unit to be used.
 */
export function setCssRectToElement(element: ElementCSSInlineStyle, rect: RectangleLike, unit: CSSLengthUnit = "px"): void {
    const { style } = element;
    style.left = `${rect.left}${unit}`;
    style.top = `${rect.top}${unit}`;
    style.width = `${rect.width}${unit}`;
    style.height = `${rect.height}${unit}`;
}

// CSS variables --------------------------------------------------------------

/**
 * Returns the current value of the specified CSS variable.
 *
 * @param name
 *  The name of the CSS variable (with leading double-dash, e.g. "--text").
 *
 * @param [source]
 *  The source element to be used to fetch the variable. By default, the global
 *  hidden storage node with CSS class namespace ".io-ox-office-main" will be
 *  used.
 *
 * @returns
 *  The current value of the specified CSS variable.
 */
export function resolveCssVar(name: string, source?: NodeOrJQuery): string {
    const elem = toNode(source) ?? $hiddenStorageNode[0];
    return window.getComputedStyle(elem).getPropertyValue(name).trim();
}

// DOM node positions ---------------------------------------------------------

/**
 * Returns whether the passed CSS border position is oriented horizontally
 * (either "left" or "right").
 *
 * @param pos
 *  The CSS border position.
 *
 * @returns
 *  Whether the passed border position is either "left" or "right".
 */
export function isHorizontalPosition(pos: CSSBorderPos): boolean {
    return (pos === "left") || (pos === "right");
}

/**
 * Returns whether the passed CSS border position is oriented vertically
 * (either "top" or "bottom").
 *
 * @param pos
 *  The CSS border position.
 *
 * @returns
 *  Whether the passed border position is either "top" or "bottom".
 */
export function isVerticalPosition(pos: CSSBorderPos): boolean {
    return (pos === "top") || (pos === "bottom");
}

/**
 * Returns whether the passed CSS border position is the leading side (either
 * "top" or "left").
 *
 * @param pos
 *  The CSS border position.
 *
 * @returns
 *  Whether the passed border position is either "top" or "left".
 */
export function isLeadingPosition(pos: CSSBorderPos): boolean {
    return (pos === "top") || (pos === "left");
}

/**
 * Returns whether the passed CSS border position is the trailing side (either
 * "bottom" or "right").
 *
 * @param pos
 *  The CSS border position.
 *
 * @returns
 *  Whether the passed border position is either "bottom" or "right".
 */
export function isTrailingPosition(pos: CSSBorderPos): boolean {
    return (pos === "bottom") || (pos === "right");
}

// hidden storage in DOM ------------------------------------------------------

/**
 * Inserts new HTML elements into a global hidden storage container. The nodes
 * will not be visible but will be part of the living DOM, thus it will be
 * possible to access and modify the geometry of the nodes.
 *
 * @param source
 *  The HTML elements to be inserted into the hidden storage container.
 *
 * @param [prepend=false]
 *  If set to `true`, the passed elements will be inserted before all other
 *  child elements of the hidden storage container.
 *
 * @returns
 *  The HTML elements passed to this function.
 */
export function insertHiddenNodes<ET extends NodeOrJQuery>(source: ET, prepend?: boolean): ET {
    if (prepend) {
        $hiddenStorageNode.prepend(source);
    } else {
        $hiddenStorageNode.append(source);
    }
    return source;
}

/**
 * Returns the HTML elements contained in the global hidden storage container
 * matching the passed CSS selector.
 *
 * @param selector
 *  The jQuery selector used to search for the direct children in the global
 *  hidden storage node.
 *
 * @returns
 *  All matching HTML elements, as jQuery collection.
 */
export function findHiddenNodes(selector: JQuery.Selector): JQuery {
    return $hiddenStorageNode.children(selector);
}

/**
 * Returns whether the global hidden storage container contains HTML elements
 * matching the passed CSS selector.
 *
 * @param selector
 *  The jQuery selector used to search for the direct children in the global
 *  hidden storage node.
 *
 * @returns
 *  Whether the global hidden storage container contains at least one matching
 *  HTML element.
 */
export function hasHiddenNodes(selector: JQuery.Selector): boolean {
    return findHiddenNodes(selector).length > 0;
}
