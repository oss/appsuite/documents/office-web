/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { math, fmt, dict } from "@/io.ox/office/tk/algorithms";
import { createCanvas, resolveCssVar } from "@/io.ox/office/tk/dom/domutils";

// types ======================================================================

/**
 * The type of the original color a color model has been created from.
 */
export enum ColorType {

    /**
     * Full transparency (e.g. after parsing the CSS keyword "transparent").
     */
    TRANSPARENT = "transparent",

    /**
     * Any generic color in the RGB color model (e.g. after parsing CSS colors
     * in `#RRGGB` style, or `rgb()` function style).
     */
    RGB = "rgb",

    /**
     * Any generic color in the HSL color model (e.g. after parsing CSS colors
     * in `hsl()` function style).
     */
    HSL = "hsl",

    /**
     * A preset color (e.g. after parsing CSS color names).
     */
    PRESET = "preset",

    /**
     * A system color (e.g. after parsing CSS color names).
     */
    SYSTEM = "system",

    /**
     * A scheme color.
     */
    SCHEME = "scheme"
}

/**
 * A descriptor with details about a color.
 */
export interface ColorDescriptor {

    /**
     * The RGB color model of the color.
     */
    rgb: RGBModel;

    /**
     * The HSL color model of the color.
     */
    hsl: HSLModel;

    /**
     * The value of the alpha channel (0 to 1). This property is provided for
     * convenience, it is also contained in the RGB color model, and in the HSL
     * color model.
     */
    a: number;

    /**
     * The luma (weighted lightness) of the color (0 to 1).
     */
    y: number;

    /**
     * Whether the color is considered to be dark (the luma is less than a
     * specific threshold value).
     */
    dark: boolean;

    /**
     * The RGBA representation of the color, as 8-bit values in an array with
     * exactly four elements (RGBA order as used in `ImageData` objects).
     */
    bytes: Uint8ClampedArray;

    /**
     * The exact CSS representation of the color.
     */
    css: string;

    /**
     * The upper-case hexadecimal RRGGBB value, ignoring the actual opacity of
     * the color.
     */
    hex: string;

    /**
     * The type of the original color this descriptor has been created from.
     */
    type: ColorType;
}

/**
 * The RGBA channels of a color resolved form a CSS color. The array contains
 * four bytes with the values of the color channels.
 */
export interface ResolvedRGBA extends Uint8ClampedArray {
    /** The hexadecimal representation of the RGB channels (no alpha). */
    hex3: string;
    /** The hexadecimal representation of the RGBA channels. */
    hex4: string;
}

// constants ==================================================================

// threshold luma for dark colors
const Y_DARK = 0.2;

// maps the names of all CSS preset colors to the RGB color values
const PRESET_COLOR_MAP: Dict<string> = {
    aliceblue: "f0f8ff",        antiquewhite: "faebd7",         aqua: "00ffff",             aquamarine: "7fffd4",
    azure: "f0ffff",            beige: "f5f5dc",                bisque: "ffe4c4",           black: "000000",
    blanchedalmond: "ffebcd",   blue: "0000ff",                 blueviolet: "8a2be2",       brown: "a52a2a",
    burlywood: "deb887",        cadetblue: "5f9ea0",            chartreuse: "7fff00",       chocolate: "d2691e",
    coral: "ff7f50",            cornflowerblue: "6495ed",       cornsilk: "fff8dc",         crimson: "dc143c",
    darkblue: "00008b",         darkcyan: "008b8b",             darkgoldenrod: "b8860b",    darkgray: "a9a9a9",
    darkgreen: "006400",        darkgrey: "a9a9a9",             darkkhaki: "bdb76b",        darkmagenta: "8b008b",
    darkolivegreen: "556b2f",   darkorange: "ff8c00",           darkorchid: "9932cc",       darkred: "8b0000",
    darksalmon: "e9967a",       darkseagreen: "8fbc8f",         darkslateblue: "483d8b",    darkslategray: "2f4f4f",
    darkslategrey: "2f4f4f",    darkturquoise: "00ced1",        darkviolet: "9400d3",       deeppink: "ff1493",
    deepskyblue: "00bfff",      dimgray: "696969",              dimgrey: "696969",          dodgerblue: "1e90ff",
    firebrick: "b22222",        floralwhite: "fffaf0",          forestgreen: "228b22",      fuchsia: "ff00ff",
    gainsboro: "dcdcdc",        ghostwhite: "f8f8ff",           gold: "ffd700",             goldenrod: "daa520",
    gray: "808080",             green: "008000",                greenyellow: "adff2f",      grey: "808080",
    honeydew: "f0fff0",         hotpink: "ff69b4",              indianred: "cd5c5c",        indigo: "4b0082",
    ivory: "fffff0",            khaki: "f0e68c",                lavender: "e6e6fa",         lavenderblush: "fff0f5",
    lawngreen: "7cfc00",        lemonchiffon: "fffacd",         lightblue: "add8e6",        lightcoral: "f08080",
    lightcyan: "e0ffff",        lightgoldenrodyellow: "fafad2", lightgray: "d3d3d3",        lightgreen: "90ee90",
    lightgrey: "d3d3d3",        lightpink: "ffb6c1",            lightsalmon: "ffa07a",      lightseagreen: "20b2aa",
    lightskyblue: "87cefa",     lightslategray: "778899",       lightslategrey: "778899",   lightsteelblue: "b0c4de",
    lightyellow: "ffffe0",      lime: "00ff00",                 limegreen: "32cd32",        linen: "faf0e6",
    maroon: "800000",           mediumaquamarine: "66cdaa",     mediumblue: "0000cd",       mediumorchid: "ba55d3",
    mediumpurple: "9370db",     mediumseagreen: "3cb371",       mediumslateblue: "7b68ee",  mediumspringgreen: "00fa9a",
    mediumturquoise: "48d1cc",  mediumvioletred: "c71585",      midnightblue: "191970",     mintcream: "f5fffa",
    mistyrose: "ffe4e1",        moccasin: "ffe4b5",             navajowhite: "ffdead",      navy: "000080",
    oldlace: "fdf5e6",          olive: "808000",                olivedrab: "6b8e23",        orange: "ffa500",
    orangered: "ff4500",        orchid: "da70d6",               palegoldenrod: "eee8aa",    palegreen: "98fb98",
    paleturquoise: "afeeee",    palevioletred: "db7093",        papayawhip: "ffefd5",       peachpuff: "ffdab9",
    peru: "cd853f",             pink: "ffc0cb",                 plum: "dda0dd",             powderblue: "b0e0e6",
    purple: "800080",           rebeccapurple: "663399",        red: "ff0000",              rosybrown: "bc8f8f",
    royalblue: "4169e1",        saddlebrown: "8b4513",          salmon: "fa8072",           sandybrown: "f4a460",
    seagreen: "2e8b57",         seashell: "fff5ee",             sienna: "a0522d",           silver: "c0c0c0",
    skyblue: "87ceeb",          slateblue: "6a5acd",            slategray: "708090",        slategrey: "708090",
    snow: "fffafa",             springgreen: "00ff7f",          steelblue: "4682b4",        tan: "d2b48c",
    teal: "008080",             thistle: "d8bfd8",              tomato: "ff6347",           turquoise: "40e0d0",
    violet: "ee82ee",           wheat: "f5deb3",                white: "ffffff",            whitesmoke: "f5f5f5",
    yellow: "ffff00",           yellowgreen: "9acd32"
};

// maps the names of all CSS system colors to the RGB color values (values will be initialized lazily)
const SYSTEM_COLOR_MAP = dict.fill<string | null>([
    "activeborder",     "activecaption",    "appworkspace",         "background",
    "buttonface",       "buttonhighlight",  "buttonshadow",         "buttontext",
    "captiontext",      "graytext",         "highlight",            "highlighttext",
    "inactiveborder",   "inactivecaption",  "inactivecaptiontext",  "infobackground",
    "infotext",         "menu",             "menutext",             "scrollbar",
    "threeddarkshadow", "threedface",       "threedhighlight",      "threedlightshadow",
    "threedshadow",     "window",           "windowframe",          "windowtext"
], null);

// a <canvas> element used to resolve CSS colors
const colorCanvas = createCanvas(1, 1);
const colorContext = colorCanvas.getContext("2d", { willReadFrequently: true })!;

// regular expressions matching the RGB/HSL function style of CSS colors
const RGBA_FUNC_RE = /^rgba?\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*(1|0|0?\.\d+)\s*)?\)$/;
const HSLA_FUNC_RE = /^hsla?\(\s*(\d+)\s*,\s*(\d+)%\s*,\s*(\d+)%\s*(?:,\s*(1|0|0?\.\d+)\s*)?\)$/;

// private functions ==========================================================

// converts the passed string representing a hexadecimal digit
function parseHex1(hex: string): number {
    return parseInt(hex, 16) / 15;
}

// converts the passed string representing a hexadecimal byte value
function parseHex2(hex: string): number {
    return parseInt(hex, 16) / 255;
}

// converts the passed string representing a decimal value
function parseDec(dec: string, quot: number): number {
    return Math.min(1, parseInt(dec, 10) / quot);
}

// class AlphaModel ===========================================================

/**
 * Base class for various color models, providing the alpha channel of a
 * color.
 *
 * @param a
 *  The value of the alpha channel (opacity). The value `0` represents full
 *  transparency, the value `1` represents full opacity.
 */
export abstract class AlphaModel {

    // properties -------------------------------------------------------------

    /**
     * The value of the alpha channel (opacity), in the closed interval
     * `[0;1]`. The value `0` represents full transparency, the value `1`
     * represents full opacity.
     */
    a: number;

    // constructor ------------------------------------------------------------

    protected constructor(a: number) {
        this.a = math.clamp(a, 0, 1);
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates an instance of the class `RGBModel` representing the color of
     * this model.
     *
     * @returns
     *  An instance of the class `RGBModel` representing the color of this
     *  model.
     */
    abstract toRGB(): RGBModel;

    /**
     * Creates an instance of the class `HSLModel` representing the color of
     * this model.
     *
     * @returns
     *  An instance of the class `HSLModel` representing the color of this
     *  model.
     */
    abstract toHSL(): HSLModel;

    /**
     * Returns the CSS representation of the color contained in this model.
     *
     * @returns
     *  The CSS representation of the color contained in this model
     */
    abstract toCSS(): string;

    /**
     * Returns a descriptor object with detailed calculated information about
     * this color.
     *
     * @param type
     *  The type of the original color this model has been created from.
     *
     * @returns
     *  The descriptor object for this model.
     */
    getDescriptor(type: ColorType): ColorDescriptor {

        // shortcuts to values used repeatedly
        const rgb = this.toRGB();
        const y = rgb.getLuma();
        const bytes = rgb.toBytes();

        // create a color descriptor
        return {
            rgb,
            hsl: this.toHSL(),
            a: this.a,
            y,
            dark: y <= Y_DARK,
            bytes,
            css: this.toCSS(),
            hex: fmt.formatInt(bytes[0] * 0x10000 + bytes[1] * 0x100 + bytes[2], 16, { digits: 6 }),
            type
        };
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns the value of the specified color channel.
     *
     * @param channel
     *  The name of the color channel. The supported channel names depend on
     *  the subclass representing a specific color model. The channel name "a"
     *  will return the alpha channel provided by this base class.
     *
     * @returns
     *  The value of the color channel.
     */
    get(channel: string): number {
        return (this as unknown as Dict<number>)[channel];
    }

    /**
     * Changes the specified color channel to the specified value.
     *
     * @param channel
     *  The name of the color channel. The supported channel names depend on
     *  the subclass representing a specific color model. The channel name "a"
     *  will manipulate the alpha channel provided by this base class.
     *
     * @param value
     *  The new value of the color channel.
     *
     * @returns
     *  A reference to this instance.
     */
    set(channel: string, value: number): this {
        (this as unknown as Dict<number>)[channel] = math.clamp(value, 0, 1);
        return this;
    }

    /**
     * Modulates the specified color channel (multiplies the color channel with
     * the specified factor).
     *
     * @param channel
     *  The name of the color channel. The supported channel names depend on
     *  the subclass representing a specific color model. The channel name "a"
     *  will manipulate the alpha channel provided by this base class.
     *
     * @param factor
     *  The modulation factor. MUST be non-negative.
     *
     * @returns
     *  A reference to this instance.
     */
    modulate(channel: string, factor: number): this {
        return this.set(channel, this.get(channel) * factor);
    }

    /**
     * Shifts the specified color channel (adds the specified value to the
     * color channel).
     *
     * @param channel
     *  The name of the color channel. The supported channel names depend on
     *  the subclass representing a specific color model. The channel name "a"
     *  will manipulate the alpha channel provided by this base class.
     *
     * @param shift
     *  The value to be added to the color channel.
     *
     * @returns
     *  A reference to this instance.
     */
    offset(channel: string, shift: number): this {
        return this.set(channel, this.get(channel) + shift);
    }
}

// class RGBModel =============================================================

/**
 * Possible names of RGB color channels.
 */
export type RGBChannelKey = "r" | "g" | "b";

/**
 * Representation of a color using the RGB color model. This class adds the
 * color channels "r", "g", and "b" which can be manipulated by the methods of
 * the base class `AlphaModel`.
 *
 * @param r
 *  The value of the red channel.
 *
 * @param g
 *  The value of the green channel.
 *
 * @param b
 *  The value of the blue channel.
 *
 * @param a
 *  The value of the alpha channel.
 */
export class RGBModel extends AlphaModel implements Cloneable<RGBModel> {

    // static functions -------------------------------------------------------

    /**
     * Parses the passed hexadecimal RGB string and creates an instance of the
     * class `RGBModel`.
     *
     * @param rgb
     *  The string representation of a 6-digit hexadecimal number (an RGB color
     *  in the format `RRGGBB`).
     *
     * @param [extended]
     *  If set to `true`, the parameter `rgb` can also be a 3-digit hexadecimal
     *  number in the format `RGB`, or a 4-digit hexadecimal number in the
     *  format `RGBA`, or an 8-digit hexadecimal number in the format
     *  `RRGGBBAA`.
     *
     * @returns
     *  The new RGB color model created from the passed string; or `null`, if
     *  the passed string is not a valid hexadecimal number.
     */
    static parseHex(rgb: string, extended = false): RGBModel | null {

        if (/^[0-9a-f]+$/i.test(rgb)) {
            switch (rgb.length) {
                case 3: return extended ? new RGBModel(parseHex1(rgb[0]), parseHex1(rgb[1]), parseHex1(rgb[2]), 1) : null;
                case 4: return extended ? new RGBModel(parseHex1(rgb[0]), parseHex1(rgb[1]), parseHex1(rgb[2]), parseHex1(rgb[3])) : null;
                case 6: return new RGBModel(parseHex2(rgb.slice(0, 2)), parseHex2(rgb.slice(2, 4)), parseHex2(rgb.slice(4)), 1);
                case 8: return extended ? new RGBModel(parseHex2(rgb.slice(0, 2)), parseHex2(rgb.slice(2, 4)), parseHex2(rgb.slice(4, 6)), parseHex2(rgb.slice(6))) : null;
            }
        }

        return null;
    }

    /**
     * Creates an instance of the class `RGBModel` from the passed RGBA byte
     * components.
     *
     * @param bytes
     *  The array of bytes representing a pixel. The array MUST contain three
     *  or four elements in order `R,G,B[,A]`, as used for example by the class
     *  `ImageData`.
     *
     * @returns
     *  The new RGB color model created from the passed bytes.
     */
    static parseBytes(bytes: ArrayLike<number>): RGBModel {
        return new RGBModel(bytes[0] / 255, bytes[1] / 255, bytes[2] / 255, (bytes[3] ?? 255) / 255);
    }

    // properties -------------------------------------------------------------

    /**
     * The value of the red channel, in the interval `[0;1]`.
     */
    r: number;

    /**
     * The value of the green channel, in the interval `[0;1]`.
     */
    g: number;

    /**
     * The value of the blue channel, in the interval `[0;1]`.
     */
    b: number;

    // constructor ------------------------------------------------------------

    constructor(r: number, g: number, b: number, a: number) {
        super(a);
        this.r = math.clamp(r, 0, 1);
        this.g = math.clamp(g, 0, 1);
        this.b = math.clamp(b, 0, 1);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a clone of this RGB color model.
     *
     * @returns
     *  A clone of this RGB color model.
     */
    clone(): RGBModel {
        return new RGBModel(this.r, this.g, this.b, this.a);
    }

    /**
     * Returns the luma (the weighted lightness) of this RGB color.
     *
     * @returns
     *  The luma of this color, in the interval `[0;1]`.
     */
    getLuma(): number {
        return 0.2126 * this.r + 0.7152 * this.g + 0.0722 * this.b;
    }

    /**
     * Returns a reference to this instance. Used to ease the conversion from
     * different color models to an RGB color model.
     *
     * @returns
     *  A reference to this instance.
     */
    toRGB(): this {
        return this;
    }

    /**
     * Creates an instance of the class `HSLModel` representing the color of
     * this RGB color model.
     *
     * @returns
     *  An instance of the class `HSLModel` representing the color of this RGB
     *  color model.
     */
    toHSL(): HSLModel {

        const { r, g, b } = this;
        const max = Math.max(r, g, b);
        const min = Math.min(r, g, b);
        const df = max - min;
        const sum = max + min;

        const h = (df === 0) ? 0 : (max === r) ? math.modulo((g - b) / df * 60, 360) : (max === g) ? ((b - r) / df * 60 + 120) : ((r - g) / df * 60 + 240);
        const s = ((sum === 0) || (sum === 2)) ? 0 : (sum < 1) ? (df / sum) : (df / (2 - sum));
        const l = sum / 2;

        return new HSLModel(h, s, l, this.a);
    }

    /**
     * Returns the byte representation of the color contained in this model.
     *
     * @returns
     *  The byte representation of the color contained in this model. The array
     *  will contain 4 integers, in the order `R,G,B,A` as used for example in
     *  `ImageData` objects.
     */
    toBytes(): Uint8ClampedArray {
        const bytes = new Uint8ClampedArray(4);
        // double rounding to workaround rounding errors during RGB/HSL conversion
        bytes[0] = Math.round(Math.round(this.r * 25500000) / 100000);
        bytes[1] = Math.round(Math.round(this.g * 25500000) / 100000);
        bytes[2] = Math.round(Math.round(this.b * 25500000) / 100000);
        bytes[3] = Math.round(this.a * 255);
        return bytes;
    }

    /**
     * Returns the CSS representation of the color contained in this model.
     *
     * @returns
     *  The CSS representation of the color contained in this model.
     */
    toCSS(): string {

        // prefer keyword "transparent" for fully transparent colors
        if (this.a <= 0) { return "transparent"; }

        // opaque color: return #RRGGBB style; otherwise return rgba() function style
        const bytes = this.toBytes();
        return (this.a >= 1) ? `#${fmt.formatInt(bytes[0] * 0x10000 + bytes[1] * 0x100 + bytes[2], 16, { digits: 6, lower: true })}` :
            `rgba(${bytes[0]},${bytes[1]},${bytes[2]},${math.roundp(this.a, 0.001)})`;
    }

    /**
     * Applies a gamma shift to this color model.
     *
     * @param gamma
     *  The gamma shift to be applied to the color channels.
     *
     * @returns
     *  A reference to this instance.
     */
    gamma(gamma: number): this {
        this.r **= gamma;
        this.g **= gamma;
        this.b **= gamma;
        return this;
    }

    /**
     * Changes this color model to its inverse RGB color.
     *
     * @returns
     *  A reference to this instance.
     */
    inv(): this {
        this.r = 1 - this.r;
        this.g = 1 - this.g;
        this.b = 1 - this.b;
        return this;
    }

    /**
     * Changes this color model to its weighted grayscale.
     *
     * @returns
     *  A reference to this instance.
     */
    gray(): this {
        this.r = this.g = this.b = this.getLuma();
        return this;
    }

    /**
     * Changes the specified color channel to the specified value, and applies
     * a gamma correction.
     *
     * @param channel
     *  The name of the color channel.
     *
     * @param value
     *  The new value of the color channel.
     *
     * @param gamma
     *  The gamma shift to be applied to the passed channel value.
     *
     * @returns
     *  A reference to this instance.
     */
    setGamma(channel: RGBChannelKey, value: number, gamma: number): this {
        return this.set(channel, Math.max(0, value) ** gamma);
    }

    /**
     * Modulates the specified color channel (multiplies the color channel with
     * the specified factor), and applies a gamma correction.
     *
     * @param channel
     *  The name of the color channel.
     *
     * @param factor
     *  The modulation factor. MUST be non-negative.
     *
     * @param gamma
     *  The gamma shift to be applied to the passed channel value before
     *  (inverse gamma) and after modulation.
     *
     * @returns
     *  A reference to this instance.
     */
    modulateGamma(channel: RGBChannelKey, factor: number, gamma: number): this {
        return this.setGamma(channel, this[channel] ** (1 / gamma) * factor, gamma);
    }

    /**
     * Shifts the specified color channel (adds the specified value to the
     * color channel), and applies a gamma correction.
     *
     * @param channel
     *  The name of the color channel.
     *
     * @param shift
     *  The value to be added to the color channel.
     *
     * @param gamma
     *  The gamma shift to be applied to the passed channel value before
     *  (inverse gamma) and after shifting.
     *
     * @returns
     *  A reference to this instance.
     */
    offsetGamma(channel: RGBChannelKey, shift: number, gamma: number): this {
        return this.setGamma(channel, this[channel] ** (1 / gamma) + shift, gamma);
    }

    /**
     * Changes this color model to its inverse RGB color with gamma correction.
     *
     * @returns
     *  A reference to this instance.
     */
    invGamma(gamma: number): this {
        return this.gamma(1 / gamma).inv().gamma(gamma);
    }

    /**
     * Darkens this color by decreasing the values of the RGB color channels
     * according to the passed value with gamma correction.
     *
     * @param value
     *  The amount to darken this color, in the closed interval `[0;1]`. The
     *  number `0` will not modify this color, the number `1` will change this
     *  color to black, all numbers inside the interval will darken this color
     *  accordingly.
     *
     * @param gamma
     *  The gamma shift to be applied to the RGB color channel values before
     *  (inverse gamma) and after darkening.
     *
     * @returns
     *  A reference to this instance.
     */
    darkenGamma(value: number, gamma: number): this {
        const factor = math.clamp(1 - value, 0, 1);
        return this.gamma(1 / gamma).modulate("r", factor).modulate("g", factor).modulate("b", factor).gamma(gamma);
    }

    /**
     * Lightens this color by increasing the values of the RGB color channels
     * according to the passed value with gamma correction.
     *
     * @param value
     *  The amount to lighten this color, in the closed interval `[0;1]`. The
     *  number `0` will not modify this color, the number `1` will change this
     *  color to white, all numbers inside the interval will lighten this color
     *  accordingly.
     *
     * @param gamma
     *  The gamma shift to be applied to the RGB color channel values before
     *  (inverse gamma) and after lightening.
     *
     * @returns
     *  A reference to this instance.
     */
    lightenGamma(value: number, gamma: number): this {
        const factor = math.clamp(1 - value, 0, 1);
        return this.gamma(1 / gamma).inv().modulate("r", factor).modulate("g", factor).modulate("b", factor).inv().gamma(gamma);
    }
}

// class HSLModel =============================================================

/**
 * Possible names of HSL color channels.
 */
export type HSLChannelKey = "h" | "s" | "l";

/**
 * Representation of a color using the HSL color model. This class adds the
 * color channels "h", "s", and "l" which can be manipulated by the methods of
 * the base class `AlphaModel`.
 *
 * @param h
 *  The value of the hue channel, in degress. The value `0` represents red,
 *  `120` represents green, and `240` represents blue.
 *
 * @param s
 *  The value of the saturation channel. The value `0` represents gray, the
 *  value `1` represents the fully saturated color.
 *
 * @param l
 *  The value of the luminance channel. The value `0` represents black
 *  (regardless of the other values), and the value `1` represents white.
 */
export class HSLModel extends AlphaModel implements Cloneable<HSLModel> {

    // properties -------------------------------------------------------------

    /**
     * The value of the hue channel, in degrees, in the half-open interval
     * `[0;360)`. The value `0` represents red, `120` represents green, and
     * `240` represents blue.
     */
    h: number;

    /**
     * The value of the saturation channel, in the interval `[0;1]`. The value
     * `0` represents gray, the value `1` represents the fully saturated color.
     */
    s: number;

    /**
     * The value of the luminance channel, in the interval `[0;1]`. The value
     * `0` represents black (regardless of the other values), and the value `1`
     * represents white.
     */
    l: number;

    // constructor ------------------------------------------------------------

    constructor(h: number, s: number, l: number, a: number) {
        super(a);
        this.h = math.modulo(h, 360);
        this.s = math.clamp(s, 0, 1);
        this.l = math.clamp(l, 0, 1);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a clone of this HSL color model.
     *
     * @returns
     *  A clone of this HSL color model.
     */
    clone(): HSLModel {
        return new HSLModel(this.h, this.s, this.l, this.a);
    }

    /**
     * Creates an instance of the class `RGBModel` representing the color of
     * this HSL color model.
     *
     * @returns
     *  An instance of the class `RGBModel` representing the color of this HSL
     *  color model.
     */
    toRGB(): RGBModel {

        const h = this.h / 60;
        const s = this.s;
        let r = (math.clamp(Math.abs(h - 3) - 1, 0, 1) - 0.5) * s + 0.5;
        let g = (math.clamp(2 - Math.abs(2 - h), 0, 1) - 0.5) * s + 0.5;
        let b = (math.clamp(2 - Math.abs(4 - h), 0, 1) - 0.5) * s + 0.5;

        let l = this.l;
        if (l < 0.5) {
            l *= 2;
            r *= l;
            g *= l;
            b *= l;
        } else if (l > 0.5) {
            l = 2 - 2 * l;
            r = 1 - (1 - r) * l;
            g = 1 - (1 - g) * l;
            b = 1 - (1 - b) * l;
        }

        return new RGBModel(r, g, b, this.a);
    }

    /**
     * Returns a reference to this instance. Used to ease the conversion from
     * different color models to an HSL color model.
     *
     * @returns
     *  A reference to this instance.
     */
    toHSL(): this {
        return this;
    }

    /**
     * Returns the CSS representation of the color contained in this model.
     *
     * @returns
     *  The CSS representation of the color contained in this model
     */
    toCSS(): string {

        // prefer keyword "transparent" for fully transparent colors
        if (this.a <= 0) { return "transparent"; }

        // opaque color: return hsl() function style; otherwise return hsla() function style
        const hsl = `${Math.round(this.h)},${Math.round(this.s * 100)}%,${Math.round(this.l * 100)}%`;
        return (this.a >= 1) ? `hsl(${hsl})` : `hsla(${hsl},${math.roundp(this.a, 0.001)})`;
    }

    /**
     * Changes the hue channel to the specified value.
     *
     * @param hue
     *  The new value of the hue channel, in degrees.
     *
     * @returns
     *  A reference to this instance.
     */
    setH(hue: number): this {
        this.h = math.clamp(hue, 0, 360) % 360;
        return this;
    }

    /**
     * Modulates the hue channel (multiplies the hue value with the specified
     * factor).
     *
     * @param factor
     *  The modulation factor. MUST be non-negative.
     *
     * @returns
     *  A reference to this instance.
     */
    modulateH(factor: number): this {
        return this.setH(this.h * factor);
    }

    /**
     * Shifts the hue channel (adds the specified value to the hue value).
     *
     * @param shift
     *  The value to be added to the hue channel.
     *
     * @returns
     *  A reference to this instance.
     */
    offsetH(shift: number): this {
        return this.setH(this.h + shift);
    }

    /**
     * Changes this color model to its inverse hue value.
     *
     * @returns
     *  A reference to this instance.
     */
    comp(): this {
        this.h = (this.h + 180) % 360;
        return this;
    }
}

// public functions ===========================================================

/**
 * Returns the RGB value of the specified CSS preset color.
 *
 * @param presetColor
 *  The name of a preset color, as used in CSS. The character case of the color
 *  name will be ignored.
 *
 * @returns
 *  The hexadecimal RGB value of the preset color, as 6-digit string; or
 *  `null`, if the passed color name is invalid.
 */
export function getPresetColor(presetColor: string): string | null {
    return PRESET_COLOR_MAP[presetColor.toLowerCase()] || null;
}

/**
 * Returns the RGB value of the specified CSS system color.
 *
 * @param systemColor
 *  The name of a system color, as used in CSS. The character case of the color
 *  name will be ignored.
 *
 * @returns
 *  The hexadecimal RGB value of the system color, as 6-digit string; or
 *  `null`, if the passed color name is invalid.
 */
export function getSystemColor(systemColor: string): string | null {
    const key = systemColor.toLowerCase();
    let rgb = SYSTEM_COLOR_MAP[key];
    if (rgb === null) {
        SYSTEM_COLOR_MAP[key] = rgb = resolveCssColor(systemColor).hex3;
    }
    return rgb || null;
}

/**
 * Converts the passed CSS color string to a color descriptor.
 *
 * @param value
 *  The CSS color value. The following CSS colors will be accepted:
 *  - The keyword `"transparent"`.
 *  - Predefined color names, e.g. `"Red"`.
 *  - System color names, e.g. `"ButtonFace"`.
 *  - Hexadecimal colors (6-digit, and 3-digit), with leading hash sign.
 *  - Hexadecimal colors with alpha channel (8-digit, and 4-digit), with
 *    leading hash sign.
 *  - RGB colors in function style, using the `rgb()` or `rgba()` function.
 *  - HSL colors in function style, using the `hsl()` or `hsla()` function.
 *
 *  The following CSS colors will NOT be accepted:
 *  - New space-separated syntax for function style (e.g. "rgb(0 0 0 / 1)")
 *  - Percent values for function style (e.g. "rgb(0%, 0%, 0%, 100%)")
 *  - Other CSS color models ("hwb", "lab", etc.).
 *
 * @returns
 *  A color descriptor, if the passed string could be parsed successfully;
 *  otherwise `null`.
 */
export function parseCssColor(value: string): ColorDescriptor | null {

    // no valid string passed
    if (value.length === 0) { return null; }

    // CSS keyword "transparent"
    if (value === "transparent") {
        return new RGBModel(0, 0, 0, 0).getDescriptor(ColorType.TRANSPARENT);
    }

    // predefined color names
    const presetColor = getPresetColor(value);
    if (presetColor) {
        return RGBModel.parseHex(presetColor)!.getDescriptor(ColorType.PRESET);
    }

    // system color names
    const systemColor = getSystemColor(value);
    if (systemColor) {
        return RGBModel.parseHex(systemColor)!.getDescriptor(ColorType.SYSTEM);
    }

    // 3-digit hexadecimal color (#RGB), 4-digit hexadecimal color (#RGBA),
    // 6-digit hexadecimal color (#RRGGBB), 8-digit hexadecimal color (#RRGGBBAA)
    if (value.startsWith("#")) {
        const model = RGBModel.parseHex(value.slice(1), true);
        return model ? model.getDescriptor(ColorType.RGB) : null;
    }

    // RGB/RGBA color in function style
    const rgbParts = RGBA_FUNC_RE.exec(value);
    if (rgbParts) {
        const r = parseDec(rgbParts[1], 255);
        const g = parseDec(rgbParts[2], 255);
        const b = parseDec(rgbParts[3], 255);
        const a = rgbParts[4] ? parseFloat(rgbParts[4]) : 1;
        return new RGBModel(r, g, b, a).getDescriptor(ColorType.RGB);
    }

    // HSL/HSLA color in function style
    const hslParts = HSLA_FUNC_RE.exec(value);
    if (hslParts) {
        const h = math.modulo(parseInt(hslParts[1], 10), 360);
        const s = parseDec(hslParts[2], 100);
        const l = parseDec(hslParts[3], 100);
        const a = hslParts[4] ? parseFloat(hslParts[4]) : 1;
        return new HSLModel(h, s, l, a).getDescriptor(ColorType.HSL);
    }

    return null;
}

/**
 * Resolves the RGBA channel values of the passed CSS color string.
 *
 * @param value
 *  The CSS color value. Can be any color supported by the current browser.
 *
 * @returns
 *  The RGBA channel values.
 */
export function resolveCssColor(value: string): ResolvedRGBA {
    colorContext.clearRect(0, 0, 1, 1);
    colorContext.fillStyle = value;
    colorContext.fillRect(0, 0, 1, 1);
    const rgba = colorContext.getImageData(0, 0, 1, 1).data;
    const hex3 = fmt.formatInt(rgba[0] * 0x10000 + rgba[1] * 0x100 + rgba[2], 16, { digits: 6, lower: true });
    const hex4 = hex3 + fmt.formatInt(rgba[3], 16, { digits: 2, lower: true });
    return Object.assign(rgba, { hex3, hex4 });
}

/**
 * Rteurns the number of colors in the color scheme used for highlighting of
 * specific document contents.
 *
 * @returns
 *  The number of colors in the Documents color scheme.
 */
export function getSchemeColorCount(): number {
    // ATTENTION: this value MUST NOT be defined as global constant (CSS may be loaded a little later)
    return parseInt(resolveCssVar("--scheme-color-count"), 10) || 1;
}

/**
 * Converts the passed zero-based index to the identifier of a scheme color
 * defined in the LESS module `tk/dom/exports.less`.
 *
 * @param index
 *  An arbitrary zero-based index to be converted to a scheme color index.
 *
 * @returns
 *  The zero-based index of a scheme color associated to the passed index.
 */
export function getSchemeColorIndex(index: number): number {
    return math.modulo(index, getSchemeColorCount()) + 1;
}
