/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { createIcon as createBSIcon } from "$/io.ox/core/components";

import { is, map, jpromise } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import type { ElementBaseOptions, ElementStyleOptions } from "@/io.ox/office/tk/dom/domutils";
import { setElementClasses, setElementStyles, createElement, createDiv, createSvg, createStyle, loadImage, insertHiddenNodes } from "@/io.ox/office/tk/dom/domutils";
import { getDeviceResolution } from "@/io.ox/office/tk/dom/device";
import { Canvas } from "@/io.ox/office/tk/canvas/canvas";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";

// load the generated SVG sprite file (vite-plugin-svg-sprite)
import svgSprite from "virtual:svg/docs-icons.svg";

// types ======================================================================

/**
 * Valid identifiers for Bootstrap icons. Can be expanded as desired (there is
 * no way to auto-generate the names of all existing Bootstrap icons).
 */
export type BSIconId =
    "bi:123" |
    "bi:arrow-down-up" |
    "bi:arrow-left-right" |
    "bi:arrow-repeat" |
    "bi:arrows-angle-expand" |
    "bi:arrows-angle-contract" |
    "bi:bar-chart" |
    "bi:bicycle" |
    "bi:box-arrow-down" |
    "bi:box-arrow-down-left" |
    "bi:box-arrow-down-right" |
    "bi:box-arrow-in-down" |
    "bi:box-arrow-in-down-left" |
    "bi:box-arrow-in-down-right" |
    "bi:box-arrow-in-left" |
    "bi:box-arrow-in-right" |
    "bi:box-arrow-in-up" |
    "bi:box-arrow-in-up-left" |
    "bi:box-arrow-in-up-right" |
    "bi:box-arrow-left" |
    "bi:box-arrow-right" |
    "bi:box-arrow-up" |
    "bi:box-arrow-up-left" |
    "bi:box-arrow-up-right" |
    "bi:bug-fill" |
    "bi:calculator" |
    "bi:calendar" |
    "bi:card-heading" |
    "bi:card-list" |
    "bi:caret-down-fill" |
    "bi:caret-left-fill" |
    "bi:caret-right-fill" |
    "bi:caret-up-fill" |
    "bi:check" |
    "bi:check-square" |
    "bi:chevron-double-down" |
    "bi:chevron-double-left" |
    "bi:chevron-double-right" |
    "bi:chevron-double-up" |
    "bi:chevron-down" |
    "bi:chevron-left" |
    "bi:chevron-right" |
    "bi:chevron-up" |
    "bi:circle-fill" |
    "bi:clipboard-plus" |
    "bi:clock" |
    "bi:crop" |
    "bi:cloud-download" |
    "bi:cloud-upload" |
    "bi:dash-circle" |
    "bi:diagram-3-fill" |
    "bi:disc" |
    "bi:download" |
    "bi:envelope" |
    "bi:exclamation-circle" |
    "bi:exclamation-octagon" |
    "bi:exclamation-triangle" |
    "bi:exclamation-triangle-fill" |
    "bi:eye" |
    "bi:eye-slash" |
    "bi:file" |
    "bi:file-earmark-text" |
    "bi:file-ppt" |
    "bi:files" |
    "bi:film" |
    "bi:fonts" |
    "bi:gear" |
    "bi:globe" |
    "bi:image" |
    "bi:image-fill" |
    "bi:info-circle" |
    "bi:info-circle-fill" |
    "bi:keyboard" |
    "bi:layout-text-window-reverse" |
    "bi:life-preserver" |
    "bi:link" |
    "bi:list" |
    "bi:lock-fill" |
    "bi:megaphone-fill" |
    "bi:ox-ai" |
    "bi:palette" |
    "bi:pause-fill" |
    "bi:pencil" |
    "bi:pencil-square" |
    "bi:play-circle" |
    "bi:play-fill" |
    "bi:plus-circle-fill" |
    "bi:plus-lg" |
    "bi:printer" |
    "bi:question" |
    "bi:reply-fill" |
    "bi:record-fill" |
    "bi:save" |
    "bi:scissors" |
    "bi:search" |
    "bi:share" |
    "bi:skip-backward-fill" |
    "bi:skip-end-fill" |
    "bi:square" |
    "bi:stop" |
    "bi:stop-fill" |
    "bi:sun-fill" |
    "bi:table" |
    "bi:three-dots-vertical" |
    "bi:trash" |
    "bi:truck" |
    "bi:tv" |
    "bi:watch" |
    "bi:wrench" |
    "bi:x" |
    "bi:x-circle-fill" |
    "bi:x-lg" |
    "bi:x-octagon" |
    "bi:zoom-in" |
    "bi:zoom-out";

/**
 * Valid identifiers for SVG icons.
 */
export type SVGIconId = `svg:${string}`;

/**
 * Valid identifiers for PNG icons.
 */
export type PNGIconId = `png:${string}`;

/**
 * Valid identifiers for plain-text icons.
 */
export type TXTIconId = `txt:${string}`;

/**
 * Valid identifiers for icon elements.
 */
export type IconId = BSIconId | SVGIconId | PNGIconId | TXTIconId;

/**
 * Options for creating a new icon element.
 */
export interface CreateIconOptions extends ElementBaseOptions, ElementStyleOptions {

    /**
     * A custom size of the icon root element, either as number in CSS pixels;
     * or as string with CSS length unit.
     */
    size?: number | string;
}

// private types ==============================================================

// type identifiers of scaling factors for PNG sprite
type PNGSpriteScale = "sd" | "hd";

// private functions ==========================================================

/**
 * Reads the specified PNG sprite image.
 */
async function loadPngSprite(spriteScale: PNGSpriteScale): Promise<HTMLImageElement> {
    let spriteModule: typeof import("*.png");
    switch (spriteScale) {
        case "sd": spriteModule = await import("virtual:png/docs-icons-sd.png"); break;
        case "hd": spriteModule = await import("virtual:png/docs-icons-hd.png"); break;
    }
    // modules "virtual:png/docs-icons-*.png" are exporting data URLs
    return await loadImage(spriteModule.default);
}

/**
 * Create the selector for an SVG icon according to the current UI language.
 */
function getSVGIconSelector(iconId: string): string {
    const elemId = `io-ox-docs-svg-${iconId}`;
    const elemIdWithLang = `${elemId};${LOCALE_DATA.language}`;
    return document.getElementById(elemIdWithLang) ? `#${elemIdWithLang}` : `#${elemId}`;
}

/**
 * Initializes loading and updating of PNG sprites.
 */
async function initializePngSprites(): Promise<void> {

    // load the CSS rules for the PNG sprites (vite-plugin-png-sprite)
    await import("virtual:png/docs-icons.css");

    // current scaling factor for PNG sprite according to device resolution/zoom
    let spriteScale: Opt<PNGSpriteScale>;
    // cache for <style> elements for different scaling factors
    const styleCache = new Map<PNGSpriteScale, Promise<HTMLStyleElement>>();

    // updates the PNG sprite currently used
    async function updateSprite(): Promise<void> {

        // select the icon set to be used according to device resolution and browser zoom
        const newSpriteScale = spriteScale = (getDeviceResolution() >= 1.25) ? "hd" : "sd";

        // try to resolve existing sprite from the cache
        await map.upsert(styleCache, newSpriteScale, async () => {

            // load the PNG data of the sprite
            const imageEl = await loadPngSprite(newSpriteScale);

            // use a canvas to adjust the raw image data (contains alpha channel in RGB channels only)
            const canvas = Canvas.fromImage(imageEl, { willReadFrequently: true });
            canvas.renderPixels(context => {
                for (let d = context.data, i = 0, l = d.length; i < l; i += 4) {
                    d[i + 3] = d[i];        // copy R channel to A channel
                    d.fill(255, i, i + 3);  // set RGB channels to white
                }
            });

            // create a blob URL for the sprite image
            const spriteBlob = await canvas.getBlob();
            const spriteURL = URL.createObjectURL(spriteBlob);
            canvas.destroy();

            // create a CSS rule that connects the icon elements to the sprite PNG
            return document.head.appendChild(createStyle([
                `:root[data-docs-icons-sc="${newSpriteScale}"] i.docs-icon {`,
                "  background-color: currentColor;",
                `  -webkit-mask-image: url("${spriteURL}");`,
                `  mask-image: url("${spriteURL}");`,
                "}"
            ]));
        });

        // activate the sprite via data attribute on root element, after the sprite PNG has been
        // downloaded and converted, to prevent flickering when changing browser zoom; but only when
        // "spriteScale" still matches, i.e. race condition when quickly changing browser zoom
        if (spriteScale === newSpriteScale) {
            document.documentElement.dataset.docsIconsSc = spriteScale;
        }
    }

    // listen to changes of device resolution and UI theme
    for (const type of ["change:resolution", "change:theme"] as const) {
        globalEvents.on(type, jpromise.wrapFloating(updateSprite));
    }

    // initialize the sprite currently in use
    await updateSprite();
}

/**
 * Updates icon settings after UI locale has changed.
 */
function updateLocale(): void {

    // select localized PNG icons by data attribute in root element
    document.documentElement.dataset.docsIconsLc = LOCALE_DATA.lc;

    // update localized SVG icons
    for (const iconEl of document.querySelectorAll<SVGElement>("svg.docs-icon")) {
        const iconId = iconEl.dataset.iconId ?? "";
        const useEl = iconEl.querySelector("use");
        useEl?.setAttribute("href", getSVGIconSelector(iconId));
    }
}

// global initialization ======================================================

// insert the SVG sprite into the DOM to be able to refer to the <symbol> elements
insertHiddenNodes(createDiv()).innerHTML = svgSprite;

// initialize PNG sprite handling (vite-plugin-png-sprite)
jpromise.floating(initializePngSprites());

// keep localized icons up-to-date
globalEvents.on("change:locale", updateLocale);
updateLocale();

// public functions ===========================================================

/**
 * Creates a DOM element representing a predefined icon.
 *
 * @param iconId
 *  The identifier of the icon. The type of the returned icon element depends
 *  on the icon type prefix:
 *  - "bi:": An `<svg>` element for a Bootstrap icon.
 *  - "svg:": An `<svg>` element for a Documents SVG icon.
 *  - "png:": An `<i>` element for a Documents PNG icon.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`, or to a number as
 *  convenience shortcut for the option `size`.
 *
 * @returns
 *  The DOM element for the specified icon.
 */
export function createIcon(iconId: BSIconId | SVGIconId, options?: CreateIconOptions | string | number): SVGElement;
export function createIcon(iconId: PNGIconId, options?: CreateIconOptions | string | number): HTMLElement;
export function createIcon(iconId: IconId, options?: CreateIconOptions | string | number): HTMLElement | SVGElement;
// implementation
export function createIcon(iconId: IconId, options?: CreateIconOptions | string | number): HTMLElement | SVGElement {

    // normalize the passed options
    options = is.number(options) ? { size: options } : is.string(options) ? { classes: options } : options;

    const tokens = /^([a-z]+):(.*)$/.exec(iconId);
    const type = tokens?.[1] ?? "";
    const id = tokens?.[2] ?? "";

    // the icon node to be returned
    let iconNode: Opt<HTMLElement | SVGElement>;
    // additional CSS class for the icon node
    let iconClass: Opt<string>;

    // handle all supported namespaces
    switch (type) {

        // create an <svg> element for Bootstrap icons
        case "bi": {
            iconClass = "bi"; // TODO: core bug, class "bi" will not be added before icon is loaded
            iconNode = createBSIcon(`bi/${id}.svg`)[0];
            break;
        }

        // create an <svg> element for private SVG icons
        case "svg": {
            iconClass = "bi docs-icon"; // class "bi" to inherit standard CSS
            iconNode = createSvg("svg");
            const useNode = iconNode.appendChild(createSvg("use"));
            useNode.setAttribute("href", getSVGIconSelector(id));
            break;
        }

        // create an <i> element for private PNG icons
        case "png":
            iconClass = "docs-icon";
            iconNode = createElement("i");
            break;

        // create an <i> element for plain-text icons
        case "txt":
            iconClass = "docs-txt-icon";
            iconNode = createElement("i", { label: id });
            break;

        // notify about invalid identifiers
        default: {
            const msgToken = (type === "fa") ? "deprecated FontAwesome" : "invalid";
            window.console.error(`createIcon: ${msgToken} icon identifier "${iconId}"`);
        }
    }

    // fallback icon for invalid identifiers
    if (!iconNode) {
        iconNode = createBSIcon("bi/x-circle-fill.svg")[0];
        iconNode.style.color = "red";
    }

    // set common attributes and styles
    setElementClasses(iconNode, iconClass);
    setElementClasses(iconNode, options);
    setElementStyles(iconNode, options?.style);
    iconNode.setAttribute("aria-hidden", "true");
    iconNode.dataset.iconId = id;
    if (options?.size) {
        const size = is.number(options.size) ? `${options.size}px` : options.size;
        iconNode.style.width = iconNode.style.height = size;
    }
    return iconNode;
}

/**
 * Creates the HTML markup for a DOM element representing a predefined icon.
 *
 * @param iconId
 *  The identifier of the icon.
 *
 * @param [options]
 *  Optional parameters. Can be set to a string containing CSS class names as
 *  convenience shortcut for the option `classes`, or to a number as
 *  convenience shortcut for the option `size`.
 *
 * @returns
 *  The HTML markup for the specified icon.
 */
export function createIconMarkup(iconId: IconId, options?: CreateIconOptions | string | number): string {
    return createIcon(iconId, options).outerHTML;
}
