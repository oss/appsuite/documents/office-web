/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, math, fun, ary, map } from "@/io.ox/office/tk/algorithms";
import { globalLogger } from "@/io.ox/office/tk/utils/logger";
import { LOCALE_DATA } from "@/io.ox/office/tk/locale";
import { escapeHTML, createText, createDiv, createSpan, detachChildren, insertHiddenNodes } from "@/io.ox/office/tk/dom/domutils";
import { type Canvas2DContext, Canvas } from "@/io.ox/office/tk/canvas";

const { min, max, round } = Math;

// globals ====================================================================

// global helper node for calculation of normal line height of a font
const lineHeightNode = fun.do(() => {
    const helperNode = insertHiddenNodes(createDiv("font-metrics-helper"));
    // create the complete HTML markup for ten text lines (in case a browser uses fractional line heights internally)
    helperNode.innerHTML = ary.fill(10, "<span>X</span>").join("<br>");
    return helperNode;
});

// global helper node for calculation of the base line position of a font
const baseLineNode = fun.do(() => {
    const helperNode = insertHiddenNodes(createDiv("font-metrics-helper"));
    helperNode.append(createSpan({ label: "X" }));
    helperNode.append(createSpan({ style: { display: "inline-block", width: "1px", height: "0" } }));
    return helperNode;
});

const baseLineChildSpan = baseLineNode.lastChild! as HTMLSpanElement;

// global helper node for splitting text into multiple lines
const textLinesNode = insertHiddenNodes(createDiv({
    classes: "font-metrics-helper",
    style: { whiteSpace: "pre-wrap", wordWrap: "break-word", verticalAlign: "top" }
}));

// global helper node for detection of writing direction
const [bidiNode, bidiChildSpan, bidiTextNode] = fun.do(() => {
    const helperNode = insertHiddenNodes(createDiv({ classes: "font-metrics-helper", style: { width: 0 } }));
    const spanNode = helperNode.appendChild(createSpan({ style: { position: "relative" } }));
    const textNode = spanNode.appendChild(createText("X"));
    return [helperNode, spanNode, textNode];
});

// global canvas for font width calculation
const textWidthCanvas = new Canvas({ _kind: "singleton" });

// global helper node for custom font metrics calculation
const customMetricsNode = insertHiddenNodes(createDiv("font-metrics-helper"));

// cache for calculated font metrics, mapped by font keys
const fontMetricsCache = new Map<string, Map<string, unknown>>();

// constants ==================================================================

/**
 * A flag specifying whether the automatic detection of the default writing
 * direction of arbitrary texts is supported by the current browser.
 * Technically, this flag specifies whether the HTML element attribute `dir`
 * supports the value `auto`.
 */
export const AUTO_BIDI_SUPPORT = fun.do(() => {
    try {
        // IE11 throws when executing this assignment
        bidiNode.dir = "auto";
        return true;
    } catch {
        globalLogger.warn("$badge{Font} - no support for automatic detection of writing direction");
    }
    return false;
});

// private constants ----------------------------------------------------------

// all digits, as array
const DIGITS = "0123456789".split("");

// private functions ==========================================================

/**
 * Fetches, caches, and returns a specific font metrics property. If a cache
 * key has been specified, and the font metrics cache contains an entry for the
 * passed font and cache key, that cached value will be returned. Otherwise,
 * the callback function will be invoked, and its result will be cached.
 *
 * @param font
 *  The font settings.
 *
 * @param callback
 *  The callback function that will be invoked, unless the font metrics cache
 *  already contains a cached value for the font and cache key. The return
 *  value of this function will be cached if specified (see parameter
 *  `cacheKey` below), and will be returned by this function.
 *
 * @param [cacheKey]
 *  If specified, a map key for the font metrics cache that will be checked
 *  first. An existing cache entry will be returned without invoking the
 *  callback function. Otherwise, the new value returned by the callback
 *  function will be inserted into the cache.
 *
 * @returns
 *  The return value of the callback function.
 */
function fetchAndCacheResult<T>(font: Font, callback: FuncType<T>, cacheKey?: string): T {

    // the unique key of the passed font
    const fontKey = font.key;
    // all known cached metrics settings for the font
    const cacheEntry = cacheKey ? map.upsert(fontMetricsCache, fontKey, () => new Map<string, unknown>()) : undefined;

    // look for an existing value in the cache
    if (cacheKey && cacheEntry!.has(cacheKey)) {
        return cacheEntry!.get(cacheKey) as T;
    }

    // fetch result from the specified callback function
    const result = callback();

    // insert the result into the cache entry
    if (cacheKey) { cacheEntry!.set(cacheKey, result); }
    return result;
}

/**
 * Prepares a helper DOM node used to calculate a specific font metrics
 * property, and invokes the passed callback function.
 *
 * @param font
 *  The font settings for the helper node.
 *
 * @param node
 *  The DOM element to be prepared with the passed font settings.
 *
 * @param callback
 *  The callback function that will be invoked after preparing the passed
 *  helper node. The return value of this function will be cached if specified
 *  (see parameter `cacheKey` below), and will be returned by this function.
 *
 * @param [cacheKey]
 *  If specified, a map key for the font metrics cache that will be checked
 *  first. An existing cache entry will be returned without invoking the
 *  callback function. Otherwise, the new value returned by the callback
 *  function will be inserted into the cache.
 *
 * @returns
 *  The return value of the callback function.
 */
function calculateNodeMetrics<T>(font: Font, node: HTMLDivElement, callback: (node: HTMLDivElement) => T, cacheKey?: string): T {
    return fetchAndCacheResult(font, () => {

        // initialize formatting of the helper node
        node.style.fontFamily = font.family;
        node.style.fontSize = `${font.size}pt`;
        node.style.fontWeight = font.bold ? "bold" : "normal";
        node.style.fontStyle = font.italic ? "italic" : "normal";

        // fetch result from the specified callback function
        return callback(node);

    }, cacheKey);
}

/**
 * Prepares the helper canvas used to calculate specific text widths, and
 * invokes the passed callback function.
 *
 * @param font
 *  The font settings for the canvas.
 *
 * @param callback
 *  The callback function that will be invoked after preparing the helper
 *  helper canvas. Receives the prepared rendering context of the canvas
 *  element as first parameter. Must return the resulting text width that will
 *  be returned to the caller of this function, and that will be stored in the
 *  cache if needed.
 *
 * @param [cacheKey]
 *  If specified, a map key for the text width cache that will be checked
 *  first. An existing cache entry will be returned without invoking the
 *  callback function. Otherwise, the new value returned by the callback
 *  function will be inserted into the cache.
 *
 * @returns
 *  The return value of the callback function.
 */
function calculateCanvasMetrics<T>(font: Font, callback: (context: Canvas2DContext) => T, cacheKey?: string): T {
    return fetchAndCacheResult(font, () => textWidthCanvas.render(context => {
        context.setFontStyle({ font: font.getCanvasFont() });
        return callback(context);
    }), cacheKey);
}

/**
 * Returns the exact width of the passed text, rendered with the specified
 * font.
 *
 * @param font
 *  The font settings.
 *
 * @param text
 *  The text to be measured.
 *
 * @returns
 *  The exact width of the passed text, in pixels.
 */
function getTextWidth(font: Font, text: string): number {
    return calculateCanvasMetrics(font, context => context.getTextWidth(text));
}

// public functions ===========================================================

/**
 * Determines the default writing direction of the passed text, based on the
 * first strong character in the text (that is significant for bidirectional
 * writing), and the current browser's implementation of the Unicode
 * Bidirectional Algorithm (UBA).
 *
 * @param text
 *  The text to be examined.
 *
 * @returns
 *  The value `false`, if the text should be written from left to right; or
 *  `true`, if the text should be written from right to left.
 */
export function isRightToLeft(text: string): boolean {

    // fall-back if automatic detection is not supported; do not touch the DOM for empty strings
    if (!AUTO_BIDI_SUPPORT || (text.length === 0)) { return LOCALE_DATA.rtl; }

    // insert passed text; if the span element floats out of its parent to the left, is is right-to-left
    bidiTextNode.nodeValue = text;
    return bidiChildSpan.offsetLeft < 0;
}

// interface DigitFontMeasures ================================================

/**
 * Font measurement settings for the digit characters `0` to `9`.
 */
export interface DigitFontMeasures {

    /**
     * The widths of the digits `0` to `9`, in pixels.
     */
    widths: number[];

    /**
     * The minimum width of any of the digits, in pixels.
     */
    minWidth: number;

    /**
     * The maximum width of any of the digits.
     */
    maxWidth: number;

    /**
     * The digit with the smallest width; or `0`, if all digits have the same
     * width.
     */
    minDigit: number;

    /**
     * The digit with the largest width; or `0`, if all digits have the same
     * width.
     */
    maxDigit: number;
}

// class Font =================================================================

/**
 * Helper structure used to transport font information, and to receive
 * specific font measurements, such as the normal line height, the position
 * of the font base line, or the exact width of arbitrary text.
 *
 * @param family
 *  The CSS font family (case-independent), including all known fall-back
 *  fonts, separated with commas.
 *
 * @param size
 *  The font size, in points.
 *
 * @param [bold]
 *  Whether the font is set to bold character style.
 *
 * @param [italic]
 *  Whether the font is set to italic characters style.
 */
export class Font implements Cloneable<Font> {

    // properties -------------------------------------------------------------

    /**
     * The CSS font family, including all known fall-back fonts, separated with
     *  commas, with lower-case characters.
     */
    family: string;

    /**
     * The font size, in points, rounded to 1/10 of points. Note that the unit
     * string "pt" has to be added to this number when setting this font size
     * as CSS property correctly.
     */
    size: number;

    /**
     * Whether the font is set to bold character style.
     */
    bold: boolean;

    /**
     * Whether the font is set to italic character style.
     */
    italic: boolean;

    // constructor ------------------------------------------------------------

    constructor(family: string, size: number, bold = false, italic = false) {
        this.family = family.toLowerCase();
        this.size = math.roundp(size, 0.1);
        this.bold = bold;
        this.italic = italic;
    }

    // public getters ---------------------------------------------------------

    /**
     * Returns a unique string key for this font that can be used for example
     * as key in an associative map.
     *
     * @returns
     *  A unique string key for this font instance.
     */
    get key(): string {
        return `${this.family}|${this.size}|${(this.bold ? 1 : 0) + (this.italic ? 2 : 0)}`;
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a clone of this font instance.
     *
     * @returns
     *  A clone of this font instance.
     */
    clone(): Font {
        return new Font(this.family, this.size, this.bold, this.italic);
    }

    /**
     * Converts the font settings in this instance to a string suitable to be
     * set as font style at an HTML `<canvas>` element.
     *
     * @returns
     *  The font settings of this instance, as HTML `<canvas>` font style.
     */
    getCanvasFont(): string {
        return `${this.bold ? "bold " : ""}${this.italic ? "italic " : ""}${this.size}pt ${this.family}`;
    }

    /**
     * Calculates the normal line height for this font, as reported by the
     * current browser. Note that this value may vary for the same font in
     * different browsers.
     *
     * @returns
     *  The normal line height for this font, in pixels (with a precision of
     *  1/10 pixel for browsers using fractional line heights internally).
     */
    getNormalLineHeight(): number {
        return calculateNodeMetrics(this, lineHeightNode, node => node.offsetHeight / 10, "lineheight");
    }

    /**
     * Calculates the position of the font base line for the specified line
     * height, as reported by the current browser. Note that this value may
     * vary for the same font in different browsers.
     *
     * @param [lineHeight]
     *  The line height to be used to determine the position of the font base
     *  line. If omitted, the normal line height, as reported by the method
     *  `getNormalLineHeight()`, will be used.
     *
     * @returns
     *  The position of the font base line for the specified line height, in
     *  pixels, relative to the top border of a text line.
     */
    getBaseLineOffset(lineHeight?: number): number {

        // CSS line height with unit, or 'normal'; also used as cache key
        const cssLineHeight = is.number(lineHeight) ? `${round(lineHeight)}px` : "normal";

        return calculateNodeMetrics(this, baseLineNode, node => {
            node.style.lineHeight = cssLineHeight;
            return baseLineChildSpan.offsetTop;
        }, `baseline.${cssLineHeight}`);
    }

    /**
     * Returns the exact width of the passed text, in pixels.
     *
     * @param text
     *  The text whose width will be calculated. If the text consists of one
     *  character only, the calculated text width will be cached internally for
     *  subsequent invocations with the same font settings.
     *
     * @returns
     *  The exact width of the passed text, in pixels.
     */
    getTextWidth(text: string): number {

        switch (text.length) {
            case 0:
                // empty string: always zero
                return 0;

            case 1:
                // single character: calculate width of 10 characters for higher precision, store in cache
                return calculateCanvasMetrics(this, context => context.getCharacterWidth(text), `textwidth.${text}`);

            default:
                // multiple characters: calculate text width, do not store in cache
                return getTextWidth(this, text);
        }
    }

    /**
     * Returns font measurement settings for the digits `0` to `9`.
     *
     * @returns
     *  The font measurement settings of the digits for this font.
     */
    getDigitMeasures(): DigitFontMeasures {
        return calculateCanvasMetrics(this, context => {
            const widths = DIGITS.map(digit => context.getCharacterWidth(digit));
            const minWidth = min(...widths);
            const maxWidth = max(...widths);
            const minDigit = widths.indexOf(minWidth);
            const maxDigit = widths.indexOf(maxWidth);
            return { widths, minWidth, maxWidth, minDigit, maxDigit };
        }, "digits");
    }

    /**
     * Splits the passed string to text lines that all fit into the specified
     * pixel width.
     *
     * @param text
     *  The text that will be split into single text lines.
     *
     * @param width
     *  The maximum width of the text lines, in pixels.
     *
     * @returns
     *  An array of strings containing the single text lines, all fitting into
     *  the specified text line width.
     */
    getTextLines(text: string, width: number): string[] {

        // empty strings, or single-character strings cannot be split
        if (text.length <= 1) { return [text]; }

        // use a DOM helper node to take advantage of text auto-wrapping provided by the browser
        return calculateNodeMetrics(this, textLinesNode, node => {

            // the resulting substrings
            const textLines: string[] = [];

            // set the passed width at the helper node
            node.style.width = `${width}px`;

            // create the HTML mark-up, wrap each character into its own span element
            // (for performance: with this solution, the DOM will be modified once,
            // and all node offsets can be read without altering the DOM again)
            node.innerHTML = escapeHTML(text).replace(/&(#\d+|\w+);|./g, "<span>$&</span>");

            // all DOM span elements
            const spanNodes = node.childNodes as NodeListOf<HTMLSpanElement>;
            // shortcut to the number of spans
            const spanCount = spanNodes.length;
            // start index of the current text line
            let lineIndex = 0;
            // vertical position of the spans in the current text line
            let lineOffset = spanNodes[0].offsetTop;
            // vertical position of the spans in the last text line
            const maxOffset = spanNodes[spanCount - 1].offsetTop;
            // predicate function for `fastFindFirstIndex`
            const findPredicate = (span: HTMLSpanElement): boolean => lineOffset < span.offsetTop;

            // process all text lines but the last text line
            while (lineOffset < maxOffset) {

                // index of the first span element in the next text line
                const nextIndex = ary.fastFindFirstIndex(spanNodes, findPredicate, lineIndex);

                // append the text of the current text line to the result array
                textLines.push(text.slice(lineIndex, nextIndex));

                // continue with the next text line
                lineIndex = nextIndex;
                lineOffset = spanNodes[lineIndex].offsetTop;
            }

            // push the remaining last text line to the result array
            if (lineIndex < text.length) {
                textLines.push(text.slice(lineIndex));
            }

            return textLines;
        });
    }

    /**
     * Provides the ability to calculate and cache custom font metrics.
     *
     * @param callback
     *  The callback function that will be invoked after preparing the helper
     *  DOM node. Receives the helper DOM element that can be used to calculate
     *  the result. The return value of this function will be cached if
     *  specified (see parameter `cacheKey` below), and will be returned by
     *  this method.
     *
     * @param [cacheKey]
     *  If specified, a unique map key for the font metrics cache that will be
     *  checked first. An existing cache entry will be returned without
     *  invoking the callback function. Otherwise, the new value returned by
     *  the callback function will be inserted into the cache. If omitted, the
     *  callback function will always be invoked, and the result will not be
     *  inserted into the cache.
     *
     * @returns
     *  The font metric property from the cache, or returned by the callback.
     */
    getCustomFontMetrics<T>(callback: (node: HTMLDivElement) => T, cacheKey?: string): T {
        if (cacheKey) { cacheKey = `custom:${cacheKey}`; }
        return calculateNodeMetrics(this, customMetricsNode, node => {
            const result = callback(node);
            detachChildren(node);
            node.removeAttribute("style");
            return result;
        }, cacheKey);
    }
}
