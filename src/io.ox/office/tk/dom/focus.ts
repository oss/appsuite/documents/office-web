/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { is } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import { globalLogger, Logger } from "@/io.ox/office/tk/utils/logger";

import type { NodeOrJQuery, ContainsNodeOptions } from "@/io.ox/office/tk/dom/domutils";
import { toNode, toJQuery, containsNode, insertHiddenNodes } from "@/io.ox/office/tk/dom/domutils";

// types ======================================================================

/**
 * Type of elements that can retrieve browser focus.
 *
 * The interface `HTMLOrSVGElement` on its own is only a helper mixin type that
 * does not extend the `Element` interface.
 */
export type FocusableElement = Element & HTMLOrSVGElement;

/**
 * Optional parameters for the function `setFocus()`.
 */
export interface SetFocusOptions {

    /**
     * If set to `true`, a global focus event will be emitted immediately after
     * focusing the specified element, regardless of the current focused
     * element. Default value is `false`.
     */
    forceEvent?: boolean;
}

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted after "focusin" and "focusout" events have bubbled
         * up to the document root node.
         *
         * @param focusNode
         *  The DOM element currently focused.
         *
         * @param blurNode
         *  The DOM element that was focused before.
         *
         * @param keyEvent
         *  The "keydown" DOM event that caused changing the browser focus.
         */
        "change:focus": [
            focusNode: FocusableElement | null,
            blurNode: FocusableElement | null,
            keyEvent: KeyboardEvent | null
        ];
    }
}

// constants ==================================================================

// CSS selector for DOM elements that can receive the browser focus
const FOCUSABLE_SELECTOR = "button,input,textarea,select,a[href],[tabindex]";

// exclusion list for focusable DOM elements
const FOCUSABLE_FILTER = ':visible:not([tabindex^="-"])';

// a helper node to be used to hide the soft-keyboard on mobile devices
const hiddenFocusNode = insertHiddenNodes($('<div id="io-ox-office-temp-focus-target" tabindex="-1">'))[0];

// singleton class ChangeFocusLogger ==========================================

/**
 * A special logger for DOM focus change events, bound to the debug
 * configuration flag "office:log-focus".
 */
class ChangeFocusLogger extends Logger {

    constructor() {
        super("office:log-focus", { tag: "FOCUS", tagColor: 0x00FFFF });
    }

    /**
     * Logs a new focus event.
     *
     * @param context
     *  The message prefix to be printed in front of the target node.
     *
     * @param target
     *  The focused target node.
     */
    logTarget(context: string, target: unknown): void {
        this.log(() => [`context="${context}" target=`, target || "<none>", "active=", getFocus() ?? "<none>"]);
    }
}

/**
 * A special logger for DOM focus events, bound to the debug configuration flag
 * "office:log-focus".
 */
export const focusLogger = new ChangeFocusLogger();

// focus change tracker =======================================================

{
    // the DOM node currently focused
    let focusNode: FocusableElement | null = null;
    // whether the last received event was a "focusout" event
    let pendingFocusOut = false;
    // last "keydown" event received in this stack frame
    let lastKeyEvent: KeyboardEvent | null = null;

    // updates the current focus node, and emits the global focus event
    const updateFocus = (target: unknown): void => {
        const blurNode = focusNode;
        // ignore "focusin" events on the document root (sent by Firefox when returning focus to browser tab)
        if (isFocusableElement(target) && (focusNode !== target)) {
            focusNode = target;
            globalEvents.emit("change:focus", focusNode, blurNode, lastKeyEvent);
        }
    };

    // save the "keydown" events to be passed to the handlers (listen to "keydown" events in the capturing phase,
    // otherwise custom focus handlers will be executed before the "keydown" event bubbles up to the document node)
    document.addEventListener("keydown", keyEvent => {
        lastKeyEvent = keyEvent;
        window.setTimeout(() => { lastKeyEvent = null; }, 0);
    }, { capture: true });

    // listen to the global bubbling "focusin" event
    document.addEventListener("focusin", event => {
        focusLogger.logTarget("document:focusin", event.target);
        pendingFocusOut = false;
        updateFocus(event.target);
    });

    // listen to the global bubbling "focusout" event
    document.addEventListener("focusout", event => {
        focusLogger.logTarget("document:focusout", event.target);
        // ignore "focusout" event whose target does not match the current focus node
        if (event.target === focusNode) {
            pendingFocusOut = true;
            window.setTimeout(() => {
                if (pendingFocusOut) {
                    pendingFocusOut = false;
                    updateFocus(null);
                }
            }, 0);
        }
    });

    // log all focus events (also fired from outside)
    globalEvents.on("change:focus", node => focusLogger.logTarget("change:focus", node));

    // initialize current focus node by calling the 'focusin' event handler directly
    updateFocus(getFocus());
}

// private functions ==========================================================

/**
 * Sets the focus to the specified element, and writes a message to the global
 * focus logger.
 */
function implSetFocus(element: FocusableElement): void {
    focusLogger.logTarget("setFocus()", element);
    element.focus();
}

// public functions ===========================================================

/**
 * Returns whether the passed value is a focusable DOM element, i.e. an element
 * with a `focus` method.
 *
 * @param value
 *  The value to be checked.
 *
 * @returns
 *  Whether the passed value is a focusable DOM element.
 */
export function isFocusableElement(value: unknown): value is FocusableElement {
    return (value instanceof Element) && ("focus" in value) && is.function(value.focus);
}

/**
 * Returns the active (focused) element of the HTML document. Some browsers
 * throw when accessing the uninitialized property `document.activeElement`.
 * This function wraps a try/catch around the property access.
 *
 * @returns
 *  The active (focused) element of the HTML document; or `null` if no active
 *  element is available.
 */
export function getFocus(): FocusableElement | null {
    // Edge throws if this property will be accessed when it is empty
    try { return document.activeElement as FocusableElement | null; } catch { }
    return null;
}

/**
 * Sets the browser focus to the specified DOM element.
 *
 * @param elem
 *  The DOM element to be focused. If nullish, nothing will happen.
 */
export function setFocus(elem: NodeOrJQuery<FocusableElement>, options?: SetFocusOptions): void {
    const element = toNode(elem);
    if (element) {
        implSetFocus(element);
        if (options?.forceEvent) {
            globalEvents.emit("change:focus", element, element, null);
        }
    }
}

/**
 * Sets the browser focus to a hidden DOM element that is not editable. Can be
 * used to explicitly hide the soft-keyboard on mobile devices.
 */
export function hideFocus(): void {
    implSetFocus(hiddenFocusNode);
}

/**
 * Returns whether the specified node is the active (focused) element of the
 * HTML document. Some browsers throw when accessing the uninitialized property
 * `document.activeElement`. This function wraps a try/catch around the
 * property access.
 *
 * @param elem
 *  A DOM element, or a JQuery collection (only its first element will be
 *  used).
 *
 * @returns
 *  Whether the specified node is the active (focused) element of the HTML
 *  document.
 */
export function isFocused(elem: NodeOrJQuery<FocusableElement>): boolean {
    return getFocus() === toNode(elem);
}

/**
 * Returns whether one of the descendant nodes in the passed element contains
 * the active (focused) element.
 *
 * @param elem
 *  A DOM element, or a JQuery collection (only its first element will be
 *  used). The descendants will be checked for the active (focused) element.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  Whether one of the descendants of the passed element contains the active
 *  (focused) element.
 */
export function containsFocus(elem: NodeOrJQuery<FocusableElement>, options?: ContainsNodeOptions): boolean {
    return containsNode(elem, getFocus(), options);
}

/**
 * Returns whether the passed element can receive the browser focus.
 *
 * By convention, all focusable DOM elements MUST have a non-negative value in
 * their attribute "tabindex" (also elements which are natively focusable, such
 * as `<input>` elements of type "text").
 *
 * @param elem
 *  A DOM element, or a JQuery collection (only its first element will be
 *  used).
 *
 * @returns
 *  Whether the passed element can receive the browser focus.
 */
export function isFocusable(elem: NodeOrJQuery<FocusableElement>): boolean {
    // convert to single-element JQuery collection
    const $node = toJQuery(elem, true);
    // check for the "tabindex" attribute (must exist, must not be negative)
    // check visibility (via JQuery plug-in ":visible")
    return ($node.length === 1) && $node.is(FOCUSABLE_SELECTOR) && $node.is(FOCUSABLE_FILTER);
}

/**
 * Returns all focusable control elements contained in the specified DOM
 * elements.
 *
 * @param elem
 *  A DOM element, or a non-empty JQuery object.
 *
 * @returns
 *  A JQuery collection with all focusable descendant elements.
 */
export function findFocusable(elem: NodeOrJQuery<FocusableElement>): JQuery {
    // first, filter all descendants for the "tabindex" attribute
    // second, filter these nodes for JQuery's ":visible" pseudo-class (performance)
    return toJQuery(elem, true).find(FOCUSABLE_SELECTOR).filter(FOCUSABLE_FILTER);
}

/**
 * Sets the browser focus to the first focusable element contained in the
 * specified DOM element.
 *
 * @param elem
 *  The DOM element to be focused, as DOM node or non-empty JQuery object.
 *
 * @param [options]
 *  Optional parameters.
 */
export function setFocusInto(elem: NodeOrJQuery<FocusableElement>, options?: SetFocusOptions): void {
    setFocus(findFocusable(elem), options);
}

/**
 * Clears the current browser selection ranges.
 */
export function clearBrowserSelection(): void {
    try {
        window.getSelection()?.removeAllRanges();
    } catch (err) {
        globalLogger.exception(err);
    }
}
