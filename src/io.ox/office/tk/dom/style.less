/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

@import "@/io.ox/office/tk/css/mixins";
@import "@/io.ox/office/tk/css/variables";

/* ========================================================================= */
/* definitions for application DOM tree                                      */
/* ========================================================================= */

/* restrict all definitions to OX Documents applications */
.io-ox-office-main {

    // Overrides for Dark Theme ===============================================

    :root.dark & {
        --restrained-overlay-color: hsla(var(--shade-base), 5%);
        --hover-overlay-color: hsla(var(--shade-base), 9%);
        --border-overlay-color: hsla(var(--shade-base), 12%);
        --control-selected-text-color: white; // "--selected" is too dark
        --menu-background: var(--background-50); // brighter background in dark mode like core popups
        --listmenu-selected-fill-color: var(--background-300);
        --listmenu-framed-selected-border-color: var(--accent-200);
        --text-field-background: var(--background-100);
    }

    // Generic Helpers ========================================================

    // generic class to hide (collapse) an element
    .hidden {
        display: none !important;
    }

    // helper class to let an element rotate endlessly
    .do-spin {

        @keyframes spinner {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        animation: spinner 2s linear 0s infinite normal;
    }

    // common settings for contact picture elements (as background image)
    .contact-picture {
        flex: 0 0 auto;
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        background-color: fade(white, 50%); // DOCS-4166: light background for all themes (fallback image is translucent gray)
        box-shadow: 0 0 5px var(--border-overlay-color);
        border-radius: 50%;
        overflow: hidden;
    }

    // Icons ==================================================================

    [data-icon-id] {
        flex: 0 0 auto;
    }

    .docs-txt-icon {
        min-width: 18px;
        font-size: 1.15em;
        font-style: normal;
        font-weight: bold;
        text-align: center;
    }

    // Generic Push Buttons ===================================================

    button {
        position: relative;
        gap: var(--caption-spacing);

        &:disabled {
            pointer-events: none;
        }

        // generic additions for text labels
        > .label-node {
            flex: 1 1 auto;
            .text-overflow-hidden();
        }

        // generic additions for caret symbols
        > .caret-node {
            display: flex;
            flex: 0 0 auto;
            opacity: 0.5;

            // the caret icon element
            > [data-icon-id] {
                width: var(--caret-width);
                height: var(--caret-width);
                align-self: center;
            }
        }

        // special settings for custom borderless buttons
        &.btn-bare {
            background: none;
            color: var(--link);

            &:not(:disabled) {
                .hover-overlay(@custom-rules: {
                    border-color: var(--hover-overlay-color);
                    color: var(--link-hover);
                });
            }

            .focus-border-shadow();
            &:active {
                outline: none;
                box-shadow: none;
            }
        }

        // special "Delete" button in lists
        &.btn-delete {
            background: none;
            color: var(--link);

            // hover effect: white on red (like in Core UI, no variables available)
            &:not(:disabled) {
                &:hover, &:active {
                    background: #c00;
                    color: white;
                }
                &:active {
                    outline: none;
                }
            }
        }
    }

    // Control Captions (Icon and Text) =======================================

    // common settings for caption elements (icon and text in buttons and labels)
    a.btn, .group {

        svg.bi {
            flex: 0 0 auto;
        }

        > .caption {
            position: relative; // e.g. together with absolute background element
            display: flex;
            flex-grow: 1;
            align-items: center;
            min-width: 18px;

            // do not show padding whitespace if nothing is inside the element
            &:empty { display: none; }

            // give SVG icons a fixed size
            > svg.bi {
                width: 18px;
                height: 18px;
                padding: 1px;

                &.small-icon {
                    width: 14px;
                    height: 14px;
                }
            }

            // text label <span> element occupies available space in caption
            > span {
                flex-grow: 1;
            }

            &:not(.wrap-text) > span {
                .text-overflow-hidden();
            }

            &.wrap-text > span {
                line-height: normal;
                white-space: normal;
            }

            // standard distance between child elements
            > :not(:first-child) {
                margin-left: var(--caption-spacing);
            }
        }

        // add distance to preceding contents
        > :not(.abs) + .caption {
            margin-left: var(--caption-spacing);
        }

        // bare dropdown caret buttons
        &.caret-button > .caption:empty {
            display: none;
        }
    }

    // Button Elements ========================================================

    // button elements inside controls, lists, popup menus, etc.
    a.btn {

        // prevent system pop-up when tapping and holding button on iPad/iPhone
        -webkit-touch-callout: none;

        // prevent built-in focus styling
        .focus-no-outline();
        &:active { box-shadow: none; }

        // there are problems on touch devices with bubbling the events,
        // it's more solid when only the anchor itself reacts on touch
        * { pointer-events: none; }

        // custom bitmap icons
        > .bmp-icons {
            display: flex;
            flex: 0 0 auto;
            align-items: center;
        }

        // hide one bitmap icon according to selection state
        &:not(.selected) > .bmp-icons > img:not(.for-unselected) { display: none; }
        &.selected > .bmp-icons > img:not(.for-selected) { display: none; }

        // background for ambiguous toggle buttons
        &[role="button"]:not(.selected).ambiguous {
            background-color: fade(black, 2%);
            background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAK3RFWHRDcmVhdGlvbiBUaW1lAERpIDE0IEphbiAyMDE0IDIxOjQ2OjUyICswMTAw9OL4PQAAAAd0SU1FB94BDhQvMWGyuocAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAEZ0FNQQAAsY8L/GEFAAAAPElEQVR42mNgIAAYQURaWpokkFID4luzZs16jqyABUqDJB2gbKwKbqHRxANGbILIbmLBoRHuJlwK4G4CAPIbDHnTEBvvAAAAAElFTkSuQmCC");
            background-size: 4px 4px;
        }
    }

    // Checkboxes, Radio Buttons ==============================================

    // generic design for checkboxes and radio buttons, e.g. in dialogs
    a.btn.custom-checkmark {
        display: inline-flex;
        align-items: center;
        position: relative;
        padding-left: 0;
        padding-right: 0;
        border-radius: 0;

        // checkmark/radiomark design according to CoreUI
        > .checkmark {
            .flex-center();
            width: 16px;
            height: 16px;

            // increased distance to caption icon/label
            + .caption {
                margin-left: var(--distance-h);
            }
        }

        // specific styling for checkboxes (flat design without box)
        &:not(.boxed-mode) > .checkmark {

            > [data-icon-id] {
                width: 1.5rem;
                height: 1.5rem;
            }

            // hide checkmark for unchecked state
            &[data-state="false"] > [data-icon-id] {
                display: none;
            }
        }

        // basic formatting for boxed checkmarks and radiomarks
        &.boxed-mode > .checkmark {
            background: var(--background);
            border: 1px solid var(--border);
        }

        // focus effect around checkmark
        &.boxed-mode:focus:not(:active) > .checkmark {
            .focus-border-shadow-effect() !important;
        }

        // specific styling for checkboxes (boxed design as in CoreUI)
        &:not([role="radio"]).boxed-mode > .checkmark {
            border-radius: 4px;

            // checked state
            &[data-state="true"] {
                background: var(--checkbox-svg);
                background-color: var(--accent);
                border: none;
            }

            // ambiguous state
            &[data-state="null"] {
                background: var(--checkbox-ambiguous-svg);
                background-color: var(--border-bright);
            }
        }

        // specific styling for radio buttons (boxed design as in CoreUI)
        &[role="radio"].boxed-mode > .checkmark {
            border-radius: 50%;

            // checked state
            &[data-state="true"] {
                background: var(--radio-svg);
                background-color: var(--accent);
                border: none;
            }
        }
    }

    // Text Input Elements ====================================================

    // no native styling for <input> and <textarea> elements on iPad/iPhone
    input, textarea {
        -webkit-appearance: none;
        background-color: var(--text-field-background);
    }

    // standard styling of placeholders in input fields
    input::placeholder, textarea::placeholder, [data-placeholder]::before {
        font-size: 0.9em;
        font-style: italic;
        color: var(--text-gray);
        opacity: 0.7;
    }

    // custom placeholder
    [data-placeholder]:empty::before {
        content: attr(data-placeholder);
    }

    // List Select Elements ===================================================

    select {
        overflow: auto;
        background-color: var(--text-field-background);
        border: 1px solid var(--border);

        // fix mouse pointer style
        cursor: pointer;
        &:disabled { cursor: not-allowed; }

        // expanded multi-line design (no dropdown list)
        &[size]:not([size="1"]) {
            padding: 0;
            background-image: none; // bootstrap dropdown caret
        }

        option {
            padding: var(--control-distance) var(--control-padding);
            position: relative; // for hover overlay effect
            .hover-overlay();

            // override selection color (not supported in all browsers though)
            &:checked {
                .list-selected-colors();
            }
        }
    }
}

/* ========================================================================= */
/* global DOM elements                                                       */
/* ========================================================================= */

// global hidden DOM storage
#io-ox-office-temp {
    position: absolute;
    top: -100%;
    bottom: 100%;
    left: 0;
    right: 0;
    overflow: hidden;

    > .font-metrics-helper {
        position: relative;
        display: inline-block;
        width: auto;
        margin: 0;
        padding: 0;
        border: none;
        line-height: normal;
        white-space: pre;
    }
}
