/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

declare global {

    interface Element {
        webkitRequestFullscreen(options?: FullscreenOptions): void | Promise<void>;
    }

    interface Document {
        readonly webkitFullscreenElement: Element | null;
        webkitExitFullscreen(): void | Promise<void>;
        readonly webkitFullscreenEnabled: boolean;
    }

}

/**
 * Using the Fullscreen API to check, whether the full screen mode can be activated.
 *
 * @returns
 *  A Boolean value which is true if the document and the elements within can be placed
 *  into full-screen mode by calling Element.requestFullscreen(). If full-screen mode
 *  is not available, this value is false.
 */
export function isFullscreenEnabled(): boolean {
    return document.fullscreenEnabled || document.webkitFullscreenEnabled;
}

/**
 * Using the Fullscreen API to check, whether there is an element in full screen mode.
 *
 * @returns
 *  A Boolean value which is true if there is an element in fullscreen mode.
 */
export function isFullscreenActive(): boolean {
    return !!document.fullscreenElement || !!document.webkitFullscreenElement;
}

/**
 * Using the Fullscreen API to receive that HTML element, that is currently being
 * presented in full screen mode. Or null, if full-screen mode is currently not in
 * use.
 *
 * @returns
 *  The node that is currently in full screen mode; or `null`, if full-screen mode is
 *  currently not in use.
 */
export function getFullscreenElement(): Element | null {
    return document.fullscreenElement ?? document.webkitFullscreenElement;
}

/**
 * Entering the full screen mode for the specified element using the Fullscreen API.
 *
 * @param element
 *  The node that will be set to full screen mode.
 *
 * @returns
 *  A promise that will be resolved when the Fullscreen API successfully entered the
 *  full screen mode. If this fails, the promise is rejected. If the Fullscreen API
 *  is not used, a rejected promise is returned.
 */
export async function enterFullscreen(element: Element): Promise<void> {

    if (element.requestFullscreen) {
        await element.requestFullscreen();
    } else if (element.webkitRequestFullscreen) {
        await element.webkitRequestFullscreen();
    } else {
        throw new Error("Fullscreen API not found!");
    }
}

/**
 * Exiting the full screen mode using the Fullscreen API.
 *
 * @returns
 *  A promise that will be resolved when the Fullscreen API successfully exited the
 *  full screen mode. If this fails, the promise is rejected. If the Fullscreen API
 *  is not used, a rejected promise is returned.
 */
export async function exitFullscreen(): Promise<void> {

    if (document.exitFullscreen) {
        await document.exitFullscreen();
    } else if (document.webkitExitFullscreen) {
        await document.webkitExitFullscreen();
    } else {
        throw new Error("Fullscreen API not found!");
    }
}

/**
 * Toggle the fullscreen mode for the specified element using the Fullscreen API.
 *
 * Info:
 * For entering the full screen mode, this function can only be called from a user-initiated
 * event (click, key, or touch event), otherwise the browser will deny the request.
 *
 * @param element
 *  The element to be toggled.
 *
 * @param [state]
 *  Starting, leaving or toggling full screen mode. A state of 'true' starts full screen
 *  mode, 'false' exits the full screen mode and 'undefined' toggles the full screen state.
 *
 * @returns
 *  A promise that will be resolved when the Fullscreen API successfully toggled the full
 *  screen mode. If this fails, the promise is rejected. If the Fullscreen API is not used,
 *  a rejected promise is returned.
 */
export async function toggleFullscreen(element: Element, state?: boolean): Promise<void> {
    if (state === undefined) { state = !isFullscreenActive(); }
    await (state ? enterFullscreen(element) : exitFullscreen());
}

/**
 * Registering a function for the "fullscreenchange" event.
 *
 * @param element
 *  The node at which the handler will be registered.
 *
 * @param handler
 *  The handler for the event "fullscreenchange".
 */
export function addChangeFullscreenHandler(element: Element, handler: EventListenerOrEventListenerObject): void {
    ["fullscreenchange", "webkitfullscreenchange"].forEach(
        eventType => element.addEventListener(eventType, handler)
    );
}

/**
 * Removing the handler for the "fullscreenchange" event.
 *
 * @param element
 *  The node at which the handler will be registered.
 *
 * @param handler
 *  The handler for the event "fullscreenchange".
 */
export function removeChangeFullscreenHandler(element: Element, handler: EventListenerOrEventListenerObject): void {
    ["fullscreenchange", "webkitfullscreenchange"].forEach(
        eventType => element.removeEventListener(eventType, handler)
    );
}
