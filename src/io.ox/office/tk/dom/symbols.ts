/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * The replacement data for a symbol character.
 */
export interface SymbolChar {

    /**
     * The Unicode replacement character for a symbol character.
     */
    char: string;

    /**
     * Whether rendering with a font with serifs is preferred.
     */
    serif: boolean;
}

// private types --------------------------------------------------------------

// literal configuration for a symbol character (as array to reduce code size)
// - code: the code point in the symbol font
// - char: the Unicode replacement character
// - serif: whether to prefer rendering the character with serifs
type SymbolCharConfig = [code: number, char: string, serif?: boolean];

// shadow areas with repeated symbol characters
interface SymbolShadowConfig {
    first: number;
    last: number;
    offset: number;
}

// literal configuration for a symbol font
interface SymbolFontConfig {
    fontNames: string[];
    unicode?: boolean;
    symbols: readonly SymbolCharConfig[];
    shadows?: readonly SymbolShadowConfig[];
}

// constants ==================================================================

const SYMBOL_FONT_CONFIGS: SymbolFontConfig[] = [{
    fontNames: ["symbol"],
    symbols: [
        [0x20, " "],  // Space
        [0x21, "!", true],
        [0x22, "\u2200"],  // For all
        [0x23, "#", true],
        [0x24, "\u2203"],  // There exists
        [0x25, "%", true],
        [0x26, "&", true],
        [0x27, "\u220b"],  // Contains as member
        [0x28, "(", true],
        [0x29, ")", true],
        [0x2a, "\u2217", true],  // Asterisk
        [0x2b, "+", true],  // Plus sign
        [0x2c, ",", true],  // Comma
        [0x2d, "\u2212", true],  // Minus sign
        [0x2e, ".", true],  // Full stop
        [0x2f, "/", true],  // Solidus
        [0x30, "0", true],  // Digit zero
        [0x31, "1", true],  // Digit one
        [0x32, "2", true],  // Digit two
        [0x33, "3", true],  // Digit three
        [0x34, "4", true],  // Digit four
        [0x35, "5", true],  // Digit five
        [0x36, "6", true],  // Digit six
        [0x37, "7", true],  // Digit seven
        [0x38, "8", true],  // Digit eight
        [0x39, "9", true],  // Digit nine
        [0x3a, ":", true],  // Colon
        [0x3b, ";", true],  // Semicolon
        [0x3c, "<", true],  // Less-than sign
        [0x3d, "=", true],  // Equals sign
        [0x3e, ">", true],  // Greater-than sign
        [0x3f, "?", true],  // Question mark
        [0x40, "\u2245", true],  // Approximately equal to
        [0x41, "\u0391", true],  // Greek capital letter alpha
        [0x42, "\u0392", true],  // Greek capital letter beta
        [0x43, "\u03a7", true],  // Greek capital letter chi
        [0x44, "\u0394", true],  // Greek capital letter delta
        [0x45, "\u0395", true],  // Greek capital letter epsilon
        [0x46, "\u03a6", true],  // Greek capital letter phi
        [0x47, "\u0393", true],  // Greek capital letter gamma
        [0x48, "\u0397", true],  // Greek capital letter eta
        [0x49, "\u0399", true],  // Greek capital letter iota
        [0x4a, "\u03d1", true],  // Greek theta symbol
        [0x4b, "\u039a", true],  // Greek capital letter kappa
        [0x4c, "\u039b", true],  // Greek capital letter lambda
        [0x4d, "\u039c", true],  // Greek capital letter mu
        [0x4e, "\u039d", true],  // Greek capital letter nu
        [0x4f, "\u039f", true],  // Greek capital letter omicron
        [0x50, "\u03a0", true],  // Greek capital letter pi
        [0x51, "\u0398", true],  // Greek capital letter theta
        [0x52, "\u03a1", true],  // Greek capital letter rho
        [0x53, "\u03a3", true],  // Greek capital letter sigma
        [0x54, "\u03a4", true],  // Greek capital letter tau
        [0x55, "\u03a5", true],  // Greek capital letter upsilon
        [0x56, "\u03c2", true],  // Greek small letter final sigma
        [0x57, "\u03a9", true],  // Greek capital letter omega
        [0x58, "\u039e", true],  // Greek capital letter xi
        [0x59, "\u03a8", true],  // Greek capital letter psi
        [0x5a, "\u0396", true],  // Greek capital letter zeta
        [0x5b, "[", true],  // Left square bracket
        [0x5c, "\u2234", true],  // Therefore
        [0x5d, "]", true],  // Right square bracket
        [0x5e, "\u22a5", true],  // Up tack
        [0x5f, "_", true],  // Low line
        [0x60, "\xaf", true],  // Macron
        [0x61, "\u03b1", true],  // Greek small letter alpha
        [0x62, "\u03b2", true],  // Greek small letter beta
        [0x63, "\u03c7", true],  // Greek small letter chi
        [0x64, "\u03b4", true],  // Greek small letter delta
        [0x65, "\u03b5", true],  // Greek small letter epsilon
        [0x66, "\u03d5", true],  // Greek phi symbol
        [0x67, "\u03b3", true],  // Greek small letter gamma
        [0x68, "\u03b7", true],  // Greek small letter eta
        [0x69, "\u03b9", true],  // Greek small letter iota
        [0x6a, "\u03c6", true],  // Greek small letter phi
        [0x6b, "\u03ba", true],  // Greek small letter kappa
        [0x6c, "\u03bb", true],  // Greek small letter lamda
        [0x6d, "\u03bc", true],  // Greek small letter mu
        [0x6e, "\u03bd", true],  // Greek small letter nu
        [0x6f, "\u03bf", true],  // Greek small letter omicron
        [0x70, "\u03c0", true],  // Greek small letter pi
        [0x71, "\u03b8", true],  // Greek small letter theta
        [0x72, "\u03c1", true],  // Greek small letter rho
        [0x73, "\u03c3", true],  // Greek small letter sigma
        [0x74, "\u03c4", true],  // Greek small letter tau
        [0x75, "\u03c5", true],  // Greek small letter upsilon
        [0x76, "\u03d6", true],  // Greek pi symbol
        [0x77, "\u03c9", true],  // Greek small letter omega
        [0x78, "\u03be", true],  // Greek small letter xi
        [0x79, "\u03c8", true],  // Greek small letter psi
        [0x7a, "\u03b6", true],  // Greek small letter zeta
        [0x7b, "{", true],  // Left curly bracket
        [0x7c, "|", true],  // Vertical line
        [0x7d, "}", true],  // Right curly bracket
        [0x7e, "\u223c", true],  // Tilde operator
        [0xa0, "\u20ac", true],  // Euro sign
        [0xa1, "\u03d2", true],  // Greek upsilon with hook symbol
        [0xa2, "\u2032", true],  // Prime
        [0xa3, "\u2264", true],  // Less-than or equal to
        [0xa4, "\u2044", true],  // Fraction slash
        [0xa5, "\u221e", true],  // Infinity
        [0xa6, "\u0192", true],  // Latin small letter f with hook
        [0xa7, "\u2663", true],  // Black club suit
        [0xa8, "\u2666", true],  // Black diamond suit
        [0xa9, "\u2665", true],  // Black heart suit
        [0xaa, "\u2660", true],  // Black spade suit
        [0xab, "\u2194", true],  // Left rigth arrow
        [0xac, "\u2190", true],  // Leftwards arrow
        [0xad, "\u2191", true],  // Upwards arrow
        [0xae, "\u2192", true],  // Rigthwards arrow
        [0xaf, "\u2193", true],  // Downwards arrow
        [0xb0, "\xb0", true],  // Degree sign
        [0xb1, "\xb1", true],  // Plus-minus sign
        [0xb2, "\u2033", true],  // Double prime
        [0xb3, "\u2265", true],  // Greater-than or equal to
        [0xb4, "\xd7", true],  // Multiplication sign
        [0xb5, "\u221d", true],  // Proportional to
        [0xb6, "\u2202", true],  // Partial differential
        [0xb7, "\u2022"],  // Bullet
        [0xb8, "\xf7", true],  // Division sign
        [0xb9, "\u2260", true],  // Not equal to
        [0xba, "\u2261", true],  // Identical to
        [0xbb, "\u2248", true],  // Almost equal sign
        [0xbc, "\u2026", true],  // Horizontal ellipsis
        [0xbd, "|", true],  // Pipe symbol
        [0xbe, "\u2014", true],  // Horizontal line
        [0xbf, "\u21b5", true],  // Downward arrow with corner leftwards
        [0xc0, "\u2135"],  // Alef symbol
        [0xc1, "\u2111"],  // Black-letter capital I
        [0xc2, "\u211c"],  // Black-letter capital R
        [0xc3, "\u2118"],  // Script capital P
        [0xc4, "\u2297"],  // Circled times
        [0xc5, "\u2295"],  // Circled plus
        [0xc6, "\u2205"],  // Empty set
        [0xc7, "\u2229"],  // Intersection
        [0xc8, "\u222a"],  // Union
        [0xc9, "\u2283"],  // Superset of
        [0xca, "\u2287"],  // Superset of or equal to
        [0xcb, "\u2284"],  // Not a subset of
        [0xcc, "\u2282"],  // Subset of
        [0xcd, "\u2286"],  // Subset of or equal to
        [0xce, "\u2208"],  // Element of
        [0xcf, "\u2209"],  // Not an element of
        [0xd0, "\u2220"],  // Angle
        [0xd1, "\u2207"],  // Nabla
        [0xd2, "\xae", true],  // Registered sign
        [0xd3, "\xa9", true],  // Copyright sign
        [0xd4, "\u2122", true],  // Trade mark sign
        [0xd5, "\u220f", true],  // N-ary product
        [0xd6, "\u221a", true],  // Square root
        [0xd7, "\u22c5"],  // Dot operator
        [0xd8, "\xac", true],  // Not sign
        [0xd9, "\u2227"],  // Logical and
        [0xda, "\u2228"],  // Logical or
        [0xdb, "\u21d4"],  // Left right double arrow
        [0xdc, "\u21d0"],  // Leftwards double arrow
        [0xdd, "\u21d1"],  // Upwards double arrow
        [0xde, "\u21d2"],  // Rightwards double arrow
        [0xdf, "\u21d3"],  // Downwards double arrow
        [0xe0, "\u25ca", true],  // Lozenge
        [0xe1, "\u2329"],  // Left-pointing angle bracket
        [0xe2, "\xae"],  // Registered sign (sans serif)
        [0xe3, "\xa9"],  // Copyright sign (sans serif)
        [0xe4, "\u2122"],  // Trade mark sign (sans serif)
        [0xe5, "\u2211", true],  // N-ary summation
        [0xe6, "\u239b", true],  // Left parenthesis upper hook
        [0xe7, "\u239c", true],  // Left parenthesis extension
        [0xe8, "\u239d", true],  // Left parenthesis lower hook
        [0xe9, "\u23a1", true],  // Left square bracket upper corner
        [0xea, "\u23a2", true],  // Left square bracket extension
        [0xeb, "\u23a3", true],  // Left square bracket lower corner
        [0xec, "\u23a7", true],  // Left curly bracket upper hook
        [0xed, "\u23a8", true],  // Left curly bracket middle piece
        [0xee, "\u23a9", true],  // Left curly bracket lower hook
        [0xef, "\u23aa", true],  // Curly bracket extension
        [0xf1, "\u232a", true],  // Right-pointing angle bracket
        [0xf2, "\u222b", true],  // Integral
        [0xf3, "\u2320", true],  // Top half integral
        [0xf4, "\u23ae", true],  // Integral extension
        [0xf5, "\u2321", true],  // Bottom half integral
        [0xf6, "\u239e", true],  // Right parenthesis upper hook
        [0xf7, "\u239f", true],  // Right parenthesis extension
        [0xf8, "\u23a0", true],  // Right parenthesis lower hook
        [0xf9, "\u23a4", true],  // Right square bracket upper corner
        [0xfa, "\u23a5", true],  // Right square bracket extension
        [0xfb, "\u23a6", true],  // Right square bracket lower corner
        [0xfc, "\u23ab", true],  // Right curly bracket upper hook
        [0xfd, "\u23ac", true],  // Right curly bracket middle piece
        [0xfe, "\u23ad", true]  // Right curly bracket lower hook
    ],
    shadows: [{
        first: 0x00,
        last: 0xff,
        offset: 0xf000
    }]
}, {
    fontNames: ["wingdings"],
    symbols: [
        [0x20, " "],  // Space
        [0x21, "\u270f"],  // Pencil
        [0x22, "\u2702"],  // Black scissors
        [0x23, "\u2701"],  // Upper blade scissors
        [0x28, "\u260e"],  // Black telephone
        [0x29, "\u2706"],  // Telephone location sign
        [0x2a, "\u2709"],  // Envelope
        [0x36, "\u231b"],  // Hourglass
        [0x37, "\u2328"],  // Keyboard
        [0x3e, "\u2707"],  // Tape drive
        [0x3f, "\u270d"],  // Writing hand
        [0x41, "\u270c"],  // Victory hand
        [0x45, "\u261c"],  // White left pointing index
        [0x46, "\u261e"],  // White right pointing index
        [0x47, "\u261d"],  // White up pointing index
        [0x48, "\u261f"],  // White down pointing index
        [0x4a, "\u263a"],  // White smiling face
        [0x4c, "\u2639"],  // White frowning face
        [0x4e, "\u2620"],  // Skull and crossbones
        [0x4f, "\u2690"],  // White flag
        [0x51, "\u2708"],  // Airplane
        [0x52, "\u263c"],  // White sun with rays
        [0x54, "\u2744"],  // Snowflake
        [0x56, "\u271e"],  // Shadowed white Latin cross
        [0x58, "\u2720"],  // Maltese cross
        [0x59, "\u2721"],  // Star of David
        [0x5a, "\u262a"],  // Star and crescent
        [0x5b, "\u262f"],  // Yin Yang
        [0x5c, "\u0950"],  // Devanagari Om
        [0x5d, "\u2638"],  // Wheel of Dharma
        [0x5e, "\u2648"],  // Aries
        [0x5f, "\u2649"],  // Taurus
        [0x60, "\u264a"],  // Gemini
        [0x61, "\u264b"],  // Cancer
        [0x62, "\u264c"],  // Leo
        [0x63, "\u264d"],  // Virgo
        [0x64, "\u264e"],  // Libra
        [0x65, "\u264f"],  // Scorpio
        [0x66, "\u2650"],  // Sagittarius
        [0x67, "\u2651"],  // Capricorn
        [0x68, "\u2652"],  // Aquarius
        [0x69, "\u2653"],  // Pisces
        [0x6c, "\u25cf"],  // Black circle
        [0x6d, "\u274d"],  // Shadowed white circle
        [0x6e, "\u25a0"],  // Black square
        [0x6f, "\u25a1"],  // White square
        [0x71, "\u2751"],  // Lower right shadowed white square
        [0x72, "\u2752"],  // Upper right shadowed white square
        [0x73, "\u2b27"],  // Black medium lozenge
        [0x74, "\u29eb"],  // Black lozenge
        [0x75, "\u25c6"],  // Black diamond
        [0x76, "\u2756"],  // Black diamond minus white X
        [0x78, "\u2327"],  // X in a rectangle box
        [0x79, "\u2353"],  // APL functional symbol quad up caret
        [0x7a, "\u2318"],  // Place of interest sign
        [0x7b, "\u2740"],  // White florette
        [0x7c, "\u273f"],  // Black florette
        [0x7d, "\u275d"],  // Heavy double turned comma quotation mark ornament
        [0x7e, "\u275e"],  // Heavy double comma quotation mark ornament
        [0x7f, "\u25af"],  // (White vertical rectangle)
        [0x80, "\u24ea"],  // Circled digit zero
        [0x81, "\u2460"],  // Circled digit one
        [0x82, "\u2461"],  // Circled digit two
        [0x83, "\u2462"],  // Circled digit three
        [0x84, "\u2463"],  // Circled digit four
        [0x85, "\u2464"],  // Circled digit five
        [0x86, "\u2465"],  // Circled digit six
        [0x87, "\u2466"],  // Circled digit seven
        [0x88, "\u2467"],  // Circled digit eight
        [0x89, "\u2468"],  // Circled digit nine
        [0x8a, "\u2469"],  // Circled number ten
        [0x8b, "\u24ff"],  // Negative circled digit zero
        [0x8c, "\u2776"],  // Dingbat negative circled digit one
        [0x8d, "\u2777"],  // Dingbat negative circled digit two
        [0x8e, "\u2778"],  // Dingbat negative circled digit three
        [0x8f, "\u2779"],  // Dingbat negative circled digit four
        [0x90, "\u277a"],  // Dingbat negative circled digit five
        [0x91, "\u277b"],  // Dingbat negative circled digit six
        [0x92, "\u277c"],  // Dingbat negative circled digit seven
        [0x93, "\u277d"],  // Dingbat negative circled digit eight
        [0x94, "\u277e"],  // Dingbat negative circled digit nine
        [0x95, "\u277f"],  // Dingbat negative circled number ten
        [0x9e, "\xb7"],  // Middle dot
        [0x9f, "\u2022"],  // Bullet
        [0xa0, "\u25aa"],  // Black small square
        [0xa1, "\u25cb"],  // White circle
        [0xa4, "\u25c9"],  // Fisheye
        [0xa5, "\u25ce"],  // Bullseye
        [0xa6, "\u274d"],  // Shadowed white circle
        [0xa7, "\u25aa"],  // Black small square
        [0xa8, "\u25fb"],  // White medium square
        [0xaa, "\u2726"],  // Black four pointed star
        [0xab, "\u2605"],  // Black star
        [0xac, "\u2736"],  // Six pointed black star
        [0xad, "\u2734"],  // Eight pointed black star
        [0xae, "\u2739"],  // Twelve pointed black star
        [0xaf, "\u2735"],  // Eight pointed pinwheel star
        [0xb1, "\u2316"],  // Position indicator
        [0xb3, "\u2311"],  // Square lozenge
        [0xb5, "\u272a"],  // Circled white star
        [0xb6, "\u2730"],  // Shadowed white star
        [0xd5, "\u232b"],  // Erase to the left
        [0xd6, "\u2326"],  // Erase to the right
        [0xd8, "\u27a2"],  // Three-D top-lighted rightwards arrowhead
        [0xdc, "\u27b2"],  // Circled heavy white rightwards arrow
        [0xe8, "\u2794"],  // Heavy wide-headed rightwards arrow
        [0xef, "\u21e6"],  // Leftwards white arrow
        [0xf0, "\u21e8"],  // Rightwards white arrow
        [0xf1, "\u21e7"],  // Upwards white arrow
        [0xf2, "\u21e9"],  // Downwards white arrow
        [0xf3, "\u2b04"],  // Left right white arrow
        [0xf4, "\u21f3"],  // Up down white arrow
        [0xf5, "\u2b01"],  // North west white arrow
        [0xf6, "\u2b00"],  // North east white arrow
        [0xf7, "\u2b03"],  // South west white arrow
        [0xf8, "\u2b02"],  // South east white arrow
        [0xf9, "\u25ad"],  // White rectangle
        [0xfa, "\u25ab"],  // White small square
        [0xfb, "\u2717"],  // Ballot X
        [0xfc, "\u2713"],  // Check mark
        [0xfd, "\u2612"],  // Ballot box with X
        [0xfe, "\u2611"]  // Ballot box with check
    ],
    shadows: [{
        first: 0x00,
        last: 0xff,
        offset: 0xf000
    }]
}, {
    fontNames: ["wingdings2", "wingdings 2"],
    symbols: [
        [0x97, "\u25cf"],
        [0x9e, "\u29bf"]
    ],
    shadows: [{
        first: 0x00,
        last: 0xff,
        offset: 0xf000
    }]
}, {
    fontNames: ["opensymbol", "starsymbol"],
    unicode: true, // Unicode compliant: unmentioned characters are mapped to themselves
    symbols: [
        [0xe001, "\u2b2a"],
        [0xe002, "\u2666"],
        [0xe003, "\u25c6"],
        [0xe004, "\u2b29"],
        [0xe005, "\u274d"],
        [0xe006, "\u2794"],
        [0xe007, "\u2713"],
        [0xe008, "\u25cf"],
        [0xe009, "\u274d"],
        [0xe00a, "\u25fc"],
        [0xe00b, "\u2752"],
        [0xe00c, "\u25c6"],
        [0xe00d, "\u2756"],
        [0xe00e, "\u29eb"],
        [0xe00f, "\u24ff"],
        [0xe010, "\u24ea"],
        [0xe013, "\u2742"],
        [0xe01b, "\u270d"],
        [0xe01e, "\u2022"],
        [0xe021, "\xa9"],
        [0xe024, "\xae"],
        [0xe025, "\u21e8"],
        [0xe026, "\u21e9"],
        [0xe027, "\u21e6"],
        [0xe028, "\u21e7"],
        [0xe02b, "\u279e"],
        [0xe032, "\u2741"],
        [0xe036, "(", true],
        [0xe037, ")", true],
        [0xe03a, "\u20ac"],
        [0xe080, "\u2030", true],
        [0xe081, "\ufe38"],
        [0xe082, "\ufe37"],
        [0xe083, "+", true],
        [0xe084, "<", true],
        [0xe085, ">", true],
        [0xe086, "\u2264", true],
        [0xe087, "\u2265", true],
        [0xe089, "\u2208", true],
        [0xe08a, "\u0192", true],
        [0xe08b, "\u2026", true],
        [0xe08c, "\u2192", true],
        [0xe08d, "\u221a", true],
        [0xe090, "\u2225", true],
        [0xe091, "\u02c6", true],
        [0xe092, "\u02c7", true],
        [0xe093, "\u02d8", true],
        [0xe094, "\u02ca", true],
        [0xe095, "\u02cb", true],
        [0xe096, "\u02dc", true],
        [0xe097, "\xaf", true],
        [0xe098, "\u2192", true],
        [0xe09b, "\u20db"],
        [0xe09e, "(", true],
        [0xe09f, ")", true],
        [0xe0a0, "\u2221"],
        [0xe0aa, "\u2751"],
        [0xe0ac, "\u0393", true],
        [0xe0ad, "\u0394", true],
        [0xe0ae, "\u0398", true],
        [0xe0af, "\u039b", true],
        [0xe0b0, "\u039e", true],
        [0xe0b1, "\u03a0", true],
        [0xe0b2, "\u03a3", true],
        [0xe0b3, "\u03a5", true],
        [0xe0b4, "\u03a6", true],
        [0xe0b5, "\u03a8", true],
        [0xe0b6, "\u03a9", true],
        [0xe0b7, "\u03b1", true],
        [0xe0b8, "\u03b2", true],
        [0xe0b9, "\u03b3", true],
        [0xe0ba, "\u03b4", true],
        [0xe0bb, "\u03f5", true],
        [0xe0bc, "\u03b6", true],
        [0xe0bd, "\u03b7", true],
        [0xe0be, "\u03b8", true],
        [0xe0bf, "\u03b9", true],
        [0xe0c0, "\u03ba", true],
        [0xe0c1, "\u03bb", true],
        [0xe0c2, "\u03bc", true],
        [0xe0c3, "\u03bd", true],
        [0xe0c4, "\u03be", true],
        [0xe0c5, "\u03bf", true],
        [0xe0c6, "\u03c0", true],
        [0xe0c7, "\u03c1", true],
        [0xe0c8, "\u03c3", true],
        [0xe0c9, "\u03c4", true],
        [0xe0ca, "\u03c5", true],
        [0xe0cb, "\u03d5", true],
        [0xe0cc, "\u03c7", true],
        [0xe0cd, "\u03c8", true],
        [0xe0ce, "\u03c9", true],
        [0xe0cf, "\u03b5", true],
        [0xe0d0, "\u03d1", true],
        [0xe0d1, "\u03d6", true],
        [0xe0d3, "\u03c2", true],
        [0xe0d4, "\u03c6", true],
        [0xe0d5, "\u2202", true],
        [0xe0d9, "\u22a4", true],
        [0xe0db, "\u2190", true],
        [0xe0dc, "\u2191", true],
        [0xe0dd, "\u2193", true],
        [0xe41d, "\u270d"],
        [0xe448, "\u24ea"],
        [0xe449, "\u2460"],
        [0xe44a, "\u2461"],
        [0xe44b, "\u2462"],
        [0xe44c, "\u2463"],
        [0xe44d, "\u2464"],
        [0xe44e, "\u2465"],
        [0xe44f, "\u2466"],
        [0xe450, "\u2467"],
        [0xe451, "\u2468"],
        [0xe452, "\u2469"],
        [0xe453, "\u24ff"],
        [0xe454, "\u2776"],
        [0xe455, "\u2777"],
        [0xe456, "\u2778"],
        [0xe457, "\u2779"],
        [0xe458, "\u277a"],
        [0xe459, "\u277b"],
        [0xe45a, "\u277c"],
        [0xe45b, "\u277d"],
        [0xe45c, "\u277e"],
        [0xe45d, "\u277f"],
        [0xe548, "\u2460"],
        [0xe549, "\u2461"],
        [0xe54a, "\u2462"],
        [0xe54b, "\u2463"],
        [0xe54c, "\u2464"],
        [0xe54d, "\u2465"],
        [0xe54e, "\u2466"],
        [0xe54f, "\u2467"],
        [0xe550, "\u2468"],
        [0xe551, "\u2469"],
        [0xe552, "\u24ff"],
        [0xe553, "\u2776"],
        [0xe554, "\u2777"],
        [0xe555, "\u2778"],
        [0xe556, "\u2779"],
        [0xe557, "\u277a"],
        [0xe558, "\u277b"],
        [0xe559, "\u277c"],
        [0xe55a, "\u277d"],
        [0xe55b, "\u277e"],
        [0xe55c, "\u277f"]
    ]
}];

// class SymbolFont ===========================================================

class SymbolFont {

    // static fields ----------------------------------------------------------

    static #registry?: Map<string, Opt<SymbolFont>>;

    static get(fontName: string): Opt<SymbolFont> {

        // create the registry once on first call
        if (!SymbolFont.#registry) {
            SymbolFont.#registry = new Map();
            for (const fontConfig of SYMBOL_FONT_CONFIGS) {
                const symbolFont = new SymbolFont(fontConfig);
                for (const key of fontConfig.fontNames) {
                    SymbolFont.#registry.set(key, symbolFont);
                }
            }
        }

        return SymbolFont.#registry.get(fontName.toLowerCase());
    }

    // properties -------------------------------------------------------------

    readonly #map = new Map<number, SymbolChar>();
    readonly #unicode: boolean;

    // constructor ------------------------------------------------------------

    constructor(config: SymbolFontConfig) {

        // process all symbol characters
        for (const [code, char, serif = false] of config.symbols) {

            // create the map entry for the main codepoint
            const symbolChar: SymbolChar = { char, serif };
            this.#map.set(code, symbolChar);

            // generate shadow entries for alternative codepoints
            if (config.shadows) {
                for (const { first, last, offset } of config.shadows) {
                    if ((first <= code) && (code <= last)) {
                        this.#map.set(code + offset, symbolChar);
                    }
                }
            }
        }

        // whether font is Unicode compliant (map all other characters to themselves)
        this.#unicode = !!config.unicode;
    }

    // public methods ---------------------------------------------------------

    get(symbol: string): Opt<Readonly<SymbolChar>> {

        // get codepoint of symbol character
        const code = (symbol.length === 1) ? symbol.charCodeAt(0) : -1;
        if (code <= 0) { return undefined; }

        // return existing entry in this map
        const entry = this.#map.get(code);
        if (entry) { return entry; }

        // return passed character for Unicode-compliant symbol fonts
        return this.#unicode ? { char: symbol, serif: false } : undefined;
    }
}

// public functions ===========================================================

/**
 * Returns whether the passed font name is a supported symbol font.
 *
 * @param fontName
 *  The font name (case-insensitive).
 *
 * @returns
 *  Whether the passed font name is a supported symbol font.
 */
export function isSymbolFont(fontName: string): boolean {
    return !!SymbolFont.get(fontName);
}

/**
 * Returns a replacement character for a symbol from a symbol font.
 *
 * @param fontName
 *  The font name (case-insensitive).
 *
 * @param symbol
 *  The symbol character from the passed font. Must contain one character only.
 *
 * @returns
 *  The replacement character for the passed symbol, if available.
 */
export function getSymbolReplacement(fontName: string, symbol: string): Opt<SymbolChar> {
    return SymbolFont.get(fontName)?.get(symbol);
}
