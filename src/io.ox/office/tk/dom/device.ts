/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import _ from "$/underscore";

import { math, fun } from "@/io.ox/office/tk/algorithms";
import { globalEvents } from "@/io.ox/office/tk/events";
import { createDiv } from "@/io.ox/office/tk/dom/domutils";
import { UNITTEST } from "@/io.ox/office/tk/config";

// global events ==============================================================

declare global {
    interface DocsGlobalEventMap {

        /**
         * Will be emitted when zooming the browser, i.e. when the device pixel
         * resolution has changed. May not work reliably on all browsers.
         *
         * @param newResolution
         *  The new (current) device resolution (device pixels per CSS pixel).
         *
         * @param oldResolution
         *  The old (previous) device resolution (device pixels per CSS pixel).
         */
        "change:resolution": [newResolution: number, oldResolution: number];
    }
}

// constants ==================================================================

/**
 * Specifies whether the current display is a retina display.
 */
export const RETINA_DISPLAY = _.device("retina");

/**
 * Specifies whether the current device is a touch device.
 */
export const TOUCH_DEVICE = _.device("touch"); // bug 39937: this is the "official" touch detection!

/**
 * Specifies whether the current device is small (smart phones).
 */
export const SMALL_DEVICE = _.device("smartphone");

/**
 * Specifies whether the current device is medium sized (tablets with a width
 * of less than 1024px).
 */
export const MEDIUM_DEVICE = _.device("tablet");

/**
 * Specifies whether the current device is small (smart phones) or medium sized
 * (tablets with a width of less than 1024px).
 */
export const COMPACT_DEVICE = SMALL_DEVICE || MEDIUM_DEVICE;

/**
 * Specifies whether Safari browser runs with iOS on the current device. The
 * query includes the "ios" and "macos" flags for iPhone, iPad, and iPadOS
 * support.
 */
export const IOS_SAFARI_DEVICE = TOUCH_DEVICE && _.device("(ios || macos) && safari");

// calculate size of system scroll bars
const scrollBarSize = fun.do(() => {
    const node = createDiv({ style: { width: "100px", height: "100px", overflow: "scroll" } });
    document.body.appendChild(node);
    const width = 100 - node.clientWidth;
    const height = 100 - node.clientHeight;
    document.body.removeChild(node);
    return { width, height };
});

/**
 * The width of a vertical system scrollbar, in CSS pixels.
 */
export const SCROLLBAR_WIDTH = scrollBarSize.width;

/**
 * The height of a horizontal system scrollbar, in CSS pixels.
 */
export const SCROLLBAR_HEIGHT = scrollBarSize.height;

// private constants ----------------------------------------------------------

// workaround required for portrait/landscape detection
const IOS_PORTRAIT_DETECTION = TOUCH_DEVICE && _.browser.iOS;

// private functions ==========================================================

/**
 * Returns whether the property `window.orientation` is not set to +-90deg.
 */
function isPortraitOrientation(): boolean {
    return Math.abs(window.orientation) !== 90;
}

let resolution = getDeviceResolution();
function updateDeviceResolution(): void {
    const newResolution = getDeviceResolution();
    if (resolution !== newResolution) {
        globalEvents.emit("change:resolution", newResolution, resolution);
        resolution = newResolution;
    }
}

// update current resolution on window resize events (browser zoom changed)
window.addEventListener("resize", updateDeviceResolution);

// update current resolution even without events (no global interval timers in unit tests!)
if (!UNITTEST) {
    window.setInterval(updateDeviceResolution, 2500);
}

// public functions ===========================================================

/**
 * Returns whether the display is in portrait mode currently (the current
 * screen width is less than the current screen height). On mobile devices, the
 * screen orientation may change over time according to the position of the
 * device.
 *
 * @returns
 *  Whether the display is in portrait mode currently.
 */
export function isPortrait(): boolean {
    // iOS: check `window.orientation` (screen width and height is always constant)
    // Other: use screen width and height (meaning of `window.orientation` differs on Android devices)
    return IOS_PORTRAIT_DETECTION ? isPortraitOrientation() : (screen.height > screen.width);
}

/**
 * Returns whether the display is in landscape mode currently (the current
 * screen width is greater than the current screen height). On mobile devices,
 * the screen orientation may change over time according to the position of the
 * device.
 *
 * @returns
 *  Whether the display is in landscape mode currently.
 */
export function isLandscape(): boolean {
    return !isPortrait();
}

/**
 * Returns the current effective screen width according to the orientation of
 * the device, in pixels.
 *
 * @returns
 *  The current effective screen width according to the orientation of the
 *  device, in pixels.
 */
export const getScreenWidth: FuncType<number> = IOS_PORTRAIT_DETECTION ?
    // iOS: `screen.width` may return screen height depending on device orientation
    (() => isPortraitOrientation() ? screen.width : screen.height) :
    () => screen.width;

/**
 * Returns the current effective screen height according to the orientation of
 * the device, in pixels.
 *
 * @returns
 *  The current effective screen height according to the orientation of the
 *  device, in pixels.
 */
export const getScreenHeight: FuncType<number> = IOS_PORTRAIT_DETECTION ?
    // iOS: `screen.height` may return screen width depending on device orientation
    (() => isPortraitOrientation() ? screen.height : screen.width) :
    () => screen.height;

/**
 * Returns the current effective width of the inner window area according to
 * the orientation of the device, in pixels.
 *
 * @returns
 *  The current effective width of the inner window area according to the
 *  orientation of the device, in pixels.
 */
export const getWindowWidth: FuncType<number> = IOS_PORTRAIT_DETECTION ?
    // iOS: `window.innerWidth` may return window height depending on device orientation
    (() => isPortraitOrientation() ? window.innerWidth : window.innerHeight) :
    () => window.innerWidth;

/**
 * Returns the current effective height of the inner window area according to
 * the orientation of the device, in pixels.
 *
 * @returns
 *  The current effective height of the inner window area according to the
 *  orientation of the device, in pixels.
 */
export const getWindowHeight: FuncType<number> = IOS_PORTRAIT_DETECTION ?
    // iOS: `window.innerHeight` may return window width depending on device orientation
    (() => isPortraitOrientation() ? window.innerHeight : window.innerWidth) :
    () => window.innerHeight;

/**
 * Returns the current device pixel resolution from the browser window.
 *
 * @returns
 *  The current device pixel resolution from the browser window.
 */
export function getDeviceResolution(): number {
    return math.clamp(window.devicePixelRatio || 1, 0.01, 100);
}

/**
 * Adds device-specific CSS marker classes to the specified DOM element. The
 * following CSS classes will be added:
 *
 * - "touch", if the device has a touch surface.
 * - "browser-firefox", if the current browser is Firefox.
 * - "browser-webkit", if the current browser is based on WebKit.
 * - "browser-chrome", if the current browser is Chrome.
 * - "browser-safari", if the current browser is Safari.
 * - "browser-ie", if the current browser is Internet Explorer.
 *
 * @param elem
 *  The DOM element to be marked with device-specific classes.
 */
export function addDeviceMarkers(elem: HTMLElement): void {
    const { classList } = elem;
    // marker for touch devices
    classList.toggle("touch", TOUCH_DEVICE);
    // marker for browser types
    classList.toggle("browser-firefox", !!_.browser.Firefox);
    classList.toggle("browser-webkit", !!_.browser.WebKit);
    classList.toggle("browser-chrome", !!_.browser.Chrome);
    classList.toggle("browser-safari", !!_.browser.Safari);
}
