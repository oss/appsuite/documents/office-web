/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import gt from "gettext";

// constants ==================================================================

/**
 * Generic button label for an "Okay" button.
 */
//#. generic button label for an "OK" button
export const OK_LABEL = gt("OK");

/**
 * Generic button label for a "Cancel" button.
 */
//#. generic button label for a "Cancel" button
export const CANCEL_LABEL = gt("Cancel");

/**
 * Generic button label for a "Close" button.
 */
//#. generic button label for a "Close" button
export const CLOSE_LABEL = gt("Close");

/**
 * Generic button label for a "Yes" button.
 */
//#. generic button label for a "Yes" button
export const YES_LABEL = gt("Yes");

/**
 * Generic button label for a "No" button.
 */
//#. generic button label for a "No" button
export const NO_LABEL =  gt("No");
