/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { Point, Polar } from "@/io.ox/office/tk/algorithms";
import { is, math, coord, pick } from "@/io.ox/office/tk/algorithms";

// types ======================================================================

/**
 * Interface for rectangle-like objects.
 */
export interface RectangleLike {

    /**
     * The coordinate of the left border of the rectangle.
     */
    left: number;

    /**
     * The coordinate of the top border of the rectangle.
     */
    top: number;

    /**
     * The width of the rectangle.
     */
    width: number;

    /**
     * The height of the rectangle.
     */
    height: number;
}

/**
 * The exact position and size of a one-dimensional interval.
 */
export interface IntervalPosition {

    /**
     * The offset of the interval.
     */
    offset: number;

    /**
     * The size of the interval.
     */
    size: number;
}

/**
 * Type for rectangle coordinates as strings with CSS unit.
 */
export type CSSRectangle = MapType<RectangleLike, string>;

// private functions ==========================================================

/**
 * Returns the coordinate of the right border of a rectangle.
 *
 * @param rect
 *  An arbitrary rectangle-like object.
 *
 * @returns
 *  The coordinate of the right border of the passed rectangle.
 */
function right(rect: RectangleLike): number {
    return rect.left + rect.width;
}

/**
 * Returns the coordinate of the bottom border of a rectangle.
 *
 * @param rect
 *  An arbitrary rectangle-like object.
 *
 * @returns
 *  The coordinate of the bottom border of the passed rectangle.
 */
function bottom(rect: RectangleLike): number {
    return rect.top + rect.height;
}

// class Rectangle ============================================================

/**
 * Helper class that represents an arbitrary rectangle with a start position,
 * and a size.
 *
 * @param left
 *  Horizontal start coordinate of the rectangle. Can be any floating-point
 *  number, including negative offsets.
 *
 * @param top
 *  Vertical start coordinate of the rectangle. Can be any floating-point
 *  number, including negative offsets.
 *
 * @param width
 *  The width of the rectangle. Must not be negative. May be zero, if the
 *  rectangle represents a vertical hair line.
 *
 * @param height
 *  The height of the rectangle. Must not be negative. May be zero, if the
 *  rectangle represents a horizontal hair line.
 */
export class Rectangle implements RectangleLike, Equality<RectangleLike>, Cloneable<Rectangle> {

    // static methods ---------------------------------------------------------

    /**
     * Constructs a `Rectangle` instance from an arbitrary rectangle-like JS
     * object.
     *
     * @param rect
     *  A partial rectangle-like object to be converted. Missing properties
     *  will be set to zero (e.g. a `Point` or `Size` object). Accepts also
     *  real instances of `Rectangle` for cloning.
     *
     * @returns
     *  A new instance of the class `Rectangle` at the specified location.
     */
    static from(rect: Partial<RectangleLike>): Rectangle {
        return new Rectangle(rect.left || 0, rect.top || 0, rect.width || 0, rect.height || 0);
    }

    /**
     * Constructs a `Rectangle` instance from two interval positions with
     * offsets and sizes.
     *
     * @param hPos
     *  The left offset and width of the rectangle.
     *
     * @param vPos
     *  The top offset and height of the rectangle.
     *
     * @returns
     *  A new instance of the class `Rectangle` at the specified location.
     */
    static fromIntervals(hPos: IntervalPosition, vPos: IntervalPosition): Rectangle {
        return new Rectangle(hPos.offset, vPos.offset, hPos.size, vPos.size);
    }

    /**
     * Constructs a `Rectangle` instance from arbitrary JSON data.
     *
     * @param json
     *  The JSON data to be parsed. Expects an object with the numeric
     *  properties `left`, `top`, `width`, and `height`. Missing or invalid
     *  properties will fall back to zero.
     *
     * @returns
     *  A new instance of the class `Rectangle` at the specified location.
     */
    static parseJSON(json: unknown): Rectangle {
        return new Rectangle(pick.number(json, "left", 0), pick.number(json, "top", 0), pick.number(json, "width", 0), pick.number(json, "height", 0));
    }

    // properties -------------------------------------------------------------

    left: number;
    top: number;
    width: number;
    height: number;

    // constructor ------------------------------------------------------------

    constructor(left: number, top: number, width: number, height: number) {
        this.left = left;
        this.top = top;
        this.width = Math.max(0, width);
        this.height = Math.max(0, height);
    }

    // public methods ---------------------------------------------------------

    /**
     * Returns a clone of this rectangle instance.
     *
     * @returns
     *  A clone of this rectangle instance.
     */
    clone(): Rectangle {
        return new Rectangle(this.left, this.top, this.width, this.height);
    }

    /**
     * Returns whether the passed rectangle is equal to this rectangle.
     *
     * @param rect
     *  The other rectangle to be compared to this rectangle. This value can be
     *  any rectangle-like object.
     *
     * @returns
     *  Whether both rectangles are exactly equal.
     */
    equals(rect: RectangleLike): boolean {
        return (this.left === rect.left) && (this.top === rect.top) && (this.width === rect.width) && (this.height === rect.height);
    }

    /**
     * Returns whether the passed rectangle differs from this rectangle.
     *
     * @param rect
     *  The other rectangle to be compared to this rectangle. This value can be
     *  any rectangle-like object.
     *
     * @returns
     *  Whether the passed rectangle differs from this rectangle.
     */
    differs(rect: RectangleLike): boolean {
        return !this.equals(rect);
    }

    /**
     * Returns the coordinate of the right border of this rectangle.
     *
     * @returns
     *  The coordinate of the right border of this rectangle.
     */
    right(): number {
        return right(this);
    }

    /**
     * Returns the coordinate of the lower border of this rectangle.
     *
     * @returns
     *  The coordinate of the lower border of this rectangle.
     */
    bottom(): number {
        return bottom(this);
    }

    /**
     * Returns the X coordinate of the center point of this rectangle.
     *
     * @returns
     *  The X coordinate of the center point of this rectangle.
     */
    centerX(): number {
        return this.left + this.width / 2;
    }

    /**
     * Returns the Y coordinate of the center point of this rectangle.
     *
     * @returns
     *  The Y coordinate of the center point of this rectangle.
     */
    centerY(): number {
        return this.top + this.height / 2;
    }

    /**
     * Returns the length of the diagonals of this rectangle.
     *
     * @returns
     *  The length of the diagonals of this rectangle.
     */
    diag(): number {
        return math.radius(this.width, this.height);
    }

    /**
     * Returns the area size of this rectangle.
     *
     * @returns
     *  The area size of this rectangle.
     */
    area(): number {
        return this.width * this.height;
    }

    /**
     * Returns whether this rectangle is a square.
     *
     * @returns
     *  Whether this rectangle is a square.
     */
    square(): boolean {
        return this.width === this.height;
    }

    /**
     * Returns the position of the top-left corner of this rectangle.
     *
     * @returns
     *  The position of the top-left corner of this rectangle.
     */
    topLeft(): Point {
        return { x: this.left, y: this.top };
    }

    /**
     * Returns the position of the top-right corner of this rectangle.
     *
     * @returns
     *  The position of the top-right corner of this rectangle.
     */
    topRight(): Point {
        return { x: this.right(), y: this.top };
    }

    /**
     * Returns the position of the bottom-left corner of this rectangle.
     *
     * @returns
     *  The position of the bottom-left corner of this rectangle.
     */
    bottomLeft(): Point {
        return { x: this.left, y: this.bottom() };
    }

    /**
     * Returns the position of the bottom-right corner of this rectangle.
     *
     * @returns
     *  The position of the bottom-right corner of this rectangle.
     */
    bottomRight(): Point {
        return { x: this.right(), y: this.bottom() };
    }

    /**
     * Returns the position of the center of the left border of this rectangle.
     *
     * @returns
     *  The position of the center of the left border of this rectangle.
     */
    leftCenter(): Point {
        return { x: this.left, y: this.centerY() };
    }

    /**
     * Returns the position of the center of the right border of this
     * rectangle.
     *
     * @returns
     *  The position of the center of the right border of this rectangle.
     */
    rightCenter(): Point {
        return { x: this.right(), y: this.centerY() };
    }

    /**
     * Returns the position of the center of the top border of this rectangle.
     *
     * @returns
     *  The position of the center of the top border of this rectangle.
     */
    topCenter(): Point {
        return { x: this.centerX(), y: this.top };
    }

    /**
     * Returns the position of the center of the bottom border of this
     * rectangle.
     *
     * @returns
     *  The position of the center of the bottom border of this rectangle.
     */
    bottomCenter(): Point {
        return { x: this.centerX(), y: this.bottom() };
    }

    /**
     * Returns the position of the center of this rectangle.
     *
     * @returns
     *  The position of the center of this rectangle.
     */
    center(): Point {
        return { x: this.centerX(), y: this.centerY() };
    }

    /**
     * Returns whether this rectangle contains a specific X coordinate.
     *
     * @param x
     *  The X coordinate to be checked.
     *
     * @returns
     *  Whether the passed X coordinate is inside this rectangle.
     */
    containsX(x: number): boolean {
        return (this.left <= x) && (x <= right(this));
    }

    /**
     * Returns whether this rectangle contains a specific Y coordinate.
     *
     * @param y
     *  The Y coordinate to be checked.
     *
     * @returns
     *  Whether the passed Y coordinate is inside this rectangle.
     */
    containsY(y: number): boolean {
        return (this.top <= y) && (y <= bottom(this));
    }

    /**
     * Returns whether this rectangle contains a specific dimensionless (i.e.
     * of size 0x0) point.
     *
     * @param point
     *  The coordinates of the point to be checked.
     *
     * @param x
     *  The X coordinate of the point to be checked.
     *
     * @param y
     *  The Y coordinate of the point to be checked.
     *
     * @returns
     *  Whether the specified point is inside this rectangle.
     */
    containsPoint(point: Point): boolean;
    containsPoint(x: number, y: number): boolean;
    // implementation
    containsPoint(x: Point | number, y?: number): boolean {
        if (!is.number(x)) { y = x.y; x = x.x; }
        return this.containsX(x) && this.containsY(y!);
    }

    /**
     * Returns whether this rectangle contains an entire pixel (a rectangle
     * with size 1x1).
     *
     * @param point
     *  The top-left position of the pixel to be checked.
     *
     * @param x
     *  The X coordinate of the left border of the pixel to be checked.
     *
     * @param y
     *  The Y coordinate of the top border of the pixel to be checked.
     *
     * @returns
     *  Whether the specified pixel is inside this rectangle.
     */
    containsPixel(point: Point): boolean;
    containsPixel(x: number, y: number): boolean;
    // implementation
    containsPixel(x: Point | number, y?: number): boolean {
        if (!is.number(x)) { y = x.y; x = x.x; }
        return (this.left <= x) && (x + 1 <= right(this)) && (this.top <= y!) && (y! + 1 <= bottom(this));
    }

    /**
     * Returns whether this rectangle completely contains the passed rectangle.
     *
     * @param rect
     *  The rectangle to be checked. Can be any rectangle-like object.
     *
     * @returns
     *  Whether the passed rectangle is completely located inside this
     *  rectangle.
     */
    contains(rect: RectangleLike): boolean {
        return (this.left <= rect.left) && (right(rect) <= right(this)) && (this.top <= rect.top) && (bottom(rect) <= bottom(this));
    }

    /**
     * Returns whether this rectangle and the passed rectangle overlap each
     * other.
     *
     * @param rect
     *  The rectangle to be checked. Can be any rectangle-like object.
     *
     * @returns
     *  Whether the passed rectangle overlaps with this rectangle.
     */
    overlaps(rect: RectangleLike): boolean {
        return (this.left < right(rect)) && (rect.left < right(this)) && (this.top < bottom(rect)) && (rect.top < bottom(this));
    }

    /**
     * Returns the bounding rectangle of this rectangle and the passed
     * rectangle (the smallest rectangle that contains both rectangles).
     *
     * @param rect
     *  The other rectangle. Can be any rectangle-like object.
     *
     * @returns
     *  The bounding rectangle containing both rectangles.
     */
    boundary(rect: RectangleLike): Rectangle {

        const l = Math.min(this.left, rect.left);
        const t = Math.min(this.top, rect.top);
        const r = Math.max(right(this), right(rect));
        const b = Math.max(bottom(this), bottom(rect));

        return new Rectangle(l, t, r - l, b - t);
    }

    /**
     * Returns the common area of this rectangle and the passed rectangle.
     *
     * @param rect
     *  The other rectangle. Can be any rectangle-like object.
     *
     * @returns
     *  The location of the rectangle covered by this rectangle and the passed
     *  rectangle, if existing; otherwise null.
     *
     *  If one of the rectangles has zero width or height and is covered by the
     *  other rectangle, the resulting rectangle will have zero width or height
     *  too. Otherwise, the rectangles must overlap in order to return an
     *  existing intersection rectangle.
     */
    intersect(rect: RectangleLike): Rectangle | null {

        const l = Math.max(this.left, rect.left);
        const t = Math.max(this.top, rect.top);
        const r = Math.min(right(this), right(rect));
        const b = Math.min(bottom(this), bottom(rect));

        if (((l < r) || ((l === r) && ((this.width === 0) || (rect.width === 0)))) &&
            ((t < b) || ((t === b) && ((this.height === 0) || (rect.height === 0))))
        ) {
            return new Rectangle(l, t, r - l, b - t);
        }

        return null;
    }

    /**
     * Returns all areas of this rectangle that are NOT contained in the passed
     * rectangle.
     *
     * @param rect
     *  The rectangle to be subtracted from this rectangle. Can be any
     *  rectangle-like object.
     *
     * @returns
     *  The areas that are contained in this rectangle but not in the passed
     *  rectangle. Will be an empty array, if the passed rectangle completely
     *  covers this rectangle. Will be an array with a clone of this rectangle,
     *  if the passed rectangle is completely outside this rectangle.
     */
    remaining(rect: RectangleLike): Rectangle[] {

        // check if the passed rectangle covers this rectangle at all
        if (!this.overlaps(rect)) { return [this.clone()]; }

        // the resulting rectangles
        const rectangles: Rectangle[] = [];

        // shortcuts to the coordinates of this rectangle
        const l1 = this.left, t1 = this.top, w1 = this.width, r1 = right(this), b1 = bottom(this);
        // shortcuts to the coordinates of the other rectangle
        const l2 = rect.left, t2 = rect.top, r2 = right(rect), b2 = bottom(rect);
        // vertical coordinates of resulting areas left/right of rect
        const t = Math.max(t1, t2), b = Math.min(b1, b2), h = b - t;

        // if this starts above rect, extract the upper part of this
        if (t1 < t2) { rectangles.push(new Rectangle(l1, t1, w1, t2 - t1)); }
        // if this starts left of rect, extract the left part of this
        if (l1 < l2) { rectangles.push(new Rectangle(l1, t, l2 - l1, h)); }
        // if this ends right of rect, extract the right part of this
        if (r2 < r1) { rectangles.push(new Rectangle(r2, t, r1 - r2, h)); }
        // if this ends below rect, extract the lower part of this
        if (b2 < b1) { rectangles.push(new Rectangle(l1, b2, w1, b1 - b2)); }

        return rectangles;
    }

    /**
     * Returns the polar coordinates of the specified point, relative to the
     * center of this rectangle.
     *
     * @param point
     *  The position of the point to be converted.
     *
     * @param x
     *  The X coordinate of the point to be converted.
     *
     * @param y
     *  The Y coordinate of the point to be converted.
     *
     * @returns
     *  The polar coordinates of the point, relative to the center point of
     *  this rectangle.
     */
    pointToPolar(point: Point): Polar;
    pointToPolar(x: number, y: number): Polar;
    // implementation
    pointToPolar(x: Point | number, y?: number): Polar {
        if (!is.number(x)) { y = x.y; x = x.x; }
        return coord.pointToPolar(x - this.centerX(), y! - this.centerY());
    }

    /**
     * Returns the absolute cartesian coordinates of the specified point, given
     * as polar coordinates relative to the center of this rectangle.
     *
     * @param polar
     *  The polar coordinates of the point to be converted, relative to the
     *  center point of this rectangle.
     *
     * @param r
     *  The radius of the point to be converted, relative to the center point.
     *
     * @param a
     *  The angle between the radius and the positive X axis, in radians,
     *  relative to the center point.
     *
     * @returns
     *  The absolute position of the point.
     */
    polarToPoint(polar: Polar): Point;
    polarToPoint(r: number, a: number): Point;
    // implementation
    polarToPoint(r: Polar | number, a?: number): Point {
        if (!is.number(r)) { a = r.a; r = r.r; }
        const point = coord.polarToPoint(r, a!);
        point.x += this.centerX();
        point.y += this.centerY();
        return point;
    }

    /**
     * Rotates the specified dimensionless point (i.e. of size 0x0) around the
     * center of this rectangle.
     *
     * @param a
     *  The rotation angle, in radians.
     *
     * @param coords
     *  The coordinates of the point to be rotated.
     *
     * @param x
     *  The X coordinate of the point to be rotated.
     *
     * @param y
     *  The Y coordinate of the point to be rotated.
     *
     * @returns
     *  The absolute position of the rotated point.
     */
    rotatePoint(a: number, coords: Point): Point;
    rotatePoint(a: number, x: number, y: number): Point;
    // implementation
    rotatePoint(a: number, x: Point | number, y?: number): Point {
        if (!is.number(x)) { y = x.y; x = x.x; }
        const polar = this.pointToPolar(x, y!);
        return this.polarToPoint(polar.r, a + polar.a);
    }

    /**
     * Returns the location of the bounding box covered by this rectangle, if
     * it will be rotated around its center point by the specified angle.
     *
     * @param a
     *  The rotation angle for this rectangle, in radians.
     *
     * @returns
     *  The location of the bounding box of the rotated rectangle.
     */
    rotatedBoundingBox(a: number): Rectangle {

        // the polar coordinates of the top-left corner relative to the center point
        const polar = this.pointToPolar(this.left, this.top);

        // the rotated location of the corner p1, relative to the center
        const point1 = coord.polarToPoint(polar.r, a + polar.a);
        // the rotated location of corner p2 (corner p1 mirrored at X axis), relative to the center
        const point2 = coord.polarToPoint(polar.r, a - polar.a);

        // the maximum distances of the rotated corners from the X/Y axes
        // (half of the size of the resulting bounding raectangle)
        const w2 = Math.max(Math.abs(point1.x), Math.abs(point2.x));
        const h2 = Math.max(Math.abs(point1.y), Math.abs(point2.y));

        // the location of the bounding box
        const center = this.center();
        return new Rectangle(center.x - w2, center.y - h2, 2 * w2, 2 * h2);
    }

    /**
     * Initializes this instance with the passed coordinates.
     *
     * @param left
     *  The new horizontal start coordinate for this rectangle.
     *
     * @param top
     *  The new vertical start coordinate for this rectangle.
     *
     * @param width
     *  The new width for this rectangle.
     *
     * @param height
     *  The new height for this rectangle.
     *
     * @returns
     *  A reference to this instance.
     */
    set(left: number, top: number, width: number, height: number): this {
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
        return this;
    }

    /**
     * Sets the contents of this rectangle to the contents of the passed
     * rectangle.
     *
     * @param rect
     *  The position to be assigned to this rectangle. This value can be any
     *  rectangle-like object.
     *
     * @returns
     *  A reference to this instance.
     */
    assign(rect: RectangleLike): this {
        this.left = rect.left;
        this.top = rect.top;
        this.width = rect.width;
        this.height = rect.height;
        return this;
    }

    /**
     * Expands the location of this rectangle in-place.
     *
     * @param dl
     *  The distance to move the left border to the left. Positive numbers will
     *  expand the rectangle (property `left` will be decreased), and negative
     *  numbers will shrink the rectangle (property `left` will be increased).
     *
     * @param [dt]
     *  The distance to move the top border to the top. Positive numbers will
     *  expand the rectangle (property `top` will be decreased), and negative
     *  numbers will shrink the rectangle (property `top` will be increased).
     *  If this parameter will be omitted (one parameter passed only), the
     *  value of `dl` will be used for this border.
     *
     * @param [dr]
     *  The distance to move the right border to the right. Positive numbers
     *  will expand the rectangle (property `width` will be increased), and
     *  negative numbers will shrink the rectangle (property `width` will be
     *  decreased). If this parameter will be omitted (one or two parameters
     *  passed only), the value of `dl` will be used for this border.
     *
     * @param [db]
     *  The distance to move the bottom border to the bottom. Positive numbers
     *  will expand the rectangle (property `height` will be increased), and
     *  negative numbers will shrink the rectangle (property `height` will be
     *  decreased). If this parameter will be omitted (one, two, or three
     *  parameters passed only), the value of `dt` will be used for this border
     *  (if `dt` has been omitted too, `dl` will be used).
     *
     * @returns
     *  A reference to this instance.
     */
    expandSelf(dl: number, dt = dl, dr = dl, db = dt): this {
        this.left -= dl;
        this.top -= dt;
        this.width = Math.max(0, this.width + dl + dr);
        this.height = Math.max(0, this.height + dt + db);
        return this;
    }

    /**
     * Moves the location of this rectangle in-place.
     *
     * @param dx
     *  The horizontal move distance. Negative values will move the rectangle
     *  to the left.
     *
     * @param dy
     *  The vertical move distance. Negative values will move the rectangle up.
     *
     * @returns
     *  A reference to this instance.
     */
    translateSelf(dx: number, dy: number): this {
        this.left += dx;
        this.top += dy;
        return this;
    }

    /**
     * Scales the location of this rectangle in-place. The start position and
     * size of this rectangle will be scaled relative to the origin (0, 0).
     *
     * @param fx
     *  The horizontal scaling factor. MUST be positive.
     *
     * @param [fy]
     *  The vertical scaling factor. MUST be positive. If omitted, the value of
     *  "fx" will be usd instead.
     *
     * @returns
     *  A reference to this instance.
     */
    scaleSelf(fx: number, fy = fx): this {
        this.left *= fx;
        this.top *= fy;
        this.width *= fx;
        this.height *= fy;
        return this;
    }

    /**
     * Rotates this rectangle by 90 degrees in-place around its center point.
     *
     * @returns
     *  A reference to this instance.
     */
    rotateSelf(): this {
        const diff = this.width - this.height;
        this.left += diff / 2;
        this.top -= diff / 2;
        this.width -= diff;
        this.height += diff;
        return this;
    }

    /**
     * Rounds all properties of this rectangle to integers.
     *
     * @returns
     *  A reference to this instance.
     */
    roundSelf(): this {
        this.left = Math.round(this.left);
        this.top = Math.round(this.top);
        this.width = Math.round(this.width);
        this.height = Math.round(this.height);
        return this;
    }

    /**
     * Returns the location of this rectangle as CSS property set.
     *
     * @param [unit="px"]
     *  The CSS unit to be added to the CSS property values.
     *
     * @returns
     *  The location of this rectangle as CSS property set.
     */
    toCSS(unit = "px"): CSSRectangle {
        return { left: `${this.left}${unit}`, top: `${this.top}${unit}`, width: `${this.width}${unit}`, height: `${this.height}${unit}` };
    }

    /**
     * Returns the string representation of this rectangle, for debug logging.
     *
     * @returns
     *  The string representation of this rectangle, for debug logging.
     */
    toString(): string {
        return `{l=${this.left} t=${this.top} w=${this.width} h=${this.height}}`;
    }

    /**
     * Returns the location of this rectangle as plain JSON object.
     *
     * @returns
     *  The location of this rectangle as plain JSON object.
     */
    toJSON(): RectangleLike {
        return { left: this.left, top: this.top, width: this.width, height: this.height };
    }
}
