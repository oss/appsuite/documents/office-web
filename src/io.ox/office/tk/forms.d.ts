/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { NodeSource, NodeOrJQuery, AnyKeyEvent, IconId } from "@/io.ox/office/tk/dom";

// types ======================================================================

export interface CreateCaptionOptions {
    label?: string | null;
    labelStyle?: Partial<CSSStyleDeclaration>;
    icon?: IconId | null;
    iconStyle?: Partial<CSSStyleDeclaration>;
    tooltip?: string | null;
    textAlign?: "left" | "center" | "right";
    attributes?: Dict;
}

export interface CreateButtonNodeOptions extends CreateCaptionOptions {
    classes?: string | null;
}

export interface CursorFocusNavigationOptions {
    homeEnd?: boolean;
}

// constants ==================================================================

export const HIDDEN_CLASS: string;
export const SELECTED_SELECTOR: string;

// functions ==================================================================

export function isVisibleNode(nodes: NodeSource): boolean;
export function showNodes(nodes: NodeSource, visible: boolean): void;
export function setToolTip(nodes: NodeSource, tooltip: Nullable<string>): void;
export function createCaptionNode(options?: CreateCaptionOptions): JQuery<HTMLSpanElement>;
export function getButtonValue(nodes: NodeSource): unknown;
export function triggerClickForKey(event: AnyKeyEvent): void;
export function enableCursorFocusNavigation(node: NodeOrJQuery, options?: CursorFocusNavigationOptions): void;
