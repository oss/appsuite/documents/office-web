/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { is, itr, ary, set, map, jpromise } from "@/io.ox/office/tk/algorithms";
import { DEBUG, registerDebugCommand } from "@/io.ox/office/tk/config";

// types ======================================================================

/**
 * Configuration options for `DObject` instances.
 */
export interface DObjectConfig {

    /**
     * Ownership kind of the instance.
     *
     * By default, instances that are not owned by any other object will be
     * reported as "orphaned" by the debug command `office.debug.objects` in
     * the browser console. A few objects need to be marked so that they will
     * not be reported as orphaned.
     *
     * - `singleton`: Used for objects that will be created globally and are
     *   expected to NEVER be destroyed. Destroying a singleton will produce a
     *   warning in the browser console.
     *
     * - `root`: Used for objects are known to not have a parent.
     *
     * - `private`: Other objects whose ownership will be managed internally by
     *   a class implementation. They will never be reported as orphaned.
     */
    _kind?: "singleton" | "root" | "private";
}

/**
 * The types of objects that can be owned by an instance of class `DObject`,
 * which will be released automatically on destruction.
 */
export type Releasable = Destroyable | Disconnectable;

/**
 * All types of values that can be registered as members owned by an instance
 * of class `DObject`, which will be released automatically on destruction. The
 * elements of arrays, sets, and maps will all be released individually, and
 * the collections will be cleared.
 */
export type MemberType = Releasable | Releasable[] | Set<Releasable> | Map<unknown, Releasable>;

/**
 * Type definition of an event handler function bound to the specified object
 * type as calling context.
 *
 * @template ThisT
 *  The type of the calling context the handler will be bound to.
 *
 * @template ArgsT
 *  The types of the arguments the handler will be invoked with.
 */
export type HandlerFn<ThisT, ArgsT extends unknown[]> = (this: ThisT, ...args: ArgsT) => void;

/**
 * Options for awaiting the result of promises or promise-like values.
 */
export interface OnPromiseOptions {

    /**
     * If set to `true`, *all* input values (also plain values and settled
     * JQuery promises) will be handled asynchronously in the next scheduler
     * tick, which mimics the behavior of `Promise#then`.
     *
     * By default, plain values and already settled JQuery promises will be
     * handled immediately, which mimics the behavior of old JQuery 2.x
     * promises in general, as well as the synchronous behavior of the methods
     * `JPromise#done`, `JPromise#fail`, and `JPromise#always`.
     */
    async?: boolean;
}

/**
 * Result descriptor of a fulfilled promise.
 */
export interface PromiseFulfilResult<FT> {
    fulfilled: true;
    value: FT;
}

/**
 * Result descriptor of a rejected promise.
 */
export interface PromiseRejectResult {
    fulfilled: false;
    value: unknown;
}

/**
 * Result descriptor of a settled promise.
 */
export type PromiseSettleResult<FT> = PromiseFulfilResult<FT> | PromiseRejectResult;

// private globals ============================================================

const enum DestroyState {
    /** Method `destroy` is currently running. */
    DESTROYING = 1,
    /** Method `destroy` has finished. */
    DESTROYED = 2,
}

/** Pool of all living `DObject` instances, with "singleton" flags. */
const instancePool = new Map<DObject, DObjectConfig["_kind"]>();

/** Weak pool of all destroyed `DObject` instances. */
const destroyedPool = new WeakMap<DObject, number>();

// private functions ==========================================================

/**
 * Disconnects and/or destroys an object that is owned by an instance of
 * `DObject`.
 *
 * @param obj
 *  The object to be released.
 */
function releaseObject(obj: Releasable): void {
    if (is.disconnectable(obj)) { obj.disconnect(); }
    if (is.destroyable(obj)) { obj.destroy(); }
}

/**
 * Disconnects and/or destroys a member of `DObject`.
 *
 * @param member
 *  The member to be released. Can be a single object, or an array, a set, or
 *  a map with member objects.
 */
function releaseMember(member: MemberType): void {
    if (is.array(member)) {
        for (const obj of member) { releaseObject(obj); }
        member.length = 0;
    } else if ((member instanceof Set) || (member instanceof Map)) {
        for (const obj of member.values()) { releaseObject(obj); }
        member.clear();
    } else {
        releaseObject(member);
    }
}

// class DObject ==============================================================

/**
 * A generic base class for all classes that need globally unique identifiers
 * for the instances, or want to register destructor code that will be executed
 * when the method `destroy` has been invoked ("DObject" is a shortcut for
 * "destroyable object").
 *
 * To register destructor code, the subclass must implement the synchronous
 * method `destructor` (which MUST call its super destructor by itself).
 */
export class DObject implements Identifiable, Destroyable {

    /** Counter for globally unique instance identifiers. */
    static #uidCounter = 0;

    // static functions -------------------------------------------------------

    /**
     * Creates a globally unique string to be used as object identifier.
     *
     * @returns
     *  A globally unique string to be used as object identifier.
     */
    static makeUid(): string {
        return `uid${(DObject.#uidCounter += 1)}`;
    }

    /**
     * Returns whether the passed value is an instance of `DObject` that has
     * been destroyed already.
     *
     * @param value
     *  The value to be checked.
     *
     * @returns
     *  Whether the passed value is a destroyed instance of `DObject`.
     */
    static isDestroyed(value: unknown): value is never {
        return (value instanceof DObject) && value.destroyed;
    }

    /**
     * Logs statistics about all existing instances of `DObject` to the browser
     * console. Useful to detect memory leaks.
     */
    static logUsageStats(): void {

        // maps with instance counts by class name
        type CountMap = Map<string, number>;
        const allCounts: CountMap = new Map();
        const singletonCounts: CountMap = new Map();
        const rootCounts: CountMap = new Map();
        const orphanCounts: CountMap = new Map();

        // increases the instance count for a class
        const incCount = (counts: CountMap, obj: DObject): void => {
            map.add(counts, obj.constructor.name, 1);
        };

        // set of all orphaned objects
        const orphanSet = set.from(instancePool, ([obj, kind]) => kind ? undefined : obj);

        // removes an object and all its descendants from "orphanSet"
        const removeObject = (member: Releasable): void => {
            if (member instanceof DObject) {
                removeDescendants(member);
                orphanSet.delete(member);
            }
        };

        // removes the descendants of an object from "orphanSet"
        const removeDescendants = (obj: DObject): void => {
            if (!obj.#members) { return; }
            for (const member of obj.#members) {
                if (is.array(member) || (member instanceof Set) || (member instanceof Map)) {
                    member.forEach(removeObject);
                } else {
                    removeObject(member);
                }
            }
        };

        // count all registered instances, and reduce "orphanSet"
        for (const [obj, kind] of instancePool) {
            incCount(allCounts, obj);
            switch (kind) {
                case "singleton": incCount(singletonCounts, obj); break;
                case "root":      incCount(rootCounts, obj);      break;
                default:          /* nothing to do */
            }
            removeDescendants(obj);
        }

        // count remaining orphans
        for (const obj of orphanSet) {
            incCount(orphanCounts, obj);
        }

        // writes a list with class names and instance counts
        const dumpList = (title: string, counts: CountMap): void => {
            const total = itr.reduce(counts.values(), 0, (sum, count) => sum + count);
            const lines = [`${title}: ${total}`];
            if (counts.size) {
                const array = ary.sortFrom(counts, ([, count]) => -count);
                lines.push(...array.slice(0, 25).map(([name, count]) => String(count).padStart(6, " ") + " " + name));
                if (array.length > 25) { lines.push(`   ... (${array.length - 25} more lines)`); }
            }
            window.console.log(lines.join("\n"));
        };

        dumpList("All instances", allCounts);
        dumpList("Singleton instances", singletonCounts);
        dumpList("Root instances", rootCounts);
        dumpList("Orphaned instances", orphanCounts);
    }

    // properties -------------------------------------------------------------

    /** The globally unique identifier for this object instance. */
    readonly #uid = DObject.makeUid();

    /** All own destroyable objects that will be destroyed automatically. */
    #members: Opt<MemberType[]>;

    // public getters ---------------------------------------------------------

    /**
     * The globally unique identifier for this object instance.
     */
    get uid(): string {
        return this.#uid;
    }

    /**
     * Whether this instance has been destroyed (while and after running the
     * method `destroy`).
     */
    get destroyed(): boolean {
        return destroyedPool.has(this);
    }

    // constructor ------------------------------------------------------------

    constructor(config?: DObjectConfig) {
        // add instance to global pool of all living instances
        if (DEBUG) { instancePool.set(this, config?._kind); }
    }

    // public methods ---------------------------------------------------------

    /**
     * Registers a callback function for the passed value or promise awaiting
     * its fulfillment. If the promise will reject, or if this instance will be
     * destroyed before the promise fulfils, the callback function will not be
     * invoked.
     *
     * By default, the callback function will be invoked immediately when
     * calling `onFulfilled` with a plain value or a fulfilled JQuery promise
     * (but never for fulfilled native promises whose state and value cannot be
     * examined synchronously).
     *
     * When passing the option `async`, the callback will not be invoked before
     * the next scheduler tick, even for plain values, or for fulfilled JQuery
     * promises, which mimics the behavior of `Promise#then`.
     *
     * @param data
     *  The value or promise to be awaited.
     *
     * @param handler
     *  The callback function to be invoked when the promise fulfils. Will be
     *  invoked with this instance as calling context, and receives the result
     *  value carried with the promise.
     *
     * @param [options]
     *  Optional parameters.
     */
    onFulfilled<FT>(data: MaybeAsync<FT>, handler: HandlerFn<this, [result: FT]>, options?: OnPromiseOptions): void {

        // callback function that checks "destroyed" state and invokes the handler
        const fn = (result: FT): void => { if (!this.destroyed) { handler.call(this, result); } };

        // handle all kinds of input arguments
        if (data instanceof Promise) {
            // native promises: always asynchronously
            jpromise.floating(data.then(fn));
        } else if (jpromise.is(data)) {
            // JQuery promises: use the `done()` handler for synchronous mode
            if (options?.async) {
                jpromise.floating(data.then(fn));
            } else {
                data.done(fn);
            }
        } else if (options?.async) {
            // plain values with "async" option: use a native promise to defer callback
            jpromise.floating(Promise.resolve(data).then(fn));
        } else {
            // plain values: call immediately
            fn(data);
        }
    }

    /**
     * Registers a callback function for the passed value or promise awaiting
     * its rejection. If a plain value has passed, or if the promise will
     * fulfil, or if this instance will be destroyed before the promise
     * rejects, the callback function will not be invoked.
     *
     * By default, the callback function will be invoked immediately when
     * calling `onRejected` with a rejected JQuery promise (but never for
     * rejected native promises whose state and value cannot be examined
     * synchronously).
     *
     * When passing the option `async`, the callback will not be invoked before
     * the next scheduler tick, even for rejected JQuery promises, which mimics
     * the behavior of `Promise#catch`.
     *
     * @param data
     *  The value or promise to be awaited.
     *
     * @param handler
     *  The callback function to be invoked when the promise rejects. Will be
     *  invoked with this instance as calling context, and receives the reason
     *  for rejection carried with the promise.
     *
     * @param [options]
     *  Optional parameters.
     */
    onRejected(data: MaybeAsync<unknown>, handler: HandlerFn<this, [reason: unknown]>, options?: OnPromiseOptions): void {

        // callback function that checks "destroyed" state and invokes the handler
        const fn = (reason: unknown): void => { if (!this.destroyed) { handler.call(this, reason); } };

        // handle all kinds of input arguments
        if (data instanceof Promise) {
            // native promises: always asynchronously
            data.catch(fn);
        } else if (jpromise.is(data)) {
            // JQuery promises: use the `fail()` handler for synchronous mode
            if (options?.async) {
                data.catch(fn);
            } else {
                data.fail(fn);
            }
        }

        // never invoke the callback for plain values
    }

    /**
     * Registers a callback function for the passed value or promise awaiting
     * its settlement (fulfillment or rejection). If this instance will be
     * destroyed before the promise settles, the callback function will not be
     * invoked.
     *
     * By default, the callback function will be invoked immediately when
     * calling `onSettled` with a settled JQuery promise or a plain value (but
     * never for native promises whose state and value cannot be examined
     * synchronously).
     *
     * When passing the option `async`, the callback will not be invoked before
     * the next scheduler tick, even for JQuery promises, which mimics the
     * behavior of `Promise#finally`.
     *
     * @param data
     *  The value or promise to be awaited.
     *
     * @param handler
     *  The callback function to be invoked when the promise settles. Will be
     *  invoked with this instance as calling context, and receives a result
     *  object with a boolean property specifying whether the promise was
     *  fulfilled or rejected, and the fulfillment result or rejection reason.
     *
     * @param [options]
     *  Optional parameters.
     */
    onSettled<FT>(data: MaybeAsync<FT>, handler: HandlerFn<this, [PromiseSettleResult<FT>]>, options?: OnPromiseOptions): void {

        // callback function that checks "destroyed" state and invokes the handler
        const fulfilFn = (value: FT): void => { if (!this.destroyed) { handler.call(this, { fulfilled: true, value }); } };
        const rejectFn = (value: unknown): void => { if (!this.destroyed) { handler.call(this, { fulfilled: false, value }); } };

        // handle all kinds of input arguments
        if (data instanceof Promise) {
            // native promises: always asynchronously
            jpromise.floating(data.then(fulfilFn, rejectFn));
        } else if (jpromise.is(data)) {
            // JQuery promises: use the `fail()` handler for synchronous mode
            if (options?.async) {
                jpromise.floating(data.then(fulfilFn, rejectFn));
            } else {
                data.done(fulfilFn);
                data.fail(rejectFn);
            }
        } else if (options?.async) {
            // plain values with "async" option: use a native promise to defer callback
            jpromise.floating(Promise.resolve().then(() => fulfilFn(data)));
        } else {
            // plain values: call immediately
            fulfilFn(data);
        }
    }

    /**
     * Destroys this object. Invokes all existing `destructor` methods in the
     * class hierarchy in reversed order. Afterwards, all properties of this
     * instance will be deleted (except the property `uid`), and a single
     * property `destroyed` set to `true` will be inserted.
     */
    destroy(): void {

        // warn when destroying singletons
        if (DEBUG && (instancePool.get(this) === "singleton")) {
            window.console.warn(`${this.constructor.name}: destroying a singleton`);
        }

        // mark this instance to be in "destroying" state while destructors are running
        destroyedPool.set(this, DestroyState.DESTROYING);

        // run all destructor methods
        try {
            this.destructor();
        } catch (err) {
            window.console.error(`${this.constructor.name}: unhandled exception in destructor\n`, err);
        }

        // check that "destroyed" tag that has been set in *own* destructor (i.e. no subclass has forgotten to call super destructors)
        if (destroyedPool.get(this) !== DestroyState.DESTROYED) {
            window.console.error(`${this.constructor.name}: incomplete destructor chain (missing "super.destructor" call)`);
        }
    }

    // protected methods ------------------------------------------------------

    /**
     * Teardown code invoked by method `destroy`. Subclasses MUST call their
     * super destructors manually!
     */
    protected destructor(): void {

        // release all own registered members
        if (this.#members) {
            for (let i = this.#members.length - 1; i >= 0; i -= 1) {
                releaseMember(this.#members[i]);
            }
            this.#members = undefined;
        }

        // delete all own public members, to detect any misuse after destruction
        for (const key in this) { delete this[key]; }

        // remove from global pool of all living instances
        if (DEBUG) { instancePool.delete(this); }

        // mark this instance to be in "destroyed" state (done in this destructor, to be able
        // to detect that the complete chain of super destructors has been called by the subclass)
        destroyedPool.set(this, DestroyState.DESTROYED);
    }

    /**
     * Registers the passed object as member of this instance, i.e. binds it to
     * the own lifetime. When destroying this instance, all registered members
     * will be destroyed automatically (in reversed order of their registration
     * using this method). Intended to be used for instance fields during
     * construction.
     *
     * @example
     *  class Example extends DObject {
     *      x = this.member(new X());
     *      #y = this.member(new Set<Y>());
     *      ...
     *  }
     *
     * @param member
     *  The object to be bound to this instance. Can also be arrays, sets, or
     *  maps containing destroyable elements.
     *
     * @returns
     *  The passed object.
     */
    protected member<T extends MemberType>(member: T): T {
        (this.#members ??= []).push(member);
        return member;
    }
}

// global initialization ======================================================

registerDebugCommand("objects", DObject.logUsageStats);
