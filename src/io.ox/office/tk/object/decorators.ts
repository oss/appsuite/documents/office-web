/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fun, map } from "@/io.ox/office/tk/algorithms";

import type { EObject, DebounceOptions } from "@/io.ox/office/tk/object/eobject";

// types ======================================================================

export type MethodDecoratorContext<ThisT extends object, RetT, ArgsT extends unknown[]> = ClassMethodDecoratorContext<ThisT, ThisFuncType<ThisT, RetT, ArgsT>>;

/**
 * A decorator function for class instance methods and static class functions.
 *
 * @param method
 *  The original method being decorated.
 *
 * @param context
 *  The decorator context object.
 *
 * @returns
 *  The new method intended to replace the original method.
 */
export type MethodDecorator<ThisT extends object, RetT, ArgsT extends unknown[]> = (
    methodFn: ThisFuncType<ThisT, RetT, ArgsT>,
    context: MethodDecoratorContext<ThisT, RetT, ArgsT>
) => ThisFuncType<ThisT, RetT, ArgsT>;

export type AnyMethodDecorator = MethodDecorator<any, any, any[]>;

// decorators =================================================================

/**
 * A decorator for class methods (instance and static) that guards the methods
 * against recursive calls.
 *
 * The decorator will wrap the method implementation. Recursive calls of the
 * guarded method will return immediately.
 */
export function guardMethod<ThisT extends object, ArgsT extends unknown[]>(
    method: ThisFuncType<ThisT, void, ArgsT>,
    _context: MethodDecoratorContext<ThisT, void, ArgsT>
): ThisFuncType<ThisT, void, ArgsT> {

    // a weak cache for every class instance with its own guarded version of `method`
    const cache = new WeakMap<ThisT, FuncType<void, ArgsT>>();

    // return the decorated instance method
    return function (this, ...args) {
        // resolve wrapped version of `method` from cache, or create a new version on first usage
        const wrapper = map.upsert(cache, this, () => fun.guard(method.bind(this)));
        // invoke the `guard`-wrapped method
        wrapper(...args);
    };
}

/**
 * A decorator for class methods (instance and static) that invokes the method
 * implementation once per instance, and caches the result of the method call
 * for subsequent invocations.
 *
 * The decorator will wrap the method implementation. On first invocation (per
 * class instance), it will call the original method and store the result (a
 * return value or a thrown exception) in an internal cache. Subsequent
 * invocations will always result in the same behavior (either return the
 * cached value, or rethrow the cached exception).
 */
export function onceMethod<ThisT extends object, RetT, ArgsT extends unknown[]>(
    method: ThisFuncType<ThisT, RetT, ArgsT>,
    _context: MethodDecoratorContext<ThisT, RetT, ArgsT>
): ThisFuncType<ThisT, RetT, ArgsT> {

    // a weak cache for every class instance with its own `once`-wrapped version of `methodFn`
    const cache = new WeakMap<ThisT, FuncType<RetT, ArgsT>>();

    // return the decorated instance method
    return function (this, ...args) {
        // resolve wrapped version of `method` from cache, or create a new version on first usage
        const wrapper = map.upsert(cache, this, () => fun.once(method.bind(this)));
        // invoke the `once`-wrapped method
        return wrapper(...args);
    };
}

/**
 * A decorator factory for class method decorators (instance and static) that
 * caches multiple results of method invocations using a hash function for
 * subsequent invocations.
 *
 * The decorator will wrap the method implementation. On every invocation, the
 * hash function will be called with the actual arguments passed to the method
 * first. If the internal cache contains a result for the class instance and
 * hash value, it will be replicated (either by returning the cached value, or
 * by rethrowing the cached exception). Otherwise, the original method will be
 * called, and the result will be stored in the cache.
 *
 * @param hash
 *  The hash function. Receives the arguments passed to the method invocation.
 *
 * @returns
 *  The method decorator function.
 */
export function memoizeMethod<ThisT extends object, RetT, HashT, ArgsT extends unknown[]>(
    hash: ThisFuncType<ThisT, HashT, ArgsT>
): MethodDecorator<ThisT, RetT, ArgsT> {

    // return the decorator function that will patch a method descriptor
    // eslint-disable-next-line @typescript-eslint/no-shadow
    return function memoizeMethod(method): ThisFuncType<ThisT, RetT, ArgsT> {

        // a weak cache for every class instance with its own `memoize`-wrapped version of `methodFn`
        const cache = new WeakMap<ThisT, FuncType<RetT, ArgsT>>();

        // redefine the instance method
        return function (this, ...args) {
            // resolve wrapped version of `method` from cache, or create a new version on first usage
            const wrapperFn = map.upsert(cache, this, () => fun.memoize(method.bind(this), hash.bind(this)));
            // invoke the `memoize`-wrapped method
            return wrapperFn(...args);
        };
    };
}

/**
 * A *decorator factory* that creates decorators for instance methods of
 * subclasses of `EObject` to debounce the invocations of the method
 * implementation.
 *
 * @param [options]
 *  Optional parameters defining the behavior of the debounced method.
 *
 * @returns
 *  The method decorator function.
 */
export function debounceMethod<ThisT extends EObject>(options?: DebounceOptions): MethodDecorator<ThisT, void, []> {

    // minimum and maximum delay time
    const minDelay = options?.delay ?? 0;
    const maxDelay = options?.maxDelay ?? 0;

    // return the decorator function that will patch a method descriptor
    // eslint-disable-next-line @typescript-eslint/no-shadow
    return function debounceMethod(methodFn) {

        // optionally, call the method immediately on every invocation
        const immediateFn = options?.immediate ? methodFn : undefined;

        // weak caches for every `EObject` instance with its own min/max timers
        // (JS engines will collect `WeakMap` entries with garbage-collected keys automatically)
        const minTimerCache = new WeakMap<ThisT, Abortable>();
        const maxTimerCache = new WeakMap<ThisT, Abortable>();

        // redefine the instance method according to debounce type
        switch (minDelay) {

            // create a new microtask
            case "microtask":
                return function (this) {
                    // queue a new microtask, unless there is already one
                    map.upsert(maxTimerCache, this, () => this.queueMicrotask(() => {
                        maxTimerCache.delete(this);
                        methodFn.call(this);
                    }));
                    // optionally, call the method immediately
                    immediateFn?.call(this);
                };

            // request an animation frame
            case "animationframe":
                return function (this) {
                    // request a new animation frame, unless a request is already running
                    map.upsert(maxTimerCache, this, () => this.requestAnimationFrame(() => {
                        maxTimerCache.delete(this);
                        methodFn.call(this);
                    }));
                    // optionally, call the method immediately
                    immediateFn?.call(this);
                };

            // setup min/max timeouts
            default:
                return function (this) {

                    // timer callback that will abort all timers and invoke the wrapped class method
                    const invokeFn = (): void => {
                        map.remove(minTimerCache, this)?.abort();
                        map.remove(maxTimerCache, this)?.abort();
                        methodFn.call(this);
                    };

                    // on every call, abort old timer and create a new timer for minimum delay
                    minTimerCache.get(this)?.abort();
                    minTimerCache.set(this, this.setTimeout(invokeFn, minDelay));

                    // on first call, create a timer for the maximum delay
                    if (maxDelay > 0) {
                        map.upsert(maxTimerCache, this, () => this.setTimeout(invokeFn, maxDelay));
                    }

                    // optionally, call the method immediately
                    immediateFn?.call(this);
                };
        }
    };
}
