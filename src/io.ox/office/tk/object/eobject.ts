/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PromiseRejectFn, PromiseExecutorFn } from "@/io.ox/office/tk/algorithms";
import { is, str, fun, jpromise } from "@/io.ox/office/tk/algorithms";
import type { PropertySet } from "@/io.ox/office/tk/containers";

import type {
    InferEventArgs, JQueryEmitter, CoreEmitter, DocsEmitter, TypedEmitter, AllEventsEmitter, AnyEmitter,
    EventHandlerFn, TypedEventHandlerFn, TypedAllEventsHandlerFn,
    DOMEventHandlerFn, JQueryEventHandlerFn, CoreEventHandlerFn, CoreAllEventsHandlerFn,
    EventBindOptions, DOMBindOptions, JQueryBindOptions, ResolveDOMEventKeys
} from "@/io.ox/office/tk/events";
import { EventHub, resolveEventTypes, globalEvents } from "@/io.ox/office/tk/events";
import { AbortError } from "@/io.ox/office/tk/object/aborterror";
import { type HandlerFn, DObject } from "@/io.ox/office/tk/object/dobject";
import { EmitterWrapperContainer } from "@/io.ox/office/tk/object/emitterwrapper";
import type { Logger } from "@/io.ox/office/tk/utils/logger";

const { floor, max } = Math;

// types ======================================================================

/**
 * Event handler for a changed property value in a `PropertySet` instance.
 *
 * @template T
 *  The context type of the event handler (type of `this`).
 *
 * @template S
 *  The data type of the entire property dictionary.
 *
 * @template K
 *  The name of the changed property.
 */
export type ChangedPropHandlerFn<T, S extends object, K extends keyof S> = EventHandlerFn<T, Pair<S[K]>>;

/**
 * Type definition of a handler function for a timer or interval timer, bound
 * to the specified object type as calling context.
 */
export type TimerFn<ThisT> = HandlerFn<ThisT, []>;

/**
 * Settings for an interval timer.
 */
export interface IntervalTimerOptions {

    /**
     * The initial delay (in milliseconds) for the first invocation of the
     * callback function. Default is the value set in the option `interval`.
     */
    delay?: number;

    /**
     * The interval time (in milliseconds) between invocations of the callback
     * function.
     */
    interval: number;

    /**
     * The maximum number of iterations. If omitted, the interval timer will
     * run forever (until the `breakLoop` function has been called by the
     * callback function, or the timer has been be aborted explicitly).
     */
    count?: number;
}

/**
 * Settings for the timing of asynchronous loops.
 */
export interface AsyncTimingOptions {

    /**
     * The duration (in milliseconds) of a single time slice. The loop callback
     * function will be invoked repeatedly until the time has elapsed. Set this
     * value to `0` to enforce that every iteration runs in its own time slice.
     * Default value is `200`.
     */
    slice?: number;

    /**
     * The interval time (in milliseconds) between two time slices. Default
     * value is `10`.
     */
    interval?: number;
}

/**
 * All settings for asynchronous loops.
 */
export interface AsyncLoopOptions extends AsyncTimingOptions {

    /**
     * The maximum number of iterations to be executed. If omitted, the loop
     * will run forever (until the `breakLoop` function has been called).
     */
    count?: number;
}

/**
 * Type definition of a handler function in an asynchronous loop.
 */
export type AsyncLoopHandlerFn<ThisT, RT, ArgsT extends unknown[]> = (this: ThisT, ...args: ArgsT) => MaybeAsync<RT>;

/**
 * Configuration options for debounced functions created with the method
 * `EObject#debounce`.
 */
export interface DebounceOptions {

    /**
     * The delay time (in milliseconds) for the deferred callback, or a keyword
     * for using a different debounce method than browser timeouts.
     *
     * - A numeric delay time will use browser timeouts. The delay will restart
     *   after each call of the resulting debounced function, causing the
     *   execution of the deferred callback to be postponed until the debounced
     *   function has not been called again during the delay (this is the
     *   behavior of the function `_.debounce`).
     *
     * - The keyword `microtask` will queue a microtask that runs immediately
     *   after the current script task (based on `window.queueMicrotask`).
     *
     * - The keyword `animationframe` will request an animation frame for the
     *   debounced function (based on `window.requestAnimationFrame`).
     *
     * Default value is `0`.
     */
    delay?: number | "microtask" | "animationframe";

    /**
     * If specified, a delay time used as upper limit to invoke the deferred
     * callback after the first invocation of the resulting debounced function,
     * even if it has been called repeatedly afterwards and the normal delay
     * time is still running. If omitted, no upper limit will be active, the
     * deferred callback may be debounced forever, if the debounced function
     * will be called repeatedly.
     */
    maxDelay?: number;

    /**
     * If set to `true`, the debounced function implementation will also be
     * invoked immediately when calling the debounced function wrapper. Default
     * value is `false`.
     */
    immediate?: boolean;
}

// private types --------------------------------------------------------------

/**
 * A wrapper object containing all settings for a running timer.
 */
interface TimerContext<HandleT> {
    handle: HandleT;
    timer: Abortable;
    aborted?: true;
    clear(): void;
    done(): void;
}

// class BreakLoopToken =======================================================

/**
 * A special exception class that can be thrown from an asynchronous loop to
 * indicate leaving the loop early. Used by function `breakLoop`.
 */
export class BreakLoopToken extends Error {

    override readonly name = "BreakLoopToken";

    constructor() {
        super("break loop");
    }
}

// private functions ==========================================================

/**
 * Creates and returns a helper context object for a running timer.
 *
 * @param handle
 *  The handle of the system timer to be wrapped by the context object.
 *
 * @param clearFn
 *  A function used to abort the system timer specified by a handle.
 *
 * @returns
 *  The helper context object containing a new `Abortable` to be returned to
 *  external code. The caller may change the `handler` property, e.g. when
 *  implementing interval loops with separate system timers.
 */
function createTimerContext<HandleT>(handle: HandleT, clearFn: FuncType<void, [HandleT]>): TimerContext<HandleT> {

    // helper function that aborts the system timer (use `context.handle`
    // because it may differ from passed `handle`, e.g. for interval timers)
    const clear = (): void => clearFn(context.handle);

    // helper function that marks the timer as finished
    const done = (): void => { context.aborted = true; };

    // create the abortable timer object
    const timer: Abortable = { abort() { clear(); done(); } };

    // create and return the timer context helper object
    const context: TimerContext<HandleT> = { handle, timer, clear, done };
    return context;
}

// public functions ===========================================================

/**
 * Throws a special exception that will immediately leave an asynchronous loop.
 * Can be used similarly to the `break` statement in native for-loops.
 */
export function breakLoop(): never {
    throw new BreakLoopToken();
}

// class EObject ==============================================================

/**
 * A generic base class that extends the functionality of a `DObject`. Adds
 * helper methods for event handling, dealing with asynchronous code, and
 * creating timeouts (consider "EObject" to be a shortcut for "extended object"
 * or "event enabled object").
 *
 * All event handling, asynchronous code execution, and timing is bound to the
 * lifetime of an instance of this class. If an instance will be destroyed, all
 * event handlers will be deregistered, and none of the pending actions (timers
 * etc.) will be executed anymore.
 *
 * @template EvtMapT
 *  The type mapping interface for the supported event types. The interface
 *  keys are the supported event type keys, the interface property types are
 *  tuples for the types of the arguments emitted for that event.
 */
export class EObject<EvtMapT = Empty> extends DObject implements DocsEmitter<EvtMapT> {

    // for runtime type detection of `DocsEmitter` interface
    // eslint-disable-next-line @typescript-eslint/class-literal-property-style
    get __typeTag(): "DocsEmitter" { return "DocsEmitter"; }

    // container for all event handlers of events emitted by this instance
    #hub: Opt<EventHub<Record<PropertyKey, unknown[]>>>;

    // all event handlers bound to external event emitters
    #handlers: Opt<EmitterWrapperContainer>;

    // the reject functions of all pending promises
    #promises: Opt<Map<symbol, PromiseRejectFn>>;

    // constructor ------------------------------------------------------------

    protected override destructor(): void {

        // reject all running promises
        if (this.#promises) {
            const error = new AbortError(true);
            this.#promises.forEach(reject => reject(error));
            this.#promises = undefined;
        }

        // unbind from all external event emitters
        if (this.#handlers) {
            this.#handlers.destroy();
            this.#handlers = undefined;
        }

        // destroy the internal event hub
        if (this.#hub) {
            this.#hub.destroy();
            this.#hub = undefined;
        }

        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Adds an event handler to the specified event emitter. On destruction of
     * this instance, the event handler will be removed from the event emitter
     * automatically.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param type
     *  The type name of the event to start listening to. Can also be an array
     *  of event names.
     *
     * @param handler
     *  The callback function to be invoked if the event emitter fires. Event
     *  handlers must be synchronous, and MUST NOT throw exceptions. Will be
     *  invoked with this instance as calling context, and receives all
     *  parameters fired by the event emitter.
     *
     * @param [options]
     *  Optional parameters.
     */
    listenTo<T extends EventTarget, K extends ResolveDOMEventKeys<T>>(emitter: T, type: OrArray<K>, handler: DOMEventHandlerFn<this, T, K>, options?: DOMBindOptions): void;
    listenTo<K extends keyof JTriggeredEventMap>(emitter: JQueryEmitter, type: OrArray<K>, handler: JQueryEventHandlerFn<this, K>, options?: JQueryBindOptions): void;
    listenTo<E, K extends keyof E>(emitter: TypedEmitter<E>, type: OrArray<K>, handler: TypedEventHandlerFn<this, E, K>, options?: EventBindOptions): void;
    listenTo<E, K extends keyof E>(emitter: CoreEmitter<E>, type: OrArray<K>, handler: CoreEventHandlerFn<this, E, K>, options?: EventBindOptions): void;
    // implementation
    listenTo<E>(emitter: AnyEmitter<E>, type: OrArray<keyof E>, handler: AnyFunction, options?: EventBindOptions): void {
        this.#handlers ??= new EmitterWrapperContainer(this);
        this.#handlers.bind(emitter, type, handler, options);
    }

    /**
     * Removes one or more event handlers from the specified event emitter that
     * have been registered using the method `listenTo`. This method does not
     * need to be called from the destructor method though. All handlers will
     * be unbound automatically when destroying this instance.
     *
     * @param emitter
     *  The event emitter to stop listening to.
     *
     * @param [type]
     *  The type name of the event to stop listening to. Can also be an array
     *  of event names. If omitted, all event handlers registered for this
     *  instance will be removed from the event emitter.
     *
     * @param [handler]
     *  The callback function to be removed from the event emitter. If omitted,
     *  all event handlers registered for this instance with matching event
     *  type will be removed from the event emitter.
     */
    stopListeningTo<T extends EventTarget, K extends ResolveDOMEventKeys<T>>(emitter: T, type: OrArray<K>, handler?: DOMEventHandlerFn<this, T, K>): void;
    stopListeningTo<K extends keyof JTriggeredEventMap>(emitter: JQueryEmitter, type: OrArray<K>, handler?: JQueryEventHandlerFn<this, K>): void;
    stopListeningTo<E, K extends keyof E>(emitter: TypedEmitter<E>, type: OrArray<K>, handler?: TypedEventHandlerFn<this, E, K>): void;
    stopListeningTo<E, K extends keyof E>(emitter: CoreEmitter<E>, type: OrArray<K>, handler?: CoreEventHandlerFn<this, E, K>): void;
    stopListeningTo<E>(emitter: AnyEmitter<E>): void;
    // implementation
    stopListeningTo<E>(emitter: AnyEmitter<E>, type?: OrArray<keyof E>, handler?: AnyFunction): void {
        this.#handlers?.unbind(emitter, type, handler);
    }

    /**
     * Removes all event handlers for a specific scope symbol from an event
     * emitter that have been registered using the method `listenTo`. This
     * method does not need to be called from the destructor method though. All
     * handlers will be unbound automatically when destroying this instance.
     *
     * @param scope
     *  The scope symbol used when registering the event handlers.
     *
     * @param [emitter]
     *  The event emitter to stop listening to. If omitted, all registered
     *  scoped event handlers will be removed from all event emitters.
     */
    stopListeningToScope<E>(scope: symbol, emitter?: AnyEmitter<E>): void {
        this.#handlers?.unbind(emitter, undefined, undefined, scope);
    }

    /**
     * Removes all event handlers from all event emitters that have been
     * registered using the method `listenTo`. This method does not need to be
     * called from the destructor method though. All handlers will be unbound
     * automatically when destroying this instance.
     */
    stopListening(): void {
        this.#handlers?.unbind();
    }

    /**
     * Adds an all-events handler to the specified event emitter. On
     * destruction of this instance, the event handler will be removed from the
     * event emitter automatically.
     *
     * @param emitter
     *  The event emitter to start listening to.
     *
     * @param handler
     *  The callback function to be invoked if the event emitter fires. Event
     *  handlers must be synchronous, and MUST NOT throw exceptions. Will be
     *  invoked with this instance as calling context, and receives all
     *  parameters fired by the event emitter.
     *
     * @param [options]
     *  Optional parameters.
     */
    listenToAllEvents<E>(emitter: TypedEmitter<E>, handler: TypedAllEventsHandlerFn<this, E>, options?: EventBindOptions): void;
    listenToAllEvents<E>(emitter: CoreEmitter<E>, handler: CoreAllEventsHandlerFn<this, E>, options?: EventBindOptions): void;
    // implementation
    listenToAllEvents<E>(emitter: AllEventsEmitter<E>, handler: AnyFunction, options?: EventBindOptions): void {
        this.#handlers ??= new EmitterWrapperContainer(this);
        this.#handlers.bind(emitter, "*", handler, options);
    }

    /**
     * Removes one or more all-event handlers from the specified event emitter
     * that have been registered using the method `listenToAllEvents`. This
     * method does not need to be called from the destructor method though. All
     * handlers will be unbound automatically when destroying this instance.
     *
     * @param emitter
     *  The event emitter to stop listening to.
     *
     * @param [handler]
     *  The callback function to be removed from the event emitter. If omitted,
     *  all event handlers registered for this instance with matching event
     *  type will be removed from the event emitter.
     */
    stopListeningToAllEvents<E>(emitter: TypedEmitter<E>, handler?: TypedAllEventsHandlerFn<this, E>): void;
    stopListeningToAllEvents<E>(emitter: CoreEmitter<E>, handler?: CoreAllEventsHandlerFn<this, E>): void;
    // implementation
    stopListeningToAllEvents<E>(emitter: AllEventsEmitter<E>, handler?: AnyFunction): void {
        this.#handlers?.unbind(emitter, "*", handler);
    }

    /**
     * Adds an event handler to the global event hub. On destruction of this
     * instance, the event handler will be removed from the event emitter
     * automatically. This method is just a convenience shortcut for calling
     * `this.listenTo(globalEvents, ...);`.
     *
     * @param type
     *  The type name of the global event to start listening to.
     *
     * @param handler
     *  The callback function to be invoked if the global event fires. Event
     *  handlers must be synchronous, and MUST NOT throw exceptions. Will be
     *  invoked with this instance as calling context, and receives all
     *  parameters fired by the global event.
     *
     * @param [options]
     *  Optional parameters.
     */
    listenToGlobal<K extends keyof DocsGlobalEventMap>(type: K, handler: TypedEventHandlerFn<this, DocsGlobalEventMap, K>, options?: EventBindOptions): void {
        this.listenTo(globalEvents, type, handler, options);
    }

    /**
     * Removes one or more event handlers from the global event hub that have
     * been registered using the method `listenToGlobal`. This method does not
     * need to be called from the destructor method though. All handlers will
     * be unbound automatically when destroying this instance.
     *
     * @param type
     *  The type name of the global event to stop listening to.
     *
     * @param [handler]
     *  The callback function to be removed from the global event hub. If
     *  omitted, all event handlers registered for this instance with matching
     *  event type will be removed from the global event hub.
     */
    stopListeningToGlobal<K extends keyof DocsGlobalEventMap>(type: K, handler?: TypedEventHandlerFn<this, DocsGlobalEventMap, K>): void {
        this.stopListeningTo(globalEvents, type, handler);
    }

    /**
     * Adds an event handler to a `PropertySet` that will observe one of its
     * properties. On destruction of this instance, the event handler will be
     * removed from the property set automatically.
     *
     * @param propSet
     *  The property set to be observed.
     *
     * @param propKey
     *  The key of the property to be observed.
     *
     * @param handler
     *  The callback function to be invoked if the specified property has been
     *  changed. Event handlers must be synchronous, and MUST NOT throw
     *  exceptions. Will be invoked with this instance as calling context, and
     *  receives the new value, and the old value of the property.
     *
     * @param [options]
     *  Optional parameters.
     */
    listenToProp<S extends object, K extends keyof S>(propSet: PropertySet<S>, propKey: K, handler: ChangedPropHandlerFn<this, S, K>, options?: EventBindOptions): void {
        this.listenTo(propSet, "change:props", (newProps, oldProps) => {
            // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
            if (propKey in newProps) { handler.call(this, newProps[propKey]!, oldProps[propKey]!); }
        }, options);
    }

    // DocsEmitter interface --------------------------------------------------

    /**
     * Registers an event handler at the event hub of this instance.
     *
     * @param type
     *  The type name of the event to start listening to. Can also be an array
     *  of event names.
     *
     * @param handler
     *  The callback function to be invoked for the specified event type.
     */
    on<K extends keyof EvtMapT, A extends InferEventArgs<EvtMapT, K>>(type: "*", handler: TypedAllEventsHandlerFn<this, EvtMapT, K, A>): this;
    on<K extends keyof EvtMapT>(type: OrArray<K>, handler: TypedEventHandlerFn<this, EvtMapT, K>): this;
    // implementation
    on(type: OrArray<PropertyKey>, handler: AnyFunction): this {
        this.#hub ??= this.member(new EventHub());
        this.#hub.on(type, handler.bind(this), { key: handler });
        return this;
    }

    /**
     * Registers an event handler at the event hub of this instance, that will
     * be invoked exactly once, and will be removed automatically afterwards.
     *
     * @param type
     *  The type name of the event to start listening to. Can also be an array
     *  of event names.
     *
     * @param handler
     *  The callback function to be invoked for the specified event type.
     */
    one<K extends keyof EvtMapT, A extends InferEventArgs<EvtMapT, K>>(type: "*", handler: TypedAllEventsHandlerFn<this, EvtMapT, K, A>): this;
    one<K extends keyof EvtMapT>(type: OrArray<K>, handler: TypedEventHandlerFn<this, EvtMapT, K>): this;
    // implementation
    one(type: OrArray<PropertyKey>, handler: AnyFunction): this {
        this.#hub ??= this.member(new EventHub());
        this.#hub.on(type, handler.bind(this), { key: handler, once: true });
        return this;
    }

    /**
     * Removes one or more event handlers from the event hub of this instance.
     *
     * @param [type]
     *  The type name of the event to stop listening to. Can also be an array
     *  of event names. If omitted, removes all handlers.
     *
     * @param [handler]
     *  The callback function to unbind. If omitted, removes all handlers for
     *  the specified event type.
     */
    off<K extends keyof EvtMapT, A extends InferEventArgs<EvtMapT, K>>(type: "*", handler?: TypedAllEventsHandlerFn<this, EvtMapT, K, A>): this;
    off<K extends keyof EvtMapT>(type?: OrArray<K>, handler?: TypedEventHandlerFn<this, EvtMapT, K>): this;
    // implementation
    off(types?: OrArray<PropertyKey>, handler?: AnyFunction): this {
        // do not create the event hub if it is missing
        this.#hub?.off(types, handler);
        return this;
    }

    /**
     * Emits a new event.
     *
     * @param type
     *  The type of the event to be emitted.
     *
     * @param args
     *  The additional parameters to be passed to the event handler callbacks.
     */
    trigger<K extends keyof EvtMapT>(type: K, ...args: InferEventArgs<EvtMapT, K>): this {
        // do not create the event hub if it is missing
        this.#hub?.emit(type, ...args);
        return this;
    }

    // timers -----------------------------------------------------------------

    /**
     * Invokes the passed callback function after the specified delay time. If
     * this instance will be destroyed before the delay time has been elapsed,
     * the callback function will not be invoked at all.
     *
     * @param fn
     *  The callback function to be invoked after the delay time. Will be
     *  invoked with this instance as calling context, and does not receive any
     *  arguments.
     *
     * @param [delay=0]
     *  The time (in milliseconds) to postpone execution of the passed callback
     *  function.
     *
     * @returns
     *  A small object with method `abort` that can be used to abort the
     *  running timer.
     */
    setTimeout(fn: TimerFn<this>, delay?: number): Abortable {

        // create a new system timer that invokes the callback function
        const handle = window.setTimeout(() => {
            try {
                if (!this.destroyed) { fn.call(this); }
            } catch (err) {
                window.console.error(`${this.constructor.name}: unhandled exception in timer\n`, err);
            }
            context.done();
        }, delay);

        // create the timer context helper, return the timer object
        const context = createTimerContext(handle, window.clearTimeout);
        return context.timer;
    }

    /**
     * Invokes the passed callback function repeatedly after the specified
     * interval time. If this instance will be destroyed while the interval
     * timer is running, the callback function will not be invoked anymore.
     *
     * @param fn
     *  The callback function to be invoked repeatedly. Will be invoked with
     *  this instance as calling context. Receives the zero-based loop index as
     *  first parameter.
     *
     * @param options
     *  The interval time (in milliseconds) between two invocations of the
     *  callback function; or an options object with the interval time and
     *  additional settings.
     *
     * @returns
     *  A small object with method `abort` that can be used to abort the
     *  running interval timer.
     */
    setInterval(fn: HandlerFn<this, [number]>, options: number | IntervalTimerOptions): Abortable {

        // convert number to options object
        if (is.number(options)) { options = { interval: options }; }

        // the cycle count (return immediately if not positive)
        const count = floor(options.count ?? Infinity);
        if (count <= 0) { return { abort: fun.undef }; }

        // the interval time and initial delay time
        const interval = options.interval;
        const delay = options.delay ?? interval;

        // the cycle index
        let index = 0;

        // the system timer callback to be called repeatedly
        const timerFn = (): void => {

            // invoke the callback function, stop interval timer when throwing `BreakLoopToken`
            try {
                if (!this.destroyed) { fn.call(this, index); }
            } catch (err) {
                if (err instanceof BreakLoopToken) {
                    context.aborted = true;
                } else {
                    window.console.error(`${this.constructor.name}: unhandled exception in interval timer\n`, err);
                }
            }

            // create a new system timer for next loop iteration, unless:
            // - this instance has been destroyed (even from within the callback function)
            // - `context.aborted` is true (callback has invoked `timer.abort` by itself)
            // - the maximum cycle count has been reached
            index += 1;
            if (!this.destroyed && !context.aborted && (index < count)) {
                context.handle = window.setTimeout(timerFn, interval);
            } else {
                context.done();
            }
        };

        // create the initial system timer
        const handle = window.setTimeout(timerFn, delay);

        // create the timer context helper, return the timer object
        const context = createTimerContext(handle, window.clearTimeout);
        return context.timer;
    }

    /**
     * Invokes the passed callback function in the next microtask. If this
     * instance will be destroyed before the microtask will start, the callback
     * function will not be invoked at all.
     *
     * @param fn
     *  The callback function to be invoked in the next microtask. Will be
     *  invoked with this instance as calling context, and does not receive any
     *  arguments.
     *
     * @returns
     *  A small object with method `abort` that can be used to abort the
     *  pending callback.
     */
    queueMicrotask(fn: TimerFn<this>): Abortable {

        // register a microtask that invokes the callback function
        window.queueMicrotask(() => {
            try {
                if (!context.aborted && !this.destroyed) { fn.call(this); }
            } catch (err) {
                window.console.error(`${this.constructor.name}: unhandled exception in microtask\n`, err);
            }
            context.done();
        });

        // create the timer context helper, return the timer object
        const context = createTimerContext(null, fun.undef);
        return context.timer;
    }

    /**
     * Invokes the passed callback function in the next browser animation
     * frame. If this instance will be destroyed before the animation frame is
     * ready, the callback function will not be invoked at all.
     *
     * @param fn
     *  The callback function to be invoked in the next browser animation
     *  frame. Will be invoked with this instance as calling context, and does
     *  not receive any arguments.
     *
     * @returns
     *  A small object with method `abort` that can be used to abort the
     *  pending callback.
     */
    requestAnimationFrame(fn: TimerFn<this>): Abortable {

        // request a new animation frame that invokes the callback function
        const handle = window.requestAnimationFrame(() => {
            try {
                if (!this.destroyed) { fn.call(this); }
            } catch (err) {
                window.console.error(`${this.constructor.name}: unhandled exception in animation frame\n`, err);
            }
            context.done();
        });

        // create the timer context helper, return the timer object
        const context = createTimerContext(handle, window.cancelAnimationFrame);
        return context.timer;
    }

    // async ------------------------------------------------------------------

    /**
     * Creates a new JQuery promise, invokes the passed callback function, and
     * returns the promise. The callback function is responsible to settle the
     * promise at some point by calling one of the provided functions `fulfil`
     * or `reject`.
     *
     * This function mimics the behavior of the native `Promise` constructor.
     *
     * If this instance will be destroyed while the promise is still pending,
     * it will be rejected with an `AbortError`.
     *
     * @param executorFn
     *  The callback function that will be invoked immediately. Receives the
     *  callback functions `fulfilFn` and `rejectFn` as parameters. If this
     *  function throws an exception, the returned promise will be rejected.
     *
     * @returns
     *  The new JQuery promise.
     */
    newJPromise<FT = void>(executorFn: PromiseExecutorFn<FT>): JPromise<FT> {

        // create the rejection map on demand
        this.#promises ??= new Map();
        // create a unique identifier as map key for the rejection function
        const key = Symbol("promise");

        // create the new promise, and register the rejection function for destruction
        const promise = jpromise.new<FT>((fulfilFn, rejectFn) => {
            this.#promises!.set(key, rejectFn);
            executorFn.call(this, fulfilFn, rejectFn);
        });

        // remove the rejection function from the map when the promise settles
        promise.always(() => this.#promises?.delete(key));

        return promise;
    }

    /**
     * Creates a new pending `JDeferred` object. If this instance will be
     * destroyed while the deferred is still pending, it will be rejected with
     * an `AbortError`.
     *
     * @returns
     *  The new `JDeferred` object.
     */
    newJDeferred<FT = void>(): JDeferred<FT> {

        // create the rejection map on demand
        this.#promises ??= new Map();
        // create a unique identifier as map key for the rejection function
        const key = Symbol("deferred");

        // create the new promise, and register the rejection function for destruction
        const deferred = jpromise.deferred<FT>();
        this.#promises.set(key, reason => deferred.reject(reason));

        // remove the rejection function from the map when the promise settles
        deferred.always(() => this.#promises?.delete(key));

        return deferred;
    }

    /**
     * Invokes the passed callback function repeatedly in an asynchronous loop.
     *
     * Tries to pack multiple (synchronous) invocations of the callback into
     * the same script execution frame, until the specified total execution
     * time of such a frame is exceeded, and continues with a new execution
     * frame in another browser timeout afterwards.
     *
     * If this instance will be destroyed while the loop is running, the
     * callback function will not be invoked anymore, and the promise will be
     * rejected with an `AbortError`.
     *
     * @param fn
     *  The callback function to be invoked repeatedly in an asynchronous loop.
     *  Receives the zero-based loop index as first parameter. Must call the
     *  `breakLoop` function to finish the loop (similar to the `break`
     *  statement in for-loops).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise representing the state and result of the asynchronous loop.
     */
    asyncLoop(
        fn: AsyncLoopHandlerFn<this, unknown, [index: number]>,
        options?: AsyncLoopOptions
    ): JPromise {

        return this.newJPromise((fulfilFn, rejectFn) => {

            // the maximum number of iterations
            const count = options?.count ?? Infinity;
            if (count <= 0) { fulfilFn(); return; }

            // the duration of a time slice
            const slice = options?.slice ?? 200;
            // the interval time between time slices
            const interval = max(10, options?.interval ?? 10);

            // current iteration index
            let index = 0;
            // start time of current time slice
            let t0 = 0;

            // returns whether the `index` points to the last iteration
            const isLoopDone = Number.isFinite(count) ? (() => index + 1 === count) : fun.false;
            // returns whether the time of the current slice has exceeded now
            const isSliceDone: FuncType<boolean> = () => slice <= (Date.now() - t0);

            // ensure correct calling context
            fn = fn.bind(this);

            // invokes the callback function repeatedly
            const loopFn = (): void => {

                // loop as long as the callback returns synchronous results
                let promise: JPromise<unknown>;
                for (; ; index += 1) {
                    promise = jpromise.invoke(fn, index);
                    if (!jpromise.isFulfilled(promise) || isLoopDone() || isSliceDone()) { break; }
                }

                // fulfilment: start the next loop step
                this.onFulfilled(promise, nextFn);

                // rejection: exit the loop
                this.onRejected(promise, err => (err instanceof BreakLoopToken) ? fulfilFn() : rejectFn(err));
            };

            // starts the next loop step (decide whether to remain in the current time slice)
            const nextFn = (): void => {

                // loop finished completely
                if (isLoopDone()) { fulfilFn(); return; }

                // update loop index
                index += 1;

                // start next time slice after the configured delay time
                if (isSliceDone()) {
                    this.setTimeout(startFn, interval);
                } else {
                    loopFn();
                }
            };

            // starts the next time slice (sets `t0` to current timestamp)
            const startFn = (): void => {
                t0 = Date.now();
                loopFn();
            };

            // start the loop immediately
            startFn();
        });
    }

    /**
     * Visits all values of the passed data source in an asynchronous loop.
     *
     * Tries to pack multiple (synchronous) invocations of the callback into
     * the same script execution frame, until the specified total execution
     * time of such a frame is exceeded, and continues with a new execution
     * frame in another browser timeout afterwards.
     *
     * If this instance will be destroyed while the loop is running, the
     * callback function will not be invoked anymore, and the promise will be
     * rejected with an `AbortError`.
     *
     * @param source
     *  The data source to be visited.
     *
     * @param fn
     *  The callback function to be invoked repeatedly in an asynchronous loop.
     *  Receives the following parameters:
     *  1. The current value from the iterator.
     *  2. A zero-based iteration index.
     *
     *  May call the `breakLoop` function to finish the loop early (similar to
     *  the `break` statement in for-loops).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise representing the state and result of the asynchronous loop.
     */
    asyncForEach<VT>(
        source: Iterable<VT>,
        fn: AsyncLoopHandlerFn<this, void, [VT, number]>,
        options?: AsyncTimingOptions
    ): JPromise {

        // immediately fetch first result, do not start loop on empty data source
        const iterator = source[Symbol.iterator]();
        let result = iterator.next();
        if (result.done) { return jpromise.resolve(); }

        // ensure correct calling context
        fn = fn.bind(this);

        // process iterator values as long as available, immediately exit the loop after the last
        // value has been consumed (do not add trailing delay after asynchronous last result)
        return this.asyncLoop(index => {
            const promise = jpromise.invoke(fn, result.value as VT, index);
            result = iterator.next();
            return result.done ? jpromise.fastThen(promise, breakLoop) : promise;
        }, options);
    }

    /**
     * Reduces all values of the passed data source in an asynchronous loop to
     * a single result value.
     *
     * Tries to pack multiple (synchronous) invocations of the callback into
     * the same script execution frame, until the specified total execution
     * time of such a frame is exceeded, and continues with a new execution
     * frame in another browser timeout afterwards.
     *
     * If this instance will be destroyed while the loop is running, the
     * callback function will not be invoked anymore, and the promise will be
     * rejected with an `AbortError`.
     *
     * @param source
     *  The data source to be visited.
     *
     * @param initial
     *  The initial value to be passed to the first invocation of the callback
     *  function.
     *
     * @param fn
     *  The callback function to be invoked repeatedly in an asynchronous loop.
     *  Receives the following parameters:
     *  1. The intermediate accumulated value (the return value of the previous
     *     invocation, or the initial value).
     *  2. The current value from the iterator.
     *  3. A zero-based iteration index.
     *
     *  May call the `breakLoop` function to finish the loop early (similar to
     *  the `break` statement in for-loops). If the function returns
     *  `undefined`, the current result value passed to the function will be
     *  used (useful for e.g. aggregating arrays or records).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise representing the state and result of the asynchronous loop.
     *  When finishing the loop successfully, the promise will be fulfilled
     *  with the return value of the last invocation of the callback function.
     */
    asyncReduce<RT, VT>(
        source: Iterable<VT>,
        initial: RT,
        fn: AsyncLoopHandlerFn<this, Opt<RT>, [RT, VT, number]>,
        options?: AsyncTimingOptions
    ): JPromise<RT> {

        // process the data source, collect intermediate result in `initial`
        const promise = this.asyncForEach(source, (value, index) => {
            return jpromise.fastThen(fn.call(this, initial, value, index), result => {
                if (result !== undefined) { initial = result; }
            });
        }, options);

        // loop finished: return final result of last callback in a promise
        return jpromise.fastThen(promise, fun.const(initial));
    }

    // protected methods ------------------------------------------------------

    /**
     * Creates a debounced function that can be called multiple times during
     * the current script execution. The deferred callback will be executed
     * later once in a browser timeout.
     *
     * If this instance will be destroyed, pending debounced function calls
     * will not be completed anymore.
     *
     * @param [directFn]
     *  A function that will be invoked immediately, every time the resulting
     *  debounced function has been called. Will be called in the context of
     *  this instance. Receives all parameters that have been passed to the
     *  debounced function. This parameter can be omitted completely.
     *
     * @param deferFn
     *  A function that will be called in a browser timeout after the resulting
     *  debounced function has been called at least once during the execution
     *  of the current script. Will be called in the context of this instance.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  The resulting debounced function that can be called multiple times, and
     *  that executes the deferred callback once after execution of the current
     *  script ends. Passes all arguments to the direct callback, and returns
     *  its result.
     */
    protected debounce(deferFn: FuncType<void>, options?: DebounceOptions): FuncType<void>;
    protected debounce<RT, AT extends unknown[]>(directFn: FuncType<RT, AT>, deferFn: FuncType<void>, options?: DebounceOptions): FuncType<RT, AT>;
    // implementation
    protected debounce(...args: unknown[]): AnyFunction<unknown[]> {

        // decompose the argument list
        const hasDirectFn = is.function(args[1]);
        const options = (hasDirectFn ? args[2] : args[1]) as Opt<DebounceOptions>;
        const directFn = (hasDirectFn || options?.immediate) ? (args[0] as AnyFunction<unknown[]>) : undefined;
        const deferFn = (hasDirectFn ? args[1] : args[0]) as AnyFunction<unknown[]>;

        // minimum and maximum delay time
        const minDelay = options?.delay ?? 0;
        const maxDelay = options?.maxDelay ?? 0;

        // timers for minimum and maximum delay
        let minTimer: Opt<Abortable>;
        let maxTimer: Opt<Abortable>;

        // timer callback invoking the deferred callback
        const timerFn = (): void => {
            minTimer?.abort();
            maxTimer?.abort();
            minTimer = maxTimer = undefined;
            deferFn.call(this);
        };

        // the preparation function for the deferred callback
        let requestTimer: VoidFunction;
        switch (minDelay) {
            case "microtask":
                requestTimer = (): void => {
                    if (!maxTimer) {
                        maxTimer = this.queueMicrotask(() => {
                            maxTimer = undefined;
                            deferFn.call(this);
                        });
                    }
                };
                break;
            case "animationframe":
                requestTimer = (): void => {
                    if (!maxTimer) {
                        maxTimer = this.requestAnimationFrame(() => {
                            maxTimer = undefined;
                            deferFn.call(this);
                        });
                    }
                };
                break;
            default:
                requestTimer = () => {
                    // on every call, create a new timeout executing the deferred callback
                    minTimer?.abort();
                    minTimer = this.setTimeout(timerFn, minDelay);
                    // on first call, create a timer for the maximum delay
                    if (!maxTimer && (maxDelay > 0)) {
                        maxTimer = this.setTimeout(timerFn, maxDelay);
                    }
                };
                break;
        }

        // without direct callback, invoking the debounced function simply prepares the timers
        if (!directFn) { return requestTimer; }

        // otherwise: always invoke direct callback, afterwards prepare the timers
        return (...fnArgs) => {
            const result = directFn.call(this, ...fnArgs);
            requestTimer();
            return result;
        };
    }

    /**
     * Creates a synchronized function wrapping a callback function that
     * executes asynchronous code. The synchronized function buffers multiple
     * fast invocations in a queue, and executes the callback function
     * successively, always waiting for the asynchronous code. In difference to
     * debounced functions, invocations of a synchronized functions will never
     * be skipped, and each call of the asynchronous callback function receives
     * its original arguments passed to the synchronized functions.
     *
     * @param fn
     *  A callback function that will be called every time the synchronized
     *  function has been called. Will be called in the context of this
     *  instance. Receives all parameters passed to the synchronized function.
     *  If the callback function returns a pending promise, subsequent
     *  invocations of the synchronized function will be postponed until the
     *  promise will settle.
     *
     * @returns
     *  The synchronized function that can be called multiple times, and that
     *  executes the asynchronous callback function sequentially. Returns a
     *  promise that will settle after the callback function has been invoked.
     *  If the callback function returns a promise, the synchronized function
     *  will wait for it, and will forward its state and response. Otherwise,
     *  the promise will fulfil with the return value of the callback function.
     *  If this instance will be destroyed, processing the chain of pending
     *  function invocations will be aborted.
     */
    protected synchronize<RT, AT extends unknown[]>(fn: FuncType<MaybeAsync<RT>, AT>): FuncType<JPromise<RT>, AT> {

        interface QueueEntry {
            args: AT;
            deferred: JDeferred<RT>;
        }

        // cached arguments of pending calls of the synchronized function
        const queue: QueueEntry[] = [];
        // whether processing the queue has started
        let running = false;

        // starts a new background loop that processes the queue
        const startQueue = (): void => {

            // nothing to do if loop is running already
            if (running || !queue.length) { return; }

            // immediately set the `running` flag (prevent synchronous recursive calls!)
            running = true;

            // start the background loop that processes all pending invocations
            const loopPromise = this.asyncLoop(() => {

                // fetch next pending call from queue
                const entry = queue.shift();
                if (!entry) { breakLoop(); }

                // create the promise representing the result of the callback function
                const promise = jpromise.invoke(() => fn.call(this, ...entry.args));

                // forward result of the promise ()
                return jpromise.fastThen(promise, result => entry.deferred.resolve(result), reason => entry.deferred.reject(reason));
            });

            // reset the `running` flag after the last invocation has finished
            loopPromise.done(() => {
                running = false;
                // retry to start the queue (prevent race condition when adding call while finishing loop)
                startQueue();
            });
        };

        // create and return the synchronized function
        return (...args: AT) => {
            // create a new queue entry
            const deferred = this.newJDeferred<RT>();
            queue.push({ args, deferred });
            // if needed, start a new loop that processes the queue
            startQueue();
            return deferred.promise();
        };
    }

    /**
     * Logs all emitted events to the specified logger.
     */
    protected logEvents<K extends keyof EvtMapT>(logger: Logger, types: OrArray<K>, textFn?: (...args: InferEventArgs<EvtMapT, K>) => string): void {
        const badge = "$badge{" + this.constructor.name + "}";
        for (const type of resolveEventTypes(types, "*")) {
            this.on(type as K, (...args) => {
                logger.trace(() => str.concatTokens(badge, "$event{" + type + "}", textFn?.(...args)));
            });
        }
    }
}
