/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// @ts-nocheck

import _ from '$/underscore';
import $ from '$/jquery';

import { is, ary, jpromise, debug } from '@/io.ox/office/tk/algorithms';
import * as Utils from '@/io.ox/office/tk/utils';
import { EObject } from '@/io.ox/office/tk/object/eobject';

// private functions ==========================================================

/**
 * Returns whether the passed promise can be aborted (it is abortable and in
 * pending state).
 *
 * @param {JPromise} promise
 *  The promise to be checked.
 *
 * @returns {boolean}
 *  Whether the passed promise can be aborted.
 */
function isPendingAbortable(promise) {
    return jpromise.isPending(promise) && is.function(promise.abort);
}

// class DeferredWrapper ======================================================

/**
 * A small helper structure that collects all data needed to abort the deferred
 * object associated to an abortable promise synchronously.
 */
class DeferredWrapper {

    constructor(deferred, failFunc, abortFunc) {
        this._deferred = deferred;
        this._failFunc = failFunc;
        this._abortFunc = abortFunc;
    }

    // public methods ---------------------------------------------------------

    /**
     * Rejects the wrapped deferred object, and invokes all available callback
     * handler functions passed to the constructor.
     *
     * @param {Any} cause
     *  The abort cause to be passed to all callback functions, and to reject
     *  the deferred object with.
     */
    abort(cause) {
        if (this._abortFunc) { this._abortFunc(cause); }
        if (this._failFunc) { this._failFunc(cause); }
        this._deferred.reject(cause);
    }
}

// class PromiseChain =========================================================

/**
 * A small helper structure that collects the wrappers of all deferred objects
 * representing a promise chain, as well as all abortable promises returned by
 * the done or fail handlers of chained promises. Implements aborting the
 * entire promise chain (all deferred objects, and all nested abortable
 * promises returned by the done/fail handlers).
 */
class PromiseChain {

    constructor() {
        this._wrappers = [];
        this._abortables = [];
    }

    // public methods ---------------------------------------------------------

    /**
     * Creates a new wrapper for the passed deferred object, and appends it to
     * the promise chain.
     *
     * @param {jQuery.Deferred} deferred
     *  The deferred object to be appended to the promise chain.
     *
     * @param {Function|Null} failFunc
     *  The reject handler of the chained promise that will be called when
     *  aborting the chained promise.
     *
     * @param {*} abortFunc
     *  A custom abort handler that will be called when aborting the chained
     *  promise.
     */
    createWrapper(deferred, failFunc, abortFunc) {

        // nothing to do for resolved/rejected deferreds
        if (deferred.state() !== 'pending') { return; }

        // the array of all wrappers of pending deferred objects
        var wrappers = this._wrappers;

        // create a new wrapper instance, and push it to the array
        var wrapper = new DeferredWrapper(deferred, failFunc, abortFunc);
        wrappers.push(wrapper);

        // remove the wrapper from the array when the deferred object resolves/rejects
        deferred.always(() => ary.deleteFirst(wrappers, wrapper));
    }

    /**
     * Aborts the entire promise chain.
     *
     * @param {Any} cause
     *  The abort cause to be passed to all callback functions, and to reject
     *  the deferred objects with.
     */
    abort(cause) {

        // the array of all wrappers of pending deferred objects
        var wrappers = this._wrappers;

        // wrappers remove themselves from the array after abort
        while (wrappers.length > 0) {
            wrappers[0].abort(cause);
        }

        // abort all promises returned from done/fail handlers
        this._abortables.forEach(function (promise) {
            promise.abort(cause);
        });
    }
}

// class BaseObject ===========================================================

/**
 * A generic base class. Provides helper methods for running asynchronous code,
 * and listening to events and promises, regarding the lifetime of the
 * instances.
 *
 * @deprecated
 *  Use `EObject` instead.
 */
export class BaseObject extends EObject {

    constructor(config) {

        super(config);

        // all pending abortable promise chains, mapped by unique keys
        this.__chains = null;

        // all destructor callbacks
        this.__dtors = null;
    }

    // protected methods ------------------------------------------------------

    /**
     * Registers a destructor callback function that will be invoked when the
     * method `destroy()` has been called on this instance.
     *
     * @param {Function} destructor
     *  A destructor callback function. The method `destroy()`  will invoke all
     *  registered destructor callbacks in reverse order of their registration.
     *  The callback functions will be invoked in the context of this instance.
     *
     * @returns {BaseObject}
     *  A reference to this instance.
     */
    registerDestructor(destructor) {
        (this.__dtors || (this.__dtors = [])).unshift(destructor);
        return this;
    }

    // public methods ---------------------------------------------------------

    /**
     * Destroys this object. Invokes all destructor callback functions that
     * have been registered for this instance in reverse order.
     */
    destructor() {

        // abort all asynchronous code to prevent JS errors from callbacks
        var chains = this.__chains;
        if (chains) {
            for (var key in chains) {
                try {
                    chains[key].abort('destroy');
                } catch (err) {
                    window.console.error('unhandled exception in destructor\n', err);
                }
            }
        }

        // invoke all destructor callbacks
        var destructors = this.__dtors;
        if (destructors) {
            destructors.forEach(function (destructor) {
                try {
                    destructor.call(this);
                } catch (err) {
                    window.console.error('unhandled exception in destructor\n', err);
                }
            }, this);
        }

        super.destructor();
    }

    // promises ---------------------------------------------------------------

    /**
     * Creates a promise for the passed deferred object representing code
     * running asynchronously. The promise will contain an additional method
     * `abort()` that (when called before the deferred object has been resolved
     * or rejected) invokes the specified callback function, and rejects the
     * passed deferred object. Additionally, the promise will be stored
     * internally as long as it is in pending state. When this instance will be
     * destroyed, all pending promises will be aborted automatically with the
     * value `"destroy"`.
     *
     * @param {jQuery.Deferred} deferred
     *  The deferred object to create an abortable promise for.
     *
     * @param {Function} [callback]
     *  An optional callback function that will be invoked when the promise has
     *  been aborted, and the deferred object is still pending. Will be called
     *  in the context of this instance.
     *
     * @param {Number} [timeout]
     *  If specified and a positive number, the delay time in milliseconds
     *  after the promise returned by this method will be rejected with the
     *  value 'timeout', if the passed original deferred object is still
     *  pending.
     *
     * @returns {jQuery.Promise}
     *  A promise for the passed deferred object, with an additional method
     *  abort().
     */
    createAbortablePromise(deferred, callback, timeout) {

        // prepare the storage for the chained promises
        var chains = this.__chains || (this.__chains = {});
        // the unique map key for the promise chain
        var chainKey = _.uniqueId('promise');
        // storage entry for a promise chain
        var chain = chains[chainKey] = new PromiseChain();
        // whether the promise has been aborted
        var aborted = false;

        // the implementation of the abort() method attached to all chained promises
        function abortMethod(cause) {

            // prevent recursive calls from callback functions
            if (aborted) { return this; }
            aborted = true;

            // cause may be omitted, fall-back to 'abort'
            cause = (arguments.length === 0) ? 'abort' : cause;

            // immediately abort all chained promises created with the wrapped then() method
            // (rejecting the root promise does not reject the chained promises synchronously!)
            chain.abort(cause);
            chain = null;

            // remove dead promise chain from the storage
            delete chains[chainKey];

            return this;
        }

        // resolves/rejects the passed deferred object according to the result and 'then' callback handler
        function chainPromiseResult(deferred, method, result, handler) {
            if (handler) {
                try {
                    result = handler(result);
                    if (result instanceof Promise) {
                        result.then(value => deferred.resolve(value), error => deferred.reject(error));
                    } else if (jpromise.is(result)) {
                        result.done(deferred.resolve.bind(deferred));
                        result.fail(deferred.reject.bind(deferred));
                        result.progress(deferred.notify.bind(deferred));
                        // register pending abortable promises in chain to be able to forward abort
                        if (isPendingAbortable(result)) {
                            chain._abortables.push(result);
                        }
                    } else {
                        deferred.resolve(result);
                    }
                } catch (err) {
                    // catch all errors, log but ignore internal script errors
                    debug.logScriptError(err);
                    deferred.reject(err);
                }
            } else {
                deferred[method](result);
            }
        }

        // wrapper function to be passed into _.wrap() to reimplement the then() method of a promise
        function thenMethodWrapper(thenMethod, doneFunc, failFunc, progressFunc, abortFunc) {

            // the promise returned by the original then() method
            var corePromise = thenMethod.call(this);
            // a proxy deferred object that can be rejected manually on abort()
            var proxyDeferred = new $.Deferred();

            // forward resolved state of the (asynchronous) core promise with the result of the done handler
            corePromise.done(function (result) {
                chainPromiseResult(proxyDeferred, 'resolve', result, doneFunc);
            });

            // forward rejected state of the (asynchronous) core promise with the result of the fail handler
            corePromise.fail(function (result) {
                chainPromiseResult(proxyDeferred, 'reject', result, failFunc);
            });

            // forward progress of the (asynchronous) core promise with the result of the progress handler
            corePromise.progress(function (result) {
                proxyDeferred.notify(progressFunc ? progressFunc(result) : result);
            });

            // bug 54845: prevent duplicate asynchronous invocation of fail callback after aborting synchronously
            proxyDeferred.always(function () {
                doneFunc = failFunc = null;
            });

            // create the abortable promise for the internal proxy deferred
            return implCreatePromise(proxyDeferred, failFunc, abortFunc);
        }

        // creates an abortable promise for the passed deferred object
        function implCreatePromise(deferred, failFunc, abortFunc) {

            // register the deferred object in the chain
            chain.createWrapper(deferred, failFunc, abortFunc);

            // create and extend the abortable promise
            var promise = deferred.promise();
            promise.abort = abortMethod;
            promise.then = _.wrap(promise.then, thenMethodWrapper);
            promise.pipe = null; // prevent using the deprecated pipe() method
            return promise;
        }

        // create the abortable promise for the passed root deferred
        var promise = implCreatePromise(deferred, null, callback ? callback.bind(this) : null);

        // abort automatically after the specified timeout
        if ((promise.state() === 'pending') && is.number(timeout) && (timeout > 0)) {
            var timer = window.setTimeout(function () {
                if (promise.state() === 'pending') { promise.abort('timeout'); }
            }, timeout);
            promise.always(function () {
                if (timer) { window.clearTimeout(timer); timer = null; }
            });
        }

        return promise;
    }

    /**
     * Creates an abortable promise that has been resolved with the passed
     * result value.
     *
     * @param {Any} [result]
     *  The result value to be provided by the returned promise. If omitted,
     *  the promise will be resolved without a value.
     *
     * @returns {jQuery.Promise}
     *  An abortable promise that has been resolved with the passed result
     *  value. The method abort() of the promise will do nothing. The method
     *  then() of the promise will return abortable promises too.
     */
    createResolvedPromise(result) {
        return this.createAbortablePromise(new $.Deferred().resolve(result));
    }

    /**
     * Creates an abortable promise that has been rejected with the passed
     * result value.
     *
     * @param {Any} [result]
     *  The result value to be provided by the returned promise. If omitted,
     *  the promise will be rejected without a value.
     *
     * @returns {jQuery.Promise}
     *  An abortable promise that has been rejected with the passed result
     *  value. The method abort() of the promise will do nothing. The method
     *  then() of the promise will return abortable promises too.
     */
    createRejectedPromise(result) {
        return this.createAbortablePromise(new $.Deferred().reject(result));
    }

    /**
     * Converts the passed arbitrary value to a promise.
     *
     * @param {MaybeAsync<T>} result
     *  The result value to be converted to a promise. If this value is already
     *  a promise, it will be returned immediately. Otherwise, a promise will
     *  be created that has been resolved with the passed value.
     *
     * @returns {JPromise<T>}
     *  The promise that has been resolved or rejected with the passed result
     *  value.
     */
    convertToPromise(result) {

        // convert a native promise to a JQuery promise
        if (result instanceof Promise) {
            return this.createResolvedPromise().then(() => result);
        }

        // directly return a passed JQuery promise
        if (jpromise.is(result)) { return result; }

        // return a resolved promise if nothing has been passed
        return this.createResolvedPromise(result);
    }

    /**
     * Creates a new pending deferred object that is bound to the lifetime of
     * this instance.
     *
     * @returns {JDeferred}
     *  A new pending deferred object.
     */
    createDeferred() {

        // the new deferred object (will be registered for logging in automated test environments)
        var deferred = $.Deferred();
        // reject the deferred object when this instance will be destroyed
        this.createAbortablePromise(deferred);

        return deferred;
    }

    // events -----------------------------------------------------------------

    /**
     * Returns a promise that will be resolved when the passed event emitter
     * triggers the specified event.
     *
     * @param {AnyEmitter} emitter
     *  The event emitter. See method `listenTo()` for more details about
     *  supported object types.
     *
     * @param {String} [type]
     *  The type of the event to wait for. Can be a space-separated list of
     *  event type names. In the latter case, the first event matching one of
     *  the specified types will resolve the promise, subsequent events with
     *  matching type will do nothing.
     *
     * @param {Number} [timeout]
     *  If specified, a delay time in milliseconds. If the event emitter does
     *  not trigger the expected event before the time has elapsed, the promise
     *  will be rejected with the string 'timeout'.
     *
     * @returns {JPromise<T>}
     *  An abortable promise that will be resolved with an array containing all
     *  parameters passed with the event, or that will be rejected with the
     *  string 'timeout' after the specified delay time. The promise contains
     *  the additional method abort() that allows to stop listening to the
     *  pending event immediately.
     */
    waitForEvent(emitter, type, timeout) {

        // normalize parameters
        if (is.number(type)) { timeout = type; type = null; }

        // the deferred object representing the event state
        var deferred = $.Deferred();
        // the abortable promise for the deferred object, with timeout
        var promise = this.createAbortablePromise(deferred, null, timeout);
        // the event handler resolving the deferred object
        var listener = (...args) => deferred.resolve(args);

        // listen to the specified event(s), and resolve the deferred object
        // (listener will be unregistered automatically)
        if (is.string(type)) {
            this.listenTo(emitter, type, listener, { while: promise });
        } else  {
            this.listenToAllEvents(emitter, listener, { while: promise });
        }

        return promise;
    }

    // timers -----------------------------------------------------------------

    /**
     * Invokes the passed callback function once in a browser timeout. If this
     * instance will be destroyed before the callback function has been
     * started, it will not be called anymore.
     *
     * @param {Function} callback
     *  The callback function that will be executed in a browser timeout after
     *  the delay time. Does not receive any parameters. Will be called in the
     *  context of this instance.
     *
     * @param {Number} [delay=0]
     *  The time (in milliseconds) the execution of the passed callback
     *  function will be delayed at least.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the callback function has been
     *  executed. If the callback function returns a simple value or object,
     *  the promise will be resolved with that value. If the callback function
     *  returns a promise by itself, its state and result value will be
     *  forwarded to the promise returned by this method. The promise contains
     *  an additional method abort() that can be called before the timeout has
     *  been fired to cancel the pending execution of the callback function. In
     *  that case, the promise will be rejected. If this instance will be
     *  destroyed before invoking the callback function, the timer will be
     *  aborted automatically, and the promise will be rejected.
     */
    executeDelayed(callback, delay) {

        // a pending abortable result promise returned by the callback function
        var abortablePromise = null;
        // the deferred object representing the timer state
        var deferred = $.Deferred();

        // create a new synchronized timer
        var handle = window.setTimeout(() => {

            // timer fired successfully: immediately clear the timer handle
            handle = null;

            // the result of the callback function
            var result = callback.call(this);

            // forward the result to the promise returned by this method
            if (result instanceof Promise) {
                result.then(
                    value => deferred.resolve(value),
                    error => deferred.reject(error)
                );
            } else if (jpromise.is(result)) {
                if (isPendingAbortable(result)) { abortablePromise = result; }
                result.done(value => deferred.resolve(value));
                result.fail(error => deferred.reject(error));
            } else {
                deferred.resolve(result);
            }
        }, delay || 0);

        // the abortable promise for the deferred object
        return this.createAbortablePromise(deferred, () => {
            if (handle) { window.clearTimeout(handle); handle = null; }
            if (abortablePromise) { abortablePromise.abort(); }
        });
    }

    /**
     * Invokes the passed callback function repeatedly in a browser timeout
     * loop. If this instance will be destroyed before the callback function
     * has been started, or while the callback function will be repeated, it
     * will not be called anymore.
     *
     * @param {Function} callback
     *  The callback function that will be invoked repeatedly in a browser
     *  timeout loop after the initial delay time. Receives the zero-based
     *  index of the execution cycle as first parameter. Will be called in the
     *  context of this instance. The return value of the function will be used
     *  to decide whether to continue the repeated execution. If the function
     *  returns Utils.BREAK, execution will be stopped. If the function returns
     *  a promise, looping will be deferred until the promise is resolved or
     *  rejected. After resolving the promise, the delay time will start, and
     *  the callback will be invoked again. Otherwise, after the promise has
     *  been rejected, execution will be stopped, and the promise returned by
     *  this method will be rejected too. All other return values will be
     *  ignored, and the callback loop will continue.
     *
     * @param {Object|Number} [options]
     *  Optional parameters:
     *  - {Number} [options.delay]
     *      The time (in milliseconds) the initial invocation of the passed
     *      callback function will be delayed. If this option will be omitted,
     *      the callback function will be executed immediately (synchronously)
     *      once with the invocation of this method.
     *  - {Number} [options.repeatDelay]
     *      The time (in milliseconds) the repeated invocation of the passed
     *      callback function will be delayed. This value must not be less than
     *      10 milliseconds. If omitted, the specified initial delay time that
     *      has been passed with the option 'delay' will be used, unless it has
     *      been omitted, or is less than 10 milliseconds (in this case, the
     *      minimum value of 10 milliseconds will be used).
     *  - {Number} [options.cycles]
     *      If specified, the maximum number of cycles to be executed.
     *  - {Boolean} [options.fastAsync=false]
     *      If set to true, and the callback function returns a pending
     *      promise, the delay time for the next invocation of the callback
     *      function will be reduced by the time the promise is pending. By
     *      default, the delay time always starts after the promise has been
     *      resolved.
     *  This parameter can also be a number, allowing to pass the value of the
     *  option 'delay' (and also 'repeatDelay' which defaults to the value of
     *  the option 'delay') directly.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved or rejected after the callback function
     *  has been invoked the last time. The promise will be resolved, if the
     *  callback function has returned the Utils.BREAK object in the last
     *  iteration, or if the maximum number of iterations (see option 'cycles')
     *  has been reached. It will be rejected, if the callback function has
     *  returned a rejected promise. The returned promise contains an
     *  additional method abort() that can be called before or while the
     *  callback loop is executed to stop the loop immediately. In that case,
     *  the promise will be rejected. If this instance will be destroyed before
     *  or during the callback loop, the loop timer will be aborted
     *  automatically, and the promise will be rejected.
     */
    repeatDelayed(callback, options) {

        // self reference for local functions
        var self = this;
        // the delay time for the first invocation of the callback
        var initDelay = Number.isFinite(options) ? Math.max(0, options) : Utils.getIntegerOption(options, 'delay', null, 0);
        // the delay time for the next invocations of the callback
        var repeatDelay = Math.max(10, Utils.getIntegerOption(options, 'repeatDelay', initDelay));
        // the number of cycles to be executed
        var cycles = Utils.getIntegerOption(options, 'cycles');
        // whether to reduce delay time after asynchronous callbacks
        var fastAsync = Utils.getBooleanOption(options, 'fastAsync', false);
        // the index of the next cycle
        var index = 0;
        // the current timer promise that defers invoking the callback
        var timer = null;
        // current pending abortable result promise returned by the callback function
        var abortablePromise = null;
        // the resulting deferred object
        var deferred = $.Deferred();

        // bind callback to this instance
        callback = callback.bind(this);

        // invokes the callback function
        function invokeCallback() {

            // the result of the callback, immediately break the loop if callback returns Utils.BREAK
            var result = callback(index);
            if (result === Utils.BREAK) {
                deferred.resolve(Utils.BREAK);
                return;
            }

            // wait for the result
            if (result instanceof Promise) {
                result.then(() => startNextTimer(-1), error => deferred.reject(error));
            } else if (jpromise.is(result)) {
                if (isPendingAbortable(result)) { abortablePromise = result; }
                var t0 = (result.state() === 'pending') ? Date.now() : -1;
                result.done(function () { startNextTimer(t0); });
                result.fail(deferred.reject.bind(deferred));
            } else {
                startNextTimer(-1);
            }
        }

        // creates and registers a browser timeout that executes the callback
        function createTimer(delay) {
            abortablePromise = null;
            timer = self.executeDelayed(invokeCallback, delay);
        }

        function startNextTimer(t0) {
            // do not create a new timer if execution has been aborted while the asynchronous code was running
            if (deferred.state() !== 'pending') { return; }
            // decide whether to start the next iteration
            index += 1;
            if (!is.number(cycles) || (index < cycles)) {
                createTimer((fastAsync && (t0 > 0)) ? Math.max(0, repeatDelay - (Date.now() - t0)) : repeatDelay);
            } else {
                deferred.resolve(null);
            }
        }

        // immediately invoke the callback function, if no delay time has been specified
        if (initDelay === null) {
            invokeCallback();
        } else {
            createTimer(initDelay);
        }

        // the abortable promise for the deferred object
        return this.createAbortablePromise(deferred, function () {
            if (timer) { timer.abort(); timer = null; }
            if (abortablePromise) { abortablePromise.abort(); }
        });
    }

    /**
     * Invokes the passed callback function repeatedly until it returns a
     * specific value. To prevent performance problems (frozen user interface
     * of the browser), the callback function may be invoked from several time
     * slices of a browser timeout loop. In difference to the method
     * BaseObject.repeatDelayed() that uses an own browser timeout for each
     * invocation of the callback function, this method tries to pack multiple
     * invocations into a single time slice until the total execution time of
     * the time slice is exceeded, and continues with a new time slice in
     * another browser timeout afterwards. If this instance will be destroyed
     * while the loop is running, it will be aborted immediately.
     *
     * @param {Function} callback
     *  The callback function that will be invoked repeatedly. Receives the
     *  zero-based index of the execution cycle as first parameter. Will be
     *  called in the context of this instance. The return value of the
     *  function will be used to decide whether to continue the repeated
     *  execution (see parameter 'callback' of the method
     *  BaseObject.repeatDelayed() for details).
     *
     * @param {Object} [options]
     *  Optional parameters:
     *  - {Number} [options.delay]
     *      The initial delay time (in milliseconds) before invoking the
     *      callback function the first time. If omitted, the callback function
     *      will be executed immediately (synchronously) once with the
     *      invocation of this method.
     *  - {Number} [options.slice=200]
     *      The time (in milliseconds) for a single synchronous time slice. The
     *      callback function will be invoked repeatedly until the time has
     *      elapsed; or until the callback function returns Utils.BREAK, a
     *      pending promise, or a rejected promise (see parameter 'callback'
     *      above for more details).
     *  - {Number} [options.interval=10]
     *      The delay time (in milliseconds) between two time slices. If the
     *      callback function returns a pending promise, the implementation
     *      will wait for that promise to resolve, and will add a shorter delay
     *      (shortened by the time the promise has needed to resolve) before
     *      continuing the loop.
     *  - {Number} [options.cycles]
     *      If specified, the maximum number of cycles to be executed.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after the callback function returns
     *  Utils.BREAK, or if the maximum number of iterations (see option
     *  'cycles') has been reached; or that will be rejected, if the callback
     *  function returns a rejected promise. The promise will be notified each
     *  time a new time slice starts (with the zero-based index of the next
     *  iteration cycle). The promise contains an additional method abort()
     *  that can be called to abort the loop immediately. In that case, the
     *  promise will be rejected. If this instance will be destroyed before or
     *  during the callback loop, the loop timer will be aborted automatically,
     *  and the promise will be rejected.
     */
    repeatSliced(callback, options) {

        // slice time for synchronous processing
        var slice = Utils.getIntegerOption(options, 'slice', 200, 10);
        // the number of cycles to be executed
        var cycles = Utils.getIntegerOption(options, 'cycles');
        // index of the next cycle
        var index = 0;
        // the resulting deferred object
        var deferred = $.Deferred();

        // bind callback to this instance
        callback = callback.bind(this);

        // create the interval timer with the passed delay options
        var timer = this.repeatDelayed(function () {

            // start time of this iteration
            var start = Date.now();

            // notify the own deferred object about the progress (once per time slice)
            deferred.notify(index);

            // time slice implementation: invoke callback repeatedly (loop body returns on any exit conditions)
            for (;;) {

                // exit loop, if the specified number of cycles has been reached
                if (is.number(cycles) && (index >= cycles)) {
                    deferred.resolve();
                    return Utils.BREAK;
                }

                // invoke the callback function
                const result = callback(index);
                index += 1;

                // callback has returned Utils.BREAK: resolve the own deferred
                // object immediately, and break the background loop
                if (result === Utils.BREAK) {
                    deferred.resolve(Utils.BREAK);
                    return Utils.BREAK;
                }

                // callback has returned a pending or rejected promise: return it to defer
                // the loop (ignore resolved promises and continue with next invocation)
                if ((result instanceof Promise) || (jpromise.is(result) && !jpromise.isFulfilled(result))) {
                    return result;
                }

                // check elapsed time at the end of the loop body (this ensures
                // that the callback will be executed at least once)
                if (Date.now() - start >= slice) {
                    return;
                }
            }
        }, {
            delay: Utils.getIntegerOption(options, 'delay', null),
            repeatDelay: Utils.getIntegerOption(options, 'interval', 10, 10),
            fastAsync: true
        });

        // resolve/reject the own deferred object when the timer has finished
        timer.done(deferred.resolve.bind(deferred));
        timer.fail(deferred.reject.bind(deferred));

        // the abortable promise for the resulting deferred object
        return this.createAbortablePromise(deferred, function () {
            if (timer) { timer.abort(); timer = null; }
        });
    }

    /**
     * Invokes the passed callback function for all values of the passed value
     * sequence. To prevent performance problems (frozen user user interface of
     * the browser), the callback function will be invoked from several time
     * slices of a browser timeout loop. If this instance will be destroyed
     * while the loop is running, it will be aborted immediately.
     *
     * @param {Iterable} source
     *  The source data.
     *
     * @param {Function} callback
     *  The callback function that will be invoked for each valid result of the
     *  source sequence. Receives the following parameters:
     *  (1) {Any} value
     *      The property "value" from the iterator result.
     *  Will be called in the context of this instance. The return value of the
     *  callback function will be used to decide whether to continue to process
     *  the next iterator result (see parameter 'callback' of the method
     *  BaseObject.repeatDelayed() for details).
     *
     * @param {Object} [options]
     *  Optional parameters. See method BaseObject.repeatSliced() for details
     *  about all supported options.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after all values of the sequence have
     *  been processed successfully, or the callback function returns
     *  Utils.BREAK; or that will be rejected, if the callback function returns
     *  a rejected promise. The promise will NOT be notified about the progress
     *  (sequences do not have to provide their size). The promise contains an
     *  additional method abort() that can be called to cancel the loop
     *  immediately while processing the sequence. In that case, the promise
     *  will be rejected. If this instance will be destroyed before or during
     *  the callback loop, the loop timer will be aborted automatically, and
     *  the promise will be rejected.
     */
    iterateSliced(source, callback, options) {
        const iterator = source[Symbol.iterator]();
        return this.repeatSliced(() => {
            const result = iterator.next();
            return result.done ? Utils.BREAK : callback.call(this, result.value);
        }, options);
    }

    /**
     * Invokes the passed callback function for all elements contained in the
     * passed data array. To prevent performance problems (frozen user user
     * interface of the browser), the callback function will be invoked from
     * several time slices of a browser timeout loop. If this instance will be
     * destroyed while the loop is running, it will be aborted immediately.
     *
     * @param {Array|jQuery|String} array
     *  A JavaScript array, or another array-like object that provides a
     *  'length' property, and element access via bracket notation.
     *
     * @param {Function} callback
     *  The callback function that will be invoked for each array element.
     *  Receives the following parameters:
     *  (1) {Any} element
     *      The current array element.
     *  (2) {Number} index
     *      The array index of the current element.
     *  (3) {Array|jQuery} array
     *      The entire array as passed in the 'array' parameter.
     *  Will be called in the context of this instance. The return value of the
     *  callback function will be used to decide whether to continue to process
     *  the next array elements (see parameter 'callback' of the method
     *  BaseObject.repeatDelayed() for details).
     *
     * @param {Object} [options]
     *  Optional parameters. See method BaseObject.repeatSliced() for details
     *  about all supported options. Additionally, the following options are
     *  supported:
     *  - {Boolean} [options.reverse=false]
     *      If set to true, the array elements will be visited in reversed
     *      order. In reverse mode, the callback function may remove the array
     *      element currently visited, or elements that are following the
     *      visited element, from the array in-place.
     *
     * @returns {jQuery.Promise}
     *  A promise that will be resolved after all array elements have been
     *  processed successfully, or the callback function returns Utils.BREAK;
     *  or that will be rejected, if the callback function returns a rejected
     *  promise. The promise will be notified about the progress (as a
     *  floating-point value between 0.0 and 1.0). The promise contains an
     *  additional method abort() that can be called to cancel the loop
     *  immediately while processing the array. In that case, the promise will
     *  be rejected. If this instance will be destroyed before or during the
     *  callback loop, the loop timer will be aborted automatically, and the
     *  promise will be rejected.
     */
    iterateArraySliced(array, callback, options) {

        // whether to iterate in reversed order
        var reverse = options && options.reverse;
        // current array index
        var arrayIndex = reverse ? (array.length - 1) : 0;
        // the resulting deferred object
        var deferred = $.Deferred();

        // do nothing if passed array is empty
        if (array.length === 0) {
            deferred.notify(1).resolve();
            return this.createAbortablePromise(deferred);
        }

        // bind callback to this instance
        callback = callback.bind(this);

        // start the sliced loop
        var timer = this.repeatSliced(function () {

            // end of array reached
            if ((arrayIndex < 0) || (arrayIndex >= array.length)) {
                deferred.notify(1).resolve();
                return Utils.BREAK;
            }

            // invoke the callback function for a single array element
            var result = callback(array[arrayIndex], arrayIndex, array);

            // next array index
            arrayIndex = reverse ? (arrayIndex - 1) : (arrayIndex + 1);

            // immediately resolve after last element unless the result is a pending or rejected promise
            if (((arrayIndex < 0) || (arrayIndex >= array.length)) && !(jpromise.is(result) && !jpromise.isFulfilled(result))) {
                deferred.notify(1).resolve();
                return Utils.BREAK;
            }

            return result;
        }, options);

        // resolve/reject the own deferred object according to the timer result
        timer.done(deferred.resolve.bind(deferred));
        timer.fail(deferred.reject.bind(deferred));

        // notify the own deferred object about the progress (once per time slice)
        timer.progress(function (index) { deferred.notify(index / array.length); });

        // the abortable promise of the resulting deferred object
        return this.createAbortablePromise(deferred, function () {
            if (timer) { timer.abort(); timer = null; }
        });
    }
}
