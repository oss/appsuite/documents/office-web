/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { EObject } from "@/io.ox/office/tk/object/eobject";

// types ======================================================================

export interface JAbortablePromise<FT = void> extends JPromise<FT>, Abortable {
    promise(): JAbortablePromise<FT>;
    then<RT>(doneFn: (result: FT) => MaybeAsync<RT>, failFn?: (error: unknown) => MaybeAsync<RT>): JAbortablePromise<RT>;
    catch<RT>(failFn: (error: unknown) => MaybeAsync<RT>): JAbortablePromise<RT>;
}

export interface JOptAbortablePromise<FT = void> extends JPromise<FT>, Partial<Abortable> {
    promise(): JOptAbortablePromise<FT>;
    then<RT>(doneFn: (result: FT) => MaybeAsync<RT>, failFn?: (error: unknown) => MaybeAsync<RT>): JOptAbortablePromise<RT>;
    catch<RT>(failFn: (error: unknown) => MaybeAsync<RT>): JOptAbortablePromise<RT>;
}

export interface SlicedLoopOptions {
    delay?: number;
    slice?: number;
    interval?: number;
    cycles?: number;
}

// class BaseObject ===========================================================

/** @deprecated */
export class BaseObject<EvtMapT = Empty> extends EObject<EvtMapT> {

    /** @deprecated */
    createAbortablePromise<FT = void>(deferred: JDeferred<FT>, fn?: VoidFunction, timeout?: number): JAbortablePromise<FT>;
    /** @deprecated */
    createResolvedPromise(): JAbortablePromise;
    /** @deprecated */
    createResolvedPromise<FT>(result: FT): JAbortablePromise<FT>;

    /** @deprecated */
    executeDelayed(fn: VoidFunction, delay?: number): JAbortablePromise;
    /** @deprecated */
    repeatSliced(fn: (index: number) => MaybeAsync<unknown>, options?: SlicedLoopOptions): JAbortablePromise;
    /** @deprecated */
    iterateSliced<VT>(source: Iterable<VT>, fn: (value: VT) => MaybeAsync<unknown>, options?: SlicedLoopOptions): JAbortablePromise;
}
