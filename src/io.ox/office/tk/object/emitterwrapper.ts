/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import $ from "$/jquery";

import { is, map, dict, jpromise } from "@/io.ox/office/tk/algorithms";
import { DObject } from "@/io.ox/office/tk/object/dobject";
import type { EventBindOptions, DOMBindOptions, JQueryBindOptions, JQueryEmitter, BackboneEmitter, CoreEmitter, DocsEmitter, AnyEmitter } from "@/io.ox/office/tk/events";
import { isDOMEmitter, isJQueryEmitter, isBackboneEmitter, isCoreEmitter, isDocsEmitter, resolveEventTypes, invokeHandler, EventHub } from "@/io.ox/office/tk/events";

// types ======================================================================

/**
 * A wrapper object for an event handler callback function, used as map entry.
 */
interface HandlerWrapper {
    invoke: AnyFunction;
    options: Opt<EventBindOptions>;
}

// private functions ==========================================================

/**
 * Converts the DOM event listener options to the parameter to be passed to
 * `addEventListener()` and `removeEventListener()`.
 */
function convertDOMOptions(options?: DOMBindOptions): Opt<AddEventListenerOptions> {

    // nothing to pass without options object
    if (!options) { return undefined; }

    // extract options for DOM event targets (`capture` and `passive` only; `once` is handled separately)
    const domOptions = dict.createPartial<AddEventListenerOptions>();
    if (is.boolean(options.capture)) { domOptions.capture = options.capture; }
    if (is.boolean(options.passive)) { domOptions.passive = options.passive; }
    return domOptions;
}

// event emitter adapters =====================================================

/**
 * An interface that provides a uniform bind/unbind API for different types of
 * event emitters.
 */
interface EmitterAdapter<
    E extends AnyEmitter<any> = AnyEmitter<any>,
    O extends EventBindOptions = EventBindOptions
> {
    /** Type specifier for "listen to all events" mode, if available. */
    all?: string;
    /** Type detection for arbitrary event emitters. */
    is(emitter: AnyEmitter<any>): boolean;
    /** Adds an event handler to the event emitter depending on its type. */
    bind(emitter: E, type: string, handler: AnyFunction, options?: O): void;
    /** Removes an event handler from the event emitter depending on its type. */
    unbind(emitter: E, type: string, handler: AnyFunction, options?: O): void;
}

/** Bind/unbind adapter for DOM event targets. */
const DOM_ADAPTER: EmitterAdapter<EventTarget, DOMBindOptions> = {
    is:     isDOMEmitter,
    bind:   (emitter, type, handler, options) => emitter.addEventListener(type, handler, convertDOMOptions(options)),
    unbind: (emitter, type, handler, options) => emitter.removeEventListener(type, handler, convertDOMOptions(options))
};

/** Bind/unbind adapter for JQuery objects. */
const JQUERY_ADAPTER: EmitterAdapter<JQueryEmitter, JQueryBindOptions> = {
    is:     isJQueryEmitter,
    bind:   (emitter, type, handler, options) => options?.delegated ? emitter.on(type, options.delegated, handler) : emitter.on(type, handler),
    unbind: (emitter, type, handler, options) => options?.delegated ? emitter.off(type, options.delegated, handler) : emitter.off(type, handler)
};

/** Bind/unbind adapter for Backbone emitters. */
const BACKBONE_ADAPTER: EmitterAdapter<BackboneEmitter<any>> = {
    all:    "all",
    is:     isBackboneEmitter,
    bind:   (emitter, type, handler) => emitter.on(type, handler),
    unbind: (emitter, type, handler) => emitter.off(type, handler)
};

/** Bind/unbind adapter for CoreUI emitters. */
const CORE_ADAPTER: EmitterAdapter<CoreEmitter<any>> = {
    all:    "triggered",
    is:     isCoreEmitter,
    bind:   (emitter, type, handler) => emitter.on(type, handler),
    unbind: (emitter, type, handler) => emitter.off(type, handler)
};

/** Bind/unbind adapter for event hubs. */
const HUB_ADAPTER: EmitterAdapter<EventHub<any>> = {
    all:     "*",
    is:      emitter => emitter instanceof EventHub,
    bind:    (emitter, type, handler) => emitter.on(type, handler),
    unbind:  (emitter, type, handler) => emitter.off(type, handler)
};

/** Bind/unbind adapter for internal emitters. */
const DOCS_ADAPTER: EmitterAdapter<DocsEmitter<any>> = {
    all:    "*",
    is:     emitter => isDocsEmitter(emitter) && !DObject.isDestroyed(emitter),
    bind:   (emitter, type, handler) => emitter.on(type, handler),
    unbind: (emitter, type, handler) => emitter.off(type, handler)
};

/** Array with all existing emitter adapters. */
const ALL_ADAPTERS: readonly EmitterAdapter[] = [
    DOM_ADAPTER,
    JQUERY_ADAPTER,
    BACKBONE_ADAPTER,
    CORE_ADAPTER,
    HUB_ADAPTER,
    DOCS_ADAPTER
];

// class EmitterWrapper =======================================================

/**
 * Helper base class for wrapper objects containing event handlers bound to an
 * external event emitter of a specific type.
 *
 * Provides a common API for binding event handlers to, and unbinding them from
 * the wrapped event emitter.
 *
 * Subclasses must implement the abstract methods according to the type of the
 * wrapped event emitter.
 */
class EmitterWrapper extends DObject {

    /** The wrapped event emitter. */
    readonly #emitter: AnyEmitter<any>;
    /** The calling context for all event handlers. */
    readonly #context: object;
    /** The adapter implementing bind/unbind for the event emitter. */
    readonly #adapter: EmitterAdapter;
    /** All registered event handlers, mapped by event type identifiers. */
    readonly #entries = new Map<string, Map<AnyFunction, HandlerWrapper>>();

    // constructor ------------------------------------------------------------

    /**
     * @param emitter
     *  The event emitter represented by this wrapper instance.
     *
     * @param context
     *  The calling context for all event handlers.
     */
    public constructor(emitter: AnyEmitter<any>, context: object) {
        super();

        this.#context = context;
        this.#emitter = emitter;

        // select the correct adapter for binding/unbinding event listeners
        const adapter = ALL_ADAPTERS.find(adapter => adapter.is(emitter));
        if (!adapter) { throw new TypeError("unsupported event emitter"); }
        this.#adapter = adapter;
    }

    protected override destructor(): void {
        this.unbind();
        super.destructor();
    }

    // public methods ---------------------------------------------------------

    /**
     * Binds the passed event type and handler to the wrapped event emitter.
     */
    bind(types: OrArray<PropertyKey>, key: AnyFunction, fn: AnyFunction<unknown[]>, options?: EventBindOptions): void {

        // the event emitter may have been destroyed already
        if (!this.#adapter.is(this.#emitter)) {
            this.#entries.clear();
            return;
        }

        // bind the passed event handler to the context object
        const bound = fn.bind(this.#context);

        // process multiple event types
        for (const type of this.#resolveTypes(types)) {

            // "once" mode: create a wrapper that automatically stops listening to the emitter
            const invoke: AnyFunction<unknown[]> = options?.once ? (...args) => {
                this.unbind(type, key);
                return invokeHandler(bound, ...args);
            } : bound;

            // push a new wrapper object into the internal store
            const entries = map.upsert(this.#entries, type, () => new Map(null));
            entries.delete(key);
            entries.set(key, { invoke, options });

            // bind the effective callback function to the event emitter
            this.#adapter.bind(this.#emitter, type, invoke, options);

            // while-mode: restrict listening to the promise's pending phase
            if (options?.while) {
                this.onSettled(options.while, () => this.unbind(type, key));
            }
        }
    }

    /**
     * Unbinds all matching event handlers from the wrapped event emitter.
     */
    unbind(types?: OrArray<PropertyKey>, key?: AnyFunction, scope?: symbol): void {

        // the event emitter may have been destroyed already
        if (!this.#adapter.is(this.#emitter)) {
            this.#entries.clear();
            return;
        }

        // unbind all handlers of the specified type, or process all registered types
        for (const type of this.#resolveTypes(types)) {
            const entries = this.#entries.get(type);
            if (entries) {
                for (const fn of key ? [key] : entries.keys()) {
                    const entry = entries.get(fn);
                    if (entry && (!scope || (scope === entry.options?.scope))) {
                        this.#adapter.unbind(this.#emitter, type, entry.invoke, entry.options);
                        entries.delete(fn);
                    }
                }
            }
        }
    }

    // private methods --------------------------------------------------------

    #resolveTypes(types?: OrArray<PropertyKey>): Iterable<string> {
        return (types === undefined) ? this.#entries.keys() : resolveEventTypes(types, this.#adapter.all);
    }
}

// class EmitterWrapperContainer ==============================================

// virtual map keys for the individual DOM nodes covered by JQuery collections, to prevent
// reusing the same map key for DOM listeners and JQuery listeners on the same element
const JQ_SYMBOLS = new WeakMap<EventTarget, symbol>();

// type of map keys for storing `EmitterWrapper` instances in a map
type EmitterWrapperKey = AnyEmitter<any> | symbol;

/**
 * Container for all event handlers listened to by a single object instance.
 * Implementation helper class used by class `EObject` to collect all active
 * event listeners.
 */
export class EmitterWrapperContainer extends DObject {

    // calling context for all event handlers
    readonly #context: object;

    // all event emitters this instance is listening to (mapped by unique symbol per emitter)
    readonly #emitters = this.member(new Map<EmitterWrapperKey, EmitterWrapper>());

    // constructor ------------------------------------------------------------

    constructor(context: object) {
        super({ _kind: "private" });
        this.#context = context;
    }

    // public functions -------------------------------------------------------

    /**
     * Adds an event handler to the specified event emitter. On destruction of
     * this instance, the event handler will be removed from the event emitter
     * automatically.
     */
    bind(emitter: AnyEmitter<any>, types: OrArray<PropertyKey>, handler: AnyFunction, options?: EventBindOptions): void {

        // do not start listening if a settled JQuery promise has been passed
        const promise = options?.while;
        if (jpromise.is(promise) && jpromise.isSettled(promise)) { return; }

        // generate separate keys for all nodes contained in JQuery collections
        for (const [key, emitter2] of this.#yieldForBind(emitter)) {
            const wrapper = map.upsert(this.#emitters, key, () => new EmitterWrapper(emitter2, this.#context));
            wrapper.bind(types, handler, handler, options);
        }
    }

    /**
     * Removes one or more event handlers from the specified event emitter that
     * have been registered using the method `bind`.
     */
    unbind(emitter?: AnyEmitter<any>, types?: OrArray<PropertyKey>, handler?: AnyFunction, scope?: symbol): void {

        // unbind all emitters if no emitter has been specified
        for (const key of emitter ? this.#yieldForUnbind(emitter) : this.#emitters.keys()) {
            this.#emitters.get(key)?.unbind(types, handler, scope);
        }
    }

    /**
     * Unbinds this instance from all registered event emitters.
     */
    release(): void {
        map.destroy(this.#emitters);
    }

    // private methods --------------------------------------------------------

    /**
     * Yields all map keys and resulting emitters for the passed event emitter.
     * If the emitter is a JQuery collection, all its DOM nodes will be
     * processed separately.
     */
    *#yieldForBind(emitter: AnyEmitter<any>): IterableIterator<[EmitterWrapperKey, AnyEmitter<any>]> {

        // returns existing or new symbol for a DOM node wrapped in a JQuery collection
        const getSymbol = (node: EventTarget): symbol => map.upsert(JQ_SYMBOLS, node, () => Symbol("node emitter key"));

        // DOCS-4078: map keys must NOT be JQuery objects, but separate symbols for the embedded DOM nodes!
        // This allows to register/unregister event listeners using different JQuery collection instances
        // covering the same DOM elements.
        if (isJQueryEmitter(emitter)) {
            if (emitter.length === 1) {
                // performance: do not re-wrap single DOM node into a new JQuery collection
                yield [getSymbol(emitter[0]), emitter];
            } else {
                for (const node of emitter.get()) {
                    yield [getSymbol(node), $(node)];
                }
            }
        } else {
            yield [emitter, emitter];
        }
    }

    /**
     * Yields all map keys for the passed event emitter. If the emitter is a
     * JQuery collection, all its DOM nodes will be processed separately.
     */
    *#yieldForUnbind(emitter: AnyEmitter<any>): IterableIterator<EmitterWrapperKey> {

        // DOCS-4078: map keys must NOT be JQuery objects, but separate symbols for the embedded DOM nodes!
        // This allows to register/unregister event listeners using different JQuery collection instances
        // covering the same DOM elements.
        if (isJQueryEmitter(emitter)) {
            for (const node of emitter.get()) {
                const symbol = JQ_SYMBOLS.get(node);
                if (symbol) { yield symbol; }
            }
        } else {
            yield emitter;
        }
    }
}
