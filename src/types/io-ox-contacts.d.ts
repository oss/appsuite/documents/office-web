/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
// ambient type declarations for JS modules in "io.ox/contacts"
// ============================================================================

declare module "$/io.ox/contacts/api" {

    export interface PictureDataOptions {
        user_id?: number | string;
        folder_id?: number | string;
        contact_id?: number | string;
    }

    export interface PictureHaloOptions {
        width?: number; // default: 48
        height?: number; // default: 48
        scaleType?: string; // default: "cover"
        lazyload?: boolean;
        effect?: string; // default: "show"
        urlOnly?: boolean;
        fallback?: boolean; // default: true
    }

    const api: {
        pictureHalo(targetNode: JQuery, data: PictureDataOptions, options: PictureHaloOptions & { urlOnly: true }): string;
        pictureHalo(targetNode: JQuery, data: PictureDataOptions, options?: PictureHaloOptions): JQuery;
    };

    export default api;
}
