/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
// ambient type declarations for basic JS modules
// ============================================================================

declare module "$/underscore" {

    import { UnderscoreStatic } from "underscore";

    interface OXUnderscore extends UnderscoreStatic {

        browser: Dict<boolean | number>;

        noI18n: {
            (text: string): string;
            fix(text: string): string;
            format(text: string, ...args: unknown[]): string;
            assemble(text: string, argFn: (index: number) => unknown, textFn?: (text: string) => unknown): unknown[];
        };

        device(condition: string): boolean;
    }

    const _: OXUnderscore;
    export default _;
}

// ============================================================================

declare module "$/jquery" {

    // OX extensions for the JQuery library must be in a "declare global" block
    // (the type definitions in `@types/jquery` do not export the types explicitly).
    // See "./globals.d.ts" (this file cannot contain a "declare global" block).

    export { default } from "jquery";
}

// ============================================================================

declare module "$/backbone" {

    import Backbone from "backbone";
    import { InferEventArgs, TypedEventHandlerFn, TypedAllEventsHandlerFn } from "@/io.ox/office/tk/events";

    /**
     * Shape of objects implementing the Backbone event system, with strongly
     * typed event names and arguments.
     *
     * @template E
     *  The type mapping interface for the supported event types. The interface
     *  keys are the supported event type keys, the interface property types
     *  are tuples for the types of the arguments emitted for that event.
     */
    export interface BackboneEmitter<E> {

        on<K extends keyof E, A extends InferEventArgs<E, K>>(type: "all", callback: TypedAllEventsHandlerFn<this, E, K, A>): this;
        on<K extends keyof E>(type: K, callback: TypedEventHandlerFn<this, E, K>): this;

        once<K extends keyof E, A extends InferEventArgs<E, K>>(type: "all", callback: TypedAllEventsHandlerFn<this, E, K, A>): this;
        once<K extends keyof E>(type: K, callback: TypedEventHandlerFn<this, E, K>): this;

        off<K extends keyof E, A extends InferEventArgs<E, K>>(type: "all", callback?: TypedAllEventsHandlerFn<this, E, K, A> | null): this;
        off<K extends keyof E>(type?: K | null, callback?: TypedEventHandlerFn<this, E, K> | null): this;

        trigger<K extends keyof E & string>(type: K, ...args: InferEventArgs<E, K>): this;

        listenTo<E2, K extends keyof E2>(emitter: BackboneEmitter<E2>, event: K, callback: TypedEventHandlerFn<this, E2, K>): this;
        listenToOnce<E2, K extends keyof E2>(emitter: BackboneEmitter<E2>, event: K, callback: TypedEventHandlerFn<this, E2, K>): this;
        stopListening<E2, K extends keyof E2>(emitter?: BackboneEmitter<E2>, event?: K, callback?: TypedEventHandlerFn<this, E2, K>): this;
    }

    // helper type: `Backbone.Model` without the `EventsMixin` interface
    type BareModel<AttrMapT extends Backbone.ObjectHash> = Omit<Backbone.Model<AttrMapT>, keyof Backbone.EventsMixin>;

    /**
     * The `BackboneEmitter` interface as base class.
     */
    interface BackboneEvents<E = Empty> extends BackboneEmitter<E> { }
    class BackboneEvents<E = Empty> implements BackboneEmitter<E> { }
    export { type BackboneEvents };

    /**
     * Type mapping for the events emitted by `BackboneModel` instances.
     */
    export interface BackboneModelEventMap<AttrMapT extends Backbone.ObjectHash> extends
        Record<`change:${string}`, [model: BackboneModel<AttrMapT>, value: AttrMapT[keyof AttrMapT]]> {
        change: [model: BackboneModel<AttrMapT>];
    }

    interface BackboneModel<
        AttrMapT extends Backbone.ObjectHash,
        EvtMapT extends BackboneModelEventMap<AttrMapT> = BackboneModelEventMap<AttrMapT>
    > extends BareModel<AttrMapT>, BackboneEmitter<EvtMapT> { }

    /**
     * Replacement type for class `Backbone.Model` with strong event types.
     */
    class BackboneModel<
        AttrMapT extends Backbone.ObjectHash,
        EvtMapT extends BackboneModelEventMap<AttrMapT> = BackboneModelEventMap<AttrMapT>
    > implements BareModel<AttrMapT>, BackboneEmitter<EvtMapT> { }

    export { type BackboneModel };

    export default Backbone;
}

// ============================================================================

declare module "$/moment" {

    export { default } from "moment";
}

// ============================================================================

declare module "$/url" {

    export function locationHash(): Dict<string>;
    export function locationHash(key: string): Opt<string>;
    export function locationHash(key: string, value: string | number | boolean | null): void;
}

// ============================================================================

declare module "$/ox" {

    import { BackboneEmitter } from "$/backbone";
    import { App, AppLoaderFn } from "$/io.ox/core/desktop";

    /**
     * Type mapping for the events emitted by the `ox` global object.
     */
    export interface OXStaticEventMap {
        "app:init": [app: App];
        "app:start": [app: App];
        "app:ready": [app: App];
        "app:resume": [app: App];
        "app:stop": [app: App];
        "change:locale": [];
        "change:locale:data": [];
        "change:document:title": [title: string];
        beforeunload: [unsavedChanges: boolean];
    }

    export interface OXStatic extends BackboneEmitter<OXStaticEventMap> {

        readonly abs: string;
        readonly apiRoot: string;
        readonly debug?: boolean;
        readonly language: string;
        readonly locale: string;
        readonly office?: {
            readonly isOffice?: boolean;
            readonly _UNITTEST?: boolean;
        };
        readonly online: boolean;
        readonly root: string;
        readonly session: string;
        readonly serverConfig: {
            readonly version?: string;
            readonly productName?: string;
            readonly languages?: Dict<string>;
            readonly staySignedIn?: boolean;
            readonly openInSingleTab?: boolean;
        };
        readonly theme: string;
        readonly ui: never; // import from "io.ox/core/desktop" instead!
        readonly user: string;
        readonly user_id: number;
        readonly version: string;
        readonly tabHandlingEnabled?: boolean;

        launch(loader: AppLoaderFn, data?: unknown): Promise<App>;
    }

    const ox: OXStatic;
    export default ox;
}

// ============================================================================

declare module "*.json" {
    const content: unknown;
    export default content;
}
