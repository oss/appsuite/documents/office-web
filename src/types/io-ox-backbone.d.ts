/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
// ambient type declarations for JS modules in "io.ox/backbone"
// ============================================================================

declare module "$/io.ox/backbone/mini-views/abstract" {

    export interface AbstractViewConfig<DT extends object> extends Backbone.ViewOptions<Backbone.Model<DT>> {
        model: Backbone.Model<DT>;
        name?: string;
    }

    export default abstract class AbstractView<
        DT extends object,
        ConfigT extends AbstractViewConfig<DT>,
        ElementT extends HTMLElement = HTMLDivElement
    > extends Backbone.View<Backbone.Model<DT>> {

        readonly el: ElementT; // `Backbone.View` declares this property as `any`
        readonly $el: JQuery<ElementT>; // `Backbone.View` declares this property as `JQuery`
        readonly name?: string;

        protected constructor(config?: ConfigT);

        render(): this;

        dispose(): void;

        protected setup?(config: ConfigT): void;
    }
}

// ============================================================================

declare module "$/io.ox/backbone/mini-views/dropdown" {

    import AbstractView, { AbstractViewConfig } from "$/io.ox/backbone/mini-views/abstract";
    import { NodeOrJQuery } from "@/io.ox/office/tk/dom";

    export interface DropdownConfig<DT extends object> extends AbstractViewConfig<DT> {
        label?: string | NodeOrJQuery | FuncType<NodeOrJQuery>;
        smart?: boolean;
        margin?: number;
        caret?: boolean;
        dropup?: boolean;
        keep?: boolean; // keep open after click
    }

    export interface DropdownOptionOptions {
        title?: string;
        radio?: boolean;
    }

    export interface DropdownLinkOptions {
        icon?: boolean;
        data?: unknown;
    }

    export default class Dropdown<DT extends object> extends AbstractView<DT, DropdownConfig<DT>> {

        readonly $toggle: JQuery<HTMLAnchorElement>; // only available after calling `render()`!
        readonly $ul: JQuery;
        readonly $placeholder: JQuery;
        readonly smart?: boolean;
        readonly margin: number;

        constructor(config?: DropdownConfig<DT>);

        header(text: string): this;
        divider(): this;
        group(text: string, nodes: string | NodeOrJQuery): this;

        append(nodes: string | NodeOrJQuery): this;
        option<KT extends KeysOf<DT>>(name: KT, value: DT[KT], text: string, options?: DropdownOptionOptions): this;
        link(name: string, text: string, callback?: FuncType<void>, options?: DropdownLinkOptions): this;
    }
}

// ============================================================================

declare module "$/io.ox/backbone/mini-views/listutils" {

    export interface DeleteButtonOptions {
        title?: string;
    }

    const api: {
        controlsDelete(options?: DeleteButtonOptions): JQuery<HTMLAnchorElement>;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/backbone/views/disposable" {

    import Backbone from "$/backbone";
    import { JQueryEventHandlerFn } from "@/io.ox/office/tk/events";

    class DisposableView extends Backbone.View {

        dispose(automatic?: boolean): void;
        onDispose(): void;
        listenToDOM<K extends keyof JTriggeredEventMap>(element: Element, event: K, handler: JQueryEventHandlerFn<this, K>): void;
    }

    export default DisposableView;
}

// ============================================================================

declare module "$/io.ox/backbone/views/extensible" {

    import DisposableView from "$/io.ox/backbone/views/disposable";

    class ExtensibleView extends DisposableView {

        extend(extensions: Dict): this;
        invoke(type?: string): this;
        inject(functions: Dict<AnyFunction>): this;
        render(): this;

        disableFormElements(): void;
        enableFormElements(): void;

        busy(): this;
        idle(): this;
    }

    export default ExtensibleView;
}

// ============================================================================

declare module "$/io.ox/backbone/views/modal" {

    import ExtensibleView from "$/io.ox/backbone/views/extensible";
    import { NodeOrJQuery } from "@/io.ox/office/tk/dom";

    export interface ModelDialogFocusOptions {
        focus?: string | NodeOrJQuery;
    }

    export interface ModelDialogConfig extends ModelDialogFocusOptions {
        title: string;
        width?: number;
        enter?: string;
        async?: boolean;
        maximize?: boolean;
        previousFocus?: NodeOrJQuery | false;
    }

    export interface ModelDialogFooterButtonOptions {
        action?: string;
        label?: string;
        placement?: "left" | "right";
        className?: string;
        disabled?: boolean;
    }

    export interface ModelDialogFooterCheckboxOptions {
        action?: string;
        label?: string;
        className?: string;
        status?: boolean;
    }

    class ModalDialog extends ExtensibleView {

        $header: JQuery;
        $body: JQuery;
        $footer: JQuery;

        constructor(config?: ModelDialogConfig);

        setFocus(options?: ModelDialogFocusOptions): void;

        addDescription(descr: string | NodeOrJQuery): this;

        addButton(options?: ModelDialogFooterButtonOptions): this;
        addCloseButton(): this;
        addCancelButton(options?: ModelDialogFooterButtonOptions): this;
        addAlternativeButton(options?: ModelDialogFooterButtonOptions): this;

        addCheckbox(options?: ModelDialogFooterCheckboxOptions): this;

        invokeAction(action: string): void;

        pause(): void;
        resume(): void;

        open(): this;
        close(): this;
    }

    export default ModalDialog;
}

// ============================================================================

declare module "$/io.ox/backbone/mini-views" {

    import _AbstractView from "$/io.ox/backbone/mini-views/abstract";

    const api: {
        AbstractView: typeof _AbstractView;
    };

    export default api;
}
