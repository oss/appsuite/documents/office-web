/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
// ambient type declarations for JS modules in "io.ox/files"
// ============================================================================

declare module "$/io.ox/files/api" {

    export type FileType = "image" | "audio" | "video" | "vcf" | "doc" | "xls" | "ppt" | "pdf" | "zip" | "txt" | "guard";

    class Model extends Backbone.Model {

        // static constants attached to the class prototype (doh!)
        readonly types: Record<FileType, RegExp>;

        isFolder(): boolean;
        isFile(): boolean;
        isDriveItem(): boolean;

        isSVG(mimetype?: string): boolean;
        isTiff(mimetype?: string): boolean;
        isImage(mimetype?: string): boolean;
        isAudio(mimetype?: string): boolean;
        isVideo(mimetype?: string): boolean;

        isPDF(mimetype?: string): boolean;
        isZIP(mimetype?: string): boolean;
        isText(mimetype?: string): boolean;

        isOffice(mimetype?: string): boolean;
        isWordprocessing(mimetype?: string): boolean;
        isSpreadsheet(mimetype?: string): boolean;
        isPresentation(mimetype?: string): boolean;

        isPgp(mimetype: string): boolean;
        isGuard(): boolean;
        isEncrypted(): boolean;
        isLocked(): boolean;

        isMailAttachment(): boolean;
        isComposeAttachment(): boolean;
        isPIMAttachment(): boolean;
        isEmptyFile(): boolean;
        supportsPreview(): boolean;

        getDisplayName(): string;
        getExtension(): string;
        getGuardExtension(): string;
        getMimeType(): string;
        getGuardMimeType(): string;
        getFileType(): FileType | "folder" | false;
        getGuardType(): FileType | "folder" | false;
    }

    const api: {
        Model: typeof Model;
    };

    export default api;
}
