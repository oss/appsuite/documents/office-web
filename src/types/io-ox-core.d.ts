/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
// ambient type declarations for JS modules in "io.ox/core"
// ============================================================================

declare module "$/io.ox/core/capabilities" {

    const api: {
        has(cap: string): boolean;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/core/components" {
    // path to icon file, or SVG markup, or promise with SVG markup, or callback returning any of them
    export function createIcon(source: MaybeAsync<string> | FuncType<MaybeAsync<string>>): JQuery<SVGElement>;
}

// ============================================================================

declare module "$/io.ox/core/desktop" {

    import { BackboneModelEventMap, BackboneModel } from "$/backbone";
    import { CoreEmitter } from "$/io.ox/core/event";

    /**
     * Type mapping for the events emitted by `AppWindow` instances.
     */
    export interface AppWindowEventMap {
        open: [];
        beforeshow: [];
        show: [];
        beforehide: [];
        hide: [];
        busy: [];
        idle: [];
        close: [];
        beforequit: [];
        quit: [];
        destroy: [];
    }

    export interface AppWindow extends CoreEmitter<AppWindowEventMap> {

        readonly nodes: {
            readonly outer: JQuery;
        };
        readonly state: {
            visible: boolean;
        };
        floating?: Backbone.View;

        setTitle(title: string): void;

        busy(): void;
        idle(): void;
    }

    export interface AppLauncher {
        getApp(data?: unknown): App;
    }

    export interface AppLauncherModule {
        default: AppLauncher;
    }

    export type AppLoaderFn = (data?: unknown) => Promise<AppLauncherModule>;

    /**
     * Data attributes that can be set at the application instance (Backbone
     * model attributes).
     */
    export interface AppAttributeMap extends Dict {
        title: string;
        startHidden: boolean;
    }

    /**
     * Type mapping for the events emitted by `App` instances.
     */
    export interface AppEventMap<AttrMapT extends AppAttributeMap = AppAttributeMap> extends BackboneModelEventMap<AttrMapT> {
        launch: [app: App];
        resume: [app: App];
        quit: [];
    }

    class App<
        AttrMapT extends AppAttributeMap = AppAttributeMap,
        EvtMapT extends AppEventMap<AttrMapT> = AppEventMap<AttrMapT>
    > extends BackboneModel<AttrMapT, EvtMapT> {

        static get(name: string): App[];
        static isCurrent(app: App): boolean;
        static getCurrentApp(): App | null;
        static getCurrentFloatingApp(): Opt<App>;
        static getCurrentWindow(): AppWindow | null;

        hasUnsavedChanges?: () => boolean;

        getName(): string;

        getWindow(): Opt<AppWindow>;
        getWindowNode(): JQuery;

        setState(state: Dict<string>): void;
        setTitle(title: string): void;
        setQuit(fn: (options?: object) => MaybeAsync): void;

        quit(force?: boolean, options?: object): JPromise;
    }

    // type export only, no named export at runtime!
    export type { App };

    const ui: {
        App: typeof App;
    };

    export default ui;
}

// ============================================================================

declare module "$/io.ox/core/event" {

    import { InferEventArgs } from "@/io.ox/office/tk/events";

    /**
     * A callback handler function for events triggered by a JQuery-like core
     * event emitter (instance of class `Events`, or classes implementing the
     * `CoreEmitter` interface).
     */
    export type CoreEventHandlerFn<T, E, K extends keyof E> = (this: T, event: JEvent, ...args: InferEventArgs<E, K>) => void;

    /**
     * A callback handler function for the "triggered" event emitted by a
     * JQuery-like core event emitter (instance of class `Events`, or any other
     * class implementing the `CoreEmitter` interface).
     */
    export type CoreAllEventsHandlerFn<T, E> = (this: T, event: JEvent, type: keyof E, ...args: InferEventArgs<E>) => void;

    /**
     * Shape of objects implementing the (JQuery-like) core event system.
     *
     * @template E
     *  The type mapping interface for the supported event types. The interface
     *  keys are the supported event type keys, the interface property types
     *  are tuples for the types of the arguments (without the leading event
     *  object) emitted for that event.
     */
    export interface CoreEmitter<E> {

        on(type: "triggered", handler: CoreAllEventsHandlerFn<this, E>): this;
        on<K extends keyof E>(type: K, handler: CoreEventHandlerFn<this, E, K>): this;

        one(type: "triggered", handler: CoreAllEventsHandlerFn<this, E>): this;
        one<K extends keyof E>(type: K, handler: CoreEventHandlerFn<this, E, K>): this;

        off(type: "triggered", handler?: CoreAllEventsHandlerFn<this, E>): this;
        off<K extends keyof E>(type?: K, handler?: CoreEventHandlerFn<this, E, K>): this;

        trigger<K extends keyof E>(type: K, ...args: InferEventArgs<E, K>): this;
    }

    // declaration merging to add all properties of base interfaces automatically
    interface Events<E> extends CoreEmitter<E>, Destroyable { }

    class Events<E> implements CoreEmitter<E>, Destroyable {
        static extend<T extends object, E>(obj: T): T & CoreEmitter<E> & { events: Events<E> };
        constructor();
    }

    export default Events;
}

// ============================================================================

declare module "$/io.ox/core/http" {

    export interface RequestOptions {
        module?: string;
        params?: Dict;
        data?: Dict | string;
        dataType?: string;
        appendColumns?: boolean;
        columnModule?: string;
        appendSession?: boolean;
        processData?: boolean;
        processResponse?: boolean;
        cursor?: boolean;
    }

    export interface ResponsePromise extends JPromise<unknown>, Abortable { }

    const api: {
        GET(options?: RequestOptions): ResponsePromise;
        POST(options?: RequestOptions): ResponsePromise;
        PUT(options?: RequestOptions): ResponsePromise;
        DELETE(options?: RequestOptions): ResponsePromise;
        UPLOAD(options?: RequestOptions): ResponsePromise;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/core/http-client" {

    interface ExtendOptions {
        prefixUrl?: string;
        timeout?: number;
        hooks?: object;
        searchParams?: URLSearchParams;
    }

    interface PayLoad {
        json: { payload: string };
    }

    interface ResponsePromise<R = unknown> extends Promise<R> {
        json<T = unknown>(): Promise<T>;
    }

    class HttpClient {
        extend(options?: ExtendOptions): HttpClient;
        post(route: string, payload: PayLoad): ResponsePromise;
    }

    export const httpClient: HttpClient;
}

// ============================================================================

declare module "$/io.ox/core/locale" {

    export interface CoreLocaleData {

        number: string;

        date: string;
        dateShort: string;
        dateMedium: string;
        dateLong: string;
        dateFull: string;

        time: string;
        timeLong: string;

        firstDayOfWeek: string;
        firstDayOfYear: number;
    }

    const api: {
        number(n: number, d?: number): string;
        currency(n: number, code?: string): string;
        percent(n: number, d?: number): string;

        current(): string;

        getLocaleData(): CoreLocaleData;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/core/manifests" {

    export interface ManifestManager {
        hasPluginsFor(moduleName: string): Opt<boolean>;
        loadPluginsFor(moduleName: string): Promise<void>;
        pluginsFor(moduleName: string): string[];
        pluginPoints?: Dict<Dict[]>;
    }

    export const manifestManager: ManifestManager;
}

// ============================================================================

declare module "$/io.ox/core/yell" {

    export interface YellConfig {
        type?: string;
        message?: string;
        duration?: number;
        focus?: boolean;
        closeOnClick?: boolean;
    }

    // different call signatures for the yell() function
    function yell(config: YellConfig): Opt<JQuery<HTMLDivElement>>;
    function yell(type: string, message?: string, config?: YellConfig): Opt<JQuery<HTMLDivElement>>;
    function yell(type: "close" | "destroy"): void;

    // methods attached to the yell() function
    namespace yell {
        export function done(): void;
        export function close(): void;
    }

    export default yell;
}

// ============================================================================

declare module "$/io.ox/core/settings" {

    import { BackboneEvents } from "$/backbone";

    export interface SettingsLoadData {
        tree: Dict;
        meta: Dict;
    }

    /**
     * Type mapping for the events emitted by `Settings` instances.
     */
    export interface SettingsEventMap extends
        Record<`change:${string}`, [value: unknown, previous: unknown]> {
        change: [path: string, value: unknown, previous: unknown];
    }

    export class Settings extends BackboneEvents<SettingsEventMap> {
        constructor(path: string, defaults: () => MaybeAsync<Dict>);
        ensureData(): Promise<void>;
        contains(key: string): boolean;
        get(): Dict;
        get(key: string, def?: unknown): unknown;
        set(key: string, value: unknown, options?: { silent?: boolean }): this;
        remove(key: string): this;
        stringify(): string;
        detach(): this;
        load(): JPromise<SettingsLoadData>;
        reload(): JPromise<SettingsLoadData>;
        isPending(): boolean;
        save(custom?: unknown, options?: { force?: boolean }): JPromise;
        merge(custom?: unknown): JPromise;
        saveAndYell(custom?: unknown, options?: { force?: boolean; debug?: boolean }): JPromise;
    }

    export function load(path: string): Settings;

    export const settings: Settings;
}

// ============================================================================

declare module "$/io.ox/core/api/autocomplete" {

    export interface AutocompleteConfig {
        users?: boolean;
        contacts?: boolean;
        distributionlists?: boolean;
        resources?: boolean;
        groups?: boolean;
        split?: boolean;
        limit?: number;
    }

    export interface AutocompleteData {
        first_name?: string;
        last_name?: string;
        display_name?: string;
        field?: "email1" | "email2" | "email3";
        email1?: string;
        email2?: string;
        email3?: string;
        internal_userid: string | number;
        folder_id?: string | number;
        id?: string | number;
    }

    export default class Autocomplete {
        constructor(config?: AutocompleteConfig);
        search(query: string): JPromise<AutocompleteData[]>;
    }
}

// ============================================================================

declare module "$/io.ox/core/api/tab" {

    import { BackboneEmitter } from "$/backbone";
    import { InferEventArgs } from "@/io.ox/office/tk/events";

    /**
     * Options for creating the URL of a new browser tab.
     */
    export interface CreateURLOptions {

        /**
         * The keys of all anchor parameters in the current URL to be ignored.
         */
        exclude?: string[] | string;

        /**
         * A string to extend the URL with (before the anchor parameters). May
         * also contain query parameters. Must not contain a leading slash
         * character.
         */
        suffix?: string;
    }

    export interface OpenTabOptions {
        windowName: string;
        windowType: string;
        parentName?: string;
    }

    /**
     * Type mapping for the events emitted by `TabAPI.communicationEvents`.
     */
    export interface TabCommunicationEventMap {
        "show-in-drive": [{ folder_id: string; id: string; targetWindow?: string }];
        "get-active-windows": [{ exceptWindow?: string }];
        "update-ox-object": [{ language?: string; locale?: string; theme?: string; exceptWindow?: string }];
    }

    const api: {
        communicationEvents: BackboneEmitter<TabCommunicationEventMap>;

        createUrl(params?: Dict, options?: CreateURLOptions): string;

        openNewTab(url: string, options?: OpenTabOptions): Window | null;

        openParentTab(url: string): void;
        openParentTab(params?: Dict, options?: CreateURLOptions): void;

        openChildTab(url: string): Window | null;
        openChildTab(params?: Dict, options?: CreateURLOptions): Window | null;

        getParentWindowName(): string;

        propagate<K extends keyof TabCommunicationEventMap>(key: K, ...args: InferEventArgs<TabCommunicationEventMap, K>): void;

        getGuestMode(): boolean;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/core/settings/util" {

    import { Settings } from "$/io.ox/core/settings";
    import { NodeOrJQuery } from "@/io.ox/office/tk/dom";

    export function checkbox(name: string, label: string, settings: Settings): JQuery;
    export function fieldset(label: string, ...children: NodeOrJQuery[]): JQuery<HTMLFieldSetElement>;
}

// ============================================================================

declare module "$/io.ox/core/theming/main" {

    export interface ThemeData {
        theme: string;
        accentColor: string;
    }

    const api: {
        getCurrent(): ThemeData;
        restoreCurrent(): void;
        saveCurrent(): void;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/core/tk/visibility-api-util" {

    export interface VisApiEvent {
        currentHiddenState: boolean;
        oldState: boolean;
    }

    const api: {
        readonly isHidden: boolean;
        readonly isSupported: boolean;
        readonly hiddenAttribute: string;
        readonly visibilityChangeEvent: string;
    };

    export default api;
}

// ============================================================================

declare module "$/io.ox/core/tk/wizard" {

    import { BackboneEvents } from "$/backbone";
    import { AppLoaderFn } from "$/io.ox/core/desktop";
    import DisposableView from "$/io.ox/backbone/views/disposable";
    import { CSSBorderPos, NodeOrJQuery } from "@/io.ox/office/tk/dom";

    export interface WizardRegistryOptions {
        id: string;
    }

    export interface WizardRegistry {
        add(options: WizardRegistryOptions, fn: () => unknown): Backbone.Model;
    }

    export interface StepConfig {
        modal?: boolean; // default: true
        focusWatcher?: boolean; // default: true
        back?: boolean; // default: true
        next?: boolean; // default: true
        enableBack?: boolean; // default: true
        labelBack?: string; // default: "Back"
        labelDone?: string; // default: "Finish"
    }

    export interface StepReferToOptions {

        /**
         * The preferred position of the popup box, relative to the DOM element
         * referred by the step. If the box does not fit at the specified
         * position, it will be placed at one of the other borders. Default
         * value is "right".
         */
        position?: CSSBorderPos;
    }

    export interface StepHotSpotOptions {
        left?: number;
        top?: number;
        tooltip?: string;
    }

    export type StepHotSpotPair = [selector: string, options?: StepHotSpotOptions];

    class Step<ParentT extends Wizard> extends DisposableView {

        readonly parent: ParentT;

        constructor(config?: StepConfig);

        title(...contents: Array<string | NodeOrJQuery>): this;
        content(...contents: Array<string | NodeOrJQuery>): this;
        footer(...contents: Array<string | NodeOrJQuery>): this;

        waitFor(selector: string, timeout?: number): this;
        referTo(selector: string, options?: StepReferToOptions): this;
        hotspot(selector: string, options?: StepHotSpotOptions): this;
        hotspot(hotspots: StepHotSpotPair[]): this;
        navigateTo(loader: AppLoaderFn, data?: unknown): this;
    }

    // class will not be exported at runtime
    export type { Step };

    /**
     * Type mapping for the events emitted by `Wizard` instances.
     */
    export interface WizardEventMap {
        "change:step": [];
        "skip:step": [];
        "before:start": [];
        start: [];
        "before:stop": [];
        stop: [];
    }

    export default class Wizard extends BackboneEvents<WizardEventMap> {

        static readonly registry: WizardRegistry;

        constructor();

        step(config?: StepConfig): Step<this>;

        start(): this;
        close(): this;

        protected getCurrentStep(): Opt<Step<this>>;
        protected withCurrentStep(fn: (step: Step<this>) => void): void;
        protected setCurrentStep(index: number): void;
        protected get(index: number): Opt<Step<this>>;
    }
}
