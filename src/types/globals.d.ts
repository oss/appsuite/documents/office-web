/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
// global type definitions
// ============================================================================

/**
 * Shortcut for `undefined` or `null`.
 */
type nullish = undefined | null;

/**
 * Shortcut for the types of all falsy values (except `NaN`).
 */
type falsy = nullish | false | 0 | "";

/**
 * Shortcut for all primitive value types.
 */
type primitive = nullish | number | bigint | string | boolean | symbol;

/**
 * Adds `undefined` to the type specifier.
 *
 * @template T
 *  The type to be extended with `undefined`.
 *
 * @returns
 *  The passed type, extended with `undefined`.
 */
type Opt<T> = T | undefined;

/**
 * Adds `undefined` and `null` to the type specifier.
 *
 * Note: The opposite type `NonNullable<T>` removing `null` and `undefined`
 * from a type is available in the standard library of TypeScript.
 *
 * @template T
 *  The type to be extended with `undefined` and `null`.
 *
 * @returns
 *  The passed type, extended with `undefined` and `null`.
 */
type Nullable<T> = T | nullish;

/**
 * Adds the readonly array type to the passed type.
 *
 * @template T
 *  The type to be extended with its readonly array type.
 *
 * @returns
 *  The passed type, extended with its readonly array type.
 */
type OrArray<T> = T | readonly T[];

/**
 * Makes all properties in the passed type writable (removes all `readonly`
 * modifiers).
 *
 * @template T
 *  The type to be made writable.
 *
 * @returns
 *  The passed type, with `readonly` removed from all properties.
 */
type Writable<T> = {
    -readonly [P in keyof T]: T[P];
};

/**
 * A tuple type for two values of different types (optionally of the same
 * type).
 *
 * @template T
 *  The type of the first value.
 *
 * @template U
 *  The type of the second value.
 */
type Pair<T, U = T> = [first: T, second: U];

/**
 * A plain object with arbitrary string properties of the specified type.
 *
 * @template T
 *  The type of the property values of the dictionary type.
 */
interface Dict<T = unknown> {
    [key: string]: T;
}

/**
 * A empty interface intended to be used as default value for generic type
 * parameters. Better readable than the plain `{}` type.
 */
interface Empty { }

/**
 * A convenience alias for partial records.
 *
 * @template KT
 *  The type of the property keys in the record.
 *
 * @template VT
 *  The type of the property values in the record.
 */
type PtRecord<KT extends PropertyKey, VT> = Partial<Record<KT, VT>>;

/**
 * A convenience alias for records with readonly properties.
 *
 * @template KT
 *  The type of the property keys in the record.
 *
 * @template VT
 *  The type of the property values in the record.
 */
type RoRecord<KT extends PropertyKey, VT> = Readonly<Record<KT, VT>>;

/**
 * Readable alias for the type of a function.
 *
 * @template RetT
 *  The type of the return value of the function.
 *
 * @template ArgsT
 *  The types of the function parameters. If omitted, a function without
 *  parameters will be declared.
 */
type FuncType<RetT, ArgsT extends unknown[] = []> = (...args: ArgsT) => RetT;

/**
 * Readable alias for the type of a function with typed `this` context.
 *
 * @template ThisT
 *  The type of the `this` context.
 *
 * @template RetT
 *  The type of the return value of the function.
 *
 * @template ArgsT
 *  The types of the function parameters. If omitted, a function without
 *  parameters will be declared.
 */
type ThisFuncType<ThisT extends object, RetT, ArgsT extends unknown[] = []> = (this: ThisT, ...args: ArgsT) => RetT;

/**
 * Readable alias for the type of any function (`this` type and parameter types
 * are `any`, and return type is `unknown` to prevent linter errors using `any`
 * values). Useful for example in generic type constraints.
 */
type AnyFunction<ArgsT extends unknown[] = any[]> = (this: any, ...args: ArgsT) => unknown;

/**
 * Readable alias for the type of a class constructor function.
 *
 * @template InstT
 *  The instance type of the class (type of the value returned by the class
 *  constructor function).
 *
 * @template ArgsT
 *  The types of the constructor parameters. If omitted, a constructor function
 *  without parameters will be declared.
 */
type CtorType<InstT, ArgsT extends unknown[] = []> = new (...args: ArgsT) => InstT;

/**
 * Readable alias for the type of an abstract class constructor function.
 *
 * @template InstT
 *  The instance type of the class (type of the value returned by the class
 *  constructor function).
 *
 * @template ArgsT
 *  The types of the constructor parameters. If omitted, a constructor function
 *  without parameters will be declared.
 */
type AbstractCtorType<InstT, ArgsT extends unknown[] = []> = abstract new (...args: ArgsT) => InstT;

/**
 * Returns the string keys of all properties of type `T`. Omits number keys and
 * symbol keys.
 *
 * @template T
 *  The type to be examined.
 *
 * @returns
 *  The string keys of all properties in `T`.
 */
type KeysOf<T> = keyof T & string;

/**
 * Returns the string keys of all properties of type `T` having the type `F`.
 *
 * The following example sets type `NumKeys` to `"num1"|"num2"` (the names of
 * all numeric properties of `Test`):
 *
 * @example
 *  interface Test { num1: number; str1: string; num2: number; }
 *  type NumKeys = KeysOfType<Test, number>;
 *
 * @template T
 *  The type to be examined, containing properties of various types.
 *
 * @template F
 *  The type of the properties to be filtered.
 *
 * @returns
 *  The keys of all properties in `T` having type `F`.
 */
type KeysOfType<T, F> = { [K in keyof T]: T[K] extends F ? K : never }[KeysOf<T>];

/**
 * Replaces the value types of all properties of an object type.
 */
type MapType<T, V> = { [K in keyof T]: V };

/**
 * Replaces the value types of all properties of an object type, and turns the
 * new type to a partial type (all properties are optional).
 */
type PtMapType<T, V> = { [K in keyof T]?: V };

/**
 * Adds a type union (type operator `|`) to the value types of all properties
 * of an object type.
 */
type MapUnion<T, V> = { [K in keyof T]: T[K] | V };

/**
 * Adds a type intersection (type operator `&`) to the value types of all
 * properties of an object type.
 */
type MapIntersect<T, V> = { [K in keyof T]: T[K] & V };

/**
 * Shortcut for an `Iterable` type for map entries with leading generic key.
 */
type EntryIterable<KT, VT> = Iterable<[KT, VT]>;

/**
 * Shortcut for an `IterableIterator` type for map entries with leading generic
 * key.
 */
type EntryIterator<KT, VT> = IterableIterator<[KT, VT]>;

/**
 * Shortcut for an `Iterable` type for array entries with leading element
 * index.
 */
type IndexedIterable<VT> = EntryIterable<number, VT>;

/**
 * Shortcut for an `IterableIterator` type for array entries with leading
 * element index.
 */
type IndexedIterator<VT> = EntryIterator<number, VT>;

/**
 * Shortcut for an `Iterable` type for dictionary entries with leading string
 * key.
 */
type KeyedIterable<VT> = EntryIterable<string, VT>;

/**
 * Shortcut for an `IterableIterator` type for dictionary entries with leading
 * string key.
 */
type KeyedIterator<VT> = EntryIterator<string, VT>;

/**
 * Generic concept of a resource with a globally unique identifier.
 */
interface Identifiable {

    /**
     * The globally unique identifier for this instance.
     */
    readonly uid: string;
}

/**
 * Generic concept of an object with a key property. In difference to UIDs,
 * different objects may have the same key.
 */
interface Keyable {

    /**
     * The key of the object.
     */
    readonly key: string;
}

/**
 * Generic concept of a destroyable resource.
 */
interface Destroyable {

    /**
     * Destroys this instance. Afterwards, the instance must not be used
     * anymore, and all references to it must be released. The actual behavior
     * of this method is dependent on the object implementing this interface.
     */
    destroy(): void;
}

/**
 * Generic concept of a disconnectable resource (e.g. DOM observers).
 */
interface Disconnectable {

    /**
     * Disconnects this instance from all resources it is observing.
     */
    disconnect(): void;
}

/**
 * Generic concept of an abortable process.
 */
interface Abortable {

    /**
     * Aborts the process represented by this instance.
     */
    abort(): void;
}

/**
 * General concept of objects that can compare themselves to a value (usually
 * but not necessarily of their own type).
 */
interface Equality<T> {

    /**
     * Returns whether the passed value is considered to be equal to this
     * object.
     *
     * @param value
     *  The value to compare this object against.
     *
     * @returns
     *  Whether the passed value is considered to be equal to this object.
     */
    equals(value: T): boolean;
}

/**
 * General concept of objects that can clone themselves.
 */
interface Cloneable<T> {

    /**
     * Returns a clone of this object.
     *
     * @returns
     *  The clone of this object.
     */
    clone(): T;
}

/**
 * General concept of objects that can clone themselves and return an instance
 * of their actual subclass.
 */
interface CloneableThis {

    /**
     * Returns a clone of this object.
     *
     * @returns
     *  The clone of this object.
     */
    clone(): this;
}

// TypeScript extensions ------------------------------------------------------

// workaround for https://github.com/microsoft/TypeScript/issues/16655
interface Array<T> {
    filter<U extends T>(predicate: BooleanConstructor, context?: object): Array<Exclude<U, falsy>>;
}

// workaround for https://github.com/microsoft/TypeScript/issues/16655
interface ReadonlyArray<T> {
    filter<U extends T>(predicate: BooleanConstructor, context?: object): Array<Exclude<U, falsy>>;
}

// workaround for https://github.com/microsoft/TypeScript/issues/38752
interface Promise<T> {
    finally(onfinally: FuncType<Promise<void>>): Promise<T>;
}

// workaround for https://github.com/microsoft/TypeScript-DOM-lib-generator/issues/1261
interface NodeListOf<TNode extends Node> extends NodeList {
    item(index: number): TNode | null;
}

// JQuery extensions ----------------------------------------------------------

/**
 * Shortcut for JQuery's `Event` type.
 */
type JEvent = JQuery.Event;

/**
 * Shortcut for JQuery's `TriggeredEvent` type.
 */
type JTriggeredEvent<DataT = unknown> = JQuery.TriggeredEvent<EventTarget, DataT, EventTarget, EventTarget>;

/**
 * Shortcut for JQuery's `TypeToTriggeredEventMap` event type mapping.
 */
type JTriggeredEventMap<DataT = unknown> = JQuery.TypeToTriggeredEventMap<EventTarget, DataT, EventTarget, EventTarget>;

// OX extensions for the JQuery library must be globally declared (the type
// definitions in `@types/jquery` do not export the types explicitly).
interface JQuery {
    busy(options?: { empty?: boolean; immediate?: boolean }): this;
    idle(): this;
    dropdown(method: string): this;
}

// environment extensions -----------------------------------------------------

// debug extensions
interface Window {
    office?: {
        debug: Dict;
    };
}
