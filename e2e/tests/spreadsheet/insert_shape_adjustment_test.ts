/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Shape > Shape Adjustment");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C207210] [C310961] Adjusting basic shape", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a custom shape with adjustment handles
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "roundRect");
    I.dragMouseAt(300, 300, 600, 500);
    I.waitForChangesSaved();

    // type some text
    I.type("Lorem");
    I.pressKey("Escape");
    I.waitForChangesSaved();

    // use the adjustment handle
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle" });
    const x1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, "x");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, 40, 0);
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle" });
    const x2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, "x");
    expect(x2 - x1).to.be.within(38, 42);

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" });
    const x3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, "x");
    expect(x3 - x1).to.be.within(38, 42);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C207211] Adjusting block arrow shape", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a custom shape with adjustment handles
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rightArrow");
    I.dragMouseAt(300, 300, 600, 400);
    I.waitForChangesSaved();

    // type some text
    I.type("Lorem");
    I.pressKey("Escape");
    I.waitForChangesSaved();

    // use the adjustment handle
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle:nth-child(2)" });
    const x1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, "x");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, -40, 0);
    I.waitForChangesSaved();
    const x2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, "x");
    expect(x2 - x1).to.be.within(-42, -38);

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" });
    const x3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, "x");
    expect(x3 - x1).to.be.within(-42, -38);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C207212] Adjusting basic shape (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert a custom shape with adjustment handles
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "can");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // type some text
    I.type("Lorem");
    I.pressKey("Escape");
    I.waitForChangesSaved();

    // use the adjustment handle
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle" });
    const y1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, "y");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, 0, 40);
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle" });
    const y2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, "y");
    expect(y2 - y1).to.be.within(38, 42);

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" });
    const y3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle" }, "y");
    expect(y3 - y1).to.be.within(38, 42);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C207213] Adjusting block arrow shape (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert a custom shape with adjustment handles
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rightArrow");
    I.dragMouseAt(300, 300, 600, 400);
    I.waitForChangesSaved();

    // type some text
    I.type("Lorem");
    I.pressKey("Escape");
    I.waitForChangesSaved();

    // use the adjustment handle
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle:nth-child(2)" });
    const x1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, "x");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, -40, 0);
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".adj-handle:nth-child(2)" });
    const x2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, "x");
    expect(x2 - x1).to.be.within(-42, -38);

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" });
    const x3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, find: ".adj-handle:nth-child(2)" }, "x");
    expect(x3 - x1).to.be.within(-42, -38);

    I.closeDocument();
}).tag("stable");
