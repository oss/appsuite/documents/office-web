/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo matrix formula");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C297152] Undo of new matrix formula - Selection larger than matrix result", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("4", { leave: "up" });
    spreadsheet.enterCellText("5", { leave: "up" });
    spreadsheet.enterCellText("6", { leave: "right" });
    spreadsheet.enterCellText("7", { leave: "down" });
    spreadsheet.enterCellText("8", { leave: "down" });
    spreadsheet.enterCellText("9");

    I.pressKeys("Ctrl+Home", "Shift+Ctrl+ArrowRight", "Shift+Ctrl+ArrowDown");

    //enter matrix formula
    spreadsheet.enterCellText("=MUNIT(2)", { leave: "matrix" });

    //check the cell values and matrix formula
    await spreadsheet.selectCell("A1", { display: "1", value: 1, formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("A2", { display: "0", value: 0, formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("A3", { display: "#N/A",  error: "#NA", formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("B3", { display: "#N/A", error: "#NA", formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("B2", { display: "1", value: 1, formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("B1", { display: "0", value: 0, formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("C1", { display: "#N/A", formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("C2", { display: "#N/A", error: "#NA", formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");
    await spreadsheet.selectCell("C3", { display: "#N/A", error: "#NA", formula: "MUNIT(2)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(2)}");

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the cell values
    await spreadsheet.selectCell("A1", { value: 1, display: "1", formula: null });
    await spreadsheet.selectCell("A2", { value: 2, display: "2", formula: null });
    await spreadsheet.selectCell("A3", { value: 3, display: "3", formula: null });
    await spreadsheet.selectCell("B3", { value: 4, display: "4", formula: null });
    await spreadsheet.selectCell("B2", { value: 5, display: "5", formula: null });
    await spreadsheet.selectCell("B1", { value: 6, display: "6", formula: null });
    await spreadsheet.selectCell("C1", { value: 7, display: "7", formula: null });
    await spreadsheet.selectCell("C2", { value: 8, display: "8", formula: null });
    await spreadsheet.selectCell("C3", { value: 9, display: "9", formula: null });

    //reload and check the cell values again
    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: 1, display: "1", formula: null });
    await spreadsheet.selectCell("A2", { value: 2, display: "2", formula: null });
    await spreadsheet.selectCell("A3", { value: 3, display: "3", formula: null });
    await spreadsheet.selectCell("B3", { value: 4, display: "4", formula: null });
    await spreadsheet.selectCell("B2", { value: 5, display: "5", formula: null });
    await spreadsheet.selectCell("B1", { value: 6, display: "6", formula: null });
    await spreadsheet.selectCell("C1", { value: 7, display: "7", formula: null });
    await spreadsheet.selectCell("C2", { value: 8, display: "8", formula: null });
    await spreadsheet.selectCell("C3", { value: 9, display: "9", formula: null });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C297153] Undo of new matrix formula - Matrix result larger than selection", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A5");

    spreadsheet.enterCellText("1");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("4", { leave: "up" });
    spreadsheet.enterCellText("5", { leave: "up" });
    spreadsheet.enterCellText("6", { leave: "right" });
    spreadsheet.enterCellText("7", { leave: "down" });
    spreadsheet.enterCellText("8", { leave: "down" });
    spreadsheet.enterCellText("9", { leave: "stay" });

    I.pressKeys("Shift+Ctrl+ArrowLeft", "Shift+Ctrl+ArrowUp");

    //enter matrix formula
    spreadsheet.enterCellText("=MUNIT(4)", { leave: "matrix" });

    // check the cell values and matrix formula
    await spreadsheet.selectCell("A5", { display: "1", value: 1, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("A6", { display: "0", value: 0, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("A7", { display: "0", value: 0, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("A8", { value: null, formula: null });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");
    await spreadsheet.selectCell("B8", { value: null, formula: null });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");
    await spreadsheet.selectCell("B7", { display: "0", value: 0, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("B6", { display: "1", value: 1, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("B5", { display: "0", value: 0, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("C5", { display: "0", value: 0, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("C6", { display: "0", value: 0, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("C7", { display: "1", value: 1, formula: "MUNIT(4)" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");
    await spreadsheet.selectCell("D7", { value: null, formula: null });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the cell values
    await spreadsheet.selectCell("A5", { display: "1", value: 1, formula: null });
    await spreadsheet.selectCell("A6", { display: "2", value: 2, formula: null });
    await spreadsheet.selectCell("A7", { display: "3", value: 3, formula: null });
    await spreadsheet.selectCell("B7", { display: "4", value: 4, formula: null });
    await spreadsheet.selectCell("B6", { display: "5", value: 5, formula: null });
    await spreadsheet.selectCell("B5", { display: "6", value: 6, formula: null });
    await spreadsheet.selectCell("C5", { display: "7", value: 7, formula: null });
    await spreadsheet.selectCell("C6", { display: "8", value: 8, formula: null });
    await spreadsheet.selectCell("C7", { display: "9", value: 9, formula: null });

    await I.reopenDocument();

    //check the cell values
    await spreadsheet.selectCell("A5", { display: "1", value: 1, formula: null });
    await spreadsheet.selectCell("A6", { display: "2", value: 2, formula: null });
    await spreadsheet.selectCell("A7", { display: "3", value: 3, formula: null });
    await spreadsheet.selectCell("B7", { display: "4", value: 4, formula: null });
    await spreadsheet.selectCell("B6", { display: "5", value: 5, formula: null });
    await spreadsheet.selectCell("B5", { display: "6", value: 6, formula: null });
    await spreadsheet.selectCell("C5", { display: "7", value: 7, formula: null });
    await spreadsheet.selectCell("C6", { display: "8", value: 8, formula: null });
    await spreadsheet.selectCell("C7", { display: "9", value: 9, formula: null });

    I.closeDocument();

}).tag("stable");
