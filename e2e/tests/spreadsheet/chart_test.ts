/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Chart");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[DOCS-4711a] Chart Data Point Repeat", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/dataPointRepeat.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/delete");
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0 });
    I.clickButton("document/undo");
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[DOCS-4711b] Chart Data Point Index", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/dataPointIndex.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/delete");
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0 });
    I.clickButton("document/undo");
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    I.closeDocument();
}).tag("stable");
