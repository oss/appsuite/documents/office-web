/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo columns and rows");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C49092] Undo delete column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //Type text and check the value
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Text", { leave: "stay" });
    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("deleteMe", { leave: "stay" });
    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("D3Text", { leave: "stay" });
    I.waitForChangesSaved();

    spreadsheet.selectColumn("C");
    I.clickToolbarTab("colrow");
    I.clickButton("column/delete");
    I.waitForChangesSaved();

    //delete column - check the "column text"
    await spreadsheet.selectCell("C3", { value: "D3Text" });
    await spreadsheet.selectCell("B3", { value: "B3Text" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the "column text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("C3", { value: "deleteMe" });
    await spreadsheet.selectCell("D3", { value: "D3Text" });

    await I.reopenDocument();

    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("C3", { value: "deleteMe" });
    await spreadsheet.selectCell("D3", { value: "D3Text" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C49107] Undo delete column (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    //Type text and check the value
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Text", { leave: "stay" });
    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("deleteMe", { leave: "stay" });
    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("D3Text", { leave: "stay" });
    I.waitForChangesSaved();

    spreadsheet.selectColumn("C");
    I.clickToolbarTab("colrow");
    I.clickButton("column/delete");
    I.waitForChangesSaved();

    //delete column - check the "column text"
    await spreadsheet.selectCell("C3", { value: "D3Text" });
    await spreadsheet.selectCell("B3", { value: "B3Text" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the "column text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("C3", { value: "deleteMe" });
    await spreadsheet.selectCell("D3", { value: "D3Text" });

    await I.reopenDocument();

    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("C3", { value: "deleteMe" });
    await spreadsheet.selectCell("D3", { value: "D3Text" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49108] Undo delete row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Text", { leave: "stay" });
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("deleteMe", { leave: "stay" });
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("B5Text", { leave: "stay" });
    I.waitForChangesSaved();

    spreadsheet.selectRow("4");
    I.clickToolbarTab("colrow");
    I.clickButton("row/delete");
    I.waitForChangesSaved();

    //delete row - check the "row text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("B4", { value: "B5Text" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the "row text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("B4", { value: "deleteMe" });
    await spreadsheet.selectCell("B5", { value: "B5Text" });

    await I.reopenDocument();

    //check the "row text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("B4", { value: "deleteMe" });
    await spreadsheet.selectCell("B5", { value: "B5Text" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49109] Undo delete row (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Text", { leave: "stay" });
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("deleteMe", { leave: "stay" });
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("B5Text", { leave: "stay" });
    I.waitForChangesSaved();

    spreadsheet.selectRow("4");
    I.clickToolbarTab("colrow");
    I.clickButton("row/delete");
    I.waitForChangesSaved();

    //delete row - check the "row text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("B4", { value: "B5Text" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the "row text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("B4", { value: "deleteMe" });
    await spreadsheet.selectCell("B5", { value: "B5Text" });

    await I.reopenDocument();

    //check the "row text"
    await spreadsheet.selectCell("B3", { value: "B3Text" });
    await spreadsheet.selectCell("B4", { value: "deleteMe" });
    await spreadsheet.selectCell("B5", { value: "B5Text" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85839] Undo delete column from chart data", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/chart.xlsx");

    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("C");
    I.clickButton("column/delete");
    I.waitForChangesSaved();

    //select the chart
    I.pressKeys("Shift+F4");

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "C1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C5" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //select the chart
    I.pressKeys("ArrowUp", "Shift+F4");

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "C1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D2:D5" });

    await I.reopenDocument();

    //select the chart
    I.pressKeys("ArrowUp", "Shift+F4");

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "C1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C5" });

    I.waitForVisible({ spreadsheet: "highlight-range", range: "D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D2:D5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C234517] Undo delete column with list validation", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/listvalidation.xlsx");

    I.pressKey("ArrowRight");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B5" });

    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("B");

    //delete column
    I.clickButton("column/delete");
    I.waitForChangesSaved();

    //listvalidation is removed
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.pressKeys("ArrowLeft", "ArrowRight");

    //listvalidation is restored
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B5" });

    await I.reopenDocument();

    //check the values again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "B5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C234518] Undo delete column with list validation (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/listvalidation.ods");

    I.pressKeys("Ctrl+Home", "ArrowRight");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });

    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("B");

    //delete column
    I.clickButton("column/delete");
    I.waitForChangesSaved();

    //listvalidation is removed
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    // click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.pressKeys("ArrowLeft", "ArrowRight");

    //listvalidation is restored
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });

    await I.reopenDocument();

    //check the values again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
