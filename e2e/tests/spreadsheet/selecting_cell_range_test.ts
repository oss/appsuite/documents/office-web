/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Selecting cell ranges");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8324] Selecting cell range - Column", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/testfile_ranges.xlsx");

    // start typing a formula in cell E8
    await spreadsheet.selectCell("E8");
    spreadsheet.startCellEdit("=SUM(");

    // click on cell C1 and expect address to appear in formula, and as highlighted range
    await spreadsheet.clickCell("C1");
    spreadsheet.waitForCellEditMode("=SUM(C1");
    I.waitForElement({ spreadsheet: "highlight-range", range: "C1" });

    // modify range by dragging the highlight resizer element
    I.waitForElement({ spreadsheet: "highlight-range", range: "C1", resizer: "br" });

    // dragging
    I.dragMouseOnElement({ spreadsheet: "highlight-range", range: "C1", resizer: "bl" }, 0, 40);
    spreadsheet.waitForCellEditMode("=SUM(C1:C3");
    I.pressKey("Enter");

    // check the value
    await spreadsheet.selectCell("E8", { display: "42", value: 42, formula: "SUM(C1:C3)" });

    await I.reopenDocument();

    // check the value
    await spreadsheet.selectCell("E8", { display: "42", value: 42, formula: "SUM(C1:C3)" });
    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8325] Selecting cell range - Row", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/testfile_ranges.xlsx");

    // start typing a formula in cell E8
    await spreadsheet.selectCell("E8");
    spreadsheet.startCellEdit("=SUM(");

    // click on cell C1 and expect address to appear in formula, and as highlighted range
    await spreadsheet.clickCell("A3");
    spreadsheet.waitForCellEditMode("=SUM(A3");
    I.waitForElement({ spreadsheet: "highlight-range", range: "A3" });

    // modify range by dragging the highlight resizer element
    I.waitForElement({ spreadsheet: "highlight-range", range: "A3", resizer: "br" });

    // dragging
    I.dragMouseOnElement({ spreadsheet: "highlight-range", range: "A3", resizer: "bl" }, 340, 0);
    spreadsheet.waitForCellEditMode("=SUM(A3:D3");
    I.pressKey("Enter");

    // check the value
    await spreadsheet.selectCell("E8", { display: "52", value: 52, formula: "SUM(A3:D3)" });

    await I.reopenDocument();

    // check the value
    await spreadsheet.selectCell("E8", { display: "52", value: 52, formula: "SUM(A3:D3)" });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8326] Selecting cell range -  Single cells", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/testfile_ranges.xlsx");

    // start typing a formula in cell E8
    await spreadsheet.selectCell("E8");
    spreadsheet.startCellEdit("=SUM(");

    // click on cell C1 and expect address to appear in formula, and as highlighted range
    await spreadsheet.clickCell("A1");
    spreadsheet.waitForCellEditMode("=SUM(A1");
    I.waitForElement({ spreadsheet: "highlight-range", range: "A1" });

    // Hold the control key
    I.pressKeyDown("Control");

    await spreadsheet.clickCell("B2");
    I.waitForElement({ spreadsheet: "highlight-range", range: "B2" });
    spreadsheet.waitForCellEditMode("=SUM(A1,B2");

    await spreadsheet.clickCell("C3");
    I.waitForElement({ spreadsheet: "highlight-range", range: "C3" });
    spreadsheet.waitForCellEditMode("=SUM(A1,B2,C3");

    await spreadsheet.clickCell("D1");
    I.waitForElement({ spreadsheet: "highlight-range", range: "D1" });
    spreadsheet.waitForCellEditMode("=SUM(A1,B2,C3,D1");
    I.pressKeyUp("Control");
    I.pressKey("Enter");

    // check the value
    await spreadsheet.selectCell("E8", { display: "32", value: 32, formula: "SUM(A1,B2,C3,D1)" });

    await I.reopenDocument();

    // check the value
    await spreadsheet.selectCell("E8", { display: "32", value: 32, formula: "SUM(A1,B2,C3,D1)" });
    I.closeDocument();
}).tag("stable");
