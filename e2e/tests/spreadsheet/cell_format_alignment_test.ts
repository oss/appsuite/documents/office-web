/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell format");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8127] Horizontal text alignment select", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("auto", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "auto");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "auto" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("left", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "left");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "left" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("center", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "center");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "center" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("right", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "right");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "right" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("justify", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "justify");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "justify" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "auto" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "left" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "center" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "right" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "justify" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C330184] Horizontal text alignment select (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("auto", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "auto");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "auto" });
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("left", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "left");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "left" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("center", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "center");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "center" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("right", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "right");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "right" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("justify", { leave: "stay" });
    I.clickOptionButton("cell/alignhor", "justify");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "justify" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "auto" }); //->DOCS-3635
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "left" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "center" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "right" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "justify" }); //->DOCS-3635

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8129] Vertical text alignment select", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("top", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "top");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("middle", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "middle");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "middle" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("bottom", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "bottom");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "bottom" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("justify", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "justify");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "justify" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "middle" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "bottom" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "justify" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C330186] Vertical text alignment select (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("top", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "top");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("middle", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "middle");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "middle" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("bottom", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "bottom");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "bottom" });
    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("justify", { leave: "stay" });
    I.clickOptionButton("cell/alignvert", "justify");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "justify" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "middle" });
    I.pressKey("ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/alignvert", state: "bottom" });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "justify" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8130] Automatic word wrap", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");

    I.clickToolbarTab("colrow");

    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.clickToolbarTab("format");

    I.waitForAndClick({ docs: "control", key: "cell/linebreak" });
    spreadsheet.enterCellText("abc def ghi jkl mno pqrs tuv");

    I.clickToolbarTab("colrow");

    //check the change state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "1508" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    await I.reopenDocument();

    I.pressKey("ArrowUp");

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "1508" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8146] Cell style preselect", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");

    //set stylesheet
    I.clickOptionButton("cell/stylesheet", "Positive");
    I.waitForChangesSaved();

    spreadsheet.enterCellText("hello", { leave: "stay" });

    //check the attributes text color, background color
    spreadsheet.waitForActiveCell("B2", { display: "hello",  value: "hello", renderFill: "#c6efce", renderColor: "#006100" });

    await I.reopenDocument();

    //check the attributes text color, background color
    spreadsheet.waitForActiveCell("B2", { display: "hello",  value: "hello", renderFill: "#c6efce", renderColor: "#006100" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
