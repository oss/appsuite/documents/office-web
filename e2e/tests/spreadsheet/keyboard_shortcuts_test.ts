/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Keyboard Shortcuts");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8339] Keyboard shortcut - CTRL+Z / ALT-Backspace", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("Lorem1", { leave: "stay" });

    //delete the text
    I.pressKeys("Ctrl+Z");
    spreadsheet.waitForActiveCell("B2", { value: null });

    //delete the text
    spreadsheet.enterCellText("Lorem2", { leave: "stay" });
    I.pressKeys("Alt+Backspace");
    spreadsheet.waitForActiveCell("B2", { value: null });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("B2", { value: null });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8330][C320825][C320870]  Keyboard shortcut - CTRL+B, Ctrl+I , Ctrl+U", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");

    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });

    //set to bold
    I.pressKeys("Ctrl+B");

    //set to italic
    I.pressKeys("Ctrl+I");

    //set underline
    I.pressKeys("Ctrl+U");

    //check bold, italic, underline attributes
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    await I.reopenDocument();

    //check bold, italic, underline attributes
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C322448] Keyboard shortcut - ESC / Return / F2", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //Escape
    I.type("Lorem");
    I.pressKey("Escape");
    spreadsheet.waitForActiveCell("A1", { value: null });

    //return
    I.type("Lorem");
    I.pressKey("Enter");
    spreadsheet.waitForActiveCell("A2", { value: null });

    //F2
    await spreadsheet.selectCell("B2");
    I.wait(1);
    I.pressKeys("F2");
    spreadsheet.waitForCellEditMode();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C322447] Keyboard shortcut - Ctrl+F /Esc / Ctrl+G / Ctrl+Shift+G", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    ///open the search
    I.pressKeys("Ctrl+F");
    I.waitForVisible({ css: "[data-key='document/search/text']" });

    //close the search
    I.pressKey("Escape");
    I.waitForInvisible({ css: "[data-key='document/search/text']" });

    await spreadsheet.selectCell("B2");

    //type some text
    spreadsheet.enterCellText("one", { leave: "right" });
    spreadsheet.enterCellText("two", { leave: "right" });
    spreadsheet.enterCellText("three", { leave: "right" });

    //open the search
    I.pressKeys("Ctrl+F");
    I.waitForVisible({ css: "[data-key='document/search/text']" });
    I.fillTextField("document/search/text", "e");

    //search
    I.pressKeys("Ctrl+G");
    spreadsheet.waitForActiveCell("D2", { value: "three" });

    //search
    I.pressKeys("Ctrl+Shift+G");
    spreadsheet.waitForActiveCell("B2", { value: "one" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C322446][C115058] Keyboard shortcut - Shift + F4 / Tab / Shift+Tab /F2", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/drawing_shape_image.xlsx");

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: false });
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image", selected: false });
    I.waitForVisible({ spreadsheet: "drawing", pos: 2, type: "shape", selected: false });

    //image 1 is selected
    I.pressKeys("Shift+F4");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image", selected: false });

    //images 2 is selected
    I.pressKey("Tab");
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image", selected: true });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: false });

    //images 1 is selected
    I.pressKeys("Shift+Tab");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image", selected: false });

    //shape 1
    I.clickOnElement({ spreadsheet: "drawing", pos: 2, type: "shape", para: 0 });
    I.pressKey("F2");

    //shape 1 active
    I.waitForVisible({ spreadsheet: "grid-pane", focused: true, find: ".drawing.edit-active .textframecontent > .textframe" });

    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: false });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: false });
    I.waitForVisible({ spreadsheet: "drawing",  type: "shape", pos: 2, selected: false, find: ".textframecontent > .textframe" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C322453] Keyboard shortcut - navigation", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //open the cell style dropdown
    I.clickButton("cell/stylesheet");
    I.pressKey("Escape");

    //click on the fontname input
    I.click({ css: "[data-key='character/fontname'] input" });
    I.waitForFocus({ css: "[data-key='character/fontname'] input" });
    I.pressKey("Tab");

    //click on the fontsize input
    I.wait(0.5);
    I.click({ css: "[data-key='character/fontsize'] input" });
    I.waitForFocus({ css: "[data-key='character/fontsize'] input" });
    I.pressKey("Escape");

    //insert a sheet
    spreadsheet.insertSheet();

    //check the active sheet
    spreadsheet.waitForActiveSheet(1);
    I.pressKeys("Ctrl+Alt+PageUp");

    //check the active sheet
    spreadsheet.waitForActiveSheet(0);
    I.pressKeys("Ctrl+Alt+PageDown");

    //check the active sheet
    spreadsheet.waitForActiveSheet(1);

    await I.reopenDocument();

    //check the active sheet
    spreadsheet.waitForActiveSheet(1);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C320824][C321488] Keyboard shortcut - Ctrl +Shift + End / Shift + End", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/shortcut.xlsx");

    await spreadsheet.selectCell("A1");
    // [C320824]
    I.pressKeys("Ctrl+Shift+End");

    //check the cell range
    I.waitForElement({ spreadsheet: "selection-range", range: "A1:M20" });

    // [C321488]
    await spreadsheet.selectCell("E5");
    spreadsheet.enterCellText("E5", { leave: "stay" });

    await spreadsheet.selectCell("K5");
    I.pressKeys("Shift+End");
    I.waitForElement({ spreadsheet: "selection-range", range: "K5:M5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C321372] Keyboard shortcut - Shift + Tab / Shift +Enter (with protected cells)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/C321372.xlsx");

    await spreadsheet.selectCell("A1");
    I.pressKeys("Shift+Tab");

    //check the cell focus
    I.waitForElement({ spreadsheet: "selection-range", range: "C7" });
    spreadsheet.waitForActiveCell("C7", { value: "unprotected cell", display: "unprotected cell" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C321487] [C321491] Keyboard shortcut - Shift+Home(POS1) /Shift+Ctrl+Home (Pos1)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/shortcut.xlsx");

    await spreadsheet.selectCell("E5");
    I.pressKeys("Shift+Home");

    //check the cell focus
    I.waitForElement({ spreadsheet: "selection-range", range: "A5:E5" });

    //type text
    await spreadsheet.selectCell("K5");
    spreadsheet.enterCellText("hallo", { leave: "stay" });

    I.pressKeys("Shift+Ctrl+Home");

    I.waitForElement({ spreadsheet: "selection-range", range: "A1:K5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("Clipboard test Copy/Paste", ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("42", { leave: "stay" });
    I.copy();
    I.pressKey("ArrowDown");
    I.paste();
    spreadsheet.waitForActiveCell("A2", { value: 42 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C322619][C8338] Keyboard shortcut -  CTRL+ Space /Alt+PAGE DOWN/UP", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.xlsx"); // font family -> courier

    // select column
    await spreadsheet.selectCell("E5");
    I.pressKeys("Ctrl+Space");
    I.waitForElement({ spreadsheet: "selection-range", range: "E1:E1048576" });

    // select row
    await spreadsheet.selectCell("D8");
    I.pressKeys("Shift+Space");
    I.waitForElement({ spreadsheet: "selection-range", range: "A8:XFD8" });

    await spreadsheet.selectCell("A1");
    I.pressKey("ArrowRight");
    // B1 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "B1" });

    I.pressKey("ArrowDown");
    // B2 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "B2" });

    // [C8338]
    // cell P2 is selected
    I.pressKeys("Alt+PageDown");
    I.waitForElement({ spreadsheet: "selection-range", range: "P2" });

    I.pressKeys("Alt+PageUp");

    // cell B2 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "B2" });

    await spreadsheet.selectCell("H5");
    I.pressKey("Home");
    // A5 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "A5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C320869][C320882] Keyboard shortcut - Shift + Ctrl + Space", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // [C320869]
    I.pressKeys("Shift+Ctrl+Space");

    // all cells are selected
    I.waitForElement({ spreadsheet: "selection-range", range: "A1:XFD1048576" });

    // [C320882]
    await spreadsheet.selectCell("H5");
    I.pressKeys("Shift+Enter");

    I.waitForElement({ spreadsheet: "selection-range", range: "H4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C322452] Keyboard shortcut -  CTRL + Insert / Shift + Insert, Shift + delete / Shift + Insert", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A1");
    spreadsheet.enterCellText("hello", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { display: "hello" });

    // copy the cell text
    I.pressKeys("CTRL+Insert");

    // A1 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "A1" });

    //paste the cell
    await spreadsheet.selectCell("A3");
    I.paste();

    // A3 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "A3" });
    spreadsheet.waitForActiveCell("A3", { display: "hello" });

    await spreadsheet.selectCell("B1");
    spreadsheet.enterCellText("miau", { leave: "stay" });

    //cut the cell text
    I.cut();
    I.wait(1);

    spreadsheet.waitForActiveCell("B1", { value: null });

    // pasting in cell B3
    await spreadsheet.selectCell("B3");
    I.pressKeys("Shift+Insert");
    I.wait(1);

    I.waitForElement({ spreadsheet: "selection-range", range: "B3" });
    spreadsheet.waitForActiveCell("B3", { display: "miau" });

    await I.reopenDocument();

    //check the cell values again
    await spreadsheet.selectCell("A1", { display: "hello" });
    await spreadsheet.selectCell("A3", { display: "hello" });
    await spreadsheet.selectCell("B1", { value: null });
    await spreadsheet.selectCell("B3", { display: "miau" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C320877][C320878] Keyboard shortcut - Home (POS1) /Home(End)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // [C320877]
    await spreadsheet.selectCell("H5");

    // Home
    I.pressKey("Home");

    // A5 is selected
    I.waitForElement({ spreadsheet: "selection-range", range: "A5" });

    // [C320878]
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3", { leave: "stay" });

    await spreadsheet.selectCell("E3");
    spreadsheet.enterCellText("E3", { leave: "stay" });

    // End
    await spreadsheet.selectCell("B3");
    I.pressKey("End");
    I.waitForElement({ spreadsheet: "selection-range", range: "E3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C320876] Keyboard shortcut - Ctrl+Alt+PageUp / Ctrl+Alt+PageDown", ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    // active sheet 2
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    I.pressKeys("Ctrl+Alt+PageUp");

    // active sheet 1
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 0 });

    I.pressKeys("Ctrl+Alt+PageDown");

    // active sheet 2
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C320879] Keyboard shortcut - Ctrl + Home (POS1)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("H5");
    I.pressKeys("Ctrl+Home");
    I.waitForElement({ spreadsheet: "selection-range", range: "A1" });

    // hide row 1
    spreadsheet.selectRow("1");
    I.waitForVisible({ spreadsheet: "row-resize", pos: "1", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "1" });
    I.waitForContextMenuVisible();
    I.clickButton("row/hide", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "1", collapsed: true });
    I.waitForInvisible({ spreadsheet: "row-header", pos: "1" });

    await spreadsheet.selectCell("A5");
    I.pressKeys("Ctrl+Home");
    I.waitForElement({ spreadsheet: "selection-range", range: "A2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8332][C8331][C8333]Keyboard shortcut - Tab / ESC /CTRL+F6", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // [C8332]
    // click on the fontname input
    I.click({ docs: "control", key: "character/fontname", pane: "main-pane", focused: false });

    I.waitForVisible({ docs: "control", key: "character/fontname", pane: "main-pane", focused: true });
    I.pressKey("Tab");

    // click on the fontname input
    I.waitForVisible({ docs: "control", key: "character/fontsize", pane: "main-pane", focused: true });

    //I.waitForFocus({ css: "[data-key='character/fontsize'] input" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", pane: "main-pane", focused: true });

    // [C8331]
    //open the cell style dropdown
    I.clickButton("cell/stylesheet");

    I.pressKey("Escape");

    // [C8333]
    I.pressKeys("Ctrl+F6");

    I.waitForVisible({ docs: "control", key: "cell/autoformula", pane: "formula-pane", focused: true });
    I.pressKeys("Ctrl+F6");
    I.waitForElement({ spreadsheet: "selection-range", range: "A1" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C309011] Keyboard shortcut through a complete spreadheet - Ctrl+F6", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    I.clickToolbarTab("insert");
    I.clickButton("threadedcomment/insert");
    I.wait(0.5);

    I.type("word");
    I.click({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    I.clickButton("view/pane/search");

    await spreadsheet.selectCell("B2");

    // closer comments step1
    I.pressKeys("Ctrl+F6");
    I.waitForFocus({ css: ".comments-pane > .header > button[data-action='close']" });

    // active sheet step2
    I.pressKeys("Ctrl+F6");
    I.waitForVisible({ docs: "control", key: "view/sheet/active", pane: "status-pane", focused: true });

    // help step3
    I.pressKeys("Ctrl+F6");
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button" });

    // settings step4
    I.pressKeys("Ctrl+F6");
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button" });

    // contact picuture step5
    I.pressKeys("Ctrl+F6");
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button" });

    // filetab step6
    I.pressKeys("Ctrl+F6");
    I.waitForVisible({ docs: "control", key: "view/toolbars/tab", pane: "top-pane",  focused: true });

    // autoformula step7
    I.pressKeys("Ctrl+F6");
    I.waitForVisible({ docs: "control", key: "cell/autoformula", pane: "main-pane", focused: true });

    // search step8
    I.pressKeys("Ctrl+F6");
    I.waitForVisible({ docs: "control", key: "document/search/text", pane: "search-pane", focused: true });

    // autoformula step9
    I.pressKeys("Ctrl+F6");
    I.waitForVisible({ docs: "control", key: "cell/autoformula", pane: "formula-pane", focused: true });

    // focus inside spreadsheet step10
    I.pressKeys("Ctrl+F6");
    spreadsheet.waitForCellFocus();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-5047] Cursor traversal over merged range", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // create a merged range
    await spreadsheet.selectRange("B2:C3");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    // small helper function for "pressKeys" and "waitForActiveCell"
    const traverseTo = (keys: string, target: string): void => {
        I.pressKeys(keys);
        spreadsheet.waitForActiveCell(target);
    };

    // cursor right/left
    await spreadsheet.selectCell("A3");
    traverseTo("ArrowRight", "B2");
    traverseTo("ArrowRight", "D3");
    traverseTo("ArrowLeft", "B2");
    traverseTo("ArrowLeft", "A3");

    // cursor down/up
    await spreadsheet.selectCell("C1");
    traverseTo("ArrowDown", "B2");
    traverseTo("ArrowDown", "C4");
    traverseTo("ArrowUp", "B2");
    traverseTo("ArrowUp", "C1");

    // Tab, Shift+Tab (same as "cursor right/left")
    await spreadsheet.selectCell("A3");
    traverseTo("Tab", "B2");
    traverseTo("Tab", "D3");
    traverseTo("Shift+Tab", "B2");
    traverseTo("Shift+Tab", "A3");

    // Enter, Shift+Enter (same as "cursor down/up")
    await spreadsheet.selectCell("C1");
    traverseTo("Enter", "B2");
    traverseTo("Enter", "C4");
    traverseTo("Shift+Enter", "B2");
    traverseTo("Shift+Enter", "C1");

    // move active cell in range selection
    await spreadsheet.selectRange("A1:D4");
    spreadsheet.waitForActiveCell("A1");

    // Tab
    traverseTo("3*Tab", "D1");
    traverseTo("Tab", "A2");
    traverseTo("Tab", "B2");
    traverseTo("Tab", "D2");
    traverseTo("Tab", "A3");
    traverseTo("Tab", "D3");
    traverseTo("Tab", "A4");
    traverseTo("3*Tab", "D4");
    traverseTo("Tab", "A1");

    // Shift+Tab
    traverseTo("Shift+Tab", "D4");
    traverseTo("3*Shift+Tab", "A4");
    traverseTo("Shift+Tab", "D3");
    traverseTo("Shift+Tab", "A3");
    traverseTo("Shift+Tab", "D2");
    traverseTo("Shift+Tab", "B2");
    traverseTo("Shift+Tab", "A2");
    traverseTo("Shift+Tab", "D1");
    traverseTo("3*Shift+Tab", "A1");

    // Enter
    traverseTo("3*Enter", "A4");
    traverseTo("Enter", "B1");
    traverseTo("Enter", "B2");
    traverseTo("Enter", "B4");
    traverseTo("Enter", "C1");
    traverseTo("Enter", "C4");
    traverseTo("Enter", "D1");
    traverseTo("3*Enter", "D4");
    traverseTo("Enter", "A1");

    // Shift+Enter
    traverseTo("Shift+Enter", "D4");
    traverseTo("3*Shift+Enter", "D1");
    traverseTo("Shift+Enter", "C4");
    traverseTo("Shift+Enter", "C1");
    traverseTo("Shift+Enter", "B4");
    traverseTo("Shift+Enter", "B2");
    traverseTo("Shift+Enter", "B1");
    traverseTo("Shift+Enter", "A4");
    traverseTo("3*Shift+Enter", "A1");

    I.closeDocument();
}).tag("stable");
