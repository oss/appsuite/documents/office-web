/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Autosum");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8242] Sum automatically - Of a selected range in a column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers in column A
    spreadsheet.enterCellText("3");
    spreadsheet.enterCellText("5");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("6");

    // select A2:A3, insert AutoSum
    await spreadsheet.selectRange("A2:A3");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("A5", { value: 7, formula: "SUM(A2:A3)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A5", { value: 7, formula: "SUM(A2:A3)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8243] Sum automatically - Of a selected range in a row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers in row 1
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("5", { leave: "right" });
    spreadsheet.enterCellText("2", { leave: "right" });
    spreadsheet.enterCellText("6");

    // select B1:C1, insert AutoSum
    await spreadsheet.selectRange("B1:C1");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("E1", { value: 7, formula: "SUM(B1:C1)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("E1", { value: 7, formula: "SUM(B1:C1)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8244] Sum automatically - Numbers and strings in a column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers and strings in column A
    spreadsheet.enterCellText("3");
    spreadsheet.enterCellText("text");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("6");

    // select A7, insert AutoSum
    await spreadsheet.selectCell("A7");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar

    // expect cell edit mode with prefilled formula
    spreadsheet.waitForCellEditMode("=SUM(A3:A6)");
    I.pressKey("Enter");
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("A7", { value: 8, formula: "SUM(A3:A6)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A7", { value: 8, formula: "SUM(A3:A6)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8245] Sum automatically - Numbers and strings in a row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers and strings in row 1
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("text", { leave: "right" });
    spreadsheet.enterCellText("2", { leave: "right" });
    spreadsheet.enterCellText("6", { leave: "right" });

    // select G1, insert AutoSum
    await spreadsheet.selectCell("G1");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar

    // expect cell edit mode with prefilled formula
    spreadsheet.waitForCellEditMode("=SUM(C1:F1)");
    I.pressKey("Enter");
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("G1", { value: 8, formula: "SUM(C1:F1)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("G1", { value: 8, formula: "SUM(C1:F1)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8246] Sum automatically - Of a multi selected range in columns", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers
    spreadsheet.enterCellText("3");
    spreadsheet.enterCellText("5");
    spreadsheet.enterCellText("2", { leave: "right" });
    spreadsheet.enterCellText("4", { leave: "up" });
    spreadsheet.enterCellText("-2", { leave: "up" });
    spreadsheet.enterCellText("2", { leave: "left" });

    // select all cells, insert AutoSum
    await spreadsheet.selectRange("A1:B3");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("A4", { value: 10, formula: "SUM(A1:A3)" });
    await spreadsheet.selectCell("B4", { value: 4, formula: "SUM(B1:B3)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A4", { value: 10, formula: "SUM(A1:A3)" });
    await spreadsheet.selectCell("B4", { value: 4, formula: "SUM(B1:B3)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8247] Sum automatically - Of a multi selected range in rows", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("5", { leave: "right" });
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("4", { leave: "left" });
    spreadsheet.enterCellText("-2", { leave: "left" });
    spreadsheet.enterCellText("2", { leave: "up" });

    // select all cells, insert AutoSum
    await spreadsheet.selectRange("A1:C2");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("A3", { value: 5, formula: "SUM(A1:A2)" });
    await spreadsheet.selectCell("B3", { value: 3, formula: "SUM(B1:B2)" });
    await spreadsheet.selectCell("C3", { value: 6, formula: "SUM(C1:C2)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A3", { value: 5, formula: "SUM(A1:A2)" });
    await spreadsheet.selectCell("B3", { value: 3, formula: "SUM(B1:B2)" });
    await spreadsheet.selectCell("C3", { value: 6, formula: "SUM(C1:C2)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8248] Sum automatically - Currency in a row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers, set cells to Currency format
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("5", { leave: "right" });
    spreadsheet.enterCellText("2");
    await spreadsheet.selectRange("A1:C1");
    I.clickButton("cell/numberformat/category", { value: "currency" });
    I.waitForChangesSaved();

    // insert AutoSum
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("D1", { value: 10, formula: "SUM(A1:C1)" });
    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("D1", { value: 10, formula: "SUM(A1:C1)" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85753] Sum automatically - Different number format in a column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few preformatted numbers
    spreadsheet.enterCellText("1 1/2");
    spreadsheet.enterCellText("5");
    spreadsheet.enterCellText("$2");

    // insert AutoSum
    await spreadsheet.selectRange("A1:A3");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear in cells A3:C3
    await spreadsheet.selectCell("A4", { value: 8.5, display: "8 1/2", formula: "SUM(A1:A3)" });
    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "fraction" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A4", { value: 8.5, display: "8 1/2", formula: "SUM(A1:A3)" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "fraction" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85906] Sum automatically - Different number format in a row (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // enter a few preformatted numbers
    spreadsheet.enterCellText("1 1/2", { leave: "right" });
    spreadsheet.enterCellText("5", { leave: "right" });
    spreadsheet.enterCellText("$2");

    // select all cells, insert AutoSum
    await spreadsheet.selectRange("A1:C1");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear in cells A3:C3
    await spreadsheet.selectCell("D1", { value: 8.5, display: "8 1/2", formula: "SUM(A1:C1)" });
    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "fraction" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("D1", { value: 8.5, display: "8 1/2", formula: "SUM(A1:C1)" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "fraction" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C35087] Sum automatically - Of a selected range in a column (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // enter a few numbers in column A
    spreadsheet.enterCellText("3");
    spreadsheet.enterCellText("5");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("6");

    // select A2:A3, insert AutoSum
    await spreadsheet.selectRange("A2:A3");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("A5", { value: 7, formula: "SUM(A2:A3)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A5", { value: 7, formula: "SUM(A2:A3)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C35088] Sum automatically - Of a selected range in a row (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // enter a few numbers in row 1
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("5", { leave: "right" });
    spreadsheet.enterCellText("2", { leave: "right" });
    spreadsheet.enterCellText("6");

    // select B1:C1, insert AutoSum
    await spreadsheet.selectRange("B1:C1");
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("E1", { value: 7, formula: "SUM(B1:C1)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("E1", { value: 7, formula: "SUM(B1:C1)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C35086] Sum automatically - Currency in a row (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // enter a few numbers, set cells to Currency format
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("5", { leave: "right" });
    spreadsheet.enterCellText("2");
    await spreadsheet.selectRange("A1:C1");
    I.clickButton("cell/numberformat/category", { value: "currency" });
    I.waitForChangesSaved();

    // insert AutoSum
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" }); // do not use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    await spreadsheet.selectCell("D1", { value: 10, formula: "SUM(A1:C1)" });
    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("D1", { value: 10, formula: "SUM(A1:C1)" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    I.closeDocument();
}).tag("stable");
