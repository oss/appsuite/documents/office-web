/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Data > Autofilter and sorting");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C85631] Sort range with filter as header (date/string)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/date_string.xlsx");

    await spreadsheet.selectRange("C1:C3");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C1" });

    //sorting buttons
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "aa" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "bb" });

    //OK
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("C1", { value: 42350, display: "12/12/2015" });
    await spreadsheet.selectCell("C2", { value: "bb", display: "bb" });
    await spreadsheet.selectCell("C3", { value: "aa", display: "aa" });

    await I.reopenDocument();

    await spreadsheet.selectCell("C1", { value: 42350, display: "12/12/2015" });
    await spreadsheet.selectCell("C2", { value: "bb", display: "bb" });
    await spreadsheet.selectCell("C3", { value: "aa", display: "aa" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85635] Sort range with filter as header (string/number)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/string_number.xlsx");

    await spreadsheet.selectRange("C1:C3");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C1" });

    //sorting buttons
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 2 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 3 });

    //OK
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("C1", { value: "one", display: "one" });
    await spreadsheet.selectCell("C2", { value: 3, display: "3" });
    await spreadsheet.selectCell("C3", { value: 2, display: "2" });

    await I.reopenDocument();

    await spreadsheet.selectCell("C1", { value: "one", display: "one" });
    await spreadsheet.selectCell("C2", { value: 3, display: "3" });
    await spreadsheet.selectCell("C3", { value: 2, display: "2" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-4446] Change sort order", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/DOCS-4446-sortorder.xlsx");

    // open "Custom Sort" dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.clickButton("table/sort/dialog", { inside: "popup-menu" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-custom-sort-dialog" });

    // check initial sort order
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(1) .sort-by", withText: "col1" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(2) .sort-by", withText: "col2" });

    // remove first sorting rule, create a new rule
    I.waitForAndClick({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(1) button[data-action=delete]" });
    I.waitForAndClick({ docs: "dialog", area: "footer", find: "button[data-action=add]" });
    I.waitForAndClick({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(2) .sort-by .btn" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: 0, withText: "col1" });

    // check new sort order
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(1) .sort-by", withText: "col2" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(2) .sort-by", withText: "col1" });

    // apply new sort order
    I.waitForAndClick({ docs: "dialog", area: "footer", find: "button[data-action=ok]" });
    I.waitForChangesSaved();

    // check sorted cells
    await spreadsheet.selectCell("A2", { value: 1 });
    await spreadsheet.selectCell("A3", { value: 2 });
    await spreadsheet.selectCell("A4", { value: 1 });
    await spreadsheet.selectCell("A5", { value: 2 });
    await spreadsheet.selectCell("B2", { value: 1 });
    await spreadsheet.selectCell("B3", { value: 1 });
    await spreadsheet.selectCell("B4", { value: 2 });
    await spreadsheet.selectCell("B5", { value: 2 });

    // reopen "Custom Sort" dialog
    I.clickButton("table/sort", { caret: true });
    I.clickButton("table/sort/dialog", { inside: "popup-menu" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-custom-sort-dialog" });

    // check current sort order
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(1) .sort-by", withText: "col2" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(2) .sort-by", withText: "col1" });
    I.pressKey("Escape");

    await I.reopenDocument();

    // open "Custom Sort" dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.clickButton("table/sort/dialog", { inside: "popup-menu" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-custom-sort-dialog" });

    // check current sort order
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(1) .sort-by", withText: "col2" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule-list .sort-rule:nth-child(2) .sort-by", withText: "col1" });
    I.pressKey("Escape");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
