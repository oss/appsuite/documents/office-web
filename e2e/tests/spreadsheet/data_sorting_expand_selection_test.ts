/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Data > Sorting > Expand selection");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C15606] Sorting - over rows selection", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting_over_row_selection.xlsx");
    await spreadsheet.selectRange("A5:C5");

    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });

    //descendig
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort", withText: "Descending" });

    //check the dialg buttons
    I.waitForVisible({ css: "button[data-action=ok]" });
    I.waitForVisible({ css: "button[data-action=cancel]" });
    I.waitForVisible({ css: "button[data-action=no]" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    I.pressKeys("Ctrl+Home");

    //check the values  and the order
    await spreadsheet.selectCell("L3", { display: "TRUE", value: true, formula: "A3=G3" });
    await spreadsheet.selectCell("L4", { display: "TRUE", value: true, formula: "A4=G4" });
    await spreadsheet.selectCell("L5", { display: "TRUE", value: true, formula: "A5=G5" });
    await spreadsheet.selectCell("L6", { display: "TRUE", value: true, formula: "A6=G6" });
    await spreadsheet.selectCell("M3", { display: "TRUE", value: true, formula: "B3=H3" });
    await spreadsheet.selectCell("M4", { display: "TRUE", value: true, formula: "B4=H4" });
    await spreadsheet.selectCell("M5", { display: "TRUE", value: true, formula: "B5=H5" });
    await spreadsheet.selectCell("M6", { display: "TRUE", value: true, formula: "B6=H6" });
    await spreadsheet.selectCell("N3", { display: "TRUE", value: true, formula: "C3=I3" });
    await spreadsheet.selectCell("N4", { display: "TRUE", value: true, formula: "C4=I4" });
    await spreadsheet.selectCell("N5", { display: "TRUE", value: true, formula: "C5=I5" });
    await spreadsheet.selectCell("N6", { display: "TRUE", value: true, formula: "C6=I6" });
    await spreadsheet.selectCell("O3", { display: "TRUE", value: true, formula: "D3=J3" });
    await spreadsheet.selectCell("O4", { display: "TRUE", value: true, formula: "D4=J4" });
    await spreadsheet.selectCell("O5", { display: "TRUE", value: true, formula: "D5=J5" });
    await spreadsheet.selectCell("O6", { display: "TRUE", value: true, formula: "D6=J6" });

    await I.reopenDocument();

    //check the toolbar (workaround for flexible toolbar)
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });

    //check the values and the order
    await spreadsheet.selectCell("L3", { display: "TRUE", value: true, formula: "A3=G3" });
    await spreadsheet.selectCell("L4", { display: "TRUE", value: true, formula: "A4=G4" });
    await spreadsheet.selectCell("L5", { display: "TRUE", value: true, formula: "A5=G5" });
    await spreadsheet.selectCell("L6", { display: "TRUE", value: true, formula: "A6=G6" });
    await spreadsheet.selectCell("M3", { display: "TRUE", value: true, formula: "B3=H3" });
    await spreadsheet.selectCell("M4", { display: "TRUE", value: true, formula: "B4=H4" });
    await spreadsheet.selectCell("M5", { display: "TRUE", value: true, formula: "B5=H5" });
    await spreadsheet.selectCell("M6", { display: "TRUE", value: true, formula: "B6=H6" });
    await spreadsheet.selectCell("N3", { display: "TRUE", value: true, formula: "C3=I3" });
    await spreadsheet.selectCell("N4", { display: "TRUE", value: true, formula: "C4=I4" });
    await spreadsheet.selectCell("N5", { display: "TRUE", value: true, formula: "C5=I5" });
    await spreadsheet.selectCell("N6", { display: "TRUE", value: true, formula: "C6=I6" });
    await spreadsheet.selectCell("O3", { display: "TRUE", value: true, formula: "D3=J3" });
    await spreadsheet.selectCell("O4", { display: "TRUE", value: true, formula: "D4=J4" });
    await spreadsheet.selectCell("O5", { display: "TRUE", value: true, formula: "D5=J5" });
    await spreadsheet.selectCell("O6", { display: "TRUE", value: true, formula: "D6=J6" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15616] Sorting - 'Expand the selection? - No'", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/sorting_over_column_no_expand.xlsx");
    await spreadsheet.selectRange("B3:B6");

    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });

    //descendig
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort", withText: "Descending" });

    //check the dialg buttons
    I.waitForVisible({ css: "button[data-action=ok]" });
    I.waitForVisible({ css: "button[data-action=cancel]" });
    I.waitForAndClick({ css: "button[data-action=no]" });
    I.waitForChangesSaved();

    //check the values and the order
    await spreadsheet.selectCell("L3", { display: "TRUE", value: true, formula: "A3=G3" });
    await spreadsheet.selectCell("L4", { display: "TRUE", value: true, formula: "A4=G4" });
    await spreadsheet.selectCell("L5", { display: "TRUE", value: true, formula: "A5=G5" });
    await spreadsheet.selectCell("L6", { display: "TRUE", value: true, formula: "A6=G6" });
    await spreadsheet.selectCell("M3", { display: "TRUE", value: true, formula: "B3=H3" });
    await spreadsheet.selectCell("M4", { display: "TRUE", value: true, formula: "B4=H4" });
    await spreadsheet.selectCell("M5", { display: "TRUE", value: true, formula: "B5=H5" });
    await spreadsheet.selectCell("M6", { display: "TRUE", value: true, formula: "B6=H6" });
    await spreadsheet.selectCell("N3", { display: "TRUE", value: true, formula: "C3=I3" });
    await spreadsheet.selectCell("N4", { display: "TRUE", value: true, formula: "C4=I4" });
    await spreadsheet.selectCell("N5", { display: "TRUE", value: true, formula: "C5=I5" });
    await spreadsheet.selectCell("N6", { display: "TRUE", value: true, formula: "C6=I6" });
    await spreadsheet.selectCell("O3", { display: "TRUE", value: true, formula: "D3=J3" });
    await spreadsheet.selectCell("O4", { display: "TRUE", value: true, formula: "D4=J4" });
    await spreadsheet.selectCell("O5", { display: "TRUE", value: true, formula: "D5=J5" });
    await spreadsheet.selectCell("O6", { display: "TRUE", value: true, formula: "D6=J6" });

    await I.reopenDocument();

    //check the values and the order
    await spreadsheet.selectCell("L3", { display: "TRUE", value: true, formula: "A3=G3" });
    await spreadsheet.selectCell("L4", { display: "TRUE", value: true, formula: "A4=G4" });
    await spreadsheet.selectCell("L5", { display: "TRUE", value: true, formula: "A5=G5" });
    await spreadsheet.selectCell("L6", { display: "TRUE", value: true, formula: "A6=G6" });
    await spreadsheet.selectCell("M3", { display: "TRUE", value: true, formula: "B3=H3" });
    await spreadsheet.selectCell("M4", { display: "TRUE", value: true, formula: "B4=H4" });
    await spreadsheet.selectCell("M5", { display: "TRUE", value: true, formula: "B5=H5" });
    await spreadsheet.selectCell("M6", { display: "TRUE", value: true, formula: "B6=H6" });
    await spreadsheet.selectCell("N3", { display: "TRUE", value: true, formula: "C3=I3" });
    await spreadsheet.selectCell("N4", { display: "TRUE", value: true, formula: "C4=I4" });
    await spreadsheet.selectCell("N5", { display: "TRUE", value: true, formula: "C5=I5" });
    await spreadsheet.selectCell("N6", { display: "TRUE", value: true, formula: "C6=I6" });
    await spreadsheet.selectCell("O3", { display: "TRUE", value: true, formula: "D3=J3" });
    await spreadsheet.selectCell("O4", { display: "TRUE", value: true, formula: "D4=J4" });
    await spreadsheet.selectCell("O5", { display: "TRUE", value: true, formula: "D5=J5" });
    await spreadsheet.selectCell("O6", { display: "TRUE", value: true, formula: "D6=J6" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8876] Sorting - over columns selection", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/sorting_over_column_selection.xlsx");
    await spreadsheet.selectRange("B3:B6");

    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });

    //descendig
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort", withText: "Descending" });

    //check the dialog buttons
    I.waitForVisible({ css: "button[data-action=cancel]" });
    I.waitForVisible({ css: "button[data-action=no]" });
    I.waitForAndClick({ css: "button[data-action=ok]" });

    I.waitForChangesSaved();

    //check the values and the order
    await spreadsheet.selectCell("L3", { display: "TRUE", value: true, formula: "A3=G3" });
    await spreadsheet.selectCell("L4", { display: "TRUE", value: true, formula: "A4=G4" });
    await spreadsheet.selectCell("L5", { display: "TRUE", value: true, formula: "A5=G5" });
    await spreadsheet.selectCell("L6", { display: "TRUE", value: true, formula: "A6=G6" });
    await spreadsheet.selectCell("M3", { display: "TRUE", value: true, formula: "B3=H3" });
    await spreadsheet.selectCell("M4", { display: "TRUE", value: true, formula: "B4=H4" });
    await spreadsheet.selectCell("M5", { display: "TRUE", value: true, formula: "B5=H5" });
    await spreadsheet.selectCell("M6", { display: "TRUE", value: true, formula: "B6=H6" });
    await spreadsheet.selectCell("N3", { display: "TRUE", value: true, formula: "C3=I3" });
    await spreadsheet.selectCell("N4", { display: "TRUE", value: true, formula: "C4=I4" });
    await spreadsheet.selectCell("N5", { display: "TRUE", value: true, formula: "C5=I5" });
    await spreadsheet.selectCell("N6", { display: "TRUE", value: true, formula: "C6=I6" });
    await spreadsheet.selectCell("O3", { display: "TRUE", value: true, formula: "D3=J3" });
    await spreadsheet.selectCell("O4", { display: "TRUE", value: true, formula: "D4=J4" });
    await spreadsheet.selectCell("O5", { display: "TRUE", value: true, formula: "D5=J5" });
    await spreadsheet.selectCell("O6", { display: "TRUE", value: true, formula: "D6=J6" });

    await I.reopenDocument();

    //check the values and the order
    await spreadsheet.selectCell("L3", { display: "TRUE", value: true, formula: "A3=G3" });
    await spreadsheet.selectCell("L4", { display: "TRUE", value: true, formula: "A4=G4" });
    await spreadsheet.selectCell("L5", { display: "TRUE", value: true, formula: "A5=G5" });
    await spreadsheet.selectCell("L6", { display: "TRUE", value: true, formula: "A6=G6" });
    await spreadsheet.selectCell("M3", { display: "TRUE", value: true, formula: "B3=H3" });
    await spreadsheet.selectCell("M4", { display: "TRUE", value: true, formula: "B4=H4" });
    await spreadsheet.selectCell("M5", { display: "TRUE", value: true, formula: "B5=H5" });
    await spreadsheet.selectCell("M6", { display: "TRUE", value: true, formula: "B6=H6" });
    await spreadsheet.selectCell("N3", { display: "TRUE", value: true, formula: "C3=I3" });
    await spreadsheet.selectCell("N4", { display: "TRUE", value: true, formula: "C4=I4" });
    await spreadsheet.selectCell("N5", { display: "TRUE", value: true, formula: "C5=I5" });
    await spreadsheet.selectCell("N6", { display: "TRUE", value: true, formula: "C6=I6" });
    await spreadsheet.selectCell("O3", { display: "TRUE", value: true, formula: "D3=J3" });
    await spreadsheet.selectCell("O4", { display: "TRUE", value: true, formula: "D4=J4" });
    await spreadsheet.selectCell("O5", { display: "TRUE", value: true, formula: "D5=J5" });
    await spreadsheet.selectCell("O6", { display: "TRUE", value: true, formula: "D6=J6" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
