/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Dependency Manager");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85754] Dependency manager - Can dectect circular reference", ({ I, alert, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a direct circular dependency
    spreadsheet.enterCellText("=A1");
    alert.waitForVisible("A reference in the formula is dependent on the result of the formula cell.");
    alert.clickCloseButton();

    // enter an indirect circular dependency
    spreadsheet.enterCellText("=A3");
    spreadsheet.enterCellText("=A2");
    alert.waitForVisible("A reference in the formula is dependent on the result of the formula cell.");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85755] Dependency manager - Recalculation is limited in OX Spreadsheet", async ({ I, alert, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/16000formulas.xlsx");

    // wait for the complexity alert
    alert.waitForVisible("This document exceeds the spreadsheet size and complexity limits.", 20);
    alert.clickCloseButton();

    // create a formula and check that it will not be updated automatically
    spreadsheet.enterCellText("=A2+1", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1 });
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("42", { leave: "up" });
    I.wait(2); // intentionally wait here... formula must not be updated
    spreadsheet.waitForActiveCell("A1", { value: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85757] Dependency manager - Can recalculate results automatically", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/dependency_manager.xlsx");
    spreadsheet.waitForRecalculation(10);

    // sheet "CellFormulas": enter a number in cell B6
    spreadsheet.activateSheet(0);
    await spreadsheet.selectCell("B6");
    spreadsheet.enterCellText("42");

    // check dependent formulas
    await spreadsheet.selectCell("A9", { value: 84 });
    await spreadsheet.selectCell("A10", { value: 85 });
    await spreadsheet.selectCell("A11", { value: 127 });
    await spreadsheet.selectCell("A12", { value: 296 });

    // sheet "ManyFormulas": enter a number in cell B6
    spreadsheet.activateSheet(1);
    await spreadsheet.selectCell("B6");
    spreadsheet.enterCellText("42");
    spreadsheet.waitForRecalculation(10);

    // check dependent formulas
    await spreadsheet.selectCell("B9", { value: 42 });
    I.pressKeys("Ctrl+ArrowRight");
    spreadsheet.waitForActiveCell("AN9", { value: 42 });
    I.pressKeys("Ctrl+ArrowDown");
    spreadsheet.waitForActiveCell("AN33", { value: 42 });
    I.pressKeys("Ctrl+ArrowLeft");
    spreadsheet.waitForActiveCell("A33", { value: 42 });

    // sheet "ManyFormulas": enter a number in cell B42
    await spreadsheet.selectCell("B42");
    spreadsheet.enterCellText("42");
    spreadsheet.waitForRecalculation(10);

    // check dependent formulas
    I.pressKeys("Ctrl+ArrowDown");
    spreadsheet.waitForActiveCell("B45", { value: 44 });
    I.pressKeys("Ctrl+ArrowRight");
    spreadsheet.waitForActiveCell("AN45", { value: 82 });
    I.pressKeys("Ctrl+ArrowDown");
    spreadsheet.waitForActiveCell("AN69", { value: 1042 });
    I.pressKeys("Ctrl+ArrowLeft");
    spreadsheet.waitForActiveCell("A69", { value: 1003 });

    // sheet "ManyFormulas": enter a number in cell B78
    I.pressKeys("ArrowRight", "Ctrl+ArrowDown");
    spreadsheet.waitForActiveCell("B78");
    spreadsheet.enterCellText("42");
    spreadsheet.waitForRecalculation(10);

    // check dependent formulas
    I.pressKeys("Ctrl+ArrowDown");
    spreadsheet.waitForActiveCell("B81", { value: 44 });
    I.pressKeys("Ctrl+ArrowRight");
    spreadsheet.waitForActiveCell("AN81", { value: 82 });
    I.pressKeys("Ctrl+ArrowDown");
    spreadsheet.waitForActiveCell("AN105", { value: 82 });
    I.pressKeys("Ctrl+ArrowLeft");
    spreadsheet.waitForActiveCell("A105", { value: 67 });

    // check that formula results will be saved
    await I.reopenDocument();
    spreadsheet.waitForRecalculation(10);
    spreadsheet.activateSheet(0);
    I.pressKeys("Ctrl+Home", "3*Ctrl+ArrowDown", "ArrowDown");
    spreadsheet.waitForActiveCell("A9", { value: 84 });
    I.pressKey("ArrowDown");
    spreadsheet.waitForActiveCell("A10", { value: 85 });
    I.pressKey("ArrowDown");
    spreadsheet.waitForActiveCell("A11", { value: 127 });
    I.pressKey("ArrowDown");
    spreadsheet.waitForActiveCell("A12", { value: 296 });

    I.closeDocument();
}).tag("stable");
