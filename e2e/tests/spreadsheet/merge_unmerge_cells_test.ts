/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell format > Merge/Unmerge Cells");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8132] Merge cells Horizontally", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("C2");
    spreadsheet.enterCellText("C2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    I.pressKeys("Shift+ArrowLeft");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:C2" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:C2" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C38781] Merge cells Horizontally (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("C2");
    spreadsheet.enterCellText("C2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    I.pressKeys("Shift+ArrowLeft");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:C2" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:C2" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8133] Merge cells Vertically", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    I.pressKeys("Shift+ArrowUp");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the color, text for the merged cell
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3"  });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38782] Merge cells Vertically(ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    I.pressKeys("Shift+ArrowUp");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the color for the merged cell
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8136] Unmerge cells", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    I.pressKeys("Shift+ArrowUp");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "green" });

    //unmerged cells
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the merged area
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3", { merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check bgcolor and text
    await spreadsheet.selectCell("B2", { value: "B2Cell", merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3", { merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38783] Unmerge cells (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    I.pressKeys("Shift+ArrowUp");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the data-range / merge area
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "green" });

    //unmerged cells
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check bgcolor, text and merge area
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check bgcolor and text
    await spreadsheet.selectCell("B2", { value: "B2Cell" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8134] Merge cells Horizontally by Menu entry", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //B2: enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //B3: set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //C2: set bgcolor red and text
    await spreadsheet.selectCell("C2");
    spreadsheet.enterCellText("C2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "blue");
    I.waitForChangesSaved();

    //C3: set bgcolor red and text
    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("C3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "yellow");
    I.waitForChangesSaved();

    //merged cells by menu entry
    await spreadsheet.selectRange("B2:C3");
    I.clickOptionButton("cell/merge", "horizontal");
    I.waitForChangesSaved();

    //check the data-range
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3", { value: "B3Cell", merged: "B3:C3" });
    I.waitForElement({ spreadsheet: "selection-range", range: "B3:C3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    await I.reopenDocument();

    //check the data-range
    await spreadsheet.selectCell("B2", { value: "B2Cell", merged: "B2:C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:C2" });
    await spreadsheet.selectCell("B3", { value: "B3Cell", merged: "B3:C3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });
    I.waitForElement({ spreadsheet: "selection-range", range: "B3:C3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8135] Merge cells Vertically by Menu  entry", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //B2: enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //B3: set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //C2: set bgcolor red and text
    await spreadsheet.selectCell("C2");
    spreadsheet.enterCellText("C2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "blue");
    I.waitForChangesSaved();

    //C3: set bgcolor red and text
    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("C3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "yellow");
    I.waitForChangesSaved();

    //merged cells by menu entry
    await spreadsheet.selectRange("B2:C3");
    I.clickOptionButton("cell/merge", "vertical");
    I.waitForChangesSaved();

    // check bgcolor, text, range
    await spreadsheet.selectCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    await spreadsheet.selectCell("C2", { value: "C2Cell", merged: "C2:C3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "blue" });
    I.waitForElement({ spreadsheet: "selection-range", range: "C2:C3" });

    await I.reopenDocument();

    // check bgcolor, text, range
    await spreadsheet.selectCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });
    await spreadsheet.selectCell("C2", { value: "C2Cell", merged: "C2:C3" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "blue" });
    I.waitForElement({ spreadsheet: "selection-range", range: "C2:C3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8137] Unmerge cells by Menu entry", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //enter text, set green bgcolor
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //set bgcolor red and text
    await spreadsheet.selectCell("B3");
    spreadsheet.enterCellText("B3Cell", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    await spreadsheet.selectRange("B2:B3");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check the data-range/ merge area
    spreadsheet.waitForActiveCell("B2", { value: "B2Cell", merged: "B2:B3" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.waitForElement({ spreadsheet: "selection-range", range: "B2:B3" });

    //unmerged cells by menu entry
    I.clickOptionButton("cell/merge", "unmerge");
    I.waitForChangesSaved();

    //check bgcolor and text
    await spreadsheet.selectCell("B2", { value: "B2Cell", merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3", { merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check bgcolor and text
    await spreadsheet.selectCell("B2", { value: "B2Cell", merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("B3", { merged: null });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
