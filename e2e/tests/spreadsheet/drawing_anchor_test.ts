/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Drawing anchor");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C276511] Drawing property 'Move and size with cells' - hide columns", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/move_size_with_cells_hide_columns.xlsx");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    spreadsheet.selectColumns("C:D");

    //hide column C and D
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });

    //drawing is invisble
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0, type: "shape" });

    await I.reopenDocument();

    //drawing is invisble
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0, type: "shape" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276512] Drawing property 'Move and size with cells' - insert a column", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/move_and_size_one_columns.xlsx");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check the inital width/height
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape" });
    expect(rect1.width).to.be.within(59, 180);

    I.pressKey("Escape");

    //insert a column
    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("D");
    I.waitForAndClick({ docs: "control", key: "column/insert" });
    I.waitForChangesSaved();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });

    //check the new width/height
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing", { withText: "Shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(59, 270);

    await I.reopenDocument();

    I.pressKeys("Control+Home");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });

    //check the new width/height
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing", { withText: "Shape" });
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect3.width).to.be.within(59, 270);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276513] Drawing property 'Move and size with cells' - hide one column", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/move_size_with_cells_hide_columns.xlsx");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    spreadsheet.selectColumn("C");

    //hide column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });

    //drawing is visble
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });

    await I.reopenDocument();

    //drawing is visble
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C276552] (ODS) Drawing property 'To cell (resize with cell)' - hide columns", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/to_cell_resize_with_cell_hide columns.ods");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    spreadsheet.selectColumns("C:D");

    //hide column C and D
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });

    //drawing is invisble
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0, type: "shape" });

    await I.reopenDocument();

    //drawing is invisble
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0, type: "shape" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276554] Drawing property 'To cell (resize with cell)' - delete one colum", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/to_cell_resize_with_cell_delete_one_column.ods");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check the inital width/height
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.be.within(52, 167);

    I.pressKey("Escape");
    spreadsheet.selectColumn("C");

    //delete column C
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/delete", { inside: "context-menu" });

    I.pressKeys("Control+Home");

    //check the new width/height
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(52, 83);

    await I.reopenDocument();

    I.pressKeys("Control+Home");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });

    //check the new width/height
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing", { withText: "Shape" });
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect3.width).to.be.within(52, 83);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276551] (ODS) Drawing property 'To page' - hide columns", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/to_page_hide_columns.ods");

    spreadsheet.selectColumns("C:D");

    //hide column C and D
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });

    I.pressKey("ArrowRight");
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check the inital width/height
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.be.within(52, 167);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "E2:F4" });

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check the inital width/height
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(52, 167);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "E2:F4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276515] Drawing property 'Move but don't size with cells' - Insert a column", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/move_but_dont_size_insert_column.xlsx");
    spreadsheet.selectColumn("C");

    //Insert a column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/insert", { inside: "context-menu" });
    I.pressKey("ArrowRight");
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check the width/height
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.be.within(52, 167);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "D2:E4" });

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check thewidth/height
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(52, 167);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "D2:E4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276549] Drawing property 'Move and size with cells' - hide columns", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/dont_move_size_hide_coumns.xlsx");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.be.within(52, 167);

    spreadsheet.selectColumns("C:D");

    //hide column C and D
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(52, 167);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "E2:F4" });

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect3.width).to.be.within(52, 167);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "E2:F4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276550] Drawing property 'Move and size with cells' - insert one column", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/dont_move_cells_insert_column.xlsx");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.be.within(52, 168);

    spreadsheet.selectColumn("D");

    //insert column D
    I.waitForVisible({ spreadsheet: "col-resize", pos: "D", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "D" });
    I.waitForContextMenuVisible();
    I.clickButton("column/insert", { inside: "context-menu" });

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(52, 168);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "C2:D4" });

    //check the values
    await spreadsheet.selectCell("E2", { value: null });
    await spreadsheet.selectCell("E3", { value: null });
    await spreadsheet.selectCell("E4", { value: null });
    await spreadsheet.selectCell("F2", { display: "E2", value: "E2" });
    await spreadsheet.selectCell("F3",  { display: "E3", value: "E3" });
    await spreadsheet.selectCell("F4", { display: "E4", value: "E4" });

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect3.width).to.be.within(52, 168);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "C2:D4" });

    //check the values
    await spreadsheet.selectCell("E2", { value: null });
    await spreadsheet.selectCell("E3", { value: null });
    await spreadsheet.selectCell("E4", { value: null });
    await spreadsheet.selectCell("F2", { display: "E2", value: "E2" });
    await spreadsheet.selectCell("F3",  { display: "E3", value: "E3" });
    await spreadsheet.selectCell("F4", { display: "E4", value: "E4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C276514] Drawing property 'Move but don't size with cells' - hide columns", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/move_but_dont_size_hide_columns.xlsx");

    //shape is selected and visible
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.be.within(52, 180);

    spreadsheet.selectColumns("C:D");

    //insert column D
    I.waitForVisible({ spreadsheet: "col-resize", pos: "C", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect2.width).to.be.within(52, 180);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "C2:F4" });

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect3.width).to.be.within(52, 180);

    //data-range is change
    I.seeAttributesOnElements({ spreadsheet: "drawing", pos: 0 }, { "data-range": "C2:F4" });

    I.closeDocument();
}).tag("stable");
