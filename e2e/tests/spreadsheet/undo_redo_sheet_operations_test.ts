/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo sheet operations");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8267] Undo sheetname", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("document/insertsheet");

    //open the sheet tab context menu
    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1 });
    I.clickButton("sheet/rename/dialog", { withText: "Rename sheet", inside: "context-menu" });

    dialogs.waitForVisible();

    //context menu
    I.fillField({ docs: "dialog", find: "input" }, "äöüß");

    //close dialog
    dialogs.clickOkButton();

    I.waitForChangesSaved();

    //check the active state and the sheet name

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "äöüß", state: 1 });

    //click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    await I.reopenDocument();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8273] Redo sheetname", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("document/insertsheet");

    //open the sheet tab context menu
    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1 });
    I.clickButton("sheet/rename/dialog", { withText: "Rename sheet", inside: "context-menu" });

    dialogs.waitForVisible();

    //context menu
    I.fillField({ docs: "dialog", find: "input" }, "äöüß");

    //close dialog
    dialogs.clickOkButton();

    I.waitForChangesSaved();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "äöüß", state: 1 });

    //click undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    // click redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "äöüß", state: 1 });

    await I.reopenDocument();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "äöüß", state: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8268] Undo copy sheets", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("hello", { leave: "stay" });

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //close dialog
    dialogs.clickOkButton();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });
    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    await I.reopenDocument();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });
    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8269] Redo copy sheets", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("hello", { leave: "stay" });

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    //close dialog
    dialogs.clickOkButton();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2", state: 1 });
    spreadsheet.waitForActiveCell("A1", { value: "hello" });


    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    await I.reopenDocument();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8274] Undo sheet", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active",  withText: "Sheet1", state: 0 });

    //insert a sheet
    spreadsheet.insertSheet();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active",  withText: "Sheet2", state: 1 });

    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    await I.reopenDocument();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8275] Redo sheet", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert a sheet
    spreadsheet.insertSheet();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active",  withText: "Sheet1", state: 0 });

    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 1 });

    await I.reopenDocument();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272371] Undo reorder sheets (with reorder sheet dialog)", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 3 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/movesheets/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //move the sheet4
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[data-action='prev']" }); // move up button
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[data-action='prev']" }); // move up button

    //Sheet4 button is active
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".badge-list button.btn-primary:nth-child(2)", withText: "Sheet4" });

    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 3 });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 2 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 2 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272373] Redo reorder sheets (with reorder sheet dialog)", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 3 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/movesheets/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //move the sheet4
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[data-action='prev']" }); // move up button
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[data-action='prev']" }); // move up button

    //Sheet4 button is active
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".badge-list button.btn-primary:nth-child(2)", withText: "Sheet4" });

    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 3 });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 2 });

    //redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 3 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 3 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272374] Undo hide sheet", async ({ I,  spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("sheet/visible", { inside: "popup-menu" });

    //check the sheet status
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.dontSeeElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the sheet status
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    await I.reopenDocument();

    //check the sheet status
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272375] Redo hide sheet", async ({ I,  spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("sheet/visible", { inside: "popup-menu" });

    //check the sheet status
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.dontSeeElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //check the sheet status
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    //redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");

    //check the sheet status
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.dontSeeElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 0 });
    I.dontSeeElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272376] Undo protected sheet", async ({ I, alert, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });

    spreadsheet.enterCellText("lockedCell", { leave: "stay" });

    //alert protected
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    spreadsheet.enterCellText("unlockCell", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "unlockCell" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { value: "unlockCell" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272377] Redo protected sheet", async ({ I, alert, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });

    //check the protected sheet
    spreadsheet.enterCellText("lorem", { leave: "stay" });

    //alert protected
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");

    //redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");

    spreadsheet.enterCellText("lockedCell", { leave: "stay" });

    //alert protected
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    await I.reopenDocument();

    spreadsheet.enterCellText("Lorem1", { leave: "stay" });

    //alert protected
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272372] Undo reorder sheets (via drag&drop)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 3 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    // drag "Sheet4" via mouse to position between "Sheet1" and "Sheet2"
    const rect = await I.grabElementBoundingRect({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });
    I.dragMouseOnElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 3 }, -2 * rect.width, 0);
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 1 });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    I.closeDocument();
}).tag("stable");
