/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Rows and Columns > Toolbar menu");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8168] Insert row (via toolbar button)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("colrow");

    spreadsheet.enterCellText("A1Cell", { leave: "down" });
    spreadsheet.enterCellText("A2Cell", { leave: "down" });
    spreadsheet.enterCellText("A3Cell");

    spreadsheet.selectRow("2");

    I.waitForAndClick({ docs: "control", key: "row/insert" });
    I.waitForChangesSaved();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("A2", { value: null });
    await spreadsheet.selectCell("A3", { value: "A2Cell" });
    await spreadsheet.selectCell("A4", { value: "A3Cell" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("A2", { value: null });
    await spreadsheet.selectCell("A3", { value: "A2Cell" });
    await spreadsheet.selectCell("A4", { value: "A3Cell" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8165] Delete column (via toolbar button)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("colrow");

    spreadsheet.enterCellText("A1Cell", { leave: "right" });
    spreadsheet.enterCellText("B1Cell", { leave: "right" });
    spreadsheet.enterCellText("C1Cell");

    spreadsheet.selectColumn("B");

    I.waitForAndClick({ docs: "control", key: "column/delete" });
    I.waitForChangesSaved();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("B1", { value: "C1Cell" });
    await spreadsheet.selectCell("C1", { value: null });

    await I.reopenDocument();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("B1", { value: "C1Cell" });
    await spreadsheet.selectCell("C1", { value: null });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C38814] Insert row (via toolbar button), (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.clickToolbarTab("colrow");

    spreadsheet.enterCellText("A1Cell", { leave: "down" });
    spreadsheet.enterCellText("A2Cell", { leave: "down" });
    spreadsheet.enterCellText("A3Cell");

    spreadsheet.selectRow("2");

    I.waitForAndClick({ docs: "control", key: "row/insert" });
    I.waitForChangesSaved();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("A2", { value: null });
    await spreadsheet.selectCell("A3", { value: "A2Cell" });
    await spreadsheet.selectCell("A4", { value: "A3Cell" });

    await I.reopenDocument();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("A2", { value: null });
    await spreadsheet.selectCell("A3", { value: "A2Cell" });
    await spreadsheet.selectCell("A4", { value: "A3Cell" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8170] Insert column (via toolbar button)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("colrow");

    spreadsheet.enterCellText("A1Cell", { leave: "right" });
    spreadsheet.enterCellText("B1Cell", { leave: "right" });
    spreadsheet.enterCellText("C1Cell");

    spreadsheet.selectColumn("B");

    I.waitForAndClick({ docs: "control", key: "column/insert" });
    I.waitForChangesSaved();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("B1", { value: null });
    await spreadsheet.selectCell("C1", { value: "B1Cell" });
    await spreadsheet.selectCell("D1", { value: "C1Cell" });

    await I.reopenDocument();

    //check the toolbar (workaround for flexible toolbar) ->DOCS-3573
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("B1", { value: null });
    await spreadsheet.selectCell("C1", { value: "B1Cell" });
    await spreadsheet.selectCell("D1", { value: "C1Cell" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8167] Column width, min value (via toolbar)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B1");

    I.clickToolbarTab("colrow");
    I.waitForAndClick({ docs: "control", key: "column/width/active" });

    I.pressKeys("5*Backspace");

    //type 0
    I.fillTextField("column/width/active", "0");

    //check the state
    I.waitForVisible({ docs: "control", key: "column/width/active", state: "0" });

    //check the hidden coulmn
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: true });

    //hidden marker
    I.waitForVisible({ spreadsheet: "col-hidden-marker", pos: "B" });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForVisible({ docs: "control", key: "column/width/active", state: "0" });

    //I.waitForVisible({ css: ".resizer.collapsed.app-tooltip" }); TODO -> Ask Daniel

    //check the hidden column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: true });

    //hidden marker
    I.waitForVisible({ spreadsheet: "col-hidden-marker", pos: "B" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8171] Row height, min value (via toolbar)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A2");

    I.clickToolbarTab("colrow");

    I.waitForAndClick({ docs: "control", key: "row/height/active" });

    I.pressKeys("5*Backspace");

    //type 0
    I.fillTextField("row/height/active", "0");

    //check the state
    I.waitForVisible({ docs: "control", key: "row/height/active", state: "0" });

    //check the hidden row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: true });

    //hidden marker
    I.waitForVisible({ spreadsheet: "row-hidden-marker", pos: "2" });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForVisible({ docs: "control", key: "row/height/active", state: "0" });

    //check the hidden row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: true });

    //hidden marker
    I.waitForVisible({ spreadsheet: "row-hidden-marker", pos: "2" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8169] Column width, max value (via toolbar)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B1");

    I.clickToolbarTab("colrow");
    I.waitForAndClick({ docs: "control", key: "column/width/active" });
    I.pressKeys("5*Backspace");

    //type 21.65
    I.fillTextField("column/width/active", "21.65");

    //check the state
    I.waitForVisible({ docs: "control", key: "column/width/active", state: "54991" });

    //check the column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    I.waitForVisible({ docs: "control", key: "column/width/active", state: "54991" });

    //I.waitForVisible({ css: ".resizer.collapsed.app-tooltip" }); TODO -> Ask Daniel

    //check the column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49106] Column width, max value (via toolbar), (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B1");

    I.clickToolbarTab("colrow");

    I.waitForAndClick({ docs: "control", key: "column/width/active" });

    I.pressKeys("5*Backspace");

    //type 21.65
    I.fillTextField("column/width/active", "39.35");

    //check the state
    I.waitForVisible({ docs: "control", key: "column/width/active", state: "99949" });

    //check the column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    I.waitForVisible({ docs: "control", key: "column/width/active", state: "99949" });

    //I.waitForVisible({ css: ".resizer.collapsed.app-tooltip" }); TODO -> Ask Daniel

    //check the column
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8166] Row height, max value (via toolbar)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A2");

    I.clickToolbarTab("colrow");
    I.waitForAndClick({ docs: "control", key: "row/height/active" });

    I.pressKeys("5*Backspace");

    //type 5.7
    I.fillTextField("row/height/active", "5.7");

    //check the state
    I.waitForVisible({ docs: "control", key: "row/height/active", state: "14446" });

    //check the row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    I.waitForVisible({ docs: "control", key: "row/height/active", state: "14446" });

    //I.waitForVisible({ css: ".resizer.collapsed.app-tooltip" }); TODO -> Ask Daniel

    //check the row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49105] Row height, max value (via toolbar),(ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("A2");

    I.clickToolbarTab("colrow");
    I.waitForAndClick({ docs: "control", key: "row/height/active" });

    I.pressKeys("5*Backspace");

    //type 5.7
    I.fillTextField("row/height/active", "39.35");

    //check the state
    I.waitForVisible({ docs: "control", key: "row/height/active", state: "99949" });

    //check the row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    I.waitForVisible({ docs: "control", key: "row/height/active", state: "99949" });

    //check the row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C38813] Delete column (via toolbar button), (ODS)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("colrow");

    spreadsheet.enterCellText("A1Cell", { leave: "right" });
    spreadsheet.enterCellText("B1Cell", { leave: "right" });
    spreadsheet.enterCellText("C1Cell");

    spreadsheet.selectColumn("B");

    I.waitForAndClick({ docs: "control", key: "column/delete" });
    I.waitForChangesSaved();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("B1", { value: "C1Cell" });
    await spreadsheet.selectCell("C1", { value: null });

    await I.reopenDocument();

    //check the toolbar (workaround for flexible toolbar) ->DOCS-3573
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("B1", { value: "C1Cell" });
    await spreadsheet.selectCell("C1", { value: null });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8172] Delete row (via toolbar button)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("colrow");

    spreadsheet.enterCellText("A1Cell", { leave: "down" });
    spreadsheet.enterCellText("A2Cell", { leave: "down" });
    spreadsheet.enterCellText("A3Cell");

    spreadsheet.selectRow("2");

    I.waitForAndClick({ docs: "control", key: "row/delete" });
    I.waitForChangesSaved();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("A2", { value: "A3Cell" });
    await spreadsheet.selectCell("A3", { value: null });

    await I.reopenDocument();

    //check the values
    await spreadsheet.selectCell("A1", { value: "A1Cell" });
    await spreadsheet.selectCell("A2", { value: "A3Cell" });
    await spreadsheet.selectCell("A3", { value: null });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8163] Row height, resize (via insert linebreak)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("test dummy, and more text, text text", { leave: "stay" });

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    //edit mode
    I.pressKey("F2");
    I.pressKeys("4*Alt+ArrowLeft");

    //insert a line break, and leave B2
    I.pressKeys("Alt+Enter");
    I.pressKey("Enter");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2011" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2011" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49104] Row height, resize (via insert linebreak), ODS", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("test dummy, and more text, text text", { leave: "stay" });

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    //edit mode
    I.pressKey("F2");
    I.pressKeys("4*Alt+ArrowLeft");

    //insert a line break, and leave B2
    I.pressKeys("Alt+Enter");
    I.pressKey("Enter");

    //check the change state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2011" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2011" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8164] Column width, resize (via double click)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("test dummy, and more text, text text", { leave: "stay" });

    I.clickToolbarTab("colrow");

    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    //edit mode
    I.pressKey("F2");
    I.pressKeys("4*Alt+ArrowLeft");

    //insert a line break, and leave B2
    I.pressKeys("Alt+Enter");
    I.pressKey("Enter");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2011" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    await I.reopenDocument();

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2011" });
    I.waitForElement({ css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-5187] Update outdated row height in imported file (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-5187.ods");

    expect(await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "7" }, "height")).to.be.at.least(60);
    expect(await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "10" }, "height")).to.be.at.least(40);

    I.closeDocument();
}).tag("stable");
