/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Chart > Change Chart Type");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8210] Change existing chart type (Bubble to Column)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    // change chart type
    I.clickOptionButton("drawing/charttype", "column standard");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8211] Change existing chart type (Pie to Scatter)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "pie standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    // change chart type
    I.clickOptionButton("drawing/charttype", "scatter standard");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8195] Change existing chart type (Column to Pie)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    // change chart type
    I.clickOptionButton("drawing/charttype", "pie standard");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114371] Change existing chart type (Bubble to Column) (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    // change chart type
    I.clickOptionButton("drawing/charttype", "column standard");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114372] Change existing chart type (Pie to Scatter) (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "pie standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    // change chart type
    I.clickOptionButton("drawing/charttype", "scatter standard");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173445] Notification alert - data references are too complex", async ({ I, alert }) => {

    await I.loginAndOpenDocument("media/files/chart_too_complex.xlsx");

    // select first chart
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    // try to change source data direction
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.clickButton("drawing/chartexchange", { inside: "popup-menu" });
    alert.waitForVisible("Data references are too complex for this operation");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173446] Notification alert - limitation cell input", async ({ I, alert }) => {

    await I.loginAndOpenDocument("media/files/chart_too_complex.xlsx");

    // select first chart
    I.waitForAndClick({ spreadsheet: "drawing", pos: 1, type: "chart" });
    I.waitForToolbarTab("drawing");

    // try to change chart type
    I.clickOptionButton("drawing/charttype", "bar standard");
    alert.waitForVisible("It is not possible to create a chart out of more than 50 input cells");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");
