/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Spreadsheet > Clipboard > Internal clipboard OX Spreadsheet > OX Spreadsheet");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8311] Copy&Paste plain text in OX Spreadsheet", async ({ I, spreadsheet }) => {

    // open local html page
    I.openLocalFile("media/files/plaintext_1.txt");
    // eslint-disable-next-line @typescript-eslint/no-deprecated
    I.executeScript(() => document.execCommand("selectall"));

    I.copy();

    I.loginAndHaveNewDocument("spreadsheet");
    I.paste();
    I.wait(1);

    spreadsheet.waitForActiveCell("A1", { display: "plain text without format äöü€" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { display: "plain text without format äöü€" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8312] Copy&Paste semicolon separated plain text in OX Spreadsheet", async ({ I, spreadsheet }) => {

    // open local html page
    I.openLocalFile("media/files/plaintext_2.txt");
    // eslint-disable-next-line @typescript-eslint/no-deprecated
    I.executeScript(() => document.execCommand("selectall"));

    I.copy();
    I.wait(1);

    I.loginAndHaveNewDocument("spreadsheet");

    I.paste();

    await spreadsheet.selectCell("A1", { display: "plain" });
    await spreadsheet.selectCell("B1", { display: "text" });
    await spreadsheet.selectCell("C1", { value: "without" });
    await spreadsheet.selectCell("D1", { display: "format" });
    await spreadsheet.selectCell("E1", { display: "äöü€" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { display: "plain" });
    await spreadsheet.selectCell("B1", { display: "text" });
    await spreadsheet.selectCell("C1", { value: "without" });
    await spreadsheet.selectCell("D1", { display: "format" });
    await spreadsheet.selectCell("E1", { display: "äöü€" });

    I.closeDocument();
}).tag("stable");
