/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Protect Sheet");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C110341] [C110342] Protect sheet - user can protect and unprotect a sheet", async ({ I, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // type something
    spreadsheet.enterCellText("a", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "a" });

    // lock the sheet
    I.waitForChangesSaved();
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // try to type something
    I.type("b");
    alert.waitForVisible("Protected cells cannot be modified.");

    // retry after reload
    await I.reopenDocument();

    // try to type something
    I.type("b");
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    // unlock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: true });
    I.waitForChangesSaved();

    // type something
    spreadsheet.enterCellText("b", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "b" });

    // retry after reload
    await I.reopenDocument();
    spreadsheet.enterCellText("c", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "c" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C110350] [C111302] Protect sheet - user can unlock and lock a cell", async ({ I, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // unlock cell A1
    I.clickToolbarTab("data");
    I.clickButton("sheet/operation/unlocked/cell", { caret: true });
    I.clickButton("cell/unlocked", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // lock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // "lock cells" dropdown menu must be disabled
    I.waitForVisible({ docs: "control", key: "sheet/operation/unlocked/cell", disabled: true });

    // type something in A1
    spreadsheet.enterCellText("a", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "a" });

    // try to type something in A2
    I.pressKey("ArrowDown");
    I.type("b");
    alert.waitForVisible("Protected cells cannot be modified.");

    // retry after reload
    await I.reopenDocument();

    // "lock cells" dropdown menu must still be disabled
    I.clickToolbarTab("data");
    I.waitForVisible({ docs: "control", key: "sheet/operation/unlocked/cell", disabled: true });

    // type something in A1
    I.pressKeys("Ctrl+Home");
    spreadsheet.enterCellText("b", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "b" });

    // try to type something in A2
    I.pressKey("ArrowDown");
    I.type("c");
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    // unlock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: true });
    I.waitForChangesSaved();

    // lock cell A1
    I.pressKeys("Ctrl+Home");
    I.clickButton("sheet/operation/unlocked/cell", { caret: true });
    I.clickButton("cell/locked", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // lock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // "lock cells" dropdown menu must be disabled
    I.waitForVisible({ docs: "control", key: "sheet/operation/unlocked/cell", disabled: true });

    // try to type something in A1
    I.type("c");
    alert.waitForVisible("Protected cells cannot be modified.");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C110353] [C111303] Protect sheet - hide and unhide a formula", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert formula in cell A1
    spreadsheet.enterCellText("=SUM(1,3)");
    spreadsheet.enterCellText("=SUM(2,4)", { leave: "stay" });

    // formulas must be shown in formula pane
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "=SUM(2,4)");
    I.pressKey("ArrowUp");
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "=SUM(1,3)");

    // set cell to "hide formula" mode
    I.clickToolbarTab("data");
    I.clickButton("sheet/operation/unlocked/cell", { caret: true });
    I.clickButton("cell/hideformula", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // formula must still be shown in formula pane
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "=SUM(1,3)");

    // lock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // "lock cells" dropdown menu must be disabled
    I.waitForVisible({ docs: "control", key: "sheet/operation/unlocked/cell", disabled: true });

    // formula in cell A1 must not be shown in formula pane anymore
    I.waitForChangesSaved();
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");
    I.pressKey("ArrowDown");
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "SUM(2,4)");

    // retry after reload
    await I.reopenDocument();
    I.pressKeys("Ctrl+Home");
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");
    I.pressKey("ArrowDown");
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "SUM(2,4)");

    // "lock cells" dropdown menu must still be disabled
    I.clickToolbarTab("data");
    I.waitForVisible({ docs: "control", key: "sheet/operation/unlocked/cell", disabled: true });

    // unlock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: true });
    I.waitForChangesSaved();

    // set cell A1 to "show formula" mode
    I.pressKeys("Ctrl+Home");
    I.clickButton("sheet/operation/unlocked/cell", { caret: true });
    I.clickButton("cell/unhideformula", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // lock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // formula in cell A1 must be shown in formula pane
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "SUM(1,3)");
    I.pressKey("ArrowDown");
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "SUM(2,4)");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114946] [C114948] Protect sheet - user can protect and unprotect a sheet (ODS)", async ({ I, spreadsheet, alert }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // type something
    spreadsheet.enterCellText("a", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "a" });

    // lock the sheet
    I.waitForChangesSaved();
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // try to type something
    I.type("b");
    alert.waitForVisible("Protected cells cannot be modified.");

    // retry after reload
    await I.reopenDocument();

    // try to type something
    I.type("b");
    alert.waitForVisible("Protected cells cannot be modified.");
    alert.clickCloseButton();

    // unlock the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: true });
    I.waitForChangesSaved();

    // type something
    spreadsheet.enterCellText("b", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "b" });

    // retry after reload
    await I.reopenDocument();
    spreadsheet.enterCellText("c", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "c" });

    I.closeDocument();
}).tag("stable");
