/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Convert xls to xlsx");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C320527] Convert xls to xlsx", async ({ I, drive, alert }) => {

    await drive.uploadFileAndLogin("media/files/xls_to_xlsx.xls", { selectFile: true });

    // Click on 'Edit as new'
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='spreadsheet-edit-asnew-hi']" }, 10);

    // check the file to xlsx
    alert.waitForVisible("Document was converted and stored as", 60);
    alert.waitForVisible('"xls_to_xlsx.xlsx"', 60);
    alert.clickCloseButton();

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-5053a] Convert xls to xlsx (multi tab mode)", async ({ I, drive, alert }) => {

    await drive.uploadFileAndLogin("media/files/xls_to_xlsx.xls", { selectFile: true, tabbedMode: true });

    // Click on 'Edit as new'
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='spreadsheet-edit-asnew-hi']" }, 10);

    I.wait(5); // enough time until the new tab opens

    // a new tab is opened
    const tabCountAfter = await I.grabNumberOfOpenTabs();
    expect(tabCountAfter).to.equal(2);

    // switching forward to OX Text
    I.switchToNextTab();

    // check the file to xlsx
    alert.waitForVisible("Document was converted and stored as", 60);
    alert.waitForVisible('"xls_to_xlsx.xlsx"', 60);
    alert.clickCloseButton();

    I.closeDocument();

}).tag("stable");
