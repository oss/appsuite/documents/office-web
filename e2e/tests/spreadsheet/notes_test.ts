/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Comments / notes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C149417] Show next notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_notes.xlsx");

    I.dontSeeElement({ spreadsheet: "note", para: 0, pos: 0 });
    await spreadsheet.selectCell("A1");
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "a1" });

    I.clickToolbarTab("comments");

    //open the note dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/goto/next" });

    //check the values
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "b1" });

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C290662] Show previous notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_notes.xlsx");
    I.dontSeeElement({ spreadsheet: "note", para: 0, pos: 1 });

    await spreadsheet.selectCell("B1");
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "b1" });
    I.clickToolbarTab("comments");

    //open the note dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/goto/prev" });

    //check the values
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "a1" });

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C149419] Delete all notes", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/cells_with_notes.xlsx");

    I.dontSeeElement({ spreadsheet: "note", para: 0, pos: 0 });

    await spreadsheet.selectCell("A1");

    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "a1" });

    I.clickToolbarTab("comments");

    //open the note dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/show/all" });
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/delete/all" });
    I.waitForChangesSaved();

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "All notes on this sheet will be removed. Do you want to continue?" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    //check the notes
    I.dontSeeElementInDOM({ spreadsheet: "note-overlay" });

    await I.reopenDocument();

    //check the notes
    I.dontSeeElementInDOM({ spreadsheet: "note-overlay" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C163028] Delete a selected note", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_notes.xlsx");

    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.dontSeeElement({ spreadsheet: "note", pos: 1 });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });
    I.dontSeeElement({ spreadsheet: "note", pos: 3 });

    await spreadsheet.selectCell("B1");

    I.clickToolbarTab("comments");

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/delete" });
    I.waitForChangesSaved();

    //see numbers of element
    //I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 3);

    //check the notes
    I.seeNumberOfElements({ spreadsheet: "note-overlay" }, 3);

    await I.reopenDocument();

    I.clickToolbarTab("comments");

    //check the notes
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    //see numbers of element
    I.seeNumberOfElements({ spreadsheet: "note-overlay" }, 3);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223542] Show selected note", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_notes.xlsx");

    //check the values
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.dontSeeElement({ spreadsheet: "note", pos: 1 });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });
    I.dontSeeElement({ spreadsheet: "note", pos: 3 });

    await spreadsheet.selectCell("B1");

    I.clickToolbarTab("comments");

    //open the note dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/visible" });
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    //check the values
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.waitForVisible({ spreadsheet: "note", para: 1, pos: 1, withText: "b1" });
    I.dontSeeElement({ spreadsheet: "note", pos: 2  });
    I.dontSeeElement({ spreadsheet: "note", pos: 3 });

    await I.reopenDocument();

    I.clickToolbarTab("comments");

    //check the values
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.waitForVisible({ spreadsheet: "note", para: 1, pos: 1, withText: "b1" });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });
    I.dontSeeElement({ spreadsheet: "note", pos: 3 });

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223545] Hide selected note", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_open_note.xlsx");

    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "A1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "B1" });
    I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "A2" });
    I.waitForVisible({ spreadsheet: "note", pos: 3, para: 1, withText: "B2" });

    await spreadsheet.selectCell("A2");
    I.clickToolbarTab("comments");

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/visible" });
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    //check the values
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "A1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "B1" });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });
    I.waitForVisible({ spreadsheet: "note", pos: 3, para: 1, withText: "B2" });

    await I.reopenDocument();

    I.clickToolbarTab("comments");

    //check the values
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "A1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "B1" });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });
    I.waitForVisible({ spreadsheet: "note", pos: 3, para: 1, withText: "B2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223546] Hide all notes", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/show_hide_notes.xlsx");

    //check the  values
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "b1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "g2" });
    I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "e8" });

    I.clickToolbarTab("comments");

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/hide/all" });
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 3);

    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.dontSeeElement({ spreadsheet: "note", pos: 1 });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });

    await I.reopenDocument();

    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.dontSeeElement({ spreadsheet: "note", pos: 1 });
    I.dontSeeElement({ spreadsheet: "note", pos: 2 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C290661] Show all notes", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/show_all_notes.xlsx");

    I.clickToolbarTab("comments");

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/show/all" });
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 3);

    //check the values
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "b1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "g2" });
    I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "e8" });

    await I.reopenDocument();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 3);

    //check the values
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "b1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "g2" });
    I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "e8" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173454] Merged cells with notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_notes.xlsx");

    //check initial position of notes
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B2", hint: true });

    //merge cell range
    await spreadsheet.selectRange("A1:B2");
    I.waitForToolbarTab("format");
    I.clickButton("cell/merge");

    //check notes
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "B2", hint: true });

    await I.reopenDocument();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);

    //check notes
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "B2", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223567] Sort cells with comments", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/sorting_cells_with_notes.xlsx");

    //check the values
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });

    //sort cell range in descending order
    await spreadsheet.selectRange("A1:A4");
    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");

    //check the values
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });

    await I.reopenDocument();

    //check the values
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });

    I.closeDocument();
}).tag("stable");
