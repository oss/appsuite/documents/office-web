/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Data > Autofilter");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C290816] Autofilter - with descending sorting", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // prepare test data
    spreadsheet.enterCellText("Header");
    spreadsheet.enterCellText("1");
    spreadsheet.enterCellText("2");
    I.waitForChangesSaved();

    // insert an autofilter, sort cells descending
    await spreadsheet.selectCell("A1");
    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending", checked: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending", checked: true });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "sort_desc" });

    // check cell contents
    await spreadsheet.selectCell("A2", { value: 2 });
    await spreadsheet.selectCell("A3", { value: 1 });

    // check again after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "sort_desc" }, 5);
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending", checked: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending", checked: true });
    I.pressKeys("Escape");
    await spreadsheet.selectCell("A2", { value: 2 });
    await spreadsheet.selectCell("A3", { value: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8855] Autofilter - set one autofilter (via cell range selection)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_data.xlsx");

    await spreadsheet.selectRange("A1:A6");
    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a6" });

    //Close the pop-up
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //cell content
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });

    await I.reopenDocument();

    //cell content
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C15601] Autofilter - set one autofilter (via automatic expansion)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_data.xlsx");

    I.pressKeys("Ctrl+Home");
    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a6" });

    //Close the pop-up
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //content
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });

    await I.reopenDocument();

    //content
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49101] Autofilter - set one autofilter (via automatic expansion), (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_data.ods");

    I.pressKeys("Ctrl+Home");
    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a6" });

    //Close the pop-up
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //content
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });

    await I.reopenDocument();

    //content
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C15602] Autofilter - change an existing autofilter (via automatic expansion)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_data.xlsx");

    I.pressKeys("Ctrl+Home");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a6" });

    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    I.pressKeys("6*ArrowDown");

    spreadsheet.enterCellText("Cell A7", { leave: "stay" });

    I.pressKeys("Ctrl+Home");

    //open popup-menu again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values again
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a6" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a7" });

    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //check content
    await spreadsheet.selectCell("A1", { value: "Filter 1" });
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });
    await spreadsheet.selectCell("A7", { value: "Cell A7" });

    await I.reopenDocument();

    //open popup-menu again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values again
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a6" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a7" });

    //Close the pop-up
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //check content again
    await spreadsheet.selectCell("A1", { value: "Filter 1" });
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("A5", { value: "Cell A5" });
    await spreadsheet.selectCell("A6", { value: "Cell A6" });
    await spreadsheet.selectCell("A7", { value: "Cell A7" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8864] Autofilter - change autofilter settings", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //enter data
    spreadsheet.enterCellText("Rudi");
    spreadsheet.enterCellText("Kurt");
    spreadsheet.enterCellText("Otto");
    spreadsheet.enterCellText("100");

    I.pressKeys("Ctrl+Home");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values again
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: true });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "kurt", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "kurt", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "otto", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });

    //Close the pop-up
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    // hide row 4
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "filter" });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100", checked: false });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //cellbutton A1
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "filter" });

    //Content A2/A4 not visible
    I.dontSeeElement({ spreadsheet: "row-header", pos: "2" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    await I.reopenDocument();

    //cellbutton A1
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "filter" });

    //Content A2/A4 not visible
    I.dontSeeElement({ spreadsheet: "row-header", pos: "2" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    // remove autofilter (check restoration of rows)
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "filter" });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    I.waitForVisible({ spreadsheet: "row-header", pos: "2" });
    I.waitForVisible({ spreadsheet: "row-header", pos: "4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49103] Autofilter - change autofilter settings (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    //enter data
    spreadsheet.enterCellText("Rudi");
    spreadsheet.enterCellText("Kurt");
    spreadsheet.enterCellText("Otto");
    spreadsheet.enterCellText("100");

    I.pressKeys("Ctrl+Home");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values again
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "kurt", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "kurt", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "otto" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100" });

    //Close the pop-up
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //filter icon /cell button
    I.waitForElement({ css: ".grid-layer.form-layer i" });
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //A4
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100", checked: false });

    //Close the pop-up test
    I.waitForAndClick({ docs: "popup", action: "ok" });

    //filter icon /cell button
    I.waitForElement({ css: ".grid-layer.form-layer i" });

    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Content A2/A4 not visible
    I.dontSeeElement({ spreadsheet: "row-header", pos: "2" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    await I.reopenDocument();

    //filter icon /cell button
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Content A2/A4 not visible
    I.dontSeeElement({ spreadsheet: "row-header", pos: "2" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8860] Autofilter - type-independent list entries (boolean/string)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/filter_type_independent.xlsx");

    I.pressKeys("Ctrl+Home");

    I.clickToolbarTab("data");

    //open popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //check the popup values again
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "0", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "false", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "true", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "falsch", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "wahr", checked: false });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "true" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });

    //Close the pop-up
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //cellbutton A1
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "filter" });

    //Content A2/A3/A5/A6/A7 not visible
    I.dontSeeElement({ spreadsheet: "row-header", pos: "2" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "5" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "6" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "7" });

    await I.reopenDocument();

    //cellbutton A1
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1", icon: "filter" });

    //Content A2/A3/A5/A6/A7 not visible
    I.dontSeeElement({ spreadsheet: "row-header", pos: "2" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "5" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "6" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "7" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8866] Autofilter - reapply the autofilter settings", async ({ I,  spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_reapply.xlsx");

    //open popup-menu
    I.pressKeys("Ctrl+Home");
    I.clickToolbarTab("data");
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Check popup-menu
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1" });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "3" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });

    //change the cell value of A4
    await spreadsheet.selectCell("A4");
    spreadsheet.enterCellText("2", { leave: "stay" });

    //refresh
    I.clickButton("table/refresh");

    //open popup-menu again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //checkbox not active for value 2
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2", checked: false });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    await I.reopenDocument();

    //open popup-menu again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //checkbox not active for value 2
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2", checked: false });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49100] Autofilter - reapply the autofilter settings (ODS)", async ({ I,  spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_reapply.ods");

    //open popup-menu
    I.pressKeys("Ctrl+Home");
    I.clickToolbarTab("data");
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Check popup-menu
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1" });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "3" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });

    //change the cell value of A4
    await spreadsheet.selectCell("A4");
    spreadsheet.enterCellText("2", { leave: "stay" });

    //refresh
    I.clickButton("table/refresh");

    //open popup-menu again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //checkbox not active for value 2
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2", checked: false });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    await I.reopenDocument();

    //open popup-menu again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //checkbox not active for value 2
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2", checked: false });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    I.dontSeeElement({ spreadsheet: "row-header", pos: "3" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C15604] Autofilter - popup menu with number format list entries", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/auto_filter_format.xlsx");

    I.clickToolbarTab("data");

    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Check popup-menu
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "100%" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "2 €" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1.23e+08" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C311809] Autofilter - sorting with ascending Umlauts and eszett", async ({ I, spreadsheet }) => {

    // load document with german user language
    await I.loginAndOpenDocument("media/files/DOCS-2324-sorting.xlsx", { locale: "de_DE" });

    I.clickToolbarTab("data");

    //open the popup
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending", checked: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending", checked: true });

    //Close the popup
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    //check the values and the order
    await spreadsheet.selectCell("D3", { display: "WAHR", value: true, formula: "B3=C3" });
    await spreadsheet.selectCell("D4", { display: "WAHR", value: true, formula: "B4=C4" });
    await spreadsheet.selectCell("D5", { display: "WAHR", value: true, formula: "B5=C5" });
    await spreadsheet.selectCell("D6", { display: "WAHR", value: true, formula: "B6=C6" });
    await spreadsheet.selectCell("D7", { display: "WAHR", value: true, formula: "B7=C7" });
    await spreadsheet.selectCell("D8", { display: "WAHR", value: true, formula: "B8=C8" });
    await spreadsheet.selectCell("D9", { display: "WAHR", value: true, formula: "B9=C9" });
    await spreadsheet.selectCell("D10", { display: "WAHR", value: true, formula: "B10=C10" });
    await spreadsheet.selectCell("D11", { display: "WAHR", value: true, formula: "B11=C11" });
    await spreadsheet.selectCell("E3", { display: "WAHR", value: true, formula: '"ä"<"b"' });
    await spreadsheet.selectCell("E4", { display: "0", value: 0, formula: 'ZÄHLENWENN(B3:B11;">z")' });

    await I.reopenDocument();

    //check the values  and the order
    await spreadsheet.selectCell("D3", { display: "WAHR", value: true, formula: "B3=C3" });
    await spreadsheet.selectCell("D4", { display: "WAHR", value: true, formula: "B4=C4" });
    await spreadsheet.selectCell("D5", { display: "WAHR", value: true, formula: "B5=C5" });
    await spreadsheet.selectCell("D6", { display: "WAHR", value: true, formula: "B6=C6" });
    await spreadsheet.selectCell("D7", { display: "WAHR", value: true, formula: "B7=C7" });
    await spreadsheet.selectCell("D8", { display: "WAHR", value: true, formula: "B8=C8" });
    await spreadsheet.selectCell("D9", { display: "WAHR", value: true, formula: "B9=C9" });
    await spreadsheet.selectCell("D10", { display: "WAHR", value: true, formula: "B10=C10" });
    await spreadsheet.selectCell("D11", { display: "WAHR", value: true, formula: "B11=C11" });
    await spreadsheet.selectCell("E3", { display: "WAHR", value: true, formula: '"ä"<"b"' });
    await spreadsheet.selectCell("E4", { display: "0", value: 0, formula: 'ZÄHLENWENN(B3:B11;">z")' });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8857] Autofilter - set two autofilters (via cell range selection)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_data_2_columns.xlsx");

    await spreadsheet.selectRange("A1:B4");
    I.clickToolbarTab("data");
    I.clickButton("table/filter");

    await spreadsheet.selectCell("A1", { value: "Filter 1" });
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("B1", { value: "Filter 2" });
    await spreadsheet.selectCell("B2", { value: "Cell B2" });
    await spreadsheet.selectCell("B3", { value: "Cell B3" });
    await spreadsheet.selectCell("B4", { value: "Cell B4" });

    //Check popup-menu column a
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    //Check popup-menu column b
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "Filter 1" });
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("B1", { value: "Filter 2" });
    await spreadsheet.selectCell("B2", { value: "Cell B2" });
    await spreadsheet.selectCell("B3", { value: "Cell B3" });
    await spreadsheet.selectCell("B4", { value: "Cell B4" });

    //Check popup-menu column a
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    //Check popup-menu column b
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C49102] Autofilter - set two autofilters (via cell range selection) (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_data_2_columns.ods");

    await spreadsheet.selectRange("A1:B4");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");

    await spreadsheet.selectCell("A1", { value: "Filter1" });
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("B1", { value: "Filter2" });
    await spreadsheet.selectCell("B2", { value: "Cell B2" });
    await spreadsheet.selectCell("B3", { value: "Cell B3" });
    await spreadsheet.selectCell("B4", { value: "Cell B4" });

    //Check popup-menu column a
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    //Check popup-menu column b
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "Filter1" });
    await spreadsheet.selectCell("A2", { value: "Cell A2" });
    await spreadsheet.selectCell("A3", { value: "Cell A3" });
    await spreadsheet.selectCell("A4", { value: "Cell A4" });
    await spreadsheet.selectCell("B1", { value: "Filter2" });
    await spreadsheet.selectCell("B2", { value: "Cell B2" });
    await spreadsheet.selectCell("B3", { value: "Cell B3" });
    await spreadsheet.selectCell("B4", { value: "Cell B4" });

    //Check popup-menu column a
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell a4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    //Check popup-menu column b
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b2" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cell b4" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C328539] Autofilter - open a filter dropdown and click OK (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // prepare test data
    spreadsheet.enterCellText("Header");
    spreadsheet.enterCellText("1");

    await spreadsheet.selectRange("A1:A2");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //open the popup
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Check popup-menu column A
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1" });
    I.waitForAndClick({ docs: "popup", action: "ok" });

    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Check ascending/descending to data-checked='false'
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending", checked: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending", checked: false });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8865] Autofilter - toggle the autofilter", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autofilter_toggle.xlsx");

    //check the table "header"
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    await spreadsheet.selectCell("A1");
    I.clickToolbarTab("data");
    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //table header ist removed
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    I.clickButton("table/filter");
    I.waitForChangesSaved();

    //check the table "header"
    I.waitForElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8858] Autofilter - popup menu", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/autofilter_sample1.xlsx");

    //open the popup
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });

    //Check popup-menu column A
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending", checked: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending", checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "a" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d" });
    I.waitForVisible({ docs: "popup", action: "cancel" });
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();
    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-4478] Autofilter with date groups", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4478-datefilter.xlsx");

    // open autofilter popup menu, check imported entries
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true, checked: null });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000", checked: null, collapsed: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001", checked: true, collapsed: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "a", checked: true });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "a", collapseBtn: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "true", checked: true });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "true", collapseBtn: true });

    // the following elements are all collapsed
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1", withText: "January", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1 1", withText: "01", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1 2", withText: "02", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1 3", withText: "03", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2", withText: "February", checked: null });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 1", withText: "01", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2", withText: "02", checked: null });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0", withText: "00", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0 0", withText: "00", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0 0 0", withText: "00", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8", withText: "08", checked: null });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 0", withText: "00", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 0 0", withText: "00", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20", withText: "20", checked: null });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 0", withText: "00", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 20", withText: "20", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 40", withText: "40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 40", withText: "40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 40 0", withText: "40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16", withText: "16", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16 0", withText: "00", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16 0 0", withText: "00", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 3", withText: "03", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3", withText: "March", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3 1", withText: "01", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3 2", withText: "02", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3 3", withText: "03", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001 1", withText: "January", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001 1 1", withText: "01", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001 1 2", withText: "02", checked: true });

    // hide all entries for 2000-Feb
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000", collapseBtn: true });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2", checked: null });
    I.waitForAndClick({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2", checked: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 1", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 20", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 40 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 3", checked: false });

    // apply filter
    I.waitForAndClick({ docs: "popup", action: "ok" });
    I.waitForChangesSaved();

    // check hidden rows
    I.dontSeeElement({ spreadsheet: "row-header", pos: "5" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "6" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "7" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "8" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "9" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "10" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "11" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "12" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "13" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "14" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "15" });
    I.dontSeeElement({ spreadsheet: "row-header", pos: "16" });

    // reload document
    await I.reopenDocument();

    // check dropdown contents again
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1", withText: "January", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1 1", withText: "01", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1 2", withText: "02", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 1 3", withText: "03", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 1", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 0 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 20", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 20 40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 40", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 8 40 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 2 16 0 0", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 2 3", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3", withText: "March", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3 1", withText: "01", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3 2", withText: "02", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2000 3 3", withText: "03", checked: false });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001 1", withText: "January", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001 1 1", withText: "01", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "date:2001 1 2", withText: "02", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "a", checked: true });
    I.seeElementInDOM({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "true", checked: true });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
