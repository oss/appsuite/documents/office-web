/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Validity");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C223549] Can import and use list validation as range", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_as_range.xlsx");

    spreadsheet.waitForActiveCell("D2", { value: "b2" });

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b4" });

    //set b2 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "b2" });

    //active value
    spreadsheet.waitForActiveCell("D2", { value: "b2" });

    await I.reopenDocument();

    //check the values
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b4" });

    //active value
    spreadsheet.waitForActiveCell("D2", { value: "b2" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C223560] Can import and use list validation as range (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_as_range.ods");

    spreadsheet.waitForActiveCell("C2", { value: null });

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b4" });

    //set b2 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "b2" });

    //active value
    spreadsheet.waitForActiveCell("C2", { value: "b2" });

    await I.reopenDocument();

    //check the values
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b4" });

    //active value
    spreadsheet.waitForActiveCell("C2", { value: "b2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223556] Can import and use list validation as name", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_as_name.xlsx");

    spreadsheet.waitForActiveCell("D2", { value: "b9" });

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b8" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b9" });

    //set b2 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "b7" });

    //active value
    spreadsheet.waitForActiveCell("D2", { value: "b7" });

    await I.reopenDocument();

    //check the values
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b8" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b9" });

    //active value
    spreadsheet.waitForActiveCell("D2", { value: "b7" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223561] Can import and use list validation as name (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_as_name.ods");

    spreadsheet.waitForActiveCell("C2", { value: null });

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });

    //set b2 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "b3" });

    //active value
    spreadsheet.waitForActiveCell("C2", { value: "b3" });

    await I.reopenDocument();

    //check the values
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });

    //active value
    spreadsheet.waitForActiveCell("C2", { value: "b3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223557] Can import and use list validation as list", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_as_list.xlsx");

    spreadsheet.waitForActiveCell("D2",  { value: null });

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "a" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "c" });

    //set b2 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "b" });

    //active value
    spreadsheet.waitForActiveCell("D2", { value: "b" });

    await I.reopenDocument();

    //check the values
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "a" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "c" });

    //active value
    spreadsheet.waitForActiveCell("D2", { value: "b" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223562] Can import and use list validation as list (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_as_list.ods");

    spreadsheet.waitForActiveCell("C3",  { value: null });

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C3" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "a" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "c" });

    //set b2 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "b" });

    //active value
    spreadsheet.waitForActiveCell("C3", { value: "b" });

    await I.reopenDocument();

    //check the values
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "a" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "c" });

    //active value
    spreadsheet.waitForActiveCell("C3", { value: "b" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223563] Notification-Alert for list validation with no source data", async ({ I, spreadsheet, alert }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_no_source_data.xlsx");

    //open the popup-menu
    await spreadsheet.selectCell("D2", { value: null });
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    alert.waitForVisible("No source data available for the drop-down list.");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223553] Can insert columns between list validation", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_insert_column.xlsx");

    spreadsheet.waitForActiveCell("B1",  { value: null });

    //B1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //B1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("B2",  { value: null });

    //B2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B2" });

    //B2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");
    spreadsheet.waitForActiveCell("C1",  { value: null });

    //C1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C1" });

    //C1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    spreadsheet.waitForActiveCell("C1",  { value: null });
    I.pressKeys("Escape", "ArrowDown");

    //C2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //C2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "13" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "14" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "15" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "16" });

    I.pressKey("Escape");

    //insert a column
    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("C");
    I.clickButton("column/insert");
    I.waitForChangesSaved();

    I.pressKeys("ArrowUp", "ArrowLeft");

    spreadsheet.waitForActiveCell("B1",  { value: null });

    //B1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //B1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("B2",  { value: null });

    //B2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B2" });

    //B2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");
    spreadsheet.waitForActiveCell("C1",  { value: null });
    //hieer C1

    //C1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C1" });

    //C1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("C2",  { value: null });

    //C2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //C2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");

    spreadsheet.waitForActiveCell("D1",  { value: null });

    //D1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D1" });

    //D1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("D2",  { value: null });

    //D2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //D2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "13" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "14" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "15" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "16" });

    I.pressKey("Escape");

    await I.reopenDocument();

    I.pressKeys("ArrowUp", " 2*ArrowLeft");
    spreadsheet.waitForActiveCell("B1",  { value: null });

    //B1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //B1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("B2",  { value: null });

    //B2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B2" });

    //B2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");
    spreadsheet.waitForActiveCell("C1",  { value: null });

    //C1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C1" });

    //C1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("C2",  { value: null });

    //C2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //C2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");

    spreadsheet.waitForActiveCell("D1",  { value: null });

    //D1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D1" });

    //D1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("D2",  { value: null });

    //D2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //D2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "13" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "14" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "15" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "16" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C234522] Can insert columns between list validation (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_insert_column.ods");

    spreadsheet.waitForActiveCell("B1",  { value: null });

    //B1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //B1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("B2",  { value: null });

    //B2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B2" });

    //B2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");
    spreadsheet.waitForActiveCell("C1",  { value: null });

    //C1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C1" });

    //C1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    spreadsheet.waitForActiveCell("C1",  { value: null });
    I.pressKeys("Escape", "ArrowDown");

    //C2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //C2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "13" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "14" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "15" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "16" });

    I.pressKey("Escape");

    //insert a column
    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("C");
    I.clickButton("column/insert");
    I.waitForChangesSaved();

    I.pressKeys("ArrowUp", "ArrowLeft");

    spreadsheet.waitForActiveCell("B1",  { value: null });

    //B1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //B1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("B2",  { value: null });

    //B2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B2" });

    //B2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");
    spreadsheet.waitForActiveCell("C1",  { value: null });

    //C1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C1" });

    //C1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("C2",  { value: null });

    //C2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //C2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");

    spreadsheet.waitForActiveCell("D1",  { value: null });

    //D1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D1" });

    //D1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("D2",  { value: null });

    //D2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //D2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "13" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "14" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "15" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "16" });

    I.pressKey("Escape");

    await I.reopenDocument();

    I.pressKeys("ArrowUp", " 2*ArrowLeft");
    spreadsheet.waitForActiveCell("B1",  { value: null });

    //B1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B1" });

    //B1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("B2",  { value: null });

    //B2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "B2" });

    //B2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");
    spreadsheet.waitForActiveCell("C1",  { value: null });

    //C1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C1" });

    //C1 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("C2",  { value: null });

    //C2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //C2 - check the values again
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "9" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "10" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "11" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "12" });

    I.pressKeys("Escape", "ArrowRight", "ArrowUp");

    spreadsheet.waitForActiveCell("D1",  { value: null });

    //D1 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D1" });

    //D1 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    I.pressKeys("Escape", "ArrowDown");
    spreadsheet.waitForActiveCell("D2",  { value: null });

    //D2 - open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

    //D2 - check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "13" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "14" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "15" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "16" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223558] Can import and use list validation from other sheet", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_from_other_sheet.xlsx");
    await spreadsheet.selectCell("J2");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });

    //set 2d4 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2d4" });

    //active value
    spreadsheet.waitForActiveCell("J2", { value: "2d4" });

    await spreadsheet.selectCell("K2");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });

    //set 2e3 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2e3" });

    //active value
    spreadsheet.waitForActiveCell("K2", { value: "2e3" });

    await spreadsheet.selectCell("J4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d6" });

    //set 2d6 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2d6" });

    //active value
    spreadsheet.waitForActiveCell("J4", { value: "2d6" });

    await spreadsheet.selectCell("K4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e6" });

    //set 2e4 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2e4" });

    //active value
    spreadsheet.waitForActiveCell("K4", { value: "2e4" });

    await spreadsheet.selectCell("J2");

    await I.reopenDocument();

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });

    //active value
    spreadsheet.waitForActiveCell("J2", { value: "2d4" });

    await spreadsheet.selectCell("K2");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });

    //active value
    spreadsheet.waitForActiveCell("K2", { value: "2e3" });

    await spreadsheet.selectCell("J4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d6" });

    //active value
    spreadsheet.waitForActiveCell("J4", { value: "2d6" });

    await spreadsheet.selectCell("K4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e6" });

    //active value
    spreadsheet.waitForActiveCell("K4", { value: "2e4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223559] Can import and use list validation from other sheet (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_from_other_sheet.ods");
    await spreadsheet.selectCell("J2");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });

    //set 2d4 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2d4" });

    //active value
    spreadsheet.waitForActiveCell("J2", { value: "2d4" });

    await spreadsheet.selectCell("K2");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });

    //set 2e3 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2e3" });

    //active value
    spreadsheet.waitForActiveCell("K2", { value: "2e3" });

    await spreadsheet.selectCell("J4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d6" });

    //set 2d6 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2d6" });

    //active value
    spreadsheet.waitForActiveCell("J4", { value: "2d6" });

    await spreadsheet.selectCell("K4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e6" });

    //set 2e4 as value
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2e4" });

    //active value
    spreadsheet.waitForActiveCell("K4", { value: "2e4" });

    await spreadsheet.selectCell("J2");

    await I.reopenDocument();

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });

    //active value
    spreadsheet.waitForActiveCell("J2", { value: "2d4" });

    await spreadsheet.selectCell("K2");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });

    //active value
    spreadsheet.waitForActiveCell("K2", { value: "2e3" });

    await spreadsheet.selectCell("J4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d6" });

    //active value
    spreadsheet.waitForActiveCell("J4", { value: "2d6" });

    await spreadsheet.selectCell("K4");

    //open the popup-menu
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K4" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e6" });

    //active value
    spreadsheet.waitForActiveCell("K4", { value: "2e4" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C223550] Can use a complex list validation", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listvalidation_mixed.xlsx");

    //check the inital values
    await spreadsheet.selectCell("C2", { display: "1", value: 1, renderFill: "hsl(209,59%,84%)", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("B5", { display: "1",  value: 1, renderFill: "transparent", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "hsl(209,59%,84%)", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    //C2 - open the dropdown
    await spreadsheet.selectCell("C2");
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("B6", {  display: "2", value: 2, renderFill: "transparent", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("C6", { value: null, renderFill: "hsl(209,59%,84%)", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    //C6 - open the dropdown
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C6" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "red" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "orange" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "yellow" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "green" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "blue" });
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "orange" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("C6", { display: "orange", value: "orange", renderFill: "hsl(209,59%,84%)", renderColor: "#000000" });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ffc000", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("C2", { display: "2", value: 2, renderFill: "hsl(209,59%,84%)", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "hsl(209,59%,84%)", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("B5", { display: "1",  value: 1, renderFill: "transparent", renderColor: "#000000",  renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("B6", { display: "2",  value: 2, renderFill: "transparent", renderColor: "#000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("C6", { display: "orange", value: "orange", renderFill: "hsl(209,59%,84%)", renderColor: "#000000",  renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ffc000", renderColor: "#000000",  renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C237242] Can use a complex list validation (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/listval_condition.ods");

    //check the inital values
    await spreadsheet.selectCell("C2", { value: null });
    await spreadsheet.selectCell("B5", { display: "1",  value: 1, renderFill: "transparent", renderColor: "#ffffff" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "transparent", renderColor: "#000000" });

    //C2 - open the dropdown
    await spreadsheet.selectCell("C2");
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C2" });

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "1" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "3" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "4" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "5" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "6" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "7" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "8" });

    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "2" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("B5", { value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("B6", { value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "#66ffff", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: null, renderFill: "#66ffff", renderColor: "#000000" });

    //C6 - open the dropdown
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "C6" });
    I.waitForChangesSaved();

    //check the values
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "red" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "orange" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "yellow" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "green" });
    I.waitForElement({ docs: "button",  inside: "popup-menu", value: "blue" });
    I.waitForAndClick({ docs: "button",  inside: "popup-menu", value: "orange" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("C6", { display: "orange", value: "orange", renderFill: "#66ffff", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ff9900", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("C2", { display: "2", value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("B5", { display: "1",  value: 1, renderFill: "transparent", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });
    await spreadsheet.selectCell("B6", { display: "2",  value: 2, renderFill: "transparent", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "#66ffff", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });
    await spreadsheet.selectCell("C6", { display: "orange", value: "orange", renderFill: "#66ffff", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ff9900", renderColor: "#000000", renderBorderBottom: "1px hair rgba(0,0,0,0.4)", renderBorderTop: "1px hair rgba(0,0,0,0.4)", renderBorderLeft: "1px hair rgba(0,0,0,0.4)", renderBorderRight: "1px hair rgba(0,0,0,0.4)" });

    I.closeDocument();
}).tag("stable");
