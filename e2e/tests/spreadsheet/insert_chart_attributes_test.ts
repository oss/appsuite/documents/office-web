/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Chart > Set Chart Attributes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8196] [C8197] Insert column chart and change formatting", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    // change chart formatting
    I.clickOptionButton("drawing/chartcolorset", "cs7");
    I.clickOptionButton("drawing/chartstyleset", "ss5");
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs7" });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss5" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs7" });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss5" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8198] Insert column chart and switch data source orientation", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1:D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:D2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B3:D3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B4:D4" });

    // switch data source orientation
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.clickButton("drawing/chartexchange", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D2:D4" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D2:D4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8199] Insert column chart and edit data references", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForChangesSaved();

    // back to sheet, add more data
    await spreadsheet.selectCell("A5");
    spreadsheet.enterCellText("Row4", { leave: "right" });
    spreadsheet.enterCellText("33", { leave: "right" });
    spreadsheet.enterCellText("-12", { leave: "right" });
    spreadsheet.enterCellText("63", { leave: "right" });
    I.waitForChangesSaved();

    // select the chart again, start data source selection mode
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.clickButton("drawing/chartsource", { inside: "popup-menu" });

    // drag mouse from A1 to D5
    await spreadsheet.dragRange("A1:D5");
    I.waitForChangesSaved();

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1:D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:D2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B3:D3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B4:D4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B5:D5" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1:D1" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:D2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B3:D3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B4:D4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B5:D5" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173429] [C173430] Insert column chart with first row/column as label", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    await spreadsheet.selectRange("B2:C4");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForChangesSaved();

    // check the position of the source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C4" });

    // enable "first row" option
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.clickButton("drawing/chartfirstrow", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B3:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C3:C4" });

    // enable "first column" option
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.clickButton("drawing/chartfirstcol", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B3:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C3:C4" });
    I.dontSeeElementInDOM({ spreadsheet: "highlight-range", range: "B2" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstrow", state: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstcol", state: true });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B3:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C3:C4" });
    I.dontSeeElementInDOM({ spreadsheet: "highlight-range", range: "B2" });

    // disable "first row" option
    I.clickButton("drawing/chartfirstrow", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B4" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8200] [C8201] [C8202] [C8203] [C8204] [C8205] Insert column chart with titles and label settings", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForChangesSaved();

    // show the "Labels&Axes" floating menu, check initial states
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/chartlabels", { state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: true });

    // change the settings of the chart
    I.fillTextField("drawing/chart/title/text", "New chart title", { inside: "floating-menu" });
    I.fillTextField("drawing/chart/axis/x/title/text", "New X axis title", { inside: "floating-menu" });
    I.fillTextField("drawing/chart/axis/y/title/text", "New Y axis title", { inside: "floating-menu" });
    I.clickButton("drawing/chart/axis/x/labels/visible", { inside: "floating-menu" });
    I.clickButton("drawing/chart/axis/y/labels/visible", { inside: "floating-menu" });
    I.clickButton("drawing/chart/axis/x/line/visible", { inside: "floating-menu" });
    I.clickButton("drawing/chart/axis/y/line/visible", { inside: "floating-menu" });
    I.clickButton("drawing/chart/axis/x/grid/visible", { inside: "floating-menu" });
    I.clickButton("drawing/chart/axis/y/grid/visible", { inside: "floating-menu" });
    I.waitForChangesSaved();

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/chartlabels", { state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: "New chart title" });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: "New X axis title" });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: "New Y axis title" });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: false });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8206] Insert column chart with data labels", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForChangesSaved();

    // enable data point labels
    I.waitForToolbarTab("drawing");
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.clickButton("drawing/chartdatalabel", { inside: "popup-menu", state: false });
    I.waitForChangesSaved();

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartdatalabel", state: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8207] Insert column chart and change legend position", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForChangesSaved();

    // change legend position
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });
    I.clickOptionButton("drawing/chartlegend/pos", "right");
    I.waitForChangesSaved();

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "right" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8208] Insert pie chart and remove varying color by point", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "pie standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForChangesSaved();

    // change "vary colors" option
    I.waitForToolbarTab("drawing");
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.clickButton("drawing/chartvarycolor", { inside: "popup-menu", state: true });
    I.waitForChangesSaved();

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartvarycolor", state: false });

    I.closeDocument();
}).tag("stable");
