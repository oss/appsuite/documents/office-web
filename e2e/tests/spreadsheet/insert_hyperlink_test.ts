/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Hyperlink");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8186] Insert Hyperlink", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("insert");

    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", withText: "Hyperlink" });

    dialogs.waitForVisible();

    //dialog
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, "hello");
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "www.heise.de");


    dialogs.clickOkButton();

    I.clickToolbarTab("format");

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C38800] Insert Hyperlink (ODS)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.clickToolbarTab("insert");

    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", withText: "Hyperlink" });

    dialogs.waitForVisible();

    //dialog
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, "hello");
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "www.heise.de");


    dialogs.clickOkButton();

    I.clickToolbarTab("format");

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink res 0000FF" });

    await I.reopenDocument();

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "rgb 0000FF" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85908] Insert Hyperlink via autocomplete", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("http://www.heise.de", { leave: "stay" });

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38802] Insert Hyperlink via autocomplete (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("http://www.heise.de", { leave: "stay" });

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink res 0000FF" });

    await I.reopenDocument();

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "rgb 0000FF" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8189] Insert hyperlink with invalid value", ({ I, dialogs, alert }) => {

    // eslint-disable-next-line no-script-url
    const invalidURL = "javascript:alert(1)";

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("insert");

    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", withText: "Hyperlink" });

    dialogs.waitForVisible();

    //dialog
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, invalidURL);

    dialogs.clickOkButton();

    // an info message appears
    alert.waitForVisible("This hyperlink is invalid.");
    alert.clickCloseButton();

    dialogs.clickCancelButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8187] Edit inserted hyperlink", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/hyperlink.xlsx");

    await spreadsheet.openCellContextMenu("C1");
    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", inside: "context-menu", withText: "Edit hyperlink" });
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(1) > input" });
    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(2) > input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=ok]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=cancel]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=remove]" });

    I.click({ docs: "dialog", find: ".form-group:nth-child(2) > input" });
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "new text");

    dialogs.clickOkButton();

    spreadsheet.waitForActiveCell("C1", { display: "new text" });

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("C1", { display: "new text" });

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[DOCS-4505] Edit hyperlink with location fragment (XLSX)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4505-hyperlink-location.xlsx");

    await spreadsheet.selectCell("B2", { display: "https://example.org/?query=42#location" });

    await spreadsheet.openCellContextMenu("B2");
    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", inside: "context-menu", withText: "Edit hyperlink" });
    dialogs.waitForVisible();
    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(1) > input[value='https://example.org/?query=42#location']" });
    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(2) > input[value='https://example.org/?query=42#location']" });
    I.click({ docs: "dialog", find: ".form-group:nth-child(1) > input" });
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, "https://example.org?query=42#newLocation");
    I.click({ docs: "dialog", find: ".form-group:nth-child(2) > input" });
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "new text");
    dialogs.clickOkButton();
    spreadsheet.waitForActiveCell("B2", { display: "new text" });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("B2", { display: "new text" });
    await spreadsheet.openCellContextMenu("B2");
    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", inside: "context-menu", withText: "Edit hyperlink" });
    dialogs.waitForVisible();
    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(1) > input[value='https://example.org?query=42#newLocation']" });
    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(2) > input[value='new text']" });
    dialogs.clickOkButton();
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C237232] Edit inserted hyperlink (ODS)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/hyperlink.ods");

    I.pressKeys("3*ArrowUp");

    await spreadsheet.openCellContextMenu("C2");
    I.waitForAndClick({ docs: "control", key: "hyperlink/edit/dialog", inside: "context-menu", withText: "Edit hyperlink" });
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(1) > input" });
    I.waitForVisible({ docs: "dialog", find: ".form-group:nth-child(2) > input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=ok]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=cancel]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=remove]" });

    I.click({ docs: "dialog", find: ".form-group:nth-child(2) > input" });
    I.fillField({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "new text");
    dialogs.clickOkButton();

    spreadsheet.waitForActiveCell("C2", { display: "new text", renderColor: "#0000ff" });

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("C2", { display: "new text", renderColor: "#0000ff" });

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
