/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo drawings");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8293] Undo chart (bubble)", async ({ I }) => {

    await  I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.dontSeeElement({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    await I.reopenDocument();

    I.dontSeeElement({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8294] Redo chart (bubble)", async ({ I }) => {

    await  I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.dontSeeElement({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    //redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //check the labels
    I.clickButton("drawing/chartlabels", { state: false });

    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: true });
    I.pressKey("Escape");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    //color set
    I.clickButton("drawing/chartcolorset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });

    //style set
    I.clickButton("drawing/chartstyleset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    //check data points
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartdatalabel", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartvarycolor", state: false });

    //legend position
    I.clickButton("drawing/chartlegend/pos", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });

    //check data source
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartsource" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartexchange" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstrow", state: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstcol", state: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8292] Undo image", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/DSC_0401.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("spreadsheet");

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    //dialog
    dialogs.waitForVisible();

    //folder
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    //Image name
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // check the image
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // check the image
    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    await I.reopenDocument();

    // check the image
    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8291] Redo image", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/DSC_0401.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("spreadsheet");

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    //dialog
    dialogs.waitForVisible();

    //folder
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    //Image name
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // check the image
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // check the image
    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    //redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    // check the image
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    await I.reopenDocument();

    // check the image
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173439] Undo chart data (Column)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/data_for_column_chart.xlsx");

    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    //check the chart range
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A1:A3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1:B3" });

    spreadsheet.selectColumn("A");

    //context menu
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "A" });
    I.waitForContextMenuVisible();

    //delete col A
    I.clickButton("column/delete", { inside: "context-menu" });
    I.waitForChangesSaved();

    I.pressKeys("Ctrl+Multiply");
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A1:A3" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.pressKeys("Ctrl+Multiply");

    //check the  chart range
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A1:A3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1:B3" });

    await I.reopenDocument();

    //check the  chart range
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A1:A3" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B1:B3" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C162990] Undo chart (bubble) (ODS)", async ({ I }) => {

    await  I.loginAndOpenDocument("media/files/data_for_bubble.ods");

    I.pressKeys("Ctrl+Multiply");

    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.dontSeeElement({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    await I.reopenDocument();

    I.dontSeeElement({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    I.closeDocument();
}).tag("stable");
