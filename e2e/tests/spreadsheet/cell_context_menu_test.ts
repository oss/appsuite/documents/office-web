/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Context menus > Cells");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("Cell context menu", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open the context menu
    await spreadsheet.openCellContextMenu("A1");

    // cut, copy, paste
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "document/cut", withText: "Cut" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "document/copy", withText: "Copy" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "document/paste", withText: "Paste" });

    // hyperlink controls not visible
    I.dontSeeElement({ docs: "control", inside: "context-menu", key: "hyperlink/edit/dialog" });
    I.dontSeeElement({ docs: "control", inside: "context-menu", key: "hyperlink/delete" });
    I.dontSeeElement({ docs: "button", inside: "context-menu", withText: "Open hyperlink" });

    // row, column, insert submenus
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "view/row/submenu", withText: "Row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "view/column/submenu", withText: "Column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "view/insert/submenu", withText: "Insert" });

    // filter, sort
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "table/filter", withText: "Filter" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "table/sort/dialog", withText: "Sort" });

    // cell protection submenu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "view/protect/submenu", withText: "Cell protection" });

    // clear contents, freeze sheet
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "cell/clear/values", withText: "Clear contents" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "view/split/frozen", withText: "Freeze sheet" });
    I.dontSeeElement({ docs: "control", inside: "context-menu", key: "cell/merge" });

    // "Row" popup menu
    I.clickButton("view/row/submenu", { inside: "context-menu", caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "row/insert", withText: "Insert row" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "row/delete", withText: "Delete row" });
    I.pressKey("Escape");

    // "Column" popup menu
    I.clickButton("view/column/submenu", { inside: "context-menu", caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "column/insert", withText: "Insert column" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "column/delete", withText: "Delete column" });
    I.pressKey("Escape");

    // "Insert" popup menu
    I.clickButton("view/insert/submenu", { inside: "context-menu", caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/autoformula", withText: "Sum" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "function/insert/dialog", withText: "Function" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "hyperlink/edit/dialog", withText: "Hyperlink" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "name/insert/dialog", withText: "Named range" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "image/insert/dialog", withText: "Image" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "threadedcomment/insert", withText: "Comment" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "hyperlink/edit/dialog", withText: "Hyperlink" });
    I.pressKey("Escape");

    // "Protection" popup menu
    I.clickButton("view/protect/submenu", { inside: "context-menu", caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/unlocked", withText: "Unlock cell" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/hideformula", withText: "Hide formula" });
    I.pressKey("Escape");

    // close context menu
    I.pressKey("Escape");
    I.waitForContextMenuInvisible();

    // additions for hyperlinks
    spreadsheet.enterCellText("http://www.example.org/");
    await spreadsheet.openCellContextMenu("A1");

    // edit, delete, open
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "hyperlink/edit/dialog", withText: "Edit hyperlink" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "hyperlink/delete", withText: "Remove hyperlink" });
    I.waitForVisible({ docs: "button", inside: "context-menu", withText: "Open hyperlink" });

    // close context menu
    I.pressKey("Escape");
    I.waitForContextMenuInvisible();

    // additions for cell ranges
    await spreadsheet.selectRange("A1:B2");
    await spreadsheet.openCellContextMenu("A1");
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "cell/merge", withText: "Merge cells" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C38851] Insert hyperlink", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open the hyperlink dialog via context menu
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("view/insert/submenu", { inside: "context-menu" });
    I.clickButton("hyperlink/edit/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    I.fillFieldWithText({ docs: "dialog", find: ".form-group:first-child > input" }, "hello");
    I.fillFieldWithText({ docs: "dialog", find: ".form-group:last-child > input" }, "www.heise.de");

    dialogs.clickOkButton();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38861] Delete hyperlink", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("www.heise.de", { leave: "stay" });

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    // delete the hyperlink dialog via context menu
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("hyperlink/delete", { inside: "context-menu", withText: "Remove hyperlink" });

    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.dontSeeElement({ docs: "control", key: "character/color", state: "sch hyperlink" });

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.dontSeeElement({ docs: "control", key: "character/color", state: "sch hyperlink" });
    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38854] Clear content", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("hello", { leave: "stay" });

    I.clickButton("character/bold");
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });

    // clear contents via context menu
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("cell/clear/values", { inside: "context-menu" });

    spreadsheet.waitForActiveCell("A1", { value: null });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { value: null });

    I.waitForVisible({ docs: "control", key: "character/bold", state: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38852] Insert Chart", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    //select the cell range
    I.pressKeys("Ctrl+Multiply");

    // insert chart via context menu
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("view/insert/submenu", { inside: "context-menu" });
    I.clickButton("chart/insert", { inside: "popup-menu" });

    //check chart type
    I.waitForToolbarTab("drawing", { withText: "Chart" });

    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38853] Sort (outside a table)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting.xlsx");

    //cell values
    await spreadsheet.selectCell("B2", { value: 2 });
    await spreadsheet.selectCell("B3", { value: 1 });
    await spreadsheet.selectCell("B4", { value: 7 });
    await spreadsheet.selectCell("B5", { value: 5 });
    await spreadsheet.selectCell("B6", { value: 4 });

    //select the cell range
    I.pressKeys("Ctrl+Multiply");

    // open sort dialog via context menu
    await spreadsheet.openCellContextMenu("B2");
    I.clickButton("table/sort/dialog", { inside: "context-menu" });

    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-custom-sort-dialog", find: ".has-headers", withText: "Selection has headers" });

    I.waitForAndClick({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();

    I.waitForChangesSaved();

    //check sorting values
    await spreadsheet.selectCell("B2", { value: 7 });
    await spreadsheet.selectCell("B3", { value: 5 });
    await spreadsheet.selectCell("B4", { value: 4 });
    await spreadsheet.selectCell("B5", { value: 2 });
    await spreadsheet.selectCell("B6", { value: 1 });

    //check sorting values
    await I.reopenDocument();

    await spreadsheet.selectCell("B2", { value: 7 });
    await spreadsheet.selectCell("B3", { value: 5 });
    await spreadsheet.selectCell("B4", { value: 4 });
    await spreadsheet.selectCell("B5", { value: 2 });
    await spreadsheet.selectCell("B6", { value: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114849] Sort (inside a table)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sort_inside_a_table.xlsx", { expectToolbarTab: "table" });

    //check the table values
    await spreadsheet.selectCell("A4", { value: 1 });
    await spreadsheet.selectCell("A5", { value: 2 });
    await spreadsheet.selectCell("A6", { value: 3 });
    await spreadsheet.selectCell("B4", { value: 4 });
    await spreadsheet.selectCell("B5", { value: 5 });
    await spreadsheet.selectCell("B6", { value: 6 });
    await spreadsheet.selectCell("C4", { value: 7 });
    await spreadsheet.selectCell("C5", { value: 8 });
    await spreadsheet.selectCell("C6", { value: 9 });
    I.pressKeys("Ctrl+Multiply");

    // open sort dialog via context menu
    await spreadsheet.openCellContextMenu("A4");
    //Context filter not visible
    I.dontSeeElement({ docs: "control", inside: "context-menu", key: "table/filter", withText: "Filter" });
    I.clickButton("table/sort/dialog", { inside: "context-menu" });

    dialogs.waitForVisible();
    I.dontSeeElement({ docs: "dialog", id: "io-ox-office-spreadsheet-custom-sort-dialog", find: ".has-headers" });

    I.waitForAndClick({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });
    dialogs.clickOkButton();

    I.waitForChangesSaved();

    //check the table values
    await spreadsheet.selectCell("A4", { value: 3 });
    await spreadsheet.selectCell("A5", { value: 2 });
    await spreadsheet.selectCell("A6", { value: 1 });
    await spreadsheet.selectCell("B4", { value: 6 });
    await spreadsheet.selectCell("B5", { value: 5 });
    await spreadsheet.selectCell("B6", { value: 4 });
    await spreadsheet.selectCell("C4", { value: 9 });
    await spreadsheet.selectCell("C5", { value: 8 });
    await spreadsheet.selectCell("C6", { value: 7 });

    await I.reopenDocument({ expectToolbarTab: "table" });

    //check the table values
    await spreadsheet.selectCell("A4", { value: 3 });
    await spreadsheet.selectCell("A5", { value: 2 });
    await spreadsheet.selectCell("A6", { value: 1 });
    await spreadsheet.selectCell("B4", { value: 6 });
    await spreadsheet.selectCell("B5", { value: 5 });
    await spreadsheet.selectCell("B6", { value: 4 });
    await spreadsheet.selectCell("C4", { value: 9 });
    await spreadsheet.selectCell("C5", { value: 8 });
    await spreadsheet.selectCell("C6", { value: 7 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38855] Merge cells", async ({ I, spreadsheet }) => {

    //test is not stable. I need a better data-key

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    I.pressKeys("Shift+ArrowRight");

    // merge cells via context menu
    await spreadsheet.openCellContextMenu("B2");
    I.clickButton("cell/merge", { inside: "context-menu", withText: "Merge cells" });
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("B2", { merged: "B2:C2" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("B2", { merged: "B2:C2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38850] Insert function", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open dialog via context menu
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("view/insert/submenu", { inside: "context-menu", caret: true });
    I.clickButton("function/insert/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    I.type("sum");
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    spreadsheet.waitForCellEditMode("=SUM()");
    I.type("1,3");
    spreadsheet.waitForCellEditMode("=SUM(1,3)");

    I.pressKey("Enter");
    I.waitForChangesSaved();

    await spreadsheet.selectCell("A1", { value: 4, formula: "SUM(1,3)" });

    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: 4, formula: "SUM(1,3)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114851] Hide formula", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("=sum(1,2)");
    I.pressKey("ArrowUp");

    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "SUM(1,2)");

    // hide formula via cell context menu
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("view/protect/submenu", { inside: "context-menu", caret: true });
    I.clickButton("cell/hideformula", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // lock sheet via context menu
    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });
    I.clickButton("sheet/locked", { withText: "Protect sheet", inside: "context-menu" });
    I.waitForChangesSaved();

    //ckeck the hide formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");

    await I.reopenDocument();

    //ckeck the hide formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114850] Unlock cell", async ({ I, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("unlockCell", { leave: "stay" });

    // unlock cell via context menu
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("view/protect/submenu", { inside: "context-menu", caret: true });
    I.clickButton("cell/unlocked", { inside: "popup-menu" });
    I.waitForChangesSaved();

    // lock sheet via context menu
    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });
    I.clickButton("sheet/locked", { withText: "Protect sheet", inside: "context-menu" });
    I.waitForChangesSaved();

    //change the unlocked cell value
    spreadsheet.enterCellText("unlockCellWorks");

    //check the locked cells
    I.pressKeys("ArrowDown", "L");
    alert.waitForVisible("Protected cells cannot be modified.");

    await I.reopenDocument();

    I.pressKeys("Ctrl+Home");

    //change the unlocked cell value
    spreadsheet.enterCellText("unlockCellWorksAgain", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "unlockCellWorksAgain" });

    //check the locked cells
    I.pressKeys("ArrowDown", "L");
    alert.waitForVisible("Protected cells cannot be modified.");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38848] Insert row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //Set a color attribute, type text
    I.clickOptionButton("cell/fillcolor", "red");
    spreadsheet.enterCellText("A1CellText", { leave: "stay" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "red" });

    //Set a color attribute, type text
    I.pressKey("ArrowDown");
    I.clickOptionButton("cell/fillcolor", "yellow");
    spreadsheet.enterCellText("A2CellText", { leave: "stay" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });

    // insert row via context menu
    await spreadsheet.openCellContextMenu("A2");
    I.clickButton("view/row/submenu", { inside: "context-menu", caret: true });
    I.clickButton("row/insert", { inside: "popup-menu" });
    I.waitForChangesSaved();

    //Check color attributes
    await spreadsheet.selectCell("A1", { value: "A1CellText" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "red" });
    await spreadsheet.selectCell("A2", { value: null });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "red" });
    await spreadsheet.selectCell("A3", { value: "A2CellText" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });

    await I.reopenDocument();

    //Check color attributes
    await spreadsheet.selectCell("A1", { value: "A1CellText" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "red" });
    await spreadsheet.selectCell("A2", { value: null });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "red" });
    await spreadsheet.selectCell("A3", { value: "A2CellText" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38849] Delete column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //Set a color attribute, type text
    I.clickOptionButton("cell/fillcolor", "red");
    spreadsheet.enterCellText("A1CellText", { leave: "stay" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "red" });

    //Set a color attribute, type text
    I.pressKey("ArrowRight");
    I.clickOptionButton("cell/fillcolor", "yellow");
    spreadsheet.enterCellText("B1CellText", { leave: "stay" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });

    // delete column via context menu
    await spreadsheet.openCellContextMenu("A1");
    I.clickButton("view/column/submenu", { inside: "context-menu", caret: true });
    I.clickButton("column/delete", { inside: "popup-menu" });
    I.waitForChangesSaved();

    //Check color attributes
    await spreadsheet.selectCell("A1", { value: "B1CellText" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });
    await spreadsheet.selectCell("B1", { value: null });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "auto" });

    await I.reopenDocument();

    //Check color attributes
    await spreadsheet.selectCell("A1", { value: "B1CellText" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });
    await spreadsheet.selectCell("B1", { value: null });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173498] Delete comment", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/delete_comment.xlsx");

    //initials values
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B3", hint: true });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    // delete comment via context menu
    await spreadsheet.openCellContextMenu("B3");
    I.clickButton("view/comment/submenu", { inside: "context-menu", caret: true, withText: "Comment" });
    I.clickButton("threadedcomment/delete/thread", { withText: "Delete", inside: "popup-menu" });
    I.waitForChangesSaved();

    await I.reopenDocument();

    I.dontSeeElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay", address: "B3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C237233] Show / Hide comment (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_comments.ods");

    //initials values
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    // toggle comment via context menu
    await spreadsheet.openCellContextMenu("B2");
    I.clickButton("view/note/submenu", { inside: "context-menu", caret: true, withText: "Comment" });
    I.clickButton("note/visible", { withText: "Show/Hide", inside: "popup-menu" });
    I.waitForChangesSaved();

    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    ///I.waitForVisible({ spreadsheet: "note", para: 1, pos: 3, withText: "B2" }); TODO

    await I.reopenDocument();

    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.closeDocument();
}).tag("stable");
