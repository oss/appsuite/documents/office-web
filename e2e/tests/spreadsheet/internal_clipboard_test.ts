/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Clipboard > Internal");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8300] Copy&Paste text with bold attribute in a cell", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // prepare cell A1 with bold text
    spreadsheet.enterCellText("Bold", { leave: "stay" });
    I.clickButton("character/bold");
    I.waitForChangesSaved();

    // copy the cell to clipboars
    I.copy();

    // select blank/unformatted cell A2
    await spreadsheet.selectCell("A2", { value: null });
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });

    // paste from clipboard
    I.paste();
    spreadsheet.waitForActiveCell("A2", { value: "Bold" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "Bold" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    await spreadsheet.selectCell("A2", { value: "Bold" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8301] Copy&Paste text with multiple attributes in a cell", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/mixed.xlsx");

    //check the attributes
    await spreadsheet.selectCell("A1", { value: "Dummytext", renderFill: "#ffff00", renderFontStyle: "italic", renderColor: "#ff0000" });

    I.copy();

    await spreadsheet.selectCell("A2");
    I.paste();
    spreadsheet.waitForActiveCell("A2", { value: "Dummytext", renderFill: "#ffff00", renderFontStyle: "italic", renderColor: "#ff0000" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "Dummytext", renderFill: "#ffff00", renderFontStyle: "italic", renderColor: "#ff0000" });
    await spreadsheet.selectCell("A2", { value: "Dummytext", renderFill: "#ffff00", renderFontStyle: "italic", renderColor: "#ff0000" });

    I.closeDocument();
}).tag("stable");
// ----------------------------------------------------------------------------

Scenario("[C8302] Copy&Paste a hyperlink in a cell", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/hyperlink.xlsx");

    await spreadsheet.selectCell("C1");

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.copy();

    await spreadsheet.selectCell("A3");

    I.paste();

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    await spreadsheet.selectCell("C1");

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await spreadsheet.selectCell("A3");

    //hyperlink attribute
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8303] Copy&Paste a cell border in a cell", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/borders.xlsx");

    await spreadsheet.selectCell("A3");

    //Border attribute
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "single:thick" });

    I.copy();

    await spreadsheet.selectCell("A1");

    I.paste();

    //Border attribute
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "single:thick" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A3");

    //Border attribute
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "single:thick" });

    await spreadsheet.selectCell("A1");
    //Border attribute
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "single:thick" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8304] Copy&Paste a formula in a cell", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/formula.xlsx");

    await spreadsheet.selectCell("A3", { display: "30", formula: "SUM($A$1,$A$2)" });

    I.copy();

    //go to cell A4
    await spreadsheet.selectCell("A4");
    I.paste();
    spreadsheet.waitForActiveCell("A4", { display: "30", formula: "SUM($A$1,$A$2)" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A3", { display: "30", formula: "SUM($A$1,$A$2)" });
    await spreadsheet.selectCell("A4", { display: "30", formula: "SUM($A$1,$A$2)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173452] Copy&Paste a note", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/copy_paste_note.xlsx");

    //check the values
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B2", hint: true });

    I.copy();

    //go to cell B4
    await spreadsheet.selectCell("B4");
    I.paste();

    //check the values B4
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });

    await I.reopenDocument();

    //check the values
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290147] Copy&Paste a comment", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/copy_paste_comment.xlsx");

    //go to cell C2
    await spreadsheet.selectCell("C2");

    //check the values  C2
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show" });

    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "copy me" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);

    I.copy();

    //go to cell B4
    await spreadsheet.selectCell("B4");
    I.paste();

    //check the values vor B4
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "copy me" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".position" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);

    await I.reopenDocument();

    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "copy me" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".position" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "copy me" });

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173453] ODS: Copy&Paste a comment", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/copy_paste_comments.ods");

    //go to cell A1
    await spreadsheet.selectCell("A1");

    I.clickToolbarTab("comments");

    //check the values A1
    I.seeElementInDOM({ spreadsheet: "note-overlay" });
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);

    I.copy();

    //go to cell A2
    await spreadsheet.selectCell("A2");
    I.paste();

    //check the values A2
    I.seeElementInDOM({ spreadsheet: "note-overlay" });
    I.dontSeeElement({ spreadsheet: "note", pos: 1 });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);

    await I.reopenDocument();

    I.seeElementInDOM({ spreadsheet: "note-overlay" });
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    I.seeElementInDOM({ spreadsheet: "note-overlay" });
    I.dontSeeElement({ spreadsheet: "note", pos: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85966] Copy&Paste a simple cf rule - unique", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/copy_paste_unique_rule.xlsx");

    await spreadsheet.selectRange("C3:C4");
    I.copy();

    await spreadsheet.selectCell("G3");
    I.paste();

    // check the attributes
    spreadsheet.waitForActiveCell("G3", { display: "12", renderFill: "transparent", renderColor: "#000000", renderBorderTop: "none", renderBorderBottom: "none", renderBorderLeft: "none", renderBorderRight: "none" });
    await spreadsheet.selectCell("G4", { display: "12", renderFill: "transparent", renderColor: "#000000", renderBorderTop: "none", renderBorderBottom: "none", renderBorderLeft: "none", renderBorderRight: "none" });

    // change cell G3 to value 13
    await spreadsheet.selectCell("G3");
    spreadsheet.enterCellText("13", { leave: "stay" });
    spreadsheet.waitForActiveCell("G3", { display: "13", renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("G4", { display: "12", renderFill: "transparent", renderColor: "#000000", renderBorderTop: "none", renderBorderBottom: "none", renderBorderLeft: "none", renderBorderRight: "none" });

    // change cell G4 to value 14
    spreadsheet.enterCellText("14", { leave: "stay" });
    spreadsheet.waitForActiveCell("G4", { display: "14", renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    await I.reopenDocument();

    //check the attributes
    await spreadsheet.selectCell("G3", { display: "13", renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    await spreadsheet.selectCell("G4", { display: "14", renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C162987] Copy&Paste an image", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/copy_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.copy();
    I.pressKey("Escape");

    await spreadsheet.selectCell("A25");
    I.paste();

    //Check the second image
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image" });

    await I.reopenDocument();

    //Check the second image
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C125298] Copy&Paste text in a shape", async ({ I }) => {

    // open local html page
    I.openLocalFile("media/files/for_copy_paste_text_shape.txt");
    // eslint-disable-next-line @typescript-eslint/no-deprecated
    I.executeScript(() => document.execCommand("selectall"));

    I.copy();
    I.wait(1);

    await I.loginAndOpenDocument("media/files/shape.xlsx");

    I.click({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    I.paste();

    //check the pasted text
    I.waitForText("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });

    await I.reopenDocument();

    I.click({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    //check the pasted text
    I.waitForText("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C338847] Copy&Paste a chart", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/copy_chart_line.xlsx");

    spreadsheet.activateSheet(0);
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "3dataSeries", value: 0 });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });

    I.copy();

    spreadsheet.insertSheet();

    I.paste();
    I.wait(1);

    spreadsheet.activateSheet(1);
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });

    //Check the chart attributes sheet1
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "line standard" });

    // show the "Labels&Axes" floating menu, check initial states
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/chartlabels", { state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: false });

    //close the floating menu
    I.waitForVisible({ css: ".popup-header > button[data-action='close']" });
    I.waitForAndClick({ css: ".popup-header > button[data-action='close']" });

    //color set
    I.clickButton("drawing/chartcolorset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });

    //style set
    I.clickButton("drawing/chartstyleset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    //check data points
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartdatalabel", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartvarycolor", state: false });

    //legend position
    I.clickButton("drawing/chartlegend/pos", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });

    //data source
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstrow", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstcol", state: false });

    await I.reopenDocument();

    spreadsheet.activateSheet(1);
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });

    //Check the chart attributes
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "line standard" });

    // show the "Labels&Axes" floating menu, check initial states
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/chartlabels", { state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: false });

    //close the floating menu
    I.waitForVisible({ css: ".popup-header > button[data-action='close']" });
    I.waitForAndClick({ css: ".popup-header > button[data-action='close']" });

    //color set
    I.clickButton("drawing/chartcolorset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });

    //style set
    I.clickButton("drawing/chartstyleset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    //check data points
    I.clickButton("view/chart/datapoints/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartdatalabel", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartvarycolor", state: false });

    //legend position
    I.clickButton("drawing/chartlegend/pos", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });

    //data source
    I.clickButton("drawing/chartdatasource", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstrow", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/chartfirstcol", state: false });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C86442] Copy&Paste a simple cf rule - text contains", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cf_text_rule.xlsx");

    await spreadsheet.selectCell("C3");
    I.copy();

    await spreadsheet.selectCell("G3");
    I.paste();

    spreadsheet.waitForActiveCell("G3", { display: "dd" });

    spreadsheet.enterCellText("cat", { leave: "stay" });

    //check the attributes
    spreadsheet.waitForActiveCell("G3", { display: "cat", renderFill: "#ffff00", renderColor: "#000000" });

    await I.reopenDocument();

    //check the attributes
    spreadsheet.waitForActiveCell("G3", { display: "cat", renderFill: "#ffff00", renderColor: "#000000" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C275430]Cut&Copy& a note", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cut_paste_note.xlsx");

    //cut the formula
    await spreadsheet.selectCell("B2");
    I.cut();

    //go to cell B4
    await spreadsheet.selectCell("B4");
    I.paste();

    //check the values B4
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "B2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });

    await I.reopenDocument();

    //check the values
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "B2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C290149] Cut&Copy a comment", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cut_paste_comment.xlsx");

    await spreadsheet.selectCell("C2");

    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .date" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "copy me" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", find: ".bubble svg" });

    // cut cell C2
    I.cut();

    // check the cell C2
    I.dontSeeElement({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .date" });
    I.dontSeeElement({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "copy me" });
    I.dontSeeElement({ spreadsheet: "comment-overlay", address: "C2", find: ".bubble svg" });

    // insert cell B4
    await spreadsheet.selectCell("B4");
    I.paste();

    //check the cell B4
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .date" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "copy me" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B4", find: ".bubble svg" });

    await I.reopenDocument();

    //check the cell B4
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .date" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "copy me" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B4", find: ".bubble svg" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85935] Cut&Paste a formula in a cell", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("C1");
    spreadsheet.enterCellText("=A1+B1", { leave: "stay" });
    spreadsheet.waitForActiveCell("C1", { value: 0, display: "0", formula: "A1+B1" });

    // cut cell C1
    I.cut();
    spreadsheet.waitForActiveCell("C1", { value: null, display: "", formula: null });

    // insert cell C2
    await spreadsheet.selectCell("C2");
    I.paste();
    spreadsheet.waitForActiveCell("C2", { value: 0, display: "0", formula: "A1+B1" });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("C2", { value: 0, display: "0", formula: "A1+B1" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4708] Copy conditional formattings", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4708.xlsx");
    await spreadsheet.selectCell("B2", { value: 1, renderFill: "#ff0000" });
    await spreadsheet.selectCell("D2", { value: 3, renderFill: "#ff0000" });
    await spreadsheet.selectCell("F2", { value: 5, renderFill: "#ff0000" });
    await spreadsheet.selectCell("B3", { value: 11, renderFill: "#ffff00" });
    await spreadsheet.selectCell("D3", { value: null, renderFill: "#ffff00" });
    await spreadsheet.selectCell("F3", { value: 15, renderFill: "#ffff00" });

    // copy cell range with CF to clipboard
    await spreadsheet.selectRange("A3:E3");
    I.copy();

    // paste the copied cell range twice
    await spreadsheet.selectRange("A4:E5");
    I.paste();
    I.waitForChangesSaved();

    // check the pasted results
    await spreadsheet.selectCell("A4", { value: null, renderFill: null });
    await spreadsheet.selectCell("B4", { value: 11, renderFill: "#ff0000" });
    await spreadsheet.selectCell("C4", { value: 12, renderFill: "#ff0000" });
    await spreadsheet.selectCell("D4", { value: null, renderFill: "#ff0000" });
    await spreadsheet.selectCell("E4", { value: 14, renderFill: "#ff0000" });
    await spreadsheet.selectCell("F4", { value: null, renderFill: null });
    await spreadsheet.selectCell("A5", { value: null, renderFill: null });
    await spreadsheet.selectCell("B5", { value: 11, renderFill: "#ffff00" });
    await spreadsheet.selectCell("C5", { value: 12, renderFill: "#ffff00" });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ffff00" });
    await spreadsheet.selectCell("E5", { value: 14, renderFill: "#ffff00" });
    await spreadsheet.selectCell("F5", { value: null, renderFill: null });

    // undo the changes
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    await spreadsheet.selectCell("B4", { value: null, renderFill: null });
    await spreadsheet.selectCell("C4", { value: null, renderFill: null });
    await spreadsheet.selectCell("D4", { value: null, renderFill: null });
    await spreadsheet.selectCell("E5", { value: null, renderFill: null });

    // copy single blank cell into multiple CF cells (must clear all cells)
    await spreadsheet.clickCell("A1");
    I.copy();
    await spreadsheet.selectRange("D2:E3");
    I.paste();
    I.waitForChangesSaved();

    // check the pasted results
    await spreadsheet.selectCell("D2", { value: null, renderFill: null });
    await spreadsheet.selectCell("E2", { value: null, renderFill: null });
    await spreadsheet.selectCell("D3", { value: null, renderFill: null });
    await spreadsheet.selectCell("E3", { value: null, renderFill: null });

    // undo the changes
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    await spreadsheet.selectCell("D2", { value: 3, renderFill: "#ff0000" });
    await spreadsheet.selectCell("E2", { value: 4, renderFill: "#ff0000" });
    await spreadsheet.selectCell("D3", { value: null, renderFill: "#ffff00" });
    await spreadsheet.selectCell("E3", { value: 14, renderFill: "#ffff00" });

    I.closeDocument();
}).tag("stable");
