/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo insert");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8287] Undo autosum", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autosum.xlsx");

    I.pressKeys("ArrowRight", "Shift+Ctrl+ArrowDown");

    //Insert toolbar, insert sum automatically
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("D7", { value: 13, formula: "SUM(D1:D6)" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    await spreadsheet.selectCell("D7", { value: null, formula: null });

    await I.reopenDocument();

    await spreadsheet.selectCell("D7", { value: null, formula: null });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8288] Redo autosum", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/autosum.xlsx");

    I.pressKeys("ArrowRight", "Shift+Ctrl+ArrowDown");

    //Insert toolbar, insert sum automatically
    I.clickToolbarTab("insert");
    I.clickButton("cell/autoformula", { value: "SUM", pane: "main-pane" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("D7", { value: 13, formula: "SUM(D1:D6)" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    await spreadsheet.selectCell("D7", { value: null, formula: null });

    //redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    await spreadsheet.selectCell("D7", { value: 13, formula: "SUM(D1:D6)" });

    await I.reopenDocument();

    await spreadsheet.selectCell("D7", { value: 13, formula: "SUM(D1:D6)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8289] Undo hyperlink", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("www.heise.de",  { leave: "stay" });

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.dontSeeElement({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.dontSeeElement({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8290] Redo hyperlink", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("www.heise.de",  { leave: "stay" });

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    //undo
    I.clickButton("document/undo");

    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.dontSeeElement({ docs: "control", key: "character/color", state: "sch hyperlink" });

    //redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { value: "www.heise.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15595] Undo autofilter", async ({ I }) => {

    await  I.loginAndOpenDocument("media/files/data_for_autofilter.xlsx");

    I.clickToolbarTab("data");

    I.pressKeys("Ctrl+Home");

    I.clickButton("table/filter");
    I.waitForChangesSaved();

    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    await I.reopenDocument();

    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15596] Redo autofilter", async ({ I }) => {

    await  I.loginAndOpenDocument("media/files/data_for_autofilter.xlsx");

    I.clickToolbarTab("data");

    I.pressKeys("Ctrl+Home");

    I.clickButton("table/filter");
    I.waitForChangesSaved();

    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.dontSeeElement({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    //redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.waitForVisible({ spreadsheet: "cell-button", menu: "table", address: "D1" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C86427] Undo excel table", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/table_delete_undo.xlsx", { expectToolbarTab: "table" });

    //open the pop-up, and check the value - column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column D
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "D2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    spreadsheet.selectRows("3:4");
    I.waitForVisible({ spreadsheet: "row-resize", pos: "3", collapsed: false });

    //context menu
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "4" });
    I.waitForContextMenuVisible();

    //delete rows 3,4
    I.clickButton("row/delete", { inside: "context-menu" });
    I.waitForChangesSaved();
    await spreadsheet.selectCell("A1");

    //open the pop-up, and check the value - column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b3" });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c3" });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column D
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "D2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d3" });
    I.dontSeeElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    await spreadsheet.selectCell("A1");

    //open the pop-up, and check the value - column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column D
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "D2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    await I.reopenDocument();

    //open the pop-up, and check the value - column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "b6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "c6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column D
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "D2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d3" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d4" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d5" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d6" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114378] Undo excel table is not allowd", async ({ I, spreadsheet, dialogs }) => {

    await  I.loginAndOpenDocument("media/files/table_delete_undo.xlsx", { expectToolbarTab: "table" });

    spreadsheet.selectRows("2:6");

    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });

    //context menu
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "3" });
    I.waitForContextMenuVisible();

    //delete rows 3,4,5,6
    I.clickButton("row/delete", { inside: "context-menu" });

    dialogs.waitForVisible();
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "Deleting the selected rows cannot be undone. Do you want to continue" });
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    //undo button is not active
    I.waitForVisible({ css: ".view-pane.top-pane div:nth-child(1) > div:nth-child(1) > div.group.disabled > a" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
