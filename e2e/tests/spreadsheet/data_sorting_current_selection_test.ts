/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Data > Sorting > Current selection");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C15617] Sorting - Text, descending", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/sorting_text.xlsx");

    await spreadsheet.selectRange("A3:B6");

    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");
    I.waitForChangesSaved();

    //check the values and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G5", { display: "TRUE", value: true, formula: "A5=D5" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("H3", { display: "TRUE", value: true, formula: "B3=E3" });
    await spreadsheet.selectCell("H4", { display: "TRUE", value: true, formula: "B4=E4" });
    await spreadsheet.selectCell("H5", { display: "TRUE", value: true, formula: "B5=E5" });
    await spreadsheet.selectCell("H6", { display: "TRUE", value: true, formula: "B6=E6" });

    await I.reopenDocument();

    //check the values and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G5", { display: "TRUE", value: true, formula: "A5=D5" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("H3", { display: "TRUE", value: true, formula: "B3=E3" });
    await spreadsheet.selectCell("H4", { display: "TRUE", value: true, formula: "B4=E4" });
    await spreadsheet.selectCell("H5", { display: "TRUE", value: true, formula: "B5=E5" });
    await spreadsheet.selectCell("H6", { display: "TRUE", value: true, formula: "B6=E6" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C98370] Sorting - Text, ascending", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/sorting_data_ascending.xlsx");

    await spreadsheet.selectRange("A3:B6");

    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "ascending");
    I.waitForChangesSaved();

    //check the values and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G5", { display: "TRUE", value: true, formula: "A5=D5" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("H3", { display: "TRUE", value: true, formula: "B3=E3" });
    await spreadsheet.selectCell("H4", { display: "TRUE", value: true, formula: "B4=E4" });
    await spreadsheet.selectCell("H5", { display: "TRUE", value: true, formula: "B5=E5" });
    await spreadsheet.selectCell("H6", { display: "TRUE", value: true, formula: "B6=E6" });

    await I.reopenDocument();

    //check the values and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G5", { display: "TRUE", value: true, formula: "A5=D5" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("H3", { display: "TRUE", value: true, formula: "B3=E3" });
    await spreadsheet.selectCell("H4", { display: "TRUE", value: true, formula: "B4=E4" });
    await spreadsheet.selectCell("H5", { display: "TRUE", value: true, formula: "B5=E5" });
    await spreadsheet.selectCell("H6", { display: "TRUE", value: true, formula: "B6=E6" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15605] Sorting (with one cell selection) - Descending", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/C15605_sorting_data_descending.xlsx");

    await spreadsheet.selectRange("A3:A7");

    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");
    I.waitForChangesSaved();

    //check the values  and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G5", { display: "TRUE", value: true, formula: "A5=D5" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("G7", { display: "TRUE", value: true, formula: "A7=D7" });

    await I.reopenDocument();

    //check the values  and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G5", { display: "TRUE", value: true, formula: "A5=D5" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("G7", { display: "TRUE", value: true, formula: "A7=D7" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15613] Sorting (with one cell selection and a hidden cell) - Descending", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/sorting_hidden_row.xlsx");

    await spreadsheet.selectRange("A3:A7");

    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");
    I.waitForChangesSaved();

    //check the hidden row
    I.dontSeeElement({ spreadsheet: "row-header", pos: "5" });
    I.waitForVisible({ spreadsheet: "row-resize", pos: "5", collapsed: true });

    //check the values and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("G7", { display: "TRUE", value: true, formula: "A7=D7" });

    await I.reopenDocument();

    //check the hidden row
    I.dontSeeElement({ spreadsheet: "row-header", pos: "5" });
    I.waitForVisible({ spreadsheet: "row-resize", pos: "5", collapsed: true });

    //check the values and the order
    await spreadsheet.selectCell("G3", { display: "TRUE", value: true, formula: "A3=D3" });
    await spreadsheet.selectCell("G4", { display: "TRUE", value: true, formula: "A4=D4" });
    await spreadsheet.selectCell("G6", { display: "TRUE", value: true, formula: "A6=D6" });
    await spreadsheet.selectCell("G7", { display: "TRUE", value: true, formula: "A7=D7" });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15609] Sorting - over a complete column", async ({ I, spreadsheet, alert }) => {

    await I.loginAndOpenDocument("media/files/sorting_over_column.xlsx");

    spreadsheet.selectColumn("A");

    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");

    alert.waitForVisible("Sorting cannot be performed on more than 5000 columns or rows.");
    I.closeDocument();

}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8872] Sorting - over blank cells", async ({ I, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectRange("A2:A3");

    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");

    alert.waitForVisible("Sorting cannot be performed on blank cells.");
    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------
