/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Chart");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8209] Insert and delete column chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a column chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart", find: ".canvasjs-chart-container canvas" });
    I.waitForToolbarTab("drawing", { withText: "Chart" });

    // check again after reload, delete the chart
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/delete");
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0 });

    // check again after reload
    await I.reopenDocument();
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0 });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8212] Moving the layer menu via drag&drop", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a column chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });

    // show the "Labels&Axes" floating menu
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/chartlabels", { state: false });
    I.waitForVisible({ docs: "control", key: "drawing/chartlabels", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: null });

    // drag the floating menu
    const MENU_TITLE_LOCATOR = { css: ".io-ox-office-main.popup-menu.floating-menu .popup-header .title-label" };
    const rect1 = await I.grabElementBoundingRect(MENU_TITLE_LOCATOR);
    I.dragMouseOnElement(MENU_TITLE_LOCATOR, -50, -20);
    const rect2 = await I.grabElementBoundingRect(MENU_TITLE_LOCATOR);
    expect(rect2.x - rect1.x, "X coordiante of floating menu").to.equal(-50);
    expect(rect2.y - rect1.y, "Y coordiante of floating menu").to.equal(-20);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8190] Insert column chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8191] Insert bar chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bar standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bar standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bar standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173431] Insert scatter, curved chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "scatter standard curved");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard curved" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard curved" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173432] Insert scatter, marker chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "scatter standard marker");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard marker" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard marker" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173434] Insert line chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "line standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "line standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "line standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8192] Insert pie chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "pie standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173433] Insert donut chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "donut standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "donut standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "donut standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8193] Insert bubble chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173435] Insert area chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "area standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "area standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "area standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C29596] Insert scatter, markers chart", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "scatter standard marker");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard marker" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard marker" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-4609] Load native ODS Chart if chart and its representation is saved in its draw frame", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4609_rotationSamples.ods");
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114369] Insert column chart (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173437] Insert line chart (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "line standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "line standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "line standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173436] Insert bar chart (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bar standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bar standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bar standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8194] Insert scatter chart (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "scatter standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "scatter standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114370] Insert pie chart (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "pie standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "pie standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114374] Insert bubble chart (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.ods");

    // select all data and insert a chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "bubble standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "bubble standard" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-3548] Check usage of commercial version of CanvasJS", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/data_for_chart.xlsx");

    // select all data and insert a column chart
    I.pressKeys("Ctrl+Multiply");
    I.clickToolbarTab("insert");
    I.clickOptionButton("chart/insert", "column standard");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart", find: ".canvasjs-chart-container canvas" });
    I.waitForToolbarTab("drawing", { withText: "Chart" });

    // check that the anchor element with homepage link does not exist
    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "chart", find: "a.canvasjs-chart-credit" });

    I.closeDocument();
}).tag("stable");
