/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo font- and cell attributes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8276] Undo background color", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("lorem", { leave: "stay" });

    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "green" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //fillcolor is removed
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    spreadsheet.waitForActiveCell("A1", { value: "lorem" });

    await I.reopenDocument();

    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    spreadsheet.waitForActiveCell("A1", { value: "lorem" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8277] Redo background color", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("lorem", { leave: "stay" });

    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "green" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //fillcolor is removed
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    spreadsheet.waitForActiveCell("A1", { value: "lorem" });

    //Redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.seeElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    spreadsheet.waitForActiveCell("A1", { value: "lorem" });

    await I.reopenDocument();

    I.seeElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    spreadsheet.waitForActiveCell("A1", { value: "lorem" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85968] Undo conditional formatting", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cond_format.xlsx");

    spreadsheet.waitForActiveCell("C1", { value: "cat" });

    //textcolor
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    spreadsheet.waitForActiveCell("C1", { display: "cat",  value: "cat", renderFill: "#ffff00", renderColor: "#000000", renderFont: "calibri,carlito,arial,helvetica,sans-serif" });

    //set stylesheet
    I.clickOptionButton("cell/stylesheet", "Negative");
    I.waitForChangesSaved();

    //style sheet
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Negative" });

    //textcolor
    I.waitForVisible({ docs: "control", key: "character/color", state: "rgb 9C0006" });

    //fillcolor
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });

    //CF values
    spreadsheet.waitForActiveCell("C1", { display: "cat",  value: "cat", renderFill: "#ffff00", renderColor: "#9c0006", renderFont: "calibri,carlito,arial,helvetica,sans-serif" });

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //textcolor
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    //CF values
    spreadsheet.waitForActiveCell("C1", { display: "cat",  value: "cat", renderFill: "#ffff00", renderColor: "#000000", renderFont: "calibri,carlito,arial,helvetica,sans-serif" });

    await I.reopenDocument();

    //textcolor
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    //CF values
    spreadsheet.waitForActiveCell("C1", { display: "cat",  value: "cat", renderFill: "#ffff00", renderColor: "#000000", renderFont: "calibri,carlito,arial,helvetica,sans-serif" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8281] Undo cell style", async ({ I, spreadsheet }) => {
    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello", { leave: "stay" });

    // set stylesheet
    I.clickOptionButton("cell/stylesheet", "Note");
    I.waitForChangesSaved();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "rgb FFFFCC" });
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "single:thin" });
    I.waitForVisible({ docs: "control", key: "cell/border/color", state: "rgb B2B2B2" });

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });

    await I.reopenDocument();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8286] Redo cell style", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello", { leave: "stay" });

    // set stylesheet
    I.clickOptionButton("cell/stylesheet", "Note");
    I.waitForChangesSaved();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "rgb FFFFCC" });
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "single:thin" });
    I.waitForVisible({ docs: "control", key: "cell/border/color", state: "rgb B2B2B2" });

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });

    // redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });

    await I.reopenDocument();

    // check formatting
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });
    I.dontSeeElement({ docs: "control", key: "cell/fillcolor", state: "rgb FFC7CE" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85969] Undo customized cell style", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/customize_cell_style.xlsx");

    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    //default attribute
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: "11" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    //set stylesheet
    I.clickOptionButton("cell/stylesheet", "custom");
    I.waitForChangesSaved();

    //style sheet
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "custom" });

    //textcolor
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    //fontname
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Book Antiqua" });

    //fontsize
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: "18" });

    //Strike through
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //style sheet
    I.dontSeeElement({ docs: "control", key: "cell/stylesheet", state: "custom" });

    //textcolor
    I.dontSeeElement({ docs: "control", key: "character/color", state: "red" });

    //fontname
    I.dontSeeElement({ docs: "control", key: "character/fontname", state: "Book Antiqua" });

    //fontsize
    I.dontSeeElement({ docs: "control", key: "character/fontsize", state: "18" });

    //Strike through
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();
    I.dontSeeElement({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();

    //default attribute
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: "11" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    await I.reopenDocument();

    //style sheet
    I.dontSeeElement({ docs: "control", key: "cell/stylesheet", state: "custom" });

    //textcolor
    I.dontSeeElement({ docs: "control", key: "character/color", state: "red" });

    //fontname
    I.dontSeeElement({ docs: "control", key: "character/fontname", state: "Book Antiqua" });

    //fontsize
    I.dontSeeElement({ docs: "control", key: "character/fontsize", state: "18" });

    //Strike through
    I.clickButton("view/character/format/menu", { caret: true });
    I.dontSeeElement({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.clickButton("view/character/format/menu", { caret: true });

    //default attribute
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: "11" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8284] Undo font attribute", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    //default value
    I.waitForElement({ docs: "control", key: "character/fontname", state: "+mn-lt" });

    I.clickOptionButton("character/fontname", "Impact");
    I.waitForChangesSaved();

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //default value
    I.waitForElement({ docs: "control", key: "character/fontname", state: "+mn-lt" });

    await I.reopenDocument();

    //default value
    I.waitForElement({ docs: "control", key: "character/fontname", state: "+mn-lt" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8278] Redo font attribute", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    //default value
    I.waitForElement({ docs: "control", key: "character/fontname", state: "+mn-lt" });

    I.clickOptionButton("character/fontname", "Impact");
    I.waitForChangesSaved();

    //undo
    I.waitForVisible({ itemKey: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //default value
    I.waitForElement({ docs: "control", key: "character/fontname", state: "+mn-lt" });

    //redo
    I.waitForVisible({ docs: "control", key: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //Impact
    I.waitForElement({ docs: "control", key: "character/fontname", state: "Impact" });

    await I.reopenDocument();

    //Impact
    I.waitForElement({ docs: "control", key: "character/fontname", state: "Impact" });

    I.closeDocument();
}).tag("stable");


//-----------------------------------------------------------------------------

Scenario("[C15585] Undo background color (over a column)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.selectColumn("B");

    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 red
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1048576 red
    I.pressKey("ArrowLeft");
    I.waitForElement({ itemKey: "cell/fillcolor", state: "red" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1048576 no fillcoor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    await I.reopenDocument();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1048576 no fillcoor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15587] Redo background color (over a column)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.selectColumn("B");

    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 red
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1048576 red
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 red
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ itemKey: "cell/fillcolor", state: "auto" });

    //B1048576 no fillcoor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //redo
    I.waitForVisible({ itemKey: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 red
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1048576 red
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    await I.reopenDocument();

    //A1 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1 red
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //C1 no fillcolor
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //C1048576 no fillcolor
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //B1048576 red
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //A1048576 no fillcolor
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8279] Undo merge cells", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("C2", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "blue");
    I.waitForChangesSaved();

    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("D2", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    await spreadsheet.selectRange("B2:D2");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check value and text
    spreadsheet.waitForActiveCell("B2", { merged: "B2:D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check color and text
    await spreadsheet.selectCell("B2", { value: "B2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("C2", { value: "C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "blue" });
    await spreadsheet.selectCell("D2", { value: "D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    await I.reopenDocument();

    //check color and text
    await spreadsheet.selectCell("B2", { value: "B2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("C2", { value: "C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "blue" });
    await spreadsheet.selectCell("D2", { value: "D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8285] Redo merge cells", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("B2", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("C2", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "blue");
    I.waitForChangesSaved();

    I.pressKey("ArrowRight");
    spreadsheet.enterCellText("D2", { leave: "stay" });
    I.clickOptionButton("cell/fillcolor", "red");
    I.waitForChangesSaved();

    //merged cells
    await spreadsheet.selectRange("B2:D2");
    I.clickButton("cell/merge");
    I.waitForChangesSaved();

    //check value and text
    spreadsheet.waitForActiveCell("B2", { merged: "B2:D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check color and text
    await spreadsheet.selectCell("B2", { value: "B2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    await spreadsheet.selectCell("C2", { value: "C2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "blue" });
    await spreadsheet.selectCell("D2", { value: "D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "red" });

    //redo
    I.waitForVisible({ docs: "control", key: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //check value and text
    await spreadsheet.selectCell("B2", { merged: "B2:D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    await I.reopenDocument();

    //check color and text
    await spreadsheet.selectCell("B2", { merged: "B2:D2" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C287512] Undo fill color (over a column)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello", { leave: "stay" });

    //set stylesheet
    I.clickOptionButton("cell/stylesheet", "Note");
    I.waitForChangesSaved();

    //style sheet cell A1
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });

    spreadsheet.selectColumn("A");

    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //B1 no color
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    // A1 green color
    I.pressKey("ArrowLeft");
    I.waitForElement({ docs: "control", key:  "cell/fillcolor", state: "green" });

    I.pressKeys("Command+ArrowDown");

    // A1048576 green color
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.pressKey("ArrowRight");

    // B1048576 no green color
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // B1048576 no green color
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowLeft");

    // A1048576 no green color
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Command+ArrowUp");

    //style sheet cell and text in  A1
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });
    I.pressKey("ArrowRight");

    //B1 no cellstyle and no green color
    I.waitForInvisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    await I.reopenDocument();

    //B1 no cellstyle and no green color
    I.waitForInvisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowLeft");

    //style sheet cell and text in  A1
    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Note" });
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15586] Undo background color (over a row)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.selectRow("2");

    //set fillcolor
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //A2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowUp");

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("2*ArrowDown");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Command+ArrowRight");

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowUp");

    //XFD2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowUp");

    //XFD1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //XFD1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //XFD2 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Command+ArrowLeft");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowUp");

    //A2 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowUp");

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    await I.reopenDocument();

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //A2 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Command+ArrowRight");

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowUp");

    //XFD2 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowUp");

    //XFD2 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8283] Redo border attribute", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("hello", { leave: "stay" });

    I.clickOptionButton("cell/border/flags", "top-bottom");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tb" });

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "" });

    // redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tb" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tb" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8280] Undo border attribute", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("hello", { leave: "stay" });

    I.clickOptionButton("cell/border/flags", "top-bottom");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tb" });

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.dontSeeElement({ docs: "control", key: "cell/border/flags", state: "tb" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15583] Undo cell style (over a column)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.selectColumn("A");
    I.clickOptionButton("cell/stylesheet", "Positive");
    I.waitForChangesSaved();

    //A1 green
    I.pressKeys("Ctrl+Home");
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });

    // B1 Standard
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key:  "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowDown", "ArrowLeft");

    // A1048576 green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });
    I.pressKey("ArrowRight");

    // B1048577 no green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // B1048576 no green color
    I.pressKeys("Command+ArrowDown");
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowLeft");

    // A1048576 no green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowUp");

    //A1 no style
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowRight");

    //B1 no style
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    await I.reopenDocument();

    I.pressKey("ArrowLeft");

    //no style
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15589] Redo cell style (over a column)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.selectColumn("A");
    I.clickOptionButton("cell/stylesheet", "Positive");
    I.waitForChangesSaved();

    //A1 green
    I.pressKeys("Ctrl+Home");
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });

    // B1 Standard
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key:  "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowDown", "ArrowLeft");

    // A1048576 green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });
    I.pressKey("ArrowRight");

    // B1048577 no green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //A1 green
    I.pressKeys("Control+Home");
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    // B1 Standard
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key:  "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowDown", "ArrowLeft");

    // A1048576 green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowRight");

    // B1048577 no green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    //redo
    I.waitForVisible({ docs: "control", key: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //A1 green
    I.pressKeys("Ctrl+Home");
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });

    // B1 Standard
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key:  "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowDown", "ArrowLeft");

    // A1048576 green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });
    I.pressKey("ArrowRight");

    // B1048577 no green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    await I.reopenDocument();

    //A1 green
    I.pressKeys("Ctrl+Home");
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });

    // B1 Standard
    I.pressKey("ArrowRight");
    I.waitForElement({ docs: "control", key:  "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowDown", "ArrowLeft");

    // A1048576 green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Positive" });
    I.pressKey("ArrowRight");

    // B1048577 no green color
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15588] Redo background color (over a row)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.selectRow("2");

    //set fillcolor
    I.clickOptionButton("cell/fillcolor", "green");
    I.waitForChangesSaved();

    //A2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });

    I.pressKey("ArrowUp");

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.pressKeys("2*ArrowDown");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //XFD2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowDown");

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.pressKeys("Control+Home");

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //A2 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //XFD2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Control+Home");

    //redo
    I.waitForVisible({ docs: "control", key: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.pressKeys("Control+Home");

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //A2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowDown");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //XFD2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowDown");

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Control+Home");

    await I.reopenDocument();

    //A1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //A2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowDown");

    //A3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });
    I.pressKey("ArrowDown");

    //XFD2 fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "green" });
    I.pressKey("ArrowDown");

    //XFD3 no fillcolor
    I.waitForElement({ docs: "control", key: "cell/fillcolor", state: "auto" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15590] Redo cell style (over a row)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.selectRow("2");

    //set accent 2
    I.clickOptionButton("cell/stylesheet", "Accent 2");
    I.waitForChangesSaved();

    //A2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowUp");

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("2*ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.pressKeys("Control+Home");

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Control+Home");

    //redo
    I.waitForVisible({ docs: "control", key: "document/redo" });
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.pressKeys("Control+Home");

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Control+Home");

    await I.reopenDocument();

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15584] Undo cell style (over a row)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.selectRow("2");

    //set stylesheet
    I.clickOptionButton("cell/stylesheet", "Accent 2");
    I.waitForChangesSaved();

    //A2 accent 2
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowUp");

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("2*ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Accent 2" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });

    //undo
    I.waitForVisible({ docs: "control", key: "document/undo" });
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.pressKeys("Control+Home");

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Control+Home");

    await I.reopenDocument();

    //A1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //A3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Command+ArrowRight", "2*ArrowUp");

    //XFD1 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD2 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKey("ArrowDown");

    //XFD3 standard
    I.waitForElement({ docs: "control", key: "cell/stylesheet", state: "Standard" });
    I.pressKeys("Control+Home");

    I.closeDocument();
}).tag("stable");
