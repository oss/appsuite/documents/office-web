/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Data > Named ranges");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C29567] New named ranges - create workbook named ranges for one cell", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("example", { leave: "stay" });

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: 'button[data-action="close"]' });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog: title, empty text input, formula field, OK button disabled, cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input[placeholder='Enter name']" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input[placeholder='Enter formula']" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']:disabled" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "hello");

    //close dialog
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: 'button[data-action="close"]' });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "Sheet1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("=hello", { leave: "stay", expectAutoComplete: "name:hello" });
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A2", { value: "example", formula: "hello" });

    // Reload and check the values again
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A2", { value: "example", formula: "hello" });
    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C49093] New named ranges - create workbook named ranges for one cell (ODS)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("example", { leave: "stay" });

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog: title, empty text input, formula field, OK button disabled, cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']:disabled" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "hello");

    //close dialog
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "Sheet1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.pressKey("ArrowDown");

    spreadsheet.enterCellText("=hello", { leave: "stay", expectAutoComplete: "name:hello" });
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A2", { value: "example", formula: "hello" });

    // Reload and check the values again
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A2", { value: "example", formula: "hello" });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29572] New named ranges - create workbook named ranges for cell ranges", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("5");

    // select A1:A2
    I.waitForChangesSaved();
    I.pressKeys("Ctrl+Home", "Shift+ArrowDown");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog: title, empty text input, formula field, OK button disabled, cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']:disabled" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog type text and click OK
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "hello");
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "Sheet1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.waitForChangesSaved();
    I.pressKey("ArrowRight");

    spreadsheet.enterCellText("=sum(hello)");
    I.waitForChangesSaved();
    I.pressKey("ArrowUp");
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("B1", { value: 7, formula: "SUM(hello)" });

    // Reload and check the values again
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("B1", { value: 7, formula: "SUM(hello)" });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49094] New named ranges - create workbook named ranges for cell ranges (ODS)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("5");

    // select A1:A2
    I.waitForChangesSaved();
    I.pressKeys("Ctrl+Home", "Shift+ArrowDown");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog: title, empty text input, formula field, OK button disabled, cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']:disabled" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog type text and click OK
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "hello");
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "Sheet1!$A$1:$A$2" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.waitForChangesSaved();
    I.pressKey("ArrowRight");

    spreadsheet.enterCellText("=sum(hello)");
    I.waitForChangesSaved();
    I.pressKey("ArrowUp");
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("B1", { value: 7, formula: "SUM(hello)" });

    // Reload and check the values again
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("B1", { value: 7, formula: "SUM(hello)" });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29594] New named ranges - create workbook named ranges with formula = constant value", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog: title, empty text input, formula field, OK button disabled, cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']:disabled" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog: type input text, type formula text,click OK
    I.fillField({ docs: "dialog", find: ".name-label-input > input" }, "hello");
    I.fillField({ docs: "dialog", find: ".name-formula-input > input" }, "100");

    dialogs.clickOkButton();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "100" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    spreadsheet.enterCellText("=hello+1", { leave: "stay" });
    I.waitForChangesSaved();

    // Check the name ranges value from the cell B1
    spreadsheet.waitForActiveCell("A1", { value: 101 });

    // Reload and check the values again
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 101 });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49096] New named ranges - create workbook named ranges with formula = constant value (ODS)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog: title, empty text input, formula field, OK button disabled, cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']:disabled" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog: type input text, type formula text,click OK
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "hello");
    I.fillField({ docs: "dialog", find: ".name-formula-input input" }, "100");

    dialogs.clickOkButton();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "100" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    spreadsheet.enterCellText("=hello+1", { leave: "stay" });
    I.waitForChangesSaved();

    // Check the name ranges value from the cell A1
    spreadsheet.waitForActiveCell("A1", { value: 101 });

    // Reload and check the values again
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 101 });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29584] Existing workbook named range - change the formula", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/changed_name_named_ranges.xlsx");
    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "AEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Tabelle1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "DEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "Tabelle1!$D$1" });

    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/edit/dialog" });

    //Dialog: title,  text input, formula field, OK button , cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Change name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog: type input text, type formula text,click OK
    I.fillField({ docs: "dialog", find: ".name-formula-input input" }, "Tabelle1!$D$1");

    dialogs.clickOkButton();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    // I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "AEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Tabelle1!$D$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "DEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "Tabelle1!$D$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    // Check the name ranges value from the cell C3 /C4
    I.waitForChangesSaved();
    await spreadsheet.selectCell("C3", { value: "zwei", formula: "AEINS" });
    await spreadsheet.selectCell("C4", { value: "zwei", formula: "DEINS" });

    // Reload and check the values again
    await I.reopenDocument();

    // Check the name ranges value from the cell C3 /C4
    await spreadsheet.selectCell("C3", { value: "zwei", formula: "AEINS" });
    await spreadsheet.selectCell("C4", { value: "zwei", formula: "DEINS" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49099] Existing workbook named range - change the formula (ODS)", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/changed_name_named_ranges.ods");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "AEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "$Tabelle1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "DEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "$Tabelle1!$D$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/edit/dialog" });

    //Dialog: title,  text input, formula field, OK button , cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Change name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    I.pressKey(["Tab"]);

    //Dialog: type input text, type formula text,click OK
    I.fillField({ docs: "dialog", find: ".name-formula-input input" }, "$Tabelle1!$D$1");

    dialogs.clickOkButton();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "AEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "$Tabelle1!$D$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "DEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "$Tabelle1!$D$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    // Check the name ranges value from the cell C3 /C4
    await spreadsheet.selectCell("C3", { value: "zwei", formula: "AEINS" });
    await spreadsheet.selectCell("C4", { value: "zwei", formula: "DEINS" });

    // Reload and check the values again
    await I.reopenDocument();

    // Check the name ranges value from the cell C3 /C4
    await spreadsheet.selectCell("C4", { value: "zwei", formula: "DEINS" });
    await spreadsheet.selectCell("C3", { value: "zwei", formula: "AEINS" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29569] Existing workbook named range - change the name", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/change_named_range_value.xlsx");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".name", withText: "eins" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Tabelle1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/delete" });
    I.waitForAndClick({ spreadsheet: "name-item", index: 0, key: "name/edit/dialog" });

    //Dialog: title,  text input, formula field, OK button , cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Change name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog: type input text, type formula text,click OK
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "changeMe");

    dialogs.clickOkButton();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "changeMe" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Tabelle1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/edit/dialog" });

    // Check the name ranges value from the cell A2
    I.waitForChangesSaved();

    await spreadsheet.selectCell("A2", { value: "hello", formula: "changeMe" });

    // Reload and check the values again
    await I.reopenDocument();

    // Check the name ranges value from the cell A2
    await spreadsheet.selectCell("A2", { value: "hello", formula: "changeMe" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49097] Existing workbook named range - change the name (ODS)", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/changed_name_named_ranges_value.ods");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "AEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "$Tabelle1!$A$1" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "DEINS" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "$Tabelle1!$D$1" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/edit/dialog" });

    //Dialog: title,  text input, formula field, OK button , cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Change name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog: type input text, type formula text,click OK
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "changeMe");

    dialogs.clickOkButton();

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".name", withText: "changeMe" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "$Tabelle1!$A$1" });

    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    // Check the name ranges value from the cell A2
    I.waitForChangesSaved();
    await spreadsheet.selectCell("C3", { value: "eins", formula: "changeMe" });
    await spreadsheet.selectCell("C4", { value: "zwei", formula: "DEINS" });

    // Reload and check the values again
    await I.reopenDocument();

    // Check the name ranges value from the cell A2
    await spreadsheet.selectCell("C4", { value: "zwei", formula: "DEINS" });
    await spreadsheet.selectCell("C3", { value: "eins", formula: "changeMe" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29568] Existing named ranges - Delete workbook named ranges", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/delete_named_ranges.xlsx");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });

    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "A7E11" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Sheet1!$A$7:$E$11" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/delete" });

    //I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Sheet1!$A$7:$E$11" });

    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "B2C3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "Sheet1!$B$2:$C$3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 2, find: ".text", withText: "D4E5" });
    I.waitForVisible({ spreadsheet: "name-item", index: 2, find: ".formula", withText: "Sheet1!$D$4:$E$5" });
    I.waitForVisible({ spreadsheet: "name-item", index: 2, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 2, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 3, find: ".text", withText: "NewRange" });
    I.waitForVisible({ spreadsheet: "name-item", index: 3, find: ".formula", withText: "Sheet1!$A$1:$C$7" });
    I.waitForVisible({ spreadsheet: "name-item", index: 3, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 3, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 4, find: ".text", withText: "sheet2" });
    I.waitForVisible({ spreadsheet: "name-item", index: 4, find: ".formula", withText: "Sheet2!$A$1:$B$4" });
    I.waitForVisible({ spreadsheet: "name-item", index: 4, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 4, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 5, find: ".text", withText: "Zahlen" });
    I.waitForVisible({ spreadsheet: "name-item", index: 5, find: ".formula", withText: "Sheet1!$F$2:$F$6" });
    I.waitForVisible({ spreadsheet: "name-item", index: 5, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 5, key: "name/delete" });

    I.waitForAndClick({ spreadsheet: "name-item", index: 0, key: "name/delete" });

    //first element is D4E5
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "B2C3" });

    await I.reopenDocument();

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "B2C3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "Sheet1!$B$2:$C$3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/delete" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C49095] Existing named ranges - Delete workbook named ranges (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/delete_named_ranges.ods");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: closer, title, text no named ranges
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });

    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "A7E11" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "$Tabelle1!$A$7:$E$11" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".text", withText: "B2C3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, find: ".formula", withText: "$Tabelle1!$B$2:$C$3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 1, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 2, find: ".text", withText: "D4E5" });
    I.waitForVisible({ spreadsheet: "name-item", index: 2, find: ".formula", withText: "$Tabelle1!$D$4:$E$5" });
    I.waitForVisible({ spreadsheet: "name-item", index: 2, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 2, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 3, find: ".text", withText: "NewRange" });
    I.waitForVisible({ spreadsheet: "name-item", index: 3, find: ".formula", withText: "$Tabelle1!$A$1:$C$7" });
    I.waitForVisible({ spreadsheet: "name-item", index: 3, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 3, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 4, find: ".text", withText: "Sheet2" });
    I.waitForVisible({ spreadsheet: "name-item", index: 4, find: ".formula", withText: "$Tabelle2!$A$1:$B$4" });
    I.waitForVisible({ spreadsheet: "name-item", index: 4, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 4, key: "name/delete" });

    I.waitForVisible({ spreadsheet: "name-item", index: 5, find: ".text", withText: "Zahlen" });
    I.waitForVisible({ spreadsheet: "name-item", index: 5, find: ".formula", withText: "$Tabelle1!$F$1:$F$5" });
    I.waitForVisible({ spreadsheet: "name-item", index: 5, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 5, key: "name/delete" });

    I.waitForAndClick({ spreadsheet: "name-item", index: 0, key: "name/delete" });

    //first element is D4E5
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "B2C3" });

    await I.reopenDocument();

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "B2C3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "$Tabelle1!$B$2:$C$3" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/edit/dialog" });
    I.waitForVisible({ spreadsheet: "name-item", index: 0, key: "name/delete" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29583] New named ranges - unique named ranges", ({ I, dialogs, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("test", { leave: "stay" });

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".no-names-message", withText: "No named ranges defined." });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/insert/dialog" });

    //Dialog type text and click OK
    dialogs.waitForVisible();
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "unique");

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // Add a new named range
    I.clickButton("name/insert/dialog", { inside: "floating-menu" });

    //Dialog: check named ranges name is already used
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "unique");

    dialogs.clickOkButton();

    alert.waitForVisible("This name is already used. Please enter another name");
    alert.clickCloseButton();

    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29578] Existing named ranges - Name length limit", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/named_length_limit.xlsx");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForVisible({ spreadsheet: "name-menu" });

    //popup elements: title, closer,name text value, formula field value, delete button, edit button, create a new named range
    I.waitForVisible({ spreadsheet: "name-menu", find: ".title-label", withText: "Named ranges" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".text", withText: "hello" });
    I.waitForVisible({ spreadsheet: "name-menu", find: ".formula", withText: "Sheet1!$A$" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/delete" });
    I.waitForVisible({ spreadsheet: "name-menu", key: "name/insert/dialog" });
    I.waitForAndClick({ spreadsheet: "name-menu", key: "name/edit/dialog" });

    //Dialog: title,  text input, formula field, OK button , cancel button active, type text
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Change name" });
    I.waitForVisible({ docs: "dialog", find: ".name-label-input input" });
    I.waitForVisible({ docs: "dialog", find: ".name-formula-input input" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='ok']" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='cancel']" });

    //Dialog type text and click OK
    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "Xabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghi1234567888888888888endexxx");

    I.wait(2); //needed because the browser is too slow to type long text into input element

    const value = await I.grabValueFrom({ docs: "dialog", find: ".name-label-input input" });
    expect(value.length).to.equal(255);

    dialogs.clickOkButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[NoTestrailCase] New named ranges -  error alert sample", async ({ I, spreadsheet, dialogs, alert }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("2");
    I.pressKey("ArrowUp");

    //open the popup
    I.clickToolbarTab("data");
    I.clickButton("view/namesmenu/toggle");
    I.waitForPopupMenuVisible();

    I.clickButton("name/insert/dialog", { inside: "floating-menu" });

    I.fillField({ docs: "dialog", find: ".name-label-input input" }, "^^");

    dialogs.clickOkButton();

    alert.waitForVisible("This name contains invalid characters. Please enter another name.");

    dialogs.clickCancelButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
