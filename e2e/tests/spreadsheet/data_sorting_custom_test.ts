/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// private functions ==========================================================

async function checkSortedRange(range: string): Promise<void> {

    const { spreadsheet } = inject();

    const matches = /^([A-Z])(\d+):([A-Z])(\d+)$/.exec(range);
    if (!matches) { throw new Error(`invalid cell range address "${range}"`); }

    const col1 = matches[1].charCodeAt(0);
    const row1 = parseInt(matches[2], 10);
    const col2 = matches[3].charCodeAt(0);
    const row2 = parseInt(matches[4], 10);

    for (let col = col1; col <= col2; col += 1) {
        for (let row = row1; row <= row2; row += 1) {
            const address = `${String.fromCharCode(col)}${row}`;
            await spreadsheet.selectCell(address, { value: true });
        }
    }
}

// tests ======================================================================

Feature("Documents > Spreadsheet > Data > Sorting > Custom sorting");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8885] Sorting - custom sort (left to right, row 6, descending)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting_custom_l2r_descending.xlsx");
    await spreadsheet.selectRange("A3:D6");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Dialog - check the default values
    //Checkbox
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-custom-sort-dialog", find: ".has-headers input[type=checkbox]" });

    //Top to bottom, Left to right
    I.click({ docs: "dialog", find: ".sort-direction .btn", withText: "Top to bottom" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: true, withText: "Top to bottom" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: false, withText: "Left to right" });
    I.pressKey("Escape");

    //Column A, Column B, Column C, Column D
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Column A" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 0, withText: "Column A" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 1, withText: "Column B" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 2, withText: "Column C" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 3, withText: "Column D" });
    I.pressKey("Escape");

    //Ascending, Descending
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: false, withText: "Ascending" });

    I.pressKey("Escape");

    //buttons
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=ok]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=cancel]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=add]" });

    //change direction
    I.click({ docs: "dialog", find: ".sort-direction .btn", withText: "Top to bottom" });
    I.click({ docs: "button", inside: "popup-menu", value: false, withText: "Left to right" });

    //change sort by
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Row 3" });
    I.click({ docs: "button", inside: "popup-menu", value: 5, withText: "Row 6" });

    //change order
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("L3:O6");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("L3:O6");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C15614] Sorting - custom sort (top to bottom, column C, descending)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting_custom_t2b_descending.xlsx");
    await spreadsheet.selectRange("A3:D6");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //change sort by
    I.waitForAndClick({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Column A" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: 2, withText: "Column C" });

    //change order
    I.waitForAndClick({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("L3:O6");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("L3:O6");

    I.closeDocument();
}).tag("stable");
//-----------------------------------------------------------------------------

Scenario("[C15607] Sorting - Custom popup (selection has headers)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting_custom_selection_header.xlsx");
    await spreadsheet.selectRange("A3:B6");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //activate the checkbox
    I.click({ docs: "dialog", find: ".has-headers input[type=checkbox]" });

    //Cell value A3, B3
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "1" });
    I.click({ docs: "button", inside: "popup-menu", value: 1, withText: "2" });

    //change order
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I3:J6");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I3:J6");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85861] Sorting - custom (string): simple sorting", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_header_lastname_descending.xlsx");
    await spreadsheet.selectRange("A5:C11");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Set checkbox to true
    I.click({ docs: "dialog", find: ".has-headers input[type=checkbox]" });

    //change order
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I5:K11");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I5:K11");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85890] Sorting - custom sort (string): sorting with two criteria", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_header_lastname_firstname.xlsx");
    await spreadsheet.selectRange("A5:C11");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Set checkbox to true
    I.click({ docs: "dialog", find: ".has-headers input[type=checkbox]" });

    //Add sort criterion
    I.click({ docs: "dialog", area: "footer", find: "button[data-action=add]" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-by", withText: "undefined" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(2) .btn[data-action=delete]" });

    //choose the first name
    I.click({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-by", withText: "undefined" });
    I.click({ docs: "button", inside: "popup-menu", value: 1, withText: "First name" });

    //change order
    I.click({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I5:K11");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I5:K11");

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85932] Sorting - custom sort (string): simple sorting (left to right))", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_header_lastname_lr.xlsx");
    await spreadsheet.selectRange("A5:C11");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Set checkbox to true
    I.click({ docs: "dialog", find: ".has-headers input[type=checkbox]" });

    //Direction - change to left to right
    I.click({ docs: "dialog", find: ".sort-direction .btn", withText: "Top to bottom" });
    I.click({ docs: "button", inside: "popup-menu", value: false, withText: "Left to right" });

    //Sort by - change to hotzenplotz
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Last name" });
    I.click({ docs: "button", inside: "popup-menu", value: 6, withText: "Hotzenplotz" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I5:K11");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I5:K11");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C85891] Sorting - custom (currency): simple sorting", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_currency.xlsx");
    await spreadsheet.selectRange("A5:C10");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //change order
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I5:K10");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I5:K10");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85903] Sorting - custom sort (date): simple sorting", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_date.xlsx");
    await spreadsheet.selectRange("A5:C9");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Change to Datum 3
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Datum 1" });
    I.click({ docs: "button", inside: "popup-menu", value: 2, withText: "Datum 3" });

    //change to descending
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I5:K9");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I5:K9");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85916] Sorting - custom sort (string): sorted by values more than once", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_header_lastname_lastname.xlsx");
    await spreadsheet.selectRange("A5:C11");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //checkbox
    I.click({ docs: "dialog", find: ".has-headers input[type=checkbox]" });

    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Last name" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 0, withText: "Last name" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 1, withText: "First name" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: 2, withText: "Gender" });
    I.pressKey("Escape");

    //Add sort criteria
    I.click({ docs: "dialog", area: "footer", find: "button[data-action=add]" });

    //choose the first name
    I.click({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-by", withText: "undefined" });
    I.click({ docs: "button", inside: "popup-menu", value: 0, withText: "Last name" });

    //second trash icon
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(2) .btn[data-action=delete]" });

    //error icons
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(1) .sort-by.warning-icon [data-icon-id]" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-by.warning-icon [data-icon-id]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=ok]:disabled" });

    dialogs.clickCancelButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85917] Sorting - custom sort (string): row/ column criteria not specified", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/custom_header_lastname_lastname.xlsx");
    await spreadsheet.selectRange("A5:C11");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Add sort criteria
    I.click({ docs: "dialog", area: "footer", find: "button[data-action=add]" });

    //error icons
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-by", withText: "undefined" });
    I.waitForVisible({ docs: "dialog", find: ".sort-rule:nth-child(2) .sort-by.warning-icon [data-icon-id]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=ok]:disabled" });

    dialogs.clickCancelButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85933] custom (currency): simple sorting (ODS)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/header_currency.ods");
    await spreadsheet.selectRange("A5:C10");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //change to descending
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("I5:K10");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("I5:K10");

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114838] Sorting in a table with header - custom sort ('Spalte3', descending)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting_custom_table_with_header.xlsx");
    await spreadsheet.selectRange("A3:D7");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Change to Spalte3
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Spalte1" });
    I.click({ docs: "button", inside: "popup-menu", value: 2, withText: "Spalte3" });

    //change to descending
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("L3:O7");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("L3:O7");

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114839] Sorting in a table - custom sort ('Column C', descending)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/sorting_custom_table.xlsx");
    await spreadsheet.selectRange("A3:D6");

    //open the dialog
    I.clickToolbarTab("data");
    I.clickButton("table/sort", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "table/sort/dialog", withText: "Custom" });
    dialogs.waitForVisible();

    //Change to column C
    I.click({ docs: "dialog", find: ".sort-rule .sort-by", withText: "Column A" });
    I.click({ docs: "button", inside: "popup-menu", value: 2, withText: "Column C" });

    //change to descending
    I.click({ docs: "dialog", find: ".sort-rule .sort-order", withText: "Ascending" });
    I.click({ docs: "button", inside: "popup-menu", value: true, withText: "Descending" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    // check all test cells
    await checkSortedRange("L3:O6");

    await I.reopenDocument();

    // check all test cells
    await checkSortedRange("L3:O6");

    I.closeDocument();

}).tag("stable");
