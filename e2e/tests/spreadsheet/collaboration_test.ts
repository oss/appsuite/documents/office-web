/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Collaboration");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C303235] Users can edit spreadsheets concurrently", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    const fileDesc = await I.grabFileDescriptor();

    await session("Alice", async () => {
        await I.loginAndOpenDocument(fileDesc);
    });

    spreadsheet.enterCellText("42");

    await session("Alice", () => {
        spreadsheet.waitForActiveCell("A1", { value: 42 });
        I.waitForVisible({ spreadsheet: "remote-range", range: "A2" });
        I.waitNumberOfVisibleElements({ docs: "collab-menu", find: ".group.user-badge" }, 2);
    });

    I.waitForVisible({ spreadsheet: "remote-range", range: "A1" });
    I.waitNumberOfVisibleElements({ docs: "collab-menu", find: ".group.user-badge" }, 2);

    await session("Alice", () => {
        spreadsheet.enterCellText("test", { leave: "right" });
    });

    I.pressKeys("Ctrl+Home");
    spreadsheet.waitForActiveCell("A1", { value: "test" });
    I.waitForVisible({ spreadsheet: "remote-range", range: "B1" });

    I.closeDocument();

    await session("Alice", () => {
        I.closeDocument();
    });
}).tag("stable");
