/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// private functions ==========================================================

function getTodayStr(): string {
    const date = new Date();
    const m = String(date.getMonth() + 1).padStart(2, "0");
    const d = String(date.getDate()).padStart(2, "0");
    const y = date.getFullYear();
    return `${m}/${d}/${y}`;
}

// tests ======================================================================

Feature("Documents > Spreadsheet > Formulas");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C317667] Formula - move a sheet with a simple formula", ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert two sheets
    spreadsheet.insertSheet();
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.insertSheet();

    // insert formula in first sheet pointing to second sheet
    spreadsheet.activateSheet(0);
    spreadsheet.enterCellText("=SHEET()*Sheet2!A1", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // drag first sheet to end
    I.dragMouseOnElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 }, 300, 0);
    spreadsheet.waitForActiveCell("A1", { value: 126, formula: "SHEET()*Sheet2!A1" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85854] Can dectect unsupported functions", async ({ I, alert, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert formula with unsupported function
    spreadsheet.enterCellText("=ODDFPRICE(0.1,0.1,0.1,0.1,0.1,0.1,0.1,1)", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { error: "#NA" });

    // expect an error alert when reloading the document
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { error: "#NA" });
    alert.waitForVisible("This spreadsheet contains formulas with unsupported functions:");
    alert.waitForVisible("ODDFPRICE");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85855] Can dectect unsupported functions (ODS)", async ({ I, alert, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert formula with unsupported function
    spreadsheet.enterCellText('=INFO("osversion")', { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { error: "#NA" });

    // expect an error alert when reloading the document
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { error: "#NA" });
    alert.waitForVisible("This spreadsheet contains formulas with unsupported functions:");
    alert.waitForVisible("INFO");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8314] Autocomplete function names", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // start to type a formula
    spreadsheet.waitForCellFocus();
    I.type("=su");

    // expect a popup menu to appear
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "func:SUBSTITUTE" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "func:SUM" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "func:SUMPRODUCT" });
    I.dontSeeElement({ docs: "button", inside: "popup-menu", value: "func:ABS" });

    // select the SUM function
    I.click({ docs: "button", inside: "popup-menu", value: "func:SUM" });

    // expect cell edit mode with prefilled formula, insert parameters
    spreadsheet.waitForCellEditMode("=SUM()");
    I.type("1,3");
    spreadsheet.waitForCellEditMode("=SUM(1,3)");
    I.pressKey("Enter");

    // expect formula and result to appear in the cell
    I.waitForChangesSaved();
    I.pressKey("ArrowUp");
    spreadsheet.waitForActiveCell("A1", { value: 4, formula: "SUM(1,3)" });

    // expect same contents after reload
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 4, formula: "SUM(1,3)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8313] [C8315] [C8316] [C8317] [C8318] [C8319] [C8320] [C8321] [C8322] [C8323] Simple formulas - operators and functions", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter simple formulas (test operator precedence)
    spreadsheet.enterCellText("=1+2*3^4*3+2");
    spreadsheet.enterCellText("=1-64/2^4/2-1");
    spreadsheet.enterCellText("=today()");
    spreadsheet.enterCellText("=sum(a2:a1)");
    spreadsheet.enterCellText("=product(a2:a1)");
    spreadsheet.enterCellText("=round(pi(),2)");
    spreadsheet.enterCellText("140");
    spreadsheet.enterCellText('=if(a7>140,"HIGH","OK")');

    // expect formula results
    await spreadsheet.selectCell("A1", { value: 489 });
    await spreadsheet.selectCell("A2", { value: -2 });
    await spreadsheet.selectCell("A3", { display: getTodayStr(), formula: "TODAY()" });
    await spreadsheet.selectCell("A4", { value: 487, formula: "SUM(A1:A2)" });
    await spreadsheet.selectCell("A5", { value: -978, formula: "PRODUCT(A1:A2)" });
    await spreadsheet.selectCell("A6", { display: "3.14", formula: "ROUND(PI(),2)" });
    await spreadsheet.selectCell("A8", { value: "OK", formula: 'IF(A7>140,"HIGH","OK")' });

    // change source cell referred in IF function
    await spreadsheet.selectCell("A7");
    spreadsheet.enterCellText("200");
    spreadsheet.waitForActiveCell("A8", { value: "HIGH" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: 489 });
    await spreadsheet.selectCell("A2", { value: -2 });
    await spreadsheet.selectCell("A3", { display: getTodayStr(), formula: "TODAY()" });
    await spreadsheet.selectCell("A4", { value: 487, formula: "SUM(A1:A2)" });
    await spreadsheet.selectCell("A5", { value: -978, formula: "PRODUCT(A1:A2)" });
    await spreadsheet.selectCell("A6", { display: "3.14", formula: "ROUND(PI(),2)" });
    await spreadsheet.selectCell("A8", { value: "HIGH", formula: 'IF(A7>140,"HIGH","OK")' });

    I.closeDocument();
}).tag("smoketest");
