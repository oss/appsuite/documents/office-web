/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format › Number format > setting format via built-in");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8162] Number format - built-in number formats (general)", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1, display: "1" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "standard", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "standard", checked: true });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 1, display: "1" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "standard", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "standard", checked: true });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8160] Number format - built-in number formats (currency)", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("$1234.56", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1234.56, display: "$1,234.56 " });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "currency", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "currency", checked: true });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 1234.56, display: "$1,234.56 " });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "currency", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "currency", checked: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8159] Number format - built-in number formats (percentage)", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("12.5%", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 0.125, display: "12.50%" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "percent", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "percent", checked: true });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 0.125, display: "12.50%" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "percent", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "percent", checked: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8161] Number format - built-in number formats (date)", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("10/19/2013", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41566, display: "10/19/2013" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "date", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "date", checked: true });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 41566, display: "10/19/2013" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "date", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "date", checked: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85627] Number format - built-in number formats (formatted date)", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("2-Feb-14", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41672, display: "02/02/2014" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "date", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "date", checked: true });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 41672, display: "02/02/2014" });

    // open popup menu, check the value
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", state: "date", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "date", checked: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
