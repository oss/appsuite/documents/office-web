/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Table Range");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C114375] can change table style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/table.xlsx", { expectToolbarTab: "table" });

    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "TableStyleMedium2" });
    I.clickOptionButton("table/stylesheet", "TableStyleLight14");

    await I.reopenDocument({ expectToolbarTab: "table" });
    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "TableStyleLight14" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85609] Excel table reference - can insert a column", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/simple_tabellen_reference.xlsx");

    spreadsheet.selectColumn("B");

    await spreadsheet.openCellContextMenu("B2");
    I.waitForAndClick({ docs: "control", key: "view/column/submenu", inside: "context-menu" });

    //insert a column
    I.clickButton("column/insert", { withText: "Insert column", inside: "popup-menu" });

    await spreadsheet.selectCell("B1", { display: "Column1" });

    //open the pop-up, and check the value
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank", withText: "Blank cells" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    await I.reopenDocument({ expectToolbarTab: "table" });

    spreadsheet.waitForActiveCell("B1", { display: "Column1" });

    //open the pop-up, and check the value
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank", withText: "Blank cells" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173490] Excel table reference - can change and used a customized table style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/customized_table_style.xlsx", { expectToolbarTab: "table" });

    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "TableStyleMedium3" });
    I.clickOptionButton("table/stylesheet", "mhe");

    await I.reopenDocument({ expectToolbarTab: "table" });
    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "mhe" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C275431] Insert an autofilter beside the Excel table is not allowed", async ({ I, alert }) => {

    await I.loginAndOpenDocument("media/files/autofilter_beside_excel_table.xlsx");

    I.clickToolbarTab("data");
    I.clickButton("table/filter");

    alert.waitForVisible("Filter cannot be applied to another filtered range.");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");


// ----------------------------------------------------------------------------

Scenario("[C85610][C86432][C86433][C86434][C85611] Excel table reference - Can use special item specifier [#All]", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/simple_table_reference.xlsx");

    //C85610
    await spreadsheet.selectCell("G2");
    spreadsheet.enterCellText("=COUNTA(Cities[#All])", { leave: "stay" });
    spreadsheet.waitForActiveCell("G2", { display: "56", value: 56, formula: "COUNTA(Cities[#All])" });

    //C86432
    await spreadsheet.selectCell("G3");
    spreadsheet.enterCellText("=COUNTA(Cities[#DATA])", { leave: "stay" });
    spreadsheet.waitForActiveCell("G3", { display: "52", value: 52, formula: "COUNTA(Cities[#Data])" });

    //C86433
    await spreadsheet.selectCell("G4");
    spreadsheet.enterCellText("=COUNTA(Cities[#HEADERS])", { leave: "stay" });
    spreadsheet.waitForActiveCell("G4", { display: "4", value: 4, formula: "COUNTA(Cities[#Headers])" });

    //C86434
    await spreadsheet.selectCell("G5");
    spreadsheet.enterCellText("=COUNTA(Cities[#Totals])", { leave: "stay" });
    spreadsheet.waitForActiveCell("G5", { display: "1", value: 1, formula: "COUNTA(Cities[#Totals])" });

    //C85611
    await spreadsheet.selectCell("G6");
    spreadsheet.enterCellText("=COUNTA(Cities[#This row])", { leave: "stay" });
    spreadsheet.waitForActiveCell("G6", { display: "4", value: 4, formula: "COUNTA(Cities[@])" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
