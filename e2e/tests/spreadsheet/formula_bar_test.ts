/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Formula Bar");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C15615] Formula bar - Display 'Formula value' without 'Edit mode'", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a number and a formula
    spreadsheet.enterCellText("42");
    spreadsheet.enterCellText("=A1*2");
    spreadsheet.enterCellText("=A2*2", { leave: "matrix" });

    // cell A1: expect number in cell and formula bar
    await spreadsheet.selectCell("A1", { value: 42, display: "42", formula: null });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "42");

    // cell A2: expect number in cell and formula in formula bar
    await spreadsheet.selectCell("A2", { value: 84, display: "84", formula: "A1*2" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "=A1*2");

    // cell A3: expect number in cell and matrix formula in formula bar
    await spreadsheet.selectCell("A3", { value: 168, display: "168", formula: "A2*2" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=A2*2}");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114382] Formula bar - Autocomplete function names", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // start to type a formula in the formula bar
    I.click({ css: ".view-pane.formula-pane textarea" });
    I.type("=su");

    // expect a popup menu to appear
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "func:SUBSTITUTE" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "func:SUM" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "func:SUMPRODUCT" });
    I.dontSeeElement({ docs: "button", inside: "popup-menu", value: "func:ABS" });

    // select the SUM function
    I.click({ docs: "button", inside: "popup-menu", value: "func:SUM" });
    I.waitForVisible({ docs: "popup", filter: ".signature-tooltip", find: ".function-name", withText: "SUM" });

    // expect cell edit mode with prefilled formula, insert parameters
    I.waitForValue({ css: ".view-pane.formula-pane textarea" }, "=SUM()");
    I.waitForFocus({ css: ".view-pane.formula-pane textarea" }, 1, 100);
    I.type("1,3");
    I.waitForValue({ css: ".view-pane.formula-pane textarea" }, "=SUM(1,3)");
    I.pressKey("Enter");

    // expect formula and result to appear in the cell
    I.waitForChangesSaved();
    await spreadsheet.selectCell("A1", { value: 4, formula: "SUM(1,3)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: 4, formula: "SUM(1,3)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114383] Formula bar - can expand the formular bar input", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // grab collapsed height of formula pane
    I.waitForElement({ docs: "control", key: "view/formulapane/toggle/height", state: false });
    const height1 = await I.grabElementBoundingRect({ css: ".view-pane.formula-pane" }, "height");

    // expand formula pane
    I.clickButton("view/formulapane/toggle/height");
    I.waitForElement({ docs: "control", key: "view/formulapane/toggle/height", state: true });
    const height2 = await I.grabElementBoundingRect({ css: ".view-pane.formula-pane" }, "height");
    expect(height2).to.be.greaterThan(height1);

    // shrink formula pane again
    I.clickButton("view/formulapane/toggle/height");
    I.waitForElement({ docs: "control", key: "view/formulapane/toggle/height", state: false });
    const height3 = await I.grabElementBoundingRect({ css: ".view-pane.formula-pane" }, "height");
    expect(height3).to.be.equal(height1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114386] Formula bar - Of a selected range in a column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a few numbers in column A
    spreadsheet.enterCellText("3");
    spreadsheet.enterCellText("5");
    spreadsheet.enterCellText("2");
    spreadsheet.enterCellText("6");

    // select A2:A3, insert AutoSum
    await spreadsheet.selectRange("A2:A3");
    I.clickButton("cell/autoformula", { pane: "formula-pane", value: "SUM" }); // use button in formula bar
    I.waitForChangesSaved();

    // expect formula and result to appear
    I.pressKeys("Ctrl+End");
    spreadsheet.waitForActiveCell("A5", { value: 7, formula: "SUM(A2:A3)" });

    // expect same contents after reload
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A5", { value: 7, formula: "SUM(A2:A3)" });

    I.closeDocument();
}).tag("smoketest");

// ----------------------------------------------------------------------------

Scenario("[C290151] Formula bar - Sum automatically with an empty document", ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert AutoSum in empty sheet
    I.clickButton("cell/autoformula", { pane: "formula-pane", value: "SUM" }); // use button in formula bar

    // expect that cell edit mode starts
    spreadsheet.waitForCellEditMode("=SUM()");
    I.waitForVisible({ docs: "popup", filter: ".signature-tooltip", find: ".function-name", withText: "SUM" });
    I.pressKeys("2*Escape");
    spreadsheet.waitForActiveCell("A1", { value: null, formula: null });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114387] Formula bar - with a protected sheet", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    I.waitForEnabled({ css: ".view-pane.formula-pane textarea" });

    // protect the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });

    // expect that <textarea> in formula pane is disabled
    I.waitForElement({ css: ".view-pane.formula-pane textarea:disabled" });

    // unprotect the sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: true });

    // expect that <textarea> in formula pane is enabled again
    I.waitForEnabled({ css: ".view-pane.formula-pane textarea" });

    I.closeDocument();
}).tag("stable");
