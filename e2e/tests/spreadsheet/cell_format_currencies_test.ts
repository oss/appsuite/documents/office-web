/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { classSel } from "../../utils/locatorutils";

// functions ==================================================================

/**
 * Checks the preview cells in the "Custom Currency" modal dialog.
 *
 * @param pos
 *  The label of the preview cell for positive values.
 *
 * @param neg
 *  The label of the preview cell for negative values.
 *
 * @param zero
 *  The label of the preview cell for zero.
 *
 * @param red
 *  If set to `true`, the negative preview cell is expected to have red color.
 */
function expectPreviewCells(pos: string, neg: string, zero: string, red: boolean): void {
    const { I } = inject();
    I.waitForVisible({ docs: "dialog", find: ".preview-cell[data-type=positive]", withText: pos });
    I.waitForVisible({ docs: "dialog", find: ".preview-cell[data-type=negative]" + classSel("negative-red", red), withText: neg });
    I.waitForVisible({ docs: "dialog", find: ".preview-cell[data-type=zero]", withText: zero });
}

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell format > Currencies");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[DOCS-3327] Custom currency symbols", ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("$-1042", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "($1,042)", renderColor: "#ff0000" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    // open and check "Custom Currency" dialog
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.waitForVisible({ docs: "control", inside: "dialog", type: "dropdownlist", key: "currency-symbol", state: "symbol:USD" });
    I.waitForVisible({ docs: "control", inside: "dialog", type: "spinfield", key: "frac-digits", state: 0 });
    I.waitForVisible({ docs: "control", inside: "dialog", type: "checkbox", key: "group-separator", state: true });
    I.waitForVisible({ docs: "control", inside: "dialog", type: "checkbox", key: "accounting", state: false });
    I.waitForVisible({ docs: "control", inside: "dialog", type: "checkbox", key: "iso-symbol", state: false });
    I.waitForVisible({ docs: "control", inside: "dialog", type: "checkbox", key: "negative-red", state: true });
    expectPreviewCells("$1,042", "($1,042)", "$0", true);

    // select another builtin currency
    I.clickOptionButton("currency-symbol", "symbol:EUR", { inside: "dialog" });
    expectPreviewCells("1,042 €", "-1,042 €", "0 €", true);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "-1,042 €", renderColor: "#ff0000" });

    // select another localized currency
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.clickOptionButton("currency-symbol", "locale:af-ZA", { inside: "dialog" });
    expectPreviewCells("R 1,042", "-R 1,042", "R 0", true);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "-R 1,042", renderColor: "#ff0000" });

    // check quick-search field
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.clickButton("currency-symbol", { inside: "dialog", caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu",  value: "symbol:USD" });
    I.waitForVisible({ docs: "button", inside: "popup-menu",  value: "blind:USD" });
    I.waitForVisible({ docs: "button", inside: "popup-menu",  value: "symbol:EUR" });
    I.waitForVisible({ docs: "button", inside: "popup-menu",  value: "blind:EUR" });
    I.waitForVisible({ docs: "button", inside: "popup-menu",  value: "symbol:GBP" });
    I.waitForVisible({ docs: "button", inside: "popup-menu",  value: "blind:GBP" });
    I.pressKeys("E", "U");
    I.dontSeeElement({ docs: "button", inside: "popup-menu",  value: "symbol:USD" });
    I.dontSeeElement({ docs: "button", inside: "popup-menu",  value: "blind:USD" });
    I.seeElement({ docs: "button", inside: "popup-menu",  value: "symbol:EUR" });
    I.seeElement({ docs: "button", inside: "popup-menu",  value: "blind:EUR" });
    I.dontSeeElement({ docs: "button", inside: "popup-menu",  value: "symbol:GBP" });
    I.dontSeeElement({ docs: "button", inside: "popup-menu",  value: "blind:GBP" });
    I.click({ docs: "button", inside: "popup-menu",  value: "symbol:EUR" });
    I.waitForVisible({ docs: "control", inside: "dialog", type: "dropdownlist", key: "currency-symbol", state: "symbol:EUR" });
    expectPreviewCells("1,042 €", "-1,042 €", "0 €", true);

    // increase decimal places
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "frac-digits", find: ".spin-up" });
    expectPreviewCells("1,042.0 €", "-1,042.0 €", "0.0 €", true);
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "frac-digits", find: ".spin-up" });
    expectPreviewCells("1,042.00 €", "-1,042.00 €", "0.00 €", true);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "-1,042.00 €", renderColor: "#ff0000" });

    // disable group separator
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "group-separator" });
    expectPreviewCells("1042.00 €", "-1042.00 €", "0.00 €", true);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "-1042.00 €", renderColor: "#ff0000" });

    // switch to ISO symbol
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "iso-symbol" });
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "frac-digits", find: ".spin-down" });
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "frac-digits", find: ".spin-down" });
    expectPreviewCells("1042 EUR", "-1042 EUR", "0 EUR", true);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "-1042 EUR", renderColor: "#ff0000" });

    // switch to red text color
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "negative-red" });
    expectPreviewCells("1042 EUR", "-1042 EUR", "0 EUR", false);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: "-1042 EUR", renderColor: "#000000" });

    // switch to accounting format
    I.clickOptionButton("cell/numberformat/code", ":custom");
    dialogs.waitForVisible("io-ox-office-custom-currency-dialog");
    I.clickOptionButton("currency-symbol", "symbol:USD", { inside: "dialog" });
    I.waitForAndClick({ docs: "control", inside: "dialog", key: "accounting" });
    expectPreviewCells("USD1042", "USD(1042)", "USD-", false);
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    spreadsheet.waitForActiveCell("A1", { value: -1042, display: " USD(1042)", renderColor: "#000000" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "accounting" });

    I.closeDocument();
}).tag("stable");
