/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Autofill");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85613] Autofill over columns - day of week", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("Friday", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: "Friday" });
    await spreadsheet.selectCell("A2", { value: "Saturday" });
    await spreadsheet.selectCell("A3", { value: "Sunday" });
    await spreadsheet.selectCell("A4", { value: "Monday" });

    // check after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: "Friday" });
    await spreadsheet.selectCell("A2", { value: "Saturday" });
    await spreadsheet.selectCell("A3", { value: "Sunday" });
    await spreadsheet.selectCell("A4", { value: "Monday" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85742] Autofill over columns - month", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing
    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("October", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: "October" });
    await spreadsheet.selectCell("A2", { value: "November" });
    await spreadsheet.selectCell("A3", { value: "December" });
    await spreadsheet.selectCell("A4", { value: "January" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8253] [C8254] Autofill - date", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing
    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("12/29/2020", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 44194, display: "12/29/2020" });
    await spreadsheet.selectCell("A2", { value: 44195, display: "12/30/2020" });
    await spreadsheet.selectCell("A3", { value: 44196, display: "12/31/2020" });
    await spreadsheet.selectCell("A4", { value: 44197, display: "01/01/2021" });

    // create a series
    I.pressKeys("Ctrl+Home");
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 44194, display: "12/29/2020" });
    await spreadsheet.selectCell("B1", { value: 44195, display: "12/30/2020" });
    await spreadsheet.selectCell("C1", { value: 44196, display: "12/31/2020" });
    await spreadsheet.selectCell("D1", { value: 44197, display: "01/01/2021" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85729] Autofill over columns - hour", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing
    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("10:00 PM", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { display: "10:00 PM" });
    await spreadsheet.selectCell("A2", { display: "11:00 PM" });
    await spreadsheet.selectCell("A3", { display: "12:00 AM" });
    await spreadsheet.selectCell("A4", { display: "1:00 AM" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8249] [C8250] Autofill - text", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("text1", { leave: "stay" });

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: "text1" });
    await spreadsheet.selectCell("A2", { value: "text2" });
    await spreadsheet.selectCell("A3", { value: "text3" });
    await spreadsheet.selectCell("A4", { value: "text4" });

    // create a series
    I.pressKeys("Ctrl+Home");
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: "text1" });
    await spreadsheet.selectCell("B1", { value: "text2" });
    await spreadsheet.selectCell("C1", { value: "text3" });
    await spreadsheet.selectCell("D1", { value: "text4" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8251] [C8252] Autofill - number", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("1", { leave: "stay" });

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 1 });
    await spreadsheet.selectCell("A2", { value: 1 });
    await spreadsheet.selectCell("A3", { value: 1 });
    await spreadsheet.selectCell("A4", { value: 1 });

    // create a series
    I.pressKeys("Ctrl+Home");
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 1 });
    await spreadsheet.selectCell("B1", { value: 1 });
    await spreadsheet.selectCell("C1", { value: 1 });
    await spreadsheet.selectCell("D1", { value: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8255] [C8256] [C8257] [C8258] [C8259] [C8260] [C15593] [C15594] Autofill - text with cell attribute", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing
    //C8255 duplicate to scenarios [C8251] [C8252] Autofill - number?

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("text", { leave: "stay" });
    I.waitForChangesSaved();

    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.clickOptionButton("cell/alignhor", "right");
    I.waitForChangesSaved();
    I.clickOptionButton("cell/fillcolor", "yellow");
    I.waitForChangesSaved();
    I.clickOptionButton("cell/border/flags", "outer");
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    for (const address of "A1 A2 A3 A4".split(" ")) {
        await spreadsheet.selectCell(address, { value: "text" });
        I.waitForVisible({ docs: "control", key: "character/bold", state: true });
        I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "right" });
        I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });
        I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    }

    // create a series
    I.pressKeys("Ctrl+Home");
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    for (const address of "A1 B1 C1 D1".split(" ")) {
        await spreadsheet.selectCell(address, { value: "text" });
        I.waitForVisible({ docs: "control", key: "character/bold", state: true });
        I.waitForVisible({ docs: "control", key: "cell/alignhor", state: "right" });
        I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });
        I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    }

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C35084] [C35085] Autofill - number (ODS)", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing

    await I.loginAndOpenDocument("media/files/empty.ods");
    spreadsheet.enterCellText("1", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 1 });
    await spreadsheet.selectCell("A2", { value: 1 });
    await spreadsheet.selectCell("A3", { value: 1 });
    await spreadsheet.selectCell("A4", { value: 1 });

    // create a series
    I.pressKeys("Ctrl+Home");
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 1 });
    await spreadsheet.selectCell("B1", { value: 1 });
    await spreadsheet.selectCell("C1", { value: 1 });
    await spreadsheet.selectCell("D1", { value: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C38810] Autofill over row - date (ODS)", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing

    await I.loginAndOpenDocument("media/files/empty.ods");
    spreadsheet.enterCellText("12/29/2020", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: 44194, display: "12/29/2020" });
    await spreadsheet.selectCell("B1", { value: 44195, display: "12/30/2020" });
    await spreadsheet.selectCell("C1", { value: 44196, display: "12/31/2020" });
    await spreadsheet.selectCell("D1", { value: 44197, display: "01/01/2021" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C38811] Autofill over rows - text with cell attribute (background color) (ODS)", async ({ I, spreadsheet }) => {
    //TODO: Reload is missing

    await I.loginAndOpenDocument("media/files/empty.ods");
    spreadsheet.enterCellText("text", { leave: "stay" });
    I.waitForChangesSaved();
    I.clickOptionButton("cell/fillcolor", "yellow");
    I.waitForChangesSaved();

    // create a series
    const colWidth = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" }, "width");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 3 * colWidth, 0);
    I.waitForChangesSaved();

    // verify generated cells
    for (const address of "A1 B1 C1 D1".split(" ")) {
        await spreadsheet.selectCell(address, { value: "text" });
        I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "yellow" });
    }

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85629] Autofill over columns - day of week (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");
    spreadsheet.enterCellText("Friday", { leave: "stay" });
    I.waitForChangesSaved();

    // create a series
    const rowHeight = await I.grabElementBoundingRect({ spreadsheet: "row-header", pos: "1" }, "height");
    I.dragMouseOnElement({ spreadsheet: "selection-range", autofill: true }, 0, 3 * rowHeight);
    I.waitForChangesSaved();

    // verify generated cells
    await spreadsheet.selectCell("A1", { value: "Friday" });
    await spreadsheet.selectCell("A2", { value: "Saturday" });
    await spreadsheet.selectCell("A3", { value: "Sunday" });
    await spreadsheet.selectCell("A4", { value: "Monday" });

    // check after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: "Friday" });
    await spreadsheet.selectCell("A2", { value: "Saturday" });
    await spreadsheet.selectCell("A3", { value: "Sunday" });
    await spreadsheet.selectCell("A4", { value: "Monday" });

    I.closeDocument();
}).tag("stable");
