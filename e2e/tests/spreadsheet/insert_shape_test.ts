/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Shape");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C107116] Insert shape", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // start shape insertion mode
    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.clickOptionButton("shape/insert", "rect");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: true });

    // insert a shape in the sheet by dragging mouse
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // check that the shape exists
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing", { withText: "Shape" });
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.equal(301);
    expect(rect1.height).to.equal(301);

    // check again after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape" });
    expect(rect2.width).to.equal(301);
    expect(rect2.height).to.equal(301);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C310959] Insert shape and type some text", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // immediately type something
    I.type("Lorxem");
    I.waitForChangesSaved();
    I.waitForText("Lorxem", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });

    // check that DEL key in text edit mode does not delete the shape
    I.pressKeys("3*ArrowLeft", "Delete");
    I.waitForText("Lorem", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });

    // leave text edit mode
    I.pressKeys("2*Escape");
    spreadsheet.waitForCellFocus();

    // click at the end of the paragraph to re-enter text edit mode
    I.clickOnElement({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 }, { point: "right" });
    I.type("ipsum");
    I.waitForChangesSaved();
    I.waitForText("Loremipsum", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });

    // check again after reload
    await I.reopenDocument();
    I.waitForText("Loremipsum", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107117] Delete shape", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing");

    // press DEL to delete the shape
    I.pressKey("Delete");
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0 });
    I.waitForToolbarTab("format");

    // Click Undo to restore the shape
    I.clickButton("document/undo");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing");

    // click the Delete button
    I.clickButton("drawing/delete");
    I.waitForInvisible({ spreadsheet: "drawing", pos: 0 });
    I.waitForToolbarTab("format");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107120] [C107121] [C107122] Shape formatting", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing");

    // select a border style
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "sch accent1 shade 50000" });
    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "accent1" });
    I.clickOptionButton("drawing/border/style", "solid:thick");
    I.clickOptionButton("drawing/border/color", "red");
    I.clickOptionButton("drawing/fill/color", "yellow");
    I.waitForChangesSaved();

    // unselect/select drawing, check border style control
    I.pressKey("Escape");
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "yellow" });
    I.pressKey("Escape");

    // check after reload
    await I.reopenDocument();
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "yellow" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107123] Change shape width and height", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });

    // drag mouse to resize the shape
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "shape", find: ".resizers [data-pos=br]" }, 100, 50);
    I.waitForChangesSaved();
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape" });
    expect(rect1.width).to.equal(401);
    expect(rect1.height).to.equal(351);

    // check after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape" });
    expect(rect2.width).to.equal(401);
    expect(rect2.height).to.equal(351);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107124] [C115506] Change text attributes in shape", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // type something, and apply formatting
    I.type("Lorem ipsum dolor");
    I.pressKeys("Ctrl+ArrowLeft", "ArrowLeft", "Shift+Ctrl+ArrowLeft");
    I.clickToolbarTab("format");
    I.clickButton("character/bold");
    I.clickButton("view/drawing/alignment/menu", { caret: true });
    I.clickButton("paragraph/alignment", { inside: "popup-menu", value: "right" });
    I.pressKeys("2*Escape");
    I.waitForChangesSaved();

    // check formatting (wait for asynchronous text formatter)
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 1 });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 0 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 1 }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 2 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 }, { textAlign: "right" });

    // check again after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 0 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 1 }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 2 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 }, { textAlign: "right" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C125291] Insert a bullet list into a shape", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // type something, and apply a list style
    I.type("Lorem");
    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "paragraph/list/bullet", state: null });
    I.clickButton("paragraph/list/bullet");
    I.waitForVisible({ docs: "control", key: "paragraph/list/bullet", state: "small-filled-circle" });
    I.pressKey("Enter");
    I.type("ipsum");
    I.waitForChangesSaved();

    // check for bullet points (wait for asynchronous text formatter)
    I.waitForText("\u2022", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0, find: ">.list-label>span" });
    I.waitForText("\u2022", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 1, find: ">.list-label>span" });

    // change bullet point type (only the selected bullet point will be changed)
    I.clickOptionButton("paragraph/list/bullet", "check-mark");
    I.waitForText("\u2022", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0, find: ">.list-label>span" });
    I.waitForText("\u2713", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 1, find: ">.list-label>span" });

    // check again after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });
    I.waitForText("\u2022", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0, find: ">.list-label>span" });
    I.waitForText("\u2713", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 1, find: ">.list-label>span" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C125296] Insert a numbered list into a shape", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // type something, and apply a list style
    I.type("Lorem");
    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "paragraph/list/numbered", state: null });
    I.clickButton("paragraph/list/numbered");
    I.waitForVisible({ docs: "control", key: "paragraph/list/numbered", state: "arabicPeriod" });
    I.pressKey("Enter");
    I.type("ipsum");
    I.waitForChangesSaved();

    // check for bullet points (wait for asynchronous text formatter)
    I.waitForText("1.", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0, find: ">.list-label>span" });
    I.waitForText("2.", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 1, find: ">.list-label>span" });

    // change second bullet point type (the entire list will be changed)
    I.clickOptionButton("paragraph/list/numbered", "alphaUcParenBoth");
    I.waitForText("(A)", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0, find: ">.list-label>span" });
    I.waitForText("(B)", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 1, find: ">.list-label>span" });

    // check again after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });
    I.waitForText("(A)", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 0, find: ">.list-label>span" });
    I.waitForText("(B)", 1, { spreadsheet: "drawing", pos: 0, type: "shape", para: 1, find: ">.list-label>span" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107127] [C107128] [C107129] [C107130] Change Z order of shapes", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert multiple shapes into the sheet
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(400, 300, 700, 600);
    I.waitForToolbarTab("drawing");
    I.pressKeys("1", "2*Escape");

    I.clickToolbarTab("insert");
    I.clickButton("shape/insert");
    I.waitForPopupMenuVisible();
    I.waitForAndClick({ css: ".popup-content [data-section='rect'] a[data-value='rect']" });

    I.dragMouseAt(350, 350, 650, 650);
    I.waitForToolbarTab("drawing");
    I.pressKeys("2", "2*Escape");

    I.clickToolbarTab("insert");
    I.clickButton("shape/insert");
    I.waitForPopupMenuVisible();
    I.waitForAndClick({ css: ".popup-content [data-section='rect'] a[data-value='rect']" });
    I.dragMouseAt(300, 400, 600, 700);
    I.waitForToolbarTab("drawing");
    I.pressKeys("3", "2*Escape");

    I.clickToolbarTab("insert");
    I.clickButton("shape/insert");
    I.waitForPopupMenuVisible();
    I.waitForAndClick({ css: ".popup-content [data-section='rect'] a[data-value='rect']" });
    I.dragMouseAt(250, 450, 550, 750);
    I.waitForToolbarTab("drawing");
    I.pressKeys("4", "2*Escape");

    // check initial shape order
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // bring shape "2" to front
    I.clickOnElement({ spreadsheet: "drawing", pos: 1 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // move shape "3" one forward
    I.clickOnElement({ spreadsheet: "drawing", pos: 1 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "forward" });
    I.waitForChangesSaved();
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // move shape "2" one backward
    I.clickOnElement({ spreadsheet: "drawing", pos: 3 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "backward" });
    I.waitForChangesSaved();
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // move shape "3" to background
    I.clickOnElement({ spreadsheet: "drawing", pos: 3 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // check again after reload
    await I.reopenDocument();
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115507] [C115509] [C115510] [C115511] [C115512] Shape rotation and flipping", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a shape into the sheet
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" }, { cursor: "crosshair" });

    // rotate the shape by 30 degress to the right
    const r = 450 - (await I.grabPointInElement({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" })).y;
    const rad = 30 / 180 * Math.PI;
    const dx = Math.round(r * Math.sin(rad));
    const dy = Math.round(r * Math.cos(rad));
    I.pressMouseButtonOnElement({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" });
    I.moveMouseTo(450 + dx, 450 - dy);
    I.waitForText("30\xb0", 1, { spreadsheet: "drawing", pos: 0, find: ".angle-tooltip" });
    I.releaseMouseButton();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" });
    const matrix1 = await I.grab2dCssTransformationFrom({ spreadsheet: "drawing", pos: 0 });
    expect(matrix1.rotateDeg).to.equal(30);
    expect(matrix1.scaleX).to.equal(1);
    expect(matrix1.scaleY).to.equal(1);

    // flip shape vertically
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/flipv", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" });
    const matrix2 = await I.grab2dCssTransformationFrom({ spreadsheet: "drawing", pos: 0 });
    expect(matrix2.rotateDeg).to.equal(30);
    expect(matrix2.scaleX).to.equal(1);
    expect(matrix2.scaleY).to.equal(-1);

    // flip shape horizontally
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" });
    const matrix3 = await I.grab2dCssTransformationFrom({ spreadsheet: "drawing", pos: 0 });
    expect(matrix3.rotateDeg).to.equal(210);
    expect(matrix3.scaleX).to.equal(1);
    expect(matrix3.scaleY).to.equal(1);

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0 });
    const matrix4 = await I.grab2dCssTransformationFrom({ spreadsheet: "drawing", pos: 0 });
    expect(matrix4.rotateDeg).to.equal(210);
    expect(matrix4.scaleX).to.equal(1);
    expect(matrix4.scaleY).to.equal(1);

    // rotate the shape by 90 degress CW
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: 90 });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" });
    const matrix5 = await I.grab2dCssTransformationFrom({ spreadsheet: "drawing", pos: 0 });
    expect(matrix5.rotateDeg).to.equal(300);
    expect(matrix5.scaleX).to.equal(1);
    expect(matrix5.scaleY).to.equal(1);

    // rotate the shape by 90 degress CCW
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: -90 });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, find: ".rotate-handle" });
    const matrix6 = await I.grab2dCssTransformationFrom({ spreadsheet: "drawing", pos: 0 });
    expect(matrix6.rotateDeg).to.equal(210);
    expect(matrix6.scaleX).to.equal(1);
    expect(matrix6.scaleY).to.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115047] Insert shape (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // start shape insertion mode
    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.clickOptionButton("shape/insert", "rect");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: true });

    // insert a shape in the sheet by dragging mouse
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // check that the shape exists
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    expect(rect1.width).to.equal(301);
    expect(rect1.height).to.equal(301);

    // check again after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "shape" });
    expect(rect2.width).to.equal(301);
    expect(rect2.height).to.equal(301);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115114] [C125275] Change text attributes in shape (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // type something, and apply formatting
    I.type("Lorem ipsum dolor");
    I.pressKeys("Ctrl+ArrowLeft", "ArrowLeft", "Shift+Ctrl+ArrowLeft");
    I.clickToolbarTab("format");
    I.clickButton("character/bold");
    I.clickButton("view/drawing/alignment/menu", { caret: true });
    I.clickButton("paragraph/alignment", { inside: "popup-menu", value: "right" });
    I.pressKeys("2*Escape");
    I.waitForChangesSaved();

    // check formatting (wait for asynchronous text formatter)
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 1 });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 0 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 1 }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 2 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 }, { textAlign: "right" });

    // check again after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 0 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 1 }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0, span: 2 }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ spreadsheet: "drawing", pos: 0, type: "shape", para: 0 }, { textAlign: "right" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115048] [C115049] [C115050] Shape formatting (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert a shape in the sheet by dragging mouse
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", selected: true });
    I.waitForToolbarTab("drawing");

    // select a border style
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });
    I.clickOptionButton("drawing/border/style", "solid:thick");
    I.clickOptionButton("drawing/border/color", "red");
    I.clickOptionButton("drawing/fill/color", "yellow");
    I.waitForChangesSaved();

    // unselect/select drawing, check border style control
    I.pressKey("Escape");
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "yellow" });
    I.pressKey("Escape");

    // check after reload
    await I.reopenDocument();
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "yellow" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173488] [C173489] Change Z order of shapes (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert multiple shapes into the sheet
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "rect");
    I.dragMouseAt(400, 300, 700, 600);
    I.waitForToolbarTab("drawing");
    I.pressKeys("1", "2*Escape");

    I.clickToolbarTab("insert");
    I.clickButton("shape/insert");
    I.waitForPopupMenuVisible();
    I.waitForAndClick({ css: ".popup-content [data-section='shape'] a[data-value='rect']" });
    I.dragMouseAt(350, 350, 650, 650);
    I.waitForToolbarTab("drawing");
    I.pressKeys("2", "2*Escape");

    I.clickToolbarTab("insert");
    I.clickButton("shape/insert");
    I.waitForPopupMenuVisible();
    I.waitForAndClick({ css: ".popup-content [data-section='shape'] a[data-value='rect']" });
    I.dragMouseAt(300, 400, 600, 700);
    I.waitForToolbarTab("drawing");
    I.pressKeys("3", "2*Escape");

    I.clickToolbarTab("insert");
    I.clickButton("shape/insert");
    I.waitForPopupMenuVisible();
    I.waitForAndClick({ css: ".popup-content [data-section='shape'] a[data-value='rect']" });
    I.dragMouseAt(250, 450, 550, 750);
    I.waitForToolbarTab("drawing");
    I.pressKeys("4", "2*Escape");

    // check initial shape order
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // bring shape "2" to front
    I.clickOnElement({ spreadsheet: "drawing", pos: 1 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // move shape "3" one forward
    I.clickOnElement({ spreadsheet: "drawing", pos: 1 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "forward" });
    I.waitForChangesSaved();
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // move shape "2" one backward
    I.clickOnElement({ spreadsheet: "drawing", pos: 3 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "backward" });
    I.waitForChangesSaved();
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // move shape "3" to background
    I.clickOnElement({ spreadsheet: "drawing", pos: 3 }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/order", { inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    // check again after reload
    await I.reopenDocument();
    I.waitForText("3", 1, { spreadsheet: "drawing", pos: 0, para: 0 });
    I.waitForText("1", 1, { spreadsheet: "drawing", pos: 1, para: 0 });
    I.waitForText("4", 1, { spreadsheet: "drawing", pos: 2, para: 0 });
    I.waitForText("2", 1, { spreadsheet: "drawing", pos: 3, para: 0 });

    I.closeDocument();
}).tag("stable");
