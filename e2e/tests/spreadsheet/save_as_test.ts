/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > File > Save in drive");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C115057] Auto save", ({ I, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/saveas/native/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "document/saveas/encrypted/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "document/saveas/template/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control",  key: "document/saveas/pdf/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control",  key: "document/autosave", inside: "popup-menu" });
    I.clickButton("document/autosave", { inside: "popup-menu" });

    alert.waitForVisible("In OX Documents all your changes are saved automatically through AutoSave. It is not necessary to save your work manually.");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115054] Save as", ({ I, dialogs, drive, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("cat");

    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });

    I.waitForVisible({ docs: "control", key: "document/saveas/native/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "document/saveas/encrypted/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "document/saveas/template/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control",  key: "document/saveas/pdf/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control",  key: "document/autosave", inside: "popup-menu" });
    I.clickButton("document/saveas/native/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: ".subfolders li.folder", withText: "My files" });

    // Change file name
    I.fillField({ id: "save-as-filename" }, "fritzTheCat");

    // Click on the 'OK' button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The newly created file is opened for editing
    I.waitForDocumentImport({ expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: "fritzTheCat" });

    // close the document
    I.closeDocument();

    // The document saved as fritzTheCat.xlsx and visible in Drive.
    drive.waitForFile("fritzTheCat.xlsx");

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115056] Save as template", ({ I, drive, dialogs, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("cat");

    // click the "Save as template" menu entry
    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });
    I.clickButton("document/saveas/template/dialog", { inside: "popup-menu" });

    // Wait for dialog
    dialogs.waitForVisible();

    // Default file name proposal is is the original document name.
    I.seeInField({ docs: "dialog", find: "#save-as-filename" }, "unnamed");

    // The document extension is shown in the dialog titel.
    I.seeElement({ docs: "dialog", area: "header", find: ".modal-title", withText: "xltx" });

    // The document will be saved in in 'My files > Document > Templates'.
    I.seeElement({ docs: "dialog", find: ".folder.selected", withText: "My files / Documents / Templates" });

    // Change file name
    I.fillField({ docs: "dialog", find: "input#save-as-filename" }, "fritzTheCat");

    // Click on the "Save" button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    alert.waitForVisible('The document was saved as a template "fritzTheCat.xltx" in folder "My files', 5);
    //TODO check the whole string "My files / Documents / Templates". ->The document was saved as a template "fritzTheCat.xltx" in folder "My files

    // The newly created file is opened for editing
    I.waitForDocumentImport({ expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: "fritzTheCat" });

    // close the document
    I.closeDocument();

    // Go to the Templates folder in Drive
    drive.doubleClickFolder("Documents");
    drive.doubleClickFolder("Templates");

    // The file is saved as template
    drive.waitForFile("fritzTheCat.xltx");
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C310382] Save as with canceling a create folder", ({ I, dialogs, drive, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("cat");

    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "document/saveas/native/dialog" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "document/saveas/encrypted/dialog" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "document/saveas/template/dialog" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "document/saveas/pdf/dialog" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "document/autosave" });
    I.clickButton("document/saveas/native/dialog", { inside: "popup-menu" });

    I.waitForAndClick({ docs: "dialog", area: "footer", find: '[data-action="create"]' }, 3);
    I.wait(1);
    I.click({ css: ".modal[role='dialog'][data-point='io.ox/core/folder/add-popup'] .modal-footer .btn[data-action='cancel']" });

    I.pressKey("Tab");
    I.waitForAndClick({ docs: "dialog", find: ".subfolders li.folder", withText: "My files" });

    // Change file name
    I.fillField({ id: "save-as-filename" }, "fritzTheCat");

    // Click on the 'OK' button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The newly created file is opened for editing
    I.waitForDocumentImport({ expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: "fritzTheCat" });

    // close the document
    I.closeDocument();

    // The document saved as fritzTheCat.xlsx and visible in Drive.
    drive.waitForFile("fritzTheCat.xlsx");

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115055] Save as (encrypted)", async ({ I, guard, drive, dialogs, spreadsheet }) => {

    const newFileName = "myFileName";

    // open a new spreadsheet document
    I.loginAndHaveNewDocument("spreadsheet");

    // initialize Guard with a random password
    const password = guard.initPassword();

    spreadsheet.enterCellText("Hello");

    // click on 'Save as (encrypted)' menu entry
    I.clickToolbar("File");
    I.clickButton("view/saveas/menu", { caret: true });
    I.clickButton("document/saveas/encrypted/dialog", { inside: "popup-menu" });

    // change the file name
    dialogs.waitForVisible();
    I.type(newFileName);
    dialogs.clickOkButton();

    // the new created encrypted file is opened for editing
    I.waitForDocumentImport({ password, expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: newFileName });

    // the open document is encrypted
    I.waitForVisible({ docs: "control", pane: "top-pane", key: "app/encrypted" });

    // close the document
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument();

    // two files are created in drive
    drive.waitForFile(fileDesc);
    drive.waitForFile(`${newFileName}.xlsx.pgp`);
}).tag("stable");
