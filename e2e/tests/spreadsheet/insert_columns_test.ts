/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Columns");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[DOCS-4921] Insert columns after hidden column", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // hide column B
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });
    I.waitForChangesSaved();

    // check that column B is hidden
    I.dontSeeElement({ spreadsheet: "col-header", pos: "B" });

    // insert new column before column C
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.waitForContextMenuVisible();
    I.clickButton("column/insert", { inside: "context-menu" });
    I.waitForChangesSaved();

    // DOCS-4921: check that column B is still hidden
    I.dontSeeElement({ spreadsheet: "col-header", pos: "B" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");
