/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8102] Create new document", ({ I, drive, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    I.closeDocument();
    drive.waitForFile("unnamed.xlsx");
}).tag("smoketest");

// ----------------------------------------------------------------------------

Scenario("[C8103] Close new document", ({ I }) => {

    // create a new document, DO NOT add any contents
    I.loginAndHaveNewDocument("spreadsheet");
    I.closeDocument();

    // no new file is created in Drive
    I.dontSeeElement({ css: ".file-list-view .list-item:not(.file-type-folder)" });
}).tag("smoketest");

// ----------------------------------------------------------------------------

Scenario("[C204750] Create new document with 2 sheets", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.insertSheet();
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    await I.reopenDocument();
    spreadsheet.waitForActiveSheet(1);
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    I.closeDocument();
}).tag("stable");
