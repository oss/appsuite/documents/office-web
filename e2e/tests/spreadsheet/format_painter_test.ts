/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell format > Format painter");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8143] Format painter transfer text format (bold)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // prepare cell A1 with bold text
    spreadsheet.enterCellText("Bold", { leave: "stay" });
    I.clickButton("character/bold");
    I.waitForChangesSaved();

    // prepare cell A2 with normal text
    await spreadsheet.selectCell("B1");
    spreadsheet.enterCellText("Normal", { leave: "stay" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });

    // start format painter in cell A1
    await spreadsheet.selectCell("A1");
    I.clickButton("cell/painter");
    I.waitForVisible({ docs: "control", key: "cell/painter", state: true });

    // select cell B1 to paste formatting
    await spreadsheet.clickCell("B1");
    spreadsheet.waitForActiveCell("B1", { value: "Normal" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "cell/painter", state: false });

    await I.reopenDocument();

    await spreadsheet.selectCell("B1", { value: "Normal" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8145] Format painter transfer cell attributes (background color)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/backgroundcolor.xlsx");

    I.clickButton("cell/painter");

    await spreadsheet.selectCell("C1");

    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "light-green" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "light-green" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C15592] Format painter transfer cell attributes (cell styles)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cellstyle.xlsx");

    I.clickButton("cell/painter");

    await spreadsheet.selectCell("C1");

    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Negative" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Negative" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C86443] Format painter transfer number format (%)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/percentage.xlsx");

    I.clickButton("cell/painter");

    await spreadsheet.selectCell("B1", { value: 1, display: "100%" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("B1", { value: 1, display: "100%" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8144] Format painter transfer cell attributes (borders)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/border.xlsx");

    I.clickButton("cell/painter");
    await spreadsheet.selectCell("D2");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("D2");
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
