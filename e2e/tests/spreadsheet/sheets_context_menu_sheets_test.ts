/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Context menus > Sheets");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C38857] Insert sheet", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "document/insertsheet", inside: "context-menu", withText: "Insert sheet" });
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "context-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "context-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "context-menu", withText: "Protect sheet" });

    I.clickButton("document/insertsheet", { inside: "context-menu", withText: "Insert sheet"  });

    //check the active sheet
    I.waitForElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1 });

    await I.reopenDocument();

    //check the active sheet
    I.waitForElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1 });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C38858] Copy sheet", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });

    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "document/insertsheet", inside: "context-menu", withText: "Insert sheet" });
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "context-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "context-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "context-menu", withText: "Protect sheet" });

    I.clickButton("document/copysheet/dialog", { inside: "context-menu", withText: "Copy sheet"  });

    dialogs.waitForVisible();

    //close dialog
    I.fillField({ docs: "dialog", find: ".sheet-name-input input" }, "miau");

    dialogs.clickOkButton();

    //check the cell text
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });

    //check the active sheet and the sheet name
    I.waitForElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "miau", value: 1 });

    await I.reopenDocument();

    //check the cell text
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });

    //check the active sheet and the sheet name
    I.waitForElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "miau", value: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38859] Hide sheet", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1 });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "document/insertsheet", inside: "context-menu", withText: "Insert sheet" });
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "context-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "context-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "document/deletesheet", inside: "context-menu", withText: "Delete sheet" });
    I.waitForElement({ docs: "control", key: "sheet/visible", inside: "context-menu", withText: "Hide sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "context-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "context-menu", withText: "Reorder sheets" });

    I.clickButton("sheet/visible", { inside: "context-menu", withText: "Hide sheet"  });

    //open the context menu
    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });

    //check the new contex menu entry
    I.waitForElement({ docs: "control", key: "document/showsheets/dialog", inside: "context-menu", withText: "Unhide sheets" });

    await I.reopenDocument();

    //open the context menu
    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0 });

    //check the new contex menu entry
    I.waitForElement({ docs: "control", key: "document/showsheets/dialog", inside: "context-menu", withText: "Unhide sheets" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38860] Reorder sheets", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    I.rightClick({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1 });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "document/insertsheet", inside: "context-menu", withText: "Insert sheet" });
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "context-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "context-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "document/deletesheet", inside: "context-menu", withText: "Delete sheet" });
    I.waitForElement({ docs: "control", key: "sheet/visible", inside: "context-menu", withText: "Hide sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "context-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "context-menu", withText: "Reorder sheets" });

    //open the dialog
    I.clickButton("document/movesheets/dialog", { inside: "context-menu", withText: "Reorder sheets" });
    dialogs.waitForVisible();

    //move the sheet2
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[data-action='prev']" });

    I.waitForVisible({ css: "button[data-action=cancel]" });
    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.closeDocument();
}).tag("stable");
