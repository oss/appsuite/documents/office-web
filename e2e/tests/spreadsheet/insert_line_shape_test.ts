/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Shape > Line Shape Formatting");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C107238] [C107239] [C107240] Apply line formatting", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a line shape
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "line");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "connector" });
    I.waitForChangesSaved();

    // select a border style
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "sch accent1 shade 50000" });
    I.waitForVisible({ docs: "control", key: "drawing/connector/arrows", state: "none:none" });
    I.clickOptionButton("drawing/border/style", "solid:thick");
    I.clickOptionButton("drawing/border/color", "red");
    I.clickOptionButton("drawing/connector/arrows", "triangle:triangle");
    I.waitForChangesSaved();

    // unselect/select drawing, check border style control
    I.pressKey("Escape");
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/connector/arrows", state: "triangle:triangle" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "connector" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/connector/arrows", state: "triangle:triangle" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C115503] [C115504] [C115505] Apply line formatting (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // insert a line shape
    I.clickToolbarTab("insert");
    I.clickOptionButton("shape/insert", "line");
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "connector" });
    I.waitForChangesSaved();

    // select a border style
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });
    I.waitForVisible({ docs: "control", key: "drawing/connector/arrows", state: "none:none" });
    I.clickOptionButton("drawing/border/style", "solid:thick");
    I.clickOptionButton("drawing/border/color", "red");
    I.clickOptionButton("drawing/connector/arrows", "triangle:triangle");
    I.waitForChangesSaved();

    // unselect/select drawing, check border style control
    I.pressKey("Escape");
    I.click({ spreadsheet: "drawing", pos: 0 });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/connector/arrows", state: "triangle:triangle" });

    // check again after reload
    await I.reopenDocument();
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape" }); // type changes from "connector" to "shape"
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/connector/arrows", state: "triangle:triangle" });

    I.closeDocument();
}).tag("stable");
