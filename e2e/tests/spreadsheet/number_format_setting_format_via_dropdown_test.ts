/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Number format > setting format via dropdown");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8151] Number format dropdown - set fraction", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1.5", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1.5,  display: "1.5" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //open the more menu
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "fraction" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "fraction" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1.5, display: "1 1/2" });

    await I.reopenDocument();

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1.5, display: "1 1/2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35090] Number format dropdown - set fraction (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("1.5", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1.5,  display: "1.5" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "fraction" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "fraction" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1.5, display: "1 1/2" });

    await I.reopenDocument();

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1.5, display: "1 1/2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8152] Number format dropdown - set scientific", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("12345678901", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 12345678901,  display: "12345678901" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //scientific
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "scientific" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "scientific" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 12345678901, display: "1.23E+10" });

    await I.reopenDocument();

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 12345678901, display: "1.23E+10" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C35091] Number format dropdown - set scientific (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("12345678901", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 12345678901,  display: "12345678901" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //scientific
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "scientific" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "scientific" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 12345678901, display: "1.23E+10" });

    await I.reopenDocument();

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 12345678901, display: "1.23E+10" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8153] Number format dropdown - set currency", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //currency
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "currency" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35092] Number format dropdown - set currency (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //currency
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "currency" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8154] Number format dropdown - set percent", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //percent
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "percent" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35093] Number format dropdown - set percent (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //percent
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "percent" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8155] Number format dropdown - set time", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("41113.6062", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062,  display: "41113.6062" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //time
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "time" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "time" });

    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "2:32:56 PM" });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "time" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "2:32:56 PM" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35094] Number format dropdown - set time (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("41113", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41113,  display: "41113" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //time
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "time" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "time" });

    //check value
    spreadsheet.waitForActiveCell("A1", { value: 41113, display: "12:00:00 AM" });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "time" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113, display: "12:00:00 AM" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8156] Number format dropdown - set date", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("41113.6062", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062,  display: "41113.6062" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "date" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "date" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "07/23/2012" });

    await I.reopenDocument();

    //date
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "date" });
    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display:  "07/23/2012" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35095] Number format dropdown - set date (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("41113.6062", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062,  display: "41113.6062" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "date" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "date" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "07/23/2012" });

    await I.reopenDocument();

    //date
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "date" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display:  "07/23/2012" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8157] Number format dropdown - set time and date", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("41113.6062", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062,  display: "41113.6062" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "datetime" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "datetime" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "07/23/2012 2:32:56 PM" });

    await I.reopenDocument();

    //date
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "datetime" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display:  "07/23/2012 2:32:56 PM" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35096] Number format dropdown - set time and date (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods", { locale: "de_DE" });

    spreadsheet.enterCellText("41113,6062", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "41113,6062" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "datetime" });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "datetime" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "23.07.2012 14:32:56" });

    await I.reopenDocument();

    //date
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "datetime" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41113.6062, display: "23.07.2012 14:32:56" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8158] Number format dropdown - change number format type", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("41640", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41640,  display: "41640" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "date" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41640, display: "01/01/2014" });
    //open the popup more
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/code", state: "MM/DD/YYYY" });

    //change the date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "[$-F800]DDDD, MMMM D, YYYY" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41640, display: "Wednesday, January 1, 2014" });

    await I.reopenDocument();

    I.waitForElement({ docs: "control", key: "cell/numberformat/code", state: "[$-F800]DDDD, MMMM D, YYYY" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41640, display: "Wednesday, January 1, 2014" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35097] Number format dropdown - change number format type (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("41640", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 41640,  display: "41640" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "date" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41640, display: "01/01/2014" });

    //open the popup more
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/code", state: "MM/DD/YYYY" });

    //change the date
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "DDDD, MMMM D, YYYY" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41640, display: "Wednesday, January 1, 2014" });

    await I.reopenDocument();

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 41640, display: "Wednesday, January 1, 2014" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C290798] Number format dropdown - set percent and change to currency (ODS)", async ({ I, spreadsheet  }) => {

    await  I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //open popup
    I.clickButton("cell/numberformat/category", { caret: true });

    //percent
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    //open popup again
    I.clickButton("cell/numberformat/category", { caret: true });

    //change to currency
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
