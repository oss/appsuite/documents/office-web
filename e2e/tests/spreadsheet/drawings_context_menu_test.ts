/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Context menus > Image, Chart or shape");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C38856] Delete", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/image_and_chart.xlsx");

    //Chart
    I.click({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.rightClick({ spreadsheet: "drawing", pos: 0, type: "chart" });

    I.waitForContextMenuVisible();

    //context cut, copy, paste
    I.waitForVisible({ docs: "control", key: "document/cut", withText: "Cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", withText: "Copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", withText: "Paste", inside: "context-menu" });

    //context delete
    I.waitForVisible({ docs: "control", key: "drawing/delete", withText: "Delete", inside: "context-menu" });

    //context reorder
    I.waitForVisible({ docs: "control", key: "drawing/order", withText: "Reorder", inside: "context-menu" });

    //delete chart
    I.waitForAndClick({ docs: "control", key: "drawing/delete", withText: "Delete", inside: "context-menu" });

    //image
    I.click({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.rightClick({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.waitForContextMenuVisible();

    //context cut, copy, paste
    I.waitForVisible({ docs: "control", key: "document/cut", withText: "Cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", withText: "Copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", withText: "Paste", inside: "context-menu" });

    //context delete
    I.waitForVisible({ docs: "control", key: "drawing/delete", withText: "Delete", inside: "context-menu" });

    //context reorder
    I.waitForVisible({ docs: "control", key: "drawing/order", withText: "Reorder", inside: "context-menu" });

    //delete image
    I.waitForAndClick({ docs: "control", key: "drawing/delete", withText: "Delete", inside: "context-menu" });

    await I.reopenDocument();

    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115051] Reorder a drawing", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_reorder.xlsx");

    //shape rectangle
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", withText: "Back" });

    //shape triangle
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "shape", withText: "Middle" });

    //shape ellipse
    I.waitForVisible({ spreadsheet: "drawing", pos: 2, type: "shape", withText: "Front" });
    I.click({ spreadsheet: "drawing", pos: 2, type: "shape" });
    I.rightClick({ spreadsheet: "drawing", pos: 2, type: "shape" });

    I.waitForContextMenuVisible();

    //context paste
    I.waitForVisible({ docs: "control", key: "document/paste", withText: "Paste", inside: "context-menu" });

    //context delete
    I.waitForVisible({ docs: "control", key: "drawing/delete", withText: "Delete", inside: "context-menu" });

    //context reorder
    I.waitForAndClick({ docs: "control", key: "drawing/order", withText: "Reorder", inside: "context-menu" });

    //Send to back
    I.clickButton("drawing/order", { inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();

    await I.reopenDocument();

    //shape rectangle
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "shape", withText: "Front" });

    //shape triangle
    I.waitForVisible({ spreadsheet: "drawing", pos: 2, type: "shape", withText: "Middle" });

    //shape ellipse
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "shape", withText: "Back" });

    I.closeDocument();
}).tag("stable");
