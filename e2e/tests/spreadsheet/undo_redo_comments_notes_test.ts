/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo comments & notes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C290027] Undo-Redo comments & notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/delete_colum_with_comments_notes.xlsx");

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);

    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("B");

    //delete the column
    I.clickButton("column/delete");

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 0);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);

    //check the text
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "Note" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });

    await I.reopenDocument();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 2);

    //check the text
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "Note" });

    //check the bubble text
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C290025] Undo notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/notes_delete_undo.xlsx");
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    await spreadsheet.selectCell("A1");

    I.clickToolbarTab("comments");

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/delete" });
    I.waitForChangesSaved();

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //see numbers of element, element in dom
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    await I.reopenDocument();

    //see numbers of element, element in dom
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173441] Undo all notes", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/notes_delete_all_undo.xlsx");

    I.clickToolbarTab("comments");

    //see numbers of element, element in dom
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    //open the dropdown
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/delete/all" });

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "All notes on this sheet will be removed. Do you want to continue?" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    I.dontSeeElementInDOM({ spreadsheet: "note-overlay" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //see numbers of element, element in dom
    I.seeElementInDOM({ spreadsheet: "note-overlay" });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    await I.reopenDocument();

    //see numbers of element, element in dom
    I.seeElementInDOM({ spreadsheet: "note-overlay" });
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173442] Undo comment (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cells_with_comments.ods");
    await spreadsheet.selectCell("A1");

    I.clickToolbarTab("comments");

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.clickButton("note/delete");
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 3);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    await I.reopenDocument();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173443] Undo all comments (ODS)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/cells_with_comments.ods");

    I.clickToolbarTab("comments");

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);

    I.clickButton("note/delete/all");
    //I.waitForChangesSaved();

    dialogs.waitForVisible();
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "All comments on this sheet will be removed. Do you want to continue?" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    I.dontSeeElementInDOM({ spreadsheet: "note-overlay" });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    await I.reopenDocument();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 4);
    I.seeElementInDOM({ spreadsheet: "note-overlay" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173444] Undo delete column with notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/delete_column_with_note.xlsx");

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);

    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("A");

    //delete the column
    I.clickButton("column/delete");

    I.dontSeeElement({ spreadsheet: "note", pos: 0 });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);

    //check the text
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 2, withText: "Comment 1" });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });

    await I.reopenDocument();

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 1);
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });

    //check the text
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 2, withText: "Comment 1" });

    I.closeDocument();
}).tag("stable");
