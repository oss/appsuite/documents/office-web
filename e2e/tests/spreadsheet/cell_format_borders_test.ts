/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell format > Borders");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8138] Cell border - Single cell", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B3");
    I.clickOptionButton("cell/border/flags", "outer");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C38784] Cell border - Single cell (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B3");
    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/flags", "outer", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.click({ docs: "popup", action: "cancel" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Liberation Sans" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 10 });
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });

    I.closeDocument();

}).tag("stable");
//-----------------------------------------------------------------------------

Scenario("[C8139] Cell border - Multiple cells", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectRange("B3:B4");
    I.clickOptionButton("cell/border/flags", "outer");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38785] Cell border - Multiple cells(ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectRange("B3:B4");
    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/flags", "outer", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.click({ docs: "popup", action: "cancel" });

    await I.reopenDocument();

    // ODS does not save selection when closing document
    await spreadsheet.selectRange("B3:B4");
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Liberation Sans" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 10 });
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8140] Cell border - border style", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B3");
    I.clickOptionButton("cell/border/flags", "outer");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    I.clickOptionButton("cell/border/style/preset", "dashDot:medium");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "dashDot:medium" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/style/preset", state: "dashDot:medium" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38786] Cell border - border style(ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B3");
    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/flags", "outer", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.click({ docs: "popup", action: "cancel" });

    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/style", "double", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/style", state: "double" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Liberation Sans" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 10 });
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/style", state: "double" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8142] Cell border - border color", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B3");
    I.clickOptionButton("cell/border/flags", "outer");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });

    I.clickOptionButton("cell/border/color", "green");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/border/color", state: "green" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.waitForVisible({ docs: "control", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", key: "cell/border/color", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38787] Cell border - border color(ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B3");
    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/flags", "outer", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.click({ docs: "popup", action: "cancel" });

    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/color", "green", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/color", state: "green" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Liberation Sans" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 10 });
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/color", state: "green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173451] Cell border - border width (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B3");
    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/flags", "outer", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.click({ docs: "popup", action: "cancel" });

    I.clickButton("view/cell/border/menu", { caret: true });
    I.clickOptionButton("cell/border/width", 2.5, { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/width", state: "2.5" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Liberation Sans" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 10 });
    I.clickButton("view/cell/border/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/flags", state: "tblr" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "cell/border/width", state: 2.5 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
