/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell content - autocomplete");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8106] Autocomplete during input (text)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("abc", { leave: "down" });
    I.type("a");

    //Autocomplete
    spreadsheet.waitForCellEditMode("abc");

    I.pressKeys("Ctrl+Home");

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "abc" });
    await spreadsheet.selectCell("A2", { value: "abc" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8107] Autocomplete during input (text+number)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("abc11", { leave: "down" });
    I.type("a");

    //Autocomplete
    spreadsheet.waitForCellEditMode("abc");

    I.pressKeys("Ctrl+Home");

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "abc11" });
    await spreadsheet.selectCell("A2", { value: "abc11" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8108] Autocomplete during input (text with multiple matches)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("abc less", { leave: "down" });
    spreadsheet.enterCellText("abc more", { leave: "down" });
    I.type("abc");

    //autocomplete
    spreadsheet.waitForCellEditMode("abc");

    I.pressKeys("Ctrl+Home");

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "abc less" });
    await spreadsheet.selectCell("A2", { value: "abc more" });
    await spreadsheet.selectCell("A3", { value: "abc more" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8109] Autocomplete during input - delete the automatically entered characters", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("abc11", { leave: "down" });
    I.type("a");

    //autocomplete
    spreadsheet.waitForCellEditMode("abc11");
    I.pressKey("ArrowDown");

    I.type("a");
    I.pressKeys("Backspace");

    //no autocomplete
    spreadsheet.waitForCellEditMode("a");

    I.pressKeys("Ctrl+Home");

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "abc11" });
    await spreadsheet.selectCell("A2", { value: "abc11" });
    await spreadsheet.selectCell("A3", { value: "a" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
