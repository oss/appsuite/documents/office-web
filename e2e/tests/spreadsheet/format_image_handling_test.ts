/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Image handling");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C29561] Delete drawing object", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/delete");
    I.waitForChangesSaved();

    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    await I.reopenDocument();

    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29563] Assigning a border style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    //borders
    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashed:medium" });
    I.waitForChangesSaved();

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    //borders
    I.clickButton("drawing/border/style", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "dashed:medium" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29564] Assigning a colored border style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    //borders
    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashed:medium" });
    I.waitForChangesSaved();

    //border color
    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "light-green" });
    I.waitForChangesSaved();

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    //border
    I.clickButton("drawing/border/style", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "dashed:medium" });

    //border color
    I.clickButton("drawing/border/color", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "light-green" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173448] Assigning a colored border style (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ods_images.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    //border
    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashed:medium" });
    I.waitForChangesSaved();

    //border color
    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "light-green" });
    I.waitForChangesSaved();

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    //border color
    I.clickButton("drawing/border/style", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "dashed:medium" });

    //border color
    I.clickButton("drawing/border/color", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "light-green" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C38795] Delete drawing object (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ods_images.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/delete");
    I.waitForChangesSaved();

    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    await I.reopenDocument();

    I.dontSeeElement({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173449] Assigning a border style (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ods_images.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashed:medium" });
    I.waitForChangesSaved();

    await I.reopenDocument();

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    //border
    I.clickButton("drawing/border/style", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "dashed:medium" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29566] Change image width and height", async ({ I }) => {

    //TODO: check the cursor
    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    // drag mouse to resize the image
    I.waitForToolbarTab("drawing");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "image", find: ".resizers [data-pos=br]" }, 200, 200);
    I.waitForChangesSaved();

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect1.width).to.be.within(570, 612);

    // check after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect2.width).to.be.within(570, 612);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38798] Change image width and height (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ods_images.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    // drag mouse to resize the image
    I.waitForToolbarTab("drawing");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "image", find: ".resizers [data-pos=br]" }, 200, 200);
    I.waitForChangesSaved();
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect1.width).to.equal(647);
    expect(rect1.height).to.equal(465);

    // check after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect2.width).to.equal(648);
    expect(rect2.height).to.equal(465);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29565] Change image width", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    // drag mouse to resize the image
    I.waitForToolbarTab("drawing");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "image", find: ".resizers [data-pos=r]" }, 150, 200);
    I.waitForChangesSaved();

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect1.width).to.be.within(505, 530);

    // check after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect2.width).to.be.within(505, 530);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C329204] Change image height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    // drag mouse to resize the image
    I.waitForToolbarTab("drawing");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "image", find: ".resizers [data-pos=b]" }, 150, 200);
    I.waitForChangesSaved();

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect1.width).to.be.within(355, 517);

    // check after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect2.width).to.be.within(355, 517);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C329203] Change image width (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ods_images.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    // drag mouse to resize the image
    I.waitForToolbarTab("drawing");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "image", find: ".resizers [data-pos=r]" }, 100, 200);
    I.waitForChangesSaved();

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect1.width).to.equal(547);
    expect(rect1.height).to.equal(265);

    // check after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect2.width).to.equal(548);
    expect(rect2.height).to.equal(265);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C329205] Change image height (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ods_images.ods");

    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    I.waitForToolbarTab("drawing");

    // drag mouse to resize the image
    I.waitForToolbarTab("drawing");
    I.dragMouseOnElement({ spreadsheet: "drawing", pos: 0, type: "image", find: ".resizers [data-pos=b]" }, 100, 200);
    I.waitForChangesSaved();

    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect1.width).to.equal(447);
    expect(rect1.height).to.equal(465);

    // check after reload
    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image" });
    expect(rect2.width).to.equal(448);
    expect(rect2.height).to.equal(465);

    I.closeDocument();
}).tag("stable");
