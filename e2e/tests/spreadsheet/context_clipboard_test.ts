/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Context menus > Clipboard");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8299] Copy&Paste text in a cell", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/text.xlsx");

    await spreadsheet.openCellContextMenu("A1");

    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "document/copy", withText: "Copy" });

    await spreadsheet.selectCell("A3");
    I.paste();

    spreadsheet.waitForActiveCell("A3", { value: "text for clipboard", display: "text for clipboard" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A3", { value: "text for clipboard", display: "text for clipboard" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C29579] Copy&Paste an image", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/spreadsheet_image.xlsx");

    I.click({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });


    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });

    //before pasting the image - check the new width/height
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "image", selected: true });
    expect(rect1.width).to.be.within(355, 377);

    I.rightClick({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.waitForContextMenuVisible();

    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "document/copy" });

    await spreadsheet.selectCell("H2");
    I.paste();
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image" });

    //after pasting the image - check the new width/height
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 1, type: "image", selected: true });
    expect(rect2.width).to.be.within(355, 377);

    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "image" });
    I.click({ spreadsheet: "drawing", pos: 1, type: "image" });

    //after pasting the image - check the new width/height
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 1, type: "image", selected: true });
    expect(rect3.width).to.be.within(355, 377);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C85957] Copy&Paste a chart drawing", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/context_copy_chart.xlsx");

    I.click({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "chart", selected: true });

    //before pasting the image - check the new width/height
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 0, type: "chart", selected: true });
    expect(rect1.width).to.be.within(484, 496);

    I.rightClick({ spreadsheet: "drawing", pos: 0, type: "chart" });

    I.waitForContextMenuVisible();

    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "document/copy" });

    await spreadsheet.selectCell("A9");
    I.paste();
    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "chart" });

    //after pasting the image - check the new width/height
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 1, type: "chart", selected: true });
    expect(rect2.width).to.be.within(484, 496);

    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "chart" });
    I.click({ spreadsheet: "drawing", pos: 1, type: "chart" });

    //after pasting the image - check the new width/height
    const rect3 = await I.grabElementBoundingRect({ spreadsheet: "drawing", pos: 1, type: "chart", selected: true });
    expect(rect3.width).to.be.within(484, 496);

    I.closeDocument();
}).tag("stable");
