/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Character Attributes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("Default font attributes", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a value
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check default font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: false });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    // reload document
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check default font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "+mn-lt" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.waitForVisible({ docs: "control", key: "character/underline", state: false });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: false });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch dark1" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C325941] Font attributes preselect", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // preselect font attributes
    I.clickOptionButton("character/fontname", "Georgia");
    I.clickOptionButton("character/fontsize", 32);
    I.clickButton("character/bold");
    I.clickButton("character/italic");
    I.clickButton("character/underline");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });
    I.clickOptionButton("character/color", "red");

    // enter a value
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Georgia" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 32 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    // reload document
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Georgia" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 32 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C326112] Font attributes select", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // enter a value
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // select font attributes
    I.clickOptionButton("character/fontname", "Georgia");
    I.waitForChangesSaved();
    I.clickOptionButton("character/fontsize", 32);
    I.clickButton("character/bold");
    I.clickButton("character/italic");
    I.clickButton("character/underline");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });
    I.clickOptionButton("character/color", "red");

    // check font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Georgia" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 32 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    // reload document
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Georgia" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 32 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C326111] Font attributes preselect (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // preselect font attributes
    I.clickOptionButton("character/fontname", "Georgia");
    I.clickOptionButton("character/fontsize", 32);
    I.clickButton("character/bold");
    I.clickButton("character/italic");
    I.clickButton("character/underline");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });
    I.clickOptionButton("character/color", "red");

    // enter a value
    spreadsheet.enterCellText("42", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Georgia" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 32 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    // reload document
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 42 });

    // check font attributes
    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Georgia" });
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 32 });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "character/strike", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C326113] Font attribute bold select over a cell range (row/column)", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // set font attributes to rows and columns
    spreadsheet.selectRow("2");
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    spreadsheet.selectColumn("B");
    I.clickButton("character/italic");
    I.waitForChangesSaved();

    // check font attributes
    spreadsheet.waitForCellFocus();
    I.pressKeys("Ctrl+Home");
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.pressKey("ArrowRight");
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.pressKey("ArrowUp");
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });

    await I.reopenDocument();

    // check font attributes
    I.pressKeys("Ctrl+Home");
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.pressKey("ArrowDown");
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: false });
    I.pressKey("ArrowRight");
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    I.pressKey("ArrowUp");
    I.waitForVisible({ docs: "control", key: "character/bold", state: false });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });

    I.closeDocument();
}).tag("stable");
