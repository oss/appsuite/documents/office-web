/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Undo-Redo > Undo-Redo text");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8295] Undo text input", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.waitForActiveCell("A1");

    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    spreadsheet.waitForActiveCell("A1", { display: "Lorem" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A1", { value: null });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { value: null });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8296] Redo text input", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.waitForActiveCell("A1");

    spreadsheet.enterCellText("Lorem", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { display: "Lorem" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A1", { value: null });

    I.clickButton("document/redo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A1", { display: "Lorem" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A1", { display: "Lorem" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8297] Undo pasted text", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.waitForActiveCell("A1");
    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    I.copy();
    await spreadsheet.selectCell("A3");
    I.paste();

    spreadsheet.waitForActiveCell("A3", { display: "Lorem" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A3", { value: null });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A3", { value: null });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8298] Redo pasted text", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.waitForActiveCell("A1");
    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    I.copy();
    await spreadsheet.selectCell("A3");
    I.paste();

    spreadsheet.waitForActiveCell("A3", { display: "Lorem" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A3", { value: null });

    I.clickButton("document/redo");
    I.waitForChangesSaved();

    spreadsheet.waitForActiveCell("A3", { display: "Lorem" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("A3", { display: "Lorem" });

    I.closeDocument();
}).tag("stable");
