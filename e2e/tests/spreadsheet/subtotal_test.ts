/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Sheet tab bar");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8224] Subtotal value", async ({ I  }) => {

    await I.loginAndOpenDocument("media/files/cell_range.xlsx");

    I.pressKeys("Ctrl+Multiply");

    I.waitForAndClick({ docs: "control", pane: "status-pane", key: "view/status/subtotals" });

    I.waitForVisible({ css: ".list-menu a.button[data-value='sum'][data-checked='true']", withText: "Sum: 90" });

    I.waitForVisible({ css: ".list-menu a.button[data-value='min'][data-checked='false']", withText: "Min: 1" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='max'][data-checked='false']", withText: "Max: 8" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='average'][data-checked='false']", withText: "Average: 4.5" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='numbers'][data-checked='false']", withText: "Numerical count: 20" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='cells'][data-checked='false']", withText: "Count: 20" });

    I.waitForAndClick({ css: ".list-menu a.button[data-value='average'][data-checked='false']", withText: "Average: 4.5" });

    await I.reopenDocument();

    I.waitForAndClick({ docs: "control", pane: "status-pane", key: "view/status/subtotals" });

    I.waitForVisible({ css: ".list-menu a.button[data-value='sum'][data-checked='false']", withText: "Sum: 90" });

    I.waitForVisible({ css: ".list-menu a.button[data-value='min'][data-checked='false']", withText: "Min: 1" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='max'][data-checked='false']", withText: "Max: 8" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='average'][data-checked='true']", withText: "Average: 4.5" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='numbers'][data-checked='false']", withText: "Numerical count: 20" });
    I.waitForVisible({ css: ".list-menu a.button[data-value='cells'][data-checked='false']", withText: "Count: 20" });

    I.closeDocument();
}).tag("stable");
