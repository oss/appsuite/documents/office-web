/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Number format > setting format via button");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8149] Number format button - currency", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //currency
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", value: "currency" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "currency" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "$1,000.00 " });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8150] Number format button - percent", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    //currency
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/category", value: "percent" });

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    await I.reopenDocument();

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "percent" });

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "100000.00%" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[DOCS-5120] Number format button - decrease / increase decimal places", async ({ I, spreadsheet  }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //check the state
    I.waitForVisible({ docs: "control", key: "cell/numberformat/decimalincrease", disabled: true });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/decimaldecrease", disabled: true });

    spreadsheet.enterCellText("1000", { leave: "stay" });
    spreadsheet.waitForActiveCell("A1", { value: 1000,  display: "1000" });

    I.waitForAndClick({ docs: "control", key: "cell/numberformat/decimalincrease", disabled: false });

    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "1000.0" });

    I.waitForAndClick({ docs: "control", key: "cell/numberformat/decimaldecrease", disabled: false });

    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "1000" });

    I.waitForVisible({ docs: "control", key: "cell/numberformat/decimaldecrease", disabled: true });
    I.waitForAndClick({ docs: "control", key: "cell/numberformat/decimalincrease", disabled: false });

    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "1000.0" });

    await I.reopenDocument();

    //check the value
    spreadsheet.waitForActiveCell("A1", { value: 1000, display: "1000.0" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
