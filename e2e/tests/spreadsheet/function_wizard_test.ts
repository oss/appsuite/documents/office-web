/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Function Wizard");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85759] Insert function via wizard", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open "Insert function" dialog, check a few elements
    I.clickButton("function/insert/dialog");
    dialogs.waitForVisible();
    I.waitForVisible({ docs: "dialog", find: ".category-picker .btn", withText: "All" });
    I.waitForVisible({ docs: "dialog", find: ".function-picker" });
    I.waitForVisible({ docs: "dialog", find: ".description-box" });

    // select the "SUM" function and leave dialog
    I.type("sum");
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // expect cell edit mode with prefilled formula, insert parameters
    spreadsheet.waitForCellEditMode("=SUM()");
    I.type("1,3");
    spreadsheet.waitForCellEditMode("=SUM(1,3)");
    I.pressKey("Enter");

    // expect formula and result to appear in the cell
    I.waitForChangesSaved();
    await spreadsheet.selectCell("A1", { value: 4, formula: "SUM(1,3)" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: 4, formula: "SUM(1,3)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85761] Insert function via wizard - pick a category", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open "Insert function" dialog
    I.clickButton("function/insert/dialog");
    dialogs.waitForVisible();

    // check the categories dropdown menu
    I.waitForAndClick({ docs: "dialog", find: ".category-picker .btn", withText: "All" });
    I.waitForVisible({ docs: "popup" });
    const CATEGORIES = ["__all__", "complex", "conversion", "database", "datetime", "engineering", "financial", "information", "logical", "math", "matrix", "reference", "statistical", "text", "web"];
    for (const key of CATEGORIES) {
        I.seeElement({ docs: "button", inside: "popup-menu", value: key });
    }

    // select "Logical" function category
    I.click({ docs: "button", inside: "popup-menu", value: "logical" });
    I.seeElement({ docs: "dialog", find: ".function-picker option", withText: "AND" });
    I.dontSeeElement({ docs: "dialog", find: ".function-picker option", withText: "SUM" });

    // activate "TRUE" function
    I.click({ docs: "dialog", find: ".function-picker option", withText: "TRUE" });
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // expect cell edit mode with prefilled formula
    spreadsheet.waitForCellEditMode("=TRUE()");
    I.pressKey("Enter");

    // expect formula and result to appear in the cell
    I.waitForChangesSaved();
    await spreadsheet.selectCell("A1", { value: true, formula: "TRUE()" });

    // expect same contents after reload
    await I.reopenDocument();
    await spreadsheet.selectCell("A1", { value: true, formula: "TRUE()" });

    I.closeDocument();
}).tag("stable");
