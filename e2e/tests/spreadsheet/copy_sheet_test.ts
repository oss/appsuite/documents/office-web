/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Sheet tab bar > Sheet operations > Copy sheet");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8235]Copy sheet with simple text attributes", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/textattributes.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    await spreadsheet.selectCell("A1", { value: "background color" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "light-green" });
    await spreadsheet.selectCell("A2", { value: "Bold" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    await spreadsheet.selectCell("A3", { value: "Italic" });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    await spreadsheet.selectCell("A4", { value: "text with color" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    await I.reopenDocument();

    await spreadsheet.selectCell("A1", { value: "background color" });
    I.waitForVisible({ docs: "control", key: "cell/fillcolor", state: "light-green" });
    await spreadsheet.selectCell("A2", { value: "Bold" });
    I.waitForVisible({ docs: "control", key: "character/bold", state: true });
    await spreadsheet.selectCell("A3", { value: "Italic" });
    I.waitForVisible({ docs: "control", key: "character/italic", state: true });
    await spreadsheet.selectCell("A4", { value: "text with color" });
    I.waitForVisible({ docs: "control", key: "character/color", state: "red" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8236] Copy sheet with formula (simple formula)", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/simple_formula.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    //column A
    await spreadsheet.selectCell("A1", { display: "Summen" });
    await spreadsheet.selectCell("A3", { display: "1" });
    await spreadsheet.selectCell("A4", { display: "2" });
    await spreadsheet.selectCell("A5", { display: "3" });
    await spreadsheet.selectCell("A6", { display: "4" });
    await spreadsheet.selectCell("A7", { display: "5" });
    await spreadsheet.selectCell("A8", { display: "6" });
    await spreadsheet.selectCell("A9", { display: "7" });
    await spreadsheet.selectCell("A10", { display: "28", formula: "SUM(A3:A9)" });

    await I.reopenDocument();

    //column A
    await spreadsheet.selectCell("A1", { display: "Summen" });
    await spreadsheet.selectCell("A3", { display: "1" });
    await spreadsheet.selectCell("A4", { display: "2" });
    await spreadsheet.selectCell("A5", { display: "3" });
    await spreadsheet.selectCell("A6", { display: "4" });
    await spreadsheet.selectCell("A7", { display: "5" });
    await spreadsheet.selectCell("A8", { display: "6" });
    await spreadsheet.selectCell("A9", { display: "7" });
    await spreadsheet.selectCell("A10", { display: "28", formula: "SUM(A3:A9)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8238] Copy sheet with numberformat", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/numberformat.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    //sheet 2 is active
    spreadsheet.waitForActiveSheet(1);

    await spreadsheet.selectCell("A1", { value: "NUMBER", display: "NUMBER" });
    await spreadsheet.selectCell("A2", { value: 1234.5678, display: "1235" });
    await spreadsheet.selectCell("A3", { value: 1235.5678, display: "1235.57" });
    await spreadsheet.selectCell("B1", { display: "CURRENCY" });
    await spreadsheet.selectCell("B2", { value: 1234.5678, display: "1234.57 €" });
    await spreadsheet.selectCell("B3", { display: "$1,236" });
    await spreadsheet.selectCell("C1", { display: "DATE" });
    await spreadsheet.selectCell("C2", { value: 12345, display: "18.10.1933" });
    await spreadsheet.selectCell("C3", { value: 12346, display: "19.October.1933" });
    await spreadsheet.selectCell("D1", { display: "TIME" });
    await spreadsheet.selectCell("D2", { value: 0.5814814814814815, display: "13:57" });
    await spreadsheet.selectCell("D3", { value: 1.581481481481481, display: "13:57:20" });
    await spreadsheet.selectCell("E1", { display: "DATE TIME" });
    await spreadsheet.selectCell("E2", { value: 40944.01041666666, display: "05.02.12 00:15" });
    await spreadsheet.selectCell("E3", { value: 40945.01041666666, display: "06.02.2012 00:15" });
    await spreadsheet.selectCell("F1", { display: "PERCENTAGE" });
    await spreadsheet.selectCell("F2", { value: 0.3456, display: "35%" });
    await spreadsheet.selectCell("F3", { value: 0.3456, display: "34.6%" });
    await spreadsheet.selectCell("G1", { display: "SCIENTIFIC" });
    await spreadsheet.selectCell("G2", { value: 500, display: "5E+02" });
    await spreadsheet.selectCell("H1", { display: "FRACTION" });
    await spreadsheet.selectCell("H2", { value: 13.33, display: "13 1/3" });

    await I.reopenDocument();

    //sheet 2 is active
    spreadsheet.waitForActiveSheet(1);

    await spreadsheet.selectCell("A1", { value: "NUMBER", display: "NUMBER" });
    await spreadsheet.selectCell("A2", { value: 1234.5678, display: "1235" });
    await spreadsheet.selectCell("A3", { value: 1235.5678, display: "1235.57" });
    await spreadsheet.selectCell("B1", { display: "CURRENCY" });
    await spreadsheet.selectCell("B2", { value: 1234.5678, display: "1234.57 €" });
    await spreadsheet.selectCell("B3", { display: "$1,236" });
    await spreadsheet.selectCell("C1", { display: "DATE" });
    await spreadsheet.selectCell("C2", { value: 12345, display: "18.10.1933" });
    await spreadsheet.selectCell("C3", { value: 12346, display: "19.October.1933" });
    await spreadsheet.selectCell("D1", { display: "TIME" });
    await spreadsheet.selectCell("D2", { value: 0.5814814814814815, display: "13:57" });
    await spreadsheet.selectCell("D3", { value: 1.581481481481481, display: "13:57:20" });
    await spreadsheet.selectCell("E1", { display: "DATE TIME" });
    await spreadsheet.selectCell("E2", { value: 40944.01041666666, display: "05.02.12 00:15" });
    await spreadsheet.selectCell("E3", { value: 40945.01041666666, display: "06.02.2012 00:15" });
    await spreadsheet.selectCell("F1", { display: "PERCENTAGE" });
    await spreadsheet.selectCell("F2", { value: 0.3456, display: "35%" });
    await spreadsheet.selectCell("F3", { value: 0.3456, display: "34.6%" });
    await spreadsheet.selectCell("G1", { display: "SCIENTIFIC" });
    await spreadsheet.selectCell("G2", { value: 500, display: "5E+02" });
    await spreadsheet.selectCell("H1", { display: "FRACTION" });
    await spreadsheet.selectCell("H2", { value: 13.33, display: "13 1/3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8873] Copy sheet with hyperlink", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/hyperlink.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    //sheet 2 is active
    spreadsheet.waitForActiveSheet(1);

    //hyperlink attribute
    await spreadsheet.selectCell("C1", { display: "www.test.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    await I.reopenDocument();

    //sheet 2 is active
    spreadsheet.waitForActiveSheet(1);

    //hyperlink attribute
    spreadsheet.waitForActiveCell("C1", { display: "www.test.de" });
    I.waitForVisible({ docs: "control", key: "character/underline", state: true });
    I.waitForVisible({ docs: "control", key: "character/color", state: "sch hyperlink" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85612] Copy sheet with table reference", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/table_reference.xlsx", { expectToolbarTab: "table" });

    // table range has been rendered
    I.waitForVisible({ spreadsheet: "table-range", name: "table", range: "A1:C7" });

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    //sheet 2 is active
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });
    I.waitForVisible({ spreadsheet: "table-range", name: "Table1", range: "A1:C7" });

    //open the pop-up, and check the value - column A
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "berlin", withText: "Berlin" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "frankfurt", withText: "Frankfurt" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "hamburg", withText: "Hamburg" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cologne", withText: "Cologne" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "munich", withText: "Munich" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "stuttgart", withText: "Stuttgart" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "3,375,222", withText: "3,375,222" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1734272", withText: "1734272" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "93,322", withText: "93,322" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-1547628", withText: "-1547628" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-3,188,578", withText: "-3,188,578" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-4829528", withText: "-4829528" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "12", withText: "12" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "234", withText: "234" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-122", withText: "-122" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-92.66666667", withText: "-92.66666667" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-159.6666667", withText: "-159.6666667" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-226.6666667", withText: "-226.6666667" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //check the table style
    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "TableStyleMedium2" });

    await I.reopenDocument({ expectToolbarTab: "table" });

    //sheet 2 is active
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });
    I.waitForVisible({ spreadsheet: "table-range", name: "Table1", range: "A1:C7" });

    //open the pop-up, and check the value - column A
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "A1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "berlin", withText: "Berlin" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "frankfurt", withText: "Frankfurt" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "hamburg", withText: "Hamburg" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "cologne", withText: "Cologne" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "munich", withText: "Munich" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "stuttgart", withText: "Stuttgart" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "3,375,222", withText: "3,375,222" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "1734272", withText: "1734272" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "93,322", withText: "93,322" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-1547628", withText: "-1547628" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-3,188,578", withText: "-3,188,578" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-4829528", withText: "-4829528" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //open the pop-up, and check the value - column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C1" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "12", withText: "12" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "234", withText: "234" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-122", withText: "-122" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-92.66666667", withText: "-92.66666667" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-159.6666667", withText: "-159.6666667" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "-226.6666667", withText: "-226.6666667" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //check the table style
    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "TableStyleMedium2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8233] Copy sheet with a chart (column clustered)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/colum_cluster_chart.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // select first chart
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    //check the chart type
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column stacked" });

    //check the labels
    I.clickButton("drawing/chartlabels", { state: false });

    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: "Copy me" });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: true });
    I.pressKey("Escape");

    // select first chart
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    //color set
    I.clickButton("drawing/chartcolorset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });

    //style set
    I.clickButton("drawing/chartstyleset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    //legend position
    I.clickButton("drawing/chartlegend/pos", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });

    //color set
    I.clickButton("drawing/chartcolorset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });

    //style set
    I.clickButton("drawing/chartstyleset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    //legend position
    I.clickButton("drawing/chartlegend/pos", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });

    //TODO data source
    //I.clickButton("drawing/chartdatasource", { caret: true });
    //I.waitForVisible({ docs: "control", key: "drawing/chartfirstcol", withText: "First column as label" });

    // source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D2:D5" });

    //sheet 2 is active
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    await I.reopenDocument();

    //sheet 2 is active
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    // select first chart
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    //check the chart type
    I.waitForVisible({ docs: "control", key: "drawing/charttype", state: "column stacked" });

    //check the labels
    I.clickButton("drawing/chartlabels", { state: false });

    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/title/text", state: "Copy me" });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/title/text", state: null });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/labels/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/line/visible", state: true });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/line/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/x/grid/visible", state: false });
    I.waitForVisible({ docs: "control", inside: "floating-menu", key: "drawing/chart/axis/y/grid/visible", state: true });
    I.pressKey("Escape");

    // select first chart
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "chart" });
    I.waitForToolbarTab("drawing");

    //color set
    I.clickButton("drawing/chartcolorset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartcolorset", state: "cs1" });

    //style set
    I.clickButton("drawing/chartstyleset", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartstyleset", state: "ss0" });

    //legend position
    I.clickButton("drawing/chartlegend/pos", { caret: true });
    I.waitForVisible({ docs: "control", key: "drawing/chartlegend/pos", state: "bottom" });

    //TODO data source
    //I.clickButton("drawing/chartdatasource", { caret: true });
    //I.waitForVisible({ docs: "control", key: "drawing/chartfirstcol", withText: "First column as label" });

    // source data ranges
    I.waitForVisible({ spreadsheet: "highlight-range", range: "A2:A5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "B2:B5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "C2:C5" });
    I.waitForVisible({ spreadsheet: "highlight-range", range: "D2:D5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

// TODO: Redesign test
Scenario("[C118720] Copy sheet with basic shapes", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/basic_shapes1.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "shape", withText: "POS0 Rectangle" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 1, type: "shape", withText: "POS1 Ellipse" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 2, type: "shape", withText: "POS2 Isosceles triangle" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 3, type: "shape", withText: "POS3 Right triangle" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 4, type: "shape", withText: "POS4 Parallelogram" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 5, type: "shape", withText: "POS5 Trapezoid" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 6, type: "shape", withText: "POS6 Diamond" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 7, type: "shape", withText: "POS7  Pentagon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 8, type: "shape", withText: "POS8 Hexagon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 9, type: "shape", withText: "POS9 Heptagon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 10, type: "shape", withText: "POS10 Octagon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 11, type: "shape", withText: "POS11Decagon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 12, type: "shape", withText: "POS12 Dodecagon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 13, type: "shape", withText: "POS13 Pie" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 14, type: "shape", withText: "Circle segment" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 14, type: "shape", withText: "POS14 Circle segment" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 15, type: "shape", withText: "POS15 Teardrop" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 16, type: "shape", withText: "POS16 Frame" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 17, type: "shape", withText: "POS17 Corner" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 18, type: "shape", withText: "POS18 Diagonal stripe" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 19, type: "shape", withText: "POS19 Plus" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 20, type: "shape", withText: "POS20 Cylinder" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 21, type: "shape", withText: "POS21 Cube" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 22, type: "shape", withText: "POS22 Rectangle bevel" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 23, type: "shape", withText: "POS23 Ring" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 24, type: "shape", withText: "POS24 No symbol" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 25, type: "shape", withText: "POS25 Block arc" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 26, type: "shape", withText: "POS26 Folded corner" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 27, type: "shape", withText: "POS27 Smiley face" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 28, type: "shape", withText: "POS28 Heart" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 29, type: "shape", withText: "POS29 Lightning bolt" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 30, type: "shape", withText: "POS30 Sun" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 31, type: "shape", withText: "POS31 Moon" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 32, type: "shape", withText: "POS32 Cloud" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 33, type: "shape", withText: "POS33 Arc" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 34, type: "shape", withText: "POS34 Double bracket" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 35, type: "shape", withText: "POS35  Double brace" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 36, type: "shape", withText: "POS36 Left bracket" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 37, type: "shape", withText: "POS37 Right bracket" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 38, type: "shape", withText: "POS38 Left brace" });

    I.clickToolbarTab("insert");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 39, type: "shape", withText: "POS39 Right brace" });

    await I.reopenDocument();

    // TODO

    I.closeDocument();
});

//-----------------------------------------------------------------------------

Scenario("[C8237] Copy sheet with active autofilter", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/active_autofilter.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    //sheet 2 is active
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    //column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2", icon: "filter" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 1, checked: false });
    I.waitForElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 2, checked: false });
    I.waitForElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 3, checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 4 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 5 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 6 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 7 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 8 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 9 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 10 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 11 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 12 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 13 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 14 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 15 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 16 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 17 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank", withText: "Blank cells" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "e" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column D
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "D2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 0 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column E
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "E2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column F
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "F2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 1 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 2 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    await I.reopenDocument();

    //sheet 2 is active
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    //column B
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "B2", icon: "filter" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 1, checked: false });
    I.waitForElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 2, checked: false });
    I.waitForElement({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 3, checked: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 4 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 5 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 6 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 7 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 8 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 9 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 10 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 11 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 12 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 13 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 14 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 15 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 16 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 17 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank", withText: "Blank cells" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column C
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "C2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "d" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "e" });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column D
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "D2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 0 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: false });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column E
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "E2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    //column F
    I.waitForAndClick({ spreadsheet: "cell-button", menu: "table", address: "F2" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "descending" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "table/sort/order/list", value: "ascending" });

    //check the popup values
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", selectAll: true });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 1 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: 2 });
    I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: ":blank" });
    I.waitForAndClick({ docs: "popup", action: "cancel" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C162989] Copy sheet with notes", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/copy_notes.xlsx");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    I.pressKeys("Ctrl+Home");

    //check the active sheet
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    //see numbers of element
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 14);

    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A5", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A6", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A7", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B5", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B6", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B7", hint: true });

    //open the dropdown
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });

    //show the notes
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/show/all" });

    //check the note text
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "a1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "b1" });
    I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "a2" });
    I.waitForVisible({ spreadsheet: "note", pos: 3, para: 1, withText: "b2" });
    I.waitForVisible({ spreadsheet: "note", pos: 4, para: 1, withText: "a3" });
    I.waitForVisible({ spreadsheet: "note", pos: 5, para: 1, withText: "b3" });
    I.waitForVisible({ spreadsheet: "note", pos: 6, para: 1, withText: "a4" });
    I.waitForVisible({ spreadsheet: "note", pos: 7, para: 1, withText: "b4" });
    I.waitForVisible({ spreadsheet: "note", pos: 8, para: 1, withText: "a5" });
    I.waitForVisible({ spreadsheet: "note", pos: 9, para: 1, withText: "b5" });
    I.waitForVisible({ spreadsheet: "note", pos: 10, para: 1, withText: "a6" });
    I.waitForVisible({ spreadsheet: "note", pos: 11, para: 1, withText: "b6" });
    I.waitForVisible({ spreadsheet: "note", pos: 12, para: 1, withText: "a7" });
    I.waitForVisible({ spreadsheet: "note", pos: 13, para: 1, withText: "b7" });

    //hide all notes
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/hide/all" });

    await I.reopenDocument();

    //check the active sheet
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });

    //check the active sheet
    I.seeNumberOfElements({ css: ".drawing-layer.note-layer .sheet .drawing" }, 14);

    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A5", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A6", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A7", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B5", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B6", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B7", hint: true });

    //open the dropdown
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });

    //show the notes
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/show/all" });

    //check the note text
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 1, withText: "a1" });
    I.waitForVisible({ spreadsheet: "note", pos: 1, para: 1, withText: "b1" });
    I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "a2" });
    I.waitForVisible({ spreadsheet: "note", pos: 3, para: 1, withText: "b2" });
    I.waitForVisible({ spreadsheet: "note", pos: 4, para: 1, withText: "a3" });
    I.waitForVisible({ spreadsheet: "note", pos: 5, para: 1, withText: "b3" });
    I.waitForVisible({ spreadsheet: "note", pos: 6, para: 1, withText: "a4" });
    I.waitForVisible({ spreadsheet: "note", pos: 7, para: 1, withText: "b4" });
    I.waitForVisible({ spreadsheet: "note", pos: 8, para: 1, withText: "a5" });
    I.waitForVisible({ spreadsheet: "note", pos: 9, para: 1, withText: "b5" });
    I.waitForVisible({ spreadsheet: "note", pos: 10, para: 1, withText: "a6" });
    I.waitForVisible({ spreadsheet: "note", pos: 11, para: 1, withText: "b6" });
    I.waitForVisible({ spreadsheet: "note", pos: 12, para: 1, withText: "a7" });
    I.waitForVisible({ spreadsheet: "note", pos: 13, para: 1, withText: "b7" });

    //hide all notes
    I.waitForAndClick({ docs: "control", key: "view/notes/menu" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "note/hide/all" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C289994] Copy sheet with comment", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });

    // create a copy of the sheet
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 1, find: ".comment-thread-count[data-thread-count='2']" });

    // expect that all comments have been copied
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", hint: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 2, find: ".text", withText: "Child 2" });
    I.waitForAndClick({ docs: "comment", thread: 1, find: ".hide-button", withText: "View 1 more reply" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 1, find: ".text", withText: "Child 1" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118724] Copy sheet with conditional formatting", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/cond_formatting_color_scale.xlsx");
    //I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });

    // create a copy of the sheet
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    spreadsheet.waitForActiveSheet(1);

    await spreadsheet.selectCell("A1", { display: "100", renderFill: "#f8696b", renderColor: "#000000" });
    await spreadsheet.selectCell("A2", { display: "30", renderFill: "#999f75", renderColor: "#000000" });
    await spreadsheet.selectCell("A3", { display: "80", renderFill: "#dd786e", renderColor: "#000000" });
    await spreadsheet.selectCell("A4", { display: "-10", renderFill: "#63be7b", renderColor: "#000000" });
    await spreadsheet.selectCell("A5", { display: "nix", renderFill: "transparent" });
    await spreadsheet.selectCell("A6", { display: "22", renderFill: "#8ea576", renderColor: "#000000" });
    await spreadsheet.selectCell("E1", { display: "100", renderFill: "#5b9bd5", renderColor: "#000000" });
    await spreadsheet.selectCell("E2", { display: "30", renderFill: "#b5a8a5", renderColor: "#000000" });
    await spreadsheet.selectCell("E3", { display: "80", renderFill: "#6d9ecc", renderColor: "#000000" });
    await spreadsheet.selectCell("E4", { display: "13", renderFill: "#ceac98", renderColor: "#000000" });
    await spreadsheet.selectCell("E5", { display: "-13", renderFill: "hsl(24,84%,74%)", renderColor: "#000000" });
    await spreadsheet.selectCell("G1", { display: "90", renderFill: "#63be7b", renderColor: "#000000" });
    await spreadsheet.selectCell("G2", { display: "50", renderFill: "#d7df82", renderColor: "#000000" });
    await spreadsheet.selectCell("G3", { display: "50", renderFill: "#d7df82", renderColor: "#000000" });
    await spreadsheet.selectCell("G4", { display: "30", renderFill: "#fed580", renderColor: "#000000" });
    await spreadsheet.selectCell("G5", { display: "30", renderFill: "#fed580", renderColor: "#000000" });
    await spreadsheet.selectCell("G6", { display: "10", renderFill: "#fa8d72", renderColor: "#000000" });
    await spreadsheet.selectCell("G7", { display: "5", renderFill: "#f97b6e", renderColor: "#000000" });
    await spreadsheet.selectCell("G8", { display: "0", renderFill: "#f8696b", renderColor: "#000000" });

    await I.reopenDocument();

    //check the active sheet
    spreadsheet.waitForActiveSheet(1);

    await spreadsheet.selectCell("A1", { display: "100", renderFill: "#f8696b", renderColor: "#000000" });
    await spreadsheet.selectCell("A2", { display: "30", renderFill: "#999f75", renderColor: "#000000" });
    await spreadsheet.selectCell("A3", { display: "80", renderFill: "#dd786e", renderColor: "#000000" });
    await spreadsheet.selectCell("A4", { display: "-10", renderFill: "#63be7b", renderColor: "#000000" });
    await spreadsheet.selectCell("A5", { display: "nix", renderFill: "transparent" });
    await spreadsheet.selectCell("A6", { display: "22", renderFill: "#8ea576", renderColor: "#000000" });
    await spreadsheet.selectCell("E1", { display: "100", renderFill: "#5b9bd5", renderColor: "#000000" });
    await spreadsheet.selectCell("E2", { display: "30", renderFill: "#b5a8a5", renderColor: "#000000" });
    await spreadsheet.selectCell("E3", { display: "80", renderFill: "#6d9ecc", renderColor: "#000000" });
    await spreadsheet.selectCell("E4", { display: "13", renderFill: "#ceac98", renderColor: "#000000" });
    await spreadsheet.selectCell("E5", { display: "-13", renderFill: "hsl(24,84%,74%)", renderColor: "#000000" });
    await spreadsheet.selectCell("G1", { display: "90", renderFill: "#63be7b", renderColor: "#000000" });
    await spreadsheet.selectCell("G2", { display: "50", renderFill: "#d7df82", renderColor: "#000000" });
    await spreadsheet.selectCell("G3", { display: "50", renderFill: "#d7df82", renderColor: "#000000" });
    await spreadsheet.selectCell("G4", { display: "30", renderFill: "#fed580", renderColor: "#000000" });
    await spreadsheet.selectCell("G5", { display: "30", renderFill: "#fed580", renderColor: "#000000" });
    await spreadsheet.selectCell("G6", { display: "10", renderFill: "#fa8d72", renderColor: "#000000" });
    await spreadsheet.selectCell("G7", { display: "5", renderFill: "#f97b6e", renderColor: "#000000" });
    await spreadsheet.selectCell("G8", { display: "0", renderFill: "#f8696b", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8234] Copy sheet with listvalidation ", async ({ I, dialogs, spreadsheet }) => {

    async function testAllCellLists(): Promise<void> {
        spreadsheet.waitForActiveSheet(1);

        // open the popup-menu
        await spreadsheet.selectCell("D2");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D2" });

        // as range - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b2" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b3" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b4" });
        I.pressKey("Escape");

        // open the popup-menu
        await spreadsheet.selectCell("D7");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D7" });

        // as name - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b7" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b8" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b9" });
        I.pressKey("Escape");

        // open the popup-menu
        await spreadsheet.selectCell("D12");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D12" });

        // as name - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "a" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "b" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "c" });
        I.pressKey("Escape");

        // as name - check the values
        await spreadsheet.selectCell("D17");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "D17" });

        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2b2" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2b3" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2b4" });
        I.pressKey("Escape");

        // open the popup-menu
        await spreadsheet.selectCell("J2");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J2" });

        // Relativ ranges - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g2" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g3" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g4" });
        I.pressKeys("Escape", "ArrowRight");

        // open the popup-menu
        await spreadsheet.selectCell("K2");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K2" });

        // Relativ ranges - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h2" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h3" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h4" });
        I.pressKeys("ArrowLeft", "2*ArrowDown");
        I.pressKey("Escape");

        // open the popup-menu
        await spreadsheet.selectCell("J4");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J4" });

        // Relativ ranges - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g4" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g5" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g6" });
        I.pressKeys("Escape", "ArrowRight");

        // open the popup-menu
        await spreadsheet.selectCell("K4");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K4" });

        // Relativ ranges - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h4" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h5" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h6" });
        I.pressKeys("Escape", "ArrowLeft");

        // open the popup-menu
        await spreadsheet.selectCell("J6");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J6" });

        // Relativ ranges - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g6" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g7" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g8" });
        I.pressKeys("ArrowRight", "Escape");

        // open the popup-menu
        await spreadsheet.selectCell("K6");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K6" });

        // Relativ ranges - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h6" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h7" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h8" });
        I.pressKeys("Escape", "3*ArrowDown", "ArrowLeft");

        // open the popup-menu
        await spreadsheet.selectCell("J10");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J10" });

        // name with relative range - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g10" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g11" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g12" });
        I.pressKeys("Escape", "ArrowRight");

        // open the popup-menu
        await spreadsheet.selectCell("K10");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K10" });

        // name with relative range - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h10" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h11" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h12" });
        I.pressKeys("Escape", "2*ArrowDown", "ArrowLeft");

        // open the popup-menu
        await spreadsheet.selectCell("J12");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J12" });

        // name with relative range - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g12" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g13" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "g14" });
        I.pressKeys("Escape", "ArrowRight");

        // open the popup-menu
        await spreadsheet.selectCell("K12");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K12" });

        // name with relative range - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h12" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h13" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "h14" });
        I.pressKeys("Escape", "ArrowLeft", "5*ArrowDown");

        await spreadsheet.selectCell("J17");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J17" });

        // relative ranges from other sheet - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d2" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d3" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });
        I.pressKeys("Escape", "ArrowRight");

        await spreadsheet.selectCell("K17");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K17" });

        // relative ranges from other sheet - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e2" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e3" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });
        I.pressKeys("Escape", "2*ArrowDown", "ArrowLeft");

        await spreadsheet.selectCell("J19");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "J19" });

        // relative ranges from other sheet - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d4" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d5" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2d6" });
        I.pressKeys("Escape", "ArrowRight");

        await spreadsheet.selectCell("K19");
        I.waitForAndClick({ spreadsheet: "cell-button", menu: "validation", address: "K19" });

        // relative ranges from other sheet - check the values
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e4" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e5" });
        I.waitForElement({ docs: "button",  inside: "popup-menu", value: "2e6" });
    }

    await I.loginAndOpenDocument("media/files/list_validation.xlsx");

    // create a copy of the sheet
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/copysheet/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    await testAllCellLists();

    await I.reopenDocument();
    await testAllCellLists();

    I.closeDocument();
}).tag("stable");
