/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import { Env } from "../../utils/config";

Feature("Documents > Spreadsheet > Wopi > Mandatory");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook({ wopi: true });
});

// tests ======================================================================

Scenario("[WOPI_SP01] Create a new spreadsheet document with WOPI (single tab mode)", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new spreadsheet document
    I.loginAndHaveNewDocument("spreadsheet", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for keyboard
    // go to cell A1 ...
    I.pressKeys("Home", "PageUp");
    // ... and insert text
    I.type("Hello World!");
    I.pressKeys("Enter", "ArrowUp");
    await I.confirmContentInWopiSpreadsheetApp("A1", "Hello World!");

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    await I.confirmContentInWopiSpreadsheetApp("A1", "Hello World!");

    // close document and expect to arrive in Drive with the new document
    I.closeDocument({ wopi: true });
    drive.waitForFile("unnamed.xlsx");

}).tag("smoketest").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_SP02] Open an existing spreadsheet document with WOPI", async ({ I, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // open existing document
    await I.loginAndOpenDocument("media/files/1cell.xlsx", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // expect initial text contents
    wopi.clickDocumentCanvas(); // required for keyboard
    I.pressKeys("Home", "PageUp"); // go to cell A1
    await I.confirmContentInWopiSpreadsheetApp("A1", "10101");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("smoketest").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_SP03] Create a new spreadsheet document with WOPI (tabbed mode)", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new spreadsheet document
    I.loginAndHaveNewDocument("spreadsheet", { wopi: true, tabbedMode: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for keyboard
    // go to cell A1 ...
    I.pressKeys("Home", "PageUp");
    // ... and insert text
    I.type("Hello World!");
    I.pressKeys("Enter", "ArrowUp");
    await I.confirmContentInWopiSpreadsheetApp("A1", "Hello World!");

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    await I.confirmContentInWopiSpreadsheetApp("A1", "Hello World!");

    // close document and expect to arrive in Portal
    I.closeDocument({ expectPortal: "spreadsheet", wopi: true });

    // the new file is shown in the list of recent documents
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.xlsx" }, 10);

    // three tabs open after closing the document
    const tabCount = await I.grabNumberOfOpenTabs();
    expect(tabCount).to.equal(3);

    // Drive must contain the new document
    I.switchToFirstTab();
    drive.waitForApp();
    drive.waitForFile("unnamed.xlsx");

}).tag("stable").tag("wopi");
