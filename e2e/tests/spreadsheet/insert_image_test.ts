/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Image");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C61259] Insert Image (default)", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("spreadsheet");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");
    dialogs.waitForVisible();

    // folder, image name
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();

}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C63585] From Drive", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("spreadsheet");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });
    I.click({ docs: "button", inside: "popup-menu", value: "drive" });
    dialogs.waitForVisible();

    // folder and image name
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C61262] From URL", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });
    I.click({ docs: "button", inside: "popup-menu", value: "url" });
    dialogs.waitForVisible();

    // insert image URL
    const catURL = "https://www.catsbest.de/wp-content/uploads/wesen-der-katze-e1607345963935-1920x600.jpg";
    I.fillField({ docs: "dialog", find: "input" }, catURL);

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

// TODO: Redesign test
Scenario("[C309947] From URL - exceeds the maximum", async ({ I, dialogs, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickOptionButton("image/insert/dialog", "url");
    dialogs.waitForVisible();

    // insert image URL
    //TODO: use smaller image
    I.fillField({ docs: "dialog", find: ".docs-textfield:nth-child(1) > input" }, "https://upload.wikimedia.org/wikipedia/commons/b/b4/-15wiki.jpg");

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    alert.waitForVisible("The image cannot be embedded into the document because it exceeds");
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
});

//-----------------------------------------------------------------------------

Scenario("[C328564] Insert Image (default) (ODS)", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty.ods");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");
    dialogs.waitForVisible();

    // folder and image name
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C61264] From Drive (ODS)", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty.ods");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });
    I.click({ docs: "button", value: "drive",  inside: "popup-menu" });
    dialogs.waitForVisible();

    // folder and image name
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C61267] From URL (ODS)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickOptionButton("image/insert/dialog", "url");
    dialogs.waitForVisible();

    // insert image URL
    const catURL = "https://www.catsbest.de/wp-content/uploads/wesen-der-katze-e1607345963935-1920x600.jpg";
    I.fillField({ docs: "dialog", find: "input" }, catURL);

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C273756] Insert an image from Local file in a new inserted sheet (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.insertSheet();

    // insert a local image
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.chooseFile({ docs: "button", inside: "popup-menu", value: "local" }, "media/images/100x100.png");
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C61261] Local file", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert a local image
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.chooseFile({ docs: "button", inside: "popup-menu", value: "local" }, "media/images/100x100.png");
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

// after insertion the exif orientation is applied automatically via operation
Scenario("[DOCS-3610b] exif rotation, add new image via drive", async ({ I, dialogs, drive }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/imageExifOrientation8.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("spreadsheet");

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // image is inserted
    I.waitForElement({ css: ".page > .pagecontent > .p > div.rotated-drawing.selected" });
    let transformAfterOpen = await I.grab2dCssTransformationFrom({ css: ".page > .pagecontent > .p > div.rotated-drawing.selected" });
    expect(transformAfterOpen.rotateDeg).to.be.equal(270);

    await I.reopenDocument();
    I.waitForElement({ css: ".page > .pagecontent > .p > div.rotated-drawing" });
    transformAfterOpen = await I.grab2dCssTransformationFrom({ css: ".page > .pagecontent > .p > div.rotated-drawing" });
    expect(transformAfterOpen.rotateDeg).to.be.equal(270);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C61266] From Drive (ODS)", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty.ods");

    // open the "Insert image" dialog
    I.clickToolbarTab("insert");
    I.clickOptionButton("image/insert/dialog", "drive");
    dialogs.waitForVisible();

    // folder and image name
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".modal.flex.folder-picker-dialog.zero-padding.add-infostore-file.in .modal-body .preview-pane .fileinfo dd.size" });

    // insert the image
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    // reload document, check image again
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");
