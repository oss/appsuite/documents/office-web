/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Sheet tab bar > Sheet operations");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8225] Hide sheet - multiple sheet document", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "popup-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "document/deletesheet", inside: "popup-menu", withText: "Delete sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "popup-menu", withText: "Reorder sheets" });

    I.waitForAndClick({ docs: "control", key: "sheet/visible", inside: "popup-menu", withText: "Hide sheet" });

    spreadsheet.waitForActiveSheet(0);

    await I.reopenDocument();

    spreadsheet.waitForActiveSheet(0);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38805] Hide sheet - multiple sheet document (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.insertSheet();

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/deletesheet", inside: "popup-menu", withText: "Delete sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "popup-menu", withText: "Reorder sheets" });

    I.waitForAndClick({ docs: "control", key: "sheet/visible", inside: "popup-menu", withText: "Hide sheet" });

    spreadsheet.waitForActiveSheet(0);

    await I.reopenDocument();

    spreadsheet.waitForActiveSheet(0);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8227] Rename sheet - single sheet document", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "popup-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });

    I.waitForAndClick({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });

    //dialog
    I.fillField({ docs: "dialog", find: ".sheet-name-input input" }, "hello world");

    //close dialog
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "hello world", state: 0 });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "hello world", state: 0 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38806] Rename sheet - single sheet document (ODS)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });

    I.waitForAndClick({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });

    //dialog
    I.fillField({ docs: "dialog", find: ".sheet-name-input input" }, "hello world");

    //close dialog
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    //check the active state and the sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "hello world", state: 0 });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "hello world", state: 0 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8228] Rename sheet with invalid signs", ({ I, alert, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //Sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //context: check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "popup-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });

    I.waitForAndClick({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });

    //dialog
    dialogs.waitForVisible();

    //type invalid signs
    I.fillField({ docs: "dialog", find: "input" }, "/\\*?:");

    dialogs.clickOkButton();

    //check the alert
    alert.waitForVisible("This sheet name contains invalid characters. Please ensure that the name does not contain / \\\\ * ? : [ or ], and does neither start nor end with an apostrophe.");
    alert.clickCloseButton();

    dialogs.clickCancelButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8870] Rename sheet via double click - single sheet document", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    //Sheet name
    I.waitForAndDoubleClick({ docs: "control", key: "view/sheet/active", pane: "status-pane", withText: "Sheet1", state: 0 });

    //check the buttons
    dialogs.waitForVisible();
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=ok]" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action=cancel]" });
    I.fillField({ docs: "dialog", find: "input" }, "newName");
    dialogs.clickOkButton();

    //Sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", pane: "status-pane", withText: "newName", state: 0 });

    await I.reopenDocument();

    //Sheet name
    I.waitForVisible({ docs: "control", key: "view/sheet/active", pane: "status-pane", withText: "newName", state: 0 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8229] Insert sheet", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet1" });

    I.waitForAndClick({ docs: "control", key: "document/insertsheet", inside: "tool-pane" });

    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet2" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet1" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35081] Insert sheet (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet1" });

    I.waitForAndClick({ docs: "control", key: "document/insertsheet", inside: "tool-pane" });

    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet2" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet1" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", inside: "tool-pane", withText: "Sheet2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8230] Unhide sheet - multiple sheet document", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/2hiddensheets.xlsx");

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Sheet3" });

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //Popup check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "popup-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "document/deletesheet", inside: "popup-menu", withText: "Delete sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "popup-menu", withText: "Reorder sheets" });

    I.waitForAndClick({ docs: "control", key: "document/showsheets/dialog", inside: "popup-menu", withText: "Unhide sheets" });

    //select all hidden sheets
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-unhide-sheets-dialog", find: ".docs-checkbox.select-all input" });

    //dialog
    dialogs.waitForVisible();
    dialogs.clickOkButton();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet3" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38808] Unhide sheet - multiple sheet document (ODS)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/2hiddensheets.ods");

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle3" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Tabelle2" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Tabelle1" });

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //Popup check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/deletesheet", inside: "popup-menu", withText: "Delete sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "popup-menu", withText: "Reorder sheets" });

    I.waitForAndClick({ docs: "control", key: "document/showsheets/dialog", inside: "popup-menu", withText: "Unhide sheets" });

    //select all hidden sheets
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-unhide-sheets-dialog", find: ".docs-checkbox.select-all input" });

    //dialog
    dialogs.waitForVisible();
    dialogs.clickOkButton();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle1" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle2" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle3" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle1" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle2" });
    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Tabelle3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8226] Delete sheet - multiple sheet document", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.insertSheet();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.waitForAndClick({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //Popup check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "document/copysheet/dialog", inside: "popup-menu", withText: "Copy sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "popup-menu", withText: "Reorder sheets" });

    I.waitForAndClick({ docs: "control", key: "document/deletesheet", inside: "popup-menu", withText: "Delete sheet" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8231] Reorder sheets dialog (with two sheets)", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert a sheet
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/movesheets/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //inactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='next']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    //move the sheet2
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });

    //inactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='prev']" });

    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C38809] Reorder sheets dialog with three sheets (ODS)", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 2 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/movesheets/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //inactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='next']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    //select sheet2 button
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".badge-list button[data-sheet='1']", withText: "Sheet2" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    //inactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='prev']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });

    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 2 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 2 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C296707] Reorder sheets dialog (with four sheets)", async ({ I, dialogs, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 3 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/movesheets/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //inactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='next']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    //move sheet3
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".badge-list button[data-sheet='2']", withText: "Sheet3" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });

    //deactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='prev']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });

    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C296708] Reorder sheets dialog with four sheets (ODS)", async ({ I, dialogs, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 3 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    //open the context
    I.clickButton("view/sheet/more", { caret: true });
    I.clickButton("document/movesheets/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    //inactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='next']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    //move sheet3
    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".badge-list button[data-sheet='2']", withText: "Sheet3" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='prev']" });

    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='first']" });

    //deactive buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='first']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button[disabled][data-action='prev']" });

    //active buttons
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='last']" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-spreadsheet-move-sheets-dialog", find: ".button-container button:not([disabled])[data-action='next']" });

    dialogs.clickOkButton();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });

    await I.reopenDocument();

    //check the order / sheet name
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", value: 0 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", value: 1 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", value: 2 });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", value: 3 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8232] Reorder sheets - drag & drop", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet1", state: 0 });

    //insert 4 sheets
    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet2", state: 1 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet4", state: 3 });

    spreadsheet.insertSheet();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet5", state: 4 });

    //move sheet5 via mouse
    I.dragMouseOnElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 4 }, -180, 0); //-140, 0
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet5", state: 2 });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", withText: "Sheet3", state: 2 });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C35089] Delete sheet - multiple sheet document (ODS)", async ({ I, spreadsheet, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    spreadsheet.insertSheet();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.waitForAndClick({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });

    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });

    //Popup check the active entries
    I.waitForElement({ docs: "control", key: "sheet/rename/dialog", inside: "popup-menu", withText: "Rename sheet" });
    I.waitForElement({ docs: "control", key: "sheet/locked", inside: "popup-menu", withText: "Protect sheet" });
    I.waitForElement({ docs: "control", key: "document/movesheets/dialog", inside: "popup-menu", withText: "Reorder sheets" });

    I.waitForAndClick({ docs: "control", key: "document/deletesheet", inside: "popup-menu", withText: "Delete sheet" });

    dialogs.waitForVisible();
    I.waitForElement({ docs: "dialog", withText: "Deleting a sheet cannot be undone." });
    dialogs.clickOkButton();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "view/sheet/active", withText: "Sheet1" });
    I.dontSeeElement({ docs: "control", key: "view/sheet/active", withText: "Sheet2" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
