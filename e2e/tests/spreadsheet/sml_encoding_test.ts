/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > SML encoding");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[DOCS-4490] SML string encoding", async ({ I, spreadsheet }) => {

    async function checkStringEncoding(): Promise<void> {

        // wait for recalculation of all test formulas and conditional formats
        spreadsheet.waitForRecalculation();

        // cell contents
        await spreadsheet.selectCell("B3", { value: "\x18", display: "\x18" });
        await spreadsheet.selectCell("C3", { value: "\x18\x19\x1a", display: "\x18\x19\x1a" });
        await spreadsheet.selectCell("D3", { value: "_x0018_", display: "_x0018_" });
        await spreadsheet.selectCell("E3", { value: "_x0018_x0019_x001a_", display: "_x0018_x0019_x001a_" });

        // cell formulas
        await spreadsheet.selectCell("B4", { value: 1, formula: 'LEN("\x18")' });
        await spreadsheet.selectCell("C4", { value: 3, formula: 'LEN("\x18\x19\x1a")' });
        await spreadsheet.selectCell("D4", { value: 7, formula: 'LEN("_x0018_")' });
        await spreadsheet.selectCell("E4", { value: 19, formula: 'LEN("_x0018_x0019_x001a_")' });

        // shared formulas
        await spreadsheet.selectCell("B5", { value: 1, formula: 'LEN("\x18")', shared: 0 });
        await spreadsheet.selectCell("B6", { value: 1, formula: 'LEN("\x18")', shared: 0 });
        await spreadsheet.selectCell("B7", { value: 1, formula: 'LEN("\x18")', shared: 0 });
        await spreadsheet.selectCell("D5", { value: 7, formula: 'LEN("_x0018_")', shared: 1 });
        await spreadsheet.selectCell("D6", { value: 7, formula: 'LEN("_x0018_")', shared: 1 });
        await spreadsheet.selectCell("D7", { value: 7, formula: 'LEN("_x0018_")', shared: 1 });

        // matrix formulas
        await spreadsheet.selectCell("C5", { value: 3, formula: 'LEN("\x18\x19\x1a")', matrix: "C5:C7" });
        await spreadsheet.selectCell("C6", { value: 3, formula: 'LEN("\x18\x19\x1a")', matrix: "C5:C7" });
        await spreadsheet.selectCell("C7", { value: 3, formula: 'LEN("\x18\x19\x1a")', matrix: "C5:C7" });
        await spreadsheet.selectCell("E5", { value: 19, formula: 'LEN("_x0018_x0019_x001a_")', matrix: "E5:E7" });
        await spreadsheet.selectCell("E6", { value: 19, formula: 'LEN("_x0018_x0019_x001a_")', matrix: "E5:E7" });
        await spreadsheet.selectCell("E7", { value: 19, formula: 'LEN("_x0018_x0019_x001a_")', matrix: "E5:E7" });

        // number format code
        await spreadsheet.selectCell("D8", { value: "a", display: "a_x0018_" });

        // hyperlinks
        await spreadsheet.selectCell("B9", { value: "#\x18", hlink: "https://example.org/?%18#\x18" });
        await spreadsheet.selectCell("D9", { value: "#_x0018_", hlink: "https://example.org/?_x0018_#_x0018_" });

        // font name and cell style
        await spreadsheet.selectCell("B10", { renderFont: "calibri\x18" });
        await spreadsheet.selectCell("D10", { renderFont: "calibri_x0018_" });
        await spreadsheet.selectCell("B11", { renderFont: "calibri\x18" });
        I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Custom\x18" });
        await spreadsheet.selectCell("D11", { renderFont: "calibri_x0018_" });
        I.waitForVisible({ docs: "control", key: "cell/stylesheet", state: "Custom_x0018_" });

        // sheet names
        I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 2, withText: "\x18" });
        I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 3, withText: "_x0018_" });
        I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 4, withText: "_x005f_x0018_" });

        // named ranges
        I.clickToolbarTab("data");
        I.clickButton("view/namesmenu/toggle");
        I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".text", withText: "_x0018_" });
        I.waitForVisible({ spreadsheet: "name-item", index: 0, find: ".formula", withText: "_x0018_" });
        I.clickButton("view/namesmenu/toggle");

        // data validation
        // TODO: cell D12: support for other rules
        await spreadsheet.selectCell("D13");
        I.waitForAndClick({ spreadsheet: "cell-button", address: "D13", menu: "validation" });
        I.waitForVisible({ docs: "button", inside: "popup-menu", value: "_x0018_", withText: "_x0018_" });
        I.waitForVisible({ docs: "button", inside: "popup-menu", value: "_x0018_x0019_x001a_", withText: "_x0018_x0019_x001a_" });
        I.pressKey("Escape");
        // TODO: cell D14: support for prompt and error message

        // conditional formatting
        await spreadsheet.selectCell("D15", { renderFill: "#00ff00" });
        await spreadsheet.selectCell("D16", { renderFill: "#00ff00" });
        await spreadsheet.selectCell("D17", { renderFill: "#00ff00" });
        await spreadsheet.selectCell("D18", { renderFill: "#00ff00" });
        await spreadsheet.selectCell("D19", { renderFill: "#00ff00" });
        await spreadsheet.selectCell("D20", { renderFill: "#00ff00" });
        await spreadsheet.selectCell("D21", { cfDataBar: "0 7 #00ff00" });
        await spreadsheet.selectCell("D22", { cfDataBar: "0 7 #00ff00" });
        await spreadsheet.selectCell("D23", { cfIconSet: "3trafficlights1 3" });

        // cell notes
        I.clickToolbarTab("comments");
        I.clickButton("view/notes/menu", { caret: true });
        I.clickButton("note/show/all", { inside: "popup-menu" });
        I.waitForVisible({ spreadsheet: "note", pos: 2, para: 1, withText: "\u25af" }, 3); // TODO: note renderer replaces control characters with U+25AF
        I.waitForVisible({ spreadsheet: "note", pos: 3, para: 1, withText: "_x0018_" }, 3);
        I.clickButton("view/notes/menu", { caret: true });
        I.clickButton("note/hide/all", { inside: "popup-menu" });

        // threaded comments
        I.clickButton("view/settings/menu", { caret: true });
        I.clickButton("view/commentspane/show", { inside: "popup-menu" });
        I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "\x18" });
        I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "_x0018_" });
        I.click({ docs: "commentspane", find: ".header .btn[data-action=close]" });

        // drawing shape
        I.waitForVisible({ spreadsheet: "drawing", pos: 1, para: 0, withText: "_x0018_" });

        // chart source ranges
        // TODO

        // table name
        spreadsheet.activateSheet(1);
        I.waitForVisible({ spreadsheet: "grid-pane", find: '.table-layer .range[data-range="A1:A7"][data-name="t_x0018_"]' });

        // discrete filter entries
        I.waitForAndClick({ spreadsheet: "cell-button", address: "A1", menu: "table" });
        I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "\x18", checked: true });
        I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "\x18 x", checked: false });
        I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "_x0018_", checked: true });
        I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "_x0018_ x", checked: false });
        I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "_x005f_x0018_", checked: true });
        I.waitForVisible({ docs: "checkitem", inside: "popup-menu", key: "table/filter/discrete/list", value: "_x005f_x0018_ x", checked: false });
        I.pressKey("Escape");
    }

    await I.loginAndOpenDocument("media/files/DOCS-4490-sml.xlsx");

    // check all imported strings
    await checkStringEncoding();

    // delete and restore columns
    spreadsheet.activateSheet(0);
    spreadsheet.selectColumns("B:E");
    I.clickToolbarTab("colrow");
    I.clickButton("column/delete");
    I.waitForChangesSaved();
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // reload, and try again
    await I.reopenDocument();
    await checkStringEncoding();

    I.closeDocument();
}).tag("stable");
