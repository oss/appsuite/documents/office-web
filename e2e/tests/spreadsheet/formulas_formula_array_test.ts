/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > formulas > Formula - array");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C114841] Formula - can type a simple array formula", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/data_array.xlsx");

    await spreadsheet.selectCell("C8");
    spreadsheet.enterCellText("=SUM(B3:B7*C3:C7", { leave: "matrix" });

    //check the cell values, and check the matrix formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=SUM(B3:B7*C3:C7)}");
    spreadsheet.waitForActiveCell("C8", { display: "$940.00 ", formula: "SUM(B3:B7*C3:C7)" }); //do not removed empty "$940.00 "

    await I.reopenDocument();

    //check the cell values, and check the matrix formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=SUM(B3:B7*C3:C7)}");
    spreadsheet.waitForActiveCell("C8", { display: "$940.00 ", formula: "SUM(B3:B7*C3:C7)" }); //do not removed empty "$940.00 "

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114842] Formula - Formula - can type a vertical array constant", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("11");
    spreadsheet.enterCellText("22");
    spreadsheet.enterCellText("33");
    spreadsheet.enterCellText("44");

    await spreadsheet.selectRange("A1:A4");

    //enter matrix formula
    spreadsheet.enterCellText("={11|22|33|44}", { leave: "matrix" });

    //check the cell values, and check the matrix formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "={11|22|33|44}");

    await spreadsheet.selectCell("A1", { display: "11",  formula: "{11|22|33|44}" });
    await spreadsheet.selectCell("A2", { display: "22",  formula: "{11|22|33|44}" });
    await spreadsheet.selectCell("A3", { display: "33",  formula: "{11|22|33|44}" });
    await spreadsheet.selectCell("A4", { display: "44",  formula: "{11|22|33|44}" });

    await I.reopenDocument();

    //check the cell values, and check the matrix formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "={11|22|33|44}");

    await spreadsheet.selectCell("A1", { display: "11",  formula: "{11|22|33|44}" });
    await spreadsheet.selectCell("A2", { display: "22",  formula: "{11|22|33|44}" });
    await spreadsheet.selectCell("A3", { display: "33",  formula: "{11|22|33|44}" });
    await spreadsheet.selectCell("A4", { display: "44",  formula: "{11|22|33|44}" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114843] Formula - can type a horizontal array constant", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("1", { leave: "right" });
    spreadsheet.enterCellText("2", { leave: "right" });
    spreadsheet.enterCellText("3", { leave: "right" });
    spreadsheet.enterCellText("4");

    await spreadsheet.selectRange("A1:D1");

    //enter matrix formula
    spreadsheet.enterCellText("={1;2;3;4}", { leave: "matrix" });

    //check the cell values, and check the matrix formula
    spreadsheet.waitForActiveCell("A1", { display: "1",  formula: "{1;2;3;4}" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "={1;2;3;4}");
    await spreadsheet.selectCell("B1", { display: "2",  formula: "{1;2;3;4}" });
    await spreadsheet.selectCell("C1", { display: "3",  formula: "{1;2;3;4}" });
    await spreadsheet.selectCell("D1", { display: "4",  formula: "{1;2;3;4}" });

    await I.reopenDocument();

    //check the cell values, and check the matrix formula
    await spreadsheet.selectCell("A1", { display: "1",  formula: "{1;2;3;4}" });
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "={1;2;3;4}");
    await spreadsheet.selectCell("B1", { display: "2",  formula: "{1;2;3;4}" });
    await spreadsheet.selectCell("C1", { display: "3",  formula: "{1;2;3;4}" });
    await spreadsheet.selectCell("D1", { display: "4",  formula: "{1;2;3;4}" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114844] Formula - can type a naming array constant", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/naming_constant_data.xlsx");

    await spreadsheet.selectRange("A2:A8");

    spreadsheet.enterCellText("=Day", { leave: "matrix" });

    //check the cell values, and check the matrix formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=Day}");

    await spreadsheet.selectCell("A2", { display: "Monday", formula: "Day" });
    await spreadsheet.selectCell("A3", { display: "Tuesday", formula: "Day" });
    await spreadsheet.selectCell("A4", { display: "Wednesday", formula: "Day" });
    await spreadsheet.selectCell("A5", { display: "Thursday", formula: "Day" });
    await spreadsheet.selectCell("A6", { display: "Friday", formula: "Day" });
    await spreadsheet.selectCell("A7", { display: "Saturday", formula: "Day" });
    await spreadsheet.selectCell("A8", { display: "Sunday", formula: "Day" });

    await I.reopenDocument();

    //check the cell values, and check the matrix formula
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=Day}");

    await spreadsheet.selectCell("A2", { display: "Monday", formula: "Day" });
    await spreadsheet.selectCell("A3", { display: "Tuesday", formula: "Day" });
    await spreadsheet.selectCell("A4", { display: "Wednesday", formula: "Day" });
    await spreadsheet.selectCell("A5", { display: "Thursday", formula: "Day" });
    await spreadsheet.selectCell("A6", { display: "Friday", formula: "Day" });
    await spreadsheet.selectCell("A7", { display: "Saturday", formula: "Day" });
    await spreadsheet.selectCell("A8", { display: "Sunday", formula: "Day" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C114846] Formula - insert/delete columns or rows into a matrix formula is forbidden", async ({ I, alert }) => {

    await  I.loginAndOpenDocument("media/files/array_error_handling.xlsx");

    // Select and insert row
    I.waitForVisible({ spreadsheet: "row-resize", pos: "4", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "4" });
    I.waitForContextMenuVisible();
    I.clickButton("row/insert", { inside: "context-menu" });

    alert.waitForVisible("It is not possible to insert cells into a matrix formula.");
    alert.clickCloseButton();

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114847] Formula - Single cells inside a matrix formula cannot be changed", async ({ I, alert, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/array_error_single_cells.xlsx");
    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=B3:B7*C3:C7*0.1}");

    spreadsheet.enterCellText("2", { expectError: true });
    alert.waitForVisible("Parts of a matrix formula cannot be changed.");
    alert.clickCloseButton();

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114848] Formula - can type a horizontal array constant", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("=munit(4)", { leave: "matrix" });

    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");

    //check the values
    await spreadsheet.selectCell("A1", { display: "1", value: 1,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("A2", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("A3", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("A4", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B1", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B2", { display: "1", value: 1,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B3", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B4", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C1", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C2", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C3", { display: "1", value: 1,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C4", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D1", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D2", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D3", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D4", { display: "1", value: 1,  formula: "MUNIT(4)" });

    await I.reopenDocument();

    I.seeInField({ css: ".view-pane.formula-pane textarea" }, "{=MUNIT(4)}");

    //check the values
    await spreadsheet.selectCell("A1", { display: "1", value: 1,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("A2", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("A3", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("A4", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B1", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B2", { display: "1", value: 1,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B3", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("B4", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C1", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C2", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C3", { display: "1", value: 1,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("C4", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D1", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D2", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D3", { display: "0", value: 0,  formula: "MUNIT(4)" });
    await spreadsheet.selectCell("D4", { display: "1", value: 1,  formula: "MUNIT(4)" });

    I.closeDocument();
}).tag("stable");
