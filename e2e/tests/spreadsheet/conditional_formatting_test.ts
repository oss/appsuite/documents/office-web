/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// private functions ==========================================================

function get1TodayStr(): string {
    const date = new Date();
    date.setDate(date.getDate() + 1);
    const m = String(date.getMonth() + 1).padStart(2, "0");
    const d = String(date.getDate()).padStart(2, "0");
    const y = date.getFullYear();
    return `${m}/${d}/${y}`;
}

function get2TodayStr(): string {
    const date = new Date();
    date.setDate(date.getDate());
    const m = String(date.getMonth() + 1).padStart(2, "0");
    const d = String(date.getDate()).padStart(2, "0");
    const y = date.getFullYear();
    return `${m}/${d}/${y}`;
}

// tests ======================================================================

Feature("Documents > Spreadsheet > Format > Cell format > Conditional formatting");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C85767] Conditional formatting - can import and use rule 'cell value is between'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_cellvalue_between.xlsx");

    spreadsheet.enterCellText("4", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 4, renderFill: "transparent", renderColor: "#000000" });

    spreadsheet.enterCellText("3", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 3, renderFill: "#7030a0", renderColor: "#000000" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("C3", { value: 3, renderFill: "#7030a0", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C85794] Conditional formatting - can import and use rule 'cell value greater'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_cellvalue_greater.xlsx");

    spreadsheet.enterCellText("3", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 3, renderFill: "transparent", renderColor: "#000000"  });

    spreadsheet.enterCellText("4", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 4, renderFill: "#7030a0", renderColor: "#000000" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("C3", { value: 4, renderFill: "#7030a0", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85769] Conditional formatting - can import and use rule 'date'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_today_rule.xlsx");

    spreadsheet.enterCellText("=today()+1", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { display: get1TodayStr(), formula: "TODAY()+1", renderFill: "transparent", renderColor: "#000000" });

    spreadsheet.enterCellText("=today()", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { display: get2TodayStr(), formula: "TODAY()", renderFill: "hsl(209,59%,92%)", renderColor: "#000000", renderFontStyle: "italic"  });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("C3", { display: get2TodayStr(), formula: "TODAY()", renderFill: "hsl(209,59%,92%)", renderColor: "#000000", renderFontStyle: "italic"  });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85771] Conditional formatting - can import and use rule 'cell value unique'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_unique_rule.xlsx");

    //C3 - orange bg color, yellow text color
    spreadsheet.enterCellText("1", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 1, renderFill: "#ffc000", renderColor: "#ffff00" });
    I.pressKey("ArrowDown");

    //C4 - no bg color, black text color
    spreadsheet.enterCellText("1", { leave: "stay" });
    spreadsheet.waitForActiveCell("C4", { value: 1, renderFill: "transparent",  renderColor: "#000000" });

    //C3 - no bg color, black text color
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "transparent", renderColor: "#000000" });

    //C4 - orange bg color, yellow text color, black border
    I.pressKey("ArrowDown");
    spreadsheet.enterCellText("2", { leave: "stay" });
    spreadsheet.waitForActiveCell("C4", { value: 2, renderFill: "#ffc000",  renderColor: "#ffff00", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000" });

    //C3 - orange bg color, yellow text color, black border
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000"  });

    await I.reopenDocument();

    //C3 - orange bg color, yellow text color
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000"  });
    //C4 - orange bg color, yellow text color
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderTop: "1px single #000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85805] Conditional formatting - can import and use rule 'cell value duplicate'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_duplicate_rule.xlsx");

    await spreadsheet.selectCell("C3", { value: 0, renderFill: "transparent",  renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 3, renderFill: "transparent",  renderColor: "#000000" });

    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("3",  { leave: "stay" });

    //C3 - orange bg color, yellow text color, black border
    await spreadsheet.selectCell("C3", { value: 3, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    //C4 - no bg color, black text color
    await spreadsheet.selectCell("C4", { value: 1, renderFill: "transparent",  renderColor: "#000000" });
    //C5 - no bg color, black text color
    await spreadsheet.selectCell("C5", { value: 2, renderFill: "transparent", renderColor: "#000000" });
    //C6 - orange bg color, yellow text color, black border
    await spreadsheet.selectCell("C6", { value: 3, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    await I.reopenDocument();

    //C3 - orange bg color, yellow text color
    await spreadsheet.selectCell("C3", { value: 3, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });
    //C4 - no bg color, black text color
    await spreadsheet.selectCell("C4", { value: 1, renderFill: "transparent", renderColor: "#000000" });
    //C5 - no bg color, black text color
    await spreadsheet.selectCell("C5", { value: 2, renderFill: "transparent", renderColor: "#000000" });
    //C6 - orange bg color, yellow text color
    await spreadsheet.selectCell("C6", { value: 3, renderFill: "#ffc000", renderColor: "#ffff00", renderBorderTop: "1px single #000000", renderBorderBottom: "1px single #000000", renderBorderLeft: "1px single #000000", renderBorderRight: "1px single #000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85776] Conditional formatting - can import and use rule 'text contains'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_text_rule.xlsx");

    //C3 - no bg color, black text color
    await spreadsheet.selectCell("C3", { value: "dd", renderFill: "transparent", renderColor: "#000000" });
    spreadsheet.enterCellText("cat", { leave: "stay" });

    //C3 - yellow bg color/ black text
    spreadsheet.waitForActiveCell("C3", { value: "cat", renderFill: "#ffff00", renderColor: "#000000" });

    await I.reopenDocument();

    //C3 - yellow bg color/ black text
    spreadsheet.waitForActiveCell("C3", { value: "cat", renderFill: "#ffff00", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85796] Conditional formatting - can import and use rule 'topN'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_topn.xlsx");

    //initial values
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "#ffff00",  renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { value: 5, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { value: 6, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { value: 7, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C10", { value: 8, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C11", { value: 9, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C12", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C13", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C14", { value: 10, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C15", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C16", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C17", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C18", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C19", { value: 0, renderFill: "transparent", renderColor: "#000000" });

    spreadsheet.enterCellText("20");

    //changes values
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { value: 5, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { value: 6, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { value: 7, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C10", { value: 8, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C11", { value: 9, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C12", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C13", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C14", { value: 10, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C15", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C16", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C17", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C18", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C19", { value: 20, renderFill: "#ffff00", renderColor: "#000000" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { value: 5, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { value: 6, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { value: 7, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C10", { value: 8, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C11", { value: 9, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C12", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C13", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C14", { value: 10, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C15", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C16", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C17", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C18", { value: 0, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C19", { value: 20, renderFill: "#ffff00", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85814] Conditional formatting - can import and use rule 'topPercent'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_top_percent_rule.xlsx");

    //initial values
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ffff00", renderColor: "#000000" });

    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("5", { leave: "stay" });

    //change values
    await spreadsheet.selectCell("C3", { value: 5, renderFill: "#ffff00", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "transparent", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85816] Conditional formatting - can import and use rule 'colorScale'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_color_scale_2_rule.xlsx");

    //initial values
    await spreadsheet.selectCell("C3", { value: 1, renderFill: "#ff7128", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ff8a3f", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "#ffa356",  renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ffbd6e", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { value: 5, renderFill: "#ffd685", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { value: 6, renderFill: "#ffef9c", renderColor: "#000000" });

    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("10", { leave: "stay" });

    //change values
    await spreadsheet.selectCell("C3", { value: 10, renderFill: "#ffef9c", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ff7128", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "#ff8137", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ff9145", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { value: 5, renderFill: "#ffa054", renderColor: "#000000"  });
    await spreadsheet.selectCell("C8", { value: 6, renderFill: "#ffb062", renderColor: "#000000"  });

    await I.reopenDocument();

    //check again the values
    await spreadsheet.selectCell("C3", { value: 10, renderFill: "#ffef9c", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 2, renderFill: "#ff7128", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 3, renderFill: "#ff8137", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 4, renderFill: "#ff9145", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { value: 5, renderFill: "#ffa054", renderColor: "#000000"  });
    await spreadsheet.selectCell("C8", { value: 6, renderFill: "#ffb062", renderColor: "#000000"  });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85768] Conditional formatting - can import and use rule 'cell value is between' (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_cellvalue_between.ods");

    spreadsheet.enterCellText("4", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 4, renderFill: "transparent", renderColor: "#000000" });

    spreadsheet.enterCellText("3", { leave: "stay" });
    spreadsheet.waitForActiveCell("C3", { value: 3, renderFill: "#7030a0", renderColor: "#000000" });

    await I.reopenDocument();

    spreadsheet.waitForActiveCell("C3", { value: 3, renderFill: "#7030a0", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C173428] Conditional formatting - can import and use rules 'with prioity' (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_priority.ods");

    //initial value
    //red bgcolor /black text color
    spreadsheet.waitForActiveCell("A6", { value: null, renderFill: "#ff6600", renderColor: "#000000" });

    spreadsheet.enterCellText("89", { leave: "stay" });

    //red bgcolor /black text color
    spreadsheet.waitForActiveCell("A6", { value: 89, renderFill: "#ff3333", renderColor: "#000000" });

    //change value
    spreadsheet.enterCellText("90", { leave: "stay" });

    //pink bgcolor /black text color
    spreadsheet.waitForActiveCell("A6", { value: 90, renderFill: "#ff33ff", renderColor: "#000000" });

    await I.reopenDocument();

    //pink bgcolor /black text color
    spreadsheet.waitForActiveCell("A6", { value: 90, renderFill: "#ff33ff", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85934] Conditional formatting - can import and use rule 'cell value unique' (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_unique_rule.ods");

    //initial values
    await spreadsheet.selectCell("C3", { display: "1", value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { display: "1", value: 1, renderFill: "transparent", renderColor: "#000000" });

    spreadsheet.enterCellText("2", { leave: "stay" });
    spreadsheet.waitForActiveCell("C4", { display: "2", value: 2, renderFill: "#579d1c", renderColor: "#000000" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("C3", { display: "1", value: 1, renderFill: "#579d1c", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { display: "2", value: 2, renderFill: "#579d1c", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85800] Conditional formatting - can import and use rule 'topN'(ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_top10.ods");

    //initial values
    await spreadsheet.selectCell("C3", { display: "1", value: 1, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { display: "2", value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { display: "3", value: 3, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { display: "4", value: 4, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { display: "5", value: 5, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { display: "6", value: 6, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { display: "7", value: 7, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C10", { display: "8", value: 8, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C11", { display: "9", value: 9, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C12", { display: "10", value: 10, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C13", { display: "11", value: 11, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C14", { display: "12", value: 12, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C15", { display: "13", value: 13, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C16", { display: "14", value: 14, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C17", { display: "15", value: 15, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C18", { display: "16", value: 16, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C19", { display: "17", value: 17, renderFill: "transparent", renderColor: "#ff33ff" });

    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("20", { leave: "stay" });

    //changes values
    await spreadsheet.selectCell("C3", { display: "20", value: 20, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C4", { display: "2", value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { display: "3", value: 3, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { display: "4", value: 4, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { display: "5", value: 5, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { display: "6", value: 6, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { display: "7", value: 7, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C10", { display: "8", value: 8, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C11", { display: "9", value: 9, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C12", { display: "10", value: 10, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C13", { display: "11", value: 11, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C14", { display: "12", value: 12, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C15", { display: "13", value: 13, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C16", { display: "14", value: 14, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C17", { display: "15", value: 15, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C18", { display: "16", value: 16, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C19", { display: "17", value: 17, renderFill: "transparent", renderColor: "#ff33ff" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("C3", { display: "20", value: 20, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C4", { display: "2", value: 2, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { display: "3", value: 3, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { display: "4", value: 4, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C7", { display: "5", value: 5, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C8", { display: "6", value: 6, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { display: "7", value: 7, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C10", { display: "8", value: 8, renderFill: "transparent", renderColor: "#000000" });
    await spreadsheet.selectCell("C11", { display: "9", value: 9, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C12", { display: "10", value: 10, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C13", { display: "11", value: 11, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C14", { display: "12", value: 12, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C15", { display: "13", value: 13, renderFill: "transparent", renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C16", { display: "14", value: 14, renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C17", { display: "15", value: 15, renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C18", { display: "16", value: 16,  renderColor: "#ff33ff" });
    await spreadsheet.selectCell("C19", { display: "17", value: 17,  renderColor: "#ff33ff"  });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85766] Conditional formatting - can import and use with formulas (ODS)", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_formulas.ods");

    //initial values
    await spreadsheet.selectCell("B4", { value: 1, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 23, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("D4", { value: 21, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("B5", { value: 22, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 36, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });

    //insert a row
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "5" });
    I.waitForContextMenuVisible();
    I.clickButton("row/insert", { inside: "context-menu" });
    I.waitForChangesSaved();

    //check new values
    await spreadsheet.selectCell("B5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("B6", { value: 22, renderFill: "#ff0000", renderColor: "#000000"  });
    await spreadsheet.selectCell("C6", { value: 36, renderFill: "#ff0000", renderColor: "#000000"  });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ff0000", renderColor: "#000000" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("B4", { value: 1, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 23, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("D4", { value: 21, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("B5", { value: null, renderFill: "#ff66ff", renderColor: "#000000"  });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "#ff66ff", renderColor: "#000000"  });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("B6", { value: 22, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 36, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ff0000", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85765] Conditional formatting - can import and use with formula", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_formulas.xlsx");

    //initial values
    await spreadsheet.selectCell("B4", { value: 0, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 23, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("D4", { value: 21, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("B5", { value: 22, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: 36, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });

    //insert a row
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "5" });
    I.waitForContextMenuVisible();
    I.clickButton("row/insert", { inside: "context-menu" });
    I.waitForChangesSaved();

    //check new values
    await spreadsheet.selectCell("B5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("B6", { value: 22, renderFill: "#ff0000", renderColor: "#000000"  });
    await spreadsheet.selectCell("C6", { value: 36, renderFill: "#ff0000", renderColor: "#000000"  });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ff0000", renderColor: "#000000" });

    await I.reopenDocument();

    //check the values again
    await spreadsheet.selectCell("B4", { value: 0, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("C4", { value: 23, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("D4", { value: 21, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("B5", { value: null, renderFill: "#ff66ff", renderColor: "#000000"  });
    await spreadsheet.selectCell("C5", { value: null, renderFill: "#ff66ff", renderColor: "#000000"  });
    await spreadsheet.selectCell("D5", { value: null, renderFill: "#ff66ff", renderColor: "#000000" });
    await spreadsheet.selectCell("B6", { value: 22, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("C6", { value: 36, renderFill: "#ff0000", renderColor: "#000000" });
    await spreadsheet.selectCell("D6", { value: null, renderFill: "#ff0000", renderColor: "#000000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C309009] ODS document with conditional formatting and special sheet can to be imported", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/docs_2110.ods");

    //B4 yellow bg color, and black text
    await spreadsheet.selectCell("B4", { value: "B", renderFill: "#fff200", renderColor: "#000000" });
    spreadsheet.enterCellText("C", { leave: "stay" });

    //B4 green bg color, and green text
    spreadsheet.waitForActiveCell("B4", { value: "C", renderFill: "#ccffcc", renderColor: "#006600" });

    await I.reopenDocument();

    //B4 green bg color, and green text
    spreadsheet.waitForActiveCell("B4", { value: "C", renderFill: "#ccffcc", renderColor: "#006600" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C332312] Conditional formatting - can import 'iconsets'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/icon_set.xlsx");

    await spreadsheet.selectCell("B2", { cfIconSet: "3arrows 1" });
    await spreadsheet.selectCell("C2", { cfIconSet: "3arrows 2" });
    await spreadsheet.selectCell("D2", { cfIconSet: "3arrows 3" });
    await spreadsheet.selectCell("B3", { cfIconSet: "3arrowsgray 1" });
    await spreadsheet.selectCell("C3", { cfIconSet: "3arrowsgray 2" });
    await spreadsheet.selectCell("D3", { cfIconSet: "3arrowsgray 3" });
    await spreadsheet.selectCell("B4", { cfIconSet: "3flags 1" });
    await spreadsheet.selectCell("C4", { cfIconSet: "3flags 2" });
    await spreadsheet.selectCell("D4", { cfIconSet: "3flags 3" });
    await spreadsheet.selectCell("B5", { cfIconSet: "3trafficlights1 1" });
    await spreadsheet.selectCell("C5", { cfIconSet: "3trafficlights1 2" });
    await spreadsheet.selectCell("D5", { cfIconSet: "3trafficlights1 3" });
    await spreadsheet.selectCell("B6", { cfIconSet: "3trafficlights2 1" });
    await spreadsheet.selectCell("C6", { cfIconSet: "3trafficlights2 2" });
    await spreadsheet.selectCell("D6", { cfIconSet: "3trafficlights2 3" });
    await spreadsheet.selectCell("B7", { cfIconSet: "3signs 1" });
    await spreadsheet.selectCell("C7", { cfIconSet: "3signs 2" });
    await spreadsheet.selectCell("D7", { cfIconSet: "3signs 3" });
    await spreadsheet.selectCell("B8", { cfIconSet: "3symbols 1" });
    await spreadsheet.selectCell("C8", { cfIconSet: "3symbols 2" });
    await spreadsheet.selectCell("D8", { cfIconSet: "3symbols 3" });
    await spreadsheet.selectCell("B9", { cfIconSet: "3symbols2 1" });
    await spreadsheet.selectCell("C9", { cfIconSet: "3symbols2 2" });
    await spreadsheet.selectCell("D9", { cfIconSet: "3symbols2 3" });
    await spreadsheet.selectCell("B10", { cfIconSet: "3stars 1" });
    await spreadsheet.selectCell("C10", { cfIconSet: "3stars 2" });
    await spreadsheet.selectCell("D10", { cfIconSet: "3stars 3" });
    await spreadsheet.selectCell("B11", { cfIconSet: "3triangles 1" });
    await spreadsheet.selectCell("C11", { cfIconSet: "3triangles 2" });
    await spreadsheet.selectCell("D11", { cfIconSet: "3triangles 3" });
    await spreadsheet.selectCell("B12", { cfIconSet: "4arrows 1" });
    await spreadsheet.selectCell("C12", { cfIconSet: "4arrows 2" });
    await spreadsheet.selectCell("D12", { cfIconSet: "4arrows 3" });
    await spreadsheet.selectCell("E12", { cfIconSet: "4arrows 4" });
    await spreadsheet.selectCell("B13", { cfIconSet: "4arrowsgray 1" });
    await spreadsheet.selectCell("C13", { cfIconSet: "4arrowsgray 2" });
    await spreadsheet.selectCell("D13", { cfIconSet: "4arrowsgray 3" });
    await spreadsheet.selectCell("E13", { cfIconSet: "4arrowsgray 4" });
    await spreadsheet.selectCell("B14", { cfIconSet: "4redtoblack 1" });
    await spreadsheet.selectCell("C14", { cfIconSet: "4redtoblack 2" });
    await spreadsheet.selectCell("D14", { cfIconSet: "4redtoblack 3" });
    await spreadsheet.selectCell("E14", { cfIconSet: "4redtoblack 4" });
    await spreadsheet.selectCell("B15", { cfIconSet: "4rating 1" });
    await spreadsheet.selectCell("C15", { cfIconSet: "4rating 2" });
    await spreadsheet.selectCell("D15", { cfIconSet: "4rating 3" });
    await spreadsheet.selectCell("E15", { cfIconSet: "4rating 4" });
    await spreadsheet.selectCell("B16", { cfIconSet: "4trafficlights 1" });
    await spreadsheet.selectCell("C16", { cfIconSet: "4trafficlights 2" });
    await spreadsheet.selectCell("D16", { cfIconSet: "4trafficlights 3" });
    await spreadsheet.selectCell("E16", { cfIconSet: "4trafficlights 4" });
    await spreadsheet.selectCell("B17", { cfIconSet: "5arrows 1" });
    await spreadsheet.selectCell("C17", { cfIconSet: "5arrows 2" });
    await spreadsheet.selectCell("D17", { cfIconSet: "5arrows 3" });
    await spreadsheet.selectCell("E17", { cfIconSet: "5arrows 4" });
    await spreadsheet.selectCell("F17", { cfIconSet: "5arrows 5" });
    await spreadsheet.selectCell("B18", { cfIconSet: "5arrowsgray 1" });
    await spreadsheet.selectCell("C18", { cfIconSet: "5arrowsgray 2" });
    await spreadsheet.selectCell("D18", { cfIconSet: "5arrowsgray 3" });
    await spreadsheet.selectCell("E18", { cfIconSet: "5arrowsgray 4" });
    await spreadsheet.selectCell("F18", { cfIconSet: "5arrowsgray 5" });
    await spreadsheet.selectCell("B19", { cfIconSet: "5rating 1" });
    await spreadsheet.selectCell("C19", { cfIconSet: "5rating 2" });
    await spreadsheet.selectCell("D19", { cfIconSet: "5rating 3" });
    await spreadsheet.selectCell("E19", { cfIconSet: "5rating 4" });
    await spreadsheet.selectCell("F19", { cfIconSet: "5rating 5" });
    await spreadsheet.selectCell("B20", { cfIconSet: "5quarters 1" });
    await spreadsheet.selectCell("C20", { cfIconSet: "5quarters 2" });
    await spreadsheet.selectCell("D20", { cfIconSet: "5quarters 3" });
    await spreadsheet.selectCell("E20", { cfIconSet: "5quarters 4" });
    await spreadsheet.selectCell("F20", { cfIconSet: "5quarters 5" });
    await spreadsheet.selectCell("B21", { cfIconSet: "5boxes 1" });
    await spreadsheet.selectCell("C21", { cfIconSet: "5boxes 2" });
    await spreadsheet.selectCell("D21", { cfIconSet: "5boxes 3" });
    await spreadsheet.selectCell("E21", { cfIconSet: "5boxes 4" });
    await spreadsheet.selectCell("F21", { cfIconSet: "5boxes 5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85775] Conditional formatting - can import and use rule 'dataBar'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/cf_databar.xlsx");

    //initial values
    await spreadsheet.selectCell("C3", { cfDataBar: "0 3 #638ec6", display: "1" });
    await spreadsheet.selectCell("C4", { cfDataBar: "0 3 #638ec6", display: "2" });
    await spreadsheet.selectCell("C5", { cfDataBar: "0 3 #638ec6", display: "3" });

    await spreadsheet.selectCell("C3");
    spreadsheet.enterCellText("10", { leave: "stay" });

    //check the new values
    await spreadsheet.selectCell("C3", { cfDataBar: "0 10 #638ec6", display: "10" });
    await spreadsheet.selectCell("C4", { cfDataBar: "0 10 #638ec6", display: "2" });
    await spreadsheet.selectCell("C5", { cfDataBar: "0 10 #638ec6", display: "3" });

    await I.reopenDocument();

    //check again the new values
    await spreadsheet.selectCell("C3", { cfDataBar: "0 10 #638ec6", display: "10" });
    await spreadsheet.selectCell("C4", { cfDataBar: "0 10 #638ec6", display: "2" });
    await spreadsheet.selectCell("C5", { cfDataBar: "0 10 #638ec6", display: "3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85960] Conditional formatting - can import and use rule 'reverse iconsets'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/reverse.xlsx");

    //initial values
    await spreadsheet.selectCell("B1", { cfIconSet: "5arrows 5" });
    await spreadsheet.selectCell("C1", { cfIconSet: "5arrows 4" });
    await spreadsheet.selectCell("D1", { cfIconSet: "5arrows 3" });
    await spreadsheet.selectCell("E1", { cfIconSet: "5arrows 2" });
    await spreadsheet.selectCell("F1", { cfIconSet: "5arrows 1" });

    await spreadsheet.selectCell("B1");
    spreadsheet.enterCellText("5");

    //check the new values
    await spreadsheet.selectCell("B1", { cfIconSet: "5arrows 1" });
    await spreadsheet.selectCell("C1", { cfIconSet: "5arrows 4" });
    await spreadsheet.selectCell("D1", { cfIconSet: "5arrows 3" });
    await spreadsheet.selectCell("E1", { cfIconSet: "5arrows 2" });
    await spreadsheet.selectCell("F1", { cfIconSet: "5arrows 1" });

    await I.reopenDocument();

    //check again the new values
    await spreadsheet.selectCell("B1", { cfIconSet: "5arrows 1" });
    await spreadsheet.selectCell("C1", { cfIconSet: "5arrows 4" });
    await spreadsheet.selectCell("D1", { cfIconSet: "5arrows 3" });
    await spreadsheet.selectCell("E1", { cfIconSet: "5arrows 2" });
    await spreadsheet.selectCell("F1", { cfIconSet: "5arrows 1" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85965] Conditional formatting - can import and use rule 'custom icon sets'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/custom_icon_set.xlsx");

    //initial values
    await spreadsheet.selectCell("B1", { cfIconSet: "5boxes 1" });
    await spreadsheet.selectCell("C1", { cfIconSet: "5boxes 2" });
    await spreadsheet.selectCell("D1", { cfIconSet: "5boxes 3" });
    await spreadsheet.selectCell("E1", { cfIconSet: "3trafficlights1 1" });
    await spreadsheet.selectCell("F1", { cfIconSet: "5boxes 5" });

    await spreadsheet.selectCell("B1");
    spreadsheet.enterCellText("4", { leave: "stay" });

    //check the new values
    await spreadsheet.selectCell("B1", { cfIconSet: "3trafficlights1 1" });
    await spreadsheet.selectCell("C1", { cfIconSet: "5boxes 2" });
    await spreadsheet.selectCell("D1", { cfIconSet: "5boxes 3" });
    await spreadsheet.selectCell("E1", { cfIconSet: "3trafficlights1 1" });
    await spreadsheet.selectCell("F1", { cfIconSet: "5boxes 5" });

    await I.reopenDocument();

    //check again the new values
    await spreadsheet.selectCell("B1", { cfIconSet: "3trafficlights1 1" });
    await spreadsheet.selectCell("C1", { cfIconSet: "5boxes 2" });
    await spreadsheet.selectCell("D1", { cfIconSet: "5boxes 3" });
    await spreadsheet.selectCell("E1", { cfIconSet: "3trafficlights1 1" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85958] Conditional formatting - can import, use rule 'iconsets', and change the alignment", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/icon_set.xlsx");

    //check the initial values
    await spreadsheet.selectCell("B2", { cfIconSet: "3arrows 1" });
    await spreadsheet.selectCell("C2", { cfIconSet: "3arrows 2" });
    await spreadsheet.selectCell("D2", { cfIconSet: "3arrows 3" });

    //change the value and the alignment
    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("3", { leave: "stay" });

    I.clickOptionButton("cell/alignvert", "top");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });

    await spreadsheet.selectCell("B2", { cfIconSet: "3arrows 3" });
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });
    await spreadsheet.selectCell("C2", { cfIconSet: "3arrows 2" });
    await spreadsheet.selectCell("D2", { cfIconSet: "3arrows 3" });

    await I.reopenDocument();

    //check again the values
    await spreadsheet.selectCell("B2", { cfIconSet: "3arrows 3" });
    I.waitForVisible({ docs: "control", key: "cell/alignvert", state: "top" });
    await spreadsheet.selectCell("C2", { cfIconSet: "3arrows 2" });
    await spreadsheet.selectCell("D2", { cfIconSet: "3arrows 3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85936] Conditional formatting - can import and use rule 'iconsets'", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/icon_set.xlsx");

    //check the initial values
    await spreadsheet.selectCell("B2", { cfIconSet: "3arrows 1" });

    //check the new value
    spreadsheet.enterCellText("3", { leave: "stay" });
    spreadsheet.waitForActiveCell("B2", { cfIconSet: "3arrows 3" });

    await I.reopenDocument();

    //check the value again
    spreadsheet.waitForActiveCell("B2", { cfIconSet: "3arrows 3" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85963] Conditional formatting - can import 'dataBar', and work with a new inserted a row", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/databar_insert_row.xlsx");

    //check the initial values
    await spreadsheet.selectCell("C6", { value: 3, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C7", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C8", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C9", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C10", { cfDataBar: false });
    await spreadsheet.selectCell("C11", { cfDataBar: false });
    await spreadsheet.selectCell("C12", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C13", { value: 7, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C14", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C15", { value: 13, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C16", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C17", { value: 5, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C18", { cfDataBar: false });
    await spreadsheet.selectCell("C19", { cfDataBar: false });
    await spreadsheet.selectCell("C20", { value: 15, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C21", { value: 6, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C22", { value: 10, cfDataBar: "0 12 #ffc000" });

    //insert  a row
    I.clickToolbarTab("colrow");
    spreadsheet.selectRow("9");
    I.waitForAndClick({ docs: "control", key: "row/insert" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("C9");
    spreadsheet.enterCellText("10");

    //check the values
    await spreadsheet.selectCell("C6", { value: 3, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C7", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C8", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C9", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C10", { cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C11", { cfDataBar: false });
    await spreadsheet.selectCell("C12", { cfDataBar: false });
    await spreadsheet.selectCell("C13", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C14", { value: 7, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C15", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C16", { value: 13, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C17", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C18", { value: 5, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C19", { cfDataBar: false });
    await spreadsheet.selectCell("C20", { cfDataBar: false });
    await spreadsheet.selectCell("C21", { value: 15, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C22", { value: 6, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C23", { value: 10, cfDataBar: "0 12 #ffc000" });

    await I.reopenDocument();

    //check the values
    await spreadsheet.selectCell("C6", { value: 3, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C7", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C8", { value: 1, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C9", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C10", { cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C11", { cfDataBar: false });
    await spreadsheet.selectCell("C12", { cfDataBar: false });
    await spreadsheet.selectCell("C13", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C14", { value: 7, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C15", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C16", { value: 13, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C17", { value: 10, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C18", { value: 5, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C19", { cfDataBar: false });
    await spreadsheet.selectCell("C20", { cfDataBar: false });
    await spreadsheet.selectCell("C21", { value: 15, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C22", { value: 6, cfDataBar: "0 12 #ffc000" });
    await spreadsheet.selectCell("C23", { value: 10, cfDataBar: "0 12 #ffc000" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85959] Conditional formatting - can import, use rule 'iconsets', and change the font size", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/icon_set.xlsx");

    //check the initial values
    await spreadsheet.selectCell("B2", { value: 1, cfIconSet: "3arrows 1", renderFontSize: "12pt" });

    I.clickOptionButton("character/fontsize", 24);
    I.waitForChangesSaved();

    //check the new value
    spreadsheet.waitForActiveCell("B2", { value: 1, cfIconSet: "3arrows 1", renderFontSize: "24pt" });

    //check the value again
    await I.reopenDocument();

    spreadsheet.waitForActiveCell("B2", { value: 1, cfIconSet: "3arrows 1", renderFontSize: "24pt" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[DOCS-3743] Conditional formatting - cfIconSet - insert a column", async ({ I, spreadsheet }) => {

    await  I.loginAndOpenDocument("media/files/DOCS-3743.xlsx");

    //check the initial values
    await spreadsheet.selectCell("A3", { value: 1, cfIconSet: "3arrows 2", renderFontSize: "12pt" });
    await spreadsheet.selectCell("A4", { value: 2, cfIconSet: "3arrows 3", renderFontSize: "12pt" });
    await spreadsheet.selectCell("A5", { value: 0, cfIconSet: "3arrows 1", renderFontSize: "12pt" });

    I.clickToolbarTab("colrow");
    spreadsheet.selectColumn("A");
    I.waitForAndClick({ docs: "control", key: "column/insert" });
    I.waitForChangesSaved();

    await spreadsheet.selectCell("A3", { value: null });
    await spreadsheet.selectCell("A4", { value: null });
    await spreadsheet.selectCell("A5", { value: null });
    await spreadsheet.selectCell("B3", { value: 1, cfIconSet: "3arrows 2", renderFontSize: "12pt" });
    await spreadsheet.selectCell("B4", { value: 2, cfIconSet: "3arrows 3", renderFontSize: "12pt" });
    await spreadsheet.selectCell("B5", { value: 0, cfIconSet: "3arrows 1", renderFontSize: "12pt" });

    await I.reopenDocument();

    //check the value again
    await spreadsheet.selectCell("A3", { value: null });
    await spreadsheet.selectCell("A4", { value: null });
    await spreadsheet.selectCell("A5", { value: null });
    await spreadsheet.selectCell("B3", { value: 1, cfIconSet: "3arrows 2", renderFontSize: "12pt" });
    await spreadsheet.selectCell("B4", { value: 2, cfIconSet: "3arrows 3", renderFontSize: "12pt" });
    await spreadsheet.selectCell("B5", { value: 0, cfIconSet: "3arrows 1", renderFontSize: "12pt" });

    I.closeDocument();
}).tag("stable");
