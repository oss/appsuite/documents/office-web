/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > View");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C114380] Show formula bar - set to off", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello");

    //check the formula-pane
    I.waitForVisible({ css: ".view-pane.formula-pane textarea" });

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/formulapane/show", state: true });

    //deactivate the formula-pane
    I.click({ docs: "control", inside: "popup-menu", key: "view/formulapane/show", withText: "Show formula bar" });

    //check the formul-pane
    I.waitForInvisible({ css: ".view-pane.formula-pane textarea" });

    // check the formula-pane - false
    I.clickButton("view/settings/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/formulapane/show", state: false });

    await I.reopenDocument();

    //check the formul-pane
    I.waitNumberOfVisibleElements({ css: ".view-pane.formula-pane textarea" }, 0);

    // check the formula-pane - false
    I.clickButton("view/settings/menu", { caret: true });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/formulapane/show", state: false });

    I.closeDocument();

}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C290659] Show comments pane - set to on/of", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello");

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", state: false });

    //activate the comment pane
    I.click({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", withText: "Show comments" });

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", state: true });

    //deactivate the comment pane
    I.click({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", withText: "Show comments" });

    await I.reopenDocument();

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", state: false });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8223] Show grid lines - set grid lines to off/on", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello", { leave: "stay" });

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/grid/show", state: true });

    //set grid line to off
    I.click({ docs: "control", inside: "popup-menu", key: "view/grid/show", withText: "Show grid lines" });

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the gridline state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/grid/show", state: false });

    await I.reopenDocument();

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/grid/show", state: false });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8239] Hide and show sheet tabs", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    spreadsheet.enterCellText("Hello", { leave: "stay" });

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/statuspane/show", state: true });

    //set sheet tab to off
    I.click({ docs: "control", inside: "popup-menu", key: "view/statuspane/show", withText: "Show sheet tabs" });

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the sheet tab state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/statuspane/show", state: false });

    await I.reopenDocument();

    //open the popup
    I.clickButton("view/settings/menu", { caret: true });

    //check the state
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "view/statuspane/show", state: false });

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8240] Zoom out", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/settings/menu", { caret: true });

    //zoom out to 75%
    I.waitForAndClick({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "75%" });

    //zoom out to 50%
    I.waitForAndClick({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "50%" });

    // zoom icon is disabled
    I.waitForVisible({ docs: "control", key: "view/zoom/dec", inside: "popup-menu", disabled: true });
    I.waitForVisible({ docs: "control", key: "view/zoom/inc", inside: "popup-menu", disabled: false });

    await I.reopenDocument();

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "50%" });

    // zoom icon is disabled
    I.waitForVisible({ docs: "control", key: "view/zoom/dec", inside: "popup-menu", disabled: true });
    I.waitForVisible({ docs: "control", key: "view/zoom/inc", inside: "popup-menu", disabled: false });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8241] Zoom in", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/settings/menu", { caret: true });

    //zoom out to 125%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "125%" });

    //zoom out to 150%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "150%" });

    //zoom out to 175%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "175%" });

    //zoom out to 200%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "200%" });

    //zoom out to 250%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "250%" });

    //zoom out to 300%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "300%" });

    //zoom out to 400%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "400%" });

    //zoom out to 500%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "500%" });

    //zoom out to 600%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "600%" });

    //zoom out to 800%
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "800%" });

    // zoom icon is diabled
    I.waitForVisible({ docs: "control", key: "view/zoom/dec", inside: "popup-menu", disabled: false });
    I.waitForVisible({ docs: "control", key: "view/zoom/inc", inside: "popup-menu", disabled: true });

    await I.reopenDocument();

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForVisible({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "800%" });

    // zoom icon is diabled
    I.waitForVisible({ docs: "control", key: "view/zoom/dec", inside: "popup-menu", disabled: false });
    I.waitForVisible({ docs: "control", key: "view/zoom/inc", inside: "popup-menu", disabled: true });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8218] Freeze - first column", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/frozen/fixed", { inside: "popup-menu", value: "0,1" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8221] Freeze - first row and column", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/frozen/fixed", { inside: "popup-menu", value: "1,1" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8217] Freeze - first row", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/frozen/fixed", { inside: "popup-menu", value: "1,0" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left" });

    I.closeDocument();
}).tag("stable");


//-----------------------------------------------------------------------------

Scenario("[C8216] Split - cell focus in sheet", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("D4");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/dynamic", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: false });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: false });

    //remove the split
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top", frozen: false  });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left", frozen: false });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8213] Split - default cell focus", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A1");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/dynamic", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: false });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: false });

    //remove the split
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top", frozen: false  });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left", frozen: false });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8214] Split - cell focus in first column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A2");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/dynamic", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: false });

    //remove the split
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top", frozen: false  });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8215] Split - cell focus in first column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A5");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/dynamic", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: false });

    //remove the split
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top", frozen: false  });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C191129] Split - default cell focus (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("A1");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/dynamic", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: false });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: false });

    //remove the split
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top", frozen: false  });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left", frozen: false });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C191128] Freeze - first column (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/frozen/fixed", { inside: "popup-menu", value: "0,1" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C191127] Freeze - first row (ODS)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    I.clickButton("view/settings/menu", { caret: true });
    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu", caret: true });

    I.clickButton("view/split/frozen/fixed", { inside: "popup-menu", value: "1,0" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8219] Freeze - any row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A9");
    I.clickButton("view/settings/menu", { caret: true });

    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8222] Freeze - any row and column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("F13");
    I.clickButton("view/settings/menu", { caret: true });

    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "top", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "top" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8220] Freeze - any column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("H1");
    I.clickButton("view/settings/menu", { caret: true });

    I.waitForElement({ docs: "control", key: "view/split/frozen", inside: "popup-menu" });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });

    //remove the freeze
    I.waitForVisible({ spreadsheet: "header-pane", side: "left", frozen: true });
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/split/frozen", { inside: "popup-menu" });
    I.waitForInvisible({ spreadsheet: "header-pane", side: "left" });

    I.closeDocument();
}).tag("stable");
