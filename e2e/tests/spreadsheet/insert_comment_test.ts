/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Insert > Comment");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C290150] Insert an own comment", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert new comment in cell B2
    await spreadsheet.selectCell("B2");
    I.clickToolbarTab("insert");
    I.clickButton("threadedcomment/insert");

    // check availability and contents of the comments pane
    I.waitForVisible({ docs: "view-pane", pane: "comments-pane", find: ".thread.thread-author-1 .author.comment-author-1" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: 'button[data-action="send"]:disabled' });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: 'button[data-action="cancel"]' });

    // type comment text
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 0, index: 0, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".editormain", withText: "word" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: 'button[data-action="send"]' });
    I.click({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    // check that timestamp, bubble overlay, and other contents appear
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .date" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .position", withText: "B2" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B2", find: ".bubble svg" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    // check again after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .date" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".commentinfo .position", withText: "B2" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B2", find: ".bubble svg" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C289992] [C289993] [C290756] Insert comment via context menu and reply", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // insert new comment in cell B2
    await spreadsheet.openCellContextMenu("B2");
    I.waitForAndClick({ css: ".context-menu .group .button", withText: "Insert" }); // TODO: no data-key for "Insert" submenu
    I.clickButton("threadedcomment/insert", { inside: "popup-menu" });

    // type comment text
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "word" });
    I.waitForAndClick({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    // reply to the comment
    I.moveMouseToElement({ docs: "comment", thread: 0 });
    I.waitForAndClick({ docs: "comment", thread: 0, find: "button[data-action='reply']" });
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".editormain" });
    I.type("reply");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "reply" });
    I.waitForAndClick({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    // check again after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "word" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 1, find: ".text", withText: "reply" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290523] [C290528] Delete threaded comment", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", hint: true });

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // delete the comment
    await spreadsheet.selectCell("C2");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/delete/thread");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay", address: "C2" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    // recheck after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay", address: "C2" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173440] Delete threaded comment and Undo", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", hint: true });

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // delete the comment
    await spreadsheet.selectCell("C2");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/delete/thread");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay", address: "C2" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    // restore the comment
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", hint: true });

    // expect that comment has been restored
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 2, find: ".text", withText: "Child 2" });
    I.waitForAndClick({ docs: "comment", thread: 1, find: ".hide-button", withText: "View 1 more reply" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 1, find: ".text", withText: "Child 1" });

    // recheck after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290533] Edit comment via 'More' (Edit link)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // edit a comment via its "Edit" button in "More" menu
    I.moveMouseToElement({ docs: "comment", thread: 0 });
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".action .dropdown" });
    I.waitForAndClick({ css: '.smart-dropdown-container a[data-name="comment-edit"]' });
    I.wait(0.5);
    I.type(" 2");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "Comment without thread 2" });
    I.click({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "Comment without thread 2" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290795] Delete comment via 'More' (Delete thread link)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // delete the comment via its "Delete" button in "More" menu
    I.moveMouseToElement({ docs: "comment", thread: 1, index: 0 });
    I.waitForAndClick({ docs: "comment", thread: 1, index: 0, find: ".action .dropdown" });
    I.waitForVisible({ css: '.smart-dropdown-container a[data-name="comment-edit"]' });
    I.waitForAndClick({ css: '.smart-dropdown-container a[data-name="comment-delete"]' });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay", address: "C2" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290532] Delete reply via 'More'", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    await spreadsheet.selectCell("C2");

    // delete the first reply via its "Delete" button in "More" menu
    I.moveMouseToElement({ docs: "comment", thread: 1, index: 1 });
    I.waitForAndClick({ docs: "comment", thread: 1, index: 1, find: ".action .dropdown" });
    I.waitForAndClick({ css: '.smart-dropdown-container a[data-name="comment-delete"]' });
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "comment", thread: 1, index: 1, find: ".text", withText: "Child 2" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1, index: 2 });

    // recheck after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 1, index: 1, find: ".text", withText: "Child 2" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1, index: 2 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290026] Undo delete row with comments", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // delete row 2
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "2" });
    I.clickButton("row/delete", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-count" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 0 });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    // restore the row and comments
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", hint: true });

    // expect that comments have been restored
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 2, find: ".text", withText: "Child 2" });
    I.waitForAndClick({ docs: "comment", thread: 1, find: ".hide-button", withText: "View 1 more reply" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 1, find: ".text", withText: "Child 1" });

    // recheck after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290522] Delete all comments", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // delete all comments
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/delete/all");
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "All comments on this sheet will be removed. Do you want to continue?" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-count" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 0 });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    // recheck after reload
    await I.reopenDocument();
    I.dontSeeElementInDOM({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-count" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290029] Delete all comments and Undo", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // show comments pane
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    // delete all comments
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/delete/all");
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "All comments on this sheet will be removed. Do you want to continue?" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-count" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay" });
    I.dontSeeElementInDOM({ docs: "comment", thread: 0 });
    I.dontSeeElementInDOM({ docs: "comment", thread: 1 });

    // restore the comments
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "C2", hint: true });

    // expect that comments have been restored
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 2, find: ".text", withText: "Child 2" });
    I.waitForAndClick({ docs: "comment", thread: 1, find: ".hide-button", withText: "View 1 more reply" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 1, find: ".text", withText: "Child 1" });

    // recheck after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "Comment without thread" });
    I.waitForVisible({ docs: "comment", thread: 1, index: 0, find: ".text", withText: "Second comment with thread" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290526] Insert an own comment in protected sheet", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // protect the active sheet
    I.clickButton("view/sheet/more", { pane: "status-pane", caret: true });
    I.clickButton("sheet/locked", { inside: "popup-menu", state: false });

    // insert new comment in cell B2
    await spreadsheet.selectCell("B2");
    I.clickToolbarTab("insert");
    I.clickButton("threadedcomment/insert");
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "word" });
    I.waitForAndClick({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B2", find: ".bubble svg" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    // check again after reload
    await I.reopenDocument();
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("view/commentspane/show", { inside: "popup-menu", state: false });
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "word" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B2", find: ".bubble svg" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223547] [C290521] Navigate comments", async ({ I, spreadsheet, alert }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // create a comment in cell B4
    await spreadsheet.selectCell("B4");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/insert");
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 2, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 2, find: ".editormain", withText: "word" });
    I.click({ docs: "comment", thread: 2, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    // use navigation buttons in "Review" toolbar to jump forwards
    I.clickButton("comment/goto/next");
    spreadsheet.waitForActiveCell("A2");
    alert.waitForVisible("The search continues from the beginning of this document");
    alert.clickCloseButton();
    I.clickButton("comment/goto/next");
    spreadsheet.waitForActiveCell("C2");
    alert.isNotVisible();
    I.clickButton("comment/goto/next");
    spreadsheet.waitForActiveCell("B4");
    alert.isNotVisible();

    // from empty cell without comment
    await spreadsheet.selectCell("A3");
    I.clickButton("comment/goto/next");
    spreadsheet.waitForActiveCell("B4");

    // use navigation buttons in "Review" toolbar to jump backwards
    I.clickButton("comment/goto/prev");
    spreadsheet.waitForActiveCell("C2");
    alert.isNotVisible();
    I.clickButton("comment/goto/prev");
    spreadsheet.waitForActiveCell("A2");
    alert.isNotVisible();
    I.clickButton("comment/goto/prev");
    spreadsheet.waitForActiveCell("B4");
    alert.waitForVisible("The search continues from the beginning of this document");
    alert.clickCloseButton();

    // from empty cell without comment
    await spreadsheet.selectCell("A3");
    I.clickButton("comment/goto/prev");
    spreadsheet.waitForActiveCell("C2");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290527] [C293785] [C293786] Open/close comment pane", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // C290527: open/close comment pane via comment count in sheet tab
    I.dontSeeElement({ css: ".view-pane.comment-pane" });
    I.click({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.waitForVisible({ docs: "comment", thread: 0 });
    I.click({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='2']" });
    I.dontSeeElement({ css: ".view-pane.comment-pane" });

    // C293785: open/close comment pane via cell bubble icon
    await spreadsheet.selectCell("C2");
    I.waitForAndClick({ spreadsheet: "comment-overlay", address: "C2", find: ".bubble .icon-bubble-wrapper" });
    I.waitForVisible({ docs: "comment", thread: 0 });
    I.waitForVisible({ docs: "comment", thread: 1, selected: true });
    I.click({ spreadsheet: "comment-overlay", address: "C2", find: ".bubble svg" });
    I.dontSeeElement({ css: ".view-pane.comment-pane" });

    // C293786: open/close comment pane via toolbar
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show", state: false });
    I.waitForVisible({ docs: "comment", thread: 0 });
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show", state: true });
    I.dontSeeElement({ css: ".view-pane.comment-pane" });

    // open/close comment pane via "View" menu
    I.clickButton("view/settings/menu", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", state: false });
    I.waitForVisible({ docs: "comment", thread: 0 });
    I.clickButton("view/settings/menu", { caret: true });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "view/commentspane/show", state: true });
    I.dontSeeElement({ css: ".view-pane.comment-pane" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290030] [C290519] Sort cells with comments and notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // check initial position of comments and notes
    spreadsheet.activateSheet(1);
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".position", withText: "A2" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".text", withText: "A2" });
    I.waitForVisible({ docs: "comment", thread: 2, find: ".position", withText: "A3" });
    I.waitForVisible({ docs: "comment", thread: 2, find: ".text", withText: "A3" });
    I.waitForVisible({ docs: "comment", thread: 3, find: ".position", withText: "A5" });
    I.waitForVisible({ docs: "comment", thread: 3, find: ".text", withText: "A5" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A5", hint: true });

    // sort cell range in descending order
    await spreadsheet.selectRange("A1:A5");
    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");

    // check resorted comments
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "A5" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".position", withText: "A3" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".text", withText: "A3" });
    I.waitForVisible({ docs: "comment", thread: 2, find: ".position", withText: "A4" });
    I.waitForVisible({ docs: "comment", thread: 2, find: ".text", withText: "A2" });
    I.waitForVisible({ docs: "comment", thread: 3, find: ".position", withText: "A5" });
    I.waitForVisible({ docs: "comment", thread: 3, find: ".text", withText: "A1" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A5", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290031] [C290520] Merged cells with comments and notes", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.xlsx");

    // check initial position of comments and notes
    spreadsheet.activateSheet(2);
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".position", withText: "A2" });
    I.waitForVisible({ docs: "comment", thread: 1, find: ".text", withText: "A2" });
    I.waitForVisible({ docs: "comment", thread: 2, find: ".position", withText: "B2" });
    I.waitForVisible({ docs: "comment", thread: 2, find: ".text", withText: "B2" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B1", hint: true });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B2", hint: true });

    // merge cell range
    await spreadsheet.selectRange("A1:B2");
    I.clickToolbarTab("format");
    I.clickButton("cell/merge");

    // check comments
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 2, find: ".comment-thread-count[data-thread-count='1']" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "A1" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A1", hint: true });
    I.dontSeeElement({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "comment-overlay", address: "B2", hint: true });

    // reload document
    await I.reopenDocument();
    I.clickToolbarTab("comments");
    I.waitForAndClick({ docs: "control", key: "view/commentspane/show" });
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 2, find: ".comment-thread-count[data-thread-count='1']" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".position", withText: "A1" });
    I.waitForVisible({ docs: "comment", thread: 0, find: ".text", withText: "A1" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A1", hint: true });
    I.dontSeeElement({ spreadsheet: "comment-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "comment-overlay", address: "B2", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290524] Unposted comment alert (insert sheet)", async ({ I, spreadsheet, alert }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // create an unsaved comment in cell B2
    await spreadsheet.clickCell("B2");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/insert");
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "word" });

    // create a new sheet
    I.clickButton("document/insertsheet", { pane: "status-pane" });
    I.waitForChangesSaved();
    spreadsheet.waitForActiveSheet(1);

    // try to insert another comment, expect the dialog
    I.clickButton("threadedcomment/insert");
    spreadsheet.waitForActiveSheet(0);
    alert.waitForVisible("Your document contains unposted comments");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".unsaved-comment-message", withText: "Please post your comment" });
    alert.clickCloseButton();

    I.waitForAndClick({ docs: "comment", thread: 0, find: 'button[data-action="cancel"]' });
    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290525] Unposted comment alert - close the dialog with deleting the unposted comment", async ({ I, drive, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");
    spreadsheet.enterCellText("1"); // prevent auto-deletion of empty document
    I.waitForChangesSaved();

    // create an unsaved comment in cell B2
    await spreadsheet.selectCell("B2");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/insert");
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "word" });

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    // try to close the document
    I.clickButton("app/quit");
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-header", withText: "There is an unposted comment on this sheet" });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="ok"]', withText: "Continue and discard comment" });
    dialogs.clickOkButton();
    drive.waitForApp();

    // reopen document, check that the comment has been deleted
    I.openDocument(fileDesc);
    I.dontSeeElementInDOM({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-count" });
    I.dontSeeElementInDOM({ spreadsheet: "comment-overlay" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290657] Unposted comment alert - close the dialog without deleting the unposted comment", async ({ I, spreadsheet, dialogs }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // create an unsaved comment in cell B2
    await spreadsheet.selectCell("B2");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/insert");
    I.wait(0.5);
    I.waitForAndClick({ docs: "comment", thread: 0, find: ".editormain" });
    I.type("word");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".editormain", withText: "word" });

    // try to close the document
    I.clickButton("app/quit");
    dialogs.waitForVisible();
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment on this sheet" });
    I.waitForVisible({ docs: "dialog", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Continue and discard comment" });
    dialogs.clickCancelButton();

    // a message must appear in the comment frame
    I.waitForVisible({ docs: "comment", thread: 0, find: ".unsaved-comment-message", withText: "Please post your comment" });
    I.waitForAndClick({ docs: "comment", thread: 0, find: 'button[data-action="send"]' });

    // reopen document, check that the comment has been deleted
    await I.reopenDocument();
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "B2", find: ".bubble svg" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C290660] Comment layer links 'Contact data' layer", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // create a comment in cell B2
    await spreadsheet.selectCell("B2");
    I.clickToolbarTab("comments");
    I.clickButton("threadedcomment/insert");

    // click the author name and expect the contact overlay
    I.waitForAndClick({ docs: "comment", thread: 0, find: "a.author.person" });
    I.waitForVisible({ css: ".detail-popup-halo .io-ox-halo .contact-header" });
    I.waitForAndClick({ css: ".detail-popup-halo .popup-close button" });

    I.waitForAndClick({ docs: "comment", thread: 0, find: 'button[data-action="cancel"]' });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223541] Merged cells with comments (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.ods");

    // check initial position of comments
    spreadsheet.activateSheet(2);
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "B2", hint: true });

    // merge cell range
    await spreadsheet.selectRange("A1:B2");
    I.seeToolbarTab("format");
    I.clickButton("cell/merge");

    // check comments
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "B2", hint: true });

    // reload document
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "B2", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223543] Delete selected comment (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.ods");
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "C2", hint: true });

    // delete the comment
    await spreadsheet.selectCell("C2");
    I.clickToolbarTab("comments");
    I.clickButton("note/delete");
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "C2", hint: true });

    // recheck after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.dontSeeElement({ spreadsheet: "note-overlay", address: "C2", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C289991] Delete all comments (ODS)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/comments.ods");

    // delete all comments
    I.clickToolbarTab("comments");
    I.clickButton("note/delete/all");
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "All comments on this sheet will be removed. Do you want to continue?" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ spreadsheet: "note-overlay" });

    // recheck after reload
    await I.reopenDocument();
    I.dontSeeElementInDOM({ spreadsheet: "note-overlay" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C234523] Show and hide selected comment (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.ods");

    // show comment in cell A2
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });
    await spreadsheet.selectCell("A2");
    I.clickToolbarTab("comments");
    I.clickButton("note/visible", { state: false });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 0, withText: "A2" });

    // recheck after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "note", pos: 0, para: 0, withText: "A2" });
    I.clickToolbarTab("comments");
    I.clickButton("note/visible", { state: true });
    I.waitForInvisible({ spreadsheet: "note", pos: 0, para: 0, withText: "A2" });

    // recheck after reload
    await I.reopenDocument();
    I.dontSeeElement({ spreadsheet: "note", pos: 0 });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C275445] Sort cells with comments (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/comments.ods");

    // check initial position of comments
    spreadsheet.activateSheet(1);
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A2", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A5", hint: true });

    // sort cell range in descending order
    await spreadsheet.selectRange("A1:A5");
    I.clickToolbarTab("data");
    I.clickOptionButton("table/sort", "descending");

    // check resorted comments
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A5", hint: true });

    // recheck after reload
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A1", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A3", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A4", hint: true });
    I.waitForVisible({ spreadsheet: "note-overlay", address: "A5", hint: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223548] [C149418] Navigate comments (ODS)", async ({ I, spreadsheet, alert }) => {

    await I.loginAndOpenDocument("media/files/comments.ods");

    // use navigation buttons in "Review" toolbar to jump forwards
    I.clickToolbarTab("comments");
    I.clickButton("note/goto/next");
    await spreadsheet.selectCell("A2");
    alert.isNotVisible();

    I.clickButton("note/goto/next");
    spreadsheet.waitForActiveCell("C2");
    alert.isNotVisible();

    I.clickButton("note/goto/next");
    spreadsheet.waitForActiveCell("B4");
    alert.isNotVisible();

    I.clickButton("note/goto/next");
    alert.waitForVisible("No more comments found");
    alert.clickCloseButton();
    spreadsheet.waitForActiveCell("B4");

    // use navigation buttons in "Review" toolbar to jump backwards
    await spreadsheet.selectCell("A5");
    I.clickButton("note/goto/prev");
    spreadsheet.waitForActiveCell("B4");
    alert.isNotVisible();

    I.clickButton("note/goto/prev");
    spreadsheet.waitForActiveCell("C2");
    alert.isNotVisible();

    I.clickButton("note/goto/prev");
    spreadsheet.waitForActiveCell("A2");
    alert.isNotVisible();

    I.clickButton("note/goto/prev");
    alert.waitForVisible("No more comments found");
    alert.clickCloseButton();
    spreadsheet.waitForActiveCell("A2");

    I.closeDocument();
}).tag("stable");
