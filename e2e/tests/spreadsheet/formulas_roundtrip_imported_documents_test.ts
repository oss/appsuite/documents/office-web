/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Formulas > Roundtrip with imported documents");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C311742] Can import number format with system code", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/date_time_system_codes.xlsx", { locale: "de_DE" });

    await spreadsheet.selectCell("B6", { display: "Samstag, 0. Januar 1900", value: "Samstag, 0. Januar 1900" });
    await spreadsheet.selectCell("B7", { display: "Samstag, 0. Januar 1900", value: "Samstag, 0. Januar 1900" });
    await spreadsheet.selectCell("B8", { display: "Samstag, 0. Januar 1900", value: "Samstag, 0. Januar 1900" });
    await spreadsheet.selectCell("B9", { display: "Samstag, 0. Januar 1900", value: "Samstag, 0. Januar 1900" });
    await spreadsheet.selectCell("B10", { display: "00:00:00", value: "00:00:00" });
    await spreadsheet.selectCell("B11", { display: "00:00:00", value: "00:00:00" });
    await spreadsheet.selectCell("B12", { display: "00:00:00", value: "00:00:00" });
    await spreadsheet.selectCell("B13", { display: "00:00:00", value: "00:00:00" });
    await spreadsheet.selectCell("D6", { display: "Mittwoch, 1. Februar 2034", value: "Mittwoch, 1. Februar 2034" });
    await spreadsheet.selectCell("D7", { display: "Mittwoch, 1. Februar 2034", value: "Mittwoch, 1. Februar 2034" });
    await spreadsheet.selectCell("D8", { display: "Mittwoch, 1. Februar 2034", value: "Mittwoch, 1. Februar 2034" });
    await spreadsheet.selectCell("D9", { display: "Mittwoch, 1. Februar 2034", value: "Mittwoch, 1. Februar 2034" });
    await spreadsheet.selectCell("D10", { display: "05:06:07", value: "05:06:07" });
    await spreadsheet.selectCell("D11", { display: "05:06:07", value: "05:06:07" });
    await spreadsheet.selectCell("D12", { display: "05:06:07", value: "05:06:07" });
    await spreadsheet.selectCell("D13", { display: "05:06:07", value: "05:06:07" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C311744] Can import number format with colors, single section color", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/number_formats_colors.xlsx");

    await spreadsheet.selectCell("B7", { display: "123", value: 123, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B8", { display: "123", value: 123, renderColor: "#000000" });
    await spreadsheet.selectCell("B9", { display: "123", value: 123, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B10", { display: "123", value: 123, renderColor: "#00ffff" });
    await spreadsheet.selectCell("B11", { display: "123", value: 123, renderColor: "#00ff00" });
    await spreadsheet.selectCell("B12", { display: "123", value: 123, renderColor: "#ff00ff" });
    await spreadsheet.selectCell("B13", { display: "123", value: 123, renderColor: "#ff0000" });
    await spreadsheet.selectCell("B14", { display: "123", value: 123, renderColor: "#ffffff" });
    await spreadsheet.selectCell("B15", { display: "123", value: 123, renderColor: "#ffff00" });
    await spreadsheet.selectCell("C7", { display: "-123", value: -123, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C8", { display: "-123", value: -123, renderColor: "#000000" });
    await spreadsheet.selectCell("C9", { display: "-123", value: -123, renderColor: "#0000ff" });
    await spreadsheet.selectCell("C10", { display: "-123", value: -123, renderColor: "#00ffff" });
    await spreadsheet.selectCell("C11", { display: "-123", value: -123, renderColor: "#00ff00" });
    await spreadsheet.selectCell("C12", { display: "-123", value: -123, renderColor: "#ff00ff" });
    await spreadsheet.selectCell("C13", { display: "-123", value: -123, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C14", { display: "-123", value: -123, renderColor: "#ffffff" });
    await spreadsheet.selectCell("C15", { display: "-123", value: -123, renderColor: "#ffff00" });
    await spreadsheet.selectCell("D7", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D8", { display: "0", value: -0, renderColor: "#000000" });
    await spreadsheet.selectCell("D9", { display: "0", value: 0, renderColor: "#0000ff" });
    await spreadsheet.selectCell("D10", { display: "0", value: 0, renderColor: "#00ffff" });
    await spreadsheet.selectCell("D11", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D12", { display: "0", value: 0, renderColor: "#ff00ff" });
    await spreadsheet.selectCell("D13", { display: "0", value: 0, renderColor: "#ff0000" });
    await spreadsheet.selectCell("D14", { display: "0", value: 0, renderColor: "#ffffff" });
    await spreadsheet.selectCell("D15", { display: "0", value: 0, renderColor: "#ffff00" });
    await spreadsheet.selectCell("E7", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E8", { display: "abc", value: "abc", renderColor: "#000000" });
    await spreadsheet.selectCell("E9", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("E10", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E11", { display: "abc", value: "abc", renderColor: "#00ff00" });
    await spreadsheet.selectCell("E12", { display: "abc", value: "abc", renderColor: "#ff00ff" });
    await spreadsheet.selectCell("E13", { display: "abc", value: "abc", renderColor: "#ff0000" });
    await spreadsheet.selectCell("E14", { display: "abc", value: "abc", renderColor: "#ffffff" });
    await spreadsheet.selectCell("E15", { display: "abc", value: "abc", renderColor: "#ffff00" });
    await spreadsheet.selectCell("F7", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F8", { display: "TRUE", value: true, renderColor: "#000000" });
    await spreadsheet.selectCell("F9", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("F10", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F11", { display: "TRUE", value: true, renderColor: "#00ff00" });
    await spreadsheet.selectCell("F12", { display: "TRUE", value: true, renderColor: "#ff00ff" });
    await spreadsheet.selectCell("F13", { display: "TRUE", value: true, renderColor: "#ff0000" });
    await spreadsheet.selectCell("F14", { display: "TRUE", value: true, renderColor: "#ffffff" });
    await spreadsheet.selectCell("F15", { display: "TRUE", value: true, renderColor: "#ffff00" });
    await spreadsheet.selectCell("G7", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G8", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G9", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G10", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G11", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G12", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G13", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G14", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G15", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C330189] Can import number format with colors - Colors per Section (single, two, three)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/number_formats_colors.xlsx");

    await spreadsheet.selectCell("B19", { display: "123", value: 123, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B20", { display: "123", value: 123, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B21", { display: "123", value: 123, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C19", { display: "-123", value: -123, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C20", { display: "-123", value: -123, renderColor: "#0000ff" });
    await spreadsheet.selectCell("C21", { display: "-123", value: -123, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D19", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D20", { display: "0", value: 0, renderColor: "#0000ff" });
    await spreadsheet.selectCell("D21", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E19", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E20", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("E21", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("F19", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F20", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("F21", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("G19", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G20", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G21", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B23", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B24", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B25", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B26", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("C23", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C24", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C25", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C26", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("D23", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D24", { display: "0", value: 0, renderColor: "#0000ff" });
    await spreadsheet.selectCell("D25", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D26", { display: "0", value: 0, renderColor: "#0000ff" });
    await spreadsheet.selectCell("E23", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E24", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("E25", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E26", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("F23", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F24", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("F25", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F26", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("G23", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G24", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G25", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G26", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B28", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B29", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B30", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B31", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B32", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B33", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B34", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B35", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("C28", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C29", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C30", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C31", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C32", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C33", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C34", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C35", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("D28", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D29", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D30", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D31", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D32", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D33", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D34", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D35", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("E28", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E29", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("E30", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E31", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("E32", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E33", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("E34", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E35", { display: "abc", value: "abc", renderColor: "#0000ff" });
    await spreadsheet.selectCell("F28", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F29", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("F30", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F31", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("F32", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F33", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("F34", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F35", { display: "TRUE", value: true, renderColor: "#0000ff" });
    await spreadsheet.selectCell("G28", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G29", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G30", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G31", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G32", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G33", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G34", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G35", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C330193] Can import number format with colors - Colors per Section (four)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/number_formats_colors.xlsx");

    // scroll test cells into view
    I.pressKeys("Ctrl+End");

    await spreadsheet.selectCell("B37", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B38", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B39", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B40", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B41", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B42", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B43", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B44", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B45", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B46", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B47", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B48", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B49", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B50", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("B51", { display: "123.456", value: 123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("B52", { display: "123.456", value: 123.456, renderColor: "#0000ff" });
    await spreadsheet.selectCell("C37", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C38", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C39", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C40", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C41", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C42", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C43", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C44", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C45", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C46", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C47", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C48", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C49", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C50", { display: "-123.456", value: -123.456, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("C51", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("C52", { display: "-123.456", value: -123.456, renderColor: "#ff0000" });
    await spreadsheet.selectCell("D37", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D38", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D39", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D40", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D41", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D42", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D43", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D44", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D45", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D46", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D47", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D48", { display: "0", value: 0, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("D49", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D50", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D51", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("D52", { display: "0", value: 0, renderColor: "#00ff00" });
    await spreadsheet.selectCell("E37", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E38", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E39", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E40", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E41", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E42", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E43", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E44", { display: "abc", value: "abc", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("E45", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E46", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E47", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E48", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E49", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E50", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E51", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("E52", { display: "abc", value: "abc", renderColor: "#00ffff" });
    await spreadsheet.selectCell("F37", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F38", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F39", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F40", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F41", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F42", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F43", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F44", { display: "TRUE", value: true, renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("F45", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F46", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F47", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F48", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F49", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F50", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F51", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("F52", { display: "TRUE", value: true, renderColor: "#00ffff" });
    await spreadsheet.selectCell("G37", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G38", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G39", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G40", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G41", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G42", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G43", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G44", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G45", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G46", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G47", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G48", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G49", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G50", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G51", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });
    await spreadsheet.selectCell("G52", { display: "#N/A", error: "#NA", renderColor: "#a5a5a5" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
