/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Spreadsheet > Search and Replace");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C8261] Search a word", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/search.xlsx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "Lorem");
    spreadsheet.waitForActiveCell("B1");

    I.clickButton("document/search/start");
    spreadsheet.waitForActiveCell("F11");

    I.clickButton("document/search/start");
    spreadsheet.waitForActiveCell("K17");

    //check again - B1 is the first element
    I.clickButton("document/search/start");
    spreadsheet.waitForActiveCell("B1");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8262] Search a word without result", async ({ I, alert }) => {

    await I.loginAndOpenDocument("media/files/search.xlsx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "donald");

    alert.waitForVisible("No results for your search.");
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8264] Search a word and replace", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/search.xlsx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "Lorem");

    I.clickButton("document/search/start");
    I.clickButton("document/search/start");
    I.clickButton("document/search/start");
    I.pressKey("ArrowLeft");
    I.clickButton("document/search/start");
    spreadsheet.waitForActiveCell("B1", { display: "Lorem" });

    I.fillTextField("document/replace/text", "hello");
    I.clickButton("document/replace/next");

    //check the value
    await spreadsheet.selectCell("B1", { display: "hello" });
    await spreadsheet.selectCell("F11", { display: "LOREM" });
    await spreadsheet.selectCell("K17", { display: "Lorem" });

    await I.reopenDocument();

    //check the value again
    await spreadsheet.selectCell("B1", { display: "hello" });
    await spreadsheet.selectCell("F11", { display: "LOREM" });
    await spreadsheet.selectCell("K17", { display: "Lorem" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8265] Search words and replace all", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/search.xlsx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "Lorem");

    I.clickButton("document/search/start");
    I.clickButton("document/search/start");
    I.clickButton("document/search/start");
    I.pressKey("ArrowLeft");
    I.clickButton("document/search/start");
    spreadsheet.waitForActiveCell("B1", { display: "Lorem" });

    I.fillTextField("document/replace/text", "hello");
    I.clickButton("document/replace/all");

    //check the value
    await spreadsheet.selectCell("B1", { display: "hello" });
    await spreadsheet.selectCell("F11", { display: "hello" });
    await spreadsheet.selectCell("K17", { display: "hello" });

    await I.reopenDocument();
    await spreadsheet.selectCell("B1", { display: "hello" });
    await spreadsheet.selectCell("F11", { display: "hello" });
    await spreadsheet.selectCell("K17", { display: "hello" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8266] Search a formula", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/search_formula.xlsx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "=sum(");
    I.clickButton("document/search/start");

    //check cell H10
    spreadsheet.waitForActiveCell("H10", { value: 23, display: "23", formula: "SUM(3+20)" });

    I.clickButton("document/search/start");

    //check cell B3
    spreadsheet.waitForActiveCell("B3", { value: 2, display: "2", formula: "SUM(1+1)" });
    I.clickButton("document/search/start");

    //check cell H10 again
    spreadsheet.waitForActiveCell("H10", { value: 23, display: "23", formula: "SUM(3+20)" });

    await I.reopenDocument();

    //check cells
    await spreadsheet.selectCell("B3", { value: 2, display: "2", formula: "SUM(1+1)" });
    await spreadsheet.selectCell("H10", { value: 23, display: "23", formula: "SUM(3+20)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C8263] Select next search result", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/search.xlsx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "Lorem");
    I.waitForElement({ spreadsheet: "selection-range", range: "B1" });
    spreadsheet.waitForActiveCell("B1", { display: "Lorem" });

    I.clickButton("document/search/next");
    I.waitForElement({ spreadsheet: "selection-range", range: "F11" });
    spreadsheet.waitForActiveCell("F11", { display: "LOREM" });

    I.clickButton("document/search/next");
    I.waitForElement({ spreadsheet: "selection-range", range: "K17" });
    spreadsheet.waitForActiveCell("K17", { display: "Lorem" });

    I.closeDocument();
}).tag("stable");
