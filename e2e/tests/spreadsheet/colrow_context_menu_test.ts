/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Spreadsheet > Context Menus > Columns & Rows");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C8177] [C8178] Hide and show rows", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // hide row 2
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "2" });
    I.waitForContextMenuVisible();
    I.clickButton("row/hide", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: true });
    I.waitForInvisible({ spreadsheet: "row-header", pos: "2" });

    // navigate over hidden row
    I.pressKeys("Ctrl+Home", "ArrowDown");
    spreadsheet.waitForActiveCell("A3");

    // reopen the document, show hidden row
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: true });
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "3" });
    I.clickButton("row/show", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });
    I.waitForVisible({ spreadsheet: "row-header", pos: "2" });

    // navigate into shown row
    I.pressKeys("Ctrl+Home", "ArrowDown");
    spreadsheet.waitForActiveCell("A2");

    // reopen the document, recheck row visibility
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });
    I.waitForVisible({ spreadsheet: "row-header", pos: "2" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C8179] [C8180] Hide and show columns", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // hide column B
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: true });
    I.waitForInvisible({ spreadsheet: "col-header", pos: "B" });

    // navigate over hidden column
    I.pressKeys("Ctrl+Home", "ArrowRight");
    spreadsheet.waitForActiveCell("C1");

    // reopen the document, show hidden column
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: true });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.clickButton("column/show", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });
    I.waitForVisible({ spreadsheet: "col-header", pos: "B" });

    // navigate into shown column
    I.pressKeys("Ctrl+Home", "ArrowRight");
    spreadsheet.waitForActiveCell("B1");

    // reopen the document, recheck column visibility
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });
    I.waitForVisible({ spreadsheet: "col-header", pos: "B" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C234520] [C275433] Hide and show rows (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // hide row 2
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "2" });
    I.waitForContextMenuVisible();
    I.clickButton("row/hide", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: true });
    I.waitForVisible({ spreadsheet: "row-hidden-marker", pos: "2" });
    I.waitForInvisible({ spreadsheet: "row-header", pos: "2" });

    // navigate over hidden row
    I.pressKeys("Ctrl+Home", "ArrowDown");
    spreadsheet.waitForActiveCell("A3");

    // reopen the document, show hidden row
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: true });
    I.waitForVisible({ spreadsheet: "row-hidden-marker", pos: "2" });
    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "3" });
    I.clickButton("row/show", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });
    I.waitForVisible({ spreadsheet: "row-header", pos: "2" });

    // navigate into shown row
    I.pressKeys("Ctrl+Home", "ArrowDown");
    spreadsheet.waitForActiveCell("A2");

    // reopen the document, recheck row visibility
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "row-resize", pos: "2", collapsed: false });
    I.waitForVisible({ spreadsheet: "row-header", pos: "2" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C234521] [C275434] Hide and show columns (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    // hide column B
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();
    I.clickButton("column/hide", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: true });
    I.waitForVisible({ spreadsheet: "col-hidden-marker", pos: "B" });
    I.waitForInvisible({ spreadsheet: "col-header", pos: "B" });

    // navigate over hidden column
    I.pressKeys("Ctrl+Home", "ArrowRight");
    spreadsheet.waitForActiveCell("C1");

    // reopen the document, show hidden column
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: true });
    I.waitForVisible({ spreadsheet: "col-hidden-marker", pos: "B" });
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "C" });
    I.clickButton("column/show", { inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });
    I.waitForVisible({ spreadsheet: "col-header", pos: "B" });

    // navigate into shown column
    I.pressKeys("Ctrl+Home", "ArrowRight");
    spreadsheet.waitForActiveCell("B1");

    // reopen the document, recheck column visibility
    await I.reopenDocument();
    I.waitForVisible({ spreadsheet: "col-resize", pos: "B", collapsed: false });
    I.waitForVisible({ spreadsheet: "col-header", pos: "B" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8173] Insert row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A2");
    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "2" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/insert", withText: "Insert row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/delete", withText: "Delete row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/height/optimal", withText: "Set optimal row height" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/hide", withText: "Hide row" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "row/insert", withText: "Insert row" });
    I.waitForChangesSaved();

    //check the cells
    await spreadsheet.selectCell("A2", { value: null });
    await spreadsheet.selectCell("A3", { value: "Lorem" });

    await I.reopenDocument();

    //check the cells
    await spreadsheet.selectCell("A2", { value: null });
    await spreadsheet.selectCell("A3", { value: "Lorem" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8174] Insert row", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("A2");
    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "2" });
    I.waitForVisible({ spreadsheet: "selection-range", range: "A2:XFD2" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/insert", withText: "Insert row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/delete", withText: "Delete row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/height/optimal", withText: "Set optimal row height" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/hide", withText: "Hide row" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "row/delete", withText: "Delete row" });
    I.waitForChangesSaved();

    //check the cells
    await spreadsheet.selectCell("A2", { value: null });

    await I.reopenDocument();

    //check the cells
    await spreadsheet.selectCell("A2", { value: null });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8175] Insert column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("Lorem", { leave: "stay" });

    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/insert", withText: "Insert column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/delete", withText: "Delete column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/width/optimal", withText: "Set optimal column width" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/hide", withText: "Hide column" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "column/insert", withText: "Insert column" });
    I.waitForChangesSaved();

    //check the cells
    await spreadsheet.selectCell("B2", { value: null });
    await spreadsheet.selectCell("C2", { value: "Lorem" });

    await I.reopenDocument();

    //check the cells
    await spreadsheet.selectCell("B2", { value: null });
    await spreadsheet.selectCell("C2", { value: "Lorem" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8176] Delete column", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("C2");
    spreadsheet.enterCellText("Lorem", { leave: "left" });

    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/insert", withText: "Insert column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/delete", withText: "Delete column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/width/optimal", withText: "Set optimal column width" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/hide", withText: "Hide column" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "column/delete", withText: "Delete column" });
    I.waitForChangesSaved();

    //check the cells
    await spreadsheet.selectCell("B2", { value: "Lorem" });
    await spreadsheet.selectCell("C2", { value: null });

    await I.reopenDocument();

    await spreadsheet.selectCell("B2", { value: "Lorem" });
    await spreadsheet.selectCell("C2", { value: null });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8181] Set optimal column width", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("test dummy, and more text, text text", { leave: "stay" });

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/insert", withText: "Insert column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/delete", withText: "Delete column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/width/optimal", withText: "Set optimal column width" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/hide", withText: "Hide column" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "column/width/optimal", withText: "Set optimal column width" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    I.waitForToolbarTab("colrow");

    //check the change state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "6324" });

    await I.reopenDocument();

    //check the state
    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "6324" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C8182] Set optimal row height", async ({ I, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    await spreadsheet.selectCell("B2");

    I.clickToolbarTab("colrow");
    I.waitForAndClick({ docs: "control", key: "row/height/active" });
    I.fillTextField("row/height/active", "30");

    //check the state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "14446" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    spreadsheet.enterCellText("test dummy, and more text, text text", { leave: "stay" });

    I.waitForAndRightClick({ spreadsheet: "row-header", pos: "2" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/insert", withText: "Insert row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/delete", withText: "Delete row" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/height/optimal", withText: "Set optimal row height" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "row/hide", withText: "Hide row" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "row/height/optimal", withText: "Set optimal row height" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    //state is change
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    await I.reopenDocument();

    //check the state
    I.clickToolbarTab("colrow");
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "503" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2381" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C173491] Set optimal column width (ODS)", async ({ I, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/empty.ods");

    await spreadsheet.selectCell("B2");
    spreadsheet.enterCellText("test dummy, and more text, text text", { leave: "stay" });

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "476" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2258" });

    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "B" });
    I.waitForContextMenuVisible();

    //Context menu
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/insert", withText: "Insert column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/delete", withText: "Delete column" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/width/optimal", withText: "Set optimal column width" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "column/hide", withText: "Hide column" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "column/width/optimal", withText: "Set optimal column width" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    //check the change state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "476" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2258" });

    await I.reopenDocument();

    await spreadsheet.selectCell("B2");

    I.clickToolbarTab("colrow");

    //check the state
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "476" });
    I.waitForElement({  css: ".group.text-field.app-tooltip.spin-field.length-field > .input-wrapper > input", state: "2258" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-4837] Set optimal column width for blank column", async ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // original column size after import
    const rect1 = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" });

    // apply "set optimal column width" to blank column
    I.waitForAndRightClick({ spreadsheet: "col-header", pos: "A" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "column/width/optimal" });
    I.waitForContextMenuInvisible();

    // check the column size
    I.wait(0.5); // should not generate operations (no "waitForChangesSaved")
    const rect2 = await I.grabElementBoundingRect({ spreadsheet: "col-header", pos: "A" });
    expect(rect1.width).to.equal(rect2.width);

    I.closeDocument();
}).tag("stable");
