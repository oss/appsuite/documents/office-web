/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Undo - Redo");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C104281] Undo text input", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.waitForToolbarTab("drawing");

    I.typeText("hello");

    //check the text hello and the templatetext
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the templatetext
    I.pressKeys("2*Escape");
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span" })).to.not.equal("hello");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the templatetext
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span" })).to.not.equal("hello");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C115422] ODP: Undo text input", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] .p", withChild: "span:not(.templatetext)" });
    I.waitForToolbarTab("drawing");

    I.typeText("hello");

    //check the text hello and the templatetext
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the templatetext
    I.pressKeys("2*Escape");
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" })).to.not.equal("hello");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the templatetext
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" })).to.not.equal("hello");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104282] Redo text input", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //slide 1
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.waitForToolbarTab("drawing");

    I.typeText("hello");

    //check the text hello and the templatetext
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the templatetext
    I.pressKeys("2*Escape");
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.not.equal("hello");

    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //check the text hello
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the text hello
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115423] ODP: Redo font attribute", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });

    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] .p", withChild: "span:not(.templatetext)" });
    I.waitForToolbarTab("drawing");

    I.typeText("hello");

    //activate the format tab
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.pressKeys("Ctrl+A");

    I.clickOptionButton("character/fontname", "Cambria");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Cambria" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" }, "font-family")).to.include("Cambria");

    //check the text hello and the templatetext
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the templatetext
    I.pressKeys("2*Escape");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" })).to.equal("hello");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" }, "font-family")).to.not.include("Cambria");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" })).to.equal("hello");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" }, "font-family")).to.not.include("Cambria");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104283] Undo font attribute", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });

    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.waitForToolbarTab("drawing");

    I.typeText("hello");

    //activate the format tab
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.pressKeys("Ctrl+A");

    I.clickOptionButton("character/fontname", "Cambria");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Cambria" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.include("Cambria");

    //check the text hello and the templatetext
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the templatetext
    I.pressKeys("2*Escape");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.not.include("Cambria");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.not.include("Cambria");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104284] Redo font attribute", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.waitForToolbarTab("drawing");

    I.typeText("hello");
    I.waitForChangesSaved();

    //activate the format tab
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.pressKeys("Ctrl+A");

    I.clickOptionButton("character/fontname", "Cambria");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/fontname", state: "Cambria" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.include("Cambria");

    //check the text hello and the templatetext
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the templatetext
    I.pressKeys("2*Escape");
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.not.include("Cambria");

    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //check the text hello, fontfamily
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.include("Cambria");
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span.templatetext" });
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("hello");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.include("Cambria");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104302] Undo insert slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile.pptx");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //slide 2
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //slide 2 is removed
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_2" });

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //slide 2 is removed
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_2" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115427] ODP: Undo insert slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.odp", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //slide 1
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle'] span" }, 1);

    //slide 2
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 1);

    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    //duplicate slide 2
    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    //slide 3
    I.waitForVisible({ presentation: "slide", id: "slide_3" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 1);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // slide 3 is removed
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_3" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.seeElementInDOM({ presentation: "slide", id: "slide_1" });
    I.seeElementInDOM({ presentation: "slide", id: "slide_2" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //slide 1
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title'] span" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle'] span" }, 1);

    //slide 2
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 1);

    //slide 3 is removed
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_3" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104303] Redo insert slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile.pptx");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    //expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span")).to.equal("Testfile");
    //I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //slide 2
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", id: "slide_2" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");


    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //check the values again
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1" });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //slide 2 is removed
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_2" });

    //redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    //check the redo values
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("ArrowUp");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    //check the redo values again
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.pressKey("ArrowDown");

    //slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    expect(await I.grabTextFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] span" })).to.equal("Testfile");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104288] Undo insert text frame", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.waitForVisible({ docs: "control", key: "textframe/insert", state: "false" });

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickButton("textframe/insert");
    I.dragMouseOnElement({ presentation: "page" }, 350, 250);
    I.waitForChangesSaved();
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing > div.content.autoresizeheight.textframecontent" }, 1);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.dontSeeElement({ presentation: "slide", find: "> div.drawing > div.content.autoresizeheight.textframecontent" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.dontSeeElement({ presentation: "slide", find: "> div.drawing > div.content.autoresizeheight.textframecontent" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104286] Redo insert Image", async ({ I, drive, dialogs, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ presentation: "slide", find: "> div.drawing.absolute.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-type='image']" });

    //redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115425] ODP: Redo insert Image", async ({ I, drive, dialogs, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ presentation: "slide", find: "> div.drawing.absolute.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing[data-type='image']" });

    //redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C104285] Undo insert table", async ({ I, presentation, selection }) => {

    const COLOR_FIRST_ROW = "rgb(68, 114, 196)";
    const COLOR_SECOND_ROW = "rgb(207, 213, 234)";
    const COLOR_THIRD_ROW = "rgb(232, 235, 245)";

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.waitForToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKeys("2*ArrowDown", "3*ArrowRight", "Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ presentation: "slide", find: "> .drawing table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > .drawing table > tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span");

    // the correct table style is set
    I.waitForVisible({ itemKey: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //table is removed
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table tr" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table td" });

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //table is removed
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table tr" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table td" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115426] ODP: Redo insert shape", async ({ I, presentation, selection }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-placeholdertype]" }, 2); // both drawings are placeholders

    I.clickToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickOptionButton("shape/insert", "rect");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-placeholdertype]" }, 2); // only 2 drawings are place holders

    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > div.drawing.selected:nth-of-type(3) .p > span");

    I.clickButton("document/undo");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-placeholdertype]" }, 2); // both drawings are placeholders

    I.dontSeeElement({ presentation: "slide", find: "> .drawing.selected" }); // no drawing is selected

    I.clickButton("document/redo");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-placeholdertype]:not(.selected)" }, 2); // only 2 drawings are placeholders, both not selected

    I.seeElement({ presentation: "slide", find: "> .drawing.selected" }); // but the third drawing is selected after redo

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-placeholdertype]" }, 2); // only 2 drawings are placeholders

    I.dontSeeElement({ presentation: "slide", find: "> .drawing.selected" }); // no drawing is selected after reload

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115424]ODP:  Undo insert table", async ({ I, presentation, selection }) => {

    const COLOR_FIRST_ROW = "rgb(114, 159, 207)";
    const COLOR_SECOND_ROW = "rgb(213, 223, 237)";
    const COLOR_THIRD_ROW = "rgb(235, 240, 246)";

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.waitForToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKeys("2*ArrowDown", "3*ArrowRight", "Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ presentation: "slide", find: "> .drawing table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > .drawing table > tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span");

    // the correct table style is set
    I.waitForVisible({ itemKey: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });

    //undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    //table is removed
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table tr" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table td" });

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //table is removed
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table tr" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing table td" });

    I.closeDocument();
}).tag("stable");
