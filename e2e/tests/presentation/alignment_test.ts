/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Format > Text alignment");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85801][C85798][C85797][C85799] Text alignment (horizontal)", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // slide 1
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.typeText("lorem");
    I.waitForChangesSaved();

    // alignment button
    I.clickButton("view/drawing/alignment/menu", { caret: true });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "justify" });

    // alignment middle
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    I.waitForAndClick({ docs: "button", value: "left",  inside: "popup-menu" });
    I.waitForChangesSaved();

    // alignment left
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("left");

    // slide 2
    I.clickToolbarTab("slide");
    I.wait(1); //TODO?: check button state
    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitForAndClick({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // left
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("left");

    I.waitForAndClick({ docs: "button", value: "center", inside: "popup-menu" });
    I.waitForChangesSaved();

    // middle
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    // slide 3
    I.clickToolbarTab("slide");
    I.wait(1);
    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitForAndClick({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // middle
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    I.waitForAndClick({ docs: "button", value: "right", inside: "popup-menu" });
    I.waitForChangesSaved();

    // right
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("right");

    // slide 4
    I.clickToolbarTab("slide");
    I.wait(1);
    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");
    I.waitForAndClick({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // right
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("right");

    I.waitForAndClick({ docs: "button", value: "justify", inside: "popup-menu" });
    I.waitForChangesSaved();

    // justify
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("justify");

    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    // slide 1 - alignment left
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("left");

    // slide 2 - alignment center
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    // slide 3 - alignment right
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("right");

    // slide 4 - alignment justify
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("justify");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85802][C85804][C85803] Text alignment (vertical)", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // slide 1
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.typeText("hello");
    I.waitForChangesSaved();

    // alignment top
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("top");

    // alignment button
    I.clickButton("view/drawing/alignment/menu", { caret: true });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", value: "centered",  inside: "popup-menu" });
    I.waitForChangesSaved();
    I.pressKey("Escape");

    // alignment centered
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("centered");

    // slide 2
    I.clickOnElement({ presentation: "slidepane", id: "slide_1", selected: true }, { button: "right" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ itemKey: "slide/duplicateslides", inside: "context-menu" });
    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitForAndClick({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.waitForToolbarTab("drawing");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // centered
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("centered");

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForChangesSaved();

    // top
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("top");

    // slide 3
    I.clickOnElement({ presentation: "slidepane", id: "slide_2", selected: true }, { button: "right" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ itemKey: "slide/duplicateslides", inside: "context-menu" });
    I.waitForChangesSaved();
    I.wait(1);

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.waitForAndClick({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    // top
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("top");

    I.clickButton("view/drawing/alignment/menu", { caret: true });
    I.waitForAndClick({ docs: "button", value: "bottom", inside: "popup-menu" });
    I.waitForChangesSaved();

    // bottom
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("bottom");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // slide 1 - alignment centered
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("centered");

    // slide 2 - alignment top
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("top");

    // slide 3 - alignment bottom
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("bottom");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115075] ODP: Text alignment centered", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_alignment_left.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.pressKey("Tab");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // alignment left
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("left");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "justify" });
    I.waitForAndClick({ docs: "button", value: "center", inside: "popup-menu" });
    I.waitForChangesSaved();

    // alignment center
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // alignment center
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115076] ODP: Text alignment justify", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_alignment_left.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.pressKey("Tab");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // alignment left
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("left");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "justify" });
    I.waitForAndClick({ docs: "button", value: "justify", inside: "popup-menu" });
    I.waitForChangesSaved();

    // alignment center
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("justify");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // alignment center
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("justify");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115077] ODP: Text alignment middle", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_alignment_top.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.pressKey("Tab");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // alignment top
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("top");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "justify" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForAndClick({ docs: "button", value: "centered", inside: "popup-menu" });
    I.waitForChangesSaved();

    // alignment centered
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("centered");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // alignment centered
    expect(await I.grabAttributeFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" }, "verticalalign")).to.equal("centered");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85914] Font alignments in master slides", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    presentation.enterMasterView();

    I.click({ presentation: "slidepane", find: ".slide-container.master" });
    // alternative: I.click({ presentation: "slidepane", childposition: 1 });

    I.waitNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing" }, 5);

    I.waitForAndClick({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title']" });

    I.waitForToolbarTab("drawing");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    expect(await I.grabCssPropertyFrom({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title'] .p" }, "text-align")).to.equal("center");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "right" });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "top" });

    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForVisible({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", value: "right", inside: "popup-menu" });
    I.waitForChangesSaved();

    expect(await I.grabCssPropertyFrom({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title'] .p" }, "text-align")).to.equal("right");

    // check right alignment in document slide, too
    presentation.leaveMasterView();
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] .p" }, "text-align")).to.equal("right");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] .p" }, "text-align")).to.equal("right");

    presentation.enterMasterView();
    I.click({ presentation: "slidepane", find: ".slide-container.master" });
    I.waitNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing" }, 5);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title'] .p" }, "text-align")).to.equal("right");

    I.closeDocument();
}).tag("stable");
