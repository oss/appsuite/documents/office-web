/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > File > Print as PDF");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});


Scenario("[C85777] Print Presentation as PDF", async ({ I }) => {

    // create a new Presentation, type some text
    I.loginAndHaveNewDocument("presentation");
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);
    I.pressKey("Tab");

    I.typeText("Vivamus elementum semper nisi.");
    I.waitForChangesSaved();

    // Activate File toolbar
    I.clickToolbarTab("file");

    // Click on 'Print as PDF'
    I.clickButton("document/print");
    I.waitForNumberOfTabs(2, 60);
    I.wait(1);

    // close 'Print' page and then the document
    I.closeOtherTabs();
    I.wait(1);
    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C8105] Print Spreadsheet as PDF", ({ I, spreadsheet }) => {

    // create a new Spreadsheet, type some text
    I.loginAndHaveNewDocument("spreadsheet");
    I.waitForNumberOfTabs(1, 10);

    spreadsheet.enterCellText("Vivamus elementum semper nisi.");
    I.waitForChangesSaved();

    // Activate File toolbar
    I.clickToolbarTab("file");

    // Click on 'Print as PDF'
    I.clickButton("document/print");
    I.waitForNumberOfTabs(2, 60);

    // close 'Print' page and then the document
    I.closeOtherTabs();
    I.wait(1);
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C7902] Print Text as PDF", async ({ I }) => {

    // create a new text document, type some text
    I.loginAndHaveNewDocument("text");
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);
    I.typeText("Vivamus elementum semper nisi.");
    I.waitForChangesSaved();

    // Activate File toolbar
    I.clickToolbarTab("file");

    // Click on 'Print as PDF'
    I.clickButton("document/print");
    I.wait(1);
    const tabCountDocument = await I.grabNumberOfOpenTabs();
    expect(tabCountDocument).to.equal(2);

    // close 'Print' page and then the document
    I.closeOtherTabs();
    I.wait(1);
    I.closeDocument();
}).tag("stable");
