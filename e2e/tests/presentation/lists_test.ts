/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Format > Lists");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-3831] List indendation in drawing with paragraph attributes for indentation (ODP)", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3831.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);
    I.waitForAndClick({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle']" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected[data-placeholdertype='subTitle']" });

    I.waitForToolbarTab("drawing");
    I.typeText("Hello World!");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("center");

    I.waitForAndClick({ docs: "button", value: "left",  inside: "popup-menu" });

    I.waitForChangesSaved();

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p" }, "text-align")).to.equal("left");

    I.clickButton("paragraph/list/bullet");

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected[data-placeholdertype='subTitle'] .p > .list-label" });

    I.pressKey("Enter");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p > .list-label" }, 2);

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:first-child" }, "text-align")).to.equal("left");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "text-align")).to.equal("left");

    const level0MarginLeft = parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "margin-left"));

    I.clickButton("paragraph/list/incindent");
    I.waitForChangesSaved();

    const level1MarginLeft = parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "margin-left"));

    expect(level1MarginLeft).to.be.above(level0MarginLeft);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);
    I.waitForAndClick({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle']" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected[data-placeholdertype='subTitle']" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p > .list-label" }, 2);

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:first-child" }, "text-align")).to.equal("left");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "text-align")).to.equal("left");

    const level1MarginLeftReload = parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "margin-left"));

    // expect(level1MarginLeftReload).to.equal(level1MarginLeft); // TODO -> "margin-left" and bullet type are different after reload
    expect(level1MarginLeftReload).to.be.above(level0MarginLeft); // at least there is still an indentation

    I.closeDocument();

}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4439] Keeping bullet type after changing list indendation and reload (ODP)", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-4439.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);
    I.waitForAndClick({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe .p:last-child > span" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    I.seeTextEquals("–", { presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe .p:last-child > .list-label > span" });

    // margin-left 15 mm (56,7 px)
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "margin-left"), 10)).to.be.closeTo(56, 2);

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("paragraph/list/incindent");
    I.waitForChangesSaved();

    // margin-left 28 mm (105 px)
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "margin-left"), 10)).to.be.closeTo(105, 2);

    I.seeTextEquals("–", { presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe .p:last-child > .list-label > span" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    // margin-left still 28 mm (105 px)
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child" }, "margin-left"), 10)).to.be.closeTo(105, 2);
    // still the hyphen in the list
    I.seeTextEquals("–", { presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe .p:last-child > .list-label > span" });

    I.closeDocument();
}).tag("stable");
