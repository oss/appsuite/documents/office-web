/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Collaboration");

Before(async ({ I, users }) => {
    await users.create();
    await users.create();
    I.wait(30); // waiting for user provisioning
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C86103] Cursor position of collaborators", async ({ I, presentation, users, selection }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userLinkCSS = "a.person";

    function getRGBPartOfString(rgbaString: string): string {
        // returns the following pattern "(R,G,B" of any RGB/RGBA string
        return "(" + rgbaString.split(/[,()]/).splice(1, 3).join(",");
    }

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile_collaboration.pptx", { addContactPicture: true, disableSpellchecker: true, shareFile: users[1] });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // set selection to be seen by other user
    selection.setTextRangePosition({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] .p" }, 0, 8);

    await session("Alice", async () => {

        // this user has no user image
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1], disableSpellchecker: true });

        // INFO: It is not clear, whether "Alice" uses the class "user-1" or "user-2"

        // check content of document
        presentation.firstSlideAndAllSlidesInSlidePaneVisible();

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        // check existence of halo link for each user
        I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

        // check names in collaborators dialog
        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

        expect(firstUserName1).to.endWith(userName2); // users[0] is the first in the list
        expect(secondUserName1).to.endWith(userName1);

        // get collaborator user color
        const secondUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) > div > div.user-color" }, "background-color");

        // check selection of collaborator
        I.waitForVisible({ presentation: "page", find: "> .collaborative-overlay > .collaborative-selection-group > .selection-overlay" });
        const secondUserSelectionColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-selection-group > div.selection-overlay", "background-color");
        expect(secondUserSquareColor).to.have.string(getRGBPartOfString(secondUserSelectionColor));

        // check collaborator cursor
        const secondUserCursorColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor", "border-color");
        expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserCursorColor));

        // check color of collaborator flag
        I.waitForElement({ presentation: "page", find: "> .collaborative-overlay > .collaborative-cursor > .collaborative-username" });
        const secondUserFlagColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor > div.collaborative-username", "background-color");
        expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserFlagColor));

        // check user name in collaborator flag
        I.seeTextEquals(userName1, { presentation: "page", find: "> .collaborative-overlay > .collaborative-cursor > .collaborative-username" });

        // check thar the flag will be hide after a short time
        I.waitForInvisible({ presentation: "page", find: ".collaborative-overlay > .collaborative-cursor > .collaborative-username" });

        // set selection to be seen by other user
        selection.setTextRangePosition({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] .p" }, 9, 22);

    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    // check existence of halo link for each user
    I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

    // check names in collaborators dialog
    const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
    const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

    expect(firstUserName1).to.endWith(userName1); // users[0] is the first in the list
    expect(secondUserName1).to.endWith(userName2);

    // get collaborator user color
    const secondUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) > div > div.user-color" }, "background-color");

    // check selection of collaborator
    I.waitForElement({ presentation: "page", find: "> .collaborative-overlay > .collaborative-selection-group > .selection-overlay.user-2" });
    const secondUserSelectionColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-selection-group > div.selection-overlay.user-2", "background-color");
    expect(secondUserSquareColor).to.have.string(getRGBPartOfString(secondUserSelectionColor));

    // check collaborator cursor
    const secondUserCursorColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor.user-2", "border-color");
    expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserCursorColor));

    // check color of collaborator flag
    I.waitForElement({ presentation: "page", find: "> .collaborative-overlay > .collaborative-cursor.user-2 > .collaborative-username.user-2" });
    const secondUserFlagColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor.user-2 > div.collaborative-username.user-2", "background-color");
    expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserFlagColor));

    // check user name in collaborator flag
    I.seeTextEquals(userName2, { presentation: "page", find: "> .collaborative-overlay > .collaborative-cursor.user-2 > .collaborative-username.user-2" });

    // check thar the flag will be hide after a short time
    I.waitForInvisible({ presentation: "page", find: "> .collaborative-overlay > .collaborative-cursor.user-2 > .collaborative-username.user-2" });

    // closing the document activate the Portal application
    I.closeDocument();

    await session("Alice", () => {
        I.closeDocument();
    });

}).tag("stable").tag("mergerequest");

Scenario("[C303236] Users can edit presentations concurrently", async ({ I, users, presentation }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile.pptx", { shareFile: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);
    I.seeTextEquals("Testfile", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });

    I.dontSeeElement({ docs: "popup" });

    await session("Alice", async () => {

        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] }); // only login, no upload

        I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing" }, 5);
        I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);
        I.seeTextEquals("Testfile", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        I.wait(1); // increasing resilience
        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });
        expect(firstUserName1).to.endWith(userName2); // users[1] is the first in the list
        expect(secondUserName1).to.endWith(userName1);
    });

    I.seeElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    I.wait(1); // increasing resilience
    const firstUserName2 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
    const secondUserName2 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });
    expect(firstUserName2).to.endWith(userName1);  // users[2] is the first in the list
    expect(secondUserName2).to.endWith(userName2);

    I.pressKey("Tab");
    I.typeText("1");
    I.seeTextEquals("Testfile1", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });

    await session("Alice", () => {
        I.pressKey("Tab");
        I.seeTextEquals("Testfile1", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });
        I.typeText("2");
        I.seeTextEquals("Testfile12", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });
    });

    I.seeTextEquals("Testfile12", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });
    I.typeText("3");
    I.seeTextEquals("Testfile123", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });
    I.closeDocument();

    await session("Alice", () => {
        I.seeTextEquals("Testfile123", { presentation: "slide", id: "slide_1", find: ".drawing:nth-of-type(1)" });
        I.closeDocument();
    });
}).tag("stable");
