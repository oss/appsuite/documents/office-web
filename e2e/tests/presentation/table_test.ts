/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Table");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C96985] Insert row", async ({ I, selection, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(0, 0, 0)";
    const COLOR_SECOND_ROW = "rgb(204, 204, 204)";
    const COLOR_THIRD_ROW = "rgb(231, 231, 231)";

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5); //TODO überall anpassen
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 25);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });

    I.click({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(5) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(5) > td:nth-child(2) .p" }); // TODO anpassen

    I.waitForToolbarTab("table");

    I.clickButton("table/insert/row");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: " .drawing table td" }, 30);

    // there is no text in the first cell of the new inserted row
    I.seeTextEquals("", { presentation: "slide", find: "> .drawing table tr:nth-child(6) > td:nth-child(1) .p span" }); //TODO
    // dark grey color for the new inserted row
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(6) td" }, { "background-color": COLOR_SECOND_ROW });

    selection.seeCollapsedInsideElement({ presentation: "slide", find: "table > tbody > tr:nth-child(6) > td:nth-child(1) .p" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 30);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(6) td" }, { "background-color": COLOR_SECOND_ROW });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C96986] Insert column", async ({ I, selection, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(0, 0, 0)";
    const COLOR_SECOND_ROW = "rgb(204, 204, 204)";
    const COLOR_THIRD_ROW = "rgb(231, 231, 231)";

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slide", find: "> .drawing table tr" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 25);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });

    I.wait(1); // TODO
    // I.waitForVisible(".app-content > .page > .pagecontent > .slide[data-container-id='slide_2'] table > tbody > tr:nth-child(3) > td:nth-child(2)");

    I.click({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_2'] table > tbody > tr:nth-child(3) > td:nth-child(2) .p");

    // there is text in the first cell of the third column
    I.seeTextEquals("3", { presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) > td:nth-child(3) .p span" });

    I.waitForToolbarTab("table");

    I.clickButton("table/insert/column");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 30);

    // there is no text in the first cell of the third column, but in the first cell of the fourth column
    I.seeTextEquals("", { presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) > td:nth-child(3) .p span" });
    I.seeTextEquals("3", { presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) > td:nth-child(4) .p span" });
    // the new inserted cells have the correct color formatting
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td:nth-child(3)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td:nth-child(3)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td:nth-child(3)" }, { "background-color": COLOR_THIRD_ROW });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");


    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 30);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96987] Delete row", async ({ I, selection, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(0, 0, 0)";
    const COLOR_SECOND_ROW = "rgb(204, 204, 204)";
    const COLOR_THIRD_ROW = "rgb(231, 231, 231)";

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slide", find: "> .drawing table tr" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 25);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    // there is text in the first cell of the third row
    I.seeTextEquals("3", { presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) > td:nth-child(1) .p span" });

    I.wait(1); // TODO

    I.waitForAndClick({ presentation: "slide", find: "table > tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_2'] table > tbody > tr:nth-child(3) > td:nth-child(2) .p");

    I.waitForToolbarTab("table");

    I.clickButton("table/delete/row");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: " .drawing table tr" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 20);

    // there is another text in the first cell of the third row
    I.seeTextEquals("4", { presentation: "slide", find: "> .drawing table tr:nth-child(3) > td:nth-child(1) .p span" });
    // the color scheme is adapted to the rows
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 20);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96988] Delete column", async ({ I, selection, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(0, 0, 0)";
    const COLOR_SECOND_ROW = "rgb(204, 204, 204)";
    const COLOR_THIRD_ROW = "rgb(231, 231, 231)";

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slide", find: "> .drawing table tr" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: " .drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 25);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    // there is text in the first cell of the second column
    I.seeTextEquals("2", { presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) > td:nth-child(2) .p span" });

    I.wait(1); // TODO

    I.waitForAndClick({ presentation: "slide", find: "table > tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_2'] table > tbody > tr:nth-child(3) > td:nth-child(2) .p");

    I.waitForToolbarTab("table");

    I.clickButton("table/delete/column");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 20);

    // there is another text in the first cell of the second column
    I.seeTextEquals("3", { presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) > td:nth-child(2) .p span" });
    // the color scheme is not modified
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    // the new cursor position is in the first cell of the second column
    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_2'] table > tbody > tr:nth-child(1) > td:nth-child(2) .p");

    await I.reopenDocument();

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing table td" }, 20);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_2", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115128] Insert row (ODP)", async ({ I, selection, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(179, 179, 179)";
    const COLOR_SECOND_ROW = "rgb(204, 204, 204)";
    const COLOR_THIRD_ROW = "rgb(230, 230, 230)";

    await I.loginAndOpenDocument("media/files/testfile_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing table tr" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 25);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });

    I.waitForAndClick(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(5) > td:nth-child(2)");

    I.wait(1); // resilience (problematic focus caused by table formatting or controller update ?)

    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(5) > td:nth-child(2) .p");

    I.waitForToolbarTab("table");

    I.waitForVisible({ itemKey: "table/stylesheet", state: "default" });

    I.clickButton("table/insert/row");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr" }, 6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 30);

    // there is no text in the first cell of the new inserted row
    I.seeTextEquals("", { presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(6) > td:nth-child(1) .p span" });
    // dark grey color for the new inserted row
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(6) td" }, { "background-color": COLOR_SECOND_ROW });

    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(6) > td:nth-child(1) .p");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr" }, 6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 30);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(6) td" }, { "background-color": COLOR_SECOND_ROW });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115129] Delete column (ODP)", async ({ I, presentation, selection }) => {

    const COLOR_FIRST_ROW = "rgb(179, 179, 179)";
    const COLOR_SECOND_ROW = "rgb(204, 204, 204)";
    const COLOR_THIRD_ROW = "rgb(230, 230, 230)";

    await I.loginAndOpenDocument("media/files/testfile_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: " .drawing table tr" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 25);

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    // there is text in the first cell of the second column
    I.seeTextEquals("2", { presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(1) > td:nth-child(2) .p span" });

    I.waitForAndClick({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(3) > td:nth-child(2) .p");

    I.waitForToolbarTab("table");

    I.clickButton("table/delete/column");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 20);

    // there is another text in the first cell of the second column
    I.seeTextEquals("3", { presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(1) > td:nth-child(2) .p span" });
    // the color scheme is not modified
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    // the new cursor position is in the first cell of the second column
    selection.seeCollapsedInsideElement(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(1) > td:nth-child(2) .p");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 20);
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });

    //I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_THIRD_ROW });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96991] Cell fill color", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_no_style.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // fillcolor
    I.clickOptionButton("table/fillcolor", "red");
    I.waitForChangesSaved();
    I.waitForVisible({ docs: "control", key: "table/fillcolor", state: "red" });

    await I.reopenDocument();

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the fillcolor
    I.waitForVisible({ docs: "control", key: "table/fillcolor", state: "red" });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96989] Table borders", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_no_style.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    I.clickOptionButton("table/cellborder", "inner");

    // close the dropdown
    I.click({ css: ".popup-footer > button", withText: "Close" });
    I.waitForChangesSaved();

    // check borderBottom
    const borderBottomWidth1 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom-width");
    expect(borderBottomWidth1).to.equal("1px");

    // check borderLeft
    const borderLeftWidth1 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left-width");
    expect(borderLeftWidth1).to.equal("1px");

    // check borderRight
    const borderRightWidth1 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right-width");
    expect(borderRightWidth1).to.equal("1px");

    await I.reopenDocument();

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check borderBottom
    const borderBottomWidth2 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom-width");
    expect(borderBottomWidth2).to.equal("1px");

    // check borderLeft
    const borderLeftWidth2 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left-width");
    expect(borderLeftWidth2).to.equal("1px");

    // check borderRight
    const borderRightWidth2 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right-width");
    expect(borderRightWidth2).to.equal("1px");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96990] Border width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // initial state 1
    I.waitForVisible({ docs: "control", key: "table/borderwidth", state: "1" });

    // change the border width
    I.clickOptionButton("table/borderwidth", 6);

    // change the state to 6
    I.waitForVisible({ docs: "control", key: "table/borderwidth", state: "6" });

    await I.reopenDocument();

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // change the state to 6
    I.waitForVisible({ docs: "control", key: "table/borderwidth", state: "6" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96984] Table style", async ({ I, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(239, 243, 233)";
    const COLOR_SECOND_ROW = "rgb(222, 231, 209)";
    const COLOR_THIRD_ROW = "rgb(239, 243, 233)";
    const COLOR_FOURTH_ROW = "rgb(222, 231, 209)";
    const COLOR_FIFTH_ROW = "rgb(239, 243, 233)";

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // initail table styles
    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "{073A0DAA-6AF3-43AB-8588-CEC1D06C72B9}" });

    // the correct table style is set
    I.clickOptionButton("table/stylesheet", "{0505E3EF-67EA-436B-97B2-0124C06EBD24}");

    // new table style
    I.waitForVisible({ docs: "control", key: "table/stylesheet", state: "{0505E3EF-67EA-436B-97B2-0124C06EBD24}" });

    // check the number of tr, td
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 25);

    // check the row background color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_FIFTH_ROW });

    await I.reopenDocument();

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the number of tr, td
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 25);

    // check the row background color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(1) td" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(2) td" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(3) td" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(4) td" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table tr:nth-child(5) td" }, { "background-color": COLOR_FIFTH_ROW });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96992] Change table width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2"); //TODO anpassen an  die anderen tests

    const oldTableWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: "table" }, "width");
    const oldTableWidth = parseInt(oldTableWidthString, 10);

    I.click({ presentation: "slide", find: ".drawing tr:nth-child(1) td:nth-child(1)" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" }); //drawing-selection TODO überall machen
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" }, -100, 0);

    I.waitForChangesSaved();

    const newTableWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "width");
    const newTableWidth = parseInt(newTableWidthString, 10);
    expect(newTableWidth).to.be.below(oldTableWidth);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const newTableWidthString1 = await I.grabCssPropertyFrom({ presentation: "slide", find: "table" }, "width");
    const newTableWidth1 = parseInt(newTableWidthString1, 10);
    expect(newTableWidth1).to.be.below(oldTableWidth);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96993] Change table height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const oldTableHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "height");
    const oldTableHeight = parseInt(oldTableHeightString, 10);

    I.click({ presentation: "slide", find: ".drawing tr:nth-child(1) td:nth-child(1)" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" }); //drawing-selection TODO überall machen
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" }, 0, -100);

    I.waitForChangesSaved();

    const newTableHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "height");
    const newTableHeight = parseInt(newTableHeightString, 10);
    expect(newTableHeight).to.be.below(oldTableHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const newTableHeight1String = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "height");
    const newTableHeight1 = parseInt(newTableHeight1String, 10);
    expect(newTableHeight1).to.be.below(oldTableHeight);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96994] Change column width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_grid.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the inial table width
    const oldTableColumn2WidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "width");
    const oldTableColumn2Width = parseInt(oldTableColumn2WidthString, 10);
    expect(oldTableColumn2Width).to.be.equal(oldTableColumn2Width);

    // check the inial table width
    const oldTableColumn1WidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(1)" }, "width");
    const oldTableColumn1Width = parseInt(oldTableColumn1WidthString, 10);
    expect(oldTableColumn1Width).to.be.equal(oldTableColumn1Width);

    I.click({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)> div > .resize.right" });

    I.dragMouseOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)> div > .resize.right" }, -50, 0);

    // check the column 2 width
    const newTableColumn2WidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "width");
    const newTableColumn2Width = parseInt(newTableColumn2WidthString, 10);
    expect(newTableColumn2Width).to.be.below(oldTableColumn2Width);

    // check the column 1 width
    const newTableColumn1WidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(1)" }, "width");
    const newTableColumn1Width = parseInt(newTableColumn1WidthString, 10);
    expect(newTableColumn1Width).to.be.equal(oldTableColumn1Width);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the column 2 width
    const reloadTableColumn2WidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "width");
    const reloadTableColumn2Width = parseInt(reloadTableColumn2WidthString, 10);
    expect(reloadTableColumn2Width).to.be.below(oldTableColumn2Width);

    // check the column 1 width
    const reloadTableColumn1WidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(1)" }, "width");
    const reloadTableColumn1Width = parseInt(reloadTableColumn1WidthString, 10);
    expect(reloadTableColumn1Width).to.be.equal(oldTableColumn1Width);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C96996] Change table width and height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const oldTableHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "height");
    const oldTableHeight = parseInt(oldTableHeightString, 10);

    I.click({ presentation: "slide", id: "slide_2", find: ".drawing tr:nth-child(1) td:nth-child(1)" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" }, -100, -100);

    const newTableHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "height");
    const newTableHeight = parseInt(newTableHeightString, 10);
    expect(newTableHeight).to.greaterThan(oldTableHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const newTableHeight1String = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "table" }, "height");
    const newTableHeight1 = parseInt(newTableHeight1String, 10);
    expect(newTableHeight1).to.greaterThan(oldTableHeight);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115130] ODP: Cell fill color", async ({ I, presentation }) => {

    const initialColor = "rgb(230, 230, 230)";
    const redColor = "rgb(255, 0, 0)";

    await I.loginAndOpenDocument("media/files/testfile_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });
    I.waitForAndClick(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(5) > td:nth-child(2)");
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(5) > td:nth-child(2)" }, "background-color")).to.equal(initialColor);

    // set fillcolor
    I.clickOptionButton("table/fillcolor", "red");

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    I.waitForVisible({ docs: "control", key: "table/fillcolor", state: "red" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(5) > td:nth-child(2)" }, "background-color")).to.equal(redColor);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });
    I.waitForAndClick(".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(5) > td:nth-child(2)");
    I.wait(0.5);

    // check fillcolor
    I.waitForVisible({ docs: "control", key: "table/fillcolor", state: "red" });
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(5) > td:nth-child(2)" }, "background-color")).to.equal(redColor);

    I.closeDocument();

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115131A] ODP: Table borders, only inner borders", async ({ I, presentation }) => {

    const existingBorder = "1px solid rgb(255, 255, 255)";
    const notExistingBorder = "1px dotted rgba(0, 0, 0, 0)";

    await I.loginAndOpenDocument("media/files/testfile_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check initial borders
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(existingBorder);

    I.clickOptionButton("table/cellborder", "inner");

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    // close the dropdown
    I.click({ css: ".popup-footer > button", withText: "Close" });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(notExistingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(existingBorder);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(notExistingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(existingBorder);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115131B] ODP: Table borders, only inner horizontal borders", async ({ I, presentation }) => {

    const existingBorder = "1px solid rgb(255, 255, 255)";
    const notExistingBorder = "1px dotted rgba(0, 0, 0, 0)";

    await I.loginAndOpenDocument("media/files/testfile_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check initial borders
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(existingBorder);

    I.clickOptionButton("table/cellborder", "top-bottom-insideh");

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    // close the dropdown
    I.click({ css: ".popup-footer > button", withText: "Close" });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(notExistingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(notExistingBorder);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(existingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(notExistingBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(notExistingBorder);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115132] ODP: Border width", async ({ I, presentation }) => {

    const thinBorder = "1px solid rgb(255, 255, 255)";
    const thickBorder = "8px solid rgb(255, 255, 255)";

    await I.loginAndOpenDocument("media/files/testfile_2_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    I.click({ presentation: "slidepane", id: "slide_1", selected: true });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // initial state
    I.waitForVisible({ docs: "control", key: "table/borderwidth", state: "0.1" });

    // check initial borders
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(thinBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(thinBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(thinBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(thinBorder);

    // change the border width
    I.clickOptionButton("table/borderwidth", 6);

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    I.waitForVisible({ docs: "control", key: "table/borderwidth", state: "6" });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(thickBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(thickBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(thickBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(thickBorder);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.click({ presentation: "slidepane", id: "slide_1", selected: true });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    I.waitForVisible({ docs: "control", key: "table/borderwidth", state: "6" });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-top")).to.equal(thickBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-bottom")).to.equal(thickBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-left")).to.equal(thickBorder);
    expect(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2)" }, "border-right")).to.equal(thickBorder);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115133] ODP: Change table width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.click({ presentation: "slidepane", id: "slide_1", selected: true });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    const oldTableWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table" }, "width");
    const oldTableWidth = parseInt(oldTableWidthString, 10);

    I.click({ presentation: "slide", id: "slide_1", find: ".drawing tr:nth-child(1) td:nth-child(1)" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" }, -100, 0);

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    const newTableWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table" }, "width");
    const newTableWidth = parseInt(newTableWidthString, 10);
    expect(newTableWidth).to.be.below(oldTableWidth);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.click({ presentation: "slidepane", id: "slide_1", selected: true });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    const newTableWidthString1 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table" }, "width");
    const newTableWidth1 = parseInt(newTableWidthString1, 10);
    expect(newTableWidth1).to.be.below(oldTableWidth);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115134] ODP: Change row height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_grid.odp");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    // check the initial height of row 2
    const oldTableRow2HeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(2) > td:nth-child(2)" }, "height");
    const oldTableRow2Height = parseInt(oldTableRow2HeightString, 10);
    expect(oldTableRow2Height).to.be.equal(oldTableRow2Height);

    // check the initial height of row 3
    const oldTableRow1HeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(3) > td:nth-child(3)" }, "height");
    const oldTableRow1Height = parseInt(oldTableRow1HeightString, 10);
    expect(oldTableRow1Height).to.be.equal(oldTableRow1Height);

    I.click({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(2) > td:nth-child(2) .p" });

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(2) > td:nth-child(2)> div > .resize.bottom" });

    I.dragMouseOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(2) > td:nth-child(2)> div > .resize.bottom" }, 0, -50);

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    // check the row 2 height
    const newTableRow2HeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(2) > td:nth-child(2)" }, "height");
    const newTableRow2Height = parseInt(newTableRow2HeightString, 10);
    expect(newTableRow2Height).to.be.below(oldTableRow2Height);

    // check the row 3 height
    const newTableRow1HeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(3) > td:nth-child(2)" }, "height");
    const newTableRow1Height = parseInt(newTableRow1HeightString, 10);
    expect(newTableRow1Height).to.be.equal(oldTableRow1Height);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the height of row 2
    const reloadTableRow2HeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(2) > td:nth-child(2)" }, "height");
    const reloadTableRow2Height = parseInt(reloadTableRow2HeightString, 10);
    expect(reloadTableRow2Height).to.be.below(oldTableRow2Height);

    // check the height of row 3
    const reloadTableRow1HeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(3) > td:nth-child(2)" }, "height");
    const reloadTableRow1Height = parseInt(reloadTableRow1HeightString, 10);
    expect(reloadTableRow1Height).to.be.equal(oldTableRow1Height);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
