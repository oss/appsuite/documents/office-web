/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Presentation > Context menu > Shape");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C128919] Delete shape via context menu", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    // Front Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "FRONT" });
    // Middle Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "MIDDLE" });
    // Back Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "BACK" });
    I.clickOnElement({ presentation: "slide", find: ".drawing", withText: "BACK" }, { button: "right", point: "bottom" });

    I.waitForContextMenuVisible();

    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    // the deleted drawing not shown
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "FRONT" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "MIDDLE" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing .textframe span", withText: "BACK" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "FRONT" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "MIDDLE" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing .textframe span", withText: "BACK" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C128920] Reorder shape via context menu", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    // Front Shape
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });
    // Middle Shape
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });
    // Back Shape
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" }, { button: "right", point: "bottom" });

    I.waitForContextMenuVisible();

    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/order", inside: "context-menu", caret: true });

    I.waitNumberOfVisibleElements({ css: ".popup-content div[data-section='reorder'] .group" }, 4);
    I.click({ css: ".popup-content  div[data-section='reorder'] .group", withText: "Bring forward" });
    I.waitForContextMenuInvisible();
    I.waitForChangesSaved();

    // check the result
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "FRONT" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "MIDDLE" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the result again
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "FRONT" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "MIDDLE" });

    I.closeDocument();
}).tag("stable");

Scenario("[C128940] [C128941] Group shapes / Ungroup shapes via context menu", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    // Front Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "FRONT" });
    // Middle Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "MIDDLE" });
    // Back Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "BACK" });

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickOnElement({ presentation: "slide", find: ".drawing" }, { button: "right", point: "bottom" });

    I.waitForContextMenuVisible();

    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/group", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });
    // click on 'Group'
    I.click({ docs: "control", key: "drawing/group", inside: "context-menu" });

    I.waitForContextMenuInvisible();
    I.waitForChangesSaved();

    // one drawing is shown and is selected
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='group']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    // check the result again one drawing is shown
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='group']" }, 1);

    I.rightClick({ presentation: "slide", find: "> .drawing[data-type='group'] .drawing:last-child" });

    I.waitForContextMenuVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/ungroup", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });
    // click on 'Ungroup'
    I.click({ docs: "control", key: "drawing/ungroup", inside: "context-menu" });

    I.waitForContextMenuInvisible();
    I.waitForChangesSaved();

    // 3 drawings are shown and are selected
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    // Front Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "FRONT" });
    // Middle Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "MIDDLE" });
    // Back Shape
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe span", withText: "BACK" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // 3 drawings are shown
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    I.closeDocument();
}).tag("stable");
