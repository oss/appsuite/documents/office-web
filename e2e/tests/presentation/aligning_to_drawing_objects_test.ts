/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Insert > Shape formatting > Alignment guides > Aligning to drawing objects");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C274371][C274374] Align left border of a shape to left border of a drawing object / Align top border of a shape to top border of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-vertical.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imagePos = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imagePos.left).to.be.within(120, 122);
    expect(imagePos.top).to.be.within(37, 38);
    expect(imagePos.width).to.be.within(249, 251);
    expect(imagePos.height).to.be.within(249, 251);

    // initial shape values
    const shapePos = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapePos.left).to.be.within(192, 194);
    expect(shapePos.top).to.be.within(379, 381);
    expect(shapePos.width).to.be.within(214, 216);
    expect(shapePos.height).to.be.within(147, 149);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const horizontalMove = shapePos.left - imagePos.left;
    const verticalMove = shapePos.top - imagePos.top;

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - horizontalMove, y - verticalMove); // move to the left and upward

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "vert" });
    I.waitForVisible({ presentation: "guideline", type: "horz" });

    // check the red line position on vertical
    const red_line_v = await I.grabCssRectFrom({ presentation: "guideline", type: "vert" });
    expect(red_line_v.left).to.be.within(125, 129);
    expect(red_line_v.top).to.be.within(897, 899);

    // check the red line position on horizontal
    const red_line_h = await I.grabCssRectFrom({ presentation: "guideline", type: "horz" });
    expect(red_line_h.left).to.be.within(118, 122);
    expect(red_line_h.top).to.be.within(904, 906);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "vert", hidden: true });
    I.seeElementInDOM({ presentation: "guideline", type: "horz", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new values left and top are the same

    const shapePosAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapePosAfter.left).to.equals(imagePos.left);
    expect(shapePosAfter.top).to.equals(imagePos.top);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // values after reopen left and top are the same
    const shapePosReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapePosReopen.left).to.equals(imagePos.left);
    expect(shapePosReopen.top).to.equals(imagePos.top);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C274372] Align vertical middle of a shape to vertical middle of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-vertical.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.left).to.be.within(192, 194);
    expect(shapeRect.top).to.be.within(379, 381);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.height).to.be.within(147, 149);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const horizontalMove = (imageRect.left + 0.5 * imageRect.width) - (shapeRect.left + 0.5 * shapeRect.width);
    const difference_width = (0.5 * imageRect.width) - (0.5 * shapeRect.width);

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x + horizontalMove - difference_width + 10, y); // move horizontal

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "vert" });

    // check the red line position on vertical
    const red_line_v = await I.grabCssRectFrom({ presentation: "guideline", type: "vert" });
    expect(red_line_v.left).to.be.within(256, 261);
    expect(red_line_v.top).to.be.within(897, 900);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "vert", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new height value
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.left).to.be.within(137, 140);// left is changed
    expect(shapeRectAfter.top).to.be.within(365, 368);// top is changed
    expect(shapeRectAfter.width).to.equals(shapeRect.width);
    expect(shapeRectAfter.height).to.equals(shapeRect.height);

    // check the middel
    expect(shapeRectAfter.left + 0.5 * shapeRectAfter.width + 0.5).to.equals(imageRect.left + 0.5 * imageRect.width);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // check values after reopen
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });

    expect(shapeRectReopen.left).to.equals(shapeRectAfter.left);// left is same
    expect(shapeRectReopen.top).to.equals(shapeRectAfter.top);// top is same

    // check the middel
    expect(shapeRectAfter.left + 0.5 * shapeRectAfter.width + 0.5).to.equals(imageRect.left + 0.5 * imageRect.width);

    I.closeDocument();
}).tag("stable");

Scenario("[C274373][C274376] Align right border of a shape to right border of a drawing object / Align bottom border of a shape to bottom border of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-vertical.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.left).to.be.within(192, 194);
    expect(shapeRect.top).to.be.within(379, 381);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.height).to.be.within(147, 149);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const verticalMove = shapeRect.top - (imageRect.top + (imageRect.height - shapeRect.height));
    const horizontalMove = shapeRect.left - (imageRect.left + (imageRect.width - shapeRect.width));

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - horizontalMove, y - verticalMove); // move to the left and upward

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "vert" });
    I.waitForVisible({ presentation: "guideline", type: "horz" });

    // check the red line position on vertical
    const red_line_v = await I.grabCssRectFrom({ presentation: "guideline", type: "vert" });
    const leftLineV = [386, 387, 388, 391, 392, 393];// Different pixel value

    expect(red_line_v.top).to.be.within(897, 899);
    expect(red_line_v.left).to.be.oneOf(leftLineV);

    // check the red line position on horizontal
    const red_line_h = await I.grabCssRectFrom({ presentation: "guideline", type: "horz" });
    const topLineH = [1165, 1166, 1167, 1170, 1171, 1172];// Different pixel value

    expect(red_line_h.top).to.be.oneOf(topLineH);
    expect(red_line_h.left).to.be.within(118, 122);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "vert", hidden: true });
    I.seeElementInDOM({ presentation: "guideline", type: "horz", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new values right and bottom are the same
    const difference_width = imageRect.width - shapeRect.width;// values right
    const difference_height = imageRect.height - shapeRect.height;// values bottom

    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });

    expect(shapeRectAfter.left - difference_width).to.equals(imageRect.left);
    expect(shapeRectAfter.top - difference_height).to.equals(imageRect.top);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // new values right and bottom are the same after reopen
    const difference_width_reopen = imageRect.width - shapeRect.width;// values right
    const difference_height_reopen = imageRect.height - shapeRect.height;// values bottom

    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });

    expect(shapeRectReopen.left - difference_width_reopen).to.equals(imageRect.left);
    expect(shapeRectReopen.top - difference_height_reopen).to.equals(imageRect.top);

    I.closeDocument();
}).tag("stable");

Scenario("[C274375] Align horizontal middle of a shape to horizontal middle of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-horizontal.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);


    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.height).to.be.within(151, 153);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.left).to.be.within(559, 561);
    expect(shapeRect.top).to.be.within(119, 121);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement middel of
    const verticalMove = (imageRect.top + 0.5 * imageRect.height) - (shapeRect.top + 0.5 * shapeRect.height);
    const difference_height = (0.5 * imageRect.height) - (0.5 * shapeRect.height);

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the top resizers of the shape, so that the shape can be moved to top
    I.moveMouseTo(x, y - verticalMove - difference_height); // move vertical

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "horz" });

    // check the red line position on hortzontal
    const red_line_h = await I.grabCssRectFrom({ presentation: "guideline", type: "horz" });
    expect(red_line_h.top).to.be.within(1035, 1038);
    expect(red_line_h.left).to.be.within(118, 122);
    // expect(red_line_h.top).to.equals(imageRect.top + 0.5 * imageRect.height);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "horz", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new values
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.left).to.be.within(559, 562);
    expect(shapeRectAfter.top).to.be.within(86, 88);

    // check the middel
    expect(shapeRectAfter.top + 0.5 * shapeRectAfter.height).to.equals(imageRect.top + 0.5 * imageRect.height);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // values after reopen
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectReopen.left).to.equals(shapeRectAfter.left);
    expect(shapeRectReopen.top).to.equals(shapeRectAfter.top);

    // check the middel
    expect(shapeRectAfter.top + 0.5 * shapeRectAfter.height).to.equals(imageRect.top + 0.5 * imageRect.height);

    I.closeDocument();
}).tag("stable");

Scenario("[C274377] Resize left border of a shape to left border of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-vertical.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.left).to.be.within(192, 194);
    expect(shapeRect.top).to.be.within(379, 381);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.height).to.be.within(147, 149);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const horizontalMove = shapeRect.left - imageRect.left;

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y); // clicking into the left resizers of the shape, so that the shape can be moved to left
    I.moveMouseTo(x - horizontalMove, y); // move to the left

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "vert" });

    // check the red line position on vertical
    const red_line_v = await I.grabCssRectFrom({ presentation: "guideline", type: "vert" });
    expect(red_line_v.top).to.be.within(897, 899);
    expect(red_line_v.left).to.be.within(125, 129);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "vert", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new values left is the same
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.left).to.equals(imageRect.left);
    // check width after change
    expect(shapeRectAfter.width).to.be.within(286, 288);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // values after reopen left is the same
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectReopen.left).to.equals(imageRect.left);
    // check width after reopen
    expect(shapeRectReopen.width).to.equals(shapeRectAfter.width);

    I.closeDocument();
}).tag("stable");

Scenario("[C274379] Resize right border of a shape to right border of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-vertical.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.left).to.be.within(192, 194);
    expect(shapeRect.top).to.be.within(379, 381);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.height).to.be.within(147, 149);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const horizontalMove = shapeRect.left - (imageRect.left + (imageRect.width - shapeRect.width));

    // getting the position of the "right" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    I.pressMouseButtonAt(x, y); // clicking into the right resizers of the shape, so that the shape can be moved to right
    I.moveMouseTo(x - horizontalMove, y); // move to the right

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "vert" });

    // check the red line position on vertical
    const red_line_v = await I.grabCssRectFrom({ presentation: "guideline", type: "vert" });
    const leftLineV = [386, 387, 388, 391, 392, 393];// Different pixel value

    expect(red_line_v.top).to.be.within(897, 899);
    expect(red_line_v.left).to.be.oneOf(leftLineV);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "vert", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    // new width value
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.height).to.equals(shapeRect.height);
    expect(shapeRectAfter.width).to.be.within(177, 179);// width is changed
    expect(shapeRectAfter.left).to.equals(shapeRect.left);
    expect(shapeRectAfter.top).to.equals(shapeRect.top);

    // check right is the same
    const difference_width = imageRect.width - shapeRectAfter.width;// values right
    expect(shapeRectAfter.left - difference_width).to.equals(imageRect.left);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // check width after reopen
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectReopen.width).to.equals(shapeRectAfter.width);
    // check right is the same
    const difference_width_after_reopen = imageRect.width - shapeRectReopen.width;// values right
    expect(shapeRectAfter.left - difference_width_after_reopen).to.equals(imageRect.left);

    I.closeDocument();
}).tag("stable");

Scenario("[C274380] Resize top border of a shape to top border of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-horizontal.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.height).to.be.within(151, 153);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.left).to.be.within(559, 561);
    expect(shapeRect.top).to.be.within(119, 121);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='t']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const verticalMove = shapeRect.top - imageRect.top;

    // getting the position of the "top" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='t']" });

    I.pressMouseButtonAt(x, y); // clicking into the top resizers of the shape, so that the shape can be moved to top
    I.moveMouseTo(x, y - verticalMove); // move to the top

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "horz" });

    // check the red line position on vertical
    const red_line_h = await I.grabCssRectFrom({ presentation: "guideline", type: "horz" });

    expect(red_line_h.top).to.be.within(904, 907);
    expect(red_line_h.left).to.be.within(118, 122);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "horz", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='t']" });

    // new values top is the same
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.top).to.equals(imageRect.top);
    // check height after change
    expect(shapeRectAfter.height).to.be.within(233, 235);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // values after reopen top is the same
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectReopen.top).to.equals(imageRect.top);
    // check height after reopen
    expect(shapeRectReopen.height).to.equals(shapeRectAfter.height);

    I.closeDocument();
}).tag("stable");

Scenario("[C274381] Resize bottom border of a shape to bottom border of a drawing object ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-horizontal.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.height).to.be.within(151, 153);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.left).to.be.within(559, 561);
    expect(shapeRect.top).to.be.within(119, 121);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const verticalMove = shapeRect.top - (imageRect.top + (imageRect.height - shapeRect.height));

    // getting the position of the "bottom" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" });

    I.pressMouseButtonAt(x, y); // clicking into the bottom resizers of the shape, so that the shape can be moved to bottom
    I.moveMouseTo(x, y - verticalMove); // move to the bottom

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "horz" });

    // check the red line position on vertical
    const red_line_h = await I.grabCssRectFrom({ presentation: "guideline", type: "horz" });
    const topLineH = [1165, 1166, 1167, 1170, 1171, 1172];// Different pixel value

    expect(red_line_h.top).to.be.oneOf(topLineH);
    expect(red_line_h.left).to.be.within(118, 122);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "horz", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" });

    // new height value
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.height).to.be.within(167, 169);// height is changed
    expect(shapeRectAfter.width).to.equals(shapeRect.width);
    expect(shapeRectAfter.left).to.equals(shapeRect.left);
    expect(shapeRectAfter.top).to.equals(shapeRect.top);

    // check bottom is the same
    const difference_height = imageRect.height - shapeRectAfter.height;// values bottom
    expect(shapeRectAfter.top - difference_height).to.equals(imageRect.top);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // check height after reopen
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectReopen.height).to.equals(shapeRectAfter.height);
    // check bottom is the same
    const difference_height_after_reopen = imageRect.width - shapeRectReopen.height;// values right
    expect(shapeRectAfter.top - difference_height_after_reopen).to.equals(imageRect.top);

    I.closeDocument();
}).tag("stable");

Scenario("[C274382] Align a shape centred over a drawing object", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-two-drawing-objects-alignment-horizontal.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "OX2" }, 1);

    // initial image values
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='image']" });
    expect(imageRect.left).to.be.within(120, 122);
    expect(imageRect.top).to.be.within(37, 38);
    expect(imageRect.width).to.be.within(249, 251);
    expect(imageRect.height).to.be.within(249, 251);

    // initial shape values
    const shapeRect = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRect.height).to.be.within(151, 153);
    expect(shapeRect.width).to.be.within(214, 216);
    expect(shapeRect.left).to.be.within(559, 561);
    expect(shapeRect.top).to.be.within(119, 121);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" });

    // dont see the red line (guidelines) before moving the mouse
    I.dontSeeElementInDOM({ presentation: "guideline" });

    // calculate movement
    const verticalMove = shapeRect.top - imageRect.top - 10;
    const horizontalMove = shapeRect.left - (imageRect.left + (shapeRect.width / 2));

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - horizontalMove, y - verticalMove); // move to the middle and upward

    // red line visible
    I.waitForVisible({ presentation: "guideline", type: "horz" });
    I.waitForVisible({ presentation: "guideline", type: "vert" });

    // check the red line position on horizontal
    const red_line_h = await I.grabCssRectFrom({ presentation: "guideline", type: "horz" });
    expect(red_line_h.top).to.be.within(904, 906);
    expect(red_line_h.left).to.be.within(118, 121);

    // check the red line position on vertical
    const red_line_v = await I.grabCssRectFrom({ presentation: "guideline", type: "vert" });
    const leftLineV = [255, 256, 257, 259, 260, 261];// Different pixel value
    expect(red_line_v.top).to.be.within(897, 900);
    expect(red_line_v.left).to.be.oneOf(leftLineV);

    I.wait(1); // mouse move handling

    I.releaseMouseButton();

    // see the red line with class .hidden after release the mouse
    I.seeElementInDOM({ presentation: "guideline", type: "horz", hidden: true });
    I.seeElementInDOM({ presentation: "guideline", type: "vert", hidden: true });

    I.waitForChangesSaved();

    // the shape stay selected
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new height value
    const shapeRectAfter = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectAfter.height).to.equals(shapeRect.height);
    expect(shapeRectAfter.width).to.equals(shapeRect.width);
    expect(shapeRectAfter.left).to.be.within(245, 247);

    // check top is the same
    expect(shapeRectAfter.top).to.equals(imageRect.top);

    // check the middle
    expect(shapeRectAfter.left).to.equals(imageRect.left + (imageRect.width / 2));// values middle


    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, 1);

    // check top is the same
    const shapeRectReopen = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" });
    expect(shapeRectReopen.top).to.equals(imageRect.top);

    // check left after reopen
    expect(shapeRectReopen.left).to.equals(shapeRectAfter.left);
    // check the middle
    expect(shapeRectReopen.left).to.equals(imageRect.left + (imageRect.width / 2));// values middle

    I.closeDocument();
}).tag("stable");
